package co.gov.igac.snc.web.controller.test.integration;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.*;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;


/**
 * 
 * @author franz.gamba
 *
 */

public class ConsultaRestServiceUnitTest extends AbstractRestServiceTest {
	
	
	public static Logger LOGGER = LoggerFactory
			.getLogger(ConsultaRestServiceUnitTest.class);

	@Test
	public void testMostrarDatosConsulta() {
		LOGGER.debug("testMostrarDatosConsulta");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = HOST + "sigc-web-public/rest/consulta/predio/080010109000003910014000000000.xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testMostrarDatosConsultaJSON() {
		LOGGER.debug("testMostrarDatosConsultaJSON");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = HOST + "sigc-web-public/rest/consulta/predio/080010109000003910014000000000.json";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testMostrarPredioPorMunicipioYMatricula() {
		LOGGER.debug("testMostrarPredioPorMunicipioYMatricula");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = HOST + "sigc-web-public/rest/consulta/predio" +
					"/municipio/25754/matricula/050-40153055-PRUEBA.xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testMostrarPredioPorMunicipioYDireccion() {
		LOGGER.debug("testMostrarPredioPorMunicipioYDireccion");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String direccion = "carrera 2 numero 30 25 IN 11 AP 103";
			
			String encodedDir = URLEncoder.encode(direccion, "UTF-8");
			LOGGER.debug("direccion:"+direccion);
			LOGGER.debug("encodedDir:"+encodedDir);

			String url = HOST + "sigc-web-public/rest/consulta/predio" +
					"/municipio/25754/direccion/"+encodedDir+".xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testMostrarFachadaPredioPoreId() {
		LOGGER.debug("testMostrarFachadaPredioPoreId");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String codigoPredio = "257540102000011650001500000032"; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/fachada/"+codigoPredio+".xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	
	@Test
	public void testMostrarFachadaPredioPoreIdJSON() {
		LOGGER.debug("testMostrarFachadaPredioPoreIdJSON");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String codigoPredio = "257540216041711057933105117115"; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/fachada/"+codigoPredio+".json";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	/**
	 * La prueba valida el caso cuando no se encuentran datos para 
	 * el filtro solicitado
	 * 
	 * @author juan.mendez
	 */
	@Test
	public void testMostrarFachadaPredioPoreIdErrorJSON() {
		LOGGER.debug("testMostrarFachadaPredioPoreIdErrorJSON");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String codigoPredio = "257540216041711057933105117115"; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/fachada/"+codigoPredio+".json";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			boolean isError = validateServerResponse(resultado, false);
			assertTrue(isError, "Debería Lanzar Error");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test(threadPoolSize = 1, invocationCount = 1, timeOut = 10000)
	//@Test
	public void testDetallesPrediosSeleccion() {
		LOGGER.debug("testDetallesPrediosSeleccion");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String numerosPrediales = "257540216041711057932105117115," +
					"257540102000011650001500000032," +
					"257520102000011650001500000032," +
					"257540102000011650001500000033"; 
			
			String url = HOST + "sigc-web-public/rest/consulta/predio" +
					"/numeros/.xml?codigos="+ numerosPrediales ;
			
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	//@Test(threadPoolSize = 200, invocationCount = 1000, timeOut = 500)
	@Test
	public void testDetallesPrediosSeleccionJSON() {
		LOGGER.debug("testDetallesPrediosSeleccionJSON");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String numerosPrediales = "257540216041711057933105117115," +
					"257540102000011650001500000032," +
					"257540102000011650001500000033,"; 
			
			String url = HOST + "sigc-web-public/rest/consulta/predio" +
					"/numeros/.json?codigos="+ numerosPrediales ;
			
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testConsultarEstadisticasPredio() {
		LOGGER.debug("testConsultarEstadisticasPredio");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String depto = "08"; 
			String municipio = "08001"; 
			String tipo = "00"; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/concentracionDePropiedad/.xml?depto="+
					depto +"&municipio="+ municipio +"&tipo="+ tipo;
			/*String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/.xml?";*/
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testConsultarEstadisticasPredioDatosVacio() {
		LOGGER.debug("testConsultarEstadisticasPredio");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String depto = ""; 
			String municipio = ""; 
			String tipo = ""; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/.xml?depto="+ depto +"&municipio="+ municipio +"&tipo="+ tipo;
						LOGGER.debug("url:" + url);
			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * @author juan.mendez
	 */
	@Test
	public void testConsultarEstadisticasPredioDatosVacioJSON() {
		LOGGER.debug("testConsultarEstadisticasPredio");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String depto = ""; 
			String municipio = ""; 
			String tipo = ""; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/.json?depto="+ depto +"&municipio="+ municipio +"&tipo="+ tipo;
			/*String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/.xml?";*/
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
			
			/////////////////////////////////////////////////////////////////////////////////
			//Validación del Resultado entregado en formato JSON 
			/////////////////////////////////////////////////////////////////////////////////
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> resultMap = mapper.readValue(resultado,	new TypeReference<Map<String, Object>>() {});
			assertNotNull(resultMap);
			assertEquals(resultMap.size(), 1 );
			assertEquals(resultMap.keySet().contains("respuesta"), true );
			
			@SuppressWarnings("unchecked")
			Map<String, Object> respuesta = (Map<String, Object> )resultMap.get("respuesta"); 
			assertNotNull(respuesta);
			assertEquals(respuesta.size(), 2 );
			assertEquals(respuesta.keySet().contains("documentos"), true );
			assertEquals(respuesta.keySet().contains("mensaje"), true );
			
			//LOGGER.debug("documentos:" + respuesta.get("documentos"));
			
			@SuppressWarnings("unchecked")
			List<Object> documentos = (List<Object> )respuesta.get("documentos"); 
			assertNotNull(documentos);
			assertEquals(documentos.size(), 5 );
			LOGGER.debug("documentos:" + documentos);
			LOGGER.debug("documento:" + documentos.get(0));
			/////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testConsultarEstadisticasPredioSinResultadosJSON() {
		LOGGER.debug("testConsultarEstadisticasPredioSinResultadosJSON");
		try {
			HttpClient httpclient = new HttpClient();	
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String depto = "02"; 
			String municipio =""; 
			String tipo = "00"; 
			
			String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/concentracionDePropiedad/.json?depto="+ depto +"&municipio="+ municipio +"&tipo="+ tipo;
			/*String url = HOST + "sigc-web-public/rest/consulta" +
					"/predio/estadisticas/.xml?";*/
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:"+ resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
			/////////////////////////////////////////////////////////////////////////////////
			//Validación del Resultado entregado en formato JSON 
			/////////////////////////////////////////////////////////////////////////////////
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> resultMap = mapper.readValue(resultado,	new TypeReference<Map<String, Object>>() {});
			assertNotNull(resultMap);
			assertEquals(resultMap.size(), 1 );
			assertEquals(resultMap.keySet().contains("respuesta"), true );
			
			@SuppressWarnings("unchecked")
			Map<String, Object> respuesta = (Map<String, Object> )resultMap.get("respuesta"); 
			assertNotNull(respuesta);
			assertEquals(respuesta.size(), 2 );
			assertEquals(respuesta.keySet().contains("documentos"), true );
			assertEquals(respuesta.keySet().contains("mensaje"), true );
			
			//LOGGER.debug("documentos:" + respuesta.get("documentos"));
			
			@SuppressWarnings("unchecked")
			List<Object> documentos = (List<Object> )respuesta.get("documentos"); 
			assertNotNull(documentos);
			assertEquals(documentos.size(), 0 );
			/////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
}
