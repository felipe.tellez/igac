package co.gov.igac.snc.web.controller.test.integration;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/***
 * 
 * @author juan.mendez
 * 
 */
public class GeneralRestServiceUnitTest extends AbstractRestServiceTest {

	public static Logger LOGGER = LoggerFactory.getLogger(GeneralRestServiceUnitTest.class);
	

	/**
	 * Prueba consulta listado de tareas a través de REST 
	 */
	//@Test(threadPoolSize = 100, invocationCount = 10000, timeOut = 1000)
	@Test
	public void testObtenerTareas() {
		// test = GeneralRestServiceUnitTest#testObtenerTareas
		LOGGER.debug("testObtenerTareas");
		try {
			HttpClient httpclient = new HttpClient(); 
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER, PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = HOST + "sigc-web-public/rest/general/tareas/geograficas/.xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:" + resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			//assertNotNull(resultado);
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	
	
	@Test
	public void listarTareasGeograficas3() {
		
		LOGGER.debug("testObtenerTareas");
		try {
			HttpClient httpclient = new HttpClient(); 
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruatlantico17", "igac2016.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = HOST + "snc/rest/general/tareas/geograficas3/.xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:" + resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			//assertNotNull(resultado);
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
        
        @Test
	public void listarTareasGeograficas2() {
		// test = GeneralRestServiceUnitTest#testObtenerTareas
		LOGGER.debug("testObtenerTareas");
		try {
			HttpClient httpclient = new HttpClient(); 
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruatlantico30", "igac2014.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = HOST + "snc/rest/general/tareas/geograficas2/.xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:" + resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			//assertNotNull(resultado);
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

}
