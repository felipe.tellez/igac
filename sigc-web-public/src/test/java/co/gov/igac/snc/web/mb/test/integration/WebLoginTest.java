package co.gov.igac.snc.web.mb.test.integration;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/***
 * 
 * @author juan.mendez
 * 
 */
public class WebLoginTest extends BaseSeleniumTest {

	public static Logger LOGGER = LoggerFactory.getLogger(WebLoginTest.class);

	// Prueba de Carga
	// @Test(threadPoolSize = 20, invocationCount = 500, timeOut = 20000)
	@Test
	public void test01LoginOk() {
		LOGGER.debug("test01LoginOk");
		try {
			selenium.getEval("window.moveTo(1,1); window.resizeTo(1021,737);");
			selenium.open("/sigc-web-public/login.jsf");
			selenium.select("id=j_username", "label=prusoacha07 Radicador");
			selenium.click("name=j_idt42");
			selenium.waitForPageToLoad("30000");
			assertTrue(selenium.isTextPresent("Salir"));
			//selenium.click("css=#ui-active-menuitem > span.wijmo-wijmenu-text > span.wijmo-wijmenu-text");
			selenium.open("/sigc-web-public/logout");
			selenium.waitForPageToLoad("30000");
			assertTrue(selenium.isTextPresent("El Sistema Nacional Catastral es el instrumento que permite producir,"));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test(threadPoolSize = 1, invocationCount = 10, timeOut = 20000)
	//@Test
	public void test02LoginOk() {
		LOGGER.debug("test02LoginOk");
		try {
			selenium.open("/sigc-web-public/login.jsf");
			selenium.select("id=j_username", "label=prusoacha07 Radicador");
			selenium.click("name=j_idt42");
			selenium.waitForPageToLoad("30000");
			assertTrue(selenium.isTextPresent("Conservación"));
			assertTrue(selenium.isTextPresent("Actualización"));
			assertTrue(selenium.isTextPresent("Salir"));
			//selenium.click("css=#ui-active-menuitem > span.wijmo-wijmenu-text > span.wijmo-wijmenu-text");
			selenium.open("/sigc-web-public/logout");
			selenium.waitForPageToLoad("30000");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
}
