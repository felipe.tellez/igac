package co.gov.igac.snc.util.unit;

import co.gov.igac.snc.util.Constantes;
import static org.testng.Assert.fail;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/**
 * Ejemplo de prueba unitaria utilizando TestNG
 * Nota: Esta prueba es Ejecutada por Maven
 */

public class LdapTest {

	private static final Logger logger = LoggerFactory
			.getLogger(LdapTest.class);

	private static final String USERNAME = "pruatlantico3";// "pruatlantico2";//
	private static final String PASSWORD = "igac2014.";
	private static final String DOMAIN = "prudcigac.local";
	private static final String HOST = "172.17.2.1";
	private static final String DN = Constantes.LDAP_DC;

	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */
	public LdapTest() {
		// super(testName);
	}

	@BeforeSuite
	public void beforeSuite() {
		logger.debug("beforeSuite");
	}

	@AfterSuite
	public void afterSuite() {
		logger.debug("afterSuite");
	}

	/**
	 * Rigourous Test :-)
	 */
	//@Test(threadPoolSize = 10, invocationCount = 100, timeOut = 1000)
	@Test
	public void ldapConnection() {
		logger.debug("ldapConnection");
		Ldap ldap = new Ldap();

		try {
			Attributes att = ldap.authenticateUser(USERNAME, PASSWORD, DOMAIN,
					HOST, DN);
			if (att == null) {
				logger.error("Sorry your use is invalid or password incorrect");
				fail("Sorry your use is invalid or password incorrect");
			} else {
				//String s = att.get("givenName").toString();
				//logger.debug("GIVEN NAME=" + s);
			}
		} catch (NamingException e) {
			logger.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}

	}
}
