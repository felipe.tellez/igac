package co.gov.igac.snc.web.ws.consultacatastralservice;

import static org.testng.Assert.fail;
import static org.testng.Assert.assertNotNull;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import co.gov.igac.data.converted.xsd.ConsultaIGACParams;
import co.gov.igac.snc.web.controller.test.integration.AbstractRestServiceTest;


/**
 * 
 * @author franz.gamba
 *
 */

public class ConsultaWebServiceUnitTest extends AbstractRestServiceTest {
	
	
	public static Logger LOGGER = LoggerFactory
			.getLogger(ConsultaWebServiceUnitTest.class);

	//@Test(threadPoolSize = 10, invocationCount = 10, timeOut = 1000)
	@Test
	public void testConsultaCatastral() {
		LOGGER.debug("testConsultaCatastral");
		try {
			ConsultaCatastralService_Service facade = new ConsultaCatastralService_Service();
			ConsultaCatastralService service = facade.getConsultaCatastralServiceSoap11();

			ConsultaCatastralServiceRequest request = new ConsultaCatastralServiceRequest();
			ConsultaIGACParams params = new ConsultaIGACParams();
			co.gov.igac.data.converted.xsd.ObjectFactory factory = new co.gov.igac.data.converted.xsd.ObjectFactory();
                        
            //Crea los valores para el test 
            JAXBElement<String> tipoNumeroPredial= factory.createConsultaIGACParamsTipoNumeroPredial("nuevo");
			JAXBElement<String> numeroPredial = factory.createConsultaIGACParamsNumeroPredial("231820100000002700001000000000");
			//JAXBElement<String> numeroPredial = factory.createConsultaIGACParamsNumeroPredial("080010109000003910014000000000");
			//JAXBElement<String> numeroPredial = factory.createConsultaIGACParamsNumeroPredial("0800101090000039100");
			
			//JAXBElement<String> numeroDocumento = factory.createConsultaIGACParamsNumeroDocumento("2721292");
			//JAXBElement<String> tipoDocumento = factory.createConsultaIGACParamsTipoDocumento("CC");						
			JAXBElement<String> numeroPagina = factory.createConsultaIGACParamsPagina("1");			
                        
                        //Establece los valores dentro del objeto parametros
                        params.setTipoNumeroPredial(tipoNumeroPredial);
                        params.setNumeroPredial(numeroPredial);
                        //params.setNumeroDocumento(numeroDocumento);
                        //params.setTipoDocumento(tipoDocumento);
                        params.setPagina(numeroPagina);                     

                        //Establece los parametros en el requerimiento
			request.setParams(params);
			
                        //Hace el requerimiento al servicio
			ConsultaCatastralServiceResponse response = service.consultaCatastralService(request);
			//assertNotNull(response.getReturn());
			LOGGER.debug("response:"+response);
			//assertNotNull(response.getReturn().getPredio(), "No retorno predios");
			LOGGER.debug("Cantidad de predios:"+response.getReturn());                        
			LOGGER.debug("Cantidad de predios:"+response.getReturn().getPredio().size());
                        LOGGER.debug(String.valueOf(response.getReturn().getPredio().get(0).getConstruccion().size()));
                        LOGGER.debug(String.valueOf(response.getReturn().getPredio().get(0).getPropietario().get(0).getNombre().getValue()));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	
		
}
