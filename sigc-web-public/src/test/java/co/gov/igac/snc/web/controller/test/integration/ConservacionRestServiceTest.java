package co.gov.igac.snc.web.controller.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.auth.AuthScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.CommonsClientHttpRequestFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;

/**
 * Ejemplo prueba de Integración - Consulta EJB publicado en JBOSS
 * 
 * @author juan.mendez
 * 
 */
@ContextConfiguration(locations = { "classpath:testApplicationContext.xml" })
public class ConservacionRestServiceTest extends
		AbstractTestNGSpringContextTests {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ConservacionRestServiceTest.class);

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Credentials credentials;

	@BeforeClass
	protected void setUp() throws Exception {
		try {
			LOGGER.debug("beforeSuite");
			assertNotNull(applicationContext);
			assertNotNull(restTemplate);
			assertNotNull(credentials);
			CommonsClientHttpRequestFactory factory = (CommonsClientHttpRequestFactory) restTemplate
					.getRequestFactory();
			HttpClient client = factory.getHttpClient();
			client.getState().setCredentials(AuthScope.ANY, credentials);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	@AfterSuite
	public void afterSuite() {
		LOGGER.debug("afterSuite");
	}

	//@Test
	public void testRestTemplateJson() {
		LOGGER.debug("testRestTemplateJson");
		try {
			Map<String, String> urlVariables = new HashMap<String, String>();
			urlVariables.put("predio", "7");

			String url = "http://localhost:9980/sigc-web-public/rest/predio/{predio}";

			Predio result = restTemplate.getForObject(url, Predio.class,
					urlVariables);
			assertNotNull(result);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	//@Test
	public void testRestTemplateXML() {
		LOGGER.debug("testRestTemplateXML");
		try {
			Map<String, String> urlVariables = new HashMap<String, String>();
			urlVariables.put("predio", "7");

			String url = "http://localhost:9980/sigc-web-public/rest/predio/{predio}";
			
			List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
			acceptableMediaTypes.add(MediaType.APPLICATION_XML);
			
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setAccept(acceptableMediaTypes);
			
			HttpEntity<Predio> requestEntity = new HttpEntity<Predio>(requestHeaders);

			HttpEntity<Predio> userInfo = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity,
					Predio.class, urlVariables);

			assertNotNull(userInfo);
			assertNotNull(userInfo.getBody());
			/*
			 * Predio result = restTemplate.getForObject(url, Predio.class,
			 * urlVariables); assertNotNull(result);
			 */
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	//@Test
	public void testRestTemplateXMLHello() {
		LOGGER.debug("testRestTemplateXMLHello");
		try {
			Map<String, String> urlVariables = new HashMap<String, String>();
			urlVariables.put("predio", "7");

			String url = "http://localhost:9980/sigc-web-public/rest/predio/hello/{predio}.xml";
			
			
			List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
			acceptableMediaTypes.add(MediaType.APPLICATION_XML);
			
			//HttpHeaders requestHeaders = new HttpHeaders();
			//requestHeaders.setAccept(acceptableMediaTypes);
			
			//HttpEntity<UsuarioDTO> requestEntity = new HttpEntity<UsuarioDTO>(requestHeaders);
			
			String usuario = restTemplate.getForObject(url, String.class, urlVariables);
			assertNotNull(usuario);
			/*
			HttpEntity<UsuarioDTO> userInfo = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity,
					UsuarioDTO.class, urlVariables);
			assertNotNull(userInfo);
			assertNotNull(userInfo.getBody());
			*/
			/*
			 * Predio result = restTemplate.getForObject(url, Predio.class,
			 * urlVariables); assertNotNull(result);
			 */
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	//@Test
	public void testRestTemplateJsonHello() {
		LOGGER.debug("testRestTemplateJsonHello");
		try {
			Map<String, String> urlVariables = new HashMap<String, String>();
			urlVariables.put("predio", "7");

			String url = "http://localhost:9980/sigc-web-public/rest/predio/hello/{predio}.json";
			
			List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
			acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
			
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setAccept(acceptableMediaTypes);
			
			HttpEntity<UsuarioDTO> requestEntity = new HttpEntity<UsuarioDTO>(requestHeaders);

			HttpEntity<UsuarioDTO> userInfo = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity,
					UsuarioDTO.class, urlVariables);

			assertNotNull(userInfo);
			assertNotNull(userInfo.getBody());
			/*
			 * Predio result = restTemplate.getForObject(url, Predio.class,
			 * urlVariables); assertNotNull(result);
			 */
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

}