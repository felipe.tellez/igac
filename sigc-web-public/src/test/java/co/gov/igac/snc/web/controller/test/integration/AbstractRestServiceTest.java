package co.gov.igac.snc.web.controller.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.StringWriter;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


/**
 * Clase abstracta para las pruebas de los servicios REST
 * 
 * @author juan.mendez
 *
 */
public class AbstractRestServiceTest {
	
	
	public static Logger LOGGER = LoggerFactory
			.getLogger(AbstractRestServiceTest.class);

	protected static final String PWD = "igac12.";

	protected static final String USER = "prusoacha04";

	//protected static final String HOST = "http://localhost:9980/";
	protected static final String HOST = "http://localhost:8080/";

	@BeforeTest
	public void beforeTest() {
		LOGGER.debug("beforeTest...");
	}

	@AfterTest
	public void afterTest() {
		LOGGER.debug("afterTest...");
	}
	
	
	/**
	 * Valida los datos que retorna el servidor
	 * 
	 * @param response
	 */
	protected boolean validateServerResponse(String response){
		LOGGER.debug("validateServerResponse");
		assertNotNull(response);
		
		boolean isError = response.contains("mensajeError");
		
		if(response.startsWith("<")){
			Document document;
			try {
				document = DocumentHelper.parseText(response);
				OutputFormat format = OutputFormat.createPrettyPrint();
				StringWriter sw = new StringWriter();
				XMLWriter writer = new XMLWriter(sw, format);
				writer.write(document);
				if(isError){
					LOGGER.error(sw.toString());
					fail("Error:"+response);
				}else{
					LOGGER.debug("Response:\n"+sw.toString());
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				fail(e.getMessage(), e);
			}
		}else{
			if(isError){
				fail(response);
			}else{
				LOGGER.debug("Response:\n"+response);
			}
		}
		return isError;
	}
	
	/**
	 * Valida los datos que retorna el servidor
	 * 
	 * @param response
	 */
	protected boolean validateServerResponse(String response, boolean failOnError){
		LOGGER.debug("validateServerResponse");
		assertNotNull(response);
		
		boolean isError = response.contains("mensajeError");
		
		if(response.startsWith("<")){
			Document document;
			try {
				document = DocumentHelper.parseText(response);
				OutputFormat format = OutputFormat.createPrettyPrint();
				StringWriter sw = new StringWriter();
				XMLWriter writer = new XMLWriter(sw, format);
				writer.write(document);
				if(isError){
					LOGGER.error(sw.toString());
					fail("Error:"+response);
				}else{
					LOGGER.debug("Response:\n"+sw.toString());
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				fail(e.getMessage(), e);
			}
		}else{
			if(isError){
				if(failOnError){
					fail(response);
				}else{
					LOGGER.debug("Error:\n"+response);
				}
			}else{
				LOGGER.debug("Response:\n"+response);
			}
		}
		return isError;
	}
	
	
	
	
	
}
