
package co.gov.igac.snc.web.ws.consultacatastralservice;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "ConsultaCatastralService", targetNamespace = "http://co/gov/igac/snc/web/ws/consultacatastralservice", wsdlLocation = "http://snc.igac.gov.co/snc/endpoints/ConsultaCatastralService.wsdl")
public class ConsultaCatastralService_Service
    extends Service
{

    private final static URL CONSULTACATASTRALSERVICE_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(co.gov.igac.snc.web.ws.consultacatastralservice.ConsultaCatastralService_Service.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = co.gov.igac.snc.web.ws.consultacatastralservice.ConsultaCatastralService_Service.class.getResource(".");
            //url = new URL(baseUrl, "http://localhost:8080/sigc-web-public/endpoints/ConsultaCatastralService.wsdl");
            url = new URL(baseUrl, "http://snc.igac.gov.co/snc/endpoints/ConsultaCatastralService.wsdl");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'http://snc.igac.gov.co/snc/endpoints/ConsultaCatastralService.wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        CONSULTACATASTRALSERVICE_WSDL_LOCATION = url;
    }

    public ConsultaCatastralService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ConsultaCatastralService_Service() {
        super(CONSULTACATASTRALSERVICE_WSDL_LOCATION, new QName("http://co/gov/igac/snc/web/ws/consultacatastralservice", "ConsultaCatastralService"));
    }

    /**
     * 
     * @return
     *     returns ConsultaCatastralService
     */
    @WebEndpoint(name = "ConsultaCatastralServiceSoap11")
    public ConsultaCatastralService getConsultaCatastralServiceSoap11() {
        return super.getPort(new QName("http://co/gov/igac/snc/web/ws/consultacatastralservice", "ConsultaCatastralServiceSoap11"), ConsultaCatastralService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ConsultaCatastralService
     */
    @WebEndpoint(name = "ConsultaCatastralServiceSoap11")
    public ConsultaCatastralService getConsultaCatastralServiceSoap11(WebServiceFeature... features) {
        return super.getPort(new QName("http://co/gov/igac/snc/web/ws/consultacatastralservice", "ConsultaCatastralServiceSoap11"), ConsultaCatastralService.class, features);
    }

}
