package co.gov.igac.snc.web.controller.test.integration;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.StringWriter;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/***
 * 
 * @author juan.mendez
 * 
 */
public class PredioRestServiceUnitTest  {

	public static Logger LOGGER = LoggerFactory
			.getLogger(PredioRestServiceUnitTest.class);

	

	@BeforeTest
	public void beforeTest() {
		LOGGER.debug("beforeTest...");
	}

	@AfterTest
	public void afterTest() {
		LOGGER.debug("afterTest...");
	}

	// Prueba de Carga
	// @Test(threadPoolSize = 20, invocationCount = 500, timeOut = 20000)
	//@Test
	public void testPredioPorIdJSON() {
		LOGGER.debug("testPredioPorIdJSON");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(
					"pruatlantico", "Igac2012.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			HttpMethod method = new GetMethod(
					"http://localhost:9980/sigc-web-public/rest/predio/7.json");

			method.addRequestHeader("accept", "application/json");

			int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			LOGGER.debug(resultado);

			assertNotNull(resultado);
			assertTrue(resultCode == 500, "Error en el Servidor. La serialización falla por el proxy de hibernate");
			// assertTrue(resultado.startsWith("{\""));
			// assertTrue(resultado.contains("34421"));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	//@Test(threadPoolSize = 10, invocationCount = 20, timeOut = 20000)
	@Test
	public void testPredioPorIdXML() {
		LOGGER.debug("testPredioPorIdXML");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(
					"pruatlantico", "Igac2012.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			HttpMethod method = new GetMethod(
					"http://localhost:9980/sigc-web-public/rest/predio/7.xml");
			//method.addRequestHeader("accept", "application/xml");

			int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			LOGGER.debug(resultado);
			
			Document document = DocumentHelper.parseText(resultado);
			OutputFormat format = OutputFormat.createPrettyPrint();
			StringWriter sw = new StringWriter();
			XMLWriter  writer = new XMLWriter( sw, format );
	        writer.write( document );
	        LOGGER.debug(sw.toString());

			assertNotNull(resultado);
			assertTrue(resultCode == 200, "Error en el Servidor");
			// assertTrue(resultado.startsWith("<?xml version=\"1.0\""));
			// assertTrue(resultado.contains("34421"));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	//@Test
	public void testPredioHelloXML() {
		LOGGER.debug("testPredioHelloXML");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(
					"pruatlantico", "Igac2012.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			
			String url = "http://localhost:9980/sigc-web-public/rest/predio/hello/7.json" ;
			LOGGER.debug("url:"+url);
			HttpMethod method = new GetMethod(url);
			//method.addRequestHeader("accept", "application/xml");

			int resultCode = httpclient.executeMethod(method);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			LOGGER.debug(resultado);

			assertNotNull(resultado);
			assertTrue(resultCode == 200, "Error en el Servidor");
			// assertTrue(resultado.startsWith("<?xml version=\"1.0\""));
			// assertTrue(resultado.contains("34421"));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	@Test
	public void testPredioHello2XML() {
		LOGGER.debug("testPredioHello2XML");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(
					"pruatlantico", "Igac2012.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String url = "http://localhost:9980/sigc-web-public/rest/predio/hello/7.xml" ;
			LOGGER.debug("url:"+url);
			HttpMethod method = new GetMethod(url);
			//method.addRequestHeader("accept", "application/xml");

			int resultCode = httpclient.executeMethod(method);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			LOGGER.debug(resultado);
			
			Document document = DocumentHelper.parseText(resultado);
			OutputFormat format = OutputFormat.createPrettyPrint();
			StringWriter sw = new StringWriter();
			XMLWriter  writer = new XMLWriter( sw, format );
	        writer.write( document );
	        LOGGER.debug(sw.toString());

			assertNotNull(resultado);
			assertTrue(resultCode == 200, "Error en el Servidor");
			// assertTrue(resultado.startsWith("<?xml version=\"1.0\""));
			// assertTrue(resultado.contains("34421"));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	

}
