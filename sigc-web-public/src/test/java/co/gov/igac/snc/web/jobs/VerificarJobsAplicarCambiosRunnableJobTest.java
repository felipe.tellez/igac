package co.gov.igac.snc.web.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.annotations.Test;
import static org.testng.Assert.fail;
import static org.testng.Assert.assertNotNull;


import co.gov.igac.snc.web.controller.test.integration.AbstractRestServiceTest;
import co.gov.igac.snc.web.ws.consultacatastralservice.ConsultaWebServiceUnitTest;
/**
 * Prueba la clase requerida por la tarea programada
 * @author andres.eslava
 */
public class VerificarJobsAplicarCambiosRunnableJobTest extends AbstractRestServiceTest{
    
    
	public static Logger LOGGER = LoggerFactory.getLogger(ConsultaWebServiceUnitTest.class);
        
        
        @Test        
        public void testVerificarJobsAplicarCambiosRunnableJob(){            
            VerificarJobsAplicarCambiosRunnableJob verificarJobs = new VerificarJobsAplicarCambiosRunnableJob();
            verificarJobs.run();                       
        }

    
    
}
