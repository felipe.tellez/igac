package co.gov.igac.snc.util.unit;

import static org.testng.Assert.assertNotNull;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase de prueba para conexión con LDAP (Active directory)
 * 
 */
public class Ldap {

	private static Logger logger = LoggerFactory.getLogger(Ldap.class);

	@SuppressWarnings("unchecked")
	public Attributes authenticateUser(String username, String password,
			String _domain, String host, String dn) throws NamingException {
		// Create the search controls
		String principal = username + "@" + _domain;
		// String principal = username ;
		logger.debug("principal:" + principal);
		Hashtable environment = new Hashtable();
		environment.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		environment.put(Context.PROVIDER_URL, "ldap://" + host + ":389");
		environment.put(Context.SECURITY_AUTHENTICATION, "simple");
		environment.put(Context.SECURITY_PRINCIPAL, principal);
		environment.put(Context.SECURITY_CREDENTIALS, password);
		LdapContext ctxGC = null;

		Attributes attrs = null;

		try {
			ctxGC = new InitialLdapContext(environment, null);
			logger.debug("se autenticó");

			String ATTRIBUTE_FOR_USER_ = "sAMAccountName";
			String searchFilter = "(&(objectClass=user)(" + ATTRIBUTE_FOR_USER_	+ "=" + username + "))";
			logger.debug("searchFilter:" + searchFilter);

			String returnedAtts[] = { "sn", "name", "displayName", "givenName",
					"mail", "memberOf", "cn", "distinguishedName","userAccountControl",
					"lastKnownParent" };

			SearchControls searchCtls = new SearchControls();
			searchCtls.setReturningAttributes(returnedAtts);
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			NamingEnumeration answer = ctxGC.search(dn, searchFilter,searchCtls);
			assertNotNull(answer);
			// logger.debug("Answer:" + answer.hasMore());

			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				assertNotNull(sr);
				attrs = sr.getAttributes();
				assertNotNull(attrs);

				if (attrs != null) {
					for (NamingEnumeration vals = attrs.getAll(); vals.hasMoreElements();) {
						logger.debug("" + vals.nextElement());
					}
				}
			}

		} catch (NamingException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			if (ctxGC != null) {
				ctxGC.close();
			}
		}
		return attrs;
	}
}
