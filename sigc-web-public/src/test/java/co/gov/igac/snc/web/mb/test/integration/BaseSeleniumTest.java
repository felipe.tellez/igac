package co.gov.igac.snc.web.mb.test.integration;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.thoughtworks.selenium.DefaultSelenium;

public class BaseSeleniumTest {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseSeleniumTest.class);

	private static String WEB_HOST = "localhost";
	private static int WEB_PORT = 9980;

	private static String SELENIUM_HOST = "localhost";
	private static int SELENIUM_PORT = 4499;

	protected DefaultSelenium selenium;

	/**
	 * Supported browsers include: firefox mock firefoxproxy pifirefox chrome
	 * iexploreproxy iexplore firefox3 safariproxy googlechrome konqueror
	 * firefox2 safari piiexplore firefoxchrome opera iehta custom
	 * 
	 * @throws Exception
	 */
	@BeforeSuite
	public void beforeSuite() {
		LOGGER.debug("beforeSuite");
		try {
			/*
			 * String browser = System.getProperty("browser"); String port =
			 * System.getProperty("selenium.port"); LOGGER.debug("browser:" +
			 * browser); LOGGER.debug("selenium.port:" + port);
			 */
			// selenium = createSeleniumClient("http://localhost:9980/",
			// "*googlechrome");
			//selenium = createSeleniumClient("http://" + WEB_HOST + ":"+ WEB_PORT + "/", "*firefox");
			selenium = createSeleniumClient("http://" + WEB_HOST + ":"+ WEB_PORT + "/", "*chrome");
			// selenium.start("captureNetworkTraffic=true");
			assertNotNull(selenium);
			selenium.start();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	
	//@BeforeClass
    public void beforeClass() {
    	LOGGER.debug("beforeClass");
        /*
    	selenium = staticSelenium;
        if (restartSession) {
            selenium.stop();
            selenium.start();
        }
        */
    }
	
	
	/*
	@BeforeTest
    @Override
    @Parameters({"selenium.url", "selenium.browser"})
    public void setUp(@Optional String url, @Optional String browserString) throws Exception {
        if (browserString == null) browserString = runtimeBrowserString();

        WebDriver driver = null;
        if (browserString.contains("firefox") || browserString.contains("chrome")) {
          if (isInDevMode(FirefoxDriver.class, "/webdriver-extension.zip")) {
            System.setProperty("webdriver.development", "true");
            driver = Class.forName("org.openqa.selenium.firefox.FirefoxDriverTestSuite$TestFirefoxDriver")
                .asSubclass(WebDriver.class).newInstance();
          } else {
            driver = new FirefoxDriver();
          }
        } else if (browserString.contains("ie") || browserString.contains("hta")) {
          if (isInDevMode(FirefoxDriver.class, "/webdriver-extension.zip")) {
            System.setProperty("webdriver.development", "true");
            System.setProperty("jna.library.path", "..\\build;build");
          }
          driver = new InternetExplorerDriver();
        } else {
          fail("Cannot determine which browser to load: " + browserString);
        }

        if (url == null)
          url = "http://localhost:4444/selenium-server";
        selenium = new WebDriverBackedSelenium(driver, url);

        staticSelenium = selenium;
    }
	*/

    /**
     * 
     */
	@AfterSuite
	public void afterSuite() {
		LOGGER.debug("afterSuite");
		selenium.stop();
	}

	/**
	 * 
	 */
	@AfterMethod(alwaysRun = true)
	public void afterMethod() {
		LOGGER.debug("afterMethod");
		try {
			if (selenium != null){
				selenium.selectWindow("null");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * @param method
	 */
	@BeforeMethod
    public void beforeMethod(Method method) {
		LOGGER.debug("beforeMethod");
		try {
			selenium.setContext(method.getDeclaringClass().getSimpleName() + "." + method.getName());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
        
    }
	
	/*
	@BeforeMethod
    public void addNecessaryJavascriptCommands() {
      if (!(selenium instanceof WebDriverBackedSelenium))
        return;

      // We need to be a on page where we can execute JS
      ((WebDriverBackedSelenium) selenium).getUnderlyingWebDriver()
          .get("http://localhost:4444/selenium-server");
      // Read the testHelper.js script in
      String path = "/com/thoughtworks/selenium/testHelpers.js";

      InputStream is = getClass().getResourceAsStream(path);
      try {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder builder = new StringBuilder();
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
          builder.append(line)
              .append("\n");
        }
        String script = builder.toString();

        WebDriver driver = ((WebDriverBackedSelenium) selenium).getUnderlyingWebDriver();
        ((JavascriptExecutor) driver).executeScript(script);
      } catch (IOException e) {
        Assert.fail("Cannot read script", e);
      } finally {
        if (is != null) try {
          is.close();
        } catch (IOException e) {
          // Throw this away. Nothing sane to do
        }
      }
    }
	*/
	
	

	/**
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	protected DefaultSelenium createSeleniumClient(String url, String browser)
			throws Exception {
		LOGGER.debug("createSeleniumClient");
		DefaultSelenium sel = null;
		try {
			sel = new DefaultSelenium(SELENIUM_HOST, SELENIUM_PORT, browser,
					url);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
			throw e;
		}
		return sel;
	}

}
