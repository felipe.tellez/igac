package co.gov.igac.snc.web.controller.test.integration;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.io.File;
import java.net.URL;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import co.gov.igac.snc.persistence.util.ETrueFalse;
import static org.testng.Assert.assertTrue;

/***
 * 
 * @author franz.gamba
 * 
 */
public class TramiteRestServiceUnitTest extends AbstractRestServiceTest{

	public static Logger LOGGER = LoggerFactory
			.getLogger(PredioRestServiceUnitTest.class);

	private static String IMAGE_NAME = "Wrightfallingwater_01.jpg";
	private static String IMAGE_NAME_2 = "Wrightfallingwater_02.jpg";
	private static String IMAGE_NAME_PATH;
	private static String IMAGE_NAME_PATH2;
	private static String ROOT_PATH;
	private static String ZIP_GEODATA = "tr_47001_22165.gdb.zip.enc.zip";
	private static String ZIP_GEODATA_PATH;
	

	@BeforeSuite
	public void beforeSuite() {
		try {
			LOGGER.debug("beforeSuite");

			URL fileUrl = this.getClass().getClassLoader().getResource(IMAGE_NAME);
			assertNotNull(fileUrl);
			IMAGE_NAME_PATH = fileUrl.getPath();
			LOGGER.debug("IMAGE_NAME_PATH:" + IMAGE_NAME_PATH);
					

			fileUrl = this.getClass().getClassLoader().getResource(IMAGE_NAME_2);
			assertNotNull(fileUrl);
			IMAGE_NAME_PATH2 = fileUrl.getPath();
			LOGGER.debug("IMAGE_NAME_PATH2:" + IMAGE_NAME_PATH2);
			
			
		    fileUrl = this.getClass().getClassLoader().getResource(ZIP_GEODATA);
		    assertNotNull(fileUrl);
		    ZIP_GEODATA_PATH = fileUrl.getPath();
			LOGGER.debug("ZIP_GEODATA_PATH:" + ZIP_GEODATA_PATH);
		    			
			
			ROOT_PATH = new File(fileUrl.getPath()).getParentFile().getAbsolutePath();
			LOGGER.debug("ROOT_PATH:" + ROOT_PATH);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	@AfterSuite
	public void afterSuite() {
		LOGGER.debug("afterSuite");
	}

	@Test
	public void testPredioPorIdTramiteXML() {

		LOGGER.debug("testPredioPorIdTramiteXML");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,
					PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			HttpMethod method = new GetMethod(HOST
					+ "sigc-web-public/rest/tramite/87.xml");
			// method.addRequestHeader("accept", "application/xml");

			int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}

	/**
	 * Test que avanza el proceso de modificar info. geografica a modificar info. alfanumerica.
	 * @author franz.gamba
	 */
	@Test
	public void testAvanzarProcesoTramite() {
		
		LOGGER.debug("testAvanzarProcesoTramite");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,
					PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String tramiteId = "42426";
			HttpMethod method = new GetMethod(HOST
					+ "sigc-web-public/rest/tramite/proceso/avanzar/"+tramiteId+".xml");
			// method.addRequestHeader("accept", "application/xml");
			
			int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
        @Test
	public void testConsultarPredio() {
		
		LOGGER.debug("testConsultarPredio");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruatlantico16", "igac2014.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String numeroPredial = "080010003000000001789000000000";
                        String url = HOST + "snc/rest/tramite/conservacion/depuracionGeo/consultarPredio/"+numeroPredial+".xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);
			
			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:" + resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
        
        @Test
	public void testConsultarInformacionProyectada() {
		
		LOGGER.debug("testConsultarInformacionProyectada");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruquindio6", "igac2014.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String numeroTramite = "123554";
                        String url = HOST + "snc/rest/tramite/conservacion/depuracionGeo/consultarInformacionProyectada/"+numeroTramite+".xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);
			
			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:" + resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
        @Test
        public void testObtenerInfoEdicion() {
		
		LOGGER.debug("testObtenerInfoEdicion");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruatlantico17", "igac2016.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String numeroTramite = "409936";  //CANCELACION-MASIVA
                        String url = HOST + "snc/rest/tramite/conservacion/obtenerInfoEdicion/"+numeroTramite+".xml";
			LOGGER.debug("url:" + url);

			HttpMethod method = new GetMethod(url);
			
			int resultCode = httpclient.executeMethod(method);
			assertTrue(resultCode == 200, "Error en el Servidor. Código:" + resultCode);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
        
	@Test
	public void testImagenPredioAntesDespues() {

		LOGGER.debug("testImagenPredioAntesDespues");
		try {

			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,
					PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			PostMethod postMethod = new PostMethod(HOST +	"/snc/rest/tramite/imagen/3.xml");

			File image = new File(IMAGE_NAME_PATH);
			File image2 = new File(IMAGE_NAME_PATH2);

			Part[] parts = { 
					new StringPart("param_name", "value"),
					new FilePart("imagen", image), 
					new FilePart("imagen2", image2)};
			postMethod.setRequestEntity(new MultipartRequestEntity(parts,
					postMethod.getParams()));

			// Here we go!
			int resultCode = httpclient.executeMethod(postMethod);
			assertEquals(resultCode, 200);
			String resultado = postMethod.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testImagenPredioAntes() {
		
		LOGGER.debug("testImagenPredioAntes");
		try {
			
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,
					PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			PostMethod postMethod = new PostMethod(
					"http://localhost:9980/sigc-web-public/rest/tramite/imagen/inicial/42617.xml");
			
			//File image = new File(IMAGE_NAME_PATH);
			File image = new File("C:\\Users\\franz.gamba\\Pictures\\excuseme.jpg");
			//File image2 = new File(IMAGE_NAME_PATH2);
			
			Part[] parts = { 
					new StringPart("param_name", "value"),
					new FilePart("imagen", image)};
			postMethod.setRequestEntity(new MultipartRequestEntity(parts,
					postMethod.getParams()));
			
			// Here we go!
			int resultCode = httpclient.executeMethod(postMethod);
			assertEquals(resultCode, 200);
			String resultado = postMethod.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testImagenPredioDespues() {
		
		LOGGER.debug("TramiteRestServiceUnitTest#testImagenPredioDespues");
		try {
			
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,
					PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			PostMethod postMethod = new PostMethod(
					"http://localhost:9980/sigc-web-public/rest/tramite/imagen/final/42617.xml");
			
			//File image = new File(IMAGE_NAME_PATH);
			File image = new File("C:\\Users\\franz.gamba\\Pictures\\excuseme.jpg");
			//File image2 = new File(IMAGE_NAME_PATH2);
			
			Part[] parts = { 
					new StringPart("param_name", "value"),
					new FilePart("imagen", image)};
			postMethod.setRequestEntity(new MultipartRequestEntity(parts,
					postMethod.getParams()));
			
			// Here we go!
			int resultCode = httpclient.executeMethod(postMethod);
			assertEquals(resultCode, 200);
			String resultado = postMethod.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testCargaZipTramite() {

		LOGGER.debug("testCargaZipTramite");
		try {

			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER,
					PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			PostMethod postMethod = new PostMethod(
					"http://localhost:9980/sigc-web-public/rest/tramite/geodb/3.xml");

			File geoZip = new File(ZIP_GEODATA_PATH);
			
			Part[] parts = { 
					new StringPart("param_name", "value"),
					new FilePart("zip", geoZip)};
			postMethod.setRequestEntity(new MultipartRequestEntity(parts,
					postMethod.getParams()));
			LOGGER.debug("******ANTES********");	
			// Here we go!
			int resultCode = httpclient.executeMethod(postMethod);
			LOGGER.debug("******Despues********");
			assertEquals(resultCode, 200);
			String resultado = postMethod.getResponseBodyAsString();
			validateServerResponse(resultado);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testGetNombreReplicaGDBdeTramite() {
		LOGGER.debug("testGetNombreReplicaGDBdeTramite");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER, PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String idTramite = "45498";
			String url = HOST + "sigc-web-public/rest/tramite/replica/original/"+idTramite+".xml";
			LOGGER.debug("url --> " + url);
			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testGetReplicaGDBOutFolderdeTramiteTrue() {
		LOGGER.debug("testGetReplicaGDBOutFolderdeTramite");
		try 
		{
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER, PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String idTramite = "123932";
			String codigoOrganizacional = "47001";
			
			String finalFlag = ETrueFalse.FALSE.getCodigo();
			String url = HOST + "sigc-web-public/rest/tramite/replica/copiaTrabajo/" + idTramite + ".xml";
			LOGGER.debug("url --> " + url);

			PostMethod postMethod = new PostMethod(url);
			File geoZip = new File(ZIP_GEODATA_PATH);
			Part[] parts = { 
					new FilePart("replicaFile", geoZip),
					new StringPart("prediosAreas", "257540103000000190024000000000:10.0;257540103000000190025000000000:20.5"),
					new StringPart("final", finalFlag),
					new StringPart("codigoOrganizacional", codigoOrganizacional)};
			postMethod.setRequestEntity(new MultipartRequestEntity(parts,postMethod.getParams()));
			int resultCode = httpclient.executeMethod(postMethod);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = postMethod.getResponseBodyAsString();
			validateServerResponse(resultado);
		} 
		catch (Exception e){
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
        @Test
	public void testGetNombreReplicaConsultaTramite() {
		LOGGER.debug("testGetNombreReplicaConsultaTramite");
		try 
		{
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruatlantico3", "igac2014.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String idTramite = "124122";
						
			String url = HOST + "snc/rest/tramite/replica/consulta/" + idTramite + ".xml";
			LOGGER.debug("url --> " + url);

			HttpMethod method = new GetMethod(url);
                        int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} 
		catch (Exception e){
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testGetReplicaGDBOutFolderdeTramiteFalse() {
		LOGGER.debug("testGetReplicaGDBOutFolderdeTramiteFalse");
		try 
		{
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials(USER, PWD);
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
			
			String idTramite = "42893";
			String finalFlag = ETrueFalse.TRUE.getCodigo();
			String codigoOrganizacional = "08001";
			String url = HOST + "sigc-web-public/rest/tramite/replica/copiaTrabajo/" + idTramite + ".xml";
			LOGGER.debug("url:" + url);
			
			PostMethod postMethod = new PostMethod(url);
			File geoZip = new File(ZIP_GEODATA_PATH);
			Part[] parts = { 
					new FilePart("replicaFile", geoZip),
					new StringPart("prediosAreas", ""),
					new StringPart("final", finalFlag),
					new StringPart("codigoOrganizacional", codigoOrganizacional)};
			postMethod.setRequestEntity(new MultipartRequestEntity(parts,postMethod.getParams()));
			int resultCode = httpclient.executeMethod(postMethod);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = postMethod.getResponseBodyAsString();
			validateServerResponse(resultado);
		} 
		catch (Exception e) 
		{
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
	@Test
	public void testGetReplicaGDBfromOutFolderToTempFolderdeTramite() {
		LOGGER.debug("testGetReplicaGDBfromOutFolderToTempFolderdeTramite");
		try {
			HttpClient httpclient = new HttpClient();
			httpclient.getParams().setAuthenticationPreemptive(true);
			Credentials defaultcreds = new UsernamePasswordCredentials("pruatlantico16", "igac2014.");
			httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);

			String idTramite = "124364";
			String url = HOST + "snc/rest/tramite/replica/copiaTrabajo/" + idTramite + ".json";
			LOGGER.debug("url --> " + url);
			HttpMethod method = new GetMethod(url);

			int resultCode = httpclient.executeMethod(method);
			assertEquals(resultCode, 200);
			LOGGER.debug("resultCode:" + resultCode);
			String resultado = method.getResponseBodyAsString();
			validateServerResponse(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			fail(e.getMessage(), e);
		}
	}
}
