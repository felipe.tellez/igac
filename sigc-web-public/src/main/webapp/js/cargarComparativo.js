function cargarComparativo(imgAntes, imgDespues) {

    map1 = new OpenLayers.Map('imagenAntes');
    map2 = new OpenLayers.Map('imagenDespues');

    var options = {numZoomLevels: 3};
    var graphic = new OpenLayers.Layer.Image('Imagen Original', imgAntes,
            new OpenLayers.Bounds(-180, -88.759, 180, 88.759),
            new OpenLayers.Size(580, 288),
            {numZoomLevels: 5}
    );
    var graphic2 = new OpenLayers.Layer.Image('Imagen Final', imgDespues,
            new OpenLayers.Bounds(-180, -88.759, 180, 88.759),
            new OpenLayers.Size(580, 288),
            {numZoomLevels: 5}
    );
    graphic.events.on({
        loadstart: function () {
            OpenLayers.Console.log("loadstart");
        },
        loadend: function () {
            OpenLayers.Console.log("loadend");
        }
    });

    map1.addLayers([graphic]);
    map1.addControl(new OpenLayers.Control.LayerSwitcher());
    map1.zoomToMaxExtent();


    map2.addLayers([graphic2]);
    map2.addControl(new OpenLayers.Control.LayerSwitcher());
    map2.zoomToMaxExtent();


    var c1, c2, z1, z2;
    var updatingMap1 = false, updatingMap2 = false;
    map1.events.register("moveend", map1, function () {
        if (!updatingMap2) {
            c1 = this.getCenter();
            z1 = this.getZoom();
            updatingMap1 = true;
            map2.panTo(c1);
            map2.zoomTo(z1);
            updatingMap1 = false;
        }
    });
    map2.events.register("moveend", map2, function () {
        if (!updatingMap2) {
            c2 = this.getCenter();
            z2 = this.getZoom();
            updatingMap2 = true;
            map1.panTo(c2);
            map1.zoomTo(z2);
            updatingMap2 = false;
        }
    });
}  