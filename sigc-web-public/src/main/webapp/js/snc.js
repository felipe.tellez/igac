PrimeFaces.locales['es_CO'] = {
    closeText: 'Aceptar',
    prevText: 'Anterior',
    nextText: 'Siguiente',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    weekHeader: 'Semana',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Sólo hora',
    timeText: 'Tiempo',
    hourText: 'Hora',
    minuteText: 'Minuto',
    secondText: 'Segundo',
    currentText: 'Fecha actual',
    ampm: false,
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    allDayText: 'Todo el día'
};




/**
 * Completa los inputs con 0s a la izquierda
 * @author fredy.wilches
 */
function complete(object, size) {
    variable = '' + object.value;
    while (variable.length < size) {
        variable = '0' + variable;
    }
    object.value = variable;
}

/**
 * @author fredy.wilches
 * @modified pedro.garcia  adición de un parámetro para que esta función se pueda usar cuando
 *
 * Funcion para limpiar NP
 * @param stringCompIds cadena con la secuencia de ids de componentes necesaria para llegar hasta el input del campo de número predial
 */
function limpiarNP(stringCompIds) {
    for (var i = 1; i <= 12; i++) {
        document.getElementById(stringCompIds + ":s" + i).value = "";
    }
}

/**
 *@author felipe.cadena
 *
 *Funcion para limpiar un campo 
 *@param idCampo id del campo que se desea limpiar.
 *
 */
function limpiarCampo(idCampo) {
    document.getElementById(idCampo).value = "";
}
/**
 * @author fredy.wilches
 * @modified pedro.garcia  adición de un parámetro para que esta función se pueda usar cuando
 * los campos del número predial se usan dentro de un componente
 *
 * Funcion que permite saltar al siguiente segmento de un numero predial.
 * @param object Campo de texto actual
 * @param size Tamaño del segmento actual
 * @param target Indice del siguiente segmento
 * @param size2 Tamaño del segmento siguiente
 * @param stringCompIds cadena con la secuencia de ids de componentes necesaria para llegar hasta el input del compo de número predial
 */
function saltar(object, size, target, size2, stringCompIds) {
    variable = '' + object.value;
    if (variable.length == size) {
        document.getElementById(stringCompIds + "s" + target).focus();
        document.getElementById(stringCompIds + "s" + target).selectionStart = 0;
        document.getElementById(stringCompIds + "s" + target).selectionEnd = size2;
    }
}

/**
 * 
 */
var previousTabRadicacionSolicitudAvaluos = 0;
var currentTabRadicacionSolicitudAvaluos = 0;
var auxIndexRadicacionSolicitudAvaluos = 0;
var selectedTipoPersona;

/**
 * @author fabio.navarrete
 * @param event
 * @param ui
 */
function handleDetailsTabChange(event, ui) {
    //var totalTabs = document.getElementById(event.target.id).childElementCount - 2;
    previousTabRadicacionSolicitudAvaluos = currentTabRadicacionSolicitudAvaluos;
    currentTabRadicacionSolicitudAvaluos = ui.index;
    selectTipoPersona(null);

    if (previousTabRadicacionSolicitudAvaluos == 0) {
        saveProgressSRBtn.jq.click();
    }
    if (selectedTipoPersona == 'JURIDICA') {
        if (previousTabRadicacionSolicitudAvaluos == 1) {
            saveProgressRLBtn.jq.click();
        }
        if (previousTabRadicacionSolicitudAvaluos == 2) {
            saveProgressTRBtn.jq.click();
        }
    } else {
        if (previousTabRadicacionSolicitudAvaluos == 1) {
            saveProgressTRBtn.jq.click();
        }
    }
}

function selectTipoPersona(event) {
    var selectMenu = document.getElementById("tipoPersona");
    selectedTipoPersona = selectMenu[selectMenu.selectedIndex].value;
}

/**
 * @author fabio.navarrete
 * Función que actualiza los índices de los tabs seleccionados para el tabview
 * de la pantalla de radicación de solicitudes de avalúos.
 */
function updateSelectedTabs() {
    previousTabRadicacionSolicitudAvaluos = currentTabRadicacionSolicitudAvaluos;
    currentTabRadicacionSolicitudAvaluos = auxIndexRadicacionSolicitudAvaluos;
}

//Variable para el estilo de grafica de barras tipo Chart
var chartStyleChart = {
    padding: 0,
    legend: {
        display: "right",
        spacing: 10
    }

};
//--------------------------------------------------------------------------------------------------
/**
 * Función encargada de manejar los errores y cerrar y abrir una ventana cuando no se tiene error.
 * Algún parámetro debe ser pasado mediante un callback de prime con el nombre de "error" y
 * cualquier valor cuando la ejecución haya sido fallida (se haya agregado un mensaje de error al facesContext).
 * Si fue exitosa éste mensaje debe quedar vacío (null, no se debe definir).
 * Es decir, se ejecuta solo cuando no hay error.
 * validationFailed tiene el resultado de la validación de comonentes de la página
 *
 * @documented pedro.garcia
 *
 * @modified pedro.garcia se tuvo que cambiar la validación que se hace porque con primefaces 3.2
 *  no se arma el args igual que antes: si no hay un error de validación de componentes de la
 *  página, el campo validationFailed llega en null.
 * validationFailed tiene el resultado de la validación de componentes de la página
 *
 *
 * @param xhr
 * @param status
 * @param args
 * @param dialogWV nombre widget var de la página que se quiere abrir o cerrar
 * @param open indica si la página denominada con dialogWV se va a abrir (si es true) o cerrar (si es false)
 */
function handleCompleteSIC(xhr, status, args, dialogWV, open) {

    if (args != null) {
        if (args.error == null && args.validationFailed == null
                || (args.validationFailed != null && args.validationFailed == false)) {
            if (open) {
                dialogWV.show();
            } else {
                dialogWV.hide();
            }
        }
    }
}
//--------------------------------------------------------------------------------------------------
/**
 * Función encargada de manejar los errores para cerrar o abrir una ventana cuando SÍ se tiene error.
 * Algún parámetro debe ser pasado mediante un callback de prime con el nombre de "error" y
 * cualquier valor cuando la ejecución haya sido fallida (se haya agregado un mensaje de error
 * al facesContext).
 * Al definir el parámetro callBack con el nombre "error", o al ocurrir un error de validación
 * de campos se ejecuta esta función
 *
 * validationFailed tiene el resultado de la validación de componentes de la página
 *
 * @author pedro.garcia
 *
 * @param xhr
 * @param status
 * @param args
 * @param dialogWV
 * @param open
 */
function handleCompleteOnErrorSNC(xhr, status, args, dialogWV, open) {

    if (args != null) {
        if (args.error != null ||
                (args.validationFailed != null && args.validationFailed == true)) {
            if (open) {
                dialogWV.show();
            } else {
                dialogWV.hide();
            }
        }
    }
}
//--------------------------------------------------------------------------------------------------
function handleOL(xhr, status, args, url, element) {
    element = element.replace(":", "\\:");
    element = $(element);
    if (args != null && args.error == undefined && args.validationFailed == undefined) {
        url = url + args.url;
        location.href = url;
    }
}


function redireccionar(url) {
    if (url.length > 0 && url != '/snc') {

        /*var urlActual=''+window.location;
         var pos=urlActual.indexOf('sigc');
         alert(urlActual.substring(0, pos-1));
         alert(url);
         window.location=urlActual.substring(0, pos-1)+url;*/
        statusDialog2.show();
        location.href = url;
    } else {
        statusDialog2.hide();
    }
}

/**
 * Función encargada de aplicarle formato de tipo moneda a un componente de
 * texto. El formato es aplicado con plugin <a
 * href="http://code.google.com/p/jquery-formatcurrency/wiki/Usage">formatCurrency
 * de jQuery</a>.
 * 
 * @author christian.rodriguez
 * 
 * @param componenteId
 *            componente al que se le quiere aplicar el formato
 * @param activar
 *            bandera que activa o desactiva el formato moneda
 */
function aplicarFormatoMoneda(componenteId, activar) {
    var componente = document.getElementById(componenteId);

    $(componente).toNumber();
    if (activar) {
        $(componente).formatCurrency({
            decimalSymbol: '.',
            digitGroupSymbol: ',',
            roundToDecimalPlace: -2
        });
    }
}

/**
 * Metodo para cambiar el valor seleccionado de un combobox
 * 
 * @author christian.rodriguez
 * @param args
 *            debe tener un atributo nuevoValorCombo el cual equivale al val a
 *            seleccionar del combo. Lo mas comun es agregarlo desde el MB
 *            usando el RequestContext
 * @param comboID
 *            identificador del combo
 * 
 */
function cambiarValorCombo(args, comboID) {
    var combo = document.getElementById(comboID);
    $(combo).val(args.nuevoValorCombo);
}

/**
 * Oculta o muestra un componente dependiendo de un evento generado por un
 * picklist
 * 
 * @author christian.rodriguez
 * @param eventoPL
 *            evento generado por el PickList
 * @param idComponenteOcultar
 *            componente que se quiere ocultar
 * @param criterio
 *            criterio de comparación para ocultar o mostrar
 */
function toggleComponentePL(eventoPL, idComponenteOcultar, criterio) {

    var seleccionado = eventoPL.item.context.textContent;
    var componente = document.getElementById(idComponenteOcultar);
    if (seleccionado == criterio) {
        $(componente).toggle();
    }
}

// -------------------------------------------------------------------------
/**
 * Retorna la cadena valida a utilizar para incluir ':' en una cadena
 */
function validColonString() {
    return '\\:';
}
/*
 //event below works both in IE6,7 and Firefox
 window.onunload = function()
 {
 // do processing here
 alert('bye!');
 }
 
 function showUnloading(evt){
 alert('entro');
 //IE specific code to cancel the navigation
 //if you put a text value in that property IE will try to confirm
 //if you want to navigate off the page.
 //The code also works with Firefox
 evt.returnValue = "You are leaving.";
 
 //Firefox GeckoDOM specific
 //below is how to handle the cancelling of firefox
 if (!confirm('Are you sure?'))
 {
 evt.preventDefault;
 evt.stopPropagation; // this makes sure it does not bubble up
 }
 evento=evt;
 }
 
 function doUnload(e)
 {
 var e = e || window.event;
 if (window.event.clientY < 0 || posy<15)
 {
 alert(location.href);
 //location.href='';
 alert("Window is closed.");
 }
 }
 
 
 window.onunload = function (e) {
 alert('entro4');
 var e = e || window.event;
 alert('entro5 '+e);
 alert('entro6 '+e.clientX+' '+e.clientY+ ' '+posx+' '+posy);
 //IE & Firefox   
 if (e) {     
 e.returnValue = 'Are you sure?';   
 }    
 // For Safari   
 return 'Are you sure?'; 
 };
 
 var posx;var posy;
 function getMouse(e){
 posx=0;posy=0;
 var ev=(!e)?window.event:e;//IE:Moz
 if (ev.pageX){//Moz
 posx=ev.pageX+window.pageXOffset;
 posy=ev.pageY+window.pageYOffset;
 }
 else if(ev.clientX){//IE
 posx=ev.clientX+document.body.scrollLeft;
 posy=ev.clientY+document.body.scrollTop;
 }
 else{
 return false
 }//old browsers document.getElementById('myspan').firstChild.data='X='+posx+' Y='+posy;
 }
 document.onmousemove=getMouse
 
 OJO en el body
 onunload="doUnload(event)"
 
 
 */



var numeroClick = 0;

var isNS4 = (navigator.appName == "Netscape") ? 1 : 0;

// Evita el funcionamiento de la tecla backspace para regresar una página
window.history.forward(1);

/* Funciones para que se convierta a mayúsculas en mozilla*/

if (document.addEventListener && Event.KEYUP) {
    //remove this part if you do not need Netscape 4 to work
    document.captureEvents(Event.KEYUP);
}

document.onkeydown = alertkey;

function alertkey(e) {

    if (navigator.appName.indexOf("Microsoft") >= 0)
        return;
    if (!e) {
        //if the browser did not pass the event information to the
        //function, we will have to obtain it from the event register
        if (window.event) {
            //Internet Explorer
            e = window.event;
        } else {
            //total failure, we have no way of referencing the event
            return;
        }
    }
    event = e;

    if (typeof (e.keyCode) == 'number') {
        //DOM
        e = e.keyCode;
    } else if (typeof (e.which) == 'number') {
        //NS 4 compatible
        e = e.which;
    } else if (typeof (e.charCode) == 'number') {
        //also NS 6+, Mozilla 0.9+
        e = e.charCode;
    } else {
        //total failure, we have no way of obtaining the key code
        return;
    }
    //  window.alert('The key pressed has keycode ' + e + ' and is key ' + String.fromCharCode( e ) );
    event = e;
}

///////////


/* función para desactivar ctrl+u, ctrl+t, ctrl+n, tecla del menú contextual y F11*/
function desactivar() {
    if (typeof event != 'undefined') {
        var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
//    if (event.altKey)
//    alert(event.keyCode);
        // 93 tecla menu contextual
        // 122 F11

        if (event.altKey && event.keyCode == 115) {

            location.href = "/snc/logout";
            alert("Ha forzado la salida del sistema!!!");
            event.keyCode = 0;
            event.returnValue = false;
        }


        if (((event.ctrlKey && (pressedKey == "u" || pressedKey == "t" || pressedKey == "n"))) || (event.keyCode == 122 || event.keyCode == 93) || (event.altKey && event.keyCode == 115)) {
            alert("Operación inválida");
            event.keyCode = 0;
            event.returnValue = false;
        }
    }
}



function my_onkeydown_handler() {

    if (typeof event != 'undefined') {
        switch (event.keyCode) {
            case 0 :
                alert("Tecla control desactivada");
                event.returnValue = false;
                break;
        }
    }
}



function right(e) {
    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
        return false;
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {

        alert("El click derecho se encuentra deshabilitado.");
        return false;

    }
    return true;
}

//TODO :: fredy.wilches :: documentar!!!! :: pedro.garcia
function lanzar() {

    numeroClick = 0;
    document.onmousedown = right;
    document.onmouseup = right;
    if (document.layers)
        window.addEventListener(Event.MOUSEDOWN);
    if (document.layers)
        window.addEventListener(Event.MOUSEUP);
    window.onmousedown = right;
    window.onmouseup = right;
}
//TODO :: fredy.wilches :: documentar!!!! :: pedro.garcia
//TODO :: fredy.wilches :: verificar si no se usa; en tal caso, borrarlo :: pedro.garcia
function tratandoCerrar() {

    var pagina = location.href;
    if ((window.event.clientX < 0 && window.event.clientX > -3060) || (window.event.clientY < 0 && window.event.clientY > -3060))
        event.returnValue = "Si acepta, su sesión será cerrada!!!";

}

//TODO :: fredy.wilches :: documentar!!!! :: pedro.garcia
function cerrando() {
    var pagina = location.href;
    //TODO :: ¿por qué esos números de coordenadas?. "En la literatura dice que  there is no reliable cross–browser way to find the mouse coordinates relative to the document" :: pedro.garcia
    if (document.body.onbeforeunload != "" && ((window.event.clientX < 0 && window.event.clientX > -3060) || (window.event.clientY < 0 && window.event.clientY > -3060) || (window.event.clientX < -9000 && window.event.clientY < -9000))) {

        location.href = "/snc/logout";
        alert("Sesión cerrada");

    }

}

//TODO :: fredy.wilches :: verificar si no se usa; en tal caso, borrarla :: pedro.garcia
function cerrar() {
    document.body.onbeforeunload = "";
    window.close();
    return true;
}

//TODO :: fredy.wilches :: verificar si no se usa; en tal caso, borrarla :: pedro.garcia
function salir() {
    if (confirm('Realmente desea salir?')) {
        location.href = "/snc/logout";
        return true;
    }
    return false;
}

//TODO :: fredy.wilches :: verificar si no se usa; en tal caso, borrarla :: pedro.garcia      
function deshabilitar() {
    numeroClick++;
    if (numeroClick == 1)
        return true;
    else
        return false;
}

// Funcion para limitar el numero de characteres especialmente en los textArea
// objeto es el text area
// numero de caracteres
// alerta: si se muestra o no
// Se agrega = para que la condición del if se satisfaga cuando la longitud del campo
// sea mayor o igual al numero que se le envíe, de lo contrario funcionaba con
// un carácter más y generaba error al guardar en la base de datos, además se añadió
//que el nuevo valor vaya hasta el número limite -2 ya que empieza desde cero
function controlarNroChars(objeto, numero, alerta) {
    if (objeto.value.length >= numero) {
        objeto.value = objeto.value.substr(0, numero - 2);
        if (alerta)
            alert("No se pueden digitar más de " + numero + " carácteres");
    }
}



// Funcion para direccionar desde evento a una url particular del editor geográfico
// url Url donde se va a direccionar.
function abrirEditor(url)
{
    // var currentUrl = document.URL;

    //try{
    window.open(url, 'util');
//        }catch(err){
//            dialogWV.show();
//            
//        }
}

PrimeFaces.locales['es'] = {
    closeText: 'Aceptar',
    prevText: 'Anterior',
    nextText: 'Siguiente',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    weekHeader: 'Semana',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Sólo hora',
    timeText: 'Tiempo',
    hourText: 'Hora',
    minuteText: 'Minuto',
    secondText: 'Segundo',
    currentText: 'Fecha actual',
    ampm: false,
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    allDayText: 'Todo el día'
};

//Función para validar que sólo se ingresen números en un campo, cuando ingresa 
//un carácter distinto lo borra
//tomado de esta página http://lineadecodigo.com/javascript/validar-que-el-texto-introducido-es-un-numero/
//javier.aponte

function validarSiNumero(objeto, numero) {
    if (!/^([0-9])*$/.test(numero)) {
//	      alert("El valor " + numero + " no es un número");
        objeto.value = objeto.value.substr(0, objeto.value.length - 1);
    }
}


//Función para desactivar la tecla f5 porque cuando el usuario oprime esa tecla se dañan
//las funcionalidades del sistema
//tomado de esta página http://www.mozillaes.org/foros/viewtopic.php?t=13350
//javier.aponte

document.onkeydown = function (e) {
    if (e)
        document.onkeypress = function () {
            return true;
        }

    var evt = e ? e : event;
    if (evt.keyCode == 116) {
        if (e)
            document.onkeypress = function () {
                return false;
            }
        else {
            evt.keyCode = 0;
            evt.returnValue = false;
        }
    }
};

function openLoadBarLogin() {
    document.getElementById('loadBar').style.visibility = 'visible';
}

/*
 * Manejo de error con dialogo de listado de validaciones
 * felipe.cadena
 */
function handleErrorValidateList(xhr, status, args, dialogWVOK, dialogWVErr, open) {

    if (args != null) {
        if (args.error == null && args.validationFailed == null
                || (args.validationFailed != null && args.validationFailed == false)) {
            if (open) {
                dialogWVOK.show();
            } else {
                dialogWVOK.hide();
            }
        } else {
            dialogWVErr.show();
        }
    }
}


/*
 * Muestra un dialogo alterantivo cuando error
 * felipe.cadena
 */
function showAlternativeDialog(args, actualDialog, dialogErr) {

    if (args != null) {
        if (args.error == null && args.validationFailed == null
                || (args.validationFailed != null && args.validationFailed == false)) {
                actualDialog.hide();
        } else {
            dialogErr.show();
        }
    }
}

//Función para validar que sólo se ingresen números en un campo, cuando ingresa
//ademas que la parde decimal solo tenga dos valores 
//un carácter distinto lo borra
//leidy.gonzalez

function validarSiNumeroYDosDecimales(objeto, numero) {
    var decimal;
    var entero;
     if (!/^[0-9]*\.?[0-9]*$/.test(numero)) {
    	 if(objeto.value.indexOf(",")>0)
         objeto.value = objeto.value.substr(0, objeto.value.indexOf(","));
    	// objeto.value = objeto.value.replaceAll(",","");
    	 numero= objeto.value;
    }
   
    if (numero !== null && numero !=="") {
        if (numero.indexOf('.') !== -1) {
            var datos = numero.split('.');

            for (var i = 0; i < datos.length; i++) {
            	 if (i === 0) {
                     entero = datos[i].toString();
                }

                if (i === 1) {
                    if (datos[i].length > 2) {
                        decimal = (datos[i].substring(0, 2)).toString();
                    } else {
                        decimal = datos[i].toString().toString();
                    }
                }
            }

            objeto.value = entero + '.' + decimal;
        } 
    }

}

function soloNumerosConPunto(e){  
	var tcl = (document.all)?e.keyCode:e.which;
	
	if ((tcl >= 48 && tcl <= 57) || tcl == 46|| tcl == 8 || tcl == 0 || tcl == 188) {
		return true;
	}else{
		return false;
	}
}

function onChangeSlider(inputRangeWV, slider){

   var output = document.getElementById(inputRangeWV.id);
   output.value = slider.value;
}
//

function onChangeInputSlider(slider, input){
   slider.value = input.value;
}






    