var codigoSeleccionado = null;
var codigoSectorSeleccionado = null;
var codigoZona = null;
var moduleLayer = null;
var moduleTools = null;
var usuarioDto = null;
var predioRuralOfertasMapa = null;
var isIframeInitialized = false;
var iframeSrc = undefined;
var idOfertaMapa = null;
var municipioCodMapa = null;
var paramSeleccionManzanasOfertas = null;
var paramSeleccionPrediosActualizacion = null;
var paramIsRuralCU208 = null;
var idRecolectorMapa = null;
var securityToken = null;
var bodyRef;
var zhgSeleccionadas;
var zhfSeleccionadas;
var codigoMunicipioZona;

// numero predial en el buscardo de zonas por predio
var numeroPredial;


//D: ubicación base del archivo con el mapa
var baseMapPath = "comun/mapa.html";

//D: ubicación relativa del mapa. El valor inicial no importa porque se calcula luego
var relativeMapPath = "../comun/mapa.html";

jQuery(function ($) {
    //Ejecutar código al cargar un iframe
    $('#mapIframe').load(function () {
        iframeSrc = $('#mapIframe').attr('src');

        if (iframeSrc != undefined && iframeSrc != "") {
            bodyRef = $('#mapIframe').get(0).contentWindow.$("body");
            bodyRef.bind('onLoadXMLOk', onLoadXMLOkHandler);
            bodyRef.bind('onSNCMapCreated', onSNCMapCreatedHandler);
            bodyRef.bind('onSelectedPredio', onSelectedPredioHandler);
            bodyRef.bind('onSelectedManzanaOfertas', onSelectedManzanaOfertasHandler);
            bodyRef.bind('onSelectedManzanasActualizacion', onSelectedManzanasActualizacionHandler);
            bodyRef.bind('mostrarBarraCargaEvent', mostrarBarraCargaEventHandler);
            bodyRef.bind('ocultarBarraCargaEvent', ocultarBarraCargaEventHandler);
        }
    });
});

/**
 * 
 * Cambia la visibilidad del iframe del mapa
 * y hace zoom a un municipio, zona o sector
 * 
 * Este llamado se usa UNICAMENTE cuando se va a abrir el mapa
 * al hacer una seleccion de los combos de municipio, zona o sector
 * 
 * @param appName, moduleLayerParam, moduleToolsParam
 */
function cargarMapa(appName, securityTokenParam, moduleLayerParam, moduleToolsParam) {
    moduleLayer = moduleLayerParam;
    moduleTools = moduleToolsParam;
    securityToken = securityTokenParam;

    codigoSeleccionado = $('select[id*="consultaPredioMunicipio"]').val()
            .substring(0, 5);

    if (codigoSeleccionado == undefined || codigoSeleccionado == "") {
        warningMunicipioVar.show();
        return;
    }

    if (codigoSeleccionado != undefined || codigoSeleccionado != "") {
        if (moduleTools == "of") {
            codigoZona = $('select[id*="zonaSeleccionada"]').val()
                    .substring(0, 7);
            if (codigoZona == null
                    || codigoZona == undefined || codigoZona == "") {
                warningZonaVar.show();
                //alert('Debe seleccionar una zona');
                return;
            } else if (codigoZona != null
                    || codigoZona != undefined) {
                onChangeZona();
                codigoSectorSeleccionado = $('select[id*="sectorSeleccionado"]')
                        .val();
            }
        }
    }

    cargarContenidoIFrame(appName);
}

/**
 * Cambia la visibilidad del iframe del mapa
 * y hace zoom a un feature
 * 
 * Este llamado se usa UNICAMENTE cuando se le va a pasar el codigo de algun feature
 * para que cuando se abra el mapa le haga zoom de una vez.
 * 
 * @param appName
 * @param securityTokenParam
 * @param moduleLayerParam
 * @param moduleToolsParam
 * @param codigoManzanaVeredaSeleccionada
 * @returns {Boolean}
 */
function cargarMapaZoomToFeature(appName, securityTokenParam, moduleLayerParam, moduleToolsParam, codigo) {

    console.log("codigo recibido: " + codigo);
    console.log("security token: " + securityTokenParam);
    console.log("moduleTools recibido: " + moduleToolsParam);
    console.log("moduleLayer recibido: " + moduleLayerParam);
    securityToken = securityTokenParam;
    moduleLayer = moduleLayerParam;
    moduleTools = moduleToolsParam;
    codigoSeleccionado = codigo;
    codigoMunicipioZona = null;

    if (cargarContenidoIFrame(appName)) {
        zoomToFeatureSeleccionado();
    }
}

/**
 * Carga el mapa de zonas seleccionadas para un municipio especifico
 * 
 * @param  appName
 * @param  securityTokenParam
 * @param  moduleLayerParam
 * @param  moduleToolsParam
 * @param  municipioZona
 * @param  codigo codigo
 * @param  codigosZonas
 * @returns {undefined}
 * 
 * @author andres.eslava
 */
function cargarMapaZonaPorPredio(appName, securityTokenParam, moduleLayerParam, moduleToolsParam, municipioZona, codigosZonasFisicas, codigosZonasGeoeconomicas, numeroPredialSeleccionado) {

    console.log("appName : " + appName);
    console.log("Security Token : " + securityTokenParam);
    console.log("module Layer : " + moduleLayerParam);
    console.log("module Tools : " + moduleToolsParam);
    console.log("Cargando zoom zonas para el municipio con codigo:" + municipioZona);

    securityToken = securityTokenParam;
    moduleLayer = moduleLayerParam;
    moduleTools = moduleToolsParam;
    codigoMunicipioZona = municipioZona;
    zhfSeleccionadas = codigosZonasFisicas;
    zhgSeleccionadas = codigosZonasGeoeconomicas;
    numeroPredial = numeroPredialSeleccionado;

    if (cargarContenidoIFrame(appName)) {
        zoomToFeatureSeleccionado();
    }

}
function cargarContenidoIFrame(appName) {
    iframeSrc = $('#mapIframe').attr('src');
    console.log("iframe " + iframeSrc);
    if (iframeSrc == undefined || iframeSrc == "") {

        statusDialog2.show();
        getMapaPageRelativeUrl(appName);
        $('#mapIframe').attr('src', relativeMapPath);
        console.log("new mapIframe url: " + $('#mapIframe').attr('src'));
        return false;
    }
    return true;
}
/**
 * Funcion que toma el codigo recibido de la carga del mapa y hace zoom al feature
 * dependiendo del largo del codigo
 * 
 */
function zoomToFeatureSeleccionado() {

    if (codigoSeleccionado == null && codigoMunicipioZona == null) {
        return;
    }
    if (codigoSectorSeleccionado != null) {
        if (codigoSectorSeleccionado[0].length > 5) {
            onChangeSector();
        }
    }
    if ((codigoSectorSeleccionado == null || codigoSectorSeleccionado[0].length < 5) && codigoMunicipioZona == null) {
        if (codigoSeleccionado.length == 5) {
            zoomToMunicipio(codigoSeleccionado);
        } else if (codigoSeleccionado.length == 30) {
            if ((codigoSeleccionado.substring(21, 22) == "5") || (codigoSeleccionado.substring(21, 22) == "6") && (codigoSeleccionado.substring(26) != "0000")) {
                zoomToConstruccion(codigoSeleccionado);
            } else {
                zoomToPredio(codigoSeleccionado);
            }
        } else if (codigoSeleccionado.length == 17) {
            zoomToManzanaVereda(codigoSeleccionado);
        } else if (codigoSeleccionado.length > 30) {
            var codigoUni = codigoSeleccionado.split(",", 1);
            var codigoPredioStr = codigoUni.toString();
            if (codigoPredioStr.length == 17) {
                zoomToManzanasVeredas(codigoSeleccionado);
            } else if (codigoPredioStr.length == 30) {
                if ((codigoSeleccionado.substring(21, 22) == "5") || (codigoSeleccionado.substring(21, 22) == "6") && (codigoSeleccionado.substring(26) != "0000")) {
                    zoomToConstrucciones(codigoSeleccionado);
                } else {
                    zoomToPredios(codigoSeleccionado);
                }

            }
        }
    }

    if (codigoZona != null) {
        isPredioRural(codigoZona);
        codigoZona = null;
    }

    enviarModulos(moduleLayer, moduleTools);

    if (moduleTools == "of") {
        getParametrosOfertas();
    }
    if (moduleTools == "ac" || moduleTools == "ac302") {
        getParametrosActualizacion();
    }

    if (codigoMunicipioZona != null && (zhfSeleccionadas != null || zhgSeleccionadas != null)) {
        zoomToZonasSeleccionadas(codigoMunicipioZona, zhfSeleccionadas, zhgSeleccionadas, numeroPredial);
    }
}

/**
 * 
 * Funcion que envia los modulos de capas y herramientas 
 * @param moduleLayer
 * @param moduleTools
 */
function enviarModulos(moduleLayer, moduleTools) {
    $('#mapIframe')[0].contentWindow.recibirModulos(moduleLayer, moduleTools);
}

/**
 * Esta función se llama en el onchange del combobox de municipios para que actualice el mapa 
 * cuando se cambia un municipio
 */
function onChangeMunicipio() {
    codigoSeleccionado = $('select[id*="consultaPredioMunicipio"]').val()
            .substring(0, 5);
    console.log("onChangeMunicipio: " + codigoSeleccionado);
    if (isIframeInitialized) {
        zoomToMunicipio(codigoSeleccionado);
    }
}

/**
 * Esta función se llama en el onchange del combobox de zonas
 */
function onChangeZona() {
    codigoZona = $('select[id*="zonaSeleccionada"]').val()
            .substring(0, 7);
    console.log("onChangeZona: " + codigoZona);
    if (isIframeInitialized) {
        isPredioRural(codigoZona);
    }
}

/**
 * Esta función se llama en el onchange del combobox de sectores para que actualice el mapa 
 * cuando se cambia un sector
 */
function onChangeSector() {
    codigoSectorSeleccionado = $('select[id*="sectorSeleccionado"]').val();
    console.log("onChangeSector: " + codigoSectorSeleccionado);
    if (isIframeInitialized) {
        if (codigoSectorSeleccionado != null) {
            if (codigoSectorSeleccionado.length == 1) {
                zoomToSector(codigoSectorSeleccionado);
            } else if (codigoSectorSeleccionado.length > 1) {
                zoomToSectores(codigoSectorSeleccionado);
            }
        }
    }
}

/**
 * 
 * Funcion que envia los parametros necesarios para los servicios de ofertas inmobiliarias
 */
function getParametrosOfertas() {
    usuarioDto = $('input:hidden[id*="usuarioDtoMapa"]').val();
    predioRuralOfertasMapa = $('input:hidden[id*="predioRuralOfertasMapa"]').val();
    paramSeleccionManzanasOfertas = $('input:hidden[id*="paramSeleccionManzanasOfertas"]').val();
    idOfertaMapa = $('input:hidden[id*="idOfertaMapa"]').val();
    municipioCodMapa = $('input:hidden[id*="municipioCodMapa"]').val();
    idRecolectorMapa = $('input:hidden[id*="idRecolectorMapa"]').val();
    $('#mapIframe')[0].contentWindow.getParametrosOfertas(usuarioDto, idOfertaMapa, municipioCodMapa, paramSeleccionManzanasOfertas, predioRuralOfertasMapa, idRecolectorMapa, codigoSeleccionado);
}

function getParametrosActualizacion() {
    paramSeleccionPrediosActualizacion = $('input:hidden[id*="paramSeleccionPrediosActualizacion"]').val();
    paramIsRuralCU208 = $('input:hidden[id*="paramIsRuralCU208"]').val();
    console.log("getParametrosActualizacion_client: " + paramSeleccionPrediosActualizacion)
    console.log("getParametrosActualizacion_client: " + paramIsRuralCU208)
    $('#mapIframe')[0].contentWindow.getParametrosActualizacion(paramSeleccionPrediosActualizacion, codigoSeleccionado, paramIsRuralCU208);
}
/**
 * 
 * Llama a la funcion que verifica si un codigo es rural o urbano
 * @param codigoZona
 */
function isPredioRural(codigoZona) {
    $('#mapIframe')[0].contentWindow.isPredioRural(codigoZona);
}

/**
 * Esta función es el listener del evento close del diálogo que contiene al mapa
 */
function onCloseDivWV() {
    iframeSrc = undefined;
    isIframeInitialized = false;
    $('#mapIframe').attr('src', "");
}

/**
 * 
 * Respuesta al evento de selección de predio en el mapa
 */
function onSelectedPredioHandler(evt) {
    console.log("onSelectedPredioHandler: en el cliente");
    console.log("evt.predioSeleccionado: " + evt.predioSeleccionado);

    $('input:hidden[id*="numeroPredialHidden666"]').val(evt.predioSeleccionado);

    //PrimeFaces.ab({formId:'consultaPredioForm',source:'adicionarBtn',process:'adicionarBtn numeroPredial',update:'formPS msjPredio',oncomplete:function(xhr, status, args){prediosSeleccionadosDlgWV.hide();prediosSeleccionadosDlgWV.show();;}});

    //document.getElementById('adicionarBtn').click();
    //N: no se pudo identificar con algún selector de jQuery (y debe ser commandLink)
    //N: hay que decirle 'id$' para que encuentre algo cuyo atributo id termine en linkAdicionarPredioASeleccionados666
    //  porque con el cambio a primefaces 3.2 los id se arman de forma estricta (en este caso el idDelCC:idDelLink
    $('*[id$="linkAdicionarPredioASeleccionados666"]').click();
}

/**
 * 
 * Respuesta al evento de selección de manzanas para ofertas en el mapa
 * @param evt
 */
function onSelectedManzanaOfertasHandler(evt) {
    console.log("onSelectedManzanaOfertasHandler: en el cliente");
    console.log("evt.manzanaSeleccionada: " + evt.manzanaSeleccionada);

    $('input:hidden[id*="codigoManzanaHidden666"]').val(evt.manzanaSeleccionada);
    $('*[id$="linkAdicionarManzanaASeleccionados666"]').click();
}

/**
 * 
 * Respuesta al evento de selección de predios en actualizacion
 * @param evt
 */
function onSelectedManzanasActualizacionHandler(evt) {
    console.log("onSelectedManzanasActualizacionHandler: en el cliente");
    console.log("evt.manzanasActualizacion: " + evt.manzanasSeleccionadas);

    $('input:hidden[id*="codigoManzanasHidden666"]').val(evt.manzanasSeleccionadas);
    $('*[id$="linkAdicionarManzanasASeleccionados666"]').click();
}

/**
 * 
 * Respuesta a la notificación del evento de creación del mapa
 * @param evt
 */
function onSNCMapCreatedHandler(evt) {
    console.log("onSNCMapCreatedHandler");
    isIframeInitialized = true;
    statusDialog2.hide();
    zoomToFeatureSeleccionado();
    divWV.show();
}

function onLoadXMLOkHandler(evt) {
    console.log("onLoadXMLOkHandler");
    console.log("inicializando mapa...");
    $('#mapIframe')[0].contentWindow.initMap(securityToken, moduleLayer, moduleTools);
}

/**
 * 
 * Función que valida si el mapa tiene un dibujo o no
 */
function validarDibujo() {
    if (isIframeInitialized) {
        $('#mapIframe')[0].contentWindow.validarDibujo();
    } else {
        warningNoDibujo.show();
        return;
    }
}


/*
 * Carga las zonas homogeneas a las que pertenece el municipio
 * @returns {undefined}
 * @author andres.eslava
 */
function zoomToZonasSeleccionadas(codigoMunicipioZona, codigosZonasFisicas, codigosZonasGeoeconomicas, numeroPredialSeleccionado) {
    $('#mapIframe')[0].contentWindow.zoomToZonas(codigoMunicipioZona, codigosZonasFisicas, codigosZonasGeoeconomicas, numeroPredialSeleccionado);
}

/**
 * Realiza zoom a un municipio específico
 * @param codigoMunicipio
 */
function zoomToMunicipio(codigoMunicipio) {
    $('#mapIframe')[0].contentWindow.zoomToMunicipio(codigoMunicipio);
}

/**
 * Realiza zoom a un sector específico
 * @param codigoSector
 */
function zoomToSector(codigoSector) {
    $('#mapIframe')[0].contentWindow.zoomToSector(codigoSector);
}

/**
 * 
 * Realiza zoom a varios sectores
 * @param codigoSector
 */
function zoomToSectores(codigoSector) {
    $('#mapIframe')[0].contentWindow.zoomToSectores(codigoSector);
}

/**
 * Realiza zoom a un predio específico
 * D: la función zoomToPredio que se llama dentro de esta es la que está en el js snc_visor.js, el
 *      cual se carga al asignarle como source al iframe la pagina mapa.html
 * @param codigoPredio
 */
function zoomToPredio(codigoPredio) {
    $('#mapIframe')[0].contentWindow.zoomToPredio(codigoPredio);
}

/**
 * Realiza zoom a una construccion
 * D: la función zoomToConstruccion que se llama dentro de esta es la que está en el js snc_visor.js, el
 *      cual se carga al asignarle como source al iframe la pagina mapa.html
 * @param codigoPredio
 */
function zoomToConstruccion(codigoPredio) {
    $('#mapIframe')[0].contentWindow.zoomToConstruccion(codigoPredio);
}

/**
 * Realiza zoom a varios predios
 * D: la función zoomToPredios que se llama dentro de esta es la que está en el js snc_visor.js, el
 *      cual se carga al asignarle como source al iframe la pagina mapa.html
 * @param codigoPredio
 */
function zoomToPredios(codigoPredio) {
    $('#mapIframe')[0].contentWindow.zoomToPredios(codigoPredio);
}

/**
 * Realiza zoom a varias construcciones
 * D: la función zoomToConstrucciones que se llama dentro de esta es la que está en el js snc_visor.js, el
 *      cual se carga al asignarle como source al iframe la pagina mapa.html
 * @param codigoPredio
 */
function zoomToConstrucciones(codigoPredio) {
    $('#mapIframe')[0].contentWindow.zoomToConstrucciones(codigoPredio);
}

/**
 * Realiza zoom a las manzanas o veredas especificadas
 * @param manzanaVeredaSeleccionada
 */
function zoomToManzanaVereda(manzanaVeredaSeleccionada) {
    $('#mapIframe')[0].contentWindow
            .zoomToManzanaVereda(manzanaVeredaSeleccionada);
}

/**
 * Realiza zoom a las manzanas o veredas asignadas a una persona
 * @param codigoSeleccionado
 */
function zoomToManzanasVeredas(codigoSeleccionado) {
    $('#mapIframe')[0].contentWindow
            .zoomToManzanasVeredas(codigoSeleccionado);
}

/**
 * obtiene el url relativo del mapa.html
 * @author pedro.garcia
 * @param appName nombre de la aplicación que debe figurar en el url del cliente
 */
function getMapaPageRelativeUrl(appName) {

    relativeMapPath = "/" + appName;
    relativeMapPath += "/comun/mapa.html";
    console.log("ruta absoluta de mapa.html = " + relativeMapPath);
}

function seleccionarAreaOfertaPorAsignar() {
    $('#mapIframe')[0].contentWindow.executeSelectManzanaTask();
}

/**
 * 
 * Ejecuta el servicio de seleccionar area para oferta inmobiliaria
 */
function ejecutarServicioSeleccionarOferta() {
    console.log("entra a ejecutarServicioSeleccionarOferta");
    if (isIframeInitialized) {
        console.log("Iframe inicializado");
        getParametrosOfertas();
        statusDialog2.show();
        $('#mapIframe')[0].contentWindow.ejecutarServicioSeleccionarOferta();
        statusDialog2.hide();
    }
}

function mostrarBarraCargaEventHandler(evt) {
    statusDialog2.show();
}

function ocultarBarraCargaEventHandler(evt) {
    statusDialog2.hide();
}

/**
 * 
 * Carga el visor por medio de los combos de seleccion de municipios, pero se le envia
 * un parametro adicional de validacion, en el que si es true carga el mapa si no, no
 * 
 * 
 * @param appName, moduleLayerParam, moduleToolsParam, banderaCargaMapaParam
 */
function cargarMapaConValidacion(appName, securityTokenParam, moduleLayerParam, moduleToolsParam, banderaCargaMapaParam) {
    if (!banderaCargaMapaParam || banderaCargaMapaParam == undefined) {
        cargarMapa(appName, securityTokenParam, moduleLayerParam, moduleToolsParam);
    }
}

function autoResize(id) {
    var newheight;
    var newwidth;

    if (document.getElementById) {
        newheight = document.getElementById(id).contentWindow.document.body.scrollHeight;
        newwidth = document.getElementById(id).contentWindow.document.body.scrollWidth;
//		newheight = document.getElementById(id).contentDocument.scrollHeight;
//		newwidth = document.getElementById(id).contentDocument.scrollWidth;

    }

    document.getElementById(id).height = (newheight) + "px";
    document.getElementById(id).width = (newwidth) + "px";
    alert("height " + newheight + "width " + newwidth);
}
