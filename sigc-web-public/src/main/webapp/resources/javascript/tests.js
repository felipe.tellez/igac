
//---------------------------------------------------------------------

var element;
var lastFocusedInput;

function sayHello2(name) {
    var text = 'Hello ' + name; // local variable
    var sayAlert = function () {
        alert(text);
    }
    return sayAlert;
}

function regainFocus() {
    //var element = $(document.activeElement);
    console.log("on regainFocus...");
    $("input:last").focus();  //toma el input definido en idleMonitor.xhtml
}

jQuery.expr[':'].focus = function (elem) {
    return elem === document.activeElement && (elem.type || elem.href);
};

function regainFocus2() {
    //element = getFocusedElement();
    //element = $("body").getFocusedElement();
    //element === document.activeElement && ( element.type || element.href );
    element = $.fn.getFocusedElement(); //funciona el llamado, pero toma el div del dialog del ajaxstatus

    //D: parece que hay que tener como un target para ejecutar la función (que puede ser ninguno ($.) u
    //    otra cosa.
    //  no sirve: $.alertar(); ni $().alertar
    $.fn.alertar();  //funciona
    //$("body").alertar(); //funciona
    console.log("on regainFocus2...");
    var focus2 = function () {
        $(element).focus();
    }
    return focus2();
}

function regainFocus3() {
    console.log("on regainFocus3...");
    console.log("último foco:" + lastFocusedInput);
    //$(element).focus();
    //console.log($(lastFocusedInput));
    if (lastFocusedInput) {
        lastFocusedInput.focus();
        //lastFocusedInput.value("lola");
//    $(lastFocusedInput).focus();
//    $(lastFocusedInput).value("lola");
    }

}

jQuery.fn.getFocusedElement = function () {
    var elem = document.activeElement;
    return $(elem && (elem.type || elem.href) ? elem : []);
};

jQuery.fn.alertar = function () {
    alert('hola lola desde función jQuery');
};



//N: esto hay ue hacerlo para que quede como en el contexto (?. no entiendo bien la vaina) de jquery.
//  Algo como inicializar el handler para en evento onFocus
jQuery(function ($) {
    console.log("entra al init de jquery");
    //D: setup a handler for the onFocus event
    $(":input").focus(function () {
        lastFocusedInput = this;
        console.log("foco actual:" + lastFocusedInput);
    });
});