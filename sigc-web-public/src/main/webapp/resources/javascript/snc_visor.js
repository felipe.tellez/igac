dojo.require("dijit.dijit"); // optimize: load dijit layer
dojo.require("dijit.Dialog");
dojo.require("dijit.form.Button");
dojo.require("dijit.form.CheckBox");
dojo.require("dijit.form.ComboBox");
dojo.require("dijit.form.Slider");
dojo.require("dijit.layout.AccordionContainer");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.TabContainer");
dojo.require('dijit.Menu');
dojo.require('dijit.MenuItem');
dojo.require('dijit.PopupMenuItem');
dojo.require("dijit.TitlePane");
dojo.require("dijit.Toolbar");
dojo.require("dojo.DeferredList");
dojo.require("dojo.io.script");
dojo.require("dojo.string");
dojo.require("dojox.layout.FloatingPane");
dojo.require("esri.arcgis.utils");
dojo.require("esri.dijit.Legend");
dojo.require("esri.dijit.Measurement");
dojo.require("esri.dijit.OverviewMap");
dojo.require("esri.dijit.Popup");
dojo.require("esri.dijit.Scalebar");
dojo.require("esri.layers.FeatureLayer");
dojo.require("esri.map");
dojo.require("esri.SnappingManager");
dojo.require("esri.toolbars.draw");
dojo.require("esri.toolbars.navigation");
dojo.require("esri.tasks.gp");
dojo.require("esri.tasks.query");

/**
 * 
 * ******************************************************************************************
 * ******************************Variables globales******************************************
 * ******************************************************************************************
 * 
 */

var map = null;
var navToolbar = null;
var drawToolbar = null;
var loading = null;
var geometryType = null;
var moduleLayerParam = null;
var moduleToolsParam = null;
var gp = null;

var basemaps = [];
var operationalLayers = [];
var legendLayers = [];
var mapBoxConfig = null;

var identifyPredioTask;
var identifyZHFTask;
var identifyZHGTask;
var identifyManzanasTask;
var findPredioTask;
var findMunicipioTask;
var findManzanaTask;
var findZonasTask;
var handleIdentify = null;
var handleSelectPredio = null;
var handleSelectManzana = null;
var basemapSelectionHandler = null;
var resultIsPredioRural = false;
var usuarioDto = null;
var geoservicesObj = new Object();
var layersConfig = [];
var featureSet = null;
var predioRuralOfertas = null;
var municipioCodMapa = null;
var paramSeleccionManzanasOfertas = null;
var idOfertaMapa = null;
var idRecolectorMapa = null;
var features = [];
var totalExtentZoomToPredios = [];
var graphicLayerTerrenoUrbano;
var graphicLayerTerrenoRural;
var polygonSymbol;
var contadorFeatures = 0;
var mapConfig = new Object();
var securityTokenParam = null;
var identifyParams = new esri.tasks.IdentifyParameters();
var codigosManzanasParaAsignaryEjecutar;
var codigosManzanasActualizacion;
var paramSeleccionPrediosActualizacion;
var paramIsRuralCU208;

/**
 * Índices de las capas de trabajo (servicios de arcgis server config.xml)
 */
var LAYER_MUNICIPIOS_INDEX = "capaMunicipios";
var LAYER_PREDIOS_INDEX = "capaPredios";
var LAYER_OFERTAS_INDEX = "capaOfertas";
var LAYER_ZHF_INDEX = "capaZHF";
var LAYER_ZHG_INDEX = "capaZHG";

var OFERTA_ESTADO_ENEJECUCION = "ASIGNADA";
var OFERTA_ESTADO_PORASIGNAR = "";
var OFERTA_ESTADO_CONTROLCALIDAD = "CONTROL DE CALIDAD";
var OFERTA_ESTADO_FINALIZADO = "FINALIZADA";

/**
 * Mínimo nivel de zoom para activar la barra de herramientas
 */
var MIN_PREDIO_TOOLBAR_LEVEL = 9;

/**
 * Códigos de las capas en arcgis server
 */
var INDEX_TERRENO_PREDIO_RURAL = 13;
var INDEX_TERRENO_PREDIO_URBANO = 5;
var INDEX_CONSTRUCCION_PREDIO_RURAL = 12;
var INDEX_CONSTRUCCION_PREDIO_URBANO = 4;
var INDEX_MANZANAS_URBANO = 6;
var INDEX_VEREDAS_RURAL = 14;
var INDEX_SECTOR_URBANO = 8;
var INDEX_SECTOR_RURAL = 15;
var INDEX_MANZANAS_OFERTAS = 1;
var INDEX_VEREDAS_OFERTAS = 0;


var INDEX_ZHU_FISICA = 1;
var INDEX_ZHR_FISICA = 2;

var INDEX_ZHU_GEOECONOMICA = 1;
var INDEX_ZHR_GEOECONOMICA = 2;

/**
 * Municipios indice de la capa de municipios en el servicio
 */
var INDEX_MUNICIPIOS = 0;

/**
 * Municipios nombre del atributo que tiene el codigo del municipio en la capa
 */
var LAYER_MUNICIPIOS_CODIGO = "ID_ESPACIA";


/**
 * 
 * ******************************************************************************************
 * ************************Métodos de inicialización del mapa********************************
 * ******************************************************************************************
 * 
 */


/**
 * 
 * Carga de XML de configuración
 */
function loadXML() {
    var xhrArgs = {
        url: "../resources/xml/config.xml",
        handleAs: "xml",
        preventCache: true,
        load: parseXML,
        error: function (error) {
            console.log("An unexpected error occurred: " + error);
        }
    };
    // Call the asynchronous xhrGet
    var deferred = dojo.xhrGet(xhrArgs);
}

/**
 * 
 * Realiza el parsing del xml de configuración
 * 
 * @param data
 */
function parseXML(data) {
    var xmlMap = data.getElementsByTagName("map")[0];

    mapConfig.xmin = parseFloat(xmlMap.getAttribute("xmin"));
    mapConfig.ymin = parseFloat(xmlMap.getAttribute("ymin"));
    mapConfig.xmax = parseFloat(xmlMap.getAttribute("xmax"));
    mapConfig.ymax = parseFloat(xmlMap.getAttribute("ymax"));
    mapConfig.wkid = parseFloat(xmlMap.getAttribute("wkid"));

    var xmlGeo = data.getElementsByTagName("geoservice")[0];
    geoservicesObj.geoserviceUrl = xmlGeo.getAttribute("url");

    var xmlLayers = data.getElementsByTagName("layer");
    for (var i = 0; i < xmlLayers.length; i++) {
        var xmlLayer = xmlLayers[i];
        var layerObj = new Object();
        layerObj.layerId = xmlLayer.getAttribute("id");
        layerObj.layerLabel = xmlLayer.getAttribute("label");
        layerObj.layerType = xmlLayer.getAttribute("type");
        layerObj.layerAlpha = parseFloat(xmlLayer.getAttribute("alpha"));
        layerObj.layerThumb = xmlLayer.getAttribute("thumb");
        layerObj.layerUrl = xmlLayer.getAttribute("url");
        layerObj.visiblelayers = xmlLayer.getAttribute("visiblelayers");
        layerObj.visible = xmlLayer.getAttribute("visible").bool();
        layerObj.basemap = xmlLayer.getAttribute("basemap").bool();
        layerObj.moduleLayer = xmlLayer.getAttribute("moduleLayer");
        layerObj.moduleTools = xmlLayer.getAttribute("moduleTools");
        layerObj.legend = xmlLayer.getAttribute("legend");
        layerObj.securityToken = xmlLayer.getAttribute("needSecurity");
        layersConfig.push(layerObj);
    }

    //initMap(mapConfig, layersConfig);
    var event = jQuery.Event("onLoadXMLOk");
    $("body").trigger(event);
}

/**
 * 
 * Busca una capa en el XML por medio de su id
 * 
 * @param id
 * @returns
 */
function getLayerById(id) {
    var capaBuscada = null;
    for (var i = 0; i < layersConfig.length; i++) {
        if (layersConfig[i].layerId == id) {
            capaBuscada = layersConfig[i];
            break;
        }
    }
    return capaBuscada;

}

/**
 * 
 * Inicializa el mapa
 * 
 * @param mapConfig
 * @param layersConfig
 */
function initMap(securityTokenParam, moduleLayer, moduleTool) {
    console.log("init");
    loading = dojo.byId("loadingImg");

    this.securityTokenParam = securityTokenParam;
    // setup the popup window
    var popup = new esri.dijit.Popup({
        fillSymbol: new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                new esri.symbol.SimpleLineSymbol(
                        esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                        new dojo.Color([255, 0, 0]), 2), new dojo.Color([
            255, 255, 0, 0.25]))
    }, dojo.create("div"));

    var initExtent = new esri.geometry.Extent({
        "xmin": mapConfig.xmin,
        "ymin": mapConfig.ymin,
        "xmax": mapConfig.xmax,
        "ymax": mapConfig.ymax,
        "spatialReference": {
            "wkid": mapConfig.wkid
        }
    });

    map = new esri.Map("mapDiv", {
        infoWindow: popup,
        extent: initExtent,
        logo: false
    });

    dojo.place(popup.domNode, map.root);
    navToolbar = new esri.toolbars.Navigation(map);
    createMapLayers(layersConfig, securityTokenParam, moduleLayer, moduleTool);

    createGraphicsLayers();

    esri.config.defaults.map.slider = {
        left: "15px",
        top: "60px",
        width: null,
        height: "200px"
    };

    dojo.connect(dijit.byId('map'), 'resize', resizeMap);
    dojo.connect(map, "onExtentChange", onExtentChange);
    dojo.connect(map, "onMouseMove", showCoordinates);
    dojo.connect(map, "onMouseDrag", showCoordinates);
    dojo.connect(map, "onUpdateStart", showLoading);
    dojo.connect(map, "onUpdateEnd", hideLoading);
    dojo.connect(map, "onLoad", onMapLoad);
    dojo.connect(navToolbar, "onExtentHistoryChange",
            extentHistoryChangeHandler);


    $(document).ready(jQueryReady);

    polygonSymbol = new esri.symbol.SimpleFillSymbol(
            esri.symbol.SimpleFillSymbol.STYLE_SOLID,
            new esri.symbol.SimpleLineSymbol(
                    esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                    new dojo.Color([255, 0, 0]), 1),
            new dojo.Color([9, 255, 0, 0]));
}

/**
 * 
 * Creación de las capas del visor
 * 
 * @param layersConfig
 */



/*
 function createMapLayers(layersConfig, securityTokenParam, moduleLayer, moduleTool) {
 for ( var i = 0; i < layersConfig.length; i++) {
 var layerConfig = layersConfig[i];
 var layer = null;
 if (layerConfig.securityToken == "true") {
 if (layerConfig.layerType == "tiled") {
 layer = new esri.layers.ArcGISTiledMapServiceLayer(
 sncGeoservicioslayerConfig.layerUrl + "?token="
 + securityTokenParam, {
 "opacity" : layerConfig.layerAlpha,
 "id" : layerConfig.layerLabel
 });
 addLayerToMap(layer, layerConfig);
 } else if (layerConfig.layerType == "dynamic") {
 layer = new esri.layers.ArcGISDynamicMapServiceLayer(
 layerConfig.layerUrl + "?token="
 + securityTokenParam, {
 "opacity" : layerConfig.layerAlpha,
 "id" : layerConfig.layerLabel
 });
 if (layerConfig.moduleLayer == "all") {
 addLayerToMap(layer, layerConfig);
 }
 if (moduleLayer == "of") {
 if (moduleLayer == layerConfig.moduleLayer) {
 addLayerToMap(layer, layerConfig);
 }
 dojo.byId("legendOfertas").innerHTML = "<img id='arrw' src='../images/map/leyendas/capaOfertasLegend.JPG' />";
 }
 }
 } else if (layerConfig.securityToken == "false") {
 if (layerConfig.layerType == "tiled") {
 layer = new esri.layers.ArcGISTiledMapServiceLayer(
 layerConfig.layerUrl, {
 "opacity" : layerConfig.layerAlpha,
 "id" : layerConfig.layerLabel
 });
 addLayerToMap(layer, layerConfig);
 } else if (layerConfig.layerType == "dynamic") {
 layer = new esri.layers.ArcGISDynamicMapServiceLayer(
 layerConfig.layerUrl, {
 "opacity" : layerConfig.layerAlpha,
 "id" : layerConfig.layerLabel
 });
 if (layerConfig.moduleLayer == "all") {
 addLayerToMap(layer, layerConfig);
 }
 }
 } else if (layerConfig.layerType == "wms") {
 var layer1 = new esri.layers.WMSLayerInfo({
 name : layerConfig.visiblelayers,
 title : layerConfig.visiblelayers
 });
 var resourceInfo = {
 extent : new esri.geometry.Extent(-126.40869140625,
 31.025390625, -109.66552734375, 41.5283203125, {
 wkid : 4686
 }),
 layerInfos : [ layer1 ]
 };
 layer = new esri.layers.WMSLayer(layerConfig.layerUrl, {
 "opacity" : layerConfig.layerAlpha,
 "resourceInfo" : resourceInfo,
 "visibleLayers" : [ layerConfig.visiblelayers ],
 "id" : layerConfig.layerLabel
 });
 addLayerToMap(layer, layerConfig);
 } else if (layerConfig.layerType == "osm") {
 layer = new esri.layers.OpenStreetMapLayer({
 "opacity" : layerConfig.layerAlpha,
 "id" : layerConfig.layerLabel
 });
 addLayerToMap(layer, layerConfig);
 } else if (layerConfig.layerType == "mapbox") {
 console.log("mapbox...");
 mapBoxConfig = layerConfig;
 console.log("mapBoxConfig:" + mapBoxConfig.layerLabel);
 wax.tilejson(layerConfig.layerUrl, function(tj) {
 console.log("mapbox...response");
 console.log("mapBoxConfig response..:"
 + mapBoxConfig.layerLabel);
 var tjLayer = new wax.esri.connector(tj);
 tjLayer.id = mapBoxConfig.layerLabel;
 addLayerToMap(tjLayer, mapBoxConfig);
 console.log("mapbox...response end:" + tjLayer);
 });
 }
 layerConfig = null;
 gp = new esri.tasks.Geoprocessor(geoservicesObj.geoserviceUrl
 + "/OfertaInmobiliariaSeleccionar" + "?token=" + securityTokenParam);
 gp.setOutSpatialReference({
 wkid : mapConfig.wkid
 });
 }
 }
 */

function createMapLayers(layersConfig, securityTokenParam, moduleLayer, moduleTool) {
    for (var i = 0; i < layersConfig.length; i++) {
        var layerConfig = layersConfig[i];
        var layer = null;
        if (layerConfig.securityToken == "true") {
            if (layerConfig.layerType == "tiled") {
                layer = new esri.layers.ArcGISTiledMapServiceLayer(
                        sncGeoservicioslayerConfig.layerUrl + "?", {
                            "opacity": layerConfig.layerAlpha,
                            "id": layerConfig.layerLabel
                        });
                addLayerToMap(layer, layerConfig);
            } else if (layerConfig.layerType == "dynamic") {
                layer = new esri.layers.ArcGISDynamicMapServiceLayer(
                        layerConfig.layerUrl + "?", {
                            "opacity": layerConfig.layerAlpha,
                            "id": layerConfig.layerLabel
                        });
                if (layerConfig.moduleLayer == "all") {
                    addLayerToMap(layer, layerConfig);
                }
                if (moduleLayer == "of") {
                    if (moduleLayer == layerConfig.moduleLayer) {
                        addLayerToMap(layer, layerConfig);
                    }
                    dojo.byId("legendOfertas").innerHTML = "<img id='arrw' src='../images/map/leyendas/capaOfertasLegend.JPG' />";
                }
            }
        } else if (layerConfig.securityToken == "false") {
            if (layerConfig.layerType == "tiled") {
                layer = new esri.layers.ArcGISTiledMapServiceLayer(
                        layerConfig.layerUrl, {
                            "opacity": layerConfig.layerAlpha,
                            "id": layerConfig.layerLabel
                        });
                addLayerToMap(layer, layerConfig);
            } else if (layerConfig.layerType == "dynamic") {
                layer = new esri.layers.ArcGISDynamicMapServiceLayer(
                        layerConfig.layerUrl, {
                            "opacity": layerConfig.layerAlpha,
                            "id": layerConfig.layerLabel
                        });
                if (layerConfig.moduleLayer == "all") {
                    addLayerToMap(layer, layerConfig);
                }
            }
        } else if (layerConfig.layerType == "wms") {
            var layer1 = new esri.layers.WMSLayerInfo({
                name: layerConfig.visiblelayers,
                title: layerConfig.visiblelayers
            });
            var resourceInfo = {
                extent: new esri.geometry.Extent(-126.40869140625,
                        31.025390625, -109.66552734375, 41.5283203125, {
                            wkid: 4686
                        }),
                layerInfos: [layer1]
            };
            layer = new esri.layers.WMSLayer(layerConfig.layerUrl, {
                "opacity": layerConfig.layerAlpha,
                "resourceInfo": resourceInfo,
                "visibleLayers": [layerConfig.visiblelayers],
                "id": layerConfig.layerLabel
            });
            addLayerToMap(layer, layerConfig);
        } else if (layerConfig.layerType == "osm") {
            layer = new esri.layers.OpenStreetMapLayer({
                "opacity": layerConfig.layerAlpha,
                "id": layerConfig.layerLabel
            });
            addLayerToMap(layer, layerConfig);
        } else if (layerConfig.layerType == "mapbox") {
            console.log("mapbox...");
            mapBoxConfig = layerConfig;
            console.log("mapBoxConfig:" + mapBoxConfig.layerLabel);
            wax.tilejson(layerConfig.layerUrl, function (tj) {
                console.log("mapbox...response");
                console.log("mapBoxConfig response..:"
                        + mapBoxConfig.layerLabel);
                var tjLayer = new wax.esri.connector(tj);
                tjLayer.id = mapBoxConfig.layerLabel;
                addLayerToMap(tjLayer, mapBoxConfig);
                console.log("mapbox...response end:" + tjLayer);
            });
        }
        layerConfig = null;

        gp = new esri.tasks.Geoprocessor(geoservicesObj.geoserviceUrl + "/OfertaInmobiliariaSeleccionar");
        gp.setOutSpatialReference({wkid: mapConfig.wkid});

    }
}
/**
 * 
 * Creación de las capas de gráficos
 */
function createGraphicsLayers() {
    graphicLayerTerrenoUrbano = new esri.layers.GraphicsLayer();
    graphicLayerTerrenoRural = new esri.layers.GraphicsLayer();
    map.addLayer(graphicLayerTerrenoUrbano);
    map.addLayer(graphicLayerTerrenoRural);
}
/**
 * 
 * @param layer
 * @param type
 */
function addLayerToMap(layer, layerConfig) {

    if (layer != null) {

        if (layerConfig.basemap != null && layerConfig.basemap) {
            basemaps.push(layer);
            if (layerConfig.visible) {
                layer.show();
            } else {
                layer.hide();
            }
            dijit.byId("bingMenu").addChild(
                    new dijit.MenuItem({
                        label: layerConfig.layerLabel,
                        onClick: dojo.hitch(this, function () {
                            // toggleLayer, // hide other layers
                            dojo.forEach(basemaps, function (basemap) {
                                map.getLayer(basemap.id).hide();
                            });
                            var lyr = map.getLayer(layerConfig.layerLabel);
                            lyr.show();
                            dijit.byId("dropdownButton").setLabel(
                                    layerConfig.layerLabel);
                        })
                    }));
        }
        //} else {
        legendLayers.push({
            layer: layer,
            title: layerConfig.layerLabel
        });
        operationalLayers.push(layer);

        dojo.byId("legendPredios").innerHTML = "<img id='arrw' src='../images/map/leyendas/capaPrediosLegend.JPG' />";

        var checkBoxName = "checkBox" + layerConfig.layerLabel;
        var checkBox = new dijit.form.CheckBox({
            name: checkBoxName,
            value: layerConfig.layerLabel,
            checked: layerConfig.visible,
            onChange: function (evt) {
                var clayer = map.getLayer(this.value);
                if (clayer.visible) {
                    clayer.hide();
                    // dojo.byId("legend").innerHTML = "No hay leyendas para
                    // mostrar"
                } else {
                    clayer.show();
                }
                this.checked = clayer.visible;
            }
        });

        var slider = new dijit.form.HorizontalSlider({
            name: "slider" + layerConfig.layerLabel,
            value: layerConfig.layerAlpha,
            minimum: 0,
            maximum: 1,
            intermediateChanges: true,
            style: "width:150px;",
            onChange: function (value) {
                var clayer = map.getLayer(layerConfig.layerLabel);
                clayer.setOpacity(value);
            }
        });

        dojo.place(checkBox.domNode, dojo.byId("toggle"), "after");
        var checkLabel = dojo.create('label', {
            'for': checkBox.name,
            innerHTML: layerConfig.layerLabel
        }, checkBox.domNode, "after");
        dojo.place(slider.domNode, checkLabel, "after");
        dojo.place("<br />", slider, "after");
        //}
        map.addLayer(layer);
    }
}

function jQueryReady() {
    // console.log("jQuery ready event");
}

/**
 * 
 * @param evt
 */
function onBasemapSelectionChange(evt) {
    dojo.disconnect(basemapSelectionHandler);
    map.addLayers(operationalLayers);
}

/**
 * 
 * Evento que sucede cuando se agregan capas al mapa
 * 
 * @param evt
 */
function onMapLayersAddResult(evt) {
    dojo.forEach(legendLayers, function (layer) {
        var layerName = layer.title;
        var checkBoxName = "checkBox" + layer.layer.id;

        var checkBox = new dijit.form.CheckBox({
            name: checkBoxName,
            value: layer.layer.id,
            checked: layer.layer.visible,
            onChange: function (evt) {
                var clayer = map.getLayer(this.value);
                if (clayer.visible) {
                    clayer.hide();
                } else {
                    clayer.show();
                }
                this.checked = clayer.visible;
            }
        });

        var slider = new dijit.form.HorizontalSlider({
            name: "slider" + layer.layer.id,
            value: layer.layer.opacity,
            minimum: 0,
            maximum: 1,
            intermediateChanges: true,
            style: "width:150px;",
            onChange: function (value) {
                var clayer = map.getLayer(layer.layer.id);
                clayer.setOpacity(value);
            }
        });

        dojo.place(checkBox.domNode, dojo.byId("toggle"), "after");
        var checkLabel = dojo.create('label', {
            'for': checkBox.name,
            innerHTML: layerName
        }, checkBox.domNode, "after");
        dojo.place(slider.domNode, checkLabel, "after");
        dojo.place("<br />", slider, "after");
    });

}

/**
 * 
 * Inicializa los componentes del mapa y envia el evento de carga completa
 * 
 * @param theMap
 */
function onMapLoad(theMap) {

    var overviewMapDijit = new esri.dijit.OverviewMap({
        map: map,
        visible: true,
        attachTo: "bottom-right"
    });
    overviewMapDijit.startup();

    var scalebar = new esri.dijit.Scalebar({
        map: map,
        scalebarUnit: 'metric'
    });

    var measurement = new esri.dijit.Measurement({
        map: map,
        defaultAreaUnit: esri.Units.SQUARE_KILOMETERS,
        defaultLengthUnit: esri.Units.KILOMETERS
    }, dojo.byId('measurementDiv'));
    dojo.connect(measurement, "onMeasureEnd", function (activeTool, geometry) {
        this.setTool(activeTool, false);
    });
    measurement.startup();

    dojo.byId("north").innerHTML = "<img id='arrw' src='../images/map/flechaNorteIcon.png' />";

    identifyPredioTask = new esri.tasks.IdentifyTask(
            getLayerById(LAYER_PREDIOS_INDEX).layerUrl);
    identifyZHFTask = new esri.tasks.IdentifyTask(
            getLayerById(LAYER_ZHF_INDEX).layerUrl);
    identifyZHGTask = new esri.tasks.IdentifyTask(
            getLayerById(LAYER_ZHG_INDEX).layerUrl);
    findMunicipioTask = new esri.tasks.FindTask(
            getLayerById(LAYER_MUNICIPIOS_INDEX).layerUrl);

    drawToolbar = new esri.toolbars.Draw(map);
    dojo.connect(drawToolbar, "onDrawEnd", addGraphicToMap);
    sendEventOnSNCMapCreated();
}

/**
 * 
 */
function resizeMap() {
    var resizeTimer;
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
        map.resize();
        map.reposition();
    }, 500);
}

/**
 * 
 * Funcion que se ejecuta cuando se cambia el extent del mapa
 * 
 * @param extent
 */
function onExtentChange(extent, delta, outLevelChange, outLod) {
    var s = "";
    s = "<b>Xmin:</b> " + extent.xmin.toFixed(2) + "<b>  Ymin:</b> "
            + extent.ymin.toFixed(2) + "<b>  Xmax:</b> "
            + extent.xmax.toFixed(2) + "<b>  Ymax:</b> "
            + extent.ymax.toFixed(2);
    dojo.byId("info").innerHTML = s;
    dojo.byId("scale").innerHTML = "LOD Level: <i>" + outLod.level;
    dojo.byId("scale2").innerHTML = "<b>Escala Actual:</b> 1:" + outLod.scale;

    updateToolbarByDetailLevel(outLod.level);
}

/**
 * 
 * Funcion que ejecuta los componentes del mapa dependiendo del LOD
 * 
 * @param currentLevel
 */
function updateToolbarByDetailLevel(currentLevel) {
    if (currentLevel >= MIN_PREDIO_TOOLBAR_LEVEL) {
        var toggler = new dojo.fx.Toggler({
            node: "predioToolsDiv"
        });
        toggler.show();
        dijit.byId("identify").setAttribute('disabled', false);
        dijit.byId("identify").set("iconClass", "identifyIcon");
        dijit.byId("selectPredioBtn").setAttribute('disabled', false);
        dijit.byId("selectPredioBtn").set("iconClass",
                "seleccionarPredioEnabledIcon");
    } else {
        var toggler = new dojo.fx.Toggler({
            node: "predioToolsDiv"
        });
        toggler.hide();
        dijit.byId("identify").setAttribute('disabled', true);
        dijit.byId("identify").set("iconClass", "disabledIdentifyIcon");
        dijit.byId("selectPredioBtn").setAttribute('disabled', true);
        dijit.byId("selectPredioBtn").set("iconClass",
                "seleccionarPredioDisabledIcon");
    }
}

/**
 * 
 * Evento que muestra las coordenadas del puntero en el mapa
 * 
 * @param evt
 */
function showCoordinates(evt) {
    var mp = evt.mapPoint;
    dojo.byId("infoCoord").innerHTML = "<b>X:</b>" + roundNumber(mp.x, 4)
            + "<b>   Y:</b> " + roundNumber(mp.y, 4);
}

/**
 * 
 * Funcion que activa o desactiva la barra de navegación
 * 
 * @param enable
 */
function enableNavigationToolbar(enable) {
    if (!enable) {
        navToolbar.deactivate();
    }
    switchEnableIdentify(enable);
}

/**
 * 
 * Funcion que se ejecuta en la carga del mapa para mostrar el icono de carga
 */
function showLoading() {
    esri.show(loading);
}

/**
 * 
 * Funcion que se ejecuta en la carga del mapa para ocultar el icono de carga
 * 
 * @param error
 */
function hideLoading(error) {
    esri.hide(loading);
}

/**
 * 
 * Funcion que se ejecuta en la carga del mapa para manejar el extent inicial y
 * final
 */
function extentHistoryChangeHandler() {
    dijit.byId("zoomprev").disabled = navToolbar.isFirstExtent();
    dijit.byId("zoomnext").disabled = navToolbar.isLastExtent();
}

/**
 * 
 * Función que procesa los poligonos dibujados sobre el mapa
 * 
 * @param geometry
 */
function addGraphicToMap(geometry) {
    drawToolbar.deactivate();
    map.showZoomSlider();
    geometryType = geometry.type;
    switch (geometryType) {
        case "point":
            var symbol = new esri.symbol.SimpleMarkerSymbol(
                    esri.symbol.SimpleMarkerSymbol.STYLE_SQUARE, 10,
                    new esri.symbol.SimpleLineSymbol(
                            esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                            new dojo.Color([255, 0, 0]), 1), new dojo.Color([0,
                255, 0, 0.25]));
            break;
        case "polyline":
            var symbol = new esri.symbol.SimpleLineSymbol(
                    esri.symbol.SimpleLineSymbol.STYLE_DASH, new dojo.Color([255,
                        0, 0]), 1);
            break;
        case "polygon":
            var symbol = new esri.symbol.SimpleFillSymbol(
                    esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                    new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 0, 0]), 2),
                    new dojo.Color([255, 255, 0, 0.25]));
            break;
    }

    this.geometry = geometry;
    var graphic = new esri.Graphic(geometry, symbol);
    map.graphics.add(graphic);

    features[0] = graphic;

    featureSet = new esri.tasks.FeatureSet();
    featureSet.features = features;
    if (paramSeleccionManzanasOfertas == 'seleccionarManzanasPorAsignarOfertas') {
        seleccionarManzanasPorAsignarOfertas(geometry);
    }
    if (paramSeleccionManzanasOfertas == 'seleccionarManzanasEnEjecucionOfertas') {
        seleccionarManzanasEnEjecucionOfertas(geometry);
    }

    if (paramSeleccionPrediosActualizacion == 'seleccionarPrediosActualizacion') {
        seleccionarManzanasActualizacionEvt(geometry);
    }
    if (paramSeleccionPrediosActualizacion == 'recibirModulos') {
        seleccionarManzanasActualizacionCU208Evt(geometry);
    }
    return geometryType;
}

/**
 * 
 * Evento de notificación de creación del Mapa de SNC
 */
function sendEventOnSNCMapCreated() {
    console.log("on snc_visor# Mapa creado");
    var event = jQuery.Event("onSNCMapCreated");
    $("body").trigger(event);
}


/**
 * 
 * ******************************************************************************************
 * *****************Tasks para selección e identificación de predios*************************
 * ******************************************************************************************
 * 
 */


/**
 * 
 * Función del boton Mover
 */
function switchEnableMover() {
    switchEnableIdentify(false);
    switchEnableSelectPredio(false);
    switchEnableSelectManzana(false);
    map.enableMapNavigation();
    navToolbar.activate(esri.toolbars.Navigation.PAN);
}

/**
 * 
 * Función del boton Seleccionar predio
 * 
 * @param enable
 */
function switchEnableSelectPredio(enable) {
    if (enable) {
        switchEnableIdentify(false);
        switchEnableSelectManzana(false);
        map.disableMapNavigation();
        handleSelectPredio = dojo.connect(map, "onClick",
                executeSelectPredioTask);
    } else {
        dojo.disconnect(handleSelectPredio);
    }
}

/**
 * 
 * Función del boton Identify
 * 
 * @param enable
 */
function switchEnableIdentify(enable) {
    if (enable) {
        switchEnableSelectPredio(false);
        switchEnableSelectManzana(false);
        map.disableMapNavigation();
        handleIdentify = dojo.connect(map, "onClick", executeIdentifyTask);
    } else {
        dojo.disconnect(handleIdentify);
    }
}

/**
 * 
 * Evento que sucede cuando se activa la herramienta Identify
 * 
 * @param evt
 */
function executeIdentifyTask(evt) {
    console.log("executeIdentifyTask");

    //Este evento se lanza para poder mostrar la barra de carga en la pantalla
    var event = jQuery.Event("mostrarBarraCargaEvent");
    $("body").trigger(event);

    identifyParams.tolerance = 3;
    identifyParams.returnGeometry = true;
    identifyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
    identifyParams.width = map.width;
    identifyParams.height = map.height;
    identifyParams.geometry = evt.mapPoint;
    identifyParams.mapExtent = map.extent;

    var identifyPredioTaskDeferred = new dojo.Deferred();
    var identifyZHFTaskDeferred = new dojo.Deferred();
    var identifyZHGTaskDeferred = new dojo.Deferred();
    var deferredTasksList = new dojo.DeferredList([identifyPredioTaskDeferred,
        identifyZHFTaskDeferred, identifyZHGTaskDeferred]);
    deferredTasksList.then(showResultsIdentifyTasks);

    try {
        identifyPredioTask.execute(identifyParams,
                identifyPredioTaskDeferred.callback,
                identifyPredioTaskDeferred.errback);
    } catch (e) {
        console.log("Error en identifyPredioTask");
        console.log(e);
        identifyPredioTaskDeferred.errback(e);
    }
    try {
        identifyZHFTask.execute(identifyParams,
                identifyZHFTaskDeferred.callback,
                identifyZHFTaskDeferred.errback);
    } catch (e) {
        console.log("Error en identifyZHFTask");
        console.log(e);
        identifyZHFTaskDeferred.errback(e);
    }
    try {
        identifyZHGTask.execute(identifyParams,
                identifyZHGTaskDeferred.callback,
                identifyZHGTaskDeferred.errback);
    } catch (e) {
        console.log("Error en identifyZHGTask");
        console.log(e);
        identifyZHGTaskDeferred.errback(e);
    }

    dojo.disconnect(handleIdentify);
    switchEnableMover();
}

/**
 * 
 * @param r
 * @returns {Array}
 */
function showResultsIdentifyTasks(r) {
    console.log("showResultsIdentifyTasks");

    var results = [];
    r = dojo.filter(r, function (result) {
        return r[0];
    }); // filter out any failed tasks
    for (i = 0; i < r.length; i++) {
        results = results.concat(r[i][1]);
    }
    results = dojo
            .map(
                    results,
                    function (result) {
                        var feature = result.feature;
                        feature.attributes.layerName = result.layerName;

                        // Capas Urbanas
                        if (result.layerName === 'SNC_MUNICIPIOS.U_NOMENCLATURA_DOMICILIARIA') {
                            var templateDomicilaria = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Nomenclatura Domicilaria</i><br>Texto:</b> ${Texto} <br><b>Código de Terreno:</b></b> ${Terreno Codigo}");
                            feature.setInfoTemplate(templateDomicilaria);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.U_NOMENCLATURA_VIAL') {
                            var templateVial = new esri.InfoTemplate("",
                                    "<b><i>Capa: Nomenclatura Vial</i><br>Texto:</b> ${Texto}");
                            feature.setInfoTemplate(templateVial);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.U_CONSTRUCCION') {
                            var templateConstruccion = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Construcción</i><br>Código:</b> ${Codigo} <br><b>Código de Terreno:</b></b> ${Terreno_Codigo} <br><b>Tipo de Construcción:</b></b> ${Tipo_Construccion} <br><b>Tipo de Dominio:</b> ${Tipo_Dominio} <br><b>Número de Pisos:</b> ${Numero Pisos} <br><b>Número de Sotanos:</b> ${Numero_Sotanos} <br><b>Número de Mezanines:</b> ${Numero_Mezanines} <br><b>Número de Semisotanos:</b> ${Numero_Semisotanos} <br><b>Etiqueta:</b> ${Etiqueta} <br><b>Identificador:</b> ${Identificador} <br><b>Código de Edificación:</b> ${Codigo_Edificacion} <br><b>Código Anterior:</b> ${Codigo Anterior} <br><b> ");
                            feature.setInfoTemplate(templateConstruccion);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.U_TERRENO') {
                            var templateTerreno = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Terreno</i><br>Código:</b> ${Codigo} <br><b>Código de Manzana:</b></b> ${Manzana Codigo} <br><b>Número de Subterraneos:</b> ${Numero_Subterraneos} <br><b>Código Anterior:</b> ${Codigo Anterior} <br><b> ");
                            feature.setInfoTemplate(templateTerreno);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.U_MANZANA') {
                            var templateManzana = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Manzana</i><br>Código:</b> ${Codigo} <br><b>Código de Barrio:</b></b> ${Barrio Codigo} <br><b>Código Anterior:</b> ${Codigo Anterior} <br><b> ");
                            feature.setInfoTemplate(templateManzana);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.U_BARRIO') {
                            var templateBarrio = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Barrio</i><br>Código:</b> ${Codigo} <br><b>Código de Sector:</b></b> ${Sector Codigo} <br><b>Nombre:</b> ${Nombre} <br><b> ");
                            feature.setInfoTemplate(templateBarrio);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.U_SECTOR') {
                            var templateSector = new esri.InfoTemplate("",
                                    "<b><i>Capa: Sector</i><br>Código:</b> ${Codigo}");
                            feature.setInfoTemplate(templateSector);
                        } else if (result.layerName === 'ZHU_FISICA') {
                            var templateZHFU = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Zona Homogenea Física Urbana</i><br>Código:</b> ${Codigo_Zona_Fisica} <br><b>Topografía:</b></b> ${Topografia} <br><b>Influencia Vial:</b> ${Influencia_Vial} <br><b> Servicio Público:</b> ${Servicio_Publico} <br><b> Uso actual del suelo:</b> ${Uso_Actual_Suelo} <br><b> Norma de uso del suelo:</b> ${Norma_Uso_Suelo} <br><b> Tipificación Construcción:</b> ${Tipificacion_Construccion} <br><b> Vigencia:</b> ${Vigencia} <br><b>");
                            feature.setInfoTemplate(templateZHFU);
                        } else if (result.layerName === 'ZHU_GEOECONOMICA') {
                            var templateZHGU = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Zona Homogenea Geoeconómica Urbana</i><br>Código:</b> ${Codigo_Zona_Geoeconomica} <br><b>Valor metro:</b></b> ${Valor_Metro} <br><b>Subzona Física:</b> ${Subzona_Fisica} <br><b> Vigencia:</b> ${Vigencia} <br><b>");
                            feature.setInfoTemplate(templateZHGU);
                        }

                        // Capas Rurales
                        else if (result.layerName === 'SNC_MUNICIPIOS.R_NOMENCLATURA_DOMICILIARIA') {
                            var templateDomicilariaRural = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Nomenclatura Domicilaria</i><br>Texto:</b> ${Texto} <br><b>Código de Terreno:</b></b> ${Terreno Codigo}");
                            feature.setInfoTemplate(templateDomicilariaRural);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.R_NOMENCLATURA_VIAL') {
                            var templateVialRural = new esri.InfoTemplate("",
                                    "<b><i>Capa: Nomenclatura Vial</i><br>Texto:</b> ${Texto}");
                            feature.setInfoTemplate(templateVialRural);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.R_CONSTRUCCION') {
                            var templateConstruccionRural = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Construcción</i><br>Código:</b> ${Codigo} <br><b>Código de Terreno:</b></b> ${Terreno_Codigo} <br><b>Tipo de Construcción:</b></b> ${Tipo_Construccion} <br><b>Tipo de Dominio:</b> ${Tipo_Dominio} <br><b>Número de Pisos:</b> ${Numero Pisos} <br><b>Número de Sotanos:</b> ${Numero_Sotanos} <br><b>Número de Mezanines:</b> ${Numero_Mezanines} <br><b>Número de Semisotanos:</b> ${Numero_Semisotanos} <br><b>Etiqueta:</b> ${Etiqueta} <br><b>Identificador:</b> ${Identificador} <br><b>Código de Edificación:</b> ${Codigo_Edificacion} <br><b>Código Anterior:</b> ${Codigo Anterior} <br><b> ");
                            feature.setInfoTemplate(templateConstruccionRural);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.R_TERRENO') {
                            var templateTerrenoRural = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Terreno</i><br>Código:</b> ${Codigo} <br><b>Código de Manzana:</b></b> ${Manzana Codigo} <br><b>Número de Subterraneos:</b> ${Numero_Subterraneos} <br><b>Código Anterior:</b> ${Codigo Anterior} <br><b> ");
                            feature.setInfoTemplate(templateTerrenoRural);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.R_VEREDA') {
                            var templateVereda = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Vereda</i><br>Código:</b> ${Codigo} <br><b>Código de Sector:</b></b> ${Sector Codigo} <br><b>Nombre:</b></b> ${Nombre} <br><b>Código Anterior:</b> ${Codigo Anterior} <br><b>");
                            feature.setInfoTemplate(templateVereda);
                        } else if (result.layerName === 'SNC_MUNICIPIOS.R_SECTOR') {
                            var templateSectorRural = new esri.InfoTemplate("",
                                    "<b><i>Capa: Sector</i><br>Código:</b> ${Codigo}");
                            feature.setInfoTemplate(templateSectorRural);
                        } else if (result.layerName === 'ZHR_FISICA') {
                            var templateZHFR = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Zona Homogenea Física Rural</i><br>Código:</b> ${Codigo_Zona_Fisica} <br><b>Area homogenea tierra:</b></b> ${Area_Homogenea_Tierra} <br><b>Disponibilidad de agua:</b> ${Disponibilidad_Agua} <br><b> Influencia Vial:</b> ${Influencia_Vial} <br><b> Uso actual del suelo:</b> ${Uso_Actual_Suelo} <br><b> Norma de uso del suelo:</b> ${Norma_Uso_Suelo} <br><b> Vigencia:</b> ${Vigencia} <br><b>");
                            feature.setInfoTemplate(templateZHFR);
                        } else if (result.layerName === 'ZHR_GEOECONOMICA') {
                            var templateZHGR = new esri.InfoTemplate(
                                    "",
                                    "<b><i>Capa: Zona Homogenea Geoeconómica Rural</i><br>Código:</b> ${Codigo_Zona_Geoeconomica} <br><b>Valor hectarea:</b></b> ${Valor_Hectarea} <br><b>Subzona Física:</b> ${Subzona_Fisica} <br><b> Vigencia:</b> ${Vigencia} <br><b>");
                            feature.setInfoTemplate(templateZHGR);
                        }
                        return feature;
                    });
    map.infoWindow.setFeatures(results);
    map.infoWindow.show(identifyParams.geometry);

    //Este evento se lanza para poder ocultar la barra de carga en la pantalla
    var event = jQuery.Event("ocultarBarraCargaEvent");
    $("body").trigger(event);
}
/**
 * 
 * Evento que sucede cuando se activa la herramienta Seleccionar predio
 * 
 * @param evt
 */
function executeSelectPredioTask(evt) {
    console.log("executeSelectPredioTask");
    var identifyParams = new esri.tasks.IdentifyParameters();
    identifyParams.tolerance = 3;
    identifyParams.returnGeometry = true;
    identifyParams.layerIds = [INDEX_TERRENO_PREDIO_RURAL,
        INDEX_TERRENO_PREDIO_URBANO];
    identifyParams.layerOption = esri.tasks.IdentifyParameters.LAYER_OPTION_ALL;
    identifyParams.width = map.width;
    identifyParams.height = map.height;
    identifyParams.geometry = evt.mapPoint;
    identifyParams.mapExtent = map.extent;

    var deferred = identifyPredioTask.execute(identifyParams);

    deferred
            .addCallback(function (response) {
                if (response.length > 0) {
                    return dojo
                            .map(
                                    response,
                                    function (result) {
                                        var feature = result.feature;
                                        var codPredio = feature.attributes.Codigo;
                                        feature.attributes.layerName = result.layerName;

                                        var templateView = "<b> Predio </b> <br/> <tr><b>Código:</b> <td>${Codigo}</tr></td><br><tr><b>Código Anterior:</b><td>${Codigo Anterior}</tr></td>"
                                                + "<br/><a href=\"javascript:seleccionarPredio('"
                                                + codPredio
                                                + "');\">Seleccionar Predio</a>";
                                        var template = new esri.InfoTemplate(
                                                "Attributes", templateView);
                                        console.log("templateView:"
                                                + templateView);
                                        feature.setInfoTemplate(template);
                                        return feature;
                                    });
                } else {
                    var content = dojo.string
                            .substitute('No se encontraron predios en el área seleccionada');
                    dijit.byId('messageWin').setContent(content);
                    dijit.byId('messageWin').show();
                }
            });
    map.infoWindow.setFeatures([deferred]);
    map.infoWindow.show(evt.mapPoint);
    dojo.disconnect(handleSelectPredio);
    switchEnableMover();
}

/**
 * 
 * Funcion que lanza el trigger al cliente con el codigo de los predios
 * 
 * @param codPredio
 */
function seleccionarPredio(codPredio) {
    console.log("seleccionarPredio_: " + String(codPredio));
    var event = jQuery.Event("onSelectedPredio");
    event.predioSeleccionado = codPredio;
    $("body").trigger(event);
}



/**
 * 
 * ******************************************************************************************
 * ******************************Métodos para Zooms******************************************
 * ******************************************************************************************
 * 
 */

/*
 *Funcion que hace el zoom a las zonas homogeneas a la que pertenece un municipio  
 * @param {type} codigoMunicipio
 * @param {type} codigosZonasFisicas
 * @param {type} codigosZonasGeoeconomicas
 * @param {type} numeroPredialSeleccionado
 * @returns {undefined}
 * @author andres.eslava
 */
function zoomToZonas(codigoMunicipio, codigosZonasFisicas, codigosZonasGeoeconomicas, numeroPredialSeleccionado) {

    console.log("ZoomToZonas para el municipio" + codigoMunicipio);
    map.graphics.clear();

    //Valida si es rural 
    var codigoZona = null;
    if (isPredioRural(numeroPredialSeleccionado)) {
        codigoZona = '00';
    } else {
        codigoZona = '01';
    }

    var codigosZHFParaQuery = alistarCodigoFeaturesParaQuery(codigosZonasFisicas);
    var codigosZHGParaQuery = alistarCodigoFeaturesParaQuery(codigosZonasGeoeconomicas);


    // Crea la tarea para obtener las geometrisa de las zonas geoeconomicas

    ZHGLayerUrl = getLayerById(LAYER_ZHG_INDEX).layerUrl + "/"
            + INDEX_ZHU_GEOECONOMICA;

    console.log("Numero predial seleccionado " + numeroPredialSeleccionado);



    var findZHGTask = new esri.tasks.QueryTask(ZHGLayerUrl);

    var findZHGByCodigoParams = new esri.tasks.Query();
    findZHGByCodigoParams.returnGeometry = true;
    var whereZonasStr = "Codigo = '" + codigoMunicipio + codigoZona + "' and Codigo_Zona_Geoeconomica in (" + codigosZHGParaQuery + ")";

    console.log("La sentencia where es : " + whereZonasStr);
    findZHGByCodigoParams.where = whereZonasStr;
    findZHGTask.execute(findZHGByCodigoParams, showResultsFindZHGByCodigo);

    // Crea la tarea para obtener las geometrias de las zonas fisicas


    ZHFLayerUrl = getLayerById(LAYER_ZHF_INDEX).layerUrl + "/" + INDEX_ZHU_FISICA;

    console.log("Numero predial seleccionado " + numeroPredialSeleccionado);

    var findZHFTask = new esri.tasks.QueryTask(ZHFLayerUrl);

    var findZHFByCodigoParams = new esri.tasks.Query();
    findZHFByCodigoParams.returnGeometry = true;

    var whereZonasStr = "Codigo = '" + codigoMunicipio + codigoZona + "' and Codigo_Zona_Fisica in (" + codigosZHFParaQuery + ")";

    console.log("La sentencia where es : " + whereZonasStr);
    findZHFByCodigoParams.where = whereZonasStr;
    findZHFTask.execute(findZHFByCodigoParams, showResultsFindZHFByCodigo);





    // Crea la tarea para obtener la geometria del predio seleccionado

    var wherePredioStr = "Codigo = '" + numeroPredialSeleccionado + "'";

    if (isPredioRural(numeroPredialSeleccionado)) {
        var predioLayerRuralUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_TERRENO_PREDIO_RURAL;
        findPredioRuralQueryTask = new esri.tasks.QueryTask(predioLayerRuralUrl);
        var findPredioRuralByCodigoParams = new esri.tasks.Query();
        findPredioRuralByCodigoParams.where = wherePredioStr;
        findPredioRuralQueryTask.execute(findPredioRuralByCodigoParams, showResultsFindPredioByCodigo);
    } else {
        var predioLayerUrbanoUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_TERRENO_PREDIO_URBANO;
        findPredioUrbanoQueryTask = new esri.tasks.QueryTask(predioLayerUrbanoUrl);
        var findPredioUrbanoByCodigoParams = new esri.tasks.Query();
        findPredioUrbanoByCodigoParams.returnGeometry = true;
        findPredioUrbanoByCodigoParams.where = wherePredioStr;
        findPredioUrbanoQueryTask.execute(findPredioUrbanoByCodigoParams, showResultsFindPredioByCodigo);
    }
}

/**
 * 
 * Realiza zoom a un municipio dado su codigo
 * 
 * @param codMunicipio
 */
function zoomToMunicipio(codMunicipio) {
    console.log("zoomToMunicipio: " + codMunicipio);


    var findMunicipioByCodigoParams = new esri.tasks.FindParameters();
    findMunicipioByCodigoParams.returnGeometry = true;
    findMunicipioByCodigoParams.layerIds = [INDEX_MUNICIPIOS];
    findMunicipioByCodigoParams.searchFields = [LAYER_MUNICIPIOS_CODIGO];
    findMunicipioByCodigoParams.searchText = codMunicipio;
    findMunicipioTask.execute(findMunicipioByCodigoParams,
            showResultsFindMunicipioByCodigo);

}

/**
 * 
 * Realiza zoom a un predio dado su código
 * 
 * @param codPredio
 */
function zoomToPredio(codPredio) {
    console.log("on snc_visor#zoomToPredio : código predio = " + codPredio);

    var predioLayerUrl = "";
    if (isPredioRural(codPredio)) {
        predioLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_TERRENO_PREDIO_RURAL + "?";
    } else {
        predioLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_TERRENO_PREDIO_URBANO + "?";
    }
    findPredioTask = new esri.tasks.QueryTask(predioLayerUrl);

    var findPredioByCodigoParams = new esri.tasks.Query();
    findPredioByCodigoParams.returnGeometry = true;
    var whereStr = "CODIGO = '" + codPredio + "'"
    findPredioByCodigoParams.where = whereStr;
    findPredioTask.execute(findPredioByCodigoParams,
            showResultsFindPredioByCodigo);
}

/**
 * 
 * Realiza zoom a un predio dado su código (Se hizo especialmente para poder hacer zoom a las mejoras)
 * 
 * @param codPredio
 */
function zoomToConstruccion(codPredio) {
    console.log("on snc_visor#zoomToConstruccion : código construccion = " + codPredio);

    var predioLayerUrl = "";
    if (isPredioRural(codPredio)) {
        predioLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_CONSTRUCCION_PREDIO_RURAL + "?";
    } else {
        predioLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_CONSTRUCCION_PREDIO_URBANO + "?";
    }
    findPredioTask = new esri.tasks.QueryTask(predioLayerUrl);

    var findPredioByCodigoParams = new esri.tasks.Query();
    findPredioByCodigoParams.returnGeometry = true;
    var whereStr = "CODIGO = '" + codPredio + "'"
    findPredioByCodigoParams.where = whereStr;
    findPredioTask.execute(findPredioByCodigoParams,
            showResultsFindPredioByCodigo);
}

/**
 * 
 * Realiza zoom a varios predios dados sus códigos
 * 
 * @param codPredio
 */
function zoomToPredios(codPredio) {
    console.log("on snc_visor#zoomToPredios : código predios = " + codPredio);

    totalExtentZoomToPredios = [];
    graphicLayerTerrenoRural.clear();
    graphicLayerTerrenoUrbano.clear();

    var codigosPrediosRurales = "";
    var codigosPrediosUrbanos = "";
    var splittedCodigos = codPredio.split(",");

    var predioLayerRuralUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
            + INDEX_TERRENO_PREDIO_RURAL;
    findPredioRuralQueryTask = new esri.tasks.QueryTask(predioLayerRuralUrl);

    var predioLayerUrbanoUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
            + INDEX_TERRENO_PREDIO_URBANO;
    findPredioUrbanoQueryTask = new esri.tasks.QueryTask(predioLayerUrbanoUrl);

    var findPredioByCodigoQuery = new esri.tasks.Query();
    findPredioByCodigoQuery.returnGeometry = true;
    findPredioByCodigoQuery.outSpatialReference = map.spatialReference;

    for (var i = 0; i < splittedCodigos.length; i++) {
        if (isPredioRural(splittedCodigos[i])) {
            if (codigosPrediosRurales != "") {
                codigosPrediosRurales = codigosPrediosRurales.concat(",");
            }
            codigosPrediosRurales = codigosPrediosRurales
                    .concat(splittedCodigos[i]);
        } else if (!isPredioRural(splittedCodigos[i])) {
            if (codigosPrediosUrbanos != "") {
                codigosPrediosUrbanos = codigosPrediosUrbanos.concat(",");
            }
            codigosPrediosUrbanos = codigosPrediosUrbanos
                    .concat(splittedCodigos[i]);
        }
    }

    if (codigosPrediosRurales != "" || codigosPrediosRurales != null) {
        console
                .log("on snc_visor#zoomToPrediosRurales : codigosPrediosRurales = "
                        + codigosPrediosRurales);
        var codigoQuery = alistarCodigoFeaturesParaQuery(codigosPrediosRurales);
        var whereStr = "CODIGO in (" + codigoQuery + ")";
        findPredioByCodigoQuery.where = whereStr;
        findPredioRuralQueryTask.execute(findPredioByCodigoQuery, showResultsPrediosRurales);
    }
    if (codigosPrediosUrbanos != "" || codigosPrediosUrbanos != null) {
        console
                .log("on snc_visor#zoomToPrediosUrbanos : codigosPrediosUrbanos = "
                        + codigosPrediosUrbanos);
        var codigoQuery = alistarCodigoFeaturesParaQuery(codigosPrediosUrbanos);
        var whereStr = "CODIGO in (" + codigoQuery + ")";
        findPredioByCodigoQuery.where = whereStr;
        findPredioUrbanoQueryTask.execute(findPredioByCodigoQuery, showResultsPrediosUrbanos);
    }
}

/**
 * 
 * Realiza zoom a un predio dado su código
 * 
 * @param codigoSector
 */
function zoomToSector(codigoSector) {
    console.log("on snc_visor#zoomToSector : código Sector = " + codigoSector);
    var codigoSectorStr = codigoSector.toString();
    var predioLayerUrl = "";
    if (isPredioRural(codigoSectorStr)) {
        predioLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_SECTOR_RURAL + "?";
    } else {
        predioLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_SECTOR_URBANO + "?";
    }
    findPredioTask = new esri.tasks.QueryTask(predioLayerUrl);

    var findSectorByCodigoParams = new esri.tasks.Query();
    findSectorByCodigoParams.returnGeometry = true;
    var whereStr = "CODIGO = '" + codigoSectorStr + "'"
    findSectorByCodigoParams.where = whereStr;
    findPredioTask.execute(findSectorByCodigoParams,
            showResultsFindPredioByCodigo);
}

/**
 * 
 * Función que hace zoom a varios sectores
 * 
 * @param codigoSector
 */
function zoomToSectores(codigoSector) {
    console.log("zoomToSectores: " + codigoSector);
    var codigoSectorStr = codigoSector.toString();
    var sectorLayerUrl = "";
    if (isPredioRural(codigoSectorStr)) {
        sectorLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_SECTOR_RURAL + "?";
    } else {
        sectorLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_SECTOR_URBANO + "?";
    }
    findManzanaTask = new esri.tasks.QueryTask(sectorLayerUrl);

    var findSectorByCodigoParams = new esri.tasks.Query();
    findSectorByCodigoParams.returnGeometry = true;
    var codigoQuery = alistarCodigoFeaturesParaQuery(codigoSectorStr);
    var whereStr = "CODIGO in (" + codigoQuery + ")";
    findSectorByCodigoParams.where = whereStr;
    findManzanaTask.execute(findSectorByCodigoParams,
            showResultsFindBunchOfFeatures);
}

/**
 * 
 * Función que hace zoom a una manzanas o vereda dado su codigo
 * 
 * @param manzanaVeredaSeleccionada
 */
function zoomToManzanaVereda(manzanaVeredaSeleccionada) {
    console.log("zoomToManzanaVereda: " + manzanaVeredaSeleccionada);

    var manzanaLayerUrl = "";
    if (isPredioRural(manzanaVeredaSeleccionada)) {
        manzanaLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_VEREDAS_RURAL + "?";
    } else {
        manzanaLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_MANZANAS_URBANO + "?";
    }
    findManzanaTask = new esri.tasks.QueryTask(manzanaLayerUrl);

    var findManzanaByCodigoParams = new esri.tasks.Query();
    findManzanaByCodigoParams.returnGeometry = true;
    var whereStr = "CODIGO = '" + manzanaVeredaSeleccionada + "'"
    findManzanaByCodigoParams.where = whereStr;
    findManzanaTask.execute(findManzanaByCodigoParams,
            showResultsFindPredioByCodigo);
}

/**
 * 
 * Función que hace zoom a varias manzanas o veredas
 * @param codigoManzanas
 */
function zoomToManzanasVeredas(codigoManzanas) {
    console.log("zoomToManzanasVeredas: " + codigoManzanas);
    var codigoManzanasStr = codigoManzanas.toString();
    var manzanaLayerUrl = "";
    if (isPredioRural(codigoManzanas)) {
        manzanaLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_VEREDAS_RURAL + "?";
    } else {
        manzanaLayerUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
                + INDEX_MANZANAS_URBANO + "?";
    }
    findManzanaTask = new esri.tasks.QueryTask(manzanaLayerUrl);

    var findManzanaByCodigoParams = new esri.tasks.Query();
    findManzanaByCodigoParams.returnGeometry = true;
    var codigoQuery = alistarCodigoFeaturesParaQuery(codigoManzanasStr);
    var whereStr = "CODIGO in (" + codigoQuery + ")";
    findManzanaByCodigoParams.where = whereStr;
    findManzanaTask.execute(findManzanaByCodigoParams,
            showResultsFindBunchOfFeatures);
}

/**
 * 
 * ******************************************************************************************
 * ****************************Métodos de respuesta a Zooms**********************************
 * ******************************************************************************************
 * 
 */
function showResultsFindZHFByCodigo(results) {
    console.log("showResultsZHFByCodigo");
    console.log("cantidad resultados Query" + results.features.length);

    var polygonSymbolZHF = new esri.symbol.SimpleFillSymbol(
            esri.symbol.SimpleFillSymbol.STYLE_SOLID,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 255]), 2),
            new dojo.Color([4, 40, 218, 0]));
    if (results.features.length > 0) {
        //map.graphics.clear();        
        for (var i = 0; i < results.features.length; i++) {
            var graphic = results.features[i];
            graphic.setSymbol(polygonSymbolZHF);
            //  var taxLotExtent = graphic.geometry.getExtent();
            //  map.setExtent(taxLotExtent);
            map.graphics.add(graphic);
        }
    } else {
        var content = dojo.string.substitute('No se encontró la zona solicitada');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
    console.log("Salio CallBack ZHF");

}
function showResultsFindZHGByCodigo(results) {
    console.log("showResultsZHGByCodigo");
    console.log("cantidad resultados Query" + results.features.length);

    var polygonSymbolZHG = new esri.symbol.SimpleFillSymbol(
            esri.symbol.SimpleFillSymbol.STYLE_SOLID,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_NULL, new dojo.Color([255, 0, 0]), 2),
            new dojo.Color([252, 6, 22, 0.25]));
    if (results.features.length > 0) {
        for (var i = 0; i < results.features.length; i++) {
            var graphic = results.features[i];
            graphic.setSymbol(polygonSymbolZHG);
            //  var taxLotExtent = graphic.geometry.getExtent();
            //  map.setExtent(taxLotExtent);
            map.graphics.add(graphic);
        }
    } else {
        var content = dojo.string.substitute('No se encontró la zona solicitada');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
    console.log("Salio CallBack ZHG");

}

/**
 * 
 * Funcion que se ejecuta como respuesta a la funcion zoomToMunicipio
 * @param results
 */
function showResultsFindMunicipioByCodigo(results) {
    console.log("showResultsFindMunicipioByCodigo");
    console.log("results.length:" + results.length);
    if (results.length > 0) {
        map.graphics.clear();
        dojo.forEach(results, function (result) {
            var graphic = result.feature;
            switch (graphic.geometry.type) {
                case "polygon":
                    graphic.setSymbol(polygonSymbol);
                    var taxLotExtent = graphic.geometry.getExtent();
                    map.setExtent(taxLotExtent);
                    break;
            }
            map.graphics.add(graphic);
        });
    } else {
        var content = dojo.string
                .substitute('No se encontró el Municipio solicitado');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
}

function showResultsFindPredioByCodigo(results) {
    console.log("showResultsFindPredioByCodigo")
    var features = results.features;
    if (features.length > 0) {
        var polygonSymbol = new esri.symbol.SimpleFillSymbol(
                esri.symbol.SimpleFillSymbol.STYLE_SOLID,
                new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 255, 0]), 2),
                new dojo.Color([4, 144, 18, 0.75]));
        map.graphics.clear();
        dojo.forEach(features, function (result) {
            var graphic = result;
            if (graphic.geometry.type == null) {
                console.log("El tipo de geometria llega nulo ");
            } else {
                switch (graphic.geometry.type) {
                    case "polygon":
                        graphic.setSymbol(polygonSymbol);
                        map.graphics.add(graphic);
                        try {
                            var taxLotExtent = graphic.geometry.getExtent();
                            map.setExtent(taxLotExtent, true);
                        } catch (err) {
                            console.log("Error en animacion zoom: " + err);
                        }
                        break;
                }
            }
        });
    } else {
        var content = dojo.string
                .substitute('No se encontró la ubicación solicitada');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
    console.log("salio callback predio");
}

/**
 * 
 * Funcion que se ejecuta como respuesta a las funciones de zoom a varios
 * features
 * 
 * @param results
 */
function showResultsFindBunchOfFeatures(results) {
    map.graphics.clear();
    var features = results.features;
    var featExtent = esri.graphicsExtent(features);
    if (features.length > 0) {
        dojo.forEach(features, function (result) {
            result.setSymbol(polygonSymbol);
            map.graphics.add(result);
        });
        map.setExtent(featExtent);
    } else {
        var content = dojo.string
                .substitute('No se encontró la ubicación solicitada');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
    if (contadorFeatures > features.length) {
        var content = dojo.string
                .substitute('Se encontraron '
                        + features.length
                        + " elementos con información geográfica de los "
                        + contadorFeatures + " solicitados.");
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
}

/**
 * 
 * Funcion que se ejecuta como respuesta al query de zoom a predios rurales
 * 
 * @param featureSet
 */
function showResultsPrediosRurales(featureSet) {

    dojo.forEach(featureSet.features, function (feature) {
        graphicLayerTerrenoRural.add(feature.setSymbol(polygonSymbol));
    });
    setExtentZoomToPredios(graphicLayerTerrenoRural.graphics);
}

/**
 * 
 * Funcion que se ejecuta como respuesta al query de zoom a predios urbanos
 * 
 * @param featureSet
 */
function showResultsPrediosUrbanos(featureSet) {

    dojo.forEach(featureSet.features, function (feature) {
        graphicLayerTerrenoUrbano.add(feature.setSymbol(polygonSymbol));
    });
    setExtentZoomToPredios(graphicLayerTerrenoUrbano.graphics);
}

/**
 * 
 * Funcion usada para que cuadre el extent del mapa cuando se le hace zoom a
 * predios urbanos y rurales
 * 
 * @param graphics
 */
function setExtentZoomToPredios(graphics) {
    totalExtentZoomToPredios = totalExtentZoomToPredios.concat(graphics);
    var extent = esri.graphicsExtent(totalExtentZoomToPredios);
    map.setExtent(extent, true);
}

/**
 * 
 * ******************************************************************************************
 * *******************************Miscellanous***********************************************
 * ******************************************************************************************
 * 
 */

/**
 * 
 * Función que agrega comas y comillas a los códigos recibidos para poder
 * hacerles el query
 * 
 * @param codigo
 * @returns
 */
function alistarCodigoFeaturesParaQuery(codigo) {
    var codigoListo = "";
    var splittedCodigo = codigo.split(",");
    contadorFeatures = splittedCodigo.length;
    for (var i = 0; i < contadorFeatures; i++) {
        var s = "";
        s = "'" + splittedCodigo[i].toString() + "'";
        if (codigoListo != "") {
            codigoListo = codigoListo.concat(",");
        }
        codigoListo = codigoListo.concat(s);
    }
    return codigoListo;
}

/**
 * 
 * Valida si un predio es rural o urbano a partir de su código
 * 
 * @param codPredio
 * @returns {Boolean}
 */
function isPredioRural(codPredio) {
    var subCode = codPredio.substring(5, 7);
    if (subCode == "00") {
        resultIsPredioRural = true;
    } else {
        resultIsPredioRural = false;
    }
    console.log("isPredioRural " + resultIsPredioRural);
    return resultIsPredioRural;
}

/**
 * 
 * @param rnum
 * @param rlength
 * @returns
 */
function roundNumber(rnum, rlength) {
    var newnumber = Math.round(rnum * Math.pow(10, rlength))
            / Math.pow(10, rlength);
    return parseFloat(newnumber);
}

/**
 * 
 * Función que simila el boolean en javascript
 * 
 * @returns
 */
String.prototype.bool = function () {
    return (/^true$/i).test(this);
};

/**
 * 
 * Inicialización después de la carga de Dojo.
 */
dojo.ready(loadXML);


/**
 * 
 * ******************************************************************************************
 * *******************************Funcionalidades mapa***************************************
 * ******************************************************************************************
 * 
 */


/**
 * 
 * Función que muestra u oculta la leyenda por medio de un checkbox
 */
function checkLeyenda() {
    dijit.byId('leyendaFloater').show();
}

/**
 * 
 * Función que muestra u oculta los valores del extent máximos y mínimos por
 * medio de un checkbox
 */
function checkExtent() {
    if (dojo.byId("checkBoxExtent").checked) {
        dojo.style(dojo.byId("info"), "visibility", "visible");
    } else {
        dojo.style(dojo.byId("info"), "visibility", "hidden");
    }
}

/**
 * 
 * Función que hace zoom a una escala seleccionada del comboBox de escalas
 */
function zoomToScale() {
    var escalaSelected = dijit.byId('selectEscalas').value;
    if (escalaSelected != "Seleccionar") {
        var escalaValue = escalaSelected.replace("1:", "");
        console.log("escalaValue " + escalaValue);
        if (escalaValue == "20000") {
            map.setLevel(10);
        } else if (escalaValue == "10000") {
            map.setLevel(11);
        } else if (escalaValue == "5000") {
            map.setLevel(12);
        } else if (escalaValue == "2000") {
            map.setLevel(13);
        } else if (escalaValue == "1000") {
            map.setLevel(14);
        } else if (escalaValue == "500") {
            map.setLevel(15);
        }
    }
}

/**
 * 
 * Función que valida si el mapa tiene un dibujo o no
 */
function validarDibujo() {
    console.log("validarDibujo...");
    if (geometryType == null || geometryType == "") {
        var content = dojo.string.substitute('No ha seleccionado ningún área');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
}

/**
 * 
 * Función que muestra la ventana flotante de seleccion de capas
 */
function verCapasTool() {
    dijit.byId('capasFloater').show();
}

/**
 * 
 * Función que muestra la ventana flotante de herramientas de medicion
 */
function verMedirTool() {
    dijit.byId('medirFloater').show();
}

/**
 * 
 * Función que muestra la ventana flotante de herramientas de dibujo
 */
function verDibujarTool() {
    dijit.byId('dibujarFloater').show();
}

/**
 * 
 * Función que recibe los modulos de capas y herramientas para los parametros
 * del mapa
 * 
 * @param moduleLayerParam
 * @param moduleToolsParam
 * @returns
 */
function recibirModulos(moduleLayerParam, moduleToolsParam) {
    this.moduleLayerParam = moduleLayerParam;
    this.moduleToolsParam = moduleToolsParam;

    if (moduleToolsParam == "of" || moduleToolsParam == "ac" || moduleToolsParam == "ac302") {
        dojo.style(dojo.byId("dibujar"), "visibility", "visible");
    } else {
        dojo.style(dojo.byId("dibujar"), "visibility", "hidden");
    }
    if (moduleToolsParam == "co" || moduleToolsParam == "ac302") {
        dojo.style(dojo.byId("selectPredioBtn"), "visibility", "visible");
    }
}

/**
 * 
 * ******************************************************************************************
 * *******************************Ofertas Inmobiliarias**************************************
 * ******************************************************************************************
 * 
 */

/**
 * 
 * Función que ejecuta el servicio Seleccionar area para oferta inmobiliaria
 * 
 * @returns
 */
function ejecutarServicioSeleccionarOferta() {
    console.log("entra a ejecutarServicioSeleccionarOferta (en snc_visor)");
    if (resultIsPredioRural == true) {
        resultIsPredioRural = false;
    }
    if (resultIsPredioRural == false) {
        resultIsPredioRural = true;
    }
    var params = {
        "Geometria": featureSet,
        "IdOferta": idOfertaMapa,
        "Usuario": usuarioDto,
        "esUrbano": resultIsPredioRural,
        "municipio": municipioCodMapa
    };

    if (featureSet != null) {
        gp.submitJob(params, completeCallbackServicioSeleccionarOferta,
                statusCallbackServicioSeleccionarOferta, function (error) {
                    alert(error);
                    esri.hide(loading);
                });
        var event = jQuery.Event("mostrarBarraCargaEvent");
        $("body").trigger(event);
    } else {
        var content = dojo.string.substitute('No ha seleccionado ningún área');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
}

/**
 * 
 * Funcion que se ejecuta cuando se finaliza el servicio Seleccionar area para
 * oferta inmobiliaria
 * 
 * @param jobInfo
 * @returns
 */
function completeCallbackServicioSeleccionarOferta(jobInfo) {
    if (jobInfo.jobStatus !== "esriJobFailed") {
        gp.getResultData(jobInfo.jobId, "cantidadRegistros",
                displayOfertaInmobiliariaSeleccionar);
        console.log("completeCallback");
        featureSet = null;
    }
}

/**
 * 
 * Funcion que se ejecuta para dar informacion del avance de la ejecucion del
 * servicio Seleccionar area para oferta inmobiliaria
 * 
 * @param jobInfo
 * @returns
 */
function statusCallbackServicioSeleccionarOferta(jobInfo) {
    var event = jQuery.Event("mostrarBarraCargaEvent");
    $("body").trigger(event);
    var status = jobInfo.jobStatus;
    if (status === "esriJobFailed") {
        alert(status);
        esri.hide(loading);
    } else if (status === "esriJobSucceeded") {
        esri.hide(loading);
    }
}

/**
 * 
 * Función que muestra los resultados del geoservicio Seleccionar area para
 * oferta inmobiliaria
 * 
 * @param results
 * @param messages
 * @returns
 */
function displayOfertaInmobiliariaSeleccionar(results) {
    map.graphics.clear();
    console.log("cantidadRegistros: " + results.value);
    var content = dojo.string.substitute('Se seleccionaron ' + results.value
            + ' manzanas');
    dijit.byId('messageWin').setContent(content);
    dijit.byId('messageWin').show();
    if (results.value == 0) {
        var contentNoResults = dojo.string
                .substitute('No se encontraron coincidencias en la base de datos. Por favor seleccione un área diferente.');
        dijit.byId('messageWin').setContent(contentNoResults);
        dijit.byId('messageWin').show();
    }
    //Este evento se lanza para poder ocultar la barra de carga en la pantalla
    var event = jQuery.Event("ocultarBarraCargaEvent");
    $("body").trigger(event);
}

/**
 * 
 * Función del boton Seleccionar Manzana (Sólo para ofertas inmobiliarias)
 * 
 * @param enable
 */
function switchEnableSelectManzana(enable) {
    if (enable) {
        switchEnableIdentify(false);
        switchEnableSelectPredio(false);
        map.disableMapNavigation();
        dijit.byId('dibujarFloater').show();
    } else {
        dojo.disconnect(handleSelectManzana);
    }
}

/**
 * 
 * Funcion que lanza el trigger al cliente con el codigo de los predios
 * 
 * @param codManzana
 */
function seleccionarManzanaOfertas(codManzana) {
    console.log("seleccionarManzana_: " + String(codManzana));
    map.graphics.clear();
    var event = jQuery.Event("onSelectedManzanaOfertas");
    event.manzanaSeleccionada = codManzana;
    $("body").trigger(event);
}

/**
 * 
 * Evento que sucede cuando se activa la herramienta Seleccionar manzana y se
 * ejecuta cuando se necesitan asignar manzanas a un recolector
 * 
 * @param evt
 */
function seleccionarManzanasPorAsignarOfertas(geometry) {
    console.log("seleccionarManzanasPorAsignarOfertas");

    //Este evento se lanza para poder mostrar la barra de carga en la pantalla
    var event = jQuery.Event("mostrarBarraCargaEvent");
    $("body").trigger(event);

    var findManzanasByEstadoParams = new esri.tasks.Query();
    findManzanasByEstadoParams.returnGeometry = true;
    findManzanasByEstadoParams.geometry = geometry;
    findManzanasByEstadoParams.outFields = ['SNC_AVALUOS.V_AREA_OFERTA_INMOBILIARIA.MANZANA_VEREDA_CODIGO'];
    var whereStrIds = null;
    if (idOfertaMapa != null || idOfertaMapa != "") {
        whereStrIds = "AREA_CAPTURA_OFERTA_ID in (" + idOfertaMapa + ") AND ";
    }
    var whereStrEstado = "REGION_CAPTURA_OFERTA_ID is NULL";
    var whereStr = whereStrIds + whereStrEstado;
    findManzanasByEstadoParams.where = whereStr;

    var veredasOfertasLayerUrl = getLayerById(LAYER_OFERTAS_INDEX).layerUrl
            + "/" + INDEX_VEREDAS_OFERTAS;
    var manzanasOfertasLayerUrl = getLayerById(LAYER_OFERTAS_INDEX).layerUrl
            + "/" + INDEX_MANZANAS_OFERTAS;

    var codigoStr = codigosManzanasParaAsignaryEjecutar.toString();
    if (isPredioRural(codigoStr)) {
        identifyManzanasTask = new esri.tasks.QueryTask(veredasOfertasLayerUrl);
    } else {
        identifyManzanasTask = new esri.tasks.QueryTask(manzanasOfertasLayerUrl);
    }
    identifyManzanasTask.execute(findManzanasByEstadoParams,
            showManzanasSeleccionadas);
}

/**
 * 
 * Evento que sucede cuando se activa la herramienta Seleccionar manzana y se
 * ejecuta cuando se necesitan desasignar manzanas a un recolector
 * 
 * @param evt
 */
function seleccionarManzanasEnEjecucionOfertas(geometry) {
    console.log("seleccionarManzanasEnEjecucionOfertas");

    //Este evento se lanza para poder mostrar la barra de carga en la pantalla
    var event = jQuery.Event("mostrarBarraCargaEvent");
    $("body").trigger(event);

    var findManzanasByEstadoParams = new esri.tasks.Query();
    findManzanasByEstadoParams.returnGeometry = true;
    findManzanasByEstadoParams.geometry = geometry;
    findManzanasByEstadoParams.outFields = ['SNC_AVALUOS.V_AREA_OFERTA_INMOBILIARIA.MANZANA_VEREDA_CODIGO'];
    var whereStrIds = null;
    if (idOfertaMapa != null || idOfertaMapa != "") {
        if (idRecolectorMapa != null || idRecolectorMapa != "") {
            whereStrIds = "AREA_CAPTURA_OFERTA_ID in (" + idOfertaMapa
                    + ") AND ASIGNADO_RECOLECTOR_ID = " + idRecolectorMapa
                    + " AND ";
        }
    }
    var whereStrEstado = "REGION_CAPTURA_OFERTA_ID is '"
            + OFERTA_ESTADO_ENEJECUCION + "'";
    var whereStr = whereStrIds + whereStrEstado;
    findManzanasByEstadoParams.where = whereStr;

    var veredasOfertasLayerUrl = getLayerById(LAYER_OFERTAS_INDEX).layerUrl
            + "/" + INDEX_VEREDAS_OFERTAS;
    var manzanasOfertasLayerUrl = getLayerById(LAYER_OFERTAS_INDEX).layerUrl
            + "/" + INDEX_MANZANAS_OFERTAS;

    var codigoStr = codigosManzanasParaAsignaryEjecutar.toString();
    if (isPredioRural(codigoStr)) {
        identifyManzanasTask = new esri.tasks.QueryTask(veredasOfertasLayerUrl);
    } else {
        identifyManzanasTask = new esri.tasks.QueryTask(manzanasOfertasLayerUrl);
    }
    identifyManzanasTask.execute(findManzanasByEstadoParams,
            showManzanasSeleccionadas);
}

/**
 * 
 * Funcion que se ejecuta en respuesta al query de seleccion de manzanas para
 * ofertas inmobiliarias
 */
function showManzanasSeleccionadas(results) {

    var features = results.features;
    //Este evento se lanza para poder ocultar la barra de carga en la pantalla
    var event = jQuery.Event("ocultarBarraCargaEvent");
    $("body").trigger(event);
    if (features.length > 0) {
        var codigoManzana = null;
        var codigoManzanas = [];
        for (var i = 0; i < features.length; i++) {
            var codigo = 'SNC_AVALUOS.V_AREA_OFERTA_INMOBILIARIA.MANZANA_VEREDA_CODIGO';
            codigoManzana = features[i].attributes[codigo];
            codigoManzanas.push(codigoManzana);
        }
        console.log("codigoManzanas " + codigoManzanas);
        var content = dojo.string.substitute("Se seleccionaron "
                + features.length + " manzanas");
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
        seleccionarManzanaOfertas(codigoManzanas);
        return codigoManzanas;
    } else {
        var content = dojo.string
                .substitute('No se encontraron manzanas en el área seleccionada');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }


    dojo.disconnect(handleSelectPredio);
    switchEnableMover();
}

/**
 * 
 * Función que recibe los parametros para ofertas inmobiliarias
 * 
 * @param usuarioDto
 * @returns
 */
function getParametrosOfertas(usuarioDto, idOfertaMapa, municipioCodMapa,
        paramSeleccionManzanasOfertas, predioRuralOfertas, idRecolectorMapa, codigosManzanasParaAsignaryEjecutar) {
    this.usuarioDto = usuarioDto;
    this.municipioCodMapa = municipioCodMapa;
    this.predioRuralOfertas = predioRuralOfertas;
    this.paramSeleccionManzanasOfertas = paramSeleccionManzanasOfertas;
    this.idOfertaMapa = idOfertaMapa;
    this.idRecolectorMapa = idRecolectorMapa;
    this.codigosManzanasParaAsignaryEjecutar = codigosManzanasParaAsignaryEjecutar;

    if (this.paramSeleccionManzanasOfertas == 'seleccionarManzanasPorAsignarOfertas'
            || this.paramSeleccionManzanasOfertas == 'seleccionarManzanasEnEjecucionOfertas') {
        dojo.style(dojo.byId("selectManzanaBtn"), "visibility", "visible");
    }

    console.log("usuarioDto visor.js " + this.usuarioDto);
    console.log("idOfertaMapa visor.js " + this.idOfertaMapa);
    console.log("municipioCodMapa visor.js " + this.municipioCodMapa);
    console.log("paramSeleccionManzanasOfertas visor.js "
            + this.paramSeleccionManzanasOfertas);
    console.log("predioRuralOfertas visor.js " + this.predioRuralOfertas);
    console.log("idRecolectorMapa visor.js " + this.idRecolectorMapa);
    console.log("codigosManzanasParaAsignaryEjecutar visor.js " + this.codigosManzanasParaAsignaryEjecutar);
}

/**
 * 
 * ******************************************************************************************
 * ************************************Actualización*****************************************
 * ******************************************************************************************
 * 
 */


/**
 * 
 * Evento que sucede cuando se activa la herramienta Dibujar y se
 * ejecuta cuando se necesitan asignar predios en actualizacion
 * 
 * @param evt
 */
function seleccionarManzanasActualizacionEvt(geometry) {
    console.log("seleccionarManzanasActualizacionEvt");

    //Este evento se lanza para poder mostrar la barra de carga en la pantalla
    var event = jQuery.Event("mostrarBarraCargaEvent");
    $("body").trigger(event);

    var findPrediosDeActualizacionParams = new esri.tasks.Query();
    findPrediosDeActualizacionParams.returnGeometry = true;
    findPrediosDeActualizacionParams.geometry = geometry;
    findPrediosDeActualizacionParams.outFields = ['CODIGO'];
    var whereStrIds = null;
    var codigoStr = codigosManzanasActualizacion.toString();
    if (codigoStr != null || codigoStr != "") {
        whereStrIds = "CODIGO in (" + codigoStr + ")";
    }
    findPrediosDeActualizacionParams.where = whereStrIds;

    var predioLayerRuralUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
            + INDEX_VEREDAS_RURAL;

    var predioLayerUrbanoUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
            + INDEX_MANZANAS_URBANO;

    if (isPredioRural(codigoStr)) {
        findPredioRuralQueryTask = new esri.tasks.QueryTask(predioLayerRuralUrl);
        findPrediosDeActualizacionParams.execute(findPredioByCodigoQuery,
                showManzanasSeleccionadasActualizacion);
    } else {
        findPredioUrbanoQueryTask = new esri.tasks.QueryTask(predioLayerUrbanoUrl);
        findPredioUrbanoQueryTask.execute(findPrediosDeActualizacionParams,
                showManzanasSeleccionadasActualizacion);
    }
}

/**
 * 
 * Evento que sucede cuando se activa la herramienta Dibujar y se
 * ejecuta cuando se necesitan asignar predios en actualizacion
 * 
 * @param evt
 */
function seleccionarManzanasActualizacionCU208Evt(geometry) {
    console.log("seleccionarManzanasActualizacionCU208Evt");

    //Este evento se lanza para poder mostrar la barra de carga en la pantalla
    var event = jQuery.Event("mostrarBarraCargaEvent");
    $("body").trigger(event);

    var findPrediosDeActualizacionParams = new esri.tasks.Query();
    findPrediosDeActualizacionParams.returnGeometry = true;
    findPrediosDeActualizacionParams.geometry = geometry;
    findPrediosDeActualizacionParams.outFields = ['CODIGO'];

    var predioLayerRuralUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
            + INDEX_VEREDAS_RURAL;

    var predioLayerUrbanoUrl = getLayerById(LAYER_PREDIOS_INDEX).layerUrl + "/"
            + INDEX_MANZANAS_URBANO;

    if (paramIsRuralCU208 == true) {
        findPredioRuralQueryTask = new esri.tasks.QueryTask(predioLayerRuralUrl);
        findPrediosDeActualizacionParams.execute(findPredioByCodigoQuery,
                showManzanasSeleccionadasActualizacion);
    } else {
        findPredioUrbanoQueryTask = new esri.tasks.QueryTask(predioLayerUrbanoUrl);
        findPredioUrbanoQueryTask.execute(findPrediosDeActualizacionParams,
                showManzanasSeleccionadasActualizacion);
    }
}

/**
 * 
 * Funcion que se ejecuta en respuesta al query de seleccion de manzanas para
 * ofertas inmobiliarias
 */
function showManzanasSeleccionadasActualizacion(results) {

    //Este evento se lanza para poder ocultar la barra de carga en la pantalla
    var event = jQuery.Event("ocultarBarraCargaEvent");
    $("body").trigger(event);

    var features = results.features;
    if (features.length > 0) {
        var codigoPredio = null;
        var codPredios = [];
        for (var i = 0; i < features.length; i++) {
            var codigo = 'CODIGO';
            codigoPredio = features[i].attributes[codigo];
            codPredios.push(codigoPredio);
        }
        console.log("codigoManzanas " + codPredios);
        var content = dojo.string.substitute("Se seleccionaron "
                + features.length + " manzanas");
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
        seleccionarManzanasActualizacion(codPredios);
        return codPredios;
    } else {
        var content = dojo.string
                .substitute('No se encontraron predios en el área seleccionada');
        dijit.byId('messageWin').setContent(content);
        dijit.byId('messageWin').show();
    }
    dojo.disconnect(handleSelectPredio);
    switchEnableMover();
}

/**
 * 
 * Funcion que lanza el trigger al cliente con el codigo de los predios
 * @param codPredios
 */
function seleccionarManzanasActualizacion(codManzanas) {
    console.log("seleccionarManzanasActualizacion_: " + String(codManzanas));
    map.graphics.clear();
    var event = jQuery.Event("onSelectedManzanasActualizacion");
    event.manzanasSeleccionadas = codManzanas;
    $("body").trigger(event);
}

function getParametrosActualizacion(paramSeleccionPrediosActualizacion, codigosManzanasActualizacion, paramIsRuralCU208) {
    this.paramSeleccionPrediosActualizacion = paramSeleccionPrediosActualizacion;
    this.codigosManzanasActualizacion = codigosManzanasActualizacion;
    this.paramIsRuralCU208 = paramIsRuralCU208;
}
