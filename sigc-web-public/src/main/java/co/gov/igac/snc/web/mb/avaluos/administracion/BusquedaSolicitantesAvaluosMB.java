package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Creado para manejar los eventos de la página busquedaSolicitanteAvaluos.xhtml. Encargada de
 * realizar busqeudas de solicitantes de avalúos comerciales
 *
 * @author christian.rodriguez
 *
 */
@Component("busquedaSolicitanteAvaluos")
@Scope("session")
public class BusquedaSolicitantesAvaluosMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 2734396405771696317L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(BusquedaSolicitantesAvaluosMB.class);

    private FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante;

    /**
     * Contiene los solicitantes encontrados en la busqueda
     */
    private List<Solicitante> solicitantesEncontrados;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean banderaEsPersonaNatural;
    private boolean banderaPanelResultadosBusquedaSolicitantes;

    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaSolicitante getFiltroDatosConsultaSolicitante() {
        return filtroDatosConsultaSolicitante;
    }

    public void setFiltroDatosConsultaSolicitante(
        FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante) {
        this.filtroDatosConsultaSolicitante = filtroDatosConsultaSolicitante;
    }

    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    public void setBanderaEsPersonaNatural(boolean banderaEsPersonaNatural) {
        this.banderaEsPersonaNatural = banderaEsPersonaNatural;
    }

    public List<Solicitante> getSolicitantesEncontrados() {
        return this.solicitantesEncontrados;
    }

    public void setSolicitantesEncontrados(List<Solicitante> solicitantesEncontrados) {
        this.solicitantesEncontrados = solicitantesEncontrados;
    }

    public boolean isBanderaPanelResultadosBusquedaSolicitantes() {
        return banderaPanelResultadosBusquedaSolicitantes;
    }

    public void setBanderaPanelResultadosBusquedaSolicitantes(
        boolean banderaPanelResultadosBusquedaSolicitantes) {
        this.banderaPanelResultadosBusquedaSolicitantes = banderaPanelResultadosBusquedaSolicitantes;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on BusquedaSolicitantesAvaluosMB init");

        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.filtroDatosConsultaSolicitante
            .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());

        this.banderaEsPersonaNatural = true;
        this.banderaPanelResultadosBusquedaSolicitantes = false;
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia el tipo de persona. Activa la bandera de si es
     * persona natural y cambia el tipo de documento por defecto
     *
     * @author lorena.salamanca
     * @modified christian.rodriguez
     */
    public void onChangeTipoPersona() {

        if (this.filtroDatosConsultaSolicitante.getTipoPersona().equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {

            this.banderaEsPersonaNatural = true;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());

        } else {
            this.banderaEsPersonaNatural = false;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT.getCodigo());
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener del botón de buscar, llama al método que busca los solicitantes de acuerdo al filtro
     * especificado por el usuario
     *
     * @author christian.rodriguez
     */
    public void buscar() {

        if (this.filtroDatosConsultaSolicitante != null) {

            this.solicitantesEncontrados = this.getTramiteService()
                .buscarSolicitantesPorFiltroConContratoInteradministrativo(
                    this.filtroDatosConsultaSolicitante);
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que puede ser llamado desde otros ManagedBeans para ejecutar el método de busqueda y
     * retornar los solicitantes encontrados
     *
     * @author christian.rodriguez
     * @return Lsita de solicitantes encontrados
     */
    public List<Solicitante> buscarYDevolverResultados() {
        this.buscar();
        return this.solicitantesEncontrados;
    }
}
