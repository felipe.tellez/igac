/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util.converters;

import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.util.Constantes;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter para los objetos CalificacionConstruccion para poder ser usados como valor en los items
 * de los combos
 *
 * @author pedro.garcia
 */
//TODO pedro.garcia revisar el forClass. Debería ser para CalificacionConstruccion
@FacesConverter(value = "califConstrConverter",
    forClass = co.gov.igac.snc.web.util.CalificacionConstruccionTreeNode.class)
public class CalificacionConstruccionConverter implements Converter {

//--------------------------------------------------------------------------------------------------
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        CalificacionConstruccion answer = null;

        answer = new CalificacionConstruccion();
        String[] objectAttributes;
        objectAttributes = value.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setId(new Long(objectAttributes[0]));
        answer.setComponente(objectAttributes[1]);
        answer.setElemento(objectAttributes[2]);
        answer.setDetalleCalificacion(objectAttributes[3]);
        answer.setPuntos(new Integer(objectAttributes[4]).intValue());

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((CalificacionConstruccion) o).toString();
    }

}
