package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.persistence.util.EUnidadTipoDominio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author jonathan.chacon
 */
@Component("liqudacionAvaluoPredioFiscal")
@Scope("session")
public class LiquidacionAvaluoPredioFiscalMB extends SNCManagedBean {

    Double valorMtTerreno = 0.0;
    Double valorMtTerrenoP = 0.0;
    Double valorMtConstr = 0.0;
    Double valorMtConstrP = 0.0;
    Calendar c = Calendar.getInstance();
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProyectarConservacionMB.class);

    /**
     * Metodo que realiza el calculo de el avaluo para desenglobes con predios condicion 0
     *
     * @param areaTotalTerrenoUnidadF
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @return
     */
    public PPredioAvaluoCatastral calcDesenglobeCondCero(Double areaTotalTerrenoUnidadF,
        Tramite tramite,
        PPredio predioSeleccionado, UsuarioDTO usuario, PPredioAvaluoCatastral avaluo) {

        c.set(Calendar.DAY_OF_YEAR, 1);
        try {

            //Validaciones
            if ((areaTotalTerrenoUnidadF <= 0 ||
                 areaTotalTerrenoUnidadF >= tramite.getPredio().getAreaTerreno()) && !tramite.
                getPredio().isMejora()) {
                addMensajeError("El área ingresada no es valida.");
                return avaluo;
            }

            List<PPredio> prediosP = getConservacionService().obtenerPPrediosPorTramiteIdActAvaluos(
                tramite.getId());
            List<PredioAvaluoCatastral> avaluoPredioOriginal = getConservacionService().
                obtenerHistoricoAvaluoPredio(tramite.getPredio().getId());

            for (PPredio predio : prediosP) {
                if (!predioSeleccionado.getId().equals(predio.getId())) {
                    valorMtTerrenoP += predio.getAreaTerreno();
                }
            }

            if ((valorMtTerrenoP + areaTotalTerrenoUnidadF) > tramite.getPredio().getAreaTerreno()) {
                addMensajeError("El área ingresada no es valida. El valor máximo es de : " +
                    (tramite.getPredio().getAreaTerreno() - valorMtTerrenoP));
                return avaluo;
            }

            if (!tramite.getPredio().isMejora()) {
                valorMtTerreno = (double) Math.round(avaluoPredioOriginal.get(avaluoPredioOriginal.
                    size() - 1).getValorTerreno() / tramite.getPredio().getAreaTerreno());
                avaluo.
                    setValorTerreno((double) Math.round(valorMtTerreno * areaTotalTerrenoUnidadF));
                avaluo.setValorTerrenoPrivado((double) Math.round(valorMtTerreno *
                    areaTotalTerrenoUnidadF));
            } else {
                avaluo.setValorTerreno(0.0);
                avaluo.setValorTerrenoPrivado(0.0);
            }

            avaluo.setPPredio(predioSeleccionado);
            avaluo.setCancelaInscribe(predioSeleccionado.getCancelaInscribe());
            avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
            avaluo.setUsuarioLog(usuario.getLogin());
            avaluo.setFechaLog(new Date());
            avaluo.setVigencia(c.getTime());
            avaluo.setFechaInscripcionCatastral(c.getTime());

            if (predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
                avaluo.setValorTotalConstruccion(0.0);
                avaluo.setValorTotalConsConvencional(0.0);
                avaluo.setValorTotalConsNconvencional(0.0);
                avaluo.setValorTotalConstruccionPrivada(0.0);
            } else {
                double totalConvencional = 0.0;
                double totalNoConvencional = 0.0;
                double totalprivada = 0.0;

                for (PUnidadConstruccion pUnidadConstruccion : predioSeleccionado.
                    getPUnidadConstruccions()) {

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalNoConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoDominio.PRIVADO.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalprivada += pUnidadConstruccion.getAvaluo();
                    }
                }

                avaluo.setValorTotalConsConvencional(totalConvencional);
                avaluo.setValorTotalConsNconvencional(totalNoConvencional);
                avaluo.setValorTotalConstruccionPrivada(totalprivada);
                avaluo.setValorTotalConstruccion(totalConvencional + totalNoConvencional);
            }

            avaluo.setValorTerrenoComun(0.0);
            avaluo.setValorConstruccionComun(0.0);
            avaluo.setValorTotalAvaluoVigencia(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            avaluo.setValorTotalAvaluoCatastral(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());

            predioSeleccionado.setAreaTerreno(areaTotalTerrenoUnidadF);
            predioSeleccionado.setAvaluoCatastral(avaluo.getValorTotalAvaluoCatastral());

            //redondeo de cifras
            redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

            if (avaluo.getId() == null) {
                avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
            } else {
                avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
            }

            getConservacionService().actualizarPPredio(predioSeleccionado);

            addMensajeInfo("Valor guardado correctamente");

        } catch (NumberFormatException ex) {
            addMensajeError("El área ingresada debe ser un numero.");
            LOGGER.error("error el valor ingresado no es valido." + ex);
            return avaluo;
        } catch (Exception e) {
            addMensajeError("Error guardando el avalúo");
            LOGGER.error("error en ProyectarConservacionMB#guardarAvaluoPredioFiscal : " + e);
            return avaluo;
        }

        return avaluo;
    }

    /**
     *
     * @param areaTotalTerrenoUnidadF
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @return
     */
    public PPredioAvaluoCatastral calcDesenglobeMejora(Double areaTotalTerrenoUnidadF,
        Tramite tramite,
        PPredio predioSeleccionado, UsuarioDTO usuario, PPredioAvaluoCatastral avaluo) {

        c.set(Calendar.DAY_OF_YEAR, 1);
        Double valorTotalConstrucciones = 0.0;

        try {

            List<PredioAvaluoCatastral> avaluoPredioOriginal = getConservacionService().
                obtenerHistoricoAvaluoPredio(tramite.getPredio().getId());

            if (!tramite.getPredio().isMejora()) {
                valorMtTerreno = avaluoPredioOriginal.get(avaluoPredioOriginal.size() - 1).
                    getValorTerreno() / tramite.getPredio().getAreaTerreno();
                avaluo.
                    setValorTerreno((double) Math.round(valorMtTerreno * areaTotalTerrenoUnidadF));
                avaluo.setValorTerrenoPrivado((double) Math.round(valorMtTerreno *
                    areaTotalTerrenoUnidadF));
            } else {
                avaluo.setValorTerreno(0.0);
                avaluo.setValorTerrenoPrivado(0.0);
            }

            avaluo.setPPredio(predioSeleccionado);
            avaluo.setCancelaInscribe(predioSeleccionado.getCancelaInscribe());
            avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
            avaluo.setUsuarioLog(usuario.getLogin());
            avaluo.setFechaLog(new Date());
            avaluo.setVigencia(c.getTime());
            avaluo.setFechaInscripcionCatastral(c.getTime());

            if (predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
                avaluo.setValorTotalConstruccion(0.0);
                avaluo.setValorTotalConsConvencional(0.0);
                avaluo.setValorTotalConsNconvencional(0.0);
                avaluo.setValorTotalConstruccionPrivada(0.0);
            } else {
                double totalConvencional = 0.0;
                double totalNoConvencional = 0.0;
                double totalprivada = 0.0;

                for (PUnidadConstruccion pUnidadConstruccion : predioSeleccionado.
                    getPUnidadConstruccions()) {

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalNoConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoDominio.PRIVADO.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalprivada += pUnidadConstruccion.getAvaluo();
                    }
                }

                avaluo.setValorTotalConsConvencional(totalConvencional);
                avaluo.setValorTotalConsNconvencional(totalNoConvencional);
                avaluo.setValorTotalConstruccionPrivada(totalprivada);
            }

            avaluo.setValorTerrenoComun(0.0);
            avaluo.setValorConstruccionComun(0.0);
            avaluo.setValorTotalAvaluoVigencia(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            avaluo.setValorTotalAvaluoCatastral(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());

            predioSeleccionado.setAreaTerreno(areaTotalTerrenoUnidadF);
            predioSeleccionado.setAvaluoCatastral(avaluo.getValorTotalAvaluoCatastral());

            //redondeo de cifras
            redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

            if (avaluo.getId() == null) {
                avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
            } else {
                avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
            }

            getConservacionService().actualizarPPredio(predioSeleccionado);
            addMensajeInfo("Valor guardado correctamente");

        } catch (NumberFormatException ex) {
            addMensajeError("El área ingresada debe ser un numero.");
            LOGGER.error("error el valor ingresado no es valido." + ex);
            return null;
        } catch (Exception e) {
            addMensajeError("Error guardando el avalúo");
            LOGGER.error("error en ProyectarConservacionMB#guardarAvaluoPredioFiscal : " + e);
            return null;
        }

        return avaluo;
    }

    /**
     * Método que realiza el calculo de los avaluos en una mutacion tercera para predios fiscales
     *
     * @param valorMetroFiscal
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @return
     */
    public PPredioAvaluoCatastral calcMutacionTercera(Double valorMetroFiscal, Tramite tramite,
        PPredio predioSeleccionado,
        UsuarioDTO usuario, PPredioAvaluoCatastral avaluo) {

        c.set(Calendar.DAY_OF_YEAR, 1);

        try {

            avaluo.setValorTerreno(valorMetroFiscal * predioSeleccionado.getAreaTerreno());

            if (tramite.getPredio().getAreaConstruccion() == 0) {
                avaluo.setValorTerreno(predioSeleccionado.getAvaluoCatastral());
            } else if (tramite.getPredio().isMejora()) {
                avaluo.setValorTerreno(0.0);
            }

            avaluo.setPPredio(predioSeleccionado);
            avaluo.setUsuarioLog(usuario.getLogin());

            if (predioSeleccionado.getPUnidadConstruccions().isEmpty() && tramite.getPredio().
                isMejora()) {

                addMensajeError(
                    "Una mutación de tercera de una mejora no puede quedar sin área de construcción");
                return null;

            }

            if (predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
                avaluo.setValorTotalConstruccion(0.0);
                avaluo.setValorTotalConsConvencional(0.0);
                avaluo.setValorTotalConsNconvencional(0.0);
                avaluo.setValorTotalConstruccionPrivada(0.0);
            } else {
                for (PUnidadConstruccion unidad : predioSeleccionado.getPUnidadConstruccions()) {
                    if (!unidad.getCancelaInscribe().equals("C")) {
                        valorMtConstr += unidad.getAvaluo();
                    }
                }
                avaluo.setValorTotalConstruccion(valorMtConstr);

                double totalConvencional = 0.0;
                double totalNoConvencional = 0.0;
                double totalprivada = 0.0;

                for (PUnidadConstruccion pUnidadConstruccion : predioSeleccionado.
                    getPUnidadConstruccions()) {

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalNoConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoDominio.PRIVADO.getCodigo()) &&
                         !pUnidadConstruccion.getCancelaInscribe()
                            .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                        totalprivada += pUnidadConstruccion.getAvaluo();
                    }
                }

                avaluo.setValorTotalConsConvencional(totalConvencional);
                avaluo.setValorTotalConsNconvencional(totalNoConvencional);
                avaluo.setValorTotalConstruccionPrivada(totalprivada);

            }

            avaluo.setPPredio(predioSeleccionado);
            avaluo.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
            avaluo.setUsuarioLog(usuario.getLogin());
            avaluo.setFechaLog(new Date());
            avaluo.setVigencia(c.getTime());
            avaluo.setFechaInscripcionCatastral(c.getTime());

            avaluo.setValorTerrenoComun(0.0);
            avaluo.setValorConstruccionComun(0.0);
            avaluo.setValorTotalAvaluoVigencia((double) Math.round(avaluo.getValorTerreno() +
                avaluo.getValorTotalConstruccion()));
            avaluo.setValorTotalAvaluoCatastral((double) Math.round(avaluo.getValorTerreno() +
                avaluo.getValorTotalConstruccion()));

            predioSeleccionado.setAvaluoCatastral(avaluo.getValorTotalAvaluoCatastral());

            //redondeo de cifras
            redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

            if (avaluo.getId() == null) {
                avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
            } else {
                avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
            }

            getConservacionService().actualizarPPredio(predioSeleccionado);
            addMensajeInfo("Valor guardado correctamente");

        } catch (NumberFormatException ex) {
            addMensajeError("El área ingresada debe ser un numero.");
            LOGGER.error("error el valor ingresado no es valido." + ex);
            return null;
        } catch (Exception e) {
            addMensajeError("Error guardando el avalúo");
            LOGGER.error("error en ProyectarConservacionMB#guardarAvaluoPredioFiscal : " + e);
            return null;
        }

        return avaluo;
    }

    /**
     * Método que realiza la liquidacion de los englobes para predios fiscales
     *
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @return
     */
    public PPredioAvaluoCatastral calcAvaluoEnglobe(Tramite tramite, PPredio predioSeleccionado,
        UsuarioDTO usuario, PPredioAvaluoCatastral avaluo) {

        ProyectarConservacionMB pcmb = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");
        try {

            if (avaluo.getValorTotalConstruccion() < 0 || avaluo.getValorTerreno() < 0) {
                addMensajeError("El valor del avalúo ingresado debe ser mayor a cero.");
                return avaluo;
            }

            if (pcmb.isBanderaSoloMejorasEnglobe()) {
                if (avaluo.getValorTotalConstruccion() <= 0) {
                    addMensajeError("El valor del avalúo ingresado debe ser mayor a cero.");
                    return avaluo;
                }
            } else {
                //verifica que los predios originales no tengan construcciones
                int ucons = 0;
                for (Predio p : tramite.getPredios()) {
                    ucons += p.getTotalUnidadesConstruccion();
                }

                if (ucons == 0 && avaluo.getValorTotalConstruccion() > 0) {
                    addMensajeError(
                        "Los predios originales no tienen construcciones asociadas, verifique el valor ingresado.");
                    return avaluo;
                }
            }

            if (avaluo.getValorTotalAvaluoCatastral() == 0.0) {
                avaluo.setValorTotalAvaluoCatastral(avaluo.getValorTerreno() + avaluo.
                    getValorTotalConstruccion());
            }

            if (avaluo != null && avaluo.getValorTotalAvaluoCatastral() != null &&
                 avaluo.getValorTerreno() != null && avaluo.getValorTerreno() != null) {
                if ((avaluo.getValorTotalAvaluoCatastral() < (avaluo.getValorTerreno() +
                     avaluo.getValorTotalConstruccion()) - 1000) ||
                     (avaluo.getValorTotalAvaluoCatastral() > (avaluo.getValorTerreno() +
                     avaluo.getValorTotalConstruccion()) + 1000) &&
                     !tramite.getPredio().isMejora()) {
                    addMensajeError("Avalúo total no coincide con los la suma de los " +
                         "avalúos de los predios originales, Verifique");
                    return avaluo;
                }
            }

            if (pcmb.isBanderaSoloMejorasEnglobe()) {
                avaluo.setValorTotalAvaluoCatastral(this.trimDouble(avaluo.
                    getValorTotalConstruccion()));

                if (avaluo.getValorTotalConstruccion() == 0) {
                    addMensajeError("Debe Cambiar los Valores para liquidar el terreno");
                    return avaluo;
                }
            }
            avaluo.setValorTotalAvaluoCatastral(this.trimDouble(avaluo.
                getValorTotalAvaluoCatastral()));

            if (pcmb.isBanderaSoloMejorasEnglobe()) {
                pcmb.setValorMetroFiscal(0.0);
                avaluo.setValorTerreno(0.0);
                avaluo.setValorTerrenoComun(0.0);
                avaluo.setValorTerrenoPrivado(0.0);

            } else {
                pcmb.setValorMetroFiscal((double) Math.round(avaluo.getValorTerreno() / pcmb.
                    getAreaTotalTerrenoUnidadF()));
            }

            pcmb.getAreaTotalTerrenoUnidad();

            if (pcmb.getAreaTotalTerrenoUnidadF() == 0 && !tramite.getPredio().isMejora()) {
                addMensajeError("El área de terreno debe ser mayor a cero");
                return avaluo;
            }

            predioSeleccionado.setAreaTerreno(pcmb.getAreaTotalTerrenoUnidadF());

            List<PUnidadConstruccion> unidades = predioSeleccionado.getPUnidadConstruccions();

            double totalConvencional = 0.0;
            double totalNoConvencional = 0.0;
            double totalprivada = 0.0;

            for (PUnidadConstruccion pUnidadConstruccion : unidades) {

                if (pUnidadConstruccion.getTipoConstruccion().
                    equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                    totalConvencional += pUnidadConstruccion.getAvaluo();
                }

                if (pUnidadConstruccion.getTipoConstruccion().
                    equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                    totalConvencional += pUnidadConstruccion.getAvaluo();
                }

                if (pUnidadConstruccion.getTipoConstruccion().
                    equals(EUnidadTipoDominio.PRIVADO.getCodigo())) {
                    totalprivada += pUnidadConstruccion.getAvaluo();
                }
            }

            avaluo.setValorTotalConsConvencional(totalConvencional);
            avaluo.setValorTotalConsNconvencional(totalNoConvencional);
            avaluo.setValorTotalConstruccionPrivada(totalprivada);

            avaluo.setPPredio(predioSeleccionado);
            avaluo.setCancelaInscribe(predioSeleccionado.getCancelaInscribe());
            avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
            avaluo.setUsuarioLog(usuario.getLogin());
            avaluo.setFechaLog(new Date());
            avaluo.setVigencia(c.getTime());
            avaluo.setFechaInscripcionCatastral(c.getTime());
            avaluo.setValorTerrenoComun(0.0);
            avaluo.setValorConstruccionComun(0.0);
            avaluo.setValorTotalAvaluoVigencia(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            avaluo.setValorTotalAvaluoCatastral(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());

            predioSeleccionado.setAvaluoCatastral(avaluo.getValorTotalAvaluoCatastral());

            //redondeo de cifras
            redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

            if (avaluo.getId() == null) {
                avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
            } else {
                avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
            }

            getConservacionService().actualizarPPredio(predioSeleccionado);
            addMensajeInfo("Valor guardado correctamente");

        } catch (NumberFormatException ex) {
            addMensajeError("El valor ingresado debe ser un numero.");
            LOGGER.error("error el valor ingresado no es valido." + ex);
            return avaluo;
        } catch (Exception e) {
            addMensajeError("Error guardando el avalúo");
            LOGGER.error("error en LiquidacionAvaluoPredioFiscalMB#calcAvaluoEnglobe : " + e);
            return avaluo;
        }

        return avaluo;
    }

    /**
     * Metodo que liquida los avalúos de los tramites quinta omitido que tienen fecha de inscripcion
     * catastral
     *
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @param vigencia
     * @return avaluo
     */
    public PPredioAvaluoCatastral calcAvaluoQuintaOmitido(Double areaTotalTerrenoUnidadF,
        Tramite tramite, PPredio predioSeleccionado,
        UsuarioDTO usuario, PPredioAvaluoCatastral avaluo, String vigencia) {

        //instancia de datos
        //se recupera el predio completo ya que le faltaban algunos datos por instanciar
        Predio pred = this.getConservacionService().obtenerPredioPorId(predioSeleccionado.getId());
        int anioVigencia = Integer.parseInt(vigencia.substring(0, 4));
        int aanioVigencia = avaluo.getVigencia().getYear();
        int anioActual = Calendar.getInstance().get(Calendar.YEAR);
        Double porcentajeLiquidacion = 0.0;
        ProyectarConservacionMB pcmb = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");

        //Se obtiene el último avalúo proyectado para saber desde que anio liquidar
        List<PPredioAvaluoCatastral> listaAvaluos = getConservacionService()
            .obtenerListaPPredioAvaluoCatastralPorIdPPredio(predioSeleccionado.getId());
        PPredioAvaluoCatastral avaluoUltimaVigencia = listaAvaluos.get(0);
        int anioUltimaVigenciaProyectada = Integer.parseInt(avaluoUltimaVigencia.getVigencia().
            toString().substring(0, 4));

        if (anioUltimaVigenciaProyectada > anioVigencia && anioUltimaVigenciaProyectada !=
            anioActual + 1) {
            anioVigencia = anioUltimaVigenciaProyectada + 1;
        }

        //Ciclo
        while (anioVigencia != anioActual + 1) {

            List<PPredioAvaluoCatastral> listaAvaluosv = getConservacionService()
                .obtenerListaPPredioAvaluoCatastralPorIdPPredio(predioSeleccionado.getId());
            PPredioAvaluoCatastral avaluoUltimaVigenciav = listaAvaluosv.get(0);

            vigencia = Integer.toString(anioVigencia);
            porcentajeLiquidacion = this.getConservacionService()
                .obtenerPorcentajeIncrementoVigenciaPredio("01/01/" + vigencia, predioSeleccionado.
                    getId());

            porcentajeLiquidacion = (porcentajeLiquidacion / 100) + 1;
            LOGGER.debug("Porcentaje liquidación " + anioVigencia + " : " + porcentajeLiquidacion);

            if (anioVigencia != anioActual) {
                avaluo = new PPredioAvaluoCatastral();
                avaluo.setValorTerreno(avaluoUltimaVigenciav.getValorTerreno() *
                    porcentajeLiquidacion);
                avaluo.setValorTotalAvaluoCatastral(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral() * porcentajeLiquidacion);
                avaluo.setValorTotalAvaluoVigencia(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral() * porcentajeLiquidacion);
                avaluo.setValorTotalAvaluoCatastral(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral() * porcentajeLiquidacion);
                avaluo.setValorTotalAvaluoVigencia(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral() * porcentajeLiquidacion);
                predioSeleccionado.setAvaluoCatastral(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral() * porcentajeLiquidacion);
                avaluo.setValorTotalConstruccion(avaluoUltimaVigenciav.getValorTotalConstruccion() *
                    porcentajeLiquidacion);
            } else {
                avaluo.setValorTerreno(pcmb.getValorMetroFiscal() * areaTotalTerrenoUnidadF);
                avaluo.setValorTerrenoPrivado(pcmb.getValorMetroFiscal() * areaTotalTerrenoUnidadF);
                avaluo.setValorTotalAvaluoCatastral(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral());
                avaluo.setValorTotalAvaluoVigencia(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral());
                avaluo.setValorTotalAvaluoCatastral(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral());
                avaluo.setValorTotalAvaluoVigencia(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral());
                predioSeleccionado.setAvaluoCatastral(avaluoUltimaVigenciav.
                    getValorTotalAvaluoCatastral());
                avaluo.setValorTotalConstruccion(avaluoUltimaVigenciav.getValorTotalConstruccion());
            }
            try {
                //traer el avalúo mas reciente y liquidar hasta el año actual
                SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/mm/yy");
                Date vig = formatoFecha.parse("01/01/" + vigencia);
                avaluo.setVigencia(vig);
//                avaluo.setFechaInscripcionCatastral(vig);

                //unidades
                List<PUnidadConstruccion> unidades = predioSeleccionado.getPUnidadConstruccions();

                double totalConvencional = 0.0;
                double totalNoConvencional = 0.0;
                double totalprivada = 0.0;
                double totalConstruccion = 0.0;

                for (PUnidadConstruccion pUnidadConstruccion : unidades) {

                    totalConstruccion += pUnidadConstruccion.getAvaluo();
                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                        totalConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                        totalNoConvencional += pUnidadConstruccion.getAvaluo();
                    }

                    if (pUnidadConstruccion.getTipoConstruccion().
                        equals(EUnidadTipoDominio.PRIVADO.getCodigo())) {
                        totalprivada += pUnidadConstruccion.getAvaluo();
                    }
                }

                avaluo.setValorTotalConsConvencional(totalConvencional);
                avaluo.setValorTotalConsNconvencional(totalNoConvencional);
                avaluo.setValorTotalConstruccionPrivada(totalprivada);
                avaluo.setValorTotalConstruccion(totalConstruccion);

                avaluo.setPPredio(predioSeleccionado);
                avaluo.setCancelaInscribe(predioSeleccionado.getCancelaInscribe());
                avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
                avaluo.setUsuarioLog(usuario.getLogin());
                avaluo.setFechaLog(new Date());

                avaluo.setValorTerrenoComun(0.0);
                avaluo.setValorTerrenoPrivado(0.0);
                avaluo.setValorConstruccionComun(0.0);
                pcmb.getAreaTotalTerrenoUnidad();
                //redondeo de cifras
                redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

                if (avaluo.getId() == null) {
                    avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
                } else {
                    avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
                }

                getConservacionService().actualizarPPredio(predioSeleccionado);
                anioVigencia += 1;
            } catch (ParseException ex) {
                LOGGER.error(
                    "Error calculando el avaluo LiquidacionAvaluoPredioFiscalMB#calcAvaluoQuintaOmitido : " +
                    ex);
            }
        }//fin while

        addMensajeInfo("Valor guardado correctamente");
        return avaluo;
    }

    /**
     * Método que liquida el avaluo para los trámites de quinta Nuevo predio fiscal
     *
     * @param areaTotalTerrenoUnidadF
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @return
     */
    public PPredioAvaluoCatastral calcAvaluoQuintaNuevo(Double areaTotalTerrenoUnidadF,
        Tramite tramite, PPredio predioSeleccionado,
        UsuarioDTO usuario, PPredioAvaluoCatastral avaluo) {

        c.set(Calendar.DAY_OF_YEAR, 1);
        ProyectarConservacionMB pcmb = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");

        try {

            avaluo.setValorTerreno(pcmb.getValorMetroFiscal() * areaTotalTerrenoUnidadF);
            avaluo.setValorTerrenoPrivado(pcmb.getValorMetroFiscal() * areaTotalTerrenoUnidadF);

            double totalConvencional = 0.0;
            double totalNoConvencional = 0.0;
            double totalprivada = 0.0;
            double totalConstruccion = 0.0;
            for (PUnidadConstruccion pUnidadConstruccion : predioSeleccionado.
                getPUnidadConstruccions()) {

                totalConstruccion += pUnidadConstruccion.getAvaluo();
                if (pUnidadConstruccion.getTipoConstruccion().
                    equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                     !pUnidadConstruccion.getCancelaInscribe()
                        .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    totalConvencional += pUnidadConstruccion.getAvaluo();
                }

                if (pUnidadConstruccion.getTipoConstruccion().
                    equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                     !pUnidadConstruccion.getCancelaInscribe()
                        .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    totalNoConvencional += pUnidadConstruccion.getAvaluo();
                }

                if (pUnidadConstruccion.getTipoConstruccion().
                    equals(EUnidadTipoDominio.PRIVADO.getCodigo()) &&
                     !pUnidadConstruccion.getCancelaInscribe()
                        .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    totalprivada += pUnidadConstruccion.getAvaluo();
                }
            }

            avaluo.setValorTotalConsConvencional(totalConvencional);
            avaluo.setValorTotalConsNconvencional(totalNoConvencional);
            avaluo.setValorTotalConstruccionPrivada(totalprivada);
            avaluo.setValorTotalConstruccion(totalConstruccion);

            avaluo.setPPredio(predioSeleccionado);
            avaluo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
            avaluo.setUsuarioLog(usuario.getLogin());
            avaluo.setFechaLog(new Date());
            avaluo.setVigencia(c.getTime());
            avaluo.setFechaInscripcionCatastral(c.getTime());

            avaluo.setValorTerrenoComun(0.0);
            avaluo.setValorConstruccionComun(0.0);
            avaluo.setValorTotalAvaluoVigencia(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            avaluo.setValorTotalAvaluoCatastral(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            pcmb.getAreaTotalTerrenoUnidad();

            predioSeleccionado.setAreaTerreno(areaTotalTerrenoUnidadF);
            predioSeleccionado.setAvaluoCatastral(avaluo.getValorTotalAvaluoCatastral());

            //redondeo de cifras
            redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

            if (avaluo.getId() == null) {
                avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
            } else {
                avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
            }

            getConservacionService().actualizarPPredio(predioSeleccionado);

            addMensajeInfo("Valor guardado correctamente");

        } catch (NumberFormatException ex) {
            addMensajeError("El área ingresada debe ser un numero.");
            LOGGER.error("error el valor ingresado no es valido." + ex);
            return avaluo;
        } catch (Exception e) {
            addMensajeError("Error guardando el avalúo");
            LOGGER.error("error en ProyectarConservacionMB#guardarAvaluoPredioFiscal : " + e);
            return avaluo;
        }

        return avaluo;
    }

    /**
     *
     * @param areaTotalTerrenoUnidadF
     * @param tramite
     * @param predioSeleccionado
     * @param usuario
     * @param avaluo
     * @return
     */
    public PPredioAvaluoCatastral calcAvaluoRectificacionAreaTerreno(Double areaTotalTerrenoUnidadF,
        Tramite tramite, PPredio predioSeleccionado, UsuarioDTO usuario,
        PPredioAvaluoCatastral avaluo) {

        c.set(Calendar.DAY_OF_YEAR, 1);
        ProyectarConservacionMB pcmb = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");

        try {

            avaluo.setValorTerreno(pcmb.getValorMetroFiscal() * areaTotalTerrenoUnidadF);
            avaluo.setValorTerrenoPrivado(pcmb.getValorMetroFiscal() * areaTotalTerrenoUnidadF);

//            double totalConvencional = 0.0;
//            double totalNoConvencional = 0.0;
//            double totalprivada = 0.0;
//            double totalConstruccion = 0.0;
//            for (PUnidadConstruccion pUnidadConstruccion : predioSeleccionado.getPUnidadConstruccions()) {
//
//                totalConstruccion += pUnidadConstruccion.getAvaluo();
//                
//                if (pUnidadConstruccion.getTipoConstruccion().
//                    equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())
//                    && !pUnidadConstruccion.getCancelaInscribe()
//                    .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
//                    totalConvencional += pUnidadConstruccion.getAvaluo();
//                }
//
//                if (pUnidadConstruccion.getTipoConstruccion().
//                    equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())
//                    && !pUnidadConstruccion.getCancelaInscribe()
//                    .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
//                    totalNoConvencional += pUnidadConstruccion.getAvaluo();
//                }
//
//                if (pUnidadConstruccion.getTipoConstruccion().
//                    equals(EUnidadTipoDominio.PRIVADO.getCodigo())
//                    && !pUnidadConstruccion.getCancelaInscribe()
//                    .equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
//                    totalprivada += pUnidadConstruccion.getAvaluo();
//                }
//            }
//            avaluo.setValorTotalConsConvencional(totalConvencional);
//            avaluo.setValorTotalConsNconvencional(totalNoConvencional);
//            avaluo.setValorTotalConstruccionPrivada(totalprivada);
//            avaluo.setValorTotalConstruccion(totalConstruccion);
            avaluo.setPPredio(predioSeleccionado);
            avaluo.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
            avaluo.setUsuarioLog(usuario.getLogin());
            avaluo.setFechaLog(new Date());
            avaluo.setVigencia(c.getTime());
            avaluo.setFechaInscripcionCatastral(c.getTime());

            avaluo.setValorTerrenoComun(0.0);
            avaluo.setValorConstruccionComun(0.0);
            avaluo.setValorTotalAvaluoVigencia(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            avaluo.setValorTotalAvaluoCatastral(avaluo.getValorTerreno() + avaluo.
                getValorTotalConstruccion());
            pcmb.getAreaTotalTerrenoUnidad();

            predioSeleccionado.setAreaTerreno(areaTotalTerrenoUnidadF);
            predioSeleccionado.setAvaluoCatastral(avaluo.getValorTotalAvaluoCatastral());

            //redondeo de cifras
            redondeoAvaluosPrediosFiscales(predioSeleccionado, avaluo);

            if (avaluo.getId() == null) {
                avaluo = getConservacionService().guardarPPredioAvaluoCatastral(avaluo);
            } else {
                avaluo = getConservacionService().actualizarPPredioAvaluoCatastral(avaluo);
            }

            getConservacionService().actualizarPPredio(predioSeleccionado);

            addMensajeInfo("Valor guardado correctamente");

        } catch (NumberFormatException ex) {
            addMensajeError("El área ingresada debe ser un numero.");
            LOGGER.error("error el valor ingresado no es valido." + ex);
            return avaluo;
        } catch (Exception e) {
            addMensajeError("Error guardando el avalúo");
            LOGGER.error("error en ProyectarConservacionMB#guardarAvaluoPredioFiscal : " + e);
            return avaluo;
        }

        return avaluo;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * Método necesario para redondear el avalúo TOTAL de los predios a la cifra mil mas cercana
     *
     * @param predioSeleccionado
     * @param avaluo
     * @return
     */
    public boolean redondeoAvaluosPrediosFiscales(PPredio predioSeleccionado,
        PPredioAvaluoCatastral avaluo) {

        predioSeleccionado.setAvaluoCatastral(trimDouble(predioSeleccionado.getAvaluoCatastral()));
        avaluo.setValorTotalAvaluoCatastral(trimDouble(avaluo.getValorTotalAvaluoCatastral()));

        return true;
    }

    /**
     * Funcion para redondear al valor mil mas cercano
     *
     * @param valor
     * @return
     */
    public double trimDouble(double valor) {

        Double roundedNumber = (double) Math.round(valor / 1000);
        roundedNumber = roundedNumber * 1000;

        return roundedNumber;
    }

}//fin clase
