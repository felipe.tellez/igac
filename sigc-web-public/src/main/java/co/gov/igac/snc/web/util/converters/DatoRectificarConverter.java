/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util.converters;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.util.Constantes;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converter para los objetos CalificacionAnexo para poder ser usados como valor en los items de los
 * combos
 *
 * @author pedro.garcia
 */
@FacesConverter(value = "datoRectificarConverter",
    forClass = co.gov.igac.snc.persistence.entity.tramite.DatoRectificar.class)
public class DatoRectificarConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatoRectificarConverter.class);

//--------------------------------------------------------------------------------------------------
    @Implement
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String objectAsString) {

        DatoRectificar answer;
        String[] objectAttributes;

        answer = new DatoRectificar();
        objectAttributes = objectAsString.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setId(Long.parseLong(objectAttributes[0]));
        answer.setTipo(objectAttributes[1]);
        answer.setNombre(objectAttributes[2]);
        answer.setTabla(objectAttributes[3]);
        answer.setColumna(objectAttributes[4]);
        answer.setNaturaleza(objectAttributes[5]);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    @Implement
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        DatoRectificar objDT = (DatoRectificar) o;
        String answer = objDT.toString();
        return answer;
    }

}
