package co.gov.igac.snc.web.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import co.gov.igac.snc.web.mb.generales.GeneralMB;

/**
 * Conversor para obtener de cualquier dominio a partir del Código su respectivo Valor
 *
 * @author fredy.wilches
 *
 */
//XXX::fredy.wilches::
public class ComboBoxConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ComboBoxConverter.class);

    @Override
    public Object getAsObject(FacesContext paramFacesContext,
        UIComponent paramUIComponent, String paramString) {

        return paramString;
    }

    @Override
    public String getAsString(FacesContext paramFacesContext,
        UIComponent paramUIComponent, Object paramObject) {
        try {
            String id = paramUIComponent.getId();
            if (id.indexOf('-') != -1) {
                id = id.substring(0, id.indexOf('-'));
            }
            String label = getDescripcion(id, paramObject);
            if (label == null) {
                return (String) paramObject;
            } else {
                return label;
            }
        } catch (SecurityException e) {
            LOGGER.error(e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return (String) paramObject;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @modified pedro.garcia no retornar sin asignar antes
     * @param id
     * @param paramObject
     * @return
     */
    public static String getDescripcion(String id, Object paramObject) {

        String answer = null;
        try {
            GeneralMB generalMB = (GeneralMB) UtilidadesWeb
                .getManagedBean("general");
            Class clazz = generalMB.getClass();
            Method m;
            m = clazz.getMethod("get" + id);
            List<SelectItem> lista = (ArrayList<SelectItem>) m
                .invoke(generalMB);

            for (SelectItem si : lista) {
                if (si.getValue().toString().equals(paramObject.toString())) {
                    answer = si.getLabel();
                    break;
                }
            }
        } catch (SecurityException e) {
            LOGGER.error(e.getMessage());
        } catch (NoSuchMethodException e) {
            LOGGER.error(e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Retorna el valor de un registro asociado a un dominio
     *
     * @author christian.rodriguez
     * @param id del combo que contiene los valores del dominio
     * @param paramObject valor del registro del dominio del que se quiere obtener el codigor
     * @return codigo de la descripcion asociada, null si no existe
     */
    public static String getCodigo(String id, Object paramObject) {

        String answer = null;
        try {
            GeneralMB generalMB = (GeneralMB) UtilidadesWeb
                .getManagedBean("general");
            Class clazz = generalMB.getClass();
            Method m;
            m = clazz.getMethod("get" + id);
            List<SelectItem> lista = (ArrayList<SelectItem>) m
                .invoke(generalMB);

            for (SelectItem si : lista) {
                if (si.getLabel().toString().equalsIgnoreCase(paramObject.toString())) {
                    answer = (String) si.getValue();
                    break;
                }
            }
        } catch (SecurityException e) {
            LOGGER.error(e.getMessage());
        } catch (NoSuchMethodException e) {
            LOGGER.error(e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return answer;

    }

}
