package co.gov.igac.snc.web.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Tarea que se encarga de reenviar un job geográfico que está esperando hace más de n horas. (Estas
 * tareas son manejadas por un pool de instancias asociados a una cola administrada por spring en la
 * capa web)
 *
 * @author Juan Carlos
 *
 */
public class ReenviarJobTask extends AbstractLocator implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = -7332773578521079455L;
    public static Logger LOGGER = LoggerFactory.getLogger(ReenviarJobTask.class);

    private ProductoCatastralJob job;

    /**
     *
     * @param job a ejecutar
     */
    public ReenviarJobTask(ProductoCatastralJob job) {
        this.job = job;
    }

    /**
     * Método encargado de enviar la ejecución del job pendiente
     */
    public void run() {
        LOGGER.debug("Begin	----- ");
        this.getGeneralesService().reenviarJobGeograficoEsperando(this.job.getId());
        LOGGER.debug("End 	----- ");
    }

}
