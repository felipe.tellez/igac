package co.gov.igac.snc.web.util;

import co.gov.igac.snc.util.EUnidadMedidaAreaTerreno;

/**
 * Clase creada para realizar conversiones entre unidades de area a hectareas y metros cuadrados
 *
 * @author christian.rodriguez
 *
 */
public class ConversorUnidadesArea {

    /**
     * Convierte un valor de una unidad origen a metro cuadrado
     *
     * @author christian.rodriguez
     * @param medida valor a convertir
     * @param unidadOrigen String con la unidad origen
     * @return valor convertido
     */
    public static Double convertirAMetroCuadrado(Double medida, String unidadOrigen) {

        if (medida != null && medida > 0) {
            if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.CUADRA.getCodigo())) {
                return convetirCuadraAMetroCuadrado(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.FANEGADA.getCodigo())) {
                return convetirFanegadaAMetroCuadrado(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.HECTAREA.getCodigo())) {
                return convetirHectareaAMetroCuadrado(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.KILOMETRO_CUADRADO
                .getCodigo())) {
                return convetirKilometroCuadradoAMetroCuadrado(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.PLAZA.getCodigo())) {
                return convetirPlazaAMetroCuadrado(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.METRO_CUADRADO
                .getCodigo())) {
                return medida;
            }
        }
        return 0.0;
    }

    /**
     * Convierte un valor de fanegadas a metros cuadrados
     *
     * @author christian.rodriguez
     * @param medida valor en fanegadas
     * @return valor convertido a metros cuadrados
     */
    public static Double convetirFanegadaAMetroCuadrado(Double fanegadas) {
        return fanegadas * 6440;
    }

    /**
     * Convierte un valor de cuadras a metros cuadrados
     *
     * @author christian.rodriguez
     * @param cuadras valor en cuadras
     * @return valor convertido a metros cuadrados
     */
    private static Double convetirCuadraAMetroCuadrado(Double cuadras) {
        return 0.0;
    }

    /**
     * Convierte un valor de hectareas a metros cuadrados
     *
     * @author christian.rodriguez
     * @param hectareas valor en hectareas
     * @return valor convertido a metros cuadrados
     */
    private static Double convetirHectareaAMetroCuadrado(Double hectareas) {
        return hectareas * 10000;
    }

    /**
     * Convierte un valor de kilometros cuadrados a metros cuadrados
     *
     * @author christian.rodriguez
     * @param kilometrosCuadrados valor en kilometros cuadrados
     * @return valor convertido a metros cuadrados
     */
    private static Double convetirKilometroCuadradoAMetroCuadrado(Double kilometrosCuadrados) {
        return kilometrosCuadrados * 1000000;
    }

    /**
     * Convierte un valor de plazas a metros cuadrados
     *
     * @author christian.rodriguez
     * @param plazas valor en plazas
     * @return valor convertido a metros cuadrados
     */
    private static Double convetirPlazaAMetroCuadrado(Double plazas) {
        return 0.0;
    }

    /**
     * Convierte un valor de una unidad origen a hectarea
     *
     * @author christian.rodriguez
     * @param medida valor a convertir
     * @param unidadOrigen String con la unidad origen
     * @return valor convertido
     */
    public static Double convertirAHectarea(Double medida, String unidadOrigen) {

        if (medida != null && medida > 0) {
            if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.CUADRA.getCodigo())) {
                return convetirCuadraAHectarea(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.FANEGADA.getCodigo())) {
                return convetirFanegadaAHectarea(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.KILOMETRO_CUADRADO
                .getCodigo())) {
                return convetirKilometroCuadradoAHectarea(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.METRO_CUADRADO
                .getCodigo())) {
                return convetirMetroCuadradoAHectarea(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.PLAZA.getCodigo())) {
                return convetirPlazaAHectarea(medida);
            } else if (unidadOrigen.equalsIgnoreCase(EUnidadMedidaAreaTerreno.HECTAREA.getCodigo())) {
                return medida;
            }
        }
        return 0.0;
    }

    /**
     * Convierte un valor de cuadras a hectareas
     *
     * @author christian.rodriguez
     * @param cuadras valor en cuadras
     * @return valor convertido a hectareas
     */
    private static Double convetirCuadraAHectarea(Double cuadras) {
        return 0.0;
    }

    /**
     * Convierte un valor de fanegada
     *
     * @author christian.rodriguez
     * @param medida valor en fanegadas
     * @return valor convertido a hectareas
     */
    public static Double convetirFanegadaAHectarea(Double fanegadas) {
        Double fanegadasEnMetrosCuadrados = (fanegadas / 6440);
        return convetirMetroCuadradoAHectarea(fanegadasEnMetrosCuadrados);
    }

    /**
     * Convierte un valor de kilometro cuadrado
     *
     * @author christian.rodriguez
     * @param kilometrosCuadrados valor en kilometros cuadrados
     * @return valor convertido a hectareas
     */
    private static Double convetirKilometroCuadradoAHectarea(Double kilometrosCuadrados) {
        Double kilometrosCuadradosEnMetrosCuadrados = (kilometrosCuadrados / 1000000);
        return convetirMetroCuadradoAHectarea(kilometrosCuadradosEnMetrosCuadrados);
    }

    /**
     * Convierte un valor de metro cuadrado a hectareas
     *
     * @author christian.rodriguez
     * @param metrosCuadrados valor en metros cuadrados
     * @return valor convertido a hectareas
     */
    private static Double convetirMetroCuadradoAHectarea(Double metrosCuadrados) {
        return metrosCuadrados / 10000;
    }

    /**
     * Convierte un valor de plazas a hectareas
     *
     * @author christian.rodriguez
     * @param plazas valor en plazas
     * @return valor convertido a hectareas
     */
    private static Double convetirPlazaAHectarea(Double plazas) {
        return 0.0;
    }

}
