/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Managed bean del CU-SA-AC-128 Consultar Avaluadores asignados
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-128
 */
@Component("consultarAvaluadoresAsignados")
@Scope("session")
public class ConsultaAvaluadoresAsignadosMB extends SNCManagedBean {

    /**
     * Serial ID generado
     */
    private static final long serialVersionUID = 297583064013776641L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaAvaluadoresAsignadosMB.class);

    /**
     * Sec. Radicado del avalúo que se están consultando lso avaluadores
     */
    private String secRadicado;

    /**
     * Lista con los avaluadores asociados al sec radicado
     */
    private List<ProfesionalAvaluo> avaluadores;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public String getSecRadicado() {
        return this.secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    public List<ProfesionalAvaluo> getAvaluadores() {
        return this.avaluadores;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    // ---------------------------------Métodos---------------------------------
    /**
     * Método que se llama desde otros MB para consultar los avaluadores asociados a un sec radicado
     * (avaluo {@link Avaluo})
     *
     * @author christian.rodriguez
     */
    public void cargarAvaluadoresAsignados() {

        if (this.secRadicado != null && !this.secRadicado.isEmpty()) {
            this.avaluadores = this.getAvaluosService()
                .consultarAvaluadoresPorSecRadicado(this.secRadicado);
        }
    }
}
