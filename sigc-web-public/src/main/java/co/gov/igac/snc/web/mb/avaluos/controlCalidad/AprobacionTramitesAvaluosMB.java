/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del cu Aprobación trámites de avalúos
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-129
 */
@Component("aprobacionTramitesAvaluos")
@Scope("session")
public class AprobacionTramitesAvaluosMB extends SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AprobacionTramitesAvaluosMB.class);

    /**
     * Contiene los avalúos por aprobar
     */
    private List<Avaluo> avaluosPorAprobar;

    /**
     * Contiene los avalúos seleccionados para aprobar
     */
    private Avaluo[] avaluosSeleccionados;

    /**
     * Id de los tramites seleccionados desde process
     */
    private List<Long> tramitesId;

    /**
     * Contiene el tipo de los tramites, corresponde al un valor de la enumeración
     * {@link ETramiteTipoTramite}
     */
    private String tipoTramites;

    /**
     * Contiene la cadena de texto con la forma presentable del tipo de trámite
     */
    private String tipoTramitesTitulo;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si se esta trabajando con tramites de
     * {@link ETramiteTipoTramite#SOLICITUD_DE_AVALUO}
     */
    private boolean banderaEsSolicitudDeAvaluo;

    /**
     * Bandera que indica si se esta trabajando con tramites de
     * {@link ETramiteTipoTramite#SOLICITUD_DE_CORRECCION}
     */
    private boolean banderaEsSolicitudDeCorreccion;

    /**
     * Bandera que indica si se esta trabajando con tramites de
     * {@link ETramiteTipoTramite#SOLICITUD_DE_COTIZACION}
     */
    private boolean banderaEsSolicitudDeCotizacion;

    /**
     * Bandera que indica si se esta trabajando con tramites de
     * {@link ETramiteTipoTramite#SOLICITUD_DE_IMPUGNACION}
     */
    private boolean banderaEsSolicitudDeImpugnacion;

    /**
     * Bandera que indica si se esta trabajando con tramites de
     * {@link ETramiteTipoTramite#SOLICITUD_DE_REVISION}
     */
    private boolean banderaEsSolicitudDeRevision;

    /**
     * Bandera que indica si se muestran o no todas las columnas de la tabla de avalúos
     */
    private boolean banderaMostrarTablaExpandida;

    // ----------------------------Métodos SET y GET----------------------------
    public Avaluo[] getAvaluosSeleccionados() {
        return this.avaluosSeleccionados;
    }

    public void setAvaluosSeleccionados(Avaluo[] avaluosSeleccionados) {
        this.avaluosSeleccionados = avaluosSeleccionados;
    }

    public List<Avaluo> getAvaluosPorAprobar() {
        return this.avaluosPorAprobar;
    }

    public String getTipoTramitesTitulo() {
        return this.tipoTramitesTitulo;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsSolicitudDeAvaluo() {
        return this.banderaEsSolicitudDeAvaluo;
    }

    public boolean isBanderaEsSolicitudDeCorreccion() {
        return this.banderaEsSolicitudDeCorreccion;
    }

    public boolean isBanderaEsSolicitudDeCotizacion() {
        return this.banderaEsSolicitudDeCotizacion;
    }

    public boolean isBanderaEsSolicitudDeImpugnacion() {
        return this.banderaEsSolicitudDeImpugnacion;
    }

    public boolean isBanderaEsSolicitudDeRevision() {
        return this.banderaEsSolicitudDeRevision;
    }

    public boolean isBanderaMostrarTablaExpandida() {
        return this.banderaMostrarTablaExpandida;
    }

    public void setBanderaMostrarTablaExpandida(
        boolean banderaMostrarTablaExpandida) {
        this.banderaMostrarTablaExpandida = banderaMostrarTablaExpandida;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        this.extraerDatosProcess();
        this.cargarAvaluosDeTramitesSeleccionados();
        this.definirSiSonTramitesDeRevisionConSubsidio();
        this.inicializarVariablesTipoTramite();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que se encarga de revisar si al tener tramites de tipo SOLICITUD_DE_REVISION estos
     * están marcados con subsidio de impugnacion. De ser así, se deben tratar los trámites como si
     * fueran impugnaciones
     *
     * @author christian.rodriguez
     */
    private void definirSiSonTramitesDeRevisionConSubsidio() {
        if (this.avaluosPorAprobar != null && !this.avaluosPorAprobar.isEmpty()) {

            Solicitud solicitud = this.avaluosPorAprobar.get(0).getTramite()
                .getSolicitud();

            if (tipoTramites.equals(ETramiteTipoTramite.SOLICITUD_DE_REVISION
                .toString()) &&
                 solicitud.getSubsidioImpugnacion().equals(
                    ESiNo.SI.getCodigo())) {

                this.tipoTramites = ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION
                    .toString();
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que en un futuro deberá leer los datos del arbol del proceso y extraer los ids de los
     * trámites a aprobar
     *
     * @author christian.rodriguez
     */
    private void extraerDatosProcess() {

        this.tramitesId = new ArrayList<Long>();

        this.inicializarDatosQuemados();
    }

    private void inicializarDatosQuemados() {

        this.tramitesId = new ArrayList<Long>();
        this.tramitesId.add(43086L);
        this.tramitesId.add(43267L);
        this.tramitesId.add(43268L);
        this.tramitesId.add(43286L);
        this.tramitesId.add(43318L);
        this.tramitesId.add(43322L);
        this.tramitesId.add(43327L);
        this.tramitesId.add(43439L);
        this.tramitesId.add(43470L);

        this.tipoTramites = ETramiteTipoTramite.SOLICITUD_DE_AVALUO.toString();
        // this.tipoTramites = ETramiteTipoTramite.SOLICITUD_DE_CORRECCION
        // .toString();
        // this.tipoTramites = ETramiteTipoTramite.SOLICITUD_DE_COTIZACION
        // .toString();
        // this.tipoTramites = ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION
        // .toString();
        // this.tipoTramites =
        // ETramiteTipoTramite.SOLICITUD_DE_REVISION.toString();

    }

    // -------------------------------------------------------------------------
    /**
     * Inicializa las banderas y strings que se usan para determinar las columans y acciones
     * disponibles en la interfaz dependiendo del tipo de trámite
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesTipoTramite() {

        this.banderaEsSolicitudDeAvaluo = false;
        this.banderaEsSolicitudDeCorreccion = false;
        this.banderaEsSolicitudDeCotizacion = false;
        this.banderaEsSolicitudDeImpugnacion = false;
        this.banderaEsSolicitudDeRevision = false;

        if (this.tipoTramites.equals(ETramiteTipoTramite.SOLICITUD_DE_AVALUO
            .toString())) {
            this.banderaEsSolicitudDeAvaluo = true;
            this.tipoTramitesTitulo = "Avalúos por aprobar";
        } else if (this.tipoTramites
            .equals(ETramiteTipoTramite.SOLICITUD_DE_CORRECCION.toString())) {
            this.banderaEsSolicitudDeCorreccion = true;
            this.tipoTramitesTitulo = "Correcciones por aprobar";
        } else if (this.tipoTramites
            .equals(ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.toString())) {
            this.banderaEsSolicitudDeCotizacion = true;
            this.tipoTramitesTitulo = "Cotizaciones por aprobar";
        } else if (this.tipoTramites
            .equals(ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION.toString())) {
            this.banderaEsSolicitudDeImpugnacion = true;
            this.tipoTramitesTitulo = "Impugnaciones por aprobar";
        } else if (this.tipoTramites
            .equals(ETramiteTipoTramite.SOLICITUD_DE_REVISION.toString())) {
            this.banderaEsSolicitudDeRevision = true;
            this.tipoTramitesTitulo = "Revisiones por aprobar";
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los avalúos asociados a los ids de los {@link #tramitesId}
     *
     * @author christian.rodriguez
     */
    private void cargarAvaluosDeTramitesSeleccionados() {

        if (this.tramitesId != null && !this.tramitesId.isEmpty()) {

            FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();

            filtro.setAprobado(ESiNo.NO.getCodigo());
            filtro.setTramitesId(this.tramitesId);

            this.avaluosPorAprobar = this.getAvaluosService()
                .buscarAvaluosPorFiltro(filtro);

        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón aprobar trámites.
     *
     * @author christian.rodriguez
     */
    public void aprobarTramites() {

        if (this.avaluosPorAprobar != null && !this.avaluosPorAprobar.isEmpty()) {

            this.definirAtributoReservadoRevisionImpugnacion();

            boolean aprobacionExitosa = this.getAvaluosService()
                .aprobarAvaluos(Arrays.asList(this.avaluosSeleccionados));

            if (aprobacionExitosa) {
                this.addMensajeInfo("Avalúos aprobados satisfactóriamente.");
                this.cargarAvaluosDeTramitesSeleccionados();
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("No se pudo aprobar los avalúos seleccionados.");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que revisa que si los avalúos que se están aprobando son de tipo revision o
     * impugnación el campo reservado debe tener el mismo valor que el avalúo que se está impugnando
     * o revisado
     *
     * @author christian.rodriguez
     */
    private void definirAtributoReservadoRevisionImpugnacion() {

        if (this.tipoTramites.equals(ETramiteTipoTramite.SOLICITUD_DE_REVISION
            .toString()) ||
             this.tipoTramites
                .equals(ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION
                    .toString())) {

            Avaluo avaluoAImpugnarRevisar = null;

            for (Avaluo avaluoAAprobar : this.avaluosPorAprobar) {

                avaluoAImpugnarRevisar = this.getAvaluosService()
                    .obtenerAvaluoPorIdParaImpugnacionRevision(
                        avaluoAAprobar.getId());

                if (avaluoAImpugnarRevisar != null) {
                    avaluoAAprobar.setReservado(avaluoAImpugnarRevisar.getReservado());
                }

            }

        }

    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

}
