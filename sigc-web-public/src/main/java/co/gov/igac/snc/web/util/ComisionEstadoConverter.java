package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EProcesoConservacionNombreActividad;

/**
 * Converter para reemplazar el estado de la comisión por el nombre para mostrar al usuario.
 *
 * @author javier.aponte
 *
 */
public class ComisionEstadoConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComisionEstadoConverter.class);

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorString) {

        try {
            return valorString;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " + valorString);
            return valorString;
        }
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorObjeto) {

        String nombreEstadoComision = null;
        for (@SuppressWarnings("rawtypes") Enum e : EComisionEstado.values()) {
            if (valorObjeto.equals(((EComisionEstado) e).getCodigo())) {
                nombreEstadoComision = ((EComisionEstado) e).getValor();
                break;
            }
        }
        try {
            return nombreEstadoComision;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " + valorObjeto);
            return (String) valorObjeto;
        }
    }
}
