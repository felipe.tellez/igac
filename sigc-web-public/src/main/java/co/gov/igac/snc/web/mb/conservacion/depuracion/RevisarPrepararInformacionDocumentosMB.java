/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import co.gov.igac.generales.dto.ArcgisServerTokenDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.conservacion.asignacion.ManejoComisionesTramitesMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso Revisar y preparar información y documentos
 *
 * @author juanfelipe.garcia
 * @CU CU-NP-CO-208
 */
@Component("revisarPrepararInfDoc")
@Scope("session")
public class RevisarPrepararInformacionDocumentosMB extends ProcessFlowManager {

    /**
     * Serial
     */
    private static final long serialVersionUID = 1258963L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ManejoComisionesTramitesMB.class);
    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Trámites seleccionados de la tabla de trámites de revisar y preparar información y documentos
     */
    private Tramite[] selectedTramites;
    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    /**
     * Tramite relacionado a la actividad actual
     */
    private Tramite tramiteSeleccionado;

    /**
     * Predios relacionados al tramite actual
     */
    private List<Predio> prediosTramite;

    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;

    /**
     * Solicitud seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * variable que almacena las inconsistencias geográficas
     */
    private String inconsistenciasGeograficas;

    /**
     * Lista que contiene la información de la documentacion requerida
     */
    private List<TramiteDocumentacion> documentacionRequerida;

    /**
     * Variable de tipo streamed content que contiene los documentos asociados al trámite
     */
    private StreamedContent documentosAsociadosATramite;

    /**
     * Variable que almacena los TramiteDocumentacion seleccionados
     */
    private TramiteDocumentacion[] documentacionRequeridaSeleccionada;

    /**
     * Comision asociada al Tramite seleccionado
     */
    private Comision comisionTramiteSeleccionado;

    /**
     * Predio seleccionado de una lista de predios asociados al tramite
     */
    private Predio predioSeleccionado;

    /**
     * Documento asociado a la ultima comision del tramite
     */
    private Documento documentoComisionTramiteSeleccionado;

    /**
     * Documento generado de la ficha predial digital
     */
    private Documento fichaPredialDigital;

    /**
     * Documento generado de la carta catastral
     */
    private Documento cartaCatastralUrbana;

    /**
     * reporte necesario para mostrar la ficha predial
     */
    private ReporteDTO reporteFichaPredialDigital;

    /**
     * reporte necesario para mostrar la carta catastral
     */
    private ReporteDTO reporteCartaCatastralUrbana;

    /**
     * booleano para habilitar el boton exportar
     */
    private boolean habilitarBotonExportar;

    /**
     * booleano para habilitar el boton descargar
     */
    private boolean habilitarBotonDescargar;

    /**
     * booleano para habilitar avanzar preceso
     */
    private boolean habilitarBotonAvanzarProceso;

    /**
     * Lista de {@link TramiteInconsistencia} con las inconsistencias modificadas del
     * {@link Tramite}
     */
    private List<TramiteInconsistencia> inconsistenciasTramite;

    /**
     * Objeto que contiene los datos del reporte documento comisión
     */
    private ReporteDTO reporteComision;

    private String rol;

    /**
     * @return the selectedTramites
     */
    public Tramite[] getSelectedTramites() {
        return selectedTramites;
    }

    /**
     * @param selectedTramites the selectedTramites to set
     */
    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    /**
     * @return the currentUser
     */
    public UsuarioDTO getCurrentUser() {
        return currentUser;
    }

    /**
     * @param currentUser the currentUser to set
     */
    public void setCurrentUser(UsuarioDTO currentUser) {
        this.currentUser = currentUser;
    }

    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public Solicitud getSolicitudSeleccionada() {
        return this.solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;

        if (this.solicitudSeleccionada != null && this.solicitudSeleccionada.getId() != null) {
            this.solicitudSeleccionada = this.getTramiteService()
                .buscarSolicitudFetchTramitesBySolicitudId(this.solicitudSeleccionada.getId());
        }
    }

    public Documento getDocumentoComisionTramiteSeleccionado() {
        return documentoComisionTramiteSeleccionado;
    }

    public void setDocumentoComisionTramiteSeleccionado(
        Documento documentoComisionTramiteSeleccionado) {
        this.documentoComisionTramiteSeleccionado = documentoComisionTramiteSeleccionado;
    }

    public Comision getComisionTramiteSeleccionado() {
        return comisionTramiteSeleccionado;
    }

    public void setComisionTramiteSeleccionado(Comision comisionTramiteSeleccionado) {
        this.comisionTramiteSeleccionado = comisionTramiteSeleccionado;
    }

    public String getInconsistenciasGeograficas() {
        return inconsistenciasGeograficas;
    }

    public void setInconsistenciasGeograficas(String inconsistenciasGeograficas) {
        this.inconsistenciasGeograficas = inconsistenciasGeograficas;
    }

    public List<TramiteDocumentacion> getDocumentacionRequerida() {
        return documentacionRequerida;
    }

    public void setDocumentacionRequerida(List<TramiteDocumentacion> documentacionRequerida) {
        this.documentacionRequerida = documentacionRequerida;
    }

    public List<Predio> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<Predio> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public TramiteDocumentacion[] getDocumentacionRequeridaSeleccionada() {
        return documentacionRequeridaSeleccionada;
    }

    public void setDocumentacionRequeridaSeleccionada(
        TramiteDocumentacion[] documentacionRequeridaSeleccionada) {
        this.documentacionRequeridaSeleccionada = documentacionRequeridaSeleccionada;
    }

    public Documento getFichaPredialDigital() {
        return fichaPredialDigital;
    }

    public void setFichaPredialDigital(Documento fichaPredialDigital) {
        this.fichaPredialDigital = fichaPredialDigital;
    }

    public ReporteDTO getReporteFichaPredialDigital() {
        return reporteFichaPredialDigital;
    }

    public void setReporteFichaPredialDigital(ReporteDTO reporteFichaPredialDigital) {
        this.reporteFichaPredialDigital = reporteFichaPredialDigital;
    }

    public Documento getCartaCatastralUrbana() {
        return cartaCatastralUrbana;
    }

    public void setCartaCatastralUrbana(Documento cartaCatastralUrbana) {
        this.cartaCatastralUrbana = cartaCatastralUrbana;
    }

    public Predio getPredioSeleccionado() {
        return predioSeleccionado;
    }

    public void setPredioSeleccionado(Predio predioSeleccionado) {
        this.predioSeleccionado = predioSeleccionado;
    }

    public ReporteDTO getReporteCartaCatastralUrbana() {
        return reporteCartaCatastralUrbana;
    }

    public void setReporteCartaCatastralUrbana(ReporteDTO reporteCartaCatastralUrbana) {
        this.reporteCartaCatastralUrbana = reporteCartaCatastralUrbana;
    }

    public StreamedContent getDocumentosAsociadosATramite() {
        return documentosAsociadosATramite;
    }

    public void setDocumentosAsociadosATramite(StreamedContent documentosAsociadosATramite) {
        this.documentosAsociadosATramite = documentosAsociadosATramite;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public boolean isHabilitarBotonExportar() {
        return habilitarBotonExportar;
    }

    public void setHabilitarBotonExportar(boolean habilitarBotonExportar) {
        this.habilitarBotonExportar = habilitarBotonExportar;
    }

    public boolean isHabilitarBotonDescargar() {
        return habilitarBotonDescargar;
    }

    public void setHabilitarBotonDescargar(boolean habilitarBotonDescargar) {
        //this.habilitarBotonDescargar = habilitarBotonDescargar;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public ReporteDTO getReporteComision() {
        return reporteComision;
    }

    public void setReporteComision(ReporteDTO reporteComision) {
        this.reporteComision = reporteComision;
    }

    /**
     * Constructor
     */
    public RevisarPrepararInformacionDocumentosMB() {
    }

    @PostConstruct
    public void init() {

        LOGGER.debug("on RevisarPrepararInformacionDocumentosMB#init ");
        this.documentacionRequerida = new ArrayList<TramiteDocumentacion>();
        this.setCurrentUser(MenuMB.getMenu().getUsuarioDto());
        this.habilitarBotonDescargar = false;
        this.habilitarBotonExportar = false;

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "asignarDigitalizadorTopografo");

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        Long idtramiteActual = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();
        this.tramiteSeleccionado = new Tramite();
        this.tramiteSeleccionado.setId(idtramiteActual);

        //obtener datos de la actividad
        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.tramiteSeleccionado.getId());

        //obtener la informacion del tramite
        this.cargarDatosTramite();

        //obtener la documentacion de la ultima comision
        for (ComisionTramite ct : this.tramiteSeleccionado.getComisionTramites()) {
            //javier.aponte se cambia de CANCELADA.getValor() a CANCELADA.toString() porque en la base de datos
            //el estado de la comisión se está guardando como 'CANCELADA' y no como 'Cancelada' luego esta condición
            //nunca se estaba cumpliendo
            if (ct.getComision().getEstado() != null &&
                 !ct.getComision().getEstado().equals(EComisionEstado.CANCELADA.toString()) &&
                 !ct.getComision().getEstado().equals(EComisionEstado.FINALIZADA.toString())) {
                this.comisionTramiteSeleccionado = ct.getComision();
                this.documentoComisionTramiteSeleccionado = this.getTramiteService().
                    buscarDocumentoPorId(ct.getComision().getMemorandoDocumentoId());
                this.reporteComision = this.reportsService.consultarReporteDeGestorDocumental(
                    this.documentoComisionTramiteSeleccionado.getIdRepositorioDocumentos());

            }
        }

        //obtener la documentacion requerida del tramite
        Tramite tempTramite = this.getTramiteService().consultarDocumentacionDeTramite(
            this.tramiteSeleccionado.getId());
        if (tempTramite.getTramiteDocumentacions() != null &&
             !tempTramite.getTramiteDocumentacions().isEmpty()) {

            for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                if (td.getRequerido() != null &&
                     td.getRequerido().equals(ESiNo.SI.toString())) {
                    this.documentacionRequerida.add(td);
                }
            }

        }
        this.habilitarBotonAvanzarProceso = false;
        //validación para mostrar el botón avanzar proceso
        if (this.comisionTramiteSeleccionado.getEstado().equals(EComisionEstado.EN_EJECUCION.
            getCodigo()) ||
             this.comisionTramiteSeleccionado.getEstado().equals(EComisionEstado.FINALIZADA.
                getCodigo())) {

            this.habilitarBotonAvanzarProceso = true;
        }

        this.determinarRol();

    }

    /**
     * Método para cargar los datos relacionados al tramite que se deben mostrar.
     */
    private void cargarDatosTramite() {
        LOGGER.debug("Cargando datos tramite...");

        this.tramiteSeleccionado = this.getTramiteService().
            buscarTramiteTraerDocumentoResolucionPorIdTramite(this.tramiteSeleccionado.getId());

        this.prediosTramite = this.tramiteSeleccionado.getPredios();

        // Set de las inconsistencias geográficas del trámite.
        this.inconsistenciasTramite = new ArrayList<TramiteInconsistencia>();
        for (TramiteInconsistencia ti : this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramiteSeleccionado.getId())) {
            if (!ti.getDepurada().equals("CC")) {
                this.inconsistenciasTramite.add(ti);
            }
        }

        this.parametroEditorGeografico = this.currentUser.getLogin() + "," +
            this.tramiteSeleccionado.getId() +
            "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class,
                this.currentProcessActivity.getNombre());

        LOGGER.debug("Fin carga datos...");
    }

    /**
     * Método para habilitar el boton de la descarga
     */
    public void habilitarDescarga() {

        if (this.documentacionRequeridaSeleccionada != null &&
            (this.documentacionRequeridaSeleccionada.length > 0 ||
            seleccionoDocumentacionPredio())) {
            this.habilitarBotonExportar = true;
        } else if (seleccionoDocumentacionPredio()) {
            this.habilitarBotonExportar = true;
        } else {
            this.habilitarBotonExportar = false;
        }

    }

    /**
     * Método para determinar si selecciono un documento de los predios
     */
    private boolean seleccionoDocumentacionPredio() {
        boolean respuesta = false;
        for (Predio p : this.prediosTramite) {
            if (p.isImprimirCartaCatastral() ||
                p.isDescargarCapasGeograficas() ||
                p.isImprimirFichaPredial()) {
                respuesta = true;
            }
        }
        return respuesta;
    }

    /**
     * Método para generar la ficha predial digital
     *
     * @author javier.aponte
     */
    public void generarFichaPredialDigital() {

        try {

            this.fichaPredialDigital = this.getConservacionService().
                buscarDocumentoPorPredioIdAndTipoDocumento(this.predioSeleccionado.getId(),
                    ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());

            if (this.fichaPredialDigital != null) {

                this.reporteFichaPredialDigital = this.reportsService.
                    consultarReporteDeGestorDocumental(
                        this.fichaPredialDigital.getIdRepositorioDocumentos());

            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeWarn("No existe ficha predial digital");
                return;
            }

        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage());
        }

    }

    /**
     * Método para generar la carta catastral urbana
     *
     * @author javier.aponte
     */
    public void generarCartaCatastralUrbana() {

        try {

            this.cartaCatastralUrbana = this.getConservacionService().
                buscarDocumentoPorNumeroPredialAndTipoDocumento(this.predioSeleccionado.
                    getNumeroPredial().substring(0, 17),
                    ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

            if (this.cartaCatastralUrbana != null) {

                this.reporteCartaCatastralUrbana = this.reportsService.
                    consultarReporteDeGestorDocumental(
                        this.cartaCatastralUrbana.getIdRepositorioDocumentos());
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeWarn("No existe carta catastral urbana");
                return;
            }
        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage());
        }

    }

    /**
     * Metodo para validar si el rol corresponde a un topografo
     *
     * @return
     */
    public boolean isRolTopografo() {
        boolean permitido = false;
        for (String rol : this.currentUser.getRoles()) {
            if (rol.equalsIgnoreCase(ERol.TOPOGRAFO.getRol())) {
                permitido = true;
            }
        }
        return permitido;
    }

    /**
     * Metodo encargado de realizar la descarga de los documentos seleccionados de alfresco, generar
     * un comprimido y publicarlo en la carpeta tempa
     */
    public void descargarDocumentos() {

        this.habilitarBotonDescargar = false;
        List<String> rutaDocumentosADescargar = new ArrayList<String>();
        boolean shapeDescargado = false;
        ReporteDTO documentoDTO;

        for (TramiteDocumentacion tramDocumentacion : this.documentacionRequeridaSeleccionada) {

            if (tramDocumentacion.getDocumentoSoporte() != null &&
                 tramDocumentacion.getDocumentoSoporte().getIdRepositorioDocumentos() != null &&
                 !tramDocumentacion.getDocumentoSoporte().getIdRepositorioDocumentos().trim().
                    isEmpty()) {
                documentoDTO = this.reportsService.
                    consultarReporteDeGestorDocumental(tramDocumentacion.getDocumentoSoporte().
                        getIdRepositorioDocumentos());
                if (documentoDTO != null) {
                    rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                }

            }

        }

        for (Predio predioTemp : this.prediosTramite) {

            if (predioTemp.isImprimirFichaPredial()) {

                this.fichaPredialDigital = this.getConservacionService().
                    buscarDocumentoPorPredioIdAndTipoDocumento(predioTemp.getId(),
                        ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());

                if (this.fichaPredialDigital != null) {
                    documentoDTO =
                        this.reportsService.consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(
                            this.fichaPredialDigital.getIdRepositorioDocumentos());

                    if (documentoDTO != null) {
                        rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                    }

                } else {
                    this.addMensajeWarn(
                        "No existe la ficha predial para el predio con número predial " +
                        predioTemp.getNumeroPredial());
                }

            }
            if (predioTemp.isImprimirCartaCatastral()) {

                this.cartaCatastralUrbana = this.getConservacionService().
                    buscarDocumentoPorNumeroPredialAndTipoDocumento(predioTemp.getNumeroPredial().
                        substring(0, 17),
                        ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());

                if (this.cartaCatastralUrbana != null) {
                    documentoDTO =
                        this.reportsService.consultarReporteDeGestorDocumental(
                            this.cartaCatastralUrbana.getIdRepositorioDocumentos());

                    if (documentoDTO != null) {
                        rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                    }

                } else {
                    this.addMensajeWarn(
                        "No existe la carta catastral para el predio con número predial " +
                        predioTemp.getNumeroPredial());
                }
            }
            if (predioTemp.isDescargarCapasGeograficas() && !shapeDescargado) {
                String prediosParametro = null;

                for (Predio p : this.getPrediosTramite()) {
                    if (prediosParametro == null) {
                        prediosParametro = p.getNumeroPredial();
                    } else {
                        prediosParametro += "," + p.getNumeroPredial();
                    }
                }

                String ruta = this.getGeneralesService().obtenerRutaDatosGeograficosTramiteAsShape(
                    this.tramiteSeleccionado.getId().toString(), prediosParametro,
                    this.tramiteSeleccionado.getClaseMutacion(), currentUser);
                documentoDTO = null;

                if (ruta != null) {
                    documentoDTO = this.reportsService.consultarReporteDeGestorDocumental(ruta);
                    shapeDescargado = true;
                }

                if (documentoDTO != null) {
                    rutaDocumentosADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                }
            }

        }

        if (!rutaDocumentosADescargar.isEmpty()) {

            String rutaZipDocumentos = this.getGeneralesService().crearZipAPartirDeRutaDeDocumentos(
                rutaDocumentosADescargar);

            if (rutaZipDocumentos != null) {
                InputStream stream;
                File archivoDocumentosAsociadosATramite = new File(rutaZipDocumentos);
                if (!archivoDocumentosAsociadosATramite.exists()) {
                    LOGGER.warn("Error el archivo de origen no existe");
                    this.addMensajeError("Ocurrió un error al descargar los archivos");
                    return;
                }

                try {
                    stream = new FileInputStream(archivoDocumentosAsociadosATramite);
                    this.documentosAsociadosATramite = new DefaultStreamedContent(stream,
                        Constantes.TIPO_MIME_ZIP, "Documentos_tramite.zip");
                    this.habilitarBotonDescargar = true;
                } catch (FileNotFoundException e) {
                    LOGGER.error(
                        "Archivo no encontrado, no se puede descargar los documentos asociados al trámite " +
                        e.getMessage());
                }
            }

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeWarn(
                "No existen los documentos que se seleccionaron para descargar, por favor desseleccione e intente con otros archivos");
            return;
        }

    }

    /**
     * Método para determinar el tipo de rol de la actividad y en base a eso determinar el tipo de
     * funcionario que se va a asignar.
     *
     * @author felipe.cadena
     */
    public void determinarRol() {

        LOGGER.debug("on RevisarPrepararInformacionDocumentosMB#determinarRol ");

        String[] roles = this.currentUser.getRoles();
        this.rol = null;

        for (String rolUser : roles) {
            if (rolUser.equalsIgnoreCase(ERol.EJECUTOR_TRAMITE.getRol())) {

                this.rol = ERol.EJECUTOR_TRAMITE.getRol();
                break;
            }
            if (rolUser.equalsIgnoreCase(ERol.TOPOGRAFO.getRol())) {
                this.rol = ERol.TOPOGRAFO.getRol();
                break;
            }
        }

    }

    /**
     * Metodo encargado de comenzar el avance del proceso
     *
     * @return
     */
    public String confirmAction() {

        this.forwardThisProcess();

        UtilidadesWeb.removerManagedBean("revisarPrepararInfDoc");
        this.tareasPendientesMB.init();
        return "index";
    }

    private void forwardThisProcess() {

        if (this.rol == null) {
            this.addMensajeError(
                "Los roles del usuario no corresponden a roles del sistema para esta actividad.");
            return;
        }

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();
        }
        super.forwardProcess();
    }

    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }

    @Implement
    @Override
    public void setupProcessMessage() {
        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();

        usuariosActividad = new ArrayList<UsuarioDTO>();

        messageDTO = new ActivityMessageDTO();
        messageDTO.setUsuarioActual(this.currentUser);
        messageDTO.setActivityId(this.currentProcessActivity.getId());

        if (this.rol.equals(ERol.EJECUTOR_TRAMITE.getRol())) {
            processTransition = ProcesoDeConservacion.ACT_DEPURACION_REGISTRAR_VISITA_TERRENO;
            observaciones = "";

            usuarioDestinoActividad.copiarEstructuraOrganizacional(this.currentUser);
            usuarioDestinoActividad.setLogin(this.currentUser.getLogin());
            usuariosActividad.add(usuarioDestinoActividad);

        } else if (this.rol.equals(ERol.TOPOGRAFO.getRol())) {
            processTransition = ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_INFORME_VISITA;
            observaciones = "";

            usuarioDestinoActividad.copiarEstructuraOrganizacional(this.currentUser);
            usuarioDestinoActividad.setLogin(this.currentUser.getLogin());
            usuariosActividad.add(usuarioDestinoActividad);
        }

        messageDTO.setComment(observaciones);
        //D: según alejandro.sanchez la transición que importa es la que se define en la
        //   solicitud catastral; sin embargo, por si acaso se hace también en el messageDTO
        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setTerritorial(this.currentUser.getDescripcionEstructuraOrganizacional());
        solicitudCatastral.setUsuarios(usuariosActividad);
        messageDTO.setSolicitudCatastral(solicitudCatastral);

        Calendar cal = Calendar.getInstance();
        cal.setTime(this.tramiteSeleccionado.getFechaRadicacion());
        messageDTO.getSolicitudCatastral().setNumeroRadicacion(this.tramiteSeleccionado.
            getNumeroRadicacion());
        messageDTO.getSolicitudCatastral().setFechaRadicacion(cal);
        messageDTO.getSolicitudCatastral().setTipoTramite(this.tramiteSeleccionado.
            getTipoTramiteCadenaCompleto());

        this.setMessage(messageDTO);
    }

    @Implement
    @Override
    public void doDatabaseStatesUpdate() {
        this.getTramiteService().actualizarTramite(this.tramiteSeleccionado, this.currentUser);
    }

    //--------------------------------------------------------//
    /**
     * Método ejecutado al dar click en el botón cerrar, redirije al arbol de procesos.
     *
     * @modified by leidy.gonzalez 28/07/2014 Se agrega la nomenclatura "this" para que obtenga el
     * objeto de la clase.
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("revisarPrepararInfDoc");

        this.tareasPendientesMB.init();

        return "index";
    }

    public boolean isHabilitarBotonAvanzarProceso() {
        return habilitarBotonAvanzarProceso;
    }

    public void setHabilitarBotonAvanzarProceso(boolean habilitarBotonAvanzarProceso) {
        this.habilitarBotonAvanzarProceso = habilitarBotonAvanzarProceso;
    }

}
