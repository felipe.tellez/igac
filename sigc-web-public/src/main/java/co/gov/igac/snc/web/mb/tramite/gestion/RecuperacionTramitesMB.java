package co.gov.igac.snc.web.mb.tramite.gestion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTarea;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EProcesosAsincronicos;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETramiteTareaResultado;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import static com.sun.faces.facelets.util.Path.context;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import oracle.sql.ARRAY;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Clase asociada a la recuperacion de tramites que fallaron en la ejecución de procesos complejos.
 *
 * @cu CU-NP-CO-217
 * @author felipe.cadena
 */
@Component("recuperarTramites")
@Scope("session")
public class RecuperacionTramitesMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 7576908157594023347L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RecuperacionTramitesMB.class);

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Filtro para los datos de la consulta.
     */
    private FiltroDatosConsultaTramite datosConsultaTramite;

    /**
     * Objeto usado como contenedor de los datos de un solicitante para su consulta.
     */
    private FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private NumeroPredialPartes numeroPredialPartes;

    /**
     * Variable que almacena el login del funcionario ejecutor seleccionado.
     */
    private String funcionarioEjecutor;

    /**
     * Lista que contiene los funcionarios ejecutores que se encuentren asociados, a la territorial
     * del usuario autenticado
     */
    private List<SelectItem> ejecutoresSelectItem;

    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO responsableSIG;

    /**
     * Variable item list con los valores del tipo de actividad
     */
    private List<SelectItem> tipoActividadItemList;

    /**
     * Actividad seleccionada para la consulta
     */
    private String actividadSeleccionada;

    /**
     * Valor asociado al radio de seleccion de tipo de persona
     */
    private int tipoPersona;

    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean tipoPersonaN;

    /**
     * Bandera de seleccion de solicitante para la busqueda de solicitudes
     */
    private boolean banderaSolicitanteSolicitudSeleccionado;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites
     */
    private List<Tramite> tramitesResult;

    /*
     * Lista de tramits seleccionados
     */
    private Tramite[] tramitesSeleccionados;

    private Tramite tramiteTest;

    private Integer tareasProcesadas;
    private Integer tareasPorProcesar;

    private Integer progress;

    private List<ProductoCatastralJob> jobs;

    // --------------------GETTERS AND SETTERS--------------------//
    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public Integer getTareasProcesadas() {
        return tareasProcesadas;
    }

    public void setTareasProcesadas(Integer tareasProcesadas) {
        this.tareasProcesadas = tareasProcesadas;
    }

    public Integer getTareasPorProcesar() {
        return tareasPorProcesar;
    }

    public void setTareasPorProcesar(Integer tareasPorProcesar) {
        this.tareasPorProcesar = tareasPorProcesar;
    }

    public Tramite[] getTramitesSeleccionados() {
        return tramitesSeleccionados;
    }

    public void setTramitesSeleccionados(Tramite[] tramitesSeleccionados) {
        this.tramitesSeleccionados = tramitesSeleccionados;
    }

    public Tramite getTramiteTest() {
        return tramiteTest;
    }

    public void setTramiteTest(Tramite tramiteTest) {
        this.tramiteTest = tramiteTest;
    }

    public FiltroDatosConsultaTramite getDatosConsultaTramite() {
        return datosConsultaTramite;
    }

    public void setDatosConsultaTramite(FiltroDatosConsultaTramite datosConsultaTramite) {
        this.datosConsultaTramite = datosConsultaTramite;
    }

    public FiltroDatosConsultaSolicitante getFiltroDatosConsultaSolicitante() {
        return filtroDatosConsultaSolicitante;
    }

    public void setFiltroDatosConsultaSolicitante(
        FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante) {
        this.filtroDatosConsultaSolicitante = filtroDatosConsultaSolicitante;
    }

    public String getFuncionarioEjecutor() {
        return funcionarioEjecutor;
    }

    public void setFuncionarioEjecutor(String funcionarioEjecutor) {
        this.funcionarioEjecutor = funcionarioEjecutor;
    }

    public List<SelectItem> getEjecutoresSelectItem() {
        return ejecutoresSelectItem;
    }

    public void setEjecutoresSelectItem(List<SelectItem> ejecutoresSelectItem) {
        this.ejecutoresSelectItem = ejecutoresSelectItem;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public UsuarioDTO getResponsableSIG() {
        return responsableSIG;
    }

    public void setResponsableSIG(UsuarioDTO responsableSIG) {
        this.responsableSIG = responsableSIG;
    }

    public List<SelectItem> getTipoActividadItemList() {
        return tipoActividadItemList;
    }

    public void setTipoActividadItemList(List<SelectItem> tipoActividadItemList) {
        this.tipoActividadItemList = tipoActividadItemList;
    }

    public String getActividadSeleccionada() {
        return actividadSeleccionada;
    }

    public void setActividadSeleccionada(String actividadSeleccionada) {
        this.actividadSeleccionada = actividadSeleccionada;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public boolean isTipoPersonaN() {
        return tipoPersonaN;
    }

    public void setTipoPersonaN(boolean tipoPersonaN) {
        this.tipoPersonaN = tipoPersonaN;
    }

    public boolean isBanderaSolicitanteSolicitudSeleccionado() {
        return banderaSolicitanteSolicitudSeleccionado;
    }

    public void setBanderaSolicitanteSolicitudSeleccionado(
        boolean banderaSolicitanteSolicitudSeleccionado) {
        this.banderaSolicitanteSolicitudSeleccionado = banderaSolicitanteSolicitudSeleccionado;
    }

    public List<Tramite> getTramitesResult() {
        return tramitesResult;
    }

    public void setTramitesResult(List<Tramite> tramitesResult) {
        this.tramitesResult = tramitesResult;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("deprecation")
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del RecuperacionTramitesMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.tramiteTest = new Tramite();

        this.datosConsultaTramite = new FiltroDatosConsultaTramite();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();

        this.cargarFuncionariosEjecutores();
        this.cargarActividades();

        //this.jobs = this.getConservacionService().obtenerJobsErrorAGS();
        this.jobs = new ArrayList<ProductoCatastralJob>();
        this.tareasPorProcesar = this.jobs.size();
        this.tareasProcesadas = -1;
        this.progress = 0;

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        if (usuario.getDescripcionUOC() != null) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionUOC(),
                ERol.RESPONSABLE_SIG);;
        }

        //si no existe Responsable SIG en la UOC se buscan en la territorial.
        if (usuarios.isEmpty()) {
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_SIG);
        }
        if (!usuarios.isEmpty()) {
            this.responsableSIG = usuarios.get(0);
        }

        //actualizar estado tramites geograficos
        //this.actualizarTramitesEnAGSError();
    }

    /**
     * Metodo para inicializar las posibles actividades donde estan los tramites.
     *
     * @author felipe.cadena
     */
    public void cargarActividades() {

        this.tipoActividadItemList = new LinkedList<SelectItem>();

        EProcesosAsincronicos[] epas = EProcesosAsincronicos.values();
        for (EProcesosAsincronicos epa : epas) {
            if (!epa.toString().contains("ERROR")) {
                SelectItem si = new SelectItem(epa.toString(), epa.toString());
                this.tipoActividadItemList.add(si);
            }

        }

    }

    /**
     * Método encargado de cargar los funcionarios ejecutores en una lista de de SelecItem asociados
     * a la territorial del usuario en sesion
     */
    public void cargarFuncionariosEjecutores() {

        this.ejecutoresSelectItem = new ArrayList<SelectItem>();

        List<UsuarioDTO> ejecutores;
        ejecutores = this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario
                    .getDescripcionEstructuraOrganizacional(),
                ERol.EJECUTOR_TRAMITE);

        if (ejecutores != null && !ejecutores.isEmpty()) {
            for (UsuarioDTO ejecutor : ejecutores) {
                this.ejecutoresSelectItem.add(new SelectItem(ejecutor
                    .getLogin(), ejecutor.getNombreCompleto()));
            }
        }

    }

    // ----------------------------------------------------- //
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de persona
     */
    public void tipoPersonaSel() {
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        if (this.tipoPersona == 0) {
            this.tipoPersonaN = true;
            this.banderaSolicitanteSolicitudSeleccionado = false;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo());
        } else if (this.tipoPersona == 1) {
            this.tipoPersonaN = false;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT
                    .getCodigo());
            this.banderaSolicitanteSolicitudSeleccionado = false;
        }
    }

    /**
     * Metodo para buscar los tramites para recuperar
     *
     *
     * @author felipe.cadena
     */
    public void buscar() {
        this.datosConsultaTramite.setTarea(this.actividadSeleccionada);
        this.datosConsultaTramite.setResultado("%" + ETramiteTareaResultado.ERROR.toString());
        this.datosConsultaTramite.setFuncionarioEjecutor(this.funcionarioEjecutor);
        this.inicializarNumeroPredial();
        this.datosConsultaTramite.setNumeroPredialPartes(this.numeroPredialPartes);

        this.tramitesResult = this.getTramiteService().buscarTramitesParaRecuperacion(
            this.datosConsultaTramite, this.filtroDatosConsultaSolicitante);

        if (this.tramitesResult == null) {
            this.tramitesResult = new ArrayList<Tramite>();
            this.addMensajeError("Error al consultar los tramites para la recuperación");
        }

    }

    /**
     * Prepara el numero predial del predio para la consulta basado en los parametros del usuario.
     *
     * @author felipe.cadena
     */
    public void inicializarNumeroPredial() {

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartes.
            setDepartamentoCodigo(this.datosConsultaPredio.getNumeroPredialS1());
        this.numeroPredialPartes.setMunicipioCodigo(this.datosConsultaPredio.getNumeroPredialS2());
        this.numeroPredialPartes.setTipoAvaluo(this.datosConsultaPredio.getNumeroPredialS3());
        this.numeroPredialPartes.setSectorCodigo(this.datosConsultaPredio.getNumeroPredialS4());
        this.numeroPredialPartes.setComunaCodigo(this.datosConsultaPredio.getNumeroPredialS5());
        this.numeroPredialPartes.setBarrioCodigo(this.datosConsultaPredio.getNumeroPredialS6());
        this.numeroPredialPartes.setManzanaVeredaCodigo(this.datosConsultaPredio.
            getNumeroPredialS7());
        this.numeroPredialPartes.setPredio(this.datosConsultaPredio.getNumeroPredialS8());
        this.numeroPredialPartes.
            setCondicionPropiedad(this.datosConsultaPredio.getNumeroPredialS9());
        this.numeroPredialPartes.setEdificio(this.datosConsultaPredio.getNumeroPredialS10());
        this.numeroPredialPartes.setPiso(this.datosConsultaPredio.getNumeroPredialS11());
        this.numeroPredialPartes.setUnidad(this.datosConsultaPredio.getNumeroPredialS12());

    }

    /**
     * Metodo que avanza los tramites seleccionados segun su actividad y paso hasta el cual logró
     * llegar.
     *
     * @author felipe.cadena
     *
     */
    public void aplicarCambios() {

        LOGGER.debug("Inicio aplicacion cambios.");

        for (Tramite t : this.tramitesSeleccionados) {

            if (t.getResultado().equals(ProductoCatastralJob.AGS_ERROR.toString())) {
                this.addMensajeError("El tramite: " + t.getNumeroRadicacion() +
                    
                    " presenta un error geográfico, por favor comuniquese con el administrador del sistema.");

                continue;
            }
            if (this.obtenerNumeroEnviosArecuperacion(t) > 1) {
                this.addMensajeError("El tramite: " + t.getNumeroRadicacion() +
                    
                    " ya se ha enviado a recuperar previamente, por favor comuniquese con el administrador del sistema.");

                continue;
            }

            t.setResultado(ETramiteTareaResultado.RECUPERACION.toString());

            TramiteTarea tt = new TramiteTarea(
                t,
                t.getTarea(),
                String.valueOf(t.getPaso()),
                ETramiteTareaResultado.RECUPERACION.toString(),
                "Inicio recuperación",
                new Date(),
                this.usuario.getLogin());

            tt = this.getConservacionService().actualizarTramiteTarea(tt);
            t = this.getTramiteService().guardarActualizarTramite2(t);

            if (t.getTarea().equals(EProcesosAsincronicos.GENERAR_COPIA_CONSULTA.toString())) {
                this.avanzarCopiaConsulta(t);
            } else if (t.getTarea().equals(EProcesosAsincronicos.GENERAR_COPIA_EDICION.toString())) {
                this.avanzarCopiaEdicion(t);
            } else if (t.getTarea().equals(EProcesosAsincronicos.APLICAR_CAMBIOS_CONSERVACION.
                toString()) ||
                 t.getTarea().equals(EProcesosAsincronicos.APLICAR_CAMBIOS_ACTUALIZACION.toString())) {
                this.avanzarACConservacion(t);
            } else if (t.getTarea().equals(EProcesosAsincronicos.APLICAR_CAMBIOS_DEPURACION.
                toString())) {
                this.avanzarACDepuracion(t);
            }
        }
        //actualiza el estado de los tramites
        this.buscar();

    }

    /**
     * Método para avanzar cuando es una generacion de replica de consulta
     *
     * @author felipe.cadena
     *
     * @param tramite
     */
    public void avanzarCopiaConsulta(Tramite tramite) {

        LOGGER.debug("Tramite " + tramite.getId() + " - - " + tramite.getTarea());

        int paso = Integer.valueOf(tramite.getPaso());

        if (paso > 1) {
            this.responsableSIG = null;
        }

        this.usuario = this.obtenerUsuarioEnvio(tramite);

        this.generarReplicaConsulta(tramite, this.usuario, this.responsableSIG, paso - 1);

    }

    /**
     * Método para avanzar cuando es una generacion de replica de edicion
     *
     * @author felipe.cadena
     *
     * @param tramite
     */
    public void avanzarCopiaEdicion(Tramite tramite) {

        LOGGER.debug("Tramite " + tramite.getId() + " - - " + tramite.getTarea());

        int paso = Integer.valueOf(tramite.getPaso());

        this.usuario = this.obtenerUsuarioEnvio(tramite);

        this.generarReplicaConsulta(tramite, this.usuario, this.responsableSIG, paso - 1);

    }

    /**
     * Método para avanzar cuando se aplican cambios de conservacion
     *
     * @author felipe.cadena
     *
     * @param tramite
     */
    public void avanzarACConservacion(Tramite tramite) {

        LOGGER.debug("Tramite " + tramite.getId() + " - - " + tramite.getTarea());

        int paso;
        try {
            paso = Integer.valueOf(tramite.getPaso());
        } catch (NumberFormatException e) {
            if (tramite.getPaso().equals("1.1")) {
                //this.recuperarTramiteGeografico();
            }
            return;
        }

        this.aplicarCambiosConservacion(tramite, this.usuario, paso - 1);

    }

    /**
     * Metodo para invocar el aplicar cambios desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void aplicarCambiosConservacion(Tramite tramite, UsuarioDTO usuario, int paso) {

        String message;

        try {
            //tramite = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(tramite.getId());
            //felipe.cadena::26-08-2015::Se consideran los tramites de actualizacion
            if (tramite.isActualizacion()) {
                this.getConservacionService().aplicarCambiosActualizacion(tramite, usuario, paso);
            } else {
                this.getConservacionService().aplicarCambiosConservacion(tramite, usuario, paso);
            }

        } catch (ExcepcionSNC ex) {
            message = "Error en AplicarCambiosMB#aplicarCambiosConservacion buscando el " +
                 "trámite con id " + tramite.getId().toString();
            LOGGER.error(message);
        }

    }

    /**
     * Método para avanzar cuando se aplican cambios de depuracion
     *
     * @author felipe.cadena
     *
     * @param tramite
     */
    public void avanzarACDepuracion(Tramite tramite) {

        LOGGER.debug("Tramite " + tramite.getId() + " - - " + tramite.getTarea());

        int paso;
        paso = Integer.valueOf(tramite.getPaso());

        this.aplicarCambiosDepuracion(tramite, this.usuario, paso - 1);

    }

    /**
     * Metodo para invocar el proceso aplicar cambios de depuracion desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void aplicarCambiosDepuracion(Tramite tramite, UsuarioDTO usuario, int paso) {

        try {
            //tramite = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(tramite.getId());
            this.getConservacionService().aplicarCambiosDepuracion(tramite, usuario, paso);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error al enviar el proceso de aplicar cambios.");
        }

    }

    /**
     * Metodo para invocar la generacion de la replica de consulta desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param usuarioResponsableSIG -- Solo se usa para el envio a depuracion(paso 0), puede ser
     * null en los demas casos
     * @param paso
     */
    public void generarReplicaConsulta(Tramite tramite, UsuarioDTO usuario,
        UsuarioDTO usuarioResponsableSIG, int paso) {

        try {
            tramite = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(tramite.getId());
            this.getConservacionService().generarReplicaConsulta(tramite, usuario,
                usuarioResponsableSIG, paso);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en el poceso de generacion de replica de consulta");
        }

    }

    /**
     * Metodo para invocar la generacion de la replica de edicion desde un paso determinado
     *
     *
     * @author felipe.cadena
     * @param tramite
     * @param usuario
     * @param paso
     */
    public void generarReplicaEdicion(Tramite tramite, UsuarioDTO usuario, int paso) {

        try {
            tramite = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(tramite.getId());
            this.getConservacionService().generarReplicaEdicion(tramite, usuario, paso);

        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en el poceso de generacion de replica de consulta");
        }

    }

    /**
     * Envio tramite a depuracion
     *
     * @author felipe.cadena
     */
    public void enviarADepuracion() {
        try {
            if (this.tramiteTest.getId() == null) {
                this.addMensajeError("Se debe especificar el id del tramite");
                return;
            }

            LOGGER.debug("Tramite id >> " + this.tramiteTest.getId());
            this.tramiteTest = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(
                this.tramiteTest.getId());
            // this.responsableSIG = this.getGeneralesService().getCacheUsuario("pruatlantico28");

            //this.terminarGeneracionCopiaConsulta(this.tramiteTest, 1);
        } catch (Exception e) {
            this.addMensajeError("Error al enviar depuracion " + e.getMessage());
        }

    }

    /**
     * Envio tramite a depuracion
     *
     * @author felipe.cadena
     */
    public void generarReplicaDeConsulta() {
        try {
            if (this.tramiteTest.getId() == null) {
                this.addMensajeError("Se debe especificar el id del tramite");
                return;
            }

            LOGGER.debug("Tramite id >> " + this.tramiteTest.getId());
            this.tramiteTest = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(
                this.tramiteTest.getId());

            // Se genera la replica sin avanzar actividad
            //this.terminarGeneracionCopiaConsulta(this.tramiteTest, 2);
        } catch (Exception e) {
            this.addMensajeError("Error al enviar depuracion " + e.getMessage());
        }

    }

    /**
     * Envio tramite a depuracion
     *
     * @author felipe.cadena
     */
    public void finalizarCopiaConsulta() {

        try {
            if (this.tramiteTest.getId() == null) {
                this.addMensajeError("Se debe especificar el id del tramite");
                return;
            }

            LOGGER.debug("Tramite id >> " + this.tramiteTest.getId());
            this.tramiteTest = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(
                this.tramiteTest.getId());

            ProductoCatastralJob job = this.getGeneralesService().obtenerUltimoJobPorTramite(
                this.tramiteTest.getId(), this.tramiteTest.getTarea());

            if (job == null || !job.getEstado().equals(ProductoCatastralJob.AGS_TERMINADO)) {
                this.addMensajeError("No existe job para terminar el tramite");
                return;
            }

            this.responsableSIG = this.getGeneralesService().getCacheUsuario("pruatlantico28");

            //this.getConservacionService().finalizarGenerarReplicaConsulta(job);
        } catch (Exception e) {
            this.addMensajeError("Error al enviar depuracion " + e.getMessage());
        }

    }

    /**
     * Metodo para actualizar el estado de los tramites que fallaron en la tarea asincronica en el
     * ARCGIS server.
     *
     * @author felipe.cadena
     */
    public void actualizarEstadoTramitesAGS() {

        //this.tareasPorProcesar = jobs.size();
        this.tareasProcesadas = 0;

        for (ProductoCatastralJob productoCatastralJob : jobs) {
            List<ProductoCatastralJob> jobsAProcesar = new LinkedList<ProductoCatastralJob>();
            jobsAProcesar.add(productoCatastralJob);

//            if (!this.getConservacionService().actualizarEstadoTramitesAGS(jobsAProcesar)) {
//                this.addMensajeError("Error al actualizar los tramites en ARCGIS Server, las lista puede estar desactualizada");
//            }
            this.tareasProcesadas++;
        }

    }

    /**
     * Actualiza el estado del proceso relacionado a la barra de progreso
     *
     * @author felipe.cadena
     * @return
     */
    public Integer getProgress() {

        if (this.tareasProcesadas < 0 || this.progress == 100) {
            this.tareasProcesadas = 0;
            return 0;
        }

        if (this.tareasProcesadas == 0) {
            this.actualizarEstadoTramitesAGS();
        }

        if (this.jobs.isEmpty()) {
            RequestContext ctx = RequestContext.getCurrentInstance();
            ctx.execute("pbAjax.cancel();");
            ctx.execute("inicio.enable();");
            this.addMensajeInfo("Actualización de tareas finalizada");
            progress = 100;
            return progress;
        }
        if (this.jobs.size() <= this.tareasProcesadas) {
            RequestContext ctx = RequestContext.getCurrentInstance();
            ctx.execute("pbAjax.cancel();");
            ctx.execute("inicio.enable();");
            this.addMensajeInfo("Actualización de tareas finalizada");
            progress = 100;
            return progress;
        }
//        ProductoCatastralJob productoCatastralJob = this.jobs.get(this.tareasProcesadas);
//        List<ProductoCatastralJob> jobsAProcesar = new LinkedList<ProductoCatastralJob>();
//        jobsAProcesar.add(productoCatastralJob);
//
//        if (!this.getConservacionService().actualizarEstadoTramitesAGS(jobsAProcesar)) {
//            this.addMensajeError("Error al actualizar los tramites en ARCGIS Server, la lista puede estar desactualizada");
//            return 100;
//        }
//        this.tareasProcesadas++;

        progress = (this.tareasProcesadas * 100) / this.tareasPorProcesar;
        if (progress == null) {
            progress = 0;
        }
        if (progress == 100) {
            this.addMensajeInfo("Actualización de tareas finalizada");
        }
        return progress;
    }

    /**
     * Action sobre el botón 'Volver', redirige a la página principal removiendo el managed bean
     * actual.
     *
     * @author felipe.cadena
     * @return
     */
    public String volverAlInicio() {
        UtilidadesWeb.removerManagedBean("consultaTramite");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Retorne al numero de veces que el tramite se ha enviado a recuperar
     *
     * @author felipe.cadena
     *
     * @param t
     * @return
     */
    private int obtenerNumeroEnviosArecuperacion(Tramite t) {

        List<TramiteTarea> tareasAsociadasAlTramite = new ArrayList<TramiteTarea>();
        int numeroEnvios = 0;

        tareasAsociadasAlTramite = this.getConservacionService().obtenerTramiteTareaPorIdTramite(t.
            getId(), null);

        for (TramiteTarea tt : tareasAsociadasAlTramite) {

            if (tt.getResultado().equals(ETramiteTareaResultado.RECUPERACION.toString())) {
                numeroEnvios++;
            }

        }

        return numeroEnvios;
    }

    /**
     * Retorna el usuario correspondiente para continuar el proceso
     *
     * @author felipe.cadena
     *
     * @param t
     * @return
     */
    private UsuarioDTO obtenerUsuarioEnvio(Tramite t) {

        List<TramiteTarea> tareasAsociadasAlTramite = new ArrayList<TramiteTarea>();
        UsuarioDTO usuarioR;
        String usuarioLogin = "";
        long maxId = 0;

        tareasAsociadasAlTramite = this.getConservacionService().obtenerTramiteTareaPorIdTramite(t.
            getId(), null);

        for (TramiteTarea tt : tareasAsociadasAlTramite) {

            if (tt.getId() > maxId && !tt.getResultado().equals(ETramiteTareaResultado.RECUPERACION.
                toString())) {
                usuarioLogin = tt.getUsuarioLog();
            }

        }

        usuarioR = this.getGeneralesService().getCacheUsuario(usuarioLogin);

        return usuarioR;
    }

    /**
     * Metodo que actualiza el estados de los tramites que han presentado error en la parte
     * geografica
     *
     * @author felipe.cadena
     */
    public void actualizarTramitesEnAGSError() {
        //Tramites que estan en procesos geograficos actualmente y no se han registrado aun con error
        List<TramiteTarea> tramitesEnGeografico = this.getConservacionService().
            obtenerTramitesEnJobGeografico();

        for (TramiteTarea tt : tramitesEnGeografico) {

            Long idJob = tt.getJobId();

            if (idJob != null) {
                ProductoCatastralJob pcj = this.getConservacionService().
                    obtenerProductoCatastralJobById(idJob);
                if (pcj.getEstado().equals(ProductoCatastralJob.AGS_ERROR)) {
                    //Error en la ejecución del proceso geografico
                    Tramite t = tt.getTramite();
                    tt.setResultado(ProductoCatastralJob.AGS_ERROR);
                    t.setResultado(tt.getResultado());
                    t.setTarea(tt.getTarea());
                    t.setPaso(tt.getPaso());

                    tt = this.getConservacionService().actualizarTramiteTarea(tt);
                    t = this.getTramiteService().guardarActualizarTramite2(t);
                }
            }

        }

    }

}
