package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradminisAdicion;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.radicacion.ConsultaInformacionSolicitudMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Consulta, agrega y midifica las adiciones relacionadas a un contrato interadministrativo, cuando
 * se llama desde al caso de uso 008 se debe llamar el metodo
 *
 * @cu CU-SA-AC-089
 *
 * @author felipe.cadena
 */
@Component("administracionAdicionesContrato")
@Scope("session")
public class AdministracionAdicionesContratoMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -5932430938232717255L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaInformacionSolicitudMB.class);
    /**
     * Contrato relacionado a las adiciones de la consulta
     */
    private ContratoInteradministrativo contrato;
    /**
     * Adicion seleccionada para la edicion.
     */
    private ContratoInteradminisAdicion adicionSelecionada;
    /**
     * Valor total de todas las adiciones relacionadas al contrato.
     */
    private Double valorTotalAdiciones;
    /**
     * Lista de las adiciones realcionadas al contrato.
     */
    private List<ContratoInteradminisAdicion> adicionesLista;

    /**
     * Guarda la fecha final de la adición, para recuperarla si la hueva fecha no es valida.
     */
    private Date fechaAuxiliarAdicion;

    /**
     * Es true si se trata de una modificación y false si es una creación.
     */
    private Boolean banderaEdicion = null;

    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    // --------getters-setters----------------
    public ContratoInteradministrativo getContrato() {
        return this.contrato;
    }

    public void setContrato(ContratoInteradministrativo contrato) {
        this.contrato = contrato;
    }

    public Boolean getBanderaEdicion() {
        return banderaEdicion;
    }

    public void setBanderaEdicion(Boolean banderaEdicion) {
        this.banderaEdicion = banderaEdicion;
    }

    public Date getFechaAuxiliarAdicion() {
        return fechaAuxiliarAdicion;
    }

    public void setFechaAuxiliarAdicion(Date fechaAuxiliarAdicion) {
        this.fechaAuxiliarAdicion = fechaAuxiliarAdicion;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ContratoInteradminisAdicion getAdicionSelecionada() {
        return this.adicionSelecionada;
    }

    public void setAdicionSelecionada(ContratoInteradminisAdicion adicionSelecionada) {
        this.adicionSelecionada = adicionSelecionada;
    }

    public Double getValorTotalAdiciones() {
        return this.valorTotalAdiciones;
    }

    public void setValorTotalAdiciones(Double valorTotalAdiciones) {
        this.valorTotalAdiciones = valorTotalAdiciones;
    }

    public List<ContratoInteradminisAdicion> getAdicionesLista() {
        return this.adicionesLista;
    }

    public void setAdicionesLista(List<ContratoInteradminisAdicion> adicionesLista) {
        this.adicionesLista = adicionesLista;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdministracionAdicionesContratoMB init");
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.buscarAdiciones();

    }

    /**
     * Método para llamar este caso de uso desde el CU-SA-AC-008
     *
     * @authos felipe.cadena
     *
     * @param contrato
     */
    public void cargarDesdeCU008(ContratoInteradministrativo contrato) {

        this.contrato = this.getAvaluosService().
            obtenerContratoInteradministrativoPorIdConEntidad(contrato.getId());

        this.iniciarVariables();

    }

    /**
     * Método para buscar las adiciones relacionadas al contrato.
     *
     * @author felipe.cadena
     *
     */
    public void buscarAdiciones() {
        LOGGER.debug("Buscando adiciones...");

        this.adicionesLista = this.getAvaluosService()
            .buscarAdicionesPorIdContrato(this.contrato.getId());
        this.actualizarTotal();
        LOGGER.debug("Busqueda completa");
    }

    /**
     * Método para crear o modifica un adicion seleccionada
     *
     * @author felipe.cadena
     *
     */
    public void crearEditarAdicion() {
        LOGGER.debug("Editando adicion...");

        if (this.validarFechaAdicion()) {

            this.getAvaluosService().guardarActualizarContratoAdicion(this.adicionSelecionada);
            if (!this.banderaEdicion) {
                this.adicionesLista.add(adicionSelecionada);
            }
            this.actualizarTotal();
        }

        LOGGER.debug("Edicion completa");
    }

    private boolean validarFechaAdicion() {
        Format formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA_USUARIO_2);

        if (this.adicionSelecionada.getFechaFin().before(this.contrato.getFechaFin())) {
            this.addMensajeError("La fecha final de la adición: " +
                 formatter.format(this.adicionSelecionada.getFechaFin()) +
                 " debe ser mayor a la fecha de terminación del contrato: " +
                 formatter.format(this.contrato.getFechaFin()));
            this.adicionSelecionada.setFechaFin(fechaAuxiliarAdicion);
            return false;
        }

        return true;
    }

    /**
     * Método para caputar la fecha de la adición antes de la modificación
     *
     * @author felipe.cadena
     */
    public void prepararEdicion() {
        this.banderaEdicion = true;
        this.fechaAuxiliarAdicion = new Date(this.adicionSelecionada.getFechaFin().getTime());

    }

    /**
     * Método para preparar la entidad que se va a persistir
     *
     * @author felipe.cadena
     *
     */
    public void prepararCreacionAdicion() {
        LOGGER.debug("preparando creacion...");

        this.banderaEdicion = false;
        this.adicionSelecionada = new ContratoInteradminisAdicion();
        this.adicionSelecionada.setContratoInteradminsitrativoId(this.contrato.getId());
        this.adicionSelecionada.setNumero(String.valueOf(this.adicionesLista.size() + 1));
        this.adicionSelecionada.setUsuarioLog(this.usuario.getLogin());
        this.adicionSelecionada.setFechaLog(new Date());
    }

    /**
     * Método para calcular el valor total de las adiciones relacionadas al contrato.
     *
     * @author felipe.cadena
     */
    public void actualizarTotal() {
        this.valorTotalAdiciones = 0d;
        for (ContratoInteradminisAdicion adicion : this.adicionesLista) {
            if (!(adicion.getValor() == null)) {
                this.valorTotalAdiciones += adicion.getValor();
            }
        }
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaInformacionSolicitudMB#cerrar");

        UtilidadesWeb.removerManagedBean("consultaContratosInteradministrativos");

        return ConstantesNavegacionWeb.INDEX;
    }
}
