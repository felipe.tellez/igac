package co.gov.igac.snc.web.util;

import java.io.Serializable;

/**
 * Objeto para mostrar los items del estudio de costos en la aprobacion del gasto
 *
 * @author franz.gamba
 *
 */
public class EgresoConceptoGastoParaConvenioDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7454856672882692880L;

    private String codigo;
    private String item;
    private Double total;

    public EgresoConceptoGastoParaConvenioDTO() {

    }

    /**
     * Full constructor
     *
     * @param codigo
     * @param item
     * @param total
     */
    public EgresoConceptoGastoParaConvenioDTO(String codigo, String item,
        Double total) {
        super();
        this.codigo = codigo;
        this.item = item;
        this.total = total;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
