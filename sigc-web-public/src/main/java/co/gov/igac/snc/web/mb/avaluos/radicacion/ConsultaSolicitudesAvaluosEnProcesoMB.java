package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.FiltroPredio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso CU-SA-AC-153 Consultar Solicitudes de Avalúos en Proceso,
 *
 * @cu CU-SA-AC-153
 *
 * @author felipe.cadena
 */
/*
 * Se usa la clase FiltroDatosConsultaAvaluo para enviar los datos ingresado por el usuario y
 * realizar la consulta, si algun campo es nulo o vacio no se toma en cuanta como filtro de la
 * consulta.
 */
@Component("consultaSolicitudesAvaluosEnProceso")
@Scope("session")
public class ConsultaSolicitudesAvaluosEnProcesoMB extends SNCManagedBean {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -6091068902929204424L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaSolicitudesAvaluosEnProcesoMB.class);
    //-----criterios de busqueda------------
    /**
     * Número de radicacion para la busqueda
     */
    private String numeroRadicacion;
    /**
     * Número de sec. radicado para la busqueda
     */
    private String numeroSecRadicado;
    /**
     * Fecha inicial del rango de busqueda
     */
    private Date fechaRadicacionDesde;
    /**
     * Fecha final del rango de busqueda
     */
    private Date fechaRadicacionHasta;
    /**
     * Nombre o razón social del solicitante para la busqueda
     */
    private String nombreSolicitante;
    /**
     * Territorial para la busqueda
     */
    private String territorial;
    /**
     * Tipo de tramite a buscar
     */
    private String tipoTramite;
    /**
     * Departamento para la busqueda
     */
    private String departamento;
    /**
     * Municipio para la busqueda
     */
    private String municipio;
    /**
     * Estado actual del tramite a buscar
     */
    private String estadoTramite;
    /**
     * Tipo de empresa asociada al tramite
     */
    private String tipoEmpresa;
    /**
     * Nombre del avaluador asociado al tramite
     */
    private String nombreAvaluador;
    /**
     * Tipo de avalúo para la busqueda
     */
    private String tipoAvaluo;
    /**
     * Direccion del predio relacionado al avalúo
     */
    private String direccion;
    /**
     * Número predial del predio asociado al avaluo
     */
    private String numeroPredial;

    private Avaluo avaluoSeleccionado;
    /**
     * Capturas los datos de la busqueda relacionados al predio.
     */
    private FiltroPredio datosPredio;
    /**
     * Bandera que indica si se deben mostrar todas las columnas en la tabla de resultados
     */
    private boolean banderaMostrarTodasColumnas;
    //--------banderas para los enlaces de la tabla de resultados
    /**
     * Bandera que indica si el usuario es de sede central
     */
    private boolean banderaSedeCentral;
    //-------------------------------------------------------------
    /**
     * Lista donde se guarda los resultados de la busqueda
     */
    private List<Avaluo> avaluosBusqueda;
    /**
     * Lista de Items municipios asociados al departamento seleccionado
     */
    private List<SelectItem> municipiosLista;
    /**
     * Lista de Items departamentos
     */
    private List<SelectItem> departamentosLista;
    /**
     * Lista para seleccionar la territorial a asignar, varia dependiendo de la territorial actual.
     */
    private List<SelectItem> listaTerritoriales;
    @Autowired
    private GeneralMB generalMB;
    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    // --------getters-setters----------------
    public boolean isBanderaSedeCentral() {
        return this.banderaSedeCentral;
    }

    public void setBanderaSedeCentral(boolean banderaSedeCentral) {
        this.banderaSedeCentral = banderaSedeCentral;
    }

    public String getNombreAvaluador() {
        return nombreAvaluador;
    }

    public void setNombreAvaluador(String nombreAvaluador) {
        this.nombreAvaluador = nombreAvaluador;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public boolean isBanderaMostrarTodasColumnas() {
        return banderaMostrarTodasColumnas;
    }

    public void setBanderaMostrarTodasColumnas(boolean banderaMostrarTodasColumnas) {
        this.banderaMostrarTodasColumnas = banderaMostrarTodasColumnas;
    }

    public FiltroPredio getDatosPredio() {
        return datosPredio;
    }

    public void setDatosPredio(FiltroPredio datosPredio) {
        this.datosPredio = datosPredio;
    }

    public Avaluo getAvaluoSeleccionado() {
        return avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public List<SelectItem> getListaTerritoriales() {
        return this.listaTerritoriales;
    }

    public void setListaTerritoriales(List<SelectItem> listaTerritoriales) {
        this.listaTerritoriales = listaTerritoriales;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<SelectItem> getDepartamentosLista() {
        return this.departamentosLista;
    }

    public void setDepartamentosLista(List<SelectItem> departamentosLista) {
        this.departamentosLista = departamentosLista;
    }

    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getNumeroSecRadicado() {
        return this.numeroSecRadicado;
    }

    public void setNumeroSecRadicado(String numeroSecRadicado) {
        this.numeroSecRadicado = numeroSecRadicado;
    }

    public Date getFechaRadicacionDesde() {
        return this.fechaRadicacionDesde;
    }

    public void setFechaRadicacionDesde(Date fechaRadicacionDesde) {
        this.fechaRadicacionDesde = fechaRadicacionDesde;
    }

    public Date getFechaRadicacionHasta() {
        return this.fechaRadicacionHasta;
    }

    public void setFechaRadicacionHasta(Date fechaRadicacionHasta) {
        this.fechaRadicacionHasta = fechaRadicacionHasta;
    }

    public String getNombreSolicitante() {
        return this.nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getTerritorial() {
        return this.territorial;
    }

    public void setTerritorial(String territorial) {
        this.territorial = territorial;
    }

    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return this.municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstadoTramite() {
        return this.estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public String getTipoEmpresa() {
        return this.tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public List<Avaluo> getAvaluosBusqueda() {
        return this.avaluosBusqueda;
    }

    public void setAvaluosBusqueda(List<Avaluo> avaluosBusqueda) {
        this.avaluosBusqueda = avaluosBusqueda;
    }

    public List<SelectItem> getMunicipiosLista() {
        return this.municipiosLista;
    }

    public void setMunicipiosLista(List<SelectItem> municipiosLista) {
        this.municipiosLista = municipiosLista;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on consultaSolicitudesAvaluosEnProceso init");
        this.iniciarVariables();

    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {
        this.datosPredio = new FiltroPredio();
        List<Departamento> deptos = this.getGeneralesService().getCacheDepartamentos();

        this.departamentosLista = new ArrayList<SelectItem>();

        for (Departamento depto : deptos) {
            this.departamentosLista.add(new SelectItem(depto
                .getCodigo(), depto.getCodigo() + "-" +
                 depto.getNombre()));
        }

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        if (this.usuario.getCodigoTerritorial() == null) {
            this.banderaSedeCentral = true;
        } else {
            this.banderaSedeCentral = false;
        }

        this.iniciarTerritoriales();
    }

    /**
     * Método para actualizar la lista de municipios con respecto al departamento
     *
     * @author felipe.cadena
     */
    public void actualizarMunicipios() {
        LOGGER.debug("Actualizando municipios...");
        this.municipiosLista = new ArrayList<SelectItem>();
        List<Municipio> muns = this.getGeneralesService()
            .getCacheMunicipiosPorDepartamento(
                this.departamento);
        this.datosPredio.setNumeroPredialS1(this.departamento);

        for (Municipio mun : muns) {
            this.municipiosLista.add(new SelectItem(mun
                .getCodigo(), mun.getCodigo().substring(2) + "-" +
                 mun.getNombre()));
        }

        LOGGER.debug("Municipios actualizados");

    }

    /**
     * Actualiza el valor correspondiente al municipio del número predial.
     *
     * @author felipe.cadena
     */
    public void listenerMunicipiosMenu() {
        if (this.municipio != null) {
            this.datosPredio.setNumeroPredialS2(this.municipio.substring(2));
        } else {
            this.datosPredio.setNumeroPredialS2(null);
        }
    }

    /**
     * Método para iniciar la lista de territoriales que se pueden asignar.
     *
     * @author felipe.cadena
     *
     */
    public void iniciarTerritoriales() {

        LOGGER.debug("Iniciar territoriales... ");

        if (this.usuario.getCodigoTerritorial() == null) {
            this.listaTerritoriales = this.generalMB.getTerritorialesConDireccionGeneral();
        } else {
            SelectItem si = new SelectItem(this.usuario.getCodigoTerritorial(),
                this.obtenerNombreTerritorial(this.usuario.getCodigoTerritorial()));
            this.listaTerritoriales = new ArrayList<SelectItem>();
            this.listaTerritoriales.add(si);
        }

    }

    /**
     * Método para realizar la validacion de las fechas ingresadas
     *
     * @author felipe.cadena
     *
     * @return
     *
     */
    public boolean validarFechas() {

        if (this.fechaRadicacionDesde == null && this.fechaRadicacionHasta == null) {
            return true;
        }
        if (this.fechaRadicacionDesde == null) {
            this.addMensajeError("Debe Ingresar la fecha Radicación Desde,  " +
                 "Intente nuevamente");
            return false;
        }
        if (this.fechaRadicacionHasta == null) {
            this.addMensajeError("Debe Ingresar la fecha Radicación Hasta," +
                 "  Intente nuevamente");
            return false;
        }

        if (this.fechaRadicacionDesde.after(this.fechaRadicacionHasta)) {
            this.addMensajeError("La 'Fecha radicación desde' es mayor que la " +
                 "'Fecha radicación hasta',  Intente nuevamente");
            return false;
        } else {
            return true;
        }
    }

    private String obtenerNombreTerritorial(String codigo) {
        EstructuraOrganizacional eo;

        eo = this.getGeneralesService().obtenerTerritorialPorCodigo(codigo);

        return codigo + " - " + eo.getNombre();

    }

    /**
     * Método para realizar la busqueda de avaluos dependiendo de los parametros ingresados por el
     * usuario.
     *
     * Se llama desde consultaSolicitudesAvaluosEnProceso.xhtml (p:commandButton id="buscarBtn")
     *
     * @author felipe.cadena
     *
     */
    public void buscarAvaluos() {
        LOGGER.debug("Iniciando busqueda avaluos...");

        this.avaluosBusqueda = new ArrayList<Avaluo>();
        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();

        if (!validarFechas()) {
            return;
        }

        filtro.setDepartamento(this.departamento);
        filtro.setFechaInicial(this.fechaRadicacionDesde);
        filtro.setFechaFinal(this.fechaRadicacionHasta);
        filtro.setMunicipio(this.municipio);

        filtro.setDireccion(this.direccion);
        filtro.setNumeroPredial(this.numeroPredialFormato());
        filtro.setNumeroRadicacion(this.numeroRadicacion);
        filtro.setNumeroSecRadicado(this.numeroSecRadicado);
        filtro.setTerritorial(this.territorial);
        filtro.setTipoEmpresa(this.tipoEmpresa);
        filtro.setTipoTramite(this.tipoTramite);
        filtro.setTipoAvaluo(this.tipoAvaluo);
        filtro.setNombreAvaluador(this.nombreAvaluador);
        filtro.setEstadoTramiteAvaluo(ETramiteEstado.EN_PROCESO.getCodigo());

        if (!(this.tipoEmpresa == null)) {
            if (this.tipoEmpresa.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {
                filtro.setNombreRazonSocial(this.nombreSolicitante);
                filtro.setNombreSolicitante(null);
            } else {
                filtro.setNombreSolicitante(this.nombreSolicitante);
                filtro.setNombreRazonSocial(null);
            }
        } else {
            filtro.setNombreSolicitante(null);
            filtro.setNombreRazonSocial(null);
        }

        avaluosBusqueda = this.getAvaluosService().buscarAvaluosPorFiltro(filtro);

        if (this.avaluosBusqueda == null) {
            this.addMensajeInfo("No se encontraron datos de acuerdo a los " +
                 "criterios de búsqueda,  intente nuevamente");
            return;
        }
        LOGGER.debug("Busqueda terminada");

    }

    /**
     * Método para unificar el número predial y enviarlo al filtro
     *
     * @return
     */
    public String numeroPredialFormato() {
        StringBuilder np = new StringBuilder();
        if ("".equals(this.datosPredio.getNumeroPredialS1()) ||
            this.datosPredio.getNumeroPredialS1() == null) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS1());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS2()) ||
            this.datosPredio.getNumeroPredialS2() == null) {
            np.append("___");
        } else {
            np.append(this.datosPredio.getNumeroPredialS2());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS3())) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS3());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS4())) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS4());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS5())) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS5());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS6())) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS6());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS7())) {
            np.append("____");
        } else {
            np.append(this.datosPredio.getNumeroPredialS7());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS8())) {
            np.append("____");
        } else {
            np.append(this.datosPredio.getNumeroPredialS8());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS9())) {
            np.append("_");
        } else {
            np.append(this.datosPredio.getNumeroPredialS9());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS10())) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS10());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS11())) {
            np.append("__");
        } else {
            np.append(this.datosPredio.getNumeroPredialS11());
        }
        if ("".equals(this.datosPredio.getNumeroPredialS12())) {
            np.append("____");
        } else {
            np.append(this.datosPredio.getNumeroPredialS12());
        }

        LOGGER.debug("Numero Predial >>" + np.toString());
        return np.toString();
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaInformacionSolicitudMB#cerrar");

        UtilidadesWeb.removerManagedBean("consultaSolicitudesAvaluosEnProceso");

        ConsultaInformacionSolicitudMB cisMB = (ConsultaInformacionSolicitudMB) UtilidadesWeb
            .getManagedBean("consultaInformacionSolicitud");
        cisMB.cerrar();

        LOGGER.debug("finalizando ConsultaInformacionSolicitudMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
