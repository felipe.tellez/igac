package co.gov.igac.snc.web.util.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.util.Constantes;

/**
 * Converter para los objetos RecursoHumano para usarlo en los selectItems de los combos
 *
 * @author javier.aponte
 */
@FacesConverter(value = "recursoHumanoConverter",
    forClass = co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano.class)
public class RecursoHumanoConverter implements Converter {

    //---------------------------//
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        RecursoHumano answer = new RecursoHumano();

        String[] objectAttributes;
        objectAttributes = value.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setId(Long.parseLong(objectAttributes[0]));
        answer.setActividad(objectAttributes[1]);
        answer.setCargo(objectAttributes[2]);
        answer.setDocumentoIdentificacion(objectAttributes[3]);
        answer.setNombre(objectAttributes[4]);
        answer.setTipo(objectAttributes[5]);

        return answer;

    }

    //---------------------------//
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object o) {
        return ((RecursoHumano) o).toString();
    }

}
