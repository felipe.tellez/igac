/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracionObservacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.ValidacionYAdicionNuevoTramiteMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.Date;
import java.util.LinkedList;
import javax.faces.model.SelectItem;

/**
 * MB para el caso de uso Revisar tareas a realizar
 *
 * @author javier.aponte
 * @CU CU-SA-AC-203
 */
@Component("revisionTareasARealizar")
@Scope("session")
public class RevisionTareasARealizarMB extends ProcessFlowManager {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(RevisionTareasARealizarMB.class);

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextService;

    @Autowired
    private ValidacionYAdicionNuevoTramiteMB validacionTramiteMB;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentProcessActivity;

    /**
     * Tramite relacionado a la actividad actual
     */
    private Tramite tramiteSeleccionado;

    /**
     * Predios relacionados al tramite actual
     */
    private List<Predio> prediosTramite;

    /**
     * Entidad de depuración asociada al trámite.
     */
    private TramiteDepuracion tramiteDepuracion;

    /**
     * Bandera para habilitar el avance del proceso
     */
    private boolean banderaAvanzarProceso;

    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;

    /**
     * Variable donde se almacenan los nuevos trámites adicionados
     */
    private List<Tramite> nuevosTramitesAdicionados;

    /**
     * Variable donde se almacena el trámite nuevo adicionado seleccionado en la tabla de trámites
     * asociados
     */
    private Tramite nuevoTramiteAdicionadoSeleccionado;

    /**
     * Variable booleana para saber si se van a ver los documentos asociados o si se va a solicitar
     * nuevos Documentos
     */
    private boolean verSolicitudDocsBool;

    /**
     * Rol del usuario actul.
     */
    private String rol;

    /**
     * Inconsistencias relacionadas al trámite
     */
    private List<TramiteInconsistencia> inconsistenciasTramite;

    /**
     * documentación del trámite que se ha seleccionado de la tabla
     */
    private TramiteDocumentacion selectedTramiteDocumentacion;

    //---------------Banderas de edición ------------------------------
    private boolean activarBotonAvanzarProceso;

    private boolean activarIngresoObservaciones;

    private boolean activarPanelRequiereNuevosTramites;

    private boolean activarEntradaRequiereLevantamiento;

    private boolean activarEntradaRequiereIrATerreno;

    private boolean activarEntradaRequiereNuevosTramites;

    private boolean activarLecturaRequiereLevantamiento;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Bandera para saber si el tramite se mueve a edicion geografica
     */
    private boolean banderaAvanceAGeografico;

    /**
     * Observacion ingresada cuando se necesita asignar un topografo
     */
    private TramiteDepuracionObservacion observacionLevantamientoTopografico;

    /**
     * Bandera para determinar si el tramite de depuracion tiene tramites asociados
     */
    private boolean banderaTramitesAsociadosDepuracion;

    //---------------------Getters and Setters---------------------//
    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public TramiteDepuracionObservacion getObservacionLevantamientoTopografico() {
        return observacionLevantamientoTopografico;
    }

    public void setObservacionLevantamientoTopografico(
        TramiteDepuracionObservacion observacionLevantamientoTopografico) {
        this.observacionLevantamientoTopografico = observacionLevantamientoTopografico;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public List<Predio> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<Predio> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public TramiteDepuracion getTramiteDepuracion() {
        return tramiteDepuracion;
    }

    public void setTramiteDepuracion(TramiteDepuracion tramiteDepuracion) {
        this.tramiteDepuracion = tramiteDepuracion;
    }

    public boolean isBanderaAvanzarProceso() {
        return banderaAvanzarProceso;
    }

    public void setBanderaAvanzarProceso(boolean banderaAvanzarProceso) {
        this.banderaAvanzarProceso = banderaAvanzarProceso;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public boolean isActivarBotonAvanzarProceso() {
        return activarBotonAvanzarProceso;
    }

    public void setActivarBotonAvanzarProceso(boolean activarBotonAvanzarProceso) {
        this.activarBotonAvanzarProceso = activarBotonAvanzarProceso;
    }

    public boolean isActivarIngresoObservaciones() {
        return activarIngresoObservaciones;
    }

    public void setActivarIngresoObservaciones(boolean activarIngresoObservaciones) {
        this.activarIngresoObservaciones = activarIngresoObservaciones;
    }

    public boolean isActivarPanelRequiereNuevosTramites() {
        return activarPanelRequiereNuevosTramites;
    }

    public void setActivarPanelRequiereNuevosTramites(
        boolean activarPanelRequiereNuevosTramites) {
        this.activarPanelRequiereNuevosTramites = activarPanelRequiereNuevosTramites;
    }

    public boolean isActivarEntradaRequiereLevantamiento() {
        return activarEntradaRequiereLevantamiento;
    }

    public void setActivarEntradaRequiereLevantamiento(
        boolean activarEntradaRequiereLevantamiento) {
        this.activarEntradaRequiereLevantamiento = activarEntradaRequiereLevantamiento;
    }

    public boolean isActivarEntradaRequiereIrATerreno() {
        return activarEntradaRequiereIrATerreno;
    }

    public void setActivarEntradaRequiereIrATerreno(
        boolean activarEntradaRequiereIrATerreno) {
        this.activarEntradaRequiereIrATerreno = activarEntradaRequiereIrATerreno;
    }

    public boolean isActivarEntradaRequiereNuevosTramites() {
        return activarEntradaRequiereNuevosTramites;
    }

    public void setActivarEntradaRequiereNuevosTramites(
        boolean activarEntradaRequiereNuevosTramites) {
        this.activarEntradaRequiereNuevosTramites = activarEntradaRequiereNuevosTramites;
    }

    public boolean isActivarLecturaRequiereLevantamiento() {
        return activarLecturaRequiereLevantamiento;
    }

    public void setActivarLecturaRequiereLevantamiento(
        boolean activarLecturaRequiereLevantamiento) {
        this.activarLecturaRequiereLevantamiento = activarLecturaRequiereLevantamiento;
    }

    public List<Tramite> getNuevosTramitesAdicionados() {
        return nuevosTramitesAdicionados;
    }

    public void setNuevosTramitesAdicionados(List<Tramite> nuevosTramitesAdicionados) {
        this.nuevosTramitesAdicionados = nuevosTramitesAdicionados;
    }

    public Tramite getNuevoTramiteAdicionadoSeleccionado() {
        return nuevoTramiteAdicionadoSeleccionado;
    }

    public void setNuevoTramiteAdicionadoSeleccionado(
        Tramite nuevoTramiteAdicionadoSeleccionado) {
        this.nuevoTramiteAdicionadoSeleccionado = nuevoTramiteAdicionadoSeleccionado;
    }

    public boolean getVerSolicitudDocsBool() {
        return verSolicitudDocsBool;
    }

    public void setVerSolicitudDocsBool(boolean verSolicitudDocsBool) {
        this.verSolicitudDocsBool = verSolicitudDocsBool;
    }

    public TramiteDocumentacion getSelectedTramiteDocumentacion() {
        return selectedTramiteDocumentacion;
    }

    public void setSelectedTramiteDocumentacion(
        TramiteDocumentacion selectedTramiteDocumentacion) {
        this.selectedTramiteDocumentacion = selectedTramiteDocumentacion;
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public boolean isBanderaTramitesAsociadosDepuracion() {
        return banderaTramitesAsociadosDepuracion;
    }

    public void setBanderaTramitesAsociadosDepuracion(boolean banderaTramitesAsociadosDepuracion) {
        this.banderaTramitesAsociadosDepuracion = banderaTramitesAsociadosDepuracion;
    }

    //-------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("en RevisionTareasARealizarMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        this.validacionTramiteMB.setUsuario(this.currentUser);
        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "revisarTareasARealizar");

        this.tramiteSeleccionado = new Tramite();
        this.banderaAvanzarProceso = false;

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        Long idtramiteActual = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();
        this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.tramiteSeleccionado.setId(idtramiteActual);
        this.cargarDatosTramite();

        //Inicializar variables
        this.activarBotonAvanzarProceso = false;
        this.activarIngresoObservaciones = false;
        this.activarPanelRequiereNuevosTramites = false;

        this.activarEntradaRequiereIrATerreno = false;
        this.activarEntradaRequiereLevantamiento = false;
        this.activarEntradaRequiereNuevosTramites = false;
        this.activarLecturaRequiereLevantamiento = false;

        this.nuevoTramiteAdicionadoSeleccionado = new Tramite();
        //se cargan las inconsostencias asociadas al tramite
        this.actualizarInconsistencias();
        this.determinarRol();

        if (this.tramiteDepuracion.getTramiteDepuracionObservacions() != null &&
             !this.tramiteDepuracion.getTramiteDepuracionObservacions().isEmpty()) {
            this.observacionLevantamientoTopografico = this.tramiteDepuracion.
                getTramiteDepuracionObservacions().get(0);
        } else {
            this.observacionLevantamientoTopografico = new TramiteDepuracionObservacion();
        }

        if (this.tramiteSeleccionado.getTramite() != null) {
            this.banderaTramitesAsociadosDepuracion = true;
        }
    }

    //--------------------------------------------------------------------------------------------------
    //----------- bpm
    /**
     *
     */
    @Implement
    public boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------

    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO message = new ActivityMessageDTO();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();
        String transitionName = "";
        UsuarioDTO usuarioDestinoActividad = null;

        if (this.rol.equals(ERol.TOPOGRAFO.toString()) && this.tramiteDepuracion.getCampo().equals(
            ESiNo.SI.getCodigo())) {

            usuarioDestinoActividad = this.currentUser;
            usuariosActividad.add(usuarioDestinoActividad);
            transitionName = ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION;
        } else if (this.rol.equals(ERol.TOPOGRAFO.toString()) && this.tramiteDepuracion.getCampo().
            equals(ESiNo.NO.getCodigo())) {

            usuarioDestinoActividad = this.currentUser;
            usuariosActividad.add(usuarioDestinoActividad);
            transitionName =
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_TOPOGRAFIA;
        } else if (this.rol.equals(ERol.RESPONSABLE_CONSERVACION.toString()) &&
            this.tramiteDepuracion.getLevantamiento().equals(ESiNo.SI.getCodigo())) {

            List<UsuarioDTO> usuarios = this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    this.currentUser.getDescripcionEstructuraOrganizacional(),
                    ERol.DIRECTOR_TERRITORIAL);

            if (usuarios == null || usuarios.isEmpty()) {
                usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.currentUser.getDescripcionTerritorial(),
                    ERol.DIRECTOR_TERRITORIAL);
            }

            usuarioDestinoActividad = usuarios.get(0);

            usuariosActividad.add(usuarioDestinoActividad);
            transitionName = ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_TOPOGRAFO;
        } else if (this.rol.equals(ERol.RESPONSABLE_CONSERVACION.toString()) &&
            this.tramiteDepuracion.getLevantamiento().equals(ESiNo.NO.getCodigo())) {

            usuarioDestinoActividad = this.currentUser;
            usuariosActividad.add(usuarioDestinoActividad);
            transitionName = ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_EJECUTOR;
        } else if (this.rol.equals(ERol.DIGITALIZADOR_DEPURACION.toString()) &&
            this.tramiteDepuracion.getNuevosTramites().equals(ESiNo.SI.getCodigo())) {

            transitionName = ProcesoDeConservacion.ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES;
            usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.currentUser.getDescripcionEstructuraOrganizacional(),
                ERol.COORDINADOR);

            // En caso de que se hayan asociado nuevos trámites se debe guardar
            // la actividad donde se asociaron en depuración, para poder
            // distinguir estos trámites en la actividad de 'Aprobar nuevos
            // trámites', e identificar cuales trámites son provenientes de
            // depuración y cuales de conservación.
            if (this.validacionTramiteMB.getTramitesAsociadosATramiteInicial() != null &&
                 !this.validacionTramiteMB
                    .getTramitesAsociadosATramiteInicial().isEmpty()) {
                solicitudCatastral.setObservaciones(this.currentProcessActivity
                    .getNombre());
            }
        } else {

            usuarioDestinoActividad = this.currentUser;
            usuariosActividad.add(usuarioDestinoActividad);
            transitionName =
                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION;

//            this.banderaAvanceAGeografico = true;
//            this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(this.currentProcessActivity, this.currentUser);
        }
        this.banderaAvanceAGeografico = false;

        message.setActivityId(this.currentProcessActivity.getId());

        if (usuarioDestinoActividad != null) {
            usuariosActividad.add(usuarioDestinoActividad);
        }
        solicitudCatastral.setUsuarios(usuariosActividad);
        message.setTransition(transitionName);
        solicitudCatastral.setTransicion(transitionName);

        message.setUsuarioActual(this.currentUser);
        message.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(message);

        if (this.getMessage().getComment() != null) {
            message.setComment(this.getMessage().getComment());
        }

    }

    //--------------------------------------------------
    /**
     *
     */
    @Implement
    public void doDatabaseStatesUpdate() {
        // do nothing
    }

    /**
     * Método para determinar el tipo de rol de la actividad y en base a eso determinar el tipo de
     * funcionario que se va a asignar.
     *
     * @author felipe.cadena
     */
    public void determinarRol() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#determinarRol ");

        String actividad = this.currentProcessActivity.getNombre();

        String[] roles = this.currentUser.getRoles();
        for (String role : roles) {
            if (actividad.equals(ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_TERRENO.
                toString())) {

                this.rol = ERol.RESPONSABLE_CONSERVACION.toString();
                this.activarEntradaRequiereLevantamiento = true;
            }
            if (role.equals(ERol.DIRECTOR_TERRITORIAL.toString())) {
                this.rol = ERol.DIRECTOR_TERRITORIAL.toString();
            }
            if (actividad.equals(ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_POR_REALIZAR.
                toString())) {

                this.rol = ERol.DIGITALIZADOR_DEPURACION.toString();
                this.activarEntradaRequiereNuevosTramites = true;
            }
            if (actividad.equals(ProcesoDeConservacion.ACT_DEPURACION_EVALUAR_INFO_ASOCIADA.
                toString())) {

                this.rol = ERol.TOPOGRAFO.toString();
                this.activarLecturaRequiereLevantamiento = true;
                this.activarEntradaRequiereIrATerreno = true;

            }
        }
    }

    /**
     * Método para actualizar las inconsistencias asociadas al trámite
     *
     * @author felipe.cadena
     */
    public void actualizarInconsistencias() {

        this.inconsistenciasTramite = this.getTramiteService().
            buscarTramiteInconsistenciaPorTramiteId(tramiteSeleccionado.getId());

    }

//--------------------------METODOS---------------
    /**
     * Método para cargar los datos relacionados al tramite que se deben mostrar.
     *
     * @author javier.aponte
     */
    public void cargarDatosTramite() {
        LOGGER.debug("Cargando datos tramite...");

        this.tramiteSeleccionado = this.getTramiteService().
            buscarTramiteTraerDocumentoResolucionPorIdTramite(this.tramiteSeleccionado.getId());

        this.prediosTramite = this.tramiteSeleccionado.getPredios();

        this.tramiteDepuracion = this.getTramiteService().
            buscarUltimoTramiteDepuracionPorIdTramite(this.tramiteSeleccionado.getId());

        if (this.tramiteDepuracion.getNuevosTramites() != null &&
            this.tramiteDepuracion.getNuevosTramites().equals(ESiNo.SI.getCodigo())) {
            List<Tramite> trs = this.tramiteSeleccionado.getTramites();
            this.validacionTramiteMB.setTramitesAsociadosATramiteInicial(trs);
            List<SelectItem> orden = new LinkedList<SelectItem>();
            for (Tramite t : trs) {
                orden.add(new SelectItem(t.getOrdenEjecucion()));
            }
            this.validacionTramiteMB.setOrdenEjecucionTramites(orden);
        }

        this.tramiteDepuracion.setLevantamiento(ESiNo.NO.getCodigo());
        this.tramiteDepuracion.setNuevosTramites(ESiNo.NO.getCodigo());
        this.tramiteDepuracion.setCampo(ESiNo.NO.getCodigo());

        this.parametroEditorGeografico = this.currentUser.getLogin() + "," +
            this.tramiteSeleccionado.getId() +
            "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class,
                this.currentProcessActivity.getNombre());;
        LOGGER.debug("Fin carga datos...");
    }

    /**
     * Método para avanzar el proceso.
     *
     * @author javier.aponte
     */
    public String avanzarProceso() {
        LOGGER.debug("Avanzando proceso...");

        this.setupProcessMessage();

        this.forwardProcess();

        //D: hay que hacer esto aquí porque no se puede factorizar. (me da pereza explicar por qué)
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método encargado de terminar la sesión y retornar al usuario al arbol de tareas
     *
     * @author javier.aponte
     */
    public String cerrarPaginaPrincipal() {

        LOGGER.debug("iniciando RevisionTareasARealizarMB#cerrar");

        UtilidadesWeb.removerManagedBean("revisionTareasARealizar");

        LOGGER.debug("finalizando RevisionTareasARealizarMB#cerrar");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método encargado de activar el campo de observaciones cuando en la opción requiere
     * levantamiento se selicciona si
     *
     * @author javier.aponte
     */
    public void activarObservaciones() {

        this.activarIngresoObservaciones = false;
        if (ESiNo.SI.getCodigo().equals(this.tramiteDepuracion.getLevantamiento())) {
            this.activarIngresoObservaciones = true;
        }

    }

    /**
     * Método encargado de activar el panel de requiere nuevos trámites cuando en la opción requiere
     * nuevo trámite se selecciona si
     *
     * @author javier.aponte
     */
    public void activarPanelRequiereNuevosTramites() {

        this.activarPanelRequiereNuevosTramites = false;
        if (ESiNo.SI.getCodigo().equals(this.tramiteDepuracion.getNuevosTramites())) {
            this.activarPanelRequiereNuevosTramites = true;
        }

    }

    /**
     * Método para revisar el orden de los nuevos trámites asociados al trámite inicial
     *
     * @author javier.aponte
     */
    public void revisarOrden() {
        if (this.validacionTramiteMB.getTramitesAsociadosATramiteInicial() != null &&
             !this.validacionTramiteMB.getTramitesAsociadosATramiteInicial().isEmpty()) {

            boolean validador;
            validador = this.validarOrdenTramitesNuevos();
            if (!validador) {
                String mensaje = "Algunos de los trámites asociados presentan" +
                     " el mismo orden de ejecución," +
                     " por favor corrija los datos e intente nuevamente";
                this.addMensajeError(mensaje);
//				return false;
            }

        }
//		return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * código "migrado" para validar que los trámites nuevos tengan diferente orden asignado. Basado
     * en lo hecho por juen.agudelo para poder llamarlo desde otra clase
     *
     * @author pedro.garcia
     */
    public boolean validarOrdenTramitesNuevos() {

        boolean answer = true;
        Integer contadorTemporal;
        int validador;

        for (Tramite tTemp : this.validacionTramiteMB.getTramitesAsociadosATramiteInicial()) {
            contadorTemporal = tTemp.getOrdenEjecucion();
            validador = 0;

            for (Tramite tTemp2 : this.validacionTramiteMB.getTramitesAsociadosATramiteInicial()) {
                if (tTemp2.getOrdenEjecucion().equals(contadorTemporal)) {
                    validador++;
                }
            }

            if (validador > 1) {
                return false;
            }
        }
        return answer;

    }

    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.selectedTramiteDocumentacion.getDocumentoSoporte().getArchivo());
    }

    /**
     * Método asociado al botón guardar de la pantalla de revisar tareas a realizar
     *
     * @author javier.aponte
     */
    public void guardarInformacion() {

        if (this.rol.equals(ERol.RESPONSABLE_CONSERVACION.toString()) &&
             this.tramiteDepuracion.getLevantamiento().equals(ESiNo.SI.getCodigo())) {
            List<TramiteDepuracionObservacion> obs = new ArrayList<TramiteDepuracionObservacion>();
            if (this.tramiteDepuracion.getTramiteDepuracionObservacions() == null ||
                 this.tramiteDepuracion.getTramiteDepuracionObservacions().isEmpty()) {
                this.observacionLevantamientoTopografico.setFecha(new Date());
                this.observacionLevantamientoTopografico.setFechaLog(new Date());
                this.observacionLevantamientoTopografico.setApruebaControlCalidad(" ");
                this.observacionLevantamientoTopografico.setNumeroRevision(0l);
                this.observacionLevantamientoTopografico.
                    setTramiteDepuracion(this.tramiteDepuracion);
                this.observacionLevantamientoTopografico.setUsuarioLog(this.currentUser.getLogin());

                obs.add(this.observacionLevantamientoTopografico);
                this.tramiteDepuracion.setTramiteDepuracionObservacions(obs);
            } else {
                obs.add(this.observacionLevantamientoTopografico);
                this.tramiteDepuracion.setTramiteDepuracionObservacions(obs);
            }

        }

        if (this.tramiteDepuracion.getNuevosTramites().equals(ESiNo.SI.getCodigo())) {
            Tramite t;

            t = this.validacionTramiteMB.getTramiteInicial();
            t = this.getTramiteService().guardarActualizarTramite2(t);

        }

        this.getTramiteService().actualizarTramiteDepuracion(this.tramiteDepuracion);
        this.activarBotonAvanzarProceso = true;

    }
}
