package co.gov.igac.snc.web.util;

/**
 * Enumeración para el dominio TRAMITE_DOCUMENTO_METO_COMU
 *
 * @author juanfelipe.garcia
 */
public enum ETramiteDocumentoMetoComu {
    CORREO_ELECTRONICO("1"),
    FAX("2"),
    TELEFONO("3"),
    CORREO_POSTAL("4"),
    PERSONAL("5");

    private String valor;

    private ETramiteDocumentoMetoComu(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

}
