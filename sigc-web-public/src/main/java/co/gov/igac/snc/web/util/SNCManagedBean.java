package co.gov.igac.snc.web.util;

import co.gov.igac.snc.util.Constantes;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.controller.AbstractLocator;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.EntradaProyeccionMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.FichaMatrizMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import org.primefaces.context.RequestContext;

/*
 * @modified pedro.garcia 30-08-2012 Cambio de nombre de la clase (para no confundirla con la de
 * faces)
 */
public class SNCManagedBean extends AbstractLocator {

    /**
     *
     */
    private static final long serialVersionUID = -1971839772790505202L;
    /**
     * Variables especificas para el codigo estructurado y asignarle los valores del buscador
     */
    //DetalleCodigoEstructurado detalleCodigoEstructurado = new DetalleCodigoEstructurado();

    protected final String DEFAULT_COMBOS = "Seleccionar...";

    protected final String DEFAULT_COMBOS_TODOS = "Todos";

    private static final Logger LOGGER = LoggerFactory.getLogger(SNCManagedBean.class);

    public SNCManagedBean() {

    }

    /**
     *
     */
    public void invalidarSesion() {
        LOGGER.debug("invalidarSesion");
        try {
            // clearKeepAlive();
            // clearView();
            HttpSession sesion = (HttpSession) FacesContext.getCurrentInstance().
                getExternalContext().getSession(false);
            if (sesion != null) {
                sesion.removeAttribute("user");
                sesion.invalidate();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void addMensajeError(Exception exception) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
            FacesMessage.SEVERITY_ERROR, exception.getMessage(), exception.toString()));
    }

    public void addMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
            FacesMessage.SEVERITY_ERROR, mensaje, null));
    }

    public void addMensajeErrorWithCallback(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
            FacesMessage.SEVERITY_ERROR, mensaje, null));
        this.addErrorCallback();
    }

    public void addMensajeError(String idComponente, String mensaje) {
        FacesContext.getCurrentInstance().addMessage(idComponente, new FacesMessage(
            FacesMessage.SEVERITY_ERROR, mensaje, null));
    }

    public void addMensajeInfo(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
            FacesMessage.SEVERITY_INFO, mensaje, mensaje));
    }

    public void addMensajeInfo(String idComponente, String mensaje) {
        FacesContext.getCurrentInstance().addMessage(idComponente, new FacesMessage(
            FacesMessage.SEVERITY_INFO, mensaje, null));
    }

    public void addMensajeWarn(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
            FacesMessage.SEVERITY_WARN, mensaje, null));
    }

    public void addMensajeWarn(String idComponente, String mensaje) {
        FacesContext.getCurrentInstance().addMessage(idComponente, new FacesMessage(
            FacesMessage.SEVERITY_WARN, mensaje, null));
    }

    public void resetearUIInput(String[] idsUiInput) {
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        UIComponent comp = null;
        for (int i = 0; i < idsUiInput.length; i++) {
            comp = viewRoot.findComponent(idsUiInput[i]);
            if (comp != null && comp instanceof UIInput) {
                ((UIInput) comp).resetValue();
            }
        }
    }

    public void removerBeanKeepAlive(String bean) {
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        Map<String, Object> atributos = viewRoot.getAttributes();
        atributos.remove("org.ajax4jsf.viewbean:" + bean);
    }

    public void removerAtributoDeUIViewRoot(String atributo) {
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        Map<String, Object> atributos = viewRoot.getAttributes();
        atributos.remove(atributo);
    }

    public String getInitParameter(String parameterName) {
        return FacesContext.getCurrentInstance().getExternalContext().
            getInitParameter(parameterName);
    }

    public Map<String, String> getRequestParameterMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }

    public Map<String, Object> getRequestMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
    }

    public Map<String, Object> getSessionMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    public Map<String, Object> getApplicationMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getApplicationMap();
    }

    public Object getRequestAttribute(String attributeName) {
        Map<String, Object> map = getRequestMap();
        return map.get(attributeName);
    }

    public void setRequestAttribute(String attributeName, Object attributeValue) {
        Map<String, Object> map = getRequestMap();
        map.put(attributeName, attributeValue);
    }

    public Object getSessionAttribute(String attributeName) {
        Map<String, Object> map = getSessionMap();
        return map.get(attributeName);
    }

    public void setSessionAttribute(String attributeName, Object attributeValue) {
        Map<String, Object> map = getSessionMap();
        map.put(attributeName, attributeValue);
    }

    public String getRequestParameter(String parameterName) {
        return getRequestParameterMap().get(parameterName);
    }

    public void setRequestParameter(String parameterName, String parameterValue) {
        getRequestParameterMap().put(parameterName, parameterValue);
    }

    public SelectItem getSelectItemPorValue(Object value, List<SelectItem> items) {
        SelectItem item = null;
        for (SelectItem selectItem : items) {
            if (value.equals(selectItem.getValue())) {
                item = selectItem;
                break;
            }
        }
        return item;
    }

    public boolean getErrores() {
        FacesContext fc = FacesContext.getCurrentInstance();
        if (fc.getMaximumSeverity() == null) {
            return false;
        } else if (fc.getMaximumSeverity().equals(FacesMessage.SEVERITY_ERROR) || fc.
            getMaximumSeverity().equals(FacesMessage.SEVERITY_FATAL)) {
            return true;
        } else {
            return false;
        }

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Obtiene el token actualizado de seguridad para AGS
     *
     * @return
     */
    public String getTokenParam() {
        String token = "vacio";
        /* try { token = MenuMB.getMenu().getUsuarioWithGISAuthorization().getSigToken().getToken().
         * toString(); } catch(Exception ex) { LOGGER.error("No se pudo obtener el token de
         * seguridad de SIG: " + ex.getMessage()); token = "";
        } */
        return token;
    }

    public void setTokenParam() {
        //Vacío para cumplir con el estándar
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Lanza un error de servidor para que el llamado ajax falle y se pueda usar el atributo
     * 'onError' de los componentes de primefaces
     *
     * @author pedro.garcia
     */
    public void throwErrorForAjax(String cause) {
        LOGGER.debug("lanzando error");
        try {
            FacesContext.getCurrentInstance().getExternalContext().responseSendError(500,
                "error lanzado: " + cause);
        } catch (IOException ex) {
            LOGGER.error("error lanzando error");
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Action del botón "cerrar" de la página principal de algún CU Método encargado de cerrar la
     * páginc del CU, terminar la sesión y redireccionar a la lista de tareas
     *
     * @author pedro.garcia
     */
    public String cerrarVentanaCUIrAListaTareas() {

        UtilidadesWeb.removerManagedBeans();

        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------

    public String getDEFAULT_COMBOS() {
        return this.DEFAULT_COMBOS;
    }

    /**
     * Agrega un evento de error al contexto necsario para bloquear acciones de elemnetos emergentes
     * cuando se presenta un error
     *
     * @author felipe.cadena
     */
    public void addErrorCallback() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT, Constantes.ERROR_REQUEST_CONTEXT);
    }

    /**
     * Retorna una instancia del MB generales
     *
     * @author felipe.cadena Retorna el MB de proyeccion
     *
     * felipe.cadena
     * @return
     */
    public ProyectarConservacionMB getProyectarConservacionMB() {
        return (ProyectarConservacionMB) UtilidadesWeb.getManagedBean("proyectarConservacion");
    }

    /**
     * Retorna el MB de proyeccion
     *
     * felipe.cadena
     *
     * @return
     */
    public EntradaProyeccionMB getEntradaProyeccionMB() {
        return (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
    }

    /**
     * Retorna el MB de temas generales
     *
     * felipe.cadena
     *
     * @return
     */
    public GeneralMB getGeneralMB() {
        return (GeneralMB) UtilidadesWeb.getManagedBean("general");
    }

    /**
     * Retorna el MB de temas ficha matriz
     *
     * leidy.gonzalez
     *
     * @return
     */
    public FichaMatrizMB getFichaMatrizMB() {
        return (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
    }

    /**
     * Procesa mensajes retornados por un procedimiento almacenado de base de datos recibe un
     * mensaje que se muestra cuado el procedimiento no genero erroes
     *
     * @author felipe.cadena
     * @param mensajes
     */
    public void procesarProcedimientoDB(Object[] mensajes, String mensajeOk) {

        if (mensajes == null || mensajes.length == 0 || ((List) mensajes[0]).isEmpty()) {
            this.addMensajeInfo(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(mensajeOk));
        } else {
            String[] errores = UtilidadesWeb.extraerMensajeSP(mensajes);
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(errores[0]));
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                Constantes.ERROR_REQUEST_CONTEXT);
            LOGGER.warn(ECodigoErrorVista.ERROR_DB_PROC.getMensajeTecnico(errores[1]));
        }

    }

}
