package co.gov.igac.snc.web.mb.tramite;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.impl.procesador.ProcesadorDocumentosImpl;
import co.gov.igac.sigc.documental.interfaces.IProcesadorDocumentos;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaCancelarTramites;
import co.gov.igac.snc.util.ValidacionCancelacionTramiteDTO;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("cancelarTramites")
@Scope("session")
public class CancelacionTramitesMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 7232235271496584561L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CancelacionTramitesMB.class);

    // **************** SERVICIOS ********************
    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private IContextListener contexto;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    // ------------------------------------------------------------------------
    private FiltroDatosConsultaCancelarTramites filtro;

    private LazyDataModel<Tramite> tramiteLazyModel;

    private List<SelectItem> mutacionClases;

    private List<SelectItem> tramiteSubtipos;

    /**
     * Esta variable permite continuar con el proceso de cancelaciòn, siempre que no exista un
     * tramite que sea de otra regional.
     */
    private boolean procesarCancelacion;

    /**
     * Usuario actual del sistema
     */
    private UsuarioDTO usuario;

    /**
     * lista de tramites paginados
     */
    private List<Tramite> rows;

    /**
     * Trámites seleccionados
     */
    private Tramite[] selectedTramites;

    /**
     * Ruta del documento cargado
     */
    private String rutaMostrar;

    /**
     * Archivo a cargar en la carpeta temporal
     */
    private File archivoResultado;

    /**
     * TrámiteEstado asociado al tramite selecionado
     */
    private TramiteEstado selectedTramiteEstado;

    private Documento documentoMostrar;

    /**
     * Variable que contiene el número de radicado de correspondencia asociado al documento de
     * cancelación de trámite
     */
    private String numeroRadicado;

    // ------------------------------ Banderas -----------------------------
    /**
     * Bandera que visualiza la tabla de resultados de la busqueda
     */
    private boolean banderaBusquedaTramites;

    /**
     * Bandera que visualiza los datos del bloqueo si se selecciono un trámite para bloquear
     */
    private boolean banderaTramiteSeleccionado;

    private boolean banderaDocumentoCargado;

    private boolean banderaMutacion;
    private boolean banderaTramiteSubtipo;

    private String tipoMimeDocTemporal;

    /**
     * Objeto que contiene los datos del reporte de el oficio de no procedencia
     */
    private ReporteDTO oficioCancelacion;

    /**
     * lista de usuarios con el rol de responsables de conservación
     */
    private List<UsuarioDTO> responsablesConservacion;

    /**
     * Bandera que indica si el documento ha sido radicado
     */
    private boolean banderaDocumentoRadicado;

    /**
     * Lista que almacena los tramites con radicacion cerrada exitosa
     */
    private List<Tramite> radicacionesCerradasExitosas;

    /**
     * Determina si el tramite seleccionado tiene trámites asociados
     */
    private boolean banderaTramitesAsociados;

    /**
     * Bandera para determinar si un tramite es valido para cancelar.
     */
    private boolean banderaCancelarTramite;

    /**
     * Lista de errores que impiden que un trámite se pueda cancelar
     */
    private List<ValidacionCancelacionTramiteDTO> listaValidacionesCancelacionTramite;

    /**
     * Valor de la cantidad màxima de trámites que pueden ser seleccionados para cancelar
     */
    private int cantidadMaximaTramitesACancelar;

    /**
     * Objeto que contiene los reportes unidos de los trámites cancelados
     */
    private ReporteDTO reporteUnidoCancelacionTramites;
// ---------------------------- Init --------------------------------

    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.banderaDocumentoCargado = false;
        this.banderaMutacion = false;
        this.banderaTramiteSubtipo = false;
        this.setFiltro(new FiltroDatosConsultaCancelarTramites());

        Parametro p = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.MAX_CANTIDAD_TRAMITES_A_CANCELAR.name());
        this.cantidadMaximaTramitesACancelar = p.getValorNumero().intValue();
        this.procesarCancelacion = true;
    }

    // ---------------------------- Methods --------------------------------
    public CancelacionTramitesMB() {

    }

    public FiltroDatosConsultaCancelarTramites getFiltro() {
        return filtro;
    }

    public boolean isBanderaCancelarTramite() {
        return banderaCancelarTramite;
    }

    public void setBanderaCancelarTramite(boolean banderaCancelarTramite) {
        this.banderaCancelarTramite = banderaCancelarTramite;
    }

    public boolean isBanderaTramitesAsociados() {
        return banderaTramitesAsociados;
    }

    public void setBanderaTramitesAsociados(boolean banderaTramitesAsociados) {
        this.banderaTramitesAsociados = banderaTramitesAsociados;
    }

    public boolean isBanderaBusquedaTramites() {
        return banderaBusquedaTramites;
    }

    public void setBanderaBusquedaTramites(boolean banderaBusquedaTramites) {
        this.banderaBusquedaTramites = banderaBusquedaTramites;
    }

    public Tramite[] getSelectedTramites() {
        return selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public boolean isBanderaTramiteSeleccionado() {
        return banderaTramiteSeleccionado;
    }

    public void setBanderaTramiteSeleccionado(boolean banderaTramiteSeleccionado) {
        this.banderaTramiteSeleccionado = banderaTramiteSeleccionado;
    }

    public void setFiltro(FiltroDatosConsultaCancelarTramites filtro) {
        this.filtro = filtro;
    }

    public LazyDataModel<Tramite> getTramiteLazyModel() {
        return tramiteLazyModel;
    }

    public void setTramiteLazyModel(LazyDataModel<Tramite> tramiteLazyModel) {
        this.tramiteLazyModel = tramiteLazyModel;
    }

    public List<SelectItem> getMutacionClases() {
        return mutacionClases;
    }

    public void setMutacionClases(List<SelectItem> mutacionClases) {
        this.mutacionClases = mutacionClases;
    }

    public TramiteEstado getSelectedTramiteEstado() {
        return selectedTramiteEstado;
    }

    public void setSelectedTramiteEstado(TramiteEstado selectedTramiteEstado) {
        this.selectedTramiteEstado = selectedTramiteEstado;
    }

    public List<SelectItem> getTramiteSubtipos() {
        return this.tramiteSubtipos;
    }

    public void setTramiteSubtipos(List<SelectItem> tramiteSubtipos) {
        this.tramiteSubtipos = tramiteSubtipos;
    }

    public boolean isBanderaDocumentoCargado() {
        return this.banderaDocumentoCargado;
    }

    public void setBanderaDocumentoCargado(boolean banderaDocumentoCargado) {
        this.banderaDocumentoCargado = banderaDocumentoCargado;
    }

    public boolean isBanderaMutacion() {
        return this.banderaMutacion;
    }

    public void setBanderaMutacion(boolean banderaMutacion) {
        this.banderaMutacion = banderaMutacion;
    }

    public boolean isBanderaTramiteSubtipo() {
        return this.banderaTramiteSubtipo;
    }

    public void setBanderaTramiteSubtipo(boolean banderaTramiteSubtipo) {
        this.banderaTramiteSubtipo = banderaTramiteSubtipo;
    }

    public String getTipoMimeDocTemporal() {
        return this.tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public String getRutaMostrar() {
        return this.rutaMostrar;
    }

    public Documento getDocumentoMostrar() {
        return this.documentoMostrar;
    }

    public void setDocumentoMostrar(Documento documentoMostrar) {
        this.documentoMostrar = documentoMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public String getNumeroRadicado() {
        return this.numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public ReporteDTO getReporteUnidoCancelacionTramites() {
        return this.reporteUnidoCancelacionTramites;
    }

    public void setReporteUnidoCancelacionTramites(ReporteDTO reporteUnidoCancelacionTramites) {
        this.reporteUnidoCancelacionTramites = reporteUnidoCancelacionTramites;
    }

    public boolean isBanderaDocumentoRadicado() {
        return this.banderaDocumentoRadicado;
    }

    public List<Tramite> getRadicacionesCerradasExitosas() {
        return this.radicacionesCerradasExitosas;
    }

    public List<ValidacionCancelacionTramiteDTO> getListaValidacionesCancelacionTramite() {
        return listaValidacionesCancelacionTramite;
    }

    public void setListaValidacionesCancelacionTramite(
        List<ValidacionCancelacionTramiteDTO> listaValidacionesCancelacionTramite) {
        this.listaValidacionesCancelacionTramite = listaValidacionesCancelacionTramite;
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado en el boton cargar documento, para adjuntar el documento de soporte de
     * cancelación de trámite
     */
    /*
     * @modified leidy.gonzalez #10683 22-12-2014 :: cambio de caracteres especiales por espacion
     */
    public void handleFileUpload(FileUploadEvent eventoCarga) {

        String regex = "[^_0-9a-zA-ZñÑáéíóúÁÉÍÓÚ ,\\.\\-]";
        this.rutaMostrar = eventoCarga.getFile().getFileName().replaceAll(regex, "");

        try {
            String directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();

            this.archivoResultado = new File(directorioTemporal, this.rutaMostrar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();
            this.banderaDocumentoCargado = true;
            this.documentoMostrar = new Documento();
            this.documentoMostrar.setArchivo(this.archivoResultado.getPath());
            this.addMensajeInfo("Proceso exitoso: archivo '" +
                 this.rutaMostrar +
                 "' cargado exitosamente.");

        } catch (IOException e) {
            this.banderaDocumentoCargado = false;
            String mensaje = "El archivo seleccionado no pudo ser cargado en" +
                 " el sistema. Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
            LOGGER.error("Ocurrió un error en el método CancelacionTramitesMB#handleFileUpload", e);
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado en el boton buscar para encontrar trámites por filtro de busqueda
     */
    public void buscar() {

        boolean validacionParametrosMinimosBusqueda = validarParametrosMinimosBusqueda();
        if (validacionParametrosMinimosBusqueda) {

            if (this.filtro.getTipoTramite().equals(Constantes.NULL_TEXT)) {
                this.filtro.setTipoTramite(null);
            }
            this.filtro.setEsVisibleMnpios(true);

            this.tramiteLazyModel = new LazyDataModel<Tramite>() {

                /**
                 *
                 */
                private static final long serialVersionUID = 8330862702403692993L;

                @Override
                public List<Tramite> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<Tramite> answer = new ArrayList<Tramite>();
                    populateTramitesLazyly(answer, first, pageSize);

                    return answer;
                }
            };
            poblar();
            this.selectedTramiteEstado = new TramiteEstado();
            this.selectedTramiteEstado.setFechaInicio(new Date(System
                .currentTimeMillis()));

        } else {
            this.banderaBusquedaTramites = false;
        }

        this.banderaBusquedaTramites = true;
        this.banderaTramiteSeleccionado = false;
        this.selectedTramites = null;

    }

//--------------------------------------------------------------------------------------------------
    public void poblar() {

        int count;
        this.filtro.setEsVisibleMnpios(true);
        count = getTramiteService().contarResultadosBusquedaTramiteCancelacion(
            this.usuario.getCodigoEstructuraOrganizacional(),
            this.filtro);

        if (count == 0) {
            this.filtro.setEsVisibleMnpios(false);
            count = getTramiteService().contarResultadosBusquedaTramiteCancelacion(
                this.filtro);
            if (count == -1) {
                this.addMensajeError(
                    "La unidad operativa no esta registrada en la estructura organizacional del sistema");
            }
        }

        if (count > 0) {
            this.tramiteLazyModel.setRowCount(count);
        } else {
            this.tramiteLazyModel.setRowCount(0);
        }
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified by juanfelipe.garcia 27-06-2013 adicion de restriccion para no mostrar tramites
     * IPER @modified by cesar.vega 31/03/2017. Sobrecarga del método buscarTramitesParaCancelacion
     * para traer trámites de otra Territorial a la del FuncionarioEjecutor
     */
    private void populateTramitesLazyly(List<Tramite> answer, int first,
        int pageSize) {

        rows = new ArrayList<Tramite>();

        int size;

        for (Tramite t : this.getTramiteService().buscarTramitesParaCancelacion(
            this.usuario.getCodigoEstructuraOrganizacional(), filtro,
            first, pageSize)) {
            if (!Constantes.FUNCIONARIO_EJECUTOR_IPER.equals(t.getFuncionarioEjecutor())) {
                rows.add(t);
            }

        }
        size = rows.size();
        if (size == 0) {
            // El funcionario radicador pertenece a un Depto-Mnpio diferente al del Trámite
            this.filtro.setEsVisibleMnpios(false);

            for (Tramite t : this.getTramiteService().buscarTramitesParaCancelacion(
                filtro, first, pageSize)) {
                if (!Constantes.FUNCIONARIO_EJECUTOR_IPER.equals(t.getFuncionarioEjecutor())) {
                    rows.add(t);
                }
            }
        }

        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida que por lo menos se incluya un parametro en la busqueda
     */
    private boolean validarParametrosMinimosBusqueda() {

        if (this.filtro.getNumeroSolicitud() != null &&
             !this.filtro.getNumeroSolicitud().isEmpty()) {
            return true;
        } else if (this.filtro.getTipoTramite() != null &&
             !this.filtro.getTipoTramite().isEmpty() &&
             !this.filtro.getTipoTramite().equals(Constantes.NULL_TEXT)) {
            return true;
        } else if (this.filtro.getNumeroRadicacion() != null &&
             !this.filtro.getNumeroRadicacion().isEmpty()) {
            return true;
        } else if (this.filtro.getNumeroIdentificacion() != null &&
             !this.filtro.getNumeroIdentificacion().isEmpty()) {
            return true;
        } else if (this.filtro.getDepartamentoNumPredial() != null &&
             !this.filtro.getDepartamentoNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getMunicipioNumPredial() != null &&
             !this.filtro.getMunicipioNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getZonaNumPredial() != null &&
             !this.filtro.getZonaNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getSectorNumPredial() != null &&
             !this.filtro.getSectorNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getComunaNumPredial() != null &&
             !this.filtro.getComunaNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getBarrioNumPredial() != null &&
             !this.filtro.getBarrioNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getManzanaNumPredial() != null &&
             !this.filtro.getManzanaNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getTerrenoNumPredial() != null &&
             !this.filtro.getTerrenoNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getCondicionPropiedadNumPredial() != null &&
             !this.filtro.getCondicionPropiedadNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getEdificioNumPredial() != null &&
             !this.filtro.getEdificioNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getPisoNumPredial() != null &&
             !this.filtro.getPisoNumPredial().isEmpty()) {
            return true;
        } else if (this.filtro.getUnidadNumPredial() != null &&
             !this.filtro.getUnidadNumPredial().isEmpty()) {
            return true;
        }

        String mensaje = "La busqueda debe contener por lo menos un parámetro." +
             " Por favor corrija los datos e intente nuevamente.";
        this.addMensajeError(mensaje);
        return false;
    }

//--------------------------------------------------------------------------------------------------
    public void changeTipoTramite() {
        this.mutacionClases = new ArrayList<SelectItem>();
        this.tramiteSubtipos = new ArrayList<SelectItem>();
        if (filtro.getTipoTramite() != null &&
             filtro.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo())) {
            this.banderaMutacion = true;
            this.mutacionClases.add(new SelectItem("", this.DEFAULT_COMBOS));
            this.mutacionClases.addAll(this.generalMB.getMutacionClaseV());
        } else if (filtro.getTipoTramite() != null &&
             !filtro.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo())) {
            this.banderaMutacion = false;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor en el combo de tipo de trámite
     */
    public void changeClaseMutacion() {
        this.tramiteSubtipos = new ArrayList<SelectItem>();

        if (filtro.getClaseMutacion() != null &&
             filtro.getClaseMutacion().equals(
                EMutacionClase.SEGUNDA.getCodigo())) {
            this.banderaTramiteSubtipo = true;
            this.tramiteSubtipos.add(new SelectItem("", this.DEFAULT_COMBOS));
            this.tramiteSubtipos.addAll(this.generalMB.getSubtipoMutacionSClase());
        }

        if (filtro.getClaseMutacion() != null &&
             filtro.getClaseMutacion().equals(
                EMutacionClase.QUINTA.getCodigo())) {
            this.banderaTramiteSubtipo = true;
            this.tramiteSubtipos.add(new SelectItem("", this.DEFAULT_COMBOS));
            this.tramiteSubtipos.addAll(this.generalMB.getSubtipoMutacionQClase());
        }

        if (filtro.getClaseMutacion() != null &&
             !(filtro.getClaseMutacion().equals(
                EMutacionClase.QUINTA.getCodigo()) ||
            filtro.getClaseMutacion().equals(
                EMutacionClase.SEGUNDA.getCodigo()))) {
            this.banderaTramiteSubtipo = false;
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado para cerrar cancelar trámites
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("cancelarTramites");
        return "index";
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado para al seleccionar un trámite para cancelar
     */
    /*
     * modified by javier.aponte :: #17765 Se realizan modificaciones para que las validaciones se
     * hagan a varios trámites que se pueden cancelar masivamente :: 13/06/2015
     */
    public void cancelarTramites() {
        this.banderaTramiteSeleccionado = true;
        String territorial = "";
        Actividad act = null;

        this.procesarCancelacion = true;

        if (this.selectedTramites.length > this.cantidadMaximaTramitesACancelar) {
            this.addMensajeError("El sistema sólo permite cancelar máximo " +
                this.cantidadMaximaTramitesACancelar +
                 " trámites, por favor corrija los datos e intente nuevamente");
            return;
        }

        for (int i = 0; i < this.selectedTramites.length; i++) {
            this.selectedTramites[i] = this.getTramiteService().buscarTramitePorId(
                this.selectedTramites[i].getId());
        }
        this.rutaMostrar = new String();

        this.consultarTramitesAsociados();

        if (!this.banderaCancelarTramite) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

        for (Tramite tramiteTemp : this.selectedTramites) {

            if (tramiteTemp.getTipoTramite().equals(
                ETramiteTipoTramite.AUTOESTIMACION.getCodigo())) {
                act = this.obtenerActividadActualTramiteNumerosolicitud(tramiteTemp.getSolicitud().
                    getNumero());
            } else {
                act = this.obtenerActividadActualTramite(tramiteTemp.getNumeroRadicacion(),
                    tramiteTemp.getId());
            }

            if (act != null && !(act.getNombre().equals(
                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES) ||
                act.getNombre().equals(ProcesoDeConservacion.ACT_DIGITALIZACION_ESCANEAR) ||
                act.getNombre().equals(
                    ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO))) {

                territorial = this.getGeneralesService().
                    getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                        tramiteTemp.getMunicipio().getCodigo());

                if (!this.usuario.getDescripcionEstructuraOrganizacional().equals(territorial)) {

                    this.agregarMensajeValidacionCancelacionTramite(tramiteTemp.
                        getNumeroRadicacion(),
                        "El trámite a cancelar pertenece a la Territorial o UOC Radicadora " +
                        territorial);
                    this.procesarCancelacion = false;

                }
            }

        }

        if (!this.procesarCancelacion) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado para cancelar un trámite
     */
    /*
     * @modified juanfelipe.garcia :: 03-05-2013 :: Adicion de logica CU-NP-CO-178 @modified
     * felipe.cadena :: 22-08-2014 :: Se agrega la logica para la cancelacion de los tramites
     * asociados 1. Radica en CORDIS y genera el documento de cancelación 2. Cambia el estado en
     * trámite y crea el registro de trámite estado 3. Elimina la proyección en la base de datos 4.
     * Avanza la actividad en el process.
     *
     * @modified by javier.aponte :: #17765 Se realizan modificaciones para realizar la cancelación
     * masiva de trámites :: 13/06/2015
     */
    public void confirmAction() {

        String mensaje;
        Documento documento;
        List<Tramite> tramitesAsociadosAlSeleccionado;
        List<String> rutasDocumentosCancelacion = new ArrayList<String>();

        for (Tramite tramiteTemp : this.selectedTramites) {

            if (this.selectedTramiteEstado.getMotivo().length() >
                Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO) {
                this.addMensajeError("El tamaño del texto del motivo no debe ser superior a " +
                     Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO +
                     " caracteres");
                return;
            }

            if (this.archivoResultado != null) {

                documento = new Documento();
                TipoDocumento td = new TipoDocumento();

                td.setId(ETipoDocumento.DOCUMENTO_SOPORTE_DE_CANCELACION_DE_TRAMITE.getId());
                documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                documento.setTipoDocumento(td);
                documento.setBloqueado(ESiNo.NO.getCodigo());
                documento.setArchivo(this.rutaMostrar);

                //Generar documento de cancelación de trámite catastral.
                //javier.aponte :: refs#13653 Ahora se genera el documento de cancelación antes para garantizar que
                //se genera correctamente; antes de hacer todo lo correspondiente a base de datos :: 30/07/2015
                try {
                    this.radicar(tramiteTemp);
                    if (this.oficioCancelacion != null) {
                        rutasDocumentosCancelacion.add(this.oficioCancelacion.
                            getRutaCargarReporteAAlfresco());
                    }
                } catch (Exception ex) {
                    mensaje =
                        "Ocurrió un error al radicar o al generar el reporte. Para el trámite con número de radicación " +
                         tramiteTemp.getNumeroRadicacion() +
                         ". Por favor intente nuevamente o comuníquese con el administrador";
                    LOGGER.error(
                        "Error al radicar o al generar el reporte del documento de cancelación del trámite con id: " +
                         tramiteTemp.getId() + ": " + ex);
                    this.addMensajeError(mensaje);
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                        Constantes.ERROR_REQUEST_CONTEXT);
                    return;
                }

                try {
                    this.cancelarTramiteIndividual(tramiteTemp, documento, false);
                } catch (ExcepcionSNC e) {
                    this.addMensajeError(
                        "Ocurrió un error al radicar o al generar el reporte. Para el trámite con número de radicación " +
                         tramiteTemp.getNumeroRadicacion() + ":" + e.getMensaje() +
                         ". Por favor intente nuevamente o comuníquese con el administrador");
                    LOGGER.error(e.getMensaje());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                        Constantes.ERROR_REQUEST_CONTEXT);
                    return;
                }

                tramitesAsociadosAlSeleccionado = this.getTramiteService().
                    consultarTramitesAsociadosATramiteId(tramiteTemp.getId());

                //felipe.cadena::Cancelacion de tramites asociados
                for (Tramite tas : tramitesAsociadosAlSeleccionado) {
                    this.cancelarTramiteIndividual(tas, documento, true);
                }

            } else {
                this.addMensajeError(
                    "Debe anexar un documento como soporte de la cancelación del trámite.");
                return;
            }

        }

        if (!rutasDocumentosCancelacion.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.
                unirArchivosPDF(rutasDocumentosCancelacion);
            this.reporteUnidoCancelacionTramites =
                this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

        this.init();
        this.inicializarDatosMB();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que realiza las acciones correspondientes para cancelar el trámite
     *
     * @param tramite
     * @param Documento
     * @param isAsociado varibale booleana para determinar si el trámite es asociado o no
     * @author javier.aponte
     */
    public void cancelarTramiteIndividual(Tramite tramite, Documento documento, boolean isAsociado) {

        String mensaje;
        boolean resultado;
        String observaciones = "", processTransition = "";
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();

        usuariosActividad = new ArrayList<UsuarioDTO>();

        this.selectedTramiteEstado.setTramite(tramite);
        documento.setTramiteId(tramite.getId());

        resultado = this.getTramiteService().cancelarTramite(
            tramite, this.selectedTramiteEstado,
            this.usuario, documento);
        //En el gestor de procesos sólo se cancelan trámites que no son asociados, es decir, los trámites padre :: 18/07/2016 :: javier.aponte
        if (!isAsociado) {
            boolean resultadoAvanzarProcess = this.getTramiteService().cancelarTramiteEnProcess(
                    tramite, this.selectedTramiteEstado.getMotivo());
            if (!resultadoAvanzarProcess) {
                mensaje = "PR001: Ocurrió un error al cancelar el trámite, "
                        + "con número de radicación: " + tramite.getNumeroRadicacion()
                        + " por favor comuníquese con la mesa de ayuda con el código PR001";
                this.addMensajeError(mensaje);
                LOGGER.error(
                        "BD001: Ocurrió un error al cancelar el trámite en el process, para el trámite "
                        + tramite.getId());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                        Constantes.ERROR_REQUEST_CONTEXT);
                return;
            }
        }

        if (resultado) {

            boolean resultadoReversarProyeccion = this.getTramiteService().reversarProyeccion(
                tramite);
            if (!resultadoReversarProyeccion) {
                mensaje = "BD001: Ocurrió un error al cancelar el trámite, " +
                     "con número de radicación: " + tramite.getNumeroRadicacion() +
                     " por favor comuníquese con la mesa de ayuda con el código BD001";
                this.addMensajeError(mensaje);
                LOGGER.error(
                    "BD001: Ocurrió un error al reversar la proyección en la base de datos, para el trámite " +
                    tramite.getId());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
                return;
            }
            mensaje = "El trámite con número de radicación " +
                 tramite.getNumeroRadicacion() +
                 " se canceló exitosamente.";
            this.addMensajeInfo(mensaje);

        } else {
            mensaje =
                "Ocurrió un error inesperado al cancelar el trámite con número de radicación " +
                tramite.getNumeroRadicacion() +
                 ". Por favor intente nuevamente o comuníquese con el administrador";
            this.addMensajeError(mensaje);
        }

        //Si e ltrámite cancelado es de vía gubernativa se reanuda la ultima actividad del trámite
        //referencia y se avanza a aplicar cambios
        //se cambió al usuario de la sesión, para que quede el registro en process 
        //de quien fue el responsable de terminar la ejecución del tramite
        usuarioDestinoActividad.setLogin(this.usuario.getLogin());
        usuariosActividad.add(usuarioDestinoActividad);

        solicitudCatastral.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        solicitudCatastral.setUsuarios(usuariosActividad);

        if (tramite.isViaGubernativa() || tramite.getSolicitud().isViaGubernativa()) {
            Tramite tramiteReferencia = this.getTramiteService().
                buscarTramitePorNumeroRadicacion(tramite.getNumeroRadicacionReferencia());

            List<Actividad> actividades = this.getProcesosService().
                getActividadesPorIdObjetoNegocio(tramiteReferencia.getId());

            try {
                if (tramiteReferencia.getEstado().equals(ETramiteEstado.RECIBIDO.getCodigo())) {

                    Actividad act = actividades.get(0);

                    //Se reanuda la actividad del trámite referencia de la vía gubernativa
                    this.getProcesosService().reanudarActividad(act.getId());

                    //Se avanza el trámite a aplicar cambios
                    UsuarioDTO responsableConservacion;
                    responsableConservacion = this.getTramiteService().
                        buscarFuncionariosPorRolYTerritorial(
                            usuario.getDescripcionEstructuraOrganizacional(),
                            ERol.RESPONSABLE_CONSERVACION).get(0);
                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);
                    getProcesosService().avanzarActividad(responsableConservacion, act.getId(),
                        solicitudCatastral);
                }
            } catch (Exception ex) {
                mensaje = ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(tramiteReferencia.
                    getNumeroRadicacion());
                this.addMensajeWarn(mensaje);
                LOGGER.error(ECodigoErrorVista.ACT_RECLAMADA.getMensajeTecnico());
            }
        }
    }

//--------------------------------------------------------------------------------------------------        
    /*
     * @author juanfelipe.garcia @throws Exception
     */
 /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarReporteCancelacion(Long tramiteTempId) throws Exception {
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.CANCELACION_TRAMITE;
        FirmaUsuario firma;
        String documentoFirma;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", String.valueOf(tramiteTempId));

        UsuarioDTO responsableConservacion = null;

        if (this.responsablesConservacion == null) {
            this.responsablesConservacion = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);
        }

        if (this.responsablesConservacion != null &&
             !this.responsablesConservacion.isEmpty() &&
             this.responsablesConservacion.get(0) != null) {
            responsableConservacion = this.responsablesConservacion.get(0);
        }

        if (responsableConservacion != null &&
             responsableConservacion.getNombreCompleto() != null) {
            parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                responsableConservacion.getNombreCompleto());
        }

        if (this.numeroRadicado != null) {
            parameters.put("NUMERO_RADICADO", this.numeroRadicado);
        }

        parameters.put("MOTIVO_CANCELACION", this.selectedTramiteEstado.getMotivo());

        if (responsableConservacion != null && responsableConservacion.getNombreCompleto() != null) {

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                responsableConservacion.getNombreCompleto());

            if (firma != null && this.usuario.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                parameters.put("FIRMA_USUARIO", documentoFirma);
            } else {
                this.addMensajeInfo("El usuario: " + responsableConservacion.getNombreCompleto() +
                     " no tiene firma de usuario asociada.");
            }
        }

        this.oficioCancelacion = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);
    }
//--------------------------------------------------------------------------------------------------        

    /*
     * @modified juanfelipe.garcia :: 03-05-2013 :: Adicion de logica CU-NP-CO-176
     */
    public void radicar(Tramite tramiteTemp) throws Exception {

        Documento documentoCancelacion;

        try {

            // 1. Generación del número de Radicación en correspondencia
            // (sticker de radicación)
            //v1.1.7
            documentoCancelacion = this.generarRadicadoYGuardarDocumento(tramiteTemp.getId(),
                tramiteTemp.getSolicitud());
            if (documentoCancelacion == null) {
                LOGGER.error(
                    "CancelacionTramitesMB#radicar: Error al generar el número de radicado para el trámite: " +
                     tramiteTemp.getId());
                throw new Exception("No se pudo generar el número de radicado");
            }
            LOGGER.debug("Número Radicado: " + this.numeroRadicado);

            // 2. Generación del reporte con el número de radicación
            this.generarReporteCancelacion(tramiteTemp.getId());
            if (this.oficioCancelacion == null) {
                LOGGER.error(
                    "CancelacionTramitesMB#radicar: Error al generar el reporte de cancelación para el trámite: " +
                     tramiteTemp.getId());
                throw new Exception("No se pudo generar el reporte de cancelación");
            }

            //Guarda el documento en Alfresco y en la base de datos y lo asocia con
            //un trámite documento
            this.guardarDocumentoCancelacion(documentoCancelacion, tramiteTemp, true);

            //18779.  Cerrar trámite en cordis 15/09/2016 wilmanjose.vega
            final List<Tramite> tramitesSolicitud = getTramiteService().consultarTramitesSolicitud(
                tramiteTemp.getSolicitud().getId());
            if (estanTodosTramitesEstadoCierre(tramitesSolicitud, tramiteTemp)) {
                cerrarRadicacionTramite(tramiteTemp.getSolicitud().getNumero(),
                    this.numeroRadicado, documentoCancelacion.getUsuarioCreador());
            }

            //#18439 Se consulta los trámites asociados al trámite padre
            List<Tramite> tramitesAsociadosAlSeleccionado = this.getTramiteService().
                consultarTramitesAsociadosATramiteId(tramiteTemp.getId());

            for (Tramite tas : tramitesAsociadosAlSeleccionado) {
                //#18439 Se asocia el documento de cancelación del trámite padre a los trámites hijos :: 06/07/2016 :: javier.aponte
                this.guardarDocumentoCancelacion(documentoCancelacion, tas, false);
                //#18439 Se llama el método para el cierre de radicación de los trámites hijos :: 06/07/2016 :: javier.aponte
                this.cerrarRadicacionDelTramite(tas);
            }

            this.banderaDocumentoRadicado = true;

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error en el método CancelacionTramitesMB#radicar", e);
            throw new Exception("Ocurrió un error al radicar el oficio " + e.getMessage());
        }
    }

    /**
     * Método encargado de realizar el cierre de radicación cuando se cancela un trámite
     *
     * @author javier.aponte
     */
    private void cerrarRadicacionDelTramite(Tramite tramite) {

        //Realizar el cierre de una radicación cuando se cancela un trámite.
        String numeroRadicacionOrigen;

        if (tramite.getTramite() != null) {
            numeroRadicacionOrigen = tramite.getTramite().getNumeroRadicacion();
        } else {
            numeroRadicacionOrigen = tramite.getNumeroRadicacion();
        }

        // número de radicación, generado anteriormente con el documento 
        String numeroRadicacionGeneracionDoc = this.numeroRadicado;

        // mensaje al cerrar la radicación
        String mensaje = "Trámite cancelado";

        // cerrar radicacion del tramite  
        radicarPares(numeroRadicacionOrigen, tramite.getNumeroRadicacion(),
            numeroRadicacionGeneracionDoc, mensaje);

    }

    /**
     * Método encargado de generar el número de radicado de cancelación del trámite, retorna un
     * documento con los datos del documento de cancelación.
     *
     * @author javier.aponte
     * @throws Exception
     */
    private Documento generarRadicadoYGuardarDocumento(Long tramiteTempId, Solicitud solicitudTemp)
        throws Exception {

        Documento documento;

        documento = this.getGeneralesService().buscarDocumentoPorTramiteIdyTipoDocumento(
            tramiteTempId,
            ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.getId());

        if (documento == null) {

            this.obtenerNumeroRadicado(solicitudTemp);

            if (this.numeroRadicado == null) {
                LOGGER.error(
                    "CancelacionTramitesMB#radicar: Error al generar el número de radicado para el trámite: " +
                     tramiteTempId);
                throw new Exception("No se pudo generar el número de radicación");
            }

        } else {
            this.numeroRadicado = documento.getNumeroRadicacion();
        }

        if (this.numeroRadicado != null && documento == null) {

            documento = new Documento();

            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.getId());

            documento.setTipoDocumento(tipoDocumento);
            documento.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(tramiteTempId);
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documento.setNumeroRadicacion(this.numeroRadicado);
            documento.setFechaRadicacion(new Date());

            //Se guarda el documento para no perder el número de resolución principalmente
            documento = this.getGeneralesService().guardarDocumento(documento);

        }

        return documento;
    }

//--------------------------------------------------------------------------------------------------    
    /*
     * @author juanfelipe.garcia
     */
//TODO :: juanfelipe.garcia :: 28-08-2013 :: documentar el método. Actualmente solo se usa dentro de este MB, luego debería ser privado :: pedro.garcia
    public boolean radicarPares(String origen, String numRadicacion, String numRadDocumento,
        String mensaje) {
        boolean resultado = false;
        try {
            Map<String, String> parametros = new HashMap<String, String>();
            parametros.put("ORIGEN_RADICACION", origen);
            parametros.put("NUMERO_RADICACION", numRadicacion);
            parametros.put("NUMERO_RADICACION_DOCUMENTO", numRadDocumento);
            parametros.put("MENSAJE", mensaje);
            resultado = true;
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error en el método CancelacionTramitesMB#radicarPares", e);
            this.addMensajeError("Ocurrió un error al radicar el oficio " + e.getMessage());
        }
        return resultado;
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private void obtenerNumeroRadicado(Solicitud solicitudTemp) {

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.
            getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Cancelación trámite"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        this.numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en el gestor
     * documental y en la base de datos mediante el llamado al método guardar documento de la
     * interfaz de ITramite. Posteriormente le asocia el documento al trámite documento creado al
     * principio. Metodo original de javier.aponte, ajustado para guardar documento de cancelacion
     *
     * @param Documento documento de la cancelación que se desea guardar
     * @param Tramite tramite al que se le va a asociar el documento de cancelación
     * @param actualizar variable que indica si se debe actualizar el documento de cancelación ó, en
     * el caso que se envíe en false se genera otro documento
     * @throws Exception
     * @author javier.aponte
     */
    public void guardarDocumentoCancelacion(Documento documento, Tramite tramiteTemp,
        boolean actualizar) throws Exception {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();

        tramiteDocumento.setTramite(tramiteTemp);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            if (!actualizar) {
                documento.setId(null);
            }
            documento.setTramiteId(tramiteTemp.getId());
            if (this.oficioCancelacion != null) {
                documento.setArchivo(this.oficioCancelacion.getRutaCargarReporteAAlfresco());
            }

            // Se sube el archivo al gestor documental y se actualiza el documento
            documento = this.getTramiteService().guardarDocumento(this.usuario, documento);

            if (documento != null &&
                 documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                this.getTramiteService().actualizarTramiteDocumento(tramiteDocumento);
            }

        } catch (Exception e) {
            throw new Exception("Error al guardar el oficio cancelacion en alfresco: " + e);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización
     *
     * @author javier.aponte
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();
        String fileSeparator = System.getProperty("file.separator");

        String tipoMime = "";
        try {
            tipoMime = this.tipoMimeDocTemporal = fileNameMap
                .getContentTypeFor(directorioTemporal + fileSeparator +
                     this.rutaMostrar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return tipoMime;
    }
    // -------------------------------------------------------------------------

    /**
     * Método que limpia los campos de busqueda
     *
     * @author javier.aponte
     */
    public void limpiarfiltro() {
        this.filtro = new FiltroDatosConsultaCancelarTramites();
        this.banderaMutacion = false;
        this.banderaTramiteSubtipo = false;
        this.tramiteLazyModel = null;
        this.banderaBusquedaTramites = false;
        this.banderaTramiteSeleccionado = false;
    }
    // -------------------------------------------------------------------------

    /**
     * Método que permite inicializar los datos del MB
     *
     * @author javier.aponte
     */
    private void inicializarDatosMB() {

        this.banderaBusquedaTramites = false;
        this.banderaTramiteSeleccionado = false;
        this.selectedTramites = null;
        this.filtro.setTipoTramite(null);

    }

    /**
     * Metodo para consultar los tramites asociados al tramite seleccionado para la cancelacion.
     *
     * @author felipe.cadena
     */
    /*
     * modified by javier.aponte :: #17765 Se realizan modificaciones para que las validaciones se
     * hagan a varios trámites que se pueden cancelar masivamente :: 13/06/2015
     */
    private void consultarTramitesAsociados() {

        Boolean resolucion = false;
        Boolean depuracion = false;
        Boolean cancelacionOtros = false;

        boolean error = false;
        String mensajeError = "";
        List<Tramite> tramitesAsociadosAlSeleccionado;
        this.banderaCancelarTramite = true;
        this.listaValidacionesCancelacionTramite = null;

        //No se permiten cancelar los tramites asociados
        for (Tramite tramiteTemp : this.selectedTramites) {
            if (tramiteTemp.getTramite() != null) {
                error = true;
                mensajeError = " a cancelar es asociado";
            }

            //#17765 :: Se agregan las siguientes validaciones para el trámite padre, aunque no debería suceder, se dejan
            //acá porque hubo algunos casos donde no funcionó. :: 13/06/2016 :: javier.aponte
            if (tramiteTemp.getResultadoDocumento() != null) {
                error = true;
                mensajeError = " tiene generada una resolución";
            }
            if (ETramiteEstado.RECHAZADO.getCodigo().equals(tramiteTemp.getEstado())) {
                error = true;
                mensajeError = " está rechazado";
            } else if (ETramiteEstado.ARCHIVADO.getCodigo().equals(tramiteTemp.getEstado())) {
                error = true;
                mensajeError = " se encuentra archivado";
            } else if (ETramiteEstado.FINALIZADO_APROBADO.getCodigo().
                equals(tramiteTemp.getEstado())) {
                error = true;
                mensajeError = " ya finalizó";
            }
            if (error) {
                this.banderaCancelarTramite = false;
                this.agregarMensajeValidacionCancelacionTramite(tramiteTemp.getNumeroRadicacion(),
                    Constantes.TRAMITE_CON_NUMERO_RADICACION + tramiteTemp.getNumeroRadicacion() +
                     mensajeError + " y no puede ser cancelado");
            }

            tramitesAsociadosAlSeleccionado = this.getTramiteService().
                consultarTramitesAsociadosATramiteId(tramiteTemp.getId());

            //Se descartan los tramites finalizados
            for (int i = 0; i < tramitesAsociadosAlSeleccionado.size(); i++) {
                Tramite t = tramitesAsociadosAlSeleccionado.get(i);
                if (t.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.toString())) {
                    tramitesAsociadosAlSeleccionado.remove(t);
                }
            }

            this.banderaTramitesAsociados = !tramitesAsociadosAlSeleccionado.isEmpty();

            for (Tramite t : tramitesAsociadosAlSeleccionado) {
                Actividad act = this.obtenerActividadActualTramite(t.getNumeroRadicacion(),
                    tramiteTemp.getId());

                if (act == null) {
                    act = new Actividad();
                    act.setNombre("El trámite se encuentra pendiente de activación");
                }
                t.setActividadActualTramite(act);

                if (t.getResultadoDocumento() != null) {
                    resolucion = true;
                }

                if (act.getNombre().equals(
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {
                    depuracion = true;
                }

                if (!this.filtro.isEsVisibleMnpios() && (act.getNombre().equals(
                    ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES) ||
                    act.getNombre().equals(ProcesoDeConservacion.ACT_DIGITALIZACION_ESCANEAR) ||
                    act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO))) {
                    cancelacionOtros = true;
                }
            }

            if (!banderaTramitesAsociados && !this.filtro.isEsVisibleMnpios()) {
                Actividad act = this.
                    obtenerActividadActualTramite(tramiteTemp.getNumeroRadicacion(), tramiteTemp.
                        getId());
                if (act.getNombre().equals(ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES) ||
                     act.getNombre().equals(ProcesoDeConservacion.ACT_DIGITALIZACION_ESCANEAR) ||
                     act.getNombre().equals(
                        ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO)) {
                    cancelacionOtros = true;
                }
            }

            //Si alguna de los tramites tiene resolucion no se puede cancelar el tramite padre.
            if (resolucion) {
                this.banderaCancelarTramite = false;
                this.agregarMensajeValidacionCancelacionTramite(tramiteTemp.getNumeroRadicacion(),
                    Constantes.TRAMITE_CON_NUMERO_RADICACION + tramiteTemp.getNumeroRadicacion() +
                    
                    " a cancelar tiene tramites asociados a los cuales se generó la resolución y no pueden ser cancelados");
            } else if (depuracion) {
                this.banderaCancelarTramite = false;
                this.agregarMensajeValidacionCancelacionTramite(tramiteTemp.getNumeroRadicacion(),
                    Constantes.TRAMITE_CON_NUMERO_RADICACION + tramiteTemp.getNumeroRadicacion() +
                    " a cancelar tiene tramites asociados a los cuales se están aplicando cambios por el proceso de depuración y no pueden ser cancelados");
            } else if (!cancelacionOtros && !this.usuario.getDescripcionEstructuraOrganizacional().
                equals(
                    this.getGeneralesService().
                        getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                            tramiteTemp.getMunicipio().getCodigo()))) {
                this.banderaCancelarTramite = false;
                this.agregarMensajeValidacionCancelacionTramite(tramiteTemp.getNumeroRadicacion(),
                    Constantes.TRAMITE_CON_NUMERO_RADICACION + tramiteTemp.getNumeroRadicacion() +
                    " a cancelar no pertenece a la Territorial o UOC Radicadora");
            }
        }
    }

    /**
     * Método encargado de agregar una mensaje de error de las validaciones de cancelación de
     * trámite
     *
     * @param numeroRadicacion número de radicación del trámite
     * @param mensajeError mensaje de error por el cual no se puede cancelar el trámite
     * @author javier.aponte
     */
    public void agregarMensajeValidacionCancelacionTramite(String numeroRadicacion,
        String mensajeError) {

        if (this.listaValidacionesCancelacionTramite == null) {
            this.listaValidacionesCancelacionTramite =
                new ArrayList<ValidacionCancelacionTramiteDTO>();
        }

        ValidacionCancelacionTramiteDTO validacionCancelacionDelTramite =
            new ValidacionCancelacionTramiteDTO();
        validacionCancelacionDelTramite.setNumeroRadicacion(numeroRadicacion);
        validacionCancelacionDelTramite.setRazonNoCancelacion(mensajeError);

        this.listaValidacionesCancelacionTramite.add(validacionCancelacionDelTramite);

    }

    /**
     * Metodo para obtener la actividad actual de un tramite determinado
     *
     * @author felipe.cadena
     *
     * @param tramite
     * @return
     */
    public Actividad obtenerActividadActualTramite(String numeroRadicacionTramiteAsociado,
        Long tramiteTempId) {

        Actividad act = null;
        Map filtros = new EnumMap<EParametrosConsultaActividades, String>(
            EParametrosConsultaActividades.class);

        filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION,
            numeroRadicacionTramiteAsociado);
        filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, String.valueOf(tramiteTempId));
        List<Actividad> actividades = this.getProcesosService().consultarListaActividades(filtros);

        if (actividades != null && !actividades.isEmpty()) {
            act = actividades.get(0);
        }

        return act;

    }

    /**
     * Metodo para obtener la actividad actual de un tramite determinado por id tramite
     *
     * @author Carlos.Ferro
     *
     * @param tramite
     * @return
     */
    public Actividad obtenerActividadActualTramiteNumerosolicitud(String numeroSolicitud) {

        Actividad act = null;
        Map filtros = new EnumMap<EParametrosConsultaActividades, String>(
            EParametrosConsultaActividades.class);

        filtros.put(EParametrosConsultaActividades.NUMERO_SOLICITUD, numeroSolicitud);
        List<Actividad> actividades = this.getProcesosService().consultarListaActividades(filtros);

        if (actividades != null && !actividades.isEmpty()) {
            act = actividades.get(0);
        }

        return act;

    }

    //18779.  Cerrar trámite en cordis 15/09/2016 wilmanjose.vega
    private boolean cerrarRadicacionTramite(final String radicado,
        final String radicadoResponde, final String usuario) {
        boolean resultado = true;
        final Object[] resultadoCierre = this.getTramiteService()
            .finalizarRadicacion(radicado, radicadoResponde, usuario);
        if (resultadoCierre != null) {
            for (final Object objeto : resultadoCierre) {
                final List lista = (List) objeto;
                if (lista != null && !lista.isEmpty()) {
                    int numeroErrores = 0;
                    for (final Object obj : lista) {
                        final Object[] arrObjetos = (Object[]) obj;
                        //TODO: Eliminar la validación del código de error de oracle luego de que 
                        //el servicio de CORDIS solucione el error: http://redmine.igac.gov.co/issues/21662
                        if (!arrObjetos[0].equals("0") &&
                            !arrObjetos[1].toString().contains("0-ORA-0000")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + arrObjetos[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al cerrar radicación");
                        resultado = false;
                    }
                }
            }
        }
        return resultado;
    }

    private boolean estanTodosTramitesEstadoCierre(final List<Tramite> tramitesSolicitud,
        final Tramite tramiteActual) {
        boolean retval = true;
        if (tramitesSolicitud != null) {
            for (final Tramite tramite : tramitesSolicitud) {
                final boolean esTramiteEnEstadoCierre = tramite.getEstado().equals(
                    ETramiteEstado.ARCHIVADO.getCodigo()) ||
                    tramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo()) ||
                    tramite.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo());
                if (!esTramiteEnEstadoCierre && !tramite.equals(tramiteActual)) {
                    retval = false;
                    break;
                }
            }
        }
        return retval;
    }

    public boolean isProcesarCancelacion() {
        return procesarCancelacion;
    }

}
