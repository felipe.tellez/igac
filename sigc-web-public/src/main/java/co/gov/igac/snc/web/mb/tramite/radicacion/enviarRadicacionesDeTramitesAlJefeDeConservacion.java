package co.gov.igac.snc.web.mb.tramite.radicacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.fachadas.IGenerales;

@Component("enviarRadicacionesDeTramtieAlJefeDeConservacion")
@Scope("session")
/**
 *
 */
public class enviarRadicacionesDeTramitesAlJefeDeConservacion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2067310196103472548L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        enviarRadicacionesDeTramitesAlJefeDeConservacion.class);

    //@Autowired
    //private ITramite tramiteService;
    @Autowired
    private IGenerales generalesService;

    public IGenerales getGeneralesService() {
        return generalesService;
    }

    public void setGeneralesService(IGenerales generalesService) {
        this.generalesService = generalesService;
    }

    public Date getFechaRadicacion() {
        return fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public List<SelectItem> getTipoTramtie() {
        return tipoTramtie;
    }

    public void setTipoTramtie(List<SelectItem> tipoTramtie) {
        this.tipoTramtie = tipoTramtie;
    }

    public List<SelectItem> getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(List<SelectItem> clasificacion) {
        this.clasificacion = clasificacion;
    }

    private Date fechaRadicacion;
    private List<SelectItem> tipoTramtie;
    private List<SelectItem> clasificacion;

    public enviarRadicacionesDeTramitesAlJefeDeConservacion() {
    }

    @PostConstruct
    public void init() {

    }

    public void buscarSolicitud() {
        /* System.out.println(" **************************** ");
         * System.out.println(this.generalesSvc);
		System.out.println(this.generalesSvc.findDominioByNombre(EDominio.SOLICITUD_CLASE_CORRESPONDENCIA)); */
    }

}
