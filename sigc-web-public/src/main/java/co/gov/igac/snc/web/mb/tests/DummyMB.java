/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tests;

import co.gov.igac.snc.web.util.SNCManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pagm
 */
@Component("dummyMB")
@Scope("session")
//OJO: the extends statement must be before the implements one
public class DummyMB extends SNCManagedBean implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(DummyMB.class);

    private List<DummyObject> listaParaRepeat;

    private String cadenaBoba;

    /**
     * para tomar los valores seleccionados de un SelectManyMenu
     */
    private String[] selectedFromSelectManyMenu;

    /**
     * para tomar los valores seleccionados de un SelectOneMenu
     */
    private String selectedFromSelectOneMenu;

    //-----------   para el ejemplo de validaciíon de required en un selectOneMenu --
    private String selectOneStringValue;
    private List<SelectItem> somItems;

    private EntityManager em;

    //--------------------------  methods  --------------------
    public String getSelectOneStringValue() {
        return this.selectOneStringValue;
    }

    public void setSelectOneStringValue(String selectOneStringValue) {
        this.selectOneStringValue = selectOneStringValue;
    }

    public List<SelectItem> getSomItems() {
        return this.somItems;
    }

    public void setSomItems(List<SelectItem> somItems) {
        this.somItems = somItems;
    }

    public String getSelectedFromSelectOneMenu() {
        return this.selectedFromSelectOneMenu;
    }

    public void setSelectedFromSelectOneMenu(String selectedFromSelectOneMenu) {
        this.selectedFromSelectOneMenu = selectedFromSelectOneMenu;
    }

    public String[] getSelectedFromSelectManyMenu() {
        return this.selectedFromSelectManyMenu;
    }

    public void setSelectedFromSelectManyMenu(String[] selectedFromSelectManyMenu) {
        this.selectedFromSelectManyMenu = selectedFromSelectManyMenu;
    }

    public String getCadenaBoba() {
        return cadenaBoba;
    }

    public void setCadenaBoba(String cadenaBoba) {
        this.cadenaBoba = cadenaBoba;
    }

    public List<DummyObject> getListaParaRepeat() {
        return listaParaRepeat;
    }

    public void setListaParaRepeat(List<DummyObject> listaParaRepeat) {
        this.listaParaRepeat = listaParaRepeat;
    }

    @PostConstruct
    public void init() {

        this.listaParaRepeat = new ArrayList<DummyObject>();

        this.listaParaRepeat.add(new DummyObject("pato"));
        this.listaParaRepeat.add(new DummyObject("perro"));
        this.listaParaRepeat.add(new DummyObject("burro"));

        this.cadenaBoba = "florinda la linda";

        initSelectOneMenu();

    }
//--------------------------------------------------------------------------------------------------

    public void foo() {
        for (DummyObject o : this.listaParaRepeat) {
            LOGGER.debug("validado: " + o.isValidado() + " || modificado: " + o.isModificado());
        }
    }

    public void foo(String s) {
        LOGGER.debug("cadena recibida desde interfaz: " + s);
    }

    public void fooListener(javax.faces.event.AjaxBehaviorEvent event) {
        LOGGER.debug("del evento: " + event.toString());
    }

    public void printSelectedFromSelectManyMenu() {

        LOGGER.debug("valores seleccionados: ");

        for (String s : this.selectedFromSelectManyMenu) {
            LOGGER.debug("value = " + s);
        }

    }

    public void printSelectedFromSelectOneMenu() {

        LOGGER.debug("valore seleccionado: " + this.selectOneStringValue);

    }
//--------------------------------------------------------------------------------------------------

    private void initSelectOneMenu() {

        SelectItem item;

        this.somItems = new ArrayList<SelectItem>();
        item = new SelectItem("", "Select");
        this.somItems.add(item);

        item = new SelectItem("lola", "lola");
        this.somItems.add(item);

        item = new SelectItem("flora", "flora");
        this.somItems.add(item);

        item = new SelectItem("No selection", "la que no cuenta como selección", "", false, true,
            true);
        this.somItems.add(item);

        item = new SelectItem(null, "NULL");
        this.somItems.add(item);

    }
//------------------------------------------------------------------------------------------------

    public void selectionListener() {
        LOGGER.debug("seleccionó un valor del combo");
    }
//------------------------------------------------------------------------------------------------

    public void printSelectOneSelection() {
        if (this.selectOneStringValue == null) {
            LOGGER.debug("es null");
        } else if (this.selectOneStringValue.isEmpty()) {
            LOGGER.debug("es vacía");
        } else {
            LOGGER.debug("es " + this.selectOneStringValue);
        }

    }

    private void goo() {

        em.merge(this);
    }
//------------------------------------------------------------------------------------------------

    public void callingABusinessMethodThrowingException() {

        try {
            this.getTramiteService().lanzarExcepcionEntreCapasTest();
        } catch (Exception e) {
            LOGGER.error("catch an exception thrown in a business method.");
        }

    }

}
