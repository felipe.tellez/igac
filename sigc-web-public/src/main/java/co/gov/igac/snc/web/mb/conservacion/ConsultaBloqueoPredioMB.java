/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace las veces de managed bean para la consulta del detalle del bloqueo de un predio
 *
 * @author pedro.garcia
 */
@Component("consultaBloqueoPredio")
@Scope("session")
public class ConsultaBloqueoPredioMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaBloqueoPredioMB.class);

    private Predio predioForPredioBloqueo;

    private List<PredioBloqueo> predioBloqueos;

    private boolean existPredioBloqueo;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte del bloqueo de una persona
     */
    private ReporteDTO reporteBloqueoPersona;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    //TODO quitar esto. Es solo para pruebas de la página individual
    private String numeroPredial;

    //-------------------    methods   ----------------------
    public ReporteDTO getReporteBloqueoPersona() {
        return reporteBloqueoPersona;
    }

    public void setReporteBloqueoPersona(ReporteDTO reporteBloqueoPersona) {
        this.reporteBloqueoPersona = reporteBloqueoPersona;
    }

    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

//--------------------------------------------------------------------------------------------------
    //@PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaBloqueoPredioMB");

        ConsultaDatosUbicacionPredioMB mbConsultaUbicacion =
            (ConsultaDatosUbicacionPredioMB) UtilidadesWeb.getManagedBean(
                "consultaDatosUbicacionPredio");
        this.predioForPredioBloqueo = mbConsultaUbicacion.getPredio();

        if (this.predioForPredioBloqueo.getNumeroPredial() != null) {
            this.predioBloqueos = this.getConservacionService().
                obtenerPredioBloqueosPorNumeroPredial(this.predioForPredioBloqueo.getNumeroPredial());
            if (this.predioBloqueos != null) {
                this.existPredioBloqueo = true;
                for (final PredioBloqueo pb : this.predioBloqueos) {
                    if (pb.getEntidadId() != null) {
                        final Entidad entidad = this.getConservacionService().findEntidadById(pb.
                            getEntidadId());
                        pb.setNombreEntidad(entidad.getNombre());
                    } else {
                        pb.setNombreEntidad(" ");
                    }
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @return the existPredioBloqueo
     */
    public boolean isExistPredioBloqueo() {
        return existPredioBloqueo;
    }

    /**
     * @param existPredioBloqueo the existPredioBloqueo to set
     */
    public void setExistPredioBloqueo(boolean existPredioBloqueo) {
        this.existPredioBloqueo = existPredioBloqueo;
    }

    public void setPredioForPredioBloqueo(Predio predioForPredioBloqueo) {
        this.predioForPredioBloqueo = predioForPredioBloqueo;
    }
//--------------------------------------------------------------------------------------------------	

    public Predio getPredioForPredioBloqueo() {

        if (this.predioBloqueos != null) {
            this.existPredioBloqueo = true;
        }
        return this.predioForPredioBloqueo;
    }
//--------------------------------------------------------------------------------------------------	

    public void setPredioBloqueos(List<PredioBloqueo> predioBloqueos) {
        this.predioBloqueos = predioBloqueos;
    }
//-------------------	

    public List<PredioBloqueo> getPredioBloqueos() {
        /* if(this.numeroPredial != null) { this.predioBloqueo =
         * this.remoteConservacion.getPredioBloqueo(this.numeroPredial); if (this.predioBloqueo !=
         * null) { this.existPredioBloqueo = true; }
		} */
        return this.predioBloqueos;
    }
//--------------------------------------------------------------------------------------------------

    public void obtenerUrlArchivoBloqueo() {

        String idDocumentoGestorDoc;

        //idDocumentoGestorDoc = this.predioBloqueo.getDocumentoSoporteBloqueo().getIdRepositorioDocumentos();
        idDocumentoGestorDoc = "aac573ae-9c57-4de3-b877-b435b3056ec3";

        this.reporteBloqueoPersona = this.reportsService.consultarReporteDeGestorDocumental(
            idDocumentoGestorDoc);

    }

}
