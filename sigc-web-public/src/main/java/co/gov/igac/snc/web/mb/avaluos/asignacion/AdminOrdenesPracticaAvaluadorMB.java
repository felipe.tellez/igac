package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB para el caso de uso CU-SA-AC-111 Administrar órdenes de práctica por avaluador
 *
 * @author christian.rodriguez
 */
@Component("adminOrdenesPracticaAvaluador")
@Scope("session")
public class AdminOrdenesPracticaAvaluadorMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 6277978758758305628L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaCargaTrabajoMB.class);

    /**
     * Variable que contiene la información del usuario activo
     */
    private UsuarioDTO usuario;

    /**
     * {@link ProfesionalAvaluo} al cual se le están adminsitrando las {@link OrdenPractica}
     */
    private ProfesionalAvaluo profesional;

    /**
     * {@link OrdenPractica} asociadas al {@link #profesional}
     */
    private List<OrdenPractica> ordenesPractica;

    /**
     * Orden practica seleccionada
     */
    private OrdenPractica ordenPracticaSeleccionada;

    /**
     * {@link Avaluo} (tramites) asociados a la {@link #ordenPracticaSeleccionada}
     */
    private List<Avaluo> avaluosOrdenPractica;

    /**
     * {@link Avaluo} seleccionados para desasignación
     */
    private Avaluo[] avaluosSeleccionados;

    /**
     * Lista de avaluadores para seleccion
     */
    private List<SelectItem> listaAvaluadores;

    /**
     * Variable usada para guardar el id dle profesional que se selecciona de la lsita desplegable
     * de avaluadores
     */
    private Long profesionalSeleccionadoId;

    // --------------------------------Reportes---------------------------------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte de la orden de practica
     */
    private ReporteDTO ordenPracticaReporte;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si se selecionaron varios profesionales como entrada la CU o únicamente
     * uno
     */
    private boolean banderaVariosProfesionales;

    // ----------------------------Métodos SET y GET----------------------------
    public ProfesionalAvaluo getProfesional() {
        return this.profesional;
    }

    public List<OrdenPractica> getOrdenesPractica() {
        return this.ordenesPractica;
    }

    public OrdenPractica getOrdenPracticaSeleccionada() {
        return this.ordenPracticaSeleccionada;
    }

    public void setOrdenPracticaSeleccionada(
        OrdenPractica ordenPracticaSeleccionada) {
        this.ordenPracticaSeleccionada = ordenPracticaSeleccionada;
    }

    public List<Avaluo> getAvaluosOrdenPractica() {
        return this.avaluosOrdenPractica;
    }

    public Avaluo[] getAvaluosSeleccionados() {
        return this.avaluosSeleccionados;
    }

    public void setAvaluosSeleccionados(Avaluo[] avaluosSeleccionados) {
        this.avaluosSeleccionados = avaluosSeleccionados;
    }

    public List<SelectItem> getListaAvaluadores() {
        return this.listaAvaluadores;
    }

    public Long getProfesionalSeleccionadoId() {
        return this.profesionalSeleccionadoId;
    }

    public void setProfesionalSeleccionadoId(Long profesionalSeleccionadoId) {
        this.profesionalSeleccionadoId = profesionalSeleccionadoId;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getOrdenPracticaReporte() {
        return this.ordenPracticaReporte;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaVariosProfesionales() {
        return this.banderaVariosProfesionales;
    }

    // ---------------------------------Métodos---------------------------------
    // -------------------------------------------------------------------------
    /**
     * Listener que carga las {@link OrdenPractica} del {@link ProfesionalAvaluo} seleccionado de la
     * lsita desplegable {@link #listaAvaluadores}
     *
     * @author christian.rodriguez
     */
    public void cargarOrdenesPracticaProfesional() {

        this.profesional = this.getAvaluosService()
            .obtenerProfesionalAvaluoPorId(this.profesionalSeleccionadoId);
        this.cargarOrdenesPracticaProfesionalSeleccionado();
        this.inicializarVariablesSeleccion();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga las {@link OrdenPractica} asociadas al {@link #profesional}
     *
     * @author christian.rodriguez
     */
    private void cargarOrdenesPracticaProfesionalSeleccionado() {
        if (this.profesional != null) {
            this.ordenesPractica = this.getAvaluosService()
                .consultarOrdenesPracticaPorAvaluadorIdYTipoVinculacion(
                    this.profesional.getId(),
                    this.profesional.getTipoVinculacion());
        } else {
            this.ordenesPractica = null;
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que se ejecuta cuando se selecciona una orden de practica. SE encarga de cargar los
     * {@link Avaluo} (tramites) asociados a la {@link #ordenPracticaSeleccionada}
     *
     * @author christian.rodriguez
     */
    public void cargarTramitesAsociadosAOrdenPracticaSeleccionada(
        SelectEvent event) {

        this.ordenPracticaSeleccionada = (OrdenPractica) event.getObject();

        this.cargarTramitesAsociadosAOrdenPracticaSeleccionada();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los trámites (Avaluos) asociados a la {@link #ordenPracticaSeleccionada}
     *
     * @author christian.rodriguez
     */
    private void cargarTramitesAsociadosAOrdenPracticaSeleccionada() {
        this.avaluosOrdenPractica = this.getAvaluosService()
            .consultarAvaluosPorOrdenPracticaIdYProfesionalId(
                this.profesional.getId(),
                this.ordenPracticaSeleccionada.getId());
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón de eliminar. Se encargar de borrar la {@link #ordenPracticaSeleccionada}
     * desasociarla y borrar el documento asociado de la bd y alfresco
     *
     * @author christian.rodriguez
     */
    public void eliminarOrden() {

        boolean borradoExitoso = false;

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.ordenPracticaSeleccionada != null) {

            borradoExitoso = this.getAvaluosService().borrarOrdenPracticaPorId(
                this.ordenPracticaSeleccionada.getId());

            if (borradoExitoso) {

                this.addMensajeInfo("Se eliminó correctamente la orden de práctica número " +
                     this.ordenPracticaSeleccionada.getNumero());

                this.inicializarVariablesSeleccion();
                this.cargarOrdenesPracticaProfesionalSeleccionado();

            } else {
                this.addMensajeError("No se pudo borrar la orden de práctica.");
                context.addCallbackParam("error", "error");
            }
        } else {
            this.addMensajeError("Debe seleccionar una orden de práctica para eliminar.");
            context.addCallbackParam("error", "error");
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón desasociar trámites. SE encarga de desasociar los avlauos de la
     * {@link #ordenPracticaSeleccionada}. Tabmein revisa que si la orden de práctica quedó sin
     * avalúos esta sera eliminada y se borrara del documento asociado
     *
     * @author christian.rodriguez
     */
    public void desasociarTramite() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.avaluosSeleccionados != null &&
             this.avaluosSeleccionados.length != 0) {

            boolean desasignacionExitosa = false;

            desasignacionExitosa = this.getAvaluosService()
                .desasociarTramitesDeOrdenDePractica(
                    this.ordenPracticaSeleccionada.getId(),
                    this.avaluosSeleccionados);

            if (desasignacionExitosa) {
                if (this.avaluosOrdenPractica == null ||
                     this.avaluosOrdenPractica.isEmpty()) {
                    this.eliminarOrden();
                } else {
                    this.generarDocumentoOrdenPractica();
                    this.cargarTramitesAsociadosAOrdenPracticaSeleccionada();
                    this.addMensajeInfo("Se desasociaron correctamente los trámites seleccionados");
                }
            }

        } else {
            this.addMensajeError("Debe seleccionar trámites para desasociar.");
            context.addCallbackParam("error", "error");
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se encarga de recuperar el acta del avaluo seleccionado para ser visualizada
     *
     * @author christian.rodriguez
     */
    public void cargarDocumentoOrdenPractica() {
        if (this.ordenPracticaSeleccionada != null) {

            Documento ordenPracticaDocumento = this.getGeneralesService()
                .buscarDocumentoPorId(
                    this.ordenPracticaSeleccionada.getDocumentoId());

            this.ordenPracticaReporte = this.reportsService
                .consultarReporteDeGestorDocumental(
                    ordenPracticaDocumento.getIdRepositorioDocumentos());

            if (this.ordenPracticaReporte == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "La orden de práctica asociada no pudo ser cargada.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método para cargar la información requerida por este caso de uso cuando es ejecutado destre
     * otro managed bean
     *
     * @author christian.rodriguez
     * @param profesionalId identificador del {@link ProfesionalAvaluo} del cual se quieren
     * administrar las {@link OrdenPractica}
     */
    public void cargarDesdeOtrosCU(List<ProfesionalAvaluo> profesionales) {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.inicializarVariablesSeleccion();

        if (profesionales.size() == 1) {
            this.banderaVariosProfesionales = false;
            this.profesional = profesionales.get(0);
            this.cargarOrdenesPracticaProfesionalSeleccionado();
        } else {
            this.banderaVariosProfesionales = true;
            this.ordenesPractica = null;
            this.profesionalSeleccionadoId = null;
            this.crearListaSeleccionAvaluadores(profesionales);
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Inicializa las variables de seleccion y listas auxiliares
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesSeleccion() {
        this.avaluosSeleccionados = new Avaluo[]{};
        this.ordenPracticaSeleccionada = null;
        this.avaluosOrdenPractica = null;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los avaluadores en una lista para seleccion
     *
     * @author christian.rodriguez
     * @param profesionales lista de {@link ProfesionalAvaluo} que se van a cargar dentro de la
     * lista de selección
     */
    private void crearListaSeleccionAvaluadores(
        List<ProfesionalAvaluo> profesionales) {

        this.listaAvaluadores = new ArrayList<SelectItem>();
        this.listaAvaluadores.add(new SelectItem(null, this.DEFAULT_COMBOS));

        for (ProfesionalAvaluo profesional : profesionales) {
            listaAvaluadores.add(new SelectItem(profesional.getId(),
                profesional.getNombreCompleto()));
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que genera el documento de la orden de práctica y reemplaza el existente asociado a la
     * {@link #ordenPracticaSeleccionada}
     *
     * @author christian.rodriguez
     */
    private void generarDocumentoOrdenPractica() {
        if (this.generarReporte()) {
            if (this.guardarEnAlfresco()) {
                this.addMensajeInfo(
                    "Se genero nuevamente el documento asociado a la orden de práctica");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError("No pudo generarse el documento de la orden de práctica.");
                context.addCallbackParam("error", "error");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que genera el nuevo reporte en el gestor documental
     *
     * @author christian.rodriguez
     */
    private boolean guardarEnAlfresco() {

        Documento ordenPractica = this.getGeneralesService()
            .buscarDocumentoPorId(
                this.ordenPracticaSeleccionada.getDocumentoId());

        String antiguoIdDocumentoAlfresco = ordenPractica
            .getIdRepositorioDocumentos();

        Solicitud solicitud = this.avaluosOrdenPractica.get(0).getTramite()
            .getSolicitud();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(solicitud
                .getNumero(), solicitud.getFecha(),
                ETipoDocumento.MEMORANDO_DE_ORDEN_DE_PRACTICA.getNombre(),
                this.ordenPracticaReporte
                    .getRutaCargarReporteAAlfresco());

        ordenPractica = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, ordenPractica);

        if (!antiguoIdDocumentoAlfresco.equalsIgnoreCase(ordenPractica
            .getIdRepositorioDocumentos())) {

            if (this.getGeneralesService().borrarDocumentoEnAlfresco(
                antiguoIdDocumentoAlfresco)) {
                return true;
            }
        }
        return false;

    }

    // -------------------------------------------------------------------------
    /**
     * Método que genera el reporte de la orden de práctica
     *
     * @author christian.rodriguez
     */
    private boolean generarReporte() {

        boolean reporteGenerado = false;

        // TODO::christian.rodriguez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        // TODO::christian.rodriguez :: Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_RADICADO", "");

//TODO :: christian.rodriguez :: si el resultado es null aún así dice reporteGenerado = true. preguntar si el resultado es null y hacer algo (mostrar mensaje) :: pedro.garcia
        this.ordenPracticaReporte = this.reportsService.generarReporte(
            parameters, enumeracionReporte, this.usuario);

        reporteGenerado = true;

        return reporteGenerado;
    }
}
