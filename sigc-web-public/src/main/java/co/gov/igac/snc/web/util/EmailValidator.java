package co.gov.igac.snc.web.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;

/**
 *
 * @author fredy.wilches Clase validador de correos electrónicos
 */
public class EmailValidator implements Validator {

    public void validate(FacesContext context, UIComponent toValidate,
        Object value) {
        String email = (String) value;
        if (email != null && !email.trim().equals("") && (email.indexOf('@') == -1 || email.indexOf(
            '.') == -1)) {
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Correo electrónico no válido", null);
            context.addMessage(toValidate.getClientId(context), message);
        }
    }
}
