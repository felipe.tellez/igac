package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Locale;

import co.gov.igac.snc.comun.utilerias.Conversion;

/**
 * Esta clase representa una resolución de actualización.
 *
 * @author jamir.avila.
 *
 */
//TODO::jamir.avila::13-12-2011::NO USAR 'ñ', por estándar del proyecto::pedro.garcia
public class Resolucion implements Serializable {

    /**
     * Identificador de versión por omisión.
     */
    private static final long serialVersionUID = 1L;

    /** Tipo de resolución: Ordenar ejecución de la actualización, Ordenar ejecución de la formación */
    private String tipoResolucionNombre;
    /** Año de la resolución. */
    private int anio;
    /** Fecha de la resolución. */
    private java.util.Date fecha;
    /** Plantilla con el contenido de la resolución. */
    private String contenido;

    /** Managed Bean del formulario */
    private GestionarResolucionMB managedBean;

    /** Conversión de números a letras. */
    private Conversion conversion;

    /** Format de los mensajes */
    private MessageFormat messageFormat;

    /** Enumeración para los campos que se diligencian de la plantilla de la resolución. */
    private enum EValoresResolucion {
        NUMERO_DE_RESOLUCION,
        FECHA_DE_RESOLUCION,
        TIPO_DE_RESOLUCION,
        ZONA,
        MUNICIPIO,
        TERRITORIAL,
        DEPARTAMENTO,
        ANIO_SIGUIENTE_EN_LETRAS,
        ANIO_SIGUIENTE,
        DIA_DE_LA_RESOLUCION_EN_LETRAS,
        ANIO_DE_LA_RESOLUCION_EN_LETRAS,
        DIRECTOR_TERRITORIAL
    }

    /**
     * Constructor de una resolución de actualización.
     */
    public Resolucion(String tipoResolucionNombre, GestionarResolucionMB gestionarResolucionMB,
        String contenido) {
        managedBean = gestionarResolucionMB;

        this.tipoResolucionNombre = tipoResolucionNombre;
        Calendar hoy = Calendar.getInstance();
        anio = hoy.get(Calendar.YEAR);
        fecha = new java.util.Date(hoy.getTimeInMillis());

        Locale colombia = new Locale("ES", "es_CO");
        messageFormat = new MessageFormat(contenido, colombia);

        conversion = new Conversion();
    }

    /**
     * Este método combina la plantilla con los datos y regresa el resultado.
     */
    private void actualizarContenido() {
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(fecha);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);
        int unAnio = calendario.get(Calendar.YEAR);

        // Llenar los valores con los que se llena la plantilla.
        Object[] parametros = new Object[EValoresResolucion.values().length];
        parametros[EValoresResolucion.NUMERO_DE_RESOLUCION.ordinal()] = "00-000-000-0000-2011";
        parametros[EValoresResolucion.FECHA_DE_RESOLUCION.ordinal()] = fecha;
        parametros[EValoresResolucion.TIPO_DE_RESOLUCION.ordinal()] = getTipoResolucionNombre();
        parametros[EValoresResolucion.ZONA.ordinal()] = managedBean.getZonaNombre();
        parametros[EValoresResolucion.MUNICIPIO.ordinal()] = managedBean.getMunicipioNombre();
        parametros[EValoresResolucion.TERRITORIAL.ordinal()] = managedBean.getTerritorialNombre();
        parametros[EValoresResolucion.DEPARTAMENTO.ordinal()] = managedBean.getDepartamentoNombre();
        parametros[EValoresResolucion.ANIO_SIGUIENTE_EN_LETRAS.ordinal()] = conversion.
            numeroALetras(anio + 1);
        parametros[EValoresResolucion.ANIO_SIGUIENTE.ordinal()] = anio + 1;
        parametros[EValoresResolucion.DIA_DE_LA_RESOLUCION_EN_LETRAS.ordinal()] = conversion.
            numeroALetras(dia);
        parametros[EValoresResolucion.ANIO_DE_LA_RESOLUCION_EN_LETRAS.ordinal()] = conversion.
            numeroALetras(unAnio);
        parametros[EValoresResolucion.DIRECTOR_TERRITORIAL.ordinal()] = managedBean.
            getNombreDirectorTerritorial().toUpperCase();

        contenido = messageFormat.format(parametros);
    }

    public String getTipoResolucionNombre() {
        return tipoResolucionNombre;
    }

    public void setTipoResolucionNombre(String tipoResolucionNombre) {
        this.tipoResolucionNombre = tipoResolucionNombre;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public java.util.Date getFecha() {
        return fecha;
    }

    public void setFecha(java.util.Date fecha) {
        this.fecha = fecha;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getContenido() {
        actualizarContenido();
        return contenido;
    }
}
