/**
 * IGAC proyecto SNC
 *
 * @author javier.aponte
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace las veces de managed bean para la consulta de ficha matriz de un predio
 *
 * @author javier.aponte
 * @modified juan.agudelo
 */
@Component("consultaFichaMatrizPredio")
@Scope("session")
public class ConsultaFichaMatrizPredioMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 1679925237332118474L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaFichaMatrizPredioMB.class);

    private Predio predioForFichaMatriz;

    private boolean existenFichaMatrizPredio;

    private FichaMatriz fichaMatrizSearchResult;

    private List<String> numerosPredialesPredioRelacionados;

    private List<UsoConstruccion> usosConstruccion;

    private Integer totalNumerosPrediales;

    private Integer totalUsosConstruccion;

    private UnidadConstruccion predioUnidadConstruccionSeleccionado;

    private boolean banderaVistaConstruccionesConvencionalesExtendida;

    private Predio predio;

    /**
     * Variable PPredioZona usada para almacenar las zonas homogéneas del PPredio
     */
    private List<PredioZona> zonasHomogeneas;

    /**
     * árbol para el modelo de unidades de construcción en lectura
     */
    private TreeNode treeComponentesConstruccion;

    public FichaMatriz getFichaMatrizSearchResult() {
        return fichaMatrizSearchResult;
    }

    public void setFichaMatrizSearchResult(FichaMatriz fichaMatrizSearchResult) {
        this.fichaMatrizSearchResult = fichaMatrizSearchResult;
    }

    public List<PredioZona> getZonasHomogeneas() {
        return zonasHomogeneas;
    }

    public void setZonasHomogeneas(List<PredioZona> zonasHomogeneas) {
        this.zonasHomogeneas = zonasHomogeneas;
    }

    public boolean isBanderaVistaConstruccionesConvencionalesExtendida() {
        return banderaVistaConstruccionesConvencionalesExtendida;
    }

    public void setBanderaVistaConstruccionesConvencionalesExtendida(
        boolean banderaVistaConstruccionesConvencionalesExtendida) {
        this.banderaVistaConstruccionesConvencionalesExtendida =
            banderaVistaConstruccionesConvencionalesExtendida;
    }

    public boolean isExistenFichaMatrizPredio() {
        return existenFichaMatrizPredio;
    }

    public void setExistenFichaMatrizPredio(boolean existenFichaMatrizPredio) {
        this.existenFichaMatrizPredio = existenFichaMatrizPredio;
    }

    public Predio getPredioForFichaMatriz() {
        return this.predioForFichaMatriz;
    }

    public void setPredioForFichaMatriz(Predio predioForFichaMatriz) {
        this.predioForFichaMatriz = predioForFichaMatriz;
    }

    public UnidadConstruccion getPredioUnidadConstruccionSeleccionado() {
        return predioUnidadConstruccionSeleccionado;
    }

    public void setPredioUnidadConstruccionSeleccionado(
        UnidadConstruccion predioUnidadConstruccionSeleccionado) {
        this.predioUnidadConstruccionSeleccionado = predioUnidadConstruccionSeleccionado;
    }

    public List<String> getNumerosPredialesPredioRelacionados() {
        return numerosPredialesPredioRelacionados;
    }

    public void setNumerosPredialesPredioRelacionados(
        List<String> numerosPredialesPredioRelacionados) {
        this.numerosPredialesPredioRelacionados = numerosPredialesPredioRelacionados;
    }

    public List<UsoConstruccion> getUsosConstruccion() {
        return usosConstruccion;
    }

    public void setUsosConstruccion(List<UsoConstruccion> usosConstruccion) {
        this.usosConstruccion = usosConstruccion;
    }

    public Integer getTotalNumerosPrediales() {
        return totalNumerosPrediales;
    }

    public void setTotalNumerosPrediales(Integer totalNumerosPrediales) {
        this.totalNumerosPrediales = totalNumerosPrediales;
    }

    public Integer getTotalUsosConstruccion() {
        return totalUsosConstruccion;
    }

    public void setTotalUsosConstruccion(Integer totalUsosConstruccion) {
        this.totalUsosConstruccion = totalUsosConstruccion;
    }

    /**
     * Método que retorna el área a mostrar de la ficha matriz, basado en la condición de propiedad
     * del predio
     *
     * @author david.cifuentes
     * @return
     */
    public Double getAreaTerrenoFichaMatriz() {
        Double areaTerrenoFM = 0D;
        if (this.fichaMatrizSearchResult != null && this.fichaMatrizSearchResult.getPredio() != null) {
            if (EPredioCondicionPropiedad.CP_8
                .getCodigo().
                equals(this.fichaMatrizSearchResult.getPredio().getCondicionPropiedad())) {
                // Condominio
                areaTerrenoFM = this.fichaMatrizSearchResult.getAreaTotalTerrenoComun() +
                    this.fichaMatrizSearchResult.getAreaTotalTerrenoPrivada();
            } else {
                areaTerrenoFM = this.fichaMatrizSearchResult.getPredio().getAreaTerreno();
            }
        }
        return areaTerrenoFM;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaFichaMatrizPredioMB");

        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb
            .getManagedBean("consultaPredio");

        /**
         * C: ahora se hacen las consultas por id del predio N: ver método
         * ConsultaPredioMB#getSelectedPredioId para entender de dónde se obtiene el id
         */
        Long predioSeleccionadoId = mb001.getSelectedPredioId();

        if (predioSeleccionadoId != null) {

            this.getPredioById(predioSeleccionadoId);

        }
        this.consultarZonas();
    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza la consulta de las zonas homogeneas de un predio.
     *
     * @author david.cifuentes
     */
    public void consultarZonas() {
        if (this.predio != null) {
            if (this.fichaMatrizSearchResult != null) {
                this.zonasHomogeneas = this.getConservacionService().
                    obtenerZonasUltimaVigenciaPorIdPredio(this.fichaMatrizSearchResult.getPredio().
                        getId());
            } else {
                this.zonasHomogeneas = this.getConservacionService().
                    obtenerZonasUltimaVigenciaPorIdPredio(this.predio.getId());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * se define esta método solo para mantener el estándar del tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * @modified juan.agudelo
     * @modified felipe.cadena :: 26/05/2014 :: #7923 :: se consulta la ficha asociada correctamente
     * @param predioId
     */
    private void getPredioById(Long predioId) {

        this.predio = this.getConservacionService()
            .obtenerPredioPorId(predioId);

        String numeroPredial = this.predio.getNumeroPredial();

        //se envia el numero del predio matriz. #7923
        numeroPredial = numeroPredial.substring(0, 22);
        numeroPredial = numeroPredial.concat("00000000");
        try {

            this.fichaMatrizSearchResult = this.getConservacionService()
                .getFichaMatrizByNumeroPredialPredio(numeroPredial);

            this.setPredioForFichaMatriz(predio);
            this.setExistenFichaMatrizPredio(false);
            if (this.fichaMatrizSearchResult != null &&
                 this.fichaMatrizSearchResult.getFichaMatrizPredios() != null) {

                if (this.numerosPredialesPredioRelacionados != null) {

                    this.numerosPredialesPredioRelacionados = new ArrayList<String>();

                    this.totalNumerosPrediales = this.fichaMatrizSearchResult
                        .getFichaMatrizPredios().size();

                    for (FichaMatrizPredio predioRelacionado : this.fichaMatrizSearchResult
                        .getFichaMatrizPredios()) {
                        this.numerosPredialesPredioRelacionados
                            .add(predioRelacionado.getNumeroPredial());
                    }

                    this.usosConstruccion = this.getConservacionService()
                        .buscarUsosConstruccionPorNumerosPredialesPredio(
                            this.numerosPredialesPredioRelacionados);
                    this.totalUsosConstruccion = this.usosConstruccion.size();
                }
                this.setExistenFichaMatrizPredio(true);
            }
        } catch (Exception ex) {
            LOGGER.info("Error en la consulta de ficha matriz " +
                 ex.getMessage());
        }
    }

}
