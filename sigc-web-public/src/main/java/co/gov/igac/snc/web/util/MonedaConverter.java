package co.gov.igac.snc.web.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Convertidor para valores de tipo Moneda se creo principalmente para usarlo al registrar una
 * oferta inmobiliaria ver registroOfertaInmobiliaria.xhtml
 *
 * @author christian.rodriguez
 *
 */
@FacesConverter(value = "converterMoneda")
public class MonedaConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonedaConverter.class);

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorString) {

        valorString = valorString.replace(',', ' ');
        valorString = valorString.replace('$', ' ');
        valorString = valorString.replace(" ", "");

        if (valorString == null || (valorString.trim().length() == 0 || valorString.equals("0"))) {
            return new Double(0.0);
        }

        try {
            return Double.valueOf(valorString);
        } catch (NumberFormatException e) {
            LOGGER.error("Ocurrió un error al convertir el valor " + valorString + " a un Double");
            return null;
        }

    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 01-08-2012 Se adiciona validación para que acepte String como entrada.
     * OJO: queda implementado solo para texto de salida en los xhtml (falta hacerlo en getAsObject
     * para que funcione para los inputText) Se comenta la línea que cambia '.' por ',' porque se ve
     * más bonito
     *
     * @modified pedro.garcia 06-08-2012 se adiciona validación para que la cadena que puede llegar
     * como parámetro tenga formato de número
     *
     * @modified pedro.garcia 15-08-2012. Se crea un método estático con el cuerpo que tenía este
     * método para que pueda ser usado como conversor desde un MB sin tener que enviar los
     * parámetros FacesContext y UIComponent
     */
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorObjecto) {

        String answer = "";

        answer = getAsString(valorObjecto);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see MonedaConverter#getAsString(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     * @author pedro.garcia
     */
    public static String getAsString(Object valorObjecto) {

        String answer = null;

        Double valueDouble = 0.0;
        String valueString, valorAsString;

        try {
            if (valorObjecto instanceof Double) {
                valueDouble = (Double) valorObjecto;
            } else if (valorObjecto instanceof String) {
                valueString = (String) valorObjecto;
                //D: acepta cadenas de números donde haya uno o más separadores (',' o '.')
                //   y cadenas que representen números en notación científica
                if (valueString.matches("^\\d+([.,]\\d+)*$") ||
                    valueString.matches("^\\d+[.,]\\d+[E][-]*\\d+$")) {
                    valueDouble = new Double(valueString);
                } else {
                    return "Cadena no tiene formato para moneda";
                }
            }

            BigDecimal value = new BigDecimal(valueDouble);

            NumberFormat numberFormatter = new DecimalFormat("#,###,###");
            valorAsString = numberFormatter.format(value);
            valorAsString = valorAsString.replace('.', ',');

            answer = "$".concat(valorAsString);
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al convertir el valor " + valorObjecto + " a un String");
            answer = "$0.0";
        }

        return answer;
    }

}
