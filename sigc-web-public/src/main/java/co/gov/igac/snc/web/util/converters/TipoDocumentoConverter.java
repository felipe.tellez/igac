package co.gov.igac.snc.web.util.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import co.gov.igac.snc.persistence.entity.generales.Numeracion;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.util.Constantes;

/**
 * Converter para los objetos TipoDocumento para usarlo en los selectItems de los combos
 *
 * @author david.cifuentes
 */
@FacesConverter(value = "tipoDocumentoConverter",
    forClass = co.gov.igac.snc.persistence.entity.generales.TipoDocumento.class)
public class TipoDocumentoConverter implements Converter {

    //---------------------------//
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        TipoDocumento answer = new TipoDocumento();
        Numeracion numeracionDocumentoTemp = new Numeracion();

        String[] objectAttributes;
        objectAttributes = value.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setId(Long.parseLong(objectAttributes[0]));
        numeracionDocumentoTemp.setId(Long.parseLong(objectAttributes[1]));
        answer.setNumeracionDocumento(numeracionDocumentoTemp);
        answer.setNombre(objectAttributes[2]);
        answer.setClase(objectAttributes[3]);
        answer.setGenerado(objectAttributes[4]);

        return answer;

    }

    //---------------------------//
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object o) {
        return ((TipoDocumento) o).toString();
    }

}
