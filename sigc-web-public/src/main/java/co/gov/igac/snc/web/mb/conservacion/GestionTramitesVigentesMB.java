/*
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.util.FiltroDatosConsultaTramites;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Clase que hace de managed bean para gestionTramitesVigentes
 *
 * @author pedro.garcia
 */
//D: OJO el valor de un dato que corresponda a un dominio es el código de ese dominio, no el id
@Component("gestionTramitesVigentes")
@Scope("session")
public class GestionTramitesVigentesMB extends SNCManagedBean {

    private static final long serialVersionUID = -5172998811646733085L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GestionTramitesVigentesMB.class);

    /**
     * Interfaces de servicio
     */
    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaTramites datosConsultaTramites;

    /**
     * listas con los valores de los combos de selección para la búsqueda
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;
    private ArrayList<SelectItem> tiposTramiteItemList;
    private ArrayList<SelectItem> clasesMutacionTramiteItemList;
    private ArrayList<SelectItem> subtiposMutacionTramiteItemList;
    private ArrayList<SelectItem> clasificacionesTramiteItemList;
    private ArrayList<SelectItem> estadosTramiteItemList;
    private ArrayList<SelectItem> funcionariosEjecutoresItemList;
    private ArrayList<SelectItem> funcionariosRadicadoresItemList;

    private ArrayList<Tramite> tramitesSearchResultList;

    /*
     * para manejar el orden en que se muestran los combos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;

    private boolean renderResultPanel;

//---------   methods  -----------------------------------------------------------------------------
    public void setTramitesSearchResultList(ArrayList<Tramite> tramitesSearchResultList) {
        this.tramitesSearchResultList = tramitesSearchResultList;
    }

    public ArrayList<Tramite> getTramitesSearchResultList() {
        return this.tramitesSearchResultList;
    }
//--------------------------------------------------------------------------------------------------    

    @SuppressWarnings("unused")
    @PostConstruct
    public void init() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.tiposTramiteItemList = new ArrayList<SelectItem>();
        this.clasesMutacionTramiteItemList = new ArrayList<SelectItem>();
        this.clasificacionesTramiteItemList = new ArrayList<SelectItem>();
        this.subtiposMutacionTramiteItemList = new ArrayList<SelectItem>();
        this.estadosTramiteItemList = new ArrayList<SelectItem>();
        this.funcionariosEjecutoresItemList = new ArrayList<SelectItem>();
        this.funcionariosRadicadoresItemList = new ArrayList<SelectItem>();
        this.datosConsultaTramites = new FiltroDatosConsultaTramites();

        init_departamentos_list:
            {

                List<Departamento> dptos;

                this.departamentosItemList.add(new SelectItem("", "Sin seleccionar..."));

//TODO aquí se debe usar el id de territorial que llegue de la sesión
                dptos = this.getGeneralesService().getCacheDepartamentosPorTerritorial("11000");
                if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                    Collections.sort(dptos);
                } else {
                    Collections.sort(dptos, Departamento.getComparatorNombre());
                }
                for (Departamento d : dptos) {
                    if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                        this.departamentosItemList.add(new SelectItem(d.getCodigo(), d.getCodigo() +
                            "-" + d.getNombre()));
                    } else {
                        this.departamentosItemList.add(new SelectItem(d.getCodigo(), d.getNombre()));
                    }
                }
            }// end of init_departamentos_list

        this.municipiosItemList.add(new SelectItem("", "Todos"));

        this.setRenderResultPanel(false);
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getDepartamentosItemList() {
        return this.departamentosItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getMunicipiosItemList() {

        List<Municipio> municipios;
        String departamentoId;

        departamentoId = this.datosConsultaTramites.getDepartamentoId();
        if (departamentoId != null) {
            if (!this.municipiosItemList.isEmpty()) {
                this.municipiosItemList.clear();
                this.municipiosItemList.add(new SelectItem("", "Todos"));
            }
            municipios = this.getGeneralesService().
                getCacheMunicipiosPorDepartamento(departamentoId);
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(municipios);
            } else {
                Collections.sort(municipios, Municipio.getComparatorNombre());
            }

            for (Municipio m : municipios) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    this.municipiosItemList.add(new SelectItem(m.getCodigo(), m.getCodigo().
                        substring(2) +
                         "-" + m.getNombre()));
                } else {
                    this.municipiosItemList.add(new SelectItem(m.getCodigo(), m.getNombre()));
                }
            }
        }
        return this.municipiosItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getTiposTramiteItemList() {

        List<Dominio> tiposTramite;

        tiposTramite = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.TRAMITE_TIPO_TRAMITE);
        for (Dominio d : tiposTramite) {
            this.tiposTramiteItemList.add(new SelectItem(d.getCodigo(), d.getCodigo() + "-" + d.
                getValor()));
        }

        return this.tiposTramiteItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getClasesMutacionTramiteItemList() {

        List<Dominio> clasesMutacionTramite;

        clasesMutacionTramite = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.MUTACION_CLASE);
        for (Dominio d : clasesMutacionTramite) {
            this.clasesMutacionTramiteItemList.add(new SelectItem(d.getCodigo(),
                d.getCodigo() + "-" + d.getValor()));
        }

        return this.clasesMutacionTramiteItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getClasificacionesTramiteItemList() {

        List<Dominio> clasificacionesTramite;

        clasificacionesTramite = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.MUTACION_CLASIFICACION);
        for (Dominio d : clasificacionesTramite) {
            this.clasificacionesTramiteItemList.add(new SelectItem(d.getCodigo(), d.getValor()));
        }

        return this.clasificacionesTramiteItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getSubtiposMutacionTramiteItemList() {
        List<Dominio> subtiposMutacion = null;
        String claseMutacion;

        claseMutacion = this.datosConsultaTramites.getClaseMutacionCodigo();
        if (claseMutacion != null && claseMutacion.compareTo("") != 0) {
            if (!this.subtiposMutacionTramiteItemList.isEmpty()) {
                this.subtiposMutacionTramiteItemList.clear();
            }
            //D: mutación de segunda clase
            if (this.datosConsultaTramites.getClaseMutacionCodigo().compareToIgnoreCase("2") == 0) {
                subtiposMutacion = this.getGeneralesService().getCacheDominioPorNombre(
                    EDominio.MUTACION_2_SUBTIPO);
            } //D: mutación de quinta clase
            else if (this.datosConsultaTramites.getClaseMutacionCodigo().compareToIgnoreCase("5") ==
                0) {
                subtiposMutacion = this.getGeneralesService().getCacheDominioPorNombre(
                    EDominio.MUTACION_5_SUBTIPO);
            }

            for (Dominio d : subtiposMutacion) {
                this.subtiposMutacionTramiteItemList.
                    add(new SelectItem(d.getCodigo(), d.getValor()));
            }

        }

        return this.subtiposMutacionTramiteItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getEstadosTramiteItemList() {
        List<Dominio> estadosMutacion;

        estadosMutacion = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.TRAMITE_ESTADO);
        for (Dominio d : estadosMutacion) {
            this.estadosTramiteItemList.add(new SelectItem(d.getCodigo(), d.getValor()));
        }

        return this.estadosTramiteItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getFuncionariosEjecutoresItemList() {

//TODO esto queda pendiente hasta que se defina la comunicación con el LDAP
        return this.funcionariosEjecutoresItemList;
    }
//--------------------------------------------------------------------------------------------------

    public List<SelectItem> getFuncionariosRadicadoresItemList() {

//TODO esto queda pendiente hasta que se defina la comunicación con el LDAP
        return this.funcionariosRadicadoresItemList;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @return the datosConsultaTramites
     */
    public FiltroDatosConsultaTramites getDatosConsultaTramites() {
        return datosConsultaTramites;
    }

    /**
     * @param datosConsultaTramites the datosConsultaTramites to set
     */
    public void setDatosConsultaTramites(FiltroDatosConsultaTramites datosConsultaTramites) {
        this.datosConsultaTramites = datosConsultaTramites;
    }

    public boolean isRenderResultPanel() {
        return renderResultPanel;
    }

    public void setRenderResultPanel(boolean renderResultPanel) {
        this.renderResultPanel = renderResultPanel;
    }

//--------------------------------------------------------------------------------------------------
    public void searchTramites() {
        LOGGER.debug("ejecutando GestionTramitesVigentesMB#searchTramites...");

        this.renderResultPanel = true;

    }

}
