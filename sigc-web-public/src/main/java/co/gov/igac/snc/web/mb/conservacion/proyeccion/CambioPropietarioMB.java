package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ESeccionEjecucion;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.apache.commons.collections15.list.AbstractLinkedList;

/**
 *
 * @author fabio.navarrete
 * @modified by fredy.wilches
 *
 * Con respecto al campo CANCELA_INSCRIBE: - null es como estan originalmente en Conservación - M
 * cuando se asocian, solo se les puede modificar el % de participación. Tocó usar este estado para
 * diferenciarlos de los (I)nscritos y de los null - I nuevos (Se pueden modificar) - C son los
 * vigentes (M) que se cancelan, porque los I que se eliminen se deben eliminar de la BD
 *
 * DIAGRAMA DE ESTADOS Al iniciar el caso todos los null deben pasarse C Pasan de C a M al
 * asociarlos Pasan de M a C cuando se eliminan (Cancelan) Se eliminan de la BD cuando son I y se
 * eliminan (Cancelan)
 *
 * Estas mismas reglas aplican para las justificaciones de propiedad Se deben validar % de
 * participacion, relacion entre personas y justificacion de propiedad, campos obligatorios,....
 *
 * Propietarios vigentes = null + M + C aunque null no deberia haber porque al inicio pasan a C
 * Propietarios nuevos = null + M + I aunque null no deberia haber porque al inicio pasan a C
 *
 */
@Component("cambioPropietario")
@Scope("session")
public class CambioPropietarioMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 2184496880277340270L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CambioPropietarioMB.class);

    private PPersonaPredio propietarioSeleccionado;
    private UsuarioDTO usuario;
    private PersonaPredio[] propietariosVigentesSeleccionados;

    private PPersonaPredio[] propietariosCanceladosSeleccionados;

    /**
     * Listado de personas que coinciden por tipo de documento y numero
     */
    private List<PPersona> propietariosCoincidentes;

    /**
     * Variable que almacena los propietarios vigentes
     */
    private List<PPersonaPredio> propietariosVigentes;

    /**
     * Persona seleccionada cuando al buscar existe mas de una persona con la misma identificacion
     */
    private PPersona personaSeleccionada;

    private PPersona personaSeleccionadaEditar;

    @Autowired
    private ProyectarConservacionMB proyectarConservacionMB;

    @Autowired
    private GeneralMB generalMB;

    private boolean nuevo;

    /**
     * Variable para manejar la edición de propietarios
     */
    private int edicion;// estados 0-no es edición, 1-es edición, 2-editado correctamente

    private String mensaje = "";

    /**
     * control para que cuando se ingrese el nit identificacion 999 asume que es predio de la nación
     * y no deja modifica la razon social
     */
    private boolean disableRazonSocial = false;

    /**
     * Variable para manejar los propietarios asociados a lo predios de Cancelacion Masiva
     */
    private List<PPersonaPredio> personasPredioBase;

    /**
     * control para que cuando se valide que el tramite es Cancelacion Masiva
     */
    private boolean banderaCancelacionMasiva;

    /**
     * Variable para manejar el tramite que se carga en la Proyeccion.
     */
    private Tramite tramite;

    /**
     * control para que cuando se valide que el tramite es Cancelacion Masiva
     */
    private boolean banderaVerPredios;

    /**
     * lista de personas predios asociados a un determinado trámite
     */
    private ArrayList<PersonaPredio> personaConPredios;

    /**
     * lista de personas predios asociados a un determinado trámite
     */
    private ArrayList<Long> idPredios;

    /**
     * id del predio del que se van a consultar los predios asociados, para mostrarlos y ver
     * detalles de cada uno
     */
    private Long idDePersonasPredios;

    /**
     * lista de predios asociados a un determinado trámite
     */
    private ArrayList<TramiteDetallePredio> prediosDeTramiteEnglobe;

    /**
     * lista de predios asociados a un determinado trámite
     */
    private List<PPersonaPredio> pPersonasPredioSeleccionado;

    //private List<PPersonaPredio> propietariosNuevosNoCancelados;
    private EntradaProyeccionMB entradaProyeccionMB;

    private HashMap<Long, List<PPredio>> mapPPrediosPorPersona = new HashMap<Long, List<PPredio>>();

    private List<PPredio> prediosPropietario = new ArrayList<PPredio>();

    //--------------------------------GET AND SET----------------------------
    public List<PPredio> getPrediosPropietario() {
        return prediosPropietario;
    }

    public void setPrediosPropietario(List<PPredio> prediosPropietario) {
        this.prediosPropietario = prediosPropietario;
    }

    public PPersonaPredio getPropietarioSeleccionado() {
        return propietarioSeleccionado;
    }

    public HashMap<Long, List<PPredio>> getMapPPrediosPorPersona() {
        return mapPPrediosPorPersona;
    }

    public void setMapPPrediosPorPersona(HashMap<Long, List<PPredio>> mapPPrediosPorPersona) {
        this.mapPPrediosPorPersona = mapPPrediosPorPersona;
    }

    public List<PPersonaPredio> getpPersonasPredioSeleccionado() {
        return pPersonasPredioSeleccionado;
    }

    public void setpPersonasPredioSeleccionado(
        List<PPersonaPredio> pPersonasPredioSeleccionado) {
        this.pPersonasPredioSeleccionado = pPersonasPredioSeleccionado;
    }

    public ArrayList<PersonaPredio> getPersonaConPredios() {
        return personaConPredios;
    }

    public void setPersonaConPredios(ArrayList<PersonaPredio> personaConPredios) {
        this.personaConPredios = personaConPredios;
    }

    public ArrayList<Long> getIdPredios() {
        return idPredios;
    }

    public void setIdPredios(ArrayList<Long> idPredios) {
        this.idPredios = idPredios;
    }

    public Long getIdDePersonasPredios() {
        return idDePersonasPredios;
    }

    public void setIdDePersonasPredios(Long idDePersonasPredios) {
        this.idDePersonasPredios = idDePersonasPredios;
    }

    public ArrayList<TramiteDetallePredio> getPrediosDeTramiteEnglobe() {
        return prediosDeTramiteEnglobe;
    }

    public void setPrediosDeTramiteEnglobe(
        ArrayList<TramiteDetallePredio> prediosDeTramiteEnglobe) {
        this.prediosDeTramiteEnglobe = prediosDeTramiteEnglobe;
    }

    public List<PPersonaPredio> getPersonasPredioBase() {
        return personasPredioBase;
    }

    public void setPersonasPredioBase(List<PPersonaPredio> personasPredioBase) {
        this.personasPredioBase = personasPredioBase;
    }

    public void setPropietarioSeleccionado(
        PPersonaPredio propietarioSeleccionado) {
        this.nuevo = false;
        this.propietarioSeleccionado = propietarioSeleccionado;
    }

    public PPredio getPredioSeleccionado() {
        return proyectarConservacionMB.getPredioSeleccionado();
    }

    public Tramite getTramite() {
        return proyectarConservacionMB.getTramite();
    }

    public List<PPersona> getPropietariosCoincidentes() {
        return propietariosCoincidentes;
    }

    public void setPropietariosCoincidentes(List<PPersona> propietariosCoincidentes) {
        this.propietariosCoincidentes = propietariosCoincidentes;
    }

    public PPersona getPersonaSeleccionada() {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(PPersona personaSeleccionada) {
        this.personaSeleccionada = personaSeleccionada;
    }

    public List<PPersonaPredio> getPropietariosVigentes() {

        return propietariosVigentes;

    }

    public boolean isBanderaCancelacionMasiva() {
        return banderaCancelacionMasiva;
    }

    public void setBanderaCancelacionMasiva(boolean banderaCancelacionMasiva) {
        this.banderaCancelacionMasiva = banderaCancelacionMasiva;
    }

    public List<PersonaPredio> getPropietariosVigentes2() {

        if (this.proyectarConservacionMB.getPredioOrigen() != null) {
            return this.proyectarConservacionMB.getPredioOrigen().getPersonaPredios();
        } else {
            return new ArrayList<PersonaPredio>();
        }
    }

    public PersonaPredio[] getPropietariosVigentesSeleccionados() {
        return propietariosVigentesSeleccionados;
    }

    public void setPropietariosVigentesSeleccionados(
        PersonaPredio[] propietariosVigentesSeleccionados) {
        this.propietariosVigentesSeleccionados = propietariosVigentesSeleccionados;
    }

    public PPersonaPredio[] getPropietariosCanceladosSeleccionados() {
        return propietariosCanceladosSeleccionados;
    }

    public void setPropietariosCanceladosSeleccionados(
        PPersonaPredio[] propietariosCanceladosSeleccionados) {
        this.propietariosCanceladosSeleccionados = propietariosCanceladosSeleccionados;
    }

    public List<PPersonaPredio> getPropietariosNuevos() {

        List<PPersonaPredio> propietariosNuevos = new ArrayList<PPersonaPredio>();

        //@modified by leidy.gonzalez :: #20417:: Valida visualiza Infromacion en los Predios Nuevos,
        // para los tramites de Cancelacion Masiva
        if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

//			if(this.pPersonasPredioSeleccionado != null && !this.pPersonasPredioSeleccionado.isEmpty()){
//					for (PPersonaPredio ppp : this.pPersonasPredioSeleccionado) {
//						
//						if (ppp.getCancelaInscribe() == null || 
//								ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) || 
//								ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
//							propietariosNuevos.add(ppp);
//						}
//					}
//			}else if(this.getPredioSeleccionado() != null
//				&& this.getPredioSeleccionado().getPPersonaPredios() != null
//				&& !this.getPredioSeleccionado().getPPersonaPredios().isEmpty()){
//                for (PPersonaPredio ppp : this.getPredioSeleccionado().getPPersonaPredios()) {
//						
//						if (ppp.getCancelaInscribe() == null || 
//								ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) || 
//								ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
//							propietariosNuevos.add(ppp);
//						}
//					}
//            }
            List<PPersonaPredio> personasPredio =
                this.getConservacionService().
                    buscarPPersonaPrediosPorPredioIdInscritos(this.getPredioSeleccionado().getId());

            if (this.personasPredioBase != null &&
                 !this.personasPredioBase.isEmpty() &&
                 (personasPredio == null || personasPredio.isEmpty())) {

                this.pPersonasPredioSeleccionado = this.getConservacionService().
                    actualizarPPersonaPredioYPropiedadCancelados(this.personasPredioBase,
                        this.usuario, this.getPredioSeleccionado());

                propietariosNuevos.addAll(this.pPersonasPredioSeleccionado);
            } else {
                propietariosNuevos.addAll(this.personasPredioBase);
            }

        } else if (this.getPredioSeleccionado() != null &&
             this.getPredioSeleccionado().getPPersonaPredios() != null &&
             !this.getPredioSeleccionado().getPPersonaPredios().isEmpty()) {
            for (PPersonaPredio ppp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (ppp.getCancelaInscribe() == null ||
                    ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                    propietariosNuevos.add(ppp);
                }
            }
        }

        return propietariosNuevos;
    }

    public boolean isDisableRazonSocial() {
        return disableRazonSocial;
    }

    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.banderaCancelacionMasiva = false;

        if (this.proyectarConservacionMB.getTramite() != null) {
            this.tramite = this.proyectarConservacionMB.getTramite();
        }

        //@modified by leidy.gonzalez::#20417:: Valida TODOS los propietarios Vigentes de los Predios
        //para los tramites de Cancelacion Masiva
        if (this.getPredioSeleccionado() != null &&
             this.getPredioSeleccionado().getPPersonaPredios() != null) {
            this.pPersonasPredioSeleccionado = this.getPredioSeleccionado().getPPersonaPredios();
        }

        this.banderaVerPredios = false;

        if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {
            this.banderaCancelacionMasiva = true;
            this.verificarPropietariosCancelacion();

        }

        this.setupPropietarios();
        //version 1.1.5 - javier.aponte_7525
        this.propietariosVigentes = new ArrayList<PPersonaPredio>();
        this.edicion = 0;
        if (this.getPredioSeleccionado() != null && this.getPredioSeleccionado().
            getPPersonaPredios() != null) {
            for (PPersonaPredio ppp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (ppp.getCancelaInscribe() == null ||
                    ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    propietariosVigentes.add((PPersonaPredio) ppp.clone());
                }
            }
        }
    }

    public void buscarPropietario() {

        List<PPersona> ppersonasAux = null;
        try {
            propietariosCoincidentes = null;

            if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion() != null &&
                this.propietarioSeleccionado.getPPersona()
                    .getNumeroIdentificacion() != null &&
                !this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().equals("0")) {

                ppersonasAux = this.getConservacionService()
                    .buscarPPersonasPorTipoNumeroIdentificacion(
                        this.propietarioSeleccionado.getPPersona()
                            .getTipoIdentificacion(),
                        this.propietarioSeleccionado.getPPersona()
                            .getNumeroIdentificacion());
                ppersonasAux = this.filtrarPropietariosCoincidentes(ppersonasAux);
            }

            /*
             * if (ppersonasAux != null && ppersonasAux.size() == 1) {
             *
             * this.propietarioSeleccionado.getPPersona().setTipoIdentificacion(ppersonasAux.get(0).getTipoIdentificacion());
             * this.propietarioSeleccionado.getPPersona().setNumeroIdentificacion(ppersonasAux.get(0).getNumeroIdentificacion());
             * this.propietarioSeleccionado.getPPersona().setPrimerNombre(ppersonasAux.get(0).getPrimerNombre());
             * this.propietarioSeleccionado.getPPersona().setSegundoNombre(ppersonasAux.get(0).getSegundoNombre());
             * this.propietarioSeleccionado.getPPersona().setPrimerApellido(ppersonasAux.get(0).getPrimerApellido());
             * this.propietarioSeleccionado.getPPersona().setSegundoApellido(ppersonasAux.get(0).getSegundoApellido());
             * this.propietarioSeleccionado.getPPersona().setSigla(ppersonasAux.get(0).getSigla());
             * this.propietarioSeleccionado.getPPersona().setRazonSocial(ppersonasAux.get(0).getRazonSocial());
             * if(ppersonasAux.get(0).getDigitoVerificacion() != null){
             * this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(ppersonasAux.get(0).getDigitoVerificacion());
             * }
             * this.propietarioSeleccionado.getPPersona().setTipoPersona(ppersonasAux.get(0).getTipoPersona());
             *
             * //this.propietarioSeleccionado.getPPersona().setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
             * this.propietarioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
             * this.cambiarCancelaInscribeHijos(this.propietarioSeleccionado,
             * EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
             *
             *
             * this.addMensajeInfo("Propietario encontrado con dicha identificación"); } else {
             */
            if (ppersonasAux == null || ppersonasAux.size() == 0) {
                this.addMensajeWarn("Persona no encontrada en el sistema");
            } else {
                propietariosCoincidentes = ppersonasAux;
                //this.addMensajeError("Existe más de un propietario con esta identificación");
            }
            //se calcula al digito de verificación
            if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                String dv = null;
                if (this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion() != null &&
                    !this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().
                        equals("0")) {
                    dv = String.valueOf(this.getGeneralesService().calcularDigitoVerificacion(
                        this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion()));
                }
                this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(dv);
            } else {
                this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(null);
            }

            this.propietarioSeleccionado.getPPersona().setPrimerNombre(null);
            this.propietarioSeleccionado.getPPersona().setSegundoNombre(null);
            this.propietarioSeleccionado.getPPersona().setPrimerApellido(null);
            this.propietarioSeleccionado.getPPersona().setSegundoApellido(null);
            this.propietarioSeleccionado.getPPersona().setSigla(null);
            this.propietarioSeleccionado.getPPersona().setRazonSocial(null);

            /*
             * PPersona personaAux = this.propietarioSeleccionado .getPPersona();
             * this.adicionarPropietario(); this.propietarioSeleccionado.getPPersona()
             * .setTipoIdentificacion( personaAux.getTipoIdentificacion());
             * this.propietarioSeleccionado.getPPersona() .setNumeroIdentificacion(
								personaAux.getNumeroIdentificacion()); */
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e.getMessage());
        }
    }

    /**
     * Método para filtrar los propietarios duplicados
     *
     * @author felipe.cadena
     * @param propietarios
     * @return
     */
    private List<PPersona> filtrarPropietariosCoincidentes(List<PPersona> propietarios) {
        List<PPersona> resultado = new ArrayList<PPersona>();

        for (PPersona pp : propietarios) {
            boolean existe = false;
            for (int i = 0; i < resultado.size(); i++) {
                PPersona ppAux = resultado.get(i);

                if (pp.getNombre().equals((ppAux.getNombre()))) {
                    existe = true;
                    break;
                }
            }
            if (!existe) {
                resultado.add(pp);
            }
        }

        return resultado;
    }

    /*
     * @modified by juanfelipe.garcia :: 23-04-2014 :: ajustes para el CC-NP-CO-044
     */
    public void aceptarPropietario() {
        // si era null pone M
        // si era I se queda I
        // si era C se queda M lo reactiva  
        // si era M queda M

        // @modified by leidy.gonzalez:: #20417:: Se valida que no permita
        // ingresar num.Identificacion en 0
        if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva() ||
            this.proyectarConservacionMB.getTramite().isEnglobeVirtual()) {
            if (this.propietarioSeleccionado != null &&
                 this.propietarioSeleccionado.getPPersona() != null &&
                 this.propietarioSeleccionado.getPPersona()
                    .getNumeroIdentificacion() != null &&
                 Constantes.CERO.equals(this.propietarioSeleccionado
                    .getPPersona().getNumeroIdentificacion())) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "El propietario no puede tener número de identificación es cero.");
                return;
            }
        }

        // @modified by leidy.gonzalez:: #24701:: Se valida que no permita
        //Se valida que para los tramites de Englobe Virtual se genere una Nueva PPersona
        //Para ser asociada a cada predio
        if (this.proyectarConservacionMB.getTramite().isEnglobeVirtual()) {

            PPersona personaNueva = new PPersona();
            personaNueva = this.propietarioSeleccionado.getPPersona();
            personaNueva.setId(null);

            personaNueva = this.getConservacionService().actualizarPPersona(personaNueva);

            this.propietarioSeleccionado.setPPersona(personaNueva);
        }

        //felipe.cadena :: Se valida no ingresar identificaciones de propietarios en ceros #6747
        String id = this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion();
        boolean ceros = Pattern.matches("0*", id);
        boolean valido = false;
        String nombre = "";
        int comparacionNombres = 0;
        int comparacionCedulas = 0;
        int comparacionNit = 0;
        int comparacionCedulasExtranjeria = 0;
        int comparacionCedulasMilitares = 0;
        int comparacionLibretasMilitares = 0;
        int comparacionNuip = 0;
        int comparacionPasaportes = 0;
        int comparacionRegistrosCiviles = 0;
        int comparacionTarjetasIdentidad = 0;
        //Para tipo de identificación cedula de ciudadana se
        // permite que registre en el nmero de identificacin el valor cero (0)

        this.propietarioSeleccionado.getPPersona().setNumeroIdentificacion(
            this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().toUpperCase());

        if (id.equals("0") &&
             (this.propietarioSeleccionado.getPPersona().
                getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo()) ||
            this.propietarioSeleccionado.getPPersona().
                getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.NIT.getCodigo()))) {
            valido = true;
        } else if ((ceros && !this.getTramite().isEsRectificacion()) || (ceros &&
            (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.NUIP.getCodigo()) || this.propietarioSeleccionado.
            getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.PASAPORTE.getCodigo()) ||
            this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.CEDULA_EXTRANJERIA.getCodigo())))) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "El número de identificación del propietario no puede contener solo ceros");
            return;
        }

        if (this.propietarioSeleccionado.getPPersona().getPrimerNombre() != null &&
             !this.propietarioSeleccionado.getPPersona().getPrimerNombre().isEmpty()) {
            this.propietarioSeleccionado.getPPersona().setPrimerNombre(this.propietarioSeleccionado.
                getPPersona().getPrimerNombre().toUpperCase());
            nombre = nombre.concat(this.propietarioSeleccionado.getPPersona().getPrimerNombre()).
                concat("_");
        }
        if (this.propietarioSeleccionado.getPPersona().getPrimerApellido() != null &&
             !this.propietarioSeleccionado.getPPersona().getPrimerApellido().isEmpty()) {
            this.propietarioSeleccionado.getPPersona().setPrimerApellido(
                this.propietarioSeleccionado.getPPersona().getPrimerApellido().toUpperCase());
            nombre = nombre.concat(this.propietarioSeleccionado.getPPersona().getPrimerApellido()).
                concat("_");
        }
        if (this.propietarioSeleccionado.getPPersona().getSegundoNombre() != null &&
             !this.propietarioSeleccionado.getPPersona().getSegundoNombre().isEmpty()) {
            this.propietarioSeleccionado.getPPersona().setSegundoNombre(
                this.propietarioSeleccionado.getPPersona().getSegundoNombre().toUpperCase());
            nombre = nombre.concat(this.propietarioSeleccionado.getPPersona().getSegundoNombre()).
                concat("_");
        }
        if (this.propietarioSeleccionado.getPPersona().getSegundoApellido() != null &&
             !this.propietarioSeleccionado.getPPersona().getSegundoApellido().isEmpty()) {
            this.propietarioSeleccionado.getPPersona().setSegundoApellido(
                this.propietarioSeleccionado.getPPersona().getSegundoApellido().toUpperCase());
            nombre = nombre.concat(this.propietarioSeleccionado.getPPersona().getSegundoApellido()).
                concat("_");
        }
        if (this.propietarioSeleccionado.getPPersona().getRazonSocial() != null &&
             !this.propietarioSeleccionado.getPPersona().getRazonSocial().isEmpty()) {
            this.propietarioSeleccionado.getPPersona().setRazonSocial(this.propietarioSeleccionado.
                getPPersona().getRazonSocial().toUpperCase());
            nombre = nombre.concat(this.propietarioSeleccionado.getPPersona().getRazonSocial());
        }

        //Si existe más de un propietario con tipo de identificación Cedula de ciudadana y número de 
        //identificación cero; se debe validar que el nombre de los propietarios son diferentes entre sí. 
        if (valido) {
            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                String name = "";

                if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo())) {
                    if (pp.getPPersona().getPrimerNombre() != null &&
                         !pp.getPPersona().getPrimerNombre().isEmpty()) {
                        name = name.concat(pp.getPPersona().getPrimerNombre()).concat("_");
                    }
                    if (pp.getPPersona().getPrimerApellido() != null &&
                         !pp.getPPersona().getPrimerApellido().isEmpty()) {
                        name = name.concat(pp.getPPersona().getPrimerApellido()).concat("_");
                    }
                    if (pp.getPPersona().getSegundoNombre() != null &&
                         !pp.getPPersona().getSegundoNombre().isEmpty()) {
                        name = name.concat(pp.getPPersona().getSegundoNombre()).concat("_");
                    }
                    if (pp.getPPersona().getSegundoApellido() != null &&
                         !pp.getPPersona().getSegundoApellido().isEmpty()) {
                        name = name.concat(pp.getPPersona().getSegundoApellido()).concat("_");
                    }

                    if (nombre.equals(name)) {
                        comparacionNombres++;
                    }
                } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                    if (pp.getPPersona().getRazonSocial() != null &&
                         !pp.getPPersona().getRazonSocial().isEmpty()) {
                        name = name.concat(pp.getPPersona().getRazonSocial()).concat("_");
                    }

                    if (nombre.equals(name)) {
                        comparacionNombres++;
                    }
                }

                if (this.nuevo && comparacionNombres > 0 &&
                     this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("El propietario con número de identificación " +
                        this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion() +
                         " ya fue agregado");
                    return;

                } else if (!this.nuevo && comparacionNombres > 1 &&
                     this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("El propietario con número de identificación " +
                        this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion() +
                         " ya fue agregado");
                    return;

                } else if (this.nuevo && comparacionNombres > 0) {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("Existe mas de un propietario repetido");
                    return;
                } else if (!this.nuevo && comparacionNombres > 1) {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("Existe mas de un propietario repetido");
                    return;
                }

            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo())) {

            ///la edicion debe validar 
            //Si los propietarios tienen cedula diferente de cero se debe validar que estas deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionCedulas++;
                }
            }

            if (this.nuevo && comparacionCedulas > 0 &&
                 this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("El propietario con número de identificación " +
                    this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion() +
                     " ya fue agregado");
                return;

            } else if (!this.nuevo && comparacionCedulas > 1 &&
                 this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("El propietario con número de identificación " +
                    this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion() +
                     " ya fue agregado");
                return;

            } else if (this.nuevo && comparacionCedulas > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionCedulas > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {

            //@modified by hector.arias:: #12893:: 09/05/2017:: Se valida que no se repitan propietarios con el mismo NIT.
            //Si los propietarios tienen NIT diferente de cero se debe validar que estas deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.NIT.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionNit++;
                }
            }

            if (this.nuevo && comparacionNit > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionNit > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.CEDULA_EXTRANJERIA.getCodigo())) {

            //@modified by hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con la misma cedula 
            //de extrangeria. Si los propietarios tienen cedula de estrangeria diferente de cero se debe validar que estas 
            //deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.CEDULA_EXTRANJERIA.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionCedulasExtranjeria++;
                }
            }

            if (this.nuevo && comparacionCedulasExtranjeria > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionCedulasExtranjeria > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.CEDULA_MILITAR.getCodigo())) {

            //hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con la misma Cedula Militar.
            //Si los propietarios tienen Cedula Militar diferente de cero se debe validar que estas deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.CEDULA_MILITAR.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionCedulasMilitares++;
                }
            }

            if (this.nuevo && comparacionCedulasMilitares > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionCedulasMilitares > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.LIBRETA_MILITAR.getCodigo())) {

            //@modified by hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con la misma
            //Libreta Militar. Si los propietarios tienen Libreta Militar diferente de cero se debe validar que estas deben ser 
            //diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.LIBRETA_MILITAR.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionLibretasMilitares++;
                }
            }

            if (this.nuevo && comparacionLibretasMilitares > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionLibretasMilitares > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.NUIP.getCodigo())) {

            //@modified by hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con el mismo NUIP.
            //Si los propietarios tienen NUIP diferente de cero se debe validar que estas deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.NUIP.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionNuip++;
                }
            }

            if (this.nuevo && comparacionNuip > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionNuip > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.PASAPORTE.getCodigo())) {

            //@modified by hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con el mismo Pasaporte.
            //Si los propietarios tienen Paraporte diferente de cero se debe validar que estas deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.PASAPORTE.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionPasaportes++;
                }
            }

            if (this.nuevo && comparacionPasaportes > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionPasaportes > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.REGISTRO_CIVIL.getCodigo())) {

            //@modified by hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con el mismo 
            //Registro civil. Si los propietarios tienen NIT diferente de cero se debe validar que estas deben ser 
            //diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.REGISTRO_CIVIL.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionRegistrosCiviles++;
                }
            }

            if (this.nuevo && comparacionRegistrosCiviles > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionRegistrosCiviles > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        } else if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.TARJETA_IDENTIDAD.getCodigo())) {

            //@modified by hector.arias:: #12893:: 19/05/2017:: Se valida que no se repitan propietarios con el mismo NIT.
            //Si los propietarios tienen NIT diferente de cero se debe validar que estas deben ser diferentes entre sí
            String numIdentificacion = this.propietarioSeleccionado.getPPersona().
                getNumeroIdentificacion();

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (pp.getPPersona().getTipoIdentificacion().
                    equals(EPersonaTipoIdentificacion.TARJETA_IDENTIDAD.getCodigo()) &&
                     numIdentificacion.trim().equals(pp.getPPersona().getNumeroIdentificacion().
                        trim()) &&
                     pp.getCancelaInscribe() != null && !pp.getCancelaInscribe().equals("C")) {

                    comparacionTarjetasIdentidad++;
                }
            }

            if (this.nuevo && comparacionTarjetasIdentidad > 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            } else if (!this.nuevo && comparacionTarjetasIdentidad > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Existe mas de un propietario repetido");
                return;
            }
        }

        try {
            if (this.getConservacionService().conteoPersonaPrediosPorPersonaId(
                this.propietarioSeleccionado.getPPersona().getId()) <= 1) {
                this.propietarioSeleccionado.setPPredio(this
                    .getPredioSeleccionado());
                this.propietarioSeleccionado.setTipo(this.generalMB
                    .getPersonaPredioTipoV().get(0).getLabel().toString()
                    .toUpperCase());

                //lorena.salamanca :: 12-11-2015 :: Redmine 11137 - Para la complementacion de participacion y nombres
                //se guarda la proyeccion porque los registros estaban quedando sin Cancela Inscribe y no guardaba bien.
                if (this.getTramite().isEsComplementacion()) {
                    for (TramiteRectificacion tr : this.getTramite().getTramiteRectificacions()) {
                        if (tr.getDatoRectificar().getNombre().equals(
                            EDatoRectificarNombre.PARTICIPACION.getCodigo()) ||
                            tr.getDatoRectificar().getNombre().equals(
                                EDatoRectificarNombre.SEGUNDO_APELLIDO.getCodigo()) ||
                            tr.getDatoRectificar().getNombre().equals(
                                EDatoRectificarNombre.SEGUNDO_NOMBRE.getCodigo())) {
                            this.propietarioSeleccionado.setCancelaInscribe(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                            this.propietarioSeleccionado.getPPersona().setCancelaInscribe(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                            //this.propietarioSeleccionado.getPPersona().getpp 

                        }
                    }
                }

                PPersonaPredio personaPredio = (PPersonaPredio) this
                    .getConservacionService().guardarPPersonaPredio(
                        usuario, this.propietarioSeleccionado);
                PPersona persona = personaPredio.getPPersona();
                /*
                 * PPersona persona = (PPersona) this.conservacionService
                 * .guardarProyeccion(usuario, this.propietarioSeleccionado.getPPersona());
                 */

                this.propietarioSeleccionado.setId(personaPredio.getId());
                this.propietarioSeleccionado.setCancelaInscribe(personaPredio
                    .getCancelaInscribe());
                this.propietarioSeleccionado
                    .setPorcentajeParticipacion(personaPredio
                        .getPorcentajeParticipacion());
                this.propietarioSeleccionado.getPPersona().setId(
                    persona.getId());
                // this.propietarioSeleccionado.getPPersona().setCancelaInscribe(persona.getCancelaInscribe());
                this.cambiarCancelaInscribeHijos(this.propietarioSeleccionado,
                    personaPredio.getCancelaInscribe());
                this.propietarioSeleccionado.setUsuarioLog(this.usuario
                    .getLogin());
                this.propietarioSeleccionado.setFechaLog(new Date());

                if (this.nuevo) {
                    this.getPredioSeleccionado().getPPersonaPredios()
                        .add(propietarioSeleccionado);
                }

                if (this.edicion == 1) {
                    this.edicion = 2;
                }
                //this.listaPropietariosNuevos.put(propietarioSeleccionado.getId(),propietarioSeleccionado);

                //this.addMensajeInfo("Información almacenada");
                this.mensaje = "Información almacenada";
            } else if (this.getConservacionService().conteoPersonaPrediosPorPersonaId(
                this.propietarioSeleccionado.getPPersona().getId()) > 1) {
                PPersona personaNueva = new PPersona();
                personaNueva = this.propietarioSeleccionado.getPPersona();
                personaNueva.setId(null);

                personaNueva = this.getConservacionService().actualizarPPersona(personaNueva);

                this.propietarioSeleccionado.setPPredio(this
                    .getPredioSeleccionado());
                this.propietarioSeleccionado.setTipo(this.generalMB
                    .getPersonaPredioTipoV().get(0).getLabel().toString()
                    .toUpperCase());
                this.propietarioSeleccionado.setPPersona(personaNueva);

                //lorena.salamanca :: 12-11-2015 :: Redmine 11137 - Para la complementacion de participacion y nombres
                //se guarda la proyeccion porque los registros estaban quedando sin Cancela Inscribe y no guardaba bien.
                if (this.getTramite().isEsComplementacion()) {
                    for (TramiteRectificacion tr : this.getTramite().getTramiteRectificacions()) {
                        if (tr.getDatoRectificar().getNombre().equals(
                            EDatoRectificarNombre.PARTICIPACION.getCodigo()) ||
                            tr.getDatoRectificar().getNombre().equals(
                                EDatoRectificarNombre.SEGUNDO_APELLIDO.getCodigo()) ||
                            tr.getDatoRectificar().getNombre().equals(
                                EDatoRectificarNombre.SEGUNDO_NOMBRE.getCodigo())) {
                            this.propietarioSeleccionado.setCancelaInscribe(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                            this.propietarioSeleccionado.getPPersona().setCancelaInscribe(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                            //this.propietarioSeleccionado.getPPersona().getpp 

                        }
                    }
                }

                PPersonaPredio personaPredio = (PPersonaPredio) this
                    .getConservacionService().guardarPPersonaPredio(
                        usuario, this.propietarioSeleccionado);
                PPersona persona = personaPredio.getPPersona();

                /*
                 * PPersona persona = (PPersona) this.conservacionService
                 * .guardarProyeccion(usuario, this.propietarioSeleccionado.getPPersona());
                 */
                this.propietarioSeleccionado.setId(personaPredio.getId());
                this.propietarioSeleccionado.setCancelaInscribe(personaPredio
                    .getCancelaInscribe());
                this.propietarioSeleccionado
                    .setPorcentajeParticipacion(personaPredio
                        .getPorcentajeParticipacion());
                this.propietarioSeleccionado.getPPersona().setId(
                    persona.getId());
                // this.propietarioSeleccionado.getPPersona().setCancelaInscribe(persona.getCancelaInscribe());
                this.cambiarCancelaInscribeHijos(this.propietarioSeleccionado,
                    personaPredio.getCancelaInscribe());
                this.propietarioSeleccionado.setUsuarioLog(this.usuario
                    .getLogin());
                this.propietarioSeleccionado.setFechaLog(new Date());

                if (this.nuevo) {
                    this.getPredioSeleccionado().getPPersonaPredios()
                        .add(propietarioSeleccionado);
                }

                //this.listaPropietariosNuevos.put(propietarioSeleccionado.getId(),propietarioSeleccionado);
                if (this.edicion == 1) {
                    this.edicion = 2;
                }

                //this.addMensajeInfo("Información almacenada");
                this.mensaje = "Información almacenada";
            }
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }
    }

    /*
     * public void eliminarPropietario(){ this.getPredioSeleccionado().getPPersonaPredios().remove(
     * this.propietarioSeleccionado); this.conservacionService.eliminarPPersonaPredio
     * (this.propietarioSeleccionado); }
     */
    //TODO: fredy.wilches	
    /* public void eliminarPropietario() {
     * this.propietariosNuevos.remove(this.propietarioSeleccionado);
	} */
    public void cancelarPropietario() {
        // si era null queda C
        // si era I se elimina
        // si era C se queda C No deberia darse
        // si era M queda C
        try {
            PPersonaPredio pp = (PPersonaPredio) this.getConservacionService()
                .eliminarProyeccion(usuario, this.propietarioSeleccionado);
            if (this.propietarioSeleccionado.getCancelaInscribe() != null &&
                 !this.propietarioSeleccionado.getCancelaInscribe().isEmpty() &&
                 this.propietarioSeleccionado.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {

                //En este punto un mismo documento puede estar como I porque lo adicionaron y como M o C porque era vigente
//				if(this.proyectarConservacionMB.getTramite().isCancelacionMasiva() ){
//                    for (PPersonaPredio personaPredioSel : this.pPersonasPredioSeleccionado) {
//                        if(this.propietarioSeleccionado != null
//                                && this.propietarioSeleccionado.equals(personaPredioSel)){
//                            
//                            this.getConservacionService().eliminarPPersonaPredio(personaPredioSel);
//                        }
//                        
//                    }
//                }
                if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

                    this.pPersonasPredioSeleccionado.remove(this.propietarioSeleccionado);

                }

                this.getPredioSeleccionado().getPPersonaPredios().remove(
                    this.propietarioSeleccionado);
                this.propietarioSeleccionado = new PPersonaPredio();
                this.addMensajeInfo("Propietario eliminado");
            } else {
                this.propietarioSeleccionado.setCancelaInscribe(pp.getCancelaInscribe());
                this.cambiarCancelaInscribeHijos(this.propietarioSeleccionado, pp.
                    getCancelaInscribe());

                this.addMensajeInfo("Propietario cancelado");
            }

            //Ahora es necesario que se recalcule los valores de la tabla p_predio_avaluo_catastral
            //después de cancelar o eliminar el propietario
            this.getFormacionService()
                .revertirProyeccionAvaluosParaUnPredio(
                    this.getTramite().getId(),
                    this.getPredioSeleccionado().getId());

            if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {
                this.getPropietariosNuevos();
            }

            if (getPropietariosNuevos() == null && getPropietariosNuevos().isEmpty() &&
                this.proyectarConservacionMB.getTramite().isPrimera()) {
                this.proyectarConservacionMB.setBanderaTramitePrimeraSinInscripcionYDecretos(true);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }
    }

    /**
     * Método invocado cuando se dispone a la creación de una nueva persona.
     */
    public void adicionarPropietario() {
        // inicia I
        this.propietarioSeleccionado = new PPersonaPredio();
        this.nuevo = true;

        this.propietarioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
            getCodigo());

        PPersona p = new PPersona();
        this.propietarioSeleccionado.setPPersona(p);
        p.setDireccionPais(null);
        p.setDireccionDepartamento(null);
        p.setDireccionMunicipio(null);
        //p.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
        p.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
        p.setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
        p.setUsuarioLog(this.usuario.getLogin());
        p.setFechaLog(new Date());
        this.cambiarCancelaInscribeHijos(this.propietarioSeleccionado,
            EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
    }

    public void asociarPropietariosVigentes() {

        int contador = 0;
        List<PPersonaPredio> pPersonaPredioAux = new ArrayList<PPersonaPredio>();

        //@modified by leidy.gonzalez :: #20417:: Modifica Inscribe de Propietarios Proyectados	Asoc
        if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {

            if (propietariosCanceladosSeleccionados.length <= 0) {
                this.addMensajeWarn("Debe seleccionar al menos un propietario a asociar");
                return;
            }

            for (PPersonaPredio pVigenteSelect : propietariosCanceladosSeleccionados) {

                contador = 0;

                if (this.pPersonasPredioSeleccionado != null && !this.pPersonasPredioSeleccionado.
                    isEmpty()) {

                    for (PPersonaPredio pNuevo : this.pPersonasPredioSeleccionado) {

                        //Si el tipo de identificación es X "No especificado" válida que los nombres sean diferentes
                        if (EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo().equals(
                            pVigenteSelect.getPPersona().getTipoIdentificacion()) ||
                             pVigenteSelect.getPPersona().getNumeroIdentificacion().equals("0")) {
                            if (pNuevo.getCancelaInscribe() != null && !pNuevo.getCancelaInscribe().
                                isEmpty() &&
                                 !pNuevo.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.CANCELA.getCodigo()) &&
                                 pVigenteSelect.getPPersona().getNombre().equalsIgnoreCase(pNuevo.
                                    getPPersona().getNombre())) {

                                contador++;
                            }
                        } else {
                            if (pNuevo.getCancelaInscribe() != null && !pNuevo.getCancelaInscribe().
                                isEmpty() &&
                                 !pNuevo.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.CANCELA.getCodigo()) &&
                                 pVigenteSelect.getPPersona().getTipoIdentificacion().equals(pNuevo.
                                    getPPersona().getTipoIdentificacion()) &&
                                 pVigenteSelect.getPPersona().getNumeroIdentificacion().equals(
                                    pNuevo.getPPersona().getNumeroIdentificacion())) {

                                contador++;
                            }
                        }

                    }
                }

                if (contador >= 1) {

                    if (EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo().equals(
                        pVigenteSelect.getPPersona().getTipoIdentificacion())) {
                        this.addMensajeError("El propietario con identificación: " + pVigenteSelect.
                            getPPersona().getTipoIdentificacion() + " " +
                             pVigenteSelect.getPPersona().getNumeroIdentificacion() + ", nombre: " +
                            pVigenteSelect.getPPersona().getNombre() +
                             " ya fue agregado");
                        return;
                    } else {
                        this.addMensajeError("El propietario con identificación: " + pVigenteSelect.
                            getPPersona().getTipoIdentificacion() + " " +
                             pVigenteSelect.getPPersona().getNumeroIdentificacion() +
                            " ya fue agregado");
                        return;
                    }
                }
            }
            if (contador <= 0) {
                this.pPersonasPredioSeleccionado = this.getConservacionService().
                    actualizarPPersonaPredioYPropiedadCancelados(this.pPersonasPredioSeleccionado,
                        this.usuario, this.getPredioSeleccionado());

                if (this.pPersonasPredioSeleccionado != null && !this.pPersonasPredioSeleccionado.
                    isEmpty()) {
                    this.getPredioSeleccionado().
                        setPPersonaPredios(this.pPersonasPredioSeleccionado);
                    this.getPropietariosNuevos();
                }
            }
        } else {

            if (propietariosVigentesSeleccionados.length <= 0) {
                this.addMensajeWarn("Debe seleccionar al menos un propietario a asociar");

            } else {
                for (PersonaPredio pVigenteSelect : propietariosVigentesSeleccionados) {

                    contador = 0;
                    for (PPersonaPredio pNuevo : this.getPredioSeleccionado().getPPersonaPredios()) {

                        if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pNuevo.
                            getCancelaInscribe()) ||
                             EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pNuevo.
                                getCancelaInscribe())) {

                            //Si el tipo de identificación es X "No especificado" válida que los nombres sean diferentes
                            if (EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo().equals(
                                pVigenteSelect.getPersona().getTipoIdentificacion()) ||
                                 pVigenteSelect.getPersona().getNumeroIdentificacion().equals("0")) {
                                if (pVigenteSelect.getPersona().getNombre().equalsIgnoreCase(pNuevo.
                                    getPPersona().getNombre())) {
                                    contador++;
                                }
                            } else {
                                if (pVigenteSelect.getPersona().getTipoIdentificacion().equals(
                                    pNuevo.getPPersona().getTipoIdentificacion()) &&
                                     pVigenteSelect.getPersona().getNumeroIdentificacion().equals(
                                        pNuevo.getPPersona().getNumeroIdentificacion())) {
                                    contador++;
                                }
                            }
                        }
                    }
                    if (contador >= 1) {

                        if (EPersonaTipoIdentificacion.NO_ESPECIFICADO.getCodigo().equals(
                            pVigenteSelect.getPersona().getTipoIdentificacion())) {
                            this.addMensajeError("El propietario con identificación: " +
                                pVigenteSelect.getPersona().getTipoIdentificacion() + " " +
                                 pVigenteSelect.getPersona().getNumeroIdentificacion() +
                                ", nombre: " +
                                pVigenteSelect.getPersona().getNombre() +
                                 " ya fue agregado");
                        } else {
                            this.addMensajeError("El propietario con identificación: " +
                                pVigenteSelect.getPersona().getTipoIdentificacion() + " " +
                                 pVigenteSelect.getPersona().getNumeroIdentificacion() +
                                " ya fue agregado");
                        }
                        continue;
                    }

                }
            }
            if (contador <= 0) {
                pPersonaPredioAux = this.getConservacionService().
                    actualizarPPersonaPredioAndPPersonaPredioPropiedad(
                        this.propietariosVigentesSeleccionados, this.getPredioSeleccionado().
                            getPPersonaPredios(), this.usuario);

                if (pPersonaPredioAux != null && !pPersonaPredioAux.isEmpty()) {
                    this.getPredioSeleccionado().setPPersonaPredios(pPersonaPredioAux);
                }
            }
        }

    }

    /*
     * @modified by juanfelipe.garcia :: 23-04-2014 :: ajustes para el CC-NP-CO-044 @modified by
     * juanfelipe.garcia :: 15-05-2014 :: ajuste para complementaciones #7229
     */
    public boolean validarSeccion() {
        if (this.getTramite().isModificacionInscripcionCatastral()) {
            return true;
        }
        boolean valido = true;
        List<PPersonaPredio> nuevos = this.getPropietariosNuevos();

        if (nuevos == null || nuevos.size() == 0) {
            this.addMensajeError("Debe haber al menos un nuevo propietario/poseedor");
            valido = false;
        }
        int cuantos = 0;

        String tipoIdentificacion;
        String numeroIdentificacion;
        String nombre;
        String name;

        for (PPersonaPredio pp : nuevos) {
            tipoIdentificacion = pp.getPPersona().getTipoIdentificacion();
            numeroIdentificacion = pp.getPPersona().getNumeroIdentificacion();

            nombre = this.concatenarNombrePersona(pp.getPPersona());

            cuantos = 0;
            //Para tipo de identificación cedula de ciudadana se
            // permite que registre en el nmero de identificacin el valor cero (0)
            for (PPersonaPredio pp2 : nuevos) {

                name = this.concatenarNombrePersona(pp2.getPPersona());

                if (numeroIdentificacion.equals("0") &&
                     (tipoIdentificacion.
                        equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo()) ||
                     tipoIdentificacion.
                        equals(EPersonaTipoIdentificacion.NIT.getCodigo()))) {

                    if (nombre.equalsIgnoreCase(name)) {
                        cuantos++;
                    }

                } else if (!numeroIdentificacion.equals("0") && tipoIdentificacion.equals(pp2.
                    getPPersona().getTipoIdentificacion()) && numeroIdentificacion.equals(pp2.
                        getPPersona().getNumeroIdentificacion())) {
                    cuantos++;
                }
            }
            if (cuantos > 1) {
                Tramite tramiteProyectado = this.proyectarConservacionMB.getTramite();
                if (!tramiteProyectado.isRectificacion() &&
                    !tramiteProyectado.isRevisionAvaluo() &&
                    !tramiteProyectado.isCuarta() &&
                    !tramiteProyectado.isSegundaDesenglobe() &&
                    !tramiteProyectado.isTercera() &&
                    !tramiteProyectado.isEsComplementacion()) {
                    this.addMensajeError(
                        "No puede haber mas de un propietario/poseedor con la misma identificación, o el mismo nombre si el número de identificación es cero");
                    valido = false;
                }
            }
        }

        // validación 100%
        boolean validarAvance = true;
        boolean validarTodasPart = true;
        int numPorcentajes = 0;
        Double percentage = new Double(0);
        for (PPersonaPredio ppp : this.getPropietariosNuevos()) {
            if (ppp.getParticipacion() != null && ppp.getParticipacion() != 0.0) {
                percentage += ppp.getParticipacion().doubleValue();
            }
            if (ppp.getParticipacion() != null) {
                numPorcentajes++;
            }
        }
        //version 1.1.5 - javier.aponte_7525
        percentage = Math.rint(percentage * 100) / 100;

        validarTodasPart = (numPorcentajes == this.getPropietariosNuevos().size()) ||
             (percentage.equals(Double.valueOf(0)) && numPorcentajes == 0);

        validarAvance = percentage.equals(Double.valueOf(100)) ||
             (percentage.equals(Double.valueOf(0)) && numPorcentajes == 0);

        if (!validarAvance) {
            valido = false;
            this.addMensajeError("La suma de participación debe ser igual a 100");
        }
        if (!validarTodasPart) {
            valido = false;
            this.addMensajeError(
                "Si existe participación, esta debe estar registrada para todos los propietarios del predio");
        }
        return valido;
    }

    /**
     * Método encargado de concatenar el nombre de la persona
     *
     * @author javier.aponte
     */
    public String concatenarNombrePersona(PPersona persona) {

        String nombre = new String();
        if (persona.getPrimerNombre() != null &&
             !persona.getPrimerNombre().isEmpty()) {
            nombre = nombre.concat(persona.getPrimerNombre()).concat("_");
        }
        if (persona.getPrimerApellido() != null &&
             !persona.getPrimerApellido().isEmpty()) {
            nombre = nombre.concat(persona.getPrimerApellido()).concat("_");
        }
        if (persona.getSegundoNombre() != null &&
             !persona.getSegundoNombre().isEmpty()) {
            nombre = nombre.concat(persona.getSegundoNombre()).concat("_");
        }
        if (persona.getSegundoApellido() != null &&
             !persona.getSegundoApellido().isEmpty()) {
            nombre = nombre.concat(persona.getSegundoApellido()).concat("_");
        }
        if (persona.getRazonSocial() != null &&
             !persona.getRazonSocial().isEmpty()) {
            nombre = nombre.concat(persona.getRazonSocial());
        }

        return nombre;
    }

    /**
     * Llamado al método que realiza los cambios en las tablas la proyección.
     */
    /*
     * @modified juanfelipe.garcia :: 07-03-2014 :: refs 7185 @modified by juanfelipe.garcia ::
     * 23-04-2014 :: ajustes para el CC-NP-CO-044
     */
    public void validarGuardar() {

        boolean resultadoValidacion = this.validarSeccion();
        proyectarConservacionMB.getValidezSeccion().put(ESeccionEjecucion.PROPIETARIOS,
            resultadoValidacion);

        //felipe.cadena :: Se valida no ingresar identificaciones de propietarios en ceros #6747
        //javier.aponte :: #13446 Se agrega validación para que el usuario log de PPersona no sea MIG2015 :: 14-07-2015
        for (PPersonaPredio ppp : this.getPropietariosNuevos()) {
            if (!ppp.getPPersona().getUsuarioLog().
                equals(Constantes.USUARIO_LOG_MIGRADOS) &&
                 !Constantes.USUARIO_LOG_MIGRADOS_2015.
                    equals(ppp.getPPersona().getUsuarioLog())) {
                String id = ppp.getPPersona().getNumeroIdentificacion();
                boolean ceros = Pattern.matches("0*", id);
                //Para tipo de identificación cedula de ciudadana se
                // permite que registre en el nmero de identificacin el valor cero (0)
                if (id.equals("0") &&
                     (ppp.getPPersona().
                        getTipoIdentificacion().
                        equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo()) ||
                    ppp.getPPersona().
                        getTipoIdentificacion().
                        equals(EPersonaTipoIdentificacion.NIT.getCodigo()))) {

                } else if (ceros && !this.getTramite().isEsRectificacion() && !this.getTramite().
                    isTercera() && !this.getTramite().isPrimera() &&
                     !this.getTramite().isEsTramiteDeRevisionDeAvaluo() && !this.getTramite().
                    isEsComplementacion() &&
                     !this.getTramite().isCancelacionPredio() && !this.getTramite().
                    isTramiteDeAutoavaluo()) {
                    this.addMensajeError("El número de identificación del propietario " + ppp.
                        getPPersona().getNombre() + " no puede contener solo ceros");
                    return;
                }
            }
        }

        for (PPersonaPredio ppp : this.getPropietariosNuevos()) {
            ppp.setPPredio(this.getPredioSeleccionado());
        }
        try {
            this.getPredioSeleccionado().setPPredioAvaluoCatastrals(null);
            this.proyectarConservacionMB.setPredioSeleccionado(this.getConservacionService().
                guardarActualizarPPredio(this.getPredioSeleccionado()));
            if (!this.proyectarConservacionMB.getTramite().isQuintaMasivo()) {
                this.addMensajeInfo("Propietarios/poseedores almacenados de forma exitosa");
            }
        } catch (Exception e) {
            proyectarConservacionMB.getValidezSeccion().put(ESeccionEjecucion.PROPIETARIOS, false);
            this.addMensajeError("Hubo problemas guardando los propietarios/poseedores");
        }

    }

    public void tipoIdentificacionChange() {
        if (!this.getTramite().isRectificacion() && !this.getTramite().isEsComplementacion()) {
            this.disableRazonSocial = false;
            if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion()
                .equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                this.propietarioSeleccionado.getPPersona().setTipoPersona(
                    EPersonaTipoPersona.JURIDICA.getCodigo());
                if (this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion() != null &&
                    this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().equals(
                        "999")) {
                    this.disableRazonSocial = true;
                }
            } else {
                this.propietarioSeleccionado.getPPersona().setTipoPersona(
                    EPersonaTipoPersona.NATURAL.getCodigo());
            }
        }
    }

    public void numeroIdentificacionChange() {

        //felipe.cadena :: Se valida no ingresar identificaciones de propietarios en ceros #6747
        String id = this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion();
        boolean ceros = Pattern.matches("0*", id);

        boolean valido = false;
        //Para tipo de identificación cedula de ciudadana se
        // permite que registre en el nmero de identificacin el valor cero (0)
        //Se agrega código que permite registrar el número de identificación el valor cero (0) cuando el tipo de identificación
        //es NIT según el control de cambios CC-NP-CO-066
        if (id.equals("0") &&
             (this.propietarioSeleccionado.getPPersona().
                getTipoIdentificacion().equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.
                    getCodigo()) ||
            this.propietarioSeleccionado.getPPersona().
                getTipoIdentificacion().equals(EPersonaTipoIdentificacion.NIT.getCodigo()))) {
            valido = true;
        } else if (ceros && !this.getTramite().isEsRectificacion()) {

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "El número de identificación del propietario no puede contener solo ceros");
            return;
        }

        //@modified by leidy.gonzalez:: #20417:: Se valida que no permita ingresar num.Identificacion en 0 
        if (this.proyectarConservacionMB.getTramite().isCancelacionMasiva() ||
             this.proyectarConservacionMB.getTramite().isEnglobeVirtual()) {
            if (this.propietarioSeleccionado != null &&
                 this.propietarioSeleccionado.getPPersona() != null &&
                 this.propietarioSeleccionado.getPPersona()
                    .getNumeroIdentificacion() != null &&
                 Constantes.CERO.equals(this.propietarioSeleccionado
                    .getPPersona().getNumeroIdentificacion())) {

                this.addMensajeError(
                    "El propietario no puede tener número de identificación es cero.");
                return;
            }
        }

        //se calcula al digito de verificación
        //Se agrega validación para que cuando el número de identificación sea cero no calcule el dv
        if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion().
            equals(EPersonaTipoIdentificacion.NIT.getCodigo()) &&
            !this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().equals("0")) {
            String dv = String.valueOf(this.getGeneralesService().calcularDigitoVerificacion(
                this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion()));

            this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(dv);
        } else {
            this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(null);
        }

        if (!valido && !this.getTramite().isRectificacion() && !this.getTramite().
            isEsComplementacion()) {

            this.buscarPropietario();
        }
        this.disableRazonSocial = false;
        if (this.propietarioSeleccionado.getPPersona().getTipoIdentificacion()
            .equals(EPersonaTipoIdentificacion.NIT.getCodigo()) &&
             this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().equals(
                Constantes.NIT_LA_NACION)) {
            this.disableRazonSocial = true;
            this.propietarioSeleccionado.getPPersona().setRazonSocial(
                Constantes.RAZON_SOCIAL_LA_NACION);

        }

    }

    /* public List<PPersonaPredio> getPropietariosNuevosNoCancelados() { return
     * this.propietariosNuevosNoCancelados; }
     *
     * public void updatePropietariosNuevosNoCancelados() { this.propietariosNuevosNoCancelados =
     * new ArrayList<PPersonaPredio>(); for (PPersonaPredio ppp : this.propietariosNuevos) { if
     * (ppp.getCancelaInscribe() == null || !ppp.getCancelaInscribe().equals(
     * EProyeccionCancelaInscribe.CANCELA.getCodigo())) { propietariosNuevosNoCancelados.add(ppp); }
     * }
	} */
    /**
     * Método encargado de establecer las condiciones iniciales para los propietarios, los que
     * tengan CANCELA_INSCRIBE en null pasan a C
     */
    public void setupPropietarios() {

        if (this.getPredioSeleccionado() != null && this.getPredioSeleccionado().
            getPPersonaPredios() != null) {
            for (PPersonaPredio ppp : this.getPredioSeleccionado().getPPersonaPredios()) {
                if (ppp.getCancelaInscribe() == null) {
                    if ((this.proyectarConservacionMB.getTramite().isRectificacion() ||
                         this.proyectarConservacionMB.getTramite().isCancelacionPredio() ||
                         this.proyectarConservacionMB.getTramite().isCuarta()) &&
                         !this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {
                        ppp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                        cambiarCancelaInscribeHijos(ppp, EProyeccionCancelaInscribe.MODIFICA.
                            getCodigo());
                    } else {
                        if (this.proyectarConservacionMB.getTramite().isQuintaOmitido() ||
                            this.proyectarConservacionMB.getTramite().isEsComplementacion() ||
                             this.proyectarConservacionMB.getTramite().isTercera() ||
                            this.proyectarConservacionMB.getTramite().isSegundaDesenglobe() ||
                             this.proyectarConservacionMB.getTramite().isRevisionAvaluo() ||
                             this.proyectarConservacionMB.getTramite().isQuintaMasivo() ||
                             this.proyectarConservacionMB.getTramite().isCancelacionMasiva()) {
                            // dejarlos en null
                        } else {
                            ppp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                            cambiarCancelaInscribeHijos(ppp, EProyeccionCancelaInscribe.CANCELA.
                                getCodigo());
                        }
                    }

                }
            }
        }

    }

    /**
     * Normalmente si se cambia el estado de PPersonaPredio, los objetos dependientes tambien deben
     * cambiar al mismo estado (I y C), excepto cuando el estado es M, la justificacion de propiedad
     * puede ser I o M.
     *
     * @param ppp
     * @param nuevoEstado
     * @param soloSiEsNulo
     */
    private void cambiarCancelaInscribeHijos(PPersonaPredio ppp, String nuevoEstado,
        boolean... soloSiEsNulo) {

        boolean ssen = soloSiEsNulo != null && soloSiEsNulo.length > 0 && soloSiEsNulo[0];

        if (ppp.getPPersona() != null) {
            if (!ssen || ppp.getPPersona().getCancelaInscribe() == null) {
                ppp.getPPersona().setCancelaInscribe(nuevoEstado);
            }
        }
        if (ppp.getPPersonaPredioPropiedads() != null) {
            for (PPersonaPredioPropiedad pPersPrePro : ppp.getPPersonaPredioPropiedads()) {
                if (pPersPrePro != null) {
                    if (!ssen || pPersPrePro.getCancelaInscribe() == null || nuevoEstado.equals(
                        EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                        pPersPrePro.setCancelaInscribe(nuevoEstado);
                        this.getConservacionService().guardarActualizarPPersonaPredioPropiedad(
                            pPersPrePro);
                    }
                }
            }
        }
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    public boolean isHayMasDeUnaCoincidencia() {
        return this.propietariosCoincidentes != null && this.propietariosCoincidentes.size() >= 1;
    }

    public void setHayMasDeUnaCoincidencia(boolean b) {

    }

    public void aceptarSeleccion() {
        this.propietarioSeleccionado.getPPersona().setTipoIdentificacion(personaSeleccionada.
            getTipoIdentificacion());
        this.propietarioSeleccionado.getPPersona().setNumeroIdentificacion(personaSeleccionada.
            getNumeroIdentificacion());
        this.propietarioSeleccionado.getPPersona().setPrimerNombre(personaSeleccionada.
            getPrimerNombre());
        this.propietarioSeleccionado.getPPersona().setSegundoNombre(personaSeleccionada.
            getSegundoNombre());
        this.propietarioSeleccionado.getPPersona().setPrimerApellido(personaSeleccionada.
            getPrimerApellido());
        this.propietarioSeleccionado.getPPersona().setSegundoApellido(personaSeleccionada.
            getSegundoApellido());
        this.propietarioSeleccionado.getPPersona().setSigla(personaSeleccionada.getSigla());
        this.propietarioSeleccionado.getPPersona().setRazonSocial(personaSeleccionada.
            getRazonSocial());
        if (EPersonaTipoIdentificacion.NIT.getCodigo().equals(this.propietarioSeleccionado.
            getPPersona().getTipoIdentificacion()) &&
             !this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion().equals("0")) {
            if (this.propietarioSeleccionado.getPPersona().getDigitoVerificacion() == null) {
                String dv = String.valueOf(this.getGeneralesService().calcularDigitoVerificacion(
                    this.propietarioSeleccionado.getPPersona().getNumeroIdentificacion()));
                this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(dv);
            }
        } else {
            this.propietarioSeleccionado.getPPersona().setDigitoVerificacion(personaSeleccionada.
                getDigitoVerificacion());
        }
        this.propietarioSeleccionado.getPPersona().setTipoPersona(personaSeleccionada.
            getTipoPersona());
    }

    public void cerrarEsc(org.primefaces.event.CloseEvent event) {
        if (!this.nuevo && this.edicion == 1) {
            this.propietarioSeleccionado.setPPersona(this.personaSeleccionadaEditar);
        } else if (!mensaje.isEmpty()) {
            this.addMensajeInfo(this.mensaje);
        }
    }

    public void editarPropietario() {
        PPersona ppp = new PPersona();
        ppp = (PPersona) this.propietarioSeleccionado.getPPersona().clone();
        this.personaSeleccionadaEditar = ppp;
        this.edicion = 1;
    }

    /**
     * Metodo que permite verificar los propietarios asociados a los predios de la Cancelacion
     * Masiva.
     *
     */
    public void verificarPropietariosCancelacion() {

        this.personasPredioBase = new ArrayList<PPersonaPredio>();
        this.idPredios = new ArrayList<Long>();
        String concatenaPersonaPredio = new String();
        Map<String, PPersonaPredio> mapPersonaPredio = new HashMap<String, PPersonaPredio>();
        this.mapPPrediosPorPersona = new HashMap<Long, List<PPredio>>();
        this.prediosPropietario = new ArrayList<PPredio>();

        this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb
            .getManagedBean("entradaProyeccion");

        for (PPredio ppredioTemp : this.entradaProyeccionMB.getPrediosResultantes()) {

            if (ppredioTemp.getId() != null) {
                idPredios.add(ppredioTemp.getId());
            }
        }

        if (this.idPredios != null && !this.idPredios.isEmpty()) {

            List<PPersonaPredio> consultapersonasPredio = this.getConservacionService()
                .buscarPPersonasPredioContandoPredios(this.idPredios);

            if (consultapersonasPredio != null && !consultapersonasPredio.isEmpty() &&
                this.personasPredioBase.isEmpty()) {

                for (PPersonaPredio personaPredio : consultapersonasPredio) {
                    //Se valida si se ha seleccionado Predio
                    if (this.getPredioSeleccionado() != null &&
                         this.getPredioSeleccionado().getPPersonaPredios() != null &&
                         personaPredio.getPPredio() != null &&
                         this.getPredioSeleccionado().getId().equals(personaPredio.getPPredio().
                            getId())) {

                        concatenaPersonaPredio =
                            personaPredio.getPPersona().getTipoIdentificacion() + "-" +
                            personaPredio.getPPersona().getNumeroIdentificacion() + "-" +
                            personaPredio.getPPersona().getNombre();

                        if (concatenaPersonaPredio != null && !concatenaPersonaPredio.isEmpty()) {

                            mapPersonaPredio.put(concatenaPersonaPredio, personaPredio);

                            //LLama metodo de cargue de pPredios por pPersona
                            this.mapPPrediosPorPersona = this.
                                listaDePPredioPorPersona(personaPredio);
                        }

                    } else {

                        if (Constantes.CERO.equals(personaPredio.getPPersona().
                            getNumeroIdentificacion())) {
                            concatenaPersonaPredio = personaPredio.getPPersona().
                                getTipoIdentificacion() + "-" +
                                personaPredio.getPPersona().getNumeroIdentificacion() + "-" +
                                personaPredio.getPPersona().getNombre();

                            if (concatenaPersonaPredio != null && !concatenaPersonaPredio.isEmpty()) {

                                mapPersonaPredio.put(concatenaPersonaPredio, personaPredio);

                                //LLama metodo de cargue de pPredios por pPersona
                                this.mapPPrediosPorPersona = this.listaDePPredioPorPersona(
                                    personaPredio);

                            }
                        }

                        if (personaPredio.getPPersona().getTipoIdentificacion() != null &&
                            personaPredio.getPPersona().getNumeroIdentificacion() != null &&
                            personaPredio.getPPersona().getNombre() != null &&
                             !Constantes.CERO.equals(personaPredio.getPPersona().
                                getNumeroIdentificacion())) {

                            concatenaPersonaPredio = personaPredio.getPPersona().
                                getTipoIdentificacion() + "-" +
                                personaPredio.getPPersona().getNumeroIdentificacion() + "-" +
                                personaPredio.getPPersona().getNombre();

                            if (concatenaPersonaPredio != null && !concatenaPersonaPredio.isEmpty()) {

                                mapPersonaPredio.put(concatenaPersonaPredio, personaPredio);
                                //LLama metodo de cargue de pPredios por pPersona
                                this.mapPPrediosPorPersona = this.listaDePPredioPorPersona(
                                    personaPredio);

                            }

                        }
                    }

                }

                if (mapPersonaPredio != null && !mapPersonaPredio.isEmpty()) {
                    for (Entry<String, PPersonaPredio> pp : mapPersonaPredio.entrySet()) {

                        this.personasPredioBase.add(pp.getValue());
                    }
                }

                if (this.personasPredioBase != null &&
                     !this.personasPredioBase.isEmpty()) {

                    for (PPersonaPredio pPersonaPredioFiltrado : this.personasPredioBase) {

                        pPersonaPredioFiltrado.setListPPredioPorPersona(this.mapPPrediosPorPersona.
                            get(pPersonaPredioFiltrado.getPPersona().getId()));
                    }
                }

            }
        }

    }

    /*
     * Metodo que permite obtener la lista de PPredios que tiene una PPersona
     *
     */
    public HashMap<Long, List<PPredio>> listaDePPredioPorPersona(PPersonaPredio personaPredio) {

        List<PPredio> listPPredios = this.mapPPrediosPorPersona.get(personaPredio.getPPersona().
            getId());

        if (listPPredios == null) {

            listPPredios = new ArrayList<PPredio>();
            listPPredios.add(personaPredio.getPPredio());
            this.mapPPrediosPorPersona.put(personaPredio.getPPersona().getId(), listPPredios);

        } else {

            listPPredios.add(personaPredio.getPPredio());
        }

        return this.mapPPrediosPorPersona;
    }

    /*
     *
     */
    public void consultarPrediosParaDetalles() {

        if (this.prediosPropietario != null &&
             !this.prediosPropietario.isEmpty()) {

        }
    }

    public PPersona getPersonaSeleccionadaEditar() {
        return personaSeleccionadaEditar;
    }

    public void setPersonaSeleccionadaEditar(PPersona personaSeleccionadaEditar) {
        this.personaSeleccionadaEditar = personaSeleccionadaEditar;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public boolean isBanderaVerPredios() {
        return banderaVerPredios;
    }

    public void setBanderaVerPredios(boolean banderaVerPredios) {
        this.banderaVerPredios = banderaVerPredios;
    }

}
