package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed Bean asociado al caso de uso CU-SA-AC-122 Solicitar Documentos Faltantes
 *
 * @author rodrigo.hernandez
 *
 */
@Component("solicitudDocFaltantes")
@Scope("session")
public class SolicitudDocumentosFaltantesMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -2623362046972251038L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitudDocumentosFaltantesMB.class);

    /**
     * Solicitud asociada
     */
    private Solicitud solicitud;

    /**
     * Propietario de la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Representante legal de la solicitud
     */
    private SolicitanteSolicitud representanteLegal;

    private SolicitudDocumento solicitudDocumento;

    /**
     * Documento donde se almacena el comunicado generado
     */
    private Documento documentoComunicado;

    private String nota;

    /**
     * Archivo generado para el comunicado
     */
    private File fileReporte;

    /**
     * Codigo del Tipo de territorial que se selecciona en pantalla
     */
    private String codigoTerritorial;

    /**
     * Numero de radicado generado para el comunicado
     */
    private String numeroRadicacionComunicado;

    /**
     * Codigo del municipio sede de la territorial
     */
    private Municipio municipioSedeTerritorial;

    private List<DetalleDocumentoFaltante> listaDetalleDocumentosFaltantes;

    private List<DetalleDocumentoFaltante> listaDetalleDocumentosBasicosFaltantes;

    private List<DetalleDocumentoFaltante> listaDetalleDocumentosAdicionalesFaltantes;

    private DetalleDocumentoFaltante detalleDocumentoFaltante;

    private TipoDocumento tipoDocumentoAdicionalSeleccionado;

    /**
     * usuario actual del sistema
     */
    private UsuarioDTO usuario;

    @Autowired
    private GeneralMB generalMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    private ReporteDTO solicitudReporteDTO;

    /**
     * Variable que indica si el boton de aceptar, en el panel Documentación Básica Faltante, esta
     * habilitado
     */
    private boolean banderaHabilitarAceptarDocBasicaFaltante;

    /**
     * Variable que indica si el boton de aceptar, en el panel Documentación Adicional Faltante,
     * esta habilitado
     */
    private boolean banderaHabilitarAceptarDocAdicionalFaltante;

    /**
     * Variable que indica si el solicitante asociado a la solicitud es persona juridica o no
     */
    private boolean banderaSolicitanteEsPersonaJuridica;

    /**
     * Variable que indica si ya fue generado el comunicado de documentos faltantes
     */
    private boolean banderaComunicadoGenerado;

    /**
     * Variable que indica si la radicacion del comunicado ya fue realizada o no
     * </br>
     *
     * <b>true</b> La radicacion ya fue realizada</br> <b>false</b> La radicacion no ha sido
     * realizada
     *
     */
    private boolean banderaRadicacionRealizada;

    /**
     * Listado de documentos adicionales
     */
    private List<SelectItem> listaTiposDocumento;

    /**
     * Listado de documentos faltantes
     */
    private List<SolicitudDocumentacion> listaDocumentosFaltantes;

    private List<TipoDocumento> listaTiposDocumentosAdicionales;

    private SolicitudDocumentacion documentoFaltanteSeleccionado;

    /**
     * la territorial donde se está realizando la solicitud de documentos.
     */
    private EstructuraOrganizacional territorialSolicitud;

    @Autowired
    protected IContextListener contextoWeb;

    /* ----- Setters y Getters ------------------------------ */
    public List<SelectItem> getListaTiposDocumento() {
        return listaTiposDocumento;
    }

    public void setListaTiposDocumento(List<SelectItem> listaTiposDocumento) {
        this.listaTiposDocumento = listaTiposDocumento;
    }

    public boolean isBanderaHabilitarAceptarDocBasicaFaltante() {
        return banderaHabilitarAceptarDocBasicaFaltante;
    }

    public void setBanderaHabilitarAceptarDocBasicaFaltante(
        boolean banderaHabilitarAceptarDocBasicaFaltante) {
        this.banderaHabilitarAceptarDocBasicaFaltante = banderaHabilitarAceptarDocBasicaFaltante;
    }

    public boolean isBanderaHabilitarAceptarDocAdicionalFaltante() {
        return banderaHabilitarAceptarDocAdicionalFaltante;
    }

    public void setBanderaHabilitarAceptarDocAdicionalFaltante(
        boolean banderaHabilitarAceptarDocAdicionalFaltante) {
        this.banderaHabilitarAceptarDocAdicionalFaltante =
            banderaHabilitarAceptarDocAdicionalFaltante;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public SolicitanteSolicitud getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public SolicitanteSolicitud getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(SolicitanteSolicitud representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public boolean isBanderaSolicitanteEsPersonaJuridica() {
        return banderaSolicitanteEsPersonaJuridica;
    }

    public void setBanderaSolicitanteEsPersonaJuridica(
        boolean banderaSolicitanteEsPersonaJuridica) {
        this.banderaSolicitanteEsPersonaJuridica = banderaSolicitanteEsPersonaJuridica;
    }

    public String getCodigoTerritorial() {
        return codigoTerritorial;
    }

    public void setCodigoTerritorial(String codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public Municipio getMunicipioSedeTerritorial() {
        return municipioSedeTerritorial;
    }

    public void setMunicipioSedeTerritorial(Municipio municipioSedeTerritorial) {
        this.municipioSedeTerritorial = municipioSedeTerritorial;
    }

    public List<SolicitudDocumentacion> getListaDocumentosFaltantes() {
        return listaDocumentosFaltantes;
    }

    public void setListaDocumentosFaltantes(
        List<SolicitudDocumentacion> listaDocumentosFaltantes) {
        this.listaDocumentosFaltantes = listaDocumentosFaltantes;
    }

    public boolean isBanderaComunicadoGenerado() {
        return banderaComunicadoGenerado;
    }

    public void setBanderaComunicadoGenerado(boolean banderaComunicadoGenerado) {
        this.banderaComunicadoGenerado = banderaComunicadoGenerado;
    }

    public SolicitudDocumentacion getDocumentoFaltanteSeleccionado() {
        return documentoFaltanteSeleccionado;
    }

    public void setDocumentoFaltanteSeleccionado(
        SolicitudDocumentacion documentoFaltanteSeleccionado) {
        this.documentoFaltanteSeleccionado = documentoFaltanteSeleccionado;
    }

    public List<DetalleDocumentoFaltante> getListaDetalleDocumentosFaltantes() {
        return listaDetalleDocumentosFaltantes;
    }

    public void setListaDetalleDocumentosFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosFaltantes) {
        this.listaDetalleDocumentosFaltantes = listaDetalleDocumentosFaltantes;
    }

    public List<DetalleDocumentoFaltante> getListaDetalleDocumentosBasicosFaltantes() {
        return listaDetalleDocumentosBasicosFaltantes;
    }

    public void setListaDetalleDocumentosBasicosFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosBasicosFaltantes) {
        this.listaDetalleDocumentosBasicosFaltantes = listaDetalleDocumentosBasicosFaltantes;
    }

    public List<DetalleDocumentoFaltante> getListaDetalleDocumentosAdicionalesFaltantes() {
        return listaDetalleDocumentosAdicionalesFaltantes;
    }

    public void setListaDetalleDocumentosAdicionalesFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosAdicionalesFaltantes) {
        this.listaDetalleDocumentosAdicionalesFaltantes = listaDetalleDocumentosAdicionalesFaltantes;
    }

    public DetalleDocumentoFaltante getDetalleDocumentoFaltante() {
        return detalleDocumentoFaltante;
    }

    public void setDetalleDocumentoFaltante(
        DetalleDocumentoFaltante detalleDocumentoFaltante) {
        this.detalleDocumentoFaltante = detalleDocumentoFaltante;
    }

    public TipoDocumento getTipoDocumentoAdicionalSeleccionado() {
        return tipoDocumentoAdicionalSeleccionado;
    }

    public void setTipoDocumentoAdicionalSeleccionado(
        TipoDocumento tipoDocumentoAdicionalSeleccionado) {
        this.tipoDocumentoAdicionalSeleccionado = tipoDocumentoAdicionalSeleccionado;
    }

    public List<TipoDocumento> getListaTiposDocumentosAdicionales() {
        return listaTiposDocumentosAdicionales;
    }

    public void setListaTiposDocumentosAdicionales(
        List<TipoDocumento> listaTiposDocumentosAdicionales) {
        this.listaTiposDocumentosAdicionales = listaTiposDocumentosAdicionales;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public SolicitudDocumento getSolicitudDocumento() {
        return solicitudDocumento;
    }

    public void setSolicitudDocumento(SolicitudDocumento solicitudDocumento) {
        this.solicitudDocumento = solicitudDocumento;
    }

    public Documento getDocumentoComunicado() {
        return documentoComunicado;
    }

    public void setDocumentoComunicado(Documento documentoComunicado) {
        this.documentoComunicado = documentoComunicado;
    }

    public boolean isBanderaRadicacionRealizada() {
        return banderaRadicacionRealizada;
    }

    public void setBanderaRadicacionRealizada(boolean banderaRadicacionRealizada) {
        this.banderaRadicacionRealizada = banderaRadicacionRealizada;
    }

    public EstructuraOrganizacional getTerritorialSolicitud() {
        return territorialSolicitud;
    }

    public void setTerritorialSolicitud(
        EstructuraOrganizacional territorialSolicitud) {
        this.territorialSolicitud = territorialSolicitud;
    }

    public String getNumeroRadicacionComunicado() {
        return numeroRadicacionComunicado;
    }

    public void setNumeroRadicacionComunicado(String numeroRadicacionComunicado) {
        this.numeroRadicacionComunicado = numeroRadicacionComunicado;
    }

    public ReporteDTO getSolicitudReporteDTO() {
        return solicitudReporteDTO;
    }

    public void setSolicitudReporteDTO(ReporteDTO solicitudReporteDTO) {
        this.solicitudReporteDTO = solicitudReporteDTO;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio SolicitudDocumentosFaltantesMB#init");

        this.iniciarDatos();
        this.cargarDatosDesdeProcess();

        LOGGER.debug("Fin SolicitudDocumentosFaltantesMB#init");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.numeroRadicacionComunicado = "";

        this.iniciarCamposTexto();
        this.iniciarListasComboBox();
        this.iniciarBanderas();

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#iniciarDatos");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar los campos del formulario
     *
     * @author rodrigo.hernandez
     */
    private void iniciarCamposTexto() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#iniciarValores");

        this.nota = "";

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#iniciarValores");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar los comboBox (selectOneMenu) de la página
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListasComboBox() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#iniciarListasComboBox");

        this.listaTiposDocumentosAdicionales = this.getGeneralesService()
            .getAllTipoDocumento();

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#iniciarListasComboBox");
    }

    // ------------------------------------------------------------------------------------
    private void iniciarBanderas() {
        this.banderaComunicadoGenerado = false;
        this.banderaHabilitarAceptarDocAdicionalFaltante = false;
        this.banderaHabilitarAceptarDocBasicaFaltante = false;
        this.banderaRadicacionRealizada = false;
        this.banderaSolicitanteEsPersonaJuridica = false;
    }

    // ------------------------------------------------------------------------------------
    // TODO::rodrigo.hernandez::Eliminar método cuando se integre con process
    /**
     * Método para simular carga de datos desde motor de procesos
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosDesdeProcess() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#cargarDatosDesdeProcess");

        Long solicitudId = 43500L;

        this.solicitud = this.getTramiteService()
            .buscarSolicitudFetchTramitesBySolicitudId(solicitudId);

        /*
         * Se busca el SolicitudDocumento asociado a la solicitud
         */
        this.solicitudDocumento = this
            .getAvaluosService()
            .obtenerSolicitudDocumentoPorSolicitudId(this.solicitud.getId());

        this.cargarSolicitante();
        this.cargarDatosTerritorial();
        this.buscarDocumentosFaltantesAsociados();

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#cargarDatosDesdeProcess");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Inicia el flujo desde el caso de uso CU-SA-AC-002 - Administrar documentacion trámite avalúo
     *
     * @author rodrigo.hernandez
     */
    public void cargarDatosDesdeCU002(Solicitud solicitud) {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#cargarDatosDesdeCU002");

        this.solicitud = solicitud;
        this.cargarSolicitante();
        this.cargarDatosTerritorial();
        this.buscarDocumentosFaltantesAsociados();

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#cargarDatosDesdeCU002");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Inicia datos del Solicitante
     *
     * @author rodrigo.hernandez
     */
    private void cargarSolicitante() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#cargarSolicitante");

        this.solicitante = this
            .getTramiteService()
            .buscarSolicitantesSolicitudBySolicitudIdRelacion(
                this.solicitud.getId(),
                ESolicitanteSolicitudRelac.PROPIETARIO.toString())
            .get(0);

        // Se valida el tipo de persona para cargar o no el representante legal
        this.banderaSolicitanteEsPersonaJuridica = this
            .esSolicitanteSeleccionadoPersonaJuridica();
        if (this.banderaSolicitanteEsPersonaJuridica) {
            this.cargarRepresentanteLegal();
        }

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#cargarSolicitante");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Inicia datos del Representante Legal
     *
     * @author rodrigo.hernandez
     */
    private void cargarRepresentanteLegal() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#cargarRepresentanteLegal");

        this.representanteLegal = this
            .getTramiteService()
            .buscarSolicitantesSolicitudBySolicitudIdRelacion(
                this.solicitud.getId(),
                ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                    .getValor().toUpperCase()).get(0);

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#cargarRepresentanteLegal");
    }

    /**
     * Método para cargar los datos asociados a la territorial (Territorial Asignada y Municipio
     * Sede Territorial)
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosTerritorial() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#cargarDatosTerritorial");

        this.codigoTerritorial = usuario.getCodigoTerritorial();

        this.municipioSedeTerritorial = this
            .getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                this.usuario.getCodigoEstructuraOrganizacional())
            .getMunicipio();

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#cargarDatosTerritorial");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar los documentos que fueron registrados como faltantes
     *
     * @author rodrigo.hernandez
     */
    public void buscarDocumentosFaltantesAsociados() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#buscarDocumentosFaltantesAsociados");

        this.listaDetalleDocumentosFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaDetalleDocumentosBasicosFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaDetalleDocumentosAdicionalesFaltantes = new ArrayList<DetalleDocumentoFaltante>();

        /* Se traen los DetalleDocumentoFaltantes asociados a la solicitud */
        this.listaDetalleDocumentosFaltantes = this.solicitudDocumento
            .getDetalleDocumentoFaltantes();

        this.crearListaDetallesDocumentosBasicosFaltantes();
        this.crearListaDetallesDocumentosAdicionalesFaltantes();

        LOGGER.debug("Fin SolicitudDocumentacionMB#buscarDocumentosFaltantesAsociados");
    }

    /**
     * Método para crear la lista de detalles de documentos básicos faltantes
     *
     * @author rodrigo.hernandez
     */
    private void crearListaDetallesDocumentosBasicosFaltantes() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#crearListaDetallesDocumentosBasicosFaltantes");

        /* Se buscan los tipos de documentos que son básicos */
        List<TipoDocumento> listaTipoDocsBasicos = this.getGeneralesService()
            .getCacheTiposDocumentoPorTipoTramite(this.solicitud.getTipo(),
                null, null);

        /*
         * Se crea el modelo de la tabla de detalles de documentos basicos faltantes.
         *
         * Se asume que no siempre se van a recibir todos los tipos de documentos, por ende, se
         * carga la tabla con la lista por default de los documentos basicos
         */
        for (TipoDocumento tipoDocumento : listaTipoDocsBasicos) {
            DetalleDocumentoFaltante ddf = new DetalleDocumentoFaltante();
            ddf.setTipoDocumento(tipoDocumento);
            ddf.setCantidadEntrega(0L);
            ddf.setCantidadSolicitud(0L);
            ddf.setUsuarioLog(this.usuario.getLogin());
            ddf.setFechaLog(Calendar.getInstance().getTime());
            this.listaDetalleDocumentosBasicosFaltantes.add(ddf);
        }

        /*
         * Se actualiza el modelo de la tabla de detalles de documentos basicos faltantes con los
         * DetalleDocumentoFaltante's traídos de la BD
         */
        for (int cont = 0; cont < this.listaDetalleDocumentosBasicosFaltantes
            .size(); cont++) {
            for (int contAux = 0; contAux < this.listaDetalleDocumentosFaltantes
                .size(); contAux++) {
                if (this.listaDetalleDocumentosFaltantes
                    .get(contAux)
                    .getTipoDocumento()
                    .getId()
                    .equals(this.listaDetalleDocumentosBasicosFaltantes
                        .get(cont).getTipoDocumento().getId())) {
                    this.listaDetalleDocumentosBasicosFaltantes.set(cont,
                        this.listaDetalleDocumentosFaltantes.get(contAux));
                    /*
                     * Cuando encuentra al DetalleDocumentoFaltante que tiene el mismo tipo de
                     * solicitud, se sale del for, para seguir con el siguiente objeto a comparar
                     * (Se asume que no hay tipos de documento repetidos)
                     */
                    break;
                }
            }
        }

        LOGGER.debug("Fin SolicitudDocumentacionMB#crearListaDetallesDocumentosBasicosFaltantes");
    }

    /**
     * Método para crear la lista de detalles de documentos adicionales faltantes
     *
     * @author rodrigo.hernandez
     */
    private void crearListaDetallesDocumentosAdicionalesFaltantes() {
        LOGGER.debug(
            "Inicio SolicitudDocumentacionMB#crearListaDetallesDocumentosAdicionalesFaltantes");

        /* Se buscan los tipos de documentos que son adicionales */
        List<TipoDocumento> listaTipoDocsAdicionales = this
            .getGeneralesService().getAllTipoDocumento();

        /*
         * Se crea el modelo de la tabla de detalles de documentos basicos adicionales
         */
        for (TipoDocumento tipoDocumento : listaTipoDocsAdicionales) {
            for (DetalleDocumentoFaltante detalleDocFaltante : this.listaDetalleDocumentosFaltantes) {
                /*
                 * Se valida si el DetalleDocumentoFaltante que se trae desde la BD, tiene asociado
                 * un tipo de documento adicional
                 */
                if (detalleDocFaltante.getTipoDocumento().getId()
                    .equals(tipoDocumento.getId()) &&
                     !this.listaDetalleDocumentosBasicosFaltantes
                        .contains(detalleDocFaltante)) {
                    this.listaDetalleDocumentosAdicionalesFaltantes
                        .add(detalleDocFaltante);
                }
            }
        }

        LOGGER.
            debug("Fin SolicitudDocumentacionMB#crearListaDetallesDocumentosAdicionalesFaltantes");
    }

    /**
     * Método para guardar las modificaciones hechas a los documentos basicos faltantes
     *
     * @author rodrigo.hernandez
     *
     */
    private void guardarDocumentosBasicosFaltantes() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#guardarDocumentosBasicosFaltantes");

        this.getAvaluosService().guardarActualizarDetalleDocumentoFaltantes(
            this.listaDetalleDocumentosBasicosFaltantes);

        LOGGER.debug("Fin SolicitudDocumentacionMB#guardarDocumentosBasicosFaltantes");
    }

    /**
     * Método para guardar las modificaciones hechas a los documentos adicionales faltantes
     *
     * @author rodrigo.hernandez
     *
     */
    private void guardarDocumentosAdicionalesFaltantes() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#guardarDocumentosAdicionalesFaltantes");

        this.getAvaluosService().guardarActualizarDetalleDocumentoFaltantes(
            this.listaDetalleDocumentosAdicionalesFaltantes);

        LOGGER.debug("Fin SolicitudDocumentacionMB#guardarDocumentosAdicionalesFaltantes");
    }

    /**
     * Método para agregar un nuevo detalleDocumentoFaltante asociado a un tipo de documento
     * adicional
     *
     * @author rodrigo.hernandez
     *
     */
    public void agregarDocumentoAdicional() {

        LOGGER.debug("Inicio SolicitudDocumentacionMB#agregarDocumentoAdicional");

        String mensaje = "";
        boolean banderaExisteTipoDocumento = false;

        DetalleDocumentoFaltante detalleFaltanteAdicional = new DetalleDocumentoFaltante();

        detalleFaltanteAdicional
            .setTipoDocumento(this.tipoDocumentoAdicionalSeleccionado);
        detalleFaltanteAdicional.setCantidadEntrega(0L);
        detalleFaltanteAdicional.setCantidadSolicitud(0L);
        detalleFaltanteAdicional.setUsuarioLog(this.usuario.getLogin());
        detalleFaltanteAdicional.setFechaLog(Calendar.getInstance().getTime());

        /*
         * Se valida que la lista de documentos faltantes adicionales no este vacía
         */
        if (!this.listaDetalleDocumentosAdicionalesFaltantes.isEmpty()) {
            /*
             * Se recorre la lista de detalle documentos faltantes adicionales para validar si ya
             * existe un detalle con el tipo de documento que se quiere adicionar
             */
            for (DetalleDocumentoFaltante ddf : this.listaDetalleDocumentosAdicionalesFaltantes) {
                if (ddf.getTipoDocumento()
                    .getId()
                    .equals(detalleFaltanteAdicional.getTipoDocumento()
                        .getId())) {
                    banderaExisteTipoDocumento = true;
                    break;
                }
            }
        }

        /*
         * Si no existe el tipo de documento asociado a un detalle, se adiciona a la tabla de
         * detalleDocumentoFaltante adicionales
         */
        if (!banderaExisteTipoDocumento) {
            this.listaDetalleDocumentosAdicionalesFaltantes
                .add(detalleFaltanteAdicional);
        } else {
            mensaje = "El documento que quiere adicionar ya existe";
            this.addMensajeError(mensaje);
        }

        LOGGER.debug("Fin SolicitudDocumentacionMB#agregarDocumentoAdicional");
    }

    /**
     * Método para eliminar documento adicional de la lista
     *
     * @author rodrigo.hernandez
     */
    public void eliminarDocumentoAdicional() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#eliminarDocumentoAdicional");

        /* Si el detalle tiene id es porque esta en la BD */
        if (this.detalleDocumentoFaltante.getId() != null) {
            this.getAvaluosService().borrarDetalleDocumentoFaltante(
                this.detalleDocumentoFaltante);
        }

        /* Se elimina de la lista de la tabla */
        this.listaDetalleDocumentosAdicionalesFaltantes
            .remove(this.detalleDocumentoFaltante);

        LOGGER.debug("Fin SolicitudDocumentacionMB#eliminarDocumentoAdicional");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para crear documento(reporte) de solicitud de documentos faltantes
     * </br> </br> - Llamado desde la pagina
     * <b>solicitarDocumentosFaltantes.xhtml</b></br> - action para botón
     * <b>Generar documento</b>
     *
     * Este método se debe llamar cuando:</br></br> Se genere el comunicado (Sin numero de
     * radicado)</br> Se radique el comunicado (Con numero de radicado)
     *
     *
     * @author rodrigo.hernandez
     */
    public void generarSolicitudDocumentosFaltantes() {
        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#generarSolicitudDocumentosFaltantes");

        // TODO::rodrigo.hernandez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        /*
         * Se crea mapa de parametros para enviarlo al servicio que genera el reporte
         */
        Map<String, String> parameters = new HashMap<String, String>();

        // TODO :: rodrigo.hernandez :: Parametros quemados, reemplazar cuando
        // se haya desarrollado el reporte :: 30/10/12
        parameters.put("NUMERO_RADICADO", this.numeroRadicacionComunicado);

        this.solicitudReporteDTO = this.reportsService.generarReporte(
            parameters, enumeracionReporte, this.usuario);

        this.banderaComunicadoGenerado = true;

        this.crearDocumentoYSolicitudDocumento();

        LOGGER.debug(
            "finalizando SolicitudDocumentosFaltantesMB#generarSolicitudDocumentosFaltantes");
    }

    // ---------------------------------------------------------------------------------------
    /**
     * Método para crear el objeto SolicitudDocumento
     *
     * @author rodrigo.hernandez
     */
    private void crearDocumentoYSolicitudDocumento() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#crearDocumentoYSolicitudDocumento");

        // this.urlArchivo = this.solicitudReporteDTO

        /*
         * Se define el nombre del archivo de la carpeta donde consultar el archivo del comunicado
         * creado
         */
        String nombreArchivoCarpetaTemporal = this.getGeneralesService()
            .publicarArchivoEnWeb(
                this.solicitudReporteDTO.getArchivoReporte().getPath());

        /*
         * Se crea un nuevo objeto Documento a partir del archivo creado para el comunicado
         */
        Documento documento = this.crearDocumento(nombreArchivoCarpetaTemporal,
            this.numeroRadicacionComunicado);

        this.solicitudDocumento = new SolicitudDocumento();
        this.solicitudDocumento.setSoporteDocumento(documento);
        this.solicitudDocumento.setIdRepositorioDocumentos(documento
            .getIdRepositorioDocumentos());
        this.solicitudDocumento.setFechaLog(Calendar.getInstance().getTime());
        this.solicitudDocumento.setUsuarioLog(this.usuario.getLogin());
        this.solicitudDocumento.setFecha(new Date(System.currentTimeMillis()));
        this.solicitudDocumento.setSolicitud(this.solicitud);

        this.solicitudDocumento = this.getTramiteService()
            .guardarActualizarSolicitudDocumento(this.solicitudDocumento);

        /*
         * Si se está realizando la radicacion del comunicado se salta pasos para asociar la lista
         * de DetalleDocumentoFaltantes con la SolicitudDocumento y guardar los
         * DetalleDocumentoFaltantes
         */
        if (!this.banderaRadicacionRealizada) {
            /* Se asocia la SolicitudDocumento con los DetalleDocumentoFaltantes */
            this.asociarSolicitudDocumentoADetalleDocumentosFaltantes();

            /* Se guardan en BD las listas de DetalleDocumentoFaltante */
            this.guardarDocumentosAdicionalesFaltantes();
            this.guardarDocumentosBasicosFaltantes();
        }

        LOGGER.debug("Fin SolicitudDocumentacionMB#crearDocumentoYSolicitudDocumento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear el objeto Documento asociado al SolicitudDocumento
     *
     * @author rodrigo.hernandez
     */
    private Documento crearDocumento(String urlArchivo,
        String numeroRadicacionComunicado) {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#crearDocumento");

        Documento documento = new Documento();

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento.setId(
            ETipoDocumento.OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES
                .getId());

        tipoDocumento.setNombre(
            ETipoDocumento.OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES
                .getNombre());

        documento.setTipoDocumento(tipoDocumento);
        documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        documento.setNumeroRadicacion(numeroRadicacionComunicado);
        documento.setBloqueado(ESiNo.NO.getCodigo());
        documento.setArchivo(urlArchivo);
        documento.setUsuarioCreador(this.usuario.getLogin());
        documento.setUsuarioLog(this.usuario.getLogin());
        documento.setFechaLog(new Date(System.currentTimeMillis()));
        documento.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        documento.setDescripcion("Solicitud de documentos faltantes");

        documento = this.guardarDocumentoEnGestorDocumental(documento,
            tipoDocumento);

        if (documento != null) {
            documento = this.getGeneralesService().guardarDocumento(documento);
        }

        this.solicitudDocumento.setSoporteDocumento(documento);

        /*
         * Se actualiza el objeto SolicitudDocumento
         */
        this.solicitudDocumento = this.getTramiteService()
            .guardarActualizarSolicitudDocumento(solicitudDocumento);

        LOGGER.debug("Fin SolicitudDocumentacionMB#crearDocumento");

        return documento;
    }

    // -------------------------------------------------------------------------
    /**
     * Método para guardar un documento de avalúos en el gestor de documentos
     *
     * @author rodrigo.hernandez
     * @param documento {@link Documento} a guardar
     * @param tipoDocumento {@link TipoDocumento} del documento a guardar
     * @return {@link Documento} guardado con el id del gestor de documentos
     */
    private Documento guardarDocumentoEnGestorDocumental(Documento documento,
        TipoDocumento tipoDocumento) {

        LOGGER.debug("Inicio SolicitudDocumentacionMB#guardarDocumentoEnGestorDocumental");

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(this.solicitud
                .getNumero(), this.solicitud.getFecha(), tipoDocumento
                .getNombre(), this.solicitudReporteDTO
                    .getRutaCargarReporteAAlfresco());

        documento = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, documento);

        if (documento != null && documento.getIdRepositorioDocumentos() == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

        LOGGER.debug("Fin SolicitudDocumentacionMB#guardarDocumentoEnGestorDocumental");

        return documento;

    }

    /**
     * Método para asociar la SolicitudDocumento con cada DetalleDocumentoFaltante creado o
     * actualizado
     *
     * @author rodrigo.hernandez
     */
    private void asociarSolicitudDocumentoADetalleDocumentosFaltantes() {
        LOGGER.debug(
            "Inicio SolicitudDocumentacionMB#asociarSolicitudDocumentoADetalleDocumentosFaltantes");

        /*
         * Se asocia la SolicitudDocumento con cada DetalleDocumentoFaltante Basico
         */
        for (DetalleDocumentoFaltante ddf : this.listaDetalleDocumentosBasicosFaltantes) {
            ddf.setSolicitudDocumento(this.solicitudDocumento);
        }

        /*
         * Se asocia la SolicitudDocumento con cada DetalleDocumentoFaltante Adicional
         */
        for (DetalleDocumentoFaltante ddf : this.listaDetalleDocumentosAdicionalesFaltantes) {
            ddf.setSolicitudDocumento(this.solicitudDocumento);
        }

        LOGGER.debug(
            "Fin SolicitudDocumentacionMB#asociarSolicitudDocumentoADetalleDocumentosFaltantes");
    }

    // ---------------------------------------------------------------------------------------
    /**
     * Método para validar si el solicitante es Persona Natural
     *
     * @author rodrigo.hernandez
     *
     * @return <b>false: </b>Es persona natural </br> <b>true: </b>Es persona juridica </br>
     */
    private boolean esSolicitanteSeleccionadoPersonaJuridica() {
        boolean result = false;

        if (this.solicitante.getTipoPersona().equals(
            EPersonaTipoPersona.JURIDICA.getCodigo())) {
            result = true;
        }

        return result;
    }

    // -----------------------------------------------------------------------------------
    /**
     * Método para ejecutar radicado de comunicado generado
     *
     * @author rodrigo.hernandez
     */
    public void radicarComunicado() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#radicarComunicado");

        this.banderaRadicacionRealizada = true;

        this.numeroRadicacionComunicado = this.obtenerNumeroRadicado();

        /* Se actualiza el comunicado con el numero de radicacion creado */
        this.generarSolicitudDocumentosFaltantes();

        /* Una vez generado el comunicado, se envia al solicitante */
        this.enviarComunicado();

        /* Se mueve el proceso */
        this.moverProceso();

        LOGGER.debug("Fin SolicitudDocumentacionMB#radicarComunicado");
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private String obtenerNumeroRadicado() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add("800"); // Territorial=compania
        parametros.add("SC_SNC"); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add("1"); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add(""); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add("800"); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add("5000"); // Destino lugar
        parametros.add(""); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(""); // Direccion destino
        parametros.add(""); // Tipo identificacion destino
        parametros.add(""); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para enviar comunicado al solicitante
     *
     * @author rodrigo.hernandez
     */
    private void enviarComunicado() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#enviarComunicado");

        List<String> destinatarios = new ArrayList<String>();

        // String correoSolicitante = this.solicitante.getCorreoElectronico();
        String correoSolicitante = "rodrigo.hernandez@igac.gov.co";
        UsuarioDTO remitente = null;

        /* Parámetros de cuerpo del correo */
        String infoSolicitantePersonaJuridica = "";
        String nombreParam = "";
        String numeroRadicado = "";
        String fechaRadicado = "";
        String remitenteParam = "";
        Object[] datosCorreo = new Object[5];

        /* Adjuntos */
        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};

        this.territorialSolicitud = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                String.valueOf(this.solicitud.getTerritorialCodigo()));

        /*
         * Se valida que el solicitante tenga una direccion de correo a donde enviar la solicitud de
         * documentos
         */
        if (correoSolicitante != null && !correoSolicitante.isEmpty()) {

            /*
             * Se procede a crear el dato del remitente
             *
             * Si el codigo de la territorial de un usuario es null, entonces el usuario pertenece a
             * sede central
             */
            if (this.usuario.getCodigoTerritorial() == null) {
                remitente = this
                    .getGeneralesService()
                    .obtenerFuncionarioTerritorialYRol(
                        Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                        ERol.COORDINADOR_GIT_AVALUOS).get(0);

                remitenteParam = remitente.getNombreCompleto() + "<br/>" +
                     "	COORDINADOR GIT DE AVALÚOS <br/>" +
                     " SUBDIRECCIÓN DE CATASTRO";
            } else {
                remitente = this
                    .getGeneralesService()
                    .obtenerFuncionarioTerritorialYRol(
                        this.territorialSolicitud.getNombre(),
                        ERol.DIRECTOR_TERRITORIAL).get(0);

                remitenteParam = remitente.getNombreCompleto() + "<br/>" +
                     " DIRECTOR TERRITORIAL <br/>" + " TERRITORIAL " +
                     this.territorialSolicitud.getNombre();
            }

            /*
             * Se procede a crear el bloque con los datos del solicitante
             *
             * Se valida si el solicitante es persona Juridica
             */
            if (this.solicitante.isJuridica()) {
                infoSolicitantePersonaJuridica = "Doctor(a) <br/>" +
                     this.solicitante.getNombreCompleto() +
                     (this.solicitante.getCargo() == null ? "" : "<br/> " +
                     this.solicitante.getCargo()) +
                     "<br/> " +
                     this.solicitante.getDireccion() +
                     "<br/> " +
                     this.solicitante.getDireccionMunicipio().getNombre() +
                     " - " +
                     this.solicitante.getDireccionDepartamento()
                        .getNombre() + "<br/><br/><br/>";
            }

            nombreParam = this.solicitante.getPrimerNombre();

            numeroRadicado = this.solicitud.getNumero();

            SimpleDateFormat sdf = new SimpleDateFormat(
                Constantes.FORMATO_FECHA);
            fechaRadicado = sdf.format(this.solicitud.getFecha());

            /* Generación de destinatarios */
            destinatarios.add(correoSolicitante);

            /* Generación de los datos del cuerpo del correo */
            datosCorreo[0] = infoSolicitantePersonaJuridica == null ? "" :
                 infoSolicitantePersonaJuridica;
            datosCorreo[1] = nombreParam == null ? "" : nombreParam;
            datosCorreo[2] = numeroRadicado == null ? "" : numeroRadicado;
            datosCorreo[3] = fechaRadicado == null ? "" : fechaRadicado;
            datosCorreo[4] = remitenteParam == null ? "" : remitenteParam;

            String nombreArchivoCarpetaTemporal = this.solicitudReporteDTO
                .getArchivoReporte().getAbsolutePath();

            adjuntos[0] = nombreArchivoCarpetaTemporal;
            adjuntosNombres[0] = "Solicitud de documentos faltantes.pdf";

            // Envio correo
            boolean notificacionExitosa = false;
            this.getGeneralesService().enviarCorreoElectronicoConCuerpo(
                EPlantilla.MENSAJE_SOLICITUD_DOCUMENTOS_FALTANTES,
                destinatarios, null, null, datosCorreo, adjuntos,
                adjuntosNombres);

            if (notificacionExitosa) {
                this.addMensajeInfo(
                    "Se envio exitosamente el comunicado de solicitud de documentos faltantes.");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError(
                    "No se pudo enviar el oficio de no aprobación por correo electrónico.");
                context.addCallbackParam("error", "error");
            }

        }

        LOGGER.debug("Fin SolicitudDocumentacionMB#enviarComunicado");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar </br> </br> -
     * Llamado desde la pagina
     * <b>solicitarDocumentosFaltantes.xhtml</b></br> - action para botón
     * <b>Cerrar</b>
     */
    public String cerrar() {

        LOGGER.debug("iniciando SolicitudDocumentosFaltantesMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("finalizando SolicitudDocumentosFaltantesMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método para avanzar proceso
     *
     * @author rodrigo.hernandez
     */
    private void moverProceso() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#avanzarProceso");

        // TODO::rodrigo.hernandez::Implementar funcionalidad para avanzar
        // proceso
        LOGGER.debug("Fin SolicitudDocumentacionMB#avanzarProceso");
    }

}
