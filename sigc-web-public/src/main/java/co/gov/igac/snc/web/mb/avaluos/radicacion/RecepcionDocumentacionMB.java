package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.DetalleDocumentoFaltante;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed Bean asociado al caso de uso CU-SA-AC-152 Recibir documentación
 *
 * @author rodrigo.hernandez
 *
 */
@Component("recibirDocumentacion")
@Scope("session")
public class RecepcionDocumentacionMB extends SNCManagedBean {

    private static final long serialVersionUID = -4679704357776134789L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RecepcionDocumentacionMB.class);

    private FiltroDatosConsultaAvaluo filtroDatosConsultaAvaluo;

    private FiltroDatosSolicitud filtroDatosSolicitud;

    /* Campos de formulario */
    private String numeroRadicadoSolicitud;

    private String tipoTramite;

    private String secRadicado;

    private String tipoEmpresa;

    private String razonSocial;

    private String sigla;

    private String primerNombre;

    private String segundoNombre;

    private String primerApellido;

    private String segundoApellido;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /* Banderas */
    /**
     * Bandera que indica si se encontró algún trámite
     */
    private boolean banderaTramiteEncontrado;

    /**
     * Bandera que indica si el tipo de empresa seleccionado es Natural o Juridica </br> <b>true</b>
     * El tipo de empresa es Natural </br>
     * <b>false</b> El tipo de empresa es Juridica </br>
     *
     */
    private boolean banderaTipoEmpresaEsNatural;

    /**
     * Bandera que indica si el tipo de empresa ya fue seleccionado </br>
     * <b>true</b> El tipo de empresa ya fue seleccionado </br> <b>false</b> El tipo de empresa no
     * ha sido seleccionado </br>
     *
     */
    private boolean banderaTipoEmpresaSeleccionado;

    /**
     * Bandera que indica si la seleccion hecha solicitudes o tramites hechos es correcta. Es decir
     * que tengan el mismo numero de radicado <b>true</b> La seleccion es valida</br> <b>false</b>
     * La seleccion no es valida </br>
     */
    private boolean banderaSeleccionTramitesSolicitudesValida;

    /**
     * Bandera que indica si se está anexando un documento adicional. </br>
     * <b>true</b> Se está anexando un documento adicional</br> <b>false NO</b>
     * se está anexando un documento adicional </br>
     */
    private boolean banderaEsAnexoAdicional;

    /**
     * Bandera que indica si se puede anexar otro documento teniendo en cuenta el numero de
     * documentos solicitados. </br> <b>true</b> Se puede anexar documento/br> <b>false NO</b> se
     * puede anexar documento </br>
     */
    private boolean banderaEsValidoAdicionarAnexo;

    /* Listas */
    /**
     * Lista de solicitudes encontradas
     */
    private List<Solicitud> listaSolicitudes;

    /**
     * Lista de avaluos encontrados
     */
    private List<Avaluo> listaAvaluos;

    /**
     * Lista de radicaciones nuevas en correspondencia
     */
    private List<RadicacionNuevaAvaluoComercialDTO> listaRadicacionesCorrespondencia;

    private Solicitud[] solicitudesSeleccionadas;

    private Solicitud solicitudSeleccionada;

    private Avaluo[] avaluosSeleccionados;

    private Avaluo avaluoSeleccionado;

    private RadicacionNuevaAvaluoComercialDTO radicacionCorrespondenciaSeleccionada;

    private List<DetalleDocumentoFaltante> listaDetalleDocumentosFaltantes;

    private List<DetalleDocumentoFaltante> listaDetalleDocumentosBasicosFaltantes;

    private List<DetalleDocumentoFaltante> listaDetalleDocumentosAdicionalesFaltantes;

    private DetalleDocumentoFaltante detalleDocumentoFaltante;

    private List<SolicitudDocumentacion> listaSolicitudDocumentacionsAGuardar;

    private List<Documento> listaAnexos;

    private Documento anexoSeleccionado;

    private Documento documentoAnexoCargado;

    private List<SolicitudDocumentacion> listaSolicitudDocumentacionsSolicitud;

    private SolicitudDocumentacion solicitudDocumentacion;

    private List<TramiteDocumentacion> listaTramiteDocumentacions;

    private TramiteDocumentacion tramiteDocumentacion;

    private List<TipoDocumento> listaTiposDocumentosAdicionales;

    private TipoDocumento tipoDocumentoAdicionalSeleccionado;

    /**
     * usuario actual del sistema
     */
    private UsuarioDTO usuario;

    /* MB asociados */
    @Autowired
    private GeneralMB generalMB;

    /**
     * ManagedBean del caso de uso CU-SA-AC-047
     */
    private AdministracionRadicacionesMB administracionRadicacionesMB;

    /**
     * MB del caso de uso CU-SA-AC-146
     */
    private ReclasificacionDocumentoTramiteMB reclasificacionDocumentoTramiteMB;

    /* -------------SETTERS Y GETTERS ------------------------------------------ */
    public String getNumeroRadicadoSolicitud() {
        return numeroRadicadoSolicitud;
    }

    public void setNumeroRadicadoSolicitud(String numeroRadicadoSolicitud) {
        this.numeroRadicadoSolicitud = numeroRadicadoSolicitud;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public String getSecRadicado() {
        return secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    public String getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public boolean isBanderaTramiteEncontrado() {
        return banderaTramiteEncontrado;
    }

    public void setBanderaTramiteEncontrado(boolean banderaTramiteEncontrado) {
        this.banderaTramiteEncontrado = banderaTramiteEncontrado;
    }

    public List<Solicitud> getListaSolicitudes() {
        return listaSolicitudes;
    }

    public void setListaSolicitudes(List<Solicitud> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public List<Avaluo> getListaAvaluos() {
        return listaAvaluos;
    }

    public void setListaAvaluos(List<Avaluo> listaAvaluos) {
        this.listaAvaluos = listaAvaluos;
    }

    public List<RadicacionNuevaAvaluoComercialDTO> getListaRadicacionesCorrespondencia() {
        return listaRadicacionesCorrespondencia;
    }

    public void setListaRadicacionesCorrespondencia(
        List<RadicacionNuevaAvaluoComercialDTO> listaRadicacionesCorrespondencia) {
        this.listaRadicacionesCorrespondencia = listaRadicacionesCorrespondencia;
    }

    public Solicitud[] getSolicitudesSeleccionadas() {
        return solicitudesSeleccionadas;
    }

    public void setSolicitudesSeleccionadas(Solicitud[] solicitudesSeleccionadas) {
        this.solicitudesSeleccionadas = solicitudesSeleccionadas;
    }

    public Avaluo[] getAvaluosSeleccionados() {
        return avaluosSeleccionados;
    }

    public void setAvaluosSeleccionados(Avaluo[] avaluosSeleccionados) {
        this.avaluosSeleccionados = avaluosSeleccionados;
    }

    public RadicacionNuevaAvaluoComercialDTO getRadicacionCorrespondenciaSeleccionada() {
        return radicacionCorrespondenciaSeleccionada;
    }

    public void setRadicacionCorrespondenciaSeleccionada(
        RadicacionNuevaAvaluoComercialDTO radicacionCorrespondenciaSeleccionada) {
        this.radicacionCorrespondenciaSeleccionada = radicacionCorrespondenciaSeleccionada;
    }

    public ReclasificacionDocumentoTramiteMB getReclasificacionDocumentoTramiteMB() {
        return reclasificacionDocumentoTramiteMB;
    }

    public void setReclasificacionDocumentoTramiteMB(
        ReclasificacionDocumentoTramiteMB reclasificacionDocumentoTramiteMB) {
        this.reclasificacionDocumentoTramiteMB = reclasificacionDocumentoTramiteMB;
    }

    public boolean isBanderaTipoEmpresaEsNatural() {
        return banderaTipoEmpresaEsNatural;
    }

    public void setBanderaTipoEmpresaEsNatural(
        boolean banderaTipoEmpresaEsNatural) {
        this.banderaTipoEmpresaEsNatural = banderaTipoEmpresaEsNatural;
    }

    public boolean isBanderaTipoEmpresaSeleccionado() {
        return banderaTipoEmpresaSeleccionado;
    }

    public void setBanderaTipoEmpresaSeleccionado(
        boolean banderaTipoEmpresaSeleccionado) {
        this.banderaTipoEmpresaSeleccionado = banderaTipoEmpresaSeleccionado;
    }

    public Avaluo getAvaluoSeleccionado() {
        return avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public boolean isBanderaSeleccionTramitesSolicitudesValida() {
        return banderaSeleccionTramitesSolicitudesValida;
    }

    public void setBanderaSeleccionTramitesSolicitudesValida(
        boolean banderaSeleccionTramitesSolicitudesValida) {
        this.banderaSeleccionTramitesSolicitudesValida = banderaSeleccionTramitesSolicitudesValida;
    }

    public List<DetalleDocumentoFaltante> getListaDetalleDocumentosFaltantes() {
        return listaDetalleDocumentosFaltantes;
    }

    public void setListaDetalleDocumentosFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosFaltantes) {
        this.listaDetalleDocumentosFaltantes = listaDetalleDocumentosFaltantes;
    }

    public List<DetalleDocumentoFaltante> getListaDetalleDocumentosBasicosFaltantes() {
        return listaDetalleDocumentosBasicosFaltantes;
    }

    public void setListaDetalleDocumentosBasicosFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosBasicosFaltantes) {
        this.listaDetalleDocumentosBasicosFaltantes = listaDetalleDocumentosBasicosFaltantes;
    }

    public List<DetalleDocumentoFaltante> getListaDetalleDocumentosAdicionalesFaltantes() {
        return listaDetalleDocumentosAdicionalesFaltantes;
    }

    public void setListaDetalleDocumentosAdicionalesFaltantes(
        List<DetalleDocumentoFaltante> listaDetalleDocumentosAdicionalesFaltantes) {
        this.listaDetalleDocumentosAdicionalesFaltantes = listaDetalleDocumentosAdicionalesFaltantes;
    }

    public Documento getDocumentoAnexoCargado() {
        return documentoAnexoCargado;
    }

    public void setDocumentoAnexoCargado(Documento documentoAnexoCargado) {
        this.documentoAnexoCargado = documentoAnexoCargado;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Documento> getListaAnexos() {
        return listaAnexos;
    }

    public void setListaAnexos(List<Documento> listaAnexos) {
        this.listaAnexos = listaAnexos;
    }

    public Documento getAnexoSeleccionado() {
        return anexoSeleccionado;
    }

    public void setAnexoSeleccionado(Documento anexoSeleccionado) {
        this.anexoSeleccionado = anexoSeleccionado;
    }

    public SolicitudDocumentacion getSolicitudDocumentacion() {
        return solicitudDocumentacion;
    }

    public void setSolicitudDocumentacion(
        SolicitudDocumentacion solicitudDocumentacion) {
        this.solicitudDocumentacion = solicitudDocumentacion;
    }

    public DetalleDocumentoFaltante getDetalleDocumentoFaltante() {
        return detalleDocumentoFaltante;
    }

    public void setDetalleDocumentoFaltante(
        DetalleDocumentoFaltante detalleDocumentoFaltante) {
        this.detalleDocumentoFaltante = detalleDocumentoFaltante;
    }

    public TipoDocumento getTipoDocumentoAdicionalSeleccionado() {
        return tipoDocumentoAdicionalSeleccionado;
    }

    public void setTipoDocumentoAdicionalSeleccionado(
        TipoDocumento tipoDocumentoAdicionalSeleccionado) {
        this.tipoDocumentoAdicionalSeleccionado = tipoDocumentoAdicionalSeleccionado;
    }

    public List<TipoDocumento> getListaTiposDocumentosAdicionales() {
        return listaTiposDocumentosAdicionales;
    }

    public void setListaTiposDocumentosAdicionales(
        List<TipoDocumento> listaTiposDocumentosAdicionales) {
        this.listaTiposDocumentosAdicionales = listaTiposDocumentosAdicionales;
    }

    public boolean isBanderaEsAnexoAdicional() {
        return banderaEsAnexoAdicional;
    }

    public void setBanderaEsAnexoAdicional(boolean banderaEsAnexoAdicional) {
        this.banderaEsAnexoAdicional = banderaEsAnexoAdicional;
    }

    public List<SolicitudDocumentacion> getListaSolicitudDocumentacionsSolicitud() {
        return listaSolicitudDocumentacionsSolicitud;
    }

    public void setListaSolicitudDocumentacionsSolicitud(
        List<SolicitudDocumentacion> listaSolicitudDocumentacionsSolicitud) {
        this.listaSolicitudDocumentacionsSolicitud = listaSolicitudDocumentacionsSolicitud;
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public List<TramiteDocumentacion> getListaTramiteDocumentacions() {
        return listaTramiteDocumentacions;
    }

    public void setListaTramiteDocumentacions(
        List<TramiteDocumentacion> listaTramiteDocumentacions) {
        this.listaTramiteDocumentacions = listaTramiteDocumentacions;
    }

    public TramiteDocumentacion getTramiteDocumentacion() {
        return tramiteDocumentacion;
    }

    public void setTramiteDocumentacion(
        TramiteDocumentacion tramiteDocumentacion) {
        this.tramiteDocumentacion = tramiteDocumentacion;
    }

    public boolean isBanderaEsValidoAdicionarAnexo() {
        return banderaEsValidoAdicionarAnexo;
    }

    public void setBanderaEsValidoAdicionarAnexo(
        boolean banderaEsValidoAdicionarAnexo) {
        this.banderaEsValidoAdicionarAnexo = banderaEsValidoAdicionarAnexo;
    }

    public List<SolicitudDocumentacion> getListaSolicitudDocumentacionsAGuardar() {
        return listaSolicitudDocumentacionsAGuardar;
    }

    public void setListaSolicitudDocumentacionsAGuardar(
        List<SolicitudDocumentacion> listaSolicitudDocumentacionsAGuardar) {
        this.listaSolicitudDocumentacionsAGuardar = listaSolicitudDocumentacionsAGuardar;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin RecepcionDocumentacionMB#init");
    }

    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.iniciarCamposFormulario();
        this.iniciarFiltrosBusqueda();
        this.iniciarBanderas();
        this.iniciarListas();

        LOGGER.debug("Fin RecepcionDocumentacionMB#iniciarDatos");
    }

    /**
     * Se inician los campos del formulario
     *
     * @author rodrigo.hernandez
     */
    private void iniciarCamposFormulario() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#iniciarCamposFormulario");

        this.numeroRadicadoSolicitud = "";
        this.tipoTramite = "";
        this.secRadicado = "";
        this.tipoEmpresa = "";
        this.razonSocial = "";
        this.sigla = "";
        this.primerNombre = "";
        this.segundoNombre = "";
        this.primerApellido = "";
        this.segundoApellido = "";

        LOGGER.debug("Fin RecepcionDocumentacionMB#iniciarCamposFormulario");
    }

    /**
     * Se inician los filtros de búsqueda
     *
     * @author rodrigo.hernandez
     */
    private void iniciarFiltrosBusqueda() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#iniciarFiltrosBusqueda");

        this.filtroDatosConsultaAvaluo = new FiltroDatosConsultaAvaluo();
        this.filtroDatosSolicitud = new FiltroDatosSolicitud();

        LOGGER.debug("Fin RecepcionDocumentacionMB#iniciarFiltrosBusqueda");
    }

    /**
     * Método para iniciar datos de tipo boolean
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderas() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#iniciarBanderas");

        this.banderaTramiteEncontrado = false;
        this.banderaTipoEmpresaSeleccionado = false;
        this.banderaTipoEmpresaEsNatural = false;
        this.banderaSeleccionTramitesSolicitudesValida = false;
        this.banderaEsValidoAdicionarAnexo = true;

        LOGGER.debug("Fin RecepcionDocumentacionMB#iniciarBanderas");
    }

    /**
     * Método para iniciar listas, arrays y variables usadas en las tablas en página
     * RecibirDocumentacion.xhtml
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListas() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#iniciarDatosTablas");

        this.listaRadicacionesCorrespondencia = new ArrayList<RadicacionNuevaAvaluoComercialDTO>();
        this.listaSolicitudes = new ArrayList<Solicitud>();
        this.listaAvaluos = new ArrayList<Avaluo>();
        this.listaDetalleDocumentosFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaDetalleDocumentosBasicosFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaDetalleDocumentosAdicionalesFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.radicacionCorrespondenciaSeleccionada = new RadicacionNuevaAvaluoComercialDTO();
        this.listaAnexos = new ArrayList<Documento>();

        this.listaTiposDocumentosAdicionales = this.getGeneralesService()
            .getAllTipoDocumento();
        this.listaSolicitudDocumentacionsSolicitud = new ArrayList<SolicitudDocumentacion>();
        this.listaSolicitudDocumentacionsAGuardar = new ArrayList<SolicitudDocumentacion>();

        LOGGER.debug("Fin RecepcionDocumentacionMB#iniciarDatosTablas");
    }

    /**
     * Método que se llama desde pantalla para buscar trámites o solicitudes
     *
     * @author rodrigo.hernandez
     */
    public void buscarSolicitudesTramites() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#buscarSolicitudesTramites");

        /*
         * Se reinicia la bandera que determina la visibilidad del panel de documentos de
         * correspondencia
         */
        this.banderaSeleccionTramitesSolicitudesValida = false;

        /*
         * Si hay un numero de radicacion de solicitud ingresado, se busca si hay sec.radicados
         * asociados al numero de radicado de la solicitud.
         */
        if (this.secRadicado.isEmpty() &&
             !this.numeroRadicadoSolicitud.isEmpty()) {

            /*
             * Se busca si hay Sec. radicados asociados al numero de radicado de la solicitud
             */
            this.buscarTramites();

            /*
             * Si no se encontraron sec. radicados se procede a buscar solicitudes
             */
            if (!this.banderaTramiteEncontrado) {
                this.buscarSolicitudes();
            }

        } /*
         * Si hay un sec radicado ingresado, se buscan unicamente trámites
         */ else if (!this.secRadicado.isEmpty()) {

            this.buscarTramites();

        }/*
         * Si el campo del numero de radicacion de solicitud y el campo de sec radicado están
         * vacios, se busca primero las radicaciones y luego las solicitudes
         */ else if (this.secRadicado.isEmpty() &&
             this.numeroRadicadoSolicitud.isEmpty()) {

            this.buscarTramites();

            /* Si no se encontraron radicaciones se procede a buscar solicitudes */
            if (!this.banderaTramiteEncontrado) {
                this.buscarSolicitudes();
            }

        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#buscarSolicitudesTramites");
    }

    /**
     * Método para buscar solicitudes
     *
     * @author rodrigo.hernandez
     */
    public void buscarSolicitudes() {

        LOGGER.debug("Inicio RecepcionDocumentacionMB#buscarSolicitudes");

        this.listaSolicitudes.clear();

        this.filtroDatosSolicitud.setNumero(this.numeroRadicadoSolicitud);
        this.filtroDatosSolicitud.setTipoTramite(this.tipoTramite);

        if (this.tipoEmpresa != null && !this.tipoEmpresa.isEmpty()) {
            /* Se valida si el solicitante es persona natural */
            if (this.tipoEmpresa
                .equals(EPersonaTipoPersona.NATURAL.getCodigo())) {
                this.filtroDatosSolicitud.setPrimerNombre(this.primerNombre);
                this.filtroDatosSolicitud.setSegundoNombre(this.segundoNombre);
                this.filtroDatosSolicitud
                    .setPrimerApellido(this.primerApellido);
                this.filtroDatosSolicitud
                    .setSegundoApellido(this.segundoApellido);
            } else if (this.tipoEmpresa.equals(EPersonaTipoPersona.JURIDICA
                .getCodigo())) {
                this.filtroDatosSolicitud.setRazonSocial(this.razonSocial);
                this.filtroDatosSolicitud.setSigla(this.sigla);
            }
        }

        this.listaSolicitudes = this.getAvaluosService()
            .buscarSolicitudesPorSolicitante(this.filtroDatosSolicitud);

        this.banderaTramiteEncontrado = false;

        LOGGER.debug("Fin RecepcionDocumentacionMB#buscarSolicitudes");
    }

    /**
     * Método para buscar trámites
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true</b> Si se encontró algún trámite </br> <b>false</b> Si
     * <b>NO</b> se encontró algún trámite </br>
     *
     */
    private void buscarTramites() {

        LOGGER.debug("Inicio RecepcionDocumentacionMB#buscarTramites");

        this.listaAvaluos.clear();

        this.filtroDatosConsultaAvaluo
            .setNumeroRadicacion(this.numeroRadicadoSolicitud);
        this.filtroDatosConsultaAvaluo.setNumeroSecRadicado(this.secRadicado);
        this.filtroDatosConsultaAvaluo.setTipoTramite(this.tipoTramite);

        if (this.tipoEmpresa != null && !this.tipoEmpresa.isEmpty()) {
            /* Se valida si el solicitante es persona natural */
            if (this.tipoEmpresa
                .equals(EPersonaTipoPersona.NATURAL.getCodigo())) {
                this.filtroDatosConsultaAvaluo
                    .setPrimerNombre(this.primerNombre);
                this.filtroDatosConsultaAvaluo
                    .setSegundoNombre(this.segundoNombre);
                this.filtroDatosConsultaAvaluo
                    .setPrimerApellido(this.primerApellido);
                this.filtroDatosConsultaAvaluo
                    .setSegundoApellido(this.segundoApellido);
            } else if (this.tipoEmpresa.equals(EPersonaTipoPersona.JURIDICA
                .getCodigo())) {
                this.filtroDatosConsultaAvaluo
                    .setNombreRazonSocial(this.razonSocial);
                this.filtroDatosConsultaAvaluo.setSigla(this.sigla);
            }
        }

        this.listaAvaluos = this.getAvaluosService().buscarAvaluosPorFiltro(
            this.filtroDatosConsultaAvaluo);

        /* Se valida si la lista de tramites tiene alguno */
        if (this.listaAvaluos.size() > 0) {
            this.banderaTramiteEncontrado = true;
        } else {
            this.banderaTramiteEncontrado = false;
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#buscarTramites");

    }

    /**
     * Método para validar que las solicitudes/tramites seleccionados tengan el mismo numero de
     * radicado
     *
     * @author rodrigo.hernandez
     */
    public void validarTramitesSolicitudesSeleccionadas() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#validarTramitesSolicitudesSeleccionadas");

        String mensaje = "";
        this.banderaSeleccionTramitesSolicitudesValida = true;

        /* Se valida si la lista de tramites encontrados no está vacía */
        if (!this.listaAvaluos.isEmpty()) {
            /*
             * Se valida que se haya seleccionado uno o mas tramites de la tabla para activar panel
             * de docuemntos de correspondencia
             */
            if (this.avaluosSeleccionados.length > 0) {

                /*
                 * Se valida que se haya seleccionado mas de un tramite de la tabla para iniciar
                 * comparacion de numeros de radicado
                 */
                if (this.avaluosSeleccionados.length > 1) {
                    String numeroRadicado = this.avaluosSeleccionados[0]
                        .getTramite().getSolicitud().getNumero();

                    /*
                     * Se compara el numero de radicado de la solicitud de cada avaluo seleccionado
                     * para verificar que sea el mismo
                     */
                    for (int cont = 1; cont < this.avaluosSeleccionados.length; cont++) {
                        /*
                         * Si se encuentra un numero de radicado de solicitud diferente, entonces
                         * los tramites tienen diferente numero de radicado de solicitud
                         */
                        if (!numeroRadicado
                            .equals(this.avaluosSeleccionados[cont]
                                .getTramite().getSolicitud()
                                .getNumero())) {
                            this.banderaSeleccionTramitesSolicitudesValida = false;
                            break;
                        }
                    }
                    /*
                     * Se revisa si la selección es valida para definir la solicitud con la cual se
                     * van a consultar los documentos faltantes
                     */
                    if (this.banderaSeleccionTramitesSolicitudesValida) {
                        this.solicitudSeleccionada = this.avaluosSeleccionados[0]
                            .getTramite().getSolicitud();
                    }

                } else {
                    /*
                     * Se define la solicitud con la cual se van a consultar los documentos
                     * faltantes
                     */
                    this.solicitudSeleccionada = this.avaluosSeleccionados[0]
                        .getTramite().getSolicitud();
                }

            } else {
                this.banderaSeleccionTramitesSolicitudesValida = false;
            }
        } else if (!this.listaSolicitudes.isEmpty()) {

            /*
             * Se valida que se haya seleccionado una o mas solicitudes de la tabla para activar
             * panel de documentos de correspondencia
             */
            if (this.solicitudesSeleccionadas.length > 0) {

                /*
                 * Valida que se haya escogido mas de una solicitud, si es así, entonces es una
                 * seleccion invalida
                 */
                if (this.solicitudesSeleccionadas.length > 1) {
                    this.banderaSeleccionTramitesSolicitudesValida = false;
                } else {
                    /*
                     * Se define la solicitud con la cual se van a consultar los documentos
                     * faltantes
                     */
                    this.solicitudSeleccionada = this.solicitudesSeleccionadas[0];
                }

            } else {
                this.banderaSeleccionTramitesSolicitudesValida = false;
                mensaje = "Seleccione una solicitud";
                this.addMensajeError(mensaje);
            }
        }

        /* Segun el resultado de la validacion, se activa o no el panel */
        if (this.banderaSeleccionTramitesSolicitudesValida) {
            this.cargarPanelDocumentosDeCorrespondencia();
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#validarTramitesSolicitudesSeleccionadas");
    }

    /**
     * Método que llama al CU-SA-AC-47 Administración de radicaciones
     *
     * @author rodrigo.hernandez
     */
    private void cargarPanelDocumentosDeCorrespondencia() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#cargarDocumentosEnCorrespondencia");

        /*
         * Se inicia/resetea el MB de CU-SA-AC-047 Administracion de Radicaciones
         */
        this.administracionRadicacionesMB = (AdministracionRadicacionesMB) UtilidadesWeb
            .getManagedBean("administracionRadicacionesAvaluo");

        this.administracionRadicacionesMB
            .cargarDesdeCU152(this.solicitudSeleccionada);

        LOGGER.debug("Fin RecepcionDocumentacionMB#cargarDocumentosEnCorrespondencia");
    }

    /**
     * Método para cargar los documentos que fueron registrados como faltantes
     *
     * @author rodrigo.hernandez
     */
    public void buscarDocumentosFaltantesAsociados() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#buscarDocumentosFaltantesAsociados");

        this.listaDetalleDocumentosFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaDetalleDocumentosBasicosFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaDetalleDocumentosAdicionalesFaltantes = new ArrayList<DetalleDocumentoFaltante>();
        this.listaSolicitudDocumentacionsAGuardar = new ArrayList<SolicitudDocumentacion>();

        /* Se traen los DetalleDocumentoFaltante's asociados a la solicitud */
        this.listaDetalleDocumentosFaltantes = this.getAvaluosService()
            .obtenerDetallesDocumentosFaltantesPorIdSolicitud(
                this.solicitudSeleccionada.getId());

        this.crearListaDetallesDocumentosAdicionalesFaltantes();
        this.crearListaDetallesDocumentosBasicosFaltantes();

        LOGGER.debug("Fin RecepcionDocumentacionMB#buscarDocumentosFaltantesAsociados");
    }

    /**
     * Método para crear la lista de detalles de documentos básicos faltantes
     *
     * @author rodrigo.hernandez
     */
    private void crearListaDetallesDocumentosBasicosFaltantes() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#crearListaDetallesDocumentosBasicosFaltantes");

        /* Se buscan los tipos de documentos que son básicos */
        List<TipoDocumento> listaTipoDocsBasicos = this.getGeneralesService()
            .getCacheTiposDocumentoPorTipoTramite(
                this.solicitudSeleccionada.getTipo(), null, null);

        /*
         * Se crea el modelo de la tabla de detalles de documentos basicos faltantes.
         *
         * Se asume que no siempre se van a recibir todos los tipos de documentos, por ende, se
         * carga la tabla con la lista por default de los documentos basicos
         */
        for (TipoDocumento tipoDocumento : listaTipoDocsBasicos) {
            DetalleDocumentoFaltante ddf = new DetalleDocumentoFaltante();
            ddf.setTipoDocumento(tipoDocumento);
            ddf.setCantidadEntrega(0L);
            ddf.setCantidadSolicitud(0L);
            this.listaDetalleDocumentosBasicosFaltantes.add(ddf);
        }

        /*
         * Se actualiza el modelo de la tabla de detalles de documentos basicos faltantes con los
         * DetalleDocumentoFaltante's traídos de la BD
         */
        for (int cont = 0; cont < this.listaDetalleDocumentosBasicosFaltantes
            .size(); cont++) {
            for (int contAux = 0; contAux < this.listaDetalleDocumentosFaltantes
                .size(); contAux++) {
                if (this.listaDetalleDocumentosFaltantes
                    .get(contAux)
                    .getTipoDocumento()
                    .getId()
                    .equals(this.listaDetalleDocumentosBasicosFaltantes
                        .get(cont).getTipoDocumento().getId())) {
                    this.listaDetalleDocumentosBasicosFaltantes.set(cont,
                        this.listaDetalleDocumentosFaltantes.get(contAux));
                    /*
                     * Cuando encuentra al DetalleDocumentoFaltante que tiene el mismo tipo de
                     * solicitud, se sale del for, para seguir con el siguiente objeto a comparar
                     * (Se asume que no hay tipos de documento repetidos)
                     */
                    break;
                }
            }
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#crearListaDetallesDocumentosBasicosFaltantes");
    }

    /**
     * Método para crear la lista de detalles de documentos adicionales faltantes
     *
     * @author rodrigo.hernandez
     */
    private void crearListaDetallesDocumentosAdicionalesFaltantes() {
        LOGGER.debug(
            "Inicio RecepcionDocumentacionMB#crearListaDetallesDocumentosAdicionalesFaltantes");

        /* Se buscan los tipos de documentos que son adicionales */
        List<TipoDocumento> listaTipoDocsAdicionales = this
            .getGeneralesService().getAllTipoDocumento();

        /*
         * Se crea el modelo de la tabla de detalles de documentos basicos adicionales
         */
        for (TipoDocumento tipoDocumento : listaTipoDocsAdicionales) {
            for (DetalleDocumentoFaltante detalleDocFaltante : this.listaDetalleDocumentosFaltantes) {
                /*
                 * Se valida si el DetalleDocumentoFaltante que se trae desde la BD, tiene asociado
                 * un tipo de documento adicional
                 */
                if (detalleDocFaltante.getTipoDocumento().getId()
                    .equals(tipoDocumento.getId())) {
                    this.listaDetalleDocumentosAdicionalesFaltantes
                        .add(detalleDocFaltante);
                }
            }
        }

        LOGGER.
            debug("Fin RecepcionDocumentacionMB#crearListaDetallesDocumentosAdicionalesFaltantes");
    }

    /**
     * Método para agregar un nuevo detalleDocumentoFaltante asociado a un tipo de documento
     * adicional
     *
     * @author rodrigo.hernandez
     *
     */
    public void agregarDocumentoAdicional() {

        LOGGER.debug("Inicio RecepcionDocumentacionMB#agregarDocumentoAdicional");

        String mensaje = "";
        boolean banderaExisteTipoDocumento = false;

        DetalleDocumentoFaltante detalleFaltanteAdicional = new DetalleDocumentoFaltante();

        detalleFaltanteAdicional
            .setTipoDocumento(this.tipoDocumentoAdicionalSeleccionado);
        detalleFaltanteAdicional.setCantidadEntrega(0L);
        detalleFaltanteAdicional.setCantidadSolicitud(0L);

        /*
         * Se valida que la lista de documentos faltantes adicionales no este vacía
         */
        if (!this.listaDetalleDocumentosAdicionalesFaltantes.isEmpty()) {
            /*
             * Se recorre la lista de detalle documentos faltantes adicionales para validar si ya
             * existe un detalle con el tipo de documento que se quiere adicionar
             */
            for (DetalleDocumentoFaltante ddf : this.listaDetalleDocumentosAdicionalesFaltantes) {
                if (ddf.getTipoDocumento()
                    .getId()
                    .equals(detalleFaltanteAdicional.getTipoDocumento()
                        .getId())) {
                    banderaExisteTipoDocumento = true;
                    break;
                }
            }
        }

        /*
         * Si no existe el tipo de documento asociado a un detalle, se adiciona a la tabla de
         * detalleDocumentoFaltante adicionales
         */
        if (!banderaExisteTipoDocumento) {
            this.listaDetalleDocumentosAdicionalesFaltantes
                .add(detalleFaltanteAdicional);

            mensaje = "El documento fue adicionado";
            this.addMensajeInfo(mensaje);
        } else {

            mensaje = "El documento ya se encuentra agregado";
            this.addMensajeError(mensaje);
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#agregarDocumentoAdicional");

    }

    /**
     * Método que se ejecuta al cambiar el elemento seleccionado en el SelectOneMenu de Tipo de
     * Empresa
     *
     * @author rodrigo.hernandez
     */
    public void onChangeTipoEmpresa() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#onChangeTipoEmpresa");

        /* Se valida que se haya seleccionado un tipo de empresa */
        if (!this.tipoEmpresa.isEmpty()) {

            /*
             * Se activan todos los campos relacionados a una empresa o persona
             */
            this.banderaTipoEmpresaSeleccionado = true;

            /* Se valida si el tipo de empresa seleccionado es NATURAL */
            if (this.tipoEmpresa
                .equals(EPersonaTipoPersona.NATURAL.getCodigo())) {
                /*
                 * Se activa la bandera persona natural para habilitar los campos relacionados con
                 * los datos de una persona natural
                 */
                this.banderaTipoEmpresaEsNatural = true;
            }/* Se valida si el tipo
             * de empresa seleccionado es JURIDICA */ else if (this.tipoEmpresa.equals(
                EPersonaTipoPersona.JURIDICA
                    .getCodigo())) {
                /*
                 * Se desactiva la bandera de persona natural para habilitar los campos relacionados
                 * con los datos de una persona juridica
                 */
                this.banderaTipoEmpresaEsNatural = false;
            }
        } else {
            /*
             * Se desactivan todos los campos relacionados a una empresa o persona
             */
            this.banderaTipoEmpresaSeleccionado = false;
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#onChangeTipoEmpresa");
    }

    /**
     * Método para preparar las variables involucradas en el cargue y almacenamiento de anexos
     *
     * @author rodrigo.hernandez
     */
    public void alistarCargaAnexo() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#alistarCargaAnexo");

        this.documentoAnexoCargado = new Documento();

        this.solicitudDocumentacion = new SolicitudDocumentacion();

        /*
         * Se actualiza la bandera en caso de que el numero de docs entregados sea igual al numero
         * de docs solicitados
         */
        if (this.detalleDocumentoFaltante.getCantidadEntrega() >= this.detalleDocumentoFaltante
            .getCantidadSolicitud() - 1) {
            this.banderaEsValidoAdicionarAnexo = false;
        } else {
            this.banderaEsValidoAdicionarAnexo = true;
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#alistarCargaAnexo");
    }

    /**
     * Método para cargar los anexo relacionado a un documento
     *
     * Se llama desde cargarAnexosDocumentosFaltantes.xhtml, (p:fileUpload id="cargarAnexosFU")
     *
     * @author rodrigo.hernandez
     */
    public void cargarAnexo(FileUploadEvent event) {

        LOGGER.debug("Inicio RecepcionDocumentacionMB#cargarAnexo");

        this.documentoAnexoCargado.setArchivo(event.getFile().getFileName());

        File archivo = null;

        String ruta = event.getFile().getFileName();

        try {
            String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

            archivo = new File(directorioTemporal + "//" + ruta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(archivo);

            byte[] buffer = new byte[Math.round(event.getFile().getSize())];

            int bulk;
            InputStream inputStream = event.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

        } catch (Exception e) {
            String mensaje = "Error al cargar el documento";
            this.addMensajeError(mensaje);
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#cargarAnexo");
    }

    /**
     * Método para almacenar temporalmente los anexos creados
     *
     * @author rodrigo.hernandez
     */
    public void almacenarAnexo() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#almacenarAnexo");

        SolicitudDocumentacion solDoc;

        /* Se valida si se cargó algun archivo */
        if (this.documentoAnexoCargado.getArchivo() != null) {

            /* Se establecen propiedades del anexo cargado */
            this.documentoAnexoCargado
                .setTipoDocumento(this.detalleDocumentoFaltante
                    .getTipoDocumento());
            this.documentoAnexoCargado
                .setEstado(EDocumentoEstado.SIN_REGISTRAR_SOLICITUD
                    .getCodigo());
            this.documentoAnexoCargado.setUsuarioCreador(this.usuario
                .getLogin());
            this.documentoAnexoCargado.setBloqueado(ESiNo.NO.getCodigo());
            this.documentoAnexoCargado.setUsuarioLog(this.usuario.getLogin());
            this.documentoAnexoCargado.setFechaLog(new Date());
            this.documentoAnexoCargado
                .setFechaRadicacion(this.solicitudSeleccionada.getFecha());
            this.documentoAnexoCargado
                .setNumeroRadicacion(this.radicacionCorrespondenciaSeleccionada
                    .getNumeroRadicacion());

            /*
             * Se definen los atributos faltantes de la solicitudDocumentacion
             */
            solDoc = new SolicitudDocumentacion();

            solDoc.setSoporteDocumentoId(this.documentoAnexoCargado);

            /* Se define si la solicitudDocumentacion es adicional o no */
            if (this.banderaEsAnexoAdicional) {
                solDoc.setAdicional(ESiNo.SI.getCodigo());
            } else {
                solDoc.setAdicional(ESiNo.NO.getCodigo());
            }

            solDoc.setAportado(ESiNo.NO.getCodigo());
            solDoc.setFechaAporte(new Date());
            solDoc.setSolicitud(this.solicitudSeleccionada);
            solDoc.setUsuarioLog(this.usuario.getLogin());
            solDoc.setFechaLog(new Date());
            solDoc.setTipoDocumento(this.detalleDocumentoFaltante
                .getTipoDocumento());
            solDoc.setDetalle(this.documentoAnexoCargado.getDescripcion());

            this.listaSolicitudDocumentacionsAGuardar.add(solDoc);

            /* Se aumenta la cantidad de documentos entregados en 1 */
            this.detalleDocumentoFaltante
                .setCantidadEntrega(this.detalleDocumentoFaltante
                    .getCantidadEntrega() + 1);

        } else {
            String mensaje = " Por favor ingrese un archivo";
            this.addMensajeError(mensaje);

        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#almacenarAnexo");
    }

    /**
     * Método para guardar anexos en Alfresco y en BD
     *
     * @author rodrigo.hernandez
     */
    private void almacenarAnexosDefinitivamente() {

        LOGGER.debug("Fin RecepcionDocumentacionMB#almacenarAnexosDefinitivamente");

        DocumentoSolicitudDTO docSolicitudDTO;
        String mensaje = "";

        try {
            for (SolicitudDocumentacion solDoc : this.listaSolicitudDocumentacionsAGuardar) {
                /* Se crea el objeto para almacenar en Alfresco */
                docSolicitudDTO = DocumentoSolicitudDTO
                    .crearArgumentoCargarSolicitudAvaluoComercial(
                        this.solicitudSeleccionada.getNumero(),
                        this.solicitudSeleccionada.getFecha(), solDoc
                        .getSoporteDocumentoId()
                        .getTipoDocumento().getNombre(),
                        UtilidadesWeb.obtenerRutaTemporalArchivos() +
                         solDoc.getSoporteDocumentoId()
                            .getArchivo());

                // Se guarda el documento
                solDoc.setSoporteDocumentoId((this.getGeneralesService()
                    .guardarDocumento(solDoc.getSoporteDocumentoId())));

                solDoc.setAportado(ESiNo.SI.getCodigo());

                solDoc.getSoporteDocumentoId().setEstado(
                    EDocumentoEstado.RECIBIDO.getCodigo());

                /* Se almacena la solicitudDocumentacion */
                this.getTramiteService()
                    .guardarActualizarSolicitudDocumentacion(solDoc);

                // guardar documento en alfresco
                this.getGeneralesService()
                    .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                        docSolicitudDTO, solDoc.getSoporteDocumentoId());

            }

            /*
             * Se actualiza la lista de DetalleDocumentoFaltantes para documentos Basicos
             */
            for (DetalleDocumentoFaltante detDocFal : this.listaDetalleDocumentosBasicosFaltantes) {
                /*
                 * Si el detalle no tiene un SolicitudDocumento asociado es porque es un objeto que
                 * solo se usó para llenr la tabla de documentos basicos
                 */
                if (detDocFal.getSolicitudDocumento() != null) {
                    this.getAvaluosService()
                        .guardarActualizarDetalleDocumentoFaltante(
                            detDocFal);
                }
            }

            /*
             * Se actualiza la lista de DetalleDocumentoFaltantes para documentos adicionales
             */
            for (DetalleDocumentoFaltante detDocFal
                : this.listaDetalleDocumentosAdicionalesFaltantes) {
                this.getAvaluosService()
                    .guardarActualizarDetalleDocumentoFaltante(detDocFal);
            }

            mensaje = "La actualización de datos fue exitosa";
            this.addMensajeInfo(mensaje);
        } catch (Exception e) {

            mensaje = "Hubo problemas para actualizar la información";
            this.addMensajeError(mensaje);

            LOGGER.error("Error en RecepcionDocumentacionMB#almacenarAnexosDefinitivamente: " +
                 e.getMessage());
            e.printStackTrace();
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#almacenarAnexosDefinitivamente");
    }

    /**
     * Método para buscar los anexos asociados a un tipo de documento
     *
     * @author rodrigo.hernandez
     */
    public void buscarAnexosTipoDocumento() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#buscarAnexosTipoDocumento");

        /* Se traen las SolicitudDocumentacion que hay en BD */
        this.listaSolicitudDocumentacionsSolicitud = this.getAvaluosService()
            .obtenerListaSolDocsPorSolicitudYTipoDocumento(
                this.solicitudSeleccionada.getId(),
                this.detalleDocumentoFaltante.getTipoDocumento()
                    .getId());

        /* Se concatena con las SolicitudesDocumentacion nuevas */
        this.listaSolicitudDocumentacionsSolicitud
            .addAll(listaSolicitudDocumentacionsAGuardar);

        LOGGER.debug("Fin RecepcionDocumentacionMB#buscarAnexosTipoDocumento");
    }

    /**
     * Método para ejecutar almacenamiento de un anexo más de una vez
     *
     * @author rodrigo.hernandez
     */
    public void almacenarAnexoYContinuar() {
        LOGGER.debug("Inicio RecepcionDocumentacionMB#almacenarAnexoYContinuar");

        this.almacenarAnexo();
        this.alistarCargaAnexo();

        LOGGER.debug("Fin RecepcionDocumentacionMB#almacenarAnexoYContinuar");
    }

    /**
     * Método para eliminar anexo
     *
     * Se llama desde el dialogo de confirmación de eliminar anexo
     *
     * @author rodrigo.hernandez
     *
     */
    public void eliminarAnexo() {

        LOGGER.debug("Fin RecepcionDocumentacionMB#eliminarAnexo");

        if (this.solicitudDocumentacion.getAportado().equals(
            ESiNo.SI.getCodigo())) {
            /* Se borra el documento en BD y Alfresco */
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(
                this.solicitudDocumentacion.getSoporteDocumentoId());

            /* Se borra la solicitud documentación */
            this.getTramiteService().borrarSolicitudDocumentacion(
                this.solicitudDocumentacion);
        }

        if (this.detalleDocumentoFaltante.getCantidadEntrega() > 0) {
            /* Se resta la cantidad de documentos entregados en 1 */
            this.detalleDocumentoFaltante
                .setCantidadEntrega(this.detalleDocumentoFaltante
                    .getCantidadEntrega() - 1);

            /* Se actualiza el objeto DetalleDocumentoFaltante */
            this.getAvaluosService().guardarActualizarDetalleDocumentoFaltante(
                this.detalleDocumentoFaltante);
        }

        this.listaSolicitudDocumentacionsAGuardar
            .remove(this.solicitudDocumentacion);

        /* Se recarga la tabla de anexoss */
        this.buscarAnexosTipoDocumento();

        LOGGER.debug("Fin RecepcionDocumentacionMB#eliminarAnexo");
    }

    /**
     * Método para reemplazar anexo
     *
     * Se llama desde el dialogo de confirmación de eliminar anexo
     *
     * @author rodrigo.hernandez
     *
     */
    public void reemplazarAnexo() {

        LOGGER.debug("Fin RecepcionDocumentacionMB#reemplazarAnexo");

        this.eliminarAnexo();
        this.alistarCargaAnexo();

        LOGGER.debug("Fin RecepcionDocumentacionMB#reemplazarAnexo");
    }

    /**
     * Método preparacion del documento a visualizar
     *
     * @author rodrigo.hernandez
     */
    public void visualizarDocumento() {

        LOGGER.debug("Inicio RecepcionDocumentacionMB#visualizarDocumento");

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        this.setTipoMimeDocTemporal(fileNameMap
            .getContentTypeFor(this.solicitudDocumentacion
                .getSoporteDocumentoId().getArchivo()));
        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        if (this.solicitudDocumentacion.getSoporteDocumentoId().getEstado()
            .equals(EDocumentoEstado.SIN_REGISTRAR_SOLICITUD.getCodigo())) {
            previsualizacionDocMB
                .setDocumentoTemporal(this.solicitudDocumentacion
                    .getSoporteDocumentoId());
            LOGGER.debug("Doc temporal");
        } else {
            LOGGER.debug("Doc de alfresco");
            previsualizacionDocMB
                .setDocumentoAlfresco(this.solicitudDocumentacion
                    .getSoporteDocumentoId());
        }

        LOGGER.debug("Type " +
             fileNameMap.getContentTypeFor(this.solicitudDocumentacion
                .getSoporteDocumentoId().getArchivo()));
        LOGGER.debug("Visualizando Documento " +
             solicitudDocumentacion.getSoporteDocumentoId().getArchivo());

        LOGGER.debug("Fin RecepcionDocumentacionMB#visualizarDocumento");
    }

    /**
     * Método para finalizar proceso de recebir documentos
     *
     * @author rodrigo.hernandez
     */
    public void guardar() {
        LOGGER.debug("Fin RecepcionDocumentacionMB#guardar");

        String message = "";

        /* Si no hay documentos anexados no permite guardar */
        if (!this.listaSolicitudDocumentacionsAGuardar.isEmpty()) {
            this.almacenarAnexosDefinitivamente();
            this.listaSolicitudDocumentacionsAGuardar.clear();
        } else {
            message = "No se han anexado documentos, por favor verifique";
            this.addMensajeError(message);
        }

        LOGGER.debug("Fin RecepcionDocumentacionMB#guardar");
    }

    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {

        LOGGER.debug("iniciando RecepcionDocumentacionMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("finalizando RecepcionDocumentacionMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

}
