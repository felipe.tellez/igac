package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso CU-SA-AC-086 Consultar Consignacion de Anticipo
 *
 * @cu CU-SA-AC-086
 *
 * @author christian.rodriguez
 *
 */
@Component("consultaConsignacionAnticipo")
@Scope("session")
public class ConsultaConsignacionAnticipoMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -8677750343941280965L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaConsignacionAnticipoMB.class);

    /**
     * ManagedBean del caso de uso 87
     */
    private ConsultaAvaluosAprobadosMB consultaAvaluosAprobadosMB;

    /**
     * Entidad solicitante del contrato
     */
    private Solicitante entidad;

    /**
     * Almacena el contrato actualmente en edición
     */
    private ContratoInteradministrativo contratoSeleccionado;

    // ---------------------------------Banderas---------------------------------
    public boolean banderaActivarBotonConsultarAvaluos;

    // ----------------------------Métodos SET y GET----------------------------
    public ContratoInteradministrativo getContratoSeleccionado() {
        return this.contratoSeleccionado;
    }

    public Solicitante getEntidad() {
        return this.entidad;
    }

    public boolean isBanderaActivarBotonConsultarAvaluos() {
        return this.banderaActivarBotonConsultarAvaluos;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaConsignacionAnticipoMB init");

    }

    // -------------------------------------------------------------------------
    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado por el caso de uso 008
     *
     * @author christian.rodriguez
     */
    public void cargarDesdeCU08(ContratoInteradministrativo contrato, Solicitante entidad) {

        if (contrato != null && entidad != null) {

            this.banderaActivarBotonConsultarAvaluos = false;
            this.contratoSeleccionado = this.getAvaluosService()
                .obtenerContratoInteradministrativoPorIdConConsignaciones(contrato.getId());

            this.entidad = entidad;
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que se encarga de ejecutar el caso de uso 87
     *
     * @author christian.rodriguez
     */
    public void cargarConsultaAvaluosDeContrato() {
        if (this.contratoSeleccionado != null &&
             this.contratoSeleccionado.getConsignaciones() != null) {

            this.consultaAvaluosAprobadosMB = (ConsultaAvaluosAprobadosMB) UtilidadesWeb
                .getManagedBean("consultaAvaluosAprobados");
            this.consultaAvaluosAprobadosMB.cargarDesdeCU086(this.contratoSeleccionado
                .getConsignaciones().get(0));

        } else {

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("El contrato no cuenta con avalúos aprobados.");
        }
    }

}
