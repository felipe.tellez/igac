/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.asignacion.ManejoComisionesTramitesMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso Realizar informe visita terreno
 *
 * @author juanfelipe.garcia
 * @CU CU-NP-CO-211
 */
@Component("realizarInfVisTer")
@Scope("session")
public class RealizarInformeVisitaTerrenoMB extends ProcessFlowManager {

    /**
     * Serial
     */
    private static final long serialVersionUID = 1258964L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ManejoComisionesTramitesMB.class);
    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Variable que almacena el trámite que fue seleccionado
     */
    private Tramite tramiteSeleccionado;

    /**
     * Variable que almacena la comisión de depuración para el trámite que fue seleccionado
     */
    private Comision comisionTramiteSeleccionado;

    /**
     * Documento de la comisión de depuración
     */
    private Documento documentoComisionTramiteSeleccionado;

    /**
     * Inconsistencias asociadas al trámite seleccionado
     */
    private List<TramiteInconsistencia> inconsistenciasTramite =
        new ArrayList<TramiteInconsistencia>();

    //--------------------------------------------------------------------------------------------------        
    /*
     * Variables del formulario
     */
    private String tiempoProgramado;

    private String tiempoUtilizado;

    private String motivo;

    private String tareasAdicionales;

    private String tareasAdicionalesRealizadas;

    private String actividadesDesarrolladas;

    private String inconvenientesAdministrativos;

    private String inconvenientesTipo1Texto;

    private String inconvenientesTipo2Texto;
    //--------------------------------------------------------------------------------------------------        
    /**
     * Arreglo que almacena los trabajos seleccionados de la tabla de trabajos programados
     */
    private TramiteInconsistencia[] trabajoProgramadoSeleccionado;

    /**
     * Lista que almacena los inconvenientes seleccionados (social o técnico)
     */
    private List<String> inconvenientesTipo1;

    /**
     * Lista que almacena los inconvenientes seleccionados (logístico o humano)
     */
    private List<String> inconvenientesTipo2;

    //--------------------------------------------------------------------------------------------------        
    /*
     * Variables de control de componentes de la página
     */
    private Boolean habilitarBotonGenerarDoc;

    private Boolean habilitarBotonGuardarDoc;

    private Boolean habilitarAvanzarProceso;

    private Boolean habilitarTareasAdicionalesTexto;

    private Boolean habilitarTiempoProgramado;

    private Boolean habilitarInconvenientesTipo1Texto;

    private Boolean habilitarInconvenientesTipo2Texto;

    private boolean habilitarMotivo;

    //--------------------------------------------------------------------------------------------------
    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    /**
     * Predios relacionados al tramite actual
     */
    private List<Predio> prediosTramite;

    /**
     * Objeto que contiene los datos del reporte de la visita de terreno
     */
    private ReporteDTO informeVisita;

    /**
     * Objeto que contiene los datos del reporte documento comisión
     */
    private ReporteDTO reporteComision;

    //--------------------------------------------------------------------------------------------------
    //Acceso a las variables
    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public boolean isHabilitarMotivo() {
        return habilitarMotivo;
    }

    public void setHabilitarMotivo(boolean habilitarMotivo) {
        this.habilitarMotivo = habilitarMotivo;
    }

    public Comision getComisionTramiteSeleccionado() {
        return comisionTramiteSeleccionado;
    }

    public void setComisionTramiteSeleccionado(Comision comisionTramiteSeleccionado) {
        this.comisionTramiteSeleccionado = comisionTramiteSeleccionado;
    }

    public Documento getDocumentoComisionTramiteSeleccionado() {
        return documentoComisionTramiteSeleccionado;
    }

    public void setDocumentoComisionTramiteSeleccionado(
        Documento documentoComisionTramiteSeleccionado) {
        this.documentoComisionTramiteSeleccionado = documentoComisionTramiteSeleccionado;
    }

    public String getTiempoProgramado() {
        return tiempoProgramado;
    }

    public void setTiempoProgramado(String tiempoProgramado) {
        this.tiempoProgramado = tiempoProgramado;
    }

    public String getTiempoUtilizado() {
        return tiempoUtilizado;
    }

    public void setTiempoUtilizado(String tiempoUtilizado) {
        this.tiempoUtilizado = tiempoUtilizado;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public TramiteInconsistencia[] getTrabajoProgramadoSeleccionado() {
        return trabajoProgramadoSeleccionado;
    }

    public void setTrabajoProgramadoSeleccionado(
        TramiteInconsistencia[] trabajoProgramadoSeleccionado) {
        this.trabajoProgramadoSeleccionado = trabajoProgramadoSeleccionado;
    }

    public String getTareasAdicionales() {
        return tareasAdicionales;
    }

    public void setTareasAdicionales(String tareasAdicionales) {
        this.tareasAdicionales = tareasAdicionales;
    }

    public String getTareasAdicionalesRealizadas() {
        return tareasAdicionalesRealizadas;
    }

    public void setTareasAdicionalesRealizadas(String tareasAdicionalesRealizadas) {
        this.tareasAdicionalesRealizadas = tareasAdicionalesRealizadas;
    }

    public String getActividadesDesarrolladas() {
        return actividadesDesarrolladas;
    }

    public void setActividadesDesarrolladas(String actividadesDesarrolladas) {
        this.actividadesDesarrolladas = actividadesDesarrolladas;
    }

    public String getInconvenientesAdministrativos() {
        return inconvenientesAdministrativos;
    }

    public void setInconvenientesAdministrativos(String inconvenientesAdministrativos) {
        this.inconvenientesAdministrativos = inconvenientesAdministrativos;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public UsuarioDTO getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UsuarioDTO currentUser) {
        this.currentUser = currentUser;
    }

    public List<Predio> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<Predio> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public List<String> getInconvenientesTipo1() {
        return inconvenientesTipo1;
    }

    public void setInconvenientesTipo1(List<String> inconvenientesTipo1) {
        this.inconvenientesTipo1 = inconvenientesTipo1;
    }

    public List<String> getInconvenientesTipo2() {
        return inconvenientesTipo2;
    }

    public void setInconvenientesTipo2(List<String> inconvenientesTipo2) {
        this.inconvenientesTipo2 = inconvenientesTipo2;
    }

    public String getInconvenientesTipo1Texto() {
        return inconvenientesTipo1Texto;
    }

    public void setInconvenientesTipo1Texto(String inconvenientesTipo1Texto) {
        this.inconvenientesTipo1Texto = inconvenientesTipo1Texto;
    }

    public String getInconvenientesTipo2Texto() {
        return inconvenientesTipo2Texto;
    }

    public void setInconvenientesTipo2Texto(String inconvenientesTipo2Texto) {
        this.inconvenientesTipo2Texto = inconvenientesTipo2Texto;
    }

    public Boolean getHabilitarBotonGenerarDoc() {
        return habilitarBotonGenerarDoc;
    }

    public void setHabilitarBotonGenerarDoc(Boolean habilitarBotonGenerarDoc) {
        this.habilitarBotonGenerarDoc = habilitarBotonGenerarDoc;
    }

    public Boolean getHabilitarAvanzarProceso() {
        return habilitarAvanzarProceso;
    }

    public void setHabilitarAvanzarProceso(Boolean habilitarAvanzarProceso) {
        this.habilitarAvanzarProceso = habilitarAvanzarProceso;
    }

    public Boolean getHabilitarTareasAdicionalesTexto() {
        return habilitarTareasAdicionalesTexto;
    }

    public void setHabilitarTareasAdicionalesTexto(Boolean habilitarTareasAdicionalesTexto) {
        this.habilitarTareasAdicionalesTexto = habilitarTareasAdicionalesTexto;
    }

    public Boolean getHabilitarTiempoProgramado() {
        return habilitarTiempoProgramado;
    }

    public void setHabilitarTiempoProgramado(Boolean habilitarTiempoProgramado) {
        this.habilitarTiempoProgramado = habilitarTiempoProgramado;
    }

    public Boolean getHabilitarInconvenientesTipo1Texto() {
        return habilitarInconvenientesTipo1Texto;
    }

    public void setHabilitarInconvenientesTipo1Texto(Boolean habilitarInconvenientesTipo1Texto) {
        this.habilitarInconvenientesTipo1Texto = habilitarInconvenientesTipo1Texto;
    }

    public Boolean getHabilitarInconvenientesTipo2Texto() {
        return habilitarInconvenientesTipo2Texto;
    }

    public void setHabilitarInconvenientesTipo2Texto(Boolean habilitarInconvenientesTipo2Texto) {
        this.habilitarInconvenientesTipo2Texto = habilitarInconvenientesTipo2Texto;
    }

    public ReporteDTO getInformeVisita() {
        return informeVisita;
    }

    public void setInformeVisita(ReporteDTO informeVisita) {
        this.informeVisita = informeVisita;
    }

    public Boolean getHabilitarBotonGuardarDoc() {
        return habilitarBotonGuardarDoc;
    }

    public void setHabilitarBotonGuardarDoc(Boolean habilitarBotonGuardarDoc) {
        this.habilitarBotonGuardarDoc = habilitarBotonGuardarDoc;
    }

    public ReporteDTO getReporteComision() {
        return reporteComision;
    }

    public void setReporteComision(ReporteDTO reporteComision) {
        this.reporteComision = reporteComision;
    }

    //--------------------------------------------------------------------------------------------------
    //Métodos de la clase
    /**
     * Constructor
     */
    public RealizarInformeVisitaTerrenoMB() {
    }

    @PostConstruct
    public void init() {

        this.currentUser = MenuMB.getMenu().getUsuarioDto();
        this.tramiteSeleccionado = new Tramite();

        //booleanos de control pagina
        this.habilitarBotonGenerarDoc = false;
        this.habilitarBotonGuardarDoc = false;
        this.habilitarAvanzarProceso = false;
        this.habilitarTareasAdicionalesTexto = false;
        this.habilitarTiempoProgramado = true;
        this.habilitarInconvenientesTipo1Texto = false;
        this.habilitarInconvenientesTipo2Texto = false;
        //valor inicial
        this.tareasAdicionales = "NO";

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "realizarInfVisTer");

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        //Carga el objeto del proceso y la actividad actual
        Long idtramiteActual = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();
        this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        //datos de prueba
        this.tramiteSeleccionado.setId(idtramiteActual);

        //cargar datos del tramite
        this.cargarDatosTramite();

        //obtener la documentacion de la ultima comision
        if (this.tramiteSeleccionado.getComisionTramiteDepuracion() != null) {
            Comision comisionDep = this.tramiteSeleccionado.getComisionTramiteDepuracion().
                getComision();
            if (comisionDep.getEstado() != null //javier.aponte se cambia de CANCELADA.getValor() a CANCELADA.toString() porque en la base de datos
                //el estado de la comisión se está guardando como 'CANCELADA' y no como 'Cancelada' luego esta condición
                //nunca se estaba cumpliendo
                &&
                 !comisionDep.getEstado().equals(EComisionEstado.CANCELADA.toString()) &&
                 !comisionDep.getEstado().equals(EComisionEstado.FINALIZADA.toString())) {
                this.comisionTramiteSeleccionado = comisionDep;
                this.comisionTramiteSeleccionado = this.getTramiteService().buscarComisionPorId(
                    comisionDep.getId());
                this.documentoComisionTramiteSeleccionado = this.getTramiteService().
                    buscarDocumentoPorId(comisionDep.getMemorandoDocumentoId());
                this.reporteComision = this.reportsService.consultarReporteDeGestorDocumental(
                    this.documentoComisionTramiteSeleccionado.getIdRepositorioDocumentos());

                List<ComisionTramite> listaComisionTramite = this.getTramiteService().
                    buscarComisionesTramitePorComisionId(this.comisionTramiteSeleccionado.getId());

                if (listaComisionTramite.size() > 1) {
                    this.habilitarTiempoProgramado = true;
                } else {
                    this.tiempoProgramado = this.comisionTramiteSeleccionado.getDuracion().
                        toString();
                    this.habilitarTiempoProgramado = false;
                }
            }
        }
        // Set de las inconsistencias geográficas del trámite.
        this.inconsistenciasTramite = new ArrayList<TramiteInconsistencia>();
        for (TramiteInconsistencia ti : this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramiteSeleccionado.getId())) {
            if (!ti.getDepurada().equals("CC")) {
                this.inconsistenciasTramite.add(ti);
            }
        }

    }

    //--------------------------------------------------------------------------------------------------        
    /**
     * Método para cargar los datos relacionados al tramite que se deben mostrar.
     *
     * @return
     */
    private void cargarDatosTramite() {
        LOGGER.debug("Cargando datos tramite...");

        this.tramiteSeleccionado = this.getTramiteService().
            buscarTramiteTraerDocumentoResolucionPorIdTramite(this.tramiteSeleccionado.getId());

        this.prediosTramite = this.tramiteSeleccionado.getPredios();

        // Set de las inconsistencias geográficas del trámite.
        this.inconsistenciasTramite = new ArrayList<TramiteInconsistencia>();
        for (TramiteInconsistencia ti : this.getTramiteService().
            buscarTramiteInconsistenciaPorTramiteId(this.tramiteSeleccionado.getId())) {
            if (!ti.getDepurada().equals("CC")) {
                this.inconsistenciasTramite.add(ti);
            }
        }

        this.parametroEditorGeografico = this.currentUser.getLogin() + "," +
            this.tramiteSeleccionado.getId() +
            "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class,
                this.currentProcessActivity.getNombre());

        LOGGER.debug("Fin carga datos...");
    }

    //--------------------------------------------------------------------------------------------------        
    /**
     * Método Encargado de generar el documento resultado de la visita
     *
     * @return
     */
    public void generarDocumento() {
        if (this.validarCampos()) {

            EReporteServiceSNC enumeracionReporte =
                EReporteServiceSNC.INFORME_VISITA_TERRENO_CON_RESULTADO_LEVANTAMIENTO;

            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID", String.valueOf(this.tramiteSeleccionado.getId()));
            if (this.tiempoProgramado != null) {
                parameters.put("TIEMPO_PROGRAMADO", String.valueOf(this.tiempoProgramado));
            }
            if (this.tiempoUtilizado != null) {
                parameters.put("TIEMPO_UTILIZADO", String.valueOf(this.tiempoUtilizado));
            }
            if (this.motivo != null) {
                parameters.put("MOTIVO_TIEMPO_ADICIONAL", String.valueOf(this.motivo));
            }

            String trabProgramado = "";
            for (TramiteInconsistencia ti : this.inconsistenciasTramite) {
                trabProgramado = trabProgramado.concat(ti.getInconsistencia()) + "<br/>";
            }
            if (!trabProgramado.isEmpty()) {
                parameters.put("TRABAJO_PROGRAMADO", String.valueOf(trabProgramado));
            }

            String trabRealizado = "";
            for (TramiteInconsistencia ti : this.trabajoProgramadoSeleccionado) {
                trabRealizado = trabRealizado.concat(ti.getInconsistencia()) + "<br/>";
            }
            if (!trabProgramado.isEmpty()) {
                parameters.put("TRABAJO_REALIZADO", String.valueOf(trabRealizado));
            }
            if (this.tareasAdicionalesRealizadas != null) {
                parameters.put("TAREAS_ADICIONALES", String.
                    valueOf(this.tareasAdicionalesRealizadas));
            }
            if (this.inconvenientesAdministrativos != null) {
                parameters.put("INCONVENIENTES_ADMINISTRATIVOS", String.valueOf(
                    this.inconvenientesAdministrativos));
            }
            if (this.actividadesDesarrolladas != null) {
                parameters.put("ACTIVIDADES_DESARROLLADOS", String.valueOf(
                    this.actividadesDesarrolladas));
            }

            if (this.inconvenientesTipo1Texto != null) {
                for (String s : this.inconvenientesTipo1) {
                    if (s.equals("De tipo social")) {
                        parameters.put("INCONVENIENTE_FACTOR_HUMANO", String.valueOf(
                            this.inconvenientesTipo1Texto));
                    } else if (s.equals("De tipo técnico")) {
                        parameters.put("INCONVENIENTE_TIPO_TECNICO", String.valueOf(
                            this.inconvenientesTipo1Texto));
                    }
                }
            }

            if (this.inconvenientesTipo2Texto != null) {
                for (String s : this.inconvenientesTipo2) {
                    if (s.equals("De tipo logístico")) {
                        parameters.put("INCONVENIENTE_TIPO_LOGISTICO", String.valueOf(
                            this.inconvenientesTipo2Texto));
                    } else if (s.equals("Factor humano")) {
                        parameters.put("INCONVENIENTE_TIPO_SOCIAL", String.valueOf(
                            this.inconvenientesTipo2Texto));
                    }
                }
            }

            this.informeVisita = reportsService.generarReporte(parameters,
                enumeracionReporte, this.currentUser);
            if (this.informeVisita != null) {
                this.habilitarBotonGuardarDoc = true;
            }
        } else {
            this.habilitarBotonGuardarDoc = false;
            return;
        }

    }

    public void radicar() {

    }

    //-------------------------------------------------------------------------------------------------- 
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en Alfresco y
     * en la base de datos mediante el llamado al método guardar documento de la interfaz de
     * ITramite. Posteriormente le asocia el documento al trámite documento creado al principio.
     *
     * @throws Exception
     */
    public void guardarDocumento() throws Exception {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Documento documento = new Documento();

        tramiteDocumento.setTramite(this.tramiteSeleccionado);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.currentUser.getLogin());

        try {

            this.generarDocumento();

            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.OTRO
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.informeVisita != null) {
                documento.setArchivo(this.informeVisita.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(this.tramiteSeleccionado.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.currentUser.getLogin());
            documento.setUsuarioLog(this.currentUser.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.currentUser.
                getCodigoEstructuraOrganizacional());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.currentUser, documento);

            if (documento != null &&
                 documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos()
                    .trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                tramiteDocumento = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumento);

                this.habilitarAvanzarProceso = true;
            }

        } catch (Exception e) {
            throw new Exception("Error al guardar el oficio cancelacion en alfresco: " + e.
                getMessage());
        }

    }

    //--------------------------------------------------------------------------------------------------        
    /**
     * Método Encargado de hacer la validación de los campos del formulario
     *
     * @return
     */
    private boolean validarCampos() {
        boolean valido = true;
        if (this.tiempoProgramado.isEmpty() || this.tiempoUtilizado.isEmpty()) {
            this.addMensajeError("Debe registrar el tiempo programado y el tiempo registrado");
            valido = false;
        }

        if (this.habilitarMotivo) {
            if (this.motivo.isEmpty()) {
                this.addMensajeError("Debe registrar el motivo");
                valido = false;
            }
        }

        if (!this.inconvenientesTipo1.isEmpty() && this.inconvenientesTipo1Texto.isEmpty()) {
            this.addMensajeError("Debe registrar la información de los ítems seleccionados");
            valido = false;
        }

        if (!this.inconvenientesTipo2.isEmpty() && this.inconvenientesTipo2Texto.isEmpty()) {
            this.addMensajeError("Debe registrar la información de los ítems seleccionados");
            valido = false;
        }

        if (this.tareasAdicionales.equals(ESiNo.SI.toString()) && this.tareasAdicionalesRealizadas.
            isEmpty()) {
            this.addMensajeError("Debe registrar las tareas adicionales realizadas");
            valido = false;
        }
        return valido;
    }

    //--------------------------------------------------------------------------------------------------        
    /*
     * Listeners para habilitar los diferentes editores de la pagina
     */
    public void tareasAdicionalesListener() {
        if (this.tareasAdicionales.equals(ESiNo.SI.toString())) {
            this.habilitarTareasAdicionalesTexto = true;
        } else {
            this.habilitarTareasAdicionalesTexto = false;
            this.tareasAdicionalesRealizadas = "";
        }
    }

    public void inconvenientesTipo1Listener() {
        if (!this.inconvenientesTipo1.isEmpty()) {
            this.habilitarInconvenientesTipo1Texto = true;
        } else {
            this.habilitarInconvenientesTipo1Texto = false;
        }
    }

    public void inconvenientesTipo2Listener() {
        if (!this.inconvenientesTipo2.isEmpty()) {
            this.habilitarInconvenientesTipo2Texto = true;
        } else {
            this.habilitarInconvenientesTipo2Texto = false;
        }
    }

    //--------------------------------------------------------------------------------------------------        
    /**
     * Método para retornar al arbol de tareas, action del botón "cerrar" que forza el init del MB.
     *
     * @return
     */
    public String cerrar() {
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb
            .getManagedBean("tareasPendientes");
        tareasPendientesMB.cerrar();
        return ConstantesNavegacionWeb.INDEX;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que avanza el proceso.
     *
     * @return
     */
    public String avanzarProceso() {

        this.forwardThisProcess();

        UtilidadesWeb.removerManagedBean("realizarInfVisTer");
        this.tareasPendientesMB.init();
        return "index";
    }

    private void forwardThisProcess() {
        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();
        }
        super.forwardProcess();
    }

    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }

    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones, processTransition;
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();

        processTransition =
            ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_TOPOGRAFIA;
        usuariosActividad.add(this.currentUser);

        //this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(this.currentProcessActivity, this.currentUser);
        solicitudCatastral = new SolicitudCatastral();
        messageDTO = new ActivityMessageDTO();

        messageDTO.setActivityId(this.currentProcessActivity.getId());
        observaciones = "El trámite sale de la actividad informe visita terreno.";
        messageDTO.setComment(observaciones);
        messageDTO.setUsuarioActual(this.currentUser);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setUsuarios(usuariosActividad);

        messageDTO.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(messageDTO);

    }

    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

    }

    /**
     * Metodo para habilitar o deshabilitar el campo motivo segun sea el caso
     *
     * @author felipe.cadena
     */
    public void updateMotivo() {
        this.habilitarMotivo = Double.valueOf(this.tiempoProgramado) < Double.valueOf(
            this.tiempoUtilizado);

        if (!habilitarMotivo) {
            this.motivo = "";
        }
    }

    //end of class
}
