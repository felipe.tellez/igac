package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EArchivoAnexoFormato;
import co.gov.igac.snc.persistence.util.EDocumentoArchivoAnexoEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author
 *
 *
 * XXX: lorena.salamanca:: Este MB fue tomado de BloqueoPredioMB suprimiendo lo que no se usaba,
 * dejando unicamente lo necesario para que el xhtml se muestre correctamente. Sin embargo, deje de
 * una vez los metodos usados para subir los archivos. Revisar. 26-03-2012
 * @modified javier.barajas ajuste y correcion de metodos para trabajar bajo el proceso de
 * actualizacion
 * @cu 213 Bloqueo Predio de Zonas Especiales para actualizacion
 *
 */
@Component("bloquearPrediosParaZonasEnCondicionesEspeciales")
@Scope("session")
public class BloqueoPrediosParaZonasEnCondicionesEspecialesMB extends
    SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(BloqueoPrediosParaZonasEnCondicionesEspecialesMB.class);
    /**
     * interfaces de servicio
     */

    @Autowired
    private IContextListener contexto;

    @Autowired
    private GeneralMB generalService;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;

    /**
     * Orden apra la lista de Municipios
     */
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * Codigo del departamento
     */
    private String departamentoCodigo;

    /**
     * Codigo del municipio
     */
    private String municipioCodigo;

    /**
     * datos de la busqueda
     */
    private Departamento departamento;
    private Municipio municipio;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamentoCod;
    private String selectedMunicipioCod;

    /**
     * Codigo de sector en predio
     */
    private String sectorCodigo;

    /**
     * Documento archivo anexo soporte del bloqueo masivo
     */
    private DocumentoArchivoAnexo documentoArchivoAnexo;

    /**
     * Ruta y tipo del documento resultado del bloqueo masivo
     */
    private String documentoResultadoBloqueoMasivo;
    private String doctypeDocumentoResultado;

    /**
     * Tipo de avaluo en predio
     */
    private String tipoAvaluo;

    /**
     * Lista que contiene los predios seleccionados para el bloqueo
     */
    private List<Predio> prediosSeleccionados;

    /**
     * Lista que contiene los predios seleccionados de la lista para bloquear
     */
    private Predio[] prediosSelectTipo;

    /**
     * Lista que contiene los predios seleccionados de la lista para bloquear y seleccione uno
     */
    private Predio prediosSelectTipo1;

    /**
     * variable donde se cargan los resultados paginados de la consulta de predios bloqueados
     */
    private LazyDataModel<PredioBloqueo> lazyAssignedBloqueos;

    /**
     * lista de PredioBloqueo paginados
     */
    private List<PredioBloqueo> rows;

    /**
     * filtro de datos para la consulta de predios bloqueados
     */
    private FiltroDatosConsultaPrediosBloqueo filtroCPredioB;

    /**
     * filtro de datos para la consulta de predios bloqueados por rango
     */
    private FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB;

    /**
     * Bandera de visualización de la tabla de resultados
     */
    private boolean banderaBuscar;

    /**
     * Almacena el valor del radio button para el tipo de bloqueo a realizar
     */
    private int tipoBloqueo;

    /**
     * Visualización bloqueo individual
     */
    private boolean tipoBloqueo0;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteBloqueo;

    /**
     * Mensaje del boton que permite cargar archivos
     */
    private String mensajeCargaArchivo;

    /**
     * Visualización bloqueo por rangos
     */
    private boolean tipoBloqueo1;

    /**
     * Visualización bloqueo masivo
     */
    private boolean tipoBloqueo2;

    /**
     * Mensaje de salida del bloqueo de persona
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * indican si los combos están ordenados por nombre (si no, lo están por código)
     */
    private boolean municipiosOrderedByNombre;
    private boolean departamentosOrderedByNombre;

    /**
     * Condición de propiedad para el predio a bloquear
     */
    private String condicionPropiedad;

    /**
     * Código del predio a bloquear
     */
    private String predioB;

    /**
     * Manzana o vereda del predio a bloquear
     */
    private String manzanaVereda;

    /**
     * Código del sector del prédio a bloquear
     */
    private String sectorCodigoB;

    /**
     * Fecha inicial del bloqueo de predios por rango
     */
    private Date fechaInicial;

    /**
     * Fecha final del bloqueo de predios por rango
     */
    private Date fechaFinal;

    /**
     * Fecha de recibido el bloqueo
     */
    private Date fechaRecibido;

    /**
     * Motivo del bloqueo de el/los predios
     */
    private String motivoBloqueo;

    /**
     * Almacena el tipo de archivo valido dependiendo de la opcion de carga
     */
    private String tiposDeArchivoValidos;

    /**
     * Visualizacion del boton de carga de archivos
     */
    private boolean tipoBloqueoCargar;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Lista que contiene los predios bloqueados o el rango de predios bloqueados
     */
    private List<PredioBloqueo> prediosBloqueados;

    /**
     * PredioBloqueo que delimitan el rango de inserción
     */
    private PredioBloqueo predioBloqueo;

    /**
     * Predio final del rango de bloqueo
     */
    private Predio predioFinal;

    /**
     * Generador aleatorio para nombres de archivos
     */
    private Random generadorNumeroAleatorioNombreArchivo;

    /**
     * Ruta del archivo cargado
     */
    private String rutaMostrar;

    /**
     * Archivo resultado cargado
     */
    private File archivoResultado;
    private File archivoMasivoResultado;

    /**
     * Banderas de visualización de los botones guardar y generar informe
     */
    private boolean banderaBotonGuardar;

    /**
     * Banderas para mostrar en la vista
     */
    private boolean banderaBotonGenerarInforme;
    private boolean banderaValidar;
    private boolean laBandera;
    private boolean banderaBotonRetirar;
    private boolean banderaVariosPredios;
    private boolean banderaSeleccionVisor;

    /**
     * Descripción del documento de soporte cargado
     */
    private String descripcionArchivo;

    /**
     * Control de carga de archivos de soporte
     */
    private int aSoporte;
    private int aBMasa;

    /**
     * Lista de predios bloqueo cargados desde un archivo de bloqueo masivo
     */
    private ArrayList<PredioBloqueo> bloqueoMasivo;

    /**
     * Variable que guarda la url del reporte de bloqueos masivo de predios
     *
     * @return
     */
    private String urlArchivoBloqueoMasivoPredios;

    /**
     * Nombre del archivo de soporte del bloqueo masivo
     */
    private String nombreDocumentoBloqueoMasivo;

    /**
     * Variables para manejar el visor Geoggrafico
     */
    private String valorSeleccionActualizacion;

    private String seleccionVisor;

    private int numeroManzanasSeleccionadas;

    private int numeroPrediosSeleccionados;

    private int numeroBarriosMunicipio;

    private String seleccionBarrio;

    private ArrayList<SelectItem> selectListaBarrios;

    private boolean rederedConsultaVisor;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * En esta variable se van a enviar los numeros prediales de los predios incluidos en la
     * actualizacion
     */
    private String codigoPrediosParaVisor;

    private String codigoMunicipio;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 24/10/12

        // Obtener la actualizacióin del arbol de tareas.
        this.setActualizacionSeleccionada(this.getActualizacionService()
            .recuperarActualizacionPorId(450L));

        this.tipoBloqueoCargar = true;
        this.banderaBuscar = false;
        this.tipoBloqueo = 0;
        this.tipoBloqueo0 = true;
        this.tipoBloqueo1 = false;
        this.tipoBloqueo2 = false;
        this.banderaSeleccionVisor = false;
        this.banderaBotonGenerarInforme = false;
        this.banderaBotonGuardar = false;
        this.banderaBotonRetirar = true;
        this.banderaValidar = false;
        this.rutaMostrar = null;
        this.mensajeCargaArchivo = "Cargar documento";
        this.prediosBloqueados = new ArrayList<PredioBloqueo>();
        this.predioBloqueo = new PredioBloqueo();
        this.predioFinal = new Predio();
        this.aSoporte = 0;
        this.aBMasa = 0;
        this.laBandera = false;
        this.fechaRecibido = generalService.getFechaActual();
        this.ordenDepartamentos = EOrden.NOMBRE;
        this.departamentosOrderedByNombre = true;
        this.ordenMunicipios = EOrden.NOMBRE;
        this.municipiosOrderedByNombre = true;

        this.departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorPais(
                Constantes.COLOMBIA);
        updateMunicipiosItemList();

        this.filtroCPredioB = new FiltroDatosConsultaPrediosBloqueo();
        this.filtroFinalCPredioB = new FiltroDatosConsultaPrediosBloqueo();
        this.urlArchivoBloqueoMasivoPredios = new String();
        this.applicationClientName = this.contexto.getClientAppNameUrl();
        this.codigoPrediosParaVisor =
            "08001010100000001,08001010100000002,08001010200000175,08001010200000123";
        this.moduleLayer = EVisorGISLayer.ACTUALIZACION.getCodigo();
        this.moduleTools = EVisorGISTools.ACTUALIZACION.getCodigo();
        this.valorSeleccionActualizacion = "seleccionarPrediosActualizacion";
        this.seleccionVisor = new String();
        this.selectListaBarrios = new ArrayList<SelectItem>();
        this.cargarBarriosVisor();

        this.mensajeSalidaConfirmationDialog = "El bloqueo" +
             " se realizó exitosamente." + " Desea continuar" +
             " en el modulo de desbloqueo de predios?";

    }
    // ---------------------------------------------------------------------

    /*
     *
     * Getters y Setters
     *
     */

    public String getSelectedMunicipioCod() {
        return this.selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String codigo) {
        this.selectedMunicipioCod = codigo;

        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.municipio = municipioSelected;
    }

    public Predio[] getPrediosSelectTipo() {
        return prediosSelectTipo;
    }

    public void setPrediosSelectTipo(Predio[] prediosSelectTipo) {
        this.prediosSelectTipo = prediosSelectTipo;
    }

    public Predio getPrediosSelectTipo1() {
        return prediosSelectTipo1;
    }

    public void setPrediosSelectTipo1(Predio prediosSelectTipo1) {

        this.prediosSelectTipo1 = prediosSelectTipo1;

    }

    public boolean isBanderaBotonRetirar() {
        return banderaBotonRetirar;
    }

    public void setBanderaBotonRetirar(boolean banderaBotonRetirar) {
        this.banderaBotonRetirar = banderaBotonRetirar;
    }

    public Documento getDocumentoSoporteBloqueo() {
        return documentoSoporteBloqueo;
    }

    public void setDocumentoSoporteBloqueo(Documento documentoSoporteBloqueo) {
        this.documentoSoporteBloqueo = documentoSoporteBloqueo;
    }

    public String getUrlArchivoBloqueoMasivoPredios() {
        return urlArchivoBloqueoMasivoPredios;
    }

    public void setUrlArchivoBloqueoMasivoPredios(
        String urlArchivoBloqueoMasivoPredios) {
        this.urlArchivoBloqueoMasivoPredios = urlArchivoBloqueoMasivoPredios;
    }

    public String getNombreDocumentoBloqueoMasivo() {
        return nombreDocumentoBloqueoMasivo;
    }

    public void setNombreDocumentoBloqueoMasivo(String nombreDocumentoBloqueoMasivo) {
        this.nombreDocumentoBloqueoMasivo = nombreDocumentoBloqueoMasivo;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public boolean isBanderaVariosPredios() {
        return banderaVariosPredios;
    }

    public void setBanderaVariosPredios(boolean banderaVariosPredios) {
        this.banderaVariosPredios = banderaVariosPredios;
    }

    public boolean isBanderaSeleccionVisor() {
        return banderaSeleccionVisor;
    }

    public void setBanderaSeleccionVisor(boolean banderaSeleccionVisor) {
        this.banderaSeleccionVisor = banderaSeleccionVisor;
    }

    public void setActualizacionSeleccionada(Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public EOrden getOrdenDepartamentos() {
        return ordenDepartamentos;
    }

    public boolean isLaBandera() {
        return laBandera;
    }

    public String getValorSeleccionActualizacion() {
        return valorSeleccionActualizacion;
    }

    public void setValorSeleccionActualizacion(String valorSeleccionActualizacion) {
        this.valorSeleccionActualizacion = valorSeleccionActualizacion;
    }

    public String getSeleccionVisor() {
        return seleccionVisor;
    }

    public void setSeleccionVisor(String seleccionVisor) {
        this.seleccionVisor = seleccionVisor;
    }

    public int getNumeroManzanasSeleccionadas() {
        return numeroManzanasSeleccionadas;
    }

    public void setNumeroManzanasSeleccionadas(int numeroManzanasSeleccionadas) {
        this.numeroManzanasSeleccionadas = numeroManzanasSeleccionadas;
    }

    public int getNumeroPrediosSeleccionados() {
        return numeroPrediosSeleccionados;
    }

    public void setNumeroPrediosSeleccionados(int numeroPrediosSeleccionados) {
        this.numeroPrediosSeleccionados = numeroPrediosSeleccionados;
    }

    public int getNumeroBarriosMunicipio() {
        return numeroBarriosMunicipio;
    }

    public void setNumeroBarriosMunicipio(int numeroBarriosMunicipio) {
        this.numeroBarriosMunicipio = numeroBarriosMunicipio;
    }

    public String getSeleccionBarrio() {
        return seleccionBarrio;
    }

    public void setSeleccionBarrio(String seleccionBarrio) {
        this.seleccionBarrio = seleccionBarrio;
    }

    public ArrayList<SelectItem> getSelectListaBarrios() {
        return selectListaBarrios;
    }

    public void setSelectListaBarrios(ArrayList<SelectItem> selectListaBarrios) {
        this.selectListaBarrios = selectListaBarrios;
    }

    public boolean isRederedConsultaVisor() {
        return rederedConsultaVisor;
    }

    public void setRederedConsultaVisor(boolean rederedConsultaVisor) {
        this.rederedConsultaVisor = rederedConsultaVisor;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getCodigoPrediosParaVisor() {
        return codigoPrediosParaVisor;
    }

    public void setCodigoPrediosParaVisor(String codigoPrediosParaVisor) {
        this.codigoPrediosParaVisor = codigoPrediosParaVisor;
    }

    public void setLaBandera(boolean laBandera) {
        this.laBandera = laBandera;
    }

    public boolean isBanderaValidar() {
        return banderaValidar;
    }

    public void setBanderaValidar(boolean banderaValidar) {
        this.banderaValidar = banderaValidar;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public String getDocumentoResultadoBloqueoMasivo() {
        return documentoResultadoBloqueoMasivo;
    }

    public void setDocumentoResultadoBloqueoMasivo(
        String documentoResultadoBloqueoMasivo) {
        this.documentoResultadoBloqueoMasivo = documentoResultadoBloqueoMasivo;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return mensajeSalidaConfirmationDialog;
    }

    public String getDoctypeDocumentoResultado() {
        return doctypeDocumentoResultado;
    }

    public void setDoctypeDocumentoResultado(String doctypeDocumentoResultado) {
        this.doctypeDocumentoResultado = doctypeDocumentoResultado;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public FiltroDatosConsultaPrediosBloqueo getFiltroFinalCPredioB() {
        return filtroFinalCPredioB;
    }

    public void setFiltroFinalCPredioB(
        FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB) {
        this.filtroFinalCPredioB = filtroFinalCPredioB;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(
        boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public boolean isBanderaBotonGenerarInforme() {
        return banderaBotonGenerarInforme;
    }

    public void setBanderaBotonGenerarInforme(boolean banderaBotonGenerarInforme) {
        this.banderaBotonGenerarInforme = banderaBotonGenerarInforme;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public Random getGeneradorNumeroAleatorioNombreArchivo() {
        return generadorNumeroAleatorioNombreArchivo;
    }

    public void setGeneradorNumeroAleatorioNombreArchivo(
        Random generadorNumeroAleatorioNombreArchivo) {
        this.generadorNumeroAleatorioNombreArchivo = generadorNumeroAleatorioNombreArchivo;
    }

    public Predio getPredioFinal() {
        return predioFinal;
    }

    public void setPredioFinal(Predio predioFinal) {
        this.predioFinal = predioFinal;
    }

    public PredioBloqueo getPredioBloqueo() {
        return predioBloqueo;
    }

    public void setPredioBloqueo(PredioBloqueo predioBloqueo) {
        this.predioBloqueo = predioBloqueo;
    }

    public List<PredioBloqueo> getPrediosBloqueados() {
        return prediosBloqueados;
    }

    public void setPrediosBloqueados(List<PredioBloqueo> prediosBloqueados) {
        this.prediosBloqueados = prediosBloqueados;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isTipoBloqueoCargar() {
        return tipoBloqueoCargar;
    }

    public void setTipoBloqueoCargar(boolean tipoBloqueoCargar) {
        this.tipoBloqueoCargar = tipoBloqueoCargar;
    }

    public String getTiposDeArchivoValidos() {
        return tiposDeArchivoValidos;
    }

    public void setTiposDeArchivoValidos(String tiposDeArchivoValidos) {
        this.tiposDeArchivoValidos = tiposDeArchivoValidos;
    }

    public String getMensajeCargaArchivo() {
        return mensajeCargaArchivo;
    }

    public void setMensajeCargaArchivo(String mensajeCargaArchivo) {
        this.mensajeCargaArchivo = mensajeCargaArchivo;
    }

    public String getMotivoBloqueo() {
        return motivoBloqueo;
    }

    public void setMotivoBloqueo(String motivoBloqueo) {
        this.motivoBloqueo = motivoBloqueo;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Date getFechaRecibido() {
        return fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    public boolean isTipoBloqueo0() {
        return tipoBloqueo0;
    }

    public void setTipoBloqueo0(boolean tipoBloqueo0) {
        this.tipoBloqueo0 = tipoBloqueo0;
    }

    public boolean isTipoBloqueo1() {
        return tipoBloqueo1;
    }

    public void setTipoBloqueo1(boolean tipoBloqueo1) {
        this.tipoBloqueo1 = tipoBloqueo1;
    }

    public boolean isTipoBloqueo2() {
        return tipoBloqueo2;
    }

    public void setTipoBloqueo2(boolean tipoBloqueo2) {
        this.tipoBloqueo2 = tipoBloqueo2;
    }

    public String getSectorCodigoB() {
        return sectorCodigoB;
    }

    public void setSectorCodigoB(String sectorCodigoB) {
        this.sectorCodigoB = sectorCodigoB;
    }

    public String getCondicionPropiedad() {
        return condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public String getPredioB() {
        return predioB;
    }

    public void setPredioB(String predioB) {
        this.predioB = predioB;
    }

    public String getManzanaVereda() {
        return manzanaVereda;
    }

    public void setManzanaVereda(String manzanaVereda) {
        this.manzanaVereda = manzanaVereda;
    }

    public int getTipoBloqueo() {
        return tipoBloqueo;
    }

    public void setTipoBloqueo(int tipoBloqueo) {
        this.tipoBloqueo = tipoBloqueo;
    }

    public FiltroDatosConsultaPrediosBloqueo getFiltroCPredioB() {
        return filtroCPredioB;
    }

    public void setFiltroCPredioB(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB) {
        this.filtroCPredioB = filtroCPredioB;
    }

    public LazyDataModel<PredioBloqueo> getLazyAssignedBloqueos() {

        return lazyAssignedBloqueos;

    }

    public void setLazyAssignedBloqueos(
        LazyDataModel<PredioBloqueo> lazyAssignedBloqueos) {
        this.lazyAssignedBloqueos = lazyAssignedBloqueos;
    }

    public List<PredioBloqueo> getRows() {
        return rows;
    }

    public void setRows(List<PredioBloqueo> rows) {
        this.rows = rows;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public String getSectorCodigo() {
        return sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public boolean isBanderaBuscar() {
        return banderaBuscar;
    }

    public void setBanderaBuscar(boolean banderaBuscar) {
        this.banderaBuscar = banderaBuscar;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public List<Predio> getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(List<Predio> prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    // --------------------------------------------------------------------------------
    public String getSelectedDepartamentoCod() {
        return this.selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String codigo) {

        this.selectedDepartamentoCod = codigo;
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento departamento : this.departamentos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        this.departamento = dptoSeleccionado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de soporte a cargar
     */
    private void nuevoDocumento() {

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento
            .setId(ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PREDIO.getId());

        this.documentoSoporteBloqueo.setFechaDocumento(new Date());
        this.documentoSoporteBloqueo.setEstado(EDocumentoEstado.RECIBIDO
            .getCodigo());
        this.documentoSoporteBloqueo.setBloqueado(ESiNo.NO.getCodigo());

        this.documentoSoporteBloqueo.setTipoDocumento(tipoDocumento);
        this.documentoSoporteBloqueo.setUsuarioCreador(this.usuario.getLogin());
        this.documentoSoporteBloqueo.setUsuarioLog(this.usuario.getLogin());
        this.documentoSoporteBloqueo.setFechaLog(new Date());

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(), eventoCarga.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            nuevoDocumento();
            this.documentoSoporteBloqueo.setArchivo(eventoCarga.getFile()
                .getFileName());

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;

            String mensaje = "El documento se cargó adecuadamente.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            e.printStackTrace();
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo para recueprar la dirección de la imagen
     */
    public String getUrlBase() {
        return FacesContext.getCurrentInstance().getExternalContext()
            .getRealPath("/");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivo de bloqueo en masa
     */
    public void archivoVM(FileUploadEvent eventoMasa) {

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoMasivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(), eventoMasa.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoMasivoResultado);

            byte[] buffer = new byte[Math.round(eventoMasa.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoMasa.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;

            lecturaArchivoMasivo(eventoMasa);

        } catch (IOException e) {
            e.printStackTrace();
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }

    }

    /**
     * Método de lectura del archivo de bloqueo masivo
     */
    @SuppressWarnings("rawtypes")
    private void lecturaArchivoMasivo(FileUploadEvent eventoMasa) {

        String sCadena;
        String partes[] = null;
        String separador = ";";
        this.bloqueoMasivo = new ArrayList<PredioBloqueo>();
        Predio predioTemp;
        PredioBloqueo pb;
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaTemp;
        boolean formatoCorrecto;

        try {

            if (eventoMasa.getFile().getContentType()
                .equals(EArchivoAnexoFormato.ARCHIVO_PLANO.getValor()) ||
                 eventoMasa
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_PLANO
                            .getExtension())) {
                BufferedReader bf = new BufferedReader(new FileReader(
                    this.archivoMasivoResultado));
                separador = ";";

                while ((sCadena = bf.readLine()) != null) {
                    partes = sCadena.split(separador);

                    if (partes.length > 5 || partes.length < 4) {
                        throw new IOException();
                    }
                    formatoCorrecto = validarEstructuraArchivoBloqueoMasivo(partes);

                    if (formatoCorrecto == true) {

                        partes = sCadena.split(separador);

                        pb = new PredioBloqueo();
                        predioTemp = new Predio();
                        predioTemp.setNumeroPredial(partes[0]);
                        pb.setPredio(predioTemp);
                        pb.setMotivoBloqueo(partes[1]);
                        try {
                            fechaTemp = formatoFecha.parse(partes[2]);
                            pb.setFechaInicioBloqueo(fechaTemp);
                            fechaTemp = formatoFecha.parse(partes[3]);
                            pb.setFechaRecibido(fechaTemp);
                            if (partes.length == 5) {
                                fechaTemp = formatoFecha.parse(partes[4]);
                                pb.setFechaTerminaBloqueo(fechaTemp);
                            }
                        } catch (ParseException e) {
                            String mensaje =
                                "Las fechas ingresadas no contienen el formato dd/MM/yyyy";
                            this.addMensajeError(mensaje);
                            throw new IOException();
                        }

                        nuevoDocumentoAnexo();
                        this.documentoArchivoAnexo
                            .setFormato(EArchivoAnexoFormato.ARCHIVO_PLANO
                                .getCodigo());
                        this.documentoArchivoAnexo.setArchivo(eventoMasa
                            .getFile().getFileName());

                        this.bloqueoMasivo.add(pb);
                        this.aBMasa = 1;
                    }
                }
                String mensaje = "El documento de bloqueo masivo se cargó adecuadamente.";
                this.addMensajeInfo(mensaje);
            } else if (eventoMasa.getFile().getContentType()
                .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                 eventoMasa
                    .getFile()
                    .getContentType()
                    .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                        .getValor()) ||
                 eventoMasa
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                            .getExtension()) ||
                 eventoMasa
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                            .getExtension())) {

                FileInputStream fis = new FileInputStream(
                    this.archivoMasivoResultado);
                XSSFWorkbook workbook = new XSSFWorkbook(fis);
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rowsX = sheet.rowIterator();

                while (rowsX.hasNext()) {
                    XSSFRow row = (XSSFRow) rowsX.next();
                    Iterator cells = row.cellIterator();
                    int c = 0;
                    partes = new String[12];
                    while (cells.hasNext()) {
                        XSSFCell cell = (XSSFCell) cells.next();
                        partes[c] = cell.toString();
                        c++;
                    }

                    formatoCorrecto = validarEstructuraArchivoBloqueoMasivo(partes);

                    if (formatoCorrecto == true) {

                        for (String p : partes) {
                            if (p == null) {
                                p = "";
                            }
                        }

                        c = 0;

                        pb = new PredioBloqueo();
                        predioTemp = new Predio();

                        predioTemp.setNumeroPredial(partes[0]);

                        pb.setPredio(predioTemp);
                        pb.setMotivoBloqueo(partes[1]);

                        try {
                            fechaTemp = formatoFecha.parse(partes[2]);
                            pb.setFechaInicioBloqueo(fechaTemp);
                            fechaTemp = formatoFecha.parse(partes[3]);
                            pb.setFechaRecibido(fechaTemp);
                            if (partes.length == 5 && !partes[4].equals("")) {
                                fechaTemp = formatoFecha.parse(partes[4]);
                                pb.setFechaTerminaBloqueo(fechaTemp);
                            }
                        } catch (ParseException e) {
                            String mensaje =
                                "Las fechas ingresadas no contienen el formato dd/MM/yyyy";
                            this.addMensajeError(mensaje);
                            throw new IOException();
                        }

                        nuevoDocumentoAnexo();
                        this.documentoArchivoAnexo.setFormato(eventoMasa
                            .getFile().getContentType());
                        if (eventoMasa
                            .getFile()
                            .getContentType()
                            .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                .getValor()) ||
                             eventoMasa
                                .getFile()
                                .getFileName()
                                .endsWith(
                                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                        .getExtension())) {
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                    .getCodigo());
                        } else {
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                                    .getCodigo());
                        }
                        this.documentoArchivoAnexo.setArchivo(eventoMasa
                            .getFile().getFileName());

                        this.bloqueoMasivo.add(pb);
                        this.aBMasa = 1;
                    } else {
                        this.aBMasa = 0;
                        String mensaje = "El archivo no contiene la estructura adecuada." +
                             " Por favor corrija los datos e intente nuevamente";
                        this.addMensajeError(mensaje);
                        break;
                    }
                }
                String mensaje = "El documento de bloqueo masivo se cargó adecuadamente.";
                this.addMensajeInfo(mensaje);
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            String mensaje = "El archivo no contiene la estructura adecuada!";
            this.addMensajeError(mensaje);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para validar la estructura basica del archivo de bloqueo masivo
     */
    private boolean validarEstructuraArchivoBloqueoMasivo(String[] partes) {

        if (partes[0] != null && partes[0].length() == 30) {
            return true;
        }

        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de bloqueo masivo a cargar
     */
    private void nuevoDocumentoAnexo() {

        TipoDocumento tipoDocumento = new TipoDocumento();
        this.documentoArchivoAnexo = new DocumentoArchivoAnexo();

        tipoDocumento
            .setId(ETipoDocumentoId.DOC_BLOQUEO_MASIVO_PERSONA.getId());

        this.documentoArchivoAnexo
            .setDescripcion(ETipoDocumentoId.DOC_BLOQUEO_MASIVO_PREDIO
                .toString());
        this.documentoArchivoAnexo.setDocumento(this.documentoSoporteBloqueo);
        this.documentoArchivoAnexo
            .setEstado(EDocumentoArchivoAnexoEstado.CARGADO.getCodigo());
        this.documentoArchivoAnexo.setFechaLog(new Date());
        this.documentoArchivoAnexo.setUsuarioLog(this.usuario.getLogin());

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado para validar los datos de la forma
     */
    public void adicionar() {

        this.banderaValidar = true;
        validar();

    }

    // ---------------------------------------------------------------
    /**
     * metodo utilizado para validar los datos de la forma
     */
    public void validar() {

        boolean validacionFiltroRangoPredios;
        Date fechaActualSinMills = DateUtils.round(
            generalService.getFechaActual(), Calendar.DAY_OF_MONTH);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(fechaActualSinMills);
        gc.add(Calendar.DAY_OF_YEAR, -1);
        fechaActualSinMills = gc.getTime();
        //this.laBandera = true;

        if (this.tipoBloqueo0 == true) {
            this.laBandera = true;
            @SuppressWarnings("unused")
            boolean tipoBloqueo0Valido = validarTipoBloqueo0();
            this.banderaVariosPredios = false;

        } else if (this.tipoBloqueo1 == true) {

            validacionFiltroRangoPredios = validacionFiltroRangoPredios();

            if (validacionFiltroRangoPredios == true) {
                this.prediosSeleccionados = this.getConservacionService()
                    .buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
                        this.filtroCPredioB, this.filtroFinalCPredioB);

                if (this.prediosSeleccionados != null &&
                    !this.prediosSeleccionados.isEmpty()) {

                    if (this.prediosSeleccionados.size() == 0) {
                        this.banderaValidar = false;
                        this.banderaVariosPredios = false;
                        String mensaje =
                            "No existe ningún predio con los números prediales ingresados" +
                            " en el departamento y municipio seleccionados." +
                            " Por favor corrija los datos e intente nuevamente." +
                            validacionFiltroRangoPredios + "Tercero";
                        this.addMensajeError(mensaje);
                    } else {
                        this.banderaValidar = true;
                        this.banderaVariosPredios = true;
                    }
                } else {
                    String mensaje =
                        "No existe ningún predio con los números prediales ingresados" +
                        " en el departamento y municipio seleccionados." +
                        " Por favor corrija los datos e intente nuevamente." + this.filtroCPredioB.
                            toString() + "I" + this.filtroFinalCPredioB.toString();
                    this.addMensajeError(mensaje);
                    this.banderaValidar = false;
                    this.banderaVariosPredios = false;
                }
            } else {
                this.banderaValidar = false;
                this.banderaVariosPredios = false;
            }
        }
        LOGGER.debug("Bandera mostrar varios:" + this.banderaVariosPredios);
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida los datos para el tipo de bloqueo 0
     */
    private boolean validarTipoBloqueo0() {

        boolean departamentoCoincidente = false;
        boolean municipioCoincidente = false;

        if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
             this.filtroCPredioB.getDepartamentoCodigo().isEmpty() ||
             this.filtroCPredioB.getMunicipioCodigo() == null ||
             this.filtroCPredioB.getMunicipioCodigo().isEmpty() ||
             this.filtroCPredioB.getTipoAvaluo() == null ||
             this.filtroCPredioB.getTipoAvaluo().isEmpty() ||
             this.filtroCPredioB.getSectorCodigo() == null ||
             this.filtroCPredioB.getSectorCodigo().isEmpty() ||
             this.filtroCPredioB.getBarrioCodigo() == null ||
             this.filtroCPredioB.getBarrioCodigo().isEmpty() ||
             this.filtroCPredioB.getManzanaVeredaCodigo() == null ||
             this.filtroCPredioB.getManzanaVeredaCodigo().isEmpty() ||
             this.filtroCPredioB.getPredio() == null ||
             this.filtroCPredioB.getPredio().isEmpty() ||
             this.filtroCPredioB.getCondicionPropiedad() == null ||
             this.filtroCPredioB.getCondicionPropiedad().isEmpty() ||
             this.filtroCPredioB.getEdificio() == null ||
             this.filtroCPredioB.getEdificio().isEmpty() ||
             this.filtroCPredioB.getPiso() == null ||
             this.filtroCPredioB.getPiso().isEmpty() ||
             this.filtroCPredioB.getUnidad() == null ||
             this.filtroCPredioB.getUnidad().isEmpty()) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Para bloquear un predio debe incluir todos los campos" +
                 " del número predial. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
            return false;
        }

        for (Departamento dTemp : this.departamentos) {

            if (dTemp.getCodigo().equals(
                this.filtroCPredioB.getDepartamentoCodigo())) {
                departamentoCoincidente = true;
                break;
            }
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Usted está intentando bloquear un predio que corresponde" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
            return false;
        }

        for (Municipio mTemp : this.municipios) {

            if (mTemp.getCodigo3Digitos().equals(
                this.filtroCPredioB.getMunicipioCodigo())) {
                municipioCoincidente = true;
                break;
            }
        }

        if (municipioCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Usted está intentando bloquear un predio que corresponde" +
                 " a un municipio fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
            return false;
        }

        if (this.fechaFinal != null &&
             this.fechaFinal.before(this.fechaInicial)) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "La fecha final no puede ser menor a la fecha inicial." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.laBandera == true) {
            if (this.tipoBloqueo0 == true) {
                this.prediosSeleccionados = this
                    .getConservacionService()
                    .buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
                        this.filtroCPredioB);
                LOGGER.debug("NUMERO PREDIOS:" + this.prediosSeleccionados.size());
                if (this.prediosSeleccionados != null) {

                    if (this.prediosSeleccionados.size() == 0) {
                        this.banderaValidar = false;
                        this.laBandera = false;
                        String mensaje = "No existe ningún predio con el número predial ingresado" +
                             " en el departamento y municipio seleccionados." +
                             " Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        return false;
                    } else if (this.prediosSeleccionados.size() > 1) {
                        this.banderaValidar = false;
                        this.laBandera = false;
                        String mensaje =
                            "Existen múltiples predios con el número predial ingresado" +
                             " en el departamento y municipio seleccionados." +
                             " Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        return false;
                    } else if (this.prediosSeleccionados.size() == 1) {
                        this.banderaValidar = true;
                        this.laBandera = false;
                        LOGGER.debug("predios seleccionados " +
                             this.prediosSeleccionados.size());
                        return false;
                    }
                }
            }
        }

        return true;
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * metodo utilizado validar los datos basicos de la busqueda por rangos
     */
    private boolean validacionFiltroRangoPredios() {

        boolean departamentoCoincidente = false;
        boolean municipioCoincidente = false;

        String mensaje = "Los números prediales tanto del número predial inicial como del número" +
             " predial final deben contener información en los campos hasta manzana / vereda." +
             " Por favor corrija los datos e intente nuevamente.";

        if (this.filtroCPredioB == null || this.filtroFinalCPredioB == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
             this.filtroFinalCPredioB.getDepartamentoCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getMunicipioCodigo() == null ||
             this.filtroFinalCPredioB.getMunicipioCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getTipoAvaluo() == null ||
             this.filtroFinalCPredioB.getTipoAvaluo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getSectorCodigo() == null ||
             this.filtroFinalCPredioB.getSectorCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getComunaCodigo() == null ||
             this.filtroFinalCPredioB.getComunaCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getBarrioCodigo() == null ||
             this.filtroFinalCPredioB.getBarrioCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getManzanaVeredaCodigo() == null ||
             this.filtroFinalCPredioB.getManzanaVeredaCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getDepartamentoCodigo() != null && !this.filtroCPredioB
            .getDepartamentoCodigo().isEmpty()) ||
             (this.filtroFinalCPredioB.getDepartamentoCodigo() != null && !this.filtroFinalCPredioB
            .getDepartamentoCodigo().isEmpty())) {

            mensaje = "El número predial inicial y el número" +
                 " predial final deben pertenecer al mismo departamento." +
                 " Por favor corrija los datos e intente nuevamente.";

            if (this.filtroCPredioB.getDepartamentoCodigo() != null &&
                 !this.filtroCPredioB.getDepartamentoCodigo().isEmpty()) {
                if (this.filtroFinalCPredioB.getDepartamentoCodigo() == null ||
                     !this.filtroCPredioB.getDepartamentoCodigo().equals(
                        this.filtroFinalCPredioB
                            .getDepartamentoCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            } else if (this.filtroFinalCPredioB.getDepartamentoCodigo() != null &&
                 !this.filtroFinalCPredioB.getDepartamentoCodigo()
                    .isEmpty()) {
                if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
                     !this.filtroCPredioB.getDepartamentoCodigo().equals(
                        this.filtroFinalCPredioB
                            .getDepartamentoCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            }
        }

        if ((this.filtroCPredioB.getMunicipioCodigo() != null && !this.filtroCPredioB
            .getMunicipioCodigo().isEmpty()) ||
             (this.filtroFinalCPredioB.getMunicipioCodigo() != null && !this.filtroFinalCPredioB
            .getMunicipioCodigo().isEmpty())) {

            mensaje = "El número predial inicial y el número" +
                 " predial final deben pertenecer al mismo municipio." +
                 " Por favor corrija los datos e intente nuevamente.";

            if (this.filtroCPredioB.getMunicipioCodigo() != null &&
                 !this.filtroCPredioB.getMunicipioCodigo().isEmpty()) {
                if (this.filtroFinalCPredioB.getMunicipioCodigo() == null ||
                     !this.filtroCPredioB.getMunicipioCodigo().equals(
                        this.filtroFinalCPredioB.getMunicipioCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            } else if (this.filtroFinalCPredioB.getMunicipioCodigo() != null &&
                 !this.filtroFinalCPredioB.getMunicipioCodigo().isEmpty()) {
                if (this.filtroCPredioB.getMunicipioCodigo() == null ||
                     !this.filtroCPredioB.getMunicipioCodigo().equals(
                        this.filtroFinalCPredioB.getMunicipioCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            }
        }

        if ((this.filtroCPredioB.getTipoAvaluo() != null && !this.filtroCPredioB
            .getTipoAvaluo().isEmpty()) ||
             (this.filtroFinalCPredioB.getTipoAvaluo() != null && !this.filtroFinalCPredioB
            .getTipoAvaluo().isEmpty())) {

            mensaje = "El número predial inicial y el número" +
                 " predial final deben pertenecer a la misma zona." +
                 " Por favor corrija los datos e intente nuevamente.";

            if (this.filtroCPredioB.getTipoAvaluo() != null &&
                 !this.filtroCPredioB.getTipoAvaluo().isEmpty()) {
                if (this.filtroFinalCPredioB.getTipoAvaluo() == null ||
                     !this.filtroCPredioB.getTipoAvaluo().equals(
                        this.filtroFinalCPredioB.getTipoAvaluo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            } else if (this.filtroFinalCPredioB.getTipoAvaluo() != null &&
                 !this.filtroFinalCPredioB.getTipoAvaluo().isEmpty()) {
                if (this.filtroCPredioB.getTipoAvaluo() == null ||
                     !this.filtroCPredioB.getTipoAvaluo().equals(
                        this.filtroFinalCPredioB.getMunicipioCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            }
        }

        if (this.tipoBloqueo1 == true) {

            if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
                 this.filtroCPredioB.getDepartamentoCodigo().isEmpty() ||
                 this.filtroCPredioB.getMunicipioCodigo() == null ||
                 this.filtroCPredioB.getMunicipioCodigo().isEmpty() ||
                 this.filtroCPredioB.getTipoAvaluo() == null ||
                 this.filtroCPredioB.getTipoAvaluo().isEmpty() ||
                 this.filtroCPredioB.getSectorCodigo() == null ||
                 this.filtroCPredioB.getSectorCodigo().isEmpty() ||
                 this.filtroCPredioB.getBarrioCodigo() == null ||
                 this.filtroCPredioB.getBarrioCodigo().isEmpty() ||
                 this.filtroCPredioB.getManzanaVeredaCodigo() == null ||
                 this.filtroCPredioB.getManzanaVeredaCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getDepartamentoCodigo() == null ||
                 this.filtroFinalCPredioB.getDepartamentoCodigo()
                    .isEmpty() ||
                 this.filtroFinalCPredioB.getMunicipioCodigo() == null ||
                 this.filtroFinalCPredioB.getMunicipioCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getTipoAvaluo() == null ||
                 this.filtroFinalCPredioB.getTipoAvaluo().isEmpty() ||
                 this.filtroFinalCPredioB.getSectorCodigo() == null ||
                 this.filtroFinalCPredioB.getSectorCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getBarrioCodigo() == null ||
                 this.filtroFinalCPredioB.getBarrioCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getManzanaVeredaCodigo() == null ||
                 this.filtroFinalCPredioB.getManzanaVeredaCodigo().isEmpty()) {
                this.banderaValidar = false;
                this.laBandera = false;
                mensaje = "Para bloquear un rango de predios debe incluir los campos" +
                     " hasta manzana/vereda de los números prediales. " +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(
                    "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                    mensaje);
            }
        }

        if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
             (this.filtroCPredioB.getDepartamentoCodigo() != null && this.filtroCPredioB
            .getDepartamentoCodigo().isEmpty()) ||
             this.filtroFinalCPredioB.getDepartamentoCodigo() == null ||
             (this.filtroFinalCPredioB.getDepartamentoCodigo() != null && this.filtroFinalCPredioB
            .getDepartamentoCodigo().isEmpty())) {

            mensaje = "El campo departamento debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getDepartamentoCodigo().equals(
            this.filtroFinalCPredioB.getDepartamentoCodigo())) {

            mensaje = "El campo departamento debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        for (Departamento dTemp : this.departamentos) {

            if (dTemp.getCodigo().equals(
                this.filtroCPredioB.getDepartamentoCodigo())) {
                departamentoCoincidente = true;
                break;
            }
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando bloquear un rango de predios que corresponden" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
        }

        if (!this.filtroCPredioB.getMunicipioCodigo().equals(
            this.filtroFinalCPredioB.getMunicipioCodigo())) {

            mensaje = "El campo municipio debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        for (Municipio mTemp : this.municipios) {

            if (mTemp.getCodigo3Digitos().equals(
                this.filtroCPredioB.getMunicipioCodigo())) {
                municipioCoincidente = true;
                break;
            }
        }

        if (municipioCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando bloquear un  rango de predios que corresponden" +
                 " a un municipio fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando bloquear un rango de predios que corresponden" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando bloquear un rango de predios que corresponden" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
        }

        if (!this.filtroCPredioB.getTipoAvaluo().equals(
            this.filtroFinalCPredioB.getTipoAvaluo())) {

            mensaje = "El campo zona debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getSectorCodigo().equals(
            this.filtroFinalCPredioB.getSectorCodigo())) {

            mensaje = "El campo sector debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getComunaCodigo().equals(
            this.filtroFinalCPredioB.getComunaCodigo())) {

            mensaje = "El campo comuna debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getBarrioCodigo().equals(
            this.filtroFinalCPredioB.getBarrioCodigo())) {

            mensaje = "El campo barrio debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getMunicipioCodigo().equals(
            this.filtroFinalCPredioB.getMunicipioCodigo())) {

            mensaje = "El campo municipio debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }
        if ((this.filtroCPredioB.getPredio() == null && this.filtroFinalCPredioB
            .getPredio() != null) ||
             (this.filtroCPredioB.getPredio() != null && this.filtroFinalCPredioB
            .getPredio() == null) ||
             (this.filtroCPredioB.getPredio() != null &&
             !this.filtroCPredioB.getPredio().isEmpty() && (this.filtroFinalCPredioB
            .getPredio() == null || (this.filtroFinalCPredioB
                .getPredio() != null && this.filtroFinalCPredioB
                .getPredio().isEmpty()))) ||
             (this.filtroFinalCPredioB.getPredio() != null &&
             !this.filtroFinalCPredioB.getPredio().isEmpty() && (this.filtroCPredioB
            .getPredio() == null || (this.filtroCPredioB
                .getPredio() != null && this.filtroCPredioB.getPredio()
                .isEmpty())))) {

            mensaje = "Si se ingresa el campo terreno este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getCondicionPropiedad() == null && this.filtroFinalCPredioB
            .getCondicionPropiedad() != null) ||
             (this.filtroCPredioB.getCondicionPropiedad() != null && this.filtroFinalCPredioB
            .getCondicionPropiedad() == null) ||
             (this.filtroCPredioB.getCondicionPropiedad() != null &&
             !this.filtroCPredioB.getCondicionPropiedad()
                .isEmpty() && (this.filtroFinalCPredioB
                .getCondicionPropiedad() == null || (this.filtroFinalCPredioB
                .getCondicionPropiedad() != null && this.filtroFinalCPredioB
                .getCondicionPropiedad().isEmpty()))) ||
             (this.filtroFinalCPredioB.getCondicionPropiedad() != null &&
             !this.filtroFinalCPredioB.getCondicionPropiedad()
                .isEmpty() && (this.filtroCPredioB
                .getCondicionPropiedad() == null || (this.filtroCPredioB
                .getCondicionPropiedad() != null && this.filtroCPredioB
                .getCondicionPropiedad().isEmpty())))) {
            mensaje = "Si se ingresa el campo condición de propiedad este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getEdificio() == null && this.filtroFinalCPredioB
            .getEdificio() != null) ||
             (this.filtroCPredioB.getEdificio() != null && this.filtroFinalCPredioB
            .getEdificio() == null) ||
             (this.filtroCPredioB.getEdificio() != null &&
             !this.filtroCPredioB.getEdificio().isEmpty() && (this.filtroFinalCPredioB
            .getEdificio() == null || (this.filtroFinalCPredioB
                .getEdificio() != null && this.filtroFinalCPredioB
                .getEdificio().isEmpty()))) ||
             (this.filtroFinalCPredioB.getEdificio() != null &&
             !this.filtroFinalCPredioB.getEdificio().isEmpty() && (this.filtroCPredioB
            .getEdificio() == null || (this.filtroCPredioB
                .getEdificio() != null && this.filtroCPredioB
                .getEdificio().isEmpty())))) {
            mensaje = "Si se ingresa el campo número del edificio este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getPiso() == null && this.filtroFinalCPredioB
            .getPiso() != null) ||
             (this.filtroCPredioB.getPiso() != null && this.filtroFinalCPredioB
            .getPiso() == null) ||
             (this.filtroCPredioB.getPiso() != null &&
             !this.filtroCPredioB.getPiso().isEmpty() && (this.filtroFinalCPredioB
            .getPiso() == null || (this.filtroFinalCPredioB
                .getPiso() != null && this.filtroFinalCPredioB
                .getPiso().isEmpty()))) ||
             (this.filtroFinalCPredioB.getPiso() != null &&
             !this.filtroFinalCPredioB.getPiso().isEmpty() && (this.filtroCPredioB
            .getPiso() == null || (this.filtroCPredioB.getPiso() != null && this.filtroCPredioB
            .getPiso().isEmpty())))) {
            mensaje = "Si se ingresa el campo número de piso este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getUnidad() == null && this.filtroFinalCPredioB
            .getUnidad() != null) ||
             (this.filtroCPredioB.getUnidad() != null && this.filtroFinalCPredioB
            .getUnidad() == null) ||
             (this.filtroCPredioB.getUnidad() != null &&
             !this.filtroCPredioB.getUnidad().isEmpty() && (this.filtroFinalCPredioB
            .getUnidad() == null || (this.filtroFinalCPredioB
                .getUnidad() != null && this.filtroFinalCPredioB
                .getUnidad().isEmpty()))) ||
             (this.filtroFinalCPredioB.getUnidad() != null &&
             !this.filtroFinalCPredioB.getUnidad().isEmpty() && (this.filtroCPredioB
            .getUnidad() == null || (this.filtroCPredioB
                .getUnidad() != null && this.filtroCPredioB.getUnidad()
                .isEmpty())))) {
            mensaje = "Si se ingresa el campo número de unidad este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        return true;
    }

    // --------------------------------------------------------------------------------------------------
    private void populatePredioBloqueoLazyly(List<PredioBloqueo> answer,
        int first, int pageSize) {
        int size = 0;
        LOGGER.debug("Rows:" + rows.size());
        if (rows != null) {
            size = rows.size();
            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    public void poblar() {

        int count;
        count = this.getConservacionService().contarPrediosBloqueo(
            this.filtroCPredioB);

        if (count > 0) {
            this.lazyAssignedBloqueos.setRowCount(count);
        } else {
            this.lazyAssignedBloqueos.setRowCount(0);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton Buscar
     *
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public void buscar() {

        Departamento dep = new Departamento();
        Municipio mun = new Municipio();
        this.banderaBuscar = true;

        if (this.departamento != null) {
            this.departamento.setCodigo(this.selectedDepartamentoCod);
        }

        if (this.municipio != null) {
            this.municipio.setCodigo(this.selectedMunicipioCod);
        }

        this.departamentoCodigo = this.selectedDepartamentoCod;
        this.municipioCodigo = this.selectedMunicipioCod;
        dep.setCodigo(this.departamentoCodigo);
        mun.setCodigo(this.municipioCodigo);

        this.lazyAssignedBloqueos = new LazyDataModel<PredioBloqueo>() {

            @Override
            public List<PredioBloqueo> load(int first, int pageSize,
                String sortField, SortOrder sortOrder,
                Map<String, String> filters) {
                List<PredioBloqueo> answer = new ArrayList<PredioBloqueo>();
                populatePredioBloqueoLazyly(answer, first, pageSize);

                return answer;
            }
        };
        poblar();

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado al cerrar las ventanas modales
     */
    public void cerrarEsc(org.primefaces.event.CloseEvent event) {

        this.aBMasa = 0;
        this.aSoporte = 0;
        this.motivoBloqueo = "";
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar y bloquear otra predio
     */
    public void terminarYBloquearOtroP() {
        init();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método Bloquear el predio
	 * */
    public void bloquearPredioBoton() {

        FacesMessage msg = new FacesMessage(
            "entro en bloquearPredio, prediosSelect:" +
             this.prediosSelectTipo1.toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar
     */
    public String terminar() {
        try {
            UtilidadesWeb
                .removerManagedBean("bloquearPrediosParaZonasEnCondicionesEspeciales");
            return "index";
        } catch (Exception e) {
            this.addMensajeError("Error al cerrar la sesión");
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosArchivoBloqueo() {
        this.documentoSoporteBloqueo = new Documento();

        this.aSoporte = 0;
        this.motivoBloqueo = "";
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosArchivoBloqueoMasivo() {
        this.documentoSoporteBloqueo = new Documento();
        this.aSoporte = 0;
        this.motivoBloqueo = "";
    }

    // ----------------------------------------------------------------------------
    /**
     * Metodo para retirar predios Caso 1 Retirar Predio Bloqueo caso 2 Retirar Predio en Rango de
     * Bloqueo
     *
     */
    public void retirarPredios() {

        if (this.banderaVariosPredios) {
            if (this.prediosSelectTipo.length > 0) {
                for (Predio p : this.prediosSelectTipo) {
                    this.prediosSeleccionados.remove(p);
                }
            } else {
                String mensaje = "No a selecionado ningun predio";
                this.addMensajeError(mensaje);
            }
        } else {
            if (this.prediosSelectTipo1.getNumeroPredial() != null) {
                this.banderaValidar = false;
                this.prediosSeleccionados.clear();
                this.fechaFinal = null;
                this.fechaFinal = null;
                this.motivoBloqueo = null;
                this.selectedMunicipioCod = null;
                this.departamento = null;
                this.municipio = null;
                this.selectedDepartamentoCod = null;
                this.filtroCPredioB = new FiltroDatosConsultaPrediosBloqueo();

            } else {
                String mensaje = "No a selecionado un predio";
                this.addMensajeError(mensaje);
            }
        }

    }

    // ---------------------------------------------
    /**
     * Metodo para cargar el archivo de soporte para bloqueo
     */
    public void cargarDocumentoSoporte() {
        LOGGER.debug("ENTRO ...................cargarDocumentoSoporte");
        if (this.banderaVariosPredios) {
            if (this.prediosSelectTipo.length > 0) {
                this.laBandera = true;
                LOGGER.debug("Tamaño de la seleccion:" + this.prediosSelectTipo.length);
            } else {
                String mensaje = "No a selecionado ningun predio" + this.prediosSelectTipo1.
                    getNumeroPredial();
                this.addMensajeError(mensaje);
            }
        } else {
            if (this.prediosSelectTipo1.getNumeroPredial() != null) {
                this.laBandera = true;
            } else {
                String mensaje = "No a selecionado ningun predio" + this.prediosSelectTipo1.
                    getNumeroPredial();
                this.addMensajeError(mensaje);
            }
        }

    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Método encargado habilitar el boton retirar
     *
     * @throws NoSuchFieldException
     * @throws SecurityException
     */
    public void rowSelected(org.primefaces.event.SelectEvent evento) {
        this.banderaBotonRetirar = false;
        if (!this.banderaVariosPredios) {
            Predio selectPredio = (Predio) evento.getObject();
            this.prediosSelectTipo1 = selectPredio;
        }

    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Metodo para guardar los datos de predio bloqueo ejecutado sobre el boton guardar
     */
    public void bloquearPB() {

        RequestContext context = RequestContext.getCurrentInstance();
        this.prediosBloqueados = new ArrayList<PredioBloqueo>();
        this.predioBloqueo = new PredioBloqueo();

        if (this.tipoBloqueo == 0 && this.aSoporte == 1) {
            if (this.prediosSeleccionados.size() == 1) {

                this.predioBloqueo.setPredio(this.prediosSeleccionados.get(0));
                this.predioBloqueo.setFechaInicioBloqueo(this.fechaInicial);
                this.predioBloqueo.setFechaTerminaBloqueo(this.fechaFinal);
                this.predioBloqueo.setFechaRecibido(this.fechaRecibido);
                this.predioBloqueo.setMotivoBloqueo(this.motivoBloqueo);

                this.predioBloqueo
                    .setDocumentoSoporteBloqueo(this.documentoSoporteBloqueo);

                this.prediosBloqueados.add(predioBloqueo);

                try {

                    ResultadoBloqueoMasivo resultado = this
                        .getConservacionService().actualizarPrediosBloqueo(
                            this.usuario, this.prediosBloqueados);

                    this.guardarDocumentosActualizacionDocumento(resultado.
                        getBloqueoMasivoPrediosExitoso());

                    if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                         !resultado.getBloqueoMasivoPrediosExitoso()
                            .isEmpty()) {

                        int contadorErrores = 0;
                        for (PredioBloqueo pbTemp : resultado
                            .getBloqueoMasivoPrediosExitoso()) {

                            if (pbTemp.getDocumentoSoporteBloqueo() != null) {

                                if (pbTemp.getDocumentoSoporteBloqueo() != null) {
                                    if (pbTemp.getDocumentoSoporteBloqueo()
                                        .getIdRepositorioDocumentos() == null) {
                                        contadorErrores++;
                                        break;
                                    }
                                }
                            }
                        }

                        if (resultado.getErroresProcesoMasivo() != null &&
                             !resultado.getErroresProcesoMasivo()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio identificado con número predial ");

                            for (ErrorProcesoMasivo epm : resultado
                                .getErroresProcesoMasivo()) {
                                mensaje.append(epm.getTextoFuente()).append(" ");
                            }

                            mensaje.append(" se encuentrá cancelado y no puede ser bloqueado.");

                            this.addMensajeWarn(mensaje.toString());

                        }

                        if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                             !resultado.getBloqueoMasivoPrediosExitoso()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio  ");

                            if (contadorErrores == 0) {

                                mensaje.append(" se bloqueó exitosamente.");
                                this.addMensajeInfo(mensaje.toString());

                            } else if (contadorErrores > 0) {
                                mensaje.append(" se bloqueó exitosamente.");
                                mensaje.append(
                                    " pero se presentaron errores en el servicio de Alfresco");
                                mensaje.append(" y el documento de soporte no pudo ser almacenado!");
                                this.addMensajeWarn(mensaje.toString());
                            }
                        }

                        this.banderaBotonGuardar = false;
                        this.banderaBotonGenerarInforme = true;

                    } else {

                        if (resultado.getErroresProcesoMasivo() != null &&
                             !resultado.getErroresProcesoMasivo()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio identificado con número predial ");

                            for (ErrorProcesoMasivo epm : resultado
                                .getErroresProcesoMasivo()) {
                                mensaje.append(epm.getTextoFuente()).append(" ");
                            }

                            mensaje.append(" se encuentrá cancelado y no puede ser bloqueado.");

                            this.addMensajeInfo(mensaje.toString());
                            context.addCallbackParam("error", "error");

                        } else {
                            String mensaje = "El predio seleccionado no arrojó ningún resultado";
                            this.addMensajeError(mensaje);
                            context.addCallbackParam("error", "error");
                        }
                    }

                } catch (Exception e) {
                    String mensaje = "El predio seleccionado no existe o no pudo ser bloqueado!";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }
            } else if (this.prediosSeleccionados.size() < 1) {

                String mensaje = "No existe ningún predio con el número predial ingresado" +
                     " en el departamento y municipio seleccionados." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            } else if (this.prediosSeleccionados.size() > 1) {
                String mensaje = "Existén multiples predios con el número predial ingresado" +
                     " en el departamento y municipio seleccionados." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else if (tipoBloqueo == 0 && this.aSoporte == 0) {
            String mensaje = "No se adjuntó un documento de soporte para el bloqueo";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 1 && this.aSoporte == 1) {

            PredioBloqueo pbTemp;
            Documento docTemp;
            this.prediosBloqueados = new ArrayList<PredioBloqueo>();

            for (Predio p : this.prediosSeleccionados) {

                pbTemp = new PredioBloqueo();

                docTemp = this.documentoSoporteBloqueo;
                docTemp.setPredioId(p.getId());

                pbTemp.setFechaInicioBloqueo(this.fechaInicial);
                pbTemp.setFechaTerminaBloqueo(this.fechaFinal);
                pbTemp.setFechaRecibido(this.fechaRecibido);
                pbTemp.setMotivoBloqueo(this.motivoBloqueo);
                pbTemp.setDocumentoSoporteBloqueo(docTemp);
                pbTemp.setPredio(p);

                prediosBloqueados.add(pbTemp);
            }

            try {
                ResultadoBloqueoMasivo resultado = this
                    .getConservacionService().actualizarPrediosBloqueo(
                        usuario, prediosBloqueados);

                this.guardarDocumentosActualizacionDocumento(resultado.
                    getBloqueoMasivoPrediosExitoso());

                if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     resultado.getBloqueoMasivoPrediosExitoso().size() > 1) {
                    if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                         !resultado.getBloqueoMasivoPrediosExitoso()
                            .isEmpty()) {

                        int contadorErrores = 0;
                        for (PredioBloqueo pbTmp : resultado
                            .getBloqueoMasivoPrediosExitoso()) {

                            if (pbTmp.getDocumentoSoporteBloqueo() != null) {

                                if (pbTmp.getDocumentoSoporteBloqueo() != null) {
                                    if (pbTmp.getDocumentoSoporteBloqueo()
                                        .getIdRepositorioDocumentos() == null) {
                                        contadorErrores++;
                                        break;
                                    }
                                }
                            }
                        }

                        if (resultado.getErroresProcesoMasivo() != null &&
                             !resultado.getErroresProcesoMasivo()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio(s) identificado con número predial ");

                            for (ErrorProcesoMasivo epm : resultado
                                .getErroresProcesoMasivo()) {
                                mensaje.append(epm.getTextoFuente()).append(" ");
                            }

                            mensaje.append(" se encuentrá cancelado y no puede ser bloqueado.");

                            this.addMensajeWarn(mensaje.toString());

                        }

                        if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                             !resultado.getBloqueoMasivoPrediosExitoso()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El rango de predios");

                            if (contadorErrores == 0) {

                                mensaje.append(" se bloqueó exitosamente.");
                                this.addMensajeInfo(mensaje.toString());

                            } else if (contadorErrores > 0) {
                                mensaje.append(" se bloqueó exitosamente.");
                                mensaje.append(
                                    " pero se presentaron errores en el servicio de Alfresco");
                                mensaje.append(" y el documento de soporte no pudo ser almacenado!");
                                this.addMensajeWarn(mensaje.toString());
                            }
                        }

                        this.banderaBotonGuardar = false;
                        this.banderaBotonGenerarInforme = true;
                    }
                } else if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     resultado.getBloqueoMasivoPrediosExitoso().size() == 0) {
                    String mensaje = "El rango de predios seleccionados no arrojó ningún resultado";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                } else {
                    String mensaje =
                        "Ocurrio un error bloqueando el rango de predios y no se realizaron bloqueos.";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }
            } catch (Exception e) {
                String mensaje =
                    "El rango de predios selecionados no es válido o no pudo ser bloqueado!";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else if (this.tipoBloqueo == 1 && this.aSoporte == 0) {
            String mensaje = "No se adjuntó un documento de soporte para el bloqueo";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }
    // --------------------------------------------------------------------------------------------------

    /**
     * Metodo para guardar los datos de predio bloqueo ejecutado sobre el botón guardar
     *
     */
    public void bloquearMPB() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (this.tipoBloqueo == 2 && this.aSoporte == 1 && this.aBMasa == 1) {

            List<DocumentoArchivoAnexo> daas = null;

            try {

                for (PredioBloqueo pb : this.bloqueoMasivo) {

                    pb.setDocumentoSoporteBloqueo(this.documentoSoporteBloqueo);

                    if (pb.getDocumentoSoporteBloqueo()
                        .getDocumentoArchivoAnexos() == null ||
                         (pb.getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos() == null && !pb
                            .getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos().isEmpty())) {

                        daas = new ArrayList<DocumentoArchivoAnexo>();

                        daas.add(this.documentoArchivoAnexo);
                        pb.getDocumentoSoporteBloqueo()
                            .setDocumentoArchivoAnexos(daas);
                    }
                }

                ResultadoBloqueoMasivo resultado = this
                    .getConservacionService().bloquearMasivamentePredios(
                        this.usuario, this.bloqueoMasivo);

                this.guardarDocumentosActualizacionDocumento(resultado.
                    getBloqueoMasivoPrediosExitoso());

                this.doctypeDocumentoResultado = EArchivoAnexoFormato.ARCHIVO_PDF
                    .getValor();
                this.documentoResultadoBloqueoMasivo = this.urlArchivoBloqueoMasivoPredios;

                if (resultado != null) {

                    int contadorErrores = 0;
                    for (PredioBloqueo pbTmp : resultado
                        .getBloqueoMasivoPrediosExitoso()) {

                        if (pbTmp.getDocumentoSoporteBloqueo() != null) {

                            if (pbTmp.getDocumentoSoporteBloqueo() != null) {
                                if (pbTmp.getDocumentoSoporteBloqueo()
                                    .getIdRepositorioDocumentos() == null) {
                                    contadorErrores++;
                                    break;
                                }
                            }
                        }
                    }

                    if (contadorErrores > 0) {
                        String mensaje = "El bloqueo masivo de predios " +
                             " presentó errores en el servicio de Alfresco" +
                             " y el documento de soporte no pudo ser almacenado!";
                        this.addMensajeWarn(mensaje);
                    }

                }

                if (resultado != null &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     resultado.getErroresProcesoMasivo() != null &&
                     !resultado.getErroresProcesoMasivo().isEmpty()) {
                    String mensaje = "El bloqueo masivo se realizó pero se presentaron errores";
                    this.addMensajeInfo(mensaje);
                } else if (resultado != null &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     (resultado.getErroresProcesoMasivo() == null || (resultado
                    .getErroresProcesoMasivo() != null && resultado
                        .getErroresProcesoMasivo().isEmpty()))) {
                    String mensaje = "El bloqueo masivo se realizó exitosamente";
                    this.addMensajeInfo(mensaje);
                } else if (resultado != null &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     resultado.getBloqueoMasivoPrediosExitoso().isEmpty()) {
                    String mensaje = "El bloqueo masivo no arrojó ningún resultado";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }
            } catch (Exception e) {
                String mensaje = "El bloqueo masivo no es válido o no pudo realizar los bloqueos!";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }

        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 1) {
            String mensaje = "No se adjuntó un documento de soporte para el bloqueo";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 1 &&
             this.aBMasa == 0) {
            String mensaje;
            if (this.nombreDocumentoBloqueoMasivo == null ||
                 (this.nombreDocumentoBloqueoMasivo != null && this.nombreDocumentoBloqueoMasivo
                    .isEmpty())) {
                mensaje = "No se adjuntó un archivo de bloqueo masivo" +
                     " Por favor corrija los datos e intente neuvamente.";
            } else {
                mensaje = "El archivo que adjunto no contiene ningún bloqueo valido." +
                     " Por favor corrija los datos e intente nuevamente.";
            }
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 0) {
            String mensaje =
                "No se adjuntó un documento de soporte, ni un archivo de bloqueo masivo." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    /**
     * Método ejecutado cargar los barrios de la actualizacion
     *
     * @author javier.barajas
     */
    public void cargarBarriosVisor() {

        Actualizacion tempActualizacion = new Actualizacion();
        tempActualizacion = this.getActualizacionService().getActualizacionConResponsables(
            this.actualizacionSeleccionada.getId());
        this.codigoMunicipio = tempActualizacion.getMunicipio().getCodigo();
        List<Barrio> listaBarrios = new ArrayList<Barrio>();
        listaBarrios = this.getGeneralesService().
            getNumeroManzanasPorMunicipio(this.codigoMunicipio);
        this.numeroBarriosMunicipio = listaBarrios.size();
        this.adicionarDatosVitem(this.selectListaBarrios, listaBarrios);

    }

    /**
     * Método ejecutado cargar manzanas oara el visor
     *
     * @author javier.barajas
     *
     */
    public void cargarManzanasVisor() {

        List<ManzanaVereda> listaManzana = new ArrayList<ManzanaVereda>();
        listaManzana = this.getGeneralesService().getManzanaVeredaFromBarrio(this.seleccionBarrio);
        int i = 0;
        if (listaManzana.size() > 0) {
            for (ManzanaVereda mv : listaManzana) {
                if (i == 0) {
                    this.codigoPrediosParaVisor = mv.getCodigo();
                    if (listaManzana.size() > 1) {
                        this.codigoPrediosParaVisor = this.codigoPrediosParaVisor + ",";
                    }

                } else {
                    this.codigoPrediosParaVisor = this.codigoPrediosParaVisor + mv.getCodigo();
                    if (i < listaManzana.size() - 1) {
                        this.codigoPrediosParaVisor = this.codigoPrediosParaVisor + ",";

                    }

                }
                i++;
            }

        }

    }

    /**
     * Metodo para mostrar los predios de la seleccion del visor
     *
     * @author javier.barajas
     */

    public void seleccionVisorManzanas() {
        LOGGER.debug("seleccionVisorManzanas");
        String[] manzanasSelectVisor = null;
        List<Predio> listaPrediosTemp = new ArrayList<Predio>();
        if (!this.seleccionVisor.isEmpty()) {
            this.rederedConsultaVisor = true;
            manzanasSelectVisor = this.seleccionVisor.split(",");
            this.numeroManzanasSeleccionadas = manzanasSelectVisor.length;
            listaPrediosTemp = this.getActualizacionService().buscarPrediosTramitePorNumeroManzana(
                manzanasSelectVisor);
            this.prediosSeleccionados = listaPrediosTemp;
            this.numeroPrediosSeleccionados = listaPrediosTemp.size();
            this.banderaValidar = true;
            this.banderaVariosPredios = true;
            this.banderaSeleccionVisor = true;
            this.tipoBloqueo = 1;
            this.tipoBloqueo1 = false;
            this.tipoBloqueo0 = false;
            //this.laBandera=true;
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
                "No ha seleccionado las manzanas del visor",
                "No ha seleccionado las manzanas del visor");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    private void adicionarDatosVitem(List<SelectItem> lista,
        List<Barrio> listBarrio) {
        int i = 1;

        for (Barrio barrio : listBarrio) {
            lista.add(new SelectItem(barrio.getCodigo(), String.valueOf(i)));
            i++;
        }
    }

    public void mostrarVariablesVisor() {
        LOGGER.debug(this.applicationClientName);
        LOGGER.debug(this.moduleLayer);
        LOGGER.debug(this.moduleTools);
        LOGGER.debug(this.codigoPrediosParaVisor);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de departamentos
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.updateDepartamentosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de bloqueo
     */
    public void bloquearV() {

        if (tipoBloqueo == 0) {
            this.tipoBloqueo0 = true;
            this.tipoBloqueo1 = false;
            this.tipoBloqueo2 = false;
            this.mensajeCargaArchivo = "Cargar documento";
            this.banderaValidar = false;
            this.laBandera = false;
        } else if (tipoBloqueo == 1) {
            this.tipoBloqueo0 = false;
            this.tipoBloqueo1 = true;
            this.tipoBloqueo2 = false;
            this.mensajeCargaArchivo = "Cargar documento";
            this.banderaValidar = false;
            this.laBandera = false;
        } else if (tipoBloqueo == 2) {
            this.tipoBloqueo0 = false;
            this.tipoBloqueo1 = false;
            this.tipoBloqueo2 = true;
            this.mensajeCargaArchivo = "Cargar archivo";
            this.banderaValidar = true;
            this.laBandera = false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
            .add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de municipios
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenMunicipios() {

        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.updateMunicipiosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * actualiza la lista de municipios según el departamento seleccionado
     *
     * @author pedro.garcia
     */
    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.selectedDepartamentoCod != null) {
            this.municipios = new ArrayList<Municipio>();
            this.municipios = (ArrayList<Municipio>) this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.selectedDepartamentoCod);

            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(this.municipios);
            } else {
                Collections.sort(this.municipios,
                    Municipio.getComparatorNombre());
            }

            for (Municipio municipio : this.municipios) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    this.municipiosItemList.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    this.municipiosItemList.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener del cambio en el combo de departamentos
     */
    public void onChangeDepartamentos() {
        this.municipio = null;
        this.updateMunicipiosItemList();
        FacesMessage msg = new FacesMessage("departamento:" +
             this.departamento.getCodigo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Guardar documentos en actualizacion documento
     *
     */
    public void guardarDocumentosActualizacionDocumento(List<PredioBloqueo> prediosBloqueados) {

        for (PredioBloqueo pb : prediosBloqueados) {
            ActualizacionDocumento docActualizacion = new ActualizacionDocumento();
            docActualizacion.setActualizacion(this.actualizacionSeleccionada);
            docActualizacion.setFechaLog(new Date(System.currentTimeMillis()));
            docActualizacion.setFueGenerado(true);
            docActualizacion.setDocumento(pb.getDocumentoSoporteBloqueo());
            docActualizacion.setUsuarioLog(this.usuario.getLogin());
            try {
                docActualizacion = this.getActualizacionService().
                    guardarYActualizarActualizacionDocumento(docActualizacion);
            } catch (Exception e) {
                LOGGER.error("No se pudo guardar el documento en Actualizacion Documento");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "No se pudo guardar el documento en Actualizacion Documento",
                    "No se pudo guardar el documento en Actualizacion Documento"));
            }

        }
    }

}
