package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed Bean asociado al caso de uso CU-SA-AC-146 Reclasificar Trámite o Documento
 *
 * @author christian.rodriguez
 *
 */
@Component("reclasificarDocTra")
@Scope("session")
public class ReclasificacionDocumentoTramiteMB extends SNCManagedBean {

    private static final long serialVersionUID = -8892740765731101282L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReclasificacionDocumentoTramiteMB.class);

    @Autowired
    private GeneralMB generalMB;

    /**
     * Usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Radicación seleccionada
     */
    private RadicacionNuevaAvaluoComercialDTO radicacionNuevaSeleccionada;

    /**
     * Codigo del tipo de tramite seleccionado
     */
    private String codigoTipoTramiteNuevo;

    /**
     * Lista para selectOneMenu de Tipo de trámite
     */
    private List<SelectItem> tiposTramiteCombo;

    public RadicacionNuevaAvaluoComercialDTO getRadicacionNuevaSeleccionada() {
        return radicacionNuevaSeleccionada;
    }

    public void setRadicacionNuevaSeleccionada(
        RadicacionNuevaAvaluoComercialDTO radicacionNuevaSeleccionada) {
        this.radicacionNuevaSeleccionada = radicacionNuevaSeleccionada;
    }

    public List<SelectItem> getTiposTramiteCombo() {
        return tiposTramiteCombo;
    }

    public void setTiposTramiteCombo(List<SelectItem> tiposTramiteCombo) {
        this.tiposTramiteCombo = tiposTramiteCombo;
    }

    public String getCodigoTipoTramiteNuevo() {
        return codigoTipoTramiteNuevo;
    }

    public void setCodigoTipoTramiteNuevo(String codigoTipoTramiteNuevo) {
        this.codigoTipoTramiteNuevo = codigoTipoTramiteNuevo;
    }

    // -------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio AdministracionRadicacionesMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin AdministracionRadicacionesMB#init");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos (...)
     *
     * @author christian.rodriguez
     */
    private void iniciarDatos() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.cargarComboTiposTramite();

    }

    // --------------------------------------------------------------------------
    /**
     * Método para cargar el combo de tipos de tramite
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboTiposTramite() {
        LOGGER.debug("Inicio AdministracionRadicados#cargarComboTiposTramite");

        this.tiposTramiteCombo = new ArrayList<SelectItem>();

        for (SelectItem si : this.generalMB.getAvaluosComercialesTipoTramite()) {
            if (!si.getValue().equals(
                ETramiteTipoTramite.SOLICITUD_DE_CORRECCION
                    .getCodigo())) {
                this.tiposTramiteCombo.add(si);
            }
        }

        LOGGER.debug("Fin AdministracionRadicados#cargarComboTiposTramite");
    }

    // --------------------------------------------------------------------------
    /**
     * Método para realizar la reclasificación
     *
     * @author rodrigo.hernandez
     */
    public void reclasificarTramite() {
        String mensaje = "";
        boolean result = this.getAvaluosService()
            .reclasificarTramiteDocumentoODocumento(
                this.radicacionNuevaSeleccionada.getNumeroRadicacion(),
                this.codigoTipoTramiteNuevo,
                this.usuario);

        if (result) {
            mensaje = "Se realizó la reclasificación correctamente";
            this.addMensajeInfo(mensaje);
        } else {
            mensaje = "No se pudo reclasificar el tramite. Intente nuevamente";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------
    public String cerrar() {

        LOGGER.debug("iniciando ReclasificacionDocumentoTramiteMB#cerrar");

        UtilidadesWeb.removerManagedBean("reclasificarDocTra");

        LOGGER.debug("finalizando ReclasificacionDocumentoTramiteMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método que recibe la radicacion nueva seleccionada en CU-SA-AC-047 (Administrar Radicaciones)
     *
     * @author rodrigo.hernandez
     *
     * @param radicacionNueva - Radicacion nueva seleccionada
     */
    public void cargarRadicacionNueva(
        RadicacionNuevaAvaluoComercialDTO radicacionNueva) {
        this.radicacionNuevaSeleccionada = radicacionNueva;
    }

}
