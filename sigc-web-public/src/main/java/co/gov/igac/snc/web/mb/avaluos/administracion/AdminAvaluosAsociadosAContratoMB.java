/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB del cu
 *
 * @cu CU-SA-AC-088
 * @author pedro.garcia
 */
@Component("adminAvaluosAsociadosAContrato")
@Scope("session")
public class AdminAvaluosAsociadosAContratoMB extends SNCManagedBean {

    private static final long serialVersionUID = 31925600691265148L;

    private static final Logger LOGGER =
        LoggerFactory.getLogger(AdminAvaluosAsociadosAContratoMB.class);

    /**
     * objeto que se recibe como parámetro para mostrar los avalúos asociados a ese contrato
     */
    private ContratoInteradministrativo currentContract;

    /**
     * sumatoria de los valores de los avalúos en trámite asociados al contrato incrementados en el
     * porcentaje de IVA
     */
    private double valorComprometidoEjecucion;

    /**
     * lista de los avalúos en trámite asociados al contrato actual
     */
    private ArrayList<Avaluo> avaluosEnTramiteAsociados;

    /**
     * variable chimba para evitar usar funciones jstl
     */
    private int numeroAvaluosEnTramite;

    /**
     * guarda el avalúo que se selecciona de la tabla
     */
    private Avaluo selectedAvaluo;

    /**
     * variable que guarda los datos del usuario que usa la aplicación
     */
    private UsuarioDTO loggedInUser;

//-----------------------------    Métodos         ---------------------------------
    public Avaluo getSelectedAvaluo() {
        return this.selectedAvaluo;
    }

    public void setSelectedAvaluo(Avaluo selectedAvaluo) {
        this.selectedAvaluo = selectedAvaluo;
    }

    public int getNumeroAvaluosEnTramite() {
        return this.numeroAvaluosEnTramite;
    }

    public void setNumeroAvaluosEnTramite(int numeroAvaluosEnTramite) {
        this.numeroAvaluosEnTramite = numeroAvaluosEnTramite;
    }

    public ArrayList<Avaluo> getAvaluosEnTramiteAsociados() {
        return this.avaluosEnTramiteAsociados;
    }

    public void setAvaluosEnTramiteAsociados(ArrayList<Avaluo> avaluosEnTramiteAsociados) {
        this.avaluosEnTramiteAsociados = avaluosEnTramiteAsociados;
    }

    public double getValorComprometidoEjecucion() {
        return this.valorComprometidoEjecucion;
    }

    public void setValorComprometidoEjecucion(double valorComprometidoEjecucion) {
        this.valorComprometidoEjecucion = valorComprometidoEjecucion;
    }

    public ContratoInteradministrativo getCurrentContract() {
        return this.currentContract;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdminAvaluosAsociadosAContratoMB init");
        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * consulta los avalúos que estén en trámite asociados al contrato con el que se está trabajando
     *
     * @author pedro.garcia
     */
    private ArrayList<Avaluo> consultarAvaluosEnTramitePorContrato() {

        ArrayList<Avaluo> answer = null;
        FiltroDatosConsultaAvaluo filtroConsulta;

        filtroConsulta = new FiltroDatosConsultaAvaluo();
        filtroConsulta.setEstadoTramiteAvaluo(ETramiteEstado.EN_PROCESO.getCodigo());

        answer = (ArrayList<Avaluo>) this.getAvaluosService().consultarAvaluosDeContrato(
            this.currentContract.getId(), filtroConsulta);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * calcula el dato valor comprometido en ejecución que es la sumatoria de los valores de los
     * avalúos en trámite asociados al contrato incrementados en el porcentaje de IVA
     *
     * PRE: se ha consultado la lista de avalúos en trámite asociados
     *
     * @author pedro.garcia
     * @modified christian.rodriguez se vuelve público para que pueda servir de listener para que al
     * cerrar la ventana de este caso de uso el valor peuda ser recalculado
     * @return
     */
    public void calcularValorComprometidoEjecucion() {

        double valorCotizadoTemp, valorCotizadoConIVATemp;
        this.valorComprometidoEjecucion = 0.0;

        if (this.avaluosEnTramiteAsociados != null) {
            for (Avaluo avaluo : this.avaluosEnTramiteAsociados) {
                valorCotizadoTemp = avaluo.getValorCotizacion();

                valorCotizadoConIVATemp = (avaluo.getValorIva() != null) ?
                    valorCotizadoTemp + (valorCotizadoTemp * avaluo.getValorIva().doubleValue()) :
                    valorCotizadoTemp;

                this.valorComprometidoEjecucion += valorCotizadoConIVATemp;
            }
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "aceptar" del dialogo donde se pide el nuevo valor de cotización del avalúo
     */
    public void modificarValorCotizacionAvaluo() {

        this.selectedAvaluo.setUsuarioLog(this.loggedInUser.getLogin());
        this.selectedAvaluo.setFechaLog(new Date(System.currentTimeMillis()));
        this.getAvaluosService().actualizarAvaluo(this.selectedAvaluo);

        this.avaluosEnTramiteAsociados = consultarAvaluosEnTramitePorContrato();

        this.selectedAvaluo = null;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado por el caso de uso 008
     *
     * @author pedro.garcia
     * @modified christian.rodriguez se cambia el nombre del método y se reinicializa la variable de
     * selección
     * @param currentContract {@link ContratoInteradministrativo} que se desea modificar
     */
    public void cargarDesdeCU008(ContratoInteradministrativo currentContract) {

        if (this.currentContract == null ||
             (this.currentContract != null && this.currentContract.getId() != currentContract
            .getId())) {

            this.currentContract = currentContract;
            this.avaluosEnTramiteAsociados = this.consultarAvaluosEnTramitePorContrato();
            this.calcularValorComprometidoEjecucion();

            this.numeroAvaluosEnTramite = (this.avaluosEnTramiteAsociados != null) ?
                this.avaluosEnTramiteAsociados
                    .size() : 0;

            this.selectedAvaluo = new Avaluo();
        }

    }

//end of class
}
