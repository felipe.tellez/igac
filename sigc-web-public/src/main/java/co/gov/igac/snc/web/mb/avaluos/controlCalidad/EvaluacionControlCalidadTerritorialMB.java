package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacion;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;

/**
 *
 * Managed bean del CU-SA-AC-165 Evaluar Control de Calidad de Territorial
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-165
 */
@Component("evaluarControlCalidadTerritorial")
@Scope("session")
public class EvaluacionControlCalidadTerritorialMB extends
    EvaluacionDesempenoTerritorialYControlCalidadMB {

    private static final long serialVersionUID = 2152471045674945582L;

    // -------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.actividadAsignacion = EAvaluoAsignacionProfActi.CC_TERRITORIAL
            .getCodigo();

        super.iniciarDatosGeneral();

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Listener del botón 'Consultar evaluación a avaluadores'. Muestra el documento asociado a la
     * evaluacion de desempeño asociada al avaluo seleccionado
     *
     * @author christian.rodriguez
     */
    public void consultarEvaluacionAvaluadores() {

        this.reporteDTO = null;

        Asignacion asignacionAvaluar = this.getAvaluosService()
            .consultarAsignacionActualAvaluo(
                this.avaluoSeleccionado.getId(),
                EAvaluoAsignacionProfActi.AVALUAR.getCodigo());

        AvaluoEvaluacion evaluacionAvaluadores = this.getAvaluosService()
            .consultarEvaluacionesPorAvaluoAsignacionConAtributos(
                this.avaluoSeleccionado.getId(),
                asignacionAvaluar.getId());

        if (evaluacionAvaluadores != null) {
            this.reporteDTO = this.reportsService
                .consultarReporteDeGestorDocumental(evaluacionAvaluadores
                    .getDocumento().getIdRepositorioDocumentos());
        }

        if (this.reporteDTO == null) {

            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "La evaluación asociada no pudo ser cargada.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para mover proceso. </br> Llamado desde la pagina
     * evaluarControlCalidadTerritorial.xhtml</br> Boton Mover Proceso
     *
     * @author christian.rodriguez
     */
    public void moverProceso() {

    }

}
