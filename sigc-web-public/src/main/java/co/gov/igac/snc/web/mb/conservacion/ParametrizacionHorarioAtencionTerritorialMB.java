package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.HorarioTerritorialDTO;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.util.Date;
import org.primefaces.context.RequestContext;

/**
 * Clase que hace de MB para manejar la parametrizacion de los horarios de atencion de territoriales
 * y Unidades Operativas
 *
 * @author lorena.salamanca
 * @CU-NP-CO-252
 *
 */
@Component("parametrizarHorarioTerritorial")
@Scope("session")
public class ParametrizacionHorarioAtencionTerritorialMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ParametrizacionHorarioAtencionTerritorialMB.class);

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Territorial seleccionada
     */
    private String territorialSeleccionada;

    /**
     * Lista de Territoriales del usuario autenticado
     */
    private List<SelectItem> listaTerritoriales;

    /**
     * Determina si esta habilitada la seleccion de la territorial
     */
    private boolean banderaSeleccionTerritorial;

    /**
     * Variable item list con los valores de las UOC de la territorial
     */
    private List<SelectItem> UOCItemList;

    /**
     * Determina si esta habilitada la seleccion de uocs
     */
    private boolean banderaSeleccionUOC;

    /**
     * Codigo de la UOC seleccionada
     */
    private String UOCseleccionadaCodigo;

    /**
     * Nombre de la UOC seleccionada
     */
    private String UOCseleccionadaNombre;

    /**
     * Codigo de la Territorial
     */
    private String codigoTerritorial;

    /**
     * Hora inicial de atencion en la mañana
     */
    private Date horaInicialManhana;

    /**
     * Hora final de atencion en la mañana
     */
    private Date horaFinalManhana;

    /**
     * Hora inicial de atencion en la tarde
     */
    private Date horaInicialTarde;

    /**
     * Hora final de atencion en la tarde
     */
    private Date horaFinalTarde;

    /**
     * Fecha hoy
     */
    private Date fechaHoy;

    /**
     * Lista de las UOC de la territorial
     */
    private List<EstructuraOrganizacional> uocs;

    private List<HorarioTerritorialDTO> horarioList;
    private HorarioTerritorialDTO[] diasSeleccionados;

    // ------------- Getters & Setters ------------------------------------
    public String getCodigoTerritorial() {
        return codigoTerritorial;
    }

    public void setCodigoTerritorial(String codigoTerritorial) {
        this.codigoTerritorial = codigoTerritorial;
    }

    public String getTerritorialSeleccionada() {
        return territorialSeleccionada;
    }

    public void setTerritorialSeleccionada(String territorialSeleccionada) {
        this.territorialSeleccionada = territorialSeleccionada;
    }

    public String getUOCseleccionadaNombre() {
        return UOCseleccionadaNombre;
    }

    public void setUOCseleccionadaNombre(String UOCseleccionadaNombre) {
        this.UOCseleccionadaNombre = UOCseleccionadaNombre;
    }

    public String getUOCseleccionadaCodigo() {
        return UOCseleccionadaCodigo;
    }

    public void setUOCseleccionadaCodigo(String UOCseleccionadaCodigo) {
        this.UOCseleccionadaCodigo = UOCseleccionadaCodigo;
    }

    public List<SelectItem> getUOCItemList() {
        return UOCItemList;
    }

    public void setUOCItemList(List<SelectItem> uOCItemList) {
        UOCItemList = uOCItemList;
    }

    public boolean isBanderaSeleccionUOC() {
        return banderaSeleccionUOC;
    }

    public void setBanderaSeleccionUOC(boolean banderaSeleccionUOC) {
        this.banderaSeleccionUOC = banderaSeleccionUOC;
    }

    public boolean isBanderaSeleccionTerritorial() {
        return banderaSeleccionTerritorial;
    }

    public void setBanderaSeleccionTerritorial(boolean banderaSeleccionTerritorial) {
        this.banderaSeleccionTerritorial = banderaSeleccionTerritorial;
    }

    public String getTerritorial() {
        return territorialSeleccionada;
    }

    public void setTerritorial(String territorial) {
        this.territorialSeleccionada = territorial;
    }

    public List<SelectItem> getListaTerritoriales() {
        return listaTerritoriales;
    }

    public void setListaTerritoriales(List<SelectItem> listaTerritoriales) {
        this.listaTerritoriales = listaTerritoriales;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<EstructuraOrganizacional> getUocs() {
        return uocs;
    }

    public void setUocs(List<EstructuraOrganizacional> uocs) {
        this.uocs = uocs;
    }

    public Date getHoraInicialManhana() {
        return horaInicialManhana;
    }

    public void setHoraInicialManhana(Date horaInicialManhana) {
        this.horaInicialManhana = horaInicialManhana;
    }

    public Date getHoraFinalManhana() {
        return horaFinalManhana;
    }

    public void setHoraFinalManhana(Date horaFinalManhana) {
        this.horaFinalManhana = horaFinalManhana;
    }

    public Date getHoraInicialTarde() {
        return horaInicialTarde;
    }

    public void setHoraInicialTarde(Date horaInicialTarde) {
        this.horaInicialTarde = horaInicialTarde;
    }

    public Date getHoraFinalTarde() {
        return horaFinalTarde;
    }

    public void setHoraFinalTarde(Date horaFinalTarde) {
        this.horaFinalTarde = horaFinalTarde;
    }

    public List<HorarioTerritorialDTO> getHorarioList() {
        return horarioList;
    }

    public void setHorarioList(List<HorarioTerritorialDTO> horarioList) {
        this.horarioList = horarioList;
    }

    public Date getFechaHoy() {
        return fechaHoy;
    }

    public void setFechaHoy(Date fechaHoy) {
        this.fechaHoy = fechaHoy;
    }

    public HorarioTerritorialDTO[] getDiasSeleccionados() {
        return diasSeleccionados;
    }

    public void setDiasSeleccionados(HorarioTerritorialDTO[] diasSeleccionados) {
        this.diasSeleccionados = diasSeleccionados;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("ParametrizacionHorarioAtencionTerritorialMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.setFechaHoy(new Date());
        this.cargarTerritoriales();
        this.cargarUOCs();
        //Al iniciar el CU, carga el horario del UOC del usuario si no tiene, carga el horario de la territorial
        if (this.usuario.getCodigoUOC() == null) {
            this.cargarHorario(usuario.getCodigoTerritorial());
        } else {
            this.cargarHorario(usuario.getCodigoUOC());
        }
    }

    /**
     * Metodo para inicializar las territoriales asociadas al usuario
     *
     * @author lorena.salamanca
     */
    public void cargarTerritoriales() {
        this.territorialSeleccionada = this.usuario.getDescripcionTerritorial();
        this.banderaSeleccionTerritorial = false;
        this.codigoTerritorial = this.usuario.getCodigoTerritorial();

        //Inicio lista de seleccion
        this.listaTerritoriales = new ArrayList<SelectItem>();
        this.listaTerritoriales.add(new SelectItem(this.territorialSeleccionada));

        if (listaTerritoriales.size() > 1) {
            this.banderaSeleccionTerritorial = true;
        }
        this.UOCseleccionadaCodigo = this.usuario.getCodigoTerritorial();
    }

    /**
     * Metodo para inicializar las UOC asociadas a la territorial del usuario
     *
     * @author lorena.salamanca
     */
    /*
     * @modified by leidy.gonzalez :: #16746 :: 07/04/2016 Se mosifica consulta de donde se cargan
     * la lista de UOC
     */
    public void cargarUOCs() {
        this.codigoTerritorial = this.usuario.getCodigoTerritorial();
        this.banderaSeleccionUOC = false;

        if (this.usuario.getCodigoUOC() != null) {
            setUocs(this.getGeneralesService().buscarUOCporCodigoTerritorial(this.codigoTerritorial));
        } else {

            setUocs(this.getGeneralesService().buscarUOCporTerritorialSinRolEnUOC(
                this.codigoTerritorial, ERol.LIDER_TECNICO));
        }
        this.UOCItemList = new ArrayList<SelectItem>();

        if (getUocs() != null && !getUocs().isEmpty()) {

            for (EstructuraOrganizacional eo : getUocs()) {
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }

        } else {
            setUocs(this.getGeneralesService().buscarUOCporCodigoTerritorial(this.codigoTerritorial));

            for (EstructuraOrganizacional eo : getUocs()) {
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        }

        if (!UOCItemList.isEmpty() && this.usuario.getCodigoUOC() == null) {
            this.banderaSeleccionUOC = true;
        }
        this.UOCseleccionadaCodigo = this.usuario.getCodigoUOC();
        this.refrescarHorarioByUOC();

    }

    /**
     * Metodo que asigna los valores de jornada continua a los dias seleccionados
     *
     * @author lorena.salamanca
     */
    public void establecerJornadaContinua() {
        if (this.validarHorarioJornadaContinua()) {
            for (HorarioTerritorialDTO horario : this.diasSeleccionados) {
                horario.setHoraInicialManhana(this.horaInicialManhana);
                horario.setHoraFinalManhana(null);
                horario.setHoraInicialTarde(null);
                horario.setHoraFinalTarde(this.horaFinalTarde);
            }
            this.guardarHorario();
            this.limpiarCampos();
        }
    }

    /**
     * Metodo que valida que los horarios seleccionados para Jornada Continua sean coherentes
     *
     * @author lorena.salamanca
     * @return
     */
    public boolean validarHorarioJornadaContinua() {
        //Si hace falta la hora inicial o final
        if (this.horaInicialManhana == null || this.horaFinalTarde == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("No puede faltar horario para mañana o tarde.");
            return false;
        }

        //Si la hora final es mas temprano que la hora inicial
        if (this.horaInicialManhana.after(horaFinalTarde)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("La hora final no puede suceder antes de la hora inicial.");
            return false;
        }
        return true;
    }

    /**
     * Metodo que asigna los valores de jornada dividida a los dias seleccionados
     *
     * @author lorena.salamanca
     */
    public void establecerJornadaDividida() {
        if (this.validarHorarioJornadaDividida()) {
            for (HorarioTerritorialDTO horario : this.diasSeleccionados) {
                horario.setHoraInicialManhana(this.horaInicialManhana);
                horario.setHoraFinalManhana(this.horaFinalManhana);
                horario.setHoraInicialTarde(this.horaInicialTarde);
                horario.setHoraFinalTarde(this.horaFinalTarde);
            }

            this.guardarHorario();
            this.limpiarCampos();
        }
    }

    /*
     * @autor leidy.gonzalez :: #16091 :: 22/03/2016 Metodo que limpia campos de jornadas ingresados
     */
    public void limpiarCampos() {
        this.horaInicialManhana = null;
        this.horaFinalManhana = null;
        this.horaInicialTarde = null;
        this.horaFinalTarde = null;
        this.diasSeleccionados = null;
    }

    /**
     * Metodo que valida que los horarios seleccionados para Jornada Dividida sean coherentes
     *
     * @author lorena.salamanca
     * @return
     */
    public boolean validarHorarioJornadaDividida() {
        //Si hace falta la hora inicial o final
        if (this.horaInicialManhana == null || this.horaFinalTarde == null ||
            this.horaFinalManhana == null || this.horaInicialTarde == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("No puede faltar horario para mañana o tarde.");
            return false;
        }

        //Si la hora final de la mañana es mas temprano que la hora inicial
        if (this.horaInicialManhana.after(horaFinalManhana)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "La hora final de la mañana no puede suceder antes de la hora inicial.");
            return false;
        }

        //Si la hora final de la tarde es mas temprano que la hora inicial
        if (this.horaInicialTarde.after(horaFinalTarde)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "La hora final de la tarde no puede suceder antes de la hora inicial.");
            return false;
        }

        //Si la hora inicial de la tarde es mas temprano que la hora final de la mañana
        if (this.horaFinalManhana.after(horaInicialTarde)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "La hora inicial de la tarde no puede suceder antes de la hora final de la mañana.");
            return false;
        }
        return true;
    }

    /**
     * Metodo que borra los valores de horarios de los dias seleccionados
     *
     * @author lorena.salamanca
     */
    public void limpiarHorario() {
        if (this.diasSeleccionados.length > 0) {
            for (HorarioTerritorialDTO horario : this.diasSeleccionados) {
                horario.setHoraInicialManhana(null);
                horario.setHoraFinalManhana(null);
                horario.setHoraInicialTarde(null);
                horario.setHoraFinalTarde(null);
            }
            this.diasSeleccionados = null;
            this.guardarHorario();
        }
    }

    /**
     * Metodo que persiste en la base de datos los horarios seleccionados para la territorial o UOC
     *
     * @author lorena.salamanca
     */
    public void guardarHorario() {
        try {
            if (this.getUOCseleccionadaCodigo() != null && !this.getUOCseleccionadaCodigo().
                isEmpty()) {
                this.getConservacionService().guardarActualizarHorarioAtencion(horarioList, this.
                    getUOCseleccionadaCodigo(), usuario);
                this.addMensajeInfo("El horario de " + this.getUOCseleccionadaNombre() +
                    " se actualizó exitosamente.");
            } else if (this.usuario.getCodigoTerritorial() != null && !this.usuario.
                getCodigoTerritorial().isEmpty()) {
                this.getConservacionService().guardarActualizarHorarioAtencion(horarioList,
                    this.usuario.getCodigoTerritorial(), usuario);
                this.UOCseleccionadaCodigo = null;
                this.addMensajeInfo("El horario de " + this.usuario.getDescripcionTerritorial() +
                    " se actualizó exitosamente.");
            }
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al guardar el horario de la territorial o UOC.");
            LOGGER.error("Error al guardar el horario de la territorial o UOC " + this.
                getCodigoTerritorial() + e);
        }
    }

    /**
     * Metodo que busca en la base de datos el horario de una territorial o UOC
     *
     * @author lorena.salamanca
     * @param codigoEstructuraOrganizacional
     */
    public void cargarHorario(String codigoEstructuraOrganizacional) {
        horarioList = this.getConservacionService().buscarHorarioAtencionByEstructuraOrganizacional(
            codigoEstructuraOrganizacional);
    }

    /**
     * Metodo que se ejecuta cuando se selecciona un valor en el combo de Territorial
     *
     * @author lorena.salamanca
     */
    /*
     * Se agrega parametros codigoTerritorial en el metodo refrescarHorarioByTerritorial @modified
     * by leidy.gonzalez :: 22/03/2016 :: #16091
     */
    public void refrescarHorarioByTerritorial(String codigoTerritorial) {
        this.cargarHorario(codigoTerritorial);
    }

    /**
     * Metodo que se ejecuta cuando se selecciona un valor en el combo de UOC
     *
     * @author lorena.salamanca
     */
    /*
     * @modified by leidy.gonzalez :: 22/03/2016 :: #16091 Se valida que so visualiza el horario si
     * existe UOC Seleccionada
     */
    public void refrescarHorarioByUOC() {

        if (this.UOCseleccionadaCodigo != null && !this.UOCseleccionadaCodigo.isEmpty()) {
            for (EstructuraOrganizacional estructura : uocs) {
                if (this.UOCseleccionadaCodigo.equals(estructura.getCodigo())) {

                    this.UOCseleccionadaNombre = estructura.getNombre();
                }
            }
            this.cargarHorario(this.getUOCseleccionadaCodigo());
        } else if (this.codigoTerritorial != null) {
            this.territorialSeleccionada = this.usuario.getDescripcionTerritorial();

            if (UOCItemList != null && !UOCItemList.isEmpty() && !this.banderaSeleccionUOC) {
                this.UOCseleccionadaCodigo = this.usuario.getCodigoTerritorial();
            }
            this.refrescarHorarioByTerritorial(this.codigoTerritorial);
        }

        this.diasSeleccionados = null;
    }
}
