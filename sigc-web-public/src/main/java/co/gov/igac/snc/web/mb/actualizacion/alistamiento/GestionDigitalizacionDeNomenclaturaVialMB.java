package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.persistence.util.EAsuntoCorreoElectronicoActualizacion;

/**
 *
 * @descrition Managed bean para la gestión de digitalización de nomenclatura vial con fines
 * catastrales.
 *
 * @author david.cifuentes
 *
 */
@Component("gestionarDigitalizacionDeNomenclaturaVial")
@Scope("session")
public class GestionDigitalizacionDeNomenclaturaVialMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionDigitalizacionDeNomenclaturaVialMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS------------- */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextService;

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable que almacena el responsable SIG seleccionado.
     */
    private String responsableSIG;

    /**
     * Variable para almacenar las observaciones cuando se solicita un ajuste al responsable SIG.
     */
    private String observaciones;

    /**
     * Lista de select items con los responsables SIG.
     */
    private List<SelectItem> listaResponsableSIG;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDTO;

    /**
     * Usuario logueado
     */
    private UsuarioDTO usuario;

    /**
     * Variable booleana para indicar si ya se radico la solicitud
     */
    private boolean radicadoBool;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Variable booleana para identificar si el proyecto es de nomenclatura víal o de perímetro
     * urbano.
     */
    private boolean esNomenclaturaVial;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String manzanaCodigo;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 21/11/12
        // Obtener la actualizacióin del arbol de tareas.
        this.actualizacionSeleccionada = this.getActualizacionService()
            .recuperarActualizacionPorId(450L);

        // TODO :: Revisar de donde sale ésta información :: 21/11/2012
        this.esNomenclaturaVial = true;

        responsableSIG = new String();
        observaciones = new String();

        // TODO :: david.cifuentes :: Revisar cargue del mapa cuando se defina
        // :: 31/10/12
        this.applicationClientName = this.contextService.getClientAppNameUrl();
        this.manzanaCodigo = "08001010300000439,08001010300000520,08001010300000779";
        this.moduleLayer = "ac";
        this.moduleTools = "ac";

        // Cargue de departamentos
        cargarListaResponsablesSIG();

        this.radicadoBool = false;
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public String getResponsableSIG() {
        return responsableSIG;
    }

    public void setResponsableSIG(String responsableSIG) {
        this.responsableSIG = responsableSIG;
    }

    public boolean isRadicadoBool() {
        return radicadoBool;
    }

    public void setRadicadoBool(boolean radicadoBool) {
        this.radicadoBool = radicadoBool;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public void setActualizacionSeleccionada(
        Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public boolean isEsNomenclaturaVial() {
        return esNomenclaturaVial;
    }

    public void setEsNomenclaturaVial(boolean esNomenclaturaVial) {
        this.esNomenclaturaVial = esNomenclaturaVial;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public List<SelectItem> getListaResponsableSIG() {
        return listaResponsableSIG;
    }

    public void setListaResponsableSIG(List<SelectItem> listaResponsableSIG) {
        this.listaResponsableSIG = listaResponsableSIG;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método que realiza el cargue de departamentos en el menú de normas.
     *
     * @author david.cifuentes
     */
    public void cargarListaResponsablesSIG() {
        this.listaResponsableSIG = new LinkedList<SelectItem>();
        this.listaResponsableSIG.add(new SelectItem("", "Seleccionar..."));

        // TODO :: david.cifuentes :: Cambiar la enumeración por el rol
        // ResponsableSIG :: 30/10/2012
        List<UsuarioDTO> responsablesSIG;
        responsablesSIG = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_ACTUALIZACION);

        if (responsablesSIG != null && !responsablesSIG.isEmpty()) {
            for (UsuarioDTO responsable : responsablesSIG) {
                this.listaResponsableSIG.add(new SelectItem(responsable
                    .getLogin(), responsable.getNombreCompleto()));
            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que genera el reporte de la solicitud de digitalización de nomenclatura vial o
     * perimetro urbano.
     *
     * @author david.cifuentes
     */
    public void generarReporteDeSolicitudDigitalizacion() {

        LOGGER.debug("REPORTE : GenerarReporteDeSolicitudDigitalizacion");

        // Nombre del reporte
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        // Reemplazar parametros para el reporte.
        Map<String, String> parameters = replaceReportsParameters();

        this.reporteDTO = this.reportsService.generarReporte(
            parameters, enumeracionReporte, this.usuario);

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que reemplaza los parametros para el reporte.
     *
     * @author david.cifuentes
     */
    public Map<String, String> replaceReportsParameters() {

        LOGGER.debug("REPORTE :: Replace parameters");
        Map<String, String> parameters = new HashMap<String, String>();

        try {

            // TODO :: david.cifuentes :: Parametros quemados, reemplazar cuando
            // se haya desarrollado el reporte :: 25/10/12
            parameters.put("NUMERO_RADICADO", "");

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            return null;
        }

        return parameters;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que radica la solicitud de digitalización de nomenclatura vial o perímetro urbano
     *
     * @author david.cifuentes
     */
    public void radicar() {

        // TODO :: david.cifuentes :: Implementar la funcionalidad del radicado,
        // revisar cómo se va a definir esto en Actualización :: 26/06/2013
        this.radicadoBool = true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que avanza en el proceso desde la pantalla gestionar digitalizacion de nomenclatura
     * vial
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String avanzarProcesoDesdeGestionar() {
//TODO :: david.cifuentes :: Implementar avance cuando se defina process :: 24/10/12

        try {
            this.tareasPendientesMB.init();
        } catch (Exception e) {
            LOGGER.debug("Error en el método avanzar proceso.");
            LOGGER.error(e.getMessage(), e);
        }
        return ConstantesNavegacionWeb.INDEX;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que avanza en el proceso desde la pantalla verificar digitalizacion de nomenclatura
     * vial.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String avanzarProcesoDesdeVerificar() {
//TODO :: david.cifuentes :: Implementar avance cuando se defina process :: 24/10/12

        try {
            this.tareasPendientesMB.init();
        } catch (Exception e) {
            LOGGER.debug("Error en el método avanzar proceso.");
            LOGGER.error(e.getMessage(), e);
        }
        return ConstantesNavegacionWeb.INDEX;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el cargue de los parametros para el envio del correo electrónico de la
     * órden de ajustes, dichos parametros los setea en el managed bean
     * VisualizacionCorreoActualizacionMB.
     *
     * @author david.cifuentes
     */
    public void cargarParametrosOrdenDeAjuste() {

        if (this.observaciones == null || this.observaciones.trim().isEmpty()) {
            this.addMensajeWarn("Por favor ingrese una observación.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
        // TODO :: david.cifuentes :: Ajustar los parámetros que están quemados
        // :: 20/11/2012

        if (this.responsableSIG == null || this.responsableSIG.trim().isEmpty()) {
            this.responsableSIG = "prucundi18";
        }

        // Asunto
        String asunto;
        // Listado de archivos adjuntos
        String[] adjuntos = null;
        // Nombre de los archivos adjuntos
        String[] titulosAdjuntos = null;
        // Plantilla de contenido
        String codigoPlantillaContenido;
        // Destinatarios
        String[] destinatarios = new String[]{"david.cifuentes@igac.gov.co"};
        // Remitente
        String remitente = "david.cifuentes@igac.gov.co";

        if (this.esNomenclaturaVial) {
            codigoPlantillaContenido = EPlantilla.MENSAJE_SOLICITUD_ORDEN_AJUSTES_PROYECTO_NOM_VIAL
                .getCodigo();
            asunto =
                EAsuntoCorreoElectronicoActualizacion.ASUNTO_SOLICITUD_ORDEN_AJUSTES_NOMENCLATURA_VIAL.
                    getAsunto();
        } else {
            codigoPlantillaContenido = EPlantilla.MENSAJE_SOLICITUD_ORDEN_AJUSTES_PERIMETRO_URBANO
                .getCodigo();
            asunto =
                EAsuntoCorreoElectronicoActualizacion.ASUNTO_SOLICITUD_ORDEN_AJUSTES_PERIMETRO_URBANO.
                    getAsunto();
        }
        // Parámetros de la plantilla de contenido
        UsuarioDTO usuarioDestinatario = this.getGeneralesService()
            .getCacheUsuario(this.responsableSIG);
        String[] parametrosPlantillaContenido = new String[4];
        parametrosPlantillaContenido[0] = usuarioDestinatario
            .getNombreCompleto();
        parametrosPlantillaContenido[1] = this.actualizacionSeleccionada
            .getMunicipio().getNombre();
        parametrosPlantillaContenido[2] = this.observaciones;
        parametrosPlantillaContenido[3] = this.usuario.getNombreCompleto();

        // Cargar parámetros en el managed bean del componente.
        VisualizacionCorreoActualizacionMB visualCorreoMB =
            (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                .getManagedBean("visualizacionCorreoActualizacion");
        visualCorreoMB.inicializarParametros(asunto, destinatarios, remitente,
            adjuntos, codigoPlantillaContenido, null,
            parametrosPlantillaContenido, titulosAdjuntos);
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el cargue de los parametros para el envio del correo electrónico en la
     * solicitud de digitalización y los setea en el managed bean
     * VisualizacionCorreoActualizacionMB.
     *
     * @author david.cifuentes
     */
    public void cargarParametrosSolicitudDigitalizacion() {

        // TODO :: david.cifuentes :: Ajustar los parámetros que están quemados
        // :: 20/11/2012
        // Asunto
        String asunto;
        // Listado de archivos adjuntos
        String[] adjuntos = null;
        // Nombre de los archivos adjuntos
        String[] titulosAdjuntos = null;
        // Plantilla de contenido
        String codigoPlantillaContenido;
        // Destinatarios
        String[] destinatarios = new String[]{"david.cifuentes@igac.gov.co"};
        // Remitente
        String remitente = "david.cifuentes@igac.gov.co";

        if (this.esNomenclaturaVial) {
            codigoPlantillaContenido = EPlantilla.MENSAJE_SOLICITUD_DIGITALIZACION_PROYECTO_NOM_VIAL
                .getCodigo();
            asunto =
                EAsuntoCorreoElectronicoActualizacion.ASUNTO_SOLICITUD_DIGITALIZACION_NOMENCLATURA_VIAL.
                    getAsunto();
        } else {
            codigoPlantillaContenido = EPlantilla.MENSAJE_SOLICITUD_DIGITALIZACION_PERIMETRO_URBANO
                .getCodigo();
            asunto =
                EAsuntoCorreoElectronicoActualizacion.ASUNTO_SOLICITUD_DIGITALIZACION_PERIMETRO_URBANO.
                    getAsunto();
        }

        // Set del parametro municipio en el asunto.
        // Se instancia el messageFormat.
        Locale colombia = new Locale("ES", "es_CO");
        MessageFormat messageFormat = new MessageFormat(asunto, colombia);
        asunto = messageFormat
            .format(new String[]{this.actualizacionSeleccionada
            .getMunicipio().getNombre()});

        // Parámetros de la plantilla de contenido
        UsuarioDTO usuarioDestionatario = this.getGeneralesService()
            .getCacheUsuario(this.responsableSIG);
        String[] parametrosPlantillaContenido = new String[4];
        parametrosPlantillaContenido[0] = usuarioDestionatario
            .getNombreCompleto();
        parametrosPlantillaContenido[1] = this.actualizacionSeleccionada
            .getMunicipio().getNombre();
        parametrosPlantillaContenido[2] = this.usuario.getNombreCompleto();

        // Cargar parámetros en el managed bean del componente.
        VisualizacionCorreoActualizacionMB visualCorreoMB =
            (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                .getManagedBean("visualizacionCorreoActualizacion");
        visualCorreoMB.inicializarParametros(asunto, destinatarios, remitente,
            adjuntos, codigoPlantillaContenido, null,
            parametrosPlantillaContenido, titulosAdjuntos);
    }

    /** --------------------------------------------------------------------- */
    /**
     * Action sobre el botón 'cerrar', redirige el arbol de tareas removiendo el managed bean
     * actual.
     *
     * @author david.cifuentes
     */
    public String cerrarPaginaPrincipal() {

        UtilidadesWeb.removerManagedBean("gestionarProyectoDeNomenclaturaVial");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

}
