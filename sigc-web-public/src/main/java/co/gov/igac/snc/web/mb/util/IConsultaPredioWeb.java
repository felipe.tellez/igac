/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.util;

import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;

/**
 * Interfaz que debe ser implementada por los MB que necesiten referencia a ConsultaPredioMB
 *
 * Es necesario utilizarla porque ConsultaPredioMB se autoremueve de la session (al cerrar la
 * ventana del composite component que lo usa como backing bean), y es necesario que los MB que
 * tengan referencia a ConsultaPredioMB la liberen (para evitar inconsistencia entre los datos de la
 * referencia y los datos del de sesión)
 *
 *
 * OJO: los mb que implementen esta interfaz deben llamar a
 * this.getConsultaPredioMB().adicionarListener(this); en el init. No se debe acceder al atributo de
 * tipo ConsultaPredioMB directamente, sino por medio del método this.getConsultaPredioMB(); excepto
 * en la implementación de los métodos removerReferencia y getConsultaPredioMB
 *
 * @author fredy.wilches
 */
public interface IConsultaPredioWeb {

    /**
     * Remueve la referencia al mb de consulta predio que se tenga en el mb que implemente esta
     * interfaz
     */
    public void removerReferencia();

    /**
     * Retorna la referencia al mb de consulta predio que se use en el MB que implemente esta. Si no
     * tiene una, la debe obtener de la sesión. Además, debe adicionar este mb como "listener" del
     * ConsultaPredioMB
     *
     * @author fredy.wilches
     * @documented pedro.garcia
     */
    public ConsultaPredioMB getConsultaPredioMB();

}
