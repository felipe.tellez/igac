/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Managed bean para los reportes prediales
 *
 * @author pedro.garcia
 * @version 2.0
 * @cu ???
 */
@Component("reportesPrediales")
@Scope("session")
public class ReportesPredialesMB extends SNCManagedBean {

    /**
     * número serial autogenerado
     */
    private static final long serialVersionUID = 847294206720158127L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportesPredialesMB.class);

    //------------- banderas  ----
    /**
     * se usa para evitar que haga una búsqueda inicial que sucede sin que debiera ser así al hacer
     * load del lazy datamodel cuando se ejecuta el constructor del mb
     */
    private boolean mustSearch;

    /**
     * para saber si se usa el combo de territorial en las búsquedas
     */
    private boolean seUsaTerritorial;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * listas con los valores de los combos de selección para la búsqueda
     */
    private ArrayList<SelectItem> territorialesItemList;
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;
    private ArrayList<SelectItem> usosConstruccionItemList;
    private ArrayList<SelectItem> destinosPredioItemList;
    private ArrayList<SelectItem> zonasGeoeconomicasPredioItemList;
    private ArrayList<SelectItem> zonasFisicasPredioItemList;
    private ArrayList<SelectItem> tiposPredioItemList;
    public ArrayList<SelectItem> tipificacionesPredioItemList;

    /**
     * variable donde se tienen los resultados de la búsqueda
     */
    private LazyDataModel<Object[]> lazyModel;

    //---------------     methods   ------------------------------
    public boolean isMustSearch() {
        return this.mustSearch;
    }

    public void setMustSearch(boolean mustSearch) {
        this.mustSearch = mustSearch;
    }

    public boolean isSeUsaTerritorial() {
        return this.seUsaTerritorial;
    }

    public void setSeUsaTerritorial(boolean seUsaTerritorial) {
        this.seUsaTerritorial = seUsaTerritorial;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return this.datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return this.territorialesItemList;
    }

    public void setTerritorialesItemList(ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }
//-------------

    public void setTerritorialesItemList(List<EstructuraOrganizacional> items) {
        LOGGER.debug("entra al que retorna la lista de T");

        for (EstructuraOrganizacional territorial : items) {
            this.territorialesItemList.add(new SelectItem(territorial
                .getCodigo(), territorial.getNombre()));
        }
    }
//----------------------------------------------------------------------------------

    public ArrayList<SelectItem> getDepartamentosItemList() {
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public ArrayList<SelectItem> getUsosConstruccionItemList() {
        return this.usosConstruccionItemList;
    }

    public void setUsosConstruccionItemList(ArrayList<SelectItem> usosConstruccionItemList) {
        this.usosConstruccionItemList = usosConstruccionItemList;
    }
//----------------------------

    public void setUsosConstruccionItemList(List<UsoConstruccion> usosConstruccionList) {

        for (UsoConstruccion uc : usosConstruccionList) {
            this.usosConstruccionItemList.add(new SelectItem(uc.getId(), uc.getNombre()));
        }
    }
//---------------------------------------------------------------------------------

    public ArrayList<SelectItem> getDestinosPredioItemList() {
        return this.destinosPredioItemList;
    }

    public void setDestinosPredioItemList(ArrayList<SelectItem> destinosPredioItemList) {
        this.destinosPredioItemList = destinosPredioItemList;
    }

    public ArrayList<SelectItem> getZonasGeoeconomicasPredioItemList() {
        return this.zonasGeoeconomicasPredioItemList;
    }

    public void setZonasGeoeconomicasPredioItemList(
        ArrayList<SelectItem> zonasGeoeconomicasPredioItemList) {
        this.zonasGeoeconomicasPredioItemList = zonasGeoeconomicasPredioItemList;
    }

    public ArrayList<SelectItem> getZonasFisicasPredioItemList() {
        return this.zonasFisicasPredioItemList;
    }

    public void setZonasFisicasPredioItemList(ArrayList<SelectItem> zonasFisicasPredioItemList) {
        this.zonasFisicasPredioItemList = zonasFisicasPredioItemList;
    }

    public ArrayList<SelectItem> getTiposPredioItemList() {
        return this.tiposPredioItemList;
    }

    public void setTiposPredioItemList(ArrayList<SelectItem> tiposPredioItemList) {
        this.tiposPredioItemList = tiposPredioItemList;
    }

    public ArrayList<SelectItem> getTipificacionesPredioItemList() {
        return this.tipificacionesPredioItemList;
    }

    public void setTipificacionesPredioItemList(ArrayList<SelectItem> tipificacionesPredioItemList) {
        this.tipificacionesPredioItemList = tipificacionesPredioItemList;
    }
//----------------------------

    public void setTipificacionesPredioItemList(List<TipificacionUnidadConst> tipificacConstrList) {

        for (TipificacionUnidadConst uc : tipificacConstrList) {
            this.tipificacionesPredioItemList.add(new SelectItem(uc.getId(), uc.getTipificacion()));
        }
    }
//------------------------------------------------------------------------------------------

    public LazyDataModel<Object[]> getLazyModel() {
        return this.lazyModel;
    }

    public void setLazyModel(LazyDataModel<Object[]> lazyModel) {
        this.lazyModel = lazyModel;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on ReportesPredialesMB's init ");

        this.mustSearch = false;

        List<UsoConstruccion> ucList;
        List<TipificacionUnidadConst> tucList;

        this.datosConsultaPredio = new FiltroDatosConsultaPredio();

        this.territorialesItemList = new ArrayList<SelectItem>();
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList = new ArrayList<SelectItem>();

        LOGGER.debug("hace el init de la lista de Territoriales");
        if (this.territorialesItemList == null || this.territorialesItemList.isEmpty()) {
            List<EstructuraOrganizacional> eoList;
            eoList = this.getGeneralesService().getCacheTerritoriales();
            this.setTerritorialesItemList(eoList);
        }

        //D: armar la lista de destinos
        this.destinosPredioItemList = new ArrayList<SelectItem>();
        List<Dominio> destinosPredioDominio = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_DESTINO);
        for (Dominio d : destinosPredioDominio) {
            // D: OJO el valor de un dato que corresponda a un dominio es el
            // código de ese dominio, no el id
            this.destinosPredioItemList.add(
                new SelectItem(d.getCodigo(), d.getCodigo() + "-" + d.getValor()));
        }

        //D: armar lista de usos de construcción
        this.usosConstruccionItemList = new ArrayList<SelectItem>();
        ucList = this.getConservacionService().obtenerUsosConstruccion();
        this.setUsosConstruccionItemList(ucList);

        //D: armar la lista de tipos
        this.tiposPredioItemList = new ArrayList<SelectItem>();
        List<Dominio> tiposPredioDominio = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_TIPO);
        for (Dominio d : tiposPredioDominio) {
            this.tiposPredioItemList.add(
                new SelectItem(d.getCodigo(), d.getCodigo() + "-" + d.getValor()));
        }

        //D: armar lista de tipificaciones de unidades de construcción
        this.tipificacionesPredioItemList = new ArrayList<SelectItem>();
        tucList = this.getConservacionService().obtenerTipificacionesUnidadConstruccion();
        this.setTipificacionesPredioItemList(tucList);

        LOGGER.debug(".. termina init");

    }

//end of class
}
