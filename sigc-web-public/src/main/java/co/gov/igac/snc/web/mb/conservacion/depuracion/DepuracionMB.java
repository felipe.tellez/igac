/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracionObservacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.DigitalizadorTramitesAsignadosDTO;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EActividadControlCalidad;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import org.primefaces.context.RequestContext;

/**
 * @description Managed bean para gestionar la depuración de un trámite.
 *
 * @version 1.0
 *
 * @author david.cifuentes
 */
@Component("depuracion")
@Scope("session")
public class DepuracionMB extends SNCManagedBean implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -2869096050311713220L;
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(DepuracionMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextService;

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable {@link Tramite} que almacena el trámite seleccionado de la lista de actividades.
     */
    private Tramite tramiteSeleccionado;

    /**
     * Variable {@link Actividad} que almacena la actividad seleccionada de la lista de actividades.
     */
    private Actividad actividad;

    /**
     * Variable {@link String} para cargar las razones del porque se ha enviado el trámite a
     * depuración geográfica.
     */
    private String razonezDepuracionGeografica;

    /**
     * Variable que almacena el último {@link TramiteDepuracion} registrado para el tramite
     * seleccionado.
     */
    private TramiteDepuracion ultimoTramiteDepuracion;

    /**
     * Variable {@link TramiteInconsistencia} que almacena la inconsistencia a remover de la lista
     * de inconsistencias del {@link TramiteDepuracion}
     */
    private TramiteInconsistencia inconsistenciaAEliminar;

    /**
     * Variable {@link String} con el nombre de la actividad a mostrar en la pantalla.
     */
    private String nombreActividad;

    /**
     * Lista de {@link TramiteDepuracionObservacion} que almacena las observaciones realizadas al
     * último {@link TramiteDepuracion} registrado
     */
    private List<TramiteDepuracionObservacion> observacionesRealizadas;

    /**
     * Lista de {@link TramiteInconsistencia} con las inconsistencias modificadas del
     * {@link Tramite}
     */
    private List<TramiteInconsistencia> inconsistenciasTramite;

    /**
     * Lista de {@link TramiteInconsistencia} con las inconsistencias iniciales del {@link Tramite}
     */
    private List<TramiteInconsistencia> inconsistenciasTramiteIniciales;

    /**
     * Lista de {@link TramiteInconsistencia} de la revisión una vez que se ha enviado el
     * {@link Tramite} al editor geográfico.
     */
    private List<TramiteInconsistencia> inconsistenciasTramiteDeLaRevision;

    /**
     * Lista de {@link DigitalizadorTramitesAsignadosDTO} con los digitalizadores de la territorial
     * con su carga de trámites.
     */
    private List<DigitalizadorTramitesAsignadosDTO> digitalizadores;

    /**
     * Digitalizador seleccionado al cual se le asignaran los trámites en depuración.
     */
    private DigitalizadorTramitesAsignadosDTO digitalizadorSeleccionado;

    /**
     * Rol del usuario actual.
     */
    private String rol;

    /**
     * Variable {@link Boolean} para determinar si a un trámite ya se le ha creado una replica o no
     * y con esto determinar hacia donde se debe mover el proceso.
     */
    private boolean existeReplicaBool;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - ActualizacionInformacionGeograficasMB");

        try {
            // Cargue del usuario.
            this.usuario = MenuMB.getMenu().getUsuarioDto();

            if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
                this.addMensajeError(
                    "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
                return;
            }

            Long idtramiteActual = this.tareasPendientesMB
                .getInstanciaSeleccionada().getIdObjetoNegocio();

            // obtener datos de la actividad
            this.actividad = this.tareasPendientesMB
                .buscarActividadPorIdTramite(idtramiteActual);

            this.tramiteSeleccionado = this.getTramiteService()
                .findTramitePruebasByTramiteId(idtramiteActual);

            this.nombreActividad = EActividadControlCalidad.EJECUTOR
                .getNombre();

            this.nombreActividad = this.actividad.getNombre();

            // Cargue inicial de las variables de depuración utilizadas en la
            // pantalla
            this.cargarDatosDepuracionDelTramite();

            // Cargue de digitalizadores
            List<UsuarioDTO> digitalizadoresUsuarios = (List<UsuarioDTO>) this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario
                        .getDescripcionEstructuraOrganizacional(),
                    ERol.DIGITALIZADOR_DEPURACION);

            if (digitalizadoresUsuarios != null && !digitalizadoresUsuarios.isEmpty()) {
                this.consultarCargueTramitesADigitalizadores(digitalizadoresUsuarios);
            }

            this.determinarRol();

            //Validación  si el tramite ya tiene replica
            Long replicaOriginalId = ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId();
            List<TramiteDocumento> td = this.tramiteSeleccionado
                .getTramiteDocumentos();
            this.existeReplicaBool = false;
            for (TramiteDocumento tdTemp : td) {
                if (tdTemp.getDocumento().getTipoDocumento().getId()
                    .equals(replicaOriginalId)) {
                    this.existeReplicaBool = true;
                }
            }

            this.verificarDigitalizadorAsignado();

        } catch (Exception e) {
            this.addMensajeError(
                "Ocurrió un error al buscar el trámite, por favor intente nuevamente");
            LOGGER.error(e.getMessage(), e);
        }
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public TramiteDepuracion getUltimoTramiteDepuracion() {
        return this.ultimoTramiteDepuracion;
    }

    public void setUltimoTramiteDepuracion(
        TramiteDepuracion ultimoTramiteDepuracion) {
        this.ultimoTramiteDepuracion = ultimoTramiteDepuracion;
    }

    public Tramite getTramiteSeleccionado() {
        return this.tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(
        List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public TramiteInconsistencia getInconsistenciaAEliminar() {
        return this.inconsistenciaAEliminar;
    }

    public void setInconsistenciaAEliminar(
        TramiteInconsistencia inconsistenciaAEliminar) {
        this.inconsistenciaAEliminar = inconsistenciaAEliminar;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramiteDeLaRevision() {
        return inconsistenciasTramiteDeLaRevision;
    }

    public void setInconsistenciasTramiteDeLaRevision(
        List<TramiteInconsistencia> inconsistenciasTramiteDeLaRevision) {
        this.inconsistenciasTramiteDeLaRevision = inconsistenciasTramiteDeLaRevision;
    }

    public String getNombreActividad() {
        return this.nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getRazonezDepuracionGeografica() {
        return this.razonezDepuracionGeografica;
    }

    public void setRazonezDepuracionGeografica(
        String razonezDepuracionGeografica) {
        this.razonezDepuracionGeografica = razonezDepuracionGeografica;
    }

    public List<DigitalizadorTramitesAsignadosDTO> getDigitalizadores() {
        return digitalizadores;
    }

    public void setDigitalizadores(List<DigitalizadorTramitesAsignadosDTO> digitalizadores) {
        this.digitalizadores = digitalizadores;
    }

    public boolean isExisteReplicaBool() {
        return existeReplicaBool;
    }

    public void setExisteReplicaBool(boolean existeReplicaBool) {
        this.existeReplicaBool = existeReplicaBool;
    }

    public DigitalizadorTramitesAsignadosDTO getDigitalizadorSeleccionado() {
        return digitalizadorSeleccionado;
    }

    public void setDigitalizadorSeleccionado(
        DigitalizadorTramitesAsignadosDTO digitalizadorSeleccionado) {
        this.digitalizadorSeleccionado = digitalizadorSeleccionado;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramiteIniciales() {
        return inconsistenciasTramiteIniciales;
    }

    public void setInconsistenciasTramiteIniciales(
        List<TramiteInconsistencia> inconsistenciasTramiteIniciales) {
        this.inconsistenciasTramiteIniciales = inconsistenciasTramiteIniciales;
    }

    public List<TramiteDepuracionObservacion> getObservacionesRealizadas() {
        return observacionesRealizadas;
    }

    public void setObservacionesRealizadas(
        List<TramiteDepuracionObservacion> observacionesRealizadas) {
        this.observacionesRealizadas = observacionesRealizadas;
    }

    public Actividad getActividad() {
        return this.actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método que realiza el cargue las inconsistencias geográficas y razones de depuración
     * existentes. El cargue de las inconsistencias se realiza a partir del {@link Tramite}.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public void cargarDatosDepuracionDelTramite() {

        this.observacionesRealizadas = new ArrayList<TramiteDepuracionObservacion>();
        this.razonezDepuracionGeografica = "";

        // Cargar razones de depuración y las inconsistencias geográficas.
        if (this.tramiteSeleccionado.getTramiteDepuracions() != null &&
             !this.tramiteSeleccionado.getTramiteDepuracions().isEmpty()) {

            // Realizar la búsqueda del registro más actual.
            Date fechaMayor = this.tramiteSeleccionado.getTramiteDepuracions()
                .get(0).getFechaLog();
            TramiteDepuracion tdUltimo = null;
            for (TramiteDepuracion td : this.tramiteSeleccionado
                .getTramiteDepuracions()) {
                if (td.getFechaLog().compareTo(fechaMayor) >= 0) {
                    // El registro más actual ahora es el de td.
                    fechaMayor = td.getFechaLog();
                    tdUltimo = td;
                }
            }
            if (tdUltimo != null) {
                this.ultimoTramiteDepuracion = tdUltimo;
                // Cargar razones de depuración.
                this.razonezDepuracionGeografica = tdUltimo
                    .getRazonesSolicitudDepuracion();
                // Cargar observaciones
                this.observacionesRealizadas = this.ultimoTramiteDepuracion
                    .getTramiteDepuracionObservacions();
            }
        }

        // Set de las inconsistencias geográficas del trámite.
        if (this.tramiteSeleccionado.getTramiteInconsistencias() != null &&
             !this.tramiteSeleccionado.getTramiteInconsistencias()
                .isEmpty()) {

            this.inconsistenciasTramite = this.tramiteSeleccionado
                .getTramiteInconsistencias();
            this.inconsistenciasTramiteIniciales = new ArrayList<TramiteInconsistencia>();
            for (TramiteInconsistencia ti : this.tramiteSeleccionado
                .getTramiteInconsistencias()) {
                this.inconsistenciasTramiteIniciales
                    .add((TramiteInconsistencia) ti.clone());
            }
        }

        if (this.ultimoTramiteDepuracion == null) {
            this.ultimoTramiteDepuracion = new TramiteDepuracion();
        }
        this.inconsistenciasTramiteDeLaRevision = new ArrayList<TramiteInconsistencia>();
        LOGGER.debug("Fin carga datos...");
    }

    // ------------------------------------------------ //
    /**
     * Método que remueve la inconsistencia seleccionada de la lista de
     * {@link TramiteInconsistencia} cargada con las inconsistencias del {@link TramiteDepuracion}.
     *
     * @author david.cifuentes
     */
    public void eliminarInconsistencia() {
        if (this.inconsistenciaAEliminar != null) {
            // Se remueve de la lista, más sin embargo el cambio se guarda al
            // dar click sobre el boton guardar de la pantalla actualizar
            // inconsistencias.
            this.inconsistenciasTramite.remove(this.inconsistenciaAEliminar);
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que guarda las inconsistencias actualizadas y las asocia al trámtie depuracion. nuevas
     * ingresadas en la pantalla de control de calidad de depuración.
     *
     * @author david.cifuentes
     */
    public void guardarInconsistencias() {
        try {
            // Set de las inconsistencias
            this.tramiteSeleccionado
                .setTramiteInconsistencias(this.inconsistenciasTramite);

            // Actualización del trámite con sus respectivas inconsistencias.
            this.getTramiteService()
                .actualizarTramite(this.tramiteSeleccionado);

            // Consulta del trámite actualizado.
            this.tramiteSeleccionado = this.getTramiteService()
                .findTramitePruebasByTramiteId(
                    this.tramiteSeleccionado.getId());

            // Se actualizan nuevamente las inconsistencias del trámite
            this.inconsistenciasTramiteIniciales = new ArrayList<TramiteInconsistencia>();
            for (TramiteInconsistencia ti : this.tramiteSeleccionado
                .getTramiteInconsistencias()) {
                this.inconsistenciasTramiteIniciales
                    .add((TramiteInconsistencia) ti.clone());
            }

            this.addMensajeInfo("Se guardaron las inconsistencias satisfactoriamente");

        } catch (Exception e) {
            LOGGER.error(
                "Error al almacenar las inconsistencias del trámite: ActualizacionInconsistenciaGeograficasMB#guardarInconsistencias",
                e);
            this.addMensajeError("Error al actualizar las inconsistencias del trámite");
        }
    }

    /**
     * Valida si es posible enviar el tramite actual a edicion geografica
     *
     * @author felipe.cadena
     */
    public void validarEnvioAlEditor() {

        //felipe.cadena :: 18-06-2015 :: #13111 :: Se valida si el tramite se puede enviar a realizar modificacion
        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
        if (!generalMB.validarEnvioModificacionGeograficaDepuracion(this.tramiteSeleccionado)) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.warn(
                "Error al almacenar las inconsistencias del trámite: ActualizacionInconsistenciaGeograficasMB#guardarInconsistencias");
        }

    }

    // ------------------------------------------------ //
    /**
     * Método que avanza el proceso al momento de actualizar las inconsistencias geográficas de los
     * predios asociados a un trámite.
     *
     * @author david.cifuentes
     * @return
     */
    public String avanzarProceso() {

        //felipe.cadena :: 18-06-2015 :: #13111 :: Se valida si el tramite se puede enviar a realizar modificacion
        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
        if (!generalMB.validarEnvioModificacionGeograficaDepuracion(this.tramiteSeleccionado)) {
            LOGGER.warn(
                "Error al almacenar las inconsistencias del trámite: ActualizacionInconsistenciaGeograficasMB#guardarInconsistencias");
            return "";
        }

        try {
            UsuarioDTO usuarioDestinoActividad = null;

            if (this.actividad
                .getNombre()
                .equals(
                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION)) {
                usuarioDestinoActividad = this.usuario;

                TramiteDigitalizacion td = new TramiteDigitalizacion();

                td.setTramite(this.tramiteSeleccionado);
                td.setUsuarioDigitaliza(usuarioDestinoActividad.getLogin());
                td.setUsuarioEnvia(this.usuario.getLogin());
                td.setUsuarioLog(this.usuario.getLogin());
                td.setFechaLog(new Date());
                td.setFechaEnvia(new Date());

                this.getTramiteService()
                    .guardarActualizarTramiteDigitalizacion(td);
            } else if (this.actividad
                .getNombre()
                .equals(ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_TOPOGRAFIA)) {
                usuarioDestinoActividad = this.usuario;
            } else if (this.actividad
                .getNombre()
                .equals(ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR)) {
                if (this.digitalizadorSeleccionado != null) {
                    usuarioDestinoActividad = this.getGeneralesService()
                        .getCacheUsuario(
                            this.digitalizadorSeleccionado
                                .getFuncionarioId());

                    TramiteDigitalizacion td = new TramiteDigitalizacion();

                    td.setTramite(this.tramiteSeleccionado);
                    td.setUsuarioDigitaliza(usuarioDestinoActividad.getLogin());
                    td.setUsuarioEnvia(this.usuario.getLogin());
                    td.setUsuarioLog(this.usuario.getLogin());
                    td.setFechaLog(new Date());
                    td.setFechaEnvia(new Date());

                    this.getTramiteService()
                        .guardarActualizarTramiteDigitalizacion(td);
                } else {
                    usuarioDestinoActividad = this.usuario;
                }
            }

            this.getConservacionService()
                .enviarAModificacionInformacionGeograficaSincronica(
                    this.actividad, usuarioDestinoActividad);

            UtilidadesWeb.removerManagedBean("depuracion");
            this.tareasPendientesMB.init();
            return ConstantesNavegacionWeb.INDEX;

        } catch (Exception e) {
            LOGGER.error("Error al avanzar el proceso en DepuracionMB#avanzarProceso", e);
            this.addMensajeError("No fue posible avanzar el proceso para el trámite seleccionado.");
            return null;
        }
    }

    // ------------------------------------------------ //
    /**
     * Método para retornar al arbol de tareas, action del botón "cerrar" que forza el init del MB.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String cerrar() {
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb
            .getManagedBean("tareasPendientes");
        tareasPendientesMB.cerrar();
        return ConstantesNavegacionWeb.INDEX;
    }

    // ------------------------------------------------ //
    /**
     * Método para determinar el tipo de rol de la actividad y en base a eso determinar el tipo de
     * funcionario que se va a asignar.
     *
     * @author felipe.cadena
     */
    public void determinarRol() {

        String[] roles = this.usuario.getRoles();
        for (int i = 0; i < roles.length; i++) {

            if (roles[i].equals(ERol.RESPONSABLE_SIG.toString())) {
                this.rol = ERol.RESPONSABLE_SIG.toString();
            }
            if (roles[i].equals(ERol.RESPONSABLE_CONSERVACION.toString())) {
                this.rol = ERol.RESPONSABLE_CONSERVACION.toString();
            }
            if (roles[i].equals(ERol.DIRECTOR_TERRITORIAL.toString())) {
                this.rol = ERol.DIRECTOR_TERRITORIAL.toString();
            }
            if (roles[i].equals(ERol.DIGITALIZADOR.toString())) {
                this.rol = ERol.DIGITALIZADOR_DEPURACION.toString();
            }
            if (roles[i].equals(ERol.DIGITALIZADOR_DEPURACION.toString())) {
                this.rol = ERol.DIGITALIZADOR_DEPURACION.toString();
            }
            if (roles[i].equals(ERol.TOPOGRAFO.toString())) {
                this.rol = ERol.TOPOGRAFO.toString();
            }
            if (roles[i].equals(ERol.EJECUTOR_TRAMITE.toString())) {
                this.rol = ERol.EJECUTOR_TRAMITE.toString();
            }
        }
    }

    // ------------------------------------------------ //
    /**
     * Método para realizar el cargue de trámites en conservación y depuración que tienen los
     * digitalizadores de la territorial.
     *
     * @author david.cifuentes
     */
    public void consultarCargueTramitesADigitalizadores(
        List<UsuarioDTO> digitalizadoresUsuarios) {

        this.digitalizadores = new ArrayList<DigitalizadorTramitesAsignadosDTO>();
        DigitalizadorTramitesAsignadosDTO digitalizadorT = null;
        Map<EParametrosConsultaActividades, String> filtros = null;
        List<Actividad> actividades = null;
        List<TramiteDepuracion> td = null;

        for (UsuarioDTO d : digitalizadoresUsuarios) {

            if (d.getLogin() != null) {
                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(
                    EParametrosConsultaActividades.PROPIETARIO_ACTIVIDAD,
                    d.getLogin());
                filtros.
                    put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
                try {
                    actividades = this.getProcesosService()
                        .consultarListaActividades(filtros);

                    digitalizadorT = new DigitalizadorTramitesAsignadosDTO();
                    digitalizadorT.setFuncionario(d.getNombreCompleto());
                    digitalizadorT.setFuncionarioId(d.getLogin());

                    if (actividades != null && !actividades.isEmpty()) {
                        td = this.getTramiteService()
                            .buscarTramiteDepuracionPorDigitalizador(
                                d.getLogin());
                        if (td != null && !td.isEmpty()) {
                            digitalizadorT.setTramitesDepuracion(Long
                                .valueOf(td.size()));
                            digitalizadorT.setTramitesConservacion(Long
                                .valueOf(actividades.size() - td.size()));

                        } else {
                            digitalizadorT.setTramitesConservacion(Long
                                .valueOf(actividades.size()));
                            digitalizadorT.setTramitesDepuracion(0L);
                        }
                    } else {
                        digitalizadorT.setTramitesConservacion(0L);
                        digitalizadorT.setTramitesDepuracion(0L);
                    }
                    this.digitalizadores.add(digitalizadorT);
                } catch (ExcepcionSNC ex) {
                    LOGGER.error("Error consultando la lista de actividades para el usuario: " +
                         d.getLogin(), ex);
                    return;
                }
            }
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que verifica si el trámite tiene un digitalizador asignado.
     *
     * @author david.cifuentes
     */
    public void verificarDigitalizadorAsignado() {

        this.digitalizadorSeleccionado = null;

        if (ERol.TOPOGRAFO.toString().equals(this.rol) ||
             ERol.DIGITALIZADOR_DEPURACION.toString().equals(this.rol) ||
             ERol.DIGITALIZADOR.toString().equals(this.rol)) {
            this.digitalizadorSeleccionado = new DigitalizadorTramitesAsignadosDTO();
            this.digitalizadorSeleccionado.setFuncionario(this.usuario
                .getNombreCompleto());
            this.digitalizadorSeleccionado.setFuncionarioId(this.usuario
                .getLogin());
        } else {
            List<TramiteDigitalizacion> tramiteDigitalizacions = this
                .getTramiteService().buscarTramiteDigitalizacionPorTramite(
                    this.tramiteSeleccionado.getId());

            if (tramiteDigitalizacions != null &&
                 !tramiteDigitalizacions.isEmpty() &&
                 tramiteDigitalizacions.get(0) != null &&
                 tramiteDigitalizacions.get(0).getUsuarioDigitaliza() != null) {
                UsuarioDTO digitalizador = this.getGeneralesService()
                    .getCacheUsuario(
                        tramiteDigitalizacions.get(0)
                            .getUsuarioDigitaliza());
                if (digitalizador != null) {
                    this.digitalizadorSeleccionado = new DigitalizadorTramitesAsignadosDTO();
                    this.digitalizadorSeleccionado.setFuncionario(digitalizador
                        .getNombreCompleto());
                    this.digitalizadorSeleccionado
                        .setFuncionarioId(digitalizador.getLogin());
                }
            }
        }
    }

    // ------------------------------------------------ //
    /**
     * Método usado para determinar si se debe avanzar el proceso directamente, o si se debe
     * seleccionar un rol de digitalización o topografo.
     *
     * @author david.cifuentes
     */
    public boolean isAvanzarProcesoDirecto() {

        String[] roles = this.usuario.getRoles();
        for (int i = 0; i < roles.length; i++) {
            if (ERol.TOPOGRAFO.toString().equals(roles[i]) ||
                 ERol.DIGITALIZADOR_DEPURACION.toString()
                    .equals(roles[i]) ||
                 ERol.DIGITALIZADOR.toString().equals(roles[i])) {
                return true;
            } else if (ERol.EJECUTOR_TRAMITE.toString().equals(roles[i]) &&
                 this.digitalizadorSeleccionado != null) {
                return true;
            }
        }
        return false;
    }

}
