/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.WordUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.avaluos.GttOfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.util.CalculosEstadisticos;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaOfertasInmob;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.CalculaEstadisticasMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author pedro.garcia
 */
@Component("consultaOfertasInmob")
@Scope("session")
public class ConsultaOfertasInmobiliariasMB extends SNCManagedBean {

    private static final long serialVersionUID = 5179769835166965215L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ConsultaOfertasInmobiliariasMB.class);

    public static final String ID = "id";

    public static final String SEPARATOR_WORD = " ";

    //----------    services   ------
    @Autowired
    private CombosDeptosMunisMB combosDM;

    @Autowired
    private CalculaEstadisticasMB calculaEstadisticasMBService;

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private RegistroOfertaInmobiliariaMB registroOfertaInmobiliaria;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    private FiltroDatosConsultaOfertasInmob datosConsulta;

    /**
     * variable donde se consultan los resultados de forma lazy. Esta guarda
     */
    private LazyDataModel<OfertaInmobiliaria> resultadosBusquedaLazyModel;

    private OfertaInmobiliaria[] selectedOfertas;

    /**
     * WidgetVar destino de detalle de oferta inmobiliaria
     */
    private String destinoDetallOfertaIWV;

    /**
     * Id de la oferta seleccionada para visualizar el detatalle
     */
    private Long ofertaActualDetalleId;

    /**
     * nombre de la variable que el usuario quiere adicionar para calcular medidas estadísticas
     * correspondientes
     */
    private String nombreVariable;

    /**
     * varibles que el usuario aún no ha adicionado para hacer calculos estadisticos y que puede
     * adicionar como una nueva columna en la tabla de ofertas inmobiliarias vs variable
     */
    private List<SelectItem> variablesNumericasDisponibles;

    /**
     * Variable que el usuario selecciono para adicionar o eliminar, la variable corresponde a una
     * columna de la tabla ofertas inmobiliarias vs variable
     */
    private SelectItem variableNumericaDisponible;

    /**
     * lista de atributos de la clase Oferta Inmobiliaria sobre las que se puede hacer un cálculo
     * estadístico
     */
    private List<String> variablesNumericas;

    /**
     * variable que corresponde a las columnas de la tabla ofertas inmoliarias vs variable, la
     * columna correponde a una variable sobre la que se quiere hacer un cálculo estadístico
     */
    private List<String> columns;

    /**
     * variable que corresponde a las columnas de la tabla de resultado de los cálculos
     * estadísticos, la columna correponde a una variable sobre la que se quiere hacer un cálculo
     * estadístico
     */
    private List<String> columnsResultado;

    /**
     * Contiene los valores de un atributo de la clase Oferta Inmobiliaria para un objeto especifico
     */
    private List<Double[]> datos;

    /**
     * Contiene los resultados de los calculos estadisticos
     */
    private List<Object[]> resultadoCalculosEstadisticos;

    /**
     * variable para hacer la reflexión de la clase Oferta Inmobiliaria
     */
    private Class clase;

    /**
     * variable que almacena el usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena el url del archivo excel de las ofertas inmobiliarias seleccionadas
     */
    private String urlArchivo;

    /**
     * Variable para el reporte de ofertas seleccionadas
     */
    private StreamedContent reporteEstadisticasOfertasSeleccionadas;

    /**
     * Variable para el reporte de información de ofertas
     */
    private StreamedContent reporteExportarOfertasSeleccionadas;

    /**
     * se usa para controlar que no haga la búsqueda inicial que hace primefaces para la tabla, y
     * para que cuando haya un error de validación tampoco haga la búsqueda
     */
    private boolean mustSearch;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Esta variable es para el parametro de las capas del mapa
     */
    private String moduleLayer;
    /**
     * Esta variable es para el parametro de las herramientas del mapa
     */
    private String moduleTools;

    //----------   listas de combos  ------
    private ArrayList<SelectItem> ofertaTipoPredioItemList;

    private ArrayList<SelectItem> ofertaEstratoItemList;

    private ArrayList<SelectItem> ofertaTipoInmuebleItemList;

    private ArrayList<SelectItem> ofertaDestinoItemList;

    private ArrayList<SelectItem> ofertaTipoInvestigacionItemList;

    private ArrayList<SelectItem> ofertaTipoInvestigacionIngresoItemList;

    private ArrayList<SelectItem> ofertaCondicionJuridicaItemList;

    private ArrayList<SelectItem> ofertaVetustezItemList;

    //------   banderas   -----
    private boolean mostrarPanelBusquedaAvanzada;

    /**
     * variable para determinar si mostrar el resultado de los calculos estadisticos
     */
    private boolean mostrarResultadoCalculosEstadisticos;

    /**
     * variable para determinar si mostrar el boton de exportar a excel
     */
    private boolean mostrarExportarAExcel;

    /**
     * Variable que indica que se deben deshabilitar las opciones relacionadas con edición y
     * consulta exportación todas las consultas
     */
    private boolean banderaEsConsultaRestringida;

    /**
     * Variable que indiac que este caso de uso fue ejecutado desde el caso de uso 005 de avalúos
     */
    private boolean banderaEsConsultaDesdeCotizarCostoAvaluo;

    //------------    methods  --------
    public ArrayList<SelectItem> getOfertaTipoPredioItemList() {
        return this.ofertaTipoPredioItemList;
    }

    public void setOfertaTipoPredioItemList(ArrayList<SelectItem> ofertaTipoPredioList) {
        this.ofertaTipoPredioItemList = ofertaTipoPredioList;
    }

    public ArrayList<SelectItem> getOfertaTipoInmuebleItemList() {
        return this.ofertaTipoInmuebleItemList;
    }

    public void setOfertaTipoInmuebleItemList(ArrayList<SelectItem> ofertaTipoInmuebleItemList) {
        this.ofertaTipoInmuebleItemList = ofertaTipoInmuebleItemList;
    }

    public ArrayList<SelectItem> getOfertaDestinoItemList() {
        return this.ofertaDestinoItemList;
    }

    public void setOfertaDestinoItemList(ArrayList<SelectItem> ofertaDestinoItemList) {
        this.ofertaDestinoItemList = ofertaDestinoItemList;
    }

    public ArrayList<SelectItem> getOfertaTipoInvestigacionItemList() {
        return this.ofertaTipoInvestigacionItemList;
    }

    public String getDestinoDetallOfertaIWV() {
        return destinoDetallOfertaIWV;
    }

    public void setDestinoDetallOfertaIWV(String destinoDetallOfertaIWV) {
        this.destinoDetallOfertaIWV = destinoDetallOfertaIWV;
    }

    public Long getOfertaActualDetalleId() {
        return ofertaActualDetalleId;
    }

    public void setOfertaActualDetalleId(Long ofertaActualDetalleId) {
        this.ofertaActualDetalleId = ofertaActualDetalleId;
    }

    public void setOfertaTipoInvestigacionItemList(
        ArrayList<SelectItem> ofertaTipoInvestigacionItemList) {
        this.ofertaTipoInvestigacionItemList = ofertaTipoInvestigacionItemList;
    }

    public ArrayList<SelectItem> getOfertaTipoInvestigacionIngresoItemList() {
        return this.ofertaTipoInvestigacionIngresoItemList;
    }

    public void setOfertaTipoInvestigacionIngresoItemList(
        ArrayList<SelectItem> ofertaTipoInvestigacionIngresoItemList) {
        this.ofertaTipoInvestigacionIngresoItemList = ofertaTipoInvestigacionIngresoItemList;
    }

    public ArrayList<SelectItem> getOfertaCondicionJuridicaItemList() {
        return this.ofertaCondicionJuridicaItemList;
    }

    public void setOfertaCondicionJuridicaItemList(
        ArrayList<SelectItem> ofertaCondicionJuridicaItemList) {
        this.ofertaCondicionJuridicaItemList = ofertaCondicionJuridicaItemList;
    }

    public ArrayList<SelectItem> getOfertaVetustezItemList() {
        return this.ofertaVetustezItemList;
    }

    public void setOfertaVetustezItemList(ArrayList<SelectItem> ofertaVetustezItemList) {
        this.ofertaVetustezItemList = ofertaVetustezItemList;
    }

    public ArrayList<SelectItem> getOfertaEstratoItemList() {
        return this.ofertaEstratoItemList;
    }

    public void setOfertaEstratoItemList(ArrayList<SelectItem> ofertaEstratoItemList) {
        this.ofertaEstratoItemList = ofertaEstratoItemList;
    }

    public OfertaInmobiliaria[] getSelectedOfertas() {
        return this.selectedOfertas;
    }

    public void setSelectedOfertas(OfertaInmobiliaria[] selectedOfertas) {
        this.selectedOfertas = selectedOfertas;
    }

    public LazyDataModel<OfertaInmobiliaria> getResultadosBusquedaLazyModel() {
        return this.resultadosBusquedaLazyModel;
    }

    public void setResultadosBusquedaLazyModel(LazyDataModel<OfertaInmobiliaria> lazyModel) {
        this.resultadosBusquedaLazyModel = lazyModel;
    }

    public boolean isMostrarPanelBusquedaAvanzada() {
        return this.mostrarPanelBusquedaAvanzada;
    }

    public void setMostrarPanelBusquedaAvanzada(boolean mostrarPanelBusquedaAvanzada) {
        this.mostrarPanelBusquedaAvanzada = mostrarPanelBusquedaAvanzada;
    }

    public FiltroDatosConsultaOfertasInmob getDatosConsulta() {
        return this.datosConsulta;
    }

    public void setDatosConsulta(FiltroDatosConsultaOfertasInmob datosConsulta) {
        this.datosConsulta = datosConsulta;
    }

    public String getNombreVariable() {
        return this.nombreVariable;
    }

    public void setNombreVariable(String nombreVariable) {
        this.nombreVariable = nombreVariable;
    }

    public List<SelectItem> getVariablesNumericasDisponibles() {
        return this.variablesNumericasDisponibles;
    }

    public void setVariablesNumericasDisponibles(
        List<SelectItem> variablesNumericasDisponibles) {
        this.variablesNumericasDisponibles = variablesNumericasDisponibles;
    }

    public SelectItem getVariableNumericaDisponible() {
        return this.variableNumericaDisponible;
    }

    public void setVariableNumericaDisponible(SelectItem variableNumericaDisponible) {
        this.variableNumericaDisponible = variableNumericaDisponible;
    }

    public List<Object[]> getResultadoCalculosEstadisticos() {
        return this.resultadoCalculosEstadisticos;
    }

    public void setResultadoCalculosEstadisticos(
        List<Object[]> resultadoCalculosEstadisticos) {
        this.resultadoCalculosEstadisticos = resultadoCalculosEstadisticos;
    }

    public boolean isMostrarResultadoCalculosEstadisticos() {
        return this.mostrarResultadoCalculosEstadisticos;
    }

    public void setMostrarResultadoCalculosEstadisticos(
        boolean mostrarResultadoCalculosEstadisticos) {
        this.mostrarResultadoCalculosEstadisticos = mostrarResultadoCalculosEstadisticos;
    }

    public boolean isMostrarExportarAExcel() {
        return mostrarExportarAExcel;
    }

    public void setMostrarExportarAExcel(boolean mostrarExportarAExcel) {
        this.mostrarExportarAExcel = mostrarExportarAExcel;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<String> getColumnsResultado() {
        return columnsResultado;
    }

    public void setColumnsResultado(List<String> columnsResultado) {
        this.columnsResultado = columnsResultado;
    }

    public List<Double[]> getDatos() {
        return datos;
    }

    public void setDatos(List<Double[]> datos) {
        this.datos = datos;
    }

    public String getUrlArchivo() {
        return urlArchivo;
    }

    public void setUrlArchivo(String urlArchivo) {
        this.urlArchivo = urlArchivo;
    }

    /**
     * Genera el documento con las estadísticas calculadas para las ofertas seleccionadas
     *
     * @author javier.aponte
     * @modifiedby christian.rodriguez
     * @return Documento con las estadisticas
     */
    public StreamedContent getReporteEstadisticasOfertasSeleccionadas() {
        this.exportarOfertasAExcel(this.selectedOfertas, true, false);
        return reporteEstadisticasOfertasSeleccionadas;
    }

    /**
     * Genera el documento con la información de las ofertas seleccionadas
     *
     * @author javier.aponte
     * @modifiedby christian.rodriguez
     * @return Documento con las ofertas seleccionadas
     */
    public StreamedContent getReporteExportarOfertasSeleccionadas() {
        this.exportarOfertasAExcel(this.selectedOfertas, false, true);
        return reporteExportarOfertasSeleccionadas;
    }

    /**
     * Genera el documento con la información de todas las ofertas de la busqueda
     *
     * @author christian.rodriguez
     * @return Documento con todas las ofertas de la busqueda
     */
    public StreamedContent getReporteExportarTodasOfertas() {

        List<OfertaInmobiliaria> ofertas = this.getAvaluosService().
            buscarOfertasInmobiliariasPorFiltro(
                this.datosConsulta, null, null, 0, Integer.MAX_VALUE);

        this.exportarOfertasAExcel(ofertas.toArray(new OfertaInmobiliaria[0]), false, true);
        return reporteExportarOfertasSeleccionadas;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public boolean isBanderaEsConsultaRestringida() {
        return this.banderaEsConsultaRestringida;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * con los lazy datamodel se necesita un constructor para hacer la inicialización del mismo
     *
     * @version 2.0
     */
    public ConsultaOfertasInmobiliariasMB() {

        this.resultadosBusquedaLazyModel = new LazyDataModel<OfertaInmobiliaria>() {

            private static final long serialVersionUID = -2023737548810296238L;

            @Override
            public List<OfertaInmobiliaria> load(int first, int pageSize, String sortField,
                SortOrder sortOrder,
                Map<String, String> filters) {
                List<OfertaInmobiliaria> answer = new ArrayList<OfertaInmobiliaria>();

                populateOfertasLazyly(answer, first, pageSize, sortField, sortOrder.toString());

                return answer;
            }
        };
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {

        LOGGER.debug("init de ConsultaOfertasInmobiliariasMB");

        this.datosConsulta = new FiltroDatosConsultaOfertasInmob();
        this.mustSearch = false;

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        //---- inicialización de valores de listas para los selects
        this.initSelectItemLists();

        this.datos = new ArrayList<Double[]>();

        this.columns = new ArrayList<String>();

        this.columnsResultado = new ArrayList<String>();

        this.resultadoCalculosEstadisticos = new ArrayList<Object[]>();

        this.inicializarVariablesVisorGIS();
    }

    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        //se pone en of para que muestre la capa del mapa de ofertas inmobiliarias
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        //no se pone en of porque en este caso de uso no hay permiso para edicion 
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * método que hace llena la lista que usa el lazy datamodel para mostrar los resultados de la
     * búsqueda
     *
     * @author pedro.garcia
     *
     * @param answer es la lista que en la que se guardan los resultados
     * @param first índice de la primera fila de resultados
     * @param pageSize número de registros que va a traer
     */
    private void populateOfertasLazyly(List<OfertaInmobiliaria> answer, int first, int pageSize,
        String sortField, String sortOrder) {

        List<OfertaInmobiliaria> rows;
        int size;

        if (this.mustSearch) {
            rows = this.getAvaluosService().buscarOfertasInmobiliariasPorFiltro(
                this.datosConsulta, sortField, sortOrder, first, pageSize);
            size = rows.size();

            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Buscar" Hace las validaciones que no son posibles desde el cliente
     *
     * @author pedro.garcia
     */
    public void buscarOfertas() {

        boolean error = false;

        this.mustSearch = true;

        //D: validar que las fechas finales no sean menores que las iniciales
        if (this.datosConsulta.getFechaOfertaDesde() != null) {
            if (this.datosConsulta.getFechaOfertaHasta() == null) {
                this.datosConsulta.setFechaOfertaHasta(new Date(System.currentTimeMillis()));
            } else {
                if (this.datosConsulta.getFechaOfertaHasta().getTime() <
                    this.datosConsulta.getFechaOfertaDesde().getTime()) {
                    this.addMensajeError(
                        "La fecha de la oferta 'Desde' no debe ser mayor que la fecha 'Hasta'");
                    error = true;
                }
            }
        }

        if (this.datosConsulta.getFechaIngresoOfertaDesde() != null) {
            if (this.datosConsulta.getFechaIngresoOfertaHasta() == null) {
                this.datosConsulta.setFechaIngresoOfertaHasta(new Date(System.currentTimeMillis()));
            } else {
                if (this.datosConsulta.getFechaIngresoOfertaHasta().getTime() <
                    this.datosConsulta.getFechaIngresoOfertaDesde().getTime()) {
                    this.addMensajeError(
                        "La fecha de ingreso de la oferta 'Desde' no debe ser mayor que la fecha de ingreso 'Hasta'");
                    error = true;
                }
            }
        }

        //D: validar que si se dio una fecha final se haya dado una fecha inicial
        if (this.datosConsulta.getFechaOfertaHasta() != null) {
            if (this.datosConsulta.getFechaOfertaDesde() == null) {
                this.
                    addMensajeError("Si da una fecha final de la oferta debe dar una fecha inicial");
                error = true;
            }
        }
        if (this.datosConsulta.getFechaIngresoOfertaHasta() != null) {
            if (this.datosConsulta.getFechaIngresoOfertaDesde() == null) {
                this.addMensajeError(
                    "Si da una fecha final de ingreso de la oferta debe dar una fecha inicial");
                error = true;
            }
        }

        //D: validar que si escoge algún dato de consulta que tenga rango, el valor desde sea menor
        //  o igual que el valor hasta
        // precio:
        if (this.datosConsulta.getPrecioOfertaDesde() != null &&
            this.datosConsulta.getPrecioOfertaDesde().compareTo(Double.valueOf(0)) != 0) {
            if (this.datosConsulta.getPrecioOfertaHasta() != null &&
                this.datosConsulta.getPrecioOfertaHasta().compareTo(Double.valueOf(0)) != 0) {
                if (this.datosConsulta.getPrecioOfertaDesde() > this.datosConsulta.
                    getPrecioOfertaHasta()) {
                    this.addMensajeError(
                        "El precio 'desde' debe ser menor o igual que el precio 'hasta'");
                    error = true;
                }
            }
        }

        // área construida:
        if (this.datosConsulta.getAreaOfertaConstruidaDesde() != null &&
            this.datosConsulta.getAreaOfertaConstruidaDesde().compareTo(Double.valueOf(0)) != 0) {
            if (this.datosConsulta.getAreaOfertaConstruidaHasta() != null &&
                this.datosConsulta.getAreaOfertaConstruidaHasta().compareTo(Double.valueOf(0)) != 0) {
                if (this.datosConsulta.getAreaOfertaConstruidaDesde() >
                    this.datosConsulta.getAreaOfertaConstruidaHasta()) {
                    this.addMensajeError(
                        "El área construida 'desde' debe ser menor o igual que el área construida 'hasta'");
                    error = true;
                }
            }
        }

        // área terreno:
        if (this.datosConsulta.getAreaOfertaTerrenoDesde() != null &&
            this.datosConsulta.getAreaOfertaTerrenoDesde().compareTo(Double.valueOf(0)) != 0) {
            if (this.datosConsulta.getAreaOfertaTerrenoHasta() != null &&
                this.datosConsulta.getAreaOfertaTerrenoHasta().compareTo(Double.valueOf(0)) != 0) {
                if (this.datosConsulta.getAreaOfertaTerrenoDesde() >
                    this.datosConsulta.getAreaOfertaTerrenoHasta()) {
                    this.addMensajeError(
                        "El área de terreno 'desde' debe ser menor o igual que el área de terreno 'hasta'");
                    error = true;
                }
            }
        }

        if (error) {
            this.mustSearch = false;
            return;
        }

        //D: tomar los valores de departamento y municipio del bean
        this.datosConsulta.setDepartamentoCodigo(this.combosDM.getSelectedDepartamentoCod());
        this.datosConsulta.setMunicipioCodigo(this.combosDM.getSelectedMunicipioCod());

        //D: OJO: hay que hacer esto para que el lazy datamodel funcione correctamente
        this.resultadosBusquedaLazyModel.setRowCount(
            this.getAvaluosService().contarOfertasInmobiliariasPorFiltro(this.datosConsulta).
                intValue());

    }
//--------------------------------------------------------------------------------------------------

    private void initSelectItemLists() {

        this.ofertaTipoPredioItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaTipoPredioV();
        this.ofertaTipoPredioItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaTipoInmuebleItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaTipoInmuebleV();
        this.ofertaTipoInmuebleItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaDestinoItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaDestinoV();
        this.ofertaDestinoItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaTipoInvestigacionItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaTipoInvestigacionV();
        this.ofertaTipoInvestigacionItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaTipoInvestigacionIngresoItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaInvestigacionIngresoV();
        this.ofertaTipoInvestigacionIngresoItemList.add(0,
            new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaCondicionJuridicaItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaCondicionJuridicaV();
        this.ofertaCondicionJuridicaItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaEstratoItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaEstratoSocioecoV();
        this.ofertaEstratoItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

        this.ofertaVetustezItemList =
            (ArrayList<SelectItem>) this.generalMB.getOfertaVetustezV();
        this.ofertaVetustezItemList.add(0, new SelectItem(null, super.DEFAULT_COMBOS));

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para editar una oferta inmobiliaria seleccionada
     */
    public String modificarOfertaInmobiliaria() {
        return "modificarOfertaInmobiliaria";
    }

    public void cargarVariablesNumericas() {

        Field campo, campos[];

        this.variablesNumericasDisponibles = new ArrayList<SelectItem>();

        this.variablesNumericas = new ArrayList<String>();

        clase = OfertaInmobiliaria.class;

        campos = clase.getDeclaredFields();
        String nombreCampo;
        for (int i = 0; i < campos.length; i++) {

            campo = campos[i];
            if (campo.getName().equals(ID)) {
                continue;
            }
            if (campo.getType().isAssignableFrom(Double.class) || campo.getType().isAssignableFrom(
                Integer.class) ||
                campo.getType().isAssignableFrom(Float.class) || campo.getType().isAssignableFrom(
                Short.class) ||
                campo.getType().isAssignableFrom(Long.class)) {

                nombreCampo = campo.getName();
                nombreCampo = WordUtils.capitalize(nombreCampo.replaceAll("[A-Z]", SEPARATOR_WORD +
                    "$0"));

                this.variablesNumericas.add(nombreCampo);
                this.variablesNumericasDisponibles.add(new SelectItem(nombreCampo, nombreCampo));

            }
        }

    }

    /**
     * Método encargado de cargar las columnas de la tabla que muestra variables vs ofertas
     * inmobiliarias si el usuario desea adicionar una nueva variable se ejecuta este método para
     * cargar una columna en la tabla correspondiente a la variable
     *
     * @author javier.aponte
     */
    private void cargarColumnasDinamicas() {

        columns = new ArrayList<String>();
        columnsResultado = new ArrayList<String>();
        columnsResultado.add("Medida");
        List<SelectItem> variablesNumericasDisponiblesRemover = new ArrayList<SelectItem>();

        int limite = this.variablesNumericas.size();
        if (limite > 5) {
            limite = 5;
        }

        for (int i = 0; i < limite; i++) {
            for (SelectItem tmp : this.variablesNumericasDisponibles) {
                if (tmp.getValue().equals(this.variablesNumericas.get(i))) {
                    variablesNumericasDisponiblesRemover.add(tmp);
                    break;
                }
            }
            columns.add(this.variablesNumericas.get(i));
            columnsResultado.add(this.variablesNumericas.get(i));
        }
        this.variablesNumericasDisponibles.removeAll(variablesNumericasDisponiblesRemover);
    }

    /**
     * Método encargado de cargar objetos de tipo double que corresponden al valor de una variable
     * para determinda oferta inmobiliaria
     *
     * @author javier.aponte
     */
    private void cargarObjetosDinamicos() {
        this.datos = new ArrayList<Double[]>();

        Method metodo;
        Object valor;

        try {
            for (int i = 0; i < this.selectedOfertas.length; i++) {

                Double[] objects = new Double[this.columns.size()];

                for (int j = 0; j < columns.size(); j++) {

                    metodo = clase.getMethod("get" + columns.get(j).replace(SEPARATOR_WORD, ""),
                        null);

                    objects[j] = this.castToDouble(metodo.invoke(this.selectedOfertas[i], null));

                }

                this.datos.add(objects);

            }

        } catch (SecurityException e) {
            LOGGER.error(
                "Error en el método ConsultaOfertasInmobiliariasMB#cargarObjetosDinamicos" + e.
                    getMessage());
        } catch (NoSuchMethodException e) {
            LOGGER.error(
                "Error en el método ConsultaOfertasInmobiliariasMB#cargarObjetosDinamicos" + e.
                    getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error(
                "Error en el método ConsultaOfertasInmobiliariasMB#cargarObjetosDinamicos" + e.
                    getMessage());
        } catch (IllegalAccessException e) {
            LOGGER.error(
                "Error en el método ConsultaOfertasInmobiliariasMB#cargarObjetosDinamicos" + e.
                    getMessage());
        } catch (InvocationTargetException e) {
            LOGGER.error(
                "Error en el método ConsultaOfertasInmobiliariasMB#cargarObjetosDinamicos" + e.
                    getMessage());
        }
    }

    /**
     * Método que se encarga de cargar las variables y las ofertas seleccionadas para que se
     * muestren en una tabla de oferta seleccionada vs variable, la variable corresponde a los
     * atributos de tipo Double, Long, Integer, Short, Float de la clase Oferta Inmobiliaria
     *
     * @author javier.aponte
     */
    public void cargarOfertasSeleccionadas() {
        if (this.variablesNumericas == null) {
            this.cargarVariablesNumericas();
            this.cargarColumnasDinamicas();
        }
        this.cargarObjetosDinamicos();
        this.mostrarResultadoCalculosEstadisticos = false;
    }

    /**
     * Método ejecutado para adicionar una nueva columna en la tabla ofertas inmobiliarias vs
     * variables, la columna corresponde a una nueva variable sobre la que se quiere hacer analisis
     * estadístico
     *
     * @author javier.aponte
     */
    public void agregarVariable() {
        List<SelectItem> variablesNumericasDisponiblesRemover = new ArrayList<SelectItem>();
        columns.add(this.nombreVariable);
        columnsResultado.add(this.nombreVariable);
        for (SelectItem tmp : this.variablesNumericasDisponibles) {
            if (tmp.getValue().equals(this.nombreVariable)) {
                variablesNumericasDisponiblesRemover.add(tmp);
            }
        }
        this.nombreVariable = null;

        this.variablesNumericasDisponibles.removeAll(variablesNumericasDisponiblesRemover);

        this.cargarObjetosDinamicos();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado para remover una columna en la tabla ofertas inmobiliarias vs variables, la
     * columna corresponde a una variable sobre la que se quiere hacer analisis estadístico
     *
     * @author javier.aponte
     */
    public void removerVariable() {
        String columnToRemove = FacesContext.getCurrentInstance().getExternalContext().
            getRequestParameterMap().get("columnToRemove");

        this.columns.remove(columnToRemove);
        this.columnsResultado.remove(columnToRemove);

        this.variablesNumericasDisponibles.add(new SelectItem(columnToRemove, columnToRemove));

        this.cargarObjetosDinamicos();
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método que se encarga de de realizar las operaciones necesarias para calcular las medidas
     * estadísticas de un conjunto de datos retorna la media, varianza, coeficiente de variación.
     * coeficiente de asimetría, limite inferior y limite superior
     *
     * @author javier.aponte
     */
    public void calcularEstadisticas() {

        this.resultadoCalculosEstadisticos = new ArrayList<Object[]>();
        CalculosEstadisticos[] answer = this.calculaEstadisticasMBService.
            calcularMedidasEstadisticas(this.datos);

        Object[] medidaEstadistica = new Object[answer.length + 1];
        Object[] desviacionEstandar = new Object[answer.length + 1];
        Object[] coeficienteVariacion = new Object[answer.length + 1];
        Object[] coeficienteAsimetria = new Object[answer.length + 1];
        Object[] limiteInferior = new Object[answer.length + 1];
        Object[] limiteSuperior = new Object[answer.length + 1];

        medidaEstadistica[0] = "Media";
        desviacionEstandar[0] = "Desviación Estandar";
        coeficienteVariacion[0] = "Coeficiente Variación";
        coeficienteAsimetria[0] = "Coeficiente Asimetría";
        limiteInferior[0] = "Limite Inferior";
        limiteSuperior[0] = "Limite Superior";

        for (int i = 0; i < answer.length; i++) {

            medidaEstadistica[i + 1] = (Double.isNaN(answer[i].getMedia())) ? null : answer[i].
                getMedia();
            desviacionEstandar[i + 1] = (Double.isNaN(answer[i].getDesviacionEstandar())) ? null :
                answer[i].getDesviacionEstandar();
            coeficienteVariacion[i + 1] =
                (Double.isNaN(answer[i].getCoeficienteVariacion())) ? null : answer[i].
                getCoeficienteVariacion();
            coeficienteAsimetria[i + 1] =
                (Double.isNaN(answer[i].getCoeficienteAsimetria())) ? null : answer[i].
                getCoeficienteAsimetria();
            limiteInferior[i + 1] = (Double.isNaN(answer[i].getLimiteInferior())) ? null :
                answer[i].getLimiteInferior();
            limiteSuperior[i + 1] = (Double.isNaN(answer[i].getLimiteSuperior())) ? null :
                answer[i].getLimiteSuperior();

        }

        this.resultadoCalculosEstadisticos.add(medidaEstadistica);
        this.resultadoCalculosEstadisticos.add(desviacionEstandar);
        this.resultadoCalculosEstadisticos.add(coeficienteVariacion);
        this.resultadoCalculosEstadisticos.add(coeficienteAsimetria);
        this.resultadoCalculosEstadisticos.add(limiteInferior);
        this.resultadoCalculosEstadisticos.add(limiteSuperior);

        this.mostrarResultadoCalculosEstadisticos = true;
        this.mostrarExportarAExcel = true;
    }

    /**
     * Método que convierte a Double el objeto que se le pase como parametro. El objeto pertenece
     * unicamente a alguna de estas clases (Integer, Float Short, Long, Double)
     *
     * @param objeto
     * @return double
     * @author javier.aponte
     */
    public Double castToDouble(Object objeto) {

        if (objeto == null) {
            return null;
        }

        Double resultado = null;

        if (objeto instanceof Double) {
            resultado = ((Double) objeto).doubleValue();
        } else if (objeto instanceof Float) {
            resultado = Double.valueOf(((Float) objeto).doubleValue());
        } else if (objeto instanceof Integer) {
            resultado = Double.valueOf(((Integer) objeto).doubleValue());
        } else if (objeto instanceof Short) {
            resultado = Double.valueOf(((Short) objeto).doubleValue());
        } else if (objeto instanceof Long) {
            resultado = Double.valueOf(((Long) objeto).doubleValue());
        }

        return resultado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado para ver el detalle de una oferta inmobiliaria
     */
    public String buscarOIYEnviarAModificar() {

        this.registroOfertaInmobiliaria.init();

        this.registroOfertaInmobiliaria.setBanderaCreacionOfertaDesdeConsultaOfertas(true);

        this.registroOfertaInmobiliaria.precargaDatosCapturaOferta(null, this.usuario.getLogin());

        UtilidadesWeb.removerManagedBean("consultaOfertasInmob");

        return "/avaluos/ofertasInmob/registroOfertaInmobiliaria.jsf";
    }

    public void exportarOfertasAExcel(OfertaInmobiliaria[] ofertasAExportar,
        boolean esReporteEstadisticasOfertasSeleccionadas,
        boolean esReporteExportarOfertasSeleccionadas) {

        List<GttOfertaInmobiliaria> idsOfertasSeleccionadas = new ArrayList<GttOfertaInmobiliaria>();
        GttOfertaInmobiliaria idOfertaSeleccionada;

        Long secuencia = this.getAvaluosService().generarSecuenciaGttOfertaInmobiliaria();

        for (int i = 0; i < ofertasAExportar.length; i++) {
            idOfertaSeleccionada = new GttOfertaInmobiliaria();
            idOfertaSeleccionada.setOfertaInmobiliariaId(ofertasAExportar[i].getId());
            idOfertaSeleccionada.setSecuencia(secuencia);
            idsOfertasSeleccionadas.add(idOfertaSeleccionada);
        }
        //Guarda los ids de las ofertas inmobiliarias seleccionadas
        idsOfertasSeleccionadas = this.getAvaluosService().guardarActualizarIdsOfertasInmobiliarias(
            idsOfertasSeleccionadas);

        if (esReporteEstadisticasOfertasSeleccionadas) {
            //Generar reporte después de calcular estadisticas de ofertas inmobiliarias seleccionadas
            this.generarReporteOfertasSeleccionadas(secuencia);
        } else if (esReporteExportarOfertasSeleccionadas) {
            //Generar reporte de información de ofertas inmobiliarias seleccionadas
            this.generarReporteInformacionDeOfertas(secuencia);
        }

        //Borra los ids de las ofertas inmobiliarias seleccionadas
        this.getAvaluosService().borrarIdsOfertasInmobiliarias(idsOfertasSeleccionadas);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para generar el reporte de ofertas imboliarias seleccionadas depués de calcular
     * estadísticas
     *
     * @author javier.aponte
     * @param secuencia
     */
    public void generarReporteOfertasSeleccionadas(Long secuencia) {

        Map<String, String> parameters = new HashMap<String, String>();
        ReporteDTO reporteDTO;
        File file;
        InputStream stream;

        parameters.put("SECUENCIA", "" + secuencia);
        this.urlArchivo = new String();

        reporteDTO = this.reportsService.generarReporte(
            parameters, EReporteServiceSNC.OFERTAS_INMOBILIARIAS, this.usuario, Reporte.FORMAT_EXCEL);
        file = reporteDTO.getArchivoReporte();

        this.urlArchivo = reporteDTO.getUrlWebReporte();

        try {
            stream = new FileInputStream(file);
            this.reporteEstadisticasOfertasSeleccionadas = new DefaultStreamedContent(
                stream, "application/vnd.ms-excel",
                "OfertasInmobiliarias.xls");

            LOGGER.debug("Url público del archivo:" + this.urlArchivo);
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para generar el reporte de información de ofertas inmobiliarias
     *
     * @author javier.aponte
     * @param secuencia
     */
    public void generarReporteInformacionDeOfertas(Long secuencia) {

        Map<String, String> parameters = new HashMap<String, String>();
        ReporteDTO reporteDTO;
        File file;
        InputStream stream;

        parameters.put("SECUENCIA", "" + secuencia);
        this.urlArchivo = new String();

        reporteDTO = this.reportsService.generarReporte(
            parameters, EReporteServiceSNC.INFORMACION_DE_OFERTAS, this.usuario,
            Reporte.FORMAT_EXCEL);

        file = reporteDTO.getArchivoReporte();
        this.urlArchivo = reporteDTO.getUrlWebReporte();

        try {
            stream = new FileInputStream(file);
            this.reporteExportarOfertasSeleccionadas = new DefaultStreamedContent(
                stream, "application/vnd.ms-excel",
                "InformacionDeOfertas.xls");

            LOGGER.debug("Url público del archivo:" + this.urlArchivo);
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que se llama cuando se desean consultar ofertas desde el caso de uso 5 de avalúos, se
     * encarga de deshabilitar la edición de ofertas y asignar una lsita de depratamentos fijos
     *
     * @author christian.rodriguez
     * @param codigoTerritorial código de la {@link EstructuraOrganizacional} de la cual se quieren
     * consultar ofertas
     */
    public void cargarDesdeCUCotizarCostoAValuo(String codigoTerritorial) {

        this.init();

        this.banderaEsConsultaDesdeCotizarCostoAvaluo = true;
        this.banderaEsConsultaRestringida = true;

        List<Departamento> deptos = this.getGeneralesService()
            .getCacheDepartamentosPorTerritorial(codigoTerritorial);

        ArrayList<SelectItem> departamentosItemList = new ArrayList<SelectItem>();
        for (Departamento departamento : deptos) {
            departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                departamento.getNombre()));
        }

        this.combosDM.setDepartamentosItemList(departamentosItemList);
        this.combosDM.setSelectedDepartamentoCod(deptos.get(0).getCodigo());
        this.combosDM.onChangeDepartamentosListener();
    }

    // -----------------------------------------------------------
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("combosDeptosMunis");
        UtilidadesWeb.removerManagedBean("calculaEstadisticas");
        UtilidadesWeb.removerManagedBean("registroOfertaInmob");

        if (this.banderaEsConsultaDesdeCotizarCostoAvaluo) {
            UtilidadesWeb.removerManagedBean("consultaOfertasInmob");
            return "/avaluos/ejecucion/cotizarCostoAvaluo.jsf";
        }

        UtilidadesWeb.removerManagedBean("consultaOfertasInmob");
        return ConstantesNavegacionWeb.INDEX;
    }

    public String getNombreUsuario() {
        return this.usuario.getNombreCompleto();
    }
//end of class
}
