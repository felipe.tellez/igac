package co.gov.igac.snc.web.mb.conservacion.productos;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el registro de datos de productos solicitados
 *
 * @author javier.aponte
 */
@Component("revisionImpresionOExportacionP")
@Scope("session")
public class RevisionImpresionOExportacionProductosMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -3865342860839390196L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RevisionImpresionOExportacionProductosMB.class);

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    /**
     * Solicitud seleccionada en el árbol de tareas
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Lista de los documentos soportes de los productos catastrales solicitados
     */
    private List<Documento> documentosSoportesPC;

// -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init RegistroDatosProductosSolicitudMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        /*
         *
         * //D: se setea este bean como el actual para lo de procesos
         * this.registerCurrentManagedBean(this, "registroDatosPS");
         *
         * if(this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
         * this.addMensajeError("Error con respecto al árbol de tareas. Puede ser que no haya una
         * actividad seleccionada"); return; }
         *
         * try {
         *
         * this.currentProcessActivity =
         * this.tareasPendientesMB.buscarActividadPorIdTramite(this.tramite.getId());
         *
         *
         * }catch(Exception e){ LOGGER.error(e.getMessage()); this.addMensajeError(e); }
         */
        this.solicitudSeleccionada = this.getTramiteService().
            buscarSolicitudConSolicitantesSolicitudPorId(Long.valueOf("36836"));

        this.documentosSoportesPC = this.getTramiteService().buscarDocumentacionPorSolicitudId(Long.
            valueOf("36836"));
    }
    //--------------------------------------------------------------------------------------------------

    @Override
    public boolean validateProcess() {
        return false;
    }

    @Override
    public void setupProcessMessage() {

    }

    @Override
    public void doDatabaseStatesUpdate() {

    }

    //--------------------------------------MÉTODOS-----------------------------------------
    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Documento> getDocumentosSoportesPC() {
        return documentosSoportesPC;
    }

    public void setDocumentosSoportesPC(List<Documento> documentosSoportesPC) {
        this.documentosSoportesPC = documentosSoportesPC;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("revisionImpresionOExportacionP");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }
//end of class
}
