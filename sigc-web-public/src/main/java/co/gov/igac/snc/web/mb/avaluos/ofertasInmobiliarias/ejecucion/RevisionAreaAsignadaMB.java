/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.ejecucion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EValoresEscala;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-005 Revisar área asignada
 *
 * @author ariel.ortiz
 * @cu CU-TV-0F-005
 * @version 2.0
 *
 */
@Component("revisionAreaAsignada")
@Scope("session")
public class RevisionAreaAsignadaMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionAreaAsignadaMB.class);

    /**
     * serial auto-generado
     */
    private static final long serialVersionUID = -6403325700187545209L;

    /**
     * Archivo que contiene el PDF generado con las planchas a exportar
     */
    private StreamedContent archivoPlanchas;
    private String rutaTxtPlanchas;

    private String codigoDepartamento;
    private String codigoMunicipio;
    private String codigoZona;

    private String nombreDepartamento;
    private String nombreMunicipio;
    private String nombreZona;

    private AreaCapturaOferta areaAsignada;

    private Departamento departamentoSeleccionado;
    private Municipio municipioSeleccionado;
    private Zona zonaSeleccionada;

    private UsuarioDTO usuario;

    // ------------------ procesos
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private Actividad bpmActivityInstance;

    private boolean banderaProcesoMovido;

    /**
     * valores de lista inicial y target para el PickList
     */
    private DualListModel<String> dualAreasAsignadas = new DualListModel<String>();

    /**
     * Lista que contiene los codigos de las planchas involucradas en la oferta.
     *
     */
    private List<String> listaCodigosPlanchasSeleccionadas;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private List<String> sourceAreas = new ArrayList<String>();

    private List<String> targetAreas;

    private String valorEscala;

    private List<String> valorEscalaList;

    /**
     * variable que guarda los datos del usuario que usa la aplicación
     */
    private boolean isPredioRural;

    /**
     *
     *
     */
    private RegionCapturaOferta regionCapturaOferta;
    private List<DetalleCapturaOferta> detalleCapturaOfertaList;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    List<String> codigosManzanas;

    private String listaManzanasGIS;

    @Autowired
    private IContextListener contextoWeb;

    private Long areaAsignadaId;

    private Long idRegion;

    /**
     * Bandera que se activa cuando se disponie de la inforamción geográfica de la región
     */
    private boolean banderaHabilitarComponenteGeografico;

    // -------------- Getters y Setters ---------------
    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public String getNombreZona() {
        return nombreZona;
    }

    public void setNombreZona(String nombreZona) {
        this.nombreZona = nombreZona;
    }

    public AreaCapturaOferta getAreaAsignada() {
        return areaAsignada;
    }

    public void setAreaAsignada(AreaCapturaOferta areaAsignada) {
        this.areaAsignada = areaAsignada;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Departamento getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    public void setDepartamentoSeleccionado(Departamento departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    public Municipio getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(Municipio municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public Zona getZonaSeleccionada() {
        return zonaSeleccionada;
    }

    public void setZonaSeleccionada(Zona zonaSeleccionada) {
        this.zonaSeleccionada = zonaSeleccionada;
    }

    public boolean isBanderaProcesoMovido() {
        return banderaProcesoMovido;
    }

    public void setBanderaProcesoMovido(boolean banderaProcesoMovido) {
        this.banderaProcesoMovido = banderaProcesoMovido;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public StreamedContent getArchivoPlanchas() {
        return archivoPlanchas;
    }

    public void setArchivoPlanchas(StreamedContent archivoPlanchas) {
        this.archivoPlanchas = archivoPlanchas;
    }

    public String getRutaTxtPlanchas() {
        return rutaTxtPlanchas;
    }

    public void setRutaTxtPlanchas(String rutaTxtPlanchas) {
        this.rutaTxtPlanchas = rutaTxtPlanchas;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    public List<String> getCodigosManzanas() {
        return codigosManzanas;
    }

    public void setCodigosManzanas(List<String> codigosManzanas) {
        this.codigosManzanas = codigosManzanas;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getCodigoZona() {
        return codigoZona;
    }

    public void setCodigoZona(String codigoZona) {
        this.codigoZona = codigoZona;
    }

    public boolean isIsPredioRural() {
        return isPredioRural;
    }

    public void setPredioRural(boolean isPredioRural) {
        this.isPredioRural = isPredioRural;
    }

    public DualListModel<String> getDualAreasAsignadas() {
        return dualAreasAsignadas;
    }

    public void setDualAreasAsignadas(DualListModel<String> dualAreasAsignadas) {
        this.dualAreasAsignadas = dualAreasAsignadas;
    }

    public List<String> getListaCodigosPlanchasSeleccionadas() {
        return listaCodigosPlanchasSeleccionadas;
    }

    public void setListaCodigosPlanchasSeleccionadas(List<String> listaCodigosPlanchasSeleccionadas) {
        this.listaCodigosPlanchasSeleccionadas = listaCodigosPlanchasSeleccionadas;
    }

    public RegionCapturaOferta getRegionCapturaOferta() {
        return regionCapturaOferta;
    }

    public void setRegionCapturaOferta(RegionCapturaOferta regionCapturaOferta) {
        this.regionCapturaOferta = regionCapturaOferta;
    }

    public List<DetalleCapturaOferta> getDetalleCapturaOfertaList() {
        return detalleCapturaOfertaList;
    }

    public void setDetalleCapturaOfertaList(List<DetalleCapturaOferta> detalleCapturaOfertaList) {
        this.detalleCapturaOfertaList = detalleCapturaOfertaList;
    }

    public Long getAreaAsignadaId() {
        return areaAsignadaId;
    }

    public void setAreaAsignadaId(Long areaAsignadaId) {
        this.areaAsignadaId = areaAsignadaId;
    }

    public String getListaManzanasGIS() {
        return listaManzanasGIS;
    }

    public void setListaManzanasGIS(String listaManzanasGIS) {
        this.listaManzanasGIS = listaManzanasGIS;
    }

    public List<String> getSourceAreas() {
        return sourceAreas;
    }

    public void setSourceAreas(List<String> sourceAreas) {
        this.sourceAreas = sourceAreas;
    }

    public List<String> getTargetAreas() {
        return targetAreas;
    }

    public void setTargetAreas(List<String> targetAreas) {
        this.targetAreas = targetAreas;
    }

    public String getValorEscala() {
        return valorEscala;
    }

    public void setValorEscala(String valorEscala) {
        this.valorEscala = valorEscala;
    }

    public List<String> getValorEscalaList() {
        return valorEscalaList;
    }

    public void setValorEscalaList(List<String> valorEscalaList) {
        this.valorEscalaList = valorEscalaList;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public IContextListener getContextoWeb() {
        return contextoWeb;
    }

    public void setContextoWeb(IContextListener contextoWeb) {
        this.contextoWeb = contextoWeb;
    }

    public boolean isPredioRural() {
        return isPredioRural;
    }

    public boolean isBanderaHabilitarComponenteGeografico() {
        return banderaHabilitarComponenteGeografico;
    }

    public void setBanderaHabilitarComponenteGeografico(boolean banderaHabilitarComponenteGeografico) {
        this.banderaHabilitarComponenteGeografico = banderaHabilitarComponenteGeografico;
    }

    // -------------- methods -------------------------------------------------
    /**
     * Método inicializa el valor de la escala dependiendo de si la zona es rural o urbana
     */
    public void asignarValorDeEscala() {
        this.valorEscalaList.clear();

        if (this.isPredioRural) {
            this.valorEscalaList.add(0, EValoresEscala.ESCALA_1_5000.toString());
            this.valorEscala = EValoresEscala.ESCALA_1_5000.toString();
            this.valorEscalaList.add(1, EValoresEscala.ESCALA_1_10000.toString());
        } else {
            this.valorEscala = EValoresEscala.ESCALA_1_2000.toString();
        }
    }

    /**
     * Listener que se ejecuta al cambiar el valor del SelectOneMenu de la escala para recalcular
     * los codigos de las planchas
     */
    public void cambioValorEscala() {
        this.sourceAreas.clear();
        this.listaCodigosPlanchasSeleccionadas = this.obtenerCodigosPlanchas();
        this.dualAreasAsignadas.setSource(this.listaCodigosPlanchasSeleccionadas);
        this.dualAreasAsignadas.setTarget(this.targetAreas);

    }

    /**
     * Se encarga de llamar al método para mover el proceso
     *
     * @author christian.rodriguez
     */
    public void moverProceso() {

        if (this.forwardProcess()) {

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeError("Ocurrió un error avanzando alguna de las actividades del " +
                 "proceso");
        } else {

            this.doDatabaseStatesUpdate();

            this.addMensajeInfo("El proceso se movio satisfactoriamente.");
            this.banderaProcesoMovido = true;
        }
    }

    /**
     * Actualizar el estado de la region a 'EN RECOLECCION'
     *
     * @author christian.rodriguez
     */
    private void doDatabaseStatesUpdate() {

        if (this.regionCapturaOferta != null) {
            this.regionCapturaOferta.setEstado(EEstadoRegionCapturaOferta.EN_RECOLECCION
                .getEstado());
            this.getAvaluosService().guardarActualizarRegionCapturaOferta(this.regionCapturaOferta);

        }
    }

    /**
     * crea la instancia del proceso
     *
     * @author ariel.ortiz
     * @return false si se pudo crear la instancia
     */
    private boolean forwardProcess() {

        LOGGER.debug("Creando proceso Ofertas");
        boolean error = false;

        List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
        DivisionAdministrativa municipio, dpto, zona;
        OfertaInmobiliaria ofertaBPM;
        String idActividad = this.bpmActivityInstance.getId();
        String transition = ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_INGRESAR_OFERTAS;

        listaUsuarios.add(this.usuario);

        // D: objeto OfertaInmobiliaria es el objeto de negocio del proceso
        ofertaBPM = new OfertaInmobiliaria();
        // ofertaInmobiliariaBPM.setIdentificador(new Long(this.idOfertaMapa));
        municipio = new DivisionAdministrativa(this.municipioSeleccionado.getCodigo(),
            this.municipioSeleccionado.getNombre());
        dpto = new DivisionAdministrativa(this.departamentoSeleccionado.getCodigo(),
            this.departamentoSeleccionado.getNombre());

        ofertaBPM.setMunicipio(municipio);
        ofertaBPM.setDepartamento(dpto);

        if (this.zonaSeleccionada != null) {
            zona = new DivisionAdministrativa(this.zonaSeleccionada.getCodigo(),
                this.zonaSeleccionada.getNombre());
            ofertaBPM.setZona(zona);
        }

        ofertaBPM.setObservaciones("Revision de áreas asignadas al recolector");
        ofertaBPM.setTransicion(ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_INGRESAR_OFERTAS);
        ofertaBPM.setUsuarios(listaUsuarios);
        ofertaBPM.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        ofertaBPM.setFuncionario(this.usuario.getNombreCompleto());

        try {
            this.getProcesosService().avanzarActividad(idActividad, ofertaBPM);
        } catch (Exception ex) {
            LOGGER.error(
                "Error moviendo actividad con id " + idActividad + " a la " + "actividad " +
                 transition + " : " + ex.getMessage());
            error = true;
        }

        return error;

    }

    /**
     * Método invocado al presionar el boton cerrar.
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método que define el valor del booleano isPredioRural.
     */
    public void defineBanderaEscala() {
        if (this.codigoZona.equals("00")) {
            this.isPredioRural = true;
        } else {
            this.isPredioRural = false;
        }
    }

    /**
     * Método que se llama para inicializar las variables a usar en el SNCManagedBean
     */
    public void inicializarVariables() {

        this.idRegion = this.bpmActivityInstance.getIdObjetoNegocio();

        this.areaAsignada = this.getAvaluosService().buscarAreaCapturaOfertaPorId(
            this.bpmActivityInstance.getIdCorrelacion());

        this.departamentoSeleccionado = this.areaAsignada.getDepartamento();
        this.municipioSeleccionado = this.areaAsignada.getMunicipio();

        if (this.areaAsignada.getZona() != null) {
            this.zonaSeleccionada = this.areaAsignada.getZona();
            this.codigoZona = this.zonaSeleccionada.getCodigo();
            this.nombreZona = this.zonaSeleccionada.getNombre();
            this.defineBanderaEscala();
        }

        this.areaAsignadaId = this.areaAsignada.getId();

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.nombreDepartamento = this.departamentoSeleccionado.getNombre();
        this.nombreMunicipio = this.municipioSeleccionado.getNombre();

        this.valorEscalaList = new ArrayList<String>();
        this.codigosManzanas = new ArrayList<String>();

        this.regionCapturaOferta = this.getAvaluosService().buscarRegionCapturaOfertaPorId(
            Long.valueOf(this.idRegion));

        if (this.regionCapturaOferta.getDetalleCapturaOfertas() != null &&
             this.regionCapturaOferta.getDetalleCapturaOfertas().size() != 0) {
            this.asignarValorDeEscala();
            this.inicializarPickListPlanchas();
            this.banderaHabilitarComponenteGeografico = true;
            this.inicializarVariablesVisorGIS();
        }

    }

    /**
     * Inicialización de variables usadas para el pick list de las planchas
     */
    private void inicializarPickListPlanchas() {

        this.sourceAreas = this.obtenerCodigosPlanchas();

        this.dualAreasAsignadas.setSource(sourceAreas);
        this.dualAreasAsignadas.setTarget(this.listaCodigosPlanchasSeleccionadas);
    }

    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author ariel.ortiz
     */
    private void inicializarVariablesVisorGIS() {

        try {
            this.setApplicationClientName(this.contextoWeb.getClientAppNameUrl());
            this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
            this.moduleTools = EVisorGISTools.OFERTAS_INMOBILIARIAS.getCodigo();

            for (String manzana : this.codigosManzanas) {
                if (this.listaManzanasGIS == null) {
                    this.listaManzanasGIS = manzana;
                } else {
                    this.listaManzanasGIS = this.listaManzanasGIS.concat("," + manzana);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error inicializando las variables del Visor GIS: " + e);
        }

    }

    /**
     * Metodo que consulta el codigo de las planchas pertenecientes a las áreas asignadas a un
     * recolector en específico
     *
     * @return lista de códigos de las planchas para el pickList
     */
    public List<String> obtenerCodigosPlanchas() {

        List<String> codigosPlanchas = new ArrayList<String>();
        List<String> temporal = new ArrayList<String>();

        LOGGER.error(
            "Se debe actualizar la forma en que se consultan las áreas (Cambios en acceso al SIG en la v2 ");
        /*
         * try { UsuarioDTO usuarioConAuthSig = MenuMB.getMenu().getUsuarioWithGISAuthorization();
         * LOGGER.error("Error en el proceso de obtener los codigos de las planchas" + e); String
         * usuarioLogin = usuarioConAuthSig.getLogin();
         *
         * temporal = this.getAvaluosService().buscarDetallesAreasPorRecolector(usuarioLogin,
         * this.idRegion); for (String temp : temporal) { this.codigosManzanas.add(temp); }
         * codigosPlanchas = this.getAvaluosService().obtenerCodigosPlanchas(this.codigosManzanas,
         * this.valorEscala, this.isPredioRural, usuarioConAuthSig); } catch (Exception e) {
         * LOGGER.error("Error en el proceso de obtener los codigos de las planchas" + e); } return
         * codigosPlanchas;
         */
        return null;

    }

    /**
     * Método que trae los pdfs generados usando los códigos de área generados anteriormente
     *
     * @return lista que contiene las ubicaciones de los pdf's generados automaticamente
     */
    public List<String> obtenerPlanchasPDF() {

        // TODO:: ariel.ortiz :: traer los pdf's. Cuando el geoservicio esté
        // listo.
        // FIXME:: ariel.ortiz :: arreglar el retorno del método
        return null;
    }

    /**
     * Método que permite la descarga/visualizacion de los pdf's exportados
     *
     * @param rutaTxt
     * @return
     */
    public StreamedContent generarPlanchasExportar(String rutaTxt) {

        StreamedContent rutaExport = null;

        return rutaExport;

    }

    // ----------------Metodo
    // Init----------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.info("Estamos en RevisionAreaAsignadaMB");

        // D: obtener la instancia de proceso
        this.bpmActivityInstance = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.listaCodigosPlanchasSeleccionadas = new ArrayList<String>();

        this.inicializarVariables();

    }

}
