/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @description Managed bean para gestionar el registro del informe en el que comunica los
 * resultados del levantamiento topógraficoen campo.
 * @version 1.0
 */
@Component("registroInformeLevantamientoTopo")
@Scope("session")
public class RegistroInformeLevantamientoTopograficoMB extends SNCManagedBean
    implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -2869096050311713220L;
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistroInformeLevantamientoTopograficoMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextService;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable {@link Tramite} que almacena el trámite seleccionado de la lista de actividades.
     */
    private Tramite tramiteSeleccionado;

    /**
     * Variable {@link Actividad} que almacena la actividad seleccionada de la lista de actividades.
     */
    private Actividad actividad;

    /**
     * Variable {@link int} para capturar el tiempo programado para realizar las actividades.
     */
    private int tiempoProgramado;

    /**
     * Variable {@link int} para capturar el tiempo utilizado en la ejecución de las actividades.
     */
    private int tiempoUtilizado;

    /**
     * Variavke {@link String} para almacenar los motivos del resultado del levantamiento
     * topográfico.
     */
    private String motivos;

    /**
     * Variable {@link String} para consultar el trabajo programado para el topografo logueado.
     */
    private String trabajoProgramado;

    /**
     * Variable {@link String} para consultar el trabajo realizado po el topografo logueado antes de
     * realizar el registro del informe de levantamiento topográfico.
     */
    private String trabajoRealizado;

    /**
     * Variable {@link String} que almacena el detalle de las tareas adicionales en caso de que se
     * haya indicado que existen dichas tareas.
     */
    private String tareasAdicionales;

    /**
     * Variable {@link String} que captura el valor de Si/No para indicar si existen tareas
     * adicionales.
     */
    private String tareasAdicionalesSiNo;

    /**
     * Variable {@link String} para almacenar el resumen de actividades desarrolladas por el
     * topográfo.
     */
    private String resumenActividades;

    /**
     * Variable {@link String} los inconvenientes administrativos presentados.
     */
    private String inconvenientesAdministrativos;

    /**
     * Lista de {@link String} con los valores de los inconvenientes seleccionados.
     */
    private List<String> inconvenientesSelected;

    /**
     * Map que almacena los valores posibles de los inconvenientes presentados.
     */
    private Map<String, String> inconvenientes;

    /**
     * Variable {@link Comision} del trámite seleccionado del arbol.
     */
    private Comision comisionDelTramite;

    /**
     * Variable {@link Documento} del documento asociado a la comision
     */
    private Documento documentoComision;

    /**
     * Variable {@link String} para almacenar el detalle de los inconvenientes presentados si se ha
     * seleccionado que si hubo inconvenientes.
     */
    private String detalleInconvenientes;

    /**
     * Variable {@link boolean} usada para activar la opción de generar el documento una vez se ha
     * guardado la información.
     */
    private boolean generarDocumentoBool;

    /**
     * Variable {@link boolean} usada para activar la opción avanzar proceso una vez se ha generado
     * el informe.
     */
    private boolean avanzarProcesoBool;

    /**
     * Objeto que contiene los datos del reporte generado
     */
    private ReporteDTO reporte;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - RegistroInformeLevantamientoTopograficoMB");

        // Cargue del usuario.
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        try {
            // Lectura de la actividad seleccionada
            // TODO :: Trámite quemado para pruebas, descomentar cuando se esté
            // llamando al caso de uso desde el arbol de tareas :: 05/08/2013
            this.tramiteSeleccionado = this.getTramiteService()
                .buscarTramiteTraerDocumentoResolucionPorIdTramite(34197l);
            this.cargarInformacionTramite();

            /*
             * this.actividad = tareasPendientesMB.getInstanciaSeleccionada(); if (actividad !=
             * null) {
             *
             * this.tramiteSeleccionado = this.getTramiteService() .findTramitePruebasByTramiteId(
             * actividad.getIdObjetoNegocio());
             *
             * if (this.tramiteSeleccionado != null) { // Cargue inicial de las variables de
             * depuración utilizadas // en la pantalla this.cargarInformacionTramite(); } else {
             * this.addMensajeError("Ocurrió un error en la búsqueda del trámite seleccionado.");
             * return; }
             *
             * } else { this.addMensajeError("No se pudo obtener la actividad del process"); }
             */
            // Set de variables por default
            this.tareasAdicionalesSiNo = ESiNo.NO.getCodigo();

        } catch (Exception e) {
            this.addMensajeError(
                "Ocurrió un error al buscar el trámite, por favor intente nuevamente");
            LOGGER.error(e.getMessage(), e);
        }
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public int getTiempoProgramado() {
        return this.tiempoProgramado;
    }

    public void setTiempoProgramado(int tiempoProgramado) {
        this.tiempoProgramado = tiempoProgramado;
    }

    public int getTiempoUtilizado() {
        return this.tiempoUtilizado;
    }

    public void setTiempoUtilizado(int tiempoUtilizado) {
        this.tiempoUtilizado = tiempoUtilizado;
    }

    public String getMotivos() {
        return this.motivos;
    }

    public void setMotivos(String motivos) {
        this.motivos = motivos;
    }

    public String getInconvenientesAdministrativos() {
        return this.inconvenientesAdministrativos;
    }

    public void setInconvenientesAdministrativos(
        String inconvenientesAdministrativos) {
        this.inconvenientesAdministrativos = inconvenientesAdministrativos;
    }

    public String getTrabajoProgramado() {
        return this.trabajoProgramado;
    }

    public void setTrabajoProgramado(String trabajoProgramado) {
        this.trabajoProgramado = trabajoProgramado;
    }

    public ReporteDTO getReporte() {
        return reporte;
    }

    public void setReporteSolicitudDocumentos(ReporteDTO reporte) {
        this.reporte = reporte;
    }

    public String getTrabajoRealizado() {
        return this.trabajoRealizado;
    }

    public void setTrabajoRealizado(String trabajoRealizado) {
        this.trabajoRealizado = trabajoRealizado;
    }

    public String getDetalleInconvenientes() {
        return detalleInconvenientes;
    }

    public void setDetalleInconvenientes(String detalleInconvenientes) {
        this.detalleInconvenientes = detalleInconvenientes;
    }

    public boolean isGenerarDocumentoBool() {
        return generarDocumentoBool;
    }

    public void setGenerarDocumentoBool(boolean generarDocumentoBool) {
        this.generarDocumentoBool = generarDocumentoBool;
    }

    public boolean isAvanzarProcesoBool() {
        return avanzarProcesoBool;
    }

    public void setAvanzarProcesoBool(boolean avanzarProcesoBool) {
        this.avanzarProcesoBool = avanzarProcesoBool;
    }

    public String getTareasAdicionales() {
        return this.tareasAdicionales;
    }

    public void setTareasAdicionales(String tareasAdicionales) {
        this.tareasAdicionales = tareasAdicionales;
    }

    public String getTareasAdicionalesSiNo() {
        return this.tareasAdicionalesSiNo;
    }

    public void setTareasAdicionalesSiNo(String tareasAdicionalesSiNo) {
        this.tareasAdicionalesSiNo = tareasAdicionalesSiNo;
    }

    public String getResumenActividades() {
        return this.resumenActividades;
    }

    public void setResumenActividades(String resumenActividades) {
        this.resumenActividades = resumenActividades;
    }

    public List<String> getInconvenientesSelected() {
        return this.inconvenientesSelected;
    }

    public void setInconvenientesSelected(List<String> inconvenientesSelected) {
        this.inconvenientesSelected = inconvenientesSelected;
    }

    public Map<String, String> getInconvenientes() {
        return this.inconvenientes;
    }

    public void setInconvenientes(Map<String, String> inconvenientes) {
        this.inconvenientes = inconvenientes;
    }

    public Comision getComisionDelTramite() {
        return this.comisionDelTramite;
    }

    public void setComisionDelTramite(Comision comisionDelTramite) {
        this.comisionDelTramite = comisionDelTramite;
    }

    public Documento getDocumentoComision() {
        return this.documentoComision;
    }

    public void setDocumentoComision(Documento documentoComision) {
        this.documentoComision = documentoComision;
    }

    public Tramite getTramiteSeleccionado() {
        return this.tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public Actividad getActividad() {
        return this.actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    /**
     * Método que retorna un {@link boolean} usado para saber si se ha seleccionado o no que existen
     * tareas adicionales y de ésta manera habilitar el panel de detalle de las tareas adicionales.
     *
     * @return
     */
    public boolean isTareasAdicionalesBool() {
        if (this.tareasAdicionalesSiNo.equals(ESiNo.SI.getCodigo())) {
            return true;
        } else {
            return false;
        }
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método que realiza la inicialización de las diferentes variables usadas en la pantalla a
     * partir del {@link Tramite} y el {@link TramiteDepuracion} .
     *
     * @return
     */
    public void cargarInformacionTramite() {

        // Cargue de la comisión del trámite seleccionado
        if (this.tramiteSeleccionado.getComisionTramites() != null) {
            for (ComisionTramite ct : this.tramiteSeleccionado
                .getComisionTramites()) {
                //javier.aponte se cambia de CANCELADA.getValor() a CANCELADA.toString() porque en la base de datos
                //el estado de la comisión se está guardando como 'CANCELADA' y no como 'Cancelada' luego esta condición
                //nunca se estaba cumpliendo
                if (ct.getComision().getEstado() != null &&
                     !ct.getComision().getEstado()
                        .equals(EComisionEstado.CANCELADA.toString()) &&
                     !ct.getComision().getEstado()
                        .equals(EComisionEstado.FINALIZADA.toString())) {
                    this.comisionDelTramite = ct.getComision();
                    this.documentoComision = this.getTramiteService()
                        .buscarDocumentoPorId(
                            ct.getComision().getMemorandoDocumentoId());
                }
            }
        }

        // Búsqueda de los inconvenientes
        this.inconvenientes = new HashMap<String, String>();
        List<Dominio> inconvenientesDominio = this.getGeneralesService()
            .buscarDominioPorNombreOrderByNombre(
                EDominio.INCONVENIENTES_REG_LEV_TOPO);
        if (inconvenientesDominio != null) {
            for (Dominio d : inconvenientesDominio) {
                this.inconvenientes.put(d.getValor(), d.getCodigo());
            }
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que avanza el proceso una vez se ha generado el informe de levantamiento topográfico.
     *
     * @return
     */
    public String avanzarProceso() {
        try {

            // TODO :: Definir la transición a avanzar cuando se haya
            // definido lo de depuración en process :: 05/08/2013
            String transicion = ProcesoDeConservacion.ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO;
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();

            // TODO :: Reemplazar usuario cuando se defina en process ::
            // 05/08/2013
            List<UsuarioDTO> usuarios = this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario
                        .getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);

            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral.setTransicion(transicion);
            this.getProcesosService().avanzarActividad(this.usuario,
                this.actividad.getId(), solicitudCatastral);

            UtilidadesWeb.removerManagedBeans();
            this.tareasPendientesMB.init();
            return ConstantesNavegacionWeb.INDEX;

        } catch (Exception e) {
            LOGGER.error(
                "Error al avanzar el proceso en RegistroInformeLevantamientoTopograficoMB#avanzarProceso",
                e);
            this.addMensajeError("No fue posible avanzar el proceso para el trámite seleccionado.");
            return null;
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que persiste los datos ingresados en la base de datos, y realiza la validación
     * respectiva por cada uno de ellos.
     */
    public void guardar() {
        try {
            this.generarDocumentoBool = false;

            // TODO :: Implementar cuando se definan las entidades para la
            // información manejada en el caso de uso.
            this.addMensajeInfo("Se guardó la información adecuadamente.");
            this.generarDocumentoBool = true;
        } catch (Exception e) {
            LOGGER.error(
                "Error al avanzar el proceso en RegistroInformeLevantamientoTopograficoMB#guardar",
                e);
            this.addMensajeError("Error al almacenar la información suministrada.");
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que realiza el llamado a la generación del reporte del informe de levantamiento
     * topográfico.
     */
    public void generarDocumento() {
        try {
            this.avanzarProcesoBool = false;

            // TODO :: Reemplazar los parámetros del reporte cuando se defina,
            // mientras tanto queda quemado para pruebas
            EReporteServiceSNC enumeracionReporte =
                EReporteServiceSNC.SOLICITUD_DOCUMENTOS_FALTANTES;

            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID", "" + this.tramiteSeleccionado.getId());

            List<UsuarioDTO> usuariosTest = (List<UsuarioDTO>) this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario
                        .getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);

            // Set de los parámetros del reporte
            parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                usuariosTest.get(0).getNombreCompleto());

            this.reporte = reportsService.generarReporte(
                parameters, enumeracionReporte, this.usuario);

            this.avanzarProcesoBool = true;
        } catch (Exception e) {
            LOGGER.error(
                "Error al avanzar el proceso en RegistroInformeLevantamientoTopograficoMB#generarDocumento",
                e);
            this.addMensajeError("Error al generar el reporte.");
        }
    }

    // ------------------------------------------------ //
    /**
     * Método para retornar al arbol de tareas, action del botón "cerrar"que forza el init del MB.
     *
     * @return
     */
    public String cerrar() {
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb
            .getManagedBean("tareasPendientes");
        tareasPendientesMB.cerrar();
        return ConstantesNavegacionWeb.INDEX;
    }

}
