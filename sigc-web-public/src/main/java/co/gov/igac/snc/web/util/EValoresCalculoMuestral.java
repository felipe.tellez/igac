package co.gov.igac.snc.web.util;

/**
 *
 * @author ariel.ortiz
 *
 * @description Enumeracion usada par almacenar los posibles valores de la variable de calculo de
 * muestras para el control de calidad del caso de uso CU-TV-0F-08
 *
 * @cu CU-TV-0F-08
 *
 * @version 1.0
 */
public enum EValoresCalculoMuestral {

    AREA_CONSTRUIDA("Area construida"),
    AREA_TERRENO("Area de terreno"),
    VALOR_CALCULADO("Valor calculado"),
    VALOR_PEDIDO("Valor pedido");

    private String valorVariable;

    private EValoresCalculoMuestral(String variableSeleccion) {
        this.valorVariable = variableSeleccion;
    }

    @Override
    public String toString() {
        return this.valorVariable;
    }

}
