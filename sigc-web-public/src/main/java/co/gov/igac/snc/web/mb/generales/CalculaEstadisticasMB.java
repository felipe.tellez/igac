package co.gov.igac.snc.web.mb.generales;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.util.CalculosEstadisticos;
import co.gov.igac.snc.util.Estadistica;
import co.gov.igac.snc.web.util.SNCManagedBean;

@Component("calculaEstadisticas")
@Scope("application")
/**
 * Clase para que tiene funciones para realizar cálculos estadísticos
 *
 * @author javier.aponte
 *
 */
public class CalculaEstadisticasMB extends SNCManagedBean {

    private static final long serialVersionUID = 2840287569646192485L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CalculaEstadisticasMB.class);

    /**
     * Método que calcula las medidas estadísticas a partir de las variables numéricas de un
     * conjunto de datos, devuelve una lista de objetos de tipo CalculosEstadisticos los resultados
     * se encuentran en el mismo orden en el que envien los datos, es decir, el resultado de los
     * calculos estadísticos para el arreglo de double que estaba en la posición 0 se devolvera en
     * la posición 0 de la lista de resultado
     *
     * @param clase
     * @param objetos
     * @return
     * @author javier.aponte
     */
    public CalculosEstadisticos[] calcularMedidasEstadisticas(List<Double[]> datos) {

        LOGGER.debug("En CalculaEstadisticasMB#calcular");

        CalculosEstadisticos calculosEstadisticos;

        double[] muestra;

        int cantidadVariables = datos.get(0).length;

        CalculosEstadisticos[] answer = new CalculosEstadisticos[cantidadVariables];

        for (int j = 0; j < cantidadVariables; j++) {
            muestra = new double[datos.size()];
            for (int i = 0; i < datos.size(); i++) {
                if (datos.get(i)[j] != null) {
                    muestra[i] = datos.get(i)[j];
                } else {
                    muestra[i] = 0.0;
                }
            }

            calculosEstadisticos = new CalculosEstadisticos();

            //Almacena la media aritmetica del conjunto de datos
            calculosEstadisticos.setMedia(Estadistica.media(muestra));

            //Almacena la varianza del conjunto de datos
            calculosEstadisticos.setVarianza(Estadistica.varianza(muestra, calculosEstadisticos.
                getMedia()));

            //Almacena el coeficiente de asimetria
            calculosEstadisticos.setCoeficienteAsimetria(Estadistica.coeficienteAsimetria(muestra,
                calculosEstadisticos.getMedia()));

            answer[j] = calculosEstadisticos;

            LOGGER.debug("El valor de la media es: " + calculosEstadisticos.getMedia());

            LOGGER.debug("El valor de la varianza es: " + calculosEstadisticos.getVarianza());

        }

        return answer;

    }

}
