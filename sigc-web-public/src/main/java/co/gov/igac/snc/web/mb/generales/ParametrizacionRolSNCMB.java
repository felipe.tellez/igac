package co.gov.igac.snc.web.mb.generales;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.persistence.entity.generales.Grupo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * @description Managed bean para gestionar la parametrización de los roles de un usuario que se
 * utilizan para el SNC.
 *
 * @cu CU-NP-CO-264 Parametrizar roles de usuarios en el SNC
 *
 * @version 1.0
 *
 * @author david.cifuentes
 */
@Component("parametrizacionRolSNC")
@Scope("session")
public class ParametrizacionRolSNCMB extends SNCManagedBean implements
    Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = 6823784090033911669L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ParametrizacionRolSNCMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Lista de roles existentes en el SNC
     */
    private List<Grupo> rolesSNC;

    /**
     * Variable que almacena el rol seleccionado
     */
    private Grupo rolSeleccionado;

    /**
     * Lista de select items con los roles en el LDAP para una determinada territorial.
     */
    private List<String> rolesLDAP;

    /**
     * Variable que almacena el rol de LDAP seleccionado
     */
    private String rolLDAPSeleccionado;

    /**
     * Variable usada para la selección de los roles en el SNC
     */
    private boolean activarBool;
    private boolean desactivarBool;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - ParametrizacionRolUsuarioMB");

        try {
            this.usuario = MenuMB.getMenu().getUsuarioDto();

            // Roles existentes en la BD
            this.rolesSNC = this.getGeneralesService().buscarGrupos();

            // Roles existentes en LDAP
            this.cargarRolesLDAP();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en el cargue de la información de roles.");
        }
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<Grupo> getRolesSNC() {
        return rolesSNC;
    }

    public void setRolesSNC(List<Grupo> rolesSNC) {
        this.rolesSNC = rolesSNC;
    }

    public List<String> getRolesLDAP() {
        return rolesLDAP;
    }

    public void setRolesLDAP(List<String> rolesLDAP) {
        this.rolesLDAP = rolesLDAP;
    }

    public boolean isActivarBool() {
        return activarBool;
    }

    public void setActivarBool(boolean activarBool) {
        this.activarBool = activarBool;
    }

    public boolean isDesactivarBool() {
        return desactivarBool;
    }

    public void setDesactivarBool(boolean desactivarBool) {
        this.desactivarBool = desactivarBool;
    }

    public String getRolLDAPSeleccionado() {
        return rolLDAPSeleccionado;
    }

    public void setRolLDAPSeleccionado(String rolLDAPSeleccionado) {
        this.rolLDAPSeleccionado = rolLDAPSeleccionado;
    }

    public Grupo getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(Grupo rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de roles en LDAP
     *
     * @author david.cifuentes
     */
    public void cargarRolesLDAP() {

        this.rolesLDAP = new ArrayList<String>();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();

        List<String> roles = new ArrayList<String>();

        usuarios = this.getTramiteService().buscarTodosFuncionarios();

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los roles de LDAP.");
            return;
        }

        for (UsuarioDTO user : usuarios) {
            if (user.getRoles() != null) {
                for (String rol : user.getRoles()) {

                    if (!roles.contains(rol)) {
                        roles.add(rol);
                    }

                    if (roles != null) {
                        Collections.sort(roles);
                    }
                }
            }
        }

        if (roles != null) {
            for (String rol : roles) {
                this.rolesLDAP.add(rol);
            }
        }

        // Filtrar los roles existentes en el SNC
        this.filtrarRolesLDAP();
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el filtro de los roles de LDAP, retirando los que ya han sido activados,
     * es decir los que se encuentran registrados en el SNC
     *
     * @author david.cifuentes
     */
    public void filtrarRolesLDAP() {

        List<String> rolesRegistradosSNC = new ArrayList<String>();

        if (this.rolesSNC != null) {
            for (Grupo grupo : this.rolesSNC) {
                rolesRegistradosSNC.add(grupo.getNombre().toUpperCase());
            }
        }
        if (this.rolesLDAP != null) {
            this.rolesLDAP.removeAll(rolesRegistradosSNC);
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @author david.cifuentes
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {
        EstructuraOrganizacional estructuraOrganizacional, answer = null;
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            if (eo.getCodigo().equals(codigoTerritorial)) {
                answer = eo;
                break;
            }
        }
        if (answer != null) {
            estructuraOrganizacional = this
                .getGeneralesService()
                .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
            if (estructuraOrganizacional != null) {
                return estructuraOrganizacional;
            } else {
                return answer;
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de roles de una estructura organizacional seleccionada
     *
     * @param estructuraOrganizacional
     * @author david.cifuentes
     * @return
     */
    public Map<String, List<String>> cargarRolesEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacional,
        Map<String, List<String>> rolesEstructuraOrganizacional,
        boolean esTerritorial) {

        List<String> rolesTerritorial = new ArrayList<String>();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();

        List<String> roles = new ArrayList<String>();

        String nombreUOC = estructuraOrganizacional.getNombre().replace(' ', '_');

        if (this.usuario.getDescripcionTerritorial().equals(estructuraOrganizacional.getNombre())) {
            usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), null);
        } else if (estructuraOrganizacional.getNombre() != null && estructuraOrganizacional.
            getNombre().contains(" ")) {
            usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), nombreUOC);
        } else if (estructuraOrganizacional.getNombre() != null) {
            usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), null);
        }

        if (this.usuario.getDescripcionUOC() != null &&
             this.usuario.getDescripcionUOC().equals(estructuraOrganizacional)) {
            usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                getDescripcionTerritorial(), nombreUOC);
        }

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para " +
                 estructuraOrganizacional.getNombre());
            return null;
        }

        for (UsuarioDTO user : usuarios) {
            for (String rol : user.getRoles()) {

                if (!roles.contains(rol)) {
                    roles.add(rol);
                }

                if (roles != null) {
                    Collections.sort(roles);
                }
            }
        }

        if (roles != null) {
            for (String rol : roles) {
                rolesTerritorial.add(new String(rol));
            }
            rolesEstructuraOrganizacional.
                put(estructuraOrganizacional.getCodigo(), rolesTerritorial);
        }
        return rolesEstructuraOrganizacional;
    }

    // ----------------------------------------------------- //
    /**
     * Método que guarda un rol del LDAP como {@link Grupo} activo en el SNC
     *
     * @author david.cifuentes
     */
    public void guardarRol() {

        if (this.rolLDAPSeleccionado != null) {
            try {
                Grupo grupo = new Grupo();
                grupo.setActivo(ESiNo.SI.getCodigo());
                grupo.setDescripcion("Rol creado en la parametrización de roles del SNC.");
                grupo.setFechaLog(new Date(System.currentTimeMillis()));
                grupo.setUsuarioLog(this.usuario.getLogin());
                grupo.setNombre(this.rolLDAPSeleccionado);
                this.getGeneralesService().guardarGrupo(grupo);
                this.init();
                this.addMensajeInfo("Se guardó el rol " +
                     this.rolLDAPSeleccionado + " satisfactoriamente.");
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Ocurrió un error en la creación del rol.");
                return;
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la desactivación de un rol dentro del SNC
     *
     * @author david.cifuentes
     */
    public void desactivarRol() {
        if (this.rolSeleccionado != null) {
            try {
                // Verificar tareas en proceso del rol

                Map<EParametrosConsultaActividades, String> filtros = null;
                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(EParametrosConsultaActividades.ESTADOS,
                    EEstadoActividad.TAREAS_VIGENTES);
                filtros.put(EParametrosConsultaActividades.ROL_CONSERVACION,
                    this.rolSeleccionado.getDescripcion());

                List<Actividad> actividades = this.getProcesosService()
                    .consultarListaActividades(filtros);

                if (actividades == null || actividades.isEmpty()) {
                    this.rolSeleccionado.setActivo(ESiNo.NO.getCodigo());
                    this.getGeneralesService().guardarGrupo(
                        this.rolSeleccionado);
                    this.addMensajeInfo("Se desactivó el rol " +
                         this.rolSeleccionado.getNombre() +
                         " satisfactoriamente.");
                } else {
                    this.addMensajeError("El rol " + this.rolSeleccionado.getNombre() +
                        
                        " no puede ser desactivado debido a que existen actividades vigentes asignadas a éste.");
                    this.addErrorCallback();
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Ocurrió un error en la desactivación del rol.");
                return;
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la activación de un rol que se encuentra en estado inactivo dentro del
     * SNC.
     *
     * @author david.cifuentes
     */
    public void activarRol() {
        if (this.rolSeleccionado != null) {
            try {
                this.rolSeleccionado.setActivo(ESiNo.SI.getCodigo());
                this.getGeneralesService().guardarGrupo(this.rolSeleccionado);
                this.addMensajeInfo("Se activó el rol " +
                     this.rolSeleccionado.getNombre() +
                     " satisfactoriamente.");
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en la activación del rol seleccionado.");
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de los roles del SNC para activar y desactivar los mismos
     *
     * @author david.cifuentes
     * @param event
     */
    public void seleccionarRol(SelectEvent event) {
        this.rolSeleccionado = (Grupo) event.getObject();
        this.activarBool = false;
        this.desactivarBool = false;
        if (this.rolSeleccionado != null) {
            if (this.rolSeleccionado.isEstadoActivo()) {
                this.desactivarBool = true;
            } else {
                this.activarBool = true;
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la deselección de los roles del SNC
     *
     * @author david.cifuentes
     * @param event
     */
    public void deseleccionarRol(UnselectEvent event) {
        this.rolSeleccionado = null;
        this.activarBool = false;
        this.desactivarBool = false;
    }

    // ----------------------------------------------------- //
    /**
     * Método cerrar que forza realiza un llamado al método que remueve los managed bean de la
     * sesión y forza el init de tareasPendientesMB.
     *
     * @author david.cifuentes
     * @return
     */
    public String cerrar() {
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    // Fin de la clase
}
