package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EActualizacionContratoActividad;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 *
 *
 * @author javier.aponte
 *
 */
@Component("gestionarAsignacionDeTopografos")
@Scope("session")
public class GestionAsignacionDeTopografosMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -2964745655243791827L;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    private String nuevoTopografo;
    private String manzanaCodigo;

    private List<ActualizacionLevantamiento> actualizacionLevantamientos;
    private List<RecursoHumano> listaTopografos;

    /**
     * ArrayList de tipo SelectItem para selcción de topógrafos
     */
    private ArrayList<SelectItem> itemListTopografos;

    /**
     * Topógrafo seleccionado en el select one menu
     */
    private RecursoHumano topografoSeleccionado;

    /**
     * Actualizaciónes levantamientos seleccionados en la tabla de mazanas y áreas
     */
    private ActualizacionLevantamiento[] selectedActualizacionesLevantamientos;

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.manzanaCodigo = "08001010300000439,08001010300000520,08001010300000779";
        this.moduleLayer = "ac";
        this.moduleTools = "ac";

        this.actualizacionLevantamientos = this.getActualizacionService().
            buscarActualizacionLevantamientoPorActualizacionId(450L, null);

        this.listaTopografos = this.getActualizacionService().
            obtenerRecursoHumanoPorActividad(EActualizacionContratoActividad.TOPOGRAFO.getCodigo());

        this.itemListTopografos = new ArrayList<SelectItem>();
        for (RecursoHumano rh : listaTopografos) {
            itemListTopografos.add(new SelectItem(rh, rh.getNombre()));
        }

    }

    // **************************************************************************************************************************
    // Getters y Setters 
    // **************************************************************************************************************************
    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getNuevoTopografo() {
        return nuevoTopografo;
    }

    public void setNuevoTopografo(String nuevoTopografo) {
        this.nuevoTopografo = nuevoTopografo;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public List<RecursoHumano> getListaTopografos() {
        return listaTopografos;
    }

    public void setListaTopografos(List<RecursoHumano> listaTopografos) {
        this.listaTopografos = listaTopografos;
    }

    public ArrayList<SelectItem> getItemListTopografos() {
        return itemListTopografos;
    }

    public void setItemListTopografos(ArrayList<SelectItem> itemListTopografos) {
        this.itemListTopografos = itemListTopografos;
    }

    public RecursoHumano getTopografoSeleccionado() {
        return topografoSeleccionado;
    }

    public void setTopografoSeleccionado(RecursoHumano topografoSeleccionado) {
        this.topografoSeleccionado = topografoSeleccionado;
    }

    public ActualizacionLevantamiento[] getSelectedActualizacionesLevantamientos() {
        return selectedActualizacionesLevantamientos;
    }

    public void setSelectedActualizacionesLevantamientos(
        ActualizacionLevantamiento[] selectedActualizacionesLevantamientos) {
        this.selectedActualizacionesLevantamientos = selectedActualizacionesLevantamientos;
    }

    public List<ActualizacionLevantamiento> getActualizacionLevantamientos() {
        return actualizacionLevantamientos;
    }

    public void setActualizacionLevantamientos(
        List<ActualizacionLevantamiento> actualizacionLevantamientos) {
        this.actualizacionLevantamientos = actualizacionLevantamientos;
    }

    public void asignarTopografo() {

        if (this.selectedActualizacionesLevantamientos != null) {

            LevantamientoAsignacion levantamientoAsignacionActualizado;

            levantamientoAsignacionActualizado = this.getActualizacionService().
                asignarTopografoAManzanasYAreas(this.selectedActualizacionesLevantamientos,
                    this.topografoSeleccionado, this.usuario);

            if (levantamientoAsignacionActualizado != null) {

                for (ActualizacionLevantamiento als : this.selectedActualizacionesLevantamientos) {
                    for (ActualizacionLevantamiento al : this.actualizacionLevantamientos) {

                        if (als.getId().equals(al.getId())) {
                            al.setLevantamientoAsignacion(levantamientoAsignacionActualizado);
                        }

                    }
                }
            } else {
                this.addMensajeError(
                    "Ocurrió un error no se pudo asignar el topógrafo a las manzanas");
                return;
            }

        }

    }

    public void finalizarAsignacionTopografo() {

        for (ActualizacionLevantamiento temp : this.actualizacionLevantamientos) {
            if (temp.getLevantamientoAsignacion() == null) {
                this.addMensajeError(
                    "Faltan manzanas / áreas por asignar, no se puede finalizar la asignación hasta que todas las" +
                    "manzanas / áreas estén asignadas a un topógrafo");
                return;
            }
        }
    }

}
