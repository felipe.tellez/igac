package co.gov.igac.snc.web.jobs;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;
import static co.gov.igac.snc.web.jobs.ReenviarJobTask.LOGGER;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Representa la tarea de enviar por paquetes la aplicacion de cambios geograficos
 *
 * @author andres.eslava
 */
public class AplicarCambiosJobTask extends AbstractLocator implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = -7332773578521079455L;
    public static Logger LOGGER = LoggerFactory.getLogger(ReenviarJobTask.class);

    private ProductoCatastralJob job;

    /**
     *
     * @param job a ejecutar
     */
    public AplicarCambiosJobTask(ProductoCatastralJob job) {
        this.job = job;
    }

    /**
     * Método encargado de enviar la ejecución del job pendiente
     */
    public void run() {
        try {
            this.getGeneralesService().aplicarCambiosGeograficos(this.job);
        } catch (Exception e) {
            LOGGER.error("Error enviando el JOB de aplicacion de cambios geografica", e);
            e.printStackTrace();
        }

    }

}
