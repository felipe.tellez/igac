package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("adminInformacionAvaluoComercial")
@Scope("session")
public class AdminInformacionAvaluoComercialMB extends SNCManagedBean {

    private static final long serialVersionUID = -6287257168944246293L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdminInformacionAvaluoComercialMB.class);

    private Avaluo avaluo;

    private AvaluoPredio[] prediosSeleccionados;

    /**
     * usuario actual del sistema
     */
    private UsuarioDTO usuario;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    private ReporteDTO reporteDTO;

    /**
     * Condicion juridica del avaluo
     */
    private String condicionJuridica;

    /**
     * Marco juridico
     */
    private String marcoJuridico;

    /**
     * Otro marco juridico
     */
    private String otroMarcoJuridico;

    /**
     * Distancia a la capital del departamento
     */
    private String distanciaAlaCapitalDelDpto;

    /**
     * Bandera para indicar si se requiere tramite catastral
     */
    private boolean banderaRequiereTramiteCatastral;

    /**
     * Bandera para indicar si los predios estan completos
     */
    private boolean banderaPrediosCompletos;

    /*
     * -------- SETTERS Y GETTERS
     */
    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ReportesUtil getReportsService() {
        return reportsService;
    }

    public void setReportsService(ReportesUtil reportsService) {
        this.reportsService = reportsService;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    public String getCondicionJuridica() {
        return condicionJuridica;
    }

    public void setCondicionJuridica(String condicionJuridica) {
        this.condicionJuridica = condicionJuridica;
    }

    public String getMarcoJuridico() {
        return marcoJuridico;
    }

    public void setMarcoJuridico(String marcoJuridico) {
        this.marcoJuridico = marcoJuridico;
    }

    public String getOtroMarcoJuridico() {
        return otroMarcoJuridico;
    }

    public void setOtroMarcoJuridico(String otroMarcoJuridico) {
        this.otroMarcoJuridico = otroMarcoJuridico;
    }

    public String getDistanciaAlaCapitalDelDpto() {
        return distanciaAlaCapitalDelDpto;
    }

    public void setDistanciaAlaCapitalDelDpto(String distanciaAlaCapitalDelDpto) {
        this.distanciaAlaCapitalDelDpto = distanciaAlaCapitalDelDpto;
    }

    public boolean isBanderaRequiereTramiteCatastral() {
        return banderaRequiereTramiteCatastral;
    }

    public void setBanderaRequiereTramiteCatastral(
        boolean banderaRequiereTramiteCatastral) {
        this.banderaRequiereTramiteCatastral = banderaRequiereTramiteCatastral;
    }

    public boolean isBanderaPrediosCompletos() {
        return banderaPrediosCompletos;
    }

    public void setBanderaPrediosCompletos(boolean banderaPrediosCompletos) {
        this.banderaPrediosCompletos = banderaPrediosCompletos;
    }

    public AvaluoPredio[] getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(AvaluoPredio[] prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio AdminInformacionAvaluoComercialMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin AdminInformacionAvaluoComercialMB#init");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio AdminInformacionAvaluoComercialMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(
            172L);

        LOGGER.debug("Fin AdminInformacionAvaluoComercialMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para procesar evento al seleccionar si se realiza contrato o no
     *
     * @author rodrigo.hernandez
     */
    public void onChangeRealizaContrato() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#onChangeRealizaContrato");

        if (this.banderaPrediosCompletos) {
            //TODO something
        } else {
            //TODO something
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#onChangeRealizaContrato");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cerrar la pagina
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {

        LOGGER.debug("iniciando AdminInformacionAvaluoComercialMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("finalizando AdminInformacionAvaluoComercialMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

}
