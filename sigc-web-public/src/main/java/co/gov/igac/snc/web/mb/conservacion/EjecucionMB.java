/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacion5Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteFuente;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.asignacion.VistaDetallesTramiteMB;
import co.gov.igac.snc.web.mb.conservacion.ejecucion.VistaDetallesSolicitudMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.tramite.gestion.RecuperacionTramitesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.HistoricoProyeccionViaGubernativaDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.TramitePredioEnglobeDTO;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para la ejecución de trámites Reutiliza metodos de asignacionTramites
 *
 * @author juan.agudelo
 */
@SuppressWarnings("deprecation")
@Component("ejecucion")
@Scope("session")
public class EjecucionMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -9064304637726891681L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EjecucionMB.class);

    // --------------------------------- Servicios ----------------------------
    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    @Autowired
    private IContextListener contexto;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private VistaDetallesSolicitudMB vistaDetalleSolicitudMB;

    @Autowired
    private VistaDetallesTramiteMB vistaDetalleTramiteMB;

    @Autowired
    private ValidacionYAdicionNuevoTramiteMB validacionTramiteMB;

    // --------------------------------- Variables ----------------------------
    /**
     * Actividad de procesos
     */
    private Actividad actividad;

    /**
     * Trámite seleccionado
     */
    private Tramite tramiteSeleccionado;

    /**
     * Contiene las pruebas solicitadas para el trámite asociado
     */
    private String pruebasSolicitadas;

    /**
     * Pruebas solicitadas en el trámite, siempre sera solo un TramitePrueba por trámite
     */
    private TramitePrueba tp;

    /**
     * TramiteDocumento para edicion
     */
    private TramiteDocumento tramiteDocumentoTemporal;

    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * Estado futuro del trámite
     */
    private String estadoFuturoTramite;

    /**
     * Valor de decision sobre cancelar el trámite inicial
     */
    private String cancelarTramiteDecision;

    /**
     * Motivo de cancelar el trámite inicial
     */
    private String motivoCancelacion;

    /**
     * Trámite asociado a un trámite seleccionado
     */
    private Tramite tramiteTramiteSeleccionado;

    /**
     * Lista de trámites asociados al trámite
     */
    private List<Tramite> tramiteTramites;

    /**
     * Id del trámite seleccionado, es necesario para todas las funcioens del MB y debe ser seteado
     * desde el arbol de tareas
     */
    private long tramiteSeleccionadoId;

    /**
     * Variable usada para determinar el flujo de la pantalla de revisión de trámites asignados
     */
    private String revisarTramiteAsignadoChoose;

    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;

    /**
     * Lista que almacena los tipos de Mutaciones
     */
    private List<SelectItem> mutacionClases;

    /**
     * Lista que contiene los predios asociados a un trámite
     */
    private List<TramitePredioEnglobe> prediosTramite;

    /**
     * Lista que contiene los predios asociados a un trámite
     */
    private List<TramitePredioEnglobeDTO> prediosTramiteTemp;

    /**
     * Objeto temporal de seleccion en la tabla predioTramite
     */
    private TramitePredioEnglobeDTO selectedPredioTramite;

    /**
     * lista con los valores del combo de tipos de trámite que se permiten para el tipo de solicitud
     * creada
     */
    private ArrayList<SelectItem> tiposTramitePorSolicitud;

    /**
     * Identificaor de la actividad desde el arbol de tareas
     */
    private String idActividad;

    /**
     * Partes del número predial para mutaciones de segunda o quinta
     */
    private String numeroPredialParte1;
    private String numeroPredialParte2;
    private String numeroPredialParte3;
    private String numeroPredialParte4;
    private String numeroPredialParte5;
    private String numeroPredialParte6;
    private String numeroPredialParte7;
    private String numeroPredialParte8;
    private String numeroPredialParte9;
    private String numeroPredialParte10;
    private String numeroPredialParte11;
    private String numeroPredialParte12;

    /**
     * Lista de documentación adicional presente en el trámite seleccionado
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * Documento temporal de trámite
     */
    private TramiteDocumentacion documentoSeleccionado;

    /**
     * Datos geográficos del documento
     */
    private List<Departamento> dptosDocumento;
    private List<Municipio> munisDocumento;
    private Departamento departamentoDocumento;
    private Municipio municipioDocumento;
    private List<SelectItem> municipiosDocumento;
    private List<SelectItem> departamentosDocumento;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * Arreglo que contiene los trámites seleccionados para adicionar documentación
     */
    private Tramite[] tramitesTramiteSeleccionados;

    /**
     * Arreglo que contiene la documentación aportada para asociarla a la lista de trámites
     * asociados
     */
    private TramiteDocumentacion[] documentacionAportada;

    /**
     * Arreglo que contiene la documentación adicional para asociarla a la lista de trámites
     * asociados
     */
    private TramiteDocumentacion[] documentacionAdicionalS;

    /**
     * Variable contadora de la asignaciónd de orden para los trámites seleccionados
     */
    private List<SelectItem> ordenEjecucionTramites;

    /**
     * Ruta del documento cargado
     */
    private String rutaMostrar;

    /**
     * Archivo a cargar en la carpeta temporal
     */
    private File archivoResultado;

    /**
     * nombre temporal del archivo cargado
     */
    private String nombreTemporalArchivo;

    /**
     * Objeto que contiene los datos del reporte de auto de pruebas
     */
    private ReporteDTO reporteAutoDePruebas;

    /**
     * Documento de pruebas temporal
     */
    private Documento documentoPruebaSeleccionado;

    /**
     * Entidades encargadas de las pruebas sobre un trámite
     */
    private List<TramitePruebaEntidad> entidadesPruebas;

    private TramitePruebaEntidad selectedEntidadPruebas;

    private List<TramiteDocumento> documentosSelectedEntidad;

    /**
     * Lista que contiene los documentos de pruebas a eliminar
     */
    private List<TramiteDocumento> documentosPruebasAEliminar;

    private String activityId;

    // ------------rectificación/complementación---------------------------------------------------
    /**
     * tipo de dato para rectificación o complementación seleccionado
     */
    private String tipoDatoRectifComplSeleccionado;

    /**
     * modelo del pick list de datos a modificar o complementar Se requieren dos listas: una fuente
     * y una destino
     */
    private DualListModel<DatoRectificar> datosRectifComplDLModel;

    private List<DatoRectificar> datosRectifComplSource;
    private List<DatoRectificar> datosRectifComplTarget;

    /**
     * lista con los DatoRectificar que se han seleccionado en el pick list
     */
    private List<DatoRectificar> selectedDatosRectifComplPL;

    /**
     * lista auxiliar con los DatoRectificar de la tabla Datos por rectificar o complementar
     */
    private List<DatoRectificar> selectedDatosRectifComplDataTable;

    /**
     * lista con los DatoRectificar que se han seleccionado en el data table de resumen
     */
    private DatoRectificar[] selectedDatosRectifComplDT;

    /**
     * Lista que almacena los documentos asociados a un tramiteTramite
     */
    private List<Integer> documentosAsociadosList;

    /**
     * Estado del tramite
     */
    private TramiteEstado estadoTramite;

    /**
     * Variable para almacenar el mensaje de cancelacion del trámite
     */
    private String mensajeCancelaTramite;

    // ------------- Banderas de edicion --------------------------------------
    /**
     * Bandera que determina si la edicion de solicitar pruebas
     */
    private boolean banderaEditarSolicitarPruebas;

    /**
     * Bandera que determina la visualización de los componentes graficos dependiendo si se aprueban
     * las pruebas solicitadas *
     */
    private boolean banderaAprobarPruebas;

    /**
     * Bandera que determina la visualización de los componentes graficos dependiendo si no se
     * aprueban las pruebas solicitadas *
     */
    private boolean banderaRechazarPruebas;

    /**
     * Bandera que determina el valor de edición de la documentación de pruebas
     */
    private boolean banderaBotonEdicionDocPrueba;

    /**
     * Bandera que determina si se selecciono una clase de mutación
     */
    private boolean banderaClaseDeMutacionSeleccionada;

    /**
     * Bandera que determina si el subtipo de la mutación de segunda es englobe
     */
    private boolean banderaSubtipoEnglobe;

    /**
     * Bandera que determina si el subtipo de la mutación de quinta es nuevo
     */
    private boolean banderaSubtipoNuevo;

    /**
     * bandera para indicar si el subtipo de trámite es dato requerido
     */
    private boolean subtipoTramiteEsRequerido;

    /**
     * Bandera que indica si es tramiteTramite seleccionado es nuevo o va a ser editado
     */
    private boolean banderaEdicionTramiteTramite;

    /**
     * Bandera que indica si se guardó el motivo de rechazo de pruebas en el caso de uso de aprobar
     * realizar pruebas
     */
    private boolean banderaGuardoRechazoPruebas;

    /**
     * Motivo que se dispone en el tramiteEstado que se usa para la devolución del trámite
     */
    private String motivoDevolucion;

    /**
     * Observaciones que se asignan en el tramiteEstado que se usa para la devolución del trámite
     */
    private String observacionesDevolucion;

    /**
     * Bandera de visualización de los documentos para asociar al trámite
     */
    private boolean banderaAsociarDocumentos;

    /**
     * Lista que contiene los predios colindantes para englobe asociados al predio seleccionado
     */
    private List<Predio> listaPrediosColindantes;

    /**
     * Variable usada en un pickListpara cargar los motivos de devolución de un trámtite.
     */
    private DualListModel<String> motivosDevolucionTramites;

    /**
     * Objeto temporal de seleccion en la tabla predioColindantes
     */
    private Predio selectedPredioColindante;

    public List<SelectItem> getMutacionClases() {
        if (this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
            List<SelectItem> mutClass = this.generalMB.getMutacionClase();
            this.mutacionClases = new LinkedList<SelectItem>();

            if (mutClass != null && !mutClass.isEmpty()) {
                for (SelectItem s : mutClass) {
                    if (!s.getLabel().equals(EMutacionClase.CUARTA.getNombre()) &&
                        !s.getLabel().equals(EMutacionClase.QUINTA.getNombre())) {
                        //
                        if (s.getLabel().equals(EMutacionClase.PRIMERA.getNombre())) {
                            if (!this.tramiteSeleccionado.isQuintaNuevo()) {
                                this.mutacionClases.add(s);
                            }
                        } else if (s.getLabel().equals(EMutacionClase.TERCERA.getNombre())) {
                            if ((this.tramiteSeleccionado.isTercera() || this.tramiteSeleccionado.
                                isQuintaNuevo()) &&
                                this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.NO.getCodigo())) {
                                //
                            } else {
                                this.mutacionClases.add(s);
                            }
                        } else if (s.getLabel().equals(EMutacionClase.SEGUNDA.getNombre())) {
                            if (this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.NO.getCodigo()) &&
                                this.tramiteSeleccionado.isQuintaNuevo()) {
                                // 
                            } else {
                                this.mutacionClases.add(s);
                            }
                        } else {
                            this.mutacionClases.add(s);
                        }
                    }
                }
            }
        } else {
            this.mutacionClases = this.generalMB.getMutacionClase();
        }
        return mutacionClases;
    }

    public void setMutacionClases(List<SelectItem> mutacionClases) {
        this.mutacionClases = mutacionClases;
    }

    /**
     * Bandera de visualización del detalle del trámite en ejecución
     */
    private boolean banderaVerDetalleTramiteEjecucion;

    /**
     * Bandera de solicitud de documentos adicionales
     */
    private boolean banderaDocumentoAdicional;

    /**
     * Bandera para saber si se está editando un documento adicional
     */
    private boolean banderaEditandoDocumentoAdicional;

    /**
     * bandera que indica si un documento (requerido/adicional) se encuentra en edición o es de
     * lectura
     */
    private boolean editandoDocumentacion;

    /**
     * Bandera de visualización de datos para docuemntos encontrados en correspondencia
     */
    private boolean banderaDocumentoEncontradoEnCorrrespondencia;

    /**
     * Bandera que se activa si se cargo un documento de soporte para la prueba seleccionada
     */
    private boolean banderaDocumentoCargado;

    /**
     * Bandera que activa la visualización del boton guardar si el documentto fue cargado y si se
     * encontro un documento valido en radicacion
     */
    private boolean banderaBotonGuardarDocPrueba;

    /**
     * Bandera que indica si el documento ha sido radicado
     */
    private boolean banderaDocumentoRadicado;

    /**
     * Bandera que indica si se selecciono el boton de guardar No procedencia
     */
    private boolean banderaGuardarMotivo;

    private String numeroRadicado;

    /**
     * Bandera que indica si se selecciono generar documento
     */
    private boolean banderaGenerarDocumento;

    /**
     * Bandera que indica si se selecciono rechazar tramite
     */
    private boolean banderaRechazarTramite;

    /**
     * Objeto que contiene los datos del reporte de oficio de no procedencia
     */
    private ReporteDTO reporteOficioNoProcedencia;

    /**
     * Bandera que inidica si se debe mostrar el botón de generar documento de auto de pruebas
     */
    private boolean banderaGenerarDocumentoAutoDePruebas;

    /**
     * Bandera para mostrar el tramite asociado sin permitir editarlo
     */
    private boolean banderaVerTramitePorAsociar;

    /**
     * Variable booleana para saber si se van a ver los documentos asociados o si se va a solicitar
     * nuevos Documentos
     */
    private Boolean verSolicitudDocsBool;

    /**
     * documentación del trámite que se ha seleccionado de la tabla
     */
    private TramiteDocumentacion selectedTramiteDocumentacion;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * datos para el corre
     */
    private Object[] datosCorreo;

    @Autowired
    private IContextListener context;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * Variable para cargar las inconsistencias geográficas del predio.
     */
    private String inconsistenciasGeograficas;

    /**
     * Lista de {@link TramiteInconsistencia} con las inconsistencias modificadas del
     * {@link Tramite}
     */
    private List<TramiteInconsistencia> inconsistenciasTramite;

    /**
     * Variable {@link String} para cargar otras razones que justifiquen el envio a depuración
     * geográfica.
     */
    private String razonezDepuracionGeografica;

    /**
     * Variable {@link String} que almacena los parámetros del editor geográfico.
     */
    private String parametroEditorGeografico;

    /**
     * Bandera para hacer render el componente de trámites nuevos de la página, cuando es tramite de
     * quinta nuevo
     */
    private boolean habilitarTramitesAdicionales;

    /**
     * Bandera para habilitar los botones guardar y generar documento, No procedencia
     */
    private boolean habilitarBotonesNoProcedencia;

    /**
     * Variable {@link String} que almacena las razones de rechazo depuración.
     */
    private String razonesRechazoDepuracion;

    /**
     * Bandera para habilitar las razones de rechazo depuración
     */
    private boolean habilitarRazonesRechazoDepuracion;

    /**
     * Bandera para habilitar botones Proyectar y Auto de prueba; para vía gubernativa se valida que
     * el predio asociado al recurso de vía gubernativa tenga datos de proyección
     */
    private boolean validacionViaGubernativa;

    /**
     * Bandera para habilitar únicamente el botón de No procede para trámites de vía gubernativa, en
     * el cual el trámite original se encuentra en aplicar cambios o en estado FINALIZADO_APROBADO
     */
    private boolean habilitarSoloTramiteNoProcede;

    /**
     * Bandera para validar que cuando el trámite ya aplicó cambios en la actividad Revisar trámite
     * asignado solo debe aparecer la opción No procede.
     */
    private boolean validacionViaGubernativa2;

    /**
     * Lista que contiene los históricos de las proyecciones para vía gubernativa
     */
    private List<HistoricoProyeccionViaGubernativaDTO> listaTramitesHistoricoProyeccion;

    /**
     * Banderas para gestion de botones dependiendo el tramite en revisar tràmite
     */
    private boolean banderaOficinaDepuracion;
    private boolean banderaOficina;
    private boolean banderaTerrenoGIS;
    private boolean banderaOficinaDepuracionAsociar;
    private boolean banderaTerreno;
    private boolean banderaAutoRevision;
    private boolean banderaViaGubernativa;
    private boolean banderaPrediosFiscales;
    private boolean banderaGeneraDocumento;

    /**
     * Bandera para determinar si un tramite tiene replica de consulta asociada.
     */
    private boolean banderaReplicaConsulta;

    private static final String separadorCarpeta = System.getProperty("file.separator");

    private String rutaTemporalFoto;

    /**
     * Variable usada para habilitar o deshabilitar botones en la pantalla de revisar trámite
     * asignado para trámites de vía gubernativa.
     */
    private boolean validacionTramitesReferencia;

    private boolean usuarioDelegado;

    // ------------- methods --------------------------------------------------
    public String getRutaTemporalFoto() {
        return rutaTemporalFoto;
    }

    public void setRutaTemporalFoto(String rutaTemporalFoto) {
        this.rutaTemporalFoto = rutaTemporalFoto;
    }

    public boolean isBanderaGeneraDocumento() {
        return banderaGeneraDocumento;
    }

    public void setBanderaGeneraDocumento(boolean banderaGeneraDocumento) {
        this.banderaGeneraDocumento = banderaGeneraDocumento;
    }

    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public String getCancelarTramite() {
        return cancelarTramiteDecision;
    }

    public boolean isBanderaReplicaConsulta() {
        return banderaReplicaConsulta;
    }

    public void setBanderaReplicaConsulta(boolean banderaReplicaConsulta) {
        this.banderaReplicaConsulta = banderaReplicaConsulta;
    }

    public boolean isBanderaOficinaDepuracion() {
        return banderaOficinaDepuracion;
    }

    public void setBanderaOficinaDepuracion(boolean banderaOficinaDepuracion) {
        this.banderaOficinaDepuracion = banderaOficinaDepuracion;
    }

    public boolean isBanderaTerrenoGIS() {
        return banderaTerrenoGIS;
    }

    public void setBanderaTerrenoGIS(boolean banderaTerrenoGIS) {
        this.banderaTerrenoGIS = banderaTerrenoGIS;
    }

    public boolean isHabilitarSoloTramiteNoProcede() {
        return habilitarSoloTramiteNoProcede;
    }

    public void setHabilitarSoloTramiteNoProcede(
        boolean habilitarSoloTramiteNoProcede) {
        this.habilitarSoloTramiteNoProcede = habilitarSoloTramiteNoProcede;
    }

    public boolean isBanderaOficinaDepuracionAsociar() {
        return banderaOficinaDepuracionAsociar;
    }

    public void setBanderaOficinaDepuracionAsociar(boolean banderaOficinaDepuracionAsociar) {
        this.banderaOficinaDepuracionAsociar = banderaOficinaDepuracionAsociar;
    }

    public boolean isBanderaOficina() {
        return banderaOficina;
    }

    public void setBanderaOficina(boolean banderaOficina) {
        this.banderaOficina = banderaOficina;
    }

    public boolean isBanderaTerreno() {
        return banderaTerreno;
    }

    public void setBanderaTerreno(boolean banderaTerreno) {
        this.banderaTerreno = banderaTerreno;
    }

    public boolean isBanderaAutoRevision() {
        return banderaAutoRevision;
    }

    public void setBanderaAutoRevision(boolean banderaAutoRevision) {
        this.banderaAutoRevision = banderaAutoRevision;
    }

    public boolean isValidacionTramitesReferencia() {
        return validacionTramitesReferencia;
    }

    public void setValidacionTramitesReferencia(boolean validacionTramitesReferencia) {
        this.validacionTramitesReferencia = validacionTramitesReferencia;
    }

    public boolean isBanderaViaGubernativa() {
        return banderaViaGubernativa;
    }

    public void setBanderaViaGubernativa(boolean banderaViaGubernativa) {
        this.banderaViaGubernativa = banderaViaGubernativa;
    }

    public boolean isBanderaClaseDeMutacionSeleccionada() {
        return banderaClaseDeMutacionSeleccionada;
    }

    public boolean isBanderaSubtipoNuevo() {
        return banderaSubtipoNuevo;
    }

    public void setBanderaSubtipoNuevo(boolean banderaSubtipoNuevo) {
        this.banderaSubtipoNuevo = banderaSubtipoNuevo;
    }

    public boolean isBanderaDocumentoEncontradoEnCorrrespondencia() {
        return banderaDocumentoEncontradoEnCorrrespondencia;
    }

    public void setBanderaDocumentoEncontradoEnCorrrespondencia(
        boolean banderaDocumentoEncontradoEnCorrrespondencia) {
        this.banderaDocumentoEncontradoEnCorrrespondencia =
            banderaDocumentoEncontradoEnCorrrespondencia;
    }

    public boolean isBanderaBotonEdicionDocPrueba() {
        return banderaBotonEdicionDocPrueba;
    }

    public void setBanderaBotonEdicionDocPrueba(
        boolean banderaBotonEdicionDocPrueba) {
        this.banderaBotonEdicionDocPrueba = banderaBotonEdicionDocPrueba;
    }

    public boolean isBanderaAsociarDocumentos() {
        return banderaAsociarDocumentos;
    }

    public boolean isBanderaVerDetalleTramiteEjecucion() {
        return banderaVerDetalleTramiteEjecucion;
    }

    public void setBanderaVerDetalleTramiteEjecucion(
        boolean banderaVerDetalleTramiteEjecucion) {
        this.banderaVerDetalleTramiteEjecucion = banderaVerDetalleTramiteEjecucion;
    }

    public TramiteDocumento getTramiteDocumentoTemporal() {
        return tramiteDocumentoTemporal;
    }

    public void setTramiteDocumentoTemporal(
        TramiteDocumento tramiteDocumentoTemporal) {
        this.tramiteDocumentoTemporal = tramiteDocumentoTemporal;
    }

    public Documento getDocumentoPruebaSeleccionado() {
        return documentoPruebaSeleccionado;
    }

    public List<TramiteDocumento> getDocumentosSelectedEntidad() {
        return documentosSelectedEntidad;
    }

    public void setDocumentosSelectedEntidad(
        List<TramiteDocumento> documentosSelectedEntidad) {
        this.documentosSelectedEntidad = documentosSelectedEntidad;
    }

    public DualListModel<String> getMotivosDevolucionTramites() {
        return motivosDevolucionTramites;
    }

    public void setMotivosDevolucionTramites(
        DualListModel<String> motivosDevolucionTramites) {
        this.motivosDevolucionTramites = motivosDevolucionTramites;
    }

    public boolean isBanderaDocumentoCargado() {
        return banderaDocumentoCargado;
    }

    public void setBanderaDocumentoCargado(boolean banderaDocumentoCargado) {
        this.banderaDocumentoCargado = banderaDocumentoCargado;
    }

    public void setDocumentoPruebaSeleccionado(
        Documento documetoPruebaSeleccionado) {
        this.documentoPruebaSeleccionado = documetoPruebaSeleccionado;
    }

    public TramiteDocumentacion[] getDocumentacionAportada() {
        return documentacionAportada;
    }

    public void setDocumentacionAportada(
        TramiteDocumentacion[] documentacionAportada) {
        this.documentacionAportada = documentacionAportada;
    }

    public List<TramitePruebaEntidad> getEntidadesPruebas() {
        return entidadesPruebas;
    }

    public void setEntidadesPruebas(List<TramitePruebaEntidad> entidadesPruebas) {
        this.entidadesPruebas = entidadesPruebas;
    }

    public TramiteDocumentacion[] getDocumentacionAdicionalS() {
        return documentacionAdicionalS;
    }

    public void setDocumentacionAdicionalS(
        TramiteDocumentacion[] documentacionAdicionalS) {
        this.documentacionAdicionalS = documentacionAdicionalS;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public boolean isBanderaBotonGuardarDocPrueba() {
        return banderaBotonGuardarDocPrueba;
    }

    public void setBanderaBotonGuardarDocPrueba(
        boolean banderaBotonGuardarDocPrueba) {
        this.banderaBotonGuardarDocPrueba = banderaBotonGuardarDocPrueba;
    }

    public void setBanderaAsociarDocumentos(boolean banderaAsociarDocumentos) {
        this.banderaAsociarDocumentos = banderaAsociarDocumentos;
    }

    public List<Integer> getDocumentosAsociadosList() {
        return documentosAsociadosList;
    }

    public void setDocumentosAsociadosList(List<Integer> documentosAsociadosList) {
        this.documentosAsociadosList = documentosAsociadosList;
    }

    public String getInconsistenciasGeograficas() {
        return inconsistenciasGeograficas;
    }

    public void setInconsistenciasGeograficas(String inconsistenciasGeograficas) {
        this.inconsistenciasGeograficas = inconsistenciasGeograficas;
    }

    public String getRazonezDepuracionGeografica() {
        return razonezDepuracionGeografica;
    }

    public void setRazonezDepuracionGeografica(String razonezDepuracionGeografica) {
        this.razonezDepuracionGeografica = razonezDepuracionGeografica;
    }

    public Departamento getDepartamentoDocumento() {
        return departamentoDocumento;
    }

    public void setDepartamentoDocumento(Departamento departamentoDocumento) {
        this.departamentoDocumento = departamentoDocumento;
    }

    public Municipio getMunicipioDocumento() {
        return municipioDocumento;
    }

    public void setMunicipioDocumento(Municipio municipioDocumento) {
        this.municipioDocumento = municipioDocumento;
    }

    public Tramite[] getTramitesTramiteSeleccionados() {
        return tramitesTramiteSeleccionados;
    }

    public void setTramitesTramiteSeleccionados(
        Tramite[] tramitesTramiteSeleccionados) {
        this.tramitesTramiteSeleccionados = tramitesTramiteSeleccionados;
    }

    public boolean isBanderaEdicionTramiteTramite() {
        return banderaEdicionTramiteTramite;
    }

    public List<SelectItem> getOrdenEjecucionTramites() {
        return ordenEjecucionTramites;
    }

    public void setOrdenEjecucionTramites(
        List<SelectItem> ordenEjecucionTramites) {
        this.ordenEjecucionTramites = ordenEjecucionTramites;
    }

    public void setBanderaEdicionTramiteTramite(
        boolean banderaEdicionTramiteTramite) {
        this.banderaEdicionTramiteTramite = banderaEdicionTramiteTramite;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public boolean isEditandoDocumentacion() {
        return editandoDocumentacion;
    }

    public void setEditandoDocumentacion(boolean editandoDocumentacion) {
        this.editandoDocumentacion = editandoDocumentacion;
    }

    public void setBanderaClaseDeMutacionSeleccionada(
        boolean banderaClaseDeMutacionSeleccionada) {
        this.banderaClaseDeMutacionSeleccionada = banderaClaseDeMutacionSeleccionada;
    }

    public String getCancelarTramiteDecision() {
        return cancelarTramiteDecision;
    }

    public String getIdActividad() {
        return idActividad;
    }

    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    public void setCancelarTramiteDecision(String cancelarTramiteDecision) {
        this.cancelarTramiteDecision = cancelarTramiteDecision;
    }

    public String getTipoDatoRectifComplSeleccionado() {
        return tipoDatoRectifComplSeleccionado;
    }

    public TramitePredioEnglobeDTO getSelectedPredioTramite() {
        return selectedPredioTramite;
    }

    public void setSelectedPredioTramite(
        TramitePredioEnglobeDTO selectedPredioTramite) {
        this.selectedPredioTramite = selectedPredioTramite;
    }

    public String getNumeroPredialParte1() {
        return numeroPredialParte1;
    }

    public void setNumeroPredialParte1(String numeroPredialParte1) {
        this.numeroPredialParte1 = numeroPredialParte1;
    }

    public String getNumeroPredialParte2() {
        return numeroPredialParte2;
    }

    public void setNumeroPredialParte2(String numeroPredialParte2) {
        this.numeroPredialParte2 = numeroPredialParte2;
    }

    public String getNumeroPredialParte3() {
        return numeroPredialParte3;
    }

    public List<Departamento> getDptosDocumento() {
        return dptosDocumento;
    }

    public void setDptosDocumento(List<Departamento> dptosDocumento) {
        this.dptosDocumento = dptosDocumento;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public TramiteDocumentacion getDocumentoSeleccionado() {
        return documentoSeleccionado;
    }

    public void setDocumentoSeleccionado(
        TramiteDocumentacion documentoSeleccionado) {
        this.documentoSeleccionado = documentoSeleccionado;
    }

    public List<Municipio> getMunisDocumento() {
        return munisDocumento;
    }

    public void setMunisDocumento(List<Municipio> munisDocumento) {
        this.munisDocumento = munisDocumento;
    }

    public void setMunicipiosDocumento(List<SelectItem> municipiosDocumento) {
        this.municipiosDocumento = municipiosDocumento;
    }

    public void setDepartamentosDocumento(
        List<SelectItem> departamentosDocumento) {
        this.departamentosDocumento = departamentosDocumento;
    }

    public boolean isBanderaDocumentoAdicional() {
        return banderaDocumentoAdicional;
    }

    public void setBanderaDocumentoAdicional(boolean banderaDocumentoAdicional) {
        this.banderaDocumentoAdicional = banderaDocumentoAdicional;
    }

    public boolean isBanderaEditandoDocumentoAdicional() {
        return banderaEditandoDocumentoAdicional;
    }

    public void setBanderaEditandoDocumentoAdicional(
        boolean banderaEditandoDocumentoAdicional) {
        this.banderaEditandoDocumentoAdicional = banderaEditandoDocumentoAdicional;
    }

    public void setNumeroPredialParte3(String numeroPredialParte3) {
        this.numeroPredialParte3 = numeroPredialParte3;
    }

    public String getNumeroPredialParte4() {
        return numeroPredialParte4;
    }

    public void setNumeroPredialParte4(String numeroPredialParte4) {
        this.numeroPredialParte4 = numeroPredialParte4;
    }

    public String getNumeroPredialParte5() {
        return numeroPredialParte5;
    }

    public void setNumeroPredialParte5(String numeroPredialParte5) {
        this.numeroPredialParte5 = numeroPredialParte5;
    }

    public String getNumeroPredialParte6() {
        return numeroPredialParte6;
    }

    public void setNumeroPredialParte6(String numeroPredialParte6) {
        this.numeroPredialParte6 = numeroPredialParte6;
    }

    public String getNumeroPredialParte7() {
        return numeroPredialParte7;
    }

    public void setNumeroPredialParte7(String numeroPredialParte7) {
        this.numeroPredialParte7 = numeroPredialParte7;
    }

    public String getNumeroPredialParte8() {
        return numeroPredialParte8;
    }

    public void setNumeroPredialParte8(String numeroPredialParte8) {
        this.numeroPredialParte8 = numeroPredialParte8;
    }

    public String getNumeroPredialParte9() {
        return numeroPredialParte9;
    }

    public void setNumeroPredialParte9(String numeroPredialParte9) {
        this.numeroPredialParte9 = numeroPredialParte9;
    }

    public String getNumeroPredialParte10() {
        return numeroPredialParte10;
    }

    public void setNumeroPredialParte10(String numeroPredialParte10) {
        this.numeroPredialParte10 = numeroPredialParte10;
    }

    public String getNumeroPredialParte11() {
        return numeroPredialParte11;
    }

    public void setNumeroPredialParte11(String numeroPredialParte11) {
        this.numeroPredialParte11 = numeroPredialParte11;
    }

    public String getNumeroPredialParte12() {
        return numeroPredialParte12;
    }

    public void setNumeroPredialParte12(String numeroPredialParte12) {
        this.numeroPredialParte12 = numeroPredialParte12;
    }

    public void setTipoDatoRectifComplSeleccionado(
        String tipoDatoRectifComplSeleccionado) {
        this.tipoDatoRectifComplSeleccionado = tipoDatoRectifComplSeleccionado;
    }

    public List<DatoRectificar> getDatosRectifComplSource() {
        return datosRectifComplSource;
    }

    public List<TramitePredioEnglobe> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<TramitePredioEnglobe> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public List<TramitePredioEnglobeDTO> getPrediosTramiteTemp() {
        return prediosTramiteTemp;
    }

    public void setPrediosTramiteTemp(List<TramitePredioEnglobeDTO> prediosTramiteTemp) {
        this.prediosTramiteTemp = prediosTramiteTemp;
    }

    public void setDatosRectifComplSource(
        List<DatoRectificar> datosRectifComplSource) {
        this.datosRectifComplSource = datosRectifComplSource;
    }

    public List<DatoRectificar> getDatosRectifComplTarget() {
        return datosRectifComplTarget;
    }

    public void setDatosRectifComplTarget(
        List<DatoRectificar> datosRectifComplTarget) {
        this.datosRectifComplTarget = datosRectifComplTarget;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public DatoRectificar[] getSelectedDatosRectifComplDT() {
        return selectedDatosRectifComplDT;
    }

    public void setSelectedDatosRectifComplDT(
        DatoRectificar[] selectedDatosRectifComplDT) {
        this.selectedDatosRectifComplDT = selectedDatosRectifComplDT;
    }

    public boolean isBanderaSubtipoEnglobe() {
        return banderaSubtipoEnglobe;
    }

    public void setBanderaSubtipoEnglobe(boolean banderaSubtipoEnglobe) {
        this.banderaSubtipoEnglobe = banderaSubtipoEnglobe;
    }

    public List<DatoRectificar> getSelectedDatosRectifComplPL() {
        return selectedDatosRectifComplPL;
    }

    public void setDatosRectifComplDLModel(
        DualListModel<DatoRectificar> datosRectifComplDLModel) {
        this.datosRectifComplDLModel = datosRectifComplDLModel;
    }

    public void setSelectedDatosRectifComplPL(
        List<DatoRectificar> selectedDatosRectifComplPL) {
        this.selectedDatosRectifComplPL = selectedDatosRectifComplPL;
    }

    public boolean isSubtipoTramiteEsRequerido() {
        return subtipoTramiteEsRequerido;
    }

    public List<SelectItem> getSubtipoClaseMutacionV() {
        return subtipoClaseMutacionV;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public void setSubtipoTramiteEsRequerido(boolean subtipoTramiteEsRequerido) {
        this.subtipoTramiteEsRequerido = subtipoTramiteEsRequerido;
    }

    public List<Tramite> getTramiteTramites() {
        return tramiteTramites;
    }

    public void setTramiteTramites(List<Tramite> tramiteTramites) {
        this.tramiteTramites = tramiteTramites;
    }

    public String getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public long getTramiteSeleccionadoId() {
        return tramiteSeleccionadoId;
    }

    public void setTramiteSeleccionadoId(long tramiteSeleccionadoId) {
        this.tramiteSeleccionadoId = tramiteSeleccionadoId;
    }

    public boolean isBanderaRechazarTramite() {
        return banderaRechazarTramite;
    }

    public void setBanderaRechazarTramite(boolean banderaRechazarTramite) {
        this.banderaRechazarTramite = banderaRechazarTramite;
    }

    public Tramite getTramiteTramiteSeleccionado() {
        return tramiteTramiteSeleccionado;
    }

    public void setTramiteTramiteSeleccionado(Tramite tramiteTramiteSeleccionado) {
        this.tramiteTramiteSeleccionado = tramiteTramiteSeleccionado;
    }

    public void setCancelarTramite(String cancelarTramite) {
        this.cancelarTramiteDecision = cancelarTramite;
    }

    public String getEstadoFuturoTramite() {
        return estadoFuturoTramite;
    }

    public boolean isBanderaRechazarPruebas() {
        return banderaRechazarPruebas;
    }

    public void setBanderaRechazarPruebas(boolean banderaRechazarPruebas) {
        this.banderaRechazarPruebas = banderaRechazarPruebas;
    }

    public boolean isBanderaAprobarPruebas() {
        return banderaAprobarPruebas;
    }

    public void setBanderaAprobarPruebas(boolean banderaAprobarPruebas) {
        this.banderaAprobarPruebas = banderaAprobarPruebas;
    }

    public void setEstadoFuturoTramite(String estadoFuturoTramite) {
        this.estadoFuturoTramite = estadoFuturoTramite;
    }

    public boolean isBanderaEditarSolicitarPruebas() {
        return banderaEditarSolicitarPruebas;
    }

    public void setBanderaEditarSolicitarPruebas(
        boolean banderaEditarSolicitarPruebas) {
        this.banderaEditarSolicitarPruebas = banderaEditarSolicitarPruebas;
    }

    public String getPruebasSolicitadas() {
        return pruebasSolicitadas;
    }

    public void setPruebasSolicitadas(String pruebasSolicitadas) {
        this.pruebasSolicitadas = pruebasSolicitadas;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public TramiteEstado getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(TramiteEstado estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public boolean isBanderaDocumentoRadicado() {
        return banderaDocumentoRadicado;
    }

    public void setBanderaDocumentoRadicado(boolean banderaDocumentoRadicado) {
        this.banderaDocumentoRadicado = banderaDocumentoRadicado;
    }

    public boolean isBanderaGuardarMotivo() {
        return banderaGuardarMotivo;
    }

    public void setBanderaGuardarMotivo(boolean banderaGuardarMotivo) {
        this.banderaGuardarMotivo = banderaGuardarMotivo;
    }

    public List<Predio> getPrediosSeleccionadosMapa() {
        return this.consultaPredioMB.getPrediosSeleccionados2();
    }

    public boolean isBanderaGenerarDocumento() {
        return banderaGenerarDocumento;
    }

    public void setBanderaGenerarDocumento(boolean banderaGenerarDocumento) {
        this.banderaGenerarDocumento = banderaGenerarDocumento;
    }

    public TramitePruebaEntidad getSelectedEntidadPruebas() {
        return selectedEntidadPruebas;
    }

    public void setSelectedEntidadPruebas(
        TramitePruebaEntidad selectedEntidadPruebas) {
        this.selectedEntidadPruebas = selectedEntidadPruebas;
    }

    public boolean isBanderaGenerarDocumentoAutoDePruebas() {
        return banderaGenerarDocumentoAutoDePruebas;
    }

    public void setBanderaGenerarDocumentoAutoDePruebas(
        boolean banderaGenerarDocumentoAutoDePruebas) {
        this.banderaGenerarDocumentoAutoDePruebas = banderaGenerarDocumentoAutoDePruebas;
    }

    public Boolean getVerSolicitudDocsBool() {
        return verSolicitudDocsBool;
    }

    public void setVerSolicitudDocsBool(Boolean verSolicitudDocsBool) {
        this.verSolicitudDocsBool = verSolicitudDocsBool;
    }

    public TramiteDocumentacion getSelectedTramiteDocumentacion() {
        return selectedTramiteDocumentacion;
    }

    public void setSelectedTramiteDocumentacion(
        TramiteDocumentacion selectedTramiteDocumentacion) {
        this.selectedTramiteDocumentacion = selectedTramiteDocumentacion;
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public String getMensajeCancelaTramite() {
        return mensajeCancelaTramite;
    }

    public void setMensajeCancelaTramite(String mensajeCancelaTramite) {
        this.mensajeCancelaTramite = mensajeCancelaTramite;
    }

    public ReporteDTO getReporteAutoDePruebas() {
        return reporteAutoDePruebas;
    }

    public void setReporteAutoDePruebas(ReporteDTO reporteAutoDePruebas) {
        this.reporteAutoDePruebas = reporteAutoDePruebas;
    }

    public ReporteDTO getReporteOficioNoProcedencia() {
        return reporteOficioNoProcedencia;
    }

    public void setReporteOficioNoProcedencia(ReporteDTO reporteOficioNoProcedencia) {
        this.reporteOficioNoProcedencia = reporteOficioNoProcedencia;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public boolean isHabilitarTramitesAdicionales() {
        return habilitarTramitesAdicionales;
    }

    public void setHabilitarTramitesAdicionales(boolean habilitarTramitesAdicionales) {
        this.habilitarTramitesAdicionales = habilitarTramitesAdicionales;
    }

    public boolean isHabilitarBotonesNoProcedencia() {
        return habilitarBotonesNoProcedencia;
    }

    public void setHabilitarBotonesNoProcedencia(boolean habilitarBotonesNoProcedencia) {
        this.habilitarBotonesNoProcedencia = habilitarBotonesNoProcedencia;
    }

    public String getRazonesRechazoDepuracion() {
        return razonesRechazoDepuracion;
    }

    public boolean isHabilitarRazonesRechazoDepuracion() {
        return habilitarRazonesRechazoDepuracion;
    }

    public boolean isValidacionViaGubernativa() {
        return validacionViaGubernativa;
    }

    public void setValidacionViaGubernativa(boolean validacionViaGubernativa) {
        this.validacionViaGubernativa = validacionViaGubernativa;
    }

    public List<HistoricoProyeccionViaGubernativaDTO> getListaTramitesHistoricoProyeccion() {
        return listaTramitesHistoricoProyeccion;
    }

    public boolean isValidacionViaGubernativa2() {
        return validacionViaGubernativa2;
    }

    public void setValidacionViaGubernativa2(boolean validacionViaGubernativa2) {
        this.validacionViaGubernativa2 = validacionViaGubernativa2;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    public ArrayList<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> answer = null;
        List<TipoSolicitudTramite> tiposTramite = null;
        SelectItem itemToAdd;
        if (this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
            if (this.tramiteSeleccionado != null &&
                this.tramiteSeleccionado.getSolicitud() != null) {
                tiposTramite = this.getTramiteService()
                    .obtenerTiposDeTramitePorSolicitud(
                        ESolicitudTipo.AVISOS.getCodigo());
            }
            if (tiposTramite != null && !tiposTramite.isEmpty()) {
                answer = new ArrayList<SelectItem>();
                for (TipoSolicitudTramite tst : tiposTramite) {
                    if (this.tramiteSeleccionado.isQuintaNuevo() &&
                        (tst.getTipoTramite().
                            equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo()) ||
                        tst.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()))) {
                        //
                    } else {
                        itemToAdd = new SelectItem(tst.getTipoTramite(),
                            tst.getTipoTramiteValor());
                        answer.add(itemToAdd);
                    }
                }
            }
        } else {
            if (this.tramiteSeleccionado != null &&
                this.tramiteSeleccionado.getSolicitud() != null) {
                tiposTramite = this.getTramiteService()
                    .obtenerTiposDeTramitePorSolicitud(
                        this.tramiteSeleccionado.getSolicitud()
                            .getTipo());
            }
            if (tiposTramite != null && !tiposTramite.isEmpty()) {
                answer = new ArrayList<SelectItem>();
                for (TipoSolicitudTramite tst : tiposTramite) {
                    itemToAdd = new SelectItem(tst.getTipoTramite(),
                        tst.getTipoTramiteValor());
                    answer.add(itemToAdd);
                }
            }
        }
        this.tiposTramitePorSolicitud = answer;
        return this.tiposTramitePorSolicitud;
    }

    public void setTiposTramitePorSolicitud(ArrayList<SelectItem> tiposTramite) {
        this.tiposTramitePorSolicitud = tiposTramite;
    }

    // --------------------------------------------------------------------------------------------
    public DualListModel<DatoRectificar> getDatosRectifComplDLModel() {

        if (this.tramiteTramiteSeleccionado != null &&
            (this.tramiteTramiteSeleccionado.isRectificacion() || this.tramiteTramiteSeleccionado.
            isEsComplementacion())) {
            this.datosRectifComplSource = new ArrayList<DatoRectificar>();
            this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
            String naturaleza;

            if (this.tramiteTramiteSeleccionado != null &&
                this.tramiteTramiteSeleccionado.isRectificacion()) {
                naturaleza = EDatoRectificarNaturaleza.RECTIFICACION
                    .geCodigo();
            } else {
                naturaleza = EDatoRectificarNaturaleza.COMPLEMENTACION
                    .geCodigo();
            }

            if (this.tipoDatoRectifComplSeleccionado != null &&
                this.tipoDatoRectifComplSeleccionado.compareToIgnoreCase("") != 0 &&
                naturaleza != null && !naturaleza.isEmpty()) {
                this.datosRectifComplSource = this.getTramiteService()
                    .obtenerDatosRectificarPorTipoYNaturaleza(
                        this.tipoDatoRectifComplSeleccionado, naturaleza);
            }

            this.datosRectifComplDLModel = new DualListModel<DatoRectificar>(
                this.datosRectifComplSource, this.datosRectifComplTarget);
        }
        return this.datosRectifComplDLModel;
    }

    public boolean isBanderaVerTramitePorAsociar() {
        return banderaVerTramitePorAsociar;
    }

    public void setBanderaVerTramitePorAsociar(
        boolean banderaVerTramitePorAsociar) {
        this.banderaVerTramitePorAsociar = banderaVerTramitePorAsociar;
    }

    // adicion para ver colindantes
    public List<Predio> getListaPrediosColindantes() {
        return listaPrediosColindantes;
    }

    public void setListaPrediosColindantes(List<Predio> listaPrediosColindantes) {
        this.listaPrediosColindantes = listaPrediosColindantes;
    }

    public Predio getSelectedPredioColindante() {
        return selectedPredioColindante;
    }

    public void setSelectedPredioColindante(Predio selectedPredioColindante) {
        this.selectedPredioColindante = selectedPredioColindante;
    }

    public boolean isBanderaPrediosFiscales() {
        return banderaPrediosFiscales;
    }

    public void setBanderaPrediosFiscales(boolean banderaPrediosFiscales) {
        this.banderaPrediosFiscales = banderaPrediosFiscales;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    // --------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init de EjecucionMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.tp = new TramitePrueba();
        this.loadTramiteFromTree();
        this.applicationClientName = this.contexto.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.selectedDatosRectifComplDataTable = new ArrayList<DatoRectificar>();
        this.habilitarBotonesNoProcedencia = true;
        this.banderaPrediosFiscales = false;
        this.banderaGenerarDocumentoAutoDePruebas = false;
        this.banderaGeneraDocumento = false;
        this.documentosPruebasAEliminar = new ArrayList<TramiteDocumento>();

        try {

            LOGGER.debug("Error-generado: this.tramiteSeleccionadoId = "+this.tramiteSeleccionadoId);
            this.tramiteSeleccionado = this.getTramiteService()
                .findTramitePruebasByTramiteId(this.tramiteSeleccionadoId);

            if (this.tramiteSeleccionado.isViaGubernativa()) {
                //@modified by leidy.gonzalez:: #51998:: 20/03/2018:: ajuste para asignar funcionario ejecutor 
                //a tramites de via gubernativa
                this.asignaUsuarioEjecutor();
            }

            if (this.tramiteSeleccionado.isQuintaOmitido()) {
                Predio p = this.getConservacionService().getPredioPorNumeroPredial(
                    this.tramiteSeleccionado.getNumeroPredial());
                if (p != null) {
                    if (p.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                        this.banderaPrediosFiscales = true;
                    }
                }
            }

            if (this.tramiteSeleccionado.getPredio() != null) {
                if (this.tramiteSeleccionado.getPredio().getTipoCatastro().equals(
                    EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                    this.banderaPrediosFiscales = true;
                }
            }

            if (this.actividad.getNombre().equals(
                ProcesoDeConservacion.ACT_EJECUCION_SOLICITAR_PRUEBA) ||
                this.actividad
                    .getNombre()
                    .equals(ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_AUTO_PRUEBA)) {
                // / TODO que sean de pruebas
                if (this.tramiteSeleccionado.getTramitePruebas() == null) {
                    this.tramiteSeleccionado
                        .setTramitePruebas(new TramitePrueba());
                }
            }
            this.banderaEditarSolicitarPruebas = true;
        } catch (Exception e) {
            String mensaje = "Ocurrió un error al buscar el trámite, por favor intente nuevamente";
            this.addMensajeError(mensaje);
            LOGGER.error(e.getMessage(), e);
        }

        // Deben estar en el init del funcionario responsable de conservacion
        // Condiciones del rendered de Solicitar pruebas y Auto de pruebas
        if ((this.isUsuarioEjecutor() && (this.tramiteSeleccionado
            .getSolicitud().isRevisionAvaluo() || this.tramiteSeleccionado
                .getSolicitud().isSolicitudAutoavaluo())) ||
            (this.isUsuarioDirectorTerritorial() || this
            .isUsuarioResponsableConservacion())) {
            this.banderaAprobarPruebas = false;
            this.banderaRechazarPruebas = true;
            if (this.tramiteSeleccionado.getTramitePruebas() == null) {
                this.tramiteSeleccionado.setTramitePruebas(new TramitePrueba());
            }
            this.tramiteSeleccionado.getTramitePruebas().setAprobadas(
                ESiNo.NO.getCodigo());
        }

        // para el init de tramites asociados tramites nuevos de oficina
        // TODO::juan.agudelo::revisar la actividad de los dos siguientes
        // casos::13/10/2011::fredy.wilches
        // if
        // (this.actividad.getRutaActividad().equals(ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO))
        this.cancelarTramiteDecision = ESiNo.NO.getCodigo();
        this.tramiteTramites = new ArrayList<Tramite>();
        this.banderaAsociarDocumentos = false;

        this.banderaGuardoRechazoPruebas = false;

        this.banderaVerTramitePorAsociar = false;

        this.estadoTramite = this.getTramiteService()
            .buscarTramiteEstadoVigentePorTramiteId(
                this.tramiteSeleccionado.getId());

        if (this.estadoTramite == null || (this.estadoTramite.getEstado() != null &&
            !this.estadoTramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo()))) {
            this.estadoTramite = new TramiteEstado();
        }

        // Carga de entidades asignadas a pruebas para el CU recibir Pruebas
        if (this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA)) {

            // para el init de recibir pruebas
            this.documentoPruebaSeleccionado = new Documento();
            this.banderaDocumentoEncontradoEnCorrrespondencia = false;

            if (this.tramiteSeleccionado.getTramitePruebas() != null) {
                if (this.tramiteSeleccionado.getTramitePruebas()
                    .getTramitePruebaEntidads() != null) {
                    this.entidadesPruebas = this.getTramiteService()
                        .buscarTramitePruebaEntidadsconNumeroDocumentos(
                            this.tramiteSeleccionadoId);
                } else {
                    this.addMensajeError(
                        "Las pruebas sobre este trámite no están asociadas a ninguna entidad.");
                }

            } else {
                this.addMensajeError("Este trámite no tiene pruebas solicitadas.");
            }
        }
        /**
         * TODO Utilizar el nombre correcto como constante (Debe hacerse en el process)
         */
        if (this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE) ||
            this.actividad.getNombre().startsWith("InvocarAsociarDocumentoNuevoTramite")) {
            this.banderaVerDetalleTramiteEjecucion = true;
            this.banderaAsociarDocumentos = true;
            this.tramiteTramites = this.getTramiteService().getTramiteTramitesbyTramiteId(
                this.tramiteSeleccionadoId);
        }

        this.tramiteTramiteSeleccionado = new Tramite();

        this.datosRectifComplDLModel = new DualListModel<DatoRectificar>();

        // Inicialización de editor geográfico. 
        //excepcion cuando son predios fiscales
        if (!this.banderaPrediosFiscales) {
            this.parametroEditorGeografico = this.usuario.getLogin() + "," +
                this.tramiteSeleccionado.getId() + "," +
                UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class, this.actividad.
                    getNombre());
        }
        // Cargar motivos de devolución del trámite
        this.cargarMotivosDevolucionTramites();

        // consultar inconsistencias geográficas del trámite.
        this.inconsistenciasTramite = this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramiteSeleccionado.getId());

        this.habilitarRazonesRechazoDepuracion = false;
        //validar si el tramite paso por depuración
        TramiteDepuracion ultimoTramiteDepuracion = this.getTramiteService().
            buscarUltimoTramiteDepuracionPorIdTramite(this.tramiteSeleccionado.getId());
        // Set de las razones de rechazo depuración.
        if (ultimoTramiteDepuracion != null) {
            this.razonesRechazoDepuracion = ultimoTramiteDepuracion.getRazonesRechazo();
            if (this.razonesRechazoDepuracion != null && !this.razonesRechazoDepuracion.isEmpty()) {
                this.habilitarRazonesRechazoDepuracion = true;
            }

        }
        // variable requerida para mostrar los botones, arranca en true para no afectar la logica de los demas flujos,
        this.validacionViaGubernativa = false;
        this.habilitarSoloTramiteNoProcede = false;

        if (this.tramiteSeleccionado.isViaGubernativa()) {
            PPredio pp = null;
            try {
                pp = this.getConservacionService().obtenerPPredioCompletoById(
                    this.tramiteSeleccionado.getPredio().getId());
            } catch (Exception ex) {

            }

            if (pp != null) {
                Tramite tramiteRef = null;
                FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
                filtrosTramite.setNumeroRadicacion(this.tramiteSeleccionado.getSolicitud().
                    getTramiteNumeroRadicacionRef());
                //filtrosTramite.setEstadoTramite("0");

                List<Tramite> resultado = this.getTramiteService().
                    consultaDeTramitesPorFiltros(true, filtrosTramite, null, null, false, null, null,
                        null);

                if (resultado != null && !resultado.isEmpty()) {
                    tramiteRef = resultado.get(0);
                    this.listaTramitesHistoricoProyeccion =
                        new ArrayList<HistoricoProyeccionViaGubernativaDTO>();
                    HistoricoProyeccionViaGubernativaDTO historicoP =
                        new HistoricoProyeccionViaGubernativaDTO();
                    historicoP.setTipoProyeccion("Proyección inicial");
                    tramiteRef = this.getTramiteService().findTramitePruebasByTramiteId(tramiteRef.
                        getId());
                    historicoP.setTramiteInfo(tramiteRef);
                    this.listaTramitesHistoricoProyeccion.add(historicoP);

                    if (tramiteRef != null) {
                        List<Actividad> actividadesTramiteOriginal = this.getProcesosService().
                            getActividadesPorIdObjetoNegocio(tramiteRef.getId());

                        for (Actividad a : actividadesTramiteOriginal) {
                            if (ETramiteEstado.FINALIZADO_APROBADO.getCodigo().equals(tramiteRef.
                                getEstado()) ||
                                ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS.equals(a.
                                    getNombre())) {
                                this.habilitarSoloTramiteNoProcede = true;
                            }
                        }
                    }
                }

                //adicionar informacion de los historicos
                this.validacionViaGubernativa = true;
            }
        }

        this.habilitarTramitesAdicionales = true;

        if (this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
            this.inicializarBanderasAsignar();
            this.determinarExistenciaReplica();
        }
        this.validacionTramitesReferencia = true;

        //Cuando el trámite ya aplicó cambios en la actividad Revisar trámite asignado solo debe aparecer la opción No procede.(8935)
        this.validacionViaGubernativa2 = true;
        if (this.tramiteSeleccionado.isViaGubernativa() && this.tramiteSeleccionado.
            getNumeroRadicacionReferencia() != null) {

            String radicacionTramiteOriginal = this.tramiteSeleccionado.
                getNumeroRadicacionReferencia();
            Tramite tramiteOri = this.getTramiteService().buscarTramitePorNumeroRadicacion(
                radicacionTramiteOriginal);
            if (tramiteOri != null) {
                SolicitudCatastral solicitud = this.getProcesosService().
                    obtenerObjetoNegocioConservacion(
                        tramiteOri.getProcesoInstanciaId(),
                        tramiteOri.getId().toString(),
                        ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);

                if (solicitud != null) {
                    this.validacionViaGubernativa2 = false;
                } else if (ETramiteEstado.FINALIZADO_APROBADO.getCodigo()
                    .equals(tramiteOri.getEstado())) {
                    this.validacionTramitesReferencia = false;
                }
            }
        }

        // Método que actualiza el número de documentos de pruebas en la
        // tabla
        this.entidadesPruebas = this.getTramiteService()
            .buscarTramitePruebaEntidadsconNumeroDocumentos(
                this.tramiteSeleccionado.getId());

        if (this.entidadesPruebas != null && !this.entidadesPruebas.isEmpty()) {

            if (this.tramiteSeleccionado != null && this.tramiteSeleccionado.getTramitePruebas() !=
                null) {

                this.tramiteSeleccionado.getTramitePruebas().setTramitePruebaEntidads(
                    entidadesPruebas);

            }

            for (TramitePruebaEntidad tramitePruebaEntidadTemp : this.entidadesPruebas) {

                if (this.tramiteSeleccionado.getTramiteDocumentos() != null &&
                    !this.tramiteSeleccionado.getTramiteDocumentos().isEmpty() &&
                    this.tramiteSeleccionado.getTramitePruebas() != null &&
                    !this.tramiteSeleccionado.getTramitePruebas().getTramitePruebaEntidads().
                        isEmpty() &&
                    this.tramiteSeleccionado.getTramitePruebas().getTramitePruebaEntidads() != null) {

                    for (TramitePruebaEntidad tpe : this.tramiteSeleccionado.getTramitePruebas().
                        getTramitePruebaEntidads()) {

                        for (TramiteDocumento td : this.tramiteSeleccionado.getTramiteDocumentos()) {

                            if ((tpe.getNumeroIdentificacion() != null && td.
                                getIdPersonaNotificacion() != null &&
                                tpe.getNumeroIdentificacion().equals(td.getIdPersonaNotificacion())) &&
                                (td.getDocumento() != null &&
                                td.getDocumento().getTipoDocumento() != null && td.getDocumento().
                                getTipoDocumento().getId() != null &&
                                td.getDocumento().getTipoDocumento().getId().equals(
                                    ETipoDocumento.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId()))) {

                                tramitePruebaEntidadTemp.setDocumentosPruebas(
                                    tramitePruebaEntidadTemp.getDocumentosPruebas());

                            }
                        }

                    }
                }
            }
        }

        if (this.tramiteSeleccionado.isViaGubernativa()) {
            //@modified by leidy.gonzalez:: #51998:: 20/03/2018:: ajuste para asignar funcionario ejecutor 
            //a tramites de via gubernativa
            this.asignaUsuarioEjecutor();
        }
    }

    /**
     *
     * determina si el tramite tiene replica de consulta asociada y marca la bandera correpondiente
     *
     * @author felipe.cadena
     */
    private void determinarExistenciaReplica() {

        Documento d = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.tramiteSeleccionado.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());

        this.banderaReplicaConsulta = (d != null);

    }

    /**
     *
     * Asigna funcionario ejecutor al tramite seleccionado
     *
     * @author leidy.gonzalez
     */
    private void asignaUsuarioEjecutor() {

        this.tramiteSeleccionado.setTramitePruebas(null);
        this.tramiteSeleccionado.setFuncionarioEjecutor(this.usuario.getLogin());
        this.tramiteSeleccionado.setNombreFuncionarioEjecutor(this.usuario
            .getNombreCompleto());
        this.getTramiteService().actualizarTramite(
            this.tramiteSeleccionado, this.usuario);
    }

    /**
     * Inicializa las banderas para determinar que botones mostrar en la actividad de revisar
     * tràmite asignado
     *
     * @author felipe.cadena
     * @moodified felipe.cadena:: 24/07/02014 :: Se agrega el tipo tramite de rectificacion de zonas
     */
    private void inicializarBanderasAsignar() {
        String clasificacionTramite = this.tramiteSeleccionado.getClasificacion();
        if (clasificacionTramite == null) {
            clasificacionTramite = "";
        }

        if (this.tramiteSeleccionado.isQuintaOmitido() &&
            clasificacionTramite.equals(ETramiteClasificacion.OFICINA.toString())) {

            this.banderaOficinaDepuracion = true;
        }
        if ((this.tramiteSeleccionado.isPrimera() ||
            this.tramiteSeleccionado.isCancelacionPredio() ||
            this.tramiteSeleccionado.isModificacionInscripcionCatastral() ||
            (this.tramiteSeleccionado.isRectificacion() && !this.tramiteSeleccionado.
            isRectificacionArea() &&
            !this.tramiteSeleccionado.isRectificacionZona()) ||
            this.tramiteSeleccionado.isEsComplementacion()) &&
            clasificacionTramite.equals(ETramiteClasificacion.OFICINA.toString())) {

            this.banderaOficina = true;
        }

        if ((this.tramiteSeleccionado.isSegunda() ||
            this.tramiteSeleccionado.isTercera() ||
            this.tramiteSeleccionado.isQuintaNuevo() ||
            this.tramiteSeleccionado.isRectificacionArea() ||
            this.tramiteSeleccionado.isRectificacionZona()) &&
            clasificacionTramite.equals(ETramiteClasificacion.OFICINA.toString())) {

            this.banderaOficinaDepuracionAsociar = true;
        }

        if ((this.tramiteSeleccionado.isSegunda() ||
            this.tramiteSeleccionado.isTercera() ||
            this.tramiteSeleccionado.isQuinta() ||
            this.tramiteSeleccionado.isRectificacionArea() ||
            this.tramiteSeleccionado.isRectificacionZona()) &&
            clasificacionTramite.equals(ETramiteClasificacion.TERRENO.toString())) {

            this.banderaTerrenoGIS = true;
        }

        if ((this.tramiteSeleccionado.isPrimera() ||
            this.tramiteSeleccionado.isCancelacionPredio() ||
            this.tramiteSeleccionado.isModificacionInscripcionCatastral() ||
            (this.tramiteSeleccionado.isRectificacion() && !this.tramiteSeleccionado.
            isRectificacionArea() &&
            !this.tramiteSeleccionado.isRectificacionZona()) ||
            this.tramiteSeleccionado.isEsComplementacion()) &&
            clasificacionTramite.equals(ETramiteClasificacion.TERRENO.toString())) {

            this.banderaTerreno = true;
        }

        if (this.tramiteSeleccionado.isCuarta() ||
            this.tramiteSeleccionado.isRevisionAvaluo()) {

            this.banderaAutoRevision = true;
        }

        if (this.tramiteSeleccionado.isViaGubernativa()) {
            this.banderaViaGubernativa = true;
        }
    }

    /**
     * recupera el id de la actividad seleccionada del árbol y el id del objeto de negocio
     * necesario.
     *
     * @author fabio.navarrete
     */
    private void loadTramiteFromTree() {
        this.actividad = tareasPendientesMB.getInstanciaSeleccionada();
        if (actividad != null) {
            LOGGER.debug("Error-generado actividad.getIdObjetoNegocio = "+actividad.getIdObjetoNegocio());
            this.tramiteSeleccionadoId = actividad.getIdObjetoNegocio();
            if (tramiteSeleccionadoId == 0.0) {
                this.addMensajeError("La actividad no tiene tramiteId");
            }
            this.activityId = actividad.getId();
        } else {
            this.addMensajeError("No se pudo obtener la actividad del process");
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en solicitarPruebas
     */
    public void guardarSolicitudPruebas() {

        boolean validarDatosAprobarPruebas = validarDatosAprobarPruebas();
        if (validarDatosAprobarPruebas == true) {
            try {

                this.tp = this.tramiteSeleccionado.getTramitePruebas();
                this.tp.setTramite(this.tramiteSeleccionado);

                this.tp = this.getTramiteService()
                    .guardarActualizarTramitePruebas(this.tp, this.usuario,
                        this.estadoFuturoTramite);
                this.tramiteSeleccionado.setTramitePruebas(this.tp);
                this.addMensajeInfo("Datos almacenados exitosamente");
                this.banderaGenerarDocumentoAutoDePruebas = true;
                this.banderaGuardoRechazoPruebas = true;

            } catch (Exception e) {
                String mensaje =
                    "Ocurrió un error al guardar la solicitud de pruebas, por favor intente nuevamente";
                this.addMensajeError(mensaje);
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que valida los datos para guardar una desición de aporbación de una solicitud de
     * pruebas
     *
     * @return
     */
    private boolean validarDatosAprobarPruebas() {

        if (this.tramiteSeleccionado.getTramitePruebas() != null &&
            this.tramiteSeleccionado.getTramitePruebas().getAprobadas() != null &&
            this.tramiteSeleccionado.getTramitePruebas().getAprobadas()
                .equals(ESiNo.SI.getCodigo())) {
            if (this.tramiteSeleccionado
                .getTramitePruebas()
                .getFechaInicio()
                .before(this.tramiteSeleccionado.getTramitePruebas()
                    .getFechaFin())) {
                return true;
            } else if (this.tramiteSeleccionado
                .getTramitePruebas()
                .getFechaInicio()
                .equals(this.tramiteSeleccionado.getTramitePruebas()
                    .getFechaFin())) {
                return true;
            } else {
                String mensaje = "La fecha de fin de pruebas no puede ser anterior a la fecha " +
                    " de inicio de pruebas." +
                    " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                return false;
            }
        }

        return true;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la desición si/no en aprobarRealizarPruebas
     */
    public void aprobarRechazarPruebasDecision() {

        if (this.tramiteSeleccionado.getTramitePruebas().getAprobadas() != null &&
            this.tramiteSeleccionado.getTramitePruebas().getAprobadas()
                .equals(ESiNo.SI.getCodigo())) {
            this.banderaAprobarPruebas = true;
            this.banderaRechazarPruebas = false;
            this.tramiteSeleccionado.getTramitePruebas().setMotivoRechazo("");
        } else if (this.tramiteSeleccionado.getTramitePruebas().getAprobadas() != null &&
            this.tramiteSeleccionado.getTramitePruebas().getAprobadas()
                .equals(ESiNo.NO.getCodigo())) {
            this.banderaAprobarPruebas = false;
            this.banderaRechazarPruebas = true;
            this.tramiteSeleccionado.getTramitePruebas().setConsideraciones("");
            this.tramiteSeleccionado.getTramitePruebas().setDispone("");
            this.tramiteSeleccionado.getTramitePruebas().setFechaInicio(null);
            this.tramiteSeleccionado.getTramitePruebas().setFechaFin(null);

        }
    }

    /**
     * Método encargado de realizar el direccionamiento a las diferentes páginas a partir de la
     * opción seleccionada en la pantalla de revisar trámites asignados.
     *
     * @return
     * @author fabio.navarrete
     */
    public String revisarTramitesAsignados() {

        if (this.tramiteSeleccionado.isQuintaNuevo()) {
            this.habilitarTramitesAdicionales = false;
        }

        if (revisarTramiteAsignadoChoose
            .equals(ConstantesNavegacionWeb.TRAMITES_PARA_REALIZAR_EN_TERRENO)) {
            return ConstantesNavegacionWeb.TRAMITES_PARA_REALIZAR_EN_TERRENO;
        }

        if (revisarTramiteAsignadoChoose.equals(ConstantesNavegacionWeb.PROYECTAR)) {
            return ConstantesNavegacionWeb.PROYECTAR;
        }

        if (revisarTramiteAsignadoChoose.equals(ConstantesNavegacionWeb.DEVOLVER_TRAMITE)) {
            this.cargarMotivosDevolucionTramites();
            return ConstantesNavegacionWeb.DEVOLVER_TRAMITE;
        }

        if (revisarTramiteAsignadoChoose
            .equals(ConstantesNavegacionWeb.GESTIONAR_TRAMITE_NO_PROCEDE)) {
            return ConstantesNavegacionWeb.GESTIONAR_TRAMITE_NO_PROCEDE;
        }

        if (revisarTramiteAsignadoChoose.equals(ConstantesNavegacionWeb.SOLICITAR_PRUEBAS)) {
            return ConstantesNavegacionWeb.SOLICITAR_PRUEBAS;
        }

        if (revisarTramiteAsignadoChoose
            .equals(ConstantesNavegacionWeb.ASOCIAR_TRAMITES_NUEVOS)) {
            return ConstantesNavegacionWeb.ASOCIAR_TRAMITES_NUEVOS;
        }

        if (revisarTramiteAsignadoChoose
            .equals(ConstantesNavegacionWeb.ELABORAR_AUTO_DE_PRUEBAS)) {
            return ConstantesNavegacionWeb.ELABORAR_AUTO_DE_PRUEBAS;
        }

        if (revisarTramiteAsignadoChoose
            .equals(ConstantesNavegacionWeb.ENVIAR_A_DEPURACION_GEOGRAFICA)) {
            this.cargarDatosDepuracionDelTramite();
            return ConstantesNavegacionWeb.ENVIAR_A_DEPURACION_GEOGRAFICA;
        }

        return "";
    }

    public String getRevisarTramiteAsignadoChoose() {
        return revisarTramiteAsignadoChoose;
    }

    public void setRevisarTramiteAsignadoChoose(
        String revisarTramiteAsignadoChoose) {
        this.revisarTramiteAsignadoChoose = revisarTramiteAsignadoChoose;
    }

    /**
     * Método que valida las condiciones de no procedencia de un tramite de via gubernativa 1.
     * Finalizo el tiempo para interponer recursos 2. No existen más tramites en curso de vía
     * gubernativa
     *
     * @author juanfelipe.garcia
     * @return
     */
    private String validarNoProcedeViaGubernativa() {
        boolean val1 = false;
        //Actividad actividadTramiteOriginal = null;

        //tramite original   
        Tramite tramiteRef = null;
        FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
        filtrosTramite.setNumeroRadicacion(this.tramiteSeleccionado.getSolicitud().
            getTramiteNumeroRadicacionRef());
        filtrosTramite.setEstadoTramite("0");
        List<Tramite> resultado = this.getTramiteService().consultaDeTramitesPorFiltros(true,
            filtrosTramite, null, null, false, null, null, null);

        if (resultado != null && !resultado.isEmpty()) {
            tramiteRef = resultado.get(0);
            List<Actividad> actividadesTramiteOriginal = this.getProcesosService().
                getActividadesPorIdObjetoNegocio(tramiteRef.getId());
            //actividadTramiteOriginal = actividadesTramiteOriginal.get(0);

            Date fechaActual = new Date();
            //validar que la fecha de expiracion de la actividad 
            if (actividadesTramiteOriginal.get(0).getFechaExpiracion() != null) {
                val1 = actividadesTramiteOriginal.get(0).getFechaExpiracion().before(fechaActual);
            }

        }

        List<Tramite> recursosSuspendidos = new ArrayList<Tramite>();
        List<Tramite> recursosActivos = new ArrayList<Tramite>();
        //1. Obtener recursos suspendidos y activos
        for (Tramite t : recursosViaGubernativa()) {
            if (t.getActividadActualTramite().getEstado().equalsIgnoreCase(
                EEstadoActividad.SUSPENDIDA.toString())) {
                recursosSuspendidos.add(t);
            } else {
                recursosActivos.add(t);
            }
        }

        //No existen otros recursos de vía gubernativa en curso y
        //El tiempo para interponer recurso ya finalizó
        if (recursosActivos.size() == 1 && val1) {
            return ConstantesNavegacionWeb.GESTIONAR_TRAMITE_NO_PROCEDE;
        } else {
            return "";
        }
    }

    /**
     * Método para obtener los trámites de vía gubernativa activos, asociados a un predio.
     *
     * @return
     * @author juanfelipe.garcia
     */
    private List<Tramite> recursosViaGubernativa() {
        List<Tramite> listaTramites = new ArrayList<Tramite>();;

        //consulta otros tramites de via gubernativa asociados al predio
        FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
        FiltroDatosConsultaPredio datosConsultaPredio = new FiltroDatosConsultaPredio();

        datosConsultaPredio.
            setNumeroPredial(this.tramiteSeleccionado.getPredio().getNumeroPredial());
        filtrosTramite.setTipoSolicitud(this.tramiteSeleccionado.getSolicitud().getTipo());
        filtrosTramite.setEstadoTramite("0");
        //filtrar tramites que no esten activos
        for (Tramite t : this.getTramiteService().consultaDeTramitesPorFiltros(true, filtrosTramite,
            datosConsultaPredio, null, false, null, null, null)) {
            if (!t.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.getCodigo()) &&
                !t.getEstado().equals(ETramiteEstado.FINALIZADO.getCodigo()) &&
                !t.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo())) {

                listaTramites.add(t);
            }
        }

        return listaTramites;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en solicitarPruebas
     */
    public void guardarPruebasEjecutor() {
        guardarSolicitudPruebas();
    }

    public String solicitarPruebas() {

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(this.usuario);
        // Se envia al mismo ejecutor, quien es el de este CU. Puede ser
        // EJECUTOR, RESPONSABLE_CONSERVACION o DIRECTOR_TERRITORIAL

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        solicitudCatastral.setUsuarios(usuarios);

        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_SOLICITAR_PRUEBA);

        this.getProcesosService().avanzarActividad(activityId,
            solicitudCatastral);
        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";

    }

    public String moverElaborarAutoPruebas() {

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(this.usuario);
        // Se envia al mismo ejecutor, quien es el de este CU. Puede ser
        // RESPONSABLE_CONSERVACION o DIRECTOR_TERRITORIAL

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        solicitudCatastral.setUsuarios(usuarios);

        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_AUTO_PRUEBA);

        this.getProcesosService().avanzarActividad(this.usuario, activityId,
            solicitudCatastral);
        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";

    }

    public String moverElaborarInforme() {

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(this.usuario);
        // Se envia al mismo ejecutor, quien es el de este CU.

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        solicitudCatastral.setUsuarios(usuarios);

        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO);

        this.getProcesosService().avanzarActividad(activityId,
            solicitudCatastral);
        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en solicitarPruebas
     */
    public String enviarPruebasConservacion() {

//REVIEW :: aquí debería validarse que se haya podido hacer lo de bd que hace el llamado a este método        
        guardarSolicitudPruebas();

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();

        List<UsuarioDTO> usuarios = null;
        UsuarioDTO usuarioTemp = this.getGeneralesService().
            obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(
                this.tramiteSeleccionado, this.usuario);

        if (usuarioTemp != null) {
            usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(usuarioTemp);
        }

        solicitudCatastral.setUsuarios(usuarios);
        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO);
        // Validado por FGWL para el CU Revisar auto de prueba

        try {
            this.getProcesosService().avanzarActividad(this.usuario, this.activityId,
                solicitudCatastral);
            UtilidadesWeb.removerManagedBean("ejecucion");
            this.tareasPendientesMB.init();
            return "index";
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error moviendo la actividad con id " + this.activityId +
                " a la actividad " +
                ProcesoDeConservacion.ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO + ": " +
                ex.getMensaje());
            this.addMensajeError("Ocurrió un error avanzando la actividad.");
            return null;
        }
    }
//-------------------------------------------------------------------------------------------------

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en solicitarPruebas
     */
    public String enviarAAprobarSP() {
        guardarSolicitudPruebas();

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        solicitudCatastral.setUsuarios(this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION));
        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_SOLICITUD_PRUEBA);
        // Validado por FGWL para el CU Solicitar prueba

        this.getProcesosService().avanzarActividad(activityId,
            solicitudCatastral);
        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en rechazo de pruebas
     */
    public void guardarRechazoPruebas() {
        if (this.tramiteSeleccionado.getTramitePruebas().getMotivoRechazo()
            .length() > Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO) {
            this.addMensajeError("El tamaño del texto del motivo no debe ser superior a " +
                Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO + " caracteres");
            return;
        }
        guardarSolicitudPruebas();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton enviar a ejecutor en solicitarPruebas
     */
    public String enviarPruebasEjecutor() {

        if (this.tramiteSeleccionado.getTramitePruebas().getConsideraciones() != null &&
            !this.tramiteSeleccionado.getTramitePruebas()
                .getConsideraciones().isEmpty()) {
            guardarSolicitudPruebas();

            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(this.getGeneralesService().getCacheUsuario(
                this.tramiteSeleccionado.getFuncionarioEjecutor()));

            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);

            this.getProcesosService().avanzarActividad(activityId,
                solicitudCatastral);
            UtilidadesWeb.removerManagedBean("ejecucion");
            this.tareasPendientesMB.init();
            return "index";
        } else {

            String mensaje =
                "La solicitud de pruebas no se puede devolver al ejecutor sin una justificación." +
                " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton rechazar solicitud de pruebas en aprobarSolicitudPruebas
     */
    public String rechazarsolicitudDePruebasEjecutor() {

        if (this.tramiteSeleccionado.getTramitePruebas().getMotivoRechazo() != null &&
            !this.tramiteSeleccionado.getTramitePruebas()
                .getMotivoRechazo().isEmpty()) {

            // javier.aponte
            // Se comenta por que si el usuario guardaba y luego enviaba al
            // ejecutor este método
            // se llamaba dos veces, ahora sólo se puede enviar al ejecutor si
            // se ha guardado el
            // motivo del rechazo
            // guardarSolicitudPruebas();
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            if (this.tramiteSeleccionado.getFuncionarioEjecutor() == null) {
                this.addMensajeError("El trámite no tiene un funcionario ejecutor asignado");
                return null;
            }
            usuarios.add(this.getGeneralesService().getCacheUsuario(
                this.tramiteSeleccionado.getFuncionarioEjecutor()));

            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);

            this.enviarCorreoNotificacionRechazo();

            this.getProcesosService().avanzarActividad(this.usuario, activityId,
                solicitudCatastral);
            UtilidadesWeb.removerManagedBean("ejecucion");
            this.tareasPendientesMB.init();
            return "index";
        } else {

            String mensaje =
                "La solicitud de pruebas no se puede devolver al ejecutor sin una justificación." +
                " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que envia el correo de rechazo de una solicitud de pruebas
     */
    public void enviarCorreoNotificacionRechazo() {
        String[] adjuntos = new String[0];
        String[] titulos = new String[0];
        String contenido = "";

        MessageFormat messageFormat;

        UsuarioDTO destinatario = this.getGeneralesService().getCacheUsuario(
            this.tramiteSeleccionado.getFuncionarioEjecutor());

        this.obtenerDatosCorreo(destinatario.getLogin(),
            this.tramiteSeleccionado.getNumeroRadicacion(),
            this.tramiteSeleccionado.getTramitePruebas().getMotivoRechazo());

        Plantilla plantilla = null;
        try {
            plantilla = this.getGeneralesService().recuperarPlantillaPorCodigo(
                "MENSAJE_DE_RECHAZO_A_SOLICITUD_DE_PUREBAS");
        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error al recuperar en el método de recuperarPlantillaPorCodigo " +
                "con código MENSAJE_DE_RECHAZO_A_SOLICITUD_DE_PUREBAS");
            this.addMensajeError("Se produjó un error al recuperar la plantilla " +
                "de mensaje de rechazo a solicitud de pruebas");
        }

        if (null != plantilla) {
            contenido = plantilla.getHtml();
            if (null == contenido) {
                super.addMensajeError("Imposible recuperar la plantilla: " +
                    "MENSAJE_DE_RECHAZO_A_SOLICITUD_DE_PRUEBAS");
            } else {

                Locale colombia = new Locale("ES", "es_CO");
                messageFormat = new MessageFormat(contenido, colombia);

                contenido = messageFormat.format(this.datosCorreo);
            }
        } else {
            contenido = this.tramiteSeleccionado.getTramitePruebas()
                .getMotivoRechazo();
        }

        this.getGeneralesService()
            .enviarCorreo(destinatario.getEmail(), "Rechazo de solicitud de pruebas",
                contenido, adjuntos, titulos);

    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que inicializa los parametros del correo
     *
     * @modified pedro.garcia 13-06-2012 uso de constante para extensión de email
     */
    public void obtenerDatosCorreo(String destinatario, String numeroRad,
        String motivo) {
        Calendar hoy = Calendar.getInstance();
        Date fecha = new java.util.Date(hoy.getTimeInMillis());

        this.datosCorreo = new Object[6];
        this.datosCorreo[0] = this.usuario.getEmail();
        this.datosCorreo[1] = destinatario;
        this.datosCorreo[2] = fecha;
        this.datosCorreo[3] = numeroRad;
        this.datosCorreo[4] = motivo;
        this.datosCorreo[5] = this.usuario.getNombreCompleto();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton enviar a ejecutor en solicitarPruebas
     */
    public String enviarPruebasAbogado() {

        boolean validarDatosAprobarPruebas = validarDatosAprobarPruebas();
        if (validarDatosAprobarPruebas == true) {
            guardarSolicitudPruebas();

            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setUsuarios(this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionTerritorial(),
                    ERol.ABOGADO));
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_REVISAR_AUTO_PRUEBA);

            this.getProcesosService().avanzarActividad(this.usuario, activityId,
                solicitudCatastral);
            UtilidadesWeb.removerManagedBean("ejecucion");
            this.tareasPendientesMB.init();
            return "index";
        } else {
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en rechazo de pruebas
     */
    public void guardarAceptacionPruebas() {
        guardarSolicitudPruebas();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar en rechazo de pruebas
     */
    public void guardarAutoDePruebas() {
        guardarSolicitudPruebas();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton adicionar tramite nuevo en tramites asociados
     */
    public void adicionarTramiteNuevo() {

        this.subtipoTramiteEsRequerido = false;

        this.tramiteTramiteSeleccionado = new Tramite();
        this.banderaClaseDeMutacionSeleccionada = false;
        this.banderaSubtipoEnglobe = false;
        this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
        this.prediosTramiteTemp = new ArrayList<TramitePredioEnglobeDTO>();
        this.tramiteTramiteSeleccionado.setDepartamento(this.tramiteSeleccionado.getDepartamento());
        this.tramiteTramiteSeleccionado.setMunicipio(this.tramiteSeleccionado.getMunicipio());
        this.tramiteTramiteSeleccionado.setFuente(ETramiteFuente.DE_OFICIO.getCodigo());

        // Despues de mutaciones de 5ªrevisarTramitesAsignados
        // Cargar los mismos valores del departamento viene.
        if (this.tramiteSeleccionado.getDepartamento() != null &&
            this.tramiteSeleccionado.getDepartamento().getCodigo() != null) {
            this.numeroPredialParte1 = this.tramiteSeleccionado
                .getDepartamento().getCodigo();
        } else {
            this.numeroPredialParte1 = "";
        }

        // Cargar los mismos valores del departamento viene.
        if (this.tramiteSeleccionado.getMunicipio() != null &&
            this.tramiteSeleccionado.getMunicipio().getCodigo() != null) {
            this.numeroPredialParte2 = this.tramiteSeleccionado.getMunicipio()
                .getCodigo3Digitos();
        } else {
            this.numeroPredialParte2 = "";
        }

        this.numeroPredialParte3 = "";
        this.numeroPredialParte4 = "";
        this.numeroPredialParte5 = "";
        this.numeroPredialParte6 = "";
        this.numeroPredialParte7 = "";
        this.numeroPredialParte8 = "";
        this.numeroPredialParte9 = "";
        this.numeroPredialParte10 = "";
        this.numeroPredialParte11 = "";
        this.numeroPredialParte12 = "";

        this.banderaEdicionTramiteTramite = false;
        this.tramiteTramiteSeleccionado.setEstado(ETramiteEstado.POR_APROBAR
            .getCodigo());

        if (this.tramiteSeleccionado.getTramitePredioEnglobes() != null &&
            !this.tramiteSeleccionado.getTramitePredioEnglobes()
                .isEmpty()) {
            this.prediosTramite = this.tramiteSeleccionado
                .getTramitePredioEnglobes();

            this.prediosTramiteTemp = UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
                this.prediosTramite);
            //Ajustar esta linea si no asocia los predios, tener en cuenta la persistencia
            this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(UtilidadesWeb.
                obtenerListaTramitePredioEnglobe(this.prediosTramiteTemp));
        } else {
            this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
            if (!this.tramiteSeleccionado.isQuinta()) {
                TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                tpe.setFechaLog(new Date());
                tpe.setPredio(this.tramiteSeleccionado.getPredio());
                tpe.setUsuarioLog(this.usuario.getLogin());
                tpe.setTramite(this.tramiteSeleccionado);
                //tpe = this.getTramiteService().guardarYactualizarTramitePredioEnglobe(tpe);

                this.prediosTramite.add(tpe);
                this.prediosTramiteTemp = UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
                    this.prediosTramite);

                this.tramiteTramiteSeleccionado.setPredio(this.tramiteSeleccionado.getPredio());
            }
        }
    }

    /*
     * @modified by juanfelipe.garcia :: 27-08-2013 :: #5061 se ajustó el método para consultar los
     * datos a rectificar para complementaciones y rectificaciones
     */
    public void editableMode() {
        this.banderaEdicionTramiteTramite = true;
        if (this.tramiteTramiteSeleccionado.isEsComplementacion() ||
            this.tramiteTramiteSeleccionado.isRectificacion()) {
            this.consultarDatosARectificarPorTramiteSeleccionado();
            //se asignan los valores a esta lista, la cual se usa para validaciones posteriores
            this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
            this.selectedDatosRectifComplPL.addAll(this.selectedDatosRectifComplDataTable);
        }

        if (this.tramiteTramiteSeleccionado.getClaseMutacion() != null &&
            this.tramiteTramiteSeleccionado.getClaseMutacion().equals(EMutacionClase.SEGUNDA.
                getCodigo())) {
            this.subtipoTramiteEsRequerido = true;
        } else {
            this.subtipoTramiteEsRequerido = false;
        }

        if (this.tramiteTramiteSeleccionado.isSegundaEnglobe() &&
            this.tramiteTramiteSeleccionado.getTramitePredioEnglobes() != null &&
            !this.tramiteTramiteSeleccionado.getTramitePredioEnglobes().isEmpty()) {

            this.prediosTramite = this.tramiteTramiteSeleccionado
                .getTramitePredioEnglobes();//predios asociados sin importar el tipo
            //this.prediosTramiteTemp = UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(this.prediosTramite);
        } else {

            this.prediosTramite = new ArrayList<TramitePredioEnglobe>();

            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            tpe.setFechaLog(new Date());
            tpe.setPredio(this.tramiteTramiteSeleccionado.getPredio());
            tpe.setUsuarioLog(this.usuario.getLogin());
            tpe.setTramite(this.tramiteSeleccionado);
            //tpe = this.getTramiteService().guardarYactualizarTramitePredioEnglobe(tpe);

            this.prediosTramite.add(tpe);
            //this.tramiteTramiteSeleccionado.setPredio(this.tramiteSeleccionado.getPredio());
        }
        this.prediosTramiteTemp = UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
            this.prediosTramite);

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar trámite nuevo
     */
    /*
     * @modify by juanfelipe.garcia :: 08-10-2013 :: Adición de validaciones para tramites nuevos
     * (#5667)
     */
    public void guardarTramiteNuevo() {

        RequestContext context1 = RequestContext.getCurrentInstance();

        boolean cancelarTramiteInicial;

        if (this.cancelarTramiteDecision != null &&
            !this.cancelarTramiteDecision.isEmpty() &&
            this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo())) {

            TramiteEstado tramEstado = new TramiteEstado();

            tramEstado.setEstado(ETramiteEstado.POR_CANCELAR.getCodigo());
            tramEstado.setFechaInicio(new Date());
            tramEstado.setFechaLog(new Date());
            tramEstado.setTramite(this.tramiteSeleccionado);
            tramEstado.setUsuarioLog(this.usuario.getLogin());
            tramEstado.setMotivo(this.motivoCancelacion);

            tramEstado = this.getTramiteService().actualizarTramiteEstado(tramEstado, this.usuario);

            try {
                this.tramiteSeleccionado.setTramiteEstado(tramEstado);
            } catch (Exception e) {
                LOGGER.error(
                    "Ocurrió un errror al guardar el estado: POR_CANCELAR para el trámite con id: " +
                    this.tramiteSeleccionado.getId() + e);
            }
        }

        cancelarTramiteInicial = this.validarAsociarTramitesNuevos();

        this.establecerDatosPreliminaresTramiteNuevo();

        if (cancelarTramiteInicial) {

            if (this.tramiteTramiteSeleccionado.isSegundaEnglobe()) {

                this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(UtilidadesWeb.
                    obtenerListaTramitePredioEnglobe(this.prediosTramiteTemp));

            } else if (this.tramiteTramiteSeleccionado.isMutacion()) {

                if (!this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.getPredio() !=
                    null) {
                    this.agregarPrediosFM();
                } else if (this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.
                    getPredio() != null) {
                    //Se agregan los predios si la mutacion es de quinta masivo y es PH o condominio
                    if ((this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                        Constantes.FM_CONDOMINIO_END)) ||
                        (this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.
                        getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END))) {
                        this.agregarPrediosFM();
                    }
                } else {
                    this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
                    TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                    tpe.setFechaLog(new Date());
                    tpe.setPredio(this.tramiteSeleccionado.getPredio());
                    tpe.setUsuarioLog(this.usuario.getLogin());
                    tpe.setTramite(this.tramiteSeleccionado);

                    this.prediosTramite.add(tpe);

                    this.tramiteTramiteSeleccionado.setPredio(this.tramiteSeleccionado.getPredio());
                }

            }

            // Validar el contenido del trámite
            boolean tramiteEsValido = this.validacionTramiteMB.validarDatosTramite();

            boolean bTienePredioBloqueado = tienePrediosBloqueados();

            tramiteEsValido = !bTienePredioBloqueado;

            if (tienePrediosBloqueados()) {
                this.addMensajeError("Hay predios que estáan bloqueado y no " +
                    "pueden ser seleccionados para este trámite");
            }

            if (tramiteEsValido) {

                if (this.ordenEjecucionTramites == null ||
                    this.ordenEjecucionTramites.isEmpty()) {
                    this.ordenEjecucionTramites = new ArrayList<SelectItem>();
                    this.ordenEjecucionTramites.add(new SelectItem(1));
                } else if (this.banderaEdicionTramiteTramite == false) {
                    this.ordenEjecucionTramites.add(new SelectItem(
                        this.tramiteTramites.size() + 1));
                }

                if (this.banderaEdicionTramiteTramite == false) {
                    this.tramiteTramites.add(this.tramiteTramiteSeleccionado);
                }

                this.tramiteSeleccionado.setTramites(this.tramiteTramites);

                this.banderaEdicionTramiteTramite = false;

            } else {
                context1.addCallbackParam("error", "error");
            }

        } else {
            context1.addCallbackParam("error", "error");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de realizar validaciones cuando se cancela un trámite y se asocia uno nuevo
     * que practicamente va a venir a reemplzar el trámite que se cancela
     *
     * @return isTramiteValido
     * @author javier.aponte
     */
    private boolean validarAsociarTramitesNuevos() {

        this.validacionTramiteMB.setNuevoTramiteSeleccionado(this.tramiteTramiteSeleccionado);
        this.validacionTramiteMB.setTramitesPredioEnglobeNuevoTramiteAdicionadoDTO(
            this.prediosTramiteTemp);
        this.validacionTramiteMB.setTramiteInicial(this.tramiteSeleccionado);

        return this.validacionTramiteMB.validarAsociarTramitesNuevos();
    }

    /**
     * Método encargado de validar los datos asociados a un trámite nuevo que se desea guardar
     *
     * @author javier.aponte
     * @return
     */
    /*
     * @modified juanfelipe.garcia :: 26-09-2013 :: para rectificaciones y complementaciones se
     * ajustaron los predios asociados al tramite @modify by juanfelipe.garcia :: 08-10-2013 ::
     * Adición de validaciones para tramites nuevos (#5667)
     */
    private void establecerDatosPreliminaresTramiteNuevo() {

        this.validacionTramiteMB.setNuevoTramiteSeleccionado(this.tramiteTramiteSeleccionado);

        this.validacionTramiteMB.setUsuario(this.usuario);

        //En este punto no se pueden agregar trámites nuevos de via gubernativa
        this.validacionTramiteMB.setTramitesGubernativaAll(null);

        this.validacionTramiteMB.setListaGlobalPrediosColindantes(this.listaPrediosColindantes);

        //A los nuevos trámites se les asocia la solicitud del trámite inicial
        this.validacionTramiteMB.setSolicitudSeleccionada(this.tramiteSeleccionado.getSolicitud());

        if (this.tramiteTramiteSeleccionado.isSegundaEnglobe()) {
            this.validacionTramiteMB.setTramitesPredioEnglobeNuevoTramiteAdicionado(
                this.tramiteTramiteSeleccionado.getTramitePredioEnglobes());
        } else {
            this.validacionTramiteMB.setTramitesPredioEnglobeNuevoTramiteAdicionado(
                UtilidadesWeb.obtenerListaTramitePredioEnglobe(this.prediosTramiteTemp));
            if (this.prediosTramiteTemp != null && !this.prediosTramiteTemp.isEmpty()) {
                this.tramiteTramiteSeleccionado.
                    setPredio(this.prediosTramiteTemp.get(0).getPredio());
            }
            this.validacionTramiteMB.setNuevoTramiteSeleccionado(this.tramiteTramiteSeleccionado);
        }

        if (this.tramiteTramiteSeleccionado.isEsRectificacion() || this.tramiteTramiteSeleccionado.
            isEsComplementacion()) {

            this.validacionTramiteMB.setSelectedDatosRectificarOComplementarDataTable(
                this.selectedDatosRectifComplPL);

        }

        this.tramiteTramiteSeleccionado = this.validacionTramiteMB.establecerDatosTramiteNuevo(
            this.tramiteTramiteSeleccionado);

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que valida los datos de cancelacion del trámite inicial
     *
     * @modified pedro.garcia la validación del orden de ejecución de los trámites se hace en un
     * método aparte
     */
    private boolean validarCancelacionTramiteInicial() {
        RequestContext context1 = RequestContext.getCurrentInstance();

        if (this.motivoCancelacion != null && !this.motivoCancelacion.isEmpty() &&
            this.cancelarTramiteDecision.equals(ESiNo.NO.getCodigo())) {
            String mensaje =
                "La solicitud cuenta con un motivo de cancelación del trámite inicial" +
                " pero la decisión sobre cancelar es NO," +
                " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
            context1.addCallbackParam("error", "error");
            return false;
        } else if (this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo()) &&
            (this.motivoCancelacion == null || this.motivoCancelacion
                .isEmpty())) {
            String mensaje = "La solicitud tiene el valor SI en decisión sobre cancelar" +
                " pero no cuenta con un motivo para la cancelación," +
                " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
            context1.addCallbackParam("error", "error");
            return false;
        } else if (this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo()) &&
            (this.motivoCancelacion == null && this.motivoCancelacion
                .isEmpty())) {
            String mensaje = "La solicitud tiene el valor SI el decisión sobre cancelar" +
                " pero no cuenta con un motivo para la cancelación," +
                " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
            context1.addCallbackParam("error", "error");
            return false;
        } else if (this.cancelarTramiteDecision.equals(ESiNo.NO.getCodigo()) &&
            (this.motivoCancelacion == null || this.motivoCancelacion
                .isEmpty()) && (this.tramiteTramites.isEmpty())) {
            String mensaje = "La solicitud no cuenta ni con tramites asociados" +
                " ni con cancelación del trámite inicial," +
                " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
            context1.addCallbackParam("error", "error");
            return false;
        } else if (this.tramiteTramites != null &&
            !this.tramiteTramites.isEmpty()) {

            boolean validador;
            validador = this.validarOrdenTramitesNuevos();
            if (!validador) {
                String mensaje = "Algunos de los trámites asociados presentan" +
                    " el mismo orden de ejecución," +
                    " por favor corrija los datos e intente nuevamente";
                this.addMensajeError(mensaje);
                context1.addCallbackParam("error", "error");
                return false;
            }

        }
        return true;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que elimina el trámite seleccionado de la lista de trámites asociados al trámite
     * seleccionado
     */
    public void removerTramiteDeTramiteTramites() {

        if (this.tramiteTramiteSeleccionado != null) {
            this.tramiteTramites.remove(this.tramiteTramiteSeleccionado);
            this.ordenEjecucionTramites.remove(this.tramiteTramites.size());
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Metodo que se ejecuta al cambiar el tipo de mutación
     */
    public void cambioClaseMutacion() {

        // D: si es de segunda o quinta, se hace requerido escoger el subtipo de
        // trámite; y se pone
        // el subtipo en el trámite seleccionado
        String claseMutacionSeleccionada;
        claseMutacionSeleccionada = this.tramiteTramiteSeleccionado
            .getClaseMutacion();
        if (claseMutacionSeleccionada
            .equals(EMutacionClase.SEGUNDA.getCodigo()) ||
            claseMutacionSeleccionada.equals(EMutacionClase.QUINTA
                .getCodigo())) {
            this.subtipoTramiteEsRequerido = true;
            this.banderaClaseDeMutacionSeleccionada = true;
            this.subtipoClaseMutacionV = this.generalMB
                .getSubtipoClaseMutacionV(claseMutacionSeleccionada);

            if (this.tramiteTramiteSeleccionado.getPredio() != null) {
                this.tramiteTramiteSeleccionado.setPredio(null);
            }
            if (this.tramiteTramiteSeleccionado.getPredios() != null ||
                !this.tramiteTramiteSeleccionado.getPredios().isEmpty()) {
                this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(null);
            }
        } else {
            this.subtipoTramiteEsRequerido = false;
            this.tramiteTramiteSeleccionado.setSubtipo(null);
        }

        if (!claseMutacionSeleccionada
            .equals(EMutacionClase.SEGUNDA.getCodigo())) {
            this.banderaSubtipoEnglobe = false;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Listener del subtipo de mutación
     */
    public void actualizarDesdeSubtipo() {

        if (this.tramiteTramiteSeleccionado.getSubtipo().equals(
            EMutacion2Subtipo.ENGLOBE.getCodigo())) {
            this.banderaSubtipoEnglobe = true;
        } else {
            this.banderaSubtipoEnglobe = false;
        }
        if (this.tramiteTramiteSeleccionado.getSubtipo().equals(
            EMutacion5Subtipo.NUEVO.getCodigo())) {
            this.banderaSubtipoNuevo = true;
        } else {
            this.banderaSubtipoNuevo = false;
        }
    }

    // ------------rectificación/complementación---------------------------------------------------
    /**
     * action del botón "Adicionar datos seleccionados". Adiciona los datos seleccionados del
     * picklist de datos a rectificar o complementar a la variable que los guarda
     *
     * @author pedro.garcia
     */
    public void adicionarDatosRectifCompl() {

        LOGGER.debug("adicionando datos seleccionados...");

        // D: la primera vez que entra aquí este atributo es nulo, porque nadie
        // lo ha inicializado
        if (this.selectedDatosRectifComplPL == null) {
            this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        } else {
            this.selectedDatosRectifComplPL.clear();
        }

        // OJO: no se usa la lista target, sino el atributo target del
        // DualListModel
        for (DatoRectificar drTarget : this.datosRectifComplDLModel.getTarget()) {
            if (!this.selectedDatosRectifComplDataTable.contains(drTarget)) {
                this.selectedDatosRectifComplPL.add(drTarget);
            }
        }

        this.selectedDatosRectifComplDataTable
            .addAll(this.selectedDatosRectifComplPL);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "Retirar datos". Retira de la lista de datos para rectificación o
     * complementación los que se hayan seleccionado de la tabla resumen
     *
     * @author pedro.garcia
     */
    public void retirarDatoRectifCompl() {

        LOGGER.debug("retirando datos seleccionados...");

        // FIXME::pedro.garcia::cuando los selccionados != null, manejar como
        // arreglo
        for (DatoRectificar dtR : this.selectedDatosRectifComplDT) {
            if (this.selectedDatosRectifComplDataTable.contains(dtR)) {
                this.selectedDatosRectifComplDataTable.remove(dtR);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * action del botón "aceptar" en la página de selección de datos a rectificar
     */
    public void aceptarDatosARectificar() {

        TramiteRectificacion newTramiteRectificacion;
        List<TramiteRectificacion> tramiteRectificaciones;

        if (!this.selectedDatosRectifComplDataTable.isEmpty()) {
            tramiteRectificaciones = new ArrayList<TramiteRectificacion>();
            for (DatoRectificar dr : this.selectedDatosRectifComplDataTable) {
                newTramiteRectificacion = new TramiteRectificacion();
                newTramiteRectificacion.setDatoRectificar(dr);
                newTramiteRectificacion
                    .setTramite(this.tramiteTramiteSeleccionado);
                newTramiteRectificacion.setFechaLog(new Date());
                newTramiteRectificacion.setUsuarioLog(this.usuario.getLogin());
                tramiteRectificaciones.add(newTramiteRectificacion);
            }
            this.tramiteTramiteSeleccionado
                .setTramiteRectificacions(tramiteRectificaciones);
        } else {
            this.addMensajeInfo(
                "Un trámite de rectificación debe tener datos a rectificar asociados");
            return;
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * action ejecutada sobre el botón "adicionar predios al trámite" en la página de selección
     * predios
     */
    public void adicionarPrediosAlTramite() {
        List<Predio> predios = this.getPrediosSeleccionadosMapa();
        // this.prediosSeleccionadosMapa = predios;
        if (this.prediosTramiteTemp == null || this.prediosTramiteTemp.isEmpty()) {
            //this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
            this.prediosTramiteTemp = new ArrayList<TramitePredioEnglobeDTO>();
        }
        //se valida que no tenga predios repetidos
        HashSet hs = new HashSet();
        hs.addAll(predios);
        predios.clear();
        predios.addAll(hs);
        boolean estaBloqueado = false;
        String stNumeroPredial = "";
        for (Predio p : predios) {
            TramitePredioEnglobeDTO tpe = new TramitePredioEnglobeDTO();
            tpe.setPredio(p);
            List<PredioBloqueo> predioBloqueos = this.getConservacionService().
                obtenerPredioBloqueosPorNumeroPredial(p.getNumeroPredial());
            p.setPredioBloqueos(predioBloqueos);
            if (p.isEstaBloqueado()) {
                estaBloqueado = true;
                stNumeroPredial += p.getNumeroPredial() + ",";
            }
            this.prediosTramiteTemp.add(tpe);
        }

        if (tramiteTramiteSeleccionado != null) {
            if (predios != null && !predios.isEmpty()) {
                if (predios.size() > 1) {
                    this.tramiteTramiteSeleccionado.setPredio(predios.get(0));
                } else {
                    this.tramiteTramiteSeleccionado
                        .setTramitePredioEnglobes(null);
                    this.tramiteTramiteSeleccionado.setPredio(predios.get(0));
                }
            }
        }
    }

    public void seleccionaCancelarTramite() {
        if (this.tramiteSeleccionado.isQuintaNuevo()) {
            if (this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo())) {
                this.habilitarTramitesAdicionales = true;
            } else {
                this.habilitarTramitesAdicionales = false;
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Quita de la lista de predios del trámite el que se haya seleccionado dando click en el ícono
     *
     * @author pedro.garcia
     */
    public void removePredioFromListaPrediosTramite() {
        LOGGER.info("removePredioFromListaPrediosTramite action called");
        //this.prediosTramite.remove(this.selectedPredioTramite);
        this.prediosTramiteTemp.remove(this.selectedPredioTramite);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * marca uno de los predios como principal
     *
     * @author pedro.garcia
     */
    public void markTramiteAsPrincipal() {
        LOGGER.info("markTramiteAsPrincipal action called");

        for (TramitePredioEnglobe tpe : this.prediosTramite) {
            tpe.setEnglobePrincipal("");
        }
        this.selectedPredioTramite.setEnglobePrincipal(ESiNo.SI.getCodigo());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de trámite
     *
     * @author juan.agudelo
     *
     */
    public void cambioTipoTramite() {
        this.subtipoTramiteEsRequerido = false;
        this.banderaClaseDeMutacionSeleccionada = false;
        this.tramiteTramiteSeleccionado.setSubtipo(null);
        this.tramiteTramiteSeleccionado.setTramiteRectificacions(null);
        this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        this.selectedDatosRectifComplDataTable = new ArrayList<DatoRectificar>();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Permite actualizar la lista de documentos adicionales
     */
    public void adicionarDocumentoAdicional() {
        Random rnd = new Random();
        if (this.documentacionAdicional == null ||
            this.documentacionAdicional.isEmpty()) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }
        this.documentoSeleccionado = new TramiteDocumentacion();
        TipoDocumento td = new TipoDocumento();
        this.documentoSeleccionado.setTipoDocumento(td);
        this.documentoSeleccionado.setAporta(true);
        this.documentoSeleccionado.setIdTemporal(rnd.nextLong());
        if (this.tramiteSeleccionado != null &&
            this.tramiteSeleccionado.getDepartamento() != null &&
            this.tramiteSeleccionado.getDepartamento().getCodigo() != null &&
            !this.tramiteSeleccionado.getDepartamento().getCodigo()
                .isEmpty()) {
            this.departamentoDocumento = this.tramiteSeleccionado
                .getDepartamento();
        } else {
            this.departamentoDocumento = null;
        }
        if (this.tramiteSeleccionado != null &&
            this.tramiteSeleccionado.getMunicipio() != null &&
            this.tramiteSeleccionado.getMunicipio().getCodigo() != null &&
            !this.tramiteSeleccionado.getMunicipio().getCodigo()
                .isEmpty()) {
            this.municipioDocumento = this.tramiteSeleccionado.getMunicipio();
        } else {
            this.municipioDocumento = null;
        }
        this.updateDepartamentosDocumento();
        this.updateMunicipiosDocumento();
        this.banderaDocumentoAdicional = true;

    }

    public boolean isUsuarioEjecutor() {
        return Arrays.asList(this.usuario.getRoles()).contains(
            ERol.EJECUTOR_TRAMITE.getRol().toUpperCase());
    }

    public boolean isUsuarioDirectorTerritorial() {
        return Arrays.asList(this.usuario.getRoles()).contains(
            ERol.DIRECTOR_TERRITORIAL.getRol().toUpperCase());
    }

    public boolean isUsuarioResponsableConservacion() {
        return Arrays.asList(this.usuario.getRoles()).contains(
            ERol.RESPONSABLE_CONSERVACION.getRol().toUpperCase());
    }

    public String proyectarTramiteProcess() {

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(this.usuario);
        solicitudCatastral.setUsuarios(usuarios);
        // Se envia al mismo ejecutor, quien es el de este CU. Puede ser
        // EJECUTOR, RESPONSABLE_CONSERVACION o DIRECTOR_TERRITORIAL
        solicitudCatastral
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);

        this.getProcesosService().avanzarActividad(this.usuario, activityId,
            solicitudCatastral);
        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";
    }
//--------------------------------------------------------------------------------------------------

    public String descargarSNCProcess() {

        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(this.usuario);
        solicitudCatastral.setUsuarios(usuarios);
        // Se envia al mismo ejecutor, quien es el de este CU. Puede ser
        // EJECUTOR, RESPONSABLE_CONSERVACION o DIRECTOR_TERRITORIAL

        solicitudCatastral
            //.setTransicion(ProcesoDeConservacion.ACT_EJECUCION_DESCARGAR_DEL_SNC); 
            .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_ALISTAR_INFORMACION);

        this.getProcesosService().avanzarActividad(this.usuario, activityId,
            solicitudCatastral);
        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";

    }

    // --------------------------------------------------------------------------
    public void updateDepartamentosDocumento() {
        departamentosDocumento = new LinkedList<SelectItem>();
        departamentosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.dptosDocumento = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosDocumento);
        } else {
            Collections.sort(this.dptosDocumento,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : this.dptosDocumento) {
            if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                    departamento.getNombre()));
            } else {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

    }

    // --------------------------------------------------------------------------
    public void setSelectedMunicipioDocumento(String codigo) {
        @SuppressWarnings("unused")
        Municipio municipioSelected = null;
        if (codigo != null && this.munisDocumento != null) {
            for (Municipio municipio : this.munisDocumento) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioDocumento = null;
        }
    }

    // --------------------------------------------------------------------------
    public String getSelectedMunicipioDocumento() {
        if (this.tramiteSeleccionado != null &&
            this.tramiteSeleccionado.getMunicipio() != null) {
            return this.tramiteSeleccionado.getMunicipio().getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public List<SelectItem> getMunicipiosDocumento() {
        return this.municipiosDocumento;
    }

    // --------------------------------------------------------------------------
    public void updateMunicipiosDocumento() {
        municipiosDocumento = new ArrayList<SelectItem>();
        munisDocumento = new ArrayList<Municipio>();
        municipiosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.documentoSeleccionado != null &&
            this.departamentoDocumento != null &&
            this.departamentoDocumento.getCodigo() != null) {
            munisDocumento = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoDocumento.getCodigo());
            if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                Collections.sort(munisDocumento);
            } else {
                Collections.sort(munisDocumento,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisDocumento) {
                if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                        municipio.getNombre()));
                } else {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenMunicipiosDocumento() {
        if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosDocumento = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosDocumento = EOrden.CODIGO;
        }
        this.updateMunicipiosDocumento();
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosNombreDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenMunicipiosNombreDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosCodigoDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por CODIGO
     *
     * @return
     */
    public boolean isOrdenMunicipiosCodigoDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Bandera que determina si ya se guardo el motivo de rechazo de las pruebas
     *
     * @return
     * @author javier.aponte
     */
    public boolean isBanderaGuardoRechazoPruebas() {
        return banderaGuardoRechazoPruebas;
    }

    // --------------------------------------------------------------------------
    public void setSelectedDepartamentoDocumento(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosDocumento != null) {
            for (Departamento departamento : dptosDocumento) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.documentoSeleccionado != null) {
            this.departamentoDocumento = dptoSeleccionado;
        }
    }

    // --------------------------------------------------------------------------
    public String getSelectedDepartamentoDocumento() {
        if (this.tramiteSeleccionado != null &&
            this.tramiteSeleccionado.getDepartamento() != null) {
            return this.tramiteSeleccionado.getDepartamento().getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public List<SelectItem> getDepartamentosDocumento() {
        return departamentosDocumento;
    }

    // --------------------------------------------------------------------------
    public void onChangeOrdenDepartamentosDocumento() {
        this.updateDepartamentosDocumento();
    }

    // --------------------------------------------------------------------------
    public void onChangeDepartamentosDocumento() {
        this.municipioDocumento = null;
        this.updateMunicipiosDocumento();
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenDepartamentosDocumento() {
        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosDocumento = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosDocumento = EOrden.CODIGO;
        }
        this.updateDepartamentosDocumento();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al guardar una lista de trámites asociados a un trámite
     *
     * @author juan.agudelo
     *
     */
    /*
     * @modify by juanfelipe.garcia :: 09-10-2013 :: Adición de validaciones para tramites nuevos
     * (#5667)
     */
    public void guardarTramites() {

        //D: 0
        //validación adicional si se arrepiente de cancelar el trámite original y ya tiene tramites asociados creados
        if (this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.NO.getCodigo()) &&
            !this.tramiteTramites.isEmpty()) {
            List<Tramite> tramitesAuxList = new ArrayList<Tramite>();
            boolean tramitesValidos = true;
            for (Tramite tram : this.tramiteTramites) {
                if (this.tramiteSeleccionado.isTercera() && tram.isTercera()) {
                    tramitesValidos = false;
                } else if (this.tramiteSeleccionado.isQuintaNuevo() && (tram.isTercera() || tram.
                    isSegunda())) {
                    tramitesValidos = false;
                } else {
                    tramitesAuxList.add(tram);
                }
            }
            this.tramiteTramites.clear();
            this.tramiteTramites.addAll(tramitesAuxList);

            if (!tramitesValidos) {
                this.addMensajeError(
                    "Algunos trámites adicionales fueron eliminados, no son válidos para el trámite original.");
                return;
            }

        }

        if (this.tramiteSeleccionado.isCuarta() || this.tramiteSeleccionado.isRevisionAvaluo()) {
            this.cancelarTramiteDecision = ESiNo.NO.getCodigo();
        }

        boolean guardarT = validarCancelacionTramiteInicial();

        if (guardarT == true) {
            this.banderaAsociarDocumentos = false;
        }

        if (ESiNo.SI.getCodigo().equals(this.cancelarTramiteDecision)) {
            this.mensajeCancelaTramite = "Cancelo trámite inicial: " + ESiNo.SI.getCodigo() +
                ", motivo de la cancelación: " + this.motivoCancelacion;
        } else if (ESiNo.NO.getCodigo().equals(this.cancelarTramiteDecision)) {
            this.mensajeCancelaTramite = "Cancelo trámite inicial: " + ESiNo.NO.getCodigo();
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Método que devuelve un trámite. Crea el TramiteEstado correspondiente y además mueve el
     * trámite en el process server.
     *
     * @author fabio.navarrete
     */
    public String devolver() {

        if (this.motivosDevolucionTramites != null &&
            this.motivosDevolucionTramites.getTarget() != null &&
            !this.motivosDevolucionTramites.getTarget().isEmpty()) {

            if (this.observacionesDevolucion == null &&
                this.observacionesDevolucion.trim().isEmpty()) {
                String mensaje =
                    "Debe ingresar alguna observación antes de realizar la devolución del trámite.";
                this.addMensajeError(mensaje);
                return null;
            }

            if ((this.tramiteSeleccionado.isCuarta() || this.tramiteSeleccionado
                .getSolicitud().isRevisionAvaluo()) &&
                this.motivosDevolucionTramites.getTarget().contains(
                    "Por reclasificar")) {

                String mensaje =
                    "Para éste tipo de mutación no se permite el motivo 'Por reclasificar'.";
                this.addMensajeError(mensaje);
                return null;
            }

            this.motivoDevolucion = "";
            for (String motivo : this.motivosDevolucionTramites.getTarget()) {
                this.motivoDevolucion = this.motivoDevolucion + motivo + ", ";
            }
            this.motivoDevolucion = (this.motivoDevolucion.substring(0,
                this.motivoDevolucion.length() - 2)) + ".";

            TramiteEstado tramiteEstado = new TramiteEstado();
            tramiteEstado.setMotivo(this.motivoDevolucion);
            tramiteEstado.setEstado(ETramiteEstado.DEVUELTO.getCodigo());
            tramiteEstado.setFechaInicio(new Date());
            tramiteEstado.setFechaLog(new Date());
            tramiteEstado.setObservaciones(this.observacionesDevolucion);
            tramiteEstado.setResponsable(usuario.getLogin());
            tramiteEstado.setTramite(this.tramiteSeleccionado);
            tramiteEstado.setUsuarioLog(this.usuario.getLogin());
            this.tramiteSeleccionado.getTramiteEstados().add(tramiteEstado);
            if (this.tramiteSeleccionado.getTramitePruebas() != null &&
                this.tramiteSeleccionado.getTramitePruebas().getId() == null) {
                this.tramiteSeleccionado.setTramitePruebas(null);
            }
            this.getTramiteService().guardarActualizarTramite(
                this.tramiteSeleccionado);

            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();

            //felipe.cadena::12-09-2016::#19450::Se envian las actividades a los coordinadores y los responsables
            List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();
            usuariosActividad.addAll(this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION));

            usuariosActividad.addAll(this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(), ERol.COORDINADOR));

            solicitudCatastral.setUsuarios(usuariosActividad);
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_DEVUELTO);

            try {
                this.getProcesosService().avanzarActividad(this.usuario, activityId,
                    solicitudCatastral);
            } catch (Exception e) {
                LOGGER.error("Ocurrió un error en EjecucionMB#devolver al mover el proceso: " + e.
                    getMessage());
                return null;
            }
            UtilidadesWeb.removerManagedBean("ejecucion");
            this.tareasPendientesMB.init();
            return ConstantesNavegacionWeb.INDEX;
        } else {
            this.addMensajeError("Debe seleccionar al menos un motivo de devolución.");
            return null;
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que actualiza los estados del trámite seleccionado y lo mueve en el proceso a la
     * actividad de Depuración geográfica.
     *
     * @author david.cifuentes
     * @return
     */
    /*
     * @modified pedro.garcia 11-12-2013 CC-NP-CO-034
     */
    public String enviarADepuracionGeografica() {

        UsuarioDTO usuarioDestino = this.generalMB.validarEnvioDepuracion(
            this.tramiteSeleccionado, this.usuario, this.actividad);

        if (usuarioDestino == null) {
            return null;
        }

        this.generalMB.crearRegistroTramiteDepuracion(this.tramiteSeleccionado, this.usuario);

        try {
//			this.getConservacionService().generarReplicaDeConsultaTramiteYAvanzarProceso(
//                    this.actividad,
//                    this.usuario,
//                    usuarioDestino, 
//                    true);
            Tramite tramite = this.getTramiteService().
                buscarTramitePorId(this.tramiteSeleccionadoId);
            //felipe.cadena::ACCU::Se cambia la forma de generar la replica de consulta
            RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
                getManagedBean("recuperarTramites");
            recuperarTamitesMB.generarReplicaConsulta(tramite, this.usuario, usuarioDestino, 0);

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            this.addMensajeError("Error al avanzar el proceso.");
            return "";
        }

        //D: CC-NP-CO-034: se envia correo al responsable de conservación para que sepa que se debe
        //  retirar el trámite de la comisión, si este era de terreno y pertenecía a alguna comisión
        //  activa
        if (this.tramiteSeleccionado.getClasificacion().equals(ETramiteClasificacion.TERRENO.
            toString())) {
            ArrayList<Comision> comisionesActivasTramite;

            //D: OJO: los trámites que llegan a esta actividad son de conservación, por lo tanto se buscan
            // las comisiones de conservación, que debería ser solo una (activa)
            comisionesActivasTramite = (ArrayList<Comision>) this.getTramiteService().
                consultarComisionesActivasTramite(
                    this.tramiteSeleccionado.getId(), EComisionTipo.CONSERVACION.getCodigo());
            if (comisionesActivasTramite != null && !comisionesActivasTramite.isEmpty()) {
                if (comisionesActivasTramite.size() > 1) {
                    LOGGER.error("El trámite " + this.tramiteSeleccionado.getId() +
                        " aparece en más de una comisión de conservación activa");
                    return "";
                } else {
                    boolean envioCorreo;
                    envioCorreo = this.enviarCorreoRetiroTramitePorEnvioDepuracion(
                        this.tramiteSeleccionado.getNumeroRadicacion(),
                        comisionesActivasTramite.get(0).getNumero());

                    if (!envioCorreo) {
                        LOGGER.error(
                            "Ocurrió en error enviando correo al responsable de conservación por el movimiento del trámite " +
                            this.tramiteSeleccionado.getId());
                    }
                }
            }
        }

        this.tareasPendientesMB.init();
        return cerrar();

//TODO :: david.cifuentes :: este TODO parece ser para usted :: pedro.garcia
        // Revisar la creación registros en TramiteEstado y en TramiteDepuracion
        // :: 12/06/2013
    }
//--------------------------------------------------------------------------------------------------

    public String getMotivoDevolucion() {
        return this.motivoDevolucion;
    }

    public void setMotivoDevolucion(String motivoDevolucion) {
        this.motivoDevolucion = motivoDevolucion;
    }

    public String getObservacionesDevolucion() {
        return this.observacionesDevolucion;
    }

    public void setObservacionesDevolucion(String observacionesDevolucion) {
        this.observacionesDevolucion = observacionesDevolucion;
    }

    public List<DatoRectificar> getSelectedDatosRectifComplDataTable() {
        return selectedDatosRectifComplDataTable;
    }

    public void setSelectedDatosRectifComplDataTable(
        List<DatoRectificar> selectedDatosRectifComplDataTable) {
        this.selectedDatosRectifComplDataTable = selectedDatosRectifComplDataTable;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton asociar documentos desde el caso de uso de revisar trámite
     * asignado
     *
     * @author javier.aponte
     *
     */
    public void asociarDocumentacion() {

        boolean banderaDocumentoYaEstaAsociado = false;
        boolean mostrarMensaje = false;
        if (this.tramitesTramiteSeleccionados != null && this.tramitesTramiteSeleccionados.length >
            0) {
            if ((this.documentacionAportada != null && this.documentacionAportada.length > 0) ||
                (this.documentacionAdicionalS != null && this.documentacionAdicionalS.length > 0)) {

                for (Tramite tTemp : this.tramitesTramiteSeleccionados) {

                    if (this.documentacionAportada != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAportada) {
                            for (TramiteDocumentacion tTempDocReq : tTemp.
                                getDocumentacionRequerida()) {
                                if (tTempDocReq.getTipoDocumento().getNombre().equals(tdTemp.
                                    getTipoDocumento().getNombre())) {
                                    banderaDocumentoYaEstaAsociado = true;
                                }
                            }
                            if (banderaDocumentoYaEstaAsociado == false) {
                                if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                    tTemp.setTramiteDocumentacionsTemp(
                                        new ArrayList<TramiteDocumentacion>());
                                }
                                if (!tTemp.getTramiteDocumentacionsTemp()
                                    .contains(tdTemp)) {
                                    tTemp.getTramiteDocumentacionsTemp().add(
                                        tdTemp);
                                    mostrarMensaje = true;
                                }

                            } else {
                                String mensaje = "Ya habia asociado ese documento," +
                                    " por favor seleccione otro documento e intente nuevamente.";
                                this.addMensajeError(mensaje);
                            }
                        }
                    }

                    if (this.documentacionAdicionalS != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAdicionalS) {
                            if (tTemp.getTramiteDocumentacions() == null) {
                                tTemp.
                                    setTramiteDocumentacions(new ArrayList<TramiteDocumentacion>());
                            }
                            if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                tTemp.setTramiteDocumentacionsTemp(
                                    new ArrayList<TramiteDocumentacion>());
                            }
                            if (!tTemp.getTramiteDocumentacionsTemp()
                                .contains(tdTemp)) {
                                tTemp.getTramiteDocumentacionsTemp().add(
                                    tdTemp);
                                mostrarMensaje = true;
                            }
                        }
                    }
                }
                if (mostrarMensaje) {
                    String mensaje = "Se asoció correctamente la documentación";
                    this.addMensajeInfo(mensaje);
                }
            } else {
                String mensaje = "Debe seleccionar por lo menos un documento para" +
                    " ser asociado," +
                    " por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Debe seleccionar por lo menos un trámite para" +
                " asociar la documentación," +
                " por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    /**
     * Listener ejecutado en el cambio de tab en la página de asociar Tramites Nuevos
     *
     * @author leidy.gonzalez
     * @param event
     */
    public void onTabChangeDocumentos(TabChangeEvent event) {
        String tabId = event.getTab().getId();

        if (this.documentacionAportada != null && "tblDocumentosAdTab".equals(tabId)) {
            this.documentacionAportada = null;
        }
        if (this.documentacionAdicionalS != null && "tblDocumentacionTramTab".equals(tabId)) {
            this.documentacionAdicionalS = null;

        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton desasociar documentos desde el caso de uso de revisar trámite
     * asignado
     *
     * @author javier.aponte
     *
     */
    public void desasociarDocumentacion() {

        if (this.tramitesTramiteSeleccionados != null && this.tramitesTramiteSeleccionados.length >
            0) {
            if ((this.documentacionAportada != null && this.documentacionAportada.length > 0) ||
                (this.documentacionAdicionalS != null && this.documentacionAdicionalS.length > 0)) {

                for (Tramite tTemp : this.tramitesTramiteSeleccionados) {

                    if (this.documentacionAportada != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAportada) {
                            if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                tTemp.setTramiteDocumentacionsTemp(
                                    new ArrayList<TramiteDocumentacion>());
                            }
                            if (tTemp.getTramiteDocumentacionsTemp()
                                .contains(tdTemp)) {
                                tTemp.getTramiteDocumentacionsTemp().remove(
                                    tdTemp);
                            }
                        }
                    }

                    if (this.documentacionAdicionalS != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAdicionalS) {
                            if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                tTemp.setTramiteDocumentacionsTemp(
                                    new ArrayList<TramiteDocumentacion>());
                            }
                            if (tTemp.getTramiteDocumentacionsTemp()
                                .contains(tdTemp)) {
                                tTemp.getTramiteDocumentacionsTemp().remove(
                                    tdTemp);
                            }
                        }
                    }
                }

            } else {
                String mensaje = "Debe seleccionar por lo menos un documento para" +
                    " ser desasociado," +
                    " por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Debe seleccionar por lo menos un trámite para" +
                " desasociar la documentación," +
                " por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Metodo que avanza a elaborar informe desde cargar prueba
     *
     * @author fredy.wilches
     *
     */
    public String enviarTramitesAsociadosAvanzarProceso() {
        if (this.entidadesPruebas != null && !this.entidadesPruebas.isEmpty()) {
            for (TramitePruebaEntidad tpe : this.entidadesPruebas) {
                if (tpe.getDocumentosPruebas() <= 0) {
                    this.addMensajeError(
                        "Una o más entidades no tienen documentos asociados. Registre uno o más para cada entidad y vuelva a intentarlo.");
                    return null;
                }
            }
        }

        Solicitud solicitud = null;
        if (this.tramiteSeleccionado.getSolicitud() != null) {
            solicitud = this.getTramiteService().findSolicitudPorId(this.tramiteSeleccionado.
                getSolicitud().getId());
        }

        if (solicitud != null && solicitud.isViaGubernativa()) {
            this.getTramiteService()
                .enviarTramitesAsociadosAModificarInformacionAlfanumerica(
                    this.activityId, this.tramiteSeleccionado,
                    this.usuario);
        } else {
            this.getTramiteService().enviarTramitesAsociadosAElaborarInforme(
                this.activityId, this.tramiteSeleccionado, this.usuario);
        }

        this.tareasPendientesMB.init();
        return "index";
    }

    /**
     * Metodo que avanza a aprobar desde Asociar documento al nuevo tramite
     *
     * @author fredy.wilches
     *
     */
    public String enviarTramitesAsociadosAAprobar() {
        //TODO: mirar si en este metodo se hace la persistencia de la lista de predios asociados al tramite (segunda - englobe)
        try {

            List<TramiteDocumentacion> documentacionTramitesAsociados;
            TramiteDocumentacion nuevoTramiteDocumentacion;

            int contadorTDocsFaltantes = 0;
            int contadorTPrediosFaltantes = 0;

            for (Tramite t : this.tramiteTramites) {
                if (t.getPredio() == null && t.getTramitePredioEnglobes() == null) {
                    contadorTPrediosFaltantes++;
                }
                if (t.getTramiteDocumentacionsTemp() != null &&
                    !t.getTramiteDocumentacionsTemp().isEmpty()) {

                    documentacionTramitesAsociados = new ArrayList<TramiteDocumentacion>();

                    for (TramiteDocumentacion tramDocumentacion : t.getTramiteDocumentacionsTemp()) {
                        nuevoTramiteDocumentacion = (TramiteDocumentacion) tramDocumentacion.clone();
                        nuevoTramiteDocumentacion.setId(null);
                        documentacionTramitesAsociados.add(nuevoTramiteDocumentacion);
                    }

                    t.setTramiteDocumentacions(documentacionTramitesAsociados);

                    t.setSolicitud(this.tramiteSeleccionado.getSolicitud());
                } else {
                    contadorTDocsFaltantes++;
                }
            }

            // TODO juan.agudelo eliminar cuando se integre con procesos
            if (this.tramiteSeleccionado.getTramitePruebas() != null) {
                this.tramiteSeleccionado.setTramitePruebas(null);
            }

            if (contadorTPrediosFaltantes > 0) {
                String mensaje = "Algunos de los trámites nuevos solicitados no cuentan con" +
                    " un(os) predio(s) asociado(s)," +
                    " por favor corrija los datos e intente nuevamente";
                this.addMensajeError(mensaje);
                return null;
            }

            if (contadorTDocsFaltantes == 0) {
                this.tramiteSeleccionado.setTramites(this.tramiteTramites);
                List<Tramite> tramiteParaActualizar = new ArrayList<Tramite>();
                tramiteParaActualizar.add(this.tramiteSeleccionado);

                @SuppressWarnings("unused")
                List<Tramite> tramiteResultado = this.getTramiteService()
                    .guardarActualizarTramitesConDocumentos(
                        tramiteParaActualizar, this.usuario);

                /* SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                 * solicitudCatastral .setUsuarios(this .getTramiteService()
                 * .buscarFuncionariosPorRolYTerritorial( this.usuario
                 * .getDescripcionEstructuraOrganizacional(), ERol.COORDINADOR));
                 *
                 * solicitudCatastral
                 * .setTransicion(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES); */
                this.getTramiteService().enviarTramitesAsociadosAAprobar(
                    this.activityId, this.tramiteSeleccionado, this.usuario);

                //Utilidades.removerManagedBean("estudioDeCostosAct");
                this.tareasPendientesMB.init();
                return "index";

            } else {
                String mensaje = "Algunos de los trámites nuevos solicitados no cuentan con" +
                    " documentación asociada," +
                    " por favor corrija los datos e intente nuevamente";
                this.addMensajeError(mensaje);
                return null;
            }

        } catch (Exception e) {
            String mensaje = "Ocurrió un error al guardar los trámites asociados";
            this.addMensajeError(mensaje);
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo utilizado para guardar documentos adicionales y ejecutado en la ventana modal de
     * nuevoDocumentoTramite como accion sobre el boton Aceptar
     */
    public void guardarDocumentacion() {

        guardarDocumento();

        this.banderaDocumentoAdicional = false;
        this.editandoDocumentacion = false;

        this.archivoResultado = new File("");
        this.rutaMostrar = "";

    }

    public void guardarDocumento() {

        TramiteDocumentacion tdTemp = this.documentoSeleccionado;

        if (this.documentacionAdicional == null) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }

        if (this.departamentoDocumento != null) {
            tdTemp.setDepartamento(this.departamentoDocumento);
        }
        if (this.municipioDocumento != null) {
            tdTemp.setMunicipio(this.municipioDocumento);
        }
        if (this.documentoSeleccionado != null &&
            this.documentoSeleccionado.getTipoDocumento() != null &&
            this.documentoSeleccionado.getTipoDocumento()
                .getId() != null) {
            tdTemp.setTipoDocumento(this.documentoSeleccionado
                .getTipoDocumento());
        } else if (this.documentoSeleccionado != null &&
            this.documentoSeleccionado.getTipoDocumento() != null &&
            this.documentoSeleccionado.getTipoDocumento()
                .getClase() != null) {

            if (this.documentoSeleccionado.getTipoDocumento()
                .getClase().equals(Constantes.OTRO)) {
                tdTemp.getTipoDocumento().setId(
                    ETipoDocumento.OTRO.getId());
            } else if (this.documentoSeleccionado.getTipoDocumento()
                .getClase().equals(Constantes.OFICIO)) {
                tdTemp.getTipoDocumento().setId(
                    ETipoDocumento.OFICIOS_REMISORIOS.getId());
            } else if (this.documentoSeleccionado.getTipoDocumento()
                .getClase().equals(Constantes.RESOLUCION)) {
                tdTemp.getTipoDocumento().setId(
                    ETipoDocumento.RESOLUCION_DE_CONSERVACION
                        .getId());
            }
        }
        tdTemp.setUsuarioLog(this.usuario.getLogin());
        tdTemp.setFechaLog(new Date(System.currentTimeMillis()));
        tdTemp.setCantidadFolios(this.documentoSeleccionado.getCantidadFolios());

        tdTemp.setIdTemporal(Long.valueOf(this.documentacionAdicional.size() + 1));

        Documento doc = generarDocumento(tdTemp);
        this.documentoSeleccionado.setDocumentoSoporte(doc);

        if (this.banderaDocumentoAdicional == true) {
            tdTemp.setAdicional(ESiNo.SI.getCodigo());
            tdTemp.setRequerido(ESiNo.NO.getCodigo());
            if (!this.banderaEditandoDocumentoAdicional) {
                this.documentacionAdicional.add(tdTemp);
            }
        } else {
            tdTemp.setAdicional(ESiNo.NO.getCodigo());
            tdTemp.setRequerido(ESiNo.SI.getCodigo());
        }

        this.documentoSeleccionado = new TramiteDocumentacion();

    }

    private Documento generarDocumento(TramiteDocumentacion tdTemp) {

        Documento doc = new Documento();
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioLog(this.usuario.getLogin());
        doc.setFechaLog(new Date(System.currentTimeMillis()));
        doc.setTipoDocumento(tdTemp.getTipoDocumento());
        doc.setDescripcion(this.documentoSeleccionado.getDetalle());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setNombreDeArchivoCargado(this.nombreTemporalArchivo);

        if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
            doc.setArchivo(this.rutaMostrar);
            doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
            tdTemp.setAportado(ESiNo.SI.getCodigo());
        } else if (this.documentoSeleccionado.getDocumentoSoporte() != null) {
            doc.setArchivo(this.documentoSeleccionado.getDocumentoSoporte().getArchivo());
        } else {
            doc.setArchivo("");
        }

        return doc;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    /*
     * @modified leidy.gonzalez #10683 19-12-2014 :: cambio de caracteres especiales por espacion
     */
 /*
     * @modified by leidy.gonzalez ::#16501:: 17/03/2016:: Se valida que se almacene documento en el
     * servidor FTP
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        IDocumentosService servicioDocumental;

        DocumentoVO documentoVo;

        //String nombreArchivoSinCaracteresEspeciales = eventoCarga.getFile().getFileName().replaceAll("[^a-zA-Z0-9./]", "_");
        //this.rutaMostrar = nombreArchivoSinCaracteresEspeciales;
        this.rutaMostrar = eventoCarga.getFile().getFileName();

        try {
            // TODO esto debe ser generico no del reporte
            String directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();

            this.archivoResultado = new File(directorioTemporal, this.rutaMostrar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.nombreTemporalArchivo = eventoCarga.getFile().getFileName();

            //Generar Ruta de Imagen de Documento de Carga en el FTP
            //D: se carga el archivo en la carpeta temporal del sistema
            this.rutaTemporalFoto = UtilidadesWeb.obtenerRutaTemporalArchivos() + separadorCarpeta +
                nombreTemporalArchivo;

            //D: se carga el archivo en la ruta web de previsualización
            servicioDocumental = DocumentalServiceFactory.getService();
            documentoVo = servicioDocumental.cargarDocumentoWorkspacePreview(this.rutaTemporalFoto);

            if (documentoVo != null) {
                this.rutaTemporalFoto = documentoVo.getIdSistemaDocumental();
            }

            this.banderaDocumentoCargado = true;

            String mensaje = "El archivo seleccionado se cargo exitosamente al sistema.";
            this.addMensajeInfo(mensaje);

            if (this.banderaDocumentoCargado == true &&
                this.banderaDocumentoEncontradoEnCorrrespondencia == true) {
                this.banderaBotonGuardarDocPrueba = true;
            }

        } catch (IOException e) {
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            LOGGER.error("Error en EjecucionMB: " + mensaje);
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar documentos adicionales para eliminar
     *
     * @author juan.agudelo
     *
     */
    public void eliminarDocumentacionAdicional() {
        this.documentacionAdicional.remove(this.documentoSeleccionado);
    }

    public void eliminarDocumentacionAdicionalAsociarTramitesNuevos() {
        for (TramiteDocumentacion docAdicionalIterador : this.documentacionAdicional) {
            if ((docAdicionalIterador.getCantidadFolios() == this.documentoSeleccionado.
                getCantidadFolios()) && (docAdicionalIterador.getDepartamento() ==
                this.documentoSeleccionado.getDepartamento()) && (docAdicionalIterador.
                getMunicipio() == this.documentoSeleccionado.getMunicipio()) &&
                (docAdicionalIterador.getDetalle() == this.documentoSeleccionado.getDetalle()) &&
                (docAdicionalIterador.getDocumentoSoporte() == this.documentoSeleccionado.
                getDocumentoSoporte()) && (docAdicionalIterador.getTipoDocumento() ==
                this.documentoSeleccionado.getTipoDocumento())) {
                int index = this.documentacionAdicional.indexOf(docAdicionalIterador);
                this.documentacionAdicional.remove(index);
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método invocado para buscar una solicitud pendiente en Correspondencia, trae datos y los
     * setea en la solicitud seleccionada
     *
     * @modified felipe.cadena -- se agrega el parametro usuario
     */
    public void buscarRadicadoEnCorrespondecia() {

        Documento documento;
        //felipe.cadena::20-12-2016::Consideraciones para entidades delegadas
        if (this.usuarioDelegado) {
            if (this.documentoPruebaSeleccionado.getNumeroRadicacion() == null ||
                this.documentoPruebaSeleccionado.getNumeroRadicacion().isEmpty()) {
                this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_002.toString());
                LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_002.getMensajeTecnico());
                return;
            }
            documento = this.documentoPruebaSeleccionado;
            documento.setEstructuraOrganizacionalCod(this.usuario.getCodigoTerritorial());
        } else {
            documento = this.getTramiteService()
                .obtenerDocumentoPruebaCorrespondencia(
                    this.documentoPruebaSeleccionado.getNumeroRadicacion(), this.usuario);
        }

        if (documento != null && documento.getEstructuraOrganizacionalCod() != null) {
            boolean fueRadicado = false;
            for (TramiteDocumento doc : this.tramiteSeleccionado
                .getTramiteDocumentos()) {
                if (doc.getDocumento().getNumeroRadicacion() != null &&
                    doc.getDocumento().getNumeroRadicacion()
                        .equals(documento.getNumeroRadicacion())) {

                    fueRadicado = true;
                }
            }
            if (!fueRadicado) {

                this.documentoPruebaSeleccionado = documento;
                this.banderaDocumentoEncontradoEnCorrrespondencia = true;
                if (this.banderaDocumentoCargado == true &&
                    this.banderaDocumentoEncontradoEnCorrrespondencia == true) {
                    this.banderaBotonGuardarDocPrueba = true;
                }
            } else {
                this.addMensajeError("El documento ya fue radicado para este trámite.");
            }
        } else {
            String mensaje = "El documento no se encontró en correspondencia," +
                " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Permite actualizar la lista de documentos de pruebas
     */
    public void adicionarDocumentoPruebaAdicional() {

        this.documentoPruebaSeleccionado = new Documento();

        this.banderaDocumentoEncontradoEnCorrrespondencia = false;
        this.banderaDocumentoCargado = false;
        this.banderaBotonGuardarDocPrueba = false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author leidy.gonzalez Permite actualizar la lista de documentos de pruebas
     */
    /*
     * @modified by leidy.gonzalez:: #16989:: 27/04/2016 Se realiza modificacion para cuando se
     * edite el documento de prueba se guarde la actualizacion del documento y no del tramite
     * documento
     */
    public void guardarDocumentoCargarPruebas() {

        if (this.banderaBotonEdicionDocPrueba == true) {
            if (this.tramiteDocumentoTemporal != null &&
                this.tramiteDocumentoTemporal.getDocumento() != null &&
                this.tramiteDocumentoTemporal.getDocumento().getArchivo() != null &&
                this.nombreTemporalArchivo != null &&
                (this.documentosSelectedEntidad != null || !this.documentosSelectedEntidad.
                    isEmpty())) {

                for (TramiteDocumento tramiteDocumentoEditar : this.documentosSelectedEntidad) {

                    if (tramiteDocumentoEditar.getDocumento() != null && tramiteDocumentoEditar.
                        getDocumento().getArchivo() != null &&
                        tramiteDocumentoEditar.getDocumento().getArchivo().equals(
                            this.tramiteDocumentoTemporal.getDocumento().getArchivo())) {

                        tramiteDocumentoEditar.getDocumento().setArchivo(nombreTemporalArchivo);

                        this.getTramiteService()
                            .guardarDocumento(this.usuario,
                                tramiteDocumentoEditar.getDocumento());

                        this.tramiteDocumentoTemporal = new TramiteDocumento();
                        this.banderaBotonEdicionDocPrueba = false;

                        break;
                    }
                }

            }
        } else {

            TipoDocumento tipoDocumento = new TipoDocumento();

            tipoDocumento.setId(ETipoDocumento.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId());

            //radicaciones delegacion
            String numRadicadoExt = this.documentoPruebaSeleccionado.getNumeroRadicacion();

            this.documentoPruebaSeleccionado = new Documento();

            if (this.usuarioDelegado) {
                this.documentoPruebaSeleccionado.setNumeroRadicacion(numRadicadoExt);
            }
            this.documentoPruebaSeleccionado
                .setTramiteId(this.tramiteSeleccionado.getId());

            this.documentoPruebaSeleccionado.setArchivo(nombreTemporalArchivo);
            this.documentoPruebaSeleccionado.setFechaRadicacion(new Date());
            this.documentoPruebaSeleccionado.setBloqueado(ESiNo.NO.getCodigo());
            this.documentoPruebaSeleccionado
                .setEstado(EDocumentoEstado.RECIBIDO_PRUEBAS.getCodigo());
            this.documentoPruebaSeleccionado.setFechaLog(new Date());
            this.documentoPruebaSeleccionado.setUsuarioLog(this.usuario
                .getLogin());
            this.documentoPruebaSeleccionado.setUsuarioCreador(this.usuario
                .getLogin());
            this.documentoPruebaSeleccionado.setEstado(EDocumentoEstado.CARGADO
                .getCodigo());

            this.documentoPruebaSeleccionado.setTipoDocumento(tipoDocumento);
            // Actualiza el documento en BD y Alfresco
            this.documentoPruebaSeleccionado = this.getTramiteService()
                .guardarDocumento(this.usuario,
                    this.documentoPruebaSeleccionado);

            TramiteDocumento tdTemp = new TramiteDocumento();

            tdTemp.setDocumento(this.documentoPruebaSeleccionado);
            tdTemp.setfecha(new Date());
            tdTemp.setFechaLog(new Date());
            tdTemp.setUsuarioLog(this.usuario.getLogin());

            if (this.selectedEntidadPruebas != null && this.selectedEntidadPruebas.
                getNumeroIdentificacion() != null &&
                this.selectedEntidadPruebas.getTipoIdentificacion() != null &&
                this.selectedEntidadPruebas.getNombreCompleto() != null) {

                tdTemp.setIdPersonaNotificacion(this.selectedEntidadPruebas.
                    getNumeroIdentificacion());
                tdTemp.setTipoIdPersonaNotificacion(this.selectedEntidadPruebas.
                    getTipoIdentificacion());
                tdTemp.setPersonaNotificacion(this.selectedEntidadPruebas.getNombreCompleto());

            }

            tdTemp.setTramite(this.tramiteSeleccionado);

            tdTemp = this.getTramiteService()
                .actualizarTramiteDocumento(tdTemp);

            if (tdTemp != null) {

                this.documentosPruebasAEliminar.add(tdTemp);

                this.tramiteSeleccionado.setDocumentosPruebas(this.documentosPruebasAEliminar);
            }

            this.guardarSolicitudPruebas();

            if (this.tp != null) {
                this.selectedEntidadPruebas.setTramitePrueba(this.tp);

                this.tramiteSeleccionado.setTramitePruebas(tp);

                this.selectedEntidadPruebas = this.getTramiteService().
                    guardarActualizarTramitePruebaEntidad(this.selectedEntidadPruebas);

                for (TramitePruebaEntidad tramPruebEntidTemp : this.entidadesPruebas) {

                    if (tramPruebEntidTemp.getId().equals(this.selectedEntidadPruebas.getId())) {
                        tramPruebEntidTemp.setDocumentosPruebas(tramPruebEntidTemp.
                            getDocumentosPruebas() + 1);
                    }

                }

            }
            this.banderaBotonEdicionDocPrueba = false;
        }

    }

    /**
     * Permite actualizar la lista de documentos de pruebas
     */
    /*
     * @modified by leidy.gonzalez:: #16501:: Se realizan validaciones para que no genere problema
     * al almacenar el documento de pruebas
     */
    public void guardarDocumentoPruebas() {

        this.documentosPruebasAEliminar = new ArrayList<TramiteDocumento>();

        if (this.banderaBotonGuardarDocPrueba == true) {

            TramiteDocumento tdTemp = new TramiteDocumento();

            if (this.banderaBotonEdicionDocPrueba == true) {

                if (this.tramiteSeleccionado.getTramiteDocumentos() != null &&
                    !this.tramiteSeleccionado.getTramiteDocumentos()
                        .isEmpty()) {

                    this.tramiteSeleccionado.getTramiteDocumentos().remove(
                        this.tramiteDocumentoTemporal);
                } else {

                    this.tramiteSeleccionado
                        .setTramiteDocumentos(new ArrayList<TramiteDocumento>());

                }

            }

            TipoDocumento tipoDocumento = new TipoDocumento();

            tipoDocumento.setId(
                ETipoDocumento.AUTO_DE_PRUEBAS_REVISION_AVALUO_AUTOESTIMACION_Y_REPOSICION.getId());

            this.documentoPruebaSeleccionado = new Documento();

            this.documentoPruebaSeleccionado
                .setTramiteId(this.tramiteSeleccionado.getId());

            if (this.reporteAutoDePruebas != null && this.reporteAutoDePruebas.
                getRutaCargarReporteAAlfresco() != null) {
                this.documentoPruebaSeleccionado.setArchivo(this.reporteAutoDePruebas.
                    getRutaCargarReporteAAlfresco());
            }
            this.documentoPruebaSeleccionado.setArchivo(nombreTemporalArchivo);
            this.documentoPruebaSeleccionado.setFechaRadicacion(new Date());
            this.documentoPruebaSeleccionado.setBloqueado(ESiNo.NO.getCodigo());
            this.documentoPruebaSeleccionado
                .setEstado(EDocumentoEstado.RECIBIDO_PRUEBAS.getCodigo());
            this.documentoPruebaSeleccionado.setFechaLog(new Date());
            this.documentoPruebaSeleccionado.setUsuarioLog(this.usuario
                .getLogin());
            this.documentoPruebaSeleccionado.setUsuarioCreador(this.usuario
                .getLogin());
            this.documentoPruebaSeleccionado.setEstado(EDocumentoEstado.CARGADO
                .getCodigo());

            this.documentoPruebaSeleccionado.setTipoDocumento(tipoDocumento);
            // Actualiza el documento en BD y Alfresco
            this.documentoPruebaSeleccionado = this.getTramiteService()
                .guardarDocumento(this.usuario,
                    this.documentoPruebaSeleccionado);

            tdTemp.setDocumento(this.documentoPruebaSeleccionado);
            tdTemp.setfecha(new Date());
            tdTemp.setFechaLog(new Date());
            tdTemp.setUsuarioLog(this.usuario.getLogin());
            tdTemp.setIdPersonaNotificacion(this.tramiteSeleccionado.getSolicitud().
                getSolicitanteSolicituds().get(0).getNumeroIdentificacion());
            tdTemp.setTipoIdPersonaNotificacion(this.tramiteSeleccionado.getSolicitud().
                getSolicitanteSolicituds().get(0).getTipoIdentificacion());
            tdTemp.setPersonaNotificacion(this.tramiteSeleccionado.getSolicitud().
                getSolicitanteSolicituds().get(0).getNombreCompleto());
            tdTemp.setTramite(this.tramiteSeleccionado);

            tdTemp = this.getTramiteService()
                .actualizarTramiteDocumento(tdTemp);

            this.tramiteSeleccionado.getTramiteDocumentos().add(tdTemp);

            this.tramiteSeleccionado = this.getTramiteService()
                .guardarActualizarTramite(this.tramiteSeleccionado);

            this.tramiteSeleccionado.setTramiteDocumentos(
                this.getTramiteService().
                    obtenerTramiteDocumentosPorTramiteId(this.tramiteSeleccionadoId));

            this.banderaBotonEdicionDocPrueba = false;

        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Permite eliminar documentos de pruebas aportados
     */
    /*
     * @modified by leidy.gonzalez :: #16501 :: 07/04/2016 Se elimia metodo que cambia numero de
     * documentos en la tabla de entidades o grupos de trabajo, por un cambio directo a la variable.
     */
    public void eliminarDocumentoPruebas() {
        if (this.tramiteDocumentoTemporal != null) {

            this.tramiteSeleccionado.getTramiteDocumentos().remove(
                this.tramiteDocumentoTemporal);

            this.getTramiteService()
                .actualizarTramiteDocumentos(this.tramiteSeleccionado,
                    this.usuario);

            this.getTramiteService()
                .eliminarTramiteDocumentoConDocumentoAsociado(
                    this.tramiteDocumentoTemporal);

            List<TramiteDocumento> documentosTemp = this.documentosSelectedEntidad;
            documentosTemp.remove(this.tramiteDocumentoTemporal);

            this.documentosSelectedEntidad = documentosTemp;

            // Método que actualiza el número de documentos de pruebas en la
            // tabla
            for (TramitePruebaEntidad tramPruebEntidTemp : this.entidadesPruebas) {

                if (tramPruebEntidTemp.getId().equals(this.selectedEntidadPruebas.getId())) {
                    tramPruebEntidTemp.setDocumentosPruebas(tramPruebEntidTemp.
                        getDocumentosPruebas() - 1);
                }

            }

            if (this.tramiteSeleccionado != null && this.tramiteSeleccionado.getId() != null) {
                this.tramiteSeleccionado.setTramiteDocumentos(
                    this.getTramiteService().
                        obtenerTramiteDocumentosPorTramiteId(this.tramiteSeleccionado.getId()));
            }

        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método encargado de generar el reporte de auto de pruebas
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 11/08/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarDocumentoAutoDePruebas() {

        FirmaUsuario firma = null;
        String documentoFirma;

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.AUTO_DE_PRUEBAS;

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("TRAMITE_ID", String.valueOf(this.tramiteSeleccionadoId));
        parameters.put("ROL_USUARIO", ERol.ABOGADO.getRol());

        String estructuraOrg = this.getGeneralesService().
            getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                this.tramiteSeleccionado.getPredio().getMunicipio().getCodigo());
        List<UsuarioDTO> abogados = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
            estructuraOrg, ERol.ABOGADO);

        UsuarioDTO abogado = new UsuarioDTO();

        for (UsuarioDTO abogadoTemp : abogados) {

            //Se valida que el abogado este disponible
            //para asignacion al reporte
            if (abogadoTemp.getFechaExpiracion().after(new Date()) ||
                ELDAPEstadoUsuario.ACTIVO.codeExist(abogadoTemp.getEstado())) {

                abogado = abogadoTemp;
            }

        }
        if (abogado != null && abogado.getNombreCompleto() != null) {
            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(abogado.
                getNombreCompleto());

            if (firma != null && abogado.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {
                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());
                parameters.put("FIRMA_USUARIO", documentoFirma);
            }
            this.reporteAutoDePruebas = reportsService.generarReporte(parameters,
                enumeracionReporte, abogado);

            this.banderaBotonGuardarDocPrueba = true;
            this.banderaBotonEdicionDocPrueba = true;

            this.guardarDocumentoPruebas();

        }

        if (this.reporteAutoDePruebas != null) {

            this.banderaGeneraDocumento = true;
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método para guardar los documentos recibidos producto de las pruebas
     */
    /*
     * @modified by leidy.gonzalez:: #16501:: Se valida que valores con los que se va a realizar la
     * consulta del documento no sea nulo.
     */
    public void guardarRecibirPruebas() {

        Tramite tramiteResultado = null;

        if (this.documentosPruebasAEliminar != null && !this.documentosPruebasAEliminar.isEmpty() &&
            this.tramiteSeleccionado != null && this.usuario != null) {
            tramiteResultado = this.getTramiteService()
                .guardarActualizarTramiteDocumentos(this.tramiteSeleccionado,
                    this.documentosPruebasAEliminar, this.usuario);

            if (tramiteResultado == null) {
                String mensaje = "Ocurrio un error al procesar documentación de las pruebas," +
                    " por favor intente nuevamente";
                this.addMensajeError(mensaje);
            } else {
                String mensaje = "La documentación de las pruebas se guardo exitosamente";
                this.addMensajeInfo(mensaje);
            }

        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * código "migrado" para validar que los trámites nuevos tengan diferente orden asignado. Basado
     * en lo hecho por juen.agudelo para poder llamarlo desde otra clase
     *
     * @author pedro.garcia
     */
    public boolean validarOrdenTramitesNuevos() {

        boolean answer = true;
        Integer contadorTemporal;
        int validador;

        for (Tramite tTemp : this.tramiteTramites) {
            contadorTemporal = tTemp.getOrdenEjecucion();
            validador = 0;

            for (Tramite tTemp2 : this.tramiteTramites) {
                if (tTemp2.getOrdenEjecucion().equals(contadorTemporal)) {
                    validador++;
                }
            }

            if (validador > 1) {
                return false;
            }
        }
        return answer;

    }

    /**
     * @author fredy.wilches
     *
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarOficioNoProcedencia() {

        EReporteServiceSNC enumeracionReporte =
            EReporteServiceSNC.OFICIO_NO_PROCEDENCIA_AUTOAVALUO_REVISION_Y_VIA_GUBERNATIVA;

        //Para los tipos de trámites de mutaciones genera el reporte con un formato diferente 
        if (this.tramiteSeleccionado.isMutacion()) {
            enumeracionReporte = EReporteServiceSNC.OFICIO_NO_PROCEDENCIA_DE_MUTACIONES;
        }

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", "" + this.tramiteSeleccionado.getId());
        if (this.numeroRadicado != null) {
            parameters.put("NUMERO_RADICADO", this.numeroRadicado);
        }

        Object[] usuarioFirma = this.generalMB.buscarUsuarioFirmaOficionNoProcedencia(
            this.tramiteSeleccionado.getTipoTramite(), this.usuario);

        if ((String) usuarioFirma[2] == null) {
            this.addMensajeInfo("El usuario no tiene firma de usuario asociada en el sistema.");
        }

        parameters.put("NOMBRE_USUARIO", (String) usuarioFirma[0]);
        parameters.put("CARGO_USUARIO", (String) usuarioFirma[1]);
        parameters.put("FIRMA_USUARIO", (String) usuarioFirma[2]);

        this.reporteOficioNoProcedencia = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        this.banderaGenerarDocumento = true;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en Alfresco y
     * en la base de datos mediante el llamado al método guardar documento de la interfaz de
     * ITramite. Posteriormente le asocia el documento al trámite documento creado al principio.
     *
     * @author javier.aponte
     * @throws Exception
     */
    public void guardarDocumentoOficioNoProcedencia() throws Exception {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Documento documento = new Documento();

        tramiteDocumento.setTramite(this.tramiteSeleccionado);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();

            tipoDocumento.setId(ETipoDocumento.OFICIO_NO_PROCEDENCIA_VIA_GUBERNATIVA.getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.tramiteSeleccionado != null) {
                documento.
                    setArchivo(this.reporteOficioNoProcedencia.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(this.tramiteSeleccionado.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documento.setNumeroRadicacion(this.numeroRadicado);
            //v1.1.7
            //modificación 11/04/2014 se establece fecha de radicación
            documento.setFechaRadicacion(new Date());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.usuario, documento);

            if (documento != null &&
                documento.getIdRepositorioDocumentos() != null &&
                !documento.getIdRepositorioDocumentos()
                    .trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                tramiteDocumento = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumento);
            }

        } catch (Exception e) {
            throw new Exception(
                "Error al guardar el oficio de no procedencia en el gestor documental: " + e.
                    getMessage());
        }
    }

    /**
     * action del botón "Radicar"
     *
     * @copy by fredy.wilches
     */
    /*
     * @modified by juanfelipe.garcia :: 06-11-2013 :: adición de booleano para visualizar los
     * botones Cuando el trámite no procede
     */
    public void radicar() {

        this.banderaDocumentoRadicado = true;

        // Establece el trámite con el que se va a trabajar
        // this.radicaDocumentosMB.setSelectedTramite(this.tramite);
        // 1. Generación del número de Radicación en correspondencia
        // (sticker de radicación)
        this.numeroRadicado = this.obtenerNumeroRadicado();
        LOGGER.debug("Número Radicado: " + this.numeroRadicado);

        // 2. Generación del reporte con el número de radicación
        this.generarOficioNoProcedencia();

        // 3. Guardar el documento en el gestor documental
        try {
            this.guardarDocumentoOficioNoProcedencia();
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al guardar el oficio de no procedencia");
        }

        this.banderaRechazarTramite = true;
        this.habilitarBotonesNoProcedencia = false;
        this.tramiteSeleccionado
            .setEstado(ETramiteEstado.RECHAZADO.getCodigo());
        //Realizar cierre radicado ER
        if (this.tramiteSeleccionado != null && this.tramiteSeleccionado.getSolicitud() != null) {
            this.cerrarRadicacionTramite(this.tramiteSeleccionado
                .getSolicitud().getNumero(), numeroRadicado, rutaMostrar);
        }

        // return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private String obtenerNumeroRadicado() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(
            ETipoDocumento.OFICIO_NO_PROCEDENCIA_AUTOESTIMACION_Y_REVISION_DE_AVALUO.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("No procedencia solicitud"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getDireccion()); // Direccion destino
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.tramiteSeleccionado.getSolicitud().getSolicitanteSolicituds().get(0).
            getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    private void forwardThisProcess() {

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();

        }
        super.forwardProcess();

        // OJO: no se debe hacer estio aquí porque al dar click en "radicar" NO
        // se debe quitar el MB
        // de la sesión para permitir que se refresque el reporte y que quede
        // habilitado el botón "imprimir"
        // D: forzar el init cuando vuelva a seleccionar la actividad en el
        // árbol
        // UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");
        // D: forzar el refrezco del árbol de tareas
        // this.tareasPendientesMB.init();
    }

    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }

    @Implement
    @Override
    /*
     * @modified by juanfelipe.garcia :: 28-10-2013 :: se adiciono la territorial a la solicitud
     * catastral
     */
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;
        String mensaje;

        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();

        usuariosActividad = new ArrayList<UsuarioDTO>();
        messageDTO = new ActivityMessageDTO();

        messageDTO.setActivityId(this.actividad.getId());

        processTransition = ProcesoDeConservacion.ACT_EJECUCION_TERMINAR_EJECUCION_TRAMITE;
        observaciones = "El trámite no procede";

        //se cambió al usuario de la sesión, para que quede el registro en process 
        //de quien fue el responsable de terminar la ejecución del tramite
        usuarioDestinoActividad.setLogin(this.usuario.getLogin());

        usuariosActividad.add(usuarioDestinoActividad);

        messageDTO.setComment(observaciones);
        // D: según alejandro.sanchez la transición que importa es la que se
        // define en la
        // solicitud catastral; sin embargo, por si acaso se hace también en el
        // messageDTO
        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        solicitudCatastral.setUsuarios(usuariosActividad);
        messageDTO.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(messageDTO);

        //Si el trámite rechazado es de vía gubernativa se reanuda la ultima actividad
        //del trámite referencia.
        if (tramiteSeleccionado.isViaGubernativa()) {

            Tramite tramiteReferencia = this.getTramiteService().
                buscarTramitePorNumeroRadicacion(this.tramiteSeleccionado.
                    getNumeroRadicacionReferencia());

            try {
                if (tramiteReferencia.getEstado().equals(ETramiteEstado.RECIBIDO.getCodigo())) {
                    List<Actividad> actividades = this.getProcesosService().
                        getActividadesPorIdObjetoNegocio(tramiteReferencia.getId());
                    Actividad act = actividades.get(0);

                    //Se reanuda la actividad del trámite referencia del vía gubernativa
                    this.getProcesosService().reanudarActividad(act.getId());

                    //Se envia el trámite referencia a aplicar cambios
                    UsuarioDTO responsableConservacion;
                    responsableConservacion = this.getTramiteService().
                        buscarFuncionariosPorRolYTerritorial(
                            usuario.getDescripcionEstructuraOrganizacional(),
                            ERol.RESPONSABLE_CONSERVACION).get(0);

                    solicitudCatastral.setTransicion(
                        ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS);
                    getProcesosService().avanzarActividad(responsableConservacion, act.getId(),
                        solicitudCatastral);
                }
            } catch (Exception ex) {
                mensaje = ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(tramiteReferencia.
                    getNumeroRadicacion());
                this.addMensajeWarn(mensaje);
                LOGGER.error(ECodigoErrorVista.ACT_RECLAMADA.getMensajeTecnico());
            }

        }
    }
//--------------------------------------------------------------------------------------------------

    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        if (this.banderaRechazarTramite == true) {
            if (this.tramiteSeleccionado.getTramitePruebas() != null &&
                this.tramiteSeleccionado.getTramitePruebas().getId() == null) {
                this.tramiteSeleccionado.setTramitePruebas(null);
            }
            // Guardar el funcionario ejecutor que avanza el proceso,
            // para recuperarlo en la actividad "Comunicar auto interesado"
            this.tramiteSeleccionado.setFuncionarioEjecutor(this.usuario.getLogin());
            this.tramiteSeleccionado.setNombreFuncionarioEjecutor(this.usuario
                .getNombreCompleto());
            this.getTramiteService().actualizarTramite(
                this.tramiteSeleccionado, this.usuario);
            this.banderaRechazarTramite = false;
        }
    }

    public String cerrarPaginaPrincipal() {

        // D: forzar el init cuando vuelva a seleccionar la actividad en el
        // árbol
        UtilidadesWeb.removerManagedBean("ejecucion");
        UtilidadesWeb.removerManagedBean("combosDeptosMunis");
        // D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    public void guardarMotivoRechazoTramite() {

        LOGGER.debug("Establecer Procedencia de Solicitud MB - Guardar Motivo No procedencia");

        if (this.estadoTramite.getMotivo().length() > Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO) {
            this.addMensajeError("El tamaño del texto del motivo no debe ser superior a " +
                Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO + " caracteres");
            return;
        }

        this.estadoTramite.setFechaInicio(new Date(System.currentTimeMillis()));
        this.estadoTramite.setTramite(this.tramiteSeleccionado);
        this.estadoTramite.setUsuarioLog(this.usuario.getLogin());
        this.estadoTramite.setFechaLog(new Date());
        this.estadoTramite.setEstado(ETramiteEstado.RECHAZADO.getCodigo());
        try {
            TramiteEstado auxEstadoTramite = this.getTramiteService()
                .actualizarTramiteEstado(this.estadoTramite, this.usuario);
            if (auxEstadoTramite != null) {
                this.estadoTramite = auxEstadoTramite;
            }
            this.addMensajeInfo("Información almacenada de forma exitosa.");
        } catch (Exception e) {
            LOGGER.error("Error En Guardar Motivo No procedencia");
        }
    }
//--------------------------------------------------------------------------------------------------

    public String cerrar() {
        TareasPendientesMB tp;
        tp = (TareasPendientesMB) UtilidadesWeb.getManagedBean("tareasPendientes");
        tp.cerrar();
        //@modified by leidy.gonzalez::#59843::23/04/2018
        //Se cierra la sesion del MB para que permita el ingreso
        this.cerrarYContinuarEnLamismaTarea();
        UtilidadesWeb.removerManagedBean("tareasPendientes");
        return ConstantesNavegacionWeb.INDEX;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los documentos asociados a la entidad seleccionada
     */
    /*
     * @modified by leidy.gonzalez:: #16501:: Se valida que valores con los que se va a realizar la
     * consulta del documento no sea nulo.
     */
    public void cargarDocumentosDeEntidadPruebas() {

        if (this.tramiteSeleccionado != null &&
            this.tramiteSeleccionado.getId() != null &&
            this.selectedEntidadPruebas != null &&
            this.selectedEntidadPruebas.getNumeroIdentificacion() != null) {

            this.documentosSelectedEntidad = this.getTramiteService()
                .obtenerDocumentosPruebasDeEntidad(
                    this.tramiteSeleccionado.getId(),
                    this.selectedEntidadPruebas
                        .getNumeroIdentificacion());

            if (this.documentosSelectedEntidad != null &&
                !this.documentosSelectedEntidad.isEmpty()) {
                for (TramiteDocumento td : this.documentosSelectedEntidad) {

                    if (!td.getDocumento().getTipoDocumento().getId().equals(
                        ETipoDocumento.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId())) {
                        this.selectedEntidadPruebas = new TramitePruebaEntidad();
                    }
                }
            }
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton cerrar en revisar trámite asignado que cierra y conserva la
     * actividad en el arbol de tareas
     *
     * @author juan.agudelo
     */
    public String cerrarYContinuarEnLamismaTarea() {
        UtilidadesWeb.removerManagedBean("ejecucion");
        return "index";
    }

    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.selectedTramiteDocumentacion.getDocumentoSoporte().getArchivo());
    }

    /**
     * Consultar datos a rectificar por tipo de trámite
     *
     * @author javier.aponte
     */
    public void consultarDatosARectificarPorTramiteSeleccionado() {

        if (this.selectedDatosRectifComplDataTable != null &&
            !this.selectedDatosRectifComplDataTable.isEmpty()) {
            this.selectedDatosRectifComplDataTable.clear();
        }
        if (this.tramiteTramiteSeleccionado.getTramiteRectificacions() != null &&
            !this.tramiteTramiteSeleccionado.getTramiteRectificacions().isEmpty()) {
            for (TramiteRectificacion tr : this.tramiteTramiteSeleccionado.
                getTramiteRectificacions()) {
                if (tr.getDatoRectificar() != null) {
                    this.selectedDatosRectifComplDataTable.add(tr.getDatoRectificar());
                }
            }
        }

    }

    // -------------------------------------------------------------------------------------------------
    public void cleanRectificacionVariables() {
        this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        this.datosRectifComplSource = new ArrayList<DatoRectificar>();
        this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
        this.tipoDatoRectifComplSeleccionado = null;
    }
    // end of class

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la selección de predios en mutaciones de segunda englobe para buscar
     * la lista de predios colindantes
     */
    public void buscarPrediosColindantes() {

        if (this.banderaSubtipoEnglobe) {
            this.listaPrediosColindantes = this.getConservacionService()
                .buscarPrediosColindantesPorNumeroPredial(
                    this.selectedPredioTramite.getPredio()
                        .getNumeroPredial());
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la deselección de predios en mutaciones de segunda englobe para
     * inicializar la lista de predios colindantes y selección
     */
    public void reiniciarPrediosColindantes() {
        this.listaPrediosColindantes = null;
    }

    // -------------------------------------------------------------------------
    /**
     * Método utilizado sobre la selección de predio colindante para adicionarlo a la lista de
     * predios seleccionados del trámite
     */
    public void adicionarPrediosColindantes() {
        List<Predio> predios = this.getPrediosSeleccionadosMapa();

        if (this.prediosTramiteTemp == null ||
            this.prediosTramiteTemp.isEmpty()) {
            // this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
            this.prediosTramiteTemp = new ArrayList<TramitePredioEnglobeDTO>();
        }
        predios.add(this.selectedPredioColindante);
        //se valida que no tenga predios repetidos
        HashSet hs = new HashSet();
        hs.addAll(predios);
        predios.clear();
        predios.addAll(hs);

        for (Predio p : predios) {
            TramitePredioEnglobeDTO tpe = new TramitePredioEnglobeDTO();
            tpe.setPredio(p);
            this.prediosTramiteTemp.add(tpe);
        }

        if (this.tramiteTramiteSeleccionado != null) {
            if (predios != null && !predios.isEmpty()) {
                if (predios.size() > 1) {
                    List<TramitePredioEnglobe> prediosTramiteList =
                        new ArrayList<TramitePredioEnglobe>();
                    for (TramitePredioEnglobeDTO dto : this.prediosTramiteTemp) {
                        TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                        tpe.setEnglobePrincipal(dto.getEnglobePrincipal());
                        tpe.setFechaLog(dto.getFechaLog());
                        tpe.setId(dto.getId());
                        tpe.setPredio(dto.getPredio());
                        tpe.setTramite(dto.getTramite());
                        tpe.setUsuarioLog(dto.getUsuarioLog());
                        prediosTramiteList.add(tpe);
                    }
                    this.tramiteTramiteSeleccionado
                        .setTramitePredioEnglobes(prediosTramiteList);
                    this.tramiteTramiteSeleccionado.setPredio(predios.get(0));
                }

            }
        }
        this.listaPrediosColindantes.remove(this.selectedPredioColindante);
    }

    // ---------------------------------------------------------------- //
    /**
     * Método que realiza el cargue los motivos de devolución del trámite en un
     * {@link DualListModel} de {@link String}
     *
     * @return
     * @author david.cifuentes
     */
    public void cargarMotivosDevolucionTramites() {
        List<String> motivosDevolucion = new ArrayList<String>();

        for (MotivoEstadoTramite met : this.getTramiteService()
            .getCacheMotivoEstadoTramites()) {
            if (met.getEstado().equals(ETramiteEstado.DEVUELTO.getCodigo())) {
                motivosDevolucion.add(new String(met.getMotivo()));
            }
        }
        this.motivosDevolucionTramites = new DualListModel<String>(
            motivosDevolucion, new ArrayList<String>());
    }

    // ---------------------------------------------------------------- //
    /**
     * Método que realiza el cargue las inconsistencias y razones para enviar el trámite a
     * depuración geográfica en caso de que existan. El cargue de las inconsistencias se realiza a
     * partir del {@link Tramite}.
     *
     * @return
     * @author david.cifuentes
     */
    public void cargarDatosDepuracionDelTramite() {
        this.inconsistenciasGeograficas = "";
        this.razonezDepuracionGeografica = "";
        if (this.tramiteSeleccionado.getTramiteDepuracions() != null &&
            !this.tramiteSeleccionado.getTramiteDepuracions().isEmpty()) {
            // Realizar la búsqueda del registro más actual.
            if (this.tramiteSeleccionado.getTramiteDepuracions().size() == 1) {
                this.inconsistenciasGeograficas = this.tramiteSeleccionado
                    .getTramiteDepuracions().get(0).getInconsistencias();
                this.razonezDepuracionGeografica = this.tramiteSeleccionado
                    .getTramiteDepuracions().get(0)
                    .getRazonesSolicitudDepuracion();
            } else {
                Date fechaMayor = this.tramiteSeleccionado
                    .getTramiteDepuracions().get(0).getFechaLog();
                TramiteDepuracion tdUltimo = null;
                for (TramiteDepuracion td : this.tramiteSeleccionado
                    .getTramiteDepuracions()) {
                    if (td.getFechaLog().compareTo(fechaMayor) >= 0) {
                        // El registro más actual ahora es el de td.
                        fechaMayor = td.getFechaLog();
                        tdUltimo = td;
                    }
                }
                if (tdUltimo != null) {
                    this.inconsistenciasGeograficas = tdUltimo
                        .getInconsistencias();
                    this.razonezDepuracionGeografica = tdUltimo
                        .getRazonesSolicitudDepuracion();
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * envía correo al responsable de conservación para informarle que debe retirar el trámite, que
     * acaba de ser movido a la actividad de 'estudiar ajuste espacial requerido', de las comisiones
     * activas a las que esté asociado.
     *
     * @author pedro.garcia
     * @param numeroRadicacionTramite número de radicación del trámite
     * @param numeroComision número de comisión de las comisión en la que está el trámite
     * @return true si todo salió bien
     */
    private boolean enviarCorreoRetiroTramitePorEnvioDepuracion(
        String numeroRadicacionTramite, String numeroComision) {

        boolean answer = false;
        UsuarioDTO responsableConservacion;
        String destinatarioCorreo, contenidoCorreo;
        Locale colombia = new Locale("ES", "es_CO");

        ArrayList<UsuarioDTO> funcionariosResponsablesConservacion;

        funcionariosResponsablesConservacion = (ArrayList<UsuarioDTO>) this.getTramiteService().
            buscarFuncionariosPorRolYTerritorial(this.usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_CONSERVACION);

        if (funcionariosResponsablesConservacion != null &&
            !funcionariosResponsablesConservacion.isEmpty()) {
            responsableConservacion = funcionariosResponsablesConservacion.get(0);
            destinatarioCorreo = responsableConservacion.getEmail();
        } else {
            return answer;
        }

        Object parametrosContenidoCorreo[] = new Object[2];
        parametrosContenidoCorreo[0] = numeroRadicacionTramite;
        parametrosContenidoCorreo[1] = numeroComision;

        contenidoCorreo = new MessageFormat(
            ConstantesComunicacionesCorreoElectronico.CONTENIDO_RETIROTRAMITE_COMISION_POR_ENVIO_DEPURACION,
            colombia).format(parametrosContenidoCorreo);

        answer = this.getGeneralesService().enviarCorreo(
            destinatarioCorreo,
            ConstantesComunicacionesCorreoElectronico.ASUNTO_RETIROTRAMITE_COMISION_POR_ENVIO_DEPURACION,
            contenidoCorreo, null, null);

        return answer;

    }

    /**
     * Genera la replica de consulta cuando se requiere por parte del usuario.
     *
     * @author felipe.cadena
     */
    public String generarReplicaConsulta() {
        //se transfiere el tramite a tiempos muertos y se inicia la generacion de la replica
//          this.getConservacionService().generarReplicaDeConsultaTramiteYAvanzarProceso(
//                    this.actividad,
//                    this.usuario,
//                    this.usuario, 
//                    false);

        Tramite tramite = this.getTramiteService().buscarTramitePorId(this.tramiteSeleccionadoId);

        //felipe.cadena::ACCU::Se cambia la forma de generar la replica de consulta
        RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
            getManagedBean("recuperarTramites");
        recuperarTamitesMB.generarReplicaConsulta(tramite, this.usuario, null, 1);

        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";
    }

    /**
     * Método para avanzar el proceso para los trámites de no procedencia
     *
     * @author javier.aponte
     */
    public String avanzarProcesoNoProcede() {
        this.forwardThisProcess();

        return this.cerrarPaginaPrincipal();
    }

    public void editarDocumentoPruebas() {

    }

    private boolean tienePrediosBloqueados() {
        if (this.tramiteTramiteSeleccionado.getPredio() != null) {
            Predio predio = this.tramiteTramiteSeleccionado.getPredio();
            List<PredioBloqueo> predioBloqueos = this.getConservacionService().
                obtenerPredioBloqueosPorNumeroPredial(predio.getNumeroPredial());
            predio.setPredioBloqueos(predioBloqueos);
            return predio.isEstaBloqueado();
        }
        return false;
    }

    private boolean cerrarRadicacionTramite(final String radicado,
        final String radicadoResponde, final String usuario) {
        boolean resultado = true;
        final Object[] resultadoCierre = this.getTramiteService()
            .finalizarRadicacion(radicado, radicadoResponde, usuario);
        if (resultadoCierre != null) {
            for (final Object objeto : resultadoCierre) {
                final List lista = (List) objeto;
                if (lista != null && !lista.isEmpty()) {
                    int numeroErrores = 0;
                    for (final Object obj : lista) {
                        final Object[] arrObjetos = (Object[]) obj;
                        //TODO: Eliminar la validación del código de error de oracle luego de que 
                        //el servicio de CORDIS solucione el error: http://redmine.igac.gov.co/issues/21662
                        if (!arrObjetos[0].equals("0") &&
                            !arrObjetos[1].toString().contains("0-ORA-0000")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + arrObjetos[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al cerrar radicación");
                        resultado = false;
                    }
                }
            }
        }
        return resultado;
    }

    /**
     * Si el tramite tiene relacionado un predio ficha matriz, se agregan todos los predios
     * relacionados a esta ficha.
     *
     * @author felipe.cadena
     */
    public void agregarPrediosFM() {

        List<TramitePredioEnglobe> prediosAsociadosFM = new ArrayList<TramitePredioEnglobe>();
        if (EMutacion2Subtipo.ENGLOBE.getCodigo().equals(this.tramiteTramiteSeleccionado.
            getSubtipo())) {

            for (TramitePredioEnglobe t : this.prediosTramite) {
                if (t.getPredio().getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END) ||
                    t.getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END)) {

                    //se marca como englobe ph
                    this.tramiteTramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.ENGLOBE_PH.getCodigo());

                    List<Predio> prediosFM = this.getConservacionService().
                        buscarPrediosPorRaizNumPredial(t.getPredio().getNumeroPredial().substring(0,
                            21));
                    for (Predio predio : prediosFM) {

                        //@modified by leidy.gonzalez::#20927:: Se valida que no se duplique el predio ficha matriz,
                        //que se agrega a la lista de predios que se engloban.
                        if (t.getPredio().getId() != null && predio.getId() != null &&
                            !t.getPredio().getId().equals(predio.getId())) {

                            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                            tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                            tpe.setPredio(predio);
                            tpe.setTramite(this.tramiteTramiteSeleccionado);
                            tpe.setFechaLog(new Date());
                            tpe.setUsuarioLog(this.usuario.getLogin());
                            prediosAsociadosFM.add(tpe);

                        }
                    }
                }
            }
        } else {
            if (this.tramiteTramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                Constantes.FM_CONDOMINIO_END) ||
                this.tramiteTramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                    Constantes.FM_PH_END)) {

                if (this.tramiteTramiteSeleccionado.isSegundaDesenglobe()) {
                    //se marca como desenglobe por etapas
                    this.tramiteTramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.POR_ETAPAS.getCodigo());
                } else if (this.tramiteTramiteSeleccionado.isRectificacion()) {
                    //se marca como rectificacion matriz
                    this.tramiteTramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.RECTIFICACION_MATRIZ.getCodigo());
                }

                if (this.tramiteTramiteSeleccionado.isTercera()) {
                    //se marca como tercera PH Condominio Másiva
                    this.tramiteTramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.TERCERA_MASIVA.getCodigo());
                }

                if (this.tramiteTramiteSeleccionado.isQuinta()) {
                    //se marca como tercera PH Condominio Másiva
                    this.tramiteTramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.QUINTA_MASIVO.getCodigo());
                }
                //@modified by leidy.gonzalez :: #20420:: Agrega los predios FM, para los tramites de Cancelacion Masiva
                if (this.tramiteTramiteSeleccionado.isCancelacionPredio()) {
                    //se marca como cancelacion PH Condominio Másiva
                    this.tramiteTramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.CANCELACION_MASIVA.getCodigo());
                }

                List<Predio> prediosFM = this.getConservacionService().
                    buscarPrediosPorRaizNumPredial(this.tramiteTramiteSeleccionado.getPredio().
                        getNumeroPredial().substring(0, 22));
                for (Predio predio : prediosFM) {
                    //No se agregan los predios de mejoras o los que estan cancelados.
                    if (predio.isMejora() || EPredioEstado.CANCELADO.getCodigo().equals(predio.
                        getEstado())) {
                        continue;
                    }
                    TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                    tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                    tpe.setPredio(predio);
                    tpe.setTramite(this.tramiteTramiteSeleccionado);
                    tpe.setFechaLog(new Date());
                    tpe.setUsuarioLog(this.usuario.getLogin());
                    prediosAsociadosFM.add(tpe);

                }
            }
        }

        if (this.tramiteTramiteSeleccionado.getTramitePredioEnglobes() == null) {
            this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(prediosAsociadosFM);
        } else {
            this.tramiteTramiteSeleccionado.getTramitePredioEnglobes().addAll(prediosAsociadosFM);
        }
    }

//end of class    
}
