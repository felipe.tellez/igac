package co.gov.igac.snc.web.mb.tramite.gestion;

/**
 * @author david.cifuentes
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;
import co.gov.igac.snc.persistence.entity.formacion.ParametroDecretoCondicion;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.util.CalculoPrediosAfectadosPorDecretoDTO;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("decretoIncrementoAnual")
@Scope("session")
public class RegistrarDecretoDeIncrementoAnualMB extends SNCManagedBean {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistrarDecretoDeIncrementoAnualMB.class);

    // ------ Servicios ------//
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    // ------ Variables ------//
    /**
     * Variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena el Decreto seleccionado
     */
    private DecretoAvaluoAnual decretoSeleccionado;

    /**
     * Lista completa de los Decretos
     */
    private List<DecretoAvaluoAnual> decretosExistentes;

    /**
     * Variable booleana para saber si se está editando un decreto o si se está adicionando uno
     * nuevo
     */
    private boolean editModeBool;

    /**
     * Lista de items de parametros para armar la condicional de un decreto.
     */
    private List<SelectItem> parametrosDecretoCondicionItems;

    /**
     * Variable que almacena el parametro seleccionado.
     */
    private String parametroSeleccionado;

    /**
     * Variable usada para guardar el caracter con el que se arma la condicional.
     */
    private String caracter;

    /**
     * Variable que almacena el valor del parametro que se va a usar como componente lógico para
     * crear una condicion.
     */
    private String valorParametro;

    /**
     * Lista para almacenar los valores del parametro que se seleccionó.
     */
    private List<SelectItem> itemListValorParametros = new ArrayList<SelectItem>();

    /**
     * Lista de los condicionales de un decreto seleccionado.
     */
    private DecretoCondicion condicionalSeleccionada;

    /**
     * Variable para controlar que la misma cantidad de parentesis que abre, tambien cierre.
     */
    private int parentesis;

    /**
     * Lista de parametros decreto condicion.
     */
    private List<ParametroDecretoCondicion> parametrosDecretoCondicion;

    /**
     * Variable usada para llevar el total de los parametros ingresados - los operadores.
     */
    private int totalParentesis;

    /**
     * Variables para desactivar y activar los campos de parametros y signos para controlar la
     * validez de una expresión.
     */
    private boolean parametroBool;
    private boolean signoBool;

    /**
     * Variables para mostrar los campos de edición del decreto y de las condiciones.
     */
    private boolean mostrarDatosCondiciones;
    private boolean mostrarDatosDecreto;

    /**
     * Lista de objetos que almacena los datos de los predios afectados de un decreto.
     */
    private List<CalculoPrediosAfectadosPorDecretoDTO> listPrediosAfectados;

    /**
     * Totalización de predios
     */
    private Long totalAfectadosDecreto;
    private Long totalPrediosNacionales;

    // Vigencias válidas en años
    private Integer vigenciaMin;
    private Integer vigenciaMax;

    /**
     * Variable usada para saber si se quiere guardar una condicional nacional.
     */
    private boolean condicionalNacionalBool;

    /**
     * Variable que almacena una constante de condicional verdadera
     */
    private String condicionalVerdadera;

    /**
     * Variable que almacena un string a mostrar en la tabla de condicinales cuando existe la
     * condicional verdadera.
     */
    private String condicionalNacional;

    /**
     * Almacena el año de la vigencia de un decreto en la pantalla de edición
     */
    private Integer anioVigencia;

    // ------------------ GETTERS Y SETTERS -------------------- //
    public String getCondicionalNacional() {
        return condicionalNacional;
    }

    public void setCondicionalNacional(String condicionalNacional) {
        this.condicionalNacional = condicionalNacional;
    }

    public String getCondicionalVerdadera() {
        return condicionalVerdadera;
    }

    public boolean isCondicionalNacionalBool() {
        return condicionalNacionalBool;
    }

    public void setCondicionalNacionalBool(boolean condicionalNacionalBool) {
        this.condicionalNacionalBool = condicionalNacionalBool;
    }

    public int getParentesis() {
        return parentesis;
    }

    public void setParentesis(int parentesis) {
        this.parentesis = parentesis;
    }

    public DecretoAvaluoAnual getDecretoSeleccionado() {
        return decretoSeleccionado;
    }

    public void setDecretoSeleccionado(DecretoAvaluoAnual decretoSeleccionado) {
        this.decretoSeleccionado = decretoSeleccionado;
    }

    public List<DecretoAvaluoAnual> getDecretosExistentes() {
        return decretosExistentes;
    }

    public void setDecretosExistentes(
        List<DecretoAvaluoAnual> decretosExistentes) {
        this.decretosExistentes = decretosExistentes;
    }

    public boolean isEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public Integer getVigenciaMin() {
        return vigenciaMin;
    }

    public void setVigenciaMin(Integer vigenciaMin) {
        this.vigenciaMin = vigenciaMin;
    }

    public Integer getVigenciaMax() {
        return vigenciaMax;
    }

    public void setVigenciaMax(Integer vigenciaMax) {
        this.vigenciaMax = vigenciaMax;
    }

    public Integer getAnioVigencia() {
        return anioVigencia;
    }

    public void setAnioVigencia(Integer anioVigencia) {
        this.anioVigencia = anioVigencia;
    }

    public List<SelectItem> getParametrosDecretoCondicionItems() {
        return parametrosDecretoCondicionItems;
    }

    public void setParametrosDecretoCondicionItems(
        List<SelectItem> parametrosDecretoCondicionItems) {
        this.parametrosDecretoCondicionItems = parametrosDecretoCondicionItems;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    public List<SelectItem> getItemListValorParametros() {
        return itemListValorParametros;
    }

    public void setItemListValorParametros(
        List<SelectItem> itemListValorParametros) {
        this.itemListValorParametros = itemListValorParametros;
    }

    public String getValorParametro() {
        return valorParametro;
    }

    public void setValorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
    }

    public String getParametroSeleccionado() {
        return parametroSeleccionado;
    }

    public void setParametroSeleccionado(String parametroSeleccionado) {
        this.parametroSeleccionado = parametroSeleccionado;
    }

    public Long getTotalAfectadosDecreto() {
        return totalAfectadosDecreto;
    }

    public void setTotalAfectadosDecreto(Long totalAfectadosDecreto) {
        this.totalAfectadosDecreto = totalAfectadosDecreto;
    }

    public Long getTotalPrediosNacionales() {
        return totalPrediosNacionales;
    }

    public void setTotalPrediosNacionales(Long totalPrediosNacionales) {
        this.totalPrediosNacionales = totalPrediosNacionales;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public DecretoCondicion getCondicionalSeleccionada() {
        return condicionalSeleccionada;
    }

    public void setCondicionalSeleccionada(
        DecretoCondicion condicionalSeleccionada) {
        this.condicionalSeleccionada = condicionalSeleccionada;
    }

    public int getTotalParentesis() {
        return totalParentesis;
    }

    public void setTotalParentesis(int totalParentesis) {
        this.totalParentesis = totalParentesis;
    }

    public List<ParametroDecretoCondicion> getParametrosDecretoCondicion() {
        return parametrosDecretoCondicion;
    }

    public void setParametrosDecretoCondicion(
        List<ParametroDecretoCondicion> parametrosDecretoCondicion) {
        this.parametrosDecretoCondicion = parametrosDecretoCondicion;
    }

    public boolean isParametroBool() {
        return parametroBool;
    }

    public void setParametroBool(boolean parametroBool) {
        this.parametroBool = parametroBool;
    }

    public boolean isSignoBool() {
        return signoBool;
    }

    public void setSignoBool(boolean signoBool) {
        this.signoBool = signoBool;
    }

    public boolean isMostrarDatosCondiciones() {
        return mostrarDatosCondiciones;
    }

    public void setMostrarDatosCondiciones(boolean mostrarDatosCondiciones) {
        this.mostrarDatosCondiciones = mostrarDatosCondiciones;
    }

    public void setListPrediosAfectados(
        List<CalculoPrediosAfectadosPorDecretoDTO> listPrediosAfectados) {
        this.listPrediosAfectados = listPrediosAfectados;
    }

    public List<CalculoPrediosAfectadosPorDecretoDTO> getListPrediosAfectados() {
        return listPrediosAfectados;
    }

    public boolean isMostrarDatosDecreto() {
        return mostrarDatosDecreto;
    }

    public void setMostrarDatosDecreto(boolean mostrarDatosDecreto) {
        this.mostrarDatosDecreto = mostrarDatosDecreto;
    }

    // ------------------ M É T O D O S -------------------- //
    @PostConstruct
    public void init() {
        // usuario
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // Cargar decretos existentes
        this.decretosExistentes = this.getFormacionService().buscarDecretos();

        // Cargar parametros
        this.parametrosDecretoCondicion = this.getFormacionService()
            .consultarParametrosDecretoCondicion();
        this.parametrosDecretoCondicionItems = new ArrayList<SelectItem>();

        if (!this.parametrosDecretoCondicion.isEmpty()) {
            SelectItem item;
            for (ParametroDecretoCondicion p : this.parametrosDecretoCondicion) {
                if (p != null) {
                    item = new SelectItem();
                    item.setValue(p.getDominio());
                    item.setLabel(p.getNombre());
                    this.parametrosDecretoCondicionItems.add(item);
                }
            }
        }
        reiniciarVariables();

        // Se define el intervalo de vigencia para un decreto nuevo
        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();
        min.add(Calendar.YEAR, -50);
        max.add(Calendar.YEAR, 100);
        this.vigenciaMin = new Integer(min.get(Calendar.YEAR));
        this.vigenciaMax = new Integer(max.get(Calendar.YEAR));

        this.mostrarDatosCondiciones = false;
        this.mostrarDatosDecreto = false;
        this.condicionalVerdadera = Constantes.CONDICIONAL_VERDADERA;
        this.condicionalNacional = Constantes.CONDICIONAL_NACIONAL;
    }

    /**
     * Método que carga la lista de valores para un parametro seleccionado
     */
    public void cargarValorParametro() {

        String aux = new String();

        // Busqueda de la enumeración del parametro seleccionado
        for (EDominio dominio : EDominio.values()) {
            aux = dominio.toString();
            if (aux.equals(this.parametroSeleccionado)) {
                aux = dominio.name();
                break;
            }
        }

        this.itemListValorParametros = new ArrayList<SelectItem>();

        if (this.parametroSeleccionado != null) {
            List<Dominio> listaDominio = this.getGeneralesService()
                .buscarDominioPorNombreOrderByNombre(EDominio.valueOf(aux));
            if (!listaDominio.isEmpty()) {
                SelectItem valorParametro;
                for (Dominio dom : listaDominio) {
                    valorParametro = new SelectItem();
                    valorParametro.setValue(dom.getCodigo());
                    valorParametro.setLabel(dom.getValor());
                    this.itemListValorParametros.add(valorParametro);
                }
            }
        }

        // Concatenar parametro a la expresión
        String columnaTabla = new String("");
        if (!this.parametrosDecretoCondicion.isEmpty()) {

            for (ParametroDecretoCondicion parametro : this.parametrosDecretoCondicion) {
                if (parametro.getDominio().equals(this.parametroSeleccionado)) {
                    columnaTabla = parametro.getTabla().concat(".")
                        .concat(parametro.getColumna());
                    break;
                }
            }
            this.caracter = columnaTabla;
            this.condicionalSeleccionada
                .setCondicion(this.condicionalSeleccionada.getCondicion()
                    .concat(" " + this.caracter));
            // this.totalCaracteres += 1;
        }

        // Activar y desactivar campos en la interfaz.
        this.signoBool = true;
        this.parametroBool = false;
    }

    /**
     * Método que elimina una condicion seleccionada.
     */
    public void eliminarCondicion() {
        try {
            this.decretoSeleccionado.getDecretoCondicions().remove(
                this.condicionalSeleccionada);
            this.getFormacionService().eliminarCondicional(
                this.condicionalSeleccionada);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error eliminando la condicional del decreto.");

        }
    }

    /**
     * Método que adiciona un caracter seleccionado a la condición que se esta creando.
     */
    public void bajarCaracter() {
        this.condicionalSeleccionada.setCondicion(this.condicionalSeleccionada
            .getCondicion().concat(" " + this.caracter));
        // this.totalCaracteres -= 1;
        this.totalParentesis += this.parentesis;

        // Activar y desactivar campos en la interfaz.
        this.parametroBool = true;
        this.signoBool = false;
        this.parentesis = 0;
    }

    /**
     * Método que adiciona un parametro seleccionado a la condición que se esta creando.
     */
    public void bajarParametro() {
        this.condicionalSeleccionada.setCondicion(this.condicionalSeleccionada
            .getCondicion().concat(" '" + this.caracter + "'"));

        // Activar y desactivar campos en la interfaz.
        this.signoBool = true;
        this.parametroBool = false;
    }

    /**
     * Método que borra el último caracter añadido a la condición.
     */
    public void limpiar() {

        if (condicionalSeleccionada != null &&
             condicionalSeleccionada.getCondicion() != null) {
            String aux;
            if (this.condicionalSeleccionada.getCondicion().length() > 1) {

                aux = this.condicionalSeleccionada.getCondicion();
                this.condicionalSeleccionada.setCondicion(aux
                    .substring(0, this.condicionalSeleccionada
                        .getCondicion().length() - 1));
            }

            // Activar y desactivar campos en la interfaz.
            this.signoBool = true;
            this.parametroBool = true;
        }
    }

    /**
     * Método que guarda un decreto con sus condicionales.
     */
    public void guardarDecreto() {

        try {

            if (validarGuardarDecreto()) {

                Calendar calendar = Calendar.getInstance();
                calendar.clear();
                calendar.set(Calendar.DATE, 1);
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.YEAR, this.anioVigencia);
                Date vigencia = calendar.getTime();

                this.decretoSeleccionado.setVigencia(vigencia);
                this.decretoSeleccionado.setFechaLog(new Date(System
                    .currentTimeMillis()));
                this.decretoSeleccionado.setUsuarioLog(this.usuario.getLogin());
                this.decretoSeleccionado = this.getFormacionService()
                    .guardarDecreto(this.decretoSeleccionado);
                this.addMensajeInfo("Se guardó el decreto satisfactoriamente!");
                if (!this.editModeBool) {
                    this.decretosExistentes.add(this.decretoSeleccionado);
                }
                this.editModeBool = true;
                reiniciarVariables();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error guardando el decreto");
        }
    }

    /**
     * Método que valida que los valores ingresados para el decreto a guardar sean validos.
     */
    public boolean validarGuardarDecreto() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        if (this.decretoSeleccionado != null) {
            if (!editModeBool) {
                // Validación del nombre
                if (this.decretoSeleccionado.getDecretoNumero() == null) {
                    this.addMensajeError("Debe ingresar un valor para el número del decreto.");
                    return false;
                }

                // Validación de la vigencia
                if (this.anioVigencia == null) {
                    this.addMensajeError("La vigencia del decreto no puede estar vacia.");
                    return false;
                }

                // Validación del valor de la vigencia
                if (this.anioVigencia != null) {
                    if (this.anioVigencia.intValue() > this.vigenciaMax.intValue() ||
                         this.anioVigencia.intValue() < this.vigenciaMin.intValue()) {

                        this.addMensajeError("La vigencia debe ser un año válido entre " +
                             vigenciaMin +
                             " y " +
                             vigenciaMax + ".");
                        return false;
                    }
                }
                
//                Calendar anioDecreto = Calendar.getInstance();
//                anioDecreto.setTime(this.decretoSeleccionado.getDecretoFecha());
//                
//                //Validación el año de vigencia mayor un año a la fecha de decreto
//                if (this.anioVigencia 
//                        != (anioDecreto.get(Calendar.YEAR) + 1)) {
//                    this.addMensajeError("El año de la vigencia debe ser un año superior " +
//                            "al año del decreto");
//                    return false;
//                }

                // Validación de repetición de decreto.
                for (DecretoAvaluoAnual d : this.decretosExistentes) {
                    if (this.anioVigencia.intValue() == d.getAnioVigencia().intValue()) {
                        this.addMensajeError(
                            "Ya existe un decreto con la misma vigencia. Por favor revise la información.");
                        return false;
                    }
                    if (this.decretoSeleccionado.getDecretoNumero().equals(
                        d.getDecretoNumero())) {
                        this.addMensajeError(
                            "Ya existe un decreto con el mismo número. Por favor revise la información.");
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Método que verifica si una condicional es nacional
     */
    public void setupCondicional() {
        if (this.condicionalSeleccionada != null) {
            if (this.condicionalSeleccionada.getCondicion().equals(
                Constantes.CONDICIONAL_VERDADERA)) {
                mostrarDatosCondiciones = false;
            } else {
                mostrarDatosCondiciones = true;
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        }
    }

    /**
     * Método que valida que no haya una condicional nacional para el decreto seleccionado.
     */
    public void validarCondicionalNacional() {
        reiniciarVariables();
        if (this.condicionalNacionalBool) {
            if (this.decretoSeleccionado.getDecretoCondicions() != null &&
                 !this.decretoSeleccionado.getDecretoCondicions()
                    .isEmpty()) {
                for (DecretoCondicion dc : this.decretoSeleccionado
                    .getDecretoCondicions()) {
                    if (dc.getCondicion() != null &&
                         dc.getCondicion().equals(
                            Constantes.CONDICIONAL_VERDADERA)) {
                        this.addMensajeWarn(
                            "Ya existe una condicional nacional para éste decreto, si desea puede modificarla.");
                        RequestContext context = RequestContext
                            .getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    }
                }
            }
        }
    }

    /**
     * Método que guarda un condicional y lo asocia al decreto.
     */
    @SuppressWarnings("rawtypes")
    public void guardarCondicional() {

        if (this.condicionalNacionalBool) {
            this.condicionalSeleccionada
                .setCondicion(Constantes.CONDICIONAL_VERDADERA);
        }
        if (validarGuardarCondicional()) {

            Object[] resultadoValidarCondicional;
            boolean condicionalValido = false;

            if (this.condicionalSeleccionada.getId() != null) {
                this.decretoSeleccionado.getDecretoCondicions().remove(
                    this.condicionalSeleccionada);
            }
            this.condicionalSeleccionada
                .setDecretoAvaluoAnual(this.decretoSeleccionado);
            this.condicionalSeleccionada.setFechaLog(new Date(System
                .currentTimeMillis()));
            this.condicionalSeleccionada.setUsuarioLog(this.usuario.getLogin());
            try {

                this.condicionalSeleccionada = this.getFormacionService()
                    .guardarCondicional(this.condicionalSeleccionada);

                resultadoValidarCondicional = this.getFormacionService()
                    .validarCondicional(
                        this.condicionalSeleccionada.getId());

                int sizeObject = ((ArrayList) resultadoValidarCondicional[0])
                    .size();
                if (sizeObject > 0) {
                    Object[] nuevoArrayObject = new Object[sizeObject];

                    nuevoArrayObject = (Object[]) ((ArrayList) resultadoValidarCondicional[0])
                        .get(0);
                    Integer valorPrimero = Integer.parseInt(nuevoArrayObject[2]
                        .toString().trim());
                    Integer valorSegundo = Integer.parseInt(nuevoArrayObject[2]
                        .toString().trim());
                    if (valorPrimero.equals(Integer.valueOf(0)) &&
                         valorSegundo.equals(Integer.valueOf(0))) {
                        condicionalValido = true;
                    }
                }

                if (condicionalValido) {
                    if (this.decretoSeleccionado.getDecretoCondicions() == null) {
                        this.decretoSeleccionado.setDecretoCondicions(new ArrayList<DecretoCondicion>());
                    }
                    
                    this.decretoSeleccionado.getDecretoCondicions().add(
                        this.condicionalSeleccionada);
                    this.addMensajeInfo("Se adicionó el condional correctamente!");
                    reiniciarVariables();
                    this.mostrarDatosCondiciones = false;
                } else {
                    this.getFormacionService().eliminarCondicional(
                    this.condicionalSeleccionada);
                    this.condicionalSeleccionada = null;
                    this.addMensajeError("El condional no se pudo guardar correctamente, " +
                         "por favor verifique que el condicional sea válido");
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                LOGGER.debug("Error guardando la condicional.");
            }
        }
    }

    /**
     * Método que validaque los campos a guardar del condicional sean validos
     */
    public boolean validarGuardarCondicional() {
        if (this.condicionalSeleccionada != null &&
             this.decretoSeleccionado != null) {
            // Validación del condicional
            if (this.condicionalSeleccionada.getCondicion() == null ||
                 this.condicionalSeleccionada.getCondicion().trim()
                    .isEmpty()) {
                this.addMensajeError("La condicional no puede estar vacía.");
                return false;
            }

            // Validación de los parentesis de la condicional
            if (this.totalParentesis != 0) {
                this.addMensajeError(
                    "La condicional esta desequilibrada. Por favor revise los paréntesis de la expresión.");
                return false;
            }

            // Validación del valor del incremento
            if (this.condicionalSeleccionada.getIncremento() == null) {
                this.condicionalSeleccionada.setIncremento(0D);
            } else if (this.condicionalSeleccionada.getIncremento() < 0 ||
                 this.condicionalSeleccionada.getIncremento() > 100) {
                this.addMensajeError(
                    "Debe ingresar un valor válido para el incremento entre 0 y 100.");
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Método que reinicia las variables usadas en la adición de un nuevo Decreto
     */
    public void reiniciarVariables() {
        if (!this.editModeBool) {
            this.decretoSeleccionado = new DecretoAvaluoAnual();
            this.anioVigencia = null;
        } else {
            if (this.decretoSeleccionado != null && this.decretoSeleccionado.getAnioVigencia() !=
                null) {
                this.anioVigencia = this.decretoSeleccionado.getAnioVigencia();
            } else {
                this.decretoSeleccionado = new DecretoAvaluoAnual();
                this.anioVigencia = this.decretoSeleccionado.getAnioVigencia();
            }
        }

        this.condicionalSeleccionada = new DecretoCondicion();
        this.condicionalSeleccionada.setCondicion(String.valueOf(""));
        this.condicionalSeleccionada.setDescripcion(String.valueOf(""));
        // this.totalCaracteres = 0;
        this.totalParentesis = 0;
        this.parentesis = 0;
        this.mostrarDatosDecreto = true;

        // Activar y desactivar campos en la interfaz.
        this.signoBool = false;
        this.parametroBool = true;
    }

    /**
     * Método que ejecta un procedimiento almacenado para mostrar el calculo de predios afectados
     * por las condicionales del decreto.
     */
    @SuppressWarnings("rawtypes")
    public void verPrediosAfectadosPorDecreto() {
        try {
            Object[] prediosAfectadosObject = this.getFormacionService()
                .buscarCalculoDePrediosAfectadoPorDecreto(
                    this.decretoSeleccionado.getId());
            this.totalAfectadosDecreto = 0L;
            this.totalPrediosNacionales = this.getConservacionService()
                .contarPrediosNacionales();

            if (prediosAfectadosObject != null &&
                 prediosAfectadosObject.length > 0) {
                this.listPrediosAfectados = new ArrayList<CalculoPrediosAfectadosPorDecretoDTO>();
                int sizeObject = ((ArrayList) prediosAfectadosObject[0]).size();
                if (sizeObject > 0) {
                    Object[] nuevoArrayObject = new Object[sizeObject];
                    this.listPrediosAfectados =
                        new ArrayList<CalculoPrediosAfectadosPorDecretoDTO>();
                    for (int row = 0; row < sizeObject; row++) {
                        nuevoArrayObject = (Object[]) ((ArrayList) prediosAfectadosObject[0])
                            .get(row);
                        CalculoPrediosAfectadosPorDecretoDTO calculoPredioAfectado =
                            new CalculoPrediosAfectadosPorDecretoDTO();
                        calculoPredioAfectado.setCondicionId(Long
                            .parseLong(nuevoArrayObject[0].toString()
                                .trim()));
                        calculoPredioAfectado.setCondicion(nuevoArrayObject[1]
                            .toString());
                        calculoPredioAfectado.setIncremento(Double
                            .parseDouble(nuevoArrayObject[2].toString()
                                .trim()));
                        calculoPredioAfectado.setNumeroPrediosAfectados(Long
                            .parseLong(nuevoArrayObject[3].toString()
                                .trim()));
                        totalAfectadosDecreto += calculoPredioAfectado
                            .getNumeroPrediosAfectados();
                        this.listPrediosAfectados.add(calculoPredioAfectado);
                    }
                }
            }
            mensajeValidacionDecreto();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(
                "Error al realizar el calculo de predios afectados por el decreto actual.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
    }

    /**
     * Método que muestra un mensaje dependiendo del resultado de las totalizaciones de predios
     * afectados.
     */
    public void mensajeValidacionDecreto() {
        if (this.totalAfectadosDecreto < this.totalPrediosNacionales) {
            this.addMensajeError(
                "Es posible que en su decreto falten por considerar predios para realizar el incremento. Revise los condicionales.");
        } else if (this.totalAfectadosDecreto > this.totalPrediosNacionales) {
            this.addMensajeError(
                "Es posible que el decreto  este aplicando el incremento a un mismo predio varias veces. Revise los condicionales.");
        } else if (this.totalAfectadosDecreto.longValue() == this.totalPrediosNacionales
            .longValue()) {
            this.addMensajeInfo(
                "El decreto es válido, se está considerando la totalidad de los predios.");
        }
    }
    
    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "volver" Se debe visualizar la tabla de decretos
     *
     * @author hector.arias
     */
    public void volver() {
        
        this.decretoSeleccionado = null;
        this.mostrarDatosDecreto = false;
        this.editModeBool = false;
        
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     *
     * @author pedro.garcia
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("decretoIncrementoAnual");
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    // Fin de la clase
}
