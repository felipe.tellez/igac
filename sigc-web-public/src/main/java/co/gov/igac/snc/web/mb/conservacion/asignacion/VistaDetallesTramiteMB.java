/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.asignacion;

import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.HashSet;
import java.util.Set;

/**
 * Managed bean para la vista de los detalles de un trámite.
 *
 * Se hizo pensando en los detalles que se muestran en las pantallas de asignación, para los cuales
 * generalmente no se consultan haciendo fetch de todas las cosas que se muestran en los detalles
 * (solicitantes, documentos, etc), por lo cual se manejan variables para consultar de la BD esos
 * datos
 *
 *
 * @author pedro.garcia
 */
/*
 * @modified by leidy.gonzalez::#13960:: 01/10/2015:: Se adiciona atributos para visualizar los
 * documentos generados por el sistema
 */
@Component("vistaDetallesTramite")
@Scope("session")
public class VistaDetallesTramiteMB extends SNCManagedBean {

    private static final long serialVersionUID = 411241082605368361L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VistaDetallesTramiteMB.class);

    /**
     * Managed Bean usado para consultar los detalles del predio
     */
    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    @Autowired
    private IContextListener contexto;

    /**
     * Tramite seleccionado para vizualizar el detalle del tramite
     */
    private Tramite selectedTramiteForDetailsChecking;

    /**
     * predios del tramite seleccionado para ver sus detalles
     */
    private List<Predio> selectedTramitePrediosForDetailsChecking;

    /**
     * predio seleccionado para ver sus detalles
     */
    private Predio selectedPredioTramiteForDetailsChecking;

    /**
     * lista de los objetos TramiteRectificacion asociados al trámite (si es de rectificación o
     * complementación)
     */
    private List<TramiteRectificacion> selectedTramiteDatosRectificacionForDetailsCheking;

    /**
     * lista de solicitantes de la solicitud a la que pertenece el trámite del que se ven los
     * detalles
     */
    private List<SolicitanteSolicitud> selectedTramiteSolicitantesSolicitudForDetailsChecking;

    /**
     * lista de solicitantes del trámite del que se ven los detalles
     */
    private List<SolicitanteTramite> selectedTramiteSolicitantesForDetailsChecking;

    /**
     * lista de documentos requeridos del trámite del que se ven los detalles
     */
    private List<TramiteDocumentacion> selectedTramiteDocsRequeridosForDetailsChecking;

    /**
     * lista de documentos adicionales del trámite del que se ven los detalles
     */
    private List<TramiteDocumentacion> selectedTramiteDocsAdicionalesForDetailsChecking;

    /**
     * indica si se deben mostrar los solicitantes del trámite (lo que implica que no se muestran
     * los de la solicitud)
     */
    private boolean banderaMostrarSolicitantesTramite;

    /**
     * indica si se deben mostrar los solicitantes de la solicitud
     */
    private boolean banderaMostrarSolicitantesSolicitud;

    /**
     * Lista que contiene la información de la documentacion requerida
     */
    private List<TramiteDocumentacion> documentacionRequerida;

    /**
     * Lista que contiene la información de la documentacion adicional
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * documentación del trámite que se ha seleccionado de la tabla
     */
    private TramiteDocumentacion selectedTramiteDocumentacion;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Variable booleana para saber si se van a ver los documentos asociados o si se va a solicitar
     * nuevos Documentos
     */
    private Boolean verSolicitudDocsBool;

    private TramiteDocumentacion docAdicionalSeleccionado;

    /**
     * Lista que contiene la información de la documentacion generada por el sistema
     */
    private List<TramiteDocumento> documentacionGenerada;

    /**
     * documento generado del trámite que se ha seleccionado de la tabla
     */
    private TramiteDocumento selectedTramiteDocumento;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    private HPredio selectedHPredioTramiteForDetailsChecking;

    //---------  methods   -------------------
    public List<TramiteDocumento> getDocumentacionGenerada() {
        return documentacionGenerada;
    }

    public void setDocumentacionGenerada(
        List<TramiteDocumento> documentacionGenerada) {
        this.documentacionGenerada = documentacionGenerada;
    }

    public TramiteDocumento getSelectedTramiteDocumento() {
        return selectedTramiteDocumento;
    }

    public void setSelectedTramiteDocumento(
        TramiteDocumento selectedTramiteDocumento) {
        this.selectedTramiteDocumento = selectedTramiteDocumento;
    }

    public boolean isBanderaMostrarSolicitantesSolicitud() {
        return this.banderaMostrarSolicitantesSolicitud;
    }

    public List<TramiteDocumentacion> getDocumentacionRequerida() {
        return documentacionRequerida;
    }

    public void setDocumentacionRequerida(
        List<TramiteDocumentacion> documentacionRequerida) {
        this.documentacionRequerida = documentacionRequerida;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public void setBanderaMostrarSolicitantesSolicitud(boolean banderaMostrarSolicitantesSolicitud) {
        this.banderaMostrarSolicitantesSolicitud = banderaMostrarSolicitantesSolicitud;
    }

    public List<TramiteDocumentacion> getSelectedTramiteDocsRequeridosForDetailsChecking() {
        return this.selectedTramiteDocsRequeridosForDetailsChecking;
    }

    public void setSelectedTramiteDocsRequeridosForDetailsChecking(
        List<TramiteDocumentacion> stdrfdc) {
        this.selectedTramiteDocsRequeridosForDetailsChecking = stdrfdc;
    }

    public List<TramiteDocumentacion> getSelectedTramiteDocsAdicionalesForDetailsChecking() {
        return this.selectedTramiteDocsAdicionalesForDetailsChecking;
    }

    public void setSelectedTramiteDocsAdicionalesForDetailsChecking(
        List<TramiteDocumentacion> stdafdc) {
        this.selectedTramiteDocsAdicionalesForDetailsChecking = stdafdc;
    }

    public List<Predio> getSelectedTramitePrediosForDetailsChecking() {
        return this.selectedTramitePrediosForDetailsChecking;
    }

    public void setSelectedTramitePrediosForDetailsChecking(
        List<Predio> selectedTramitePrediosForDetailsChecking) {
        this.selectedTramitePrediosForDetailsChecking = selectedTramitePrediosForDetailsChecking;
        this.applicationClientName = this.contexto.getClientAppNameUrl();
    }

    public Predio getSelectedPredioTramiteForDetailsChecking() {
        return this.selectedPredioTramiteForDetailsChecking;
    }

    public void setSelectedPredioTramiteForDetailsChecking(
        Predio selectedPredioTramiteForDetailsChecking) {
        this.selectedPredioTramiteForDetailsChecking = selectedPredioTramiteForDetailsChecking;
    }

    public List<TramiteRectificacion> getSelectedTramiteDatosRectificacionForDetailsCheking() {
        return this.selectedTramiteDatosRectificacionForDetailsCheking;
    }

    public void setSelectedTramiteDatosRectificacionForDetailsCheking(
        List<TramiteRectificacion> selectedTramiteDatosRectificacionForDetailsCheking) {
        this.selectedTramiteDatosRectificacionForDetailsCheking =
            selectedTramiteDatosRectificacionForDetailsCheking;
    }

    public List<SolicitanteSolicitud> getSelectedTramiteSolicitantesSolicitudForDetailsChecking() {
        return this.selectedTramiteSolicitantesSolicitudForDetailsChecking;
    }

    public void setSelectedTramiteSolicitantesSolicitudForDetailsChecking(
        List<SolicitanteSolicitud> selectedTramiteSolicitantesSolicitudForDetailsChecking) {
        this.selectedTramiteSolicitantesSolicitudForDetailsChecking =
            selectedTramiteSolicitantesSolicitudForDetailsChecking;
    }

    public List<SolicitanteTramite> getSelectedTramiteSolicitantesForDetailsChecking() {
        return this.selectedTramiteSolicitantesForDetailsChecking;
    }

    public void setSelectedTramiteSolicitantesForDetailsChecking(
        List<SolicitanteTramite> selectedTramiteSolicitantesForDetailsChecking) {
        this.selectedTramiteSolicitantesForDetailsChecking =
            selectedTramiteSolicitantesForDetailsChecking;
    }

    public TramiteDocumentacion getDocAdicionalSeleccionado() {
        return docAdicionalSeleccionado;
    }

    public void setDocAdicionalSeleccionado(
        TramiteDocumentacion docAdicionalSeleccionado) {
        this.docAdicionalSeleccionado = docAdicionalSeleccionado;
    }

    public boolean isBanderaMostrarSolicitantesTramite() {
        return this.banderaMostrarSolicitantesTramite;
    }

    public void setBanderaMostrarSolicitantesTramite(boolean banderaMostrarSolicitantesTramite) {
        this.banderaMostrarSolicitantesTramite = banderaMostrarSolicitantesTramite;
    }

    public TramiteDocumentacion getSelectedTramiteDocumentacion() {
        return this.selectedTramiteDocumentacion;
    }

    public void setSelectedTramiteDocumentacion(TramiteDocumentacion selectedTramiteDocumentacion) {
        this.selectedTramiteDocumentacion = selectedTramiteDocumentacion;
    }

    public String getTipoMimeDocTemporal() {
        return this.tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public Boolean getVerSolicitudDocsBool() {
        return verSolicitudDocsBool;
    }

    public void setVerSolicitudDocsBool(Boolean verSolicitudDocsBool) {
        this.verSolicitudDocsBool = verSolicitudDocsBool;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public HPredio getSelectedHPredioTramiteForDetailsChecking() {
        return selectedHPredioTramiteForDetailsChecking;
    }

    public void setSelectedHPredioTramiteForDetailsChecking(
        HPredio selectedHPredioTramiteForDetailsChecking) {
        this.selectedHPredioTramiteForDetailsChecking = selectedHPredioTramiteForDetailsChecking;
    }
//------------------------------------------------------------------------------------------

    public Tramite getSelectedTramiteForDetailsChecking() {
        return this.selectedTramiteForDetailsChecking;
    }

    /*
     * @modified by leidy.gonzalez:: #16501:: Se modifica metodo para que cargue los documentos de
     * pruebas.
     */
 /*
     * modified by leidy.gonzalez :: #16501 :: Se genera informacion para verificar que existan
     * documento de pruebas asociado al tramite.
     */
    public void setSelectedTramiteForDetailsChecking(Tramite tramite) {
        try {

            Long idTramite = tramite.getId();
            Tramite tempTramite;
            List<Tramite> tramitesConDocumentosGenerados = new ArrayList<Tramite>();
            Documento docTemp = new Documento();
            TramitePrueba temptramitePrueba = new TramitePrueba();
            TramiteDocumento tramiteDocTemp = new TramiteDocumento();

            this.applicationClientName = this.contexto.getClientAppNameUrl();
            //D: si el trámite seleccionado es diferente al que estaba, entonces se consultan los predios
            //   de éste, así como los datos a rectificar (si es de tipo rectificación o complementación)
            //juanfelipe.garcia :: #4670 quite esta condicion porque no se cargaban bien los datos 
            //cuando idTramite era igual a  null y para no replicar codigo en este metodo
            /* if ((this.selectedTramiteForDetailsChecking != null && idTramite != null &&
             * this.selectedTramiteForDetailsChecking.getId().compareTo(idTramite) != 0) ||
             * this.selectedTramiteForDetailsChecking == null) { */

            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
            this.documentacionRequerida = new ArrayList<TramiteDocumentacion>();
            this.documentacionGenerada = new ArrayList<TramiteDocumento>();

            //if (this.selectedTramiteForDetailsChecking.getId() != idTramite) {
            //juanfelipe.garcia :: #4670 cuando idTramite es null, no mostraba los predios del tramite
            if (idTramite != null) {
                this.selectedTramitePrediosForDetailsChecking =
                     this.getTramiteService().buscarPrediosDeTramite(idTramite);
            } else {
                //para tramites temporales asigno los predios del tramite que recibo
                this.selectedTramitePrediosForDetailsChecking = new ArrayList<Predio>();

                if (tramite.isSegundaEnglobe()) {
                    for (TramitePredioEnglobe pteTemp : tramite.getTramitePredioEnglobes()) {
                        this.selectedTramitePrediosForDetailsChecking.add(pteTemp.getPredio());
                    }
                } else {
                    this.selectedTramitePrediosForDetailsChecking.add(tramite.getPredio());
                }

            }
            //juanfelipe.garcia :: #4670 cuando idTramite es null, no mostraba todos los detalles del tramite
            //D: si el trámite es de rectificación o complementación se consultan esos datos
            if (tramite.getTipoTramite().equalsIgnoreCase(ETramiteTipoTramite.RECTIFICACION.
                getCodigo()) ||
                 tramite.getTipoTramite().equalsIgnoreCase(ETramiteTipoTramite.COMPLEMENTACION.
                    getCodigo())) {
                if (idTramite != null) {
                    this.selectedTramiteDatosRectificacionForDetailsCheking =
                         this.getTramiteService().obtenerTramiteRectificacionesDeTramite(idTramite);
                } else {
                    this.selectedTramiteDatosRectificacionForDetailsCheking = tramite.
                        getTramiteRectificacions();
                }
            }

            //D: se consultan los solicitantes de la solicitud. Si la relación del solicitante
            //  es "intermediario" se supone que el trámite es de tipo aviso o autoavalúo (llegado de tesorería).
            // En este caso se deben consultar los solicitantes del trámite
            this.banderaMostrarSolicitantesTramite = false;
            this.banderaMostrarSolicitantesSolicitud = false;
            this.selectedTramiteSolicitantesSolicitudForDetailsChecking =
                 this.getTramiteService().obtenerSolicitantesSolicitud2(tramite.getSolicitud().
                    getId());

            if (this.selectedTramiteSolicitantesSolicitudForDetailsChecking != null &&
                 !this.selectedTramiteSolicitantesSolicitudForDetailsChecking.isEmpty()) {

                this.banderaMostrarSolicitantesSolicitud = true;
            }
            //D: OJO: ahora cambiaron las condiciones:
            //  se muestran los solicitantes de la solicitud y del trámite en el caso de autoavalúo.
            //  En caso contrario se muestran solo los de la solicitud.
            // se deja comentado por si acaso se necesita luego

            //for (SolicitanteSolicitud ss : this.selectedTramiteSolicitantesSolicitudForDetailsChecking) {
            //if (ss.getRelacion() != null &&
            //	ss.getRelacion().equalsIgnoreCase(ESolicitanteSolicitudRelac.TESORERIA.getCodigo())){
            if (tramite.getTipoTramite().
                equalsIgnoreCase(ETramiteTipoTramite.AUTOESTIMACION.getCodigo())) {
                this.selectedTramiteSolicitantesForDetailsChecking =
                     this.getTramiteService().obtenerSolicitantesDeTramite(idTramite);
                //break;
            }
            //}
            //}
            if (idTramite != null) {
                //D: se consultan los documentos del trámite
                tempTramite = this.getTramiteService().consultarDocumentacionDeTramite(idTramite);
                if (tempTramite.getTramiteDocumentacions() != null &&
                     !tempTramite.getTramiteDocumentacions().isEmpty()) {

                    for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                        if (td.getRequerido() != null &&
                             td.getRequerido().equals(ESiNo.SI.toString())) {
                            this.documentacionRequerida.add(td);
                        }
                    }

                    for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                        if (td.getAdicional() != null &&
                             td.getAdicional().equals(ESiNo.SI.toString()) &&
                             td.getRequerido() != null &&
                             td.getRequerido().equals(ESiNo.NO.toString())) {
                            this.documentacionAdicional.add(td);
                        }
                    }

                    for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                        //LOGGER.debug("Algo id: " + td.getRequerido());

//                        if (td.getDocumentoSoporte() != null) {
//                            LOGGER.debug("Algo doc: " + td.getDocumentoSoporte().getId());
//                        }
                    }
                }

                //@modified by dumar.penuela::#17544:: 24/05/2016:: Se adicionan las solicitudes que no tienen definido un tramite
                List<Documento> docsSolicitudTramites = this.getTramiteService().
                    buscarDocumentosPorSolicitudId(tramite.getSolicitud().getId());
                List<TramiteDocumento> tramiteDocumentoTemp = new ArrayList<TramiteDocumento>();

                for (Documento docsSolicitudTramite : docsSolicitudTramites) {
                    tramiteDocTemp = new TramiteDocumento();
                    tramiteDocTemp.setDocumento(docsSolicitudTramite);
                    tramiteDocTemp.setId(docsSolicitudTramite.getId());
                    tramiteDocumentoTemp.add(tramiteDocTemp);
                }

                if (tramiteDocumentoTemp != null && !tramiteDocumentoTemp.isEmpty()) {
                    this.documentacionGenerada.addAll(tramiteDocumentoTemp);
                }

                //@modified by dumar.penuela::#18820:: 01/08/2016:: Se adiciona el documento Auto de Pruebas que esta relacionado con el TramitePrueba.
                if (tramite.getTramitePruebas() != null &&
                     tramite.getTramitePruebas().getAutoPruebasDocumento() != null) {
                    tramiteDocTemp = new TramiteDocumento();
                    docTemp = new Documento();
                    docTemp = this.getTramiteService().buscarDocumentoPorId(tramite.
                        getTramitePruebas().getAutoPruebasDocumento().getId());
                    tramiteDocTemp.setDocumento(docTemp);
                    tramiteDocTemp.setId(tramite.getTramitePruebas().getAutoPruebasDocumento().
                        getId());
                    this.documentacionGenerada.add(tramiteDocTemp);
                }

                //@modified by leidy.gonzalez::#13960:: 01/10/2015:: Se adiciona consulta para documentos generados por el sistema
                tramitesConDocumentosGenerados = this.getTramiteService().
                    consultarDocumentacionDeTramiteGenerada(idTramite);

                if (tramitesConDocumentosGenerados != null && !tramitesConDocumentosGenerados.
                    isEmpty()) {

                    for (Tramite tramiteDocumentoGenerado : tramitesConDocumentosGenerados) {

                        if (tramiteDocumentoGenerado.getTramiteDocumentos() != null &&
                             !tramiteDocumentoGenerado.getTramiteDocumentos().isEmpty()) {
                            //

                            for (TramiteDocumento tramiteDoc : tramiteDocumentoGenerado.
                                getTramiteDocumentos()) {
                                if (tramiteDoc.getDocumento().getIdRepositorioDocumentos() != null &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumentoId.IMA_GEO_PREDIO_ANTES_TRAMITE.getId()) &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumentoId.IMA_GEO_PREDIO_DESPUES_TRAMITE.getId()) &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumentoId.COPIA_BD_GEOGRAFICA_ORIGINAL.getId()) &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumentoId.COPIA_BD_GEOGRAFICA_FINAL.getId()) &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId()) &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL_DEPURACION.
                                            getId()) &&
                                     !tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                                        ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL_DEPURACION.getId())) {

                                    this.documentacionGenerada.add(tramiteDoc);
                                }
                            }

                            Documento documentoNotificacion = this.getGeneralesService().
                                buscarDocumentoPorTramiteIdyTipoDocumento(
                                    tramiteDocumentoGenerado.getId(),
                                    ETipoDocumento.DOCUMENTO_DE_NOTIFICACION.getId());
                            if (documentoNotificacion != null) {
                                TramiteDocumento td = new TramiteDocumento();
                                td.setDocumento(documentoNotificacion);
                                td.setId(0l);
                                this.documentacionGenerada.add(td);
                            }
                        }
                    }
                }
            }
            //}

//		else {
//			this.selectedTramitePrediosForDetailsChecking =
//				this.getTramiteService().buscarPrediosDeTramite(tramite.getId());
//
//
////TODO::pedro.garcia::este còdigo se puede factorizar
//			if (tramite.getTipoTramite().equalsIgnoreCase(ETramiteTipoTramite.RECTIFICACION.getCodigo())
//					|| tramite.getTipoTramite().equalsIgnoreCase(ETramiteTipoTramite.COMPLEMENTACION.getCodigo())){
//				this.selectedTramiteDatosRectificacionForDetailsCheking =
//					this.getTramiteService().obtenerTramiteRectificacionesDeTramite(idTramite);
//			}
//
//			//D: se consultan los solicitantes de la solicitud. Si la relación del solicitante
//			//  es "intermediario" se supone que el trámite es de tipo aviso o autoavalúo (llegado de tesorería).
//			// En este caso se deben consultar los solicitantes del trámite
//			this.banderaMostrarSolicitantesTramite = false;
//			this.selectedTramiteSolicitantesSolicitudForDetailsChecking =
//				this.getTramiteService().obtenerSolicitantesSolicitud(tramite.getSolicitud().getId());
//			if (this.selectedTramiteSolicitantesSolicitudForDetailsChecking != null &&
//					!this.selectedTramiteSolicitantesSolicitudForDetailsChecking.isEmpty()) {
//				for (SolicitanteSolicitud ss : this.selectedTramiteSolicitantesSolicitudForDetailsChecking) {
//					if (ss.getRelacion() != null &&
//						ss.getRelacion().equalsIgnoreCase(ESolicitanteSolicitudRelac.TESORERIA.getCodigo())){
//						this.selectedTramiteSolicitantesForDetailsChecking =
//							this.getTramiteService().obtenerSolicitantesDeTramite(idTramite);
//						this.banderaMostrarSolicitantesTramite = true;
//						break;
//					}
//				}
//			}
//
//
//		}	
            this.selectedTramiteForDetailsChecking = tramite;
            List<TramiteDocumento> documentoPruebasTemp = new ArrayList<TramiteDocumento>();

            //Documento soporte de actualizacion
            if (selectedTramiteForDetailsChecking.isActualizacion()) {
                Documento docActualizacion = this.getGeneralesService().
                    buscarDocumentoPorTramiteIdyTipoDocumento(
                        this.selectedTramiteForDetailsChecking.getId(),
                        ETipoDocumento.DOCUMENTO_RESULTADO_DE_ACTUALIZACION.getId());
                TramiteDocumento td = new TramiteDocumento();
                td.setDocumento(docActualizacion);
                td.setId(0l);
                this.documentacionGenerada.add(td);
            }

            if (this.selectedTramiteForDetailsChecking != null &&
                 this.selectedTramiteForDetailsChecking.getTramitePruebas() != null &&
                 this.selectedTramiteForDetailsChecking.getTramiteDocumentos() != null) {

                for (TramiteDocumento tramiteDoc : this.selectedTramiteForDetailsChecking.
                    getTramiteDocumentos()) {

                    if (tramiteDoc.getDocumento() != null &&
                         tramiteDoc.getDocumento().getIdRepositorioDocumentos() != null &&
                         tramiteDoc.getDocumento().getTipoDocumento() != null &&
                         tramiteDoc.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.DOCUMENTO_RESULTADO_DE_PRUEBAS.getId())) {

                        documentoPruebasTemp.add(tramiteDoc);
                    }

                }
                if (!documentoPruebasTemp.isEmpty()) {
                    this.selectedTramiteForDetailsChecking.
                        setDocumentosPruebas(documentoPruebasTemp);
                    this.getTramiteService().actualizarTramite(
                        this.selectedTramiteForDetailsChecking);
                }

            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error en VistaDetallesTramiteMB#setSelectedTramiteForDetailsChecking: " +
                ex.getMensaje());
        }
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        LOGGER.debug("en el init de VistaDetallesTramiteMB ");
        this.applicationClientName = this.contexto.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }

//--------------------------------------------------------------------------------------------------
    public void inicializarConsultaPredioMB() {
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        mb001.cleanAllMB();
        if (this.selectedPredioTramiteForDetailsChecking != null) {
            this.consultaPredioMB.setSelectedPredioId(this.selectedPredioTramiteForDetailsChecking.
                getId());
        }
    }

//--------------------------------------------------------------------------------------------------    
    /*
     * @modified by leidy.gonzalez::#13960::01/10/2015::Se agrega visualizador para documentos
     * generados por el sistema.
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        if (this.selectedTramiteDocumentacion != null &&
             this.selectedTramiteDocumentacion.getDocumentoSoporte() != null &&
             this.selectedTramiteDocumentacion.getDocumentoSoporte().getArchivo() != null) {
            this.tipoMimeDocTemporal = fileNameMap
                .getContentTypeFor(this.selectedTramiteDocumentacion.getDocumentoSoporte().
                    getArchivo());
        }
        if (this.selectedTramiteDocumento != null &&
             this.selectedTramiteDocumento.getDocumento().getArchivo() != null &&
             this.selectedTramiteDocumento.getDocumento().getArchivo() != null) {
            this.tipoMimeDocTemporal = fileNameMap
                .getContentTypeFor(this.selectedTramiteDocumento.getDocumento().getArchivo());
        }

        return this.tipoMimeDocTemporal;
    }
//--------------------------------------------------------------------------------------------------   

    public void desasociarDocAdicionalTablaDocsAsociados() {
        if (this.selectedTramiteForDetailsChecking != null && this.docAdicionalSeleccionado != null) {
            for (TramiteDocumentacion tdTemp : this.selectedTramiteForDetailsChecking.
                getDocumentacionAdicional()) {
                if (tdTemp.getId().equals(this.docAdicionalSeleccionado.getId())) {
                    if (this.selectedTramiteForDetailsChecking.getTramiteDocumentacionsTemp() ==
                        null) {
                        this.selectedTramiteForDetailsChecking.setTramiteDocumentacionsTemp(
                            new ArrayList<TramiteDocumentacion>());
                    }
                    if (this.selectedTramiteForDetailsChecking.getTramiteDocumentacionsTemp().
                        contains(tdTemp)) {
                        this.selectedTramiteForDetailsChecking.getTramiteDocumentacionsTemp().
                            remove(tdTemp);
                    }
                    tdTemp.setDocumentoSoporte(null);
                    this.getTramiteService().eliminarTramiteDocumentacion(tdTemp);
                }

            }
            this.getTramiteService().getTramiteTramitesbyTramiteId(
                this.selectedTramiteForDetailsChecking.getId());
        }
    }

    /**
     *
     * Se modifican las variables principales de consulta de la ventana de dialogo "detalle predio"
     * para mostrar un predio historico
     *
     * juan.cruz
     *
     */
    public void inicializarConsultaHPredioMB() {
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        mb001.cleanAllMB();
        if (this.selectedHPredioTramiteForDetailsChecking != null) {
            Object[] predioHijo = new Object[4];
            this.consultaPredioMB.setPredioOrigen(true);
            Predio predioOrigenDetallado = obtenerPredioOrigenDetallado(
                selectedHPredioTramiteForDetailsChecking.getId());

            if (mb001.getSelectedPredio1() == null) {
                predioHijo[0] = mb001.getSelectedPredio2()[2];
                predioHijo[1] = mb001.getSelectedPredio2()[15];
                predioHijo[2] = this.consultaPredioMB.getConservacionService().isPredioMigradoCobol(
                    (String) mb001.getSelectedPredio2()[14]);
                Predio predio = this.getConservacionService().buscarPredioCompletoPorPredioId(Long.
                    valueOf(mb001.getSelectedPredio2()[2].toString()));
                mb001.setSelectedPredio1(predio);
                predioHijo[3] = mb001.getSelectedPredio1();
            } else {
                predioHijo[0] = mb001.getSelectedPredio1().getId();
                predioHijo[1] = mb001.getSelectedPredio1().getNumeroPredialAnterior();
                predioHijo[2] = this.getConservacionService().isPredioMigradoCobol((String) mb001.
                    getSelectedPredio1().getNumeroPredial());
                predioHijo[3] = mb001.getSelectedPredio1();
            }

            this.consultaPredioMB.setSelectedPredio1(predioOrigenDetallado);
            this.consultaPredioMB.setSelectedPredioId(predioOrigenDetallado.getId());
            this.consultaPredioMB.setSelectedPredioNumeroPredial(predioOrigenDetallado.
                getNumeroPredial());
            this.consultaPredioMB.setMigradoCobol(this.consultaPredioMB.getConservacionService().
                isPredioMigradoCobol(predioOrigenDetallado.getNumeroPredial()));

            mb001.getArbolFamiliarPredios().add(predioHijo);
        }
    }

    /**
     * Trae el detalle necesario para cargar la información de la ventana de dialogo "Detalles del
     * predio" Y todas sus pestañas correspondientes.
     *
     * juan.cruz
     *
     */
    public Predio obtenerPredioOrigenDetallado(Long predioOrigenId) {
        Predio predioOrigen = Predio.parsePredio(this.getConservacionService().
            obtenerHPredioOrigenDetallado(predioOrigenId));
        return predioOrigen;
    }

    /**
     * Método que se crea para actualizar los datos de la ventana de "Detalle predio" al momento de
     * cerrarse.
     */
    public void finalizarConsultaHPredioMB() {
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        mb001.cleanAllMB();

        ArrayList<Object[]> arbolFamiliarPredios = this.consultaPredioMB.getArbolFamiliarPredios();

        if (arbolFamiliarPredios != null && !arbolFamiliarPredios.isEmpty()) {
            int posHijoPredio = arbolFamiliarPredios.size() - 1;
            Object[] predioHijo = arbolFamiliarPredios.get(posHijoPredio);

            arbolFamiliarPredios.remove(posHijoPredio);

            //si el id es el ultimo del arbolFamiliar, significa que es el predio que se consultó
            this.consultaPredioMB.setPredioOrigen(!arbolFamiliarPredios.isEmpty());
            this.consultaPredioMB.setSelectedPredioId(Long.valueOf(predioHijo[0].toString()));
            this.consultaPredioMB.setSelectedPredioNumeroPredial((String) predioHijo[1]);
            this.consultaPredioMB.setMigradoCobol((Boolean) predioHijo[2]);
            this.consultaPredioMB.setSelectedPredio1((Predio) predioHijo[3]);
        }
    }
}
