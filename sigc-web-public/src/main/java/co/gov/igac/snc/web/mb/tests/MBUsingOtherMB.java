package co.gov.igac.snc.web.mb.tests;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro.garcia
 *
 * Se usó la anotación @Component de spring en este mb y el que va a ser usado. Se debe usar
 * @Autowired para inyectar el mb usado La otra forma posible es usando las anotaciones @ManagedBean
 * en ambos mb y @ManagedProperty para la inyección del mb usado
 *
 */
//@ManagedBean(name="managedbeanUsingOtherMB")
@Component("managedbeanUsingOtherMB")
@Scope("session")
public class MBUsingOtherMB implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MBUsingOtherMB.class);

    //@ManagedProperty(value="#{managedbeanUsedByOtherMB}")
    @Autowired
    private MBUsedByOtherMB neneBean;

    @PostConstruct
    public void init() {

        LOGGER.debug("on MBUsingOtherMB#init");
    }

    public MBUsedByOtherMB getNeneBean() {
        return neneBean;
    }

    public void setNeneBean(MBUsedByOtherMB neneBean) {
        this.neneBean = neneBean;
    }

    public void callBebeMBFunction() {
        neneBean.changeCadenaValue();
    }

}
