package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.util.Constantes;

/**
 * Converter para reemplazar una territorial.
 *
 * @author david.cifuentes
 *
 */
public class TerritorialConverter implements Converter {

    @Implement
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String objectAsString) {

        EstructuraOrganizacional answer;
        String[] objectAttributes;

        answer = new EstructuraOrganizacional();
        objectAttributes = objectAsString.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setCodigo(objectAttributes[0]);
        answer.setNombre(objectAttributes[1]);
        answer.setTipo(objectAttributes[2]);
        answer.setEstructuraOrganizacionalCod(objectAttributes[3]);

        return answer;
    }

    @Implement
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        EstructuraOrganizacional objDT = (EstructuraOrganizacional) o;
        String answer = objDT.toString();
        return answer;
    }
}
