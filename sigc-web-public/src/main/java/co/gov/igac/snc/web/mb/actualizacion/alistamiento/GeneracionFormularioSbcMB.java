package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.actualizacion.GeneracionFormularioSbc;
import co.gov.igac.snc.persistence.entity.actualizacion.PredioFormularioSbc;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.NumeroPredialListaTramiteDTO;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author lorena.salamanca
 * @modified javier.barajas -> Creacion de metodos para interactuar con la B.D, Ajustes al visor,
 * incorporacion del reporte, y creacion de la clase de transporte
 * @version 1.0
 * @cu 204 Generacion de Formularios SBC
 *
 */
@Component("generarFormularioSbc")
@Scope("session")
public class GeneracionFormularioSbcMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneracionFormularioSbcMB.class);

    /**
     * SERVICIOS
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * En esta variable se van a enviar los numeros prediales de los predios incluidos en la
     * actualizacion
     */
    private String codigoPrediosParaVisor;

    /**
     * En esta variable se recibe la lista de predios a seleccionados mediante el visor
     *
     */
    private String seleccionVisor;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * Valor para el manejo de el visor geografico
     */
    private String valorSeleccionActualizacion;
    /**
     * Lista para almacenar numeros prediales y radicados de tramite
     */
    private List<String> listaNumerosPrediales;

    /**
     * Carga el codigo de municipio para el manejo del visor geografico
     */
    private String codigoMunicipio;

    /**
     * carga la seleccion de barrios del municipio para el manejo del visor geografico
     */
    private ArrayList<SelectItem> selectListaBarrios;

    /**
     * tamaño de lazy
     */
    private int lazySize;
    /**
     * Data model para recibir los predios de la actualizacion
     *
     */
    private LazyDataModel<NumeroPredialListaTramiteDTO> lazyModel;

    /**
     * Lista de objeto de transporte
     */
    private List<NumeroPredialListaTramiteDTO> listaPrediosTramites;

    /**
     * Lista de objeto de transporte, cuando se hace la seleccion mediante visor
     */
    private List<NumeroPredialListaTramiteDTO> listaPrediosTramitesSelect;

    /**
     * Selecion de predios para generar el formulario de socializacion basica catrastral
     */
    private List<PredioFormularioSbc> prediosFormularioSbc;

    /**
     * Lista para guardar las manzanas a mostrar mediante el visor y variables a mostrar
     */
    private List<ManzanaVereda> listaManzanasVeredas;

    /**
     * Numero de manzanas seleccionadas
     */
    private int numeroManzanasSeleccionadas;

    /**
     * Numero de predios seleccionados
     */
    private int numeroPrediosSeleccionados;

    /**
     * Numero de barrios de la seleccion por visor
     */
    private int numeroBarriosMunicipio;

    /**
     * Seleccion de barrio para mostrar por visor
     */
    private String seleccionBarrio;

    /**
     * Seleccion multiple de la tabla
     */
    private NumeroPredialListaTramiteDTO[] selectNumeroPredioM;

    /**
     * Usuario de la aplicacion
     */
    private UsuarioDTO usuario;

    /**
     * Creacion del objeto de generacion de formulario SBC
     */
    private GeneracionFormularioSbc generacionFormulario;

    /**
     * Lista de seleccion del predio mediante el visor
     */
    private List<NumeroPredialListaTramiteDTO> selectPredioTramiteVisor;

    /**
     * Mostrar el boton limpiar la seleccion
     */
    private boolean renderedLimpiaBtn;

    /**
     * Cadena de busqueda de predios
     */
    private String cadenaBusqueda;

    /**
     * No de formularios en blanco
     */
    private int noFormBlanco;

    /**
     * Habilita el boton de generar formularios SBC
     */
    private boolean btnHabilitaGenerar;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDTO;

    private ReportesUtil reportsService = ReportesUtil.getInstance();
    /**
     * Variable que indica si el reporte se generó de forma correcta.
     */
    private boolean reporteGeneradoOK;

    /**
     * Variable que indica se ha hecho un a seleccion mediante el visor.
     */
    private boolean rederedConsultaVisor;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

//-------------------------------------------------------------------------------------
    /**
     * INIT
     */
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // TODO :: Modificar el set de la actualización cuando se integre
        // process, revisar si es masivo (menu) :: 12/12/12
        // Obtener la actualizacióin del arbol de tareas.
        this.actualizacionSeleccionada = this.getActualizacionService()
            .recuperarActualizacionPorId(439L);

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.codigoPrediosParaVisor =
            "08001010100000001,08001010100000002,08001010200000175,08001010200000123";
        this.moduleLayer = EVisorGISLayer.ACTUALIZACION.getCodigo();
        this.moduleTools = EVisorGISTools.ACTUALIZACION.getCodigo();
        //this.datosConsultaPredio.set
        this.valorSeleccionActualizacion = "seleccionarPrediosActualizacion";
        //TODO::this.valorSeleccionActualizacion =  EVisorGISParametrosAsignacion.SELECCIONAR_PREDIOS_ACTUALIZACION
        this.listaPrediosTramites = new ArrayList<NumeroPredialListaTramiteDTO>();
        this.listaPrediosTramitesSelect = new ArrayList<NumeroPredialListaTramiteDTO>();
        this.listaNumerosPrediales = new ArrayList<String>();
        this.selectListaBarrios = new ArrayList<SelectItem>();
        this.cargarDatosTabla();
        this.cargarBarriosVisor();
        this.renderedLimpiaBtn = false;
        this.rederedConsultaVisor = false;
        this.btnHabilitaGenerar = false;
        this.cargaFiltroVisor();

    }

    //-----------------------------Getters y Setters------------------------------------------------------
    public boolean isBtnHabilitaGenerar() {
        return btnHabilitaGenerar;
    }

    public void setBtnHabilitaGenerar(boolean btnHabilitaGenerar) {
        this.btnHabilitaGenerar = btnHabilitaGenerar;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public int getNoFormBlanco() {
        return noFormBlanco;
    }

    public void setNoFormBlanco(int noFormBlanco) {
        this.noFormBlanco = noFormBlanco;
    }

    public IContextListener getContextoWeb() {
        return contextoWeb;
    }

    public String getCadenaBusqueda() {
        return cadenaBusqueda;
    }

    public void setCadenaBusqueda(String cadenaBusqueda) {
        this.cadenaBusqueda = cadenaBusqueda;
    }

    public void setContextoWeb(IContextListener contextoWeb) {
        this.contextoWeb = contextoWeb;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    public ReportesUtil getReportsService() {
        return reportsService;
    }

    public void setReportsService(ReportesUtil reportsService) {
        this.reportsService = reportsService;
    }

    public boolean isReporteGeneradoOK() {
        return reporteGeneradoOK;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public void setActualizacionSeleccionada(Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public String getCodigoPrediosParaVisor() {
        return codigoPrediosParaVisor;
    }

    public void setCodigoPrediosParaVisor(String codigoPrediosParaVisor) {
        this.codigoPrediosParaVisor = codigoPrediosParaVisor;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getValorSeleccionActualizacion() {
        return valorSeleccionActualizacion;
    }

    public void setValorSeleccionActualizacion(String valorSeleccionActualizacion) {
        this.valorSeleccionActualizacion = valorSeleccionActualizacion;
    }

    public List<String> getListaNumerosPrediales() {
        return listaNumerosPrediales;
    }

    public void setListaNumerosPrediales(List<String> listaNumerosPrediales) {
        this.listaNumerosPrediales = listaNumerosPrediales;
    }

    public List<PredioFormularioSbc> getPrediosFormularioSbc() {
        return prediosFormularioSbc;
    }

    public void setPrediosFormularioSbc(
        List<PredioFormularioSbc> prediosFormularioSbc) {
        this.prediosFormularioSbc = prediosFormularioSbc;
    }

    public void setGeneracionFormulario(GeneracionFormularioSbc generacionFormulario) {
        this.generacionFormulario = generacionFormulario;
    }

    public GeneracionFormularioSbc getGeneracionFormulario() {
        return generacionFormulario;
    }

    public List<ManzanaVereda> getListaManzanasVeredas() {
        return listaManzanasVeredas;
    }

    public void setListaManzanasVeredas(List<ManzanaVereda> listaManzanasVeredas) {
        this.listaManzanasVeredas = listaManzanasVeredas;
    }

    public LazyDataModel<NumeroPredialListaTramiteDTO> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<NumeroPredialListaTramiteDTO> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public int getLazySize() {
        return lazySize;
    }

    public void setLazySize(int lazySize) {
        this.lazySize = lazySize;
    }

    public void setListaPrediosTramites(List<NumeroPredialListaTramiteDTO> listaPrediosTramites) {
        this.listaPrediosTramites = listaPrediosTramites;
    }

    public List<NumeroPredialListaTramiteDTO> getListaPrediosTramites() {
        return listaPrediosTramites;
    }

    public NumeroPredialListaTramiteDTO[] getSelectNumeroPredioM() {
        return selectNumeroPredioM;
    }

    public void setSelectNumeroPredioM(
        NumeroPredialListaTramiteDTO[] selectNumeroPredioM) {
        this.selectNumeroPredioM = selectNumeroPredioM;
    }

    public String getSeleccionVisor() {
        return seleccionVisor;
    }

    public void setSeleccionVisor(String seleccionVisor) {

        if (!seleccionVisor.isEmpty()) {
            this.rederedConsultaVisor = true;
        }
        this.seleccionVisor = seleccionVisor;
    }

    public boolean isRederedConsultaVisor() {
        return rederedConsultaVisor;
    }

    public void setRederedConsultaVisor(boolean rederedConsultaVisor) {
        this.rederedConsultaVisor = rederedConsultaVisor;
    }

    public List<NumeroPredialListaTramiteDTO> getSelectPredioTramiteVisor() {
        return selectPredioTramiteVisor;
    }

    public void setSelectPredioTramiteVisor(
        List<NumeroPredialListaTramiteDTO> selectPredioTramiteVisor) {
        this.selectPredioTramiteVisor = selectPredioTramiteVisor;
    }

    public int getNumeroManzanasSeleccionadas() {
        return numeroManzanasSeleccionadas;
    }

    public void setNumeroManzanasSeleccionadas(int numeroManzanasSeleccionadas) {
        this.numeroManzanasSeleccionadas = numeroManzanasSeleccionadas;
    }

    public int getNumeroPrediosSeleccionados() {
        return numeroPrediosSeleccionados;
    }

    public void setNumeroPrediosSeleccionados(int numeroPrediosSeleccionados) {
        this.numeroPrediosSeleccionados = numeroPrediosSeleccionados;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public ArrayList<SelectItem> getSelectListaBarrios() {
        return selectListaBarrios;
    }

    public void setSelectListaBarrios(ArrayList<SelectItem> selectListaBarrios) {
        this.selectListaBarrios = selectListaBarrios;
    }

    public int getNumeroBarriosMunicipio() {
        return numeroBarriosMunicipio;
    }

    public void setNumeroBarriosMunicipio(int numeroBarriosMunicipio) {
        this.numeroBarriosMunicipio = numeroBarriosMunicipio;
    }

    public String getSeleccionBarrio() {
        return seleccionBarrio;
    }

    public void setSeleccionBarrio(String seleccionBarrio) {
        this.seleccionBarrio = seleccionBarrio;
    }

    public List<NumeroPredialListaTramiteDTO> getListaPrediosTramitesSelect() {
        return listaPrediosTramitesSelect;
    }

    public void setListaPrediosTramitesSelect(
        List<NumeroPredialListaTramiteDTO> listaPrediosTramitesSelect) {
        this.listaPrediosTramitesSelect = listaPrediosTramitesSelect;
    }

    public boolean isRenderedLimpiaBtn() {
        return renderedLimpiaBtn;
    }

    public void setRenderedLimpiaBtn(boolean renderedLimpiaBtn) {
        this.renderedLimpiaBtn = renderedLimpiaBtn;
    }

    //------------------METODOS--------------------------------------------------------------------------------
    /**
     * Carga del lazy data model
     *
     * @author javier.barajas
     */
    @SuppressWarnings("serial")
    public void cargarDatosTabla() {

        this.lazyModel = new LazyDataModel<NumeroPredialListaTramiteDTO>() {

            @Override
            public List<NumeroPredialListaTramiteDTO> load(int first, int pageSize, String sortField,
                SortOrder sortOrder,
                Map<String, String> filters) {
                List<NumeroPredialListaTramiteDTO> answer =
                    new ArrayList<NumeroPredialListaTramiteDTO>();
                populatePrediosTramitesLazyly(answer, sortField, sortOrder.toString(), first,
                    pageSize);

                return answer;
            }
        };

    }

    /**
     * Carga los datos al lazydatmodel
     *
     * @param answer
     * @param sortField
     * @param sortOrder
     * @param first
     * @param pageSize
     *
     * @author javier.barajas
     */
    private void populatePrediosTramitesLazyly(List<NumeroPredialListaTramiteDTO> answer,
        String sortField,
        String sortOrder, int first, int pageSize) {

        this.buscarPrediosActualizacion(sortField, sortOrder, false, first, pageSize);
        List<Object[]> numeroFilas = null;
        numeroFilas = this.getActualizacionService().obtenerPrediosdeActualizacionPorZonas(
            this.actualizacionSeleccionada.getId(), null, null, false, null);

        this.lazySize = numeroFilas.size();

        for (NumeroPredialListaTramiteDTO filasLista : this.listaPrediosTramites) {
            answer.add(filasLista);
        }
        if (this.lazySize > 0) {
            this.lazyModel.setRowCount(this.lazySize);
        } else {
            this.lazyModel.setRowCount(0);
        }

    }

    /**
     * Busca los predios de la actualizacion por zona (Urbana, rural)
     *
     * @param sortField
     * @param sortOrder
     * @param iscount
     * @param first
     * @param pageSize
     *
     * @author javier.barajas
     */
    public void buscarPrediosActualizacion(String sortField,
        String sortOrder, boolean iscount, int first, int pageSize) {
        FacesMessage msg = new FacesMessage();

        NumeroPredialListaTramiteDTO filaPredioTramite = new NumeroPredialListaTramiteDTO();
        List<Object[]> answer = null;
        List<Tramite> tramitesNoRadicado = new ArrayList<Tramite>();
        String cadena = new String();
        try {

            answer = this.getActualizacionService().obtenerPrediosdeActualizacionPorZonas(
                this.actualizacionSeleccionada.getId(), sortField, sortOrder, iscount, first,
                pageSize);
            for (Object[] np : answer) {
                filaPredioTramite.setNumeroPredial(np[5].toString());
                this.listaNumerosPrediales.add(np[5].toString());
                cadena = cadena + np[5].toString();
                tramitesNoRadicado = this.getTramiteService().
                    buscarNoRadicadosTramiteporNumeroPredial(np[5].toString());
                filaPredioTramite.setTramitesAsociados(tramitesNoRadicado);
                this.listaPrediosTramites.add(filaPredioTramite);
                filaPredioTramite = new NumeroPredialListaTramiteDTO();
            }
        } catch (Exception e) {
            msg = new FacesMessage(
                "No se puede generar la consulta porque no tiene Convenios Asociados ");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

    }

    /**
     * guarda los predios a los que se les genero el formulario de SBC
     *
     * @author javier.barajas
     * @throws Exception
     */
    public void guardarFormularioSBC() {
        if (!this.listaPrediosTramitesSelect.isEmpty()) {

            this.generacionFormulario = new GeneracionFormularioSbc();
            Documento documento = new Documento();
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.OFICIOS_REMISORIOS.getId());
            documento.setTipoDocumento(tipoDocumento);
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setFechaDocumento(new Date(System.currentTimeMillis()));
            documento.setArchivo("TEMPORAL");
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());

            Actualizacion actualizacion = this.getActualizacionService().
                buscarActualizacionPorIdConConveniosYEntidades(this.actualizacionSeleccionada.
                    getId());

            this.generacionFormulario.setActualizacion(this.actualizacionSeleccionada);
            this.generacionFormulario.setCantidadPredios(
                (Long) (long) this.selectNumeroPredioM.length);
            this.generacionFormulario.setDocumento(documento);
            this.generacionFormulario.setFechaGeneracion(new Date(System.currentTimeMillis()));
            this.generacionFormulario.setFechaLog(new Date(System.currentTimeMillis()));
            this.generacionFormulario.setUsuarioLog(this.usuario.getLogin());
            try {
                this.generacionFormulario = this.getActualizacionService().
                    guardarGeneracionFormulariosSbc(this.actualizacionSeleccionada.getId(),
                        documento, this.generacionFormulario);
            } catch (Exception e) {
                LOGGER.error("Error al guardar los generacion formularios Sbc ", e);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "No se pudo guardar en la Base de Datos la Generación de Formularios ",
                    "No se pudo guardar en la Base de Datos la Generación de Formularios");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            this.prediosFormularioSbc = new ArrayList<PredioFormularioSbc>();
            for (NumeroPredialListaTramiteDTO numPrediales : this.listaPrediosTramitesSelect) {
                PredioFormularioSbc prediosFormularios = new PredioFormularioSbc();
                prediosFormularios.setGeneracionFormularioSbc(this.generacionFormulario);
                prediosFormularios.setPredio(this.getConservacionService().getPredioByNumeroPredial(
                    numPrediales.getNumeroPredial()));
                prediosFormularios.setFechaLog(new Date(System.currentTimeMillis()));
                prediosFormularios.setUsuarioLog(this.usuario.getLogin());
                this.prediosFormularioSbc.add(prediosFormularios);
            }
            try {
                this.prediosFormularioSbc = this.getActualizacionService().
                    guardarYactualizarPredioFormularioSbc(this.prediosFormularioSbc);
                FacesMessage msg = new FacesMessage(
                    "SE HA GUARDADO LA GENERACION DE FORMULARIOS SBC!!!:");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                this.btnHabilitaGenerar = true;
                generarFormularios();

            } catch (Exception e) {
                LOGGER.error("Error al guardar los PREDIOS formularios Sbc ", e);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Ocurrio un error guardando los predios a generar Formularios SBC ",
                    "Ocurrio un error guardando los predios a generar Formularios SBC");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

            if (!this.reporteDTO.getRutaCargarReporteAAlfresco().isEmpty() ||
                this.reporteDTO.getRutaCargarReporteAAlfresco() != null) {
                try {
                    // SubE el archivo a Alfresco y actualizo el documento
                    documento.setArchivo(this.reporteDTO.getRutaCargarReporteAAlfresco());
                    try {
                        documento = this.getActualizacionService().
                            guardarDocumentoDeActualizacionEnAlfresco(documento, actualizacion,
                                usuario);
                    } catch (Exception e) {
                        LOGGER.error("No se pudo guardar el documento en Alfresco", e);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_WARN,
                            "No se pudo guardar el documento en Alfresco",
                            "No se pudo guardar el documento en Alfresco"));
                    }
                    ActualizacionDocumento docActualizacion = new ActualizacionDocumento();
                    docActualizacion.setActualizacion(this.actualizacionSeleccionada);
                    docActualizacion.setFechaLog(new Date(System.currentTimeMillis()));
                    docActualizacion.setFueGenerado(true);
                    docActualizacion.setDocumento(documento);
                    docActualizacion.setUsuarioLog(this.usuario.getLogin());
                    try {
                        docActualizacion = this.getActualizacionService().
                            guardarYActualizarActualizacionDocumento(docActualizacion);
                    } catch (Exception e) {
                        LOGGER.
                            error("No se pudo guardar el documento en Actualizacion Documento", e);
                    }

                } catch (Exception e) {
                    LOGGER.error("No se pudo guardar el documento en Alfresco", e);
                }
            } else {
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "No se pudo guardar el documento en Alfresco ",
                    "No se pudo guardar el documento en Alfresco");
                LOGGER.warn(
                    "No se pudo guardar el documento en Alfresco: this.reporteDTO.getRutaCargarReporteAAlfresco() == null");
                FacesContext.getCurrentInstance().addMessage(null, msg1);
            }
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
                "NO HA SELECCIONADO PREDIOS", "NO HA SELECCIONADO PREDIOS");
            LOGGER.warn("NO HA SELECCIONADO PREDIOS: this.listaPrediosTramitesSelect.isEmpty()");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    /**
     * Método ejecutado cargar los barrios de la actualizacion
     *
     * @author javier.barajas
     *
     */
    public void cargarBarriosVisor() {

        Actualizacion tempActualizacion = new Actualizacion();
        tempActualizacion = this.getActualizacionService().getActualizacionConResponsables(
            this.actualizacionSeleccionada.getId());
        this.codigoMunicipio = tempActualizacion.getMunicipio().getCodigo();
        List<Barrio> listaBarrios = new ArrayList<Barrio>();
        listaBarrios = this.getGeneralesService().
            getNumeroManzanasPorMunicipio(this.codigoMunicipio);
        this.numeroBarriosMunicipio = listaBarrios.size();
        this.adicionarDatosVitem(this.selectListaBarrios, listaBarrios);

    }

    /**
     * Método ejecutado cargar manzanas oara el visor
     *
     * @author javier.barajas
     *
     */
    public void cargarManzanasVisor() {
        List<ManzanaVereda> listaManzana = new ArrayList<ManzanaVereda>();
        listaManzana = this.getGeneralesService().getManzanaVeredaFromBarrio(this.seleccionBarrio);
        int i = 0;
        if (listaManzana.size() > 0) {
            for (ManzanaVereda mv : listaManzana) {
                if (i == 0) {
                    this.codigoPrediosParaVisor = mv.getCodigo();
                    if (listaManzana.size() > 1) {
                        this.codigoPrediosParaVisor = this.codigoPrediosParaVisor + ",";
                    }

                } else {
                    this.codigoPrediosParaVisor = this.codigoPrediosParaVisor + mv.getCodigo();
                    if (i < listaManzana.size() - 1) {
                        this.codigoPrediosParaVisor = this.codigoPrediosParaVisor + ",";

                    }

                }
                i++;
            }

        }

    }

    /**
     * Carga los barrios y manzanas de la actualizacion
     *
     * @return
     *
     * @author javier.barajas
     */
    public String cargarBarriosyManzanas() {
        String barrioAnt = new String();
        String barriosAcargar = new String();
        this.listaManzanasVeredas = new ArrayList<ManzanaVereda>();

        for (int i = 0; i < this.listaNumerosPrediales.size(); i++) {
            if (i == 0) {
                barriosAcargar = this.listaNumerosPrediales.get(i).substring(0, 13);
                barrioAnt = this.listaNumerosPrediales.get(i).substring(0, 13);
                this.listaManzanasVeredas.addAll(this.buscarCodigosParaVisor(
                    this.listaNumerosPrediales.get(i).substring(0, 13)));
                if (this.listaNumerosPrediales.size() > 1) {
                    barriosAcargar = barriosAcargar + ",";
                }
            } else {
                if (!barrioAnt.equals(this.listaNumerosPrediales.get(i).substring(0, 13))) {
                    barriosAcargar = barriosAcargar + this.listaNumerosPrediales.get(i).substring(0,
                        13);
                    barrioAnt = this.listaNumerosPrediales.get(i).substring(0, 13);
                    this.listaManzanasVeredas.addAll(this.buscarCodigosParaVisor(
                        this.listaNumerosPrediales.get(i).substring(0, 13)));
                    if (i < this.listaNumerosPrediales.size() - 1) {
                        barriosAcargar = barriosAcargar + ",";

                    }
                }
            }

        }
        return barriosAcargar;

    }

    /**
     * Metodo para mostrar los predios de la seleccion del visor
     *
     * @author javier.barajas
     */
    public void seleccionVisorManzanas() {
        String[] manzanasSelectVisor = null;
        List<Predio> listaPrediosTemp = new ArrayList<Predio>();
        NumeroPredialListaTramiteDTO predioTramiteTemp = new NumeroPredialListaTramiteDTO();
        this.listaPrediosTramites = new ArrayList<NumeroPredialListaTramiteDTO>();
        List<Tramite> tramiteTemp = new ArrayList<Tramite>();
        if (!this.seleccionVisor.isEmpty()) {
            this.rederedConsultaVisor = true;
            manzanasSelectVisor = this.seleccionVisor.split(",");
            this.numeroManzanasSeleccionadas = manzanasSelectVisor.length;
            listaPrediosTemp = this.getActualizacionService().buscarPrediosTramitePorNumeroManzana(
                manzanasSelectVisor);
            for (Predio predioTemp : listaPrediosTemp) {
                tramiteTemp = this.getTramiteService().buscarNoRadicadosTramiteporNumeroPredial(
                    predioTemp.getNumeroPredial());
                predioTramiteTemp.setNumeroPredial(predioTemp.getNumeroPredial());
                predioTramiteTemp.setTramitesAsociados(tramiteTemp);
                this.listaPrediosTramites.add(predioTramiteTemp);
                this.adicionaPredioVisor(predioTramiteTemp);
                predioTramiteTemp = new NumeroPredialListaTramiteDTO();
                tramiteTemp.clear();
            }
            this.numeroPrediosSeleccionados = this.listaPrediosTramites.size();
        } else {

        }
    }

    /**
     * Obtiene los codigos para mostrar por visor
     *
     * @param barrio
     * @return
     *
     * @author javier.barajas
     */
    public List<ManzanaVereda> buscarCodigosParaVisor(String barrio) {
        return this.getActualizacionService().obtenerManzanasPorZonaActualizacion(barrio);
    }

    /**
     * Permite saber si ya se genero el reporte
     *
     * @param reporteGeneradoOK
     *
     * @author javier.barajas
     */
    public void setReporteGeneradoOK(boolean reporteGeneradoOK) {
        this.reporteGeneradoOK = reporteGeneradoOK;
    }

    /**
     * genera los reportes de generacion de formularios SBC
     *
     * @author javier.barajas
     */
    public void generarFormularios() {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.GENERACION_FORMULARIO_SBC;
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("CODIGO_GENERACION_FORMULARIO_SBC", String.valueOf(this.generacionFormulario.
            getId()));
        parameters.put("COD_ESTR_ORG", this.usuario.getCodigoEstructuraOrganizacional());
        try {
            this.reporteDTO = this.reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);
        } catch (Exception e) {
            LOGGER.debug("Error al generar el Reporte------------------------");
        }
    }

    /**
     * Convierte una lista de Barrios en una lista selectitem
     *
     * @param lista
     * @param listBarrio
     * @author javier.barajas
     */
    private void adicionarDatosVitem(List<SelectItem> lista,
        List<Barrio> listBarrio) {
        int i = 1;

        for (Barrio barrio : listBarrio) {
            lista.add(new SelectItem(barrio.getCodigo(), String.valueOf(i)));
            i++;
        }
    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("generarFormularioSbc");
        this.tareasPendientesMB.init();
        //this.init();
        return "index";
    }

    /**
     * Cuando se selecciona un nuevos predios
     *
     * @author javier.barajas
     */
    public void seleccionPredioDeResultadosBusquedaListener(SelectEvent event) {

        NumeroPredialListaTramiteDTO objetoSeleccionado = new NumeroPredialListaTramiteDTO();
        objetoSeleccionado = (NumeroPredialListaTramiteDTO) event.getObject();
        this.adicionaPredioVisor(objetoSeleccionado);
        this.rederedConsultaVisor = true;
        this.btnHabilitaGenerar = true;

    }

    /**
     * Cuando se selecciona un nuevos predios
     *
     * @author javier.barajas
     */
    public void adicionaPredioVisor(NumeroPredialListaTramiteDTO objetoSeleccionado) {

        boolean estaSelect = false;

        //Verifica si hay objetos en la lista
        if (this.listaPrediosTramitesSelect.size() > 0) {
            this.btnHabilitaGenerar = true;
            //busca si hay uno igual seleccionado
            for (NumeroPredialListaTramiteDTO npt : this.listaPrediosTramitesSelect) {
                if (npt.equals(objetoSeleccionado)) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Ya esta seleccionado este predio", "Ya esta seleccionado este predio");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    estaSelect = true;
                }
            }
            //si no hay ninguno seleccionado adiciona
            if (!estaSelect) {
                this.listaPrediosTramitesSelect.add(objetoSeleccionado);
                this.renderedLimpiaBtn = true;
            }
        } else {

            this.listaPrediosTramitesSelect.add(objetoSeleccionado);
            this.renderedLimpiaBtn = true;
        }
    }

    /**
     * Limpia la lista de seleccion
     *
     * @author javier.barajas
     */
    public void limpiarSeleccion() {
        this.listaPrediosTramitesSelect.clear();
        this.listaPrediosTramitesSelect = new ArrayList<NumeroPredialListaTramiteDTO>();
        this.selectNumeroPredioM = null;
        this.renderedLimpiaBtn = false;
        LOGGER.debug("LIMPIO SELECCION-------------");
    }

    /**
     * Verifica y muestra si se han seleccionado predios
     *
     * @author javier.barajas
     */
    public void mirar() {
        if (this.listaPrediosTramitesSelect.size() == 0) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
                "No ha se han seleccionado ningun predio", "No ha se han seleccionado ningun predio");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

    }

    /**
     * buscqueda de predio por Cadena(String)
     */
    public void buscarporCadena() {
        String zona = new String();
        String municipio = new String();

        //Si la cadena no esta vacia ni nula
        if (!this.cadenaBusqueda.equals(null) && !this.cadenaBusqueda.isEmpty()) {
            int tamano = this.cadenaBusqueda.length();
            LOGGER.debug("TAMANO DE LA CADENA" + tamano);
            //Si cumple con el numero de digitos tales como manzana, terreno etc..
            if (tamano == 17 || tamano == 21 || tamano == 22 || tamano == 24 || tamano == 26 ||
                tamano == 30) {

                municipio = this.cadenaBusqueda.substring(0, 5);
                //si la cadena de busqueda tiene los mismos codigos de la actualizacion	 
                if (this.cadenaBusqueda.substring(0, 5).equals(this.actualizacionSeleccionada.
                    getMunicipio().getCodigo())) {
                    //saca el codigo de la zona (rural, urbano)			
                    if (this.actualizacionSeleccionada.getZona().equals("RURAL")) {
                        zona = "00";
                    } else {
                        zona = "01";
                    }
                    if (tamano >= 17) {
                        NumeroPredialListaTramiteDTO temporal = new NumeroPredialListaTramiteDTO();
                        List<Predio> listaBusqueda = new ArrayList<Predio>();
                        try {
                            listaBusqueda = this.getConservacionService().
                                buscarPredioGenereacionFormularioSBCporCadena(this.cadenaBusqueda);
                            List<NumeroPredialListaTramiteDTO> selecTabla =
                                new ArrayList<NumeroPredialListaTramiteDTO>();
                            if (!listaBusqueda.isEmpty() || (listaBusqueda.size() > 0)) {
                                for (Predio p : listaBusqueda) {
                                    temporal.setNumeroPredial(p.getNumeroPredial());
                                    temporal.setTramitesAsociados(this.getTramiteService().
                                        buscarNoRadicadosTramiteporNumeroPredial(p.
                                            getNumeroPredial()));
                                    this.listaPrediosTramitesSelect.add(temporal);
                                    selecTabla.add(temporal);
                                }
                                this.seleccionarPrediosTabla(selecTabla);
                            } else {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                                    FacesMessage.SEVERITY_WARN,
                                    "No se encontro predios con la cadena de busqueda",
                                    "No se encontro predios con la cadena de busqueda"));
                            }
                        } catch (Exception e) {
                            LOGGER.error("+++++++++No se pudo hacer la consulta");
                        }

                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_WARN,
                            "Se recomienda refinar la busqueda por manzana",
                            "Se recomienda refinar la busqueda por manzana"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_WARN,
                        "Los codigos ingresados no corresponden al mismo municipio de la actualizacion",
                        "Los codigos ingresados no corresponden al mismo municipio de la actualizacion"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "La cadena ingresada no cumple con el tamaño adecuado de busqueda, Se recomienda refinar la busqueda",
                    "La cadena ingresada no cumple con el tamaño adecuado de busqueda, Se recomienda refinar la busqueda"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No ha ingresado ninguna cadena de busqueda",
                "No ha ingresado ninguna cadena de busqueda"));
        }

    }

    /**
     * genera los reportes de generacion de formularios SBC en blanco
     *
     * @author javier.barajas
     */
    public void generarFormularioenBlanco() {

        EReporteServiceSNC enumeracionReporte =
            EReporteServiceSNC.GENERACION_FORMULARIO_SBC_EN_BLANCO;
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("ACTUALIZACION_ID", String.valueOf(this.actualizacionSeleccionada.getId()));
        parameters.put("COD_ESTR_ORG", this.usuario.getCodigoEstructuraOrganizacional());
        try {
            this.reporteDTO = this.reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);
        } catch (Exception e) {
            LOGGER.error("No se pudo generar el Forumlario SBC en blanco");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo generar el Forumlario SBC en blanco",
                "No se pudo generar el Forumlario SBC en blanco"));
        }
    }

    /**
     * Muestra los predios seleccionados por busqueda en la tabla principal
     *
     * @author javier.barajas
     *
     */
    public void seleccionarPrediosTabla(List<NumeroPredialListaTramiteDTO> selectPredio) {
        this.selectNumeroPredioM = new NumeroPredialListaTramiteDTO[selectPredio.size()];
        for (int r = 0; r < selectPredio.size(); r++) {

            this.selectNumeroPredioM[r] = this.listaPrediosTramites.get(r);
        }
    }

    public void cargaFiltroVisor() {
        FiltroDatosConsultaPredio consultaD = new FiltroDatosConsultaPredio();

        Municipio m = this.getGeneralesService().getCapitalDepartamentoByCodigoDepartamento(
            this.actualizacionSeleccionada.getDepartamento().getCodigo());
        consultaD.setMunicipioId(m.getCodigo());

        consultaD.setNumeroPredialS1(consultaD.getDepartamentoId());
        consultaD.setNumeroPredialS2(consultaD.getMunicipioId().substring(2));
        this.setDatosConsultaPredio(consultaD);
    }
}
