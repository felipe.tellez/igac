package co.gov.igac.snc.web.jobs;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Este Job realiza las siguientes operaciones: Consulta los reportes generados a través de la base
 * de datos que ya terminaron y los envía a renderizar a través del servidor de reportes
 *
 *
 * @author juan.mendez
 *
 */
public class ConsultarReportesBDRunnableJob extends AbstractLocator implements Runnable,
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8672381097159518118L;

    /**
     *
     */
    private static final Logger LOGGER = Logger.getLogger(ConsultarReportesBDRunnableJob.class);

    private TaskExecutor taskExecutor;

    private Long numeroJobs;

    public void setNumeroJobs(Long cantidadJobs) {
        this.numeroJobs = cantidadJobs;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void run() {
        try {
            //TODO: consultar los reportes de base de datos que ya terminaron de forma satisfactoria

            List<RepReporteEjecucion> jobsPendientes = this.getGeneralesService().
                obtenerJobsReporteEjecucionTerminado(this.numeroJobs);

            if (jobsPendientes != null && jobsPendientes.size() > 0) {
                for (RepReporteEjecucion job : jobsPendientes) {
                    //envía los jobs a la cola (Dicha cola tiene un pool de instancias que se ejecutan en paralelo )
                    this.taskExecutor.execute(new EjecutarReporteJobTask(job));
                }
            } else {
                LOGGER.warn("No se encontraron jobs de reportes pendientes por renderizar");
            }
            //TODO: consultar los reportes de base de datos que  terminaron con error
            //TODO: qué hacer en ese caso??

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
