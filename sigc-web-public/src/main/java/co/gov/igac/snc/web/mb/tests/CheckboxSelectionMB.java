/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tests;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pagm
 */
@Component("checkboxSelection")
@Scope("session")
public class CheckboxSelectionMB implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckboxSelectionMB.class);

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private UsuarioDTO loggedInUser;

    private List<String> names;

    private String[] selectedNames;

    public String[] getSelectedNames() {
        return this.selectedNames;
    }

    public void setSelectedNames(String[] selectedName) {
        this.selectedNames = selectedName;
    }

    public List<String> getNames() {
        return this.names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public CheckboxSelectionMB() {

    }

    @PostConstruct
    public void init() {

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        this.names = new ArrayList<String>();
        this.names.add("lola");
        this.names.add("ramona");
        this.names.add("florinda");

    }

    public void rowSelectedListener() {
        LOGGER.debug("en el listener de selección de fila");
    }

    public UsuarioDTO getLoggedInUser() {
        return this.loggedInUser;
    }

    public void setLoggedInUser(UsuarioDTO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

}
