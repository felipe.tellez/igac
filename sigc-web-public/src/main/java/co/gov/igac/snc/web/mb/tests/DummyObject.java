/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tests;

/**
 *
 * @author pagm
 */
public class DummyObject {

    private String dato;
    private boolean validado;
    private boolean modificado;

    public DummyObject(String dato) {
        this.dato = dato;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public boolean isValidado() {
        return validado;
    }

    public void setValidado(boolean validado) {
        this.validado = validado;
    }

    public boolean isModificado() {
        return modificado;
    }

    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

}
