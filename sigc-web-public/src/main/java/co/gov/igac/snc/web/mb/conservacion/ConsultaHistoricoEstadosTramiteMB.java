/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Managed bean para la consulta del histórico de estados de un trámite El número de radicación debe
 * ser pasado como parámetro
 *
 * @author pedro.garcia
 */
@Component("consultaHistoricoEstadosTramite")
@Scope("request")
public class ConsultaHistoricoEstadosTramiteMB {

}
