/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el caso de uso Aplicar cambios CU-NP-CO-167
 *
 * @author pedro.garcia
 */
@Component("aplicacionCambios")
@Scope("session")
public class AplicacionCambiosMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -2540852432560354900L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AplicacionCambiosMB.class);

    private String mbComponentName = "aplicacionCambios";

    // ------------------ services ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * trámites sobre los que se está trabajando
     */
    private List<Tramite> currentTramites;

    /**
     * trámites sobre los que se está trabajando
     */
    private Tramite[] tramitesParaAplicar;

    /**
     * trámite seleccionado de la tabla
     */
    private Tramite selectedTramite;

    /**
     * trámite seleccionado en el link de número de resolución
     */
    private Tramite selectedTramiteResolucion;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    // ----- BPM --------
    /**
     * actividad que corresponde al trámite seleccionado
     */
    private Actividad selectedTramiteActivity;

    /**
     * ids de los trámites que me devuelve el bpm
     */
    private long[] currentTramitesIds;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de resolución
     */
    private ReporteDTO reporteResolucion;

    //----------------------   banderas  -----
    /**
     * indica si se aplicó cambios para algún trámite con el fin de determinar si hay que volver a
     * consultar los que están pendientes para ello
     */
    private boolean aplicoCambios;

    // ------------- methods ----
    public Tramite[] getTramitesParaAplicar() {
        return tramitesParaAplicar;
    }

    public void setTramitesParaAplicar(Tramite[] tramitesParaAplicar) {
        this.tramitesParaAplicar = tramitesParaAplicar;
    }

    public Tramite getSelectedTramite() {
        return this.selectedTramite;
    }

    public void setSelectedTramite(Tramite selectedTramite) {
        this.selectedTramite = selectedTramite;
        if (this.selectedTramite != null) {
            List<TramiteDocumento> documentosPruebas = this.getTramiteService()
                .obtenerTramiteDocumentosDePruebasPorTramiteId(
                    this.selectedTramite.getId());
            if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                this.selectedTramite.setDocumentosPruebas(documentosPruebas);
            }
        }
    }

    public Tramite getSelectedTramiteResolucion() {
        return selectedTramiteResolucion;
    }

    public void setSelectedTramiteResolucion(Tramite selectedTramiteResolucion) {
        this.selectedTramiteResolucion = selectedTramiteResolucion;
    }

    public List<Tramite> getCurrentTramites() {
        if (this.aplicoCambios) {
            obtenerTramitesActividad();
            //N como el get se ejecuta varias veces, se pone en false para evitar consultas innecesarias
            this.aplicoCambios = false;
        }
        return this.currentTramites;
    }

    public void setCurrentTramites(ArrayList<Tramite> currentTramites) {
        this.currentTramites = currentTramites;
    }

    public ReporteDTO getReporteResolucion() {
        return this.reporteResolucion;
    }

    public void setReporteResolucion(ReporteDTO reporteResolucion) {
        this.reporteResolucion = reporteResolucion;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on AplicacionCambiosMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        // D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "aplicacionCambios");

        this.aplicoCambios = false;
        obtenerTramitesActividad();

    }

//--------------------------------------------------------------------------------------------------
    /*
     * @modified david.cifuentes - Se adicionó el llamado al procedimiento que confirma la
     * proyección. @modified pedro.garcia 06-09-2013 cambio en el método que se invoca para aplicar
     * cambios 09-09-2013 manejo de respuesta de método de aplicar cambios de proyección @modified
     * felipe.cadena :: 15-09-2014 :: Se agrega nuevo manejo de aplicacion de cambios para tramites
     * no geograficos
     */
    private void aplicarCambiosIndividual() {

        boolean aplicacionCambiosProyeccionOK;
        this.selectedTramite = this.getTramiteService().buscarTramiteCompletoConResolucion(
            this.selectedTramite.getId());

        if (this.selectedTramite != null) {
            Documento docResolucion = this.selectedTramite.getResultadoDocumento();

            if (this.selectedTramite.getResultadoDocumento() == null) {
                this.addMensajeError(
                    "No se encuentra una resolución asociada al trámite, no se pueden aplicar cambios");
                return;
            }

            if (docResolucion.getNumeroDocumento() != null &&
                 !docResolucion.getNumeroDocumento().trim().isEmpty() &&
                 this.selectedTramite != null) {
                this.actualizarAnioYNumeroUltimaResolucion(docResolucion.getNumeroDocumento());
            }

            try {

                this.aplicarCambiosInicio(this.selectedTramite.getId(), this.currentUser);
                aplicacionCambiosProyeccionOK = true;

                if (aplicacionCambiosProyeccionOK) {
                    this.addMensajeInfo(
                        "Se inició el proceso que aplica los cambios en la base de datos. El resultado será notificado mediante correo electrónico. Trámite : " +
                         this.selectedTramite.getNumeroRadicacion());
                } else {
                    this.addMensajeWarn(
                        "Hubo algún problema aplicando los cambios. Revise por favor el correo electrónico.  Trámite : " +
                         this.selectedTramite.getNumeroRadicacion());
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError(
                    "Hubo algún problema aplicando los cambios. Revise por favor el correo electrónico.  Trámite : " +
                     this.selectedTramite.getNumeroRadicacion());
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aplicar cambios" en la página principal del caso de uso
     *
     * @modified felipe.cadena :: 17-04-2015:: Se actualiza para permitir la seleccion masiva de los
     * tramites para aplicar cambios
     */
    public void aplicarCambios() {

        if (this.tramitesParaAplicar == null || this.tramitesParaAplicar.length == 0) {
            this.addMensajeError("Debe seleccionar al menos un trámite para aplicar cambios");
        }

        for (Tramite t : this.tramitesParaAplicar) {

            this.selectedTramite = t;
            this.aplicarCambiosIndividual();
            this.currentTramites.remove(t);
        }
        this.tramitesParaAplicar = null;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link con el número del documento resultado para abrir la ventana donde se muestra
     */
    public void consultarDocResolucion() {

        Documento docResolucion;
        String idGestorDocumental;

        if (this.selectedTramiteResolucion.getResultadoDocumento() != null) {
            docResolucion = this.selectedTramiteResolucion
                .getResultadoDocumento();
            idGestorDocumental = docResolucion.getIdRepositorioDocumentos();

            // D: se obtiene la url del archivo (el método lo recupera del
            // alfresco y retorna la url)
            // pero hay que adicionarle la ruta del contexto de archivos
            this.reporteResolucion = this.reportsService.consultarReporteDeGestorDocumental(
                idGestorDocumental);

        } else {
            this.addMensajeError("No se encuentra un documento de resolución asociado al trámite.");
        }
    }

    // --------------------------------------------------------------------------------------------------
    // ----------- bpm
    /**
     * no se debe validar nada en cuanto al proceso
     *
     * @return
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }

    // --------------------------------------------------------------------------------------------------
    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;

        solicitudCatastral = new SolicitudCatastral();
        messageDTO = new ActivityMessageDTO();
        usuariosActividad = new ArrayList<UsuarioDTO>();
        usuariosActividad.add(this.currentUser);

        // Obtener la actividad actual:
        this.selectedTramiteActivity = this.tareasPendientesMB
            .buscarActividadPorIdTramite(this.selectedTramite.getId());

        processTransition = ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
        messageDTO.setActivityId(this.selectedTramiteActivity.getId());
        observaciones = "El trámite sale de la actividad Aplicar cambios.";
        messageDTO.setComment(observaciones);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setUsuarios(usuariosActividad);
        messageDTO.setSolicitudCatastral(solicitudCatastral);

        this.setMessage(messageDTO);

    }

    // --------------------------------------------------------------------------------------------------
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {
        // Una vez se ejecuta el procedimiento que aplica los cambios en el
        // trámite, éste por debajo realiza todos las modificaciones en la base
        // de datos, por lo que all legar a este método ya se han realizado los
        // cambios respectivos, y por ello está vacio.
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * avanza el proceso para el trámite seleccionado de la tabla
     */
    private boolean avanzarProceso() {

        boolean answer = false;

        if (this.validateProcess()) {
            this.setupProcessMessage();

            try {
                this.doDatabaseStatesUpdate();
                this.forwardProcess();

                answer = true;
            } catch (Exception e) {
                LOGGER.error("error al mover proceso en AplicacionCambiosMB: " +
                     e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en procesos. Comuníquese con el " +
                     " administrador del sistema");
                throw SNCWebServiceExceptions.EXCEPCION_0008.getExcepcion(
                    LOGGER, e, this.selectedTramite.getId(),
                    this.selectedTramite.getNumeroRadicacion(),
                    this.selectedTramiteActivity);
            }
        }
        return answer;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrescan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        this.tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean(this.mbComponentName);
        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que actualiza los datos correspondientes al documento de resolución en la entidad
     * PPredio en particular actualiza el año de la ultima resolución y el número de la última
     * resolución
     *
     * @author javier.aponte
     * @param numeroResolución número de la última resolución que se generó producto del trámite
     */
    private void actualizarAnioYNumeroUltimaResolucion(String numeroResolucion) {

        Calendar calendario = Calendar.getInstance();

        int anioUltimaResolucion = calendario.get(Calendar.YEAR);

        try {

            Object[] errors = this.getConservacionService()
                .establecerAnioYNumeroUltimaResolucion(this.selectedTramite.getId(),
                    anioUltimaResolucion, numeroResolucion, this.currentUser);
            if (errors == null || errors.length == 0) {
                LOGGER.info(
                    "Se actualizaron correctamente los campos de número y anio última resolución.");
            } else if (errors.length > 0) {
                String mensajeError =
                    "Error al actualizar el anio y el número de última resolución para los predios" +
                    " del trámite " + this.selectedTramite.getId();
                LOGGER.error(mensajeError);
                return;
            }

        } catch (Exception e) {
            LOGGER.error("Error al actualizar el predio" + e, e);
            this.addMensajeError(
                "Ocurrió un error al actualizar el año y el número de última resolución para el trámite" +
                " con número de radicación: " + this.selectedTramite.getNumeroRadicacion());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace las consultas necesarias para obtener los trámites que se muestran en la tabla de
     * trámites a los que se puede aplicar cambios.
     *
     * @author pedro.garcia
     */
    private void obtenerTramitesActividad() {

        // D: se obtienen los ids de los trámites de trabajo
        this.currentTramitesIds = this.tareasPendientesMB
            .obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        // D: se arma la lista de trámites
        this.currentTramites = (ArrayList<Tramite>) this.getTramiteService().
            buscarTramitesCompletosConResolucion(this.currentTramitesIds);

        /*
         * Se excluyen los trámites que se encuentran en aplicar cambios en la parte geográfica
         */
        List<Tramite> tramitesEnNoAplicarCambios = new ArrayList<Tramite>();
        if (this.currentTramites != null && !this.currentTramites.isEmpty()) {
            for (Tramite tram : this.currentTramites) {
                if (tram.getEstadoGdb() == null || !tram.getEstadoGdb().equals(
                    Constantes.ACT_APLICAR_CAMBIOS)) {
                    tramitesEnNoAplicarCambios.add(tram);
                }
            }
        }
        this.currentTramites = tramitesEnNoAplicarCambios;

        if (this.currentTramites != null && !this.currentTramites.isEmpty()) {
            for (Tramite tram : this.currentTramites) {
                List<TramiteDocumento> documentosPruebas = this
                    .getTramiteService()
                    .obtenerTramiteDocumentosDePruebasPorTramiteId(
                        tram.getId());
                if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                    tram.setDocumentosPruebas(documentosPruebas);
                }
            }
        }

        //D: si la variable es true se pone en false para evitar que se haga esto cada vez que se hace el get de los trámites
        if (this.aplicoCambios) {
            this.aplicoCambios = false;
        }
    }

    /**
     * Metodo para iniciar la aplicacion de cambios
     *
     * @author felipe.cadena
     * @param tramiteId
     * @param usuario
     */
    public void aplicarCambiosInicio(Long tramiteId, UsuarioDTO usuario) {
        Tramite tramite = null;
        String message;
        LOGGER.debug("on AplicacionCambiosMB#aplicarCambiosInicio ...");

        try {
            //se marca el tramite como en aplicar cambios
            //tramite = this.selectedTramite;
            tramite = this.getTramiteService().buscarTramitePorId(tramiteId);
            tramite.setEstadoGdb(Constantes.ACT_APLICAR_CAMBIOS);
            tramite = this.getTramiteService().guardarActualizarTramite2(tramite);
            if (tramite.isRevisionAvaluo()) {
                List<ComisionTramiteDato> comisionesTramiteDatos;
                if (tramite.getComisionTramites() != null && !tramite.getComisionTramites().
                    isEmpty()) {
                    comisionesTramiteDatos = this.getTramiteService()
                        .buscarComisionTramiteDatoPorTramiteId(tramite.getId());

                    for (ComisionTramite ct : tramite.getComisionTramites()) {
                        ct.setComisionTramiteDatos(comisionesTramiteDatos);
                    }
                }
            }
        } catch (Exception ex) {
            message = "Error en AplicarCambiosMB#aplicarCambiosInicio buscando el " +
                 "trámite con id " + tramiteId.toString();
            LOGGER.error(message, ex);
            throw new ExcepcionSNC("aplicarCambiosInicio", "Error", message);
        }

        //inicia la aplicacion de cambios
        Actividad actTramite = this.tareasPendientesMB.buscarActividadPorIdTramiteSeleccionadas(
            tramiteId);
        if (actTramite == null) {
            actTramite = this.tareasPendientesMB.buscarActividadPorIdTramite(tramiteId);
        }
        tramite.setActividadActualTramite(actTramite);
        LOGGER.debug("on AplicacionCambiosMB#aplicarCambiosInicio ya tiene Actividad");

        //felipe.cadena::26-08-2015::Actualizacion::Se invoca aplcar cambios de actualizacion
        //solo desde el paso 1 ya que solo se aplica alfanumerico
        if (tramite.isActualizacion()) {
            this.getConservacionService().aplicarCambiosActualizacion(tramite, usuario, 1);
        } else {
            // jonathan.chacon :: Validaciones necesarias para predios fiscales ya que en 
            //el transient de tramite geográfico no se puede consultar el ppredio en caso de quintas que no
            //traen predio asociado al tramite
            if (tramite.getPredio() != null) {
                if (tramite.getPredio().getTipoCatastro().equals(
                    EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                    this.getConservacionService().aplicarCambiosConservacion(tramite, usuario, 1);
                } else {
                    //tramites geográficos
                    if (tramite.isTramiteGeografico()) {
                        this.getConservacionService().
                            aplicarCambiosConservacion(tramite, usuario, 0);
                    } else {
                        this.getConservacionService().
                            aplicarCambiosConservacion(tramite, usuario, 1);
                    }
                }
            } else {
                try {
                    PPredio pp = this.getConservacionService().obtenerPPredioCompletoByIdTramite(
                        tramite.getId());
                    if (pp.getTipoCatastro().
                        equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                        this.getConservacionService().
                            aplicarCambiosConservacion(tramite, usuario, 1);
                    } else {
                        //tramites geográficos
                        if (tramite.isTramiteGeografico()) {
                            this.getConservacionService().aplicarCambiosConservacion(tramite,
                                usuario, 0);
                        } else {
                            this.getConservacionService().aplicarCambiosConservacion(tramite,
                                usuario, 1);
                        }
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(AplicacionCambiosMB.class.getName()).log(
                        Level.SEVERE, null, ex);
                }
            }
        }
    }

// end of class
}
