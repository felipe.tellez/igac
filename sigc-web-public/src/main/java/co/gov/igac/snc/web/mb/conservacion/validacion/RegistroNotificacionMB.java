/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDescripcionTipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDocumentoMimeTipo;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteDocumentoMetoNotif;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.IServicioTemporalDocumento;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.ServicioTemporalDocumento;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.web.util.procesos.ProcesosUtilidadesWeb;
import java.util.Calendar;

/**
 * Managed bean para el CU Registrar notificación
 *
 * @author pedro.garcia
 */
@Component("registroNotificacion")
@Scope("session")
public class RegistroNotificacionMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = 1812305781945673352L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistroNotificacionMB.class);

    // ------------------ services ------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private IContextListener contextoDocs;

    @SuppressWarnings("unused")
    private IServicioTemporalDocumento servicioTemporalDocumento;

    /**
     * Se usa aquí solo para invocar su init al cerrar la ventana del cu
     */
    @Autowired
    private TareasPendientesMB tareasPendientes;

    /**
     * trámite sober el que se está trabajando
     */
    private Tramite currentTramite;

    /**
     * Copia del trámite sober el que se está trabajando
     */
    private Tramite tramiteTemp;

    /**
     * id del trámite seleccionado de la tabla
     */
    private long currentTramiteId;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO loggedInUser;

    /**
     * Filtro de búsquedaq de trámites
     */
    private FiltroDatosConsultaTramite filtroBusquedaTramite;

    /**
     * Llista con los valores del combo de tipos de trámite acorde al el tipo de solicitud
     * seleccionado
     */
    private ArrayList<SelectItem> tiposTramitePorSolicitud;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Nombre del documento temporal
     */
    private String nombreDocumentoTemporal;

    /**
     * Fecha del sistema para los diferentes documentos
     */
    private Date fechaLogYRadicacion;
    /**
     * ids de los tramiites que estan para ser notificados
     */
    private List<Long> iDsTramitesNotif;

    // ------- reportes ----------
    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de diligencia de notificación personal
     */
    private ReporteDTO reporteDiligenciaNotificacionPersonal;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDocumentoWaterMark;

    // ---------- banderas ---------
    /**
     * indica si el notificado es solicitante del trámite
     */
    private boolean notificadoEsSolicitante;

    /**
     * indica si el botón "guardar" que mueve el proceso y guarda los doc cargados se debe habilitar
     */
    private boolean disabledGuardarButton;

    /**
     * indican si los respectivos documentos se cargaron ya
     */
    private boolean docNotifCargado;
    private boolean docAutorizacionCargado;
    private boolean visualizarDocNotif;

    /**
     * indican si existe un trámite seleccionado para activar los datos del regístro de la
     * notificación
     */
    private boolean banderaDatosRegistroNotificacion;

    /**
     * Bandera de visualización de la tabla de resultado de búsqueda de trámites
     */
    private boolean banderaBusquedaTramites;

    /**
     * Bandera que indica si el documento de nofiticación ya fue generado
     */
    private boolean banderaDocNotificacionGenerado;

    /**
     * Bandera que indica si los datos del notificado son de solo lectura
     */
    private boolean banderaDatosDeNotificadoAlmacenados;

    /**
     * Bandera que indica si el campo Tipo de persona notificada es de solo lectura
     */
    private boolean banderaTipoPersonaNotificada;

    /**
     * indica si el proceso del trámite está listo para ser avanzado
     */
    private boolean okToForwardProcess;
    
    /**
     * Bandera que indica si el boton buscar
     */
    private boolean habilitaBuscar;

    // ---------------------------------------------------------------
    /**
     * indica si el reporte se generó de forma correcta. Hubo que usar este flag porque los métodos
     * valor de un action deben devolver void o String
     */
    private boolean reporteGeneradoOK;

    /**
     * valor del combo de tipo de persona notificada
     */
    private String tipoPersonaNotif;
    public static String tipoPersonaNotifSolicitante = "solicitante";
    public static String tipoPersonaNotifAutorizado = "autorizado";

    /**
     * lista completa de solicitantes del trámite: los de la solicitud y los de trámite
     */
    private List<Solicitante> solicitantesRegistroNotificacion;

    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;

    /**
     * lista de trámites paginados
     */
    private List<Tramite> rows;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites
     */
    private LazyDataModel<Tramite> lazyAssignedTramites;

    /**
     * Trámite documentación temporal
     */
    @SuppressWarnings("unused")
    private TramiteDocumentacion tramiteDocumentacionTemp;

    /**
     * Trámite documento temporal
     */
    @SuppressWarnings("unused")
    private TramiteDocumento tramiteDocumentoTemp;

    /**
     * Bandera que indica si todos los solicitantes del trámite renunciaron
     */
    private boolean solicitantesRenuncian;

    // ----------- atributos con valores a guardar ------
    private String tipoIdentificPersonaNotif;

    private String nombrePersonaNotif;

    private String numeroIdentificPersonaNotif;

    private String renunciaARecursos;

    private String formaNotificacion;

    private Date fechaNotificacion;

    /**
     * solicitante seleccionado de la tabla
     */
    private Solicitante selectedSolicitante;

    /**
     * solicitante seleccionado de la tabla
     */
    private Solicitante[] solicitantesSeleccionados;

    // --------- carga de archivos ------
    /**
     * nombre del tipo de archivo que se carga 'notificacion', 'autorizacion'
     */
    private String archivoACargar;

    private String nombreArchivoNotificacionCargado;

    private String nombreArchivoAutorizacionCargado;

    /**
     * objetos Documento que se van a guardar en la bd
     */
    private Documento documentoNotificacion;
    private Documento documentoAutorizacionDeNotificacion;
    private TramiteDocumento tramiteDocumentoNotificacion;
    private List<TramiteDocumento> tramiteDocumentosNotificacion;

    private static String tipoDocumentoNotificacion = "notificacion";
    private static String tipoDocumentoAutorizacionDeNotificacion = "autorizacion";
    private static String tipoDocumentoRadicarRecurso = "Registro de radicación del recurso";

    /**
     * Mensaje que informa que el trámite no es apto para registrar la notificación
     */
    private String mensajeTramiteNoAptoNotificar;

    /**
     * Mensaje que informa de la culminación exitosa de las notificaciones para el trámite
     * seleccionado
     */
    private String mensajeConfirmationAvanzarProceso;
    
    /**
     * Mensaje que confirma si el usuario logeado es aquel el que va realizar el regostro de 
     * la notificacion, ya que el tramite quedara asignado a el.
     */
    private String mensajeConfirmationAsignacionRegistro;

    /**
     * Documento en el que se carga la resolución para adicionarle la marca de agua.
     */
    private Documento documentoResolucion;

    /**
     * Variable {@link String} usada para almacenar el nombre de la {@link Actividad} para poder
     * realizar la búsqueda por filtros en el process.
     */
    private String nombreActividadSelected;

    /**
     * Lista de {@link SelectItem} con las posibles actividades a filtrar en la búsqueda del
     * {@link Tramite}
     */
    private List<SelectItem> actividadesFiltro;

    /**
     * Variable {@link Documento} que almacena el documento de radicación del recurso para el
     * solicitante seleccionado.
     */
    private Documento documentoRadicarRecurso;

    /**
     * Persona seleccionada cuando al buscar existe mas de una persona con la misma identificacion
     */
    private PPersona personaSeleccionada;

    private List<PPersona> ppersonasAux;

    // ----- BPM --------
    private Actividad currentProcessActivity;

//-------------------------  methods --------------

    public Tramite getTramiteTemp() {
        return tramiteTemp;
    }

    public void setTramiteTemp(Tramite tramiteTemp) {
        this.tramiteTemp = tramiteTemp;
    }

    public boolean isHabilitaBuscar() {
        return habilitaBuscar;
    }

    public void setHabilitaBuscar(boolean habilitaBuscar) {
        this.habilitaBuscar = habilitaBuscar;
    }
    
    public List<PPersona> getPpersonasAux() {
        return ppersonasAux;
    }

    public void setPpersonasAux(List<PPersona> ppersonasAux) {
        this.ppersonasAux = ppersonasAux;
    }

    public PPersona getPersonaSeleccionada() {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(PPersona personaSeleccionada) {
        this.personaSeleccionada = personaSeleccionada;
    }

    public boolean isOkToForwardProcess() {
        return this.okToForwardProcess;
    }

    public void setOkToForwardProcess(boolean okToForwardProcess) {
        this.okToForwardProcess = okToForwardProcess;
    }

    public ArrayList<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> answer = null;
        List<TipoSolicitudTramite> tiposTramite = null;
        SelectItem itemToAdd;

        if (this.filtroBusquedaTramite.getTipoSolicitud() != null) {
            tiposTramite = this.getTramiteService()
                .obtenerTiposDeTramitePorSolicitud(
                    this.filtroBusquedaTramite.getTipoSolicitud());
        }
        if (tiposTramite != null && !tiposTramite.isEmpty()) {
            answer = new ArrayList<SelectItem>();
            answer.add(new SelectItem("", this.DEFAULT_COMBOS));
            for (TipoSolicitudTramite tst : tiposTramite) {
                itemToAdd = new SelectItem(tst.getTipoTramite(),
                    tst.getTipoTramiteValor());
                answer.add(itemToAdd);
            }
        }
        this.tiposTramitePorSolicitud = answer;
        return this.tiposTramitePorSolicitud;
    }

    public void setTiposTramitePorSolicitud(ArrayList<SelectItem> tiposTramite) {
        this.tiposTramitePorSolicitud = tiposTramite;
    }

    public List<SelectItem> getSubtipoClaseMutacionV() {
        this.subtipoClaseMutacionV = new ArrayList<SelectItem>();
        this.subtipoClaseMutacionV.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.subtipoClaseMutacionV = this.generalMB
            .getSubtipoClaseMutacionV(this.filtroBusquedaTramite
                .getClaseMutacion());
        return subtipoClaseMutacionV;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public String getTipoMimeDocTemporal() {

        if (this.nombreDocumentoTemporal != null &&
             !this.nombreDocumentoTemporal.isEmpty()) {

            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            this.tipoMimeDocTemporal = fileNameMap
                .getContentTypeFor(this.nombreDocumentoTemporal);

            if (this.nombreDocumentoTemporal
                .equals(EDocumentoMimeTipo.DESCONOCIDO.getMime())) {

                if (this.nombreDocumentoTemporal
                    .endsWith(EDocumentoMimeTipo.DOC.getExtension())) {
                    this.tipoMimeDocTemporal = EDocumentoMimeTipo.DOC.getMime();
                } else if (this.nombreDocumentoTemporal
                    .endsWith(EDocumentoMimeTipo.DOCX.getExtension())) {
                    this.tipoMimeDocTemporal = EDocumentoMimeTipo.DOCX
                        .getMime();
                }
            }
        }
        return tipoMimeDocTemporal;
    }

    public String getMensajeConfirmationAsignacionRegistro() {
        return mensajeConfirmationAsignacionRegistro;
    }

    public void setMensajeConfirmationAsignacionRegistro(
        String mensajeConfirmationAsignacionRegistro) {
        this.mensajeConfirmationAsignacionRegistro = mensajeConfirmationAsignacionRegistro;
    }

    public boolean isBanderaTipoPersonaNotificada() {
        return banderaTipoPersonaNotificada;
    }

    public void setBanderaTipoPersonaNotificada(boolean banderaTipoPersonaNotificada) {
        this.banderaTipoPersonaNotificada = banderaTipoPersonaNotificada;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public boolean isReporteGeneradoOK() {
        return reporteGeneradoOK;
    }

    public void setReporteGeneradoOK(boolean reporteGeneradoOK) {
        this.reporteGeneradoOK = reporteGeneradoOK;
    }

    public Documento getDocumentoRadicarRecurso() {
        return documentoRadicarRecurso;
    }

    public void setDocumentoRadicarRecurso(Documento documentoRadicarRecurso) {
        this.documentoRadicarRecurso = documentoRadicarRecurso;
    }

    public boolean isBanderaDocNotificacionGenerado() {
        return banderaDocNotificacionGenerado;
    }

    public void setBanderaDocNotificacionGenerado(
        boolean banderaDocNotificacionGenerado) {
        this.banderaDocNotificacionGenerado = banderaDocNotificacionGenerado;
    }

    public boolean isVisualizarDocNotif() {
        return visualizarDocNotif;
    }

    public void setVisualizarDocNotif(boolean visualizarDocNotif) {
        this.visualizarDocNotif = visualizarDocNotif;
    }

    public ReporteDTO getReporteDocumentoWaterMark() {
        return reporteDocumentoWaterMark;
    }

    public void setReporteDocumentoWaterMark(ReporteDTO reporteDocumentoWaterMark) {
        this.reporteDocumentoWaterMark = reporteDocumentoWaterMark;
    }

    public boolean isBanderaBusquedaTramites() {
        return this.banderaBusquedaTramites;
    }

    public boolean isBanderaDatosDeNotificadoAlmacenados() {
        return this.banderaDatosDeNotificadoAlmacenados;
    }

    public void setBanderaDatosDeNotificadoAlmacenados(boolean banderaDatosDeNotificadoAlmacenados) {
        this.banderaDatosDeNotificadoAlmacenados = banderaDatosDeNotificadoAlmacenados;
    }

    public void setBanderaBusquedaTramites(boolean banderaBusquedaTramites) {
        this.banderaBusquedaTramites = banderaBusquedaTramites;
    }

    public Documento getDocumentoNotificacion() {
        return this.documentoNotificacion;
    }

    public void setDocumentoNotificacion(Documento documentoNotificacion) {
        this.documentoNotificacion = documentoNotificacion;
    }

    public String getNombreActividadSelected() {
        return this.nombreActividadSelected;
    }

    public void setNombreActividadSelected(String nombreActividadSelected) {
        this.nombreActividadSelected = nombreActividadSelected;
    }

    public List<SelectItem> getActividadesFiltro() {
        return this.actividadesFiltro;
    }

    public void setActividadesFiltro(List<SelectItem> actividadesFiltro) {
        this.actividadesFiltro = actividadesFiltro;
    }

    public Documento getDocumentoResolucion() {
        return documentoResolucion;
    }

    public void setDocumentoResolucion(Documento documentoResolucion) {
        this.documentoResolucion = documentoResolucion;
    }

    public Documento getDocumentoAutorizacionDeNotificacion() {
        return this.documentoAutorizacionDeNotificacion;
    }

    public void setDocumentoAutorizacionDeNotificacion(
        Documento documentoAutorizacionDeNotificacion) {
        this.documentoAutorizacionDeNotificacion = documentoAutorizacionDeNotificacion;
    }

    public String getMensajeTramiteNoAptoNotificar() {
        return this.mensajeTramiteNoAptoNotificar;
    }

    public void setMensajeTramiteNoAptoNotificar(
        String mensajeTramiteNoAptoNotificar) {
        this.mensajeTramiteNoAptoNotificar = mensajeTramiteNoAptoNotificar;
    }

    public String getMensajeConfirmationAvanzarProceso() {
        return this.mensajeConfirmationAvanzarProceso;
    }

    public void setMensajeConfirmationAvanzarProceso(String mensaje) {
        this.mensajeConfirmationAvanzarProceso = mensaje;
    }

    public String getNombreDocumentoTemporal() {
        return nombreDocumentoTemporal;
    }

    public void setNombreDocumentoTemporal(String nombreDocumentoTemporal) {
        this.nombreDocumentoTemporal = nombreDocumentoTemporal;
    }

    public Date getFechaNotificacion() {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    public FiltroDatosConsultaTramite getFiltroBusquedaTramite() {
        return filtroBusquedaTramite;
    }

    public void setFiltroBusquedaTramite(
        FiltroDatosConsultaTramite filtroBusquedaTramite) {
        this.filtroBusquedaTramite = filtroBusquedaTramite;
    }

    public LazyDataModel<Tramite> getLazyAssignedTramites() {
        return lazyAssignedTramites;
    }

    public void setLazyAssignedTramites(
        LazyDataModel<Tramite> lazyAssignedTramites) {
        this.lazyAssignedTramites = lazyAssignedTramites;
    }

    public String getNombreArchivoNotificacionCargado() {
        return this.nombreArchivoNotificacionCargado;
    }

    public void setNombreArchivoNotificacionCargado(
        String nombreArchivoNotificacionCargado) {
        this.nombreArchivoNotificacionCargado = nombreArchivoNotificacionCargado;
    }

    public String getNombreArchivoAutorizacionCargado() {
        return this.nombreArchivoAutorizacionCargado;
    }

    public void setNombreArchivoAutorizacionCargado(
        String nombreArchivoAutorizacionCargado) {
        this.nombreArchivoAutorizacionCargado = nombreArchivoAutorizacionCargado;
    }

    public String getArchivoACargar() {
        return this.archivoACargar;
    }

    public void setArchivoACargar(String archivoACargar) {
        this.archivoACargar = archivoACargar;
    }

    public boolean isDisabledGuardarButton() {
        return this.disabledGuardarButton;
    }

    public void setDisabledGuardarButton(boolean disabledGuardarButton) {
        this.disabledGuardarButton = disabledGuardarButton;
    }

    public boolean isBanderaDatosRegistroNotificacion() {
        return banderaDatosRegistroNotificacion;
    }

    public void setBanderaDatosRegistroNotificacion(
        boolean banderaDatosRegistroNotificacion) {
        this.banderaDatosRegistroNotificacion = banderaDatosRegistroNotificacion;
    }

    public Solicitante getSelectedSolicitante() {
        return this.selectedSolicitante;
    }

    public void setSelectedSolicitante(Solicitante selectedSolicitante) {
        this.selectedSolicitante = selectedSolicitante;
    }

    public String getTipoPersonaNotifSolicitante() {
        return RegistroNotificacionMB.tipoPersonaNotifSolicitante;
    }

    public String getTipoPersonaNotifAutorizado() {
        return RegistroNotificacionMB.tipoPersonaNotifAutorizado;
    }

    public List<TramiteDocumento> getTramiteDocumentosNotificacion() {
        return tramiteDocumentosNotificacion;
    }

    public void setTramiteDocumentosNotificacion(
        List<TramiteDocumento> tramiteDocumentosNotificacion) {
        this.tramiteDocumentosNotificacion = tramiteDocumentosNotificacion;
    }

    public Tramite getCurrentTramite() {
        return this.currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
        if (this.currentTramite != null) {
            List<TramiteDocumento> documentosPruebas = this.getTramiteService()
                .obtenerTramiteDocumentosDePruebasPorTramiteId(this.currentTramite.getId());
            if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                this.currentTramite.setDocumentosPruebas(documentosPruebas);
            }
        }
    }

    public List<Solicitante> getSolicitantesRegistroNotificacion() {
        return this.solicitantesRegistroNotificacion;
    }

    public void setSolicitantesRegistroNotificacion(
        ArrayList<Solicitante> solicitantesRegistroNotificacion) {
        this.solicitantesRegistroNotificacion = solicitantesRegistroNotificacion;
    }

    public String getTipoIdentificPersonaNotif() {

        return this.tipoIdentificPersonaNotif;
    }

    public void setTipoIdentificPersonaNotif(String tipoIdentificPersonaNotif) {
        this.tipoIdentificPersonaNotif = tipoIdentificPersonaNotif;
    }

    public String getNombrePersonaNotif() {
        return this.nombrePersonaNotif;
    }

    public void setNombrePersonaNotif(String nombrePersonaNotif) {
        this.nombrePersonaNotif = nombrePersonaNotif;
    }

    public String getNumeroIdentificPersonaNotif() {
        return this.numeroIdentificPersonaNotif;
    }

    public void setNumeroIdentificPersonaNotif(String numeroIdentificPersonaNotif) {
        this.numeroIdentificPersonaNotif = numeroIdentificPersonaNotif;
    }

    public String getRenunciaARecursos() {
        return this.renunciaARecursos;
    }

    public void setRenunciaARecursos(String renunciaARecursos) {
        this.renunciaARecursos = renunciaARecursos;
    }

    public String getFormaNotificacion() {
        return this.formaNotificacion;
    }

    public void setFormaNotificacion(String formaNotificacion) {
        this.formaNotificacion = formaNotificacion;
    }

    public boolean isNotificadoEsSolicitante() {
        return this.notificadoEsSolicitante;
    }

    public void setNotificadoEsSolicitante(boolean notificadoEsSolicitante) {
        this.notificadoEsSolicitante = notificadoEsSolicitante;
    }

    public String getTipoPersonaNotif() {
        return this.tipoPersonaNotif;
    }

    public ReporteDTO getReporteDiligenciaNotificacionPersonal() {
        return reporteDiligenciaNotificacionPersonal;
    }

    public void setReporteDiligenciaNotificacionPersonal(
        ReporteDTO reporteDiligenciaNotificacionPersonal) {
        this.reporteDiligenciaNotificacionPersonal = reporteDiligenciaNotificacionPersonal;
    }

    public Solicitante[] getSolicitantesSeleccionados() {
        return solicitantesSeleccionados;
    }

    public void setSolicitantesSeleccionados(Solicitante[] solicitantesSeleccionados) {
        this.solicitantesSeleccionados = solicitantesSeleccionados;
    }

    /**
     * dependiendo de lo que escoja en el combo se cambia el valor de la bandera
     */
    public void setTipoPersonaNotif(String tipoPersonaNotif) {
        this.tipoPersonaNotif = tipoPersonaNotif;

        if (this.tipoPersonaNotif
            .equals(RegistroNotificacionMB.tipoPersonaNotifSolicitante)) {
            this.notificadoEsSolicitante = true;
            this.numeroIdentificPersonaNotif = this.selectedSolicitante.getNumeroIdentificacion();
            this.nombrePersonaNotif = this.selectedSolicitante.getNombreCompleto();
            this.tipoIdentificPersonaNotif = this.selectedSolicitante.getTipoIdentificacion();
        } else {
            this.notificadoEsSolicitante = false;
            this.numeroIdentificPersonaNotif = null;
            this.nombrePersonaNotif = null;
            this.tipoIdentificPersonaNotif = null;
        }

        this.banderaDatosDeNotificadoAlmacenados = false;
        this.banderaTipoPersonaNotificada = false;
    }
//--------------------------------------------------------------------------------------------------

    public List<Long> getiDsTramitesNotif() {
        return iDsTramitesNotif;
    }

    public void setiDsTramitesNotif(List<Long> iDsTramitesNotif) {
        this.iDsTramitesNotif = iDsTramitesNotif;
    }

    @PostConstruct
    public void init() {

        LOGGER.debug("on RegistroNotificacionMB#init ");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        // D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "registroNotificacion");
        this.banderaDocNotificacionGenerado = false;
        this.habilitaBuscar = false;
        this.filtroBusquedaTramite = new FiltroDatosConsultaTramite();
        this.filtroBusquedaTramite.setNumeroPredialPartes(new NumeroPredialPartes());

        this.tipoPersonaNotif = RegistroNotificacionMB.tipoPersonaNotifAutorizado;
        this.banderaDatosRegistroNotificacion = false;
        
        this.fechaLogYRadicacion = new Date(System.currentTimeMillis());

        // Cargue de valores para el filtro por actividad.
        this.cargarVariablesFiltroActividad();

        this.mensajeTramiteNoAptoNotificar = "El trámite seleccionado" +
             " no se encuentra para ser notificado." +
             " Por favor seleccione un trámite válido e intente nuevamente";

        this.mensajeConfirmationAvanzarProceso = "¿Está seguro de avanzar el proceso del trámite?" +
             "\n Si acepta se hará la acción y se redigirá a la pantalla principal";
        
        this.mensajeConfirmationAsignacionRegistro = "¿Está seguro de realizar el registro de notificación para el trámite seleccionado"             +"\n Una vez seleccionado el trámite, el termino de los tiempos se dará por finalizado";
        

        LOGGER.debug("RegistroNotificacionMB initalized. ");

    }

    /**
     * Método que realiza el cargue de las variables del filtro de actividad.
     *
     * @author david.cifuentes
     */
    public void cargarVariablesFiltroActividad() {

        this.actividadesFiltro = new ArrayList<SelectItem>();
        // Set de variable por defecto
        this.nombreActividadSelected = ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION;
        // Cargue de valores del menu.
        SelectItem actividadSelectItem = new SelectItem(
            this.nombreActividadSelected, Constantes.POR_NOTIFICAR);
        this.actividadesFiltro.add(actividadSelectItem);
        actividadSelectItem = new SelectItem(
            ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO, Constantes.RENUNCIA_RECURSO);
        this.actividadesFiltro.add(actividadSelectItem);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener se selección en la tabla de solicitantes actualiza los valores de los componentes
     * que guardan los datos de la persona notificada
     *
     */
    public void updateValoresPersonaNotificada(SelectEvent event) {
        this.tipoIdentificPersonaNotif = this.selectedSolicitante
            .getTipoIdentificacion();
        this.numeroIdentificPersonaNotif = this.selectedSolicitante
            .getNumeroIdentificacion();
        this.nombrePersonaNotif = this.selectedSolicitante.getNombreCompleto();
    }

//--------------------------------------------------------------------------------------------------}
    /**
     * Método que realiza el cargue del documento de registro de Notificacion en la Base de Datos.
     * Incidencia:#16089
     *
     * @author leidy.gonzalez
     */
    public void guardarDocumentoRegistroNotificacion() {

        Documento tempDocumento = null;
        Date fechaLogYRadicacion;
        TramiteDocumento tramiteDocumento = new TramiteDocumento();

        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.NOTIFICACION.getId());

        if (this.currentTramite.isTramiteMasivo() &&
             (this.solicitantesSeleccionados != null && this.solicitantesSeleccionados.length > 1)) {

            tramiteDocumento = this.getTramiteService().
                obtenerDocNotificacion(this.currentTramite.getId(),
                    this.numeroIdentificPersonaNotif, this.nombrePersonaNotif);

            tempDocumento = this.getGeneralesService().
                buscarDocumentoPorTramiteIdyTipoDocumento(this.currentTramiteId, tipoDocumento.
                    getId());

            if (tramiteDocumento == null) {

                if (tempDocumento == null) {

                    tempDocumento = new Documento();
                    fechaLogYRadicacion = new Date(System.currentTimeMillis());

                    // D: se arma el objeto Documento con todos los atributos posibles...
                    // D: el método que guarda el documento en el gestor documental recibe
                    // el nombre del archivo sin la ruta
                    tempDocumento.setArchivo(this.reporteDiligenciaNotificacionPersonal.
                        getRutaCargarReporteAAlfresco());
                    tempDocumento.setTramiteId(this.currentTramiteId);
                    tempDocumento.setTipoDocumento(tipoDocumento);
                    tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                    tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
                    tempDocumento.setUsuarioCreador(this.loggedInUser.getLogin());
                    tempDocumento.setEstructuraOrganizacionalCod(this.loggedInUser
                        .getCodigoEstructuraOrganizacional());

                    tempDocumento.setFechaLog(fechaLogYRadicacion);
                    tempDocumento.setUsuarioLog(this.loggedInUser.getLogin());
                    tempDocumento
                        .setDescripcion("Documento de registro de notificaciòn");

                    // Asociación de la identificación del solicitante o la entidad al documento.
                    // Verificar que el solicitante o afectado seleccionado no sea nulo
                    tempDocumento.setObservaciones("");

                    try {
                        // D: se guarda el documento
                        tempDocumento = this.getTramiteService()
                            .guardarDocumento(this.loggedInUser, tempDocumento);

                    } catch (Exception ex) {
                        LOGGER.error("error guardando Documento : " + ex.getMessage());
                    }
                }

                if (tempDocumento != null) {

                    tramiteDocumento = this.getTramiteService().
                        obtenerDocNotificacion(this.currentTramite.getId(),
                            this.numeroIdentificPersonaNotif, this.nombrePersonaNotif);

                    if (tramiteDocumento == null) {

                        tramiteDocumento = new TramiteDocumento();
                        tramiteDocumento.setTramite(this.currentTramite);
                        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
                        tramiteDocumento.setUsuarioLog(this.loggedInUser.getLogin());
                        tramiteDocumento.setDocumento(tempDocumento);
                        tramiteDocumento.setIdRepositorioDocumentos(tempDocumento.
                            getIdRepositorioDocumentos());
                        tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));
                        tramiteDocumento.
                            setTipoIdPersonaAutorizacion(this.tipoIdentificPersonaNotif);
                        tramiteDocumento.setIdPersonaAutorizacion(this.numeroIdentificPersonaNotif);
                        tramiteDocumento.setPersonaAutorizacion(this.nombrePersonaNotif);

                        tramiteDocumento = this.getTramiteService()
                            .actualizarTramiteDocumento(tramiteDocumento);

                    }

                    if (this.solicitantesSeleccionados.length > 1) {

                        List<TramiteDocumento> tramDocs = new ArrayList<TramiteDocumento>();

                        for (Solicitante solicitanteTemp : this.solicitantesSeleccionados) {

                            if (!this.numeroIdentificPersonaNotif.equals(solicitanteTemp.
                                getNumeroIdentificacion())) {

                                tramDocs = new ArrayList<TramiteDocumento>();

                                tramiteDocumento = this.getTramiteService().
                                    obtenerDocNotificacion(this.currentTramite.getId(),
                                        solicitanteTemp.getNumeroIdentificacion(), solicitanteTemp.
                                        getNombreCompleto());

                                if (tramiteDocumento == null) {

                                    tramiteDocumento = new TramiteDocumento();
                                    tramiteDocumento.setTramite(this.currentTramite);
                                    tramiteDocumento.setFechaLog(
                                        new Date(System.currentTimeMillis()));
                                    tramiteDocumento.setUsuarioLog(this.loggedInUser.getLogin());
                                    tramiteDocumento.setDocumento(tempDocumento);
                                    tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                                    //D: guardar en el TramiteDocumento de la comunicación de notificaión los datos
                                    //   de la persona autorizada
                                    tramiteDocumento.setPersonaAutorizacion(solicitanteTemp.
                                        getNombreCompleto());
                                    tramiteDocumento.setIdPersonaAutorizacion(
                                        solicitanteTemp.getNumeroIdentificacion());
                                    tramiteDocumento.setTipoIdPersonaAutorizacion(
                                        solicitanteTemp.getTipoIdentificacion());

                                    tramiteDocumento.setRenunciaRecursos(this.renunciaARecursos);
                                    tramiteDocumento.setMetodoNotificacion(this.formaNotificacion);

                                    tramDocs.add(tramiteDocumento);

                                }
                            }

                        }

                        this.getTramiteService()
                            .guardarYActualizarTramiteDocumentoMultiple(tramDocs);

                    }
                }

            }

        } else {

            tempDocumento = this.getGeneralesService().buscarDocumentoPorTramiteIdyTipoDocumento(
                this.currentTramiteId, tipoDocumento.getId());

            if (tempDocumento == null) {

                tempDocumento = new Documento();
                fechaLogYRadicacion = new Date(System.currentTimeMillis());

                // D: se arma el objeto Documento con todos los atributos posibles...
                // D: el método que guarda el documento en el gestor documental recibe
                // el nombre del archivo sin la ruta
                tempDocumento.setArchivo(this.reporteDiligenciaNotificacionPersonal.
                    getRutaCargarReporteAAlfresco());
                tempDocumento.setTramiteId(this.currentTramiteId);
                tempDocumento.setTipoDocumento(tipoDocumento);
                tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
                tempDocumento.setUsuarioCreador(this.loggedInUser.getLogin());
                tempDocumento.setEstructuraOrganizacionalCod(this.loggedInUser
                    .getCodigoEstructuraOrganizacional());

                tempDocumento.setFechaLog(fechaLogYRadicacion);
                tempDocumento.setUsuarioLog(this.loggedInUser.getLogin());
                tempDocumento
                    .setDescripcion("Documento de registro de notificaciòn");

                // Asociación de la identificación del solicitante o la entidad al documento.
                // Verificar que el solicitante o afectado seleccionado no sea nulo
                tempDocumento.setObservaciones("");

                try {
                    // D: se guarda el documento
                    tempDocumento = this.getTramiteService()
                        .guardarDocumento(this.loggedInUser, tempDocumento);

                    if (tempDocumento != null) {

                        tramiteDocumento = new TramiteDocumento();
                        tramiteDocumento.setTramite(this.currentTramite);
                        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
                        tramiteDocumento.setUsuarioLog(this.loggedInUser.getLogin());
                        tramiteDocumento.setDocumento(tempDocumento);
                        tramiteDocumento.setIdRepositorioDocumentos(tempDocumento.
                            getIdRepositorioDocumentos());
                        tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                        tramiteDocumento = this.getTramiteService()
                            .actualizarTramiteDocumento(tramiteDocumento);
                    }
                } catch (Exception ex) {
                    LOGGER.error("error guardando Documento : " + ex.getMessage());
                }
            }
        }

    }

    /**
     * action del botón con el mismo nombre
     *
     * 1. verifica que se hayan cargado los archivos necesarios 2. arma los Documento que se
     * requieren (notificación y/o autorización) 3. se obtiene y actualiza el TramiteDocumento
     * correspondiente a la resolución (de conservación -o trámite) 4. guarda en BD los Documento
     * que se hayan cargado y actualiza el TramiteDocumento correspondiente
     */
    /*
     * @modified pedro.garcia documentación return al encontrar un error de validación
     */
 /*
     * @modified leidy.gonzalez #10683 19-12-2014 :: cambio de caracteres especiales por espacion
     */
    public void guardarRegistroNotificacion() {

        String mensajeError = null;
        boolean showError = true;

        //D. se debe haber generado antes el documento de notificación (que es el mismo que se
        // debe cargar como documento de notificación)
        try {
            if (this.banderaDocNotificacionGenerado == true) {
                boolean isSolicitante = true;
                if (this.tipoPersonaNotif.equals(RegistroNotificacionMB.tipoPersonaNotifSolicitante)) {
                    isSolicitante = true;
                } else if (this.tipoPersonaNotif.equals(
                    RegistroNotificacionMB.tipoPersonaNotifAutorizado)) {
                    isSolicitante = false;
                }

                // 1.
                if (isSolicitante) {
                    if (!this.docNotifCargado) {
                        mensajeError = "Debe cargar el documento de notificación";
                        throw new Exception();
                    }
                } //D: si es un autorizado, debe haber cargado los dos documentos: el de notificación y el
                //   de autorización
                else {
                    if (!this.docNotifCargado || !this.docAutorizacionCargado) {
                        mensajeError =
                            "Debe cargar el documento de notificación y el de autorización";
                        throw new Exception();
                    }
                }

                // D: debido que al escoger tipo de persona notificada como
                // "solicitante" los campos con los valores de número de id y nombre quedan
                // deshabilitados, no se está validando eso al dar click en el botón
                if (this.numeroIdentificPersonaNotif == null || this.nombrePersonaNotif == null) {
                    mensajeError = "Debe seleccionar un solicitante";
                    throw new Exception();
                }

                // 2.
                //String nombreArchivoSinCaracteresEspeciales = this.nombreArchivoNotificacionCargado.replaceAll("[^a-zA-Z0-9./]", "_");
                this.documentoNotificacion = this.armarDocumento(
                    RegistroNotificacionMB.tipoDocumentoNotificacion,
                    this.nombreArchivoNotificacionCargado,
                    "Archivo de notificación suministrado por el usuario");

                if (this.tipoPersonaNotif.equals(RegistroNotificacionMB.tipoPersonaNotifAutorizado)) {
                    this.documentoAutorizacionDeNotificacion = this.armarDocumento(
                        RegistroNotificacionMB.tipoDocumentoAutorizacionDeNotificacion,
                        this.nombreArchivoAutorizacionCargado,
                        "Archivo de autorización notificación suministrado por el usuario");
                }

                // 3.
                //Registro de Notificacion para cuando se selecciona mas de un solicitante
                if (this.currentTramite.isTramiteMasivo() &&
                     (this.solicitantesSeleccionados != null &&
                    this.solicitantesSeleccionados.length > 1)) {

                    //Obtiene Lista de String
                    for (Solicitante solSelec : this.solicitantesSeleccionados) {

                        List<String> numeroIdentificacionSol = new ArrayList<String>();
                        this.tramiteDocumentosNotificacion = new ArrayList<TramiteDocumento>();

                        if (solSelec.getNumeroIdentificacion() != null &&
                             !solSelec.getNumeroIdentificacion().isEmpty()) {

                            numeroIdentificacionSol.add(solSelec.getNumeroIdentificacion());
                        }

                        if (numeroIdentificacionSol != null && !numeroIdentificacionSol.isEmpty()) {

                            this.tramiteDocumentosNotificacion = this.getTramiteService().
                                obtenerTramiteDocumentosParaRegistrosNotificacion(
                                    this.currentTramite.getId(),
                                    numeroIdentificacionSol);

                            if (this.tramiteDocumentosNotificacion == null) {
                                showError = false;
                                throw new Exception(
                                    "Error interno de la aplicación: no encontró un TramiteDocumento" +
                                     " para el trámite id " + this.currentTramite.getId());
                            }

                            //D: la fecha de notificación del TramiteDocumento se actualiza porque la que debe quedar
                            //   es la de este momento, que es cuando se considera notificado al solicitante
                            if (this.tramiteDocumentosNotificacion.get(0) != null) {

                                this.tramiteDocumentosNotificacion.get(0).setFechaNotificacion(
                                    this.fechaNotificacion);

                                if (!this.notificadoEsSolicitante) {
                                    //D: guardar en el TramiteDocumento de la comunicación de notificaión los datos
                                    //   de la persona autorizada
                                    this.tramiteDocumentosNotificacion.get(0).
                                        setPersonaAutorizacion(this.nombrePersonaNotif);
                                    this.tramiteDocumentosNotificacion.get(0).
                                        setIdPersonaAutorizacion(
                                            this.numeroIdentificPersonaNotif);
                                    this.tramiteDocumentosNotificacion.get(0).
                                        setTipoIdPersonaAutorizacion(
                                            this.tipoIdentificPersonaNotif);
                                }

                                this.tramiteDocumentosNotificacion.get(0).setRenunciaRecursos(
                                    this.renunciaARecursos);
                                this.tramiteDocumentosNotificacion.get(0).setMetodoNotificacion(
                                    this.formaNotificacion);

                                // 4. Para esto se llama a un método que hace todo
                                boolean actualizacionBDIsOk = this.getTramiteService().
                                    guardarDocumentosRegistroNotificacion(
                                        this.currentTramiteId, this.documentoNotificacion,
                                        this.documentoAutorizacionDeNotificacion,
                                        this.tramiteDocumentosNotificacion.get(0),
                                        this.loggedInUser);

                                if (!actualizacionBDIsOk) {
                                    mensajeError =
                                        "Ocurrió un error al almacenar la notificación." +
                                         " Por favor intente nuevamente";
                                    throw new Exception();

                                }
                            }

                        }
                    }

                    this.banderaDatosDeNotificadoAlmacenados = false;
                    this.banderaTipoPersonaNotificada = false;
                    this.documentoNotificacion = null;
                    this.documentoAutorizacionDeNotificacion = null;
                    this.renunciaARecursos = null;
                    this.banderaDocNotificacionGenerado = false;

                    this.addMensajeInfo("El solicitante se notificó exitosamente.");

                    //D: para hacer que se recalcule si ya fueron notificados o no
                    this.solicitantesRegistroNotificacion = (ArrayList<Solicitante>) this.
                        getTramiteService().
                        obtenerSolicitantesParaNotificarPorTramiteId(this.currentTramiteId);
                    this.cargarPropietariosNotficacion();

                    //D: se revisa si todos han sido notificados
                    this.okToForwardProcess = this.validarTodosNotificados();

                    //Registro Notificacion para cuando se selecciona un solo notificante    
                } else {

                    this.tramiteDocumentoNotificacion = this.getTramiteService().
                        obtenerTramiteDocumentoParaRegistroNotificacion(this.currentTramite.getId(),
                            this.selectedSolicitante.getNumeroIdentificacion());

                    if (this.tramiteDocumentoNotificacion == null) {
                        showError = false;
                        throw new Exception(
                            "Error interno de la aplicación: no encontró un TramiteDocumento" +
                             " para el trámite id " + this.currentTramite.getId() +
                            " y la persona con " +
                             " doc identidad " + this.selectedSolicitante.getNumeroIdentificacion());
                    }

                    //D: la fecha de notificación del TramiteDocumento se actualiza porque la que debe quedar
                    //   es la de este momento, que es cuando se considera notificado al solicitante
                    this.tramiteDocumentoNotificacion.setFechaNotificacion(this.fechaNotificacion);

                    if (!this.notificadoEsSolicitante) {
                        //D: guardar en el TramiteDocumento de la comunicación de notificaión los datos
                        //   de la persona autorizada
                        this.tramiteDocumentoNotificacion.setPersonaAutorizacion(
                            this.nombrePersonaNotif);
                        this.tramiteDocumentoNotificacion.setIdPersonaAutorizacion(
                            this.numeroIdentificPersonaNotif);
                        this.tramiteDocumentoNotificacion.setTipoIdPersonaAutorizacion(
                            this.tipoIdentificPersonaNotif);
                    }

                    this.tramiteDocumentoNotificacion.setRenunciaRecursos(this.renunciaARecursos);
                    this.tramiteDocumentoNotificacion.setMetodoNotificacion(this.formaNotificacion);

                    // 4. Para esto se llama a un método que hace todo
                    boolean actualizacionBDIsOk = this.getTramiteService().
                        guardarDocumentosRegistroNotificacion(
                            this.currentTramiteId, this.documentoNotificacion,
                            this.documentoAutorizacionDeNotificacion,
                            this.tramiteDocumentoNotificacion,
                            this.loggedInUser);

                    if (!actualizacionBDIsOk) {
                        mensajeError = "Ocurrió un error al almacenar la notificación." +
                             " Por favor intente nuevamente";
                        throw new Exception();

                    }

                    this.banderaDatosDeNotificadoAlmacenados = false;
                    this.banderaTipoPersonaNotificada = false;
                    this.documentoNotificacion = null;
                    this.documentoAutorizacionDeNotificacion = null;
                    this.renunciaARecursos = null;
                    this.banderaDocNotificacionGenerado = false;

                    this.addMensajeInfo("El solicitante se notificó exitosamente.");

                    //D: para hacer que se recalcule si ya fueron notificados o no
                    this.solicitantesRegistroNotificacion = (ArrayList<Solicitante>) this.
                        getTramiteService().
                        obtenerSolicitantesParaNotificarPorTramiteId(this.currentTramiteId);
                    this.cargarPropietariosNotficacion();

                    //D: se revisa si todos han sido notificados
                    this.okToForwardProcess = this.validarTodosNotificados();

                }

            } else {
                mensajeError = "El documento de notificación no ha sido generado" +
                     " por lo que la notificación no puede ser almacenada." +
                     " Por favor corrija los datos e intente nuevamente";
                throw new Exception();
            }
        } catch (Exception ex) {
            LOGGER.error("error en RegistroNotificacionMB#guardarRegistroNotificacion: " +
                 ex.getMessage());
            if (showError) {
                this.addMensajeError(mensajeError);
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /*
     * listeners de los componentes de carga de archivos
     */

    /**
     * este carga el documento de notificación
     *
     * @param eventoCarga
     */
    public void cargarArchivo1(FileUploadEvent eventoCarga) {

        if (!this.banderaDocNotificacionGenerado && this
            .isActividadRegistrarNotificacion()) {
            String mensaje =
                "El documento de notificación debe ser generado, firmado y escaneado " +
                 " para ser adjuntado en esta opción del formulario." +
                 " Por favor complete el proceso e intente nuevamente";
            this.addMensajeError(mensaje);

        } else {
            if (this.isActividadRadicarRecurso()) {
                this.archivoACargar = ETipoDocumento.RADICAR_RECURSO
                    .getNombre();
            } else {
                this.archivoACargar = RegistroNotificacionMB.tipoDocumentoNotificacion;
            }
            this.cargarArchivo(eventoCarga);
            this.docNotifCargado = true;
            this.visualizarDocNotif = true;
            this.verificarEstadoOkProcessRadicarRecurso();
        }

        //Si el trámite es de vía gubernativa debe renunciar al recurso
        if (this.currentTramite.getSolicitud().isViaGubernativa()) {
            this.renunciaARecursos = ESiNo.SI.getCodigo();
        }
    }

    /**
     * Este carga el documento de autorización
     *
     * @param eventoCarga
     */
    public void cargarArchivo2(FileUploadEvent eventoCarga) {
        this.archivoACargar = RegistroNotificacionMB.tipoDocumentoAutorizacionDeNotificacion;
        this.cargarArchivo(eventoCarga);
        this.docAutorizacionCargado = true;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Listener de los componentes de carga de archivos. Escribe el contenido del archivo contenido
     * en el FileUploadEvent en la carpeta temporal de documentos, y lo deja así disponible para que
     * sea creado el Documento en la base de datos
     *
     * @param eventoCarga
     */
    /*
     * @modified leidy.gonzalez #10683 19-12-2014 :: cambio de caracteres especiales por espacion
     */
    private void cargarArchivo(FileUploadEvent eventoCarga) {

        File archivoResultado = null;
        // String extension =
        // FilenameUtils.getExtension(eventoCarga.getFile().getFileName());
        this.servicioTemporalDocumento = new ServicioTemporalDocumento();
        //String nombreArchivoSinCaracteresEspeciales = eventoCarga.getFile().getFileName().replaceAll("[^a-zA-Z0-9./]", "_");
        String rutaMostrar = eventoCarga.getFile().getFileName();
        String directorioTemporal = "";

        try {
            directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();

            archivoResultado = new File(directorioTemporal, rutaMostrar);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw SNCWebServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, ex);
        }

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            if (this.archivoACargar.equals(RegistroNotificacionMB.tipoDocumentoNotificacion)) {

                this.nombreArchivoNotificacionCargado = rutaMostrar;

                if (this.documentoNotificacion == null) {
                    this.documentoNotificacion = new Documento();
                }

                this.documentoNotificacion.setArchivo(this.nombreArchivoNotificacionCargado);
            } else if (this.archivoACargar
                .equals(RegistroNotificacionMB.tipoDocumentoAutorizacionDeNotificacion)) {

                this.nombreArchivoAutorizacionCargado = rutaMostrar;

                if (this.documentoAutorizacionDeNotificacion == null) {
                    this.documentoAutorizacionDeNotificacion = new Documento();
                }

                this.documentoAutorizacionDeNotificacion.setArchivo(
                    this.nombreArchivoAutorizacionCargado);
            } else if (this.archivoACargar
                .equals(ETipoDocumento.RADICAR_RECURSO.getNombre())) {

                if (this.documentoRadicarRecurso == null) {
                    this.documentoRadicarRecurso = new Documento();
                    this.documentoNotificacion = new Documento();
                }
                this.documentoRadicarRecurso.setArchivo(rutaMostrar);
                this.documentoNotificacion.setArchivo(rutaMostrar);
                this.banderaDatosDeNotificadoAlmacenados = true;
                this.banderaTipoPersonaNotificada = true;
                this.nombreArchivoNotificacionCargado = rutaMostrar;
            }

        } catch (FileNotFoundException ex) {
            LOGGER.error("Archivo no encontrado: " + ex.getMessage(), ex);
        } catch (IOException ex) {
            LOGGER.error(RegistroNotificacionMB.class.getName(), ex);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma un objeto Documento
     *
     * PRE: el archivo que se va a guardar como Documento debió ser cargado antes
     *
     * @author pedro.garcia
     * @param tipoDocumento 'notificacion' o 'autorizacion'
     * @param nombreArchivoEnRutaTemp nombre del archivo con el que quedó en la ruta temporal
     * (carpeta temporal transitoria de archivos que se guardan en o se cargan del gestor
     * documental)
     * @param descripcionArchivo descripción del Documento
     *
     * @return true si todo OK
     */
    private Documento armarDocumento(String tipoDocumento, String nombreArchivoEnRutaTemp,
        String descripcionArchivo) {

        Documento answer = new Documento();
        TipoDocumento tipoDoc = new TipoDocumento();

        if (tipoDocumento.equals(RegistroNotificacionMB.tipoDocumentoNotificacion)) {
            tipoDoc.setId(ETipoDocumento.DOCUMENTO_DE_NOTIFICACION.getId());
            tipoDoc.setNombre(ETipoDocumento.DOCUMENTO_DE_NOTIFICACION.getNombre());
        } else if (tipoDocumento.equals(
            RegistroNotificacionMB.tipoDocumentoAutorizacionDeNotificacion)) {
            tipoDoc.setId(ETipoDocumento.DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION.getId());
            tipoDoc.setNombre(ETipoDocumento.DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION.getNombre());
            answer.setFechaRadicacion(this.fechaLogYRadicacion);
        } else if (tipoDocumento.equals(
            RegistroNotificacionMB.tipoDocumentoRadicarRecurso)) {
            tipoDoc.setId(ETipoDocumento.RADICAR_RECURSO.getId());
            tipoDoc.setNombre(ETipoDocumento.RADICAR_RECURSO.getNombre());
            answer.setFechaRadicacion(this.fechaLogYRadicacion);
        }

        // D: se arma el objeto Documento con todos los atributos posibles...
        // D: el método que guarda el documento en el gestor documental recibe
        // el nombre del archivo sin la ruta
        answer.setArchivo(nombreArchivoEnRutaTemp);
        answer.setTramiteId(this.currentTramiteId);
        answer.setTipoDocumento(tipoDoc);
        answer.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        answer.setBloqueado(ESiNo.NO.getCodigo());
        answer.setUsuarioCreador(this.loggedInUser.getLogin());
        answer.setEstructuraOrganizacionalCod(this.loggedInUser.getCodigoEstructuraOrganizacional());

        answer.setProcesoId(this.currentProcessActivity.getId());

        answer.setFechaLog(this.fechaLogYRadicacion);
        answer.setUsuarioLog(this.loggedInUser.getLogin());
        answer.setDescripcion(descripcionArchivo);

        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Generar documento de notificación" Genera el reporte con la comunicación de
     * la notificación y lo deja en la variable que se usa como value en el componente p:download de
     * la página donde se pinta
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarDocNotificacion() {

        boolean answer = false;
        EReporteServiceSNC enumeracionReporte;
        Map<String, String> parameters;
        FirmaUsuario firma = null;
        String documentoFirma = "";

        String metodoNotificacion, tipoDocumento, numeroDocumento;

        enumeracionReporte = EReporteServiceSNC.DILIGENCIA_NOTIFICACION_PERSONAL;

        List<Dominio> metodosNotificacion = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TRAMITE_DOCUMENTO_METO_NOTI);

        metodoNotificacion = null;
        if (metodosNotificacion != null && !metodosNotificacion.isEmpty()) {
            for (Dominio d : metodosNotificacion) {
                if (d.getCodigo().equals(this.formaNotificacion)) {
                    metodoNotificacion = d.getValor();
                }
            }
        }

        if (this.loggedInUser != null &&
             this.loggedInUser.getNombreCompleto() != null) {
            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                this.loggedInUser.getNombreCompleto());

            if (firma != null &&
                 this.loggedInUser.getNombreCompleto().equals(
                    firma.getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService()
                    .descargarArchivoAlfrescoYSubirAPreview(
                        firma.getDocumentoId()
                            .getIdRepositorioDocumentos());

            }
        }

        parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", String.valueOf(this.currentTramiteId));

        parameters.put("NOMBRE_NOTIFICADO", this.nombrePersonaNotif);
        tipoDocumento = (this.tipoIdentificPersonaNotif != null) ?
             this.tipoIdentificPersonaNotif : "";
        numeroDocumento = (this.numeroIdentificPersonaNotif != null) ?
             ": " + this.numeroIdentificPersonaNotif : "";
        parameters.put("DOCUMENTO_NOTIFICADO", tipoDocumento + numeroDocumento);
        parameters.put("FECHA_NOTIFICACION",
            Constantes.FORMAT_FECHA_DILIGENCIA_NOTIFICACION_PERSONAL.format(this.fechaNotificacion));
        parameters.put("METODO_NOTIFICACION", metodoNotificacion);
        parameters.put("CONDICION_NOTIFICADO", this.tipoPersonaNotif);
        if (!documentoFirma.isEmpty()) {
            parameters.put("FIRMA_USUARIO", documentoFirma);
        }

        // D: primero se genera el reporte y luego se carga como archivo
        try {
            this.reporteDiligenciaNotificacionPersonal = this.reportsService.generarReporte(
                parameters,
                enumeracionReporte, this.loggedInUser);

            if (this.reporteDiligenciaNotificacionPersonal != null) {

                cargarDocumentoNotificacion(this.reporteDiligenciaNotificacionPersonal.
                    getRutaCargarReporteAAlfresco());

                this.banderaDocNotificacionGenerado = true;
                this.visualizarDocNotif = false;

                answer = true;

                //@modified by leidy.gonzalez #16089 ::14/03/2016
                this.guardarDocumentoRegistroNotificacion();

            } else {
                LOGGER.error("error generando reporte: ");
                throw SNCWebServiceExceptions.EXCEPCION_0002
                    .getExcepcion(LOGGER);
            }

        } catch (Exception ex) {
            LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER,
                ex, enumeracionReporte.getUrlReporte());
        }

        this.reporteGeneradoOK = answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que arma el objeto documento para la notificación
     *
     * @author juan.agudelo
     */
    private void cargarDocumentoNotificacion(String nombreArchivoNotificacion) {

        this.tramiteDocumentoTemp = new TramiteDocumento();
        this.documentoNotificacion = new Documento();
        TipoDocumento tipoDoc = new TipoDocumento();

        tipoDoc.setId(ETipoDocumento.OTRO.getId());

        this.documentoNotificacion.setArchivo(nombreArchivoNotificacion);
        this.documentoNotificacion.setTramiteId(this.currentTramiteId);
        this.documentoNotificacion.setTipoDocumento(tipoDoc);
        this.documentoNotificacion.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.documentoNotificacion.setBloqueado(ESiNo.NO.getCodigo());
        this.documentoNotificacion.setUsuarioCreador(this.loggedInUser.getLogin());
        this.documentoNotificacion.setEstructuraOrganizacionalCod(this.loggedInUser
            .getCodigoEstructuraOrganizacional());
        this.documentoNotificacion.setFechaRadicacion(this.fechaLogYRadicacion);

        this.documentoNotificacion.setProcesoId(this.currentProcessActivity.getId());

        this.documentoNotificacion.setFechaLog(this.fechaLogYRadicacion);
        this.documentoNotificacion.setUsuarioLog(this.loggedInUser.getLogin());
        this.documentoNotificacion.setDescripcion(EDescripcionTipoDocumento.EDICTO
            .getDescripcion());

    }

    // --------------------------------------------------------------------------------------------------
    // ---------------- BPM ---------------------
    /**
     * nada hay que validar
     *
     * @return
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------------

    /**
     * arma el ActivityMessageDTO que se usa para avanzar el proceso.
     *
     * Si algun solicitante no renuncia a recursos se va a la actividad 'radicar recurso', que es
     * hecha por el usuario con rol 'radicador'. Si todos renuncian se va a la actividad 'aplicar
     * cambios', que es hecha por el usuario con rol 'responsable de conservación'
     *
     * @author pedro.garcia
     */
    /**
     * @modified by juanfelipe.garcia :: 26-12-2013 :: CC-NP-CO-030 adición de logica para trámites
     * de via gubernativa
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones, processTransition;
        List<UsuarioDTO> responsablesConservacion, radicadores, directorTerritorial;
        UsuarioDTO usuarioActividad;
        Actividad actividadTramiteOriginal = null;

        //Validaciones de via gubernativa 
        if (this.currentTramite.isViaGubernativa() ||
             this.currentTramite.isTipoTramiteViaGubernativaModificado() ||
             this.currentTramite.isTipoTramiteViaGubModSubsidioApelacion()) {

            List<ActivityMessageDTO> masiveMessages = new ArrayList<ActivityMessageDTO>();
            observaciones = " ";
            boolean val1 = false;
            //tramite original   
            Tramite tramiteRef = null;
            FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
            filtrosTramite.setNumeroRadicacion(this.currentTramite.getSolicitud().
                getTramiteNumeroRadicacionRef());
            filtrosTramite.setEstadoTramite("0");
            List<Tramite> resultado = this.getTramiteService().consultaDeTramitesPorFiltros(true,
                filtrosTramite, null, null, false, null, null, null);

            if (resultado != null && !resultado.isEmpty()) {
                tramiteRef = resultado.get(0);
                List<Actividad> actividadesTramiteOriginal = this.getProcesosService().
                    getActividadesPorIdObjetoNegocio(tramiteRef.getId());
                actividadTramiteOriginal = actividadesTramiteOriginal.get(0);

                Date fechaActual = new Date();
                //validar que la fecha de expiracion de la actividad 
                val1 = actividadesTramiteOriginal.get(0).getFechaExpiracion().before(fechaActual);
            }
            //Buscar al responsable de conservación
            responsablesConservacion = (List<UsuarioDTO>) this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    this.getGeneralesService().
                        getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                            this.currentTramite.getMunicipio().getCodigo()),
                    ERol.RESPONSABLE_CONSERVACION);

            if (responsablesConservacion != null && !responsablesConservacion.isEmpty() &&
                 responsablesConservacion.get(0) != null) {
                usuarioActividad = responsablesConservacion.get(0);
            } else {
                LOGGER.error("No se encontró ningún usuario responsable de conservación " +
                     "para la territorial " + this.loggedInUser.getDescripcionTerritorial());
                return;
            }
            List<Tramite> recursosSuspendidos = new ArrayList<Tramite>();
            List<Tramite> recursosActivos = new ArrayList<Tramite>();
            //1. Obtener recursos suspendidos
            for (Tramite t : recursosViaGubernativa()) {
                if (t.getActividadActualTramite().getEstado().equalsIgnoreCase(
                    EEstadoActividad.SUSPENDIDA.toString())) {
                    recursosSuspendidos.add(t);
                } else {
                    recursosActivos.add(t);
                }
            }

            //CONFIRMO el resultado de la resolución del trámite original
            if (this.currentTramite.isViaGubernativa() ||
                 this.currentTramite.getSolicitud().isViaGubernativa() &&
                 !this.currentTramite.isRecursoReposicionEnSubsidioApelacion()) {

                //2. Mover a la actividad de Finalizar
                //2.a tramite del recurso
                processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
                messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                    this.currentProcessActivity.getId(), processTransition,
                    usuarioActividad, observaciones);

                messageDTO.setUsuarioActual(this.loggedInUser);
                masiveMessages.add(messageDTO);

                //2.b recursos suspendidos
                for (Tramite t : recursosSuspendidos) {
                    processTransition =
                        ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                    messageDTO = new ActivityMessageDTO();
                    messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                        t.getActividadActualTramite().getId(), processTransition,
                        usuarioActividad, observaciones);
                    messageDTO.setUsuarioActual(this.loggedInUser);
                    masiveMessages.add(messageDTO);
                }

                //No existen otros recursos de vía gubernativa en curso            
                if (recursosActivos.size() == 0) {
                    //El tiempo para interponer recurso ya finalizó 
                    val1 = true;
                    if (val1) {
                        //3. El tramite original se mueve a la actividad de  Aplicar cambios
                        processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
                        messageDTO = new ActivityMessageDTO();
                        messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                            this.currentProcessActivity.getId(), processTransition,
                            usuarioActividad, observaciones);
                        messageDTO.setUsuarioActual(this.loggedInUser);
                        setMessage(messageDTO);
                    } else {
                        //3. El trámite original se mantiene Suspendido 
                    }
                } else {//Existen otros recursos de vía gubernativa en curso
                    //3. El trámite original se mantiene Suspendido
                }
            } else if (this.currentTramite.isTipoTramiteViaGubernativaModificado()) {
                //MODIFICO el resultado de la resolución del trámite original

                //No existen otros recursos de vía gubernativa en curso
                if (recursosActivos.size() == 1) {
                    //El tiempo para interponer recurso ya finalizó
                    if (val1) {
                        //1. El trámite del recurso se mueve a la actividad de Aplicar cambios
                        processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;

                        messageDTO = new ActivityMessageDTO();
                        messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                            this.currentProcessActivity.getId(), processTransition,
                            usuarioActividad, observaciones);
                        messageDTO.setUsuarioActual(this.loggedInUser);
                        masiveMessages.add(messageDTO);

                        //2. El trámite original y de otros recursos que se encuentren suspendidos 
                        //  con respecto al trámite original se mueven a la actividad de Finalizar
                        //tramite original
                        processTransition =
                            ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                        messageDTO = new ActivityMessageDTO();
                        messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                            actividadTramiteOriginal.getId(), processTransition,
                            usuarioActividad, observaciones);
                        messageDTO.setUsuarioActual(this.loggedInUser);
                        masiveMessages.add(messageDTO);

                        //recursos suspendidos
                        for (Tramite t : recursosSuspendidos) {
                            processTransition =
                                ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                            messageDTO = new ActivityMessageDTO();
                            messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                                t.getActividadActualTramite().getId(), processTransition,
                                usuarioActividad, observaciones);
                            messageDTO.setUsuarioActual(this.loggedInUser);
                            masiveMessages.add(messageDTO);
                        }
                    } else {//El tiempo para interponer recurso no ha finalizado   

                        //1. El trámite del recurso se mueve a la actividad de Suspender
                        Calendar tiempoFinInterponerRecurso = actividadTramiteOriginal.
                            getFechaExpiracion();
                        String motivo = "El tiempo para interponer recurso no ha finalizado";
                        this.getProcesosService().suspenderActividad(this.currentProcessActivity.
                            getId(), tiempoFinInterponerRecurso, motivo);

                        //2. El trámite original se mantiene en la actividad de Suspender
                    }
                } else {//Existen otros recursos de vía gubernativa en curso
                    //El tiempo para interponer recurso ya finalizó
                    if (val1) {
                        //1. El trámite del recurso se mueve a la actividad de Suspender
                        Calendar tiempoFinInterponerRecurso = actividadTramiteOriginal.
                            getFechaExpiracion();
                        String motivo = "Existen otros recursos de vía gubernativa en curso";
                        this.getProcesosService().suspenderActividad(this.currentProcessActivity.
                            getId(), tiempoFinInterponerRecurso, motivo);
                        //2. El trámite original se mantiene en la actividad de Suspender
                    }
                }
            } else if (this.currentTramite.isRecursoReposicionEnSubsidioApelacion() &&
                /* this.renunciaARecursos.equals(ESiNo.SI.getCodigo()) */  this.solicitantesRenuncian) {

                //2. Mover a la actividad de Finalizar
                //2.a tramite del recurso
                processTransition = ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                    this.currentProcessActivity.getId(), processTransition,
                    usuarioActividad, observaciones);

                messageDTO.setUsuarioActual(this.loggedInUser);
                masiveMessages.add(messageDTO);

                //2.b recursos suspendidos
                for (Tramite t : recursosSuspendidos) {
                    processTransition =
                        ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                    messageDTO = new ActivityMessageDTO();
                    messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                        t.getActividadActualTramite().getId(), processTransition,
                        usuarioActividad, observaciones);
                    messageDTO.setUsuarioActual(this.loggedInUser);
                    masiveMessages.add(messageDTO);
                }
                //No existen otros recursos de vía gubernativa en curso
                if (recursosActivos.size() == 1) {
                    //El tiempo para interponer recurso ya finalizó
                    if (val1) {

                        //3. El tramite original se mueve a la actividad de  Aplicar cambios
                        processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
                        messageDTO = new ActivityMessageDTO();
                        messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                            actividadTramiteOriginal.getId(), processTransition,
                            usuarioActividad, observaciones);
                        messageDTO.setUsuarioActual(this.loggedInUser);

                    } else {//El tiempo para interponer recurso no ha finalizado
                        //3. El trámite original se mantiene Suspendido 
                    }
                } else {//Existen otros recursos de vía gubernativa en curso

                    //3. El trámite original se mantiene Suspendido
                }
            } else if (this.currentTramite.isTipoTramiteViaGubModSubsidioApelacion() &&
                /* this.renunciaARecursos.equals(ESiNo.SI.getCodigo()) */  this.solicitantesRenuncian) {
                //No existen otros recursos de vía gubernativa en curso
                if (recursosActivos.size() == 1) {
                    //El tiempo para interponer recurso ya finalizó
                    if (val1) {
                        //1. El trámite del recurso se mueve a la actividad de Aplicar cambios
                        processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;

                        messageDTO = new ActivityMessageDTO();
                        messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                            this.currentProcessActivity.getId(), processTransition,
                            usuarioActividad, observaciones);
                        messageDTO.setUsuarioActual(this.loggedInUser);
                        masiveMessages.add(messageDTO);

                        //tramite original
                        processTransition =
                            ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                        messageDTO = new ActivityMessageDTO();
                        messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                            actividadTramiteOriginal.getId(), processTransition,
                            usuarioActividad, observaciones);
                        messageDTO.setUsuarioActual(this.loggedInUser);
                        masiveMessages.add(messageDTO);

                        //recursos suspendidos
                        for (Tramite t : recursosSuspendidos) {
                            processTransition =
                                ProcesoDeConservacion.ACT_VALIDACION_TERMINAR_VALIDACION_TRAMITE;
                            messageDTO = new ActivityMessageDTO();
                            messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                                t.getActividadActualTramite().getId(), processTransition,
                                usuarioActividad, observaciones);
                            messageDTO.setUsuarioActual(this.loggedInUser);
                            masiveMessages.add(messageDTO);
                        }

                    } else {//El tiempo para interponer recurso no ha finalizado
                        //1. El trámite del recurso se mueve a la actividad de Suspender
                        Calendar tiempoFinInterponerRecurso = actividadTramiteOriginal.
                            getFechaExpiracion();
                        String motivo = "Existen otros recursos de vía gubernativa en curso";
                        this.getProcesosService().suspenderActividad(this.currentProcessActivity.
                            getId(), tiempoFinInterponerRecurso, motivo);
                        //2. El trámite original se mantiene en la actividad de Suspender
                    }
                } else {//Existen otros recursos de vía gubernativa en curso
                    //1. El trámite del recurso se mueve a la actividad de Suspender
                    Calendar tiempoFinInterponerRecurso = actividadTramiteOriginal.
                        getFechaExpiracion();
                    String motivo = "Existen otros recursos de vía gubernativa en curso";
                    this.getProcesosService().
                        suspenderActividad(this.currentProcessActivity.getId(),
                            tiempoFinInterponerRecurso, motivo);
                    //2. El trámite original se mantiene en la actividad de Suspender

                }
            } else if ((this.currentTramite.isTipoTramiteViaGubModSubsidioApelacion() ||
                 this.currentTramite.isRecursoReposicionEnSubsidioApelacion()) &&
                /* this.renunciaARecursos.equals(ESiNo.NO.getCodigo()) */  !this.solicitantesRenuncian) {
                //rol “DIRECTOR TERRITORIAL”
                directorTerritorial = (List<UsuarioDTO>) this.getTramiteService().
                    buscarFuncionariosPorRolYTerritorial(this.getGeneralesService().
                        getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                            this.currentTramite.getMunicipio().getCodigo()),
                        ERol.DIRECTOR_TERRITORIAL);

                if (directorTerritorial != null && !directorTerritorial.isEmpty() &&
                     directorTerritorial.get(0) != null) {
                    usuarioActividad = directorTerritorial.get(0);
                } else {
                    LOGGER.error("No se encontró ningún usuario director territorial " +
                         "para la territorial " + this.loggedInUser.getDescripcionTerritorial());
                    return;
                }

                //El tramite se mueve a la actividad de “Revisar tramite asignado” 
                processTransition = ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                messageDTO = new ActivityMessageDTO();
                messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                    actividadTramiteOriginal.getId(), processTransition,
                    usuarioActividad, observaciones);
                messageDTO.setUsuarioActual(this.loggedInUser);
                masiveMessages.add(messageDTO);

            }

            if (!masiveMessages.isEmpty()) {
                this.setMasiveMessages(masiveMessages);
            }

        } else {
            if (this.solicitantesRenuncian == true || this.isActividadRadicarRecurso()) {
                processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
                observaciones = "Todos los solicitantes renunciaron a recursos";

                responsablesConservacion = (List<UsuarioDTO>) this.getTramiteService().
                    buscarFuncionariosPorRolYTerritorial(
                        this.getGeneralesService().
                            getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                this.currentTramite.getMunicipio().getCodigo()),
                        ERol.RESPONSABLE_CONSERVACION);

                if (responsablesConservacion != null && !responsablesConservacion.isEmpty() &&
                     responsablesConservacion.get(0) != null) {
                    usuarioActividad = responsablesConservacion.get(0);
                } else {
                    LOGGER.error("No se encontró ningún usuario responsable de conservación " +
                         "para la territorial " + this.loggedInUser.getDescripcionTerritorial());
                    return;
                }
            } else {
                processTransition = ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO;
                observaciones = "Alguno de los solicitantes no renunció a recursos";

                radicadores = (List<UsuarioDTO>) this.getTramiteService().
                    buscarFuncionariosPorRolYTerritorial(
                        this.loggedInUser.getDescripcionTerritorial(),
                        ERol.FUNCIONARIO_RADICADOR);

                if (radicadores != null && !radicadores.isEmpty() && radicadores.get(0) != null) {
                    usuarioActividad = radicadores.get(0);
                } else {
                    LOGGER.error("No se encontró ningún usuario responsable de radicación de " +
                         "conservación para la territorial " +
                         this.loggedInUser.getDescripcionTerritorial());
                    return;
                }

            }

            messageDTO = ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                this.currentProcessActivity.getId(), processTransition,
                usuarioActividad, observaciones);

            messageDTO.setUsuarioActual(this.loggedInUser);

            setMessage(messageDTO);
        }

    }
//-------------------------------------------------------------------------------------

    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

    }
//-------------------------------------------------------------------------------------

    private void forwardThisProcess() {

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();

            //D: si pudo armar el ActivityMessageDTO requerido para avanzar la actividad
            if (this.getMessage() != null) {
                this.forwardProcess();
            }
        }
    }

//------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión y redirigir a la pantalla principal
     *
     * @author pedro.garcia
     */
    public String cerrarVentanaCU() {

        this.habilitaBuscar = false;
        
        //Se transfiere tramite a Usuario Tiempos Muertos
       this.transferirTramiteAUsuarioTiemposMuertos();

        //Utilidades.removerManagedBean("registroNotificacion");
        //D: este llamado remueve de la sesión este MB y todos los que se hayan abierto durante el
        //   flujo de trabajo
        this.tareasPendientes.init();

        return "index";
    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de terminar e iniciar una neuva sesión sobre el botón si en la dialog de
     * confirmación
     *
     * @author juan.agudelo
     */
    public void terminarYContinuar() {
        UtilidadesWeb.removerManagedBean("registroNotificacion");
        init();
    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de buscar trámites por filtro ejecutado en la acción del botón buscar
     *
     * @author juan.agudelo
     */
    @SuppressWarnings("serial")
    public void buscarTramitesPorFiltro() {
               
        if (validarDatosConsultaTramite()) {

            // Cargue de ids
//        	if(this.iDsTramitesNotif == null || this.iDsTramitesNotif.isEmpty()){
//	        	this.iDsTramitesNotif = this.getTramiteService()
//	                .obtenerTramiteIdsParaNotificarPorFiltro(
//	                    this.filtroBusquedaTramite,
//	                    this.nombreActividadSelected);
//        	}
            this.lazyAssignedTramites = new LazyDataModel<Tramite>() {

//				@Override
//				public List<Tramite> load(int first, int pageSize,
//						String sortField, SortOrder sortOrder,
//						Map<String, String> filters) {
//					List<Tramite> answer = new ArrayList<Tramite>();
//					populateBusquedaTramitesLazyly(answer, first, pageSize);
//
//					return answer;
//				}
//			};
                @Override
                public List<Tramite> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<Tramite> answer = new ArrayList<Tramite>();

                    populateTramitesLazyly(answer, first, pageSize, sortField,
                        sortOrder.toString(), filters);

                    return answer;
                }
            };
            poblar();
        }

        this.banderaDatosDeNotificadoAlmacenados = false;
        this.banderaTipoPersonaNotificada = false;
        this.banderaDatosRegistroNotificacion = false;
    }

//--------------------------------------------------------------------------------------------------
    private void populateTramitesLazyly(List<Tramite> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<Tramite> rowsT;

        rowsT = this.getTramiteService().obtenerTramitesParaNotificarPorFiltro(
            this.iDsTramitesNotif, sortField,
            this.filtroBusquedaTramite, sortOrder, filters, first, pageSize);

        for (int i = 0; rowsT != null && i < rowsT.size(); i++) {
            answer.add(rowsT.get(i));
        }

    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de validar los datos basicos para la consulta de trámites
     *
     * @author juan.agudelo
     */
    private boolean validarDatosConsultaTramite() {

        NumeroPredialPartes npp = this.filtroBusquedaTramite
            .getNumeroPredialPartes();

        if (!(this.filtroBusquedaTramite.getNumeroSolicitud() != null && !this.filtroBusquedaTramite
            .getNumeroSolicitud().isEmpty()) &&
             !(this.filtroBusquedaTramite.getFechaSolicitud() != null) &&
             !(this.filtroBusquedaTramite.getTipoSolicitud() != null && !this.filtroBusquedaTramite
            .getTipoSolicitud().isEmpty()) &&
             !(this.filtroBusquedaTramite.getNumeroRadicacion() != null &&
            !this.filtroBusquedaTramite
                .getNumeroRadicacion().isEmpty()) &&
             !(this.filtroBusquedaTramite.getFechaRadicacion() != null) &&
             !(this.filtroBusquedaTramite.getTipoTramite() != null && !this.filtroBusquedaTramite
            .getTipoTramite().isEmpty()) &&
             !(this.filtroBusquedaTramite.getClaseMutacion() != null && !this.filtroBusquedaTramite
            .getClaseMutacion().isEmpty()) &&
             !(this.filtroBusquedaTramite.getSubtipo() != null && !this.filtroBusquedaTramite
            .getSubtipo().isEmpty()) &&
             !(this.filtroBusquedaTramite.getNumeroResolucion() != null &&
            !this.filtroBusquedaTramite
                .getNumeroResolucion().isEmpty()) &&
             !(this.filtroBusquedaTramite.getFechaResolucion() != null) &&
             !(this.filtroBusquedaTramite.getTipoIdentificacion() != null &&
            !this.filtroBusquedaTramite
                .getTipoIdentificacion().isEmpty()) &&
             !(this.filtroBusquedaTramite.getNumeroIdentificacion() != null &&
            !this.filtroBusquedaTramite
                .getNumeroIdentificacion().isEmpty()) &&
             !(this.filtroBusquedaTramite.getDigitoVerificacion() != null &&
            !this.filtroBusquedaTramite
                .getDigitoVerificacion().isEmpty())) {

            if ((npp.getDepartamentoCodigo() == null || npp.getDepartamentoCodigo().isEmpty()) &&
                 (npp.getMunicipioCodigo() == null || npp.getMunicipioCodigo().isEmpty()) &&
                 (npp.getTipoAvaluo() == null || npp.getTipoAvaluo().isEmpty()) &&
                 (npp.getSectorCodigo() == null || npp.getSectorCodigo().isEmpty()) &&
                 (npp.getComunaCodigo() == null || npp.getComunaCodigo().isEmpty()) &&
                 (npp.getBarrioCodigo() == null || npp.getBarrioCodigo().isEmpty()) &&
                 (npp.getManzanaVeredaCodigo() == null || !npp.getManzanaVeredaCodigo().isEmpty()) &&
                 (npp.getPredio() == null || npp.getPredio().isEmpty()) &&
                 (npp.getCondicionPropiedad() == null || npp.getCondicionPropiedad().isEmpty()) &&
                 (npp.getEdificio() == null || npp.getEdificio().isEmpty()) &&
                 (npp.getPiso() == null || npp.getPiso().isEmpty()) &&
                 (npp.getUnidad() == null || npp.getUnidad().isEmpty())) {

                String mensaje =
                    "Para realizar la busqueda de trámites se requiere por lo menos un parámetro." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.banderaBusquedaTramites = false;
                this.addMensajeError(mensaje);
                return false;
            }
        }

        if (this.filtroBusquedaTramite.getNumeroPredialPartes() != null) {

            if ((npp.getDepartamentoCodigo() != null && !npp
                .getDepartamentoCodigo().isEmpty()) ||
                 (npp.getMunicipioCodigo() != null && !npp
                .getMunicipioCodigo().isEmpty()) ||
                 (npp.getTipoAvaluo() != null && !npp.getTipoAvaluo()
                .isEmpty()) ||
                 (npp.getSectorCodigo() != null && !npp.getSectorCodigo()
                .isEmpty()) ||
                 (npp.getComunaCodigo() != null && !npp.getComunaCodigo()
                .isEmpty()) ||
                 (npp.getBarrioCodigo() != null && !npp.getBarrioCodigo()
                .isEmpty()) ||
                 (npp.getManzanaVeredaCodigo() != null && !npp
                .getManzanaVeredaCodigo().isEmpty()) ||
                 (npp.getPredio() != null && !npp.getPredio().isEmpty()) ||
                 (npp.getCondicionPropiedad() != null && !npp
                .getCondicionPropiedad().isEmpty()) ||
                 (npp.getEdificio() != null && !npp
                .getEdificio().isEmpty()) ||
                 (npp.getPiso() != null && !npp.getPiso().isEmpty()) ||
                 (npp.getUnidad() != null && !npp.getUnidad().isEmpty())) {

                if (((npp.getDepartamentoCodigo() == null || npp.getDepartamentoCodigo().isEmpty()) ||
                     (npp.getMunicipioCodigo() == null || npp.getMunicipioCodigo().isEmpty()) ||
                     (npp.getTipoAvaluo() == null || npp.getTipoAvaluo().isEmpty()) ||
                     (npp.getSectorCodigo() == null || npp.getSectorCodigo().isEmpty()) ||
                     (npp.getComunaCodigo() == null || npp.getComunaCodigo().isEmpty()) ||
                     (npp.getBarrioCodigo() == null || npp.getBarrioCodigo().isEmpty()) ||
                    
                    (npp.getManzanaVeredaCodigo() == null || npp.getManzanaVeredaCodigo().isEmpty()) ||
                     (npp.getPredio() == null || npp.getPredio().isEmpty()) ||
                     (npp.getCondicionPropiedad() == null || npp.getCondicionPropiedad().isEmpty()) ||
                     (npp.getEdificio() == null || npp.getEdificio().isEmpty()) ||
                     (npp.getPiso() == null || npp.getPiso().isEmpty()) ||
                     (npp.getUnidad() == null || npp.getUnidad().isEmpty()))) {

                    String mensaje = "Para buscar por número predial este debe ser completo." +
                         " Por favor corrija los datos e intente nuevamente.";
                    this.addMensajeError(mensaje);
                    return false;
                }
            }
        }

        return true;

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de solicitud
     *
     * @author juan.agudelo
     */
    public void cambioTipoSolicitud() {

        this.filtroBusquedaTramite.setTipoTramite(null);
        this.filtroBusquedaTramite.setClaseMutacion(null);
        this.filtroBusquedaTramite.setSubtipo(null);
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de trámite
     *
     * @author juan.agudelo
     */
    public void cambioTipoTramite() {

        this.filtroBusquedaTramite.setClaseMutacion(null);
        this.filtroBusquedaTramite.setSubtipo(null);
    }

    // ------------------------------------------------------------------------
    public void poblar() {

        try {
            
            //leidy.gonzalez::08-08-2018::#70681::Reclamar actividad por parte del usuario autenticado 
            
            List<Long> iDsTramitesProc = new ArrayList<Long>();
            
            String nombreActividades = ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION + "," +
                ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO + "," +
                ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO; 
            
            Map filtros = new HashMap<Object, List<Object>>();
                filtros.put(EParametrosConsultaActividades.TERRITORIAL, this.loggedInUser.getDescripcionEstructuraOrganizacional());
                filtros.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD,nombreActividades);
                filtros.
                    put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
            
            List<Actividad> lstActividades = this.getProcesosService().
                                    consultarListaActividades(filtros);
            
            if (lstActividades != null && !lstActividades.isEmpty()) {
                
            Parametro maxTramProc = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.MAX_TRAMITES_REGISTRAR_NOTIFICACION.toString());
                    
                if (lstActividades.size() > maxTramProc.getValorNumero()) {

                    this.addMensajeWarn(
                        "Los criterios de búsqueda ingresados no son suficientes. Agregue más criterios e intente denuevo.");
                    return;

                } else {
                    
                    for (Actividad lstActividade : lstActividades) {
                        
                        if(Long.valueOf(lstActividade.getIdObjetoNegocio()) != null){
                            iDsTramitesProc.add(lstActividade.getIdObjetoNegocio());
                        }
                            
                    }
                }

            }
            
            if(iDsTramitesProc != null && !iDsTramitesProc.isEmpty()){
                
                this.iDsTramitesNotif = this.getTramiteService()
                .obtenerTramiteIdsParaNotificarPorFiltro(
                    this.filtroBusquedaTramite,
                    this.nombreActividadSelected,iDsTramitesProc);
            }else {
                this.addMensajeWarn(
                    "Los criterios de búsqueda ingresados no son suficientes. Agregue más criterios e intente denuevo.");
                return;
            }
            

            if (this.iDsTramitesNotif != null &&
                 !this.iDsTramitesNotif.isEmpty() &&
                 this.iDsTramitesNotif.get(0).equals(-1L)) {
                this.addMensajeWarn(
                    "Los criterios de búsqueda ingresados no son suficientes. Agregue más criterios e intente denuevo.");
                this.lazyAssignedTramites.setRowCount(0);
            }
            this.banderaBusquedaTramites = true;
            this.banderaDatosRegistroNotificacion = false;

            if (this.iDsTramitesNotif.size() > 0) {
                this.lazyAssignedTramites.setRowCount(this.iDsTramitesNotif
                    .size());
            } else {
                this.lazyAssignedTramites.setRowCount(0);
            }
        } catch (Exception ex) {
            LOGGER.error("error en RegistroNotificacionMB#poblar: " +
                 ex.getMessage(), ex);
        }

    }
//--------------------------------------------------------------------------------------------------

    @SuppressWarnings("unused")
    private void populateBusquedaTramitesLazyly(List<Tramite> answer,
        int first, int pageSize) {

        this.rows = new ArrayList<Tramite>();

        int size;
        this.rows = this.getTramiteService()
            .buscarTramiteSolicitudPorFiltroTramite(filtroBusquedaTramite,
                first, pageSize);
        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado como listener sobre la acción seleccionar trámite de la tabla de resultado
     * de búsqueda de trámites
     */
    public void seleccionarTramite() {

        if (validarTramitePorNotificar()) {

            this.banderaDatosRegistroNotificacion = true;

            this.currentTramiteId = this.currentTramite.getId();

            this.solicitantesRegistroNotificacion = (ArrayList<Solicitante>) this.
                getTramiteService().
                obtenerSolicitantesParaNotificarPorTramiteId(this.currentTramiteId);

            this.cargarPropietariosNotficacion();

            //D: como pudo suceder que notificara a todos, pero no avanzara el proceso, se debe
            //   validar eso
            this.okToForwardProcess = this.validarTodosNotificados();

            //N: esto está bien porque para Conservación la lista de actividades tendrá una
            //   único elemento
            List<Actividad> actividadesProceso = this.getProcesosService()
                .obtenerActividadesProceso(this.currentTramite.getProcesoInstanciaId());

            if (actividadesProceso != null && !actividadesProceso.isEmpty()) {

                if (actividadesProceso.isEmpty()) {
                    this.currentProcessActivity = actividadesProceso.get(0);
                } else {
                    for (Actividad actividad : actividadesProceso) {
                        if (actividad.getNombre().equals(
                            ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                             actividad.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {
                            this.currentProcessActivity = actividad;
                        }
                    }
                    if (this.currentProcessActivity == null) {
                        this.currentProcessActivity = actividadesProceso.get(0);
                    }
                }

//				if(this.currentProcessActivity.getNombre()
//		        		.equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION)){            	            	
//		        	this.formaNotificacion = ETramiteDocumentoMetoNotif.PERSONAL.getValor();
//		        }else if(this.currentProcessActivity.getNombre().equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO)){            	
//		        	this.formaNotificacion = ETramiteDocumentoMetoNotif.EDICTO.getValor();
//		        }
                this.fechaNotificacion = new Date(System.currentTimeMillis());

                // N: se hacía esto para tratar de evitar que cuando no se ha escogido algo en el
                //    radio de tipo de solicitante no valide los otros campos obligatorios
                this.notificadoEsSolicitante = false;
            } else {
                String mensaje = "El trámite seleccionado no cuenta con una" +
                     " actividad asociada en el proceso.";
                this.addMensajeError(mensaje);
            }
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }
        this.banderaDatosDeNotificadoAlmacenados = false;
        this.banderaTipoPersonaNotificada = false;
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado al aceptar que un trámite no puede ser notificado sobre la acción del boton
     * aceptar en la ventana de confirmación de trámite no se encuentra para ser notificado
     */
    public void aceptarNoNotificar() {

        this.currentTramite = null;
    }

//-------------------------------------------------------------------------
    /**
     * Método que valida si el trámeite seleccionado se encuentra pendiente por notificación.
     *
     * @modified pedro.garcia orden
     */
    private boolean validarTramitePorNotificar() {

        List<Actividad> actividadesTramite;

        actividadesTramite = this.getProcesosService().obtenerActividadesProceso(
            this.currentTramite.getProcesoInstanciaId());

        if (actividadesTramite != null && !actividadesTramite.isEmpty()) {

            for (Actividad aTmp : actividadesTramite) {
                if (aTmp.getNombre()
                    .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                     aTmp.getNombre()
                        .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO) ||
                     aTmp.getNombre()
                        .equals(ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {
                    
                    if (aTmp.getUsuarioEjecutor() != null) {

                        if (ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString().equals(
                            aTmp.getUsuarioEjecutor()) || this.loggedInUser.getLogin().equals(aTmp.
                                getUsuarioEjecutor())) {

                            return true;
                        } else {
                            this.habilitaBuscar = false;
                            String mensaje = "El trámite se encuentra" +
                                " seleccionado por otro usuario.";
                            this.addMensajeWarn(mensaje);
                            return false;
                        }

                    }
                    
                    
                }
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado como listener sobre la acción deseleccionar trámite de la tabla de resultado
     * de búsqueda de trámites
     */
    public void deseleccionarTramite() {
        this.banderaDatosRegistroNotificacion = false;

        this.currentProcessActivity.setId(null);
        this.solicitantesRegistroNotificacion = null;
        this.notificadoEsSolicitante = true;
        this.banderaDatosDeNotificadoAlmacenados = false;
        this.banderaTipoPersonaNotificada = false;
    }

//-------------------------------------------------------------------------
    /**
     * Método ejecutado como listener sobre la acción seleccionar solicitante de la tabla de
     * resultado de búsqueda de solicitantes del trámite
     */
    /*
     * @modified pedro.garcia 18-07-2012 mensaje de aviso cuando se trata de notificar a alguien ya
     * notificado @modified franz.gamba 14-08-2012 segun la transicón se define la forma de
     * notificación
     */
    public void seleccionarSolicitante() {

        if (ESiNo.SI.getCodigo().equals(this.selectedSolicitante.getSolicitanteNotificado())) {
            this.addMensajeInfo("Este solicitante ya ha sido notificado");
            this.selectedSolicitante = null;
            return;
        } else {

            this.tipoPersonaNotif = RegistroNotificacionMB.tipoPersonaNotifSolicitante;
            setTipoPersonaNotif(this.tipoPersonaNotif);

            if (this.currentProcessActivity.getNombre()
                .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION)) {
                this.formaNotificacion = ETramiteDocumentoMetoNotif.PERSONAL.getCodigo();
            } else if (this.currentProcessActivity.getNombre().equals(
                ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO)) {
                this.formaNotificacion = ETramiteDocumentoMetoNotif.EDICTO.getCodigo();
            }

            this.tipoIdentificPersonaNotif = this.selectedSolicitante
                .getTipoIdentificacion();
            this.numeroIdentificPersonaNotif = this.selectedSolicitante
                .getNumeroIdentificacion();
            this.nombrePersonaNotif = this.selectedSolicitante.getNombreCompleto();
            this.banderaDatosDeNotificadoAlmacenados = false;
            this.banderaTipoPersonaNotificada = false;

        }
    }
//-------------------------------------------------------------------------

    /**
     * Método ejecutado como listener para el cambio de la seleccion de la forma de notificación
     */
    /*
     * @modified pedro.garcia 18-07-2012 el chambón del Gabriel había hecho que al cambiar el valor
     * seleccionado en el combo de forma de notificación se procesara la forma y por lo tanto la
     * acció fuera equivalente a dar click en el botón "guardar notificado". Se cambió eso para
     * conservar un flujo coherente. Ya no se llama a este método desde la interfaz
     */
    public void onChangeFormaNotificacion() {
//		if (this.formaNotificacion != null) {
//			this.banderaDatosDeNotificadoAlmacenados = true;
//		}
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado como listener sobre la acción deseleccionar solicitante de la tabla de
     * resultado de búsqueda de solicitante de trámite
     */
    public void deseleccionarSolicitante() {
        this.tipoPersonaNotif = RegistroNotificacionMB.tipoPersonaNotifAutorizado;
        setTipoPersonaNotif(this.tipoPersonaNotif);

        this.tipoIdentificPersonaNotif = null;
        this.numeroIdentificPersonaNotif = null;
        this.nombrePersonaNotif = null;
        this.banderaDatosDeNotificadoAlmacenados = false;
    }

//-------------------------------------------------------------------------
    /**
     * action del botón "guardar notificado". Hace que se venga hasta el MB, por lo cual los datos
     * del solicitante quedan guardados en las variables correspondientes. Pone en true la bandera
     * que indica ello.
     */
    /*
     * @documented pedro.garcia
     */
    public void guardarDatosNotificado() {
        this.banderaDatosDeNotificadoAlmacenados = true;
        this.banderaTipoPersonaNotificada = true;

        if (this.solicitantesSeleccionados != null &&
             this.solicitantesSeleccionados.length > 1) {

            this.addMensajeWarn(ECodigoErrorVista.MODIFICACION_PH_059
                .getMensajeUsuario());
            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_059
                .getMensajeTecnico());
            return;
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método encargado de guardar los datos de notificado y bloquear la edición de los mismos
     */
    public void editarDatosNotificado() {
        this.banderaDatosDeNotificadoAlmacenados = false;
        this.banderaTipoPersonaNotificada = false;
//		this.documentoAutorizacionDeNotificacion = null;
        this.docAutorizacionCargado = false;
//		this.documentoNotificacion = null;
        this.docNotifCargado = false;
        this.banderaDocNotificacionGenerado = false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "avanzar proceso".
     *
     * PRE: el botón que tiene como action este método solo se activa si todos lo solicitantes
     * aparecen como notificados, por lo tanto no se valida eso aquí
     *
     * 1. Se obtienen los TramiteDocumento del trámite 2. Se validan las condiciones para determinar
     * si todos los solicitantes renunciaron a recursos o no. 3. Se invoca al método que mueve el
     * proceso
     *
     * @author pedro.garcia
     */
    public String moverProceso() {

        List<TramiteDocumento> tramiteDocumentos;

        int contadorSolicitantesNoRenuncian = 0;
        int contadorSolicitantesRenuncian = 0;

        try {

            // 1.
            tramiteDocumentos = this.getTramiteService().obtenerTramiteDocumentosPorTramiteId(
                this.currentTramite.getId());

            if (this.currentTramite.getFuncionarioEjecutor() != null && this.currentTramite.
                getFuncionarioEjecutor().equals(Constantes.FUNCIONARIO_EJECUTOR_IPER)) {
                // No debe validar TramiteDocumento para IPER
            } else if (tramiteDocumentos == null || tramiteDocumentos.isEmpty()) {
                throw new Exception("No se pudieron obtener los TramiteDocumentos del trámite " +
                     "con id " + this.currentTramite.getId());
            }

            if (this.solicitantesRegistroNotificacion == null ||
                 this.solicitantesRegistroNotificacion.isEmpty()) {
                throw new Exception("No hay solicitantes del trámite");
            }

            //D: para cada TramiteDocumento se valida que:
            //   el Documento asociado sea de tipo comunicación de notificación,
            //   la persona notificada sea uno de los solicitantes del trámite,
            //   el campo renuncia_recursos sea diferente de null.
            //   Esto garantiza que se revisan los que deben serlo
            for (TramiteDocumento tdTemp : tramiteDocumentos) {

                for (Solicitante sTemp : this.solicitantesRegistroNotificacion) {

                    if (tdTemp.getDocumento().getTipoDocumento().getId().equals(
                        ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId()) &&
                         (tdTemp.getRenunciaRecursos() != null &&
                         !tdTemp.getRenunciaRecursos().isEmpty()) &&
                         tdTemp.getIdPersonaNotificacion().equals(sTemp.getNumeroIdentificacion())) {

                        if (tdTemp.getRenunciaRecursos().equals(ESiNo.SI.getCodigo())) {
                            contadorSolicitantesRenuncian++;
                        } else {
                            contadorSolicitantesNoRenuncian++;
                        }
                        break;
                    }
                }
            }

            //D: si contadorSolicitantesNoRenuncian == 0 quiere decir que todos los solicitantes
            //  renuncian a interposición de recursos.
            //  Dependiendo de esto el proceso se mueve a una u otra actividad.
            if (contadorSolicitantesNoRenuncian == 0) {
                this.solicitantesRenuncian = true;
            } else {
                this.solicitantesRenuncian = false;
            }

            this.forwardThisProcess();

        } catch (Exception ex) {
            LOGGER.error("error en RegistroNotificacionMB#moverProceso: " + ex.getMessage(), ex);
        }

        return this.cerrarVentanaCU();
    }

    /**
     * Método para obtener los trámites de vía gubernativa activos, asociados a un predio.
     *
     * @return
     * @author juanfelipe.garcia
     */
    private List<Tramite> recursosViaGubernativa() {
        List<Tramite> listaTramites = new ArrayList<Tramite>();;

        //consulta otros tramites de via gubernativa asociados al predio
        FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
        FiltroDatosConsultaPredio datosConsultaPredio = new FiltroDatosConsultaPredio();

        datosConsultaPredio.setNumeroPredial(this.currentTramite.getPredio().getNumeroPredial());
        filtrosTramite.setTipoSolicitud(this.currentTramite.getSolicitud().getTipo());
        filtrosTramite.setEstadoTramite("0");
        //filtrar tramites que no esten activos
        for (Tramite t : this.getTramiteService().consultaDeTramitesPorFiltros(true, filtrosTramite,
            datosConsultaPredio, null, false, null, null, null)) {
            if (!t.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.getCodigo()) &&
                 !t.getEstado().equals(ETramiteEstado.FINALIZADO.getCodigo()) &&
                 !t.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo())) {

                listaTramites.add(t);
            }
        }

        return listaTramites;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * recorre la lista de notificados y revisa si todos ya han sido notificados
     *
     * @author pedro.garcia
     * @return
     */
    private boolean validarTodosNotificados() {
        boolean answer = true;

        for (Solicitante solicitante : this.solicitantesRegistroNotificacion) {
            if (solicitante.getSolicitanteNotificado() == null ||
                 ESiNo.NO.getCodigo().equals(
                    solicitante.getSolicitanteNotificado())) {
                answer = false;
                break;
            }
        }

        return answer;
    }

    /**
     * Método que permite precargar un autroizado para registrar la notificacion
     */
    public void precargarAutorizado() {

        this.ppersonasAux = new ArrayList<PPersona>();

        if (this.numeroIdentificPersonaNotif == null || this.numeroIdentificPersonaNotif.isEmpty() ||
             this.tipoIdentificPersonaNotif == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Debe ingresar tipo y número de identificación.");
            return;
        } else {

            this.ppersonasAux = this.getConservacionService()
                .buscarPPersonasPorTipoNumeroIdentificacion(
                    this.tipoIdentificPersonaNotif,
                    this.numeroIdentificPersonaNotif);

            this.ppersonasAux = this.filtrarPropietariosCoincidentes(ppersonasAux);

            if (this.ppersonasAux == null || this.ppersonasAux.size() == 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeWarn("No se encontró a la persona.");
                return;
            }
        }
    }

    public void aceptarSeleccion() {

        String nombreCompleto = new String();

        if (this.personaSeleccionada.getPrimerNombre() != null &&
             !this.personaSeleccionada.getPrimerNombre().isEmpty()) {
            nombreCompleto = this.personaSeleccionada.getPrimerNombre();
        }

        if (this.personaSeleccionada.getSegundoNombre() != null &&
             !this.personaSeleccionada.getSegundoNombre().isEmpty()) {
            nombreCompleto = nombreCompleto + " " + this.personaSeleccionada.getSegundoNombre();
        }

        if (this.personaSeleccionada.getPrimerApellido() != null &&
             !this.personaSeleccionada.getPrimerApellido().isEmpty()) {
            nombreCompleto = nombreCompleto + " " + this.personaSeleccionada.getPrimerApellido();
        }

        if (this.personaSeleccionada.getSegundoApellido() != null &&
             !this.personaSeleccionada.getSegundoApellido().isEmpty()) {
            nombreCompleto = nombreCompleto + " " + this.personaSeleccionada.getSegundoApellido();
        }

        if (nombreCompleto != null && !nombreCompleto.isEmpty()) {
            this.nombrePersonaNotif = nombreCompleto;
        }

    }

    /**
     * Método para filtrar los propietarios duplicados
     *
     * @author leidy.gonzalez
     * @param propietarios
     * @return
     */
    private List<PPersona> filtrarPropietariosCoincidentes(List<PPersona> propietarios) {
        List<PPersona> resultado = new ArrayList<PPersona>();

        for (PPersona pp : propietarios) {
            boolean existe = false;
            for (int i = 0; i < resultado.size(); i++) {
                PPersona ppAux = resultado.get(i);

                if (pp.getNombre().equals((ppAux.getNombre()))) {
                    existe = true;
                    break;
                }
            }
            if (!existe) {
                resultado.add(pp);
            }
        }

        return resultado;
    }

    //--------------------------------------------------------------------------------------------
    /**
     * Método que limpia los campos de la busqueda de trámites
     */
    public void limpiarCampos() {

        this.filtroBusquedaTramite = new FiltroDatosConsultaTramite();
        this.filtroBusquedaTramite
            .setNumeroPredialPartes(new NumeroPredialPartes());
        this.lazyAssignedTramites = null;
        this.banderaBusquedaTramites = false;
        this.banderaDatosRegistroNotificacion = false;
        this.habilitaBuscar = false;
        this.nombreActividadSelected = ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION;
    }

    //--------------------------------------------------------------------------------------------
    /**
     * Método get que consulta la resolución en alfresco y realiza el llamado al método que adiciona
     * una marca de agua a ésta.
     *
     * @author david.cifuentes
     * @modified javier.aponte
     */
    public void cargarUrlDocumentoConMarcaDeAgua() {

        if (this.documentoResolucion != null &&
             this.documentoResolucion.getIdRepositorioDocumentos() != null) {

            this.reporteDocumentoWaterMark = this.reportsService.consultarReporteDeGestorDocumental(
                this.documentoResolucion.getIdRepositorioDocumentos());

        }
    }

    // ----------------------------------------------------- //
    /**
     * Método usado para saber que actividad ha sido seleccionada en la búsqueda de trámites con el
     * fin de personalizar la pantalla con algunos nombres particulares para cada actividad para el
     * caso en que sea 'Radicar recurso'.
     *
     * @author david.cifuentes
     * @return
     */
    public boolean isActividadRadicarRecurso() {
        if (ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO
            .equals(this.nombreActividadSelected)) {
            return true;
        }
        return false;
    }

    // ----------------------------------------------------- //
    /**
     * Método usado para saber que actividad ha sido seleccionada en la búsqueda de trámites con el
     * fin de personalizar la pantalla con algunos nombres particulares para cada actividad para el
     * caso en que sea 'Registrar Notificación'.
     *
     * @author david.cifuentes
     * @return
     */
    public boolean isActividadRegistrarNotificacion() {
        if (ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION
            .equals(this.nombreActividadSelected)) {
            return true;
        }
        return false;
    }

    // ----------------------------------------------------- //
    /**
     * Método para definir el título del dialog de confirmación al avanzar el proceso, dependiendo
     * de la actividad actual donde se encuentre el trámite.
     *
     * @author david.cifuentes
     * @return
     */
    public String getMessageConfirmationForward() {
        String messageConfirmation = "";
        if (this.isActividadRegistrarNotificacion()) {
            messageConfirmation = "Confirmación de notificaciones completas";
        } else if (this.isActividadRadicarRecurso()) {
            messageConfirmation = "Confirmación de renuncia a recurso completas";
        } else {
            messageConfirmation = "Confirmación";
        }
        return messageConfirmation;
    }

    // ----------------------------------------------------- //
    /**
     * Método que verifica que se haya realizado el cargue de la renuncia de recursos de los
     * solicitantes, de ser así habilita el botón de avanzar en el proceso.
     *
     * @author david.cifuentes
     * @return
     */
    public void verificarEstadoOkProcessRadicarRecurso() {
        int conteoDocumentosRadicarRecurso = 0;

        // Validar si ya se realizó la radicación del recurso para la totalidad
        // de los solicitantes.
        List<TramiteDocumentacion> tramiteDocumentacion = this
            .getTramiteService().obtenerTramiteDocumentacionPorIdTramite(
                this.currentTramiteId);

        if (tramiteDocumentacion != null) {
            for (TramiteDocumentacion td : tramiteDocumentacion) {
                if (td.getTipoDocumento() != null &&
                     ETipoDocumento.RADICAR_RECURSO.getNombre().equals(td
                        .getTipoDocumento().getNombre())) {
                    conteoDocumentosRadicarRecurso++;
                }
            }
        }

        if (this.solicitantesRegistroNotificacion.size() > conteoDocumentosRadicarRecurso) {
            this.okToForwardProcess = false;
        } else {
            this.okToForwardProcess = true;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que guarda el registro de una radicación de recurso.
     *
     * @author david.cifuentes
     * @return
     */
    public void guardarRegistroRadicarRecurso() {
        String mensajeError = null;
        boolean showError = true;

        try {

            if (this.documentoRadicarRecurso != null) {
                if (!this.notificadoEsSolicitante) {
                    if (this.documentoAutorizacionDeNotificacion == null ||
                        this.documentoAutorizacionDeNotificacion
                            .getArchivo() == null) {
                        this.addMensajeError("Por favor cargue el documento de autorización.");
                        return;
                    } else {
                        this.documentoAutorizacionDeNotificacion = this
                            .armarDocumento(
                                ETipoDocumento.DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION
                                    .getNombre(),
                                this.documentoAutorizacionDeNotificacion
                                    .getArchivo(),
                                "Archivo de autorización");
                        TramiteDocumentacion td = new TramiteDocumentacion();
                        td.setDocumentoSoporte(this.documentoAutorizacionDeNotificacion);
                        td.setFechaLog(new Date(System.currentTimeMillis()));
                        td.setFechaAporte(new Date(System.currentTimeMillis()));
                        td.setUsuarioLog(this.loggedInUser.getLogin());
                        td.setAportado(ESiNo.SI.getCodigo());
                        td.setTipoDocumento(this.documentoAutorizacionDeNotificacion
                            .getTipoDocumento());
                        td.setTramite(this.currentTramite);
                        td.setCantidadFolios(0);
                        td.setAdicional(ESiNo.SI.getCodigo());
                        this.getTramiteService().guardarTramiteDocumentacion(td);
                    }
                }
                this.documentoRadicarRecurso = this.armarDocumento(
                    ETipoDocumento.RADICAR_RECURSO.getNombre(),
                    this.documentoRadicarRecurso.getArchivo(),
                    "Archivo de radicación del recurso");

                TramiteDocumentacion td = new TramiteDocumentacion();
                td.setDocumentoSoporte(this.documentoRadicarRecurso);

                // D: la fecha de notificación del TramiteDocumento se actualiza
                // porque la que debe quedar
                // es la de este momento, que es cuando se considera notificado al
                // solicitante
                td.setFechaLog(new Date(System.currentTimeMillis()));
                td.setFechaAporte(new Date(System.currentTimeMillis()));
                td.setUsuarioLog(this.loggedInUser.getLogin());
                td.setAportado(ESiNo.SI.getCodigo());
                td.setTipoDocumento(this.documentoRadicarRecurso.getTipoDocumento());
                td.setTramite(this.currentTramite);
                td.setCantidadFolios(0);
                td.setAdicional(ESiNo.SI.getCodigo());

                // 4. Para esto se llama a un método que hace todo
                this.getTramiteService().guardarTramiteDocumentacion(td);

                this.selectedSolicitante = null;
                this.banderaDatosDeNotificadoAlmacenados = false;
                this.banderaTipoPersonaNotificada = false;
//				this.documentoRadicarRecurso = null;
//				this.documentoAutorizacionDeNotificacion = null;
//				this.documentoNotificacion = null;
//				this.renunciaARecursos = null;

                this.verificarEstadoOkProcessRadicarRecurso();
            } else {
                this.addMensajeError("Por favor seleccione el archivo a cargar.");
            }
        } catch (Exception ex) {
            LOGGER.error("error en RegistroNotificacionMB#guardarRegistroNotificacion: " +
                 ex.getMessage(), ex);
            if (showError) {
                this.addMensajeError(mensajeError);
            }
        }
    }

    /**
     * Metodo para cagar los propietarios que se deben notificar del tramite
     *
     * @author felipe.cadena
     */
    public void cargarPropietariosNotficacion() {

        //felipe.cadena::21-08-2015::Se cargan los propietarios como afectados para la notificacion para tramites de rectificacion y tercera masivas
        //leidy.gonzalez::05/10/2017::Se adiciona cargue de propietarios para desenglobe
        if (this.currentTramite.isRectificacionMatriz() || this.currentTramite.isTerceraMasiva() ||
             this.currentTramite.isDesenglobeEtapas() && !this.currentTramite.isEnglobeVirtual()) {
            List<Solicitante> afectadosPropietarios = this.getTramiteService().
                obtenerSolicitantesParaNotificacion(this.currentTramiteId);

            this.solicitantesRegistroNotificacion.addAll(afectadosPropietarios);
        }

        //leidy.gonzalez::0/06/2018::#65337::Se adiciona cargue de propietarios 
        // de predios 0 y para englobe Virtual
        if (this.currentTramite.isEnglobeVirtual()) {
            List<Solicitante> propietariosEV = this.getTramiteService().
                obtenerSolicitantesParaNotificacionEV(this.currentTramiteId);

            this.solicitantesRegistroNotificacion.addAll(propietariosEV);
        }

        List<TramiteDocumento> documentosTramite = this.getTramiteService().
            obtenerTramiteDocumentosPorTramiteId(this.currentTramiteId);

        //D: se revisan condiciones para decir si el solicitante ya fue notificado
        //D: si el trámite tiene documentos se revisan las condiciones
        if (documentosTramite != null &&
             !documentosTramite.isEmpty()) {

            for (Solicitante sTemp : this.solicitantesRegistroNotificacion) {
                for (TramiteDocumento tdTemp : documentosTramite) {

                    //D: un solicitante se da por notificado cuando ya se ha registrado la 
                    //   notificación, y por lo tanto existe un Documento de notificación asociado
                    if (tdTemp.getIdPersonaNotificacion() != null &&
                         tdTemp.getIdPersonaNotificacion().equals(
                            sTemp.getNumeroIdentificacion())) {

                        if (tdTemp.getNotificacionDocumentoId() != null) {
                            sTemp.setSolicitanteNotificado(ESiNo.SI.getCodigo());
                            break;
                        } else {
                            sTemp.setSolicitanteNotificado(ESiNo.NO.getCodigo());
                        }
                    }
                }
            }
        } //D ... si no, se asume que no ha sido notificado
        else {
            for (Solicitante sTemp : this.solicitantesRegistroNotificacion) {
                sTemp.setSolicitanteNotificado(ESiNo.NO.getCodigo());
            }
        }
    }

    /**
     * Método ejecutado como listener sobre la acción seleccionar solicitante de la tabla de
     * resultado de búsqueda de solicitantes de trámites masivos
     *
     * @author leidy.gonzalez
     */
    public void seleccionSolicitantes() {

        if (this.solicitantesSeleccionados.length > 0) {

            if (this.solicitantesSeleccionados.length == 1) {

                for (Solicitante solicitanteSel : this.solicitantesSeleccionados) {

                    if (ESiNo.SI.getCodigo().equals(solicitanteSel.getSolicitanteNotificado())) {
                        this.addMensajeInfo("Este solicitante ya ha sido notificado");
                        this.selectedSolicitante = null;

                    }

                    this.selectedSolicitante = solicitanteSel;
                    this.tipoPersonaNotif = RegistroNotificacionMB.tipoPersonaNotifSolicitante;
                    setTipoPersonaNotif(this.tipoPersonaNotif);

                    if (this.currentProcessActivity.getNombre()
                        .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION)) {
                        this.formaNotificacion = ETramiteDocumentoMetoNotif.PERSONAL.getCodigo();
                    } else if (this.currentProcessActivity.getNombre().equals(
                        ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO)) {
                        this.formaNotificacion = ETramiteDocumentoMetoNotif.EDICTO.getCodigo();
                    }

                    this.tipoIdentificPersonaNotif = this.selectedSolicitante
                        .getTipoIdentificacion();
                    this.numeroIdentificPersonaNotif = this.selectedSolicitante
                        .getNumeroIdentificacion();
                    this.nombrePersonaNotif = this.selectedSolicitante.getNombreCompleto();
                    this.banderaDatosDeNotificadoAlmacenados = false;
                    this.banderaTipoPersonaNotificada = false;

                }

            } else if (this.solicitantesSeleccionados.length > 1) {

                for (Solicitante solicitanteSel : this.solicitantesSeleccionados) {
                    this.selectedSolicitante = null;
                    if (ESiNo.SI.getCodigo().equals(solicitanteSel.getSolicitanteNotificado())) {
                        this.addMensajeInfo("Este solicitante ya ha sido notificado");
                        this.selectedSolicitante = null;

                    }

                    if (this.currentProcessActivity.getNombre()
                        .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION)) {
                        this.formaNotificacion = ETramiteDocumentoMetoNotif.PERSONAL.getCodigo();
                    } else if (this.currentProcessActivity.getNombre().equals(
                        ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO)) {
                        this.formaNotificacion = ETramiteDocumentoMetoNotif.EDICTO.getCodigo();
                    }

                    this.tipoPersonaNotif = RegistroNotificacionMB.tipoPersonaNotifAutorizado;
                    setTipoPersonaNotif(this.tipoPersonaNotif);

                    this.banderaDatosDeNotificadoAlmacenados = false;
                    this.banderaTipoPersonaNotificada = true;

                }
            }
        }

    }

    /**
     * Método ejecutado como listener sobre la acción deseleccionar solicitante de la tabla de
     * resultado de búsqueda de solicitante de trámites masivos
     *
     * @author leidy.gonzalez
     */
    public void deseleccionSolicitantes() {
        this.tipoPersonaNotif = RegistroNotificacionMB.tipoPersonaNotifAutorizado;
        setTipoPersonaNotif(this.tipoPersonaNotif);

        this.tipoIdentificPersonaNotif = null;
        this.numeroIdentificPersonaNotif = null;
        this.nombrePersonaNotif = null;
        this.banderaDatosDeNotificadoAlmacenados = false;
        this.banderaTipoPersonaNotificada = false;
    }
    
    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author leidy.gonzalez
     */
    private boolean reclamarActividad(Tramite tramiteReclamar) {
       
        List<Actividad> actividades = this.getProcesosService().obtenerActividadesProceso(tramiteReclamar.getProcesoInstanciaId());

        Actividad actividad = actividades.get(0);
        
        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.loggedInUser.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    if (tramiteReclamar.getNumeroRadicacion() != null && !tramiteReclamar.
                        getNumeroRadicacion().isEmpty()) {
                        this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                            tramiteReclamar.getNumeroRadicacion()));
                    } else {
                        this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA_NO_RAD.
                            getMensajeUsuario(tramiteReclamar.getSolicitud().getNumero()));
                    }
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.loggedInUser);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                if (tramiteReclamar.getNumeroRadicacion() != null && !tramiteReclamar.
                    getNumeroRadicacion().isEmpty()) {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                        tramiteReclamar.getNumeroRadicacion()));
                } else {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA_NO_RAD.getMensajeUsuario(
                        tramiteReclamar.getSolicitud().getNumero()));
                }
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }
    }
    
    /**
     * Metodo encargado de asociar los solicitantes y la informacion para 
     * el registro de notificacion del tramite
     *
     * @author leidy.gonzalez
     */
    public void asociarSolicitantesATramiteSeleccionado() {

        this.banderaDatosRegistroNotificacion = true;

        this.currentTramiteId = this.currentTramite.getId();
        
        try {
            
            this.solicitantesRegistroNotificacion = (ArrayList<Solicitante>) this.
                getTramiteService().
                obtenerSolicitantesParaNotificarPorTramiteId(this.currentTramiteId);

            this.cargarPropietariosNotficacion();

            //D: como pudo suceder que notificara a todos, pero no avanzara el proceso, se debe
            //   validar eso
            this.okToForwardProcess = this.validarTodosNotificados();

            this.fechaNotificacion = new Date(System.currentTimeMillis());

            // N: se hacía esto para tratar de evitar que cuando no se ha escogido algo en el
            //    radio de tipo de solicitante no valide los otros campos obligatorios
            //this.notificadoEsSolicitante = false;

            this.banderaDatosDeNotificadoAlmacenados = false;
            this.banderaTipoPersonaNotificada = false;
            
        } catch (Exception e) {
            
            LOGGER.error("error en RegistroNotificacionMB#asociarSolicitantesATramiteSeleccionado: " +
                 e.getMessage(), e);
            
            this.addMensajeError("No se pudo asociar los Solicitantes al Tramite");
        }

    }
    
    /**
     * Metodo encargado de transferir el tramite al usuario autenticado
     *
     * @author leidy.gonzalez
     */
    public void transferirTramiteAUsuario(){
        
        try {
            
            if (this.currentTramite != null && this.currentTramite.getProcesoInstanciaId() != null) {

                    List<Actividad> actividadesProceso = this.getProcesosService()
                        .obtenerActividadesProceso(this.currentTramite.getProcesoInstanciaId());

                    if (actividadesProceso != null && !actividadesProceso.isEmpty()) {

                        if (actividadesProceso.isEmpty()) {
                            this.currentProcessActivity = actividadesProceso.get(0);
                        } else {
                            for (Actividad actividad : actividadesProceso) {
                                if (actividad.getNombre().equals(
                                    ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                                    actividad.getNombre().equals(
                                        ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {
                                    this.currentProcessActivity = actividad;
                                }
                            }
                            if (this.currentProcessActivity == null) {
                                this.currentProcessActivity = actividadesProceso.get(0);
                            }
                        }
                        
                    }else {
                        String mensaje = "El trámite seleccionado no cuenta con una" +
                            " actividad asociada en el proceso.";
                        this.addMensajeError(mensaje);
                    }

                
                //Se transfiere tramite
                this.getProcesosService().transferirActividadConservacion(
                    this.currentProcessActivity.getId(), this.loggedInUser);
                this.habilitaBuscar = true;
            }

        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en RegistroNotificacionMB#transferirTramiteAUsuario: " +
                 ex.getMessage(), ex);
            
            this.addMensajeError("No se pudo transferir tramite a Usuario Logeado");
            
        }  
        
    }
    
    /**
     * Metodo encargado de cargar informacion para el registro notificacion
     *
     * @author leidy.gonzalez
     */
    public void cargueInformacionRegistroNotificacion() {
        
        if (validarTramitePorNotificar()) {
            this.banderaDatosRegistroNotificacion = true;
            this.transferirTramiteAUsuario();
            //hector.arias::21-11-2018::#70681::Reclamar actividad por parte del usuario autenticado        
            this.reclamarActividad(this.currentTramite);
            this.asociarSolicitantesATramiteSeleccionado();
        }

    }
    
    
    /**
     * Metodo encargado de transferir el tramite al usuario autenticado
     *
     * @author leidy.gonzalez
     */
    public void transferirTramiteAUsuarioTiemposMuertos(){
        
        try {
            
            if (this.currentTramite != null && this.currentTramite.getProcesoInstanciaId() != null) {

                if (this.currentProcessActivity == null || this.currentProcessActivity.getId() == null) {

                    List<Actividad> actividadesProceso = this.getProcesosService()
                        .obtenerActividadesProceso(this.currentTramite.getProcesoInstanciaId());

                    if (actividadesProceso != null && !actividadesProceso.isEmpty()) {

                        if (actividadesProceso.isEmpty()) {
                            this.currentProcessActivity = actividadesProceso.get(0);
                        } else {
                            for (Actividad actividad : actividadesProceso) {
                                if (actividad.getNombre().equals(
                                    ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                                    actividad.getNombre().equals(
                                        ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {
                                    this.currentProcessActivity = actividad;
                                }
                            }
                            if (this.currentProcessActivity == null) {
                                this.currentProcessActivity = actividadesProceso.get(0);
                            }
                        }
                        
                    }else {
                        String mensaje = "El trámite seleccionado no cuenta con una" +
                            " actividad asociada en el proceso.";
                        this.addMensajeError(mensaje);
                    }
                }
                //Consulta de Usuario de Tiempos Muertos
                UsuarioDTO usuarioTiemposMuertos = this.getGeneralesService().
                getCacheUsuario(ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());
                
                if (usuarioTiemposMuertos != null) {
                    //Se transfiere tramite
                    this.getProcesosService().transferirActividadConservacion(
                        this.currentProcessActivity.getId(), usuarioTiemposMuertos);
                }
                
            }
            
        } catch (ExcepcionSNC ex) {
            LOGGER.error("error en RegistroNotificacionMB#transferirTramiteAUsuarioTiemposMuertos: " +
                 ex.getMessage(), ex);
            
            this.addMensajeError("No se pudo transferir tramite a Usuario Tiempos Muertos");
            
        }  
        
    }

    public void setupTramiteSeleccionado() {

        this.banderaDatosRegistroNotificacion = false;

        if (this.currentTramite != null &&
                this.currentTramite.getId() != null) {

            if (this.tramiteTemp != null &&
                    this.tramiteTemp.getId() != null) {

                if(!this.tramiteTemp.getId().equals(this.currentTramite.getId())){

                    Actividad act = new Actividad();

                    List<Actividad> actividadesProceso = this.getProcesosService()
                            .obtenerActividadesProceso(this.tramiteTemp.getProcesoInstanciaId());

                    if (actividadesProceso != null && !actividadesProceso.isEmpty()) {

                        if (actividadesProceso.isEmpty()) {
                            act = actividadesProceso.get(0);
                        } else {
                            for (Actividad actividad : actividadesProceso) {
                                if (actividad.getNombre().equals(
                                        ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                                        actividad.getNombre().equals(
                                                ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {
                                    act = actividad;
                                }
                            }
                            if (act == null) {
                                act = actividadesProceso.get(0);
                            }
                        }

                    }else {
                        String mensaje = "El trámite seleccionado no cuenta con una" +
                                " actividad asociada en el proceso.";
                        this.addMensajeError(mensaje);
                    }

                    //Consulta de Usuario de Tiempos Muertos
                    UsuarioDTO usuarioTiemposMuertos = this.getGeneralesService().
                            getCacheUsuario(ProcesoDeConservacion.USUARIO_TIEMPOS_MUERTOS.toString());

                    if (usuarioTiemposMuertos != null) {
                        //Se transfiere tramite
                        this.getProcesosService().transferirActividadConservacion(
                                act.getId(), usuarioTiemposMuertos);
                    }

                }

            }

            this.tramiteTemp = (Tramite)this.currentTramite.clone1();


        }
    }

    // end of class
}
