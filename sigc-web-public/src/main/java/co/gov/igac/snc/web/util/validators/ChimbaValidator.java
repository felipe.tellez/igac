package co.gov.igac.snc.web.util.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * ejemplo de validator
 *
 * @author pagm
 */
@FacesValidator(value = "chValidator", isDefault = false)
public class ChimbaValidator implements Validator {

    /**
     * solo para probar paso de parámetros al validator desde la página
     */
    private boolean chimbaRequired;

    private static final String CHIMBA_REGEX = "chimba [a-z]{3,}";

    @Override
    public void validate(FacesContext context, UIComponent component, Object value)
        throws ValidatorException {

        Pattern pattern;
        Matcher matcher;
        String text;

        pattern = Pattern.compile(CHIMBA_REGEX);
        text = (String) value;
        matcher = pattern.matcher(text);

        initProps(component);

        if (this.chimbaRequired) {
            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage();
                message.setDetail(text + " no es algo '" + CHIMBA_REGEX + "'");
                message.setSummary(text + " no es algo '" + CHIMBA_REGEX + "'");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(message);
            } else {
                FacesMessage message = new FacesMessage();
                message.setDetail("sí es algo '" + CHIMBA_REGEX + "'");
                message.setSeverity(FacesMessage.SEVERITY_INFO);
            }
        } else {
            FacesMessage message = new FacesMessage();
            message.setDetail("no se requiere algo chimba!!");
            message.setSeverity(FacesMessage.SEVERITY_INFO);
        }

    }

    private void initProps(UIComponent component) {
        Boolean optional = Boolean.valueOf((String) component.getAttributes().
            get("chimbaRequired"));

        this.chimbaRequired = (optional == null) ? this.chimbaRequired : optional.booleanValue();
    }

    public boolean isChimbaRequired() {
        return chimbaRequired;
    }

    public void setChimbaRequired(boolean chimbaRequired) {
        this.chimbaRequired = chimbaRequired;
    }

}
