package co.gov.igac.snc.web.controller.conservacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.web.controller.BaseController;

//TODO :: juan.mendez :: documentar esta clase. NO eliminar el TODO sin hacerlo :: pedro.garcia
@Controller
@RequestMapping(value = "/predio/**")
public class PredioController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 2674011378290540885L;
    private static final Logger LOGGER = LoggerFactory.getLogger(PredioController.class);

    /**
     * interfaces de servicio
     */
    public PredioController() {

    }

    //method =RequestMethod.GET, 
    //@RequestMapping(value="/{id}", headers = { "Accept=application/xml, text/xml" })
    @RequestMapping(value = "/{id}")
    public String get(@PathVariable Long id, ModelMap modelMap) {
        Predio p = this.getConservacionService().obtenerPredioPorId(id);
        Predio predioConDatos = this.getConservacionService().
            obtenerPredioConDatosUbicacionPorNumeroPredial(p.getNumeroPredial());
        modelMap.addAttribute("predio", predioConDatos);
        return null;
    }

    /**
     * Esta versión retorna datos como json o xml
     *
     * @param id
     * @param modelMap
     * @return
     */
    //@RequestMapping(value="/hello", headers = { "Accept=application/xml, text/xml" })
    @RequestMapping(value = "/hello/{id}")
    //@ResponseBody
    public String hello(@PathVariable Long id, ModelMap modelMap) {
        String message = Math.round(Math.random() * 1000) + "";
        LOGGER.debug("hello:" + message);
        UsuarioDTO dto = new UsuarioDTO();
        dto.setLogin(message);
        dto.setIdentificacion("identificación" + id.toString());
        dto.setPrimerApellido("primer apellido prueba");
        dto.setPrimerNombre("primer nombre test");

        modelMap.addAttribute("usuario", dto);
        //return dto;
        return null;//"predio/list";
    }

    /*
     * @RequestMapping(value = "/hello3/{id}") public ModelAndView hello3(@PathVariable Long id) {
     * String message = Math.round( Math.random()*1000)+""; LOGGER.debug("hello3:"+message);
     * UsuarioDTO dto = new UsuarioDTO(); dto.setId(message);
     * dto.setIdentificacion("identificación"+id.toString()); dto.setPrimerApellido("primer apellido
     * prueba"); dto.setPrimerNombre("primer nombre test");
     *
     * ModelAndView mav = new ModelAndView("predioXmlView", "usuario", dto); return mav; }
     */
}
