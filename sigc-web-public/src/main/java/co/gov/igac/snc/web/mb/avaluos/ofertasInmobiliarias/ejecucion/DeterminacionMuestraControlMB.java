/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.ejecucion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadRecolector;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EValoresCalculoMuestral;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-08 Determinar muestras de ofertas para control de calidad
 *
 * @author ariel.ortiz
 * @cu CU-TV-0F-08
 * @version 2.0
 *
 */
@Component("determinacionMuestraControl")
@Scope("session")
public class DeterminacionMuestraControlMB extends SNCManagedBean {

    /**
     * Serial autogenerado
     */
    private static final long serialVersionUID = -3793037443016224302L;
    /**
     * Logger para el Debug
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionAreaAsignadaMB.class);

    // --------Atributos--------------------------------------------------------------
    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * Variable necesaria para realizar el calculo muestral Es insertada manualmente por el usuario
     * en el campo de texto d del formulario No hay mas informacion descriptiva de ella en el caso
     * de uso CU-TV-OF-008
	 * */
    private int variableCalculoD;

    /**
     * variables que vienen del process y que se muestran en la interfaz por defecto.
     */
    private Departamento departamentoSeleccionado;
    private String departamentoSeleccionadoCod;

    private Municipio municipioSeleccionado;
    private String municipioSeleccionadoCod;

    private String nombreRecolector;

    private int numeroOfertasCapturadas;
    private int numeroOfertasAExportar;

    private boolean banderaMuestraCalculada;
    /**
     * id del control de calidad retornado por el procedimiento almacenado de calculo
     */
    private Long controlCalidadId;

    /**
     * Control calidad asociado a la muestra
     */
    private co.gov.igac.snc.persistence.entity.avaluos.ControlCalidad controlCalidad;

    /**
     * Variable de seleccion usada para realizar el calculo muestral Se llama
     * variableControlMuestras por que el nombre que le da el caso de uso CU-TV-OF-008 es "variable"
     * Puede tomar uno de cuatro valores: Valor pedido, área de terreno, área construida, valor
     * calculado.
     */
    private String variableControlMuestras;

    private List<ControlCalidadRecolector> controlCalidadRecolector;

    /**
     * Atributo que almacena la seleccion del SelectOneMenu Se llama de esa manera debido a que la
     * variable seleccionada se llama "variable" como esta especificado en el caso de uso
     * CU-TV-OF-008
     */
    private String seleccionVariableControl;
    /**
     * Lista de variables a mostrar en el selectOneMenu. El caso de uso llama a estas variables como
     * Variables.
     */
    private List<String> listaVariables;
    private UsuarioDTO usuario;
    private Long controlCalidadIdBorrado;

    // ------------------ procesos
    @Autowired
    protected TareasPendientesMB tareasPendientesMB;

    // --------Getters y Setters------------------------------------
    public String getMunicipioSeleccionadoCodigo() {
        return municipioSeleccionadoCod;
    }

    public void setMunicipioSeleccionadoCodigo(String municipioSeleccionadoCodigo) {
        this.municipioSeleccionadoCod = municipioSeleccionadoCodigo;
    }

    public Long getControlCalidadIdBorrado() {
        return controlCalidadIdBorrado;
    }

    public void setControlCalidadIdBorrado(Long controlCalidadIdBorrado) {
        this.controlCalidadIdBorrado = controlCalidadIdBorrado;
    }

    public int getVariableCalculoD() {
        return variableCalculoD;
    }

    public void setVariableCalculoD(int variableCalculoD) {
        this.variableCalculoD = variableCalculoD;
    }

    public List<ControlCalidadRecolector> getControlCalidadRecolector() {
        return controlCalidadRecolector;
    }

    public void setControlCalidadRecolector(List<ControlCalidadRecolector> controlCalidadRecolector) {
        this.controlCalidadRecolector = controlCalidadRecolector;
    }

    public Departamento getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    public void setDepartamentoSeleccionado(Departamento departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    public String getDepartamentoSeleccionadoCod() {
        return departamentoSeleccionadoCod;
    }

    public void setDepartamentoSeleccionadoCod(String departamentoSeleccionadoCod) {
        this.departamentoSeleccionadoCod = departamentoSeleccionadoCod;
    }

    public String getMunicipioSeleccionadoCod() {
        return municipioSeleccionadoCod;
    }

    public void setMunicipioSeleccionadoCod(String municipioSeleccionadoCod) {
        this.municipioSeleccionadoCod = municipioSeleccionadoCod;
    }

    public TareasPendientesMB getTareasPendientesMB() {
        return tareasPendientesMB;
    }

    public void setTareasPendientesMB(TareasPendientesMB tareasPendientesMB) {
        this.tareasPendientesMB = tareasPendientesMB;
    }

    public ArrayList<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public ArrayList<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public Municipio getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(Municipio municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public String getNombreRecolector() {
        return nombreRecolector;
    }

    public void setNombreRecolector(String nombreRecolector) {
        this.nombreRecolector = nombreRecolector;
    }

    public int getNumeroOfertasCapturadas() {
        return numeroOfertasCapturadas;
    }

    public void setNumeroOfertasCapturadas(int numeroOfertasCapturadas) {
        this.numeroOfertasCapturadas = numeroOfertasCapturadas;
    }

    public int getNumeroOfertasAExportar() {
        return numeroOfertasAExportar;
    }

    public void setNumeroOfertasAExportar(int numeroOfertasAExportar) {
        this.numeroOfertasAExportar = numeroOfertasAExportar;
    }

    public String getVariableControlMuestras() {
        return variableControlMuestras;
    }

    public void setVariableControlMuestras(String variableControlMuestras) {
        this.variableControlMuestras = variableControlMuestras;
    }

    public String getSeleccionVariableControl() {
        return seleccionVariableControl;
    }

    public void setSeleccionVariableControl(String seleccionVariableControl) {
        this.seleccionVariableControl = seleccionVariableControl;
    }

    public List<String> getListaVariables() {
        return listaVariables;
    }

    public void setListaVariables(List<String> listaVariables) {
        this.listaVariables = listaVariables;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    // --------Metodos-------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on determinacionMuestraControlMB init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.inicializarVariables();
        this.cargarDepartamentosItemList(this.usuario.getCodigoEstructuraOrganizacional());
        this.cargarMunicipiosItemList();

    }

    /**
     * Método que inicializa las variables al comienzo. Aquí también se encuentran los datos
     * quemados para pruebas.
     *
     * @author ariel.ortiz
     */
    public void inicializarVariables() {

        this.listaVariables = new ArrayList<String>();
        // Fin datos de prueba

        this.listaVariables.add(EValoresCalculoMuestral.VALOR_PEDIDO.toString());
        this.listaVariables.add(EValoresCalculoMuestral.AREA_TERRENO.toString());
        this.listaVariables.add(EValoresCalculoMuestral.AREA_CONSTRUIDA.toString());
        this.listaVariables.add(EValoresCalculoMuestral.VALOR_CALCULADO.toString());

        this.variableCalculoD = 0;
    }

    /**
     * Método que ejecuta los procedimientos almacenados para calcular y recalcular las muestras de
     * control de calidad
     *
     * @author ariel.ortiz
     *
     */
    public void calcularMuestrasOfertas() {

        Object[] controlCalidadAnswer = null;

        this.controlCalidadRecolector = null;
        this.controlCalidadId = null;

        // Se valida si es la primera vez que se realiza el calculo, o se trata
        // de un recalculo.
        if (this.banderaMuestraCalculada) {
            // Primero se borra el calculo anterior usando el id
            // Retornado por procedimiento de calculo
            this.getAvaluosService().borrarCalculoMuestras(this.controlCalidadIdBorrado);
        }

        // Calculo de la muestra
        controlCalidadAnswer = this.calcularMuestraControlDeCalidad();

        if (this.controlCalidadId != null) {

            this.banderaMuestraCalculada = true;

            this.controlCalidadRecolector = this.getAvaluosService()
                .buscarControlCalidadRecolectorPorIdControl(this.controlCalidadId);

            if (this.controlCalidadRecolector != null && !this.controlCalidadRecolector.isEmpty()) {
                this.addMensajeInfo("Se ha generado exitósamente la muestra");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError("No se tienen suficientes ofertas para calcular la muestra");
                context.addCallbackParam("error", "error");
            }

        } else {
            @SuppressWarnings("unchecked")
            List<Object> temporal = (List<Object>) controlCalidadAnswer[1];
            Object[] listaErrores = (Object[]) temporal.get(0);
            String error = listaErrores[3].toString();

            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo calcular la muestra: " + error);
            context.addCallbackParam("error", "error");

        }
    }

    /**
     * Encargado de llamar el proceso almacenado de crear la muestra dependiendo de los valores
     * seleccionados en las variables
     *
     * @author christian.rodriguez se crea ya que antes este código estaba duplicado en el método
     * calcularMuestrasOfertas
     * @return la muestra de control de calidad
     */
    private Object[] calcularMuestraControlDeCalidad() {
        Object[] controlCalidadAnswer;
        int valorPedidoD = 0, areaTerrenoD = 0, areaConstruidaD = 0, valorCalculadoD = 0;

        if (this.seleccionVariableControl.equals(EValoresCalculoMuestral.VALOR_PEDIDO.toString())) {
            valorPedidoD = this.variableCalculoD;
        } else if (this.seleccionVariableControl.equals(EValoresCalculoMuestral.AREA_TERRENO
            .toString())) {
            areaTerrenoD = this.variableCalculoD;
        } else if (this.seleccionVariableControl.equals(EValoresCalculoMuestral.AREA_CONSTRUIDA
            .toString())) {
            areaConstruidaD = this.variableCalculoD;
        } else if (this.seleccionVariableControl.equals(EValoresCalculoMuestral.VALOR_CALCULADO
            .toString())) {
            valorCalculadoD = this.variableCalculoD;
        }

        controlCalidadAnswer = this.getAvaluosService().calcularMuestras(
            this.municipioSeleccionadoCod, valorPedidoD, areaTerrenoD, areaConstruidaD,
            valorCalculadoD, this.usuario.getLogin());

        if (controlCalidadAnswer[0] != null) {
            BigDecimal bdTemporal = (BigDecimal) controlCalidadAnswer[0];
            this.controlCalidadId = bdTemporal.longValue();
            this.controlCalidadIdBorrado = this.controlCalidadId;
        }

        return controlCalidadAnswer;
    }

    /**
     * carga la lista de municipios según el departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void cargarDepartamentosItemList(String estucturaOrganizacionalCod) {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));

        if (estucturaOrganizacionalCod != null) {
            List<Departamento> departamentos = (ArrayList<Departamento>) this.getGeneralesService()
                .getCacheDepartamentosPorTerritorial(estucturaOrganizacionalCod);

            if (departamentos != null && !departamentos.isEmpty()) {

                for (Departamento departamento : departamentos) {

                    this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                        departamento.getNombre()));

                }
            }
        }
    }

    /**
     * carga la lista de municipios según el departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void cargarMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));

        if (this.departamentoSeleccionadoCod != null) {
            List<Municipio> municipios = (ArrayList<Municipio>) this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(this.departamentoSeleccionadoCod);

            if (municipios != null && !municipios.isEmpty()) {

                for (Municipio municipio : municipios) {

                    this.municipiosItemList.add(new SelectItem(municipio.getCodigo(), municipio
                        .getNombre()));

                }
            }
        }
    }

    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Actualiza las tablas de ofertas inmobiliarias correspondientes con las ofertas seleccionadas
     * para muestra control
     *
     * @author ariel.ortiz
     *
     */
    public void guardarMuestrasOfertas() {

        this.obtenerDeptoMuniSeleccionados();

        if (this.controlCalidadRecolector != null && !this.controlCalidadRecolector.isEmpty()) {

            if (this.forwardProcess()) {
                this.addMensajeInfo("Muestra guardada existosamente");
            }
        } else {

            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("Debe calcular la muestra de control de calidad antes de guardar");
            context.addCallbackParam("error", "error");

        }
    }

    /**
     * Método que trae el departamento y municipio seleccionados
     *
     * @author christian.rodriguez
     */
    private void obtenerDeptoMuniSeleccionados() {
        if (this.departamentoSeleccionadoCod != null) {
            this.departamentoSeleccionado = this.getGeneralesService()
                .getCacheDepartamentoByCodigo(this.departamentoSeleccionadoCod);
        }
        if (this.municipioSeleccionadoCod != null) {
            this.municipioSeleccionado = this.getGeneralesService().getCacheMunicipioByCodigo(
                this.municipioSeleccionadoCod);
        }
    }

    // ---------------------------- PROCESS ------------------------------- //
    /**
     * @author christian.rodriguez
     * @return false si no hubo ningún error avanzando las actividades
     */
    private boolean forwardProcess() {

        LOGGER.debug("Creando proceso Control Calidad Ofertas");

        this.doDatabaseStatesUpdate();

        List<Long> regionesAsociadas = this.getAvaluosService()
            .obtenerIdsRegionesAsociadasAControlCalidad(this.controlCalidadId);

        for (Long idRegion : regionesAsociadas) {
            ControlCalidad controlCalidadRegionBPM = this.crearObjectoDeNegocio(idRegion,
                String.valueOf(idRegion),
                ProcesoDeControlCalidad.ACT_VALIDACION_REALIZAR_CONTROL_CALIDAD_MUESTRAS,
                ProcesoDeControlCalidad.OPERACION_CREAR_MUESTRAS);

            if (controlCalidadRegionBPM != null) {

                this.getProcesosService().crearProceso(controlCalidadRegionBPM);

                this.actualizarEstadoRegionCaptura(idRegion);

            }
        }

        return true;
    }

    /**
     * Actualiza el estado de la región de captura de ofertas, le coloca el estado de control de
     * calidad
     *
     * @author christian.rodriguez
     * @param idRegion id de la región a actualizar
     */
    private void actualizarEstadoRegionCaptura(Long idRegion) {

        RegionCapturaOferta region = this.getAvaluosService()
            .buscarRegionCapturaOfertaPorId(idRegion);
        region.setEstado(EEstadoRegionCapturaOferta.CONTROL_CALIDAD.getEstado());
        this.getAvaluosService().guardarActualizarRegionCapturaOferta(region);

    }

    /**
     * Actualiza los objetos involucrados en el proceso de negocio. Guarda las ofertas de la muestra
     * para control de calidad
     *
     * @author christian.rodriguez
     */
    private void doDatabaseStatesUpdate() {

        this.getAvaluosService().guardarOfertasMuestraControl(this.controlCalidadId);

    }

    /**
     * Crea el objeto de negocio que se va a mvoer por el proceso de control de calidad
     *
     * @author christian.rodriguez
     * @param id Identificador de la actividad. Si es el padre es el id del control de calidad si es
     * algun hijo es el Id de la región)
     * @param idCorrelacion si es un proceso hijo debe ser el Id del control de calidad, si no,
     * puede ser 0
     * @param idRegion si es un hijo debe ser el id de la región de ofertas
     * @param transicion transicion destino de la actividad
     * @param operacion
     * @return objetod de negocio con todos los valores necesarios apra crear el proceso
     */
    private ControlCalidad crearObjectoDeNegocio(Long id, String idRegion,
        String transicion, String operacion) {

        // Información de la zona
        DivisionAdministrativa departamentoBPM, municipioBPM;
        departamentoBPM = new DivisionAdministrativa(this.departamentoSeleccionado.getCodigo(),
            this.departamentoSeleccionado.getNombre());
        municipioBPM = new DivisionAdministrativa(this.municipioSeleccionado.getCodigo(),
            this.municipioSeleccionado.getNombre());

        // Información del usuario al cual se le va a asignar la actividad
        UsuarioDTO investigadorMercados;
        investigadorMercados = this.getGeneralesService().getCacheUsuario("alejandro.sanchez");
        investigadorMercados = this.buscarInvestigadorMercadoTerritorial(
            this.municipioSeleccionado.getCodigo(),
            this.usuario.getCodigoEstructuraOrganizacional());

        if (investigadorMercados == null) {
            String mensaje = "No se encontró usuario de destino para el proceso.";
            this.addMensajeError(mensaje);
            return null;
        }

        List<UsuarioDTO> listaUsuario = new ArrayList<UsuarioDTO>();
        listaUsuario.add(investigadorMercados);

        // Creación del objeto de negocio
        ControlCalidad controlCalidadBPM = new ControlCalidad();
        controlCalidadBPM.setIdentificador(id);
        controlCalidadBPM.setIdCorrelacion(0);
        controlCalidadBPM.setIdRegion(idRegion);
        controlCalidadBPM.setDepartamento(departamentoBPM);
        controlCalidadBPM.setMunicipio(municipioBPM);
        controlCalidadBPM
            .setObservaciones("Proceso de Control de Calidad para ofertas");
        controlCalidadBPM.setTransicion(transicion);
        controlCalidadBPM.setUsuarios(listaUsuario);
        controlCalidadBPM.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        controlCalidadBPM.setFuncionario(this.usuario.getNombreCompleto());
        controlCalidadBPM.setOperacion(operacion);

        return controlCalidadBPM;
    }

    /**
     * Determina el investigador de mercados de la territorial
     *
     * @author christian.rodriguez
     * @return UsuarioDTO investigador de mercados de la territorial
     */
    private UsuarioDTO buscarInvestigadorMercadoTerritorial(String codigoMunicipio,
        String codigoTerritorial) {

        boolean esMunicipioDescentralizado = this.getGeneralesService()
            .municipioPerteneceACatastroDescentralizado(codigoMunicipio);

        ERol rol;
        if (esMunicipioDescentralizado) {
            rol = ERol.COORDINADOR_GIT_AVALUOS;
        } else {
            rol = ERol.INVESTIGADOR_MERCADO;
        }

        List<UsuarioDTO> usuarios = (ArrayList<UsuarioDTO>) this.getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(codigoTerritorial, rol);

        if (usuarios != null && !usuarios.isEmpty()) {
            return usuarios.get(0);
        } else {
            return null;
        }

    }
}
