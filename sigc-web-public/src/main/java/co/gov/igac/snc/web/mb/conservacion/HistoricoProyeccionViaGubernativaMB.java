/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.DocumentoResultadoPPRedios;
import co.gov.igac.snc.util.EIdTabRevisionProyeccion;
import co.gov.igac.snc.web.mb.conservacion.validacion.InformacionCancelacionInscripcionMB;
import co.gov.igac.snc.web.util.HistoricoProyeccionViaGubernativaDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author juanfelipe.garcia
 */
@SuppressWarnings("deprecation")
@Component("histProyViaGub")
@Scope("session")
public class HistoricoProyeccionViaGubernativaMB extends ProcessFlowManager {

    private static final long serialVersionUID = -9064304637726891692L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EjecucionMB.class);

    // --------------------------------- Servicios ----------------------------
    @Autowired
    private InformacionCancelacionInscripcionMB informacionCancelacionInscripcionMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Objeto que contiene los datos del reporte documento resolución
     */
    private ReporteDTO reporteResolucion;

    /**
     * Variable {@link String} que almacena las razones de rechazo depuración.
     */
    private String razonesRechazoDepuracion;

    /**
     * Bandera para habilitar las razones de rechazo depuración
     */
    private boolean habilitarRazonesRechazoDepuracion;

    /**
     * Bandera para habilitar botones Proyectar y Auto de prueba; para vía gubernativa se valida que
     * el predio asociado al recurso de vía gubernativa tenga datos de proyección
     */
    private boolean validacionViaGubernativa;

    /**
     * Documento resultado y lista de PPredios asociados a la proyección del trámite
     */
    private DocumentoResultadoPPRedios drp;

    /**
     * Motivo del rechazo del trámite
     */
    private String motivoRechazo;

    /**
     * Observaciónes para devolver el trámite al ejecutor
     */
    private TramiteEstado observacionesDeDevolucionDeTramite;

    /**
     * Lista con los datos resumen de la cancelación o inscripción
     */
    private List<String[]> tablaResumenTramite;

    /**
     * Trámite
     */
    private Tramite tramiteInicialVG;

    private Tramite selectedTramiteResolucion;

    private List<HistoricoProyeccionViaGubernativaDTO> listaTramitesHistoricoProyeccion;

    /**
     * Lista que contiene la información del histórico de obsevaciones del trámite
     */
    private List<TramiteEstado> tablaHistoricoObservaciones;

    /**
     * Lista que contiene la información de la documentacion requerida
     */
    private List<TramiteDocumentacion> documentacionRequerida;

    /**
     * Lista que contiene la información de la documentacion adicional
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * Lista que contiene la información de la documentacion generada por el sistema
     */
    private List<Documento> documentacionGenerada;

    public String getRazonesRechazoDepuracion() {
        return razonesRechazoDepuracion;
    }

    public boolean isHabilitarRazonesRechazoDepuracion() {
        return habilitarRazonesRechazoDepuracion;
    }

    public boolean isValidacionViaGubernativa() {
        return validacionViaGubernativa;
    }

    public void setValidacionViaGubernativa(boolean validacionViaGubernativa) {
        this.validacionViaGubernativa = validacionViaGubernativa;
    }

    public DocumentoResultadoPPRedios getDrp() {
        return drp;
    }

    public void setDrp(DocumentoResultadoPPRedios drp) {
        this.drp = drp;
    }

    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    public TramiteEstado getObservacionesDeDevolucionDeTramite() {
        return observacionesDeDevolucionDeTramite;
    }

    public void setObservacionesDeDevolucionDeTramite(
        TramiteEstado observacionesDeDevolucionDeTramite) {
        this.observacionesDeDevolucionDeTramite = observacionesDeDevolucionDeTramite;
    }

    public List<String[]> getTablaResumenTramite() {
        return tablaResumenTramite;
    }

    public void setTablaResumenTramite(List<String[]> tablaResumenTramite) {
        this.tablaResumenTramite = tablaResumenTramite;
    }

    public List<HistoricoProyeccionViaGubernativaDTO> getListaTramitesHistoricoProyeccion() {
        return listaTramitesHistoricoProyeccion;
    }

    public List<TramiteEstado> getTablaHistoricoObservaciones() {
        return tablaHistoricoObservaciones;
    }

    public Tramite getTramiteInicialVG() {
        return tramiteInicialVG;
    }

    public ReporteDTO getReporteResolucion() {
        return reporteResolucion;
    }

    public void obtenerReporteResolucion() {
        this.reporteResolucion = this.reportsService.consultarReporteDeGestorDocumental(
            this.selectedTramiteResolucion.getResultadoDocumento().getIdRepositorioDocumentos());
    }

    public Tramite getSelectedTramiteResolucion() {
        return selectedTramiteResolucion;
    }

    public void setSelectedTramiteResolucion(Tramite selectedTramiteResolucion) {
        this.selectedTramiteResolucion = selectedTramiteResolucion;
    }

    public List<TramiteDocumentacion> getDocumentacionRequerida() {
        return documentacionRequerida;
    }

    public void setDocumentacionRequerida(
        List<TramiteDocumentacion> documentacionRequerida) {
        this.documentacionRequerida = documentacionRequerida;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public List<Documento> getDocumentacionGenerada() {
        return documentacionGenerada;
    }

    public void setDocumentacionGenerada(List<Documento> documentacionGenerada) {
        this.documentacionGenerada = documentacionGenerada;
    }

    // --------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init de HistoricoProyeccionViaGubernativaMB");

    }

    public void cambioTab(TabChangeEvent tce) {

        if (tce != null) {
            if (tce.getTab()
                .getId()
                .equals(EIdTabRevisionProyeccion.INFORMACION_CANCELACION_INSCRIPCION
                    .getId())) {
                this.informacionCancelacionInscripcionMB
                    .setTramiteSeleccionado(this.tramiteInicialVG);
                this.informacionCancelacionInscripcionMB.init();
            }
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método para inicializar los datos de la tabla de resumen de trámite
     */
    /**
     * @documented pedro.garcia
     * @modified pedro.garcia 02-08-2012 Se aplica esta nueva regla para el cálculo de los datos de
     * la tabla:
     *
     * OJO: esto aplica para los trámites que no son de segunda
     *
     * para propietario y predios cancelación ->(se tiene en cuenta) C, M, null inscripción ->(se
     * tiene en cuenta) I, M, null
     *
     * para área terreno, área construcción, avalúo cancelación ->(se revisa) Predio inscripción
     * ->(se revisa) PPredio
     */
    public void cargarDatosTablaResumenTramite() {

        this.drp = this.getGeneralesService()
            .buscarDocumentoResultadoPorTramiteId(this.tramiteInicialVG.getId());

        this.tramiteInicialVG = this.getTramiteService()
            .findTramitePruebasByTramiteId(this.tramiteInicialVG.getId());

        this.tablaHistoricoObservaciones = this.drp.getHistoricoObservaciones();

        if (this.drp.getHistoricoObservaciones() != null &&
             !this.drp.getHistoricoObservaciones().isEmpty() &&
             this.drp.getHistoricoObservaciones().get(0).getMotivo() != null &&
             this.drp.getHistoricoObservaciones().get(0).getMotivo()
                .contains(Constantes.PALABRA_CLAVE_TMP_HISTORICO_OBS)) {
            // TODO: fredy.wilches :: 29/05/2012 :: Condicion adicionada por
            // FGWL pero debe revisarse si es necesaria :: gabriel.agudelo

            this.motivoRechazo = this.drp
                .getHistoricoObservaciones()
                .get(0)
                .getMotivo()
                .replace(Constantes.PALABRA_CLAVE_TMP_HISTORICO_OBS.toString(), "");

            this.drp.getHistoricoObservaciones()
                .get(0)
                .setMotivo(
                    this.drp.getHistoricoObservaciones()
                        .get(this.drp.getHistoricoObservaciones()
                            .size() - 1)
                        .getMotivo()
                        .replace(
                            Constantes.PALABRA_CLAVE_TMP_HISTORICO_OBS
                                .toString(), ""));

            this.observacionesDeDevolucionDeTramite = this.drp
                .getHistoricoObservaciones().get(0);

        }

        this.tablaResumenTramite = new ArrayList<String[]>();

        String[] cancelaciones = new String[6];
        String[] inscripciones = new String[6];

        cancelaciones[0] = "Cancelaciones";
        inscripciones[0] = "Inscripciones";

        int cantidadPrediosCancelados = 0;
        int cantidadPrediosInscritos = 0;

        double areaTerrenoPCancelados = 0.0;
        double areaTerrenoPInscritos = 0.0;

        double areaConstruccionPCancelados = 0.0;
        double areaConstruccionPInscritos = 0.0;

        double avaluoPCancelados = 0.0;
        double avaluoPInscritos = 0.0;

        int propietariosCancelados = 0;
        int propietariosInscritos = 0;

        Predio tempPredio = null;

        if (this.drp.getPprediosTramite() != null && !this.drp.getPprediosTramite().isEmpty()) {

            if (!this.tramiteInicialVG.isSegunda()) {
                for (PPredio ppredio : this.drp.getPprediosTramite()) {

                    //D: obtención datos número propietarios
                    for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {

                        if (null == pppTemp.getCancelaInscribe()) {
                            propietariosInscritos++;
                            propietariosCancelados++;
                        } else {
                            if (pppTemp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                propietariosCancelados++;
                            } else if (pppTemp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                                propietariosInscritos++;
                            } else if (pppTemp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                propietariosInscritos++;
                                propietariosCancelados++;
                            }
                        }
                    }

                    //D: obtención datos número predios
                    if (null == ppredio.getCancelaInscribe()) {
                        cantidadPrediosCancelados++;
                        cantidadPrediosInscritos++;
                    } else {
                        if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                            cantidadPrediosCancelados++;
                        } else if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                            cantidadPrediosInscritos++;
                        } else if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                            cantidadPrediosCancelados++;
                            cantidadPrediosInscritos++;
                        }
                    }

                    //D: obtención datos área terreno, área construcción, autoavalúo
                    //D: para los 'C' se toman los datos del Predio.
                    //N: esto sirve porque para estos trámites el id del PPredio es el mismo del Predio
                    tempPredio = this.getConservacionService().obtenerPredioPorId(ppredio.getId());
                    if (tempPredio != null) {
                        areaTerrenoPCancelados =
                            areaTerrenoPCancelados + tempPredio.getAreaTerreno();
                        areaConstruccionPCancelados =
                            areaConstruccionPCancelados + tempPredio.getAreaConstruccion();
                        if (tempPredio.getAvaluoCatastral() != null) {
                            avaluoPCancelados = avaluoPCancelados + tempPredio.getAvaluoCatastral();
                        }
                    }

                    //D: para los 'I' se toman los datos del PPredio
                    areaTerrenoPInscritos = areaTerrenoPInscritos + ppredio.getAreaTerreno();
                    if (ppredio.getCancelaInscribe() == null ||
                        (ppredio.getCancelaInscribe() != null &&
                        (ppredio.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.
                            getCodigo()) ||
                         ppredio.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.
                            getCodigo())))) {
                        areaConstruccionPInscritos =
                            areaConstruccionPInscritos + ppredio.getAreaConstruccion();
                    }
//TODO :: fredy.wilches :: verificar si es correcto que un pppredio en un trámite de quinta (u otro tipo de trámite)n tenga este campo en null
                    avaluoPInscritos = avaluoPInscritos +
                        (ppredio.getAvaluoCatastral() == null ? 0 : ppredio.getAvaluoCatastral());

                }
            } //D: para los trámites de englobe o desenglobe aplican otras reglas para obtener los
            //   datos de la tabla resumen de proyección.
            else if (this.tramiteInicialVG.isSegunda()) {

                //D: Reglas para calcular los datos para desenglobe.
                // Cancelación:
                // La cantidad de predios cancelados siempre es 1.
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'C' o 'M'
                // El área de terreno, área de construcción y avalúo corresponden al dato del predio original
                //
                // Inscripción:
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'I' o 'M'
                // El número de predios es la SUM de los PPredios marcados con 'I' o 'M'
                // El área de terreno, área de construcción y avalúo se obtienen de la SUM de esos
                // datos de los PPredio marcados con 'I' o 'M'
                if (this.tramiteInicialVG.isSegundaDesenglobe()) {

                    //D: en desenglobe, el campro 'predio' de la tabla 'Tramite' guarda el predio inicial
                    tempPredio = this.tramiteInicialVG.getPredio();
                    cantidadPrediosCancelados = 1;

                    if (tempPredio != null) {

                        areaTerrenoPCancelados = tempPredio.getAreaTerreno();
                        areaConstruccionPCancelados = tempPredio.getAreaConstruccion();
                        avaluoPCancelados = tempPredio.getAvaluoCatastral();

                        for (PPredio ppredio : this.drp.getPprediosTramite()) {

                            if (ppredio.getCancelaInscribe() == null ||
                                (ppredio.getCancelaInscribe() != null && (ppredio.
                                getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                ppredio.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.MODIFICA.getCodigo())))) {

                                if (!ppredio.isEsPredioFichaMatriz()) {
                                    cantidadPrediosInscritos++;
                                }
                                areaTerrenoPInscritos += ppredio.getAreaTerreno();
                                areaConstruccionPInscritos += ppredio.getAreaConstruccion();
//TODO :: fredy.wilches :: definir qué pasa cuando este campo llega en null :: pedro.garcia
                                if (ppredio.getAvaluoCatastral() != null) {
                                    avaluoPInscritos += ppredio.getAvaluoCatastral();
                                }
                            }

                            /* El conteo de los propietarios cancelados se debe hacer en el predio
                             * principal.
                             */
                            if (tempPredio.getId().longValue() == ppredio.getId().longValue()) {

                                if (ppredio.getCancelaInscribe() != null && ppredio.
                                    getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                    for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {
                                        if (null == pppTemp.getCancelaInscribe() || pppTemp.
                                            getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                            propietariosCancelados++;
                                        } else if (pppTemp.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                            propietariosCancelados++;
                                        }
                                    }
                                } else if (ppredio.getCancelaInscribe() == null || ppredio.
                                    getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                    ppredio.getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                    for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {
                                        if (null == pppTemp.getCancelaInscribe() || pppTemp.
                                            getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                            propietariosCancelados++;
                                            propietariosInscritos++;
                                        } else if (pppTemp.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                            propietariosCancelados++;
                                        } else if (pppTemp.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                                            propietariosInscritos++;
                                        }
                                    }
                                }
                            } else {
                                for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {

                                    if (ppredio.getCancelaInscribe() == null || ppredio.
                                        getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                        ppredio.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

                                        if (null == pppTemp.getCancelaInscribe() || pppTemp.
                                            getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                                            pppTemp.getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                                            propietariosInscritos++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } //D: Reglas para calcular los datos para englobe.
                // Cancelación:
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'C'
                // La cantidad de predios cancelados es la SUM de PPredios marcados con 'C' más -si
                // lo hay- el PPredio marcado con 'M' (hay 0 o 1. 1 si se mantiene el número predial)
                // El área de terreno, área de construcción y avalúo se obtienen de la SUM de esos
                // datos de los Predio originales
                //
                // Inscripción:
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'I' o 'M'
                // El número de predios inscritos es siempre 1
                // El área de terreno, área de construcción y avalúo se obtienen del PPredio (que
                // puede estar marcado con 'I' o 'M')
                else if (this.tramiteInicialVG.isSegundaEnglobe()) {

                    cantidadPrediosInscritos = 1;

                    for (PPredio ppredio : this.drp.getPprediosTramite()) {

                        if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.CANCELA.getCodigo()) ||
                            ppredio.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

                            cantidadPrediosCancelados++;
                        }

                        //D: debería se solo un PPredio y estar con 'M' o 'I'
                        if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                            ppredio.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

                            areaTerrenoPInscritos += ppredio.getAreaTerreno();
                            areaConstruccionPInscritos += ppredio.getAreaConstruccion();
                            avaluoPInscritos += ppredio.getAvaluoCatastral();
                        }

                        for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {

//TODO :: fredy.wilches :: 21-08-2012 :: por ahora él definió que se sumaran a los cancelados, pero es error que ese campo esté en null :: pedro.garcia
                            if (null == pppTemp.getCancelaInscribe()) {
                                propietariosCancelados++;
                                LOGGER.error(
                                    "El campo 'cancelaInscribe del PPersonaPredio con id " +
                                     pppTemp.getId() + " está en null");
                            } else {
                                if (pppTemp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                    propietariosCancelados++;
                                } else if (pppTemp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                    pppTemp.getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                    propietariosInscritos++;
                                }
                            }
                        }
                    }

                    //D: se obtienen los Predios relacionados con el trámite. Estos están en la tabla
                    //  Tramite_Detalle_Predio
                    ArrayList<TramiteDetallePredio> prediosOriginalesTPE;
                    prediosOriginalesTPE = (ArrayList<TramiteDetallePredio>) this.
                        getTramiteService().buscarPredioEnglobesPorTramite(
                            this.getTramiteInicialVG().getId());

                    if (prediosOriginalesTPE != null) {
                        for (TramiteDetallePredio tdp : prediosOriginalesTPE) {
                            areaTerrenoPCancelados += tdp.getPredio().getAreaTerreno();
                            areaConstruccionPCancelados += tdp.getPredio().getAreaConstruccion();
                            avaluoPCancelados += tdp.getPredio().getAvaluoCatastral();
                        }
                    } else {
                        LOGGER.error("En desenglobe con id de trámite " +
                            this.getTramiteInicialVG().getId() +
                            " dio que los predios originales son null");
                    }
                }
            }
        }

        cancelaciones[1] = cantidadPrediosCancelados + "";
        cancelaciones[2] = areaTerrenoPCancelados + "";
        cancelaciones[3] = areaConstruccionPCancelados + "";

        NumberFormat nf = new DecimalFormat("#.##");
        cancelaciones[4] = nf.format(avaluoPCancelados) + "";
        cancelaciones[5] = propietariosCancelados + "";

        inscripciones[1] = cantidadPrediosInscritos + "";
        inscripciones[2] = areaTerrenoPInscritos + "";
        inscripciones[3] = areaConstruccionPInscritos + "";
        inscripciones[4] = nf.format(avaluoPInscritos) + "";
        inscripciones[5] = propietariosInscritos + "";

        //D: la fila de cancelaciones no se muestra cuando el trámite es de quinta omitido
        if (!this.tramiteInicialVG.isQuintaOmitido()) {
            this.tablaResumenTramite.add(cancelaciones);
        }

        //D: la fila de inscripciones se muestra si el trámite no es de cancelación de predio
        if (!this.tramiteInicialVG.isCancelacionPredio()) {
            this.tablaResumenTramite.add(inscripciones);
        }

    }

    // ----------------------------------------------------- //
    /**
     * Obtener los documentos asociados al tramite seleccionado
     *
     * @author juanfelipe.garcia
     */
    public void cargarDocumentos() {
        Tramite tempTramite;

        this.documentacionRequerida = new ArrayList<TramiteDocumentacion>();
        this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        this.documentacionGenerada = new ArrayList<Documento>();

        if (this.selectedTramiteResolucion != null) {
            //se consultan los documentos del trámite ()
            tempTramite = this.getTramiteService().consultarDocumentacionCompletaDeTramite(
                this.selectedTramiteResolucion.getId());
            if (tempTramite.getTramiteDocumentacions() != null &&
                 !tempTramite.getTramiteDocumentacions().isEmpty()) {

                for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                    if (td.getRequerido() != null &&
                         td.getRequerido().equals(ESiNo.SI.toString())) {
                        this.documentacionRequerida.add(td);
                    }
                }

                for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                    if (td.getAdicional() != null &&
                         td.getAdicional().equals(ESiNo.SI.toString()) &&
                        
                        (td.getRequerido() == null || td.getRequerido().equals(ESiNo.NO.toString()))) {
                        this.documentacionAdicional.add(td);
                    }
                }

            }

            //ETipoDocumento.AUTO_DE_PRUEBAS_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId()
            //ETipoDocumento.AUTO_DE_PRUEBAS_REVISION_AVALUO_AUTOESTIMACION_Y_REPOSICION.getId()
            if (tempTramite.getTramitePruebas() != null && tempTramite.getTramitePruebas().
                getAutoPruebasDocumento() != null) {
                documentacionGenerada.add(tempTramite.getTramitePruebas().getAutoPruebasDocumento());
            }
            List<ComisionTramite> comisionTramiteLista = this.getTramiteService().
                buscarComisionesTramitePorIdTramite(this.selectedTramiteResolucion.getId());

            boolean tieneDocCom = false;
            for (ComisionTramite ct : comisionTramiteLista) {
                //ETipoDocumento.MEMORANDO_DE_APROBACION_DE_COMISION.getId()
                if (ct.getComision().getMemorandoDocumentoId() != null) {
                    Long id = ct.getComision().getMemorandoDocumentoId();
                    Documento doc = this.getTramiteService().buscarDocumentoPorId(id);
                    documentacionGenerada.add(doc);
                    tieneDocCom = true;
                }
                List<ComisionEstado> comisionEstadoList = this.getTramiteService().
                    consultarHistoricoCambiosEstadoComision(ct.getComision().getId());
                //ETipoDocumento.APROBAR_SOLICITUD_DE_COMISION.getId()
                //ETipoDocumento.RECHAZO_DE_COMISION.getId()
                if (comisionEstadoList != null) {
                    for (ComisionEstado ce : comisionEstadoList) {
                        if (ce.getMemorandoDocumento() != null) {
                            if (tieneDocCom && !ce.getMemorandoDocumento().getId().equals(ct.
                                getComision().getMemorandoDocumentoId())) {
                                Documento doc = this.getTramiteService().buscarDocumentoPorId(ce.
                                    getMemorandoDocumento().getId());
                                documentacionGenerada.add(doc);
                            } else if (!tieneDocCom) {
                                Documento doc = this.getTramiteService().buscarDocumentoPorId(ce.
                                    getMemorandoDocumento().getId());
                                documentacionGenerada.add(doc);
                            }

                        }
                    }
                }
            }

            //ETipoDocumento.CONSTANCIA_DE_RADICACION
            List<Documento> docsSolicitudTramites = this.getTramiteService()
                .buscarDocumentosPorSolicitudId(tempTramite.getSolicitud().getId());
            documentacionGenerada.addAll(docsSolicitudTramites);

            List<TramiteDocumento> listaDocumentos = this.getTramiteService().
                obtenerTramiteDocumentosPorTramiteId(this.selectedTramiteResolucion.getId());

            if (listaDocumentos != null && !listaDocumentos.isEmpty()) {
                for (TramiteDocumento td : listaDocumentos) {
                    if (td.getDocumento().getTipoDocumento().getId().equals(
                        ETipoDocumento.CONSTANCIA_DE_RADICACION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_NO_PROCEDENCIA_AUTOESTIMACION_Y_REVISION_DE_AVALUO.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_NO_PROCEDENCIA_MUTACIONES.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_NO_PROCEDENCIA_VIA_GUBERNATIVA.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_PARA_RETIRAR_TRAMITE_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.INFORME_VISITA_REVISION_O_AUTOAVALUO.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.NOTIFICACION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.EDICTO_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.EDICTO_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_Y_REPOSICION.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_SUSPENSION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_APLAZAMIENTO_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_REACTIVACION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_AMPLIACION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_CANCELACION_DE_COMISION.getId())) {
                        documentacionGenerada.add(td.getDocumento());
                    }
                }
            }
            //se valida que no tenga documentos repetidos
            HashSet hs = new HashSet();
            hs.addAll(documentacionGenerada);
            documentacionGenerada.clear();
            documentacionGenerada.addAll(hs);
        }
    }

    public void radicar() {

    }

    public String cerrar() {
        return "";
    }

    @Override
    public boolean validateProcess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setupProcessMessage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doDatabaseStatesUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTramiteInicialVG(Tramite tramiteInicialVG) {
        this.tramiteInicialVG = tramiteInicialVG;
    }

}
