package co.gov.igac.snc.web.mb.tests;

import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pagm
 */
@Component("validator")
@Scope("session")
public class ValidatorMB implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidatorMB.class);
    /**
     *
     */
    private static final long serialVersionUID = -8206543064330825192L;

    private String texto;

    private String propertyActionListener;

    private String filaSeleccionada;
    private ArrayList tableValues;

    //-------------   methods   ----------------------------
    public String getPropertyActionListener() {
        return propertyActionListener;
    }

    public void setPropertyActionListener(String propertyActionListener) {
        this.propertyActionListener = propertyActionListener;
    }

    public ValidatorMB() {

    }

    @PostConstruct
    public void init() {
        this.tableValues = new ArrayList();
        this.tableValues.add("ramona");
        this.tableValues.add("lola");
        this.tableValues.add("florinda");

    }

    public void foo() {
        LOGGER.debug("on foo ... doing nothing");
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public void listener1() {
        LOGGER.debug("on listener1 ...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            LOGGER.error("error sleeping: " + ex.getMessage());
        }
        LOGGER.debug("... exiting listener1");
    }
//--------------------------------------------------------------------------------------------------

    public void rowSelectListener(SelectEvent event) {
        LOGGER.debug("valor de parámetro que debió haber llegado como property action listener: " +
            this.propertyActionListener);
    }
//--------------------------------------------------------------------------------------------------

    public void goo(String paramFromWeb) {
        LOGGER.debug("parameter sent from an EL in the action of a button: " + paramFromWeb);
    }

}
