package co.gov.igac.snc.web.mb.tareas;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.EObjetoProceso;
import co.gov.igac.snc.apiprocesos.contenedores.Actividades;
import co.gov.igac.snc.apiprocesos.contenedores.ArbolProcesos;
import co.gov.igac.snc.apiprocesos.contenedores.Macroproceso;
import co.gov.igac.snc.apiprocesos.contenedores.Organizacion;
import co.gov.igac.snc.apiprocesos.contenedores.Proceso;
import co.gov.igac.snc.apiprocesos.contenedores.Subproceso;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.locator.SNCPropertiesUtil;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 *
 * @author andres.solopaila OJO: qué código tan paila!!!
 *
 *
 * @modified by pedro.garcia, alejandro.sanchez
 */
@Component("tareasPendientes")
@Scope("session")
public class TareasPendientesMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 761725509620580700L;
    private static final String DESPLIEGUE_CANTIDAD = "{0}({1})";
    private static final Logger LOGGER = LoggerFactory
        .getLogger(TareasPendientesMB.class);

    private ArrayList<NodoProcesosGraficos> nodosVistaMacroprocesos;
    private ArrayList<NodoProcesosGraficos> nodosVistaProcesos;
    private ArrayList<NodoProcesosGraficos> nodosVistaSubprocesos;
    private ArrayList<NodoProcesosGraficos> nodosVistaActividades;
    private ArrayList<NodoProcesosGraficos> nodosVistaInstanciasActividad;
    private Actividad instanciaSeleccionada;
    private String opcionActual;

    @Autowired
    private GeneralMB generalService;

    private TreeNode root;

    private TreeNode nodoSeleccionado;

    private NodoProcesosGraficos nodoSeleccionadoInfo;

    private Organizacion organizacion;

    private List<Macroproceso> macroprocesos;

    private List<Proceso> procesos;

    private List<Subproceso> subprocesos;

    /**
     * Contenedor de referencias a las actividades de todos los subprocesos.
     */
    private List<Actividades> actividadesSubprocesos;

    private List<Actividad> instanciasActividad;

    /**
     * lista de las instancias de actividades que se seleccionan al escoger un nodo de actividad del
     * árbol
     */
    private List<Actividad> listaInstanciasActividadesSeleccionadas;

    private Actividades actividadesSeleccionadas;

    private boolean esSubproceso;
    private String url;
    private String urlIndividual;

    private String tipoProceso;

    @Autowired(required = true)
    private HttpServletRequest request;

    /**
     * Listado de los URL a los que tiene permiso debido a las actividades pendientes
     */
    private List<String> actividadesConPermiso = new ArrayList<String>();

    /**
     * variable para identificar si se trata de la aplicación de ofertas. Se coloca replicada aquí
     * porque debe estar disponible para leela en listaTareas.xhtml
     */
    private boolean esOSMI;

    /**
     * Indica si el proceso corresponde al proceso de calidad de ofertas inmobiliarias.
     */
    private boolean esOSMICalidad;

    private String urlImagenProceso;

    private String urlPagina;

    private String derechoPeticionTutela;

//----------------     methods   --------------------------------
    public boolean isEsOSMI() {
        return this.esOSMI;
    }

    public boolean isEsOSMICalidad() {
        return this.esOSMICalidad;
    }

    public void setEsOSMI(boolean esOSMI) {
        this.esOSMI = esOSMI;
    }

    public void setEsOSMICalidad(boolean esOSMICalidad) {
        this.esOSMICalidad = esOSMICalidad;
    }

    public String getDerechoPeticionTutela() {
        return derechoPeticionTutela;
    }

    /*
     * metodo que verifica si en la lista de tareas pendientes existe asociado un derecho de
     * peticion o una tutela @author leidy.gonzalez
     */
    public void setDerechoPeticionTutela(String derechoPeticionTutela) {

        if (this.listaInstanciasActividadesSeleccionadas != null) {
            for (Actividad actividad : this.listaInstanciasActividadesSeleccionadas) {
                /* if (actividad.getObservaciones() != null && (actividad.getObservacion().contains(
                 * ESolicitudFormaPeticion.DERECHO_DE_PETICION .getValor()) || actividad
                 * .getObservacion().contains( ESolicitudFormaPeticion.TUTELA .getValor()))) {
                 * derechoPeticionTutela = actividad.getObservacion();
				} */
 /* if
                 * (actividad.getObservaciones().getObservaciones()
                 * .containsValue(ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor()) ||
                 * actividad.getObservaciones().getObservaciones()
                 * .containsValue(ESolicitudFormaPeticion.TUTELA.getValor())) {
                 *
                 * derechoPeticionTutela =
                 * actividad.getObservaciones().getObservaciones().toString();
				} */
            }
        }
        this.derechoPeticionTutela = derechoPeticionTutela;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @modified alejandro.sanchez
     *
     * Se modificó la consulta de árbol de procesos, ya no se obtienen las actividades en la misma
     * consulta. Al momento de hacer clic sobre el nodo de actividades del árbol, se consultan las
     * actividades de este tipo.
     */
    @PostConstruct
    public void init() {
        this.actividadesConPermiso = new ArrayList<String>();
        ArbolProcesos arbolActividades;
        UsuarioDTO usuario = MenuMB.getMenu().getUsuarioDto();

        if (usuario == null) {
            LOGGER.error("El usuario está llegando como null al init de TareasPendientesMB");
            return;
        }
        this.instanciaSeleccionada = null;

        //D: por ahora no se debe armar árbol cuando es para ofertas
        SNCPropertiesUtil props = SNCPropertiesUtil.getInstance();
        this.esOSMI = Boolean.valueOf(props.getProperty("osmi"));

        if (this.isEsOSMI()) {
            return;
        }

        try {
            // FGWL reiniciar session y mantener MenuMB, ....
            limpiarSesion();
            int procesoActual;
            int subprocesoActual;
            int macroprocesoActual;
            this.esSubproceso = false;
            this.listaInstanciasActividadesSeleccionadas = new ArrayList<Actividad>();
            this.nodoSeleccionadoInfo = new NodoProcesosGraficos(null, null);
            /*
             * Obtener el arbol por parte del servidor de procesos
             */
            root = new DefaultTreeNode("Árbol de procesos", null);

            // Construcción de árbol de procesos
            this.nodosVistaMacroprocesos = new ArrayList<NodoProcesosGraficos>();
            this.nodosVistaProcesos = new ArrayList<NodoProcesosGraficos>();
            this.nodosVistaSubprocesos = new ArrayList<NodoProcesosGraficos>();
            this.nodosVistaActividades = new ArrayList<NodoProcesosGraficos>();
            this.nodosVistaInstanciasActividad = new ArrayList<NodoProcesosGraficos>();
            LOGGER.info("Obteniendo lista de actividades...");
            arbolActividades = this.getProcesosService().consultarTareas(usuario);
            LOGGER.info("Lista de actividades obtenidas");
            this.organizacion = (Organizacion) arbolActividades.getRootElement();
            this.macroprocesos = organizacion.getMacroprocesos();
            TreeNode nodoGrafico;
            String nombreDespliegue;
            //Recorrido del árbol en profundidad para obtener sus nodos
            /////////////////////////////////////////////MACROPROCESOS///////////////////////////////////////////
            for (int macroProcesoIdx = 0; macroProcesoIdx < macroprocesos
                .size(); macroProcesoIdx++) {

                nombreDespliegue = MessageFormat.format(DESPLIEGUE_CANTIDAD,
                    this.macroprocesos.get(macroProcesoIdx).getData()
                        .getNombreDeDespliegue(),
                    this.macroprocesos.get(macroProcesoIdx)
                        .getNumeroDeActividades());
                nodoGrafico = new DefaultTreeNode(nombreDespliegue, root);
                nodoGrafico.setExpanded(true);
                this.nodosVistaMacroprocesos.add(new NodoProcesosGraficos(
                    nodoGrafico, this.macroprocesos.get(macroProcesoIdx)));
                macroprocesoActual = this.nodosVistaMacroprocesos.size() - 1;
                this.procesos = this.macroprocesos.get(macroProcesoIdx).getProcesos();
                /////////////////////////////////////////////PROCESOS///////////////////////////////////////////
                for (int procesoIdx = 0; procesoIdx < this.procesos.size(); procesoIdx++) {

                    nombreDespliegue = MessageFormat.format(
                        DESPLIEGUE_CANTIDAD, procesos.get(procesoIdx)
                            .getData().getNombreDeDespliegue(),
                        this.procesos.get(procesoIdx).getNumeroDeActividades());
                    nodoGrafico = new DefaultTreeNode(nombreDespliegue,
                        this.nodosVistaMacroprocesos.get(macroprocesoActual)
                            .getTreeNode());
                    nodoGrafico.setExpanded(true);
                    this.nodosVistaProcesos.add(new NodoProcesosGraficos(
                        nodoGrafico, this.procesos.get(procesoIdx)));
                    procesoActual = this.nodosVistaProcesos.size() - 1;
                    this.subprocesos = this.procesos.get(procesoIdx).getSubprocesos();

                    /////////////////////////////////////////////SUBPROCESOS///////////////////////////////////////////
                    for (int subprocesoIdx = 0; subprocesoIdx < subprocesos
                        .size(); subprocesoIdx++) {
                        nombreDespliegue = MessageFormat.format(
                            DESPLIEGUE_CANTIDAD,
                            this.subprocesos.get(subprocesoIdx).getData()
                                .getNombreDeDespliegue(), this.subprocesos
                                .get(subprocesoIdx)
                                .getNumeroDeActividades());

                        nodoGrafico = new DefaultTreeNode(nombreDespliegue,
                            this.nodosVistaProcesos.get(procesoActual)
                                .getTreeNode());
                        nodoGrafico.setExpanded(true);
                        this.nodosVistaSubprocesos.add(new NodoProcesosGraficos(
                            nodoGrafico, this.subprocesos.get(subprocesoIdx)));
                        if (subprocesoIdx == 0) {
                            this.actividadesSubprocesos = new ArrayList<Actividades>();
                        }
                        List<Actividades> contenedorActividades = this.subprocesos
                            .get(subprocesoIdx).getActividades();
                        // ///////////////////////////////////////////ACTIVIDADES///////////////////////////////////////////
                        subprocesoActual = this.nodosVistaSubprocesos.size() - 1;
                        for (int actividadIdx = 0; actividadIdx < contenedorActividades
                            .size(); actividadIdx++) {
                            //v1.1.11
                            //JAAB refs#7503
                            //Se realiza un filtro para que no incluya en el arbol de tareas, aquellas que están clasificadas como de MENU
                            if (!Constantes.ACTIVIDAD_PROCESO_MENU.equals(contenedorActividades.get(
                                actividadIdx).getUrlCasoDeUso())) {
                                this.actividadesConPermiso.add(contenedorActividades.get(
                                    actividadIdx).getUrlCasoDeUso());

                                nombreDespliegue = MessageFormat.format(
                                    DESPLIEGUE_CANTIDAD, contenedorActividades
                                        .get(actividadIdx).getData()
                                        .getNombreDeDespliegue(),
                                    contenedorActividades.get(actividadIdx)
                                        .getNumeroDeActividades());
                                nodoGrafico = new DefaultTreeNode(nombreDespliegue,
                                    this.nodosVistaSubprocesos.get(subprocesoActual)
                                        .getTreeNode());
                                nodoGrafico.setExpanded(true);
                                this.nodosVistaActividades.add(new NodoProcesosGraficos(
                                    nodoGrafico, contenedorActividades
                                        .get(actividadIdx)));
                            }
                        }
                        this.actividadesSubprocesos.addAll(contenedorActividades);
                    }
                }
            }
            this.root.setExpanded(true);
            /**Se inicializa la url:
             * por que el componente de arbol en algunos casos presenta perdida del evento listener, y hace la redireccion a una pagina sin inicializar los componentes
            en estos casos se evidencia un click sin evento, que actualiza el componente y reactiva el evento listener
            **/
            this.url = "";
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMessage(), e);
            this.generalService.setErrorProceso(true);
            this.generalService.setErrorAplicacion(false);
            arbolActividades = new ArbolProcesos(usuario.getLogin(), null);
            throw e;
        }
    }

    private void limpiarSesion() {
        HttpSession session = this.request.getSession(false);
        @SuppressWarnings("unchecked")
        Enumeration<String> objetos = session.getAttributeNames();
        while (objetos.hasMoreElements()) {
            String objeto = objetos.nextElement();
            LOGGER.debug("************ Eliminando " + objeto);
            if (!objeto.equals("menu") && !objeto.equals("tareasPendientes") &&
                 !objeto.equals("SPRING_SECURITY_CONTEXT") &&
                 !objeto.startsWith("org.springframework.web") &&
                 !objeto.startsWith("javax.faces.request") &&
                 !objeto.startsWith("com.sun.faces")) {
                session.setAttribute(objeto, null);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     *
     * @modified pedro.garcia el atributo listaInstanciasActividadesSeleccionadas se debe
     * inicializar no solo en el caso de que la actividad sea individual Cambio de nombre del método
     *
     *
     * @param event
     */
    public void seleccionNodoListener(NodeSelectEvent event) {

        LOGGER.debug("busquedaNodo");
        buscarNodoSeleccionadoConInformacion();
        this.url = "";
        if (this.nodoSeleccionadoInfo.getObjetoProceso().getData().getTipo()
            .toString().equals("Actividad")) {
            this.opcionActual = ((Actividades) this.nodoSeleccionadoInfo.getObjetoProceso()).
                getRutaActividad().toString();
            LOGGER.debug("Actividad");
            this.setActividadesSeleccionadas(((Actividades) this.nodoSeleccionadoInfo
                .getObjetoProceso()));

            // N: el nombre no
            this.tipoProceso = obtenerTipoProceso(getActividadesSeleccionadas()
                .getRutaActividad().getMacroprocesoProceso());
            UsuarioDTO usuario = MenuMB.getMenu().getUsuarioDto();
            Actividades actividades = (Actividades) (this.nodoSeleccionadoInfo
                .getObjetoProceso().getNodo());
            this.listaInstanciasActividadesSeleccionadas =
                this.getProcesosService().consultarListaActividades(usuario, actividades.
                    getRutaActividad().toString());

            /* ((Actividades) nodoSeleccionadoInfo
					.getObjetoProceso()).getInstanciasActividades(); */
            if (getActividadesSeleccionadas().getTipoProcesamiento() == null ||
                getActividadesSeleccionadas().getTipoProcesamiento().equals("individual")) {
                // actividadSeleccionadas = ((Actividades) nodoSeleccionadoInfo
                // .getObjetoProceso()).getInstanciasActividades();
                this.urlIndividual = this.actividadesSeleccionadas.getUrlCasoDeUso();
                this.url = "";
            } else {
                this.url = this.actividadesSeleccionadas.getUrlCasoDeUso();
                this.urlIndividual = "";
            }
        } else {
            this.listaInstanciasActividadesSeleccionadas = null;
        }

        if (this.nodoSeleccionadoInfo.getObjetoProceso().getData().getTipo()
            .toString().equals("Subproceso")) {
            this.esSubproceso = true;
            LOGGER.debug("Subproceso");
        } else {
            this.esSubproceso = false;
        }

    }

    public Organizacion getOrganizacion() {
        return this.organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public List<Macroproceso> getMacroprocesoOrganizacion() {
        return this.macroprocesos;
    }

    public void setMacroprocesoOrganizacion(
        List<Macroproceso> macroprocesoOrganizacion) {
        this.macroprocesos = macroprocesoOrganizacion;
    }

    public List<Proceso> getProcesoOrganizacion() {
        return this.procesos;
    }

    public void setProcesoOrganizacion(List<Proceso> procesoOrganizacion) {
        this.procesos = procesoOrganizacion;
    }

    public List<Subproceso> getSubprocesoOrganizacion() {
        return this.subprocesos;
    }

    public void setSubprocesoOrganizacion(
        List<Subproceso> subprocesoOrganizacion) {
        this.subprocesos = subprocesoOrganizacion;
    }

    public List<Actividades> getActividadesOrganizacion() {
        return this.actividadesSubprocesos;
    }

    public void setActividadesOrganizacion(
        List<Actividades> actividadesOrganizacion) {
        this.actividadesSubprocesos = actividadesOrganizacion;
    }

    public List<Actividad> getActividadOrganizacion() {
        return this.instanciasActividad;
    }

    public void setActividadOrganizacion(List<Actividad> actividadOrganizacion) {
        this.instanciasActividad = actividadOrganizacion;
    }

    public String onRowSelectNavigate(SelectEvent event) {
        /*
         * FacesContext.getCurrentInstance().getExternalContext().getFlash() .put("selectedCar",
         * event.getObject());
         */
        Actividad instanciaSeleccionadaDT = (Actividad) event.getObject();

        if (instanciaSeleccionadaDT != null) {
            this.instanciaSeleccionada = instanciaSeleccionadaDT;
            LOGGER.debug("Nodo seleccionado: " + this.instanciaSeleccionada.getNombre());
            LOGGER.debug("URL: " + this.instanciaSeleccionada.getURLCasoUso());

        }
        return "carDetail?faces-redirect=true";

    }

    public TreeNode getRoot() {
        return this.root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getNodoSeleccionado() {
        return this.nodoSeleccionado;
    }

    public void setNodoSeleccionado(TreeNode nodoSeleccionado) {
        LOGGER.debug("setNodoSeleccionado");
        this.nodoSeleccionado = nodoSeleccionado;
    }

    public Actividad getInstanciaSeleccionada() {
        return this.instanciaSeleccionada;
    }

    public void setInstanciaSeleccionada(Actividad instanciaSeleccionadaP) {
        // Es necesario el siguiente condicional porque se invoca este metodo al filtrar y presionar enter
        if (instanciaSeleccionadaP != null) {
            this.instanciaSeleccionada = instanciaSeleccionadaP;
            this.url = instanciaSeleccionadaP.getURLCasoUso();
        }
    }

    public ArrayList<NodoProcesosGraficos> getMacroproceso() {
        return this.nodosVistaMacroprocesos;
    }

    public void setMacroproceso(ArrayList<NodoProcesosGraficos> macroproceso) {
        this.nodosVistaMacroprocesos = macroproceso;
    }

    public ArrayList<NodoProcesosGraficos> getProceso() {
        return this.nodosVistaProcesos;
    }

    public void setProceso(ArrayList<NodoProcesosGraficos> proceso) {
        this.nodosVistaProcesos = proceso;
    }

    public ArrayList<NodoProcesosGraficos> getSubProceso() {
        return this.nodosVistaSubprocesos;
    }

    public void setSubProceso(ArrayList<NodoProcesosGraficos> subProceso) {
        this.nodosVistaSubprocesos = subProceso;
    }

    public ArrayList<NodoProcesosGraficos> getActividades() {
        return this.nodosVistaActividades;
    }

    public void setActividades(ArrayList<NodoProcesosGraficos> actividades) {
        this.nodosVistaActividades = actividades;
    }

    public ArrayList<NodoProcesosGraficos> getActividad() {
        return this.nodosVistaInstanciasActividad;
    }

    public void setActividad(ArrayList<NodoProcesosGraficos> actividad) {
        this.nodosVistaInstanciasActividad = actividad;
    }

    public NodoProcesosGraficos getNodoSeleccionadoInfo() {
        return this.nodoSeleccionadoInfo;
    }

    public void buscarNodoSeleccionadoConInformacion() {
        LOGGER.debug("Info del nodo: " + this.nodoSeleccionado.getData().toString());
        this.nodoSeleccionadoInfo = null;
        for (int i = 0; i < this.nodosVistaMacroprocesos.size(); i++) {
            if (this.nodoSeleccionado
                .getData()
                .toString()
                .equals(this.nodosVistaMacroprocesos.get(i).getTreeNode()
                    .getData().toString())) {
                this.nodoSeleccionadoInfo = this.nodosVistaMacroprocesos.get(i);
                break;
            }
        }
        if (this.nodoSeleccionadoInfo == null) {
            for (int i = 0; i < this.nodosVistaProcesos.size(); i++) {
                if (nodoSeleccionado
                    .getData()
                    .toString()
                    .equals(this.nodosVistaProcesos.get(i).getTreeNode()
                        .getData().toString())) {
                    this.nodoSeleccionadoInfo = this.nodosVistaProcesos.get(i);
                    break;
                }
            }
        }
        if (this.nodoSeleccionadoInfo == null) {
            for (int i = 0; i < this.nodosVistaSubprocesos.size(); i++) {
                if (this.nodoSeleccionado
                    .getData()
                    .toString()
                    .equals(this.nodosVistaSubprocesos.get(i).getTreeNode()
                        .getData().toString())) {
                    this.nodoSeleccionadoInfo = this.nodosVistaSubprocesos.get(i);
                    break;
                }
            }
        }

        if (this.nodoSeleccionadoInfo == null) {
            for (int i = 0; i < this.nodosVistaActividades.size(); i++) {
                if (this.nodoSeleccionado
                    .getData()
                    .toString()
                    .equals(this.nodosVistaActividades.get(i).getTreeNode().getData().toString()) &&
                    this.nodoSeleccionado.getParent().getData().toString().equals(
                        this.nodosVistaActividades.get(i).getTreeNode().getParent().getData().
                            toString())) {
                    this.nodoSeleccionadoInfo = this.nodosVistaActividades.get(i);
                    break;
                }
            }
        }
        LOGGER.debug("Tipo Nodo: " +
             this.nodoSeleccionadoInfo.getObjetoProceso().getData().getTipo());
    }

    public boolean isEsSubproceso() {
        return this.esSubproceso;
    }

    public void setEsSubproceso(boolean esSubproceso) {
        this.esSubproceso = esSubproceso;
    }

    public void setNodoSeleccionadoInfo(
        NodoProcesosGraficos nodoSeleccionadoInfo) {
        this.nodoSeleccionadoInfo = nodoSeleccionadoInfo;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Actividades getActividadesSeleccionadas() {
        return this.actividadesSeleccionadas;
    }

    public void setActividadesSeleccionadas(Actividades actividadesSeleccionadas) {
        this.actividadesSeleccionadas = actividadesSeleccionadas;
    }

    public List<Actividad> getListaInstanciasActividadesSeleccionadas() {
        return this.listaInstanciasActividadesSeleccionadas;
    }

    public void setListaInstanciasActividadesSeleccionadas(
        List<Actividad> actividadesSeleccionadas) {
        this.listaInstanciasActividadesSeleccionadas = actividadesSeleccionadas;
    }

    public String getUrlIndividual() {
        return this.urlIndividual;
    }

    public void setTipoProceso(String tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public boolean isEsProcesoConservacion() {
        boolean result = false;
        if (this.tipoProceso != null) {
            result = this.tipoProceso.equals(ETipoProceso.CONSERVACION
                .toString());
        }
        return result;
    }

    public boolean isEsNodoActividades() {
        boolean resultado = false;
        if (this.nodoSeleccionadoInfo != null && this.nodoSeleccionadoInfo.getObjetoProceso() !=
            null) {
            resultado = this.nodoSeleccionadoInfo.getObjetoProceso().getData()
                .getTipo().equals(EObjetoProceso.ACTIVIDAD.toString());
        }
        return resultado;
    }

    public boolean isEsProcesoActualizacion() {
        boolean result = false;
        if (this.tipoProceso != null) {
            result = this.tipoProceso.equals(ETipoProceso.ACTUALIZACION
                .toString());
        }
        return result;

    }

    public boolean isEsProcesoOsmi() {
        boolean result = false;
        if (this.tipoProceso != null) {
            result = this.tipoProceso.equals(ETipoProceso.OSMI.toString());
        }
        return result;

    }

    public boolean isEsProcesoOsmiCalidad() {
        boolean result = false;
        if (this.tipoProceso != null) {
            result = this.tipoProceso.equals(ETipoProceso.OSMI_CALIDAD
                .toString());
        }
        return result;

    }

    public boolean isEsProcesoProductosCatastrales() {
        boolean result = false;
        if (this.tipoProceso != null) {
            result = this.tipoProceso.equals(ETipoProceso.PRODUCTOS_CATASTRALES
                .toString());
        }
        return result;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * retorna un arreglo con los id de los objetos de negocio (trámites) de la lista de instancias
     * de actividad seleccionadas; es decir, de las actividades que salen en la tabla resumen cuando
     * es una actividad masiva del process
     *
     * @author pedro.garcia
     */
    public long[] obtenerIdsTramitesDeInstanciasActividadesSeleccionadas() {

        long[] answer = null;
        int counter = 0;

        if (!this.listaInstanciasActividadesSeleccionadas.isEmpty()) {
            answer = new long[this.listaInstanciasActividadesSeleccionadas.size()];
            for (Actividad activity : this.listaInstanciasActividadesSeleccionadas) {
                answer[counter] = activity.getIdObjetoNegocio();
                counter++;
            }
        }
        return answer;
    }

    public String cerrar() {
        this.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    private String obtenerTipoProceso(String proceso) {
        ETipoProceso tipoProceso = null;
        for (ETipoProceso tipo : ETipoProceso.values()) {
            if (tipo.toString().equals(proceso)) {
                tipoProceso = tipo;
                break;
            }
        }
        return tipoProceso.toString();
    }

    /**
     * Permite la búsqueda de un trámite en la lista de actividades.
     *
     * @author alejandro.sanchez
     */
    public Actividad buscarActividadPorIdTramite(Long idTramite) {
        if (idTramite != null) {
            Actividades actividadesSeleccionadas = this.getActividadesSeleccionadas();
            if (actividadesSeleccionadas != null) {
                List<Actividad> actividades = this.getProcesosService().consultarListaActividades(
                    MenuMB.getMenu()
                        .getUsuarioDto(), actividadesSeleccionadas.getRutaActividad().toString());
                if (actividades != null) {
                    for (Actividad a : actividades) {
                        if (idTramite.equals(a.getIdObjetoNegocio())) {
                            //Se hace el return tan pronto lo encuentra y así se evita una condicional más. 
                            //Sé que va en contra de los estándares :D, pero son estándares no leyes.
                            return a;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Busca la actividad asociada al tramite de la lista de actividades seleccionadas en una
     * actividad seleccionada
     *
     * @author felipe.cadena
     *
     * @param idTramite
     *
     * @return
     */
    public Actividad buscarActividadPorIdTramiteSeleccionadas(Long idTramite) {
        LOGGER.debug("on TareasPendientesMB#buscarActividadPorIdTramiteSeleccionadas ...");
        Actividad actividad = null;
        if (idTramite != null && this.listaInstanciasActividadesSeleccionadas != null) {

            for (Actividad act : this.listaInstanciasActividadesSeleccionadas) {
                if (idTramite.equals(act.getIdObjetoNegocio())) {
                    actividad = act;
                    break;
                }
            }

        }
        return actividad;
    }

    public List<String> getActividadesConPermiso() {
        return this.actividadesConPermiso;
    }

    public void setActividadesConPermiso(List<String> actividadesConPermiso) {
        this.actividadesConPermiso = actividadesConPermiso;
    }

    public String getImagenProceso() {
        return this.urlImagenProceso;
    }

    public void setIdTarea(String idTarea) {
        if (idTarea != null) {
            this.urlImagenProceso = this.getProcesosService().obtenerFlujoEjecucionProceso(idTarea);
        }
    }

    public String getOpcionActual() {
        return this.opcionActual;
    }

    public void setOpcionActual(String opcionActual) {
        this.opcionActual = opcionActual;
    }

    /**
     * Método que se ejecuta al cambiar de tab en la pantalla de lista tareas.
     *
     * @author david.cifuentes
     */
    public void onTabChange(TabChangeEvent event) {

        String tabId = event.getTab().getId();

    }

    public String getUrlPagina() {
        return (this.url == null || this.url.equals("") ? "" : request.getContextPath()) +
            (this.url == null || this.url.equals("") ? "" : this.url);
    }

    public void setUrlPagina(String url) {
        this.urlPagina = url;
    }

    /**
     * Método que forza el cargue del init.
     *
     * @author david.cifuentes
     * @return
     */
    public String cargarInit() {
        try {
            // Fue necesario hacer éste método por que la pantalla de
            // tareas pendientes al limpiar la sesión por alguna razón se dañaba
            // cuando se ejecutaba únicamente el llamado al init.
            this.init();
            return ConstantesNavegacionWeb.INDEX;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método que permite activar el botón de eliminar documentos adicionales en la pantalla de
     * documentación del trámite, pero sólo si está en la actividad se pone este método acá porque
     * estaba en EjecucionMB y se cargaba este managed bean sólo por esa variable booleana, y ya que
     * este managed bean es cargado por todos los casos de uso ya estaría en sesión Bandera que se
     * usa en tablaDocumentosAsociados.xhtml para mostrar el boton de eliminar documentos
     * adicionales de los Tramite Tramites
     *
     * @author javier.aponte
     */
    public boolean isBanderaEliminarDocsAdicionales() {

        boolean answer = false;

        Actividad actividad;

        actividad = this.getInstanciaSeleccionada();

        if (actividad != null && (actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE) ||
            actividad.getNombre().startsWith("InvocarAsociarDocumentoNuevoTramite"))) {
            answer = true;
        }

        return answer;
    }

}
