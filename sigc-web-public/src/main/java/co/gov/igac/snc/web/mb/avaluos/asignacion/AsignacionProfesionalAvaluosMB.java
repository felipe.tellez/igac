/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.asignacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosCarga;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso Asignar avaluador
 *
 * @cu CU-SA-AC-003
 * @author pedro.garcia
 */
@Component("asignacionProfesionalAvaluos")
@Scope("session")
public class AsignacionProfesionalAvaluosMB extends SNCManagedBean {

    private static final long serialVersionUID = 10439640577161317L;

    private static final Logger LOGGER =
        LoggerFactory.getLogger(AsignacionProfesionalAvaluosMB.class);

    /**
     * variable donde se cargan los resultados paginados de la consulta de avalúos a los que no se
     * les ha asignado ejecutor
     */
    private LazyDataModel<Avaluo> lazyWorkingAvaluos;

    /**
     * variable donde se cargan los resultados paginados de la consulta de avalúos a los que ya se
     * les ha asignado ejecutor
     */
    private LazyDataModel<Avaluo> lazyWorkingAssignedAvaluos;

    /**
     * Trámites de avalúo seleccionados de la tabla de trámites disponibles para ser asignados.
     */
    private Avaluo[] selectedAvaluosForAsignacion;

    /**
     * Trámites de avalúo seleccionados de la tabla de trámites disponibles para ser asignados.
     */
    private Avaluo[] selectedAssignedAvaluos;

    /**
     * código de la actividad de asignación del proceso por la que se inicia el CU
     */
    private String actividadDeAsignacion;

    /**
     * lista de profesionales de avalúos de donde se seleccionan los que van a ser asignados
     */
    private ArrayList<VProfesionalAvaluosCarga> profesionalesAvaluosTerritorial;

    /**
     * profesionales de avalúos seleccionados para la asignación
     */
    private VProfesionalAvaluosCarga[] selectedProfesionalesAvaluos;

    /**
     * guardan los valores que corresponde a si se requieren los controles de calidad cuando es una
     * solicitud de corrección
     */
    private String requiereControlCalidad;
    private String requiereControlCalidadTerritorial;
    private String requiereControlCalidadSedeCentral;

    /**
     * objeto Asignacion que se inserta en la BD
     */
    private Asignacion newAsignacion;

    //----------------------  BPM   -----------------------
    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    //---------
    //--------  banderas  ------------------------
    /**
     * sirven para identificar la actividad del proceso por la que se llega al CU. Deben ser
     * excluyentes.
     */
    private boolean isActividadAvaluo;
    private boolean isActividadCCTerritorial;
    private boolean isActividadCCSedeCentral;
    private boolean isActividadCCGIT;

    /**
     * usuario que tiene la sesión
     */
    private UsuarioDTO loggedInUser;

    //------------------------------    methods    ---------------------------------
//--------------- getters and setters  ------------------------------
    public String getRequiereControlCalidad() {
        return this.requiereControlCalidad;
    }

    public void setRequiereControlCalidad(String requiereControlCalidad) {
        this.requiereControlCalidad = requiereControlCalidad;
    }

    public String getRequiereControlCalidadTerritorial() {
        return this.requiereControlCalidadTerritorial;
    }

    public void setRequiereControlCalidadTerritorial(String requiereControlCalidadTerritorial) {
        this.requiereControlCalidadTerritorial = requiereControlCalidadTerritorial;
    }

    public String getRequiereControlCalidadSedeCentral() {
        return this.requiereControlCalidadSedeCentral;
    }

    public void setRequiereControlCalidadSedeCentral(String requiereControlCalidadSedeCentral) {
        this.requiereControlCalidadSedeCentral = requiereControlCalidadSedeCentral;
    }

    public ArrayList<VProfesionalAvaluosCarga> getProfesionalesAvaluosTerritorial() {
        return this.profesionalesAvaluosTerritorial;
    }

    public void setProfesionalesAvaluosTerritorial(ArrayList<VProfesionalAvaluosCarga> pAT) {
        this.profesionalesAvaluosTerritorial = pAT;
    }

    public VProfesionalAvaluosCarga[] getSelectedProfesionalesAvaluos() {
        return this.selectedProfesionalesAvaluos;
    }

    public void setSelectedProfesionalesAvaluos(VProfesionalAvaluosCarga[] sPA) {
        this.selectedProfesionalesAvaluos = sPA;
    }

    public LazyDataModel<Avaluo> getLazyWorkingAvaluos() {
        return this.lazyWorkingAvaluos;
    }

    public void setLazyWorkingAvaluos(LazyDataModel<Avaluo> lazyWorkingAvaluos) {
        this.lazyWorkingAvaluos = lazyWorkingAvaluos;
    }

    public Avaluo[] getSelectedAvaluosForAsignacion() {
        return this.selectedAvaluosForAsignacion;
    }

    public void setSelectedAvaluosForAsignacion(Avaluo[] selectedAvaluosForAsignacion) {
        this.selectedAvaluosForAsignacion = selectedAvaluosForAsignacion;
    }

    public LazyDataModel<Avaluo> getLazyWorkingAssignedAvaluos() {
        return this.lazyWorkingAssignedAvaluos;
    }

    public void setLazyWorkingAssignedAvaluos(LazyDataModel<Avaluo> lazyWorkingAssignedAvaluos) {
        this.lazyWorkingAssignedAvaluos = lazyWorkingAssignedAvaluos;
    }

    public Avaluo[] getSelectedAssignedAvaluos() {
        return this.selectedAssignedAvaluos;
    }

    public void setSelectedAssignedAvaluos(Avaluo[] selectedAssignedAvaluos) {
        this.selectedAssignedAvaluos = selectedAssignedAvaluos;
    }

//--------
//--------------------------------------------------------------------------------------------------
    /**
     * constructor del mb
     *
     * @author pedro.garcia
     * @version 2.0
     */
    public AsignacionProfesionalAvaluosMB() {

        this.lazyWorkingAvaluos = new LazyDataModel<Avaluo>() {

            @Override
            public List<Avaluo> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Avaluo> answer = new ArrayList<Avaluo>();

                populateAvaluosLazyly(answer, first, pageSize, sortField, sortField, filters);

                return answer;
            }
        };

        this.lazyWorkingAssignedAvaluos = new LazyDataModel<Avaluo>() {

            @Override
            public List<Avaluo> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Avaluo> answer = new ArrayList<Avaluo>();

                populateAssignedAvaluosLazyly(answer, first, pageSize, sortField, sortField, filters);

                return answer;
            }
        };

    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de AsignacionProfesionalAvaluosMB");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        // D: se obtienen los ids de los trámites objetivo a partir del árbol de tareas
//		this.idsTramitesActividades = this.tareasPendientesMB
//				.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();
//FIXME :: pedro.garcia :: datos quemados para pruebas. Uncomment the line obtaining ids from BPM
        this.idsTramitesActividades = new long[2];
        this.idsTramitesActividades[0] = 43327l; //172
        this.idsTramitesActividades[1] = 43439l; //173
//        this.idsTramitesActividades[3] = 43086l;
//        this.idsTramitesActividades[2] = 43322l;
//        this.idsTramitesActividades[4] = 43318l;
//        this.idsTramitesActividades[5] = 43286l;
//        this.idsTramitesActividades[6] = 43267l;

//TODO :: snc.avaluos :: cuando se integre con bpm obtener la actividad de allí e inicializar banderas de forma correspondiente
        this.actividadDeAsignacion = EAvaluoAsignacionProfActi.AVALUAR.getCodigo();
        this.isActividadAvaluo = true;

        int count;
        count = this.getAvaluosService().contarAvaluosPorIdsParaAsignacion(
            this.idsTramitesActividades, this.actividadDeAsignacion);
        this.lazyWorkingAvaluos.setRowCount(count);

        //N: si los datos están bien, esto lo puedo cambiar por:
//		count = this.getAvaluosService().contarAvaluosPorIdsConAsignacionSinMover(
//            this.idsTramitesActividades, this.actividadDeAsignacion);
        count = this.idsTramitesActividades.length - count;

        this.lazyWorkingAssignedAvaluos.setRowCount(count);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * hace la búsqueda de trámites para asignar ejecutor
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     */
    private void populateAvaluosLazyly(List<Avaluo> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<Avaluo> rows;
        int size;

        rows = this.getAvaluosService().obtenerAvaluosPorIdsParaAsignacion(
            this.idsTramitesActividades, this.actividadDeAsignacion, false, sortField, sortOrder,
            filters, first, pageSize);

        if (rows != null) {
            size = rows.size();

            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * hace la búsqueda de trámites para asignar ejecutor
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     */
    private void populateAssignedAvaluosLazyly(List<Avaluo> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<Avaluo> rows = null;
        int size;

        rows = this.getAvaluosService().obtenerAvaluosPorIdsParaAsignacion(
            this.idsTramitesActividades, this.actividadDeAsignacion, true, sortField, sortOrder,
            filters, first, pageSize);

        if (rows != null) {
            size = rows.size();

            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Asignar profesional" de la ventana principal.
     *
     * 1. Valida, según el tipo de trámite, que se puedan hacer asignaciones masivas o no 2. hace la
     * consulta de profesionales de avalúos que pertenecen a la territorial
     */
    public void asignarProfesionalesInit() {

        String errorMessage = null;
        Avaluo primerAvaluoSeleccionado;

        // 1:
        primerAvaluoSeleccionado = this.selectedAvaluosForAsignacion[0];
        if (this.selectedAvaluosForAsignacion.length > 1) {
            if (primerAvaluoSeleccionado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_REVISION.getCodigo()) ||
                primerAvaluoSeleccionado.getTramite().getTipoTramite().equals(
                    ETramiteTipoTramite.SOLICITUD_DE_CORRECCION.getCodigo())) {
                errorMessage = "Los tipos de trámite " +
                    ETramiteTipoTramite.SOLICITUD_DE_REVISION.getNombre() + ", " +
                    ETramiteTipoTramite.SOLICITUD_DE_CORRECCION.getNombre() +
                    " solo se pueden asignar de uno en uno";

                this.addMensajeError(errorMessage);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "pailander");
                return;
            }
        }

        // 2:
        this.profesionalesAvaluosTerritorial = (ArrayList<VProfesionalAvaluosCarga>) this.
            getAvaluosService().consultarProfesionalesAvaluosCargaPorTerritorial(
                this.loggedInUser.getCodigoEstructuraOrganizacional());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Asignar" de la ventana principal donde se muestra el listado de
     * profesionales de avalúos
     *
     * 1. valida que los seleccionados estén todos activos 2. valida las condiciones de asignación
     * 3. Si se cumplen las condiciones, se guardan las asignaciones 4. calcula los tamaños de la
     * listas lazy de las tablas
     */
    public void asignarProfesionales() {

        String errorMessage = null;

        // 1:
        for (VProfesionalAvaluosCarga profesional : this.selectedProfesionalesAvaluos) {
            if (profesional.getProfesionalActivo().equals(ESiNo.NO.getCodigo())) {
                errorMessage = "Alguno de los profesionales de avalúos seleccionados no está activo";
                this.addMensajeError(errorMessage);
                return;
            }
        }

        // 2:
        if (!validarCondicionesAsignacion()) {
            return;
        }

        // 3:
        //D: primero se inserta la Asignacion ...
        Date currentDate;
        currentDate = new Date(System.currentTimeMillis());
        this.newAsignacion = new Asignacion();
        this.newAsignacion.setFecha(currentDate);
        this.newAsignacion.setFechaLog(currentDate);
        this.newAsignacion.setUsuarioLog(this.loggedInUser.getLogin());
        this.newAsignacion.setFuncionarioAsignador(this.loggedInUser.getLogin());
        this.newAsignacion.setTerritorialCodigo(this.loggedInUser.
            getCodigoEstructuraOrganizacional());

        //D: ... y luego se insertan los AvaluoAsignacionProfesional (que deben tener una Asignacion
        //   relacionada)
        this.newAsignacion = this.getAvaluosService().
            guardarActualizarAsignacion(this.newAsignacion);
        if (this.newAsignacion != null && this.newAsignacion.getId() != null) {
            ArrayList<AvaluoAsignacionProfesional> newAsignaciones;
            newAsignaciones = armarObjetosAsignaciones();
            if (!this.getAvaluosService().guardarAvaluosAsignacionProfesional(newAsignaciones)) {
                errorMessage = "Error insertando las asignaciones";
            }
        } else {
            errorMessage = "Error insertando Asignacion";
        }

        int count;
        count = this.getAvaluosService().contarAvaluosPorIdsParaAsignacion(
            this.idsTramitesActividades, this.actividadDeAsignacion);
        this.lazyWorkingAvaluos.setRowCount(count);
        count = this.idsTramitesActividades.length - count;
        this.lazyWorkingAssignedAvaluos.setRowCount(count);

        if (errorMessage != null) {
            LOGGER.error(errorMessage);
            this.addMensajeError(errorMessage);
        } else {
            this.addMensajeInfo("Asignaciones realizadas exitosamente");
            this.selectedAvaluosForAsignacion = null;
            this.selectedProfesionalesAvaluos = null;
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Valida si se cumple la condición de que haya plata para que las asignación de profesionales a
     * una solicitud de avalúo.
     *
     * Para cada uno de los avaluadores seleccionados, que sean contratistas, se valida que
     * SUM(valor cotizacion predio) Predios / # profesionales seleccionados LE saldo por pagar -
     * valor comprometido
     *
     * PRE: el número de profesionales de avalúos seleccionados es GE 1
     *
     * @return
     */
    private boolean hayRecursosParaAvaluo() {

        boolean answer = true;
        long valorTotalCotizacionPredios;
        int nofProfAvaluos;
        double comparator, disponibleProfesional;

        valorTotalCotizacionPredios = 0l;
        for (Avaluo tempAvaluo : this.selectedAvaluosForAsignacion) {
            for (AvaluoPredio tempAvaluoPredio : tempAvaluo.getAvaluoPredios()) {
                valorTotalCotizacionPredios += tempAvaluoPredio.getValorCotizacion().longValue();
            }
        }

        nofProfAvaluos = this.selectedProfesionalesAvaluos.length;
        comparator = valorTotalCotizacionPredios / nofProfAvaluos;

        for (VProfesionalAvaluosCarga profesional : this.selectedProfesionalesAvaluos) {
            if (profesional.getTipoVinculacion().equals(
                EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS.getCodigo())) {

                disponibleProfesional =
                    profesional.getSaldoPorPagar() - profesional.getValorComprometido();
                if (comparator > disponibleProfesional) {
                    answer = false;
                    break;
                }
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * valida la regla de que los seleccionados para ser asignados a una solicitud de revisión sean
     * los mismos que hicieron el avalúo
     *
     * @return
     */
    private boolean validarAsignadosParaRevision() {

        boolean answer = false;
        long[] idsProfesionalesAvaluos;
        int counter;

        counter = 0;
        idsProfesionalesAvaluos = new long[this.selectedProfesionalesAvaluos.length];
        for (VProfesionalAvaluosCarga profesional : this.selectedProfesionalesAvaluos) {
            idsProfesionalesAvaluos[counter] = profesional.getProfesionalAvaluosId().longValue();
            counter++;
        }
        answer = this.getAvaluosService().validarAsignadosRevisionSonAvaluadores(
            idsProfesionalesAvaluos, this.selectedAvaluosForAsignacion[0].getId());

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * valida las condiciones para la asignación
     *
     * 2.1 para control de calidad no puede seleccionar más de un profesional 2.2 El profesional
     * seleccionado para el control de calidad no debe ser ninguno de los que realizó el avalúo. 2.3
     * si es solicitud de cotización no debe seleccionar más de un profesional para realizar dicho
     * trámite. 2.4 es solicitud de avalúo y no alcanza la plata de los contratistas 2.5 Para una
     * solicitud de impugnación solo debe ser seleccionado un profesional 2.6 Para una solicitud de
     * impugnación el profesional seleccionado debe ser de carrera administrativa 2.7 Una solicitud
     * de revisión debe ser asignada a los mismos que hicieron el avalúo y que estén activos 2.8 El
     * control de calidad de una solicitud de revisión debe ser hecho por un solo profesional 2.9 El
     * que haga el control de calidad de una solicitud de revisión debe ser de sede central
     */
    private boolean validarCondicionesAsignacion() {

        boolean answer;
        String errorMessage = null;
        Avaluo primerAvaluoSeleccionado;
        long[] idsAvaluosSeleccionados;
        int counter;

        answer = false;
        try {

            // 2:
            if (this.isActividadCCGIT || this.isActividadCCSedeCentral ||
                this.isActividadCCTerritorial) {

                // 2.1:
                if (this.selectedProfesionalesAvaluos.length > 1) {
                    errorMessage =
                        "El control de calidad debe ser realizado por un solo profesional de avalúos";
                    throw new Exception();
                }

                // 2.2:
                idsAvaluosSeleccionados = new long[this.selectedAvaluosForAsignacion.length];
                counter = 0;
                for (Avaluo avaluo : this.selectedAvaluosForAsignacion) {
                    idsAvaluosSeleccionados[counter] = avaluo.getId();
                    counter++;
                }

                if (this.getAvaluosService().validarAsignadoControlCalidadNoEsAvaluador(
                    this.selectedProfesionalesAvaluos[0].getProfesionalAvaluosId(),
                    idsAvaluosSeleccionados)) {
                    errorMessage =
                        "El profesional de avalúos seleccionado para el control de calidad no " +
                        "debe ser el mismo que hizo el avalúo";
                    throw new Exception();
                }
            }

            primerAvaluoSeleccionado = this.selectedAvaluosForAsignacion[0];
            // 2.3:
            if (primerAvaluoSeleccionado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.getCodigo()) &&
                this.selectedProfesionalesAvaluos.length > 1) {
                errorMessage =
                    "La solicitud de cotización debe ser realizada por un solo profesional de avalúos";
                throw new Exception();
            }

            // 2.4:
            if (primerAvaluoSeleccionado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo())) {
                if (!hayRecursosParaAvaluo()) {
                    errorMessage =
                        "Alguno de los profesionales asignados no cuenta con los recursos para realizar este trámite";
                    throw new Exception();
                }
            }

            if (primerAvaluoSeleccionado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION.getCodigo())) {

                // 2.5:
                if (this.selectedProfesionalesAvaluos.length > 1) {
                    errorMessage = "El trámite de " + ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION.
                        getNombre() +
                         " debe ser realizado por un solo profesional de avalúos";
                    throw new Exception();
                }

                // 2.6:
                if (!this.selectedProfesionalesAvaluos[0].getTipoVinculacion().equals(
                    EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA.getCodigo())) {
                    errorMessage =
                        "El profesional asignado para la impugnación debe ser un profesional" +
                         " de avalúos de carrera administrativa";
                    throw new Exception();
                }
            }

            if (primerAvaluoSeleccionado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_REVISION.getCodigo())) {

                // 2.7:
                if (!validarAsignadosParaRevision()) {
                    errorMessage =
                        "El profesional(es) asignado a la revisión debe ser el mismo que realizó el avalúo";
                    throw new Exception();
                }

                if (this.isActividadCCGIT || this.isActividadCCSedeCentral ||
                    this.isActividadCCTerritorial) {

                    // 2.8:
                    if (this.selectedProfesionalesAvaluos.length > 1) {
                        errorMessage =
                            "El control de calidad de una solicitud de revisión debe ser " +
                            "realizado por un solo profesional de avalúos";
                        throw new Exception();
                    }

                    // 2.9:
                    if (!this.selectedProfesionalesAvaluos[0].getTerritorialCodigo().equals(
                        Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL)) {
                        errorMessage =
                            "El control de calidad de una solicitud de revisión debe ser " +
                            "realizado por un profesional de la sede central";
                        throw new Exception();
                    }
                }
            }

            answer = true;

        }// try
        catch (Exception ex) {
            LOGGER.error("Error validando condiciones para asignación");
            if (errorMessage != null && !errorMessage.isEmpty()) {
                this.addMensajeError(errorMessage);
            }
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma la lista de objetos de tipo AvaluoAsignacionProfesional que se van a insertar
     *
     * @return
     */
    private ArrayList<AvaluoAsignacionProfesional> armarObjetosAsignaciones() {

        ArrayList<AvaluoAsignacionProfesional> answer;
        AvaluoAsignacionProfesional newAAP;
        Date currentDate;
        ProfesionalAvaluo tempProfesional;

        currentDate = new Date(System.currentTimeMillis());
        answer = new ArrayList<AvaluoAsignacionProfesional>();

        for (Avaluo selectedAvaluo : this.selectedAvaluosForAsignacion) {
            for (VProfesionalAvaluosCarga profesional : this.selectedProfesionalesAvaluos) {
                newAAP = new AvaluoAsignacionProfesional();
                newAAP.setAceptada(ESiNo.NO.getCodigo());
                newAAP.setActividad(this.actividadDeAsignacion);
                newAAP.setFechaDesde(currentDate);
                newAAP.setFechaLog(currentDate);
                newAAP.setUsuarioLog(this.loggedInUser.getLogin());
                newAAP.setAvaluo(selectedAvaluo);

                tempProfesional = new ProfesionalAvaluo();
                tempProfesional.setId(profesional.getProfesionalAvaluosId());
                newAAP.setProfesionalAvaluo(tempProfesional);

                newAAP.setProfesionalAvaluoContratoId(profesional.getContratoId());
                newAAP.setAsignacion(this.newAsignacion);

                answer.add(newAAP);
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "desasignar profesional(es)" de la tabla de asignaciones en la páginna
     * principal.
     *
     * Borra las asignaciones correspondientes.
     *
     * 1. Borra las AvaluoAsignacionProfesional (en el método invocado se borra la Asignacion si es
     * necesario)
     */
    public void desasignarProfesionales() {

        boolean asignacionesAvaluoBorradas;

        for (Avaluo avaluo : this.selectedAssignedAvaluos) {
            asignacionesAvaluoBorradas = this.getAvaluosService().
                borrarAsignacionesVigentesDeAvaluo(avaluo.getId(), this.actividadDeAsignacion);
            if (!asignacionesAvaluoBorradas) {
                this.addMensajeError("Ocurrió un error borrando las asignaciones");
            }
        }

        int count;
        count = this.getAvaluosService().contarAvaluosPorIdsParaAsignacion(
            this.idsTramitesActividades, this.actividadDeAsignacion);
        this.lazyWorkingAvaluos.setRowCount(count);
        count = this.idsTramitesActividades.length - count;
        this.lazyWorkingAssignedAvaluos.setRowCount(count);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "confirmar aignaciones" de la página principal. Mueve el proceso para los
     * trámites asognados.
     *
     * 1. Envia correos a los asignados cuando es un control de calidad o una solicitud de
     * cotización o de corrección
     *
     * 2. mueve el proceso
     */
    public void confirmarAsignaciones() {

        boolean envioCorreosOK;

        // 1:
        for (Avaluo avaluoAsignado : this.selectedAssignedAvaluos) {
            if (debeEnviarCorreoAsignacion(avaluoAsignado)) {
                envioCorreosOK = enviarCorreosAsignacionesAvaluo(avaluoAsignado);
                if (!envioCorreosOK) {
                    this.addMensajeError(
                        "Ocurrió en error enviando correo electrónico a los asignados");
                    return;
                }
            }
        }

        // 2:
//TODO :: snc.avaluos :: aquí es donde se mueve el proceso
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Envía correos electrónicos a los implicados en la asignación según las reglas:
     *
     * PRE: al avaluo trae los AvaluoAsignacionProfesional
     *
     * @param avaluoAsignado Avaluo cuyas asignaciones se van a enviar por correo electrónico
     * @return
     */
    private boolean enviarCorreosAsignacionesAvaluo(Avaluo avaluoAsignado) {

        boolean answer = false;
        ArrayList<String> destinatariosTO;
        String cuerpoMensaje, cuerpoMensajeObservaciones, asuntoMensaje, despedidaMensaje,
            tipoTramite, territorial;
        MessageFormat messageFormat;
        Object[] argumentos;
        Asignacion asignacionAsignaciones;

        destinatariosTO = new ArrayList<String>();
        for (AvaluoAsignacionProfesional asignacion : avaluoAsignado.
            getAvaluoAsignacionProfesionals()) {
            destinatariosTO.add(asignacion.getProfesionalAvaluo().getCorreoElectronico());
        }

        tipoTramite = avaluoAsignado.getTramite().getTipoTramiteCadenaCompleto();
        asignacionAsignaciones = avaluoAsignado.getAvaluoAsignacionProfesionals().
            get(0).getAsignacion();

        messageFormat = new MessageFormat(
            ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_ASIGNACION_AVALUOS);
        argumentos = new Object[1];
        argumentos[0] = tipoTramite;
        asuntoMensaje = messageFormat.format(argumentos);

        messageFormat = new MessageFormat(
            ConstantesComunicacionesCorreoElectronico.CUERPO_CORREO_ASIGNACION_AVALUOS);
        argumentos = new Object[3];
        argumentos[0] = tipoTramite;
        argumentos[1] = avaluoAsignado.getSecRadicado();
        argumentos[2] = this.actividadDeAsignacion;
        cuerpoMensaje = messageFormat.format(argumentos);

        if (asignacionAsignaciones.getObservaciones() != null &&
            !asignacionAsignaciones.getObservaciones().isEmpty()) {
            messageFormat = new MessageFormat(
                ConstantesComunicacionesCorreoElectronico.CUERPO_OBSERVACIONES_CORREO_ASIGNACION_AVALUOS);
            argumentos = new Object[1];
            argumentos[0] = asignacionAsignaciones.getObservaciones();
            cuerpoMensajeObservaciones = messageFormat.format(argumentos);
            cuerpoMensaje += cuerpoMensajeObservaciones;
        }

        territorial = this.loggedInUser.getDescripcionEstructuraOrganizacional();
        messageFormat = new MessageFormat(
            ConstantesComunicacionesCorreoElectronico.DESPEDIDA_CORREO_ASIGNACION_AVALUOS);
        argumentos = new Object[3];
        argumentos[0] = territorial;
        argumentos[1] = this.loggedInUser.getNombreCompleto();
        if (this.loggedInUser.getCodigoEstructuraOrganizacional().equals(
            Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL)) {
            argumentos[2] = "Coordinador de GIT de Avalúos";
        } else {
            argumentos[2] = "Director de Territorial " + territorial;
        }
        despedidaMensaje = messageFormat.format(argumentos);

        cuerpoMensaje += despedidaMensaje;

        answer = this.getGeneralesService().enviarCorreo(destinatariosTO, null, null,
            asuntoMensaje, cuerpoMensaje, null, null);

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Determina si se debe enviar correo electrónico cuando se confirman las asignaciones. Se envía
     * correo si se trata de un control de calidad o de una solicitud de cotización o de corrección
     *
     * @param avaluoAsignado
     * @return
     */
    private boolean debeEnviarCorreoAsignacion(Avaluo avaluoAsignado) {

        boolean answer = false;

        if ((this.isActividadCCGIT || this.isActividadCCSedeCentral || this.isActividadCCTerritorial) ||
             avaluoAsignado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.getCodigo()) ||
             avaluoAsignado.getTramite().getTipoTramite().equals(
                ETramiteTipoTramite.SOLICITUD_DE_CORRECCION.getCodigo())) {

            answer = true;
        }

        return answer;
    }

//end of class
}
