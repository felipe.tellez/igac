package co.gov.igac.snc.web.util;

import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.GeneralMB;

public class DepartamentoConverter implements Converter {

    @Autowired
    GeneralMB generalMB;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
        String value) {
        if (!StringUtils.isEmpty(value)) {
            Map requestMap = context.getExternalContext().getRequestMap();
            String codPais = (String) requestMap.get("codigoPais");
            if (codPais == null) {
                codPais = Constantes.COLOMBIA;
            }
            List<Departamento> departamentos = generalMB
                .getDepartamentos(codPais);
            for (Departamento depto : departamentos) {
                if (depto.getCodigo().equals(value)) {
                    return depto;
                }
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            Departamento departamento = (Departamento) value;
            return departamento.getCodigo();
        }
        return null;
    }

}
