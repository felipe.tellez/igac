package co.gov.igac.snc.web.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;

public class SecurityFilter implements Filter {

    private FilterConfig filterConfig = null;
    //TODO :: juanfelipe.garcia :: ajustar estas paginas, crean brechas de seguridad   
    private String[] exclude = {"javax.faces.resource",
        "listaTareas.jsf", "login.jsf",
        "/tramite/radicacion/detalleSolicitudGeneral.jsf",
        "/conservacion/ejecucion/tramitesParaRealizarEnTerreno.jsf",
        "/conservacion/proyeccion/proyectarMutacion.jsf",
        "/conservacion/ejecucion/devolverTramite.jsf",
        "/conservacion/ejecucion/enviarADepuracionGeografica.jsf",
        "/conservacion/ejecucion/gestionarTramiteNoProcede.jsf",
        "/conservacion/ejecucion/solicitarPruebas.jsf",
        "/conservacion/ejecucion/asociarTramitesNuevos.jsf",
        "/conservacion/ejecucion/asociarTramitesNuevosRevisarTramiteAsignado.jsf",
        "/conservacion/ejecucion/elaborarAutoDePruebas.jsf",
        "/conservacion/asignacion/manejoComisionesTramites.jsf",
        "/conservacion/asignacion/estableceProcedenciaSolicitudIndividual.jsf",
        "/avaluos/ofertasInmob/registroOfertaInmobiliaria.jsf",
        "/productos/registrarCorreccionesAProductosEntregados.jsf", //TODO :: pedro.garcia :: NO SUBIR ESTO sin comentar. Es solo para pruebas
//        "/avaluos/radicacion/asociarDocsRadicacionATramitesAvaluo.jsf",
//        "/avaluos/administracion/administrarInformacionAvaluadoresExternos.jsf",
//        "/contenedorCCBusquedaPredios.jsf",
//        "/avaluos/administracion/adminAvaluosAsocAContrato.jsf",
//        "/avaluos/asignacion/generarOrdenPractica.jsf",
//        "/herramientas/busquedaBasicaPredios.jsf",
//       
//        "/conservacion/depuracion/manejoComisionesTramitesD.jsf",
//        "/conservacion/aprobarComision.jsf"            
};

    public static Logger LOGGER = LoggerFactory.getLogger(SecurityFilter.class);

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
        FilterChain fc) throws IOException, ServletException {

        HttpServletRequest hsr = (HttpServletRequest) req;
        HttpServletResponse hsres = (HttpServletResponse) res;

        String uri = hsr.getRequestURI();
        HttpSession s = hsr.getSession(false);

        for (String e : exclude) {
            if (uri.indexOf(e) != -1 || uri.indexOf("tests/") != -1) {
                /* List<VPermisoGrupo> opcionesConPermiso=menu.getOpcionesConPermiso();
                 * for(VPermisoGrupo vpg:opcionesConPermiso){ if (uri.endsWith(vpg.getUrl())){
                 * tpMB.setOpcionActual(vpg.getNombre()); break; }
				} */

                fc.doFilter(req, res);
                return;
            }
        }

        if (s == null || s.getAttribute("tareasPendientes") == null) {
            hsres.sendRedirect("/snc/login.jsf");
        } else {

            MenuMB menu = (MenuMB) s.getAttribute("menu");
            TareasPendientesMB tpMB = (TareasPendientesMB) s.getAttribute("tareasPendientes");

            List<VPermisoGrupo> opcionesConPermiso = menu.getOpcionesConPermiso();
            boolean permiso = false;
            for (VPermisoGrupo vpg : opcionesConPermiso) {
                if (uri.endsWith(vpg.getUrl())) {

                    tpMB.setOpcionActual(vpg.getNombre());

                    permiso = true;
                    break;
                }
            }
            TareasPendientesMB tareas = (TareasPendientesMB) s.getAttribute("tareasPendientes");
            List<String> actividadesConPermiso = tareas.getActividadesConPermiso();
            for (String acp : actividadesConPermiso) {
                if (uri.endsWith(acp)) {
                    permiso = true;
                    break;
                }
            }
            if (permiso) {
                fc.doFilter(req, res);
            } else {
                LOGGER.debug("URL sin permiso : " + hsr.getRequestURI());
                hsres.sendRedirect("/snc/login.jsf");
            }
        }

    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
        this.filterConfig = fc;
    }

}
