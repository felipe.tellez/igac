/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipoCalificacion;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.CalificacionConstruccionTreeNode;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.util.Date;
import javax.faces.model.SelectItem;

/**
 * Managed bean provisional para la gestión del detalle de unidades de construcción en la proyección
 *
 * TODO meterlo en ProyectarConservacionMB
 *
 * @author pedro.garcia
 */
@Component("gestionDetalleUnidadConstruccion")
@Scope("session")
public class GestionDetalleUnidadesConstruccionMB extends SNCManagedBean {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(GestionDetalleUnidadesConstruccionMB.class);

    /**
     * variable que guarda los datos del usuario que usa la aplicación
     */
    private UsuarioDTO usuario;

    private Tramite tramite;

    private PPredio predioSeleccionado;

    /**
     * temp: es la dde Fabio
     */
    private PUnidadConstruccion unidadConstruccionSeleccionada;

//TODO revisar si con la integración este sigue siendo necesario
    /**
     * árbol que se arma por defecto
     */
    private TreeNode treeComponentesConstruccion;

    /**
     * los tres árboles que se arman para los tipos de calificación de la unidad de construcción
     * Hubo que manejar 3 aparte y que se creen al principio para que no se generen errores por
     * updates
     */
    private TreeNode treeComponentesConstruccion1;
    private TreeNode treeComponentesConstruccion2;
    private TreeNode treeComponentesConstruccion3;

    /**
     * lista con todos los items del combo de usos de construcción
     */
    private ArrayList<SelectItem> usosUnidadConstruccionItemList;

    /**
     * uso de construcción seleccionado en el combo
     */
    private Long selectedUsoUnidadConstruccionId;

    /**
     * uso de construcción como objeto para obtener valores necesarios para otros campos
     */
    private UsoConstruccion selectedUsoUnidadConstruccion;

    /**
     * lista con todos los posibles usos de construcción (tabla USO_CONSTRUCCION)
     */
    private List<UsoConstruccion> usosConstruccionList;

    /**
     * lleva la suma de puntos de la unidad de construcción según lo seleccionado en en árbol
     */
    private int totalPuntosConstruccion;

//--------------   methods  ------------------------------------------
    public void setTotalPuntosConstruccion(int totalPuntosConstruccion) {
        this.totalPuntosConstruccion = totalPuntosConstruccion;
    }

    public UsoConstruccion getSelectedUsoUnidadConstruccion() {
        return selectedUsoUnidadConstruccion;
    }

    public void setSelectedUsoUnidadConstruccion(UsoConstruccion selectedUsoUnidadConstruccion) {
        this.selectedUsoUnidadConstruccion = selectedUsoUnidadConstruccion;
    }

    public Long getSelectedUsoUnidadConstruccionId() {
        return this.selectedUsoUnidadConstruccionId;
    }
//--------------------------------------------------------------------------------------------------

    public void setSelectedUsoUnidadConstruccionId(Long selectedUsoUnidadConstruccion) {
        this.selectedUsoUnidadConstruccionId = selectedUsoUnidadConstruccion;

        for (UsoConstruccion uc : this.usosConstruccionList) {
            if (uc.getId().longValue() == selectedUsoUnidadConstruccion.longValue()) {
                this.selectedUsoUnidadConstruccion = uc;
                break;
            }
        }
        this.unidadConstruccionSeleccionada.setTipoCalificacion(EUnidadConstruccionTipoCalificacion.
            getCodigoByNombre(this.selectedUsoUnidadConstruccion.getDestinoEconomico()));
    }
//--------------------------------------------------------------------------------------------------    

    public ArrayList<SelectItem> getUsosUnidadConstruccionItemList() {
        return this.usosUnidadConstruccionItemList;
    }

    public void setUsosUnidadConstruccionItemList(
        ArrayList<SelectItem> usosUnidadConstruccionItemList) {
        this.usosUnidadConstruccionItemList = usosUnidadConstruccionItemList;
    }

    public IModeloUnidadConstruccion getUnidadConstruccionSeleccionada() {
        return unidadConstruccionSeleccionada;
    }

    public void setUnidadConstruccionSeleccionada(PUnidadConstruccion unidadConstruccionSeleccionada) {
        this.unidadConstruccionSeleccionada = unidadConstruccionSeleccionada;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * retorna uno de los 3 árboles que se armaron al inicio, dependiendo del tipo de calificación
     * del uso de construcción seleccionado
     */
    public TreeNode getTreeComponentesConstruccion() {
        //this.treeComponentesConstruccion = new DefaultTreeNode("root", null);
        //armarArbolCalificacionConstruccion(this.calificacionConstruccionElements);

        if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) == 0) {
            return this.treeComponentesConstruccion2;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) == 0) {
            return this.treeComponentesConstruccion3;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo()) == 0) {
            return this.treeComponentesConstruccion1;
        } else {
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------

    public void setTreeComponentesConstruccion(TreeNode treeComponentesConstruccion) {
        this.treeComponentesConstruccion = treeComponentesConstruccion;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

//TODO: esto debe haber sido pasado como parámetro de alguna forma
        this.usuario = MenuMB.getMenu().getUsuarioDto();

//TODO de algún lado me llegan los datos número de radicación del trámite,  número predial del predio,
//     id de la unidad de construcción.
        // en el MB se llaman 	private Tramite tramite ;private PPredio predioSeleccionado;
        // PUnidadConstruccion unidadConstruccionSelecionada (?. algo así)
        this.tramite = new Tramite();
        tramite.setId(7l);
        tramite.setNumeroRadicacion("");

        this.predioSeleccionado = new PPredio();
        predioSeleccionado.setId(7l);
        predioSeleccionado.setNumeroPredial("257540216041711057933105117116");
        Departamento depto = new Departamento();
        depto.setNombre("Cundinamarca");
        Municipio muni = new Municipio();
        muni.setNombre("Soacha");
        predioSeleccionado.setDepartamento(depto);
        predioSeleccionado.setMunicipio(muni);

//FIXME la unidad de construcción debe ser pasada como parámetro
        this.unidadConstruccionSeleccionada = new PUnidadConstruccion();

        //FIXME esto es solo para ejemplo, porque este campo no puede ser null en la BD
        this.unidadConstruccionSeleccionada.setTipoCalificacion("1");

        //FIXME esto es solo para ejemplo
        this.unidadConstruccionSeleccionada.setTotalPisosConstruccion(new Integer(1));

        //FIXME esto es solo en pruebas para que no falle la inserción (OJO poner un id que exista)
        this.unidadConstruccionSeleccionada.setId(18l);

        this.usosUnidadConstruccionItemList = new ArrayList<SelectItem>();
        if (this.unidadConstruccionSeleccionada.getUsoConstruccion() != null) {
            this.selectedUsoUnidadConstruccionId =
                this.unidadConstruccionSeleccionada.getUsoConstruccion().getId();
        } else {
            this.usosUnidadConstruccionItemList.add(0, new SelectItem(null, this.DEFAULT_COMBOS));

            //D: si no inicializo esto, en el xhtml no se reconocen las propiedades de este objeto:
            //   al referirme a un atributo que no existe en el objeto, no sale error
            //this.selectedUsoUnidadConstruccion = new UsoConstruccion();
            //... falso. No funcionaba por no usar tipos java simple
        }

        this.usosConstruccionList = this.getConservacionService().obtenerUsosConstruccion();
        this.setUsosUnidadConstruccionItemList(usosConstruccionList);

        //D: armar el TreeNode para mostrar los componentes de construcción
        //D: se arman los tres posibles desde el principio porque si se intenta hacer que en el momento
        //   en que cambie el combo se arme y se refresque el nuevo árbol, no funciona (se hace el re-
        //   resco por cada línea de la tabla que muestra el árbol, y eso daña los valores de la co-
        //   lumna "puntos"
        this.treeComponentesConstruccion =
            armarArbolCalificacionConstruccion(
                this.unidadConstruccionSeleccionada.getTipoCalificacion());

        this.treeComponentesConstruccion2 =
            armarArbolCalificacionConstruccion(
                EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo());
        this.treeComponentesConstruccion3 =
            armarArbolCalificacionConstruccion(
                EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo());
        this.treeComponentesConstruccion1 =
            armarArbolCalificacionConstruccion(
                EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo());

        this.totalPuntosConstruccion = 0;
    }
//--------------------------------------------------------------------------------------------------

    /**
     *
     * @param calificacionConstruccionElements lista de objetos de tipo CalificacionConstruccion
     * obtenida de la consulta a la BD
     *
     * OJO: por el modo en que se arma el árbol (adicionando objetos que se traen de la consulta)
     * hay que hacer la consulta cada vez que se vaya a armar cada uno de los árboles. Si no se hace
     * los árboles quedan con los datos "pegados" del primero
     */
    private TreeNode armarArbolCalificacionConstruccion(String tipoCalificacion) {

        //D: obtener los registros de la tabla que sirve para armar el árbol
        List<CalificacionConstruccion> calificacionConstruccionElements =
            this.getConservacionService().obtenerTodosCalificacionConstruccion();

        TreeNode answer;
        answer = new DefaultTreeNode("root" + tipoCalificacion, null);

        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        TreeNode componenteToAdd;
        String currentComponenteName, tempComponenteName;
        String currentElementoName, tempElementoName;
        CalificacionConstruccion incompleteToAdd;
        CalificacionConstruccionTreeNode ccNode, cctnTemp, cctnTempL2;
        int puntosPorCalificacionConstruccion;
        boolean addedNode = false;

        //D: armado de la columna "componente construcción"
        currentComponenteName = "";
        for (CalificacionConstruccion comp : calificacionConstruccionElements) {
            tempComponenteName = comp.getComponente();

            //D: no se adicionan como nodos los "componente" que no tengan por lo menos un "elemento"
            //   cuyos puntos sean >= 0
            puntosPorCalificacionConstruccion =
                getPuntosPorCalificacionConstruccion(comp, tipoCalificacion);

            if (tempComponenteName.compareToIgnoreCase(currentComponenteName) != 0) {

                if (puntosPorCalificacionConstruccion >= 0) {
                    incompleteToAdd = new CalificacionConstruccion();
                    incompleteToAdd.setComponente(tempComponenteName);
                    ccNode = new CalificacionConstruccionTreeNode();
                    ccNode.setCalificacionConstruccion(incompleteToAdd);
                    componenteToAdd = new DefaultTreeNode("componente", ccNode, answer);

                    addedNode = true;
                } else {
                    addedNode = false;
                }

                currentComponenteName = tempComponenteName;
            } else {
                if (puntosPorCalificacionConstruccion >= 0) {
                    if (!addedNode) {
                        incompleteToAdd = new CalificacionConstruccion();
                        incompleteToAdd.setComponente(tempComponenteName);
                        ccNode = new CalificacionConstruccionTreeNode();
                        ccNode.setCalificacionConstruccion(incompleteToAdd);
                        componenteToAdd = new DefaultTreeNode("componente", ccNode, answer);

                        addedNode = true;
                    }
                }
            }
        }

        //D: armado de los elementos de la columna "elemento calificación"
        currentComponenteName = "";
        currentElementoName = "";
        calificacionConstruccionNodesLevel1 = answer.getChildren();
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
            tempComponenteName = cctnTemp.getCalificacionConstruccion().getComponente();
            if (tempComponenteName.compareToIgnoreCase(currentComponenteName) != 0) {
                for (CalificacionConstruccion comp : calificacionConstruccionElements) {
                    tempElementoName = comp.getElemento();
                    if (tempElementoName.compareToIgnoreCase(currentElementoName) != 0 &&
                        comp.getComponente().compareToIgnoreCase(tempComponenteName) == 0) {

                        puntosPorCalificacionConstruccion =
                            getPuntosPorCalificacionConstruccion(comp, tipoCalificacion);

                        //D: no se adicionan los "elemento" que no apliquen para el "componente";
                        //   es decir, que tengan puntos < 0
                        if (puntosPorCalificacionConstruccion >= 0) {
                            incompleteToAdd = new CalificacionConstruccion();
                            incompleteToAdd.setElemento(tempElementoName);
                            ccNode = new CalificacionConstruccionTreeNode();
                            ccNode.setCalificacionConstruccion(incompleteToAdd);
                            componenteToAdd = new DefaultTreeNode("elemento", ccNode,
                                currentComponenteNode);
                        }

                        currentElementoName = tempElementoName;
                    }
                }
                currentComponenteName = tempComponenteName;
                currentElementoName = "";
            }
        }
        //D: adición de los combos a cada elemento de nivel 2
        ArrayList<CalificacionConstruccion> comboelements;
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
            currentComponenteName = cctnTemp.getCalificacionConstruccion().getComponente();
            calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
                currentElementoName = cctnTempL2.getCalificacionConstruccion().getElemento();
                comboelements = new ArrayList<CalificacionConstruccion>();
                for (CalificacionConstruccion comp : calificacionConstruccionElements) {
                    tempComponenteName = comp.getComponente();
                    tempElementoName = comp.getElemento();
                    if (currentComponenteName.compareToIgnoreCase(tempComponenteName) == 0 &&
                        currentElementoName.compareToIgnoreCase(tempElementoName) == 0) {
                        comboelements.add(comp);
                    }
                }
//cctnTempL2.setTipoCalificacion(this.currentUnidadConstruccion.getTipoCalificacion());
                cctnTempL2.setTipoCalificacion(tipoCalificacion);

                //D: se cambian los elementos mismos (de nivel 2) adicionándoles el combo
                cctnTempL2.armarComboDetalleCalificacion(comboelements);
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    private int getPuntosPorCalificacionConstruccion(
        CalificacionConstruccion calificacionConstruccion,
        String tipoCalificacion) {

        int answer = -1;

        if (tipoCalificacion.compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) == 0) {
            answer = calificacionConstruccion.getPuntosComercial().intValue();
        } else if (tipoCalificacion.compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) == 0) {
            answer = calificacionConstruccion.getPuntosIndustrial().intValue();
        } else if (tipoCalificacion.compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo()) == 0) {
            answer = calificacionConstruccion.getPuntosResidencial().intValue();
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    public int getTotalPuntosConstruccion() {

        int answer = 0;
        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        CalificacionConstruccionTreeNode cctnTempL2;

        if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) == 0) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) == 0) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo()) == 0) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
        }

        calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion.getChildren();
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
                answer += (cctnTempL2.getSelectedDetalle() == null) ? 0 :
                    cctnTempL2.getSelectedDetalle().getPuntos();
            }
        }
        this.totalPuntosConstruccion = answer;

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * OJO: el maldito jsf acepta armar SelectItem con cualquier clase de objeto, pero al hacer la
     * selección, NO FUNCIONA!!. Por lo tanto hay que armarlo con tipo Java simple y hacer doble
     * trabajo de búsqueda.
     *
     * @param usosConstruccionList
     */
    public void setUsosUnidadConstruccionItemList(List<UsoConstruccion> usosConstruccionList) {

        for (UsoConstruccion uc : usosConstruccionList) {
            this.usosUnidadConstruccionItemList.add(new SelectItem(uc.getId(), uc.getNombre()));
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna la tipificación de la unidad de construcción dependiendo del puntaje total y de la
     * clasificación correspondiente en la tabla TIPIFICACION_UNIDAD_CONST
     */
    public String getTipificacionUnidadConstruccion() {
        String answer = "";
        TipificacionUnidadConst tuc;

        tuc = this.getConservacionService().obtenerTipificacionUnidadConstrPorPuntos(
            this.totalPuntosConstruccion);
        answer = tuc.getTipificacion();

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * acción del botón guardar Guarda en la tabla P_UNIDAD_CONSTRUCCION_COMP los valores de la
     * tabla de componente, elemento, detalle, puntos
     *
     */
    public void guardar() {

        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        List<PUnidadConstruccionComp> newUnidadesConstruccionComp;
        PUnidadConstruccionComp toInsert;
        CalificacionConstruccionTreeNode cctnTempL1, cctnTempL2;
        CalificacionConstruccion selectedDetalle;
        String currentComponenteName, currentElementoName;
        Date currentDate;

        if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) == 0) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) == 0) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().compareToIgnoreCase(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo()) == 0) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
        }

        currentDate = new Date();
        newUnidadesConstruccionComp = new ArrayList<PUnidadConstruccionComp>();
        calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion.getChildren();
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {

            cctnTempL1 = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
            currentComponenteName = cctnTempL1.getCalificacionConstruccion().getComponente();

            calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
                currentElementoName = cctnTempL2.getCalificacionConstruccion().getElemento();
                selectedDetalle = cctnTempL2.getSelectedDetalle();

                //D: armar el objeto que se va a insertar...
                toInsert = new PUnidadConstruccionComp();
                toInsert.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);
                toInsert.setComponente(currentComponenteName);
                toInsert.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                toInsert.setElementoCalificacion(currentElementoName);
                toInsert.setDetalleCalificacion(selectedDetalle.getDetalleCalificacion());
                toInsert.setPuntos(new Double(selectedDetalle.getPuntos()));
                toInsert.setFechaLog(currentDate);
//TODO cambiar mensaje de log al terminar pruebas
                toInsert.setUsuarioLog(this.usuario.getLogin() +
                    ": insert en pruebas de gestión detalle construcción.");

                newUnidadesConstruccionComp.add(toInsert);
            }
        }
        LOGGER.debug("insertando registros en P_UNIDAD_CONSTRUCCION_COMP ...");
        this.getConservacionService().insertarPUnidadConstruccionCompRegistros(
            newUnidadesConstruccionComp);
        LOGGER.debug("registros insertados en P_UNIDAD_CONSTRUCCION_COMP.");
    }

}
