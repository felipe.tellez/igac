package co.gov.igac.snc.web.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Tarea que se encarga de finalizar los jobs pendientes de procesar en SNC (Estas tareas son
 * manejadas por un pool de instancias asociados a una cola administrada por spring en la capa web)
 *
 * @author javier.aponte
 *
 */
public class VerificarJobReportesPredialesTask extends AbstractLocator implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = -7332773578521079455L;
    public static Logger LOGGER = LoggerFactory.getLogger(VerificarJobReportesPredialesTask.class);

    private ProductoCatastral job;

    /**
     *
     * @param job a ejecutar
     */
    public VerificarJobReportesPredialesTask(ProductoCatastral job) {
        this.job = job;
    }

    /**
     * Método encargado de enviar la ejecución del job pendiente
     */
    public void run() {
        this.getGeneralesService().verificarEjecucionJobReportesPredialesPendienteEnSNC(job.getId());
    }

}
