package co.gov.igac.snc.web.mb.conservacion.productos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.ProcesoDeProductosCatastrales;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EProductoCatastralEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para la revisión de los productos generados
 * @CU CU-TV-PR-003
 * @author javier.aponte
 */
 
@Component("revisionProductosGenerados")
@Scope("session")
public class RevisionProductosGeneradosMB extends ProcessFlowManager {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -3865342860839390196L;

	private static final Logger LOGGER = LoggerFactory.getLogger(RevisionProductosGeneradosMB.class);
	
	/**
	 * variable que almacena los datos del usuario en sesión
	 */
	private UsuarioDTO usuario;
	
	@Autowired
	private TareasPendientesMB tareasPendientesMB;
	
    /**
     * BPM
     */
    private Actividad currentProcessActivity;  
    
    private long idSolicitudDeTrabajo;
    
    private long idSolicitudSeleccionada;
    
    /**
     * Solicitud seleccionada en el árbol de tareas
     */
    private Solicitud solicitudSeleccionada;
    
    /**
     * Lista de los documentos soportes de los productos catastrales solicitados
     */
    private List<Documento> documentosSoportesPC;
    
    /**
     * Lista que contiene los productos catastrales asociados a la solicitud
     */
    private List<ProductoCatastral> productosCatastralesAsociados;
    
    /**
     * Variable que contiene el producto catastral seleccionado
     */
    private ProductoCatastral productoCatastralSeleccionado;
    
    /**
     * Variable que contiene el producto catastral detalle seleccionado
     */
    private ProductoCatastralDetalle productoCatastralDetalleSeleccionado;
    
    /**
     * Variable que contiene los productos catastrales detalle seleccionados
     */
    private ProductoCatastralDetalle[] productosCatastralesDetalleSeleccionados;
    
    /**
     * Variable para saber si se activa el botón de ver producto
     */
    private boolean activarBotonVerProducto;
    
    /**
     * Variable para saber si se muestra el botón de ingresar observaciones
     */
    private boolean activarBotonIngresarObservaciones;
    
    /**
     * Variable para saber si se muestra el botón de ingresar observaciones
     */
    private boolean activarBotonLimpiarObservaciones;
    
    /**
     * Variable para saber si se muestra el botón de descargar archivo
     */
    private boolean activarBotonDescargarArchivo;
    
    /**
     * Variable para saber si se muestra el botón editor GIS
     */
    private boolean activarBotonEditorGis;
    
    /**
     * Variable para saber si se activa la sección de ingresar observaciones
     */
    private boolean activarPanelIngresarObservaciones;
    
    /**
     * Variable para saber si se activa el botón de enviar a ajustes en la descripción del producto
     */
    private boolean activarBotonEnviarAAjustesDescripcionProducto;
    
    /**
     * Variable para saber si se activa el botón de enviar a ajustes en la solicitud del producto
     */
    private boolean activarBotonEnviarAAjustesSolicitudProducto;
    
     /**
     * Variable para saber si se activa el botón entregar producto en los productos asociados a la solicitud
     */
    private boolean activarBotonEntregarProducto;
    
	/**
	 * Variable encargada de almacenar las personas asociadas a los productos
	 */
	private List<ProductoCatastralDetalle> productosCatastralesDetallesAsociadasAProductos;
	
	/**
	 * Variable donde se almacenan las observaciones al producto generado
	 */
	private String observacionesAProductoGenerado;
	
	 /**
     * Variable para mostrar el aviso cuando la cantidad de productos generados es diferente a la cantidad
     * de productos solicitados
     */
    private String aviso;
    
	/**
	 * Variable para saber que está en el caso de uso 007
	 */
	private boolean isActividadImprimirOExportarProductosGenerados;
	
	/**
	 * Variable para saber que está en el caso de uso 004
	 */
	private boolean isActividadRegistraCorreccionesProductosEntregados;	
	
	/**
	 * Variable para almacenar los criterios de búsqueda de solicitud
	 */
	private FiltroDatosSolicitud filtroDatosSolicitud;
	
	/**
	 * Variable donde se cargan los resultados paginados de la consulta de
	 * solicitudes
	 */
	private List<Solicitud> solicitudesResultado;
	
	
	/**
	 * Lista de rutas de archivos generados del producto catastral
	 */
	private List<ReporteDTO> rutasArchivosAsociadosAProductoCatastralGenerado;
	
	/**
	 * Variable para almacenar la ruta de archivo seleccionada
	 */
	private ReporteDTO archivoProductoCatastralSeleccionada;
	
	/**
	 * Variable para determinar si se muestra la tabla de producto catastral detalle
	 */
	private boolean mostrarTablaProductoCatastralDetalle;
	
	/**
	 * Variable para habilitar el botón de reenvio del job, cuando éste genera
	 * un error.
	 */
	private boolean reintentarEnvioProductoBool;
	
	//--------------Variables para el reporte ----------------------------
	/**
	 * Objeto que contiene los datos del reporte de memorando de comisión
	 */
    private ReporteDTO productoGenerado;
    
    /**
     * Utilidad para la creación del reporte
     */
     private ReportesUtil reportsService = ReportesUtil.getInstance();
     
    
// -----------------------------------MÉTODOS--------------------------------------------------------
	@PostConstruct
	public void init() {

        LOGGER.debug("init revisionProductosGeneradosMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
		
        this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();
        
        // Modificado por leidy.gonzalez 22/09/2014 Se cambia estado inicial para activar botones al ingresar a la pagina
		this.activarBotonIngresarObservaciones = true;
		this.activarBotonLimpiarObservaciones = true;
		
        if(this.currentProcessActivity != null){
        	
			//D: se obtiene el id del trámite
	    	this.idSolicitudDeTrabajo = this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();
	    	
	    	this.solicitudSeleccionada = this.getTramiteService().buscarSolicitudConSolicitantesSolicitudPorId(this.idSolicitudDeTrabajo);
			
			this.documentosSoportesPC = this.getTramiteService().buscarDocumentacionPorSolicitudId(this.idSolicitudDeTrabajo);
			
			this.productosCatastralesAsociados = this.getGeneralesService().buscarProductosCatastralesPorSolicitudId(this.idSolicitudDeTrabajo);
			
			this.isActividadImprimirOExportarProductosGenerados = false;
			if(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_IMPRIMIR_EXPORTAR.equals(this.currentProcessActivity.getNombre())){
				this.isActividadImprimirOExportarProductosGenerados = true;
				this.activarBotonIngresarObservaciones = false;
				this.activarBotonLimpiarObservaciones = false;
			}
			
			this.isActividadRegistraCorreccionesProductosEntregados= false;
			if(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_CORRECCIONES_PRODUCTOS_ENTREGADOS.equals(this.currentProcessActivity.getNombre())){
				this.isActividadRegistraCorreccionesProductosEntregados = true;
			}
			
        }
        //@modified by leidy.gonzalez:: 17844:: 27/07/2016:: Se realiza ajuste al reclama actividad debido a que puede existir
        //mas de un usuario con el rol de control de calidad de productos.
        try {
            this.getProcesosService().reclamarActividad(this.currentProcessActivity.getId(), this.usuario);
        } 
        catch (ExcepcionSNC e) {
            this.addMensajeInfo(e.getMensaje());
        }
		// Modificado por leidy.gonzalez 24/07/2014 Se cambia estado inicial para activar botones al ingresar a la pagina
		this.activarBotonVerProducto = false;
		this.activarBotonDescargarArchivo = false;
		this.activarBotonEditorGis = false;
		this.activarPanelIngresarObservaciones = false;
		this.activarBotonEnviarAAjustesDescripcionProducto = false;
		this.activarBotonEnviarAAjustesSolicitudProducto = false;
		this.mostrarTablaProductoCatastralDetalle = true;
		this.reintentarEnvioProductoBool = false;
		
		this.filtroDatosSolicitud = new FiltroDatosSolicitud();
	}
	//--------------------------------------------------------------------------------------------------
	
	@Override
	public boolean validateProcess() {
		return false;
	}
	
	@Override
	public void setupProcessMessage() {
			
	}
	
	@Override
	public void doDatabaseStatesUpdate() {
	
		for(ProductoCatastral pc : this.productosCatastralesAsociados){
			
			this.getGeneralesService().guardarActualizarProductoCatastralAsociadoASolicitud(pc);
			
    	}
		
	}
	
	//--------------------------------------MÉTODOS-----------------------------------------
	public Solicitud getSolicitudSeleccionada() {
		return solicitudSeleccionada;
	}

	public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
		this.solicitudSeleccionada = solicitudSeleccionada;
	}
	
	public long getIdSolicitudSeleccionada() {
		return idSolicitudSeleccionada;
	}

	public boolean isActividadRegistraCorreccionesProductosEntregados() {
		return isActividadRegistraCorreccionesProductosEntregados;
	}

	public void setActividadRegistraCorreccionesProductosEntregados(
			boolean isActividadRegistraCorreccionesProductosEntregados) {
		this.isActividadRegistraCorreccionesProductosEntregados = isActividadRegistraCorreccionesProductosEntregados;
	}

	public void setIdSolicitudSeleccionada(long idSolicitudSeleccionada) {
		this.idSolicitudSeleccionada = idSolicitudSeleccionada;
	}
	
	public void setActivarBotonEntregarProducto(boolean activarBotonEntregarProducto) {
		this.activarBotonEntregarProducto = activarBotonEntregarProducto;
	}

	public List<Documento> getDocumentosSoportesPC() {
		return documentosSoportesPC;
	}

	public void setDocumentosSoportesPC(List<Documento> documentosSoportesPC) {
		this.documentosSoportesPC = documentosSoportesPC;
	}
	
	public List<ProductoCatastral> getProductosCatastralesAsociados() {
		return productosCatastralesAsociados;
	}

	public void setProductosCatastralesAsociados(
			List<ProductoCatastral> productosCatastralesAsociados) {
		this.productosCatastralesAsociados = productosCatastralesAsociados;
	}
	
	public ProductoCatastral getProductoCatastralSeleccionado() {
		return productoCatastralSeleccionado;
	}

	public void setProductoCatastralSeleccionado(
			ProductoCatastral productoCatastralSeleccionado) {
		this.productoCatastralSeleccionado = productoCatastralSeleccionado;
	}

	public ProductoCatastralDetalle getProductoCatastralDetalleSeleccionado() {
		return productoCatastralDetalleSeleccionado;
	}

	public void setProductoCatastralDetalleSeleccionado(
			ProductoCatastralDetalle productoCatastralDetalleSeleccionado) {
		this.productoCatastralDetalleSeleccionado = productoCatastralDetalleSeleccionado;
	}
	
	public boolean isActivarBotonVerProducto() {
		return activarBotonVerProducto;
	}

	public void setActivarBotonVerProducto(boolean activarBotonVerProducto) {
		this.activarBotonVerProducto = activarBotonVerProducto;
	}
	
	public boolean isActivarBotonIngresarObservaciones() {
		return activarBotonIngresarObservaciones;
	}

	public void setActivarBotonIngresarObservaciones(
			boolean activarBotonIngresarObservaciones) {
		this.activarBotonIngresarObservaciones = activarBotonIngresarObservaciones;
	}
	
	public boolean isActivarBotonLimpiarObservaciones() {
		return activarBotonLimpiarObservaciones;
	}

	public void setActivarBotonLimpiarObservaciones(
			boolean activarBotonLimpiarObservaciones) {
		this.activarBotonLimpiarObservaciones = activarBotonLimpiarObservaciones;
	}
	
	public boolean isActivarBotonDescargarArchivo() {
		return activarBotonDescargarArchivo;
	}

	public void setActivarBotonDescargarArchivo(boolean activarBotonDescargarArchivo) {
		this.activarBotonDescargarArchivo = activarBotonDescargarArchivo;
	}

	public boolean isReintentarEnvioProductoBool() {
		return reintentarEnvioProductoBool;
	}

	public void setReintentarEnvioProductoBool(boolean reintentarEnvioProductoBool) {
		this.reintentarEnvioProductoBool = reintentarEnvioProductoBool;
	}

	public boolean isActivarBotonEditorGis() {
		return activarBotonEditorGis;
	}

	public void setActivarBotonEditorGis(boolean activarBotonEditorGis) {
		this.activarBotonEditorGis = activarBotonEditorGis;
	}
	
	public boolean isActivarPanelIngresarObservaciones() {
		return activarPanelIngresarObservaciones;
	}

	public void setActivarPanelIngresarObservaciones(
			boolean activarPanelIngresarObservaciones) {
		this.activarPanelIngresarObservaciones = activarPanelIngresarObservaciones;
	}
	
	public boolean isActivarBotonEnviarAAjustesDescripcionProducto() {
		return activarBotonEnviarAAjustesDescripcionProducto;
	}

	public void setActivarBotonEnviarAAjustesDescripcionProducto(
			boolean activarBotonEnviarAAjustesDescripcionProducto) {
		this.activarBotonEnviarAAjustesDescripcionProducto = activarBotonEnviarAAjustesDescripcionProducto;
	}

	public List<ProductoCatastralDetalle> getProductosCatastralesDetallesAsociadasAProductos() {
		return productosCatastralesDetallesAsociadasAProductos;
	}

	public void setProductosCatastralesDetallesAsociadasAProductos(
			List<ProductoCatastralDetalle> productosCatastralesDetallesAsociadasAProductos) {
		this.productosCatastralesDetallesAsociadasAProductos = productosCatastralesDetallesAsociadasAProductos;
	}
	
	public List<ReporteDTO> getRutasArchivosAsociadosAProductoCatastralGenerado() {
		return rutasArchivosAsociadosAProductoCatastralGenerado;
	}

	public void setRutasArchivosAsociadosAProductoCatastralGenerado(
			List<ReporteDTO> rutasArchivosAsociadosAProductoCatastralGenerado) {
		this.rutasArchivosAsociadosAProductoCatastralGenerado = rutasArchivosAsociadosAProductoCatastralGenerado;
	}

	public ReporteDTO getArchivoProductoCatastralSeleccionada() {
		return archivoProductoCatastralSeleccionada;
	}

	public void setArchivoProductoCatastralSeleccionada(
			ReporteDTO archivoProductoCatastralSeleccionada) {
		this.archivoProductoCatastralSeleccionada = archivoProductoCatastralSeleccionada;
	}
	
	public boolean isMostrarTablaProductoCatastralDetalle() {
		return mostrarTablaProductoCatastralDetalle;
	}

	public void setMostrarTablaProductoCatastralDetalle(
			boolean mostrarTablaProductoCatastralDetalle) {
		this.mostrarTablaProductoCatastralDetalle = mostrarTablaProductoCatastralDetalle;
	}

	public ReporteDTO getProductoGenerado() {
		return productoGenerado;
	}

	public void setProductoGenerado(ReporteDTO productoGenerado) {
		this.productoGenerado = productoGenerado;
	}
	
	public ProductoCatastralDetalle[] getProductosCatastralesDetalleSeleccionados() {
		return productosCatastralesDetalleSeleccionados;
	}

	public void setProductosCatastralesDetalleSeleccionados(
			ProductoCatastralDetalle[] productosCatastralesDetalleSeleccionados) {
		this.productosCatastralesDetalleSeleccionados = productosCatastralesDetalleSeleccionados;
	}
	
	public String getObservacionesAProductoGenerado() {
		return observacionesAProductoGenerado;
	}

	public void setObservacionesAProductoGenerado(
			String observacionesAProductoGenerado) {
		this.observacionesAProductoGenerado = observacionesAProductoGenerado;
	}
	
	public String getAviso() {
		return aviso;
	}

	public void setAviso(String aviso) {
		this.aviso = aviso;
	}

	public boolean isActivarBotonEnviarAAjustesSolicitudProducto() {
		return activarBotonEnviarAAjustesSolicitudProducto;
	}

	public void setActivarBotonEnviarAAjustesSolicitudProducto(
			boolean activarBotonEnviarAAjustesSolicitudProducto) {
		this.activarBotonEnviarAAjustesSolicitudProducto = activarBotonEnviarAAjustesSolicitudProducto;
	}
	
	

	public boolean isActividadImprimirOExportarProductosGenerados() {
		return isActividadImprimirOExportarProductosGenerados;
	}

	public void setActividadImprimirOExportarProductosGenerados(
			boolean isActividadImprimirOExportarProductosGenerados) {
		this.isActividadImprimirOExportarProductosGenerados = isActividadImprimirOExportarProductosGenerados;
	}
	
	public FiltroDatosSolicitud getFiltroDatosSolicitud() {
		return filtroDatosSolicitud;
	}

	public void setFiltroDatosSolicitud(FiltroDatosSolicitud filtroDatosSolicitud) {
		this.filtroDatosSolicitud = filtroDatosSolicitud;
	}
	
	public List<Solicitud> getSolicitudesResultado() {
		return solicitudesResultado;
	}

	public void setSolicitudesResultado(List<Solicitud> solicitudesResultado) {
		this.solicitudesResultado = solicitudesResultado;
	}

	/**
	 * Método que se ejecuta cuando se oprime el botón de imprimir o exportar productos
	 * @author javier.aponte
	 */
	/*
	 * @modified by leidy.gonzalez:: 17844:: 27/07/2016:: Se ajusta lista de usuarios
	 * con el rol de control de calidad de productos-
	 */
	public String avanzarProcesoAImprimirOExportarProductos(){
		
		String mensaje = null;
		
		this.doDatabaseStatesUpdate();
		
    	//Avanzar el proceso
    	try{
	    	SolicitudProductoCatastral spc = new SolicitudProductoCatastral();
	    	
	    	spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
	    	spc.setIdentificador(this.solicitudSeleccionada.getId());
	    	
	    	Calendar cal = Calendar.getInstance();
	        cal.setTime(this.solicitudSeleccionada.getFecha());
	        spc.setFechaSolicitud(cal);
	    	
	    	spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());
	        
	    	List<UsuarioDTO> controladorDeCalidad = this.obtenerControladorCalidadProductos();
	        
	        //Se valida que exista el controlador de calidad
	        if(controladorDeCalidad == null){
	            this.addMensajeError("No existen usuarios con el rol de control de calidad de productos");
	            return null;
	        }
	    	
	        List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
	        transicionesUsuarios.add(new ActividadUsuarios(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_IMPRIMIR_EXPORTAR, controladorDeCalidad ));
	        spc.setActividadesUsuarios(transicionesUsuarios);
	
	        LOGGER.debug("Avanzando la actividad a imprimir o exportar productos");     
	        
	        this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);
	        
    	}catch(Exception e){
    		mensaje = "Ocurrió un error al avanzar el proceso";
    		LOGGER.error(mensaje+" " + e.getMessage(),e);
    	}
    	
    	if(mensaje==null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        }
        else {
            this.addMensajeError(mensaje);
            return null;
        }
		
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de entregar producto
	 * @author javier.aponte
	 */
	/*
	 * @modified by leidy.gonzalez:: 17844:: 27/07/2016:: Se ajusta lista de
	 * usuarios con el rol de control de calidad de productos
	 */
	public String avanzarProcesoAEntregarProducto(){
		
		String mensaje = null;
		
		this.doDatabaseStatesUpdate();
		
    	//Avanzar el proceso
    	try{
	    	SolicitudProductoCatastral spc = new SolicitudProductoCatastral();
	    	
	    	spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
	    	spc.setIdentificador(this.solicitudSeleccionada.getId());
	    	
	    	Calendar cal = Calendar.getInstance();
	        cal.setTime(this.solicitudSeleccionada.getFecha());
	        spc.setFechaSolicitud(cal);
	    	
	    	spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());
	        
	    	List<UsuarioDTO> controladorDeCalidad = this.obtenerControladorCalidadProductos();
	        
	        //Se valida que exista el controlador de calidad
	        if(controladorDeCalidad == null){
	            this.addMensajeError("No existen usuarios con el rol de control de calidad de productos");
	            return null;
	        }
	    	
	        List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
	        transicionesUsuarios.add(new ActividadUsuarios(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_ENTREGAR_PRODUCTO, controladorDeCalidad ));
	        spc.setActividadesUsuarios(transicionesUsuarios);
	
	        LOGGER.debug("Avanzando la actividad a entregar producto");     
	        
	        this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);
	        
    	}catch(Exception e){
    		mensaje = "Ocurrió un error al avanzar el proceso";
    		LOGGER.error(mensaje+" " + e.getMessage(),e);
    	}
    	
    	if(mensaje==null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        }
        else {
            this.addMensajeError(mensaje);
            return null;
        }
		
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de 
	 * ajustar solicitud de producto
	 * @author javier.aponte
	 * @modified by leidy.gonzalez 22/09/2014 Se cambia el estado del producto para que pueda volver a ejecutarse
	 * al devolverse a la actividad Registrar Datos De Productos
	 */
	public String avanzarProcesoAIngresarSolicitudProductos(){
		
		String mensaje = null;
		
		Pattern patronCodigosRegistrosYResoluciones = Pattern.compile("32|33|1598|1599|35|36|37|1595|38|1600");
	    Matcher matCodigosNoValidarCantidad = patronCodigosRegistrosYResoluciones.matcher(this.productoCatastralSeleccionado.getProducto().getCodigo());
	    
	    if(matCodigosNoValidarCantidad.matches()){
	    	if(this.productosCatastralesAsociados != null){
	    		for (ProductoCatastral pc : this.productosCatastralesAsociados) {
	    			pc.setEstado(EProductoCatastralEstado.SIN_EJECUTAR.getCodigo());
				}
	    	}
	    }
		
		this.doDatabaseStatesUpdate();
		
    	//Avanzar el proceso
    	try{
	    	SolicitudProductoCatastral spc = new SolicitudProductoCatastral();
	    	
	    	spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
	    	spc.setIdentificador(this.solicitudSeleccionada.getId());
	    	
	    	Calendar cal = Calendar.getInstance();
	        cal.setTime(this.solicitudSeleccionada.getFecha());
	        spc.setFechaSolicitud(cal);
	    	
	    	spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());
	        
	    	UsuarioDTO responsableAtencionAlUsuario = this.obtenerResponsableAtencionAlUsuario();
            List<UsuarioDTO> usuariosPotenciales = this.getGeneralesService().
                obtenerFuncionarioTerritorialYRol(this.usuario.getDescripcionUOC(), ERol.RESPONSABLE_ATENCION_USUARIO);
            
            if (usuariosPotenciales == null || usuariosPotenciales.isEmpty()) {
                usuariosPotenciales = this.getGeneralesService().
                    obtenerFuncionarioTerritorialYRol(this.usuario.getDescripcionTerritorial(), ERol.RESPONSABLE_ATENCION_USUARIO);
            }
	        
	        //Se valida que exista el controlador de calidad
	        if(usuariosPotenciales == null || usuariosPotenciales.isEmpty()){
	            this.addMensajeError("No existe usuario con el rol de control de calidad de productos");
	            return null;
	        }
	        
	        ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.addAll(usuariosPotenciales);
	    	
	        List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
	        transicionesUsuarios.add(new ActividadUsuarios(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_INGRESAR_AJUSTAR_SOLICITUD_PRODUCTO, usuarios ));
	        spc.setActividadesUsuarios(transicionesUsuarios);
	
	        LOGGER.debug("Avanzando la actividad a ingresar o ajustar productos trámite catastral");     
	        
	        this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);
	        
    	}catch(Exception e){
    		mensaje = "Ocurrió un error al avanzar el proceso";
    		LOGGER.error(mensaje+" " + e.getMessage(),e);
    	}
    	
    	if(mensaje==null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        }
        else {
            this.addMensajeError(mensaje);
            return null;
        }
		
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de 
	 * ajustes de descripción del producto
	 * @author javier.aponte
	 * @modified by leidy.gonzalez 22/09/2014 Se cambia el estado del producto para que pueda volver a ejecutarse
	 * al devolverse a la actividad Registrar Datos De Productos
	 */
	public String avanzarProcesoARegistrarDatosDeProductos(){
		
		String mensaje = null;
		
		Pattern patronCodigosRegistrosYResoluciones = Pattern.compile("32|33|1598|1599|35|36|37|1595|38|1600");
	    Matcher matCodigosNoValidarCantidad = patronCodigosRegistrosYResoluciones.matcher(this.productoCatastralSeleccionado.getProducto().getCodigo());
	    
	    if(matCodigosNoValidarCantidad.matches()){
	    	if(this.productosCatastralesAsociados != null){
	    		for (ProductoCatastral pc : this.productosCatastralesAsociados) {
	    			pc.setEstado(EProductoCatastralEstado.SIN_EJECUTAR.getCodigo());
				}
	    	}
	    }
    	
		this.doDatabaseStatesUpdate();
		
    	//Avanzar el proceso
    	try{
	    	SolicitudProductoCatastral spc = new SolicitudProductoCatastral();
	    	
	    	spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
	    	spc.setIdentificador(this.solicitudSeleccionada.getId());
	    	
	    	Calendar cal = Calendar.getInstance();
	        cal.setTime(this.solicitudSeleccionada.getFecha());
	        spc.setFechaSolicitud(cal);
	    	
	    	spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());
	        
	    	//UsuarioDTO responsableAtencionAlUsuario = this.obtenerResponsableAtencionAlUsuario();
            
            List<UsuarioDTO> usuariosPotenciales = this.getGeneralesService().
                obtenerFuncionarioTerritorialYRol(this.usuario.getDescripcionUOC(), ERol.RESPONSABLE_ATENCION_USUARIO);
            
            if (usuariosPotenciales == null || usuariosPotenciales.isEmpty()) {
                usuariosPotenciales = this.getGeneralesService().
                    obtenerFuncionarioTerritorialYRol(this.usuario.getDescripcionTerritorial(), ERol.RESPONSABLE_ATENCION_USUARIO);
            }
	        
	        //Se valida que exista el controlador de calidad
	        if(usuariosPotenciales == null || usuariosPotenciales.isEmpty()){
	            this.addMensajeError("No existe usuario con el rol de " + ERol.RESPONSABLE_ATENCION_USUARIO.getRol());
	            return null;
	        }
	        
	        ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
	        usuarios.addAll(usuariosPotenciales);
	    		    	
	        List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
	        transicionesUsuarios.add(new ActividadUsuarios(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_AJUSTAR_PRODUCTOS_TRAMITE_CATASTRAL, usuarios ));
	        spc.setActividadesUsuarios(transicionesUsuarios);
	
	        LOGGER.debug("Avanzando la actividad a registrar ajustar productos trámite catastral");     
	        
	        this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);
	        
    	}catch(Exception e){
    		mensaje = "Ocurrió un error al avanzar el proceso";
    		LOGGER.error(mensaje+" " + e.getMessage());
    	}
    	
    	if(mensaje==null) {
            this.tareasPendientesMB.init();
            return cerrarPaginaPrincipal();
        }
        else {
            this.addMensajeError(mensaje);
            return null;
        }
		
	}
	
	
	/**
	 * Método encargado de definir si se activa el botón de entregar producto en la
	 * pantalla de imprimir/ exportar productos generados 
	 * @author leidy.gonzalez
	 */
	public boolean isActivarBotonEntregarProducto() {
		boolean answer = true;
		if(this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.isEmpty()){
			for(ProductoCatastral pc : this.productosCatastralesAsociados){
				if(pc.getEntregado() != null && ESiNo.SI.getCodigo().equals(pc.getEntregado())){
					return false;
				}
			}
		}
			
		return answer;
	}

	

	/**
	 * Método encargado de definir si se activa el botón de entregar producto en la
	 * pantalla de revisar productos generados
	 * @author javier.aponte
	 */
	public boolean isActivarImprimirOExportarProductos(){
		
		boolean answer = true;
		if(this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.isEmpty()){
			for(ProductoCatastral pc : this.productosCatastralesAsociados){
				if(pc.getRevisionAprobada() == null || ESiNo.NO.getCodigo().equals(pc.getRevisionAprobada())){
					return false;
				}
			}
		}
		return answer;
	}
	
	/**
	 * Método encargado de definir si se activa el botón de enviar a ajustes en la
	 * pantalla de registrar correcciones a productos
	 * @author javier.aponte
	 */
	public boolean isActivarEnviarAAjustes(){
		
		boolean answer = false;
		if(this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.isEmpty()){
			for(ProductoCatastral pc : this.productosCatastralesAsociados){
				if(pc.getRegistraInconformidad() != null && ESiNo.SI.getCodigo().equals(pc.getRegistraInconformidad())){
					return true;
				}
			}
		}
		return answer;
	}
	
	/**
	 * Método encargado de definir si se activa el botón de observaciones de producto
	 * @author javier.aponte
	 */
	public boolean isActivarBotonObservacionesProducto(){
				   		
		boolean answer = false;
		if(this.productoCatastralSeleccionado != null){
			if(this.productoCatastralSeleccionado.getRegistraInconformidad() != null 
					&& ESiNo.SI.getCodigo().equals(this.productoCatastralSeleccionado.getRegistraInconformidad())){
					return true;
				}
			}
		return answer;
	}

	/**
	 * Método que se ejecuta cuando se selecciona un producto catastral
	 * @author javier.aponte
	 */
	public void seleccionarProductoCatastral(){
		
		this.activarBotonVerProducto = true;
		
		this.activarBotonEnviarAAjustesDescripcionProducto = false;
		this.activarBotonEnviarAAjustesSolicitudProducto = false;
		
	}
	
	/**
	 * Método que se ejecuta cuando se desselecciona un producto catastral
	 * @author javier.aponte
	 */
	public void desseleccionarProductoCatastral(){
		
		this.activarBotonVerProducto = false;
		this.activarBotonEnviarAAjustesDescripcionProducto = false;
		this.activarBotonEnviarAAjustesSolicitudProducto = false;
		
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de ver producto
	 * @author javier.aponte
	 */
	public void verProducto(){

		this.productosCatastralesDetallesAsociadasAProductos = this.getGeneralesService().
				buscarProductosCatastralesDetallePorProductoCatastralId(this.productoCatastralSeleccionado.getId());
		
		if(this.productosCatastralesAsociados == null || this.productosCatastralesAsociados.isEmpty()){
			this.addMensajeError("Ocurrió un error al consultar los datos de los productos catastrales asociados");
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			return;
		}else{
			
			boolean returnMessage = false;
			for(ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos){
				
				// Consulta del último ProductoCatastralJob de cada uno de los
				// ProductoCatastralDetalle
				ProductoCatastralJob productoCatastralJob = this.getGeneralesService().
						buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(pcd.getId());

				// Job con error
				if(productoCatastralJob != null &&
						(ProductoCatastralJob.AGS_ERROR.equals(productoCatastralJob.getEstado())
								|| ProductoCatastralJob.JPS_ERROR.equals(productoCatastralJob.getEstado())
								|| ProductoCatastralJob.SNC_ERROR.equals(productoCatastralJob.getEstado()))){
					this.addMensajeError("Ocurrió un error en la generación del producto catastral, por favor intente nuevamente dando clic en el botón 'Reintentar'");
					this.reintentarEnvioProductoBool = true;
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					returnMessage = true;
				}
				
				// Jobs en ejecución
				if(productoCatastralJob != null &&
						(ProductoCatastralJob.AGS_EN_EJECUCION.equals(productoCatastralJob.getEstado())
								|| ProductoCatastralJob.JPS_EN_EJECUCION.equals(productoCatastralJob.getEstado())
								|| ProductoCatastralJob.SNC_EN_EJECUCION.equals(productoCatastralJob.getEstado()))){
					this.addMensajeWarn("El producto catastral solicitado aún se encuentra generandose, por favor intente más tarde");
					this.reintentarEnvioProductoBool = false;
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					returnMessage = true;
				}
			}
			
			if(returnMessage){
				return;
			}
			
			this.productoCatastralSeleccionado.setActivarRevisionAprobada(true);
			for(ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos){
				if(pcd.getObservaciones() != null && !pcd.getObservaciones().trim().isEmpty()){
					this.productoCatastralSeleccionado.setActivarRevisionAprobada(false);
					break;
				}
			}
			
			//Codigos de los productos asociados a registros prediales
			Pattern patronCodigosNoValidarCantidad = Pattern.compile("32|33|1598|1599|36|37|1595|38|6|8");
		    Matcher matCodigosNoValidarCantidad = patronCodigosNoValidarCantidad.matcher(this.productoCatastralSeleccionado.getProducto().getCodigo());
		    
			if(this.productosCatastralesDetallesAsociadasAProductos.size() != this.productoCatastralSeleccionado.getCantidad()
					&& !matCodigosNoValidarCantidad.matches()){
				this.aviso= "La cantidad de productos solicitados en el(los) documento(s)" +
						"soporte(s) es diferente a la cantidad de los productos generados";
				this.activarBotonEnviarAAjustesSolicitudProducto = true;
			}else{
				this.aviso = "";
			}
			
		}
		
		this.observacionesAProductoGenerado = new String();
		
		if(this.productoCatastralSeleccionado.getRutaProductoEjecutado() != null){
			
			this.mostrarTablaProductoCatastralDetalle = false;
			
			String rutas [] = this.productoCatastralSeleccionado.getRutaProductoEjecutado().split(Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO);
			ReporteDTO archivoAsociadoAProducto;
			this.rutasArchivosAsociadosAProductoCatastralGenerado = new ArrayList<ReporteDTO>();
			for(String ruta : rutas){
				archivoAsociadoAProducto = this.reportsService.descargarArchivoDeFtp(ruta);
				this.rutasArchivosAsociadosAProductoCatastralGenerado.add(archivoAsociadoAProducto);
			}
		}else{//v1.1.9
			this.productoCatastralSeleccionado.setRutaProductoEjecutado(null);
			this.rutasArchivosAsociadosAProductoCatastralGenerado = null;
		}
	}
	
	/**
	 * Método encargado de generar el producto para revisar
	 * @author javier.aponte
	 */
	public void generarProductoAsociado(){
		
		Pattern patronCertificadoPlano = Pattern.compile("30");
		Matcher matCodigosCertificadoPlano = patronCertificadoPlano.matcher(this.productoCatastralSeleccionado.getProducto().getCodigo());
		
		
		if(this.isActividadImprimirOExportarProductosGenerados){
				this.productoGenerado = this.reportsService.consultarReporteDeGestorDocumental(this.productoCatastralDetalleSeleccionado.getRutaProductoEjecutado());
			}else{//v1.1.9
				if(this.productoCatastralDetalleSeleccionado != null && this.productoCatastralDetalleSeleccionado.getRutaProductoEjecutado() != null
						&& this.productoCatastralDetalleSeleccionado.getRutaProductoEjecutado().contains(Constantes.GENERAR_PRODUCTO_EN_PDF)){
					this.productoGenerado = this.reportsService.consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(this.productoCatastralDetalleSeleccionado.getRutaProductoEjecutado());
				}else{
					this.productoGenerado = this.reportsService.consultarReporteDeGestorDocumental(this.productoCatastralDetalleSeleccionado.getRutaProductoEjecutado());
				}
			}
			
			if(this.productoGenerado == null){
				this.productoGenerado = new ReporteDTO();
				this.productoGenerado.setUrlWebReporte(this.productoCatastralDetalleSeleccionado.getRutaProductoEjecutado());
			}
	}
	
	/**
	 * Método que se ejecuta cuando se desselecciona una fila de la pantalla de 
	 * archivos asociados a productos
	 * @author javier.aponte  
	 * @modified by leidy.gonzalez 24/07/2014 Se elimina desactivación de botones
	 * para que no haya la necesidad de seleccionar el producto para agregar observaciones.
	 */
	public void seleccionarArchivoAsociadoAProductoCatastral(){
		
		if(this.productosCatastralesDetalleSeleccionados != null && this.productosCatastralesDetalleSeleccionados.length > 0){
			this.activarBotonIngresarObservaciones = true;
			
			for(ProductoCatastralDetalle pcd : this.productosCatastralesDetalleSeleccionados){
				if(pcd.getObservaciones() != null && !pcd.getObservaciones().isEmpty()){
					this.activarBotonLimpiarObservaciones = true;
					break;
				}
			}
			
		}
		
	}
	
	/**
	 * Método que se ejecuta cuando se desselecciona una fila de la pantalla de 
	 * archivos asociados a productos
	 * @author javier.aponte  
	 */
	public void desseleccionarArchivoAsociadoAProductoCatastral(){
		
		this.activarBotonIngresarObservaciones = false;
		this.activarBotonLimpiarObservaciones = false;
		
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de ingresar observaciones
	 * @author javier.aponte
	 */
	public void ingresarObservaciones(){
		
		this.activarPanelIngresarObservaciones = true;
		
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de limpiar observaciones
	 * @author javier.aponte
	 * @modified by leidy.gonzalez 24/07/2014 Se agrega validación para cuando el producto es un registro predial
	 * y este ya se haya generado pueda limpiar las observaciones agregadas al producto catastral.
	 */
	public void limpiarObservaciones(){
		
		boolean registrarActivaNoInconformidades = true;
		
		if(this.productosCatastralesDetalleSeleccionados != null && this.productosCatastralesDetalleSeleccionados.length > 0){
			
			for(ProductoCatastralDetalle pcd : this.productosCatastralesDetalleSeleccionados){
				pcd.setObservaciones("");
				this.getGeneralesService().guardarProductoCatastralDetalle(pcd);
			}
			
			this.productoCatastralSeleccionado.setActivarRevisionAprobada(true);
			for(ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos){
				if(pcd.getObservaciones() != null && !pcd.getObservaciones().trim().isEmpty()){
					this.productoCatastralSeleccionado.setActivarRevisionAprobada(false);
					registrarActivaNoInconformidades = false;
					break;
				}
			}
			
		}else if(this.productoCatastralSeleccionado != null){
			
			this.productoCatastralSeleccionado.setObservaciones("");
			this.productoCatastralSeleccionado.setActivarRevisionAprobada(true);

			this.activarBotonEnviarAAjustesDescripcionProducto = false;
			this.activarBotonEnviarAAjustesSolicitudProducto=false;
		}
		
		if(registrarActivaNoInconformidades==true){
			productoCatastralSeleccionado.setRegistraInconformidad(ESiNo.NO.getCodigo());
		}
		
		this.doDatabaseStatesUpdate();
		
		this.limpiarCamposListaArchivosAsdociadosPorducto();
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de aceptar en el fieldset de ingresar observaciones
	 * @author javier.aponte
	 * @modified by leidy.gonzalez Se habilita boton Enviar Ajustes de la Solicitud del producto cuando se ingresan observaciones al producto.
	 * @modified by leidy.gonzalez 24/07/2014 Se agrega validación para cuando el producto es un registro predial
	 * y este ya se haya generado pueda agregar las observaciones al producto catastral.
	 * @modified by leidy.gonzalez 04/08/2014 Se elimina inicializacion de variable observacionesAProductoGenerado.
	 */
	public void aceptarIngresarObservacionesProductoCatastralDetalle(){
		
		if(this.productosCatastralesDetalleSeleccionados != null && this.productosCatastralesDetalleSeleccionados.length > 0){
			
			for(ProductoCatastralDetalle pcd : this.productosCatastralesDetalleSeleccionados){
				pcd.setObservaciones(this.observacionesAProductoGenerado);
				this.getGeneralesService().guardarProductoCatastralDetalle(pcd);
			}
			
		}else if(this.productoCatastralSeleccionado != null){
			
			this.productoCatastralSeleccionado.setObservaciones(this.observacionesAProductoGenerado);
		}
		
		if(this.observacionesAProductoGenerado != null && !this.observacionesAProductoGenerado.trim().isEmpty()){ 
			this.productoCatastralSeleccionado.setActivarRevisionAprobada(false);
			this.productoCatastralSeleccionado.setRevisionAprobadaBool(false);
			this.productoCatastralSeleccionado.setRegistraInconformidad(ESiNo.SI.getCodigo());
		}
		this.isActividadRegistraCorreccionesProductosEntregados = true;
		this.activarPanelIngresarObservaciones = false;
		this.activarBotonEnviarAAjustesDescripcionProducto = true;
		this.activarBotonEnviarAAjustesSolicitudProducto=true;
		
		this.doDatabaseStatesUpdate();
		
		this.limpiarCamposListaArchivosAsdociadosPorducto();
	}
	
	/**
	 * Método encargado de limpiar los campos al ingresar o limpar observaciones
	 * a los productos asociados a la solicitud
	 * @author leidy.gonzalez
	 */
	public void limpiarCamposListaArchivosAsdociadosPorducto(){
		
		this.productosCatastralesDetalleSeleccionados = null;
		this.productoCatastralDetalleSeleccionado = new ProductoCatastralDetalle();
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de imprimir archivo en el caso
	 * de uso de imprimir o exportar producto generado
	 * @author javier.aponte
	 */
	public void imprimirArchivoProductoAsociado(){
		
	}
	
	// ------------------------------------------------------------------------
	/**
	 * Método encargado de buscar solicitudes por filtro, ejecutado en la acción del
	 * botón buscar
	 * 
	 * @author javier.aponte
	 */
	@SuppressWarnings("serial")
	public void buscarSolicitudesPorFiltro() {
		
		List<Solicitud> respuesta;
		
		if(this.solicitudesResultado == null){
			this.solicitudesResultado = new ArrayList<Solicitud>();
		}
		
		if(!this.solicitudesResultado.isEmpty()){
			this.solicitudesResultado.clear();
		}
		
		
		this.filtroDatosSolicitud.setTipo(ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo());
				
		respuesta = this.getTramiteService().buscarSolicitudes(filtroDatosSolicitud, 0, 0);
		
		if(respuesta != null && !respuesta.isEmpty()){

			Map<EParametrosConsultaActividades, String> filtros = 
	                new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
	        filtros.put(EParametrosConsultaActividades.NOMBRE_ACTIVIDAD, ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_CORRECCIONES_PRODUCTOS_ENTREGADOS);
	        filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
	        
	        List<Actividad> actividades = null;
	        
	        try {
	            actividades = this.getProcesosService().consultarListaActividades(filtros);
	            
	            if(actividades != null && !actividades.isEmpty()){
	            	
	            	for(Solicitud solTemp : respuesta){
	            		for(Actividad actTemp: actividades){
	            		
	            			if(solTemp.getId().equals(actTemp.getIdObjetoNegocio())){
	            				this.solicitudesResultado.add(solTemp);
	            			}
	            			
	            		}
	            	}
	            	
	            }
	        }catch(ExcepcionSNC ex) {
	        	LOGGER.error("Error consultando la lista de actividades por actividad: " + 
	            		this.solicitudSeleccionada.getNumero(), ex);
	        }
		}

	}
	
	/**
	 * Accion ejecutada sobre el radio button asociado al tipo de persona
	 * @author javier.aponte
	 */
	public void tipoPersonaSel() {
		this.filtroDatosSolicitud.setTipoIdentificacion(null);
		if(this.filtroDatosSolicitud.isPersonaJuridica()){
			this.filtroDatosSolicitud.setTipoIdentificacion(EPersonaTipoIdentificacion.NIT.getCodigo());
		}
	}
	
	/**
	 * Método que se ejecuta cuando se oprime el botón de revisar solicitud
	 * @author javier.aponte
	 * @modified by leidy.gonzalez Se agrega validacion para cuando no se selecciona la solicitud a revisar.
	 * @modified by leidy.gonzalez 22/09/2014 Se agregan nuevos filtros para realizar la consulta de lista de actividades
	 */
	public String revisarSolicitud(){
		
		if (this.solicitudSeleccionada == null) {
			
			this.addMensajeError("Debe seleccionar una solicitud para ser revisada");
            return null;

		} else {

			this.documentosSoportesPC = this.getTramiteService()
					.buscarDocumentacionPorSolicitudId(
							this.solicitudSeleccionada.getId());

			this.productosCatastralesAsociados = this.getGeneralesService()
					.buscarProductosCatastralesPorSolicitudId(
							this.solicitudSeleccionada.getId());

			Map<EParametrosConsultaActividades, String> filtros = new EnumMap<EParametrosConsultaActividades, String>(
					EParametrosConsultaActividades.class);
			filtros.put(EParametrosConsultaActividades.ID_PRODUCTOS_CATASTRALES, this.solicitudSeleccionada.getId().toString());
			filtros.put(EParametrosConsultaActividades.NUMERO_SOLICITUD,this.solicitudSeleccionada.getNumero());
			filtros.put(EParametrosConsultaActividades.ESTADOS,EEstadoActividad.TAREAS_VIGENTES);
			filtros.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, this.usuario.getLogin());

			List<Actividad> actividades = null;

			try {
				actividades = this.getProcesosService()
						.consultarListaActividades(filtros);

				if (actividades != null && !actividades.isEmpty()) {
					this.currentProcessActivity = actividades.get(0);
				}
			} catch (ExcepcionSNC ex) {
				LOGGER.error("Error consultando la lista de actividades por el número de solicitud: "
						+ this.solicitudSeleccionada.getNumero(), ex);
			}
		}
		
		return ConstantesNavegacionWeb.REGISTRAR_CORRECCIONES_A_PRODUCTO;
	}
	
	//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar"
     * Se debe forzar el init del MB porque de otro modo no se refrezcan los datos 
     */
    public String cerrarPaginaPrincipal() {
    	
    	//D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
    	UtilidadesWeb.removerManagedBean("revisionProductosGenerados");
    	
		//D: forzar el refrezco del árbol de tareas
		this.tareasPendientesMB.init();
    	
    	return ConstantesNavegacionWeb.INDEX;
    }
    
    /**
     * Método encargado de obtener el responsable de atención al usuario
     * @author javier.aponte
     * @modified leidy.gonzalez 04/08/2014 Se cambia el tipo de filtro
     * para realizar la consulta de lista de actividades en process
     */
    private UsuarioDTO obtenerResponsableAtencionAlUsuario(){
    	
        UsuarioDTO responsableAtencionAlUsuario = null;
        
        String loginResponsableAtencionAlUsuario = null;
        
        
        Map<EParametrosConsultaActividades, String> filtros = 
                new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        filtros.put(EParametrosConsultaActividades.ID_PRODUCTOS_CATASTRALES, this.solicitudSeleccionada.getId().toString());
        filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
        
        List<Actividad> actividades = null;
        
        try {
            actividades = this.getProcesosService().consultarListaActividades(filtros);
            
            if(actividades != null && !actividades.isEmpty()){
            	
            	for(Actividad a : actividades){
            		if(ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_AJUSTAR_PRODUCTOS_TRAMITE_CATASTRAL.equals(a.getNombre())){
            			loginResponsableAtencionAlUsuario = a.getUsuarioEjecutor();
            			break;
            		}
            	}
            }
        }
        catch(ExcepcionSNC ex) {
        	LOGGER.error("Error consultando la lista de actividades por el número de solicitud: " + 
            		this.solicitudSeleccionada.getNumero(), ex);
        }
        
        if(loginResponsableAtencionAlUsuario != null){
        	responsableAtencionAlUsuario = this.getGeneralesService().getCacheUsuario(loginResponsableAtencionAlUsuario);
        }
        return responsableAtencionAlUsuario;
    }
    
    
    /**
     * Método encargado de buscar los productos asociados a la solicitud por id de solicitud
     * @author leidy.gonzalez 
     */
	public void buscarProductosPorIdSolicitud(){
		this.productosCatastralesAsociados = this.getGeneralesService().buscarProductosCatastralesPorSolicitudId(this.idSolicitudSeleccionada);
		
	}
	
	/**
	 * Método que realiza el envió de un nuevo job para la generación del
	 * producto.
	 * 
	 * @author david.cifuentes
	 */
	public void reintentarCreacionProducto() {
		
		ReporteDTO producto;
		String rutaProducto;
		
		Pattern patronCertificadosCatastralesNacionales;
	    Matcher matCertificadosCatastralesNacionales;
	    
	    Pattern patronCertificadoPlanoPredialCatastral;
	    Matcher matCertificadoPlanoPredialCatastral;
	    
	    Pattern patronFichaPredialDigital;
	    Matcher matFichaPredialDigital;
	    
	    Pattern patronCartaCatastralUrbana;
	    Matcher matCartaCatastralUrbana;
	    
	    EReporteServiceSNC enumeracionProducto = null;
	    Map<String, String> parameters;
	    
	    //Códigos de los productos asociados a certificados catastrales nacionales
	    patronCertificadosCatastralesNacionales = Pattern.compile("25|26|27|28");
	    
	    //Código del certificado plano predial catastral 
	    patronCertificadoPlanoPredialCatastral = Pattern.compile("30");
	    
	    //Código de la ficha predial digital
	    patronFichaPredialDigital = Pattern.compile("20");
	    
	    //Código de la carta catastral urbana
	    patronCartaCatastralUrbana = Pattern.compile("5");
	    
	    this.productosCatastralesAsociados = this.getGeneralesService().
                    buscarProductosCatastralesPorSolicitudId(this.solicitudSeleccionada.getId());
            
            this.productosCatastralesDetallesAsociadasAProductos = this.getGeneralesService().
				buscarProductosCatastralesDetallePorProductoCatastralId(this.productoCatastralSeleccionado.getId());
	    
	    if(this.productosCatastralesAsociados != null){
                
                for (ProductoCatastralDetalle pcd : this.productosCatastralesDetallesAsociadasAProductos) {

                    // Consulta del último ProductoCatastralJob de cada uno de los
                    // ProductoCatastralDetalle
                    ProductoCatastralJob job = this.getGeneralesService().
                            buscarUltimoProductoCatastralJobPorProductoCatastralDetalleId(pcd.getId());

                        this.getGeneralesService().reenviarJobsProductosCatastrales(job, job.getEstado(), job.getResultado());
                }
            }else{
                
		    //Llama la generación de todos los productos catastrales
			for(ProductoCatastral pcTemp : this.productosCatastralesAsociados){
					
			    matCertificadosCatastralesNacionales = patronCertificadosCatastralesNacionales.matcher(pcTemp.getProducto().getCodigo());
			    
			    matFichaPredialDigital = patronFichaPredialDigital.matcher(pcTemp.getProducto().getCodigo());
			    
			    matCertificadoPlanoPredialCatastral = patronCertificadoPlanoPredialCatastral.matcher(pcTemp.getProducto().getCodigo());
			    
			    matCartaCatastralUrbana = patronCartaCatastralUrbana.matcher(pcTemp.getProducto().getCodigo());
			    
			    this.productosCatastralesDetallesAsociadasAProductos = this.getGeneralesService().
		    			buscarProductosCatastralesDetallePorProductoCatastralId(pcTemp.getId());
				    
				    //Los certificados catastrales son productos que se envían a generar de manera sincronica, porque se generan muy rápido
				    if (matCertificadosCatastralesNacionales.matches()) {
				    	
				    	for(ProductoCatastralDetalle pcdTemp : this.productosCatastralesDetallesAsociadasAProductos){
				    		
				    		if(pcdTemp.getNumeroPredial() != null){
						    	enumeracionProducto = EReporteServiceSNC.
						    			CERTIFICADO_NACIONAL_POR_PROPIETARIO_POR_PREDIO;
				    		}else if(pcdTemp.getNumeroIdentificacion() != null){
				    			enumeracionProducto = EReporteServiceSNC.
						    			CERTIFICADO_NACIONAL_POR_PROPIETARIO_POR_PERSONA;
				    		}
				    		
				    		parameters = new HashMap<String, String>();
				            
				    		parameters.put("PRODUCTO_CATASTRAL_DETALLE_ID", String.valueOf(pcdTemp.getId()));
				            	
				            producto = reportsService.generarReporte(parameters, 
				                            enumeracionProducto, this.usuario);
				            
							rutaProducto = this.getGeneralesService().guardarProductoGeneradoEnGestorDocumental(
									producto.getRutaCargarReporteAAlfresco(),
									this.usuario.getCodigoEstructuraOrganizacional());
				            
				            if(producto != null){
				            	pcdTemp.setRutaProductoEjecutado(rutaProducto);
				            	
				            	this.getGeneralesService().guardarProductoCatastralDetalle(pcdTemp);
				            }
				    	}
					}
				    
				    //Generar la ficha predial digital
				    if (matFichaPredialDigital.matches()) {
				    	for(ProductoCatastralDetalle pcdTemp : this.productosCatastralesDetallesAsociadasAProductos){
				    		this.getGeneralesService().generarFichaPredialDigitalProductos(pcdTemp, this.usuario);
				    	}
				    }
				    
				    //Generar el certificado plano predial catastral
				    if (matCertificadoPlanoPredialCatastral.matches()) {
				    	for(ProductoCatastralDetalle pcdTemp : this.productosCatastralesDetallesAsociadasAProductos){
				    		this.getGeneralesService().generarCertificadoPlanoPredialCatastralProductos(pcdTemp, this.usuario);
				    	}
				    }
				    
				    //Generar la carta catastral urbana
				    if (matCartaCatastralUrbana.matches()) {
				    	for(ProductoCatastralDetalle pcdTemp : this.productosCatastralesDetallesAsociadasAProductos){
				    		this.getGeneralesService().generarCartaCatastralUrbanaProductos(pcdTemp, this.usuario);
				    	}
				    }
			}
			this.addMensajeInfo("Se realizó el reenvio de la solicitud para generar el producto catastral, el proceso puede tardar algunos minutos.");
		}
	    this.reintentarEnvioProductoBool = false;
	}
	
	/**
     * Método encargado de obtener el controlado de calidad de productos
     * @author leidy.gonzalez
     */
    private List<UsuarioDTO> obtenerControladorCalidadProductos(){
    	
        List<UsuarioDTO> controladoresCalidadProductos = new ArrayList<UsuarioDTO>();
        
        if(this.usuario.getDescripcionUOC()!=null){
        	controladoresCalidadProductos = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
							usuario.getDescripcionUOC(),
							ERol.CONTROL_CALIDAD_PRODUCTOS);
        }

        //si no existe Control calidad productos en la UOC se buscan en la territorial.
        if(controladoresCalidadProductos.isEmpty()){
        	controladoresCalidadProductos = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
							usuario.getDescripcionTerritorial(),
							ERol.CONTROL_CALIDAD_PRODUCTOS);
        }
        
        if(controladoresCalidadProductos != null && !controladoresCalidadProductos.isEmpty()){
        	return controladoresCalidadProductos;
        }
        
        return null;
    	
    }

//end of class
}
