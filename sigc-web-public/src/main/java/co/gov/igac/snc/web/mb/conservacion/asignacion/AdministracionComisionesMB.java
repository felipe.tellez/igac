/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.time.DateUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Managed bean para el caso de uso Administrar Comisiones
 *
 * Usuario: Responsable de conservación a nivel de la territorial, director territorial
 *
 * @author pedro.garcia
 * @cu CU-CO-A-07
 */
@Component("administracionComisiones")
@Scope("session")
public class AdministracionComisionesMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdministracionComisionesMB.class);

    /**
     * Variable que almacena los datos del usuario.
     */
    private UsuarioDTO usuario;

    // ------------------ services ------
    @Autowired
    private GeneralMB generalMBService;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * managed bean para pintar los trámites de una comisión y permitir el retiro de éstos
     */
    @Autowired
    private TramitesDeComisionMB tramitesDeComisionMB;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO loggedInUser;

    /**
     * territorial del usuario de la sesión
     */
    private String idTerritorial;

    /**
     * variable donde se cargan las comisiones sobre las que se trabaja
     */
    private LazyDataModel<VComision> lazyWorkingComisiones;

    /**
     * comisión seleccionada de la tabla
     */
    private VComision selectedComision;

    /**
     * Documento correspondiente al memorando del estado de la comisión en el histórico. Es el
     * contenido en el objeto ComisionEstado
     */
    private Documento selectedComisionEstadoDocumento;

    /**
     * Lista con los registros de los estados que ha tenido la comisión
     */
    private ArrayList<ComisionEstado> historicoEstadosComision;

    /**
     * objeto en el que se guardan los valores relacionados con el nuevo estado que adquiere la
     * comisión
     */
    private ComisionEstado nuevoEstadoComision;

    /**
     * nombre de la eventualidad que se va a manejar, y que se pasa a la pantalla donde se dan los
     * detalles
     */
    private String nombreEventualidad;

    /**
     * Lista de items los motivos de la eventualidad de la comisión
     */
    private List<SelectItem> motivosEventualidadItemList;

    /**
     * mensaje que se muestra al dar click en alguno de los botones de la forma principal
     */
    private String mensajeConfirmacion;

    /**
     * número de días en que se amplía una comisión
     */
    private int numeroDiasAmpliacion;

    /**
     * label y texto que se deben mostrar en la pantalla de reactivación de la comisión para mostrar
     * la fecha del último cambio de estado de la comisión
     */
    private String labelFechaUltimoEstado;
    private Date fechaUltimoEstado;

    /**
     * lista de ejecutores de la comisión seleccionada
     */
    private List<VEjecutorComision> ejecutoresComision;

    //-------------   para correos -----
    /**
     * Asunto y contenido del correo que se envía a los ejecutores asociados a la comisión. Se
     * define aquí para poder modificarla dependiendo de la acción tomada.
     */
    private String contenidoCorreoEjecutores;

    private String contenidoCorreoDirectorTerritorial;

    private String contenidoCorreoEjecutoresRetiroTrams;

    private String asuntoCorreoEjecutores;

    private String asuntoCorreoDirectorTerritorial;

    private String accionTomadaSobreComision;

    // ------------ banderas
    /**
     * para diferenciar el tipo de ususrio que ejecuta el caso de uso
     */
    private boolean esUsuarioResponsableConservacion;
    private boolean esUsuarioDirectorTerritorial;

    /**
     * para definir si los botones correspondiente se habilitan o no
     */
    private boolean enabledSuspensionButton;

    private boolean enabledCancelationButton;

    private boolean enabledPostponementButton;

    private boolean enabledReactivationButton;

    private boolean enabledExtensionButton;

    /**
     * indica si es suspensión o aplazamiento de la comisión
     */
    private boolean esSuspAplaz;

    /**
     * para definir si le eventualidad debe tener una fecha final
     */
    private boolean eventualidadIndefinida;

    /**
     * para determinar si el campo 'descripción del motivo es obligatorio'. Lo es cuando el motivo
     * es 'otro'
     */
    private boolean descripcionMotivoRequerida;

    //----------------------------
    /**
     * define la acción que se va a hacer. Se usa porque la ventana de confirmación tiene como
     * parámetro el método que se va a ejecutar en el OK, pero -debido a que fue definido como un
     * método en el CC- debe recibir un método a ajecutar, y la única forma de saber qué método
     * tendría que ejecutar es haciendo que tal método retornara el método (no el nombre). ¿se
     * puede?
     *
     * 1 = suspender 2 = cancelar 3 = aplazar 4 = reactivar 5 = ampliar
     */
    private int actionToDo;

    /**
     * Variable usada para almacenar la lista de directores de la territorial.
     */
    private List<UsuarioDTO> directoresTerritoriales;

    /**
     * tipo de usuario autenticado. Se usa para distinguir entre los dos roles que pueden tener
     * acceso a este caso de uso.
     */
    private String tipoUsuarioAutenticado;

    // ------ constantes
    private static String NUEVASFECHASERROR1 =
        "Le nueva fecha final debe ser mayor o igual que la " +
         "nueva fecha inicial";

    // ----------- para reportes
    private String numeroRadicacionMemorando;

    /**
     * Variable que indica si el reporte que se está generando es el memorando de comisión desde el
     * histórico, ó es el reporte que se está generando del estado de comisión
     */
    private boolean reporteMemorandoHistorico;

    /**
     * objeto Documento que se guarda en la base de datos
     */
    private Documento documentoMemorando;

    /**
     * id de tipo de documento que va a tener el memorando generado según la acción que se tome
     * sobre la comisión
     */
    private Long memorandoTipoDocumentoId;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteMemorandoComision;

    /**
     * usuario director territorial
     */
    private UsuarioDTO usuarioDirectorTerritorial;

    /**
     * usuario responsable de conservación
     */
    private UsuarioDTO usuarioResponsableConservacion;

// ---------- ------------------        methods      ------------------------
    public boolean isEsUsuarioResponsableConservacion() {
        return this.esUsuarioResponsableConservacion;
    }

    public void setEsUsuarioResponsableConservacion(boolean esUsuarioResponsableConservacion) {
        this.esUsuarioResponsableConservacion = esUsuarioResponsableConservacion;
    }

    public boolean isEsUsuarioDirectorTerritorial() {
        return this.esUsuarioDirectorTerritorial;
    }

    public void setEsUsuarioDirectorTerritorial(boolean esUsuarioDirectorTerritorial) {
        this.esUsuarioDirectorTerritorial = esUsuarioDirectorTerritorial;
    }

    public String getTipoUsuarioAutenticado() {
        return this.tipoUsuarioAutenticado;
    }

    public void setTipoUsuarioAutenticado(String tipoUsuarioAutenticado) {
        this.tipoUsuarioAutenticado = tipoUsuarioAutenticado;
    }

    public UsuarioDTO getLoggedInUser() {
        return this.loggedInUser;
    }

    public void setLoggedInUser(UsuarioDTO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public List<UsuarioDTO> getDirectoresTerritoriales() {
        return this.directoresTerritoriales;
    }

    public void setDirectoresTerritoriales(List<UsuarioDTO> directoresTerritoriales) {
        this.directoresTerritoriales = directoresTerritoriales;
    }

    public Documento getSelectedComisionEstadoDocumento() {
        return this.selectedComisionEstadoDocumento;
    }

    public void setSelectedComisionEstadoDocumento(
        Documento selectedComisionEstadoDocumento) {
        this.selectedComisionEstadoDocumento = selectedComisionEstadoDocumento;
    }

    public ArrayList<ComisionEstado> getHistoricoEstadosComision() {
        return this.historicoEstadosComision;
    }

    public void setHistoricoEstadosComision(
        ArrayList<ComisionEstado> historicoEstadosComision) {
        this.historicoEstadosComision = historicoEstadosComision;
    }

    public ReporteDTO getReporteMemorandoComision() {
        return reporteMemorandoComision;
    }

    public void setReporteMemorandoComision(ReporteDTO reporteMemorandoComision) {
        this.reporteMemorandoComision = reporteMemorandoComision;
    }

    public boolean isDescripcionMotivoRequerida() {
        return this.descripcionMotivoRequerida;
    }

    public void setDescripcionMotivoRequerida(boolean descripcionMotivoRequerida) {
        this.descripcionMotivoRequerida = descripcionMotivoRequerida;
    }

    public boolean isEsSuspAplaz() {
        return this.esSuspAplaz;
    }

    public void setEsSuspAplaz(boolean esSuspAplaz) {
        this.esSuspAplaz = esSuspAplaz;
    }

    public String getNombreEventualidad() {
        return this.nombreEventualidad;
    }

    public void setNombreEventualidad(String nombreEventualidad) {
        this.nombreEventualidad = nombreEventualidad;
    }

    public boolean isEnabledSuspensionButton() {
        return this.enabledSuspensionButton;
    }

    public void setEnabledSuspensionButton(boolean enabledSuspensionButton) {
        this.enabledSuspensionButton = enabledSuspensionButton;
    }

    public boolean isEnabledCancelationButton() {
        return this.enabledCancelationButton;
    }

    public void setEnabledCancelationButton(boolean enabledCancelationButton) {
        this.enabledCancelationButton = enabledCancelationButton;
    }

    public String getNumeroRadicacionMemorando() {
        return this.numeroRadicacionMemorando;
    }

    public void setNumeroRadicacionMemorando(String numeroRadicacionMemorando) {
        this.numeroRadicacionMemorando = numeroRadicacionMemorando;
    }

    public Documento getDocumentoMemorando() {
        return documentoMemorando;
    }

    public void setDocumentoMemorando(Documento documentoMemorando) {
        this.documentoMemorando = documentoMemorando;
    }

    public boolean isEnabledPostponementButton() {
        return this.enabledPostponementButton;
    }

    public void setEnabledPostponementButton(boolean enabledPostponementButton) {
        this.enabledPostponementButton = enabledPostponementButton;
    }

    public boolean isEnabledReactivationButton() {
        return this.enabledReactivationButton;
    }

    public void setEnabledReactivationButton(boolean enabledReactivationButton) {
        this.enabledReactivationButton = enabledReactivationButton;
    }

    public boolean isEnabledExtensionButton() {
        return this.enabledExtensionButton;
    }

    public void setEnabledExtensionButton(boolean enabledExtensionButton) {
        this.enabledExtensionButton = enabledExtensionButton;
    }

    public String getMensajeConfirmacion() {
        return this.mensajeConfirmacion;
    }

    public void setMensajeConfirmacion(String mensajeConfirmacion) {
        this.mensajeConfirmacion = mensajeConfirmacion;
    }

    public List<SelectItem> getMotivosEventualidadItemList() {
        return this.motivosEventualidadItemList;
    }

    public void setMotivosEventualidadItemList(
        List<SelectItem> motivosEventualidadItemList) {
        this.motivosEventualidadItemList = motivosEventualidadItemList;
    }

    public boolean isEventualidadIndefinida() {
        return this.eventualidadIndefinida;
    }

    public void setEventualidadIndefinida(boolean eventualidadIndefinida) {
        this.eventualidadIndefinida = eventualidadIndefinida;
    }

    public LazyDataModel<VComision> getLazyWorkingComisiones() {
        return this.lazyWorkingComisiones;
    }

    public void setLazyWorkingComisiones(LazyDataModel<VComision> workingComisiones) {
        this.lazyWorkingComisiones = workingComisiones;
    }

    public VComision getSelectedComision() {
        return this.selectedComision;
    }

    public void setSelectedComision(VComision selectedComision) {
        this.selectedComision = selectedComision;
    }

    public ComisionEstado getNuevoEstadoComision() {
        return nuevoEstadoComision;
    }

    public void setNuevoEstadoComision(ComisionEstado nuevoEstadoComision) {
        this.nuevoEstadoComision = nuevoEstadoComision;
    }

    public String getLabelFechaUltimoEstado() {
        return this.labelFechaUltimoEstado;
    }

    public void setLabelFechaUltimoEstado(String labelFechaUltimoEstado) {
        this.labelFechaUltimoEstado = labelFechaUltimoEstado;
    }

    public Date getFechaUltimoEstado() {
        return this.fechaUltimoEstado;
    }

    public void setFechaUltimoEstado(Date fechaUltimoEstado) {
        this.fechaUltimoEstado = fechaUltimoEstado;
    }

    public int getNumeroDiasAmpliacion() {
        return this.numeroDiasAmpliacion;
    }

    public void setNumeroDiasAmpliacion(int numeroDiasAmpliacion) {
        this.numeroDiasAmpliacion = numeroDiasAmpliacion;
    }

//--------------------------------------------------------------------------------------------------
    // --- constructor
    /**
     * @version 2.0
     */
    public AdministracionComisionesMB() {

        this.lazyWorkingComisiones = new LazyDataModel<VComision>() {

            @Override
            public List<VComision> load(int first, int pageSize,
                String sortField, SortOrder sortOrder,
                Map<String, String> filters) {

                List<VComision> answer = new ArrayList<VComision>();
                populateComisionesLazyly(answer, first, pageSize, sortField, sortOrder, filters);
                return answer;

            }

        };
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on AdministracionComisionesMB#init ");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * hace la búsqueda de comisiones (VComision)
     */
    private void populateComisionesLazyly(List<VComision> answer, int first, int pageSize,
        String sortField, SortOrder sortOrder,
        Map<String, String> filters) {

        List<VComision> rows;
        int count, size;

        if (this.idTerritorial == null || this.tipoUsuarioAutenticado == null) {
            return;
        }

        rows = this.getTramiteService().buscarComisionesParaAdministrar(this.idTerritorial,
            this.tipoUsuarioAutenticado, sortField, sortOrder.toString(), filters, first,
            pageSize);

        count = this.getTramiteService().contarComisionesParaAdministrar(this.idTerritorial,
            this.tipoUsuarioAutenticado);
        this.lazyWorkingComisiones.setRowCount(count);

        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }

    }

//--------------------------------------------------------------------------------------------------
    // ---- métodos iniciales para cada posible acción
    /**
     * se aplican reglas de validación para los campos y se inicializa el mensaje de confirmación
     */
    /**
     * Ocurre cuando se encuentra en ejecución la comisión; si la comisión no ha iniciado o ya
     * terminó esta actividad no puede realizarse.
     */
    public void initSuspension() {

        this.actionToDo = 1;

        initNuevoEstadoComision("");

        this.nombreEventualidad = "Suspensión";
        this.mensajeConfirmacion = "¿Está seguro de suspender la comisión?";
        this.esSuspAplaz = true;

    }

//----------------------------------------------------
    /**
     * Puede ocurrir en cualquier momento
     */
    public void initCancelacion() {

        this.actionToDo = 2;

        initNuevoEstadoComision("");

        this.nombreEventualidad = "Cancelación";
        this.mensajeConfirmacion = "¿Está seguro de cancelar la comisión?";
        this.esSuspAplaz = false;
    }
//----------------------------------------------------

    /**
     * Ocurre cuando está por ejecutarse una comisión y se van a correr las fechas de la comisión.
     */
    public void initAplazamiento() {

        this.actionToDo = 3;

        initNuevoEstadoComision("");

        this.nombreEventualidad = "Aplazamiento";
        this.mensajeConfirmacion = "¿Está seguro de aplazar la comisión?";
        this.esSuspAplaz = true;

    }

//---------------------------------------------------
    /**
     * Se puede hacer cuando la comisión ha sido aplazada o suspendida de forma indefinida
     *
     */
    public void initReactivacion() {

        this.actionToDo = 4;
        ComisionEstado ultimoComisionEstado;

        initNuevoEstadoComision("");

        // D: hay que hacer la consulta del último registro de cambio de estado
        // para saber la fecha en
        // que se aplazó o se suspendió
        ultimoComisionEstado = this.getTramiteService()
            .consultarUltimoCambioEstadoComision(this.selectedComision.getId());
        this.fechaUltimoEstado = ultimoComisionEstado.getFecha();

        if (this.selectedComision.getEstado().equals(EComisionEstado.APLAZADA.getCodigo())) {
            this.labelFechaUltimoEstado = "Fecha de aplazamiento: ";
        } else if (this.selectedComision.getEstado().equals(EComisionEstado.SUSPENDIDA.getCodigo())) {
            this.labelFechaUltimoEstado = "Fecha de suspensión: ";
        } else {
            this.labelFechaUltimoEstado = "Error!!";
        }

        this.mensajeConfirmacion = "¿Está seguro de reactivar la comisión?";
        this.esSuspAplaz = false;

    }

    // ----------
    /**
     * Puede ocurrir cuando la comisión está por ejecutarse o está en ejecución; es decir que la
     * fecha actual debe ser menor a la fecha de finalización de la comisión
     */
    public void initAmpliacion() {

        this.actionToDo = 5;

        // D: mínimo la debería ampliar en un día
        this.numeroDiasAmpliacion = 1;

        initNuevoEstadoComision("");
        this.nuevoEstadoComision.setFechaFin(DateUtils.addDays(
            this.selectedComision.getFechaFin(), 1));

        this.mensajeConfirmacion = "¿Está seguro de ampliar la comisión?";
        this.esSuspAplaz = false;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * listener de selección en la tabla de comisiones Actualiza las banderas para determinar qué
     * botones de acción se habilitan según las reglas definidas (ver la documentación de los
     * métodos para determinar cuándo se activa qué botón)
     *
     * @param event
     */
    public void manejarActivacionDeBotones(SelectEvent event) {

        String estadoComisionSeleccionada;
        // D: antes de ver qué botones quedan activos se ponen todos
        // deshabilitados
        this.setActionsButtonEnabledValue(false);

        // D: hay que tomar la selección de la tabla del evento porque en este punto la variable
        //    que se asignó para tomar la selección de la tabla aún no ha sido asignada
        //    y está en null
        this.selectedComision = (VComision) event.getObject();
        estadoComisionSeleccionada = this.selectedComision.getEstado();

        // D: esta opción se habilita en cualquier estado de la comisión
        this.enabledCancelationButton = true;

        if (estadoComisionSeleccionada.equals(EComisionEstado.POR_EJECUTAR.getCodigo())) {
            this.enabledPostponementButton = true;
        } else if (estadoComisionSeleccionada.equals(EComisionEstado.EN_EJECUCION.getCodigo())) {
            this.enabledSuspensionButton = true;
            this.enabledExtensionButton = true;
        } // D: la opción de reactivar solo está activa cuando se suspendió o aplazó de forma indefinida
        // En tal caso, las nuevas fechas inicial y final están en null
        else if (estadoComisionSeleccionada.equals(EComisionEstado.SUSPENDIDA.getCodigo()) ||
             estadoComisionSeleccionada.equals(EComisionEstado.APLAZADA.getCodigo())) {
            if (this.selectedComision.getNuevaFechaInicio() == null &&
                 this.selectedComision.getNuevaFechaFin() == null) {
                this.enabledReactivationButton = true;
            }
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * inicializa los valores de 'enabled' de los botones de acción de la forma
     *
     * @param b
     */
    private void setActionsButtonEnabledValue(boolean b) {

        this.enabledSuspensionButton = b;
        this.enabledCancelationButton = b;
        this.enabledPostponementButton = b;
        this.enabledReactivationButton = b;
        this.enabledExtensionButton = b;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * OJO: si no es uno de los eventos de primefaces , y no es exactamente el evento esperado, no
     * ponerle parámetro de evento al listener
     *
     * Listener del evento de marcar el check de "indefinida" para la suspensión o cancelación
     */
    public void cambioEventoIndefinidoListener() {

        this.eventualidadIndefinida = !this.eventualidadIndefinida;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Crea un nuevo objeto ComisionEstado que se usa para guardar el cambio de estado que se haga.
     * </br>
     *
     * Si el cambio de estado lo hace el responsable de conservación, al inicio, las fechas deben
     * ser iguales a las de la comisión seleccionada; si lo hace el director territorial, las fechas
     * del nuevo estado deben ser iguales a las del estado anterior.
     *
     * PRE: la variable de clase 'selectedComision' debe haber sido asignada
     *
     * @author pedro.garcia
     * @param estadoAnterior si es diferente de nulo y de vacío, se debe buscar el ComisionEstado
     * anterior para asignarle al nuevo ls fechas de este.
     */
    private void initNuevoEstadoComision(String estadoAnterior) {

        /**
         * esta se usa para relacionar el ComisionEstado con una Comision. Se tiene el id de la
         * comisión en el VComision seleccionado, pero hay que pasarle un objeto Comision al
         * ComisioEstado para hacerle la actualización
         */
        Comision relatedComision;
        ComisionEstado comisionEstadoPorAprobar;

        this.nuevoEstadoComision = new ComisionEstado();
        this.nuevoEstadoComision.setFecha(new Date(System.currentTimeMillis()));

        if (estadoAnterior != null && !estadoAnterior.isEmpty()) {
            comisionEstadoPorAprobar = this.getTramiteService().
                obtenerUltimoComisionEstadoDeComision(
                    this.selectedComision.getId(), estadoAnterior);

            if (comisionEstadoPorAprobar != null) {
                this.nuevoEstadoComision.setFechaInicio(comisionEstadoPorAprobar.getFechaInicio());
                this.nuevoEstadoComision.setFechaFin(comisionEstadoPorAprobar.getFechaFin());
            } else {
                LOGGER.error(
                    "No se obtuvo el ComisionEstado anterior para copiar las fechas en el nuevo");
            }

        } else {
            this.nuevoEstadoComision.setFechaInicio(this.selectedComision.getFechaInicio());
            this.nuevoEstadoComision.setFechaFin(this.selectedComision.getFechaFin());
        }

        // D: asignarle una Comision al VComision
        relatedComision = new Comision();
        relatedComision.setId(this.selectedComision.getId());
        this.nuevoEstadoComision.setComision(relatedComision);

        // D: datos de log
        this.nuevoEstadoComision.setUsuarioLog(this.loggedInUser.getLogin());
        this.nuevoEstadoComision.setFechaLog(new Date(System.currentTimeMillis()));

    }
//--------------------------------------------------------------------------------------------------

    /**
     * método action de la pantalla con los datos para Suspensión, Cancelación o* Aplazamiento.
     * Revisa qué acción es la requerida y llama al método que valida los campos para cada caso
     */
    public void validarCamposSCA() {

        boolean error = false;

        switch (this.actionToDo) {
            // suspensión
            case (1): {
                error = !validarCamposSuspension();
                break;
            }
            // cancelación
            case (2): {
                error = !validarCamposCancelacion();
                break;
            }
            // aplazamiento
            case (3): {
                error = !validarCamposAplazamiento();
                break;
            }
            default:
                break;
        }

        if (error) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
        }

    }

//--------------------------------------------------------------------
    /**
     * valida los campos en el caso de suspensión. La fecha de suspensión de la comisión debe estar
     * entre la fecha de inicio de comisión y la fecha de finalización de comisión
     *
     * @return true si no encontró algún error de validación
     */
    private boolean validarCamposSuspension() {

        boolean passedValidation = false, error = false;
        Date fechaSuspension;
        Calendar calFechaSuspension, calFechaInicio, calFechaFin;

        fechaSuspension = this.nuevoEstadoComision.getFecha();
        calFechaSuspension = Calendar.getInstance();

        calFechaSuspension.setTime(fechaSuspension);

        if (this.eventualidadIndefinida) {
            if (fechaSuspension.before(this.selectedComision.getFechaInicio()) ||
                 fechaSuspension.after(this.selectedComision.getFechaFin())) {

                this.addMensajeError(
                    "La fecha de suspensión debe estar entre la fecha inicial y final" +
                     " de la comisión");
                error = true;
            } else {
                this.nuevoEstadoComision.setFechaInicio(null);
                this.nuevoEstadoComision.setFechaFin(null);
            }

        } else {
            if (this.nuevoEstadoComision.getFechaInicio() == null ||
                 this.nuevoEstadoComision.getFechaFin() == null) {
                this.addMensajeError(
                    "Si la suspensión no es indefinida debe dar nuevas fechas de " +
                     "inicio y fin de la comisión");
                error = true;
            } else {
                calFechaInicio = Calendar.getInstance();
                calFechaFin = Calendar.getInstance();

                if (validarOrdenNuevasFechas()) {
                    calFechaInicio.setTime(this.selectedComision.getFechaInicio());
                    calFechaFin.setTime(this.selectedComision.getFechaFin());

                    // D: una comisión se suspende cuando esta "en ejecución",
                    // entonces la fecha de suspensión debe ser mayor o igual a la fecha de inicio
                    // y menor o igual a la fecha final.
                    // OJO: hay que tener en cuenta que las comparaciones tienen
                    // en cuenta la hora, por
                    // lo cual hay que preguntar si no son el mismo día
                    if (calFechaSuspension.before(calFechaInicio) &&
                         !(DateUtils.isSameDay(calFechaSuspension, calFechaInicio))) {
                        this.addMensajeError("La fecha de suspensión debe ser mayor o igual que " +
                             "la fecha de inicio de la comisión");
                        error = true;
                    } else if (calFechaSuspension.after(calFechaFin) &&
                         !(DateUtils.isSameDay(calFechaSuspension, calFechaFin))) {
                        this.addMensajeError("La fecha de suspensión debe ser menor o igual que " +
                             "la fecha final de la comisión");
                        error = true;
                    }
                } else {
                    this.addMensajeError(NUEVASFECHASERROR1);
                    error = true;
                }
            }
        }

        passedValidation = !error;
        return passedValidation;
    }

//--------------------------------------------------------------------
    /**
     * valida los campos en el caso de Cancelación Como una comisión puede ser cancelada en
     * cualquier momento, siempre y cuando no se haya ejecutado la comisión, entonces no se debe
     * validar la fecha ni con la fecha inicial ni con la fecha final.
     *
     * Se deja el método por si acaso hay que validar algo más adelante
     *
     * @return true si no encontró algún error de validación
     */
    private boolean validarCamposCancelacion() {
        return true;
    }
//--------------------------------------------------------------------

    /**
     * valida los campos en el caso de Aplazamiento
     *
     * Como la comisión se aplaza en el caso de que esté por ejecutarse, entonces la fecha de
     * aplazamiento debe ser menor que la nueva fecha de inicio de comisión Cuando ya se encuentra
     * en ejecución una comisión esta no puede ser aplazada
     *
     * @return true si no encontró algún error de validación
     */
    private boolean validarCamposAplazamiento() {

        boolean passedValidation = false, error = false;
        Date fechaAplazamiento;

        fechaAplazamiento = this.nuevoEstadoComision.getFecha();
        if (this.eventualidadIndefinida) {
            if (!fechaAplazamiento.before(this.selectedComision.getFechaInicio())) {
                this.addMensajeError("La fecha de aplazamiento debe ser menor que la fecha de " +
                     "inicio de la comisión");
                error = true;
            } else {
                this.nuevoEstadoComision.setFechaInicio(null);
                this.nuevoEstadoComision.setFechaFin(null);
            }
        } else {
            if (this.nuevoEstadoComision.getFechaInicio() == null ||
                 this.nuevoEstadoComision.getFechaFin() == null) {
                this.addMensajeError(
                    "Si el aplazamiento no es indefinido debe dar nuevas fechas de " +
                     "inicio y fin de la comisión");
                error = true;
            } else {
                if (validarOrdenNuevasFechas()) {
                    if (!fechaAplazamiento.before(this.nuevoEstadoComision.getFechaInicio())) {
                        this.addMensajeError(
                            "La fecha de aplazamiento debe ser menor que la nueva " +
                             "fecha de inicio de la comisión");
                        error = true;
                    }
                } else {
                    this.addMensajeError(NUEVASFECHASERROR1);
                    error = true;
                }
            }
        }
        passedValidation = !error;
        return passedValidation;
    }
//--------------------------------------------------------------------

    /**
     * valida los campos en el caso de reactivación de la comisión
     *
     * La nueva fecha inicial de la comisión debe ser mayor o igual que la fecha de inicio de la
     * comisión
     *
     */
    public void validarCamposReactivacion() {

        boolean error = false;

        //javier.aponte Si la fecha de comisión está en null, entonces no se realiza
        //ninguna validación
        if (this.selectedComision.getFechaInicio() == null) {
            return;
        }
        Calendar calFechaInicio, calNuevaFechaInicio;

        calNuevaFechaInicio = Calendar.getInstance();
        calFechaInicio = Calendar.getInstance();
        calNuevaFechaInicio.setTime(this.nuevoEstadoComision.getFechaInicio());
        calFechaInicio.setTime(this.selectedComision.getFechaInicio());

        if (validarOrdenNuevasFechas()) {
            if (!calNuevaFechaInicio.after(calFechaInicio) &&
                 !(DateUtils.isSameDay(calFechaInicio, calNuevaFechaInicio))) {
                this.addMensajeError("La nueva fecha inicial de la comisión debe ser mayor o " +
                     "igual que la fecha de inicio de la comisión");
                error = true;
            }
        } else {
            this.addMensajeError(NUEVASFECHASERROR1);
            error = true;
        }

        if (error) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
        }

    }
//--------------------------------------------------------------------

    /**
     * valida los campos en el caso de ampliación de la comisión.
     *
     * Se supone que la nueva fecha final debe ser mayor que las actuales fechas de inicio y fin de
     * la comisión
     */
    public void validarCamposAmpliacion() {

        boolean error = false;

        if (!this.nuevoEstadoComision.getFechaFin().after(
            this.selectedComision.getFechaFin())) {
            this.addMensajeError("La nueva fecha final debe ser mayor que la actual fecha " +
                 "final de la comisión");
            error = true;
        }

        if (error) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * método que se ejecuta al dar click en el 'OK' de la ventana de confirmación de alguna acción
     * sobre la comisión, cuando el rol del usuario es responsable de conservación.
     *
     * 1. hace la actualización de la comisión. 2. inserta el registro de cambio de estado 3. genera
     * el memorando 4. guarda el documento con el memorando y actualizar estado con dicho documento.
     * 5. actualiza el ComisionEstado con el memorando guardado. Se hace separado de la inserción
     * del registro para garantizar que aunque no pueda guardar el memo, de todas formas guarde el
     * registro 6. enviar correo al director territorial informando del cambio de estado de la
     * comisión
     */
    public void doRequiredActionRelatedRC() {

        int exceptionIdentifier = -1;
        String estadoComision = "NE";

        try {

            //D: cuando el responsable de conservación hace alguna acción sobre la comisión el 
            // memorando relacionado no se radica. Se radica cuando el director territorial ratifica la acción
            if (this.tipoUsuarioAutenticado.equals(ERol.RESPONSABLE_CONSERVACION.getRol())) {
                this.numeroRadicacionMemorando = null;
            }

            // 1. Actualización de la comisión
            switch (this.actionToDo) {
                // suspensión
                case (1): {
                    estadoComision = EComisionEstado.POR_APROBAR_SUSPENSION.getCodigo();
                    this.accionTomadaSobreComision = "suspendida";
                    break;
                }
                // cancelación
                case (2): {
                    estadoComision = EComisionEstado.POR_APROBAR_CANCELACION.getCodigo();
                    this.accionTomadaSobreComision = "cancelada";
                    break;
                }
                // aplazamiento
                case (3): {
                    estadoComision = EComisionEstado.POR_APROBAR_APLAZAMIENTO.getCodigo();
                    this.accionTomadaSobreComision = "aplazada";
                    break;
                }
                // reactivación
                case (4): {
                    estadoComision = EComisionEstado.POR_APROBAR_REACTIVACION.getCodigo();
                    this.accionTomadaSobreComision = "reactivada";

                    break;
                }
                // ampliación
                case (5): {
                    estadoComision = EComisionEstado.POR_APROBAR_AMPLIACION.getCodigo();
                    this.accionTomadaSobreComision = "ampliada";
                    break;
                }
                default:
                    break;
            }

            this.nuevoEstadoComision.setEstado(estadoComision);
            this.selectedComision.setEstado(estadoComision);

            try {
                // D: se guardan los cambios en la comision a partir de la vcomision
                this.getTramiteService().actualizarComision(this.loggedInUser,
                    this.selectedComision);
            } catch (Exception ex) {
                exceptionIdentifier = 1;
                throw new Exception(
                    "Ocurrió una excepción actualizando el estado de la comisión " +
                     this.selectedComision.getId() + ": " + ex.getMessage());
            }

            // 2: Guardar nuevo registro de estado de la comisión 
            try {
                // D: se guarda el registro del nuevo estado de la comision
                ComisionEstado aux = this.getTramiteService().guardarActualizarComisionEstado(
                    this.nuevoEstadoComision);
                if (aux != null) {
                    this.nuevoEstadoComision = aux;
                }

            } catch (Exception ex) {
                exceptionIdentifier = 1;
                throw new Exception("Ocurrió una excepción guardando el registro del cambio de " +
                    "estado de la comisión " + this.selectedComision.getId() + ": " + ex.
                    getMessage());
            }

            //3: 
            try {
                if (!generarReporteMemorando()) {
                    this.addMensajeError("No se pudo generar el memorando de la acción sobre " +
                         "la comisión");
                    return;
                }
            } catch (Exception exReportes) {
                exceptionIdentifier = 3;
                throw new Exception(exReportes.getMessage(), exReportes);
            }

            // 4:
            // OJO: por ahora no se guarda el memo porque nunca se puede volver a consultar y al aprobar la
            //    acción (el director territorial) se genera uno nuevo.
            //if (!this.guardarDocumentoMemorando()) {
            // 5: 
            //D: si pudo guardar el documento, lo asocia al registro de cambio de estado de comisión
            this.nuevoEstadoComision.setMemorandoDocumento(this.documentoMemorando);
            // Actualizar el estado de la comisión con el documento guardado.

            // OJO: como no se guardó el documento en el paso anterior, esto tampoco se hace
            //this.nuevoEstadoComision = this.getTramiteService().
            //        guardarActualizarComisionEstado(this.nuevoEstadoComision);
            // 6:
            if (!this.enviarCorreoInformeCECaDT()) {
                exceptionIdentifier = 4;
                throw new Exception("Error enviando correo al director territorial");
            }

//TODO :: pedro.garcia refs #5208 :: falta definir qué pasa cuando el man retira trámites de la comisión. ¿lo puede hacer? ¿mueve proceso?
        } catch (Exception ex) {
            if (exceptionIdentifier == 1) {
                this.addMensajeError("Ocurrió un error en base de datos por lo que la comisión " +
                    this.selectedComision.getNumero() + " quedó en estado inconsistente");
            } else if (exceptionIdentifier == 2) {
                this.addMensajeError("No se pudo guardar el memorando de la comisión " +
                    this.selectedComision.getNumero());
            } else if (exceptionIdentifier == 3) {
                this.addMensajeError("No se pudo generar el documento memorando.");
            } else if (exceptionIdentifier == 4) {
                this.addMensajeError(
                    "No se pudo enviar el correo con el memorando a los ejecutores" +
                    " de la comisión");
            }
            LOGGER.error(ex.getMessage());
        }

        this.selectedComision = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * método que se ejecuta al dar click en el 'radicar' cuando el director territorial aprueba la
     * acción previa tomada sobre la comisión por parte del responsable de conservación
     *
     * @author pedro.garcia
     */
    /*
     * 0. definir la acción que se va a llevar a cabo, e inicializa algunas variables que dependen
     * de esa acción 1. se obtienen los ejecutores de la comisión 2. obtiene un número de radicación
     * para el memorando 3. hace la actualización de la comisión. En caso de cancelación de la
     * comisión debe borrar las relaciones de la tabla ComisionTramite 4. inserta el registro de
     * cambio de estado (que incluye el id del Documento) 5. genera el memorando 6. guarda el
     * documento con el memorando y actualizar estado con dicho documento. 7. hacer lo
     * correspondiente a procesos para los trámites 7.1 si la comisión se canceló se invoca a un
     * método que mueve las actividades que se deben mover 7.2 si la comisión se suspende o se
     * aplaza suspende las actividades de los trámites 7.3 si la comisión se reactiva se reactivan
     * las actividades de los trámites 8. enviar correo con el memorando generado a los ejecutores
     * de la comisión, el responsable de conservación y los coordinadores
     */
    public void doRequiredActionRelatedDT() {

        int exceptionIdentifier = -1;
        ArrayList<Tramite> tramitesToMove = null;
        String estadoComision = "NE";
        String errorMessage = "";

        this.actionToDo = -1;
        // 0:
        if (this.selectedComision.getEstado().equals(EComisionEstado.POR_APROBAR_AMPLIACION.
            getCodigo())) {
            estadoComision = EComisionEstado.EN_EJECUCION.getCodigo();
            this.accionTomadaSobreComision = "ampliada";
            this.asuntoCorreoEjecutores =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_AMPLIACION_COMISION;
            this.actionToDo = 5;
        } else if (this.selectedComision.getEstado().equals(
            EComisionEstado.POR_APROBAR_APLAZAMIENTO.
                getCodigo())) {
            estadoComision = EComisionEstado.APLAZADA.getCodigo();
            this.asuntoCorreoEjecutores =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_APLAZAMIENTO_COMISION;
            this.accionTomadaSobreComision = "aplazada";
            this.actionToDo = 3;
        } else if (this.selectedComision.getEstado().equals(EComisionEstado.POR_APROBAR_CANCELACION.
            getCodigo())) {
            estadoComision = EComisionEstado.CANCELADA.getCodigo();
            this.asuntoCorreoEjecutores =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_CANCELACION_COMISION;
            this.accionTomadaSobreComision = "cancelada";
            this.actionToDo = 2;
        } //D: se reactiva cuando había sido aplazada o suspendida
        else if (this.selectedComision.getEstado().equals(EComisionEstado.POR_APROBAR_REACTIVACION.
            getCodigo())) {

            GregorianCalendar calCurrentDate, calFechaInicComision, calFechaFinComision;
            Date currentDate = new Date(System.currentTimeMillis());
            calCurrentDate = new GregorianCalendar();
            calFechaInicComision = new GregorianCalendar();
            calFechaFinComision = new GregorianCalendar();

            calCurrentDate.setTime(currentDate);
            calFechaInicComision.setTime(this.selectedComision.getNuevaFechaInicio());
            calFechaFinComision.setTime(this.selectedComision.getNuevaFechaFin());

            if (calCurrentDate.after(calFechaFinComision)) {
                this.addMensajeWarn(
                    "¡La comisión reactivada tiene una fecha final menor que la fecha actual!");
            }

            if (calCurrentDate.after(calFechaInicComision) ||
                DateUtils.isSameDay(calCurrentDate, calFechaInicComision)) {
                estadoComision = EComisionEstado.EN_EJECUCION.getCodigo();
            } else if (calCurrentDate.before(calFechaInicComision)) {
                estadoComision = EComisionEstado.POR_EJECUTAR.getCodigo();
            }

            this.asuntoCorreoEjecutores =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_REACTIVACION_COMISION;
            this.accionTomadaSobreComision = "reactivada";
            this.actionToDo = 4;
        } else if (this.selectedComision.getEstado().equals(EComisionEstado.POR_APROBAR_SUSPENSION.
            getCodigo())) {
            estadoComision = EComisionEstado.SUSPENDIDA.getCodigo();
            this.asuntoCorreoEjecutores =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_SUSPENSION_COMISION;
            this.accionTomadaSobreComision = "suspendida";
            this.actionToDo = 1;
        }

        try {

            // 1: se hace esto aquí para evitar que, en el caso de cancelación de la comisión (que
            //    borra las relaciones de comision_tramite), el resultado de la búsqueda de ejecutores
            //    dé una lista vacía
            getEjecutoresComision();

            // 2: Obtener el número de radicación para el memorando 
            //D: cuando el responsable de conservación hace alguna acción sobre la comisión el 
            // memorando relacionado no se radica. Se radica cuando el director territorial ratifica la acción
            // juan.cruz::17/07/2017::#21345:: Se cambia del funcionario ejecutor al usuario autenticado en el sistema
            UsuarioDTO usuarioDestino = this.getGeneralesService().getCacheUsuario(this.usuario.
                getLogin());

            this.obtenerTipoDocumentoMemorando();

            this.numeroRadicacionMemorando = this.obtenerNumeroRadicado(usuarioDestino);
            if (this.numeroRadicacionMemorando == null ||
                 this.numeroRadicacionMemorando.equals("")) {
                exceptionIdentifier = 1;
                throw new Exception(
                    "No se pudo generar el número de radicación para el memorando");
            }

            // 3: Actualización de la comisión
            //D: crear un nuevo ComisionEstado
            this.initNuevoEstadoComision(this.selectedComision.getEstado());
            this.nuevoEstadoComision.setEstado(estadoComision);
            this.nuevoEstadoComision.setMotivo(this.selectedComision.getMotivo());
            this.nuevoEstadoComision.setObservacion("Se aprueba el cambio de estado a " +
                estadoComision);
            this.selectedComision.setEstado(estadoComision);

            try {
                // D: se guardan los cambios en la comision a partir de la vcomision
                this.getTramiteService().actualizarComision(this.loggedInUser,
                    this.selectedComision);
            } catch (Exception ex) {
                exceptionIdentifier = 2;
                throw new Exception(
                    "Ocurrió una excepción actualizando el estado de la comisión " +
                     this.selectedComision.getId() +
                     " o borrando las relaciones con támites: " + ex.getMessage());
            }

            // 4: Guardar nuevo registro de estado de la comisión 
            try {
                // D: se guarda el registro del nuevo estado de la comision
                ComisionEstado aux = this.getTramiteService().guardarActualizarComisionEstado(
                    this.nuevoEstadoComision);
                if (aux != null) {
                    this.nuevoEstadoComision = aux;
                }

            } catch (Exception ex) {
                exceptionIdentifier = 2;
                throw new Exception("Ocurrió una excepción guardando el registro del cambio de " +
                    "estado de la comisión " + this.selectedComision.getId() + ": " + ex.
                    getMessage());
            }

            // 5: Generar reporte de estado de la comisión 
            try {
                if (!generarReporteMemorando()) {
                    this.addMensajeError("No se pudo generar el memorando de la acción sobre " +
                         "la comisión");
                    return;
                }
            } catch (Exception exReportes) {
                exceptionIdentifier = 3;
                throw new Exception(exReportes.getMessage(), exReportes);
            }

            // 6: 
            if (!this.guardarDocumentoMemorando()) {
                exceptionIdentifier = 2;
                throw new Exception("Ocurrió una excepción al guardar el memorando de cambio " +
                     "de estado de comisión");
            } //D: si pudo guardar el documento, lo asocia al registro de cambio de estado de comisión
            else {
                this.nuevoEstadoComision.setMemorandoDocumento(this.documentoMemorando);
                // Actualizar el estado de la comisión con el documento guardado.
                this.nuevoEstadoComision = this.getTramiteService().guardarActualizarComisionEstado(
                    this.nuevoEstadoComision);
            }

            // 7: hacer movimiento de trámites según el caso
            //D: se consultan los trámites asociados a la comisión seleccionada (antes de borrar las
            //   relaciones en la tabla ya que el método que los consulta busca allí)
            this.tramitesDeComisionMB.setSelectedVComision(this.selectedComision);
            this.tramitesDeComisionMB.consultarTramitesComision();
            tramitesToMove = (ArrayList<Tramite>) this.tramitesDeComisionMB.
                getTramitesAsociadosComision();

            boolean movedTramites;

            switch (this.actionToDo) {

                //D: si la comisión fue suspendida
                // 7.2:
                case (1): {
                    movedTramites = manejarMovimientoTramitesPorAplazamientoOSuspension(
                        tramitesToMove);
                    if (!movedTramites) {
                        errorMessage = "Ocurrió un error al efectuar la suspensión de la comisión.";
                        LOGGER.error(
                            "Error moviendo procesos o actualizando base de datos para la comisión suspendida. (Ver log anterior). : ");
                        exceptionIdentifier = 4;
                        throw new Exception(errorMessage);
                    }

                    break;
                }

                //D: si la comisión fue aplazada
                case (3): {
                    movedTramites = manejarMovimientoTramitesPorAplazamientoOSuspension(
                        tramitesToMove);
                    if (!movedTramites) {
                        errorMessage =
                            "Ocurrió un error al efectuar el aplazamiento de la comisión.";
                        LOGGER.error(
                            "Error moviendo procesos o actualizando base de datos para la comisión aplazada. (Ver log anterior).");
                        exceptionIdentifier = 4;
                        throw new Exception(errorMessage);
                    }

                    break;
                }

                //D: si la comisión fue cancelada
                // 7.1:
                case (2): {

                    //D: por CC-NP-CO-034 ya no se deben borrar todas las relaciones entre trámites y comisión
                    // ni cambiar el dato 'comisionado'.
                    movedTramites = manejarMovimientoTramitesPorCancelacion(tramitesToMove);
                    if (!movedTramites) {
                        errorMessage = "Ocurrió un error al efectuar la cancelación de la comisión.";
                        LOGGER.error(
                            "Error moviendo procesos o actualizando base de datos para la comisión cancelada. (Ver log anterior).");
                        exceptionIdentifier = 4;
                        throw new Exception(errorMessage);
                    }

                    break;
                }

                //D: si la comisión fue reactivada
                // 7.3:
                case (4): {

                    //D: si había sido suspendida las actividades de los trámites se habían suspendido,
                    // por lo tanto hay que reanudarlas
                    movedTramites = this.reanudarActividadesTramites(tramitesToMove);
                    if (!movedTramites) {
                        errorMessage =
                            "Ocurrió un error al reanudar las actividades de los trámites";
                        LOGGER.error(
                            "Error moviendo procesos para reanudar actividades. (Ver log anterior).");
                        exceptionIdentifier = 4;
                        throw new Exception(errorMessage);
                    }
                }

                default:
                    break;
            }

            //Establecer la nueva fecha en la comision
            Comision relatedComision;
            relatedComision = this.getTramiteService().buscarComisionPorId(this.selectedComision.
                getId());
            relatedComision.setFechaInicio(this.selectedComision.getNuevaFechaInicio());
            relatedComision.setFechaFin(this.selectedComision.getNuevaFechaFin());
            this.getTramiteService().actualizarComision(this.usuario, relatedComision);

            // 8: Enviar correo
            if (!this.enviarCorreoAprobacionCEC()) {
                exceptionIdentifier = 7;
                throw new Exception(
                    "Error enviando correo de aprobación de cambio de estado de comisión");
            }

        } catch (Exception ex) {
            switch (exceptionIdentifier) {
                case (1): {
                    this.addMensajeError(
                        "Ocurrió un error generando el número de radicado para la comisión " +
                        this.selectedComision.getNumero() + " quedó en estado inconsistente");

                    break;
                }
                case (2): {
                    this.addMensajeError(
                        "Ocurrió un error en base de datos por lo que la comisión " +
                        this.selectedComision.getNumero() + " quedó en estado inconsistente");

                    break;
                }
                case (3): {
                    this.addMensajeError("Ocurrió un error generando el memorando de la comisión " +
                        this.selectedComision.getNumero() + " quedó en estado inconsistente");

                    break;
                }
                case (4): {
                    this.addMensajeError(errorMessage);

                    break;
                }
                case (7): {
                    this.addMensajeError(
                        "No se pudo enviar el correo con el memorando a los ejecutores" +
                        " de la comisión");

                    break;
                }
                case (8): {
                    this.addMensajeError("No se pudo generar el documento memorando.");

                    break;
                }
                default:
                    break;
            }

            LOGGER.error(ex.getMessage());

        }

        this.selectedComision = null;

    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private String obtenerNumeroRadicado(UsuarioDTO usuarioSeleccionado) {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add(String.valueOf(this.memorandoTipoDocumentoId)); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Cambio estado comisión"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(usuarioSeleccionado.getCodigoTerritorial()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(usuarioSeleccionado.getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(usuarioSeleccionado.getDireccionEstructuraOrganizacional()); // Direccion destino
        parametros.add(usuarioSeleccionado.getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(usuarioSeleccionado.getIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        if (numeroRadicado != null) {
            //ultimo paso: llamar procedimiento recibirRadicado (Servicio de correspondencia para crear tramites)
            this.getTramiteService().recibirRadicado(numeroRadicado, this.usuario.getLogin());
        }

        return numeroRadicado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * valida que la nueva fecha final sea mayor o igual que la nueva ficha inicial
     */
    private boolean validarOrdenNuevasFechas() {

        boolean answer = false;
        Calendar nuevaFechaInicio, nuevaFechaFin;

        nuevaFechaInicio = Calendar.getInstance();
        nuevaFechaFin = Calendar.getInstance();

        if (this.nuevoEstadoComision.getFechaInicio() != null &&
             this.nuevoEstadoComision.getFechaFin() != null) {
            nuevaFechaInicio.setTime(this.nuevoEstadoComision.getFechaInicio());
            nuevaFechaFin.setTime(this.nuevoEstadoComision.getFechaFin());

            if (DateUtils.isSameDay(nuevaFechaInicio, nuevaFechaFin) ||
                 nuevaFechaInicio.before(nuevaFechaFin)) {
                answer = true;
            }

        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener de cambio de opción en la lista de motivos
     */
    public void cambioMotivoListener() {

        LOGGER.debug("entra al que recibe nada");
        this.descripcionMotivoRequerida = (this.nuevoEstadoComision.getMotivo()
            .equals(EComisionEstadoMotivo.OTRO.getCodigo())) ? true : false;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * listener para el cambio en el número de días de la ampliación. Debe asignar un nuevo valor a
     * la nueva fecha final de la comisión
     */
    public void cambioDiasAmpliacionListener() {

        Date nuevaFechaFinal;

        nuevaFechaFinal = this.nuevoEstadoComision.getFechaFin();
        this.nuevoEstadoComision.setFechaFin(DateUtils.addDays(nuevaFechaFinal,
            this.numeroDiasAmpliacion));

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener para el cambio en la fecha de la ampliación. Debe asignar un nuevo valor a la nueva
     * fecha final de la comisión
     */
    public void cambioFechaAmpliacionListener() {

        boolean error = false;

        if (!this.nuevoEstadoComision.getFechaFin().after(
            this.selectedComision.getFechaFin())) {
            this.addMensajeError("La nueva fecha final debe ser mayor que la actual fecha " +
                 "final de la comisión");
            error = true;
        } else {
            this.numeroDiasAmpliacion =
                UtilidadesWeb.calcularNumeroDiasEntreFechas(this.selectedComision.getFechaFin(),
                    this.nuevoEstadoComision.getFechaFin());
        }

        if (error) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "Error en fechas de ampliación");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Llama al servicio de reportes para generar el memorando de la respectiva acción sobre la
     * comisión
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia ya no debe importar que el número de radicación sea nulo porque cuando
     * la acción la hace el responsable de conservación el memorando no se radica
     */
    private boolean generarReporteMemorando() {

        LOGGER.debug("generarReporteMemorando");
        boolean answer = false;

        // Nombre del reporte
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.MEMORANDO_ESTADO_COMISION;

        // Reemplazar parametros para el reporte.			
        Map<String, String> parameters = replaceReportsParameters();
        try {
            this.reporteMemorandoComision =
                this.reportsService.generarReporte(parameters, enumeracionReporte,
                    this.loggedInUser);
            answer = true;
            this.reporteMemorandoHistorico = false;
        } catch (Exception ex) {
            LOGGER.error("error generando reporte: " + ex.getMessage());
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que reemplaza los parametros para el reporte.
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 27-04-2012 definición de atributo que guarda tipo de documento
     * 25-07-2013 el método es privado
     */
 /*
     * @modified by leidy.gonzalez :: #14054:: 20/08/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    private Map<String, String> replaceReportsParameters() {

        LOGGER.debug("replaceReportsParameters");
        Map<String, String> parameters = new HashMap<String, String>();
        String memorandoEstado = "";
        FirmaUsuario firma = null;
        String documentoFirma;

        try {

            parameters.put("COMISION_ID", "" + this.selectedComision.getId());
            if (this.numeroRadicacionMemorando != null &&
                !this.numeroRadicacionMemorando.trim().isEmpty()) {
                parameters.put("NUMERO_RADICADO", this.numeroRadicacionMemorando);
            } else {
                parameters.put("NUMERO_RADICADO", "");
            }

            UsuarioDTO directorTerritorial = null;
            if (this.directoresTerritoriales == null) {
                this.directoresTerritoriales = (List<UsuarioDTO>) this
                    .getTramiteService()
                    .buscarFuncionariosPorRolYTerritorial(
                        this.loggedInUser.getDescripcionTerritorial(),
                        ERol.DIRECTOR_TERRITORIAL);
            }
            if (this.directoresTerritoriales != null &&
                 !this.directoresTerritoriales.isEmpty() &&
                 this.directoresTerritoriales.get(0) != null) {
                directorTerritorial = this.directoresTerritoriales.get(0);
            }
            if (directorTerritorial != null &&
                 directorTerritorial.getNombreCompleto() != null) {
                parameters.put("NOMBRE_DIRECTOR_TERRITORIAL",
                    directorTerritorial.getNombreCompleto());

                firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                    directorTerritorial.getNombreCompleto());

                if (firma != null && directorTerritorial.getNombreCompleto().equals(firma.
                    getNombreFuncionarioFirma())) {

                    documentoFirma = this.getGeneralesService().
                        descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                            getIdRepositorioDocumentos());

                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }
            } else {
                parameters.put(
                    "NOMBRE_DIRECTOR_TERRITORIAL",
                    "No se encontró un " + ERol.DIRECTOR_TERRITORIAL.toString() +
                     " para la territorial " +
                     this.loggedInUser.getCodigoTerritorial());
            }

            //Parametro para identificar si es el director territorial
            boolean isDirectorTerritorial = false;
            for (String rol : this.usuario.getRoles()) {
                if (ERol.DIRECTOR_TERRITORIAL.getRol().equalsIgnoreCase(rol)) {
                    isDirectorTerritorial = true;
                }
            }
            if (isDirectorTerritorial) {
                parameters.put("ES_DIRECTOR_TERRITORIAL", "true");
            } else {
                parameters.put("ES_DIRECTOR_TERRITORIAL", "false");
            }

            // Indicar el tipo de memorando
            //D: se aprovecha aquí (dado el órden en que se ejecutan los métodos en el flujo) para
            //  definir el tipo de documento según la acción
            if (this.actionToDo > 0) {

                memorandoEstado = this.obtenerTipoDocumentoMemorando();
                parameters.put("TIPO_MEMORANDO_COMISION", memorandoEstado);
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            return null;
        }

        return parameters;
    }

    /**
     * Método para obtener el tipo de documento del memorando
     *
     * @author javier.aponte
     * @return
     */
    private String obtenerTipoDocumentoMemorando() {

        String memorandoEstado = "";
        switch (this.actionToDo) {
            // Suspensión
            case (1): {
                memorandoEstado = EComisionEstado.SUSPENDIDA.getCodigo();
                this.memorandoTipoDocumentoId =
                    ETipoDocumento.MEMORANDO_DE_SUSPENSION_DE_COMISION.getId();

                break;
            }
            // Cancelación
            case (2): {
                memorandoEstado = EComisionEstado.CANCELADA.getCodigo();
                this.memorandoTipoDocumentoId =
                    ETipoDocumento.MEMORANDO_DE_CANCELACION_DE_COMISION.getId();

                break;
            }
            // Aplazamiento
            case (3): {
                memorandoEstado = EComisionEstado.APLAZADA.getCodigo();
                this.memorandoTipoDocumentoId =
                    ETipoDocumento.MEMORANDO_DE_APLAZAMIENTO_DE_COMISION.getId();

                break;
            }
            // Reactivación
            case (4): {
                // Las validaciones para saber el estado del memorando se hacen en el 
                // reporte, para este caso.
                memorandoEstado = EComisionEstado.POR_EJECUTAR.getCodigo();
                this.memorandoTipoDocumentoId =
                    ETipoDocumento.MEMORANDO_DE_REACTIVACION_DE_COMISION.getId();

                break;
            }
            // Ampliación
            case (5): {
                memorandoEstado = EComisionEstado.EN_EJECUCION.getCodigo();
                this.memorandoTipoDocumentoId =
                    ETipoDocumento.MEMORANDO_DE_AMPLIACION_DE_COMISION.getId();

                break;
            }
            default:
                break;
        }
        return memorandoEstado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * consulta el histórico de los estados por os que ha pasado una comisión
     */
    public void consultarHistoricoComision() {

        try {
            this.historicoEstadosComision =
                (ArrayList<ComisionEstado>) this.getTramiteService().
                    consultarHistoricoCambiosEstadoComision(this.selectedComision.getId());
        } catch (Exception ex) {
            LOGGER.error("error en AdministracionComisionesMB#consultarHistoricoComision: " +
                ex.getMessage());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * hace la búsqueda del documento correspondiente al memorando que se generó para el registro de
     * cambio de estado seleccionado (en realidad no se seleccionó, sino que se relaciona por medio
     * de un link en la fila de la tabla)
     */
    public void consultarMemorandoEstadoComision() {

        String idDocumentoGestorDoc;

        //N: en esta variable tengo los datos para ir a hacer la consulta del documento en el alfresco
        // this.selectedComisionEstadoDocumento
        // D: se obtiene la url del archivo (el método lo recupera del alfresco y retorna la url;
        // además lo mueve a la carpeta temporal de documentos)
        // pero hay que adicionarle la ruta del contexto de archivos
        idDocumentoGestorDoc = this.selectedComisionEstadoDocumento.getIdRepositorioDocumentos();

        this.reporteMemorandoComision = this.reportsService.consultarReporteDeGestorDocumental(
            idDocumentoGestorDoc);

        this.reporteMemorandoHistorico = true;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Guarda el documento con el memorando generado según la acción hecha sobre la comisión
     *
     * @return true si pudo hacerlo
     */
    private boolean guardarDocumentoMemorando() {

        boolean answer = true;
        Documento tempDocumento;
        TipoDocumento tipoDoc = null;
        Date fechaLogYRadicacion;

        if (this.reporteMemorandoComision == null) {
            return false;
        }

        if (this.memorandoTipoDocumentoId != null) {
            try {
                tipoDoc = this.getGeneralesService().
                    buscarTipoDocumentoPorId(this.memorandoTipoDocumentoId);
            } catch (Exception ex) {
                LOGGER.error("error buscando TipoDocumento con id " +
                    this.memorandoTipoDocumentoId + " : " + ex.getMessage());
                return false;
            }
        }

        if (tipoDoc == null) {
            LOGGER.error("Error: no se encontró un tipo de documento " +
                this.memorandoTipoDocumentoId + " para el memorando");
            return false;
        }

        tempDocumento = new Documento();
        fechaLogYRadicacion = new Date(System.currentTimeMillis());

        // D: se arma el objeto Documento con todos los atributos posibles...
        //N: en este punto el campo 'archivo es la ruta completa' porque así lo necesita el método.
        //  Luego, en ese método que guarda el doc en el gestor documental, se cambia y ese campo 
        //  contiene solo el nombre
        tempDocumento.setArchivo(this.reporteMemorandoComision.getRutaCargarReporteAAlfresco());

        //v1.1.7
        tempDocumento.setTipoDocumento(tipoDoc);
        tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
        tempDocumento.setUsuarioCreador(this.loggedInUser.getLogin());
        tempDocumento.setEstructuraOrganizacionalCod(
            this.loggedInUser.getCodigoEstructuraOrganizacional());
        tempDocumento.setFechaRadicacion(fechaLogYRadicacion);
        tempDocumento.setNumeroRadicacion(this.numeroRadicacionMemorando);

        tempDocumento.setFechaLog(fechaLogYRadicacion);
        tempDocumento.setUsuarioLog(this.loggedInUser.getLogin());
        tempDocumento.setDescripcion("Memorando de cambio de estado de la comisión generado " +
             "por el sistema");

        try {
            // D: se guarda el documento
            this.documentoMemorando = this.getGeneralesService().guardarMemorandoConservacion(
                tempDocumento, this.numeroRadicacionMemorando, this.loggedInUser);

        } catch (Exception ex) {
            LOGGER.error("error guardando Documento memorando: " + ex.getMessage());
            return false;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método invocado para retirar los trámites de una comisión.
     *
     * 1. Llama al método del mb relacionado (el que pinta la tabla con los trámites y permite
     * retirarlos). Ese método se encarga de borrar las relaciones entre los trámites y la comisión,
     * y de registrar el cambio de estado de la comisión a 'cancelada' si se retiraron todos los
     * trámites. 2. devuelve el proceso para los trámites que se retiraron de la comisión 3. envía
     * correo a los ejecutores de los trámites retirados 4. ?
     */
    public void retirarTramitesComision() {

        boolean error = false;

        // 1.
        try {
            this.tramitesDeComisionMB.retirarTramitesComision();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }

        // 2.
        if (this.tramitesDeComisionMB.isTramitesRetiradosBD()) {
            boolean movedTramites = this.moverProcesosDeTramitesRetiradosOComisionCancelada(
                this.tramitesDeComisionMB.getSelectedTramitesComision());
        } else {
            LOGGER.error("Ocurrió un error retirando los trámites de la comisión");
            return;
        }

        // 3.
        error = this.enviarCorreoEjecutoresPorRetiroTramites();
        if (error) {
            return;
        }

        // 4.
        if (this.tramitesDeComisionMB.isCancelarComision()) {

            //D: no se genera memorando porque la acción tomada no fue cancelación sino retiro de
            //   trámites. Por eso no se ejecuta el código a continuación
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón cerrar de la ventana principal
     */
    public String cerrarVentanaCU() {

        //D: forzar el init cuando vuelva a entrar al cu
        UtilidadesWeb.removerManagedBean("administracionComisiones");

        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * mueve los procesos del bpm correspondientes a los trámites que son retirados de una comisión,
     * o los trámites que hacían parte de una comisión cancelada. Se mueven a la actividad de
     * comisionar trámites de terreno
     *
     * @return true si pudo mover todos los procesos; false ow
     */
    private boolean moverProcesosDeTramitesRetiradosOComisionCancelada(ArrayList<Tramite> tramites) {

        boolean answer = true;
        String observaciones, processTransition;
        List<UsuarioDTO> usuariosResponsablesConservacion;
        SolicitudCatastral sc;

        observaciones = "Se devuelve de actividad por retiro del trámite de la comisión, o por " +
            "cancelación de una comisión";

        //D: los trámites se devuelven a la actividad 'comisionar trámites de terreno', la cual la 
        //  hace el responsable de conservación
        processTransition = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;

        // Siempre existira un director para la territorial
        usuariosResponsablesConservacion = this.getTramiteService().
            buscarFuncionariosPorRolYTerritorial(
                this.loggedInUser.getDescripcionTerritorial(), ERol.RESPONSABLE_CONSERVACION);

        if (usuariosResponsablesConservacion == null || usuariosResponsablesConservacion.isEmpty()) {
            LOGGER.error("No se encontraron funcionarios con el rol " +
                ERol.RESPONSABLE_CONSERVACION.getRol() + " en la territorial " +
                this.loggedInUser.getDescripcionTerritorial());
            this.addMensajeError("Ocurrió un error a nivel del directorio de usuarios.");
            return false;
        }

        sc = new SolicitudCatastral();
        sc.setTransicion(processTransition);
        sc.setUsuarios(usuariosResponsablesConservacion);
        sc.setObservaciones(observaciones);

        for (Tramite tramiteToMoveBW : tramites) {
            try {
                this.getProcesosService().avanzarActividadPorIdProceso(
                    tramiteToMoveBW.getProcesoInstanciaId(), sc);
            } catch (Exception ex) {
                answer = false;
                LOGGER.error("Error moviendo el trámite " + tramiteToMoveBW.getId() +
                    " a la actividad " + processTransition + ": " + ex.getMessage());
            } finally {
                return answer;
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * lo mismo que
     *
     * @see
     * AdministracionComisionesMB#moverProcesosDeTramitesRetiradosOComisionCancelada(ArrayList<Tramite>)
     * pero se usa cuando los trámites que le llegan son los seleccionados en la tabla de trámites
     * asociados a una comisión
     *
     * @param tramites
     * @return
     */
    private boolean moverProcesosDeTramitesRetiradosOComisionCancelada(Tramite[] tramites) {

        boolean answer = false;
        ArrayList<Tramite> tramitesAL;
        Tramite tempTramite;

        if (tramites.length > 0) {
            tramitesAL = new ArrayList<Tramite>();
            for (int i = 0; i < tramites.length; i++) {
                tempTramite = tramites[i];
                tramitesAL.add(tempTramite);
            }
            answer = this.moverProcesosDeTramitesRetiradosOComisionCancelada(tramitesAL);
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * En este método se hace la búsqueda de ejecutores de la comisión que se haya seleccionado
     *
     * @return
     */
    private List<VEjecutorComision> getEjecutoresComision() {

        String tempNumeroComision;

        if (this.selectedComision.getNumero() != null) {
            tempNumeroComision = this.selectedComision.getNumero();

            this.ejecutoresComision =
                this.getTramiteService().buscarEjecutoresPorNumeroComision(tempNumeroComision);
        }
        return this.ejecutoresComision;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que envia un correo de aprobación del cambio de estado de la comisión (CEC) a los
     * ejecutores asociados, al responsable de conservación y a los coordinadores. Va adjunto el
     * memorando generado.
     *
     * @author pedro.garcia
     *
     */
    private boolean enviarCorreoAprobacionCEC() {

        boolean answer = true;
        Locale colombia = new Locale("ES", "es_CO");

        if (this.ejecutoresComision == null || this.ejecutoresComision.isEmpty()) {
            return false;
        }

        String[] adjuntos = null, titulos;
        String emailAddress, funcionarioFirmante;
        ArrayList<String> emailAddresses;

        if (this.usuarioDirectorTerritorial == null) {
            obtenerFuncionarioDirectorTerritorial();
        }

        if (this.usuarioResponsableConservacion == null) {
            obtenerFuncionarioResponsableConservacion();
        }

        if (this.reporteMemorandoComision != null) {
            adjuntos = new String[]{this.reporteMemorandoComision.getRutaCargarReporteAAlfresco()};
        }
        titulos = new String[]{
            ConstantesComunicacionesCorreoElectronico.ARCHIVO_MEMORANDO_CAMBIOESTADO_COMISION};

        //D: reemplazar parámetro de número de comisión en el asunto del correo. Esta variable se
        // inicializó en el método que llama a este
        this.asuntoCorreoEjecutores = this.asuntoCorreoEjecutores.
            replaceFirst("\\Q{p\\E}", this.selectedComision.getNumero());

        //--- contenido del correo  ---------
        this.contenidoCorreoEjecutores =
            ConstantesComunicacionesCorreoElectronico.CONTENIDO_APROBACION_MODIFICACION_COMISION;
        Object parametrosContenidoCorreoEjecutores[] = new Object[3];
        parametrosContenidoCorreoEjecutores[0] = this.selectedComision.getNumero();
        parametrosContenidoCorreoEjecutores[1] = this.accionTomadaSobreComision;

        //D: obtener funcionario que firma el correo
        funcionarioFirmante = this.usuarioDirectorTerritorial.getNombreCompleto();
        parametrosContenidoCorreoEjecutores[2] = funcionarioFirmante;

        //N: es la forma antigua. Se deja para ver cómo se encontraría un {p} en un texto
        // when using the \Q...\E escape sequence the characters between the \Q and the \E are interpreted as literal characters
        //this.contenidoCorreoEjecutores = this.contenidoCorreoEjecutores.replaceFirst("\\Q{p\\E}", this.selectedComision.getNumero());
        this.contenidoCorreoEjecutores =
            new MessageFormat(this.contenidoCorreoEjecutores, colombia).
                format(parametrosContenidoCorreoEjecutores);

        //------------
        emailAddresses = new ArrayList<String>();

        UsuarioDTO funcionarioEjecutor;

        //D: se obtienen las direcciones de los ejecutores de los trámites
        for (VEjecutorComision tempVEjecutor : this.ejecutoresComision) {
            funcionarioEjecutor = this.getGeneralesService().getCacheUsuario(tempVEjecutor.
                getIdFuncionarioEjecutor());
            emailAddress = funcionarioEjecutor.getEmail();
            emailAddresses.add(emailAddress);
        }

        //D: Se requiere que el correo se le envie tambien a todos los coordinadores de la territorial.
        List<UsuarioDTO> coordinadores =
            this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.loggedInUser.getDescripcionTerritorial(), ERol.COORDINADOR);
        for (UsuarioDTO coordinador : coordinadores) {
            emailAddress = coordinador.getEmail();
            emailAddresses.add(emailAddress);
        }

        //D: dirección del responsable de conservación
        emailAddress = this.usuarioResponsableConservacion.getEmail();
        emailAddresses.add(emailAddress);

        try {
            this.getGeneralesService().enviarCorreo(emailAddresses, this.asuntoCorreoEjecutores,
                this.contenidoCorreoEjecutores, adjuntos, titulos);
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico en " +
                 "AdministracionComisionesMB#enviarCorreo: " + e.getMessage());
            answer = false;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que envia un correo informando al director territorial que hubo un cambio de estado de
     * la comisión (CEC), el cual debe ser aprobado por él.
     *
     * @author pedro.garcia
     */
    private boolean enviarCorreoInformeCECaDT() {

        boolean answer = true;
        Locale colombia = new Locale("ES", "es_CO");

        String emailAddress, funcionarioFirmante;

        if (this.usuarioDirectorTerritorial == null) {
            obtenerFuncionarioDirectorTerritorial();
        }

        if (this.usuarioResponsableConservacion == null) {
            obtenerFuncionarioResponsableConservacion();
        }

        //D: reemplazar parámetro de número de comisión en el asunto del correo
        this.asuntoCorreoDirectorTerritorial =
            ConstantesComunicacionesCorreoElectronico.ASUNTO_CAMBIO_ESTADO_COMISION;
        this.asuntoCorreoDirectorTerritorial =
            this.asuntoCorreoDirectorTerritorial.replaceFirst(
                "\\Q{p\\E}", this.selectedComision.getNumero());

        //--- contenido del correo  ---------
        this.contenidoCorreoDirectorTerritorial =
            ConstantesComunicacionesCorreoElectronico.CONTENIDO_MODIFICACION_COMISION;
        Object parametrosContenidoCorreoEjecutores[] = new Object[3];
        parametrosContenidoCorreoEjecutores[0] = this.selectedComision.getNumero();
        parametrosContenidoCorreoEjecutores[1] = this.accionTomadaSobreComision;

        //D: obtener funcionario que firma el correo
        funcionarioFirmante = this.usuarioResponsableConservacion.getNombreCompleto();
        parametrosContenidoCorreoEjecutores[2] = funcionarioFirmante;

        //N: es la forma antigua. Se deja para ver cómo se encontraría un {p} en un texto
        //this.contenidoCorreoEjecutores = this.contenidoCorreoEjecutores.replaceFirst("\\Q{p\\E}", this.selectedComision.getNumero());
        this.contenidoCorreoDirectorTerritorial =
            new MessageFormat(this.contenidoCorreoDirectorTerritorial, colombia).
                format(parametrosContenidoCorreoEjecutores);

        //------------
        emailAddress = this.usuarioDirectorTerritorial.getEmail();
        try {
            boolean correoEnviado = this.getGeneralesService().enviarCorreo(
                emailAddress, this.asuntoCorreoDirectorTerritorial,
                this.contenidoCorreoDirectorTerritorial, null, null);
            if (!correoEnviado) {
                LOGGER.error("Error enviando correo a director territorial " +
                    this.usuarioDirectorTerritorial.getLogin() + " a la dirección " +
                    this.usuarioDirectorTerritorial.getEmail());
            }
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico en " +
                 "AdministracionComisionesMB#enviarCorreo: " + e.getMessage());
            answer = false;
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * envía correo a los ejecutores de los trámites retirados de una comisión OJO: aquí se trabaja
     * con los datos que están en el mb asociado TramitesDeComisionMB
     */
    private boolean enviarCorreoEjecutoresPorRetiroTramites() {

        boolean error = false;

        String emailAddress, idFuncionario, valueTemp, funcionarioFirmante;
        StringBuilder contenidoTempCorreo, contenidoTempTabla;
        HashMap<String, String> correosAEnviar;

        this.asuntoCorreoEjecutores =
            ConstantesComunicacionesCorreoElectronico.ASUNTO_RETIROTRAMITES_COMISION;

        //D: reemplazar parámetro número de comisión en el asunto
        this.asuntoCorreoEjecutores = this.asuntoCorreoEjecutores.replaceFirst("\\Q{p\\E}",
            this.tramitesDeComisionMB.getSelectedVComision().getNumero());

        this.contenidoCorreoEjecutoresRetiroTrams =
            ConstantesComunicacionesCorreoElectronico.CONTENIDO_RETIROTRAMITES_COMISION;

        //N: en este se deja el reemplazo a la antigüita porque hay parámetros que se asignan una vez y
        //   otros que se asignan varias veces (una por cada ejecutor)
        this.contenidoCorreoEjecutoresRetiroTrams =
            this.contenidoCorreoEjecutoresRetiroTrams.replaceFirst("\\Q{p\\E}",
                this.tramitesDeComisionMB.getSelectedVComision().getNumero());

        correosAEnviar = new HashMap<String, String>();

        //D: obtener los ids de ejecutor y los números de radicación de cada trámite retirado de
        //   ese ejecutor para armar el cuerpo del mensaje
        for (Tramite tempTramiteItem : this.tramitesDeComisionMB.getSelectedTramitesComision()) {
            idFuncionario = tempTramiteItem.getFuncionarioEjecutor();
            if (!correosAEnviar.containsKey(idFuncionario)) {
                correosAEnviar.put(idFuncionario, "");
            }

            contenidoTempTabla = new StringBuilder(correosAEnviar.get(idFuncionario));
            contenidoTempTabla.append("<tr><td>").append(tempTramiteItem.getNumeroRadicacion()).
                append("</td></tr>");
            correosAEnviar.put(idFuncionario, contenidoTempTabla.toString());
        }

        //D: obtener el funcionario que firma
        if (this.usuarioResponsableConservacion == null) {
            obtenerFuncionarioResponsableConservacion();
        }
        funcionarioFirmante = this.usuarioResponsableConservacion.getNombreCompleto();

        UsuarioDTO usuarioCorreo;

        for (String key : correosAEnviar.keySet()) {
            contenidoTempCorreo = new StringBuilder();
            contenidoTempCorreo.append("<table><tr><th><b>Numero de radicación</b></th></tr>");
            valueTemp = correosAEnviar.get(key);
            contenidoTempCorreo.append(valueTemp);
            contenidoTempCorreo.append("</table>");

            //D: reemplazo del segundo parámetro
            this.contenidoCorreoEjecutoresRetiroTrams =
                this.contenidoCorreoEjecutoresRetiroTrams.replaceFirst("\\Q{p\\E}",
                    contenidoTempCorreo.toString());

            //D: reemplazo de nombre de funcionario que firma
            this.contenidoCorreoEjecutoresRetiroTrams =
                this.contenidoCorreoEjecutoresRetiroTrams.replaceFirst("\\Q{p\\E}",
                    funcionarioFirmante);

            usuarioCorreo = this.getGeneralesService().getCacheUsuario(key);
            emailAddress = usuarioCorreo.getEmail();
            try {
                this.getGeneralesService().enviarCorreo(emailAddress, this.asuntoCorreoEjecutores,
                    this.contenidoCorreoEjecutoresRetiroTrams, null, null);
            } catch (Exception e) {
                LOGGER.debug("Error al enviar el correo electrónico en " +
                     "AdministracionComisionesMB#enviarCorreo: " + e.getMessage());
                error = true;
                break;
            }
        }

        // Se requiere que el correo se le envie tambien a todos los
        // coordinadores de la territorial.
        List<UsuarioDTO> coordinadores = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.loggedInUser.getDescripcionTerritorial(),
                ERol.COORDINADOR);
        for (UsuarioDTO coordinador : coordinadores) {
            emailAddress = coordinador.getEmail();

            try {
                this.getGeneralesService().enviarCorreo(emailAddress,
                    this.asuntoCorreoEjecutores,
                    this.contenidoCorreoEjecutoresRetiroTrams, null, null);
            } catch (Exception e) {
                LOGGER.debug("Error al enviar el correo electrónico a los coordinadores en " +
                     "AdministracionComisionesMB#enviarCorreo: " +
                     e.getMessage());
                error = true;
                break;
            }
        }

        return error;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * obtiene un funcionario director territorial para firmar el correo
     *
     * @author pedro.garcia
     *
     * @return
     */
    private void obtenerFuncionarioResponsableConservacion() {

        ArrayList<UsuarioDTO> funcionariosResponsablesConservacion;

        funcionariosResponsablesConservacion = (ArrayList<UsuarioDTO>) this.getTramiteService().
            buscarFuncionariosPorRolYTerritorial(this.loggedInUser.getDescripcionTerritorial(),
                ERol.RESPONSABLE_CONSERVACION);

        if (funcionariosResponsablesConservacion != null &&
            !funcionariosResponsablesConservacion.isEmpty()) {
            this.usuarioResponsableConservacion = funcionariosResponsablesConservacion.get(0);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * obtiene un funcionario director territorial para firmar el correo
     *
     * @author pedro.garcia
     *
     * @return
     */
    private void obtenerFuncionarioDirectorTerritorial() {

        ArrayList<UsuarioDTO> funcionariosDirectorTerritorial;

        funcionariosDirectorTerritorial = (ArrayList<UsuarioDTO>) this.getTramiteService().
            buscarFuncionariosPorRolYTerritorial(this.loggedInUser.getDescripcionTerritorial(),
                ERol.DIRECTOR_TERRITORIAL);

        if (funcionariosDirectorTerritorial != null &&
            !funcionariosDirectorTerritorial.isEmpty()) {
            this.usuarioDirectorTerritorial = funcionariosDirectorTerritorial.get(0);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del click en el link que se usa como truquini para definir el tipo de usuario que
     * ejecuta el MB. El click a ese link se fuerza al cargarse la página. Como dependiendo del tipo
     * de usuario se deben hacer diferentes acciones, lo que se haría en el init se hace aquí.
     *
     * @author pedro.garcia
     */
    public void linkDummyParaDefinirUsuarioListener() {
        LOGGER.debug("on linkDummyParaDefinirUsuarioListener");

        this.motivosEventualidadItemList = this.generalMBService.getComisionEstadoMotivoV();
        this.motivosEventualidadItemList.add(0, new SelectItem(null,
            this.generalMBService.getCombosDefaultValue()));

        //int count;
        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();
        this.idTerritorial = this.loggedInUser.getCodigoEstructuraOrganizacional();

        //D: solo para verificar que el tipo de usuario usado en la página coincida con los definidos
        if (!(this.tipoUsuarioAutenticado.equals(ERol.DIRECTOR_TERRITORIAL.getRol()) ||
             this.tipoUsuarioAutenticado.equals(ERol.RESPONSABLE_CONSERVACION.getRol()))) {
            LOGGER.error(
                "OJO: no coincide el tipo de usuario con los definidos en la aplicación!!!" +
                " Esto implica que no se mostrarán datos");
        }

        // D: al iniciar, todos los botones de administración deben estar activos
        if (this.tipoUsuarioAutenticado.equals(ERol.RESPONSABLE_CONSERVACION.getRol())) {
            this.setActionsButtonEnabledValue(true);
            this.esUsuarioResponsableConservacion = true;
        } else if (this.tipoUsuarioAutenticado.equals(ERol.DIRECTOR_TERRITORIAL.getRol())) {
            this.esUsuarioDirectorTerritorial = true;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Mueve los procesos de los trámites de una comisión que se había suspendido y está siendo
     * reactivada. Los mueva de la actividad 'tiempos muertos' a la actividad en la que estaban
     * antes.
     *
     * @author pedro.garcia
     * @param tramitesToMove lista de trámites que se van a mover
     */
    private boolean moverProcesosDeComisionReactivada(ArrayList<Tramite> tramitesToMove) {

        boolean answer;

        answer = false;

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Mueve los procesos de los trámites de una comisión que se cancela dependiendo del tipo de
     * trámite. También, para algunos, borra las relaciones entre los trámites y la comisión
     *
     * @author pedro.garcia
     * @param tramitesToMove lista de trámites que se van a mover
     */
    private boolean manejarMovimientoTramitesPorCancelacion(ArrayList<Tramite> tramitesToMove) {

        boolean answer = false;
        int numTramitesTratados;

        String idTramite, nombreActividad;
        Actividad actividad;

        List<Actividad> actividades;

        SolicitudCatastral solicitudCatastral, scRespuesta;
        String transicion;
        List<UsuarioDTO> usuarios;

        numTramitesTratados = 0;

        for (Tramite tramite : tramitesToMove) {

            idTramite = tramite.getId().toString();
            Map<EParametrosConsultaActividades, String> filtros =
                new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
            filtros.put(EParametrosConsultaActividades.ID_TRAMITE, idTramite);
            filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);

            try {
                actividades = this.getProcesosService().consultarListaActividades(filtros);
            } catch (ExcepcionSNC ex) {
                LOGGER.error("Error consultando la lista de actividades para el trámite " +
                    idTramite);
                return answer;
            }

            if (actividades != null && !actividades.isEmpty()) {

                if (actividades.size() > 1) {
                    LOGGER.warn("La lista de actividades para el trámite " + idTramite +
                        " tiene más de un elemento. Se toma la primera actividad para hacer el movimiento del trámite por cancelación de la comisión.");
                }

                actividad = actividades.get(0);
                nombreActividad = actividad.getNombre();

                solicitudCatastral = new SolicitudCatastral();
                if (nombreActividad.equals(
                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {

                    //D: mover el trámite
                    transicion = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                    usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                        this.loggedInUser.getDescripcionEstructuraOrganizacional(),
                        ERol.RESPONSABLE_CONSERVACION);
                    if (usuarios == null || usuarios.isEmpty()) {
                        LOGGER.error("No se encontraron usuarios con el rol " +
                            ERol.RESPONSABLE_CONSERVACION);
                        return answer;
                    }
                    solicitudCatastral.setUsuarios(usuarios);
                    solicitudCatastral.setTransicion(transicion);
                    scRespuesta = this.getProcesosService().avanzarActividad(
                        actividad.getId(), solicitudCatastral);
                    if (scRespuesta == null) {
                        LOGGER.error("Error en Administración de comisiones al mover el trámite " +
                            tramite.getId().toString());
                        return answer;
                    }

                    //javier.aponte CC-NO-CO-034 Si la comisión está cancelada entonces elimina las relaciones de comisión trámite
                    // y establece el trámite en comisionado NO
                    tramite = this.generalMBService.
                        eliminarRelacionComisionTramite(tramite, this.usuario);
                } //D: las otras posibles actividades en que podría estar el trámite son: 
                //  Alistar información, Cargar al SNC, Revisar Trámite devuelto. 
                //  Para estas no se debe hacer nada con el trámite
                else {
                }

            }

            //D: solo hasta este punto se considera que concluyó bien lo que tenía que hacerse con el trámite
            numTramitesTratados++;

        }

        if (numTramitesTratados == tramitesToMove.size()) {
            answer = true;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace la suspensión de los procesos de trámites, según la actividad en que estén, cuando se
     * aplaza o suspende una comisión. También hace las actualizaciones de bd correspondientes.
     * </br>
     *
     * @author pedro.garcia
     * @param tramitesToMove lista de trámites asociados a la comisión que se va a suspender o
     * aplazar
     * @return true si todo resulta bien
     */
    private boolean manejarMovimientoTramitesPorAplazamientoOSuspension(
        ArrayList<Tramite> tramitesToMove) {

        boolean answer = false, error = false;

        for (Tramite tramite : tramitesToMove) {

            String idTramite, nombreActividad;
            Actividad actividad;

            idTramite = tramite.getId().toString();
            Map<EParametrosConsultaActividades, String> filtros =
                new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
            filtros.put(EParametrosConsultaActividades.ID_TRAMITE, idTramite);
            filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);

            List<Actividad> actividades = null;

            try {
                actividades = this.getProcesosService().consultarListaActividades(filtros);
            } catch (ExcepcionSNC ex) {
                LOGGER.error("Error consultando la lista de actividades para el trámite " +
                    idTramite);
                return answer;
            }

            if (actividades != null && !actividades.isEmpty()) {

                if (actividades.size() > 1) {
                    LOGGER.warn("La lista de actividades para el trámite " + idTramite +
                        " tiene más de un elemento. Se toma la primera actividad para hacer el movimiento del trámite por cancelación de la comisión.");
                }

                actividad = actividades.get(0);
                nombreActividad = actividad.getNombre();

                //D: cuando los trámites están en esta actividad no se hace nada
                if (nombreActividad.equals(
                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_DEVUELTO)) {
                    //do nothing;
                } //D: las otras posibles actividades en que podría estar el trámite son: 
                //  Revisar tr{amite asignado, Alistar información, Cargar al SNC
                //  Para estas se debe sustender la actividad del trámite
                else if (nombreActividad.equals(
                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO) ||
                     nombreActividad.equals(ProcesoDeConservacion.ACT_EJECUCION_ALISTAR_INFORMACION) ||
                     nombreActividad.equals(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_AL_SNC)) {

                    ComisionEstado ce;
                    ce = this.getTramiteService().consultarUltimoCambioEstadoComision(
                        this.selectedComision.getId());
                    if (ce == null) {
                        return answer;
                    }

                    Calendar fechaReanudacion, fechaSuspensionHasta;

                    //D: si la suspensión a aplazamiento es a término indefinido o no
                    if (ce.getFechaFin() == null) {
                        fechaReanudacion = null;
                    } else {
                        fechaReanudacion = Calendar.getInstance();
                        fechaReanudacion.setTime(ce.getFechaInicio());
                    }

                    //D: suspender el proceso del trámite
                    fechaSuspensionHasta = null;
                    fechaSuspensionHasta = this.getProcesosService().
                        suspenderActividad(actividad.getId(), fechaReanudacion,
                            "Por suspensión o aplazamiento de la comisión");
                    if (fechaSuspensionHasta == null) {
                        error = true;
                    }
                }
            }
        }
        answer = error ? false : true;

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Reanuda las actividades en el bpm de los trámites relacionados con la comisión que se está
     * reactivando. <br />
     *
     * @author pedro.garcia
     */
    private boolean reanudarActividadesTramites(ArrayList<Tramite> tramitesToMove) {

        boolean answer = false, error = false;

        for (Tramite tramite : tramitesToMove) {

            String idTramite;
            Actividad actividad;

            idTramite = tramite.getId().toString();
            Map<EParametrosConsultaActividades, String> filtros =
                new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
            filtros.put(EParametrosConsultaActividades.ID_TRAMITE, idTramite);
            filtros.put(EParametrosConsultaActividades.ACTIVIDADES_SUSPENDIDAS,
                EEstadoActividad.ACTIVIDADES_SUSPENDIDAS);

            List<Actividad> actividades = null;

            try {
                actividades = this.getProcesosService().consultarListaActividades(filtros);
            } catch (ExcepcionSNC ex) {
                LOGGER.error("Error consultando la lista de actividades para el trámite " +
                    idTramite);
                return answer;
            }

            if (actividades != null && !actividades.isEmpty()) {

                if (actividades.size() > 1) {
                    LOGGER.warn("La lista de actividades para el trámite " + idTramite +
                        " tiene más de un elemento. Se toma la primera actividad para hacer el movimiento del trámite por cancelación de la comisión.");
                }

                actividad = actividades.get(0);
                try {

//TODO :: pedro.garcia refs #5922 :: es tarea de alejandro.sanchez hacer que la reanudación funcione
                    this.getProcesosService().reanudarActividad(actividad.getId());
                } catch (ExcepcionSNC sncEx) {
                    LOGGER.error("Error reanudando la actividad " + actividad.getId());
                    error = true;
                }
            }
        }

        answer = error ? false : true;

        return answer;
    }

// end of class    
}
