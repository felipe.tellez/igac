package co.gov.igac.snc.web.mb.generales;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("process")
@Scope("session")
public class ProcessMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -3848055265334812027L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessMB.class);

    private ProcessFlowManager currentMB;
    private String nombreMB;

    @Autowired
    private TareasPendientesMB tareasPendientes;

    public ProcessFlowManager getCurrentMB() {
        return this.currentMB;
    }

    public void setCurrentMB(ProcessFlowManager currentMB) {
        this.currentMB = currentMB;
    }

    public String getComment() {
        if (this.currentMB != null) {
            if (this.currentMB.getMessage() != null) {
                return this.currentMB.getMessage().getComment();
            }
        }
        return null;
    }

    public void setComment(String comment) {
        if (this.currentMB != null) {
            if (this.currentMB.getMessage() != null) {
                this.currentMB.getMessage().setComment(comment);
            } else {
                this.currentMB.setMessage(new ActivityMessageDTO());
                this.currentMB.getMessage().setComment(comment);
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * llama a la implementación de los métodos abstractos de ProcessFlowManager implementados en el
     * MB que herede de éste, y al método {@link ProcessFlowManager#forwardProcess} que hace el
     * avance del proceso con los MessageDTO creados
     *
     * @return
     */
    public String forwardProcess() {
        LOGGER.debug("Avanzando proceso de trámites...");
        if (this.currentMB.validateProcess()) {
            try {
                this.currentMB.setupProcessMessage();
                this.currentMB.doDatabaseStatesUpdate();
                this.currentMB.forwardProcess();

                //D: reinicializar el managed bean registrado para que al entrar a la tarea se refresquen los datos
                if (this.nombreMB != null) {
                    UtilidadesWeb.removerManagedBean(this.nombreMB);
                    this.nombreMB = null;
                }
                this.tareasPendientes.init();

                return ConstantesNavegacionWeb.INDEX;
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                return null;
            }
        }
        return null;
    }
//----------------------

    /**
     * llama a la implementación de los métodos abstractos de ProcessFlowManager implementados en el
     * MB que herede de éste, y al método ProcessFlowManager#forwardProcess que hace el avance del
     * proceso con los MessageDTO creados
     *
     * Es diferente al forwardProcess porque no debe redirigir a otra página al terminar; por lo
     * tanto retorna nada
     *
     * @author pedro.garcia
     */
    public void forwardIndividualProcess() {
        if (this.currentMB.validateProcess()) {
            this.currentMB.setupProcessMessage();
            this.currentMB.doDatabaseStatesUpdate();
            this.currentMB.forwardProcess();

        }
    }

    public String getNombreMB() {
        return nombreMB;
    }

    public void setNombreMB(String nombreMB) {
        this.nombreMB = nombreMB;
    }

    public String cerrar() {
        if (this.nombreMB != null) {
            UtilidadesWeb.removerManagedBean(this.nombreMB);
            this.nombreMB = null;
        }
        this.tareasPendientes.init();
        return ConstantesNavegacionWeb.INDEX;
    }

}
