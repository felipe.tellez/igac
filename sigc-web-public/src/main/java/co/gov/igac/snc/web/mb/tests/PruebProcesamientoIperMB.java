/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.tests;

import co.gov.igac.snc.web.mb.avaluos.asignacion.ConsultaCargaTrabajoMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("pruebProcesamientoIperMB")
@Scope("session")
public class PruebProcesamientoIperMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 6277978758758305628L;
    private static final Logger LOGGER = LoggerFactory.getLogger(PruebProcesamientoIperMB.class);

    @PostConstruct
    public void init() {
    }

    public void procesarIper() {
        this.getInterrelacionService().procesarPendientes();
    }

}
