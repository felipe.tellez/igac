package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.ArrayUtils;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.MPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EMunicipioActualizacionEstado;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.util.constantes.ConstantesResolucionesTramites;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.EntradaProyeccionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.HistoricoProyeccionViaGubernativaDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.web.util.procesos.ProcesosUtilidadesWeb;

/**
 * Managed bean para el CU Notificar por edicto
 *
 * @author pedro.garcia
 */
/*
 * @modified leidy.gonzalez :: 14043::
 */
@Component("generaResolucion")
@Scope("session")
public class GeneraResolucionMB extends ProcessFlowManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneraResolucionMB.class);
    private static final long serialVersionUID = -6100091631249529023L;

    public static final int MAX_NUMBER_OF_RETRIES = 5; //intentos de avanzar la actividad
    public static final int WAIT_TIME = 1000; //2 SEGUNDOS

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contexto;

    private RevisionProyeccionMB revisionProyeccionMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    /**
     * id del trámite seleccionado de la tabla
     */
    private long currentTramiteId;

    /**
     * trámite sobre el que se está trabajando
     */
    private Tramite currentTramite;

    /**
     * trámite temporal para consultar si ya se ha generado una resolución o no
     */
    private Tramite tramiteTemp;

    /**
     * Variable que almacena el texto resolución de donde traigo el texto considerando y el texto
     * resuelve de la resolución
     */
    private TramiteTextoResolucion tramiteTextoResolucion;

    /**
     * variable que guarda el modelo resoluciòn que se seleccione
     */
    private ModeloResolucion modeloResolucionSeleccionado;

    /**
     * Lista de las plantillas de ModeloResolucion segun el tramite
     */
    private List<ModeloResolucion> modeloResolucionList;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteResolucion;

    /**
     * Número de resolución que se genera al llamar al procedimiento almacenado
     */
    private String numeroResolucion;

    /**
     * Número de resolución que se genera al llamar al procedimiento almacenado
     */
    private Date fechaResolucion;

    /**
     * Solicitud del támite
     */
    private Solicitud solicitudSeleccionada;

    /**
     * lista de usuarios con el rol de coordinadores
     */
    private List<UsuarioDTO> coordinadores;

    /**
     * Bandera que permite saber si se generó el número de resolución
     */
    private boolean resolucionGenerada;

    //-----   BPM  --------
    private Actividad currentProcessActivity;

    private String validateProcessErrorMessage;

    /**
     * Lista que almacena los tramites con radicacion cerrada exitosa
     */
    private List<Tramite> radicacionesCerradasExitosas;

    /**
     * Bandera que habilita las ventanas de proyección de vía gubernativa
     */
    private boolean mostrarProyeccionTramiteViaGubernativa;

    /**
     * Lista que contiene los históricos de proyecciones, usado para tramites de vía gubernativa
     */
    private List<HistoricoProyeccionViaGubernativaDTO> listaTramitesHistoricoProyeccion;

    /**
     * Bandera para habilitar botones Proyectar y Auto de prueba; para vía gubernativa se valida que
     * el predio asociado al recurso de vía gubernativa tenga datos de proyección
     */
    private boolean validacionViaGubernativa;

    /**
     * Lista de trámites sobre los que se va a trabajar en conservacion
     */
    private List<Tramite> listaTramitesConservacion;

    /**
     * Lista de trámites sobre los que se va a trabajar en conservacion
     */
    private List<Tramite> listaTramitesAVisualizar;

    /**
     * Lista de trámites sobre los que se va a trabajar en actualizacion
     */
    private List<Tramite> listaTramitesActualizacion;

    /**
     * Lista de trámites sobre los que se va a trabajar en actualizacion
     */
    private List<Tramite> listaTramitesActualizacionRadicacionExitosa;

    /**
     * Lista de trámites actualizacion sobre los que se van a enviar a AplicarCambios
     */
    private List<Tramite> tramitesActualizacionAplicaCambios;

    /**
     * Lista de trámites actualizacion sobre los que se van a enviar a Comunicar Notificacion
     */
    private List<Tramite> tramitesActualizacionComunicaNotificacion;

    /**
     * Lista de trámites conservacion sobre los que se van a enviar a AplicarCambios
     */
    private List<Tramite> tramitesConservacionAplicaCambios;

    /**
     * Lista de trámites conservacion sobre los que se van a enviar a Comunicar Notificacion
     */
    private List<Tramite> tramitesConservacionComunicaNotificacion;

    /**
     * Bandera que permite saber si los tramites son de actualizacion no importando si esten en
     * conservacion
     */
    private boolean banderaActualizacion;

    /**
     * id's de los trámites seleccionados de la tabla
     */
    private long[] currentTramiteIds;

    /**
     * trámites seleccionados a visualizar en la lista
     */
    private Tramite[] tramitesSeleccionadosAVisualizar;

    /**
     * Bandera que permite saber ver el panel de la resolucion del tramite
     */
    private boolean banderaDetalleResolucion;

    /**
     * Municipio Actualizacion sobre el que se verifica el estado de reactivacion en Process para
     * los tramites de Actualizacion
     */
    private List<MunicipioActualizacion> municipioActualizacion;

    /**
     * Producto Catastral Job sobre el que se verifica el estado del josb de las resoluciones
     * generadas
     */
    private ProductoCatastralJob productoCatastralJob;

    private List<ProductoCatastralJob> productoCatastralJobs;

    /**
     * actividad que corresponde al trámite seleccionado
     */
    private Actividad selectedTramiteActivity;

    private Departamento departamento;

    private Municipio municipio;

    private MPredio mPredioQuinta;

    private Documento documento;

//------------------------------------metodos-------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on GeneraResolucionMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        EstructuraOrganizacional estructuraOrganizacional =
            this.getGeneralesService().buscarEstructuraOrganizacionalPorCodigo(this.currentUser.
                getCodigoEstructuraOrganizacional());

        this.municipio = estructuraOrganizacional.getMunicipio();
        this.departamento = estructuraOrganizacional.getMunicipio().getDepartamento();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "generaResolucion");

        //Obtiene id de Tramites
        this.currentTramiteIds = this.tareasPendientesMB.
            obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        this.banderaActualizacion = false;

        this.tramitesSeleccionadosAVisualizar = new Tramite[]{};

        this.listaTramitesActualizacion = new ArrayList<Tramite>();
        this.listaTramitesConservacion = new ArrayList<Tramite>();
        this.listaTramitesAVisualizar = new ArrayList<Tramite>();

        this.validarActividadTramitesConservacionOActualizacion();

        this.banderaDetalleResolucion = false;
        this.resolucionGenerada = false;

    }

    //--------------------------------------------------------------------------------------------------
    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public MPredio getmPredioQuinta() {
        return mPredioQuinta;
    }

    public void setmPredioQuinta(MPredio mPredioQuinta) {
        this.mPredioQuinta = mPredioQuinta;
    }

    public List<MunicipioActualizacion> getMunicipioActualizacion() {
        return municipioActualizacion;
    }

    public void setMunicipioActualizacion(
        List<MunicipioActualizacion> municipioActualizacion) {
        this.municipioActualizacion = municipioActualizacion;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Actividad getSelectedTramiteActivity() {
        return selectedTramiteActivity;
    }

    public void setSelectedTramiteActivity(Actividad selectedTramiteActivity) {
        this.selectedTramiteActivity = selectedTramiteActivity;
    }

    public boolean isBanderaDetalleResolucion() {
        return banderaDetalleResolucion;
    }

    public void setBanderaDetalleResolucion(boolean banderaDetalleResolucion) {
        this.banderaDetalleResolucion = banderaDetalleResolucion;
    }

    public ProductoCatastralJob getProductoCatastralJob() {
        return productoCatastralJob;
    }

    public void setProductoCatastralJob(ProductoCatastralJob productoCatastralJob) {
        this.productoCatastralJob = productoCatastralJob;
    }

    public List<ProductoCatastralJob> getProductoCatastralJobs() {
        return productoCatastralJobs;
    }

    public void setProductoCatastralJobs(
        List<ProductoCatastralJob> productoCatastralJobs) {
        this.productoCatastralJobs = productoCatastralJobs;
    }

    public Tramite[] getTramitesSeleccionadosAVisualizar() {
        return tramitesSeleccionadosAVisualizar;
    }

    public void setTramitesSeleccionadosAVisualizar(
        Tramite[] tramitesSeleccionadosAVisualizar) {
        this.tramitesSeleccionadosAVisualizar = tramitesSeleccionadosAVisualizar;
    }

    public List<Tramite> getListaTramitesAVisualizar() {
        return listaTramitesAVisualizar;
    }

    public void setListaTramitesAVisualizar(
        List<Tramite> listaTramitesAVisualizar) {
        this.listaTramitesAVisualizar = listaTramitesAVisualizar;
    }

    public List<Tramite> getListaTramitesConservacion() {
        return listaTramitesConservacion;
    }

    public void setListaTramitesConservacion(List<Tramite> listaTramitesConservacion) {
        this.listaTramitesConservacion = listaTramitesConservacion;
    }

    public List<Tramite> getTramitesConservacionAplicaCambios() {
        return tramitesConservacionAplicaCambios;
    }

    public void setTramitesConservacionAplicaCambios(
        List<Tramite> tramitesConservacionAplicaCambios) {
        this.tramitesConservacionAplicaCambios = tramitesConservacionAplicaCambios;
    }

    public List<Tramite> getTramitesConservacionComunicaNotificacion() {
        return tramitesConservacionComunicaNotificacion;
    }

    public void setTramitesConservacionComunicaNotificacion(
        List<Tramite> tramitesConservacionComunicaNotificacion) {
        this.tramitesConservacionComunicaNotificacion = tramitesConservacionComunicaNotificacion;
    }

    public long[] getCurrentTramiteIdsActualizacion() {
        return currentTramiteIds;
    }

    public void setCurrentTramiteIdsActualizacion(
        long[] currentTramiteIdsActualizacion) {
        this.currentTramiteIds = currentTramiteIdsActualizacion;
    }

    public List<Tramite> getListaTramitesActualizacion() {
        return listaTramitesActualizacion;
    }

    public void setListaTramitesActualizacion(
        List<Tramite> listaTramitesActualizacion) {
        this.listaTramitesActualizacion = listaTramitesActualizacion;
    }

    public List<Tramite> getTramitesActualizacionAplicaCambios() {
        return tramitesActualizacionAplicaCambios;
    }

    public void setTramitesActualizacionAplicaCambios(
        List<Tramite> tramitesActualizacionAplicaCambios) {
        this.tramitesActualizacionAplicaCambios = tramitesActualizacionAplicaCambios;
    }

    public List<Tramite> getTramitesActualizacionComunicaNotificacion() {
        return tramitesActualizacionComunicaNotificacion;
    }

    public void setTramitesActualizacionComunicaNotificacion(
        List<Tramite> tramitesActualizacionComunicaNotificacion) {
        this.tramitesActualizacionComunicaNotificacion = tramitesActualizacionComunicaNotificacion;
    }

    public boolean isBanderaActualizacion() {
        return banderaActualizacion;
    }

    public void setBanderaActualizacion(boolean banderaActualizacion) {
        this.banderaActualizacion = banderaActualizacion;
    }

    public List<Tramite> getListaTramites() {
        return listaTramitesActualizacion;
    }

    public void setListaTramites(List<Tramite> listaTramites) {
        this.listaTramitesActualizacion = listaTramites;
    }

    public long getCurrentTramiteId() {
        return currentTramiteId;
    }

    public void setCurrentTramiteId(long currentTramiteId) {
        this.currentTramiteId = currentTramiteId;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public UsuarioDTO getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UsuarioDTO currentUser) {
        this.currentUser = currentUser;
    }

    public Tramite getCurrentTramite() {
        return currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
    }

    public TramiteTextoResolucion getTramiteTextoResolucion() {
        return tramiteTextoResolucion;
    }

    public void setTramiteTextoResolucion(
        TramiteTextoResolucion tramiteTextoResolucion) {
        this.tramiteTextoResolucion = tramiteTextoResolucion;
    }

    public String getNumeroResolucion() {
        return numeroResolucion;
    }

    public void setNumeroResolucion(String numeroResolucion) {
        this.numeroResolucion = numeroResolucion;
    }

    public boolean isResolucionGenerada() {
        return resolucionGenerada;
    }

    public void setResolucionGenerada(boolean resolucionGenerada) {
        this.resolucionGenerada = resolucionGenerada;
    }

    public ReporteDTO getReporteResolucion() {
        return reporteResolucion;
    }

    public void setReporteResolucion(ReporteDTO reporteResolucion) {
        this.reporteResolucion = reporteResolucion;
    }

    public List<Tramite> getRadicacionesCerradasExitosas() {
        return radicacionesCerradasExitosas;
    }

    public boolean isMostrarProyeccionTramiteViaGubernativa() {
        return mostrarProyeccionTramiteViaGubernativa;
    }

    public void setMostrarProyeccionTramiteViaGubernativa(
        boolean mostrarProyeccionTramiteViaGubernativa) {
        this.mostrarProyeccionTramiteViaGubernativa = mostrarProyeccionTramiteViaGubernativa;
    }

    public List<HistoricoProyeccionViaGubernativaDTO> getListaTramitesHistoricoProyeccion() {
        return listaTramitesHistoricoProyeccion;
    }

    public void setListaTramitesHistoricoProyeccion(
        List<HistoricoProyeccionViaGubernativaDTO> listaTramitesHistoricoProyeccion) {
        this.listaTramitesHistoricoProyeccion = listaTramitesHistoricoProyeccion;
    }

    public boolean isValidacionViaGubernativa() {
        return validacionViaGubernativa;
    }

    public void setValidacionViaGubernativa(boolean validacionViaGubernativa) {
        this.validacionViaGubernativa = validacionViaGubernativa;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * * Método encargado de validar si los tramites son de conservacion o actualizacion
     *
     * @author leidy.gonzalez
     */
    public void validarActividadTramitesConservacionOActualizacion() {

        Long[] idTramitesLong = ArrayUtils.toObject(this.currentTramiteIds);
        List<Long> idTramites = Arrays.asList(idTramitesLong);

        //ACTUALIZACION
        //Consulta tramites de actualización
        this.listaTramitesActualizacion =
             this.getTramiteService().buscarTramitesActualizacionAgrupadoPorPredio(
                this.obtenerIdsActivos(idTramites));
        //Consulta tramites de Conservacion
        this.listaTramitesConservacion =
            this.getTramiteService().buscarTramitesConservacionPorIdTramites(idTramites);
        //Valida Rol del usuario
        for (String rol : this.currentUser.getRoles()) {
            if (rol.equals(ERol.RESPONSABLE_ACTUALIZACION.toString())) {
                if (this.listaTramitesActualizacion != null &&
                     !this.listaTramitesActualizacion.isEmpty()) {
                    for (Tramite tramiteTrabajoActualizacion : this.listaTramitesActualizacion) {
                        this.banderaActualizacion = true;
                        this.currentTramite = tramiteTrabajoActualizacion;
                    }
                    this.listaTramitesAVisualizar.addAll(this.listaTramitesActualizacion);
                    this.obtenerTramitesConJob(this.listaTramitesAVisualizar,
                        ProductoCatastralJob.TIPO_RESOLUCIONES_ACTUALIZACION);
                }

            } else if (rol.equals(ERol.RESPONSABLE_CONSERVACION.toString()) ||
                rol.equals(ERol.DIRECTOR_TERRITORIAL.toString())) {

                //Visualizar lista de tramites de conservacion.
                if (this.listaTramitesConservacion != null && !this.listaTramitesConservacion.
                    isEmpty()) {
                    for (Tramite tramiteTrabajoConservacion : this.listaTramitesConservacion) {

                        if (!tramiteTrabajoConservacion.isActualizacion()) {
                            this.banderaActualizacion = false;
                            this.currentTramite = tramiteTrabajoConservacion;

                            List<TramiteDocumento> documentosPruebas = this.getTramiteService()
                                .obtenerTramiteDocumentosDePruebasPorTramiteId(
                                    tramiteTrabajoConservacion.getId());
                            if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                                this.currentTramite.setDocumentosPruebas(documentosPruebas);
                            }

                            if (this.currentTramite.isViaGubernativa()) {

                                this.mostrarProyeccionTramiteViaGubernativa = true;

                                PPredio pp = null;
                                try {
                                    pp = this.getConservacionService().obtenerPPredioCompletoById(
                                        this.currentTramite.getPredio().getId());
                                } catch (Exception ex) {
                                    LOGGER.warn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_004.
                                        getMensajeTecnico());
                                }

                                if (pp != null) {
                                    Tramite tramiteRef = null;
                                    FiltroDatosConsultaTramite filtrosTramite =
                                        new FiltroDatosConsultaTramite();
                                    filtrosTramite.setNumeroRadicacion(tramiteTrabajoConservacion.
                                        getSolicitud().getTramiteNumeroRadicacionRef());
                                    filtrosTramite.setEstadoTramite("0");

                                    List<Tramite> resultado = this.getTramiteService().
                                        consultaDeTramitesPorFiltros(true, filtrosTramite, null,
                                            null, false, null, null, null);

                                    if (resultado != null && !resultado.isEmpty()) {
                                        tramiteRef = resultado.get(0);
                                        this.listaTramitesHistoricoProyeccion =
                                            new ArrayList<HistoricoProyeccionViaGubernativaDTO>();
                                        HistoricoProyeccionViaGubernativaDTO historicoP =
                                            new HistoricoProyeccionViaGubernativaDTO();
                                        historicoP.setTipoProyeccion("Proyección inicial");
                                        tramiteRef = this.getTramiteService().
                                            findTramitePruebasByTramiteId(tramiteRef.getId());
                                        historicoP.setTramiteInfo(tramiteRef);
                                        this.listaTramitesHistoricoProyeccion.add(historicoP);
                                    }
                                    this.validacionViaGubernativa = true;
                                }
                            }
                        }
                    }
                    this.listaTramitesAVisualizar.addAll(this.listaTramitesConservacion);

                    this.obtenerTramitesConJob(this.listaTramitesAVisualizar,
                        ProductoCatastralJob.TIPO_RESOLUCIONES_CONSERVACION);

                }
                //Visualizar el ultimo tramite de ese predio para tramites de actualizacion.
                if (this.listaTramitesActualizacion != null && !this.listaTramitesActualizacion.
                    isEmpty()) {
                    this.listaTramitesAVisualizar.addAll(this.listaTramitesActualizacion);
                    this.obtenerTramitesConJob(this.listaTramitesAVisualizar,
                        ProductoCatastralJob.TIPO_RESOLUCIONES_ACTUALIZACION);
                }

            }

        }

    }

    /**
     * * Método encargado de obtener los job de los tramites asoiados
     *
     * @author leidy.gonzalez
     */
    public void obtenerTramitesConJob(List<Tramite> tramites, String tipoProducto) {
        Tramite tramiteAObtenerId = null;
        List<Long> tramitesIds = new ArrayList<Long>();

        if (this.listaTramitesAVisualizar != null && !this.listaTramitesAVisualizar.isEmpty()) {

            for (int i = 0; i < this.listaTramitesAVisualizar.size(); i++) {
                tramiteAObtenerId = this.listaTramitesAVisualizar.get(i);
                tramitesIds.add(tramiteAObtenerId.getId());
            }
        }
        this.productoCatastralJobs = this.getGeneralesService().obtenerUltimoJobPorIdTramites(
            tramitesIds, tipoProducto);

        if (this.productoCatastralJobs != null && !this.productoCatastralJobs.isEmpty()) {

            for (ProductoCatastralJob productoCatastralJob : this.productoCatastralJobs) {

                for (Tramite tramiteJob : this.listaTramitesAVisualizar) {
                    if (productoCatastralJob.getTramiteId().equals(tramiteJob.getId())) {
                        tramiteJob.setEstadoResolucion(ESiNo.SI.getCodigo());
                    }
                }
            }

        }
    }

    /**
     ** Método encargado de refrescar la lista de tramites a visualizar por el usuario
     *
     * @author leidy.gonzalez
     */
    public void refrescarEstadoTramites() {

        this.banderaActualizacion = false;

        this.tramitesSeleccionadosAVisualizar = new Tramite[]{};

        this.listaTramitesActualizacion = new ArrayList<Tramite>();
        this.listaTramitesConservacion = new ArrayList<Tramite>();
        this.listaTramitesAVisualizar = new ArrayList<Tramite>();

        this.validarActividadTramitesConservacionOActualizacion();

        this.banderaDetalleResolucion = false;
        this.resolucionGenerada = false;
    }

    /**
     * Metodo que genera lar resoluciones
     *
     * @modified by leidy.gonzalez 22/10/2014 Se valida que se guarde la fecha de inscripción en los
     * predios proyectados para los tramites de Tercera,
     * Complementacion,ModificacionInscripcionCatastral,RevisionAvaluo
     * @return
     */
    public void generarResolucion() {
        try {

            //Se llama el método para generar el número de resolución 
            this.generarNumeroResolucionYGuardarDocumento();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return;
        }
    }

    /**
     * Método encargado de generar el número de resolución, retorna un documento con los datos de la
     * resolución.
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez
     */
    private List<Documento> generarNumeroResolucionYGuardarDocumento() {
        List<Documento> listaDocumentos = new ArrayList<Documento>();

        if (this.tramitesSeleccionadosAVisualizar != null) {
            for (Tramite tramiteSeleccionadosAVisualizar : this.tramitesSeleccionadosAVisualizar) {

                if (tramiteSeleccionadosAVisualizar.isEstadoProcesoResolucion()) {
                    this.addMensajeWarn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_001.
                        getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_001.getMensajeTecnico());
                    return null;
                }

                tramiteSeleccionadosAVisualizar.setEstadoResolucion("SI");

                this.getTramiteService().actualizarTramite(tramiteSeleccionadosAVisualizar);

                if (tramiteSeleccionadosAVisualizar.isActualizacion()) {
                    this.currentTramite = tramiteSeleccionadosAVisualizar;

                    if (tramiteSeleccionadosAVisualizar.getSolicitud().isViaGubernativa()) {
                        try {
                            this.documento = this.getGeneralesService()
                                    .buscarDocumentoPorTramiteIdyTipoDocumento(
                                            tramiteSeleccionadosAVisualizar.getId(),
                                            ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                                    getId());
                        } catch (Exception e) {
                            LOGGER.error("Error al consultar el cosumento de resolucion para revocatoria" +
                                    "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                        }
                    } else {

                        try {
                            this.documento = this
                                    .getGeneralesService()
                                    .buscarDocumentoPorTramiteIdyTipoDocumento(
                                            tramiteSeleccionadosAVisualizar.getId(),
                                            ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                                    getId());
                        } catch (Exception e) {
                            LOGGER.error("Error al consultar el cosumento de resolucion" +
                                    "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                        }
                    }

                    if (this.documento == null) {

                        try {
                            Object[] resultado = this.getGeneralesService()
                                    .generarNumeracion(
                                            ENumeraciones.NUMERACION_RESOLUCION, "",
                                            tramiteSeleccionadosAVisualizar.getDepartamento().getCodigo(),
                                            tramiteSeleccionadosAVisualizar.getMunicipio().getCodigo(), 0);
                            this.numeroResolucion = resultado[1].toString();
                            this.fechaResolucion = new Date(System.currentTimeMillis());
                            LOGGER.debug("Número de Resolución:" + this.numeroResolucion);

                        } catch (Exception e) {
                            LOGGER.error("Error al generar la numeracion de la resolucion de actualizacion" +
                                    "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                        }
                    } else {
                        this.numeroResolucion = this.documento.getNumeroDocumento();
                        this.fechaResolucion = this.documento.getFechaDocumento();
                    }

                    if (this.numeroResolucion != null && this.documento == null) {

                        this.documento = new Documento();

                        TipoDocumento tipoDocumento = new TipoDocumento();
                        // ETipoDocumento Actualizacion
                        tipoDocumento
                            .setId(
                                ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                    getId());

                        if (tramiteSeleccionadosAVisualizar.getSolicitud()
                            .isViaGubernativa()) {
                            tipoDocumento
                                .setId(
                                    ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                        getId());
                        }

                        this.documento.setTipoDocumento(tipoDocumento);
                        this.documento.setEstado(EDocumentoEstado.RESOLUCION_EN_PROYECCION.
                            getCodigo());
                        this.documento.setTramiteId(tramiteSeleccionadosAVisualizar.getId());

                    }

                    this.documento.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                    this.documento.setBloqueado(ESiNo.NO.getCodigo());
                    this.documento.setUsuarioCreador(this.currentUser.getLogin());
                    this.documento.setUsuarioLog(this.currentUser.getLogin());
                    this.documento.setFechaLog(new Date(System.currentTimeMillis()));
                    this.documento.setNumeroDocumento(this.numeroResolucion);
                    this.documento.setFechaDocumento(this.fechaResolucion);
                    this.documento.setEstructuraOrganizacionalCod(this.currentUser.
                        getCodigoEstructuraOrganizacional());

                    try {
                        this.documento = this.getGeneralesService().guardarDocumento(documento);
                        listaDocumentos.add(documento);
                    } catch (Exception e) {
                        LOGGER.error("Error al guardar documento de resolucion de actualizacion" +
                                "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                    }

                    if (this.documento != null && this.documento.getId() != null) {
                        this.generarReporteResolucion();
                    } else {
                        this.addMensajeWarn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_002.
                            getMensajeUsuario());
                        LOGGER.warn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_002.getMensajeTecnico());
                        return null;
                    }

                } else if (!tramiteSeleccionadosAVisualizar.isActualizacion()) {
                    this.currentTramite = tramiteSeleccionadosAVisualizar;

                    if (tramiteSeleccionadosAVisualizar.getSolicitud().isViaGubernativa()) {

                        try {
                            this.documento = this
                                    .getGeneralesService()
                                    .buscarDocumentoPorTramiteIdyTipoDocumento(
                                            tramiteSeleccionadosAVisualizar.getId(),
                                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                                    getId());
                        } catch (Exception e) {
                            LOGGER.error("Error al consultar documento de resolucion de conservacion de revocatoria" +
                                    "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                        }

                    } else {

                        try {
                            this.documento = this
                                    .getGeneralesService()
                                    .buscarDocumentoPorTramiteIdyTipoDocumento(
                                            tramiteSeleccionadosAVisualizar.getId(),
                                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                                    getId());
                        } catch (Exception e) {
                            LOGGER.error("Error al consultar documento de resolucion de conservacion" +
                                    "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                        }
                    }

                    if (this.documento == null) {

                        try {
                            Object[] resultado = this.getGeneralesService()
                                    .generarNumeracion(
                                            ENumeraciones.NUMERACION_RESOLUCION, "",
                                            tramiteSeleccionadosAVisualizar.getDepartamento().getCodigo(),
                                            tramiteSeleccionadosAVisualizar.getMunicipio().getCodigo(), 0);
                            this.numeroResolucion = resultado[1].toString();
                            this.fechaResolucion = new Date(System.currentTimeMillis());
                            LOGGER.debug("Número de Resolución:" + this.numeroResolucion);
                        } catch (Exception e) {
                            LOGGER.error("Error al generar la numeracion de la resolucion de conservacion" +
                                    "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                        }

                    } else {
                        this.numeroResolucion = this.documento.getNumeroDocumento();
                        this.fechaResolucion = this.documento.getFechaDocumento();
                    }

                    if (this.numeroResolucion != null && this.documento == null) {

                        this.documento = new Documento();

                        TipoDocumento tipoDocumento = new TipoDocumento();
                        // ETipoDocumento Actualizacion
                        tipoDocumento
                            .setId(
                                ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                    getId());

                        if (tramiteSeleccionadosAVisualizar.getSolicitud()
                            .isViaGubernativa()) {
                            tipoDocumento
                                .setId(
                                    ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                        getId());
                        }

                        this.documento.setTipoDocumento(tipoDocumento);
                        this.documento.setEstado(EDocumentoEstado.RESOLUCION_EN_PROYECCION.
                            getCodigo());
                        this.documento.setTramiteId(tramiteSeleccionadosAVisualizar.getId());

                        cerrarRadicacionTramite(tramiteSeleccionadosAVisualizar);

                    }

                    this.documento.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                    this.documento.setBloqueado(ESiNo.NO.getCodigo());
                    this.documento.setUsuarioCreador(this.currentUser.getLogin());
                    this.documento.setUsuarioLog(this.currentUser.getLogin());
                    this.documento.setFechaLog(new Date(System.currentTimeMillis()));
                    this.documento.setNumeroDocumento(this.numeroResolucion);
                    this.documento.setFechaDocumento(this.fechaResolucion);
                    this.documento.setEstructuraOrganizacionalCod(this.currentUser.
                        getCodigoEstructuraOrganizacional());

                    try {
                        this.documento = this.getGeneralesService().guardarDocumento(documento);
                        listaDocumentos.add(documento);
                    } catch (Exception e) {
                        LOGGER.error("Error al guardar el documento de la resolucion de conservacion" +
                                "GenerarResolucionMB#generarNumeroResolucionYGuardarDocumento.", e);

                    }

                    if (this.documento != null && this.documento.getId() != null) {
                        this.generarReporteResolucion();
                    } else {
                        this.addMensajeWarn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_002.
                            getMensajeUsuario());
                        LOGGER.warn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_002.getMensajeTecnico());
                        return null;
                    }

                    if (tramiteSeleccionadosAVisualizar.isTercera()) {
                        tramiteSeleccionadosAVisualizar.setFechaInscripcionCatastral(new Date());
                        this.getTramiteService().actualizarTramite(tramiteSeleccionadosAVisualizar);
                        this.actualizaFechaInscripcionCatastralPPredio();
                    }
                }
            }
        } else {
            LOGGER.warn(ECodigoErrorVista.GENERA_RESOLUCION_ACT_003.getMensajeTecnico());
        }

        return listaDocumentos;
    }

    private void cerrarRadicacionTramite(final Tramite tramite) {
        final List<Documento> listaDocumentos = new ArrayList<Documento>();
        List<Documento> listaMemoAprobacion = this.getGeneralesService()
            .buscarDocumentoPorNumeroDocumentoYTipo(String.valueOf(tramite.getId()),
                ETipoDocumento.MEMORANDO_DE_APROBACION_DE_COMISION.getId());
        List<Documento> listaMemoSolicitud = this.getGeneralesService()
            .buscarDocumentoPorNumeroDocumentoYTipo(String.valueOf(tramite.getId()),
                ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.getId());

        List<Documento> listaDocComision = this.getTramiteService()
            .obtenerDocumentosComisionPorTramiteId(tramite.getId());

        listaDocumentos.addAll(listaMemoAprobacion);
        listaDocumentos.addAll(listaMemoSolicitud);
        listaDocumentos.addAll(listaDocComision);

        Documento documentoFinalizaRadicacion = null;
        if (listaDocumentos != null && !listaDocumentos.isEmpty()) {
            for (final Documento doc : listaDocumentos) {
                cerrarRadicacionMemorando(doc, tramite);
                documentoFinalizaRadicacion = doc;
            }
        }
        if (documentoFinalizaRadicacion != null && tramite.getSolicitud() != null) {
            cerrarRadicacionExterna(documentoFinalizaRadicacion, tramite);
        }

    }

    private void cerrarRadicacionExterna(final Documento doc, final Tramite tramite) {
        final String radicadoResponde = obtenerNumeroRadicado(tramite.getSolicitud());
        final String radicado = tramite.getSolicitud().getNumero();
        finalizarRadicacionInterno(radicado, radicadoResponde, this.currentUser.getLogin(),
            "EXTERNA_ENVIADA");
    }

    private void cerrarRadicacionMemorando(final Documento doc, final Tramite tramite) {
        final String radicado = doc.getNumeroRadicacion();
        final String radicadoResponde = tramite.getSolicitud().getNumero();
        finalizarRadicacionInterno(radicado, radicadoResponde, this.currentUser.getLogin(),
            "INTERNA_ENVIADA");
    }

    private boolean finalizarRadicacionInterno(final String radicado,
        final String radicadoResponde, final String usuario, final String tipoCierre) {
        boolean resultado = true;
        Object[] resultadoCierre = null;
        if ("INTERNA_ENVIADA".equals(tipoCierre)) {
            resultadoCierre = this.getTramiteService()
                .finalizarRadicacionInterno(radicado, radicadoResponde, usuario);
        } else {
            resultadoCierre = this.getTramiteService()
                .finalizarRadicacion(radicado, radicadoResponde, usuario);
        }

        if (resultadoCierre != null) {
            for (final Object objeto : resultadoCierre) {
                final List lista = (List) objeto;
                if (lista != null && !lista.isEmpty()) {
                    int numeroErrores = 0;
                    for (final Object obj : lista) {
                        final Object[] arrObjetos = (Object[]) obj;
                        //TODO: Eliminar la validación del código de error de oracle luego de que 
                        //el servicio de CORDIS solucione el error: http://redmine.igac.gov.co/issues/21662
                        if (!arrObjetos[0].equals("0") &&
                            !arrObjetos[1].toString().contains("0-ORA-0000")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + arrObjetos[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al cerrar radicación");
                        resultado = false;
                    }
                }
            }
        }
        return resultado;
    }

    /**
     * Permite obsevar la previsualizacion del repote
     *
     * @author leidy.gonzalez
     *
     */
    public void generarReportePrevisualizacionResolucion() {

        FirmaUsuario firma = null;
        String documentoFirma = null;
        EReporteServiceSNC enumeracionReporte = null;
        Map<String, String> parameters = new HashMap<String, String>();

        if (this.currentTramite != null &&
            ((this.tramiteTextoResolucion.getConsiderando() != null &&
             this.tramiteTextoResolucion.getResuelve() != null &&
             this.tramiteTextoResolucion.getId() == null) ||
             (this.tramiteTextoResolucion.getId() != null &&
            this.tramiteTextoResolucion.getTitulo() != null &&
             this.tramiteTextoResolucion.getTitulo().equals(
                ConstantesResolucionesTramites.SIN_PLANTILLA) &&
             this.tramiteTextoResolucion.getConsiderando() != null &&
             this.tramiteTextoResolucion.getResuelve() != null))) {

            this.tramiteTextoResolucion.setFechaLog(new Date(System
                .currentTimeMillis()));
            this.tramiteTextoResolucion.setUsuarioLog(this.currentUser.getLogin());

            this.tramiteTextoResolucion.setTramite(this.currentTramite);

            this.tramiteTextoResolucion
                .setModeloResolucion(this.modeloResolucionSeleccionado);

        }

        if (this.tramiteTextoResolucion != null) {
            try {
                // Guardar Texto Resolución
                this.tramiteTextoResolucion = this.getTramiteService()
                    .guardarTramiteTextoResolucion(
                        this.tramiteTextoResolucion);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                this.addMensajeError(
                    "Hubo un error al guardar el texto motivo, por favor intente nuevamente");
                return;
            }
        }

        if (this.currentTramite != null &&
             this.currentTramite.isActualizacion()) {

            enumeracionReporte = this
                .getTramiteService()
                .obtenerUrlReporteResolucionesActualizacion(
                    this.currentTramite.getTipoTramite(),
                    this.currentTramite.getClaseMutacion(),
                    this.currentTramite.getSubtipo(),
                    this.currentTramite.getRadicacionEspecial(),
                    this.currentTramite
                        .isRectificacionCancelacionDobleInscripcion());

            if (enumeracionReporte != null) {

                parameters.put("TRAMITE_ID", "" + this.currentTramite.getId());

                parameters.put("COPIA_USO_RESTRINGIDO", "false");

                parameters.put("NOMBRE_RESPONSABLE_ACTUALIZACION",
                    this.currentUser.getNombreCompleto());
                
                parameters.put("CODIGO_ESTRUCTURA_ORGANIZACIONAL",
                    this.currentUser.getCodigoEstructuraOrganizacional());
                
                parameters.put("DELEGADA","FALSE");
                if (this.currentUser.getDescripcionTerritorial().contains(
                    Constantes.PREFIJO_DELEGADOS)) {
                    parameters.put("DELEGADA", "TRUE");
                }
                
                parameters.put("ES_ACTUALIZACION","FALSE");
                if (this.currentUser.getRolesCadena().toLowerCase().contains(ERol.RESPONSABLE_ACTUALIZACION.
                    getRol().toLowerCase())) {
                    parameters.put("ES_ACTUALIZACION","TRUE");
                }
                
                if (this.numeroResolucion != null) {

                    parameters.put("NUMERO_RESOLUCION", this.numeroResolucion);
                    parameters.put("FECHA_RESOLUCION",
                        Constantes.FORMAT_FECHA_RESOLUCION
                            .format(this.fechaResolucion));
                    parameters.put("FECHA_RESOLUCION_CON_FORMATO",
                        Constantes.FORMAT_FECHA_RESOLUCION_2
                            .format(this.fechaResolucion));

                }
            }
            if (this.currentUser != null &&
                 this.currentUser.getNombreCompleto() != null) {

                firma = this.getGeneralesService()
                    .buscarDocumentoFirmaPorUsuario(
                        this.currentUser.getNombreCompleto());

                if (firma != null &&
                     this.currentUser.getNombreCompleto().equals(
                        firma.getNombreFuncionarioFirma())) {

                    documentoFirma = this.getGeneralesService()
                        .descargarArchivoAlfrescoYSubirAPreview(
                            firma.getDocumentoId()
                                .getIdRepositorioDocumentos());

                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }
            }

            this.reporteResolucion = reportsService.generarReporte(
                parameters, enumeracionReporte, this.currentUser);

        } else if (this.currentTramite != null &&
             !this.currentTramite.isActualizacion()) {

            enumeracionReporte = this
                .getTramiteService()
                .obtenerUrlReporteResoluciones(this.currentTramite.getId());

            if (enumeracionReporte != null) {

                parameters.put("TRAMITE_ID", "" + this.currentTramite.getId());

                parameters.put("COPIA_USO_RESTRINGIDO", "false");

                parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                    this.currentUser.getNombreCompleto());

                if (this.currentTramite.isTipoTramiteViaGubernativaModificado() ||
                    this.currentTramite.isTipoTramiteViaGubModSubsidioApelacion() ||
                     this.currentTramite.isRecursoApelacion()) {

                    if (!this.currentTramite.getNumeroRadicacionReferencia().isEmpty()) {

                        Tramite tramiteReferencia = this.getTramiteService().
                            buscarTramitePorNumeroRadicacion(this.currentTramite.
                                getNumeroRadicacionReferencia());

                        if (tramiteReferencia != null) {
                            parameters.put("NOMBRE_SUBREPORTE_RESUELVE", this.getTramiteService().
                                obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.getId()));
                        }
                    }

                    UsuarioDTO director = null;
                    List<UsuarioDTO> directoresTerritoriales = null;

                    directoresTerritoriales = (List<UsuarioDTO>) this
                        .getTramiteService()
                        .buscarFuncionariosPorRolYTerritorial(
                            this.currentUser
                                .getDescripcionEstructuraOrganizacional(),
                            ERol.DIRECTOR_TERRITORIAL);

                    if (directoresTerritoriales != null &&
                         !directoresTerritoriales.isEmpty() &&
                         directoresTerritoriales.get(0) != null) {
                        for (UsuarioDTO udto : directoresTerritoriales) {

                            if (ELDAPEstadoUsuario.ACTIVO.codeExist(udto.getEstado())) {
                                director = udto;
                            }
                        }
                    }

                    //Se valida si el usuario logeado es el director territorial
                    //Si no lo es se envia como parametro el responsable de Conservacion
                    if (director != null &&
                         director.getNombreCompleto() != null &&
                         director.getNombreCompleto().isEmpty() &&
                         this.currentUser.getLogin() != null &&
                         this.currentUser.getLogin().equals(director.getLogin())) {

                        parameters.put("DIRECTOR_TERRITORIAL", director.getNombreCompleto());

                    }

                    firma = this.getGeneralesService()
                        .buscarDocumentoFirmaPorUsuario(
                            director.getNombreCompleto());

                    if (firma != null &&
                         director.getNombreCompleto().equals(
                            firma.getNombreFuncionarioFirma())) {

                        documentoFirma = this.getGeneralesService()
                            .descargarArchivoAlfrescoYSubirAPreview(
                                firma.getDocumentoId()
                                    .getIdRepositorioDocumentos());

                        parameters.put("FIRMA_USUARIO", documentoFirma);
                    }

                }

                if (this.numeroResolucion != null) {

                    SolicitudCatastral objNegocio;

                    UsuarioDTO coordinador = null;

                    if (this.coordinadores == null) {
                        objNegocio = this
                            .getProcesosService()
                            .obtenerObjetoNegocioConservacion(
                                this.currentTramite
                                    .getProcesoInstanciaId(),
                                ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);

                        if (objNegocio != null) {
                            this.coordinadores = (List<UsuarioDTO>) objNegocio
                                .getUsuarios();
                        }
                    }
                    if (this.coordinadores != null &&
                         !this.coordinadores.isEmpty() &&
                         this.coordinadores.get(0) != null &&
                         this.coordinadores.get(0).getLogin() != null) {
                        coordinador = this.getGeneralesService()
                            .getCacheUsuario(
                                this.coordinadores.get(0).getLogin());
                    }

                    if (coordinador != null &&
                         coordinador.getNombreCompleto() != null) {
                        parameters.put("NOMBRE_REVISOR",
                            coordinador.getNombreCompleto());
                    } else {
                        parameters.put("NOMBRE_REVISOR", null);
                    }

                    parameters.put("NUMERO_RESOLUCION", this.numeroResolucion);
                    parameters.put("FECHA_RESOLUCION",
                        Constantes.FORMAT_FECHA_RESOLUCION
                            .format(this.fechaResolucion));
                    parameters.put("FECHA_RESOLUCION_CON_FORMATO",
                        Constantes.FORMAT_FECHA_RESOLUCION_2
                            .format(this.fechaResolucion));

                }
            }

            if (this.currentUser != null &&
                 this.currentUser.getNombreCompleto() != null &&
                 !(this.currentTramite.isTipoTramiteViaGubernativaModificado() ||
                this.currentTramite.isTipoTramiteViaGubModSubsidioApelacion() ||
                 this.currentTramite.isRecursoApelacion() || this.currentTramite.isViaGubernativa())) {

                firma = this.getGeneralesService()
                    .buscarDocumentoFirmaPorUsuario(
                        this.currentUser.getNombreCompleto());

                if (firma != null &&
                     this.currentUser.getNombreCompleto().equals(
                        firma.getNombreFuncionarioFirma())) {

                    documentoFirma = this.getGeneralesService()
                        .descargarArchivoAlfrescoYSubirAPreview(
                            firma.getDocumentoId()
                                .getIdRepositorioDocumentos());

                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }
            }

            this.reporteResolucion = reportsService.generarReporte(
                parameters, enumeracionReporte, this.currentUser);
        }
    }

    /**
     *
     * @modified by javier.aponte 21/02/2014
     *
     * @modified by javier.aponte #7249 se genera la resolución sin marca de agua 11/03/2014
     *
     * @modified by leidy.gonzalez: #13244 :: 08/07/2015 se adiciona firma de usuario, si esta
     * existe al reporte
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
 /*
     * @modified by leidy.gonzalez
     */
 /*
     * Se elimina la generación de la firma escaneada en este punto para generarla sólo cuando se
     * vaya a generar el reporte @modified by javier.aponte 12/09/2016 #19708
     */
    public void generarReporteResolucion() {

        Map<String, String> parameters = new HashMap<String, String>();
        boolean resultadoOk = false;
        int contador = 0;

        if (this.currentTramite != null) {

            if (this.tramiteTemp == null) {
                this.tramiteTemp = this.getTramiteService().
                    buscarTramiteTraerDocumentoResolucionPorIdTramite(this.currentTramite.getId());
            }

            try {

                if (tramiteTemp != null && (tramiteTemp.isTercera() ||
                     this.tramiteTemp.isEsComplementacion() ||
                     this.tramiteTemp.isModificacionInscripcionCatastral() ||
                     this.tramiteTemp.isRevisionAvaluo()) &&
                     !this.tramiteTemp.isActualizacion()) {

                    PPredio pp = null;
                    pp = this.getConservacionService().obtenerPPredioCompletoById(
                        this.currentTramite.getPredio().getId());

                    pp.setFechaInscripcionCatastral(new Date(System
                        .currentTimeMillis()));
                    this.getConservacionService().guardarActualizarPPredio(pp);
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return;
            }

            try {
                this.selectedTramiteActivity = this.tareasPendientesMB
                        .buscarActividadPorIdTramiteSeleccionadas(this.currentTramite.getId());

                if (this.selectedTramiteActivity == null) {
                    this.selectedTramiteActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
                            this.currentTramite.getId());
                }

            } catch (Exception e) {
                LOGGER.error("Error al consultar la actividad por el id del tramite" +
                        "GenerarResolucionMB#generarReporteResolucion.", e);
            }

            this.tramiteTextoResolucion = this.getTramiteService()
                .buscarTramiteTextoResolucionPorIdTramite(this.currentTramite.getId());

            if (this.currentTramite.isActualizacion()) {

                if (this.tramiteTextoResolucion == null) {

                    this.consultarModeloResolucionSegunTipoDeTramite();
                    this.establecerConsiderandoYResuelve();
                    this.guardarTramiteTextoResolucion();

                }

                parameters.put("TRAMITE_ID", "" + this.currentTramite.getId());

                parameters.put("COPIA_USO_RESTRINGIDO", "false");

                parameters.put("NOMBRE_RESPONSABLE_ACTUALIZACION", this.currentUser.
                    getNombreCompleto());

                for (String rol : this.currentUser.getRoles()) {
                    if (rol.equals(ERol.RESPONSABLE_ACTUALIZACION.toString())) {
                        parameters.put("FUNCIONARIO_ACTUALIZACION",
                            "FUNCIONARIO_ACTUALIZACION");
                    }
                }

                if (this.numeroResolucion != null) {

                    parameters.put("NUMERO_RESOLUCION", this.numeroResolucion);
                    parameters.put("FECHA_RESOLUCION", Constantes.FORMAT_FECHA_RESOLUCION.format(
                        this.fechaResolucion));
                    parameters.put("FECHA_RESOLUCION_CON_FORMATO",
                        Constantes.FORMAT_FECHA_RESOLUCION_2.format(this.fechaResolucion));

                }



                //llama creacion de job
                if (this.selectedTramiteActivity != null && this.tramiteTextoResolucion != null &&
                     this.tramiteTextoResolucion.getId() != null && this.tramiteTextoResolucion.
                    getTramite() != null &&
                     this.tramiteTextoResolucion.getTramite().getId() != null &&
                     this.currentTramite.getId().equals(this.tramiteTextoResolucion.getTramite().
                        getId())) {

                    LOGGER.debug("Id Actividad del Tramite: " + this.selectedTramiteActivity.getId());

                    try {

                        this.productoCatastralJob = this.getActualizacionService()
                                .crearJobGenerarResolucionesActualizacion(this.currentTramite, parameters,
                                        this.selectedTramiteActivity, this.currentUser);
                    } catch (Exception e) {
                        LOGGER.error("Error al generar el producto catastral job para resoluciones de actualizacion" +
                                "GenerarResolucionMB#generarReporteResolucion.", e);
                    }

                    this.tramiteTextoResolucion = null;
                    return;

                } else {
                    LOGGER.debug("No existe actividad para ese Tramite");
                    return;
                }

            } else if (!this.currentTramite.isActualizacion()) {

                if (this.tramiteTextoResolucion == null) {

                    this.consultarModeloResolucionSegunTipoDeTramite();
                    this.establecerConsiderandoYResuelve();
                    this.guardarTramiteTextoResolucion();

                }

                parameters.put("TRAMITE_ID", "" + this.currentTramite.getId());

                parameters.put("COPIA_USO_RESTRINGIDO", "false");

                parameters.put("NOMBRE_RESPONSABLE_CONSERVACION", this.currentUser.
                    getNombreCompleto());

                if (this.currentUser.getCodigoEstructuraOrganizacional() != null &&
                     !this.currentUser.getCodigoEstructuraOrganizacional().isEmpty()) {
                    parameters.put("CODIGO_ESTRUCTURA_ORGANIZACIONAL", this.currentUser.
                        getCodigoEstructuraOrganizacional());
                }

                if (this.numeroResolucion != null) {

                    LOGGER.debug("Numero resolucion: " + this.numeroResolucion);

                    SolicitudCatastral objNegocio;

                    UsuarioDTO coordinador = null;

                    if (this.coordinadores == null) {

                        try {
                            objNegocio = this.getProcesosService()
                                    .obtenerObjetoNegocioConservacion(this.currentTramite.
                                                    getProcesoInstanciaId(),
                                            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);

                            if (objNegocio != null) {
                                this.coordinadores = (List<UsuarioDTO>) objNegocio.getUsuarios();
                            }
                        } catch (Exception e) {
                            LOGGER.error("Error al obtener el objeto de negocio para los coordinadores " +
                                    "en resoluciones de conservacion" +
                                    "GenerarResolucionMB#generarReporteResolucion.", e);
                        }
                    }

                    if (this.coordinadores != null && !this.coordinadores.isEmpty() &&
                         this.coordinadores.get(0) != null &&
                        this.coordinadores.get(0).getLogin() != null) {
                        coordinador = this.getGeneralesService().getCacheUsuario(this.coordinadores.
                            get(0).getLogin());
                    }

                    if (coordinador != null && coordinador.getNombreCompleto() != null) {
                        parameters.put("NOMBRE_REVISOR", coordinador.getNombreCompleto());
                    } else {
                        parameters.put("NOMBRE_REVISOR", null);
                    }

                    parameters.put("NUMERO_RESOLUCION", this.numeroResolucion);
                    parameters.put("FECHA_RESOLUCION", Constantes.FORMAT_FECHA_RESOLUCION.format(
                        this.fechaResolucion));
                    parameters.put("FECHA_RESOLUCION_CON_FORMATO",
                        Constantes.FORMAT_FECHA_RESOLUCION_2.format(this.fechaResolucion));

                }

                //llama creacion de job
                if (this.selectedTramiteActivity != null && this.tramiteTextoResolucion != null &&
                     this.tramiteTextoResolucion.getId() != null && this.tramiteTextoResolucion.
                    getTramite() != null &&
                     this.tramiteTextoResolucion.getTramite().getId() != null &&
                     this.currentTramite.getId().equals(this.tramiteTextoResolucion.getTramite().
                        getId())) {

                    LOGGER.debug("Id Actividad del tramite : " + this.selectedTramiteActivity.getId());

                    try {
                        this.productoCatastralJob = this.getConservacionService()
                                .crearJobGenerarResolucionesConservacion(this.currentTramite, parameters,
                                        this.selectedTramiteActivity, this.currentUser);
                    } catch (Exception e) {
                        LOGGER.error("Error al crear el producto catastral job para " +
                                "resoluciones de conservacion" +
                                "GenerarResolucionMB#generarReporteResolucion.", e);
                    }

                    this.tramiteTextoResolucion = null;

                } else {
                    LOGGER.debug("No existe actividad para ese Tramite");
                    return;
                }

                if (this.currentTramite.isCancelacionMasiva() &&
                     this.productoCatastralJob != null && this.productoCatastralJob.getEstado() !=
                    null &&
                     this.productoCatastralJob.getEstado().
                        equals(ProductoCatastralJob.JPS_ESPERANDO)) {

                    LOGGER.debug(
                        " Inicio: GenerarResolucion:procesarResolucionesCancelacionMasivaEnJasperServer" +
                         this.currentTramite.getId() + " job: " + this.productoCatastralJob.getId());

                    try {
                    resultadoOk = this.getConservacionService().
                        procesarResolucionesCancelacionMasivaEnJasperServer(
                            this.productoCatastralJob, this.currentTramite, contador);

                    } catch (Exception e) {
                        LOGGER.error("Error al procesar Resoluciones de conservacion " +
                                "procesarResolucionesCancelacionMasivaEnJasperServer" +
                                "GenerarResolucionMB#generarReporteResolucion.", e);
                    }

                    LOGGER.debug(
                        " Fin: GenerarResolucion:procesarResolucionesCancelacionMasivaEnJasperServer" +
                         this.currentTramite.getId() + " job: " + this.productoCatastralJob.getId());

                    if (!resultadoOk) {
                        LOGGER.error(
                            "Fallo la generacion de la resolucion para el Tramite de Cancelacion con id:" +
                             this.currentTramite.getId());
                        return;
                    } else {
                        LOGGER.debug(
                            "Se genero con exito la resolucion Tramite de Cancelacion con id: " +
                             this.currentTramite.getId());
                        return;
                    }
                }
            }
        } else {
            //Msj: Debe seleccionar tramite a Generar reporte
        }

    }

    /**
     * Método que crea un documento. Luego guarda el documento en Alfresco y en la base de datos
     * mediante el llamado al método guardar documento de la interfaz de ITramite. Finalmente como
     * el documento es una resolución le asocia el documento al trámite y actualiza el trámite
     *
     * @throws Exception
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez
     */
    public void guardarDocumentoResolucion(Documento documento) throws Exception {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();

        try {

            tramiteDocumento.setTramite(this.currentTramite);
            tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
            tramiteDocumento.setUsuarioLog(this.currentUser.getLogin());

            documento.setArchivo(this.reporteResolucion.getRutaCargarReporteAAlfresco());

            // Se sube el archivo al gestor documental y se actualiza el
            // documento
            documento = this.getTramiteService().guardarDocumento(this.currentUser, documento);

            //Crea un registro en la tabla tramite_documento, le asocia la resolución al trámite y lo actualiza
            if (documento != null) {

                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                tramiteDocumento = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumento);

                this.currentTramite.setResultadoDocumento(documento);
                this.currentTramite = this.getTramiteService().guardarActualizarTramite(
                    this.currentTramite);
                if (this.currentTramite.getResultadoDocumento() == null || this.currentTramite.
                    getResultadoDocumento().getId() == null) {
                    this.addMensajeError("Error al guardar el documento de resolución.");
                    LOGGER.debug("Error al asociar el documento de la resolución en el trámite");
                    throw new Exception("Error al guardar el documento de resolución");
                }
            } else {
                this.addMensajeError("Error al guardar el documento de resolución.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                throw new Exception("Error al guardar el documento de resolución.");
            }

            //Masivo
            if (this.tramitesSeleccionadosAVisualizar != null) {
                for (Tramite tramiteSeleccionadoActualizacion
                    : this.tramitesSeleccionadosAVisualizar) {
                    if (tramiteSeleccionadoActualizacion != null &&
                        tramiteSeleccionadoActualizacion.isActualizacion()) {
                        tramiteDocumento.setTramite(tramiteSeleccionadoActualizacion);

                        //Crea un registro en la tabla tramite_documento, le asocia la resolución al trámite y lo actualiza
                        if (documento != null) {

                            tramiteDocumento.setDocumento(documento);
                            tramiteDocumento.setIdRepositorioDocumentos(documento.
                                getIdRepositorioDocumentos());
                            tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                            tramiteDocumento = this.getTramiteService()
                                .actualizarTramiteDocumento(tramiteDocumento);

                            tramiteSeleccionadoActualizacion.setResultadoDocumento(documento);
                            tramiteSeleccionadoActualizacion = this.getTramiteService().
                                guardarActualizarTramite(tramiteSeleccionadoActualizacion);
                            if (tramiteSeleccionadoActualizacion.getResultadoDocumento() == null ||
                                tramiteSeleccionadoActualizacion.getResultadoDocumento().getId() ==
                                null) {
                                this.addMensajeError("Error al guardar el documento de resolución.");
                                LOGGER.debug(
                                    "Error al asociar el documento de la resolución en el trámite");
                                throw new Exception("Error al guardar el documento de resolución");
                            }
                        } else {
                            this.addMensajeError("Error al guardar el documento de resolución.");
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.addCallbackParam("error", "error");
                            throw new Exception("Error al guardar el documento de resolución.");
                        }

                    }
                }
            } else if (this.tramitesSeleccionadosAVisualizar != null) {
                for (Tramite tramiteSeleccionadoConservacion : this.tramitesSeleccionadosAVisualizar) {
                    if (tramiteSeleccionadoConservacion != null && tramiteSeleccionadoConservacion.
                        isActualizacion()) {
                        tramiteDocumento.setTramite(tramiteSeleccionadoConservacion);

                        //Crea un registro en la tabla tramite_documento, le asocia la resolución al trámite y lo actualiza
                        if (documento != null) {

                            tramiteDocumento.setDocumento(documento);
                            tramiteDocumento.setIdRepositorioDocumentos(documento.
                                getIdRepositorioDocumentos());
                            tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                            tramiteDocumento = this.getTramiteService()
                                .actualizarTramiteDocumento(tramiteDocumento);

                            tramiteSeleccionadoConservacion.setResultadoDocumento(documento);
                            tramiteSeleccionadoConservacion = this.getTramiteService().
                                guardarActualizarTramite(tramiteSeleccionadoConservacion);
                            if (tramiteSeleccionadoConservacion.getResultadoDocumento() == null ||
                                tramiteSeleccionadoConservacion.getResultadoDocumento().getId() ==
                                null) {
                                this.addMensajeError("Error al guardar el documento de resolución.");
                                LOGGER.debug(
                                    "Error al asociar el documento de la resolución en el trámite");
                                throw new Exception("Error al guardar el documento de resolución");
                            }
                        } else {
                            this.addMensajeError("Error al guardar el documento de resolución.");
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.addCallbackParam("error", "error");
                            throw new Exception("Error al guardar el documento de resolución.");
                        }

                    } else if (tramiteSeleccionadoConservacion != null &&
                        !tramiteSeleccionadoConservacion.isActualizacion()) {

                        tramiteDocumento.setTramite(tramiteSeleccionadoConservacion);

                        //Crea un registro en la tabla tramite_documento, le asocia la resolución al trámite y lo actualiza
                        if (documento != null) {

                            tramiteDocumento.setDocumento(documento);
                            tramiteDocumento.setIdRepositorioDocumentos(documento.
                                getIdRepositorioDocumentos());
                            tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                            tramiteDocumento = this.getTramiteService()
                                .actualizarTramiteDocumento(tramiteDocumento);

                            tramiteSeleccionadoConservacion.setResultadoDocumento(documento);
                            tramiteSeleccionadoConservacion = this.getTramiteService().
                                guardarActualizarTramite(tramiteSeleccionadoConservacion);
                            if (tramiteSeleccionadoConservacion.getResultadoDocumento() == null ||
                                tramiteSeleccionadoConservacion.getResultadoDocumento().getId() ==
                                null) {
                                this.addMensajeError("Error al guardar el documento de resolución.");
                                LOGGER.debug(
                                    "Error al asociar el documento de la resolución en el trámite");
                                throw new Exception("Error al guardar el documento de resolución");
                            }
                        } else {
                            this.addMensajeError("Error al guardar el documento de resolución.");
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.addCallbackParam("error", "error");
                            throw new Exception("Error al guardar el documento de resolución.");
                        }

                    }
                }
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error("Error al guardar el documento de la resolución en la base de datos " + e.
                getMensaje(), e);
            this.addMensajeError("Error al guardar la resolución.");
            // Lanzar la excepción al método padre (generarResolucion).
            throw (e);
        }
    }

    /**
     * Método que verifica si se requiere notificación dependiendo del tipo de trámite y del tipo de
     * solicitud
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez Se elimina metodo: verificarRequiereNotificacion
     */
    public boolean verificarRequiereNotificacion() {

        boolean answer = true;
        //MASIVO

        //Parametro Tramites Actualizacion basado en las siguientes condiciones: Tipo Tramite, Clase Mutacion, Subtipo, Tipo Radicacion Especial y Tramite de Actualizacion
        Parametro notificaTramitesActualizacionParam = this.getGeneralesService().
            getCacheParametroPorNombre(EParametro.TRAMITES_ACTUALIZACION_NOTIFICAN.toString());
        String[] listaParametrosActualizacionNotifican = null;

        if (notificaTramitesActualizacionParam.getValorCaracter() != null) {
            listaParametrosActualizacionNotifican = notificaTramitesActualizacionParam.
                getValorCaracter().split(",");
        }

        //Parametro Tramites Conservacion basado en las siguientes condiciones: Tipo Tramite, Clase Mutacion, Subtipo, Tipo Radicacion Especial y Tramite de Actualizacion
        Parametro notificaTramitesConservacionParam = this.getGeneralesService().
            getCacheParametroPorNombre(EParametro.TRAMITES_CONSERVACION_NOTIFICAN.toString());
        String[] listaParametrosConservacionNotifican = null;

        if (notificaTramitesConservacionParam.getValorCaracter() != null) {
            listaParametrosConservacionNotifican = notificaTramitesConservacionParam.
                getValorCaracter().split(",");
        }

        if (this.tramitesSeleccionadosAVisualizar != null) {
            for (Tramite tramiteSeleccionadoAvanzar : this.tramitesSeleccionadosAVisualizar) {

                if (tramiteSeleccionadoAvanzar.isActualizacion() && tramiteSeleccionadoAvanzar.
                    getResultadoDocumento() != null) {

                    if (tramiteSeleccionadoAvanzar.getTipoTramite() != null ||
                         tramiteSeleccionadoAvanzar.getClaseMutacion() != null ||
                         tramiteSeleccionadoAvanzar.getSubtipo() != null ||
                         tramiteSeleccionadoAvanzar.getRadicacionEspecial() != null) {

                        // Falta Campo Actualizacion
                        String tramiteDesenglobeEtapasComunica = tramiteSeleccionadoAvanzar.
                            getTipoTramite() + "-" +
                             tramiteSeleccionadoAvanzar.getClaseMutacion() + "-" +
                             tramiteSeleccionadoAvanzar.getSubtipo() + "-" +
                             tramiteSeleccionadoAvanzar.getRadicacionEspecial();

                        String tramiteCancelacionTramiteComunica = tramiteSeleccionadoAvanzar.
                            getTipoTramite();

                        String tramiteRectificacionMatrizTramiteComunica =
                            tramiteSeleccionadoAvanzar.getTipoTramite() +
                             "-" + tramiteSeleccionadoAvanzar.getRadicacionEspecial();

                        for (int i = 0; i < listaParametrosActualizacionNotifican.length; i++) {
                            String notificaActualizacion = listaParametrosActualizacionNotifican[i];
                            if (tramiteDesenglobeEtapasComunica.contains(notificaActualizacion) ||
                                 tramiteCancelacionTramiteComunica.contains(notificaActualizacion) ||
                                 tramiteRectificacionMatrizTramiteComunica.contains(
                                    notificaActualizacion)) {

                                this.tramitesActualizacionComunicaNotificacion.add(
                                    tramiteSeleccionadoAvanzar);
                                answer = true;

                            } else {
                                // Si no se encuentran dentro de las
                                // validaciones anteriores los tramites pasan a
                                // la lista de aplicar cambios
                                this.tramitesActualizacionAplicaCambios.add(
                                    tramiteSeleccionadoAvanzar);
                                answer = false;
                            }
                        }

                    }
                } else if (!tramiteSeleccionadoAvanzar.isActualizacion() &&
                    tramiteSeleccionadoAvanzar.getResultadoDocumento() != null) {

                    if (tramiteSeleccionadoAvanzar.getTipoTramite() != null &&
                         tramiteSeleccionadoAvanzar.getClaseMutacion() != null &&
                         tramiteSeleccionadoAvanzar.getSubtipo() != null &&
                         tramiteSeleccionadoAvanzar.getRadicacionEspecial() != null) {

                        // Falta Campo Actualizacion
                        String tramiteDesenglobeEtapasComunica = tramiteSeleccionadoAvanzar.
                            getTipoTramite() +
                             "-" +
                             tramiteSeleccionadoAvanzar.getClaseMutacion() +
                             "-" +
                             tramiteSeleccionadoAvanzar.getSubtipo() +
                             "-" +
                             tramiteSeleccionadoAvanzar.getRadicacionEspecial();

                        String tramiteCancelacionTramiteComunica = tramiteSeleccionadoAvanzar.
                            getTipoTramite();

                        String tramiteRectificacionMatrizTramiteComunica =
                            tramiteSeleccionadoAvanzar.getTipoTramite() +
                             "-" + tramiteSeleccionadoAvanzar.getRadicacionEspecial();

                        for (int i = 0; i < listaParametrosConservacionNotifican.length; i++) {
                            String notificaConservacion = listaParametrosConservacionNotifican[i];
                            if (tramiteDesenglobeEtapasComunica.contains(notificaConservacion) ||
                                 tramiteCancelacionTramiteComunica.contains(notificaConservacion) ||
                                 tramiteRectificacionMatrizTramiteComunica.contains(
                                    notificaConservacion)) {

                                this.tramitesConservacionComunicaNotificacion.add(
                                    tramiteSeleccionadoAvanzar);
                                answer = true;
                            } else {
                                // Si no se encuentran dentro de las
                                // validaciones anteriores los tramites pasan a
                                // la lista de aplicar cambios
                                this.tramitesConservacionAplicaCambios.add(
                                    tramiteSeleccionadoAvanzar);
                                answer = false;
                            }
                        }
                    }

                }
            }
        }

        //INDIVIDUAL
        /* if(this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo())){
         * if(this.currentTramite.getClaseMutacion().equals(EMutacionClase.PRIMERA.getCodigo()) ||
         * this.currentTramite.getClaseMutacion().equals(EMutacionClase.SEGUNDA.getCodigo())){
         *
         * //felipe.cadena::17-07-2015::Se envian los desenglobes por etapas a notificar
         * if(this.currentTramite.isDesenglobeEtapas()){ answer = true; }else{ answer = false; } } }
         * else
         * if(this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo())){
         * answer = false; } else
         * if(this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo())){
         * answer = true; } else
         * if(this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL.getCodigo())){
         * answer = false; } else
         * if(this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.getCodigo())){
         * answer = false; //TODO: javier.aponte :: 16-04-2012 :: se requiere verificar este método
         * después de que Clara //hable con el temático //Si en el trámite hay por lo menos una
         * rectificación asociada al predio, se debe notificar for(TramiteRectificacion tr :
         * this.currentTramite.getTramiteRectificacions()){
         * if(EDatoRectificarTipo.PREDIO.toString().equalsIgnoreCase(tr.getDatoRectificar().getTipo())){
         *
         * if(EDatoRectificarNombre.AREA_TERRENO.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())
         * ||
         * EDatoRectificarNombre.AREA_CONSTRUIDA.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())
         * ||
         * EDatoRectificarNombre.COEFICIENTE.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())
         * ||
         * EDatoRectificarNombre.DIMENSION.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())
         * ||
         * EDatoRectificarNombre.ZONA_FISICA.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())
         * ||
         * EDatoRectificarNombre.ZONA_GEOECONOMICA.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())){
         * answer = true; }
         *
         * }
         * //felipe.cadena::17-07-2015::Se envian las rectificaciones masivas de detalle a notificar
         * if(this.currentTramite.isRectificacionMatriz() &&
         * EDatoRectificarNombre.DETALLE_CALIFICACION.getCodigo().equalsIgnoreCase(tr.getDatoRectificar().getNombre())){
         * answer = true; } }
		} */
        return answer;
    }
    //--------------------------------------------------------------------------------------------------
    //----------------   BPM  ---------------------

    /**
     * se valida que haya un usuario con el rol requerido para ser destinatario de la actividad
     */
    @Implement
    public boolean validateProcess() {

        return true;

    }

    /**
     * @author javier.aponte
     */
    /*
     * @modified by juanfelipe.garcia :: 17-03-2014 :: CC-NP-CO-041 - Aplicar cambios para trámites
     * que no modifican el avalúo (7321)
     */
 /*
     * @modified by leidy.gonzalez
     */
    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO = null;
        String observaciones = "", processTransition = "";
        UsuarioDTO usuarioDestinoActividad = new UsuarioDTO();
        List<UsuarioDTO> usuariosRol;
        boolean requiereNotificacion;
        requiereNotificacion = this.verificarRequiereNotificacion();
        UsuarioDTO usuarioActividad;
        boolean exito = false;
        int intentos = 0;

        //MASIVO
        if (requiereNotificacion) {
            if (this.tramitesActualizacionComunicaNotificacion != null ||
                 this.tramitesConservacionComunicaNotificacion != null) {

                usuariosRol = this
                    .getTramiteService()
                    .buscarFuncionariosPorRolYTerritorial(
                        this.currentUser
                            .getDescripcionEstructuraOrganizacional(),
                        ERol.SECRETARIA_CONSERVACION);

                if (usuariosRol != null && !usuariosRol.isEmpty()) {

                    usuarioDestinoActividad = usuariosRol.get(0);

                }

                processTransition =
                    ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION;
                observaciones = "";

                // Avanzar a Aplicar cambios
                messageDTO = ProcesosUtilidadesWeb
                    .setupIndividualProcessMessage(
                        this.currentProcessActivity.getId(),
                        processTransition, usuarioDestinoActividad,
                        observaciones);

                messageDTO.setUsuarioActual(this.currentUser);
                this.setMessage(messageDTO);

                //Cambio Metodo Avance en Process Masivo(ALEJANDRO)
                try {
                    this.getProcesosService().avanzarActividad(
                        messageDTO.getUsuarioActual(),
                        messageDTO.getActivityId(),
                        messageDTO.getSolicitudCatastral());
                } catch (Exception e) {
                    LOGGER.error(
                        "error avanzando actividad: " + e.getMessage(), e);
                }
            }

        } else {

            if (this.tramitesActualizacionAplicaCambios != null) {

                //Aplicar Cambios a los tramites de Actualizacion
                for (Tramite tramiteActualizacionAplicaCambios
                    : this.tramitesActualizacionAplicaCambios) {

                    //inicia la aplicacion de cambios
                    Actividad actTramite = this.tareasPendientesMB.
                        buscarActividadPorIdTramiteSeleccionadas(tramiteActualizacionAplicaCambios.
                            getId());
                    if (actTramite == null) {
                        actTramite = this.tareasPendientesMB.buscarActividadPorIdTramite(
                            tramiteActualizacionAplicaCambios.getId());
                    }
                    tramiteActualizacionAplicaCambios.setActividadActualTramite(actTramite);
                    LOGGER.debug("on AplicacionCambiosMB#aplicarCambiosInicio ya tiene Actividad");

                    //se marca el tramite como en aplicar cambios
                    tramiteActualizacionAplicaCambios.setEstadoGdb(Constantes.ACT_APLICAR_CAMBIOS);
                    tramiteActualizacionAplicaCambios = this.getTramiteService().
                        guardarActualizarTramite2(tramiteActualizacionAplicaCambios);

                    if (!tramiteActualizacionAplicaCambios.getEstadoGdb().equals(
                        Constantes.ACT_APLICAR_CAMBIOS)) {
                        LOGGER.error(
                            "No se pudo Actualizar el tramite");
                    } else {

                        this.getConservacionService().aplicarCambiosActualizacion(
                            tramiteActualizacionAplicaCambios, this.currentUser, 1);
                    }
                }

            } else if (this.tramitesConservacionAplicaCambios != null) {

                usuariosRol = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.currentUser.getDescripcionEstructuraOrganizacional(),
                    ERol.FUNCIONARIO_RADICADOR);

                processTransition = ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS;
                observaciones = "";

                //Avanzar a Aplicar cambios
                List<UsuarioDTO> responsablesConservacion = (List<UsuarioDTO>) this
                    .getTramiteService()
                    .buscarFuncionariosPorRolYTerritorial(
                        this.getGeneralesService()
                            .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                this.currentTramite.getMunicipio().getCodigo()),
                        ERol.RESPONSABLE_CONSERVACION);

                if (responsablesConservacion != null &&
                     !responsablesConservacion.isEmpty() &&
                     responsablesConservacion.get(0) != null) {
                    usuarioActividad = responsablesConservacion.get(0);
                } else {
                    LOGGER.error("No se encontró ningún usuario responsable de conservación " +
                         "para la territorial " +
                         this.currentUser.getDescripcionTerritorial());
                    return;
                }

                // mensaje para mover a aplicar cambios
                messageDTO = ProcesosUtilidadesWeb
                    .setupIndividualProcessMessage(
                        this.currentProcessActivity.getId(),
                        processTransition, usuarioActividad,
                        observaciones);
                messageDTO.setUsuarioActual(this.currentUser);
                this.setMessage(messageDTO);

                //Cambio Metodo Avance en Process(ALEJANDRO)
                try {
                    this.getProcesosService().avanzarActividad(
                        messageDTO.getUsuarioActual(),
                        messageDTO.getActivityId(),
                        messageDTO.getSolicitudCatastral());
                } catch (Exception e) {
                    LOGGER.error(
                        "error avanzando actividad: " + e.getMessage(), e);
                }
            }
        }
    }

    @Implement
    public void doDatabaseStatesUpdate() {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    //-------------------------------------------------------------------------------------
    /*
     * @modified by leidy.gonzalez
     */
    private boolean forwardThisProcess() {
        if (this.validateProcess()) {
            this.setupProcessMessage();
            //this.doDatabaseStatesUpdate();
            //Se va aquitar porque el avance se hara anteriormente
            this.forwardProcess();
            return true;
        } else {
            return false;
        }
    }

    public String cerrar() {
        if (this.numeroResolucion != null) {
            UtilidadesWeb.removerManagedBean("generaResolucion");
            this.tareasPendientesMB.init();
            return ConstantesNavegacionWeb.INDEX;
        } else {
            return null;
        }
    }

    public String verProyeccionTramite() {
        return ConstantesNavegacionWeb.VER_PROYECCION;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        UtilidadesWeb.removerManagedBean("generaResolucion");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    // ----------------------------------------------------- //
    /**
     * Método para buscar los tramites realizados
     *
     * @author leidy.gonzalez
     */
    public void buscarTramitesRealizados() {
        LOGGER.debug("Consultando tramites realizados...");

        if (this.currentTramite != null && this.currentTramite.getId() != null) {

            //Valida tramite seleccionado con currentTramite
            this.solicitudSeleccionada = this.getTramiteService().
                buscarSolicitudFetchTramitesBySolicitudId(this.currentTramite.getSolicitud().getId());

            this.tramiteTextoResolucion = this.getTramiteService()
                .buscarTramiteTextoResolucionPorIdTramite(this.currentTramite.getId());

            if (this.tramiteTextoResolucion == null) {

                this.consultarModeloResolucionSegunTipoDeTramite();
                this.establecerConsiderandoYResuelve();
                this.guardarTramiteTextoResolucion();

                this.banderaDetalleResolucion = true;
            } else {
                this.banderaDetalleResolucion = true;
            }

        }

        LOGGER.debug("Tramites consultados");
    }

    /**
     * Método encargado de cerrar el panel de gestion de documentos de desbloqueo
     *
     * @author leidy.gonzalez
     */
    public void cerrarDetalleResolucion() {

        this.banderaDetalleResolucion = false;
    }

    /**
     * Método que consulta las plantillas de texto resolucion según el tipo del trámite.
     *
     * @author leidy.gonzalez
     *
     * */
    public void consultarModeloResolucionSegunTipoDeTramite() {

        LOGGER.debug("Entra a consultarModeloResolucionSegunTipoDeTramite");

        // Busqueda de los Modelos de Resolucion para
        // el trámite seleccionado.
        this.modeloResolucionList = this.getTramiteService()
            .findModelosResolucionByTipoTramite(this.currentTramite.getTipoTramite(),
                this.currentTramite.getClaseMutacion(), null);
        if (this.currentTramite.isRectificacion()) {
            List<ModeloResolucion> modelosResolucionTemp = new ArrayList<ModeloResolucion>();
            modelosResolucionTemp.addAll(this.modeloResolucionList);

            for (ModeloResolucion mrTemp : modelosResolucionTemp) {
                if (Constantes.TRAMITE_SUBTIPO_CANCELACION.equals(mrTemp.getSubtipo())) {
                    if (this.modeloResolucionList.contains(mrTemp)) {
                        this.modeloResolucionList.remove(mrTemp);
                    }
                }
            }

        }

        //Se filtra la lista cuando es un trámite de revisión de avaluo.
        if (this.currentTramite.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.
            getCodigo())) {
            this.modeloResolucionList = this.obtenerListaModelosAvaluos(this.modeloResolucionList);
        }

        //Se filtra la lista cuando es un trámite de revisión de avaluo.
        if (this.currentTramite.isCuarta()) {
            this.modeloResolucionList = this.obtenerListaModelosAutoestimacion(
                this.modeloResolucionList);
        }

        this.tramiteTextoResolucion = this.getTramiteService()
            .buscarTramiteTextoResolucionPorIdTramite(
                this.currentTramite.getId());

        this.modeloResolucionSeleccionado = new ModeloResolucion();

        // Si el trámite ya tenía un modelo, este se establece.
        if (this.tramiteTextoResolucion != null) {

            if (this.tramiteTextoResolucion
                .getModeloResolucion() != null) {
                this.modeloResolucionSeleccionado
                    .setId(this.tramiteTextoResolucion
                        .getModeloResolucion().getId());
            }

            this.modeloResolucionSeleccionado
                .setTextoTitulo(this.tramiteTextoResolucion
                    .getTitulo());

            this.modeloResolucionSeleccionado
                .setTextoConsiderando(this.tramiteTextoResolucion
                    .getConsiderando());

            this.modeloResolucionSeleccionado
                .setTextoResuelve(this.tramiteTextoResolucion
                    .getResuelve());

        } else {
            this.tramiteTextoResolucion = new TramiteTextoResolucion();
            this.tramiteTextoResolucion.setFechaLog(new Date());

            if (this.currentTramite.getClaseMutacion() != null &&
                 this.currentTramite.getClaseMutacion().equals(EMutacionClase.PRIMERA.getCodigo()) &&
                 this.modeloResolucionList.get(1) != null) {

                this.modeloResolucionSeleccionado = (ModeloResolucion) this.modeloResolucionList.
                    get(1).clone();

            } else if (this.modeloResolucionList != null &&
                 !this.modeloResolucionList.isEmpty() &&
                 this.modeloResolucionList.get(0) != null &&
                 !this.modeloResolucionList.get(0).getTextoTitulo().contains(
                    "RECTIFICACIÓN CANCELACIÓN POR DOBLE INSCRIPCIÓN")) {

                this.modeloResolucionSeleccionado = (ModeloResolucion) this.modeloResolucionList.
                    get(0).clone();

            } else if (this.currentTramite.isRectificacionCancelacionDobleInscripcion() &&
                this.modeloResolucionList != null &&
                 !this.modeloResolucionList.isEmpty() &&
                 this.modeloResolucionList.get(0) != null &&
                 this.modeloResolucionList.get(0).getTextoTitulo().contains(
                    "RECTIFICACIÓN CANCELACIÓN POR DOBLE INSCRIPCIÓN")) {

                this.modeloResolucionSeleccionado = (ModeloResolucion) this.modeloResolucionList.
                    get(0).clone();

            } else if (!this.currentTramite.isRectificacionCancelacionDobleInscripcion() &&
                this.modeloResolucionList != null &&
                 !this.modeloResolucionList.isEmpty() &&
                 this.modeloResolucionList.get(1) != null &&
                 this.modeloResolucionList.get(1).getTextoTitulo().contains(
                    "RESOLUCIÓN RECTIFICACIÓN ACTUALIZACIÓN")) {

                this.modeloResolucionSeleccionado = (ModeloResolucion) this.modeloResolucionList.
                    get(1).clone();
            } else {
                this.modeloResolucionSeleccionado.setTextoTitulo("");
                this.modeloResolucionSeleccionado.setTextoConsiderando("");
                this.modeloResolucionSeleccionado.setTextoResuelve("");
                this.modeloResolucionList = this.getTramiteService()
                    .findModelosResolucionByTipoTramite("0", "0", null);

                this.modeloResolucionSeleccionado = this.modeloResolucionList.get(0);
            }
        }
        if (this.currentUser.getDescripcionTerritorial().contains(Constantes.PREFIJO_DELEGADOS) &&
            this.currentTramite.isActualizacion()) {
            List<ModeloResolucion> modelosDelegada = this.getTramiteService()
                .findModelosResolucionByTipoTramite(this.currentTramite.getTipoTramite(),
                    this.currentTramite.getClaseMutacion(), null, this.currentUser.
                    getCodigoEstructuraOrganizacional());
            if(modelosDelegada.size()>0){
            this.modeloResolucionSeleccionado = (ModeloResolucion) modelosDelegada.
                    get(0).clone();
            }
        }
    }

    /**
     * Método que establece en el modelo de resolución el texto considerando y el texto resuelve de
     * a plantilla de texto resolucion seleccionada.
     *
     * @author leidy.gonzalez
     */
    public void establecerConsiderandoYResuelve() {

        LOGGER.debug("Entra a consultar establecerConsiderandoYResuelve");
        if (this.modeloResolucionList != null &&
             !this.modeloResolucionList.isEmpty() &&
             this.modeloResolucionSeleccionado != null) {
            for (ModeloResolucion modelo : this.modeloResolucionList) {
                if (modelo.getId().longValue() == this.modeloResolucionSeleccionado
                    .getId().longValue()) {
                    this.modeloResolucionSeleccionado.setTextoTitulo(modelo
                        .getTextoTitulo());
                    this.modeloResolucionSeleccionado
                        .setTextoConsiderando(modelo.getTextoConsiderando());
                    this.modeloResolucionSeleccionado.setTextoResuelve(modelo
                        .getTextoResuelve());
                }
            }
        }
        this.reemplazarParametrosTextoConsiderandoResuelve();
    }

    /**
     * Método que guarda el tramite texto resolucion que se modifico a partir de la plantilla.
     *
     * @author leidy.gonzalez
     */
    public void guardarTramiteTextoResolucion() {

        String txtConsiderando, txtResuelve;
        LOGGER.debug("Entra a Guardar Tramite Texto Resolucion");

        if (this.currentTramite != null && this.modeloResolucionSeleccionado != null &&
             this.modeloResolucionSeleccionado.getId() != null) {

            // Campos de USUARIO
            this.tramiteTextoResolucion.setFechaLog(new Date(System
                .currentTimeMillis()));
            this.tramiteTextoResolucion.setUsuarioLog(this.currentUser.getLogin());

            this.tramiteTextoResolucion
                .setTitulo(this.modeloResolucionSeleccionado
                    .getTextoTitulo());

            // Se debe hacer un replace de los estilos debido a que el editor los guarda de una manera y
            // al generar el reporte, jasper los traduce de otra, por tal motivo hay que reemplazarlos manualmente.
            txtConsiderando = reemplazarEstilosDelEditor(this.modeloResolucionSeleccionado
                .getTextoConsiderando());
            txtResuelve = reemplazarEstilosDelEditor(this.modeloResolucionSeleccionado
                .getTextoResuelve());

            this.modeloResolucionSeleccionado
                .setTextoConsiderando(txtConsiderando);
            this.modeloResolucionSeleccionado.setTextoResuelve(txtResuelve);
            this.tramiteTextoResolucion.setConsiderando(txtConsiderando);
            this.tramiteTextoResolucion.setResuelve(txtResuelve);

            this.tramiteTextoResolucion.setTramite(this.currentTramite);
            this.tramiteTextoResolucion
                .setModeloResolucion(this.modeloResolucionSeleccionado);

            try {
                // Guardar Texto Resolución
                this.tramiteTextoResolucion = this.getTramiteService()
                    .guardarTramiteTextoResolucion(
                        this.tramiteTextoResolucion);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                this.addMensajeError(
                    "Hubo un error al guardar el texto motivo, por favor intente nuevamente");
                return;
            }
        }
    }

    /**
     * Metodo para filtrar las opciones de plantillas dependiendo si el avaluo se confirma o se
     * rechaza.
     *
     * @author leidy.gonzalez
     * @param listaInicial
     * @return
     */
    private List<ModeloResolucion> obtenerListaModelosAvaluos(List<ModeloResolucion> listaInicial) {

        List<ModeloResolucion> resultado = new ArrayList<ModeloResolucion>();

        for (ModeloResolucion md : listaInicial) {
            if (md.getTextoTitulo().contains("RECHAZA")) {
                resultado.add(md);
            } else {
                continue;
            }
            if (md.getTextoTitulo().contains("ACEPTA")) {
                resultado.add(md);
            } else {
                continue;
            }
        }

        return resultado;

    }

    /**
     * Metodo para filtrar las opciones de plantillas dependiendo si la autoestimación se acepta o
     * se rechaza.
     *
     * @author leidy.gonzalez
     * @param listaInicial
     * @return
     */
    private List<ModeloResolucion> obtenerListaModelosAutoestimacion(
        List<ModeloResolucion> listaInicial) {

        List<ModeloResolucion> resultado = new ArrayList<ModeloResolucion>();

        for (ModeloResolucion md : listaInicial) {
            if (md.getTextoTitulo().contains("RECHAZA")) {
                resultado.add(md);
            } else {
                continue;
            }
            if (md.getTextoTitulo().contains("ACEPTA")) {
                resultado.add(md);
            } else {
                continue;
            }
        }

        return resultado;

    }

    /**
     * Método que reemplaza los estilos de negrilla y cursiva guardados por el editor
     *
     * @author leidy.gonzalez
     * @param txt
     */
    public String reemplazarEstilosDelEditor(String txt) {
        if (txt != null && !txt.trim().isEmpty()) {
            txt = txt.replaceAll("<strong>", "<b>");
            txt = txt.replaceAll("</strong>", "</b>");
            txt = txt.replaceAll("<em>", "<i>");
            txt = txt.replaceAll("</em>", "</i>");
        }
        return txt;
    }

    /**
     * Método que reemplaza los parametros del texto considerando y del texto resuelve de la
     * resolución dependiendo del tipo de trámite, la clase de mutación y el subtipo, el texto viene
     * de la tabla MODELO_RESOLUCION, los parametros se caracterizan por el patron {#} de una
     * variable en java
     *
     * @author leidy.gonzalez
     */
    public void reemplazarParametrosTextoConsiderandoResuelve() {

        if (!this.modeloResolucionSeleccionado.getTextoTitulo().equals(
            ConstantesResolucionesTramites.SIN_PLANTILLA)) {
            String tipoTramite = this.currentTramite.getTipoTramite();

            Locale colombiaLocale = new Locale("ES", "es_CO");

            String estructuraOrganizacional = null;
            String responsableConservacion = null;

            Object parametrosTextoConsiderando[] = null;
            Object parametrosTextoResuelve[] = null;

            if (this.currentUser.isTerritorial()) {
                estructuraOrganizacional = ConstantesResolucionesTramites.DIRECCION_TERRITORIAL +
                     this.currentUser.getDescripcionEstructuraOrganizacional()
                        .toUpperCase();
                responsableConservacion =
                    ConstantesResolucionesTramites.RESPONSABLE_DIRECCION_TERRITORIAL;
            }
            if (this.currentUser.isUoc()) {
                estructuraOrganizacional = ConstantesResolucionesTramites.UNIDAD_OPERATIVA +
                     this.currentUser.getDescripcionEstructuraOrganizacional()
                        .replaceAll("UOC_", "").toUpperCase();
                responsableConservacion =
                    ConstantesResolucionesTramites.RESPONSABLE_UNIDAD_OPERATIVA;
            }

            StringBuilder solicitantes = new StringBuilder();
            StringBuilder identificaciones = new StringBuilder();
            StringBuilder condiciones = new StringBuilder();
            StringBuilder radicar = new StringBuilder();
            StringBuilder radicarTramieCatastral = new StringBuilder();

            for (SolicitanteSolicitud solicitanteSolicitud : this.currentTramite
                .getSolicitud().getSolicitanteSolicituds()) {

                if (solicitanteSolicitud.getNombreCompleto() != null &&
                     !solicitanteSolicitud.getNombreCompleto().isEmpty()) {
                    solicitantes.append(solicitanteSolicitud
                        .getNombreCompleto()
                        .substring(
                            0,
                            solicitanteSolicitud.getNombreCompleto()
                                .length() - 1).toUpperCase())
                        .append(", ");

                }
                if (solicitanteSolicitud.getTipoIdentificacion() != null &&
                     !solicitanteSolicitud.getTipoIdentificacion().isEmpty() &&
                     solicitanteSolicitud.getNumeroIdentificacion() != null &&
                     !solicitanteSolicitud.getNumeroIdentificacion().isEmpty()) {
                    identificaciones.append(solicitanteSolicitud
                        .getTipoIdentificacion()).append(". No. ").append(
                        solicitanteSolicitud.getNumeroIdentificacion()).append(", ");
                }

                if (solicitanteSolicitud.getRelacion() != null &&
                     !solicitanteSolicitud.getRelacion().isEmpty()) {
                    condiciones.append(solicitanteSolicitud.getRelacion().toUpperCase()).
                        append(", ");
                }

            }

            StringBuilder senorIdentificacion = new StringBuilder();
            if (this.currentTramite.getSolicitud().getSolicitanteSolicituds().size() > 1) {
                senorIdentificacion.append(ConstantesResolucionesTramites.SENIORES).
                    append(solicitantes).append(ConstantesResolucionesTramites.IDENTIFICADOS).
                    append(identificaciones).append(ConstantesResolucionesTramites.CONDICIONES).
                    append(condiciones).append(ConstantesResolucionesTramites.RESPECTIVAMENTE);
                radicar.append(ConstantesResolucionesTramites.RADICARON);
                radicarTramieCatastral.append(ConstantesResolucionesTramites.RADICARON);
            }
            if (this.currentTramite.getSolicitud().getSolicitanteSolicituds().size() == 1) {
                senorIdentificacion.append(ConstantesResolucionesTramites.SENIOR).
                    append(solicitantes).append(ConstantesResolucionesTramites.IDENTIFICADO).
                    append(identificaciones).append(ConstantesResolucionesTramites.CONDICION).
                    append(condiciones);
                radicar.append(ConstantesResolucionesTramites.RADICO);
                radicarTramieCatastral.append(ConstantesResolucionesTramites.RADICO);
            }

            radicar.append(" " + ConstantesResolucionesTramites.NUMERO_RADICADO).
                append(this.currentTramite.getNumeroRadicacion()).
                append(", ");
            
            radicarTramieCatastral.append(" " + ConstantesResolucionesTramites.NUMERO_RADICADO_TRAMITE_CATASTRAL).
                append(this.currentTramite.getNumeroRadicacion()).
                append(", ");

            List<Dominio> tiposSolicitud = this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.SOLICITUD_TIPO);
            String tipoSolicitudValor = null;
            for (Dominio tmp : tiposSolicitud) {
                if (tmp.getCodigo().equals(this.currentTramite.getSolicitud().getTipo())) {
                    tipoSolicitudValor = tmp.getValor().toUpperCase();
                    break;
                }
            }
            String documentosJustificativos = " ";

            //documentosJustificativos = this.formarDocumentosJustificativos();
            if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) ||
                 tipoTramite.equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo()) ||
                 tipoTramite.equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()) ||
                 tipoTramite.equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo()) ||
                 tipoTramite.equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo()) ||
                 tipoTramite.equals(ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL.
                    getCodigo()) ||
                 this.currentTramite.getSolicitud().getTipo().equals(ESolicitudTipo.VIA_GUBERNATIVA.
                    getCodigo())) {

                parametrosTextoConsiderando = new Object[12];
                parametrosTextoResuelve = new Object[8];

                parametrosTextoConsiderando[0] = estructuraOrganizacional;
                parametrosTextoConsiderando[1] = responsableConservacion;
                parametrosTextoConsiderando[2] = senorIdentificacion;
                parametrosTextoConsiderando[3] = (this.currentTramite.getPredio() == null) ? "" :
                     this.currentTramite.getPredio().getNumeroPredial();
                parametrosTextoConsiderando[4] = (this.currentTramite.getMunicipio() == null) ? "" :
                     this.currentTramite.getMunicipio().getNombre().toUpperCase();
                parametrosTextoConsiderando[5] = radicar;
                parametrosTextoConsiderando[6] = tipoSolicitudValor;
                parametrosTextoConsiderando[7] = documentosJustificativos;

                parametrosTextoConsiderando[8] = this.currentTramite.getTipoTramiteCadenaCompleto().
                    toUpperCase();
                parametrosTextoConsiderando[9] = null;
                parametrosTextoConsiderando[10] = null;
                parametrosTextoConsiderando[11] = radicarTramieCatastral;

                parametrosTextoResuelve[0] = responsableConservacion;
                parametrosTextoResuelve[1] = 2;
                parametrosTextoResuelve[2] = 3;
                parametrosTextoResuelve[3] = 4;
                parametrosTextoResuelve[4] = 5;

                if (this.currentTramite.getSolicitud().isViaGubernativa()) {
                    Tramite tramiteViaGubernativa = this.buscarTramiteAsociadoViaGubernativa(
                        this.currentTramite.getSolicitud().getTramiteNumeroRadicacionRef());

                    parametrosTextoConsiderando[8] = this.currentTramite.
                        getTipoTramiteCadenaCompleto().toUpperCase();
                    parametrosTextoConsiderando[9] = tramiteViaGubernativa.getResultadoDocumento().
                        getNumeroDocumento();
                    parametrosTextoConsiderando[10] = Constantes.FORMAT_FECHA_RESOLUCION_2.format(
                        tramiteViaGubernativa.getResultadoDocumento().getFechaDocumento()).
                        toUpperCase();

                    parametrosTextoResuelve[5] = tramiteViaGubernativa.getResultadoDocumento().
                        getNumeroDocumento();
                    parametrosTextoResuelve[6] = Constantes.FORMAT_FECHA_RESOLUCION_2.format(
                        tramiteViaGubernativa.getResultadoDocumento().getFechaDocumento()).
                        toUpperCase();
                    parametrosTextoResuelve[7] = solicitantes.toString();
                }
            }
            if (parametrosTextoConsiderando != null) {
                this.modeloResolucionSeleccionado
                    .setTextoConsiderando(new MessageFormat(
                        this.modeloResolucionSeleccionado
                            .getTextoConsiderando(), colombiaLocale)
                        .format(parametrosTextoConsiderando));
            }
            if (parametrosTextoResuelve != null) {
                this.modeloResolucionSeleccionado
                    .setTextoResuelve(new MessageFormat(
                        this.modeloResolucionSeleccionado
                            .getTextoResuelve(), colombiaLocale)
                        .format(parametrosTextoResuelve));
            }
        }
    }

    /**
     * Método que busca el tramite asociado a uno de vía gubernativa, por el numero de radicación
     *
     * @author leidy.gonzalez
     * @param numeroRadicacionAsociado
     * @return
     */
    private Tramite buscarTramiteAsociadoViaGubernativa(String numeroRadicacionAsociado) {
        Tramite respuesta = null;
        Tramite resultado = this.getTramiteService().buscarTramitePorNumeroRadicacion(
            numeroRadicacionAsociado);
        if (resultado != null) {
            respuesta = resultado;
        }
        return respuesta;
    }

    /**
     * Retorna los ids para activos para procesar
     *
     * @author felipe.cadena
     * @param idTramites
     * @return
     */
    public long[] obtenerIdsActivos(List<Long> idTramites) {

        //actualizaciones asociadas a los tramites de process
        List<MunicipioActualizacion> actualizacionesTramites =
            this.getActualizacionService().obtenerMunicipioActualizacionPorIdsTramites(idTramites);

        List<Long> idsTramitesActivos = new ArrayList<Long>();

        for (MunicipioActualizacion mu : actualizacionesTramites) {
            //se aicionan solo los de procesos activos para generar resolucion
            if (mu.getEstado().equals(EMunicipioActualizacionEstado.RADICACION_EXITOSA.getCodigo()) &&
                 mu.getActivo().equals(ESiNo.SI.getCodigo())) {
                for (Tramite tr : mu.getTramitesAsociados()) {
                    for (Long idTramitesProcessTemp : this.currentTramiteIds) {
                        if (tr.getId().equals(idTramitesProcessTemp)) {
                            idsTramitesActivos.add(tr.getId());
                            break;
                        }
                    }

                }
            }
        }

        //Consulta tramites por id's agrupados por predio para Actualizacion
        long[] ids = new long[idsTramitesActivos.size()];
        for (int i = 0; i < idsTramitesActivos.size(); i++) {
            ids[i] = idsTramitesActivos.get(i);
        }

        return ids;

    }

    /**
     * Actualiza la fecha de los predios activos asociados a un tramite
     *
     * @author dumar.penuela
     */
    public void actualizaFechaInscripcionCatastralPPredio() {

        if (this.currentTramite != null) {

            try {
                for (PPredio pp : this.getConservacionService().
                    findPPrediosCompletosActivosByTramiteId(this.currentTramite.getPredio().getId())) {

                    pp.setFechaInscripcionCatastral(new Date(System.currentTimeMillis()));
                    this.getConservacionService().guardarActualizarPPredio(pp);
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return;
            }
        }
    }

    /**
     * Retorna los ids para activos para procesar #18312:: 25/07/2016:: Se unifica en un metodo la
     * carga de datos de la proyeccion.
     *
     * @author leidy.gonzalez
     * @return
     */
    public void revisarProyeccion() {

        this.revisionProyeccionMB = (RevisionProyeccionMB) UtilidadesWeb.getManagedBean(
            "revisionProyeccion");
        this.revisionProyeccionMB.setTramiteSeleccionado(this.currentTramite);
        this.revisionProyeccionMB.cargarInformacionProyeccion();
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private String obtenerNumeroRadicado(Solicitud solicitudTemp) {

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.currentUser.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.currentUser.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.
            getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Cancelación trámite"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        return this.getGeneralesService().obtenerNumeroRadicado(parametros);

    }
//end of class
}
