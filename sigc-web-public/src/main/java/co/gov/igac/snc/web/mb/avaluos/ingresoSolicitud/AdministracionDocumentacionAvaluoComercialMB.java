package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB Caso de uso CU-SA-AC-002 Administrar Documentación Trámite Avalúo Comercial
 *
 * @cu CU-SA-AC-002
 *
 * @author felipe.cadena
 *
 */
@Component("administracionDocumentacionAvaluoComercial")
@Scope("session")
public class AdministracionDocumentacionAvaluoComercialMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 3192560069126536848L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionDocumentacionAvaluoComercialMB.class);
    /**
     * Variables encabezado
     */
    private String radicacion;
    private String secRadicado;
    private Date fechaRadicacion;
    private String tipoTramite;
    private Solicitante solicitante;
    private String descripcionDocSelecionado;
    /**
     * Lista de documentos basicos.
     */
    private List<SolicitudDocumentacion> documentosBasicos;
    /**
     * Lista de documentos anexos relacionados al documento basico seleccionador.
     */
    private List<Documento> documentosAnexosSeleccionados;
    /**
     * Lista de documentos adicionales
     */
    private List<SolicitudDocumentacion> documentosAdicionales;
    /**
     * Banderas para control de acciones en la vista
     */
    private boolean banderaCargarAnexo;
    private boolean banderaReemplazarAnexo;
    private boolean banderaEliminaAnexo;
    private boolean banderaEliminarDocumentoAdicional;
    private boolean banderaAdicionar;
    private boolean banderaGuardar;
    private boolean banderaEnviarDocumentos;
    private boolean banderaConsultarAnexo;
    private boolean banderaCargaAnexoDocBasico;
    /**
     * Variable que indica si la solicitud fue almacenada definitivamente o no
     * </br> <b>True: </b>La solicitud fue almacenada definitivamente </br>
     * <b>False: </b>La solicitud fue almacenada temporalmente
     */
    private boolean banderaModoLecturaHabilitado;
    /**
     * Bandera para determinar si existe un sec_radicado asociado a los documentos si es asi los
     * documentos seran asociados a un tramite
     */
    private boolean banderaSecRadicadoAsociado;
    /**
     * usuario actual del sistema
     */
    private UsuarioDTO usuario;
    /**
     * Nuevo documento adiional
     */
    private SolicitudDocumentacion documentoAdicionalSeleccionado;
    /**
     * Documento anexo seleccionado
     */
    private Documento documentoAnexoSeleccionado;
    /**
     * Documento basico seleccionado
     */
    private SolicitudDocumentacion documentoBasicoSeleccionado;
    /**
     * Descripción del nuevo documento adicional
     */
    private String descripcionNuevoAdicional;
    /**
     * Solicitud relacionada el la documentación
     */
    private Solicitud solicitud;
    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;
    /**
     * tipo del documento adicional agregado.
     */
    private TipoDocumento tipoNuevoDocumentoAdicional;
    /**
     * listado tipos de documentos
     */
    private List<TipoDocumento> listaTiposDocumento;
    /**
     * listado tipos de documentos adicionales
     */
    private List<TipoDocumento> listaTiposDocumentoAdicionales;
    /**
     * Avaluo realcionado al sec_radicado
     */
    private Avaluo avaluo;
    /**
     * Documento para almacenar de manera temporal el archivo cargado
     */
    private Documento documentoAnexoCargado;
    /**
     * Indica si el documento anexo fue aportado
     */
    private boolean docAportado = true;

    /**
     * DTO con los datos los documentoas para la visualización.
     */
    private ReporteDTO documentoAVsiualizar;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportes = ReportesUtil.getInstance();

    // --------getters-setters----------------
    public String getDescripcionDocSelecionado() {
        return this.descripcionDocSelecionado;
    }

    public void setDescripcionDocSelecionado(String descripcionDocSelecionado) {
        this.descripcionDocSelecionado = descripcionDocSelecionado;
    }

    public ReporteDTO getDocumentoAVsiualizar() {
        return documentoAVsiualizar;
    }

    public void setDocumentoAVsiualizar(ReporteDTO documentoAVsiualizar) {
        this.documentoAVsiualizar = documentoAVsiualizar;
    }

    public List<TipoDocumento> getListaTiposDocumentoAdicionales() {
        return listaTiposDocumentoAdicionales;
    }

    public void setListaTiposDocumentoAdicionales(List<TipoDocumento> listaTiposDocumentoAdicionales) {
        this.listaTiposDocumentoAdicionales = listaTiposDocumentoAdicionales;
    }

    public boolean isDocAportado() {
        return docAportado;
    }

    public void setDocAportado(boolean docAportado) {
        this.docAportado = docAportado;
    }

    public Documento getDocumentoAnexoCargado() {
        return documentoAnexoCargado;
    }

    public void setDocumentoAnexoCargado(Documento documentoAnexoCargado) {
        this.documentoAnexoCargado = documentoAnexoCargado;
    }

    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public List<TipoDocumento> getListaTiposDocumento() {
        return this.listaTiposDocumento;
    }

    public void setListaTiposDocumento(List<TipoDocumento> listaTiposDocumento) {
        this.listaTiposDocumento = listaTiposDocumento;
    }

    public TipoDocumento getTipoNuevoDocumentoAdicional() {
        return this.tipoNuevoDocumentoAdicional;
    }

    public void setTipoNuevoDocumentoAdicional(
        TipoDocumento tipoNuevoDocumentoAdicional) {
        this.tipoNuevoDocumentoAdicional = tipoNuevoDocumentoAdicional;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getTipoMimeDocTemporal() {
        return this.tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public boolean isBanderaModoLecturaHabilitado() {
        return this.banderaModoLecturaHabilitado;
    }

    public void setBanderaModoLecturaHabilitado(
        boolean banderaModoLecturaHabilitado) {
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
    }

    public SolicitudDocumentacion getDocumentoBasicoSeleccionado() {
        return this.documentoBasicoSeleccionado;
    }

    public void setDocumentoBasicoSeleccionado(
        SolicitudDocumentacion documentoBasicoSeleccionado) {
        this.documentoBasicoSeleccionado = documentoBasicoSeleccionado;
    }

    public String getDescripcionNuevoAdicional() {
        return this.descripcionNuevoAdicional;
    }

    public void setDescripcionNuevoAdicional(String descripcionNuevoAdicional) {
        this.descripcionNuevoAdicional = descripcionNuevoAdicional;
    }

    public List<Documento> getDocumentosAnexosSeleccionados() {
        return this.documentosAnexosSeleccionados;
    }

    public void setDocumentosAnexosSeleccionados(
        List<Documento> documentosAnexosSeleccionados) {
        this.documentosAnexosSeleccionados = documentosAnexosSeleccionados;
    }

    public SolicitudDocumentacion getDocumentoAdicionalSeleccionado() {
        return this.documentoAdicionalSeleccionado;
    }

    public void setDocumentoAdicionalSeleccionado(
        SolicitudDocumentacion documentoAdicionalSeleccionado) {
        this.documentoAdicionalSeleccionado = documentoAdicionalSeleccionado;
    }

    public Documento getDocumentoAnexoSeleccionado() {
        return this.documentoAnexoSeleccionado;
    }

    public void setDocumentoAnexoSeleccionado(
        Documento documentoAnexoSeleccionado) {
        this.documentoAnexoSeleccionado = documentoAnexoSeleccionado;
    }

    public String getRadicacion() {
        return this.radicacion;
    }

    public void setRadicacion(String radicacion) {
        this.radicacion = radicacion;
    }

    public String getSecRadicado() {
        return this.secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    public Date getFechaRadicacion() {
        return this.fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public String getTipoTramite() {
        return this.tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    public Solicitante getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
    }

    public List<SolicitudDocumentacion> getDocumentosBasicos() {
        return this.documentosBasicos;
    }

    public void setDocumentosBasicos(List<SolicitudDocumentacion> documentosBasicos) {
        this.documentosBasicos = documentosBasicos;
    }

    public List<SolicitudDocumentacion> getDocumentosAdicionales() {
        return this.documentosAdicionales;
    }

    public void setDocumentosAdicionales(
        List<SolicitudDocumentacion> documentosAdicionales) {
        this.documentosAdicionales = documentosAdicionales;
    }

    public boolean isBanderaCargarAnexo() {
        return this.banderaCargarAnexo;
    }

    public void setBanderaCargarAnexo(boolean banderaCargarAnexo) {
        this.banderaCargarAnexo = banderaCargarAnexo;
    }

    public boolean isBanderaReemplazarAnexo() {
        return this.banderaReemplazarAnexo;
    }

    public void setBanderaReemplazarAnexo(boolean banderaReemplazarAnexo) {
        this.banderaReemplazarAnexo = banderaReemplazarAnexo;
    }

    public boolean isBanderaEliminaAnexo() {
        return this.banderaEliminaAnexo;
    }

    public void setBanderaEliminaAnexo(boolean banderaEliminaAnexo) {
        this.banderaEliminaAnexo = banderaEliminaAnexo;
    }

    public boolean isBanderaEliminarDocumentoAdicional() {
        return this.banderaEliminarDocumentoAdicional;
    }

    public void setBanderaEliminarDocumentoAdicional(
        boolean banderaEliminarDocumentoAdicional) {
        this.banderaEliminarDocumentoAdicional = banderaEliminarDocumentoAdicional;
    }

    public boolean isBanderaAdicionar() {
        return this.banderaAdicionar;
    }

    public void setBanderaAdicionar(boolean banderaAdicionar) {
        this.banderaAdicionar = banderaAdicionar;
    }

    public boolean isBanderaGuardar() {
        return this.banderaGuardar;
    }

    public void setBanderaGuardar(boolean banderaGuardar) {
        this.banderaGuardar = banderaGuardar;
    }

    public boolean isBanderaEnviarDocumentos() {
        return this.banderaEnviarDocumentos;
    }

    public void setBanderaEnviarDocumentos(boolean banderaEnviarDocumentos) {
        this.banderaEnviarDocumentos = banderaEnviarDocumentos;
    }

    public boolean isBanderaConsultarAnexo() {
        return this.banderaConsultarAnexo;
    }

    public void setBanderaConsultarAnexo(boolean banderaConsultarAnexo) {
        this.banderaConsultarAnexo = banderaConsultarAnexo;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdministracionDocumentacionTramiteAvaluoMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables y datos de prueba
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.documentosAdicionales = new ArrayList<SolicitudDocumentacion>();
        this.documentosBasicos = new ArrayList<SolicitudDocumentacion>();
        this.documentoAdicionalSeleccionado = new SolicitudDocumentacion();
        this.documentoAdicionalSeleccionado
            .setDocumentosSolicitud(new ArrayList<Documento>());
        this.documentoBasicoSeleccionado = new SolicitudDocumentacion();
        this.documentoBasicoSeleccionado
            .setDocumentosSolicitud(new ArrayList<Documento>());
        this.listaTiposDocumento = this.getGeneralesService()
            .getAllTipoDocumento();
    }

    /**
     *
     * Método para cargar la información necesaria para la ejecución del caso de uso, el metodo se
     * usa desde el caso de uso que deba asociar documentos a un tramite y recibe como parametro el
     * avaluo relacionado al tramite, el parametro avaluo no debe ser nulo
     *
     *
     * @author felipe.cadena
     *
     * @param solicitud
     * @param solicitante
     * @param usuario
     * @param documentosExistentes Lista de documentos si se trata de una solicitud existente
     * @param avaluo avaluo relacionado al sec_radicado
     */
    public void cargarVariablesExternasAvaluo(Solicitud solicitud,
        boolean banderaModoLecturaHabilitado,
        SolicitanteSolicitud solicitante, UsuarioDTO usuario,
        List<SolicitudDocumentacion> documentosExistentes, Avaluo avaluo) {

        if (avaluo != null) {
            this.secRadicado = avaluo.getSecRadicado();
            this.avaluo = avaluo;
            this.banderaSecRadicadoAsociado = true;
            this.cargarVariablesExternas(solicitud, banderaModoLecturaHabilitado, solicitante,
                usuario, documentosExistentes);
        }

    }

    /**
     *
     * Método para cargar la información necesaria para la ejecución del caso de uso, el metodo se
     * usa desde el caso de uso que desee incluir la administración de documentos, se utiliza para
     * consulta de docuemntos realacionados a una solcitud, o para el ingreso de la solicitud.
     *
     *
     * @author felipe.cadena
     *
     * @param solicitud
     * @param solicitante
     * @param usuario
     * @param documentosExistentes Lista de documentos si se trata de una solicitud existente
     */
    public void cargarVariablesExternas(Solicitud solicitud, boolean banderaModoLecturaHabilitado,
        SolicitanteSolicitud solicitante, UsuarioDTO usuario,
        List<SolicitudDocumentacion> documentosExistentes) {
        this.solicitud = solicitud;
        this.solicitante = solicitante.getSolicitante();
        this.tipoTramite = solicitud.getTipo();
        this.usuario = usuario;
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;

        if (this.avaluo == null) {
            this.banderaSecRadicadoAsociado = false;
        }

        this.documentosAdicionales = new ArrayList<SolicitudDocumentacion>();
        this.documentosBasicos = new ArrayList<SolicitudDocumentacion>();

        List<TipoDocumento> docsRequeridos = this.getGeneralesService().
            getCacheTiposDocumentoPorTipoTramite(solicitud.getTipo(), null, null);

        if (!documentosExistentes.isEmpty()) {

            for (SolicitudDocumentacion solicitudDocumentacion : documentosExistentes) {

                if (solicitudDocumentacion.getAdicional().equals(ESiNo.SI.getCodigo())) {
                    this.documentosAdicionales.add(solicitudDocumentacion);
                } else {
                    this.documentosBasicos.add(solicitudDocumentacion);
                }
            }
        }

        List<SolicitudDocumentacion> listaAuxiliar = new ArrayList<SolicitudDocumentacion>();

        int len = this.documentosBasicos.size();
        for (int i = 0; i < docsRequeridos.size(); i++) {
            TipoDocumento tipoDocumento = docsRequeridos.get(i);
            tipoDocumento.setDocumentos(new ArrayList<Documento>());
            SolicitudDocumentacion sd = new SolicitudDocumentacion();
            sd.setTipoDocumento(tipoDocumento);
            sd.setAportado(ESiNo.NO.getCodigo());

            if (documentosExistentes.isEmpty() || documentosExistentes == null) {
                this.documentosBasicos.add(sd);
            } else {
                boolean existe = false;
                for (int j = 0; j < len; j++) {
                    SolicitudDocumentacion solicitudDocumentacion = this.documentosBasicos.get(j);
                    if ((solicitudDocumentacion.getTipoDocumento().getId() == sd.getTipoDocumento().
                        getId())) {
                        existe = true;
                    }
                }
                if (!existe) {
                    this.documentosBasicos.add(sd);
                }
            }

        }

        this.documentosBasicos.addAll(listaAuxiliar);

        for (SolicitudDocumentacion sd : documentosBasicos) {
            if (sd.getDocumentosSolicitud() == null) {
                sd.setDocumentosSolicitud(new ArrayList<Documento>());
            }
            this.banderaCargaAnexoDocBasico = true;
            this.evaluarAportados();
        }
        for (SolicitudDocumentacion sd : documentosAdicionales) {
            if (sd.getDocumentosSolicitud() == null) {
                sd.setDocumentosSolicitud(new ArrayList<Documento>());
            }
            this.banderaCargaAnexoDocBasico = false;
            this.evaluarAportados();
        }

        this.listaTiposDocumentoAdicionales = this.cargarTipoDocsAdicionales();

    }

    /**
     * Método para cargar los tipos de documentos adicionales
     *
     * @author felipe.cadena
     * @return
     */
    public List<TipoDocumento> cargarTipoDocsAdicionales() {

        List<TipoDocumento> docs = this.getGeneralesService().
            obtenerTipoDocumentoAvaluoAdicional(this.tipoTramite);

        return docs;
    }

    /**
     * Método para cargar los anexo relacionado a un documento
     *
     * Se llama desde administrarAnexos.xhtml, (p:fileUpload id="fu")
     *
     * @author felipe.cadena
     */
    public void cargarAnexo(FileUploadEvent event) {
        LOGGER.debug("Cargando archivo anexo...");
        this.documentoAnexoCargado = new Documento();
        this.documentoAnexoCargado.setArchivo(event.getFile().getFileName());
        //this.documentoAnexoCargado.setDescripcion(event.getFile().getFileName());

        File archivo = null;

        String ruta = event.getFile().getFileName();

        try {

            String directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();

            archivo = new File(directorioTemporal, ruta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(archivo);

            byte[] buffer = new byte[Math.round(event.getFile().getSize())];

            int bulk;
            InputStream inputStream = event.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

        } catch (Exception e) {
            String mensaje = "Error al cargar el documento";
            this.addMensajeError(mensaje);
        }

        LOGGER.debug("Anexos cargados en " + archivo.getPath());
    }

    /**
     * Método para inicializar las variables de la carga del anexo.
     *
     * Se llama desde administrarAnexos.xhtml (p:commandButton id="cargarNuevoAnexoBtn")
     */
    public void prepararCargaAnexo() {
        LOGGER.debug("Preparando carga anexo");

        this.documentoAnexoCargado = new Documento();

    }

    /**
     * Metodo para relacionar el anexo cargado al grupo de documentos
     */
    public void relacionarAnexo() {
        LOGGER.debug("Relacionando anexo" + this.documentoAnexoCargado.getArchivo());

        if (this.docAportado) {
            this.documentoAnexoCargado.setEstado(EDocumentoEstado.SIN_REGISTRAR_SOLICITUD.
                getCodigo());
        } else {
            this.documentoAnexoCargado.setEstado(EDocumentoEstado.SIN_CARGAR.getCodigo());
        }
        if (this.banderaCargaAnexoDocBasico) {
            this.documentoBasicoSeleccionado.getDocumentosSolicitud().
                add(this.documentoAnexoCargado);
            this.cargarAnexosDocumento();
        } else {
            this.documentoAdicionalSeleccionado.getDocumentosSolicitud().add(
                this.documentoAnexoCargado);
            this.cargarAnexosDocumentoAdicional();
        }
        this.evaluarAportados();
    }

    /**
     * Método para cargar los anexos existentes relacionados a un documento basico
     *
     * Se llama desde administracionDocumentacionAvaluoComercial.xhtml (p:commandButton
     * id="cargaAnexoBtn")
     *
     * @author felipe.cadena
     */
    public void cargarAnexosDocumento() {
        LOGGER.debug("Cargando anexos relacionados...");
        this.banderaCargaAnexoDocBasico = true;
        this.documentosAnexosSeleccionados = new ArrayList<Documento>();

        this.documentosAnexosSeleccionados = this.documentoBasicoSeleccionado
            .getDocumentosSolicitud();
        this.descripcionDocSelecionado = this.documentoBasicoSeleccionado.getTipoDocumento().
            getNombre();
        LOGGER.debug("Doc Basico " + this.banderaCargaAnexoDocBasico);
        LOGGER.debug("Anexos cargados");
    }

    /**
     * Método para cargar los anexos existentes relacionados a un documento adicional
     *
     * Se llama desde administracionDocumentacionAvaluoComercial.xhtml (p:commandButton
     * id="cargarAnexoAdicionalBtn")
     *
     * @author felipe.cadena
     */
    public void cargarAnexosDocumentoAdicional() {
        LOGGER.debug("Cargando anexos relacionados adicional...");
        this.banderaCargaAnexoDocBasico = false;
        this.documentosAnexosSeleccionados = new ArrayList<Documento>();
        this.documentosAnexosSeleccionados = this.documentoAdicionalSeleccionado.
            getDocumentosSolicitud();
        this.descripcionDocSelecionado = this.documentoAdicionalSeleccionado.getTipoDocumento().
            getNombre();
        LOGGER.debug("Doc Basico " + this.banderaCargaAnexoDocBasico);
        LOGGER.debug("Anexos Adicional cargados ");
    }

    /**
     * Método para eliminar anexo
     *
     * Se llama desde el dialogo de confirmación de eliminar anexo
     *
     * @author felipe.cadena
     *
     */
    public void eliminarAnexo() {

        LOGGER.debug("Eliminando anexo");
        LOGGER.debug("Anexo Sleccionado " +
             this.documentoAnexoSeleccionado.getDescripcion());
        this.documentosAnexosSeleccionados.remove(this.documentoAnexoSeleccionado);

        if (this.banderaCargaAnexoDocBasico) {
            this.documentoBasicoSeleccionado.getDocumentosSolicitud().remove(
                this.documentoAnexoSeleccionado);

        } else {
            this.documentoAdicionalSeleccionado.getDocumentosSolicitud()
                .remove(documentoAnexoSeleccionado);
        }
        this.evaluarAportados();
        LOGGER.debug("Anexo eliminado");

    }

    /**
     * Método para determinar si un item de la tabla esta aportado o no.
     *
     * @author felipe.cadena
     */
    public void evaluarAportados() {
        if (this.banderaCargaAnexoDocBasico) {
            if (this.documentoBasicoSeleccionado.getDocumentosSolicitud().isEmpty()) {
                this.documentoBasicoSeleccionado.setAportado(ESiNo.NO.getCodigo());
            } else {
                for (Documento doc : this.documentoBasicoSeleccionado.getDocumentosSolicitud()) {
                    if (doc.getEstado().equals(EDocumentoEstado.SIN_CARGAR.getCodigo())) {
                        this.documentoBasicoSeleccionado.setAportado(ESiNo.NO.getCodigo());
                        break;
                    }
                }
                this.documentoBasicoSeleccionado.setAportado(ESiNo.SI.getCodigo());
            }

        } else {
            if (this.documentoAdicionalSeleccionado.getDocumentosSolicitud().isEmpty()) {
                this.documentoAdicionalSeleccionado.setAportado(ESiNo.NO.getCodigo());
            } else {
                for (Documento doc : this.documentoAdicionalSeleccionado.getDocumentosSolicitud()) {

                    if (doc.getEstado().equals(EDocumentoEstado.SIN_CARGAR.getCodigo())) {
                        this.documentoAdicionalSeleccionado.setAportado(ESiNo.NO.getCodigo());
                        break;
                    }
                }
                this.documentoAdicionalSeleccionado.setAportado(ESiNo.SI.getCodigo());
            }
        }

    }

    /**
     * Método para agregar documentos adicionales
     *
     * Se llama desde el dialogo de confirmación para agregar un documento adicional
     *
     * @author felipe.cadena
     *
     */
    public void agregarDocumentoAdicional() {
        LOGGER.debug("Agregando documento adicional...");

        SolicitudDocumentacion nuevoAdicional = new SolicitudDocumentacion();
        nuevoAdicional.setAportado(ESiNo.NO.getCodigo());
        nuevoAdicional.setAdicional(ESiNo.SI.getCodigo());
        nuevoAdicional.setSolicitud(this.solicitud);
        nuevoAdicional.setTipoDocumento(this.tipoNuevoDocumentoAdicional);
        nuevoAdicional.setDocumentosSolicitud(new ArrayList<Documento>());
        nuevoAdicional.setDetalle(descripcionNuevoAdicional);

        this.documentosAdicionales.add(nuevoAdicional);

        LOGGER.debug("Documento agregado " + this.descripcionNuevoAdicional);

    }

    /**
     * Método para eliminar documentos adicionales
     *
     * Se llama desde el dialogo de confirmación para eliminar documento adicional
     *
     * @author felipe.cadena
     */
    public void eliminarDocumentoAdicional() {
        LOGGER.debug("Eliminar documentos adicionales");
        LOGGER.debug("Eliminando ..." +
             this.documentoAdicionalSeleccionado.getDetalle());

        this.documentosAdicionales.remove(this.documentoAdicionalSeleccionado);

        LOGGER.debug("Documentos eliminados");
    }

    /**
     * Método para almacenar documentos anexos
     *
     * Se llama desde el dialogo de confirmación para almacenar los documentos de la solcitud, usado
     * en diligenciarSolicitud.xhtml
     *
     * @author felipe.cadena
     */
    public void almacenarDocumentosAnexos() {
        LOGGER.debug("Almacenando documentos...");

        //guardar documentos basicos
        for (SolicitudDocumentacion solicitudDocumentacion : this.documentosBasicos) {

            for (Documento doc : solicitudDocumentacion.getDocumentosSolicitud()) {
                if (doc.getEstado().equals(EDocumentoEstado.SIN_REGISTRAR_SOLICITUD.getCodigo())) {

                    SolicitudDocumentacion solicitudAGuardar = new SolicitudDocumentacion();
                    try {

                        if (this.banderaSecRadicadoAsociado) {
                            doc.setEstado(EDocumentoEstado.SOLICITUD_DOCS_REGISTRADA.getCodigo());
                            doc.setTramiteId(this.avaluo.getTramite().getId());
                        } else {
                            doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                        }
                        doc.setTipoDocumento(solicitudDocumentacion.getTipoDocumento());
                        doc.setUsuarioCreador(this.usuario.getLogin());
                        doc.setBloqueado(ESiNo.NO.getCodigo());

                        doc.setUsuarioLog(this.usuario.getLogin());
                        doc.setFechaLog(new Date());
                        doc.setFechaRadicacion(this.solicitud.getFecha());
                        doc.setNumeroRadicacion(this.solicitud.getNumero());

                        //objeto para almacenar la informacion a cargar en alfresco
                        DocumentoSolicitudDTO docSolicitudDTO = DocumentoSolicitudDTO.
                            crearArgumentoCargarSolicitudAvaluoComercial(
                                this.solicitud.getNumero(),
                                this.solicitud.getFecha(),
                                doc.getTipoDocumento().getNombre(),
                                UtilidadesWeb.obtenerRutaTemporalArchivos() + doc.getArchivo());

                        doc = this.getGeneralesService().guardarDocumento(doc);

                        solicitudAGuardar.setSoporteDocumentoId(doc);
                        solicitudAGuardar.setAdicional(ESiNo.NO.getCodigo());
                        solicitudAGuardar.setAportado(ESiNo.SI.getCodigo());
                        solicitudAGuardar.setFechaAporte(new Date());
                        solicitudAGuardar.setSolicitud(this.solicitud);
                        solicitudAGuardar.setUsuarioLog(this.usuario.getLogin());
                        solicitudAGuardar.setFechaLog(new Date());
                        solicitudAGuardar.
                            setTipoDocumento(solicitudDocumentacion.getTipoDocumento());
                        solicitudAGuardar.setDetalle(doc.getDescripcion());

                        this.getTramiteService()
                            .guardarActualizarSolicitudDocumentacion(solicitudAGuardar);

                        //guardar documento en alfresco
                        this.getGeneralesService().guardarDocumentosSolicitudAvaluoAlfresco(
                            this.usuario, docSolicitudDTO, doc);

                    } catch (Exception e) {
                        LOGGER.error("Error al persistir los documentos basicos de la solicitud");
                    }

                } else if (doc.getEstado().equals(EDocumentoEstado.SIN_CARGAR.getCodigo())) {

                    SolicitudDocumentacion solicitudAGuardar = new SolicitudDocumentacion();

                    solicitudAGuardar.setAdicional(ESiNo.NO.getCodigo());
                    solicitudAGuardar.setAportado(ESiNo.NO.getCodigo());
                    solicitudAGuardar.setSolicitud(this.solicitud);
                    solicitudAGuardar.setTipoDocumento(solicitudDocumentacion.getTipoDocumento());
                    solicitudAGuardar.setUsuarioLog(this.usuario.getLogin());
                    solicitudAGuardar.setFechaLog(new Date());
                    solicitudAGuardar.setDetalle(doc.getDescripcion());
                    this.getTramiteService()
                        .guardarActualizarSolicitudDocumentacion(solicitudAGuardar);

                } else {
                    //se asocian los documentos cargados anteriormente al tramite, si aun no lo estan
                    if (this.banderaSecRadicadoAsociado && doc.getEstado().equals(
                        EDocumentoEstado.RECIBIDO.getCodigo())) {
                        doc.setEstado(EDocumentoEstado.SOLICITUD_DOCS_REGISTRADA.getCodigo());
                        doc.setTramiteId(this.avaluo.getTramite().getId());
                        doc = this.getGeneralesService().actualizarDocumento(doc);
                    }
                }
            }

        }

        // guardar documentos adicionales
        for (SolicitudDocumentacion solicitudDocumentacion : this.documentosAdicionales) {

            for (Documento doc : solicitudDocumentacion.getDocumentosSolicitud()) {
                if (doc.getEstado().equals(EDocumentoEstado.SIN_CARGAR.getCodigo())) {
                    try {
                        SolicitudDocumentacion solicitudAGuardar = new SolicitudDocumentacion();

                        if (this.banderaSecRadicadoAsociado) {
                            doc.setEstado(EDocumentoEstado.SOLICITUD_DOCS_REGISTRADA.getCodigo());
                            doc.setTramiteId(this.avaluo.getTramite().getId());
                        } else {
                            doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                        }
                        doc.setTipoDocumento(solicitudDocumentacion.getTipoDocumento());
                        doc.setUsuarioCreador(this.usuario.getLogin());
                        doc.setBloqueado(ESiNo.NO.getCodigo());
                        doc.setUsuarioLog(this.usuario.getLogin());
                        doc.setFechaLog(new Date());
                        doc.setFechaRadicacion(solicitud.getFecha());
                        doc.setNumeroRadicacion(solicitud.getNumero());

                        //objeto para almacenar la informacion a cargar en alfresco
                        DocumentoSolicitudDTO docSolicitudDTO = DocumentoSolicitudDTO.
                            crearArgumentoCargarSolicitudAvaluoComercial(
                                this.solicitud.getNumero(),
                                this.solicitud.getFecha(),
                                doc.getTipoDocumento().getNombre(),
                                UtilidadesWeb.obtenerRutaTemporalArchivos() + doc.getArchivo());

                        doc = this.getGeneralesService().guardarDocumento(doc);

                        solicitudAGuardar.setSoporteDocumentoId(doc);
                        solicitudAGuardar.setDetalle(solicitudDocumentacion.getDetalle());
                        solicitudAGuardar.setAdicional(ESiNo.SI.getCodigo());
                        solicitudAGuardar.setAportado(ESiNo.SI.getCodigo());
                        solicitudAGuardar.setFechaAporte(new Date());
                        solicitudAGuardar.setSolicitud(this.solicitud);
                        solicitudAGuardar.setUsuarioLog(this.usuario.getLogin());
                        solicitudAGuardar.setFechaLog(new Date());
                        solicitudAGuardar.
                            setTipoDocumento(solicitudDocumentacion.getTipoDocumento());
                        solicitudAGuardar.setDetalle(doc.getDescripcion());

                        this.getTramiteService()
                            .guardarActualizarSolicitudDocumentacion(solicitudAGuardar);

                        //guardar documento en alfresco
                        this.getGeneralesService().guardarDocumentosSolicitudAvaluoAlfresco(usuario,
                            docSolicitudDTO, doc);

                    } catch (Exception e) {
                        LOGGER.
                            error("Error al persistir los documentos adicionales de la solicitud");
                    }

                } else if (doc.getEstado().equals(EDocumentoEstado.SIN_CARGAR.getCodigo())) {

                    SolicitudDocumentacion solicitudAGuardar = new SolicitudDocumentacion();

                    solicitudAGuardar.setAdicional(ESiNo.SI.getCodigo());
                    solicitudAGuardar.setAportado(ESiNo.NO.getCodigo());
                    solicitudAGuardar.setSolicitud(this.solicitud);
                    solicitudAGuardar.setTipoDocumento(solicitudDocumentacion.getTipoDocumento());
                    solicitudAGuardar.setUsuarioLog(this.usuario.getLogin());
                    solicitudAGuardar.setFechaLog(new Date());
                    solicitudAGuardar.setDetalle(doc.getDescripcion());
                    this.getTramiteService()
                        .guardarActualizarSolicitudDocumentacion(solicitudAGuardar);

                } else {
                    //se asocian los documentos cargados anteriormente al tramite, si aun no lo estan
                    if (this.banderaSecRadicadoAsociado && doc.getEstado().equals(
                        EDocumentoEstado.RECIBIDO.getCodigo())) {
                        doc.setEstado(EDocumentoEstado.SOLICITUD_DOCS_REGISTRADA.getCodigo());
                        doc.setTramiteId(this.avaluo.getTramite().getId());
                        doc = this.getGeneralesService().actualizarDocumento(doc);
                    }
                }
            }
        }

        LOGGER.debug("Documentos almacenados");
    }

    /**
     * Método para visualizar el primer anexo asociado desde la pantalla principal
     *
     * se llama desde administracionDocumentacionAvaluoComercial.xhtml (p:commandLink id="anexoCL")
     *
     * @author felipe.cadena
     */
    public void visualizarPrimerAnexo() {
        this.documentoAnexoSeleccionado = null;
        this.documentosAnexosSeleccionados = new ArrayList<Documento>();

        if (!this.documentoBasicoSeleccionado.getDocumentosSolicitud().isEmpty()) {

            this.documentoAnexoSeleccionado = this.documentoBasicoSeleccionado.
                getDocumentosSolicitud()
                .get(0);
        }
        LOGGER.debug("Preparando Documento " +
             this.documentoAnexoSeleccionado.getArchivo());
        this.visualizarDocumento();

    }

    /**
     * Método para visualizar el primer anexo asociado a un documento adicional desde la pantalla
     * principal
     *
     * se llama desde administracionDocumentacionAvaluoComercial.xhtml (p:commandLink
     * id="anexoAdicionalCL")
     *
     * @author felipe.cadena
     */
    public void visualizarPrimerAnexoAdicional() {
        this.documentoAnexoSeleccionado = null;
        this.documentosAnexosSeleccionados = new ArrayList<Documento>();

        if (!this.documentoAdicionalSeleccionado.getDocumentosSolicitud().isEmpty()) {
            this.documentoAnexoSeleccionado = this.documentoAdicionalSeleccionado.
                getDocumentosSolicitud()
                .get(0);
        }
        LOGGER.debug("Preparando Documento " +
             this.documentoAnexoSeleccionado.getArchivo());
        this.visualizarDocumento();

    }

    /**
     * Método preparacion del documento a visualizar
     *
     * @author felipe.cadena
     */
    public void visualizarDocumento() {
        LOGGER.debug("Visualizando Documento");

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoAnexoSeleccionado.getArchivo());
        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        File fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath() + "/" +
             this.documentoAnexoSeleccionado.getArchivo());
        if (this.documentoAnexoSeleccionado.getEstado().equals(
            EDocumentoEstado.SIN_REGISTRAR_SOLICITUD.getCodigo())) {
            this.documentoAVsiualizar = this.reportes.generarReporteDesdeUnFile(fileTemp);
        } else {
            this.documentoAVsiualizar = this.reportes.consultarReporteDeGestorDocumental(
                this.documentoAnexoSeleccionado.getIdRepositorioDocumentos());
        }

        previsualizacionDocMB.setReporteDTO(this.documentoAVsiualizar);

    }

    public void reemplazarAnexo() {
        LOGGER.debug("Reemplazando Documento");

        this.documentoAnexoCargado = documentoAnexoSeleccionado;

        this.eliminarAnexo();

    }
}
