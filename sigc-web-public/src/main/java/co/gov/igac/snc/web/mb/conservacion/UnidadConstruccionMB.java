package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.web.util.SNCManagedBean;

@Component("detalleUnidadConstruccion")
@Scope("session")
public class UnidadConstruccionMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 761725509620580700L;

    private Predio predio;

    private UnidadConstruccion unidadConstruccion;

    private UsoConstruccion usoConstruccion;

    public UsoConstruccion getUsoConstruccion() {
        return usoConstruccion;
    }

    public void setUsoConstruccion(UsoConstruccion usoConstruccion) {
        this.usoConstruccion = usoConstruccion;
    }

    private List<UnidadConstruccionComp> componentes;

    public List<UnidadConstruccionComp> getComponentes() {
        return componentes;
    }

    public void setComponentes(List<UnidadConstruccionComp> componentes) {
        this.componentes = componentes;
    }

    public UnidadConstruccionMB() {

    }

    public UnidadConstruccionMB(Predio predio, UnidadConstruccion unidad) {
        this.predio = predio;
        this.unidadConstruccion = unidad;
    }

    @PostConstruct
    public void inicio() {
        this.unidadConstruccion = new UnidadConstruccion();
        this.componentes = new ArrayList<UnidadConstruccionComp>();
        this.usoConstruccion = new UsoConstruccion();

        if (predio == null) {
            predio = new Predio();
            String numeroPredial = "257540216041711057933105117116";
            predio.setNumeroPredial(numeroPredial);
        }
        String unidad = "A";
        predio = this.getConservacionService().getPredioByNumeroPredial(predio.getNumeroPredial());

        this.unidadConstruccion = this.getConservacionService().
            findUnidadConstruccionByPredioANDUnidad(predio, unidad);
        this.componentes = this.getConservacionService().
            findUnidadConstruccionCompByUnidadConstruccion(this.unidadConstruccion);
        this.usoConstruccion = this.unidadConstruccion.getUsoConstruccion();
    }

    public UnidadConstruccion getUnidadConstruccion() {
        return unidadConstruccion;
    }

    public void setUnidadConstruccion(UnidadConstruccion unidadConstruccion) {
        this.unidadConstruccion = unidadConstruccion;
    }

    public Predio getPredio() {
        return predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }
}
