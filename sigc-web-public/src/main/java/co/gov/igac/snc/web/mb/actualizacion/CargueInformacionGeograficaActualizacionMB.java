/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.actualizacion;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import java.io.FileNotFoundException;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.EValoresActualizacionEstado;
import co.gov.igac.snc.util.FiltroCargueDTO;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECargueCicaEstado;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author hector.arias
 */
@Component("cargueInformacionGeograficaActualizacion")
@Scope("session")
public class CargueInformacionGeograficaActualizacionMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CargueInformacionGeograficaActualizacionMB.class);
    
    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Variable con el codigo del departamento seleccionado
     */
    private String departamentoSeleccionado;

    /**
     * Variable item list con los valores de las vigencias
     */
    private List<SelectItem> vigenciasItemList;

    /**
     * Variable con el año de la vigencia
     */
    private String vigenciaSeleccionada;

    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosItemList;

    /**
     * Variable con el codigo del departamento seleccionado
     */
    private String municipioSeleccionado;

    /**
     * Variable item list con los valores de los departamentos disponibles
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Poceso actual asociado al municipio y a la vigencias seleccionadas
     */
    private AxMunicipio axMunicipioActual;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     *
     * Variable que establece si el cargue contiene zonas
     */
    private String cargueZonal;

    /**
     * Variable que habilita las opciones de cargue zonal
     */
    private boolean banderaCargueZonal;
    
    /**
     * Variable que habilita las opciones de cargue predial
     */
    private boolean banderaCarguePredial;

    /**
     *
     * Variable que habilita el tab de cargar información geográfica
     */
    private boolean banderaCargueGeografico;

    /**
     * Ruta local archivo de actualizacion
     */
    private String rutaArchivoDatosActualizacion;

    private boolean banderaArchivoCargado;

    /**
     * Archivo con la informacion de cargue del proceso de actualizacion
     */
    private File archivoDatosActualizacion;

    /**
     * Lista de procesos de cargue.
     */
    private List<MunicipioActualizacion> procesosCargue;

    /**
     * Variable que habilita el botón de validación
     */
    private boolean habilitarValidacion;

    /**
     * Variable que almacena la ubicación de la file gdb en formato 7z
     */
    private String ubicacionFileGdb;

    private boolean permitirCargue;

    private String validacionPredios;

    private String validacionZonas;

    private String estadoArchivoCargadoPredial;

    private String estadoArchivoCargadoZonal;

    private String nombreArchivoCargadoPredial;

    private String nombreArchivoCargadoZonal;
    
    private MunicipioActualizacion cargueSeleccionado;
    
    private String estadoValidacionActualizacion;
    
    private String baseDeDatosExistente;
    
    /**
     * Variable que habilita el botón de descargar de inconsistencias
     * de valicación geográfica de actualización
     */
    private boolean habilitarDescarga;
    
    private File archivoErroresValidacion;
    
    /**
     * Es un objeto para poder usar el componente FileDownload de prime que sirve para que el
     * usuario puedad descargar el documento a la máquina local
     */
    private StreamedContent streamedContentReporte;
    
    /**
     * Variable de tipo streamed content para la descarga de reporte de inconsistencias
     */
    private StreamedContent reporteInconsistencias;
    
    /**
     * Tipo mime del documento
     */
    private String tipoMimeReporte;
    
    private String ultimoCargue;
    
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();
    
    private String nombreValidacion;
    
    private ReporteDTO rutaArchivoInconsistencias;
    
    private Boolean reiniciarValidacion;

    public String getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    public void setDepartamentoSeleccionado(String departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    public List<SelectItem> getVigenciasItemList() {
        return vigenciasItemList;
    }

    public void setVigenciasItemList(List<SelectItem> vigenciasItemList) {
        this.vigenciasItemList = vigenciasItemList;
    }

    public String getVigenciaSeleccionada() {
        return vigenciaSeleccionada;
    }

    public void setVigenciaSeleccionada(String vigenciaSeleccionada) {
        this.vigenciaSeleccionada = vigenciaSeleccionada;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public String getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(String municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public Map<String, List<SelectItem>> getMunicipiosDeptos() {
        return municipiosDeptos;
    }

    public void setMunicipiosDeptos(Map<String, List<SelectItem>> municipiosDeptos) {
        this.municipiosDeptos = municipiosDeptos;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public AxMunicipio getAxMunicipioActual() {
        return axMunicipioActual;
    }

    public void setAxMunicipioActual(AxMunicipio axMunicipioActual) {
        this.axMunicipioActual = axMunicipioActual;
    }

    public String getCargueZonal() {
        return cargueZonal;
    }

    public void setCargueZonal(String cargueZonal) {
        this.cargueZonal = cargueZonal;
    }

    public TareasPendientesMB getTareasPendientesMB() {
        return tareasPendientesMB;
    }

    public void setTareasPendientesMB(TareasPendientesMB tareasPendientesMB) {
        this.tareasPendientesMB = tareasPendientesMB;
    }

    public boolean isBanderaCargueZonal() {
        return banderaCargueZonal;
    }

    public void setBanderaCargueZonal(boolean banderaCargueZonal) {
        this.banderaCargueZonal = banderaCargueZonal;
    }

    public boolean isBanderaCargueGeografico() {
        return banderaCargueGeografico;
    }

    public void setBanderaCargueGeografico(boolean banderaCargueGeografico) {
        this.banderaCargueGeografico = banderaCargueGeografico;
    }

    public String getRutaArchivoDatosActualizacion() {
        return rutaArchivoDatosActualizacion;
    }

    public void setRutaArchivoDatosActualizacion(String rutaArchivoDatosActualizacion) {
        this.rutaArchivoDatosActualizacion = rutaArchivoDatosActualizacion;
    }

    public boolean isBanderaArchivoCargado() {
        return banderaArchivoCargado;
    }

    public void setBanderaArchivoCargado(boolean banderaArchivoCargado) {
        this.banderaArchivoCargado = banderaArchivoCargado;
    }

    public File getArchivoDatosActualizacion() {
        return archivoDatosActualizacion;
    }

    public void setArchivoDatosActualizacion(File archivoDatosActualizacion) {
        this.archivoDatosActualizacion = archivoDatosActualizacion;
    }

    public List<MunicipioActualizacion> getProcesosCargue() {
        return procesosCargue;
    }

    public void setProcesosCargue(List<MunicipioActualizacion> procesosCargue) {
        this.procesosCargue = procesosCargue;
    }

    public boolean isPermitirCargue() {
        return permitirCargue;
    }

    public void setPermitirCargue(boolean permitirCargue) {
        this.permitirCargue = permitirCargue;
    }

    public String getValidacionPredios() {
        return validacionPredios;
    }

    public void setValidacionPredios(String validacionPredios) {
        this.validacionPredios = validacionPredios;
    }

    public String getValidacionZonas() {
        return validacionZonas;
    }

    public void setValidacionZonas(String validacionZonas) {
        this.validacionZonas = validacionZonas;
    }

    public boolean isHabilitarValidacion() {
        return habilitarValidacion;
    }

    public void setHabilitarValidacion(boolean habilitarValidacion) {
        this.habilitarValidacion = habilitarValidacion;
    }

    public String getUbicacionFileGdb() {
        return ubicacionFileGdb;
    }

    public void setUbicacionFileGdb(String ubicacionFileGdb) {
        this.ubicacionFileGdb = ubicacionFileGdb;
    }

    public String getEstadoArchivoCargadoPredial() {
        return estadoArchivoCargadoPredial;
    }

    public void setEstadoArchivoCargadoPredial(String estadoArchivoCargadoPredial) {
        this.estadoArchivoCargadoPredial = estadoArchivoCargadoPredial;
    }

    public String getEstadoArchivoCargadoZonal() {
        return estadoArchivoCargadoZonal;
    }

    public void setEstadoArchivoCargadoZonal(String estadoArchivoCargadoZonal) {
        this.estadoArchivoCargadoZonal = estadoArchivoCargadoZonal;
    }

    public String getNombreArchivoCargadoPredial() {
        return nombreArchivoCargadoPredial;
    }

    public void setNombreArchivoCargadoPredial(String nombreArchivoCargadoPredial) {
        this.nombreArchivoCargadoPredial = nombreArchivoCargadoPredial;
    }

    public String getNombreArchivoCargadoZonal() {
        return nombreArchivoCargadoZonal;
    }

    public void setNombreArchivoCargadoZonal(String nombreArchivoCargadoZonal) {
        this.nombreArchivoCargadoZonal = nombreArchivoCargadoZonal;
    }

    public StreamedContent getReporteInconsistencias() {
        return reporteInconsistencias;
    }

    public void setReporteInconsistencias(StreamedContent reporteInconsistencias) {
        this.reporteInconsistencias = reporteInconsistencias;
    }

    public MunicipioActualizacion getCargueSeleccionado() {
        return cargueSeleccionado;
    }

    public void setCargueSeleccionado(MunicipioActualizacion cargueSeleccionado) {
        this.cargueSeleccionado = cargueSeleccionado;
    }

    public String getEstadoValidacionActualizacion() {
        return estadoValidacionActualizacion;
    }

    public void setEstadoValidacionActualizacion(String estadoValidacionActualizacion) {
        this.estadoValidacionActualizacion = estadoValidacionActualizacion;
    }

    public boolean isHabilitarDescarga() {
        return habilitarDescarga;
    }

    public void setHabilitarDescarga(boolean habilitarDescarga) {
        this.habilitarDescarga = habilitarDescarga;
    }
    
    public File getArchivoReporte() {
        return archivoErroresValidacion;
    }

    public void setArchivoReporte(File archivoErroresValidacion) {
        this.archivoErroresValidacion = archivoErroresValidacion;
    }

    public File getArchivoErroresValidacion() {
        return archivoErroresValidacion;
    }

    public void setArchivoErroresValidacion(File archivoErroresValidacion) {
        this.archivoErroresValidacion = archivoErroresValidacion;
    }

    public String getTipoMimeReporte() {
        return tipoMimeReporte;
    }

    public void setTipoMimeReporte(String tipoMimeReporte) {
        this.tipoMimeReporte = tipoMimeReporte;
    }

    public String getNombreValidacion() {
        return nombreValidacion;
    }

    public void setNombreValidacion(String nombreValidacion) {
        this.nombreValidacion = nombreValidacion;
    }

    public String getBaseDeDatosExistente() {
        return baseDeDatosExistente;
    }

    public void setBaseDeDatosExistente(String baseDeDatosExistente) {
        this.baseDeDatosExistente = baseDeDatosExistente;
    }

    public ReporteDTO getRutaArchivoInconsistencias() {
        return rutaArchivoInconsistencias;
    }

    public void setRutaArchivoInconsistencias(ReporteDTO rutaArchivoInconsistencias) {
        this.rutaArchivoInconsistencias = rutaArchivoInconsistencias;
    }

    public boolean isBanderaCarguePredial() {
        return banderaCarguePredial;
    }

    public void setBanderaCarguePredial(boolean banderaCarguePredial) {
        this.banderaCarguePredial = banderaCarguePredial;
    }

    public Boolean getReiniciarValidacion() {
        return reiniciarValidacion;
    }

    public void setReiniciarValidacion(Boolean reiniciarValidacion) {
        this.reiniciarValidacion = reiniciarValidacion;
    }

    public String getUltimoCargue() {
        return ultimoCargue;
    }

    public void setUltimoCargue(String ultimoCargue) {
        this.ultimoCargue = ultimoCargue;
    }

    // **************** SERVICIOS *******************
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    // --------------------------METODOS--------------------------//
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.ultimoCargue = "";
        this.cargarDepartamentos();
        this.listenerCargueZonal();
        this.permitirCargue = false;
        this.habilitarValidacion = false;
        this.habilitarDescarga = false;
        this.reiniciarValidacion = false;
        this.estadoArchivoCargadoPredial = "";
        this.estadoValidacionActualizacion = "";
        this.setNombreValidacion("Predial y zonal");
        this.cargueZonal = ESiNo.SI.toString();

        //Validación predial o zonal
        setValidacionPredios(ESiNo.NO.getCodigo());
        setValidacionZonas(ESiNo.NO.getCodigo());

        //cargar vigencias
        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
        this.vigenciasItemList = generalMB.getRangoAniosAscDesdeActual();
    }

    /**
     * Metodo para activar el panel de la carga para la base de datos zonal
     *
     * @author hector.arias
     */
    public boolean listenerCargueZonal() {

        //Si selecciona la opción que GDB incluye zonas entonces 
        if (this.getCargueZonal() == null || this.getCargueZonal().isEmpty()) {
            this.banderaCargueZonal = true;
        } else if (this.getCargueZonal().equals(ESiNo.SI.getCodigo())) {
            this.banderaCargueZonal = true;
        } else if (getCargueZonal().equals(ESiNo.NO.getCodigo())) {
            this.banderaCargueZonal = false;
        }

        return this.banderaCargueZonal;
    }

    /**
     * Metodo para cargar los departamentos asociados a la territorial.
     *
     * @author hector.arias
     */
    public void cargarDepartamentos() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList = new LinkedList<Municipio>();
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.usuario.getCodigoTerritorial());

        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getActualizacionService().
                    obtenerMunicipiosEnActualizacionPorDepto(dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                        m.getCodigo().substring(2, 5) + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    /**
     * Metodo para actualizar los usuarios asociados al rol seleccionado
     *
     * @author hector.arias
     */
    public void actualizarMunicipiosListener() {
        if (this.municipiosDeptos != null) {

            this.municipiosItemList = this.municipiosDeptos.get(this.departamentoSeleccionado);
        }
    }

    /**
     * Metodo para constuir la lista de departamentos asociados a la territorial
     *
     * @param departamentosList
     * @author hector.arias
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                    departamento.getCodigo() + "-" + departamento.getNombre()));
        }
    }

    public void onChange(ValueChangeEvent e) {
        banderaCargueZonal = !banderaCargueZonal;
    }

    public void listenerEstadoValidacionActualizacion(){
        
        ProductoCatastralJob job = new ProductoCatastralJob();
        
        job = this.getGeneralesService().obtenerJobsCargueActualizacion(ProductoCatastralJob.SIG_JOB_CARGUE_ACTUALIZACION
                , this.usuario.getLogin());
        
        Documento doc = new Documento();
        
        doc = this.getGeneralesService().buscarPorNumeroUltimoDocumentoYTipo("ACT-"+getMunicipioSeleccionado(), 
                ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getId());
        
        List<MunicipioActualizacion> munActGeo = new ArrayList<MunicipioActualizacion>(); 
        
        munActGeo = this.getActualizacionService().
                obtenerPorMunicipioYEstadoGeografico(this.municipioSeleccionado, 
                null, true, Integer.valueOf(this.vigenciaSeleccionada));

        if(munActGeo == null || munActGeo.isEmpty() || job == null){
            this.permitirCargue = true;
            this.habilitarDescarga = false;
            return;
        }
        
        if (job.getEstado().equals(ProductoCatastralJob.SNC_TERMINADO.toString())) {
            if (doc != null) {

                for (MunicipioActualizacion munAct : munActGeo) {

                    if (munAct.getArchivoGeografico() != null && munAct.getMunicipio().getCodigo().equals(this.municipioSeleccionado)) {
                        if (doc.getObservaciones().equals(ECargueCicaEstado.VALIDACION_EXITOSA.getCodigo())) {
                            munAct.setEstado(ECargueCicaEstado.VALIDADO.getCodigo());
                            this.getActualizacionService().
                                    guardarActualizarMunicipioActualizacion(munAct);
                            this.setEstadoValidacionActualizacion(ECargueCicaEstado.VALIDADO.getCodigo());
                            this.habilitarDescarga = false;
                        } else if (doc.getObservaciones().equals(ECargueCicaEstado.VALIDACION_FALLIDA.getCodigo())) {
                            munAct.setEstado(ECargueCicaEstado.ERRORES.getCodigo());
                            this.getActualizacionService().
                                    guardarActualizarMunicipioActualizacion(munAct);
                            this.setEstadoValidacionActualizacion(ECargueCicaEstado.ERRORES.getCodigo());
                            this.rutaArchivoInconsistencias = this.reportsService.descargarArchivoDeFtp(doc.getIdRepositorioDocumentos());
                        } else {
                            munAct.setEstado(ECargueCicaEstado.EN_PROCESO.getCodigo());
                            this.getActualizacionService().
                                    guardarActualizarMunicipioActualizacion(munAct);
                        }
                    }
                }
            }
        } else  if (job.getEstado().equals(ProductoCatastralJob.AGS_ERROR.toString())) {
            this.setEstadoValidacionActualizacion(ProductoCatastralJob.AGS_ERROR.toString());
            this.habilitarValidacion = false;
            this.permitirCargue = true;
        } else {
            this.setEstadoValidacionActualizacion(ECargueCicaEstado.EN_PROCESO.getCodigo());
            this.habilitarValidacion = false;
            this.permitirCargue = false;

        }
        
        
        if (this.getEstadoValidacionActualizacion().equals(ECargueCicaEstado.ERRORES.getCodigo())) {
            this.habilitarDescarga = true;
            this.habilitarValidacion = false;
            this.permitirCargue = true;
        }
        
        if (this.getEstadoValidacionActualizacion().equals(ECargueCicaEstado.VALIDADO.getCodigo())) {
            this.habilitarValidacion = false;
            this.permitirCargue = true;
        }
    }
    
    public void cargarOpcionPredios() {
        
        List<MunicipioActualizacion> munActGeo = new ArrayList<MunicipioActualizacion>(); 
        
        munActGeo = this.getActualizacionService().
                obtenerPorMunicipioYEstadoGeografico(this.municipioSeleccionado, 
                null, true, Integer.valueOf(this.vigenciaSeleccionada));
        
        if (!munActGeo.isEmpty()) {
            //Se eliminan los municipios actualización existentes
            for (MunicipioActualizacion municipioActualizacion : munActGeo) {
                this.getActualizacionService().eliminarMunicipioActualizacion(municipioActualizacion);
            }
        }
        
        if (this.archivoDatosActualizacion != null) {
            this.setBaseDeDatosExistente(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_006.toString());
        }
        
        this.setValidacionPredios(ESiNo.SI.getCodigo());
        this.setValidacionZonas(ESiNo.SI.getCodigo());
    }

    public void cargarOpcionZonas() {
        
        List<MunicipioActualizacion> munActGeo = new ArrayList<MunicipioActualizacion>(); 
        
        munActGeo = this.getActualizacionService().
                obtenerPorMunicipioYEstadoGeografico(this.municipioSeleccionado, 
                null, true, Integer.valueOf(this.vigenciaSeleccionada));
        
        if (!munActGeo.isEmpty()) {
            //Se eliminan los municipios actualización existentes
            for (MunicipioActualizacion municipioActualizacion : munActGeo) {
                this.getActualizacionService().eliminarMunicipioActualizacion(municipioActualizacion);
            }
        }

        if (this.archivoDatosActualizacion != null) {
            this.setBaseDeDatosExistente(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_006.toString());
        }

        this.setValidacionPredios(ESiNo.NO.getCodigo());
        this.setValidacionZonas(ESiNo.SI.getCodigo());
    }

    public void establecerCargues() {

        List<AxMunicipio> axMuns = this.getConservacionService().
                obtenerAxMunicipioPorMunicipioVigencia(this.municipioSeleccionado,
                        this.vigenciaSeleccionada);

        if (axMuns != null && !axMuns.isEmpty()) {
            this.axMunicipioActual = axMuns.get(0);

            this.listenerEstadoValidacionActualizacion();
        } else {
            this.permitirCargue = false;
            this.addMensajeWarn(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_004.toString());
            LOGGER.warn(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_004.getMensajeTecnico());
            return;
        }
        
//        List<Documento> docGDB = new ArrayList<Documento>(); 
//        
//        docGDB = this.getGeneralesService().buscarDocumentoPorTipoActualizacion(
//                                this.municipioSeleccionado,
//                        ETipoDocumento.DOC_BD_GEOGRAFICA_ACTUALIZACION.getId());
//        
//        String codigoMunicipioDocumento = "";
//        
//        if (!docGDB.isEmpty()) {
//            codigoMunicipioDocumento = docGDB.get(0).getNumeroDocumento();
//            codigoMunicipioDocumento = codigoMunicipioDocumento.substring(5, 10);
//        }
//        
//        if (!docGDB.isEmpty()) {
//            if (this.municipioSeleccionado != codigoMunicipioDocumento) {
//
//                //Elimina los registros en la tabla documento almacenado anteriormente 
//                //durante el proceso de actualización
//                for (Documento documento : docGDB) {
//                    this.getGeneralesService().eliminarDocumentoFirmaMecanica(documento);
//                }
//
//                RequestContext context = RequestContext.getCurrentInstance();
//                context.execute("cambioJurisdiccionWV.show();");
//            }
//        }

        //this.permitirCargue = true;

    }

    /**
     * Metodo para cargar archivos relacionados a un nuevo proceso de
     * actualizacion
     *
     * @param eventoCarga
     * @author hector.arias
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        String rutaZip = eventoCarga.getFile().getFileName();
        rutaZip = (this.municipioSeleccionado + ".7z");

        try {
            String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

            this.archivoDatosActualizacion = new File(directorioTemporal, rutaZip);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        if (this.getValidacionPredios().equals(ESiNo.SI.getCodigo())) {
            this.setNombreArchivoCargadoPredial(rutaZip);
        } else if (this.getValidacionZonas().equals(ESiNo.SI.getCodigo())) {
            this.setNombreArchivoCargadoZonal(rutaZip);
        }

        
        
        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoDatosActualizacion);

            byte[] buffer;
            buffer = new byte[(int) eventoCarga.getFile().getSize()];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.rutaArchivoDatosActualizacion = this.archivoDatosActualizacion.getCanonicalPath();

            this.addMensajeInfo(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_001.toString());

            this.banderaArchivoCargado = true;

        } catch (IOException e) {
            this.addMensajeInfo(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_002.toString());
            this.addErrorCallback();
            LOGGER.error(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_002.getMensajeTecnico(), e);

        }
    }

    public void procesarCargue() {
        
        //Elimina los registros en la tabla documento almacenado anteriormente 
        //durante el proceso de actualización
        List<Documento> docGDB = new ArrayList<Documento>(); 
        
        docGDB = this.getGeneralesService().buscarDocumentoPorTipoActualizacion(
                                this.municipioSeleccionado,
                        ETipoDocumento.DOC_BD_GEOGRAFICA_ACTUALIZACION.getId());
        
        if (!docGDB.isEmpty()) {
            for (Documento documento : docGDB) {
                this.getGeneralesService().eliminarDocumentoFirmaMecanica(documento);
            }
        }
        
        
        MunicipioActualizacion nuevoCargue = new MunicipioActualizacion();

        Departamento depto = new Departamento();
        depto.setCodigo(this.departamentoSeleccionado);
        nuevoCargue.setDepartamento(depto);

        Municipio mun = new Municipio();
        mun.setCodigo(this.municipioSeleccionado);
        nuevoCargue.setMunicipio(mun);
        nuevoCargue.setVigencia(Integer.valueOf(this.vigenciaSeleccionada));
        nuevoCargue.setEstado(ECargueCicaEstado.EN_PROCESO.getCodigo());
        nuevoCargue.setActivo(ESiNo.SI.getCodigo());
        nuevoCargue.setFechaLog(new Date());
        nuevoCargue.setUsuarioLog(this.usuario.getLogin());

        nuevoCargue = this.getActualizacionService().
                guardarActualizarMunicipioActualizacion(nuevoCargue);

        guardarArchivoCargue(this.archivoDatosActualizacion.getPath(), nuevoCargue);
        
        this.setCargueSeleccionado(nuevoCargue);
        this.habilitarDescarga = false;

    }

    /**
     * Método para cargar los archivos de la base de datos geográfica
     *
     * @author hector.arias
     */
    public void guardarArchivoCargue(String localPath, MunicipioActualizacion nuevoCargue) {

        try {
            DocumentoTramiteDTO dtDto = new DocumentoTramiteDTO(
                    localPath,
                    ETipoDocumento.DOC_BD_GEOGRAFICA_ACTUALIZACION.getNombre(),
                    new Date(), null, 0);

            dtDto.setNumeroPredial(this.municipioSeleccionado);
            dtDto.setNombreDepartamento(this.departamentoSeleccionado);
            dtDto.setNombreMunicipio(this.municipioSeleccionado);

            Documento d = new Documento();
            TipoDocumento td = new TipoDocumento();
            td.setId(ETipoDocumento.DOC_BD_GEOGRAFICA_ACTUALIZACION.getId());
            d.setTipoDocumento(td);
            d.setNumeroDocumento(nuevoCargue.getId() + "-"
                    + this.municipioSeleccionado);
            d.setArchivo(localPath);
            d.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            d.setBloqueado(ESiNo.NO.getCodigo());
            d.setUsuarioCreador(this.usuario.getLogin());
            d.setUsuarioLog(this.usuario.getLogin());
            d.setFechaLog(new Date());

            d = this.getGeneralesService().actualizarDocumentoYSubirArchivoAlfresco(usuario,
                    dtDto, d);

            this.habilitarValidacion = true;

            this.setEstadoValidacionActualizacion(ECargueCicaEstado.SIN_VALIDAR.getCodigo());

            nuevoCargue.setArchivoGeografico(d.getIdRepositorioDocumentos());

            this.setUbicacionFileGdb(d.getIdRepositorioDocumentos());

            //Actualiza el repositorio de la BD cargada en la tabla ax_municipio_actualizacion
            nuevoCargue = this.getActualizacionService().
                    guardarActualizarMunicipioActualizacion(nuevoCargue);
            
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy/MM/dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d.getFechaLog());
            calendar.add(Calendar.YEAR, -1);
            String fechaMenosUnAnio = formatoFecha.format(calendar.getTime());
            this.setUltimoCargue(fechaMenosUnAnio);

        } catch (Exception e) {
            this.setEstadoArchivoCargadoPredial(ECargueCicaEstado.ERROR.getCodigo());
            this.addMensajeError(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_005.toString());
            this.addErrorCallback();
            LOGGER.error(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_005.getMensajeTecnico(), e);
        }

        if (this.getValidacionPredios().equals(ESiNo.SI.getCodigo())) {
            this.setEstadoArchivoCargadoPredial(ECargueCicaEstado.CARGADA.getCodigo());
        } else if (this.getValidacionZonas().equals(ESiNo.SI.getCodigo())) {
            this.setEstadoArchivoCargadoZonal(ECargueCicaEstado.CARGADA.getCodigo());
        }

    }
    
    /**
     * Muestra el reporte de inconsistencias cuando la validación geográfica falla
     *
     * @author hector.arias
     */
    public void generarReporteInconsistencias() {

        Documento docsReporte = this.getGeneralesService().
                buscarPorNumeroUltimoDocumentoYTipo(
                        this.cargueSeleccionado.getId() + "-" +
                                this.cargueSeleccionado.getMunicipio().getCodigo(),
                        ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getId()
                );
        if (docsReporte != null) {

            ReporteDTO rdto;
            ReportesUtil rUtil = ReportesUtil.getInstance();
            rdto = rUtil.consultarReporteDeGestorDocumental(docsReporte.getIdRepositorioDocumentos());
            this.reporteInconsistencias = rdto.getStreamedContentReporte();
        }

        if (this.reporteInconsistencias == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_002.toString());
            LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_002.getMensajeTecnico());
        }
    }
    
    /**
     * 
     * @param resultado 
     * @author hector.arias
     */
    public void guardarRegistroArchivoInconsistencias(String resultado) {
        
//        DocumentoTramiteDTO dtDto = new DocumentoTramiteDTO(
//                resultado,
//                ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getNombre(),
//                new Date(), null, 0);
//
//        dtDto.setNumeroPredial(this.municipioSeleccionado);
//        dtDto.setNombreDepartamento(this.departamentoSeleccionado);
//        dtDto.setNombreMunicipio(this.municipioSeleccionado);
//
//        Documento d = new Documento();
//        TipoDocumento td = new TipoDocumento();
//        td.setId(ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION_GEOGRAFICA.getId());
//        d.setTipoDocumento(td);
//        d.setNumeroDocumento(cargueSeleccionado.getId() + "-"
//                + this.municipioSeleccionado);
//        d.setArchivo("resultado_validacion.log");
//        d.setUsuarioCreador(this.usuario.getLogin());
//        d.setUsuarioLog(this.usuario.getLogin());
//        d.setIdRepositorioDocumentos(this.getUbicacionFileGdb().toString() 
//                + "resultado_validacion.log");
//        d.setFechaLog(new Date());
//
//        d = this.getGeneralesService().actualizarDocumentoYSubirArchivoAlfresco(usuario,
//                dtDto, d);
//        
//        this.getActualizacionService().
//                guardarRegistroArchivoInconsistenciasActualizacionGeo(resultado);
    }
    
    /**
     * Método que actualiza el estado del proceso de validación geográfica
     * @author hector.arias
     * 
     */
    public void actualizarEstadoProcesoFinalizado(){
        
        this.cargueSeleccionado.setEstado(ECargueCicaEstado.VALIDADO.getCodigo());
        this.cargueSeleccionado = this.getActualizacionService().
                guardarActualizarMunicipioActualizacion(cargueSeleccionado);
    }

    /**
     * Método que lanza las validaciones geográficas
     *
     * @author hector.arias
     */
    public void validacionGeografica() {

        this.addMensajeInfo(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_003.toString());
        LOGGER.info(ECodigoErrorVista.CARGUE_GEOGRAFICO_ACTUALIZACION_003.getMensajeTecnico());
        
        List<MunicipioActualizacion> munActGeo = new ArrayList<MunicipioActualizacion>(); 
        
        munActGeo = this.getActualizacionService().
                obtenerPorMunicipioYEstadoGeografico(this.municipioSeleccionado, 
                null, true, Integer.valueOf(this.vigenciaSeleccionada));
        
        MunicipioActualizacion municipioActual = new MunicipioActualizacion();
        
        municipioActual = munActGeo.get(0);
        
        this.setEstadoValidacionActualizacion(municipioActual.getEstado());
        
        this.getGeneralesService().
                validarInconsistenciasActualizacion(this.departamentoSeleccionado, this.municipioSeleccionado, this.getUbicacionFileGdb(), this.validacionZonas, this.validacionPredios, this.usuario);

        this.permitirCargue = false;
    }
    
    public void consultarValidacionEncurso () {
        ProductoCatastralJob job = new ProductoCatastralJob();

        job = this.getGeneralesService().obtenerJobsCargueActualizacion(ProductoCatastralJob.SIG_JOB_CARGUE_ACTUALIZACION, this.usuario.getLogin());
        
        if (job != null) {
            if (job.getEstado().equals(ProductoCatastralJob.AGS_EN_EJECUCION.toString()) || job.getEstado().equals(ProductoCatastralJob.AGS_TERMINADO.toString())
                    || job.getEstado().equals(ProductoCatastralJob.SNC_EN_EJECUCION.toString())) {
                setReiniciarValidacion(true);
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("reinicarValidacionWV.show();");
            } else {
                setReiniciarValidacion(false);
                this.validacionGeografica();
            }
        } else {
            setReiniciarValidacion(false);
            this.validacionGeografica();
        }
    }
    
    public void reinicarValidacionFalse(){
        LOGGER.info("Cierre de la ventana de confirmación del reinicio de validaciones.");
    }    
        
    /**
     * Método encargado de terminar la sesión
     *
     * @author hector.arias
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("cargueInformacionGeograficaActualizacion");
        this.tareasPendientesMB.init();
        return "index";
    }

}
