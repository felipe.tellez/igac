package co.gov.igac.snc.web.listener;

import java.io.Serializable;

import co.gov.igac.snc.comun.anotaciones.Implement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.task.TaskExecutor;

/**
 * Implementación de la interfaz donde se cargan e inicializan Parámetros de la aplicación web
 *
 * @author juan.mendez
 * @version 2.0
 */
public class ContextListener implements ApplicationContextAware, IContextListener, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Log LOGGER = LogFactory.getLog(ContextListener.class);

    private ApplicationContext applicationContext;

    private TaskExecutor taskExecutor;

    private String clientAppNameUrl;

    // //////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////
    /**
     *
     *
     */
    @Override
    public void setApplicationContext(ApplicationContext appContext) throws BeansException {
        this.applicationContext = appContext;
        try {
            initContext();
        } catch (Exception e) {
            LOGGER.fatal(e.getMessage(), e);
        }
    }

    @Override
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     *
     * @throws Exception
     */
    private void initContext() throws Exception {
    }

    public TaskExecutor getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     * @see IContextListener#getClientAppNameUrl()
     */
    @Implement
    @Override
    public String getClientAppNameUrl() {
        return this.clientAppNameUrl;
    }

    /**
     * @author pedro.garcia
     * @see IContextListener#setClientAppNameUrl(String)
     */
    @Implement
    @Override
    public void setClientAppNameUrl(String appName) {
        this.clientAppNameUrl = appName;
    }

}
