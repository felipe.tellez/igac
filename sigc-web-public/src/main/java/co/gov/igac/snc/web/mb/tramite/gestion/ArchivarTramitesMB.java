package co.gov.igac.snc.web.mb.tramite.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.math.BigDecimal;
import org.primefaces.model.SortOrder;

/**
 * Clase que hace de MB para manejar la consulta y archivado de trámites
 *
 * @author juan.agudelo
 *
 */
@Component("archivarTramite")
@Scope("session")
public class ArchivarTramitesMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ArchivarTramitesMB.class);

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites par archivar
     */
    private LazyDataModel<Tramite> lazyAssignedTArchivar;

    /**
     * lista de trámites para archivar paginados
     */
    private List<Tramite> rows;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Bandera de visualización del boton desbloquear
     */
    private boolean banderaTablaBusqueda;

    /**
     * Conteo de trámites actuales para bloquear
     */
    private int count;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;

    /**
     * Orden apra la lista de Municipios
     */
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * Codigo del departamento
     */
    private String departamentoCodigo;

    /**
     * Codigo del municipio
     */
    private String municipioCodigo;

    /**
     * Lista de departamentos
     */
    private List<Departamento> dptos;

    /**
     * Lista de municipios
     */
    private List<Municipio> municipios;

    /**
     * Trámite para archivar
     */
    private Tramite tramiteParaArchivar;

    /**
     * Departamento seleccionado
     */
    private Departamento departamentoSeleccionado;

    /**
     * Municipio seleccionado
     */
    private Municipio municipioSeleccionado;

    // ------------- methods ------------------------------------
    public LazyDataModel<Tramite> getLazyAssignedTArchivar() {
        return lazyAssignedTArchivar;
    }

    public Tramite getTramiteParaArchivar() {
        return tramiteParaArchivar;
    }

    public void setTramiteParaArchivar(Tramite tramiteParaArchivar) {
        this.tramiteParaArchivar = tramiteParaArchivar;
    }

    public void setLazyAssignedTArchivar(
        LazyDataModel<Tramite> lazyAssignedTArchivar) {
        this.lazyAssignedTArchivar = lazyAssignedTArchivar;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isBanderaTablaBusqueda() {
        return banderaTablaBusqueda;
    }

    public void setBanderaTablaBusqueda(boolean banderaTablaBusqueda) {
        this.banderaTablaBusqueda = banderaTablaBusqueda;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public EOrden getOrdenDepartamentos() {
        return ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public List<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(List<Departamento> dptos) {
        this.dptos = dptos;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("BloqueoPredioMB#init");

        this.usuario = new UsuarioDTO();
        this.banderaTablaBusqueda = false;
        this.tramiteParaArchivar = null;

        // TODO debe ser el usuario del sistema
        this.usuario.setLogin("juan.agudelo");

    }

    // --------------------------------------------------------------------------------------------------
    public void poblar() {

        /*
         * count = this.remoteTramite.contarTramitesParaArchivar( this.departamentoSeleccionado,
         * this.municipioSeleccionado);
         */
        // TODO revisar la pertinencia de este metodo ya que los trámites ahora
        // se archivan directamente por el process
        count = 0;

        if (count > 0) {
            this.lazyAssignedTArchivar.setRowCount(count);
            this.banderaTablaBusqueda = true;
        } else {
            this.lazyAssignedTArchivar.setRowCount(0);
        }

    }

    // --------------------------------------------------------------------------------------------------
    private void populateTAsignarLazyly(List<Tramite> answer, int first,
        int pageSize) {

        rows = new ArrayList<Tramite>();

        int size;
        // TODO revisar la pertinencia de este metodo ya que los trámites ahora
        // se archivan directamente por el process
        /* rows = this.remoteTramite.buscarTramitesParaArchivar( this.departamentoSeleccionado, this.municipioSeleccionado); */
        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton Buscar
     *
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public void buscar() {

        this.departamentoSeleccionado = new Departamento();
        this.municipioSeleccionado = new Municipio();

        this.departamentoSeleccionado.setCodigo(this.departamentoCodigo);
        this.municipioSeleccionado.setCodigo(this.municipioCodigo);

        this.lazyAssignedTArchivar = new LazyDataModel<Tramite>() {

            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();
                populateTAsignarLazyly(answer, first, pageSize);

                return answer;
            }
        };
        poblar();
        this.banderaTablaBusqueda = true;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para archivar los tramites seleccionados
     */
    public void archivar() {
        LOGGER.debug("Tramite A: " + this.tramiteParaArchivar);
        if (!this.tramiteParaArchivar.equals(null)) {

            try {
                boolean resultado = this.getTramiteService().archivarTramite(usuario,
                    this.tramiteParaArchivar);
                this.rows.remove(this.tramiteParaArchivar);
                if (resultado == true) {
                    String mensaje = "El trámite se archivo exitosamente!";
                    this.addMensajeInfo(mensaje);
                } else {
                    String mensaje = "El trámite no pudo ser archivado";
                    this.addMensajeError(mensaje);
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                String mensaje = "El trámite no pudo ser archivado";
                this.addMensajeError(mensaje);
            }
        } else {
            String mensaje = "Ocurrio un error desconocido con el trámite seleccionado";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Contiene la lista de departamentos disponibles para Colombia
     */
    public List<SelectItem> getDepartamentos() {
        List<SelectItem> departamentos = new ArrayList<SelectItem>();
        departamentos.add(new SelectItem(null, this.DEFAULT_COMBOS));

        this.dptos = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);

        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            Collections.sort(dptos);
        } else {
            Collections.sort(dptos, Departamento.getComparatorNombre());
        }
        for (Departamento d : dptos) {
            if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
                departamentos.add(new SelectItem(d.getCodigo(), d.getCodigo() +
                     "-" + d.getNombre()));
            } else {
                departamentos.add(new SelectItem(d.getCodigo(), d.getNombre()));
            }
        }

        return departamentos;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Contiene la lista de municipios asociados a un deparatamento
     */
    public List<SelectItem> getMunicipios() {
        List<SelectItem> municipiosC = new ArrayList<SelectItem>();
        municipiosC.add(new SelectItem(null, super.DEFAULT_COMBOS));
        if (departamentoCodigo != null) {
            this.municipios = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(departamentoCodigo);
            if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                Collections.sort(municipios);
            } else {
                Collections.sort(municipios, Municipio.getComparatorNombre());
            }
            for (Municipio m : municipios) {
                if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                    municipiosC.add(new SelectItem(m.getCodigo(), m
                        .getCodigo3Digitos() + "-" + m.getNombre()));
                } else {
                    municipiosC
                        .add(new SelectItem(m.getCodigo(), m.getNombre()));
                }
            }
        }
        return municipiosC;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Ordena la lsita de Departamentos
     */
    public void cambiarOrdenDepartamentos() {
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Ordena la lista de municipios
     */
    public void cambiarOrdenMunicipios() {
        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
        }
    }

    /**
     * Cierra la radicación de la solicitud del trámite archivado.
     *
     * @param tramiteArchivado
     */
    private void cerrarRadicacionTramiteArchivado(final Tramite tramiteArchivado) {
        final String numeroRadicacion = obtenerNumeroRadicado(tramiteArchivado.getSolicitud());
        if (numeroRadicacion == null) {
            LOGGER.error("Error al realizar cierre de radicación: Número de radicado es nulo");
        } else {
            cerrarRadicacionTramite(tramiteArchivado.getSolicitud().getNumero(), numeroRadicacion,
                getUsuario().getLogin());
        }
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     * @modifiedBy wilmanjose.vega
     */
    private String obtenerNumeroRadicado(Solicitud solicitudTemp) {

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.
            getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Trámite Archivado"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(solicitudTemp.getSolicitanteSolicituds().get(0).getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email
        return this.getGeneralesService().obtenerNumeroRadicado(parametros);
    }

    //18779.  Cerrar trámite en cordis 15/09/2016 wilmanjose.vega
    //TODO: Deberia crearse un servicio de radicación.
    private boolean cerrarRadicacionTramite(final String radicado,
        final String radicadoResponde, final String usuario) {
        boolean resultado = true;
        final Object[] resultadoCierre = this.getTramiteService()
            .finalizarRadicacion(radicado, radicadoResponde, usuario);
        if (resultadoCierre != null) {
            for (final Object objeto : resultadoCierre) {
                final List lista = (List) objeto;
                if (lista != null && !lista.isEmpty()) {
                    int numeroErrores = 0;
                    for (final Object obj : lista) {
                        final Object[] arrObjetos = (Object[]) obj;
                        //TODO: Eliminar la validación del código de error de oracle luego de que 
                        //el servicio de CORDIS solucione el error: http://redmine.igac.gov.co/issues/21662
                        if (!arrObjetos[0].equals("0") &&
                            !arrObjetos[1].toString().contains("0-ORA-0000")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + arrObjetos[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al cerrar radicación");
                        resultado = false;
                    }
                }
            }
        }
        return resultado;
    }
}
