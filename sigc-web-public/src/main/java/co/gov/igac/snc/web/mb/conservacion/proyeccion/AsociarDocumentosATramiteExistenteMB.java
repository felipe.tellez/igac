package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.conservacion.asignacion.VistaDetallesTramiteMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.util.Iterator;

/**
 * Managed Bean para Asociar documentos a tramites existentes. Esto sera posible de realizar durane
 * todo el proceso de proyección sin importar los trámites
 *
 * @author franz.gamba
 *
 */
@Component("asociacionDocsTramExist")
@Scope("session")
public class AsociarDocumentosATramiteExistenteMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 2026334722171638595L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AsociarDocumentosATramiteExistenteMB.class);

    @Autowired
    private ProyectarConservacionMB proyectarConservacion;

    @Autowired
    private VistaDetallesTramiteMB detallesTramite;

//-------------------------------------------------------------------------------------------------------
    /*
     * Variables
     */
    private Tramite tramiteAD;

    private List<TramiteDetallePredio> tramitePredioEng;

    private List<PPredio> prediosDesenglobe;

    private List<Predio> prediosT;

    private boolean englobeODesenglobe;

    /**
     * documentación adicional del trámite
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * Documento temporal de trámite
     */
    private TramiteDocumentacion documentoSeleccionado;

    /**
     * Documento temporal de trámite
     */
    private TramiteDocumentacion documentoSeleccionadoAdicional;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    private StreamedContent fileTempDownload;

    /**
     * bandera que indica si un documento (requerido/adicional) se encuentra en edición o es de
     * lectura
     */
    private boolean editandoDocumentacion;

    /**
     * Bandera de solicitud de documentos adicionales
     */
    private boolean banderaDocumentoAdicional;

    private Departamento departamentoDocumento;

    private Municipio municipioDocumento;

    /**
     * Datos para el documento
     */
    private List<SelectItem> municipiosDocumento;
    private List<SelectItem> departamentosDocumento;
    private List<Departamento> dptosDocumento;
    private List<Municipio> munisDocumento;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * Ruta del documento cargado
     */
    private String rutaMostrar;

    /**
     * Archivo a cargar en la carpeta temporal
     */
    private File archivoResultado;

    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * nombre temporal del archivo cargado
     */
    private String nombreTemporalArchivo;

    /**
     * Bandera de visualización de datos para docuemntos encontrados en correspondencia
     */
    private boolean banderaDocumentoEncontradoEnCorrrespondencia;

    /**
     * Bandera que se activa si se cargo un documento de soporte para la prueba seleccionada
     */
    private boolean banderaDocumentoCargado;

    /**
     * Bandera que activa la visualización del boton guardar si el documentto fue cargado y si se
     * encontro un documento valido en radicacion
     */
    private boolean banderaBotonGuardarDocPrueba;

    /**
     * Arreglo que contiene los trámites seleccionados para adicionar documentación
     */
    private Tramite[] tramitesTramiteSeleccionados;

    /**
     * Arreglo que contiene la documentación aportada para asociarla a la lista de trámites
     * asociados
     */
    private TramiteDocumentacion[] documentacionAportada;

    /**
     * Lista de trámites asociados al trámite
     */
    private List<Tramite> tramiteTramites;

    /**
     * Bandera de visualización de los documentos para asociar al trámite
     */
    private boolean banderaAsociarDocumentos;

    /**
     * Bandera para el guardado de los docuemntos en Base de datos y alfresco cuando se adjunta un
     * documento digital a un documento ya asociado
     */
    private boolean banderaAdjuntoDocumentoDigital;

    /**
     * Bandera utilizada para el botòn eliminar documento
     */
    private boolean documentoSoporteEliminado;

    // -------------------------------------------------------------------------------------------------------
    /*
     * Metodos
     */
    public ProyectarConservacionMB getProyectarConservacion() {
        return this.proyectarConservacion;
    }

    public void setProyectarConservacion(
        ProyectarConservacionMB proyectarConservacion) {
        this.proyectarConservacion = proyectarConservacion;
    }

    public Tramite getTramiteAD() {
        return this.tramiteAD;
    }

    public void setTramiteAD(Tramite tramiteAD) {
        this.tramiteAD = tramiteAD;
    }

    public List<Predio> getPrediosT() {
        return this.prediosT;
    }

    public void setPrediosT(List<Predio> prediosT) {
        this.prediosT = prediosT;
    }

    public List<TramiteDetallePredio> getTramitePredioEng() {
        return this.tramitePredioEng;
    }

    public void setTramitePredioEng(List<TramiteDetallePredio> tramitePredioEng) {
        this.tramitePredioEng = tramitePredioEng;
    }

    public boolean isEnglobeODesenglobe() {
        return this.englobeODesenglobe;
    }

    public void setEnglobeODesenglobe(boolean isEnglobeODesenglobe) {
        this.englobeODesenglobe = isEnglobeODesenglobe;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return this.documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public TramiteDocumentacion getDocumentoSeleccionado() {
        return this.documentoSeleccionado;
    }

    public void setDocumentoSeleccionado(TramiteDocumentacion documentoSeleccionado) {
        this.documentoSeleccionado = documentoSeleccionado;
    }

    public TramiteDocumentacion getDocumentoSeleccionadoAdicional() {
        return this.documentoSeleccionadoAdicional;
    }

    public void setDocumentoSeleccionadoAdicional(
        TramiteDocumentacion documentoSeleccionadoAdicional) {
        this.documentoSeleccionadoAdicional = documentoSeleccionadoAdicional;
    }

    public boolean isEditandoDocumentacion() {
        return this.editandoDocumentacion;
    }

    public void setEditandoDocumentacion(boolean editandoDocumentacion) {
        this.editandoDocumentacion = editandoDocumentacion;
    }

    public boolean isBanderaDocumentoAdicional() {
        return this.banderaDocumentoAdicional;
    }

    public void setBanderaDocumentoAdicional(boolean banderaDocumentoAdicional) {
        this.banderaDocumentoAdicional = banderaDocumentoAdicional;
    }

    public Departamento getDepartamentoDocumento() {
        return this.departamentoDocumento;
    }

    public void setDepartamentoDocumento(Departamento departamentoDocumento) {
        this.departamentoDocumento = departamentoDocumento;
    }

    public Municipio getMunicipioDocumento() {
        return this.municipioDocumento;
    }

    public void setMunicipioDocumento(Municipio municipioDocumento) {
        this.municipioDocumento = municipioDocumento;
    }

    public List<SelectItem> getMunicipiosDocumento() {
        return this.municipiosDocumento;
    }

    public void setMunicipiosDocumento(List<SelectItem> municipiosDocumento) {
        this.municipiosDocumento = municipiosDocumento;
    }

    public List<SelectItem> getDepartamentosDocumento() {
        return this.departamentosDocumento;
    }

    public void setDepartamentosDocumento(
        List<SelectItem> departamentosDocumento) {
        this.departamentosDocumento = departamentosDocumento;
    }

    public List<Departamento> getDptosDocumento() {
        return this.dptosDocumento;
    }

    public void setDptosDocumento(List<Departamento> dptosDocumento) {
        this.dptosDocumento = dptosDocumento;
    }

    public List<Municipio> getMunisDocumento() {
        return this.munisDocumento;
    }

    public void setMunisDocumento(List<Municipio> munisDocumento) {
        this.munisDocumento = munisDocumento;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return this.ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return this.ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public String getRutaMostrar() {
        return this.rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public File getArchivoResultado() {
        return this.archivoResultado;
    }

    public void setArchivoResultado(File archivoResultado) {
        this.archivoResultado = archivoResultado;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getNombreTemporalArchivo() {
        return this.nombreTemporalArchivo;
    }

    public void setNombreTemporalArchivo(String nombreTemporalArchivo) {
        this.nombreTemporalArchivo = nombreTemporalArchivo;
    }

    public boolean isBanderaDocumentoEncontradoEnCorrrespondencia() {
        return this.banderaDocumentoEncontradoEnCorrrespondencia;
    }

    public void setBanderaDocumentoEncontradoEnCorrrespondencia(
        boolean banderaDocumentoEncontradoEnCorrrespondencia) {
        this.banderaDocumentoEncontradoEnCorrrespondencia =
            banderaDocumentoEncontradoEnCorrrespondencia;
    }

    public boolean isBanderaDocumentoCargado() {
        return this.banderaDocumentoCargado;
    }

    public void setBanderaDocumentoCargado(boolean banderaDocumentoCargado) {
        this.banderaDocumentoCargado = banderaDocumentoCargado;
    }

    public boolean isBanderaBotonGuardarDocPrueba() {
        return this.banderaBotonGuardarDocPrueba;
    }

    public void setBanderaBotonGuardarDocPrueba(
        boolean banderaBotonGuardarDocPrueba) {
        this.banderaBotonGuardarDocPrueba = banderaBotonGuardarDocPrueba;
    }

    public Tramite[] getTramitesTramiteSeleccionados() {
        return this.tramitesTramiteSeleccionados;
    }

    public void setTramitesTramiteSeleccionados(
        Tramite[] tramitesTramiteSeleccionados) {
        this.tramitesTramiteSeleccionados = tramitesTramiteSeleccionados;
    }

    public TramiteDocumentacion[] getDocumentacionAportada() {
        return this.documentacionAportada;
    }

    public void setDocumentacionAportada(
        TramiteDocumentacion[] documentacionAportada) {
        this.documentacionAportada = documentacionAportada;
    }

    public List<Tramite> getTramiteTramites() {
        return this.tramiteTramites;
    }

    public void setTramiteTramites(List<Tramite> tramiteTramites) {
        this.tramiteTramites = tramiteTramites;
    }

    public boolean isBanderaAsociarDocumentos() {
        return this.banderaAsociarDocumentos;
    }

    public void setBanderaAsociarDocumentos(boolean banderaAsociarDocumentos) {
        this.banderaAsociarDocumentos = banderaAsociarDocumentos;
    }

    public String getTipoMimeDocTemporal() {
        return this.tipoMimeDocTemporal;
    }

    public StreamedContent getFileTempDownload() {
        controladorDescargaArchivos();
        return this.fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public List<PPredio> getPrediosDesenglobe() {
        return prediosDesenglobe;
    }

    public void setPrediosDesenglobe(List<PPredio> prediosDesenglobe) {
        this.prediosDesenglobe = prediosDesenglobe;
    }

    public VistaDetallesTramiteMB getDetallesTramite() {
        return detallesTramite;
    }

    public void setDetallesTramite(VistaDetallesTramiteMB detallesTramite) {
        this.detallesTramite = detallesTramite;
    }

    public boolean isBanderaAdjuntoDocumentoDigital() {
        return banderaAdjuntoDocumentoDigital;
    }

    public void setBanderaAdjuntoDocumentoDigital(
        boolean banderaAdjuntoDocumentoDigital) {
        this.banderaAdjuntoDocumentoDigital = banderaAdjuntoDocumentoDigital;
    }

    public boolean isDocumentoSoporteEliminado() {
        return documentoSoporteEliminado;
    }

    public void setDocumentoSoporteEliminado(boolean documentoSoporteEliminado) {
        this.documentoSoporteEliminado = documentoSoporteEliminado;
    }

//-------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        this.documentoSoporteEliminado = false;
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.tramiteAD = this.proyectarConservacion.getTramite();
        this.documentacionAdicional = this.tramiteAD.getDocumentacionAdicional();

        if (this.tramiteAD != null) {
            this.tramiteAD.setTramiteDocumentos(this.getTramiteService()
                .obtenerTramiteDocumentosPorTramiteId(
                    this.tramiteAD.getId()));
            this.tramiteAD.setTramiteDocumentacions(this.getTramiteService()
                .obtenerTramiteDocumentacionPorIdTramite(
                    this.tramiteAD.getId()));

            if (this.tramiteAD.getPredios() != null) {
                this.prediosT = this.tramiteAD.getPredios();
            }
            // this.tramiteAD.getdoc
            this.englobeODesenglobe = false;
            if (tramiteAD.getSubtipo() != null) {
                if (tramiteAD.getSubtipo().equals(
                    EMutacion2Subtipo.ENGLOBE.getCodigo()) ||
                     tramiteAD.getSubtipo().equals(
                        EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                    this.englobeODesenglobe = true;
                }
                if (tramiteAD.getSubtipo().equals(
                    EMutacion2Subtipo.ENGLOBE.getCodigo())) {
                    this.tramitePredioEng = this.getTramiteService()
                        .buscarPredioEnglobesPorTramite(this.tramiteAD.getId());
                } else if (tramiteAD.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo())) {
                    this.prediosDesenglobe = new ArrayList<PPredio>();
                    if (this.proyectarConservacion.getPredioSeleccionado() != null) {
                        this.prediosDesenglobe.add(this.proyectarConservacion.
                            getPredioSeleccionado());
                    }
                }
            }
        }

        /*
         * else{ this.englobeODesenglobe = false; }
         */
        this.banderaAsociarDocumentos = true;
        this.tramiteTramites = new ArrayList<Tramite>();
        this.editandoDocumentacion = true;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Nuevo" en la ventana que permite adicionar documentos al trámite y que
     * muestra la documentación del trámite
     */
    /*
     * @modified by juanfelipe.garcia cambio de variable a documentoSeleccionadoAdicional
     */
    public void adicionarDocumentoAdicional() {

        if (this.documentacionAdicional == null ||
             this.documentacionAdicional.isEmpty()) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }
        this.documentoSeleccionadoAdicional = new TramiteDocumentacion();

        //N: hay que crearlo aquí para que pueda usarse en la página donde se carga el nuevo documento
        TipoDocumento td = new TipoDocumento();
        this.documentoSeleccionadoAdicional.setTipoDocumento(td);
        this.documentoSeleccionadoAdicional.setAporta(true);

        if (this.tramiteAD != null && this.tramiteAD.getDepartamento() != null &&
             this.tramiteAD.getDepartamento().getCodigo() != null &&
             !this.tramiteAD.getDepartamento().getCodigo().isEmpty()) {
            this.departamentoDocumento = this.tramiteAD.getDepartamento();
        } else {
            this.departamentoDocumento = null;
        }
        if (this.tramiteAD != null && this.tramiteAD.getMunicipio() != null &&
             this.tramiteAD.getMunicipio().getCodigo() != null &&
             !this.tramiteAD.getMunicipio().getCodigo().isEmpty()) {
            this.municipioDocumento = this.tramiteAD.getMunicipio();
        } else {
            this.municipioDocumento = null;
        }

        this.updateDepartamentosDocumento();
        this.updateMunicipiosDocumento();
        this.editandoDocumentacion = false;
        this.banderaDocumentoAdicional = true;
        this.banderaAsociarDocumentos = true;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al seleccionar documentos adicionales para eliminar
     *
     * @author juan.agudelo
     *
     *
     * @modified by juanfelipe.garcia cambio de variable a documentoSeleccionadoAdicional
     * @modified by felipe.cadena::25-03-2015::#11867::Se eliminan los documentos directamente en la
     * base de datos
     */
    public void eliminarDocumentacionAdicional() {

//		List<TramiteDocumentacion> documentacionAd = new LinkedList<TramiteDocumentacion>();
//		for(TramiteDocumentacion td : this.documentacionAdicional){
//			if(td != this.documentoSeleccionadoAdicional){
//				documentacionAd.add(td);
//			}
//		}
        //felipe.cadena::Se elimina el documento del tramite y de la base de datos.
        this.tramiteAD.getTramiteDocumentacions().remove(this.documentoSeleccionadoAdicional);
        this.documentacionAdicional.remove(this.documentoSeleccionadoAdicional);

        this.getTramiteService().eliminarTramiteDocumentacion(this.documentoSeleccionadoAdicional);

    }
//--------------------------------------------------------------------------

    public void updateDepartamentosDocumento() {
        departamentosDocumento = new LinkedList<SelectItem>();
        departamentosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.dptosDocumento = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosDocumento);
        } else {
            Collections.sort(this.dptosDocumento,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : this.dptosDocumento) {
            if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Actualiza los municipios del documento
     *
     * @modified juanfelipe.garcia 19-07-2013 adicion de condicion para contemplar
     * documentoSeleccionadoAdicional
     *
     */
    public void updateMunicipiosDocumento() {
        municipiosDocumento = new ArrayList<SelectItem>();
        munisDocumento = new ArrayList<Municipio>();
        municipiosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));
        if ((this.documentoSeleccionado != null || this.documentoSeleccionadoAdicional != null) &&
             this.departamentoDocumento != null &&
             this.departamentoDocumento.getCodigo() != null) {
            munisDocumento = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(this.departamentoDocumento
                    .getCodigo());
            if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                Collections.sort(munisDocumento);
            } else {
                Collections.sort(munisDocumento,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisDocumento) {
                if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------
    public void setSelectedDepartamentoDocumento(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosDocumento != null) {
            for (Departamento departamento : dptosDocumento) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.documentoSeleccionado != null) {
            this.departamentoDocumento = dptoSeleccionado;
        }
    }

    // --------------------------------------------------------------------------
    public String getSelectedDepartamentoDocumento() {
        if (this.tramiteAD != null && this.tramiteAD.getDepartamento() != null) {
            return this.tramiteAD.getDepartamento().getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosNombreDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenMunicipiosNombreDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosCodigoDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por CODIGO
     *
     * @return
     */
    public boolean isOrdenMunicipiosCodigoDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    public void setSelectedMunicipioDocumento(String codigo) {
        @SuppressWarnings("unused")
        Municipio municipioSelected = null;
        if (codigo != null && this.munisDocumento != null) {
            for (Municipio municipio : this.munisDocumento) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioDocumento = null;
        }
    }

//--------------------------------------------------------------------------
    public String getSelectedMunicipioDocumento() {
        if (this.tramiteAD != null && this.tramiteAD.getMunicipio() != null) {
            return this.tramiteAD.getMunicipio().getCodigo();
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo utilizado para guardar documentos adicionales y ejecutado en la ventana modal de
     * nuevoDocumentoTramite como accion sobre el boton "Aceptar"
     *
     * @modified by juanfelipe.garcia se quitaron validaciones sobrantes
     */
    public void guardarDocumentacion() {

        try {
            //if(this.banderaDocumentoCargado){
            String fileSeparator = System.getProperty("file.separator");
            if (this.banderaAdjuntoDocumentoDigital) {
                List<TramiteDocumentacion> documentacion = new ArrayList<TramiteDocumentacion>();
                Documento doc = this.documentoSeleccionado.getDocumentoSoporte();
                doc = doc == null ? new Documento() : doc;

                Date currentDate = new Date(System.currentTimeMillis());

                doc.setUsuarioLog(this.usuario.getLogin());
                doc.setFechaLog(currentDate);
                doc.setTramiteId(documentoSeleccionado.getTramite().getId());
                doc.setPredioId(this.proyectarConservacion.getPredioSeleccionado().getId());
                doc.setTipoDocumento(documentoSeleccionado.getTipoDocumento());
                doc.setBloqueado(ESiNo.NO.getCodigo());
                doc.setUsuarioCreador(this.usuario.getLogin());

                if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
                    doc.setArchivo(this.rutaMostrar);
                    doc.setDescripcion(documentoSeleccionado.getDetalle());
                    doc.setCantidadFolios(documentoSeleccionado.getCantidadFolios());
                    doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                    doc.setNombreDeArchivoCargado(this.nombreTemporalArchivo);
                    doc = this.getTramiteService().guardarYactualizarDocumento(doc);
                    this.documentoSeleccionado.setDocumentoSoporte(doc);
                    this.documentoSeleccionado = this.getTramiteService().
                        guardarTramiteDocumentacion(this.documentoSeleccionado);

                    DocumentoTramiteDTO dt = new DocumentoTramiteDTO();
                    dt.setFechaCreacionTramite(currentDate);
                    dt.setNombreDepartamento(this.tramiteAD.getDepartamento().getNombre());
                    dt.setNombreMunicipio(this.tramiteAD.getMunicipio().getNombre());
                    dt.setNumeroPredial(this.proyectarConservacion.getPredioSeleccionado().
                        getNumeroPredial());
                    dt.setRutaArchivo(FileUtils.getTempDirectoryPath() + fileSeparator + doc.
                        getArchivo());
                    this.getGeneralesService().actualizarDocumentoYAlfresco(this.usuario, dt, doc);
                } else if (this.documentoSoporteEliminado) {
                    doc.setArchivo(" ");
                    doc.setDescripcion(documentoSeleccionado.getDetalle());
                    doc.setCantidadFolios(documentoSeleccionado.getCantidadFolios());
                    doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                    this.getTramiteService().actualizarDocumento(doc);
                    this.getTramiteService().actualizarTramiteDocumentacionSimple(
                        documentoSeleccionado);
                } else {
                    //doc.setArchivo(" ");
                    doc.setDescripcion(documentoSeleccionado.getDetalle());
                    doc.setCantidadFolios(documentoSeleccionado.getCantidadFolios());
                    doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                    this.getTramiteService().actualizarDocumento(doc);
                    this.getTramiteService().actualizarTramiteDocumentacionSimple(
                        documentoSeleccionado);
                }

                this.banderaAdjuntoDocumentoDigital = false;

                this.tramiteAD = this.getTramiteService().consultarDocumentacionDeTramite(
                    this.tramiteAD.getId());
            } else {
                asociarDocumentoATramiteDocumentacion();
            }

            this.banderaDocumentoAdicional = false;
            this.editandoDocumentacion = false;

            this.archivoResultado = new File("");
            this.rutaMostrar = null;
            /* }else{ this.addMensajeError("Ocurrio un error al cargar el documento o" + " no fue
             * cargado un documento. Revise la información e intente de nuevo."); RequestContext
             * context = RequestContext.getCurrentInstance(); context.addCallbackParam("error",
             * "error");
			} */
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error("error guardando documentación: " + e.getMessage(), e);
            this.addMensajeError(
                "Ocurrió un error guardando el documento, por favor intente más tarde.");
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Asocia un Documento al nuevo TramiteDocumentacion que se va a adicionar al trámite
     */
    /*
     * @modified pedro.garcia 01-06-2012 c + ambio de nivel de acceso de método a private + Arreglos
     * generales para dejarlo decente + cambio de nombre @modified juanfelipe.garcia 19-07-2013
     * ahora se usa documentoSeleccionadoAdicional para este metodo
     *
     */
    private void asociarDocumentoATramiteDocumentacion() {

        LOGGER.debug(
            "on AsociarDocumentosATramiteExistenteMB#asociarDocumentoATramiteDocumentacion ...");

        String fileSeparator = System.getProperty("file.separator");
        TramiteDocumentacion tdTemp = this.documentoSeleccionadoAdicional;
        TipoDocumento tipoDocumentoForReplace;
        Long idTipoDoc;

        // Agregado para que asocie el documento al tramite -- ChRod
        tdTemp.setTramite(this.tramiteAD);

        if (this.documentacionAdicional == null) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }

        if (this.departamentoDocumento != null) {
            tdTemp.setDepartamento(this.departamentoDocumento);
        }
        if (this.municipioDocumento != null) {
            tdTemp.setMunicipio(this.municipioDocumento);
        }

        //N: siempre debería entrar aquí porque el TipoDocumento del Documento se asoció en el método
        //   adicionarDocumentoAdicional
        if (this.documentoSeleccionadoAdicional != null &&
             this.documentoSeleccionadoAdicional.getTipoDocumento() != null &&
             this.documentoSeleccionadoAdicional.getTipoDocumento().getId() != null) {

            //D: del TipoDocumento asociado al nuevo Documento solo se sabía el id de tipo, luego
            //  hay que traer el registro completo y asociarlo
            idTipoDoc = this.documentoSeleccionadoAdicional.getTipoDocumento().getId();
            tipoDocumentoForReplace = this.getGeneralesService().buscarTipoDocumentoPorId(idTipoDoc);
            this.documentoSeleccionadoAdicional.setTipoDocumento(tipoDocumentoForReplace);

            tdTemp.setTipoDocumento(this.documentoSeleccionadoAdicional.getTipoDocumento());
        }
        //        else if (this.documentoSeleccionado != null
        //				&& this.documentoSeleccionado.getTipoDocumento() != null
        //				&& this.documentoSeleccionado.getTipoDocumento().getClase() != null) {
        //
        //			if (this.documentoSeleccionado.getTipoDocumento().getClase()
        //					.equals(Constantes.OTRO)) {
        //				tdTemp.getTipoDocumento().setId(
        //						ETipoDocumentoId.OTRO.getId());
        //			} else if (this.documentoSeleccionado.getTipoDocumento().getClase()
        //					.equals(Constantes.OFICIO)) {
        //				tdTemp.getTipoDocumento().setId(
        //						ETipoDocumentoId.OFICIOS_REMISORIOS.getId());
        //			} else if (this.documentoSeleccionado.getTipoDocumento().getClase()
        //					.equals(Constantes.RESOLUCION)) {
        //				tdTemp.getTipoDocumento().setId(
        //						ETipoDocumentoId.RESOLUCION_CONSERVACION.getId());
        //			}
        //		}
        tdTemp.setUsuarioLog(this.usuario.getLogin());
        tdTemp.setFechaLog(new Date(System.currentTimeMillis()));

        //D: si la ruta no es vacía quiere decir que se cargó el archivo y se toma como aportado
        if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
            tdTemp.setAportado(this.documentoSeleccionadoAdicional.getAportado());
        }

        //D: se genera un objeto Documento que se asocia al TramiteDocumentacion que se va a
        //  adicionar al trámite
        //if(this.rutaMostrar == null && !tdTemp.getDocumentoSoporte().getArchivo().isEmpty())
        Documento doc = generarDocumento(tdTemp);
        this.documentoSeleccionadoAdicional.setDocumentoSoporte(doc);

        if (this.banderaDocumentoAdicional == true) {
            //felipe.cadena::#11867::se persiste el objeto de documentacion para que quede asociado al tramite.
            this.documentoSeleccionadoAdicional.setAdicional(ESiNo.SI.getCodigo());
            this.documentoSeleccionadoAdicional.setRequerido(ESiNo.NO.getCodigo());
            this.documentoSeleccionadoAdicional.setTramite(this.tramiteAD);
            this.documentoSeleccionadoAdicional = this.getTramiteService().
                guardarTramiteDocumentacion(this.documentoSeleccionadoAdicional);
            this.tramiteAD.getTramiteDocumentacions().add(this.documentoSeleccionadoAdicional);
            doc = this.documentoSeleccionadoAdicional.getDocumentoSoporte();
            DocumentoTramiteDTO dt = new DocumentoTramiteDTO(doc.getArchivo(),
                doc.getTipoDocumento().getNombre(),
                new Date(),
                tramiteAD.getTipoTramite(),
                tramiteAD.getId());
            dt.setNombreDepartamento(this.tramiteAD.getDepartamento().getNombre());
            dt.setNombreMunicipio(this.tramiteAD.getMunicipio().getNombre());
            dt.setNumeroPredial(this.proyectarConservacion.getPredioSeleccionado().
                getNumeroPredial());
            dt.setRutaArchivo(FileUtils.getTempDirectoryPath() + fileSeparator + doc.getArchivo());
            this.getGeneralesService().actualizarDocumentoYAlfresco(this.usuario, dt, doc);

            this.documentacionAdicional.add(this.documentoSeleccionadoAdicional);
        } else {
            tdTemp.setAdicional(ESiNo.NO.getCodigo());
            tdTemp.setRequerido(ESiNo.SI.getCodigo());
        }

        if (this.documentoSeleccionadoAdicional.getDigital() == null ||
            this.documentoSeleccionadoAdicional.getDigital().isEmpty()) {
            this.documentoSeleccionadoAdicional.setDigital(ESiNo.NO.getCodigo());
        }

        this.documentoSeleccionadoAdicional = new TramiteDocumentacion();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Crea un objeto Documento que se va a insertar en la bd como parte de un TramiteDocumentacion
     *
     * @modified juanfelipe.garcia 19-07-2013 se conserva el documentoSoporte en caso de edicion
     *
     * @param tdTemp
     * @return
     */
    private Documento generarDocumento(TramiteDocumentacion tdTemp) {

        Documento doc = new Documento();
        Date currentDate = new Date(System.currentTimeMillis());

        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setFechaDocumento(currentDate);
        if (tdTemp.getDocumentoSoporte() != null &&
            tdTemp.getDocumentoSoporte().getFechaRadicacion() != null) {
            doc.setFechaRadicacion(tdTemp.getDocumentoSoporte()
                .getFechaRadicacion());
        }
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioLog(this.usuario.getLogin());
        doc.setFechaLog(currentDate);
        doc.setTipoDocumento(tdTemp.getTipoDocumento());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setNombreDeArchivoCargado(this.nombreTemporalArchivo);
        doc.setTramiteId(tdTemp.getTramite().getId());
        doc.setDescripcion(tdTemp.getDetalle());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setCantidadFolios(tdTemp.getCantidadFolios());

        if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
            doc.setArchivo(this.rutaMostrar);
        } else if (this.rutaMostrar == null &&
            tdTemp.getDocumentoSoporte() != null &&
            tdTemp.getDocumentoSoporte().getArchivo() != null &&
            !tdTemp.getDocumentoSoporte().getArchivo().isEmpty()) {
            doc.setArchivo(tdTemp.getDocumentoSoporte().getArchivo());
        } else {
            doc.setArchivo(" ");
        }

        return doc;
    }

    /**
     * Metodo para validar la carga del archivo desde la pagina compartida de reemplazar documento.
     *
     * @author felipe.cadena
     * @param eventoCarga
     */
    public void archivoUploadTemp(FileUploadEvent eventoCarga) {
        //se encapsula este metodo porque el nombre 'archivoUploadTemp' se utiliza
        //en componentes genericos con diferentes Managed Beans y no se si 'archivoUpload'
        //se esta utilizando en mas paginas.
        this.archivoUpload(eventoCarga);

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     *
     * @modified by juanfelipe.garcia :: 29-08-2013 :: adicion de logica para no marcar anàlogo el
     * documento
     */
    /*
     * @modified leidy.gonzalez #10683 12-12-2014 :: cambio de caracteres especiales por espacion
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        //String nombreArchivoSinCaracteresEspeciales = eventoCarga.getFile().getFileName().replaceAll("[^a-zA-Z0-9./]", "_");
        //this.rutaMostrar = nombreArchivoSinCaracteresEspeciales;
        this.rutaMostrar = eventoCarga.getFile().getFileName();

        try {
            // TODO esto debe ser generico no del reporte
            String directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();

            this.archivoResultado = new File(directorioTemporal, this.rutaMostrar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.nombreTemporalArchivo = eventoCarga.getFile().getFileName();

            this.banderaDocumentoCargado = true;

            if (this.banderaDocumentoCargado == true &&
                 this.banderaDocumentoEncontradoEnCorrrespondencia == true) {
                this.banderaBotonGuardarDocPrueba = true;
            }

            // Se reinicia el valor de digitalizable y de aporta debido a que
            // estos tres campos son exluyentes entre si.
            if (this.documentoSeleccionadoAdicional != null) {
                this.documentoSeleccionadoAdicional.setAnalogo(false);
                this.documentoSeleccionadoAdicional.setDigitalizable(false);
            }

            this.addMensajeInfo("El archivo " + this.rutaMostrar + " se cargó con éxito");

        } catch (IOException e) {
            String mensaje = "El archivo seleccionado no pudo ser cargado";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------
    public void onChangeOrdenDepartamentosDocumento() {
        this.updateDepartamentosDocumento();
    }

    // --------------------------------------------------------------------------
    public void onChangeDepartamentosDocumento() {
        this.municipioDocumento = null;
        this.updateMunicipiosDocumento();
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenDepartamentosDocumento() {
        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosDocumento = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosDocumento = EOrden.CODIGO;
        }
        this.updateDepartamentosDocumento();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        this.tipoMimeDocTemporal = fileNameMap.getContentTypeFor(this.documentoSeleccionado.
            getDocumentoSoporte().getArchivo());

        return this.tipoMimeDocTemporal;
    }
// --------------------------------------------------------------------------------------------------

    /**
     * Método encargado de la descarga de un archivo temporal de office
     */
    public void controladorDescargaArchivos() {
        if (this.documentoSeleccionado != null &&
             this.documentoSeleccionado.getDocumentoSoporte() != null &&
             this.documentoSeleccionado.getDocumentoSoporte()
                .getArchivo() != null &&
             !this.documentoSeleccionado.getDocumentoSoporte()
                .getArchivo().isEmpty()) {
            File fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.documentoSeleccionado.getDocumentoSoporte()
                    .getArchivo());
            InputStream stream;
            try {
                stream = new FileInputStream(fileTemp);
                cargarDocumentoTemporalPrev();
                this.fileTempDownload = new DefaultStreamedContent(stream,
                    this.tipoMimeDocTemporal, this.documentoSeleccionado
                        .getDocumentoSoporte().getArchivo());
            } catch (FileNotFoundException e) {
                String mensaje = "Ocurrió un error al cargar al archivo." +
                     " Por favor intente nuevamente.";
                this.addMensajeError(mensaje);
            }
        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }
    }
//--------------------------------------------------------------------------------------------

    /**
     * Método ejecutado sobre el botón "Asociar documentos cargados" Asocia los documentos
     * adicionales cargados al trámite sobre el que se está trabajando.
     *
     * @author juan.agudelo copiado por franz.gamba
     *
     */
    public void asociarDocumentacion() {

        if (this.documentacionAdicional.size() > 0) {

            if (this.tramiteAD.getTramiteDocumentacions() == null) {
                this.tramiteAD.setTramiteDocumentacions(new ArrayList<TramiteDocumentacion>());
            }

            this.documentacionAdicional = this.getTramiteService().
                actualizarTramiteDocumentacionConAlfresco(
                    this.documentacionAdicional, this.tramiteAD.getSolicitud(), this.usuario);
            this.getTramiteService().actualizarTramiteDocumentacion(this.documentacionAdicional);
            for (TramiteDocumentacion tdTemp : this.documentacionAdicional) {

                if (!this.tramiteAD.getTramiteDocumentacions().contains(tdTemp)) {
                    this.tramiteAD.getTramiteDocumentacions().add(tdTemp);
                    if (this.detallesTramite.getDocumentacionAdicional() == null) {
                        this.detallesTramite.setDocumentacionAdicional(
                            new ArrayList<TramiteDocumentacion>());
                        this.detallesTramite.getDocumentacionAdicional().add(tdTemp);
                    } else {
                        this.detallesTramite.getDocumentacionAdicional().add(tdTemp);
                    }
                }
            }
            try {

                this.tramiteAD.setSolicitud(this.tramiteAD.getSolicitud());

                this.getTramiteService().
                    actualizarTramiteDocumentos(this.tramiteAD, this.usuario);
                this.proyectarConservacion.setTramite(this.getTramiteService()
                    .buscarTramitePorTramiteIdProyeccion(this.tramiteAD.getId()));

                this.tramiteAD = this.proyectarConservacion.getTramite();

                this.addMensajeInfo("La Asociación del documento se realizó exitosamente");

            } catch (Exception e) {
                String mensaje = "Ocurrió un error al guardar la solicitud de pruebas." +
                     " Por favor intente nuevamente";
                this.addMensajeError(mensaje);
                LOGGER.error(e.getMessage(), e);
            }

        } else {
            String mensaje = "Debe agregar por lo menos un documento para ser asociado." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    /**
     * Método para eliminar el documento digital asociado
     *
     * @author juanfelipe.garcia
     */
    public void eliminarDocumentoSoporte() {
        this.documentoSoporteEliminado = true;
        this.rutaMostrar = "";
        this.archivoResultado = null;
        this.nombreTemporalArchivo = new String();

        if (this.documentoSeleccionado != null) {
            this.documentoSeleccionado.setAnalogo(true);
        } else if (this.documentoSeleccionadoAdicional != null) {
            this.documentoSeleccionadoAdicional.setAnalogo(true);
        }

    }

}
