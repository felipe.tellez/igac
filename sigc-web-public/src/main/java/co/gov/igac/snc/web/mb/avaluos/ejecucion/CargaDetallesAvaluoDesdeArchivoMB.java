package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioAnexo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioCultivo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EArchivoCargueDetallesAvaluo;
import co.gov.igac.snc.persistence.util.EArchivoCarguePrediosAvaluos;
import co.gov.igac.snc.persistence.util.EAvaluoDetalle;
import co.gov.igac.snc.persistence.util.EAvaluoPredioAnexoFuente;
import co.gov.igac.snc.persistence.util.EAvaluoPredioAnexoTipo;
import co.gov.igac.snc.persistence.util.ECampoDetalleAvaluo;
import co.gov.igac.snc.persistence.util.ECultivoAspectoTecnico;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EUnidadMedida;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.CarguePrediosDeAvaluoDesdeArchivoMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Managed bean para gestionar las vistas del caso de uso Cargar Zonificación de Terreno desde
 * Archivo
 *
 * @cu CU-SA-AC-058
 * @author felipe.cadena
 */
@Component("cargaDetallesAvaluoDesdeArchivo")
@Scope("session")
public class CargaDetallesAvaluoDesdeArchivoMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 3112565174526536784L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(CarguePrediosDeAvaluoDesdeArchivoMB.class);
    @Autowired
    private IContextListener contexto;
    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;
    /**
     * Almacena el archivo cargado
     */
    private File archivoResultado;
    /**
     * Lista que contiene las zonas que se están cargando
     */
    private List<AvaluoPredioZona> zonas;
    /**
     * Lista que contiene los anexos que se están cargando
     */
    private List<AvaluoPredioAnexo> anexos;
    /**
     * Lista que contiene las maquinarias que se están cargando
     */
    private List<AvaluoPredioMaquinaria> maquinarias;
    /**
     * Lista que contiene las construcciones que se están cargando
     */
    private List<AvaluoPredioConstruccion> construcciones;
    /**
     * Lista que contiene los cultivos que se están cargando
     */
    private List<AvaluoPredioCultivo> cultivos;
    /**
     * Lista que contiene los predios que se están cargando
     */
    private List<AvaluoPredio> predios;
    /**
     * Avaluo relacionado a las zonas
     */
    private Avaluo avaluo;
    /**
     * Estructura para las causas de rechazo de la información por archivo
     */
    private List<Object[]> causasRechazo;
    /**
     * Cadena para las causas de rechazo de la información por registro
     */
    private String causasRechazoRegistro;
    /**
     * Banderas para el manejo del archivo a cargar
     */
    private boolean banderaDatosValidos;
    private boolean banderaArchivoValidado;
    private boolean banderaArchivoCargado;
    /**
     * Banderas para usar en validaciones
     */
    private boolean banderaPredioNulo;
    private boolean banderaErrorRegistro;
    private Boolean banderaTodosPredioNulo;
    /**
     * Número del predio asociado a la carga
     */
    private String numeroPredial;
    /**
     * Enumeración que determina el tipo de detalle que se va a cargar
     */
    private EAvaluoDetalle tipoDetalle;
    /**
     * Linea actual en la que se esta leyendo el archivo
     */
    private int linea;
    /**
     * Registro actual que se esta leyendo del archivo
     */
    private String registro;
    /**
     * Departamento asociado al predio
     */
    private Departamento departamentoPredio;
    /**
     * Municipio asociado al predio
     */
    private Municipio municipioPredio;

    //------------------------Getter y Setters------------------------------//
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public EAvaluoDetalle getTipoDetalle() {
        return tipoDetalle;
    }

    public void setTipoDetalle(EAvaluoDetalle tipoDetalle) {
        this.tipoDetalle = tipoDetalle;
    }

    public boolean isBanderaPredioNulo() {
        return banderaPredioNulo;
    }

    public void setBanderaPredioNulo(boolean banderaPredioNulo) {
        this.banderaPredioNulo = banderaPredioNulo;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public File getArchivoResultado() {
        return archivoResultado;
    }

    public void setArchivoResultado(File archivoResultado) {
        this.archivoResultado = archivoResultado;
    }

    public List<AvaluoPredio> getPredios() {
        return predios;
    }

    public void setPredios(List<AvaluoPredio> predios) {
        this.predios = predios;
    }

    public List<AvaluoPredioZona> getZonas() {
        return zonas;
    }

    public void setZonas(List<AvaluoPredioZona> predios) {
        this.zonas = predios;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public boolean isBanderaDatosValidos() {
        return banderaDatosValidos;
    }

    public void setBanderaDatosValidos(boolean banderaDatosValidos) {
        this.banderaDatosValidos = banderaDatosValidos;
    }

    public boolean isBanderaArchivoValidado() {
        return banderaArchivoValidado;
    }

    public void setBanderaArchivoValidado(boolean banderaArchivoValidado) {
        this.banderaArchivoValidado = banderaArchivoValidado;
    }

    public boolean isBanderaArchivoCargado() {
        return banderaArchivoCargado;
    }

    public void setBanderaArchivoCargado(boolean banderaArchivoCargado) {
        this.banderaArchivoCargado = banderaArchivoCargado;
    }

    public List<Object[]> getCausasRechazo() {
        return causasRechazo;
    }

    public void setCausasRechazo(List<Object[]> causasRechazo) {
        this.causasRechazo = causasRechazo;
    }

    public List<AvaluoPredioAnexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<AvaluoPredioAnexo> anexos) {
        this.anexos = anexos;
    }

    public List<AvaluoPredioMaquinaria> getMaquinarias() {
        return maquinarias;
    }

    public void setMaquinarias(List<AvaluoPredioMaquinaria> maquinarias) {
        this.maquinarias = maquinarias;
    }

    public List<AvaluoPredioConstruccion> getConstrucciones() {
        return construcciones;
    }

    public void setConstrucciones(List<AvaluoPredioConstruccion> construcciones) {
        this.construcciones = construcciones;
    }

    public List<AvaluoPredioCultivo> getCultivos() {
        return cultivos;
    }

    public void setCultivos(List<AvaluoPredioCultivo> cultivos) {
        this.cultivos = cultivos;
    }

    public boolean isBanderaTodosPredioNulo() {
        return banderaTodosPredioNulo;
    }

    public void setBanderaTodosPredioNulo(boolean banderaTodosPredioNulo) {
        this.banderaTodosPredioNulo = banderaTodosPredioNulo;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaOficioRemisorioTerritorialMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
        this.zonas = new ArrayList<AvaluoPredioZona>();
        this.cultivos = new ArrayList<AvaluoPredioCultivo>();
        this.maquinarias = new ArrayList<AvaluoPredioMaquinaria>();
        this.construcciones = new ArrayList<AvaluoPredioConstruccion>();
        this.anexos = new ArrayList<AvaluoPredioAnexo>();
        this.predios = new ArrayList<AvaluoPredio>();

    }

    /**
     * Método para llamar el caso de uso desde otro MB
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @param numeroPredial
     * @param tipoDetalle {@link EAvaluoDetalle}
     */
    public void cargarDetalles(Long idAvaluo, String numeroPredial, EAvaluoDetalle tipoDetalle) {
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.numeroPredial = numeroPredial;
        this.banderaTodosPredioNulo = null;
        this.tipoDetalle = tipoDetalle;
    }

    /**
     * Listener que se encarga de realizar la carga del archivo desde el componente fileupload de la
     * vista
     *
     * @author felipe.cadena
     * @param eventoCarga
     *
     */
    public void cargarArchivo(FileUploadEvent eventoCarga) {

        this.banderaArchivoValidado = false;

        Object[] cargaArchivo = UtilidadesWeb.cargarArchivo(eventoCarga, this.contexto);

        if (cargaArchivo[0] != null) {

            this.archivoResultado = (File) cargaArchivo[0];

            this.banderaArchivoCargado = true;

            String mensaje = "Documento cargado satisfactoriamente.";
            this.addMensajeInfo(mensaje);

        } else {

            this.banderaArchivoCargado = false;

            LOGGER.error("ERROR: " + cargaArchivo[2]);
            String mensaje = (String) cargaArchivo[1];
            this.addMensajeError(mensaje);
        }
    }

    /**
     * Método para validar la información del archivo cargado
     *
     * @author felipe.cadena
     *
     */
    public void validarArchivo() {
        LOGGER.debug("Validando archivo ...");
        this.causasRechazo = new ArrayList<Object[]>();

        try {

            InputStreamReader r = new InputStreamReader(new FileInputStream(this.archivoResultado));

            Reader reader = new InputStreamReader(new FileInputStream(this.archivoResultado), r.
                getEncoding());
            BufferedReader bf = new BufferedReader(reader);

            String separador = Constantes.CADENA_SEPARADOR_ARCHIVO_CARGUE_PREDIOS_AVALUOS;

            this.linea = 0;
            while ((this.registro = bf.readLine()) != null) {

                this.banderaErrorRegistro = false;
                this.causasRechazoRegistro = "";
                this.registro = this.registro.replace(separador, "¥");
                String columnas[] = this.registro.split("¥");
                this.registro = this.registro.replace("¥", separador);
                this.linea++;

                if (!this.validarEstructura(columnas)) {
                    continue;
                }
                this.validarDatosPredio(columnas);

                // Si los datos del predio son nulos no realiza validadciones sobre ellos.
                if (!this.banderaPredioNulo) {
                    String numPredial;
                    if (this.tipoDetalle == EAvaluoDetalle.PREDIOS) {
                        numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL_PREDIO.
                            getColumna()];
                    } else {
                        numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.
                            getColumna()];
                    }
                    String codigoDepto = columnas[EArchivoCargueDetallesAvaluo.DEPARTAMENTO.
                        getColumna()];
                    this.validarDepartamento(codigoDepto, numPredial);

                    String codigoMun = columnas[EArchivoCargueDetallesAvaluo.MUNICIPIO.getColumna()];
                    this.validarMunicipio(codigoMun, numPredial, codigoDepto);
                }

                if (this.tipoDetalle == EAvaluoDetalle.ZONAS_ECONOMICAS) {
                    this.validarZonas(columnas);
                }
                if (this.tipoDetalle == EAvaluoDetalle.ANEXOS) {
                    this.validarAnexos(columnas);
                }
                if (this.tipoDetalle == EAvaluoDetalle.CONSTRUCCIONES) {
                    this.validarConstrucciones(columnas);
                }
                if (this.tipoDetalle == EAvaluoDetalle.CULTIVOS) {
                    this.validarCultivos(columnas);
                }
                if (this.tipoDetalle == EAvaluoDetalle.MAQUINARIA) {
                    this.validarMaquinaria(columnas);
                }
                if (this.tipoDetalle == EAvaluoDetalle.PREDIOS) {
                    this.validarPredios(columnas);
                }

                if (this.banderaErrorRegistro) {
                    this.agregarCausaDefinitiva();
                }
            }

        } catch (FileNotFoundException e) {
            String mensaje = "El documento no pudo ser encontrado.";
            this.addMensajeError(mensaje);
        } catch (IOException e) {
            String mensaje = "El documento no pudo ser cargado.";
            this.addMensajeError(mensaje);
        }

        this.banderaArchivoValidado = true;
        LOGGER.debug("Archivo validado");

    }

    /**
     * Método para validar si la estructura del registro es valida
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
    private boolean validarEstructura(String[] columnas) {

        int numeroColumnas = this.tipoDetalle.getColumnas();

        if (columnas[0] != null && columnas.length == numeroColumnas) {
            return true;
        } else {

            String causal = "No tiene los " + numeroColumnas + " campos requeridos para la carga.";
            this.agregarLineaRechazada(causal);
            this.agregarCausaDefinitiva();
            return false;
        }

    }

    /**
     * Método para validar que los datos del predio sean consistentes todos nulos o todos con valor
     *
     * @author felipe.cadena
     * @return
     */
    private boolean validarDatosPredio(String[] columnas) {

        String depto = columnas[EArchivoCargueDetallesAvaluo.DEPARTAMENTO.getColumna()];
        String mun = columnas[EArchivoCargueDetallesAvaluo.MUNICIPIO.getColumna()];
        String numPredial;
        if (this.tipoDetalle == EAvaluoDetalle.PREDIOS) {
            numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL_PREDIO.getColumna()];
        } else {
            numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getColumna()];
        }

        if (depto == null || "".equals(depto)) {
            if (mun == null || "".equals(mun)) {
                if (numPredial == null || "".equals(numPredial)) {
                    this.banderaPredioNulo = true;
                    return true;
                }
            }
        }
        if (depto != null && !"".equals(depto)) {
            if (mun != null && !"".equals(mun)) {
                if (numPredial != null && !"".equals(numPredial)) {
                    this.banderaPredioNulo = false;
                    return true;
                }
            }
        }

        String causal = "Debe ingresar los datos completos del predio.";

        this.agregarLineaRechazada(causal);
        return false;
    }

    /**
     * Método para validar si la un campo del registro es nulo, y si lo es agrega la causal de
     * rechazo
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
//REVIEW :: felipe.cadena :: el nombre del método dice lo contrario a lo que hace  :: pedro.garcia
    private boolean validarNulo(String campo, String nombreCampo) {

        if (campo == null || "".equals(campo)) {
            String causal = nombreCampo + " debe contener información.";

            this.agregarLineaRechazada(causal);
            return false;
        } else {
            return true;
        }

    }

    /**
     * Método para validar si el campo esta dentro del dominio correspondiente en el sistema usando
     * la enumeracion correspondiente al dominio
     *
     * @author felipe.cadena
     * @param campo
     * @param dom class de la enumeración asociada al dominio
     * @param nombreCampo
     */
    private String validarDominio(String campo, Class dom, String nombreCampo) {
        Object[] values = dom.getEnumConstants();

        String valor = null;

        boolean existe = false;
        for (Object object : values) {
            ECampoDetalleAvaluo val = (ECampoDetalleAvaluo) object;
            LOGGER.debug("valor " + val.getValor());
            if (campo.equals(val.getValor())) {
                existe = true;
                valor = val.getCodigo();
                break;
            }
        }

        if (existe) {
            return valor;
        } else {
            String causal = "El " + nombreCampo + " (" + campo + ") " +
                 "contiene información que no corresponde con la existente en el sistema";

            this.agregarLineaRechazada(causal);
            return null;
        }

    }

    /**
     * Método para validar si el campo esta dentro del dominio correspondiente en el sistema
     * consultando el valor del dominio en base de datos
     *
     * @author felipe.cadena
     * @param campo
     * @param dom {@link EDominio}
     * @param nombreCampo
     */
    private String validarDominioBD(String campo, Enum dom, String nombreCampo) {

        List<Dominio> valores = this.getGeneralesService()
            .getCacheDominioPorNombre(dom);

        String codigo = null;

        boolean existe = false;
        for (Dominio dominio : valores) {
            LOGGER.debug("valor " + dominio.getValor());
            if (campo.equals(dominio.getValor())) {
                existe = true;
                codigo = dominio.getCodigo();
                break;
            }
        }

        if (existe) {
            return codigo;
        } else {
            String causa = "El " + nombreCampo + " (" + campo + ") " +
                 "contiene información que no corresponde con la existente en el sistema";

            this.agregarLineaRechazada(causa);
            return null;
        }

    }

    /**
     * Método para validar si la un campo del registro es nulo, y si lo es agrega la causal de
     * rechazo
     *
     * @author felipe.cadena
     * @param columnas
     * @return
     */
    private Double validarNumerico(String campo, String nombreCampo) {

        Double valorNumerico = null;
        if (campo.isEmpty()) {
            return null;
        }

        try {
            valorNumerico = Double.parseDouble(campo);

        } catch (Exception e) {
            String causal = "El campo " +
                 nombreCampo + " (" +
                 campo + ") debe ser númerico.";
            this.agregarLineaRechazada(causal);
        }
        return valorNumerico;

    }

    /**
     * Valida el municipio, (longitud, existencia y correspondencia con número predial)
     *
     * @author felipe.cadena
     * @param codigoMunicipio
     * @param numeroPredial
     * @param codigoDepartamento
     * @return
     */
    private Boolean validarMunicipio(String codigoMunicipio, String numeroPredial,
        String codigoDepartamento) {

        this.municipioPredio = null;

        if ("".equals(codigoMunicipio) || codigoMunicipio == null) {
            return true;
        }

        String codigoCompleto = codigoDepartamento + "" + codigoMunicipio;

        int longitudCodigo = EArchivoCargueDetallesAvaluo.MUNICIPIO.getLongitud();

        if (codigoMunicipio.length() != longitudCodigo) {

            String causal = "El campo " +
                 EArchivoCargueDetallesAvaluo.MUNICIPIO.getDescripcion() + " (" +
                 codigoMunicipio + ") debe ser de longitud " + longitudCodigo + ".";
            this.agregarLineaRechazada(causal);

        } else if (!numeroPredial.substring(2, 5).equals(codigoMunicipio)) {

            String causal = "El campo " +
                 EArchivoCargueDetallesAvaluo.MUNICIPIO.getDescripcion() + " (" +
                 codigoMunicipio +
                 ") no corresponde con el municipio del número predial " +
                 numeroPredial.substring(2, 5) + ".";
            this.agregarLineaRechazada(causal);

        } else {

            Municipio mun = this.getGeneralesService().getCacheMunicipioByCodigo(
                codigoCompleto);

            if (mun == null) {
                String causal = "El valor del campo " +
                     EArchivoCargueDetallesAvaluo.MUNICIPIO.getDescripcion() + " (" +
                     codigoMunicipio +
                     ") no coincide con ninguno registrado en el sistema.";
                this.agregarLineaRechazada(causal);
            } else {
                this.municipioPredio = mun;
                return true;
            }
        }
        return false;
    }

    /**
     * Valida el departamento, (longitud, existencia y correspondencia con número predial)
     *
     * @author felipe.cadena
     * @param codigoDepartamento
     * @param numeroPredial
     */
    private Boolean validarDepartamento(String codigoDepartamento, String numeroPredial) {
        this.departamentoPredio = null;
        if ("".equals(codigoDepartamento) || codigoDepartamento == null) {
            return true;
        }

        int longitudCodigo = EArchivoCargueDetallesAvaluo.DEPARTAMENTO.getLongitud();

        if (codigoDepartamento.length() != longitudCodigo) {

            String causal = "El campo " +
                 EArchivoCargueDetallesAvaluo.DEPARTAMENTO.getDescripcion() + " (" +
                 codigoDepartamento + ") debe ser de longitud " + longitudCodigo + ".";
            this.agregarLineaRechazada(causal);

        } else if (!numeroPredial.substring(0, 2).equals(codigoDepartamento)) {

            String causal = "El campo " +
                 EArchivoCargueDetallesAvaluo.DEPARTAMENTO.getDescripcion() + " (" +
                 codigoDepartamento +
                 ") no corresponde con el departamento del número predial " +
                 numeroPredial.substring(0, 2) + ".";
            this.agregarLineaRechazada(causal);

        } else {

            Departamento detpo = this.getGeneralesService().getCacheDepartamentoByCodigo(
                codigoDepartamento);

            if (detpo == null) {
                String causal = "El valor del campo " +
                     EArchivoCargueDetallesAvaluo.DEPARTAMENTO.getDescripcion() + " (" +
                     codigoDepartamento +
                     ") no coincide con ninguno registrado en el sistema.";
                this.agregarLineaRechazada(causal);
            } else {
                this.departamentoPredio = detpo;
                return true;
            }
        }
        return false;
    }

    /**
     * Valida la longitud del numero predial y si el numero corresponde a alguno de los predios
     * asociados al avalúo
     *
     * @author felipe.cadena
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     */
    private AvaluoPredio validarNumeroPredial(String numeroPredial) {

        int longitudColumna = 0;
        if (this.tipoDetalle == EAvaluoDetalle.PREDIOS) {
            longitudColumna = EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL_PREDIO.getLongitud();
        } else {
            longitudColumna = EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getLongitud();
        }

        if (longitudColumna != numeroPredial.length()) {

            String causal = "El campo " +
                 EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getDescripcion() + " (" +
                 numeroPredial + ") debe ser de longitud " + longitudColumna + ".";
            this.agregarLineaRechazada(causal);
        } else {

            AvaluoPredio predioTemp = null;
            for (AvaluoPredio ap : this.avaluo.getAvaluoPredios()) {
                if (ap.getNumeroPredial().equals(numeroPredial)) {
                    predioTemp = ap;
                }
            }
            if (predioTemp == null) {
                if (this.tipoDetalle == EAvaluoDetalle.PREDIOS) {
                    Predio predioSistema = this.getConservacionService().getPredioByNumeroPredial(
                        numeroPredial);

                    if (predioSistema == null) {
                        String causa = "El valor del campo " +
                             EArchivoCarguePrediosAvaluos.NUMERO_PREDIAL.getDescripcion() + " (" +
                             numeroPredial +
                             ") no coincide con ninguno predio registrado en el sistema.";
                        this.agregarLineaRechazada(causa);
                    } else {
                        AvaluoPredio aps = new AvaluoPredio(predioSistema);
                        return aps;
                    }
                } else {
                    String causal = "El valor del campo " +
                         EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getDescripcion() + " (" +
                         numeroPredial +
                         ") no está asociado al avalúo.";
                    this.agregarLineaRechazada(causal);
                }
            } else {
                return predioTemp;
            }
        }
        return null;
    }

    /**
     * Método para validar el campo circulo registral del predio
     *
     * @author felipe.cadena
     * @param circuloRegistral
     * @param matricula
     */
    private boolean validarCirculoRegistral(String circuloRegistral, Boolean matricula) {

        int longitudColumna = EArchivoCargueDetallesAvaluo.CIRCULO_REGISTRAL_PREDIO.getLongitud();
        if (matricula == null) {
            if (!circuloRegistral.isEmpty()) {
                this.agregarLineaRechazada(
                    "Debe ingresar la matricula antigua si va a registrar circulo registral");
                return false;
            } else {
                return true;
            }
        }

        if (!matricula) {

            if (circuloRegistral.isEmpty()) {
                String causa = "Debe ingresar los datos completos de la matricula inmobiliaria";
                this.agregarLineaRechazada(causa);
                return false;
            } else if (longitudColumna != circuloRegistral.length()) {
                String causa = "El campo " +
                     EArchivoCargueDetallesAvaluo.CIRCULO_REGISTRAL_PREDIO.getDescripcion() + " (" +
                     circuloRegistral + ") debe ser de longitud " + longitudColumna + ".";
                this.agregarLineaRechazada(causa);
                return false;

            } else {

                CirculoRegistral circuloRegistralTemp = this.getGeneralesService()
                    .getCirculoRegistralByCodigo(circuloRegistral);

                if (circuloRegistralTemp == null) {
                    String causa = "El valor del campo " +
                         EArchivoCargueDetallesAvaluo.CIRCULO_REGISTRAL_PREDIO.getDescripcion() +
                         " (" + circuloRegistral +
                         ") no coincide con ninguno registrado en el sistema.";
                    this.agregarLineaRechazada(causa);
                    return false;
                }
            }
        } else {
            if (!circuloRegistral.isEmpty()) {
                String causa = "No debe ingresar datos (" +
                     EArchivoCargueDetallesAvaluo.CIRCULO_REGISTRAL_PREDIO.getDescripcion() +
                     ") de la matricula inmobiliaria antigua.";
                this.agregarLineaRechazada(causa);
                return false;
            }
        }
        return true;
    }

    /**
     * Método para validar el campo matricula antigua de predio
     *
     * @author felipe.cadena
     *
     * @param matriculaAntigua
     */
    private Boolean validarMatriculaAntigua(String matriculaAntigua) {

        if (matriculaAntigua.isEmpty()) {
            return null;
        }
        if (!matriculaAntigua.equalsIgnoreCase("s") && !matriculaAntigua.equalsIgnoreCase("n")) {

            String causa = "El campo " +
                 EArchivoCargueDetallesAvaluo.MATRICULA_ANTIGUA_PREDIO.getDescripcion() + " (" +
                 matriculaAntigua +
                 ") puede contener únicamente los valores 'S', 's', 'N' ó 'n'.";
            this.agregarLineaRechazada(causa);
            return null;
        } else {
            if (matriculaAntigua.equalsIgnoreCase("s")) {
                return true;
            } else {
                return false;
            }
        }

    }

    /**
     * Método para validar si un predio esta repetido en el archivo
     *
     * @author felipe.cadena
     *
     * @param numeroPredial
     */
    private void validarPrediosDuplicados(String numeroPredial) {

        for (AvaluoPredio ap : this.predios) {

            if (ap.getNumeroPredial().equals(numeroPredial)) {
                this.agregarLineaRechazada("Número Predial duplicado");
            }
        }
    }

    /**
     * Método para validar si un predio es colindante con los predios asociados al avalúo y con los
     * demas predios ingresados en el archivo hasta el momento.
     *
     * @author felipe.cadena
     *
     * @param numeroPredial
     */
    private void validarPrediosColindantes(String numeroPredial) {

        List<AvaluoPredio> prediosAvaluo = this.getAvaluosService().
            buscarAvaluoPredioPorIdAvaluo(this.avaluo.getId());
        List<String> numerosPredialesAvaluo = new ArrayList<String>();
        List<String> numerosPredialesArchivo = new ArrayList<String>();

        for (AvaluoPredio ap : prediosAvaluo) {
            numerosPredialesAvaluo.add(ap.getNumeroPredial());
        }

        for (AvaluoPredio ap : this.predios) {
            numerosPredialesArchivo.add(ap.getNumeroPredial());
        }

        if (!(this.getGeneralesService().esColindante(numeroPredial, numerosPredialesAvaluo) &&
             this.getGeneralesService().esColindante(numeroPredial, numerosPredialesArchivo))) {
            this.agregarLineaRechazada(
                "El predio no es colindandte con los demas predios ingresados");
        }
    }

    /**
     * Método para validar los campos del archivo cuando se cargan zonas
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
    private boolean validarZonas(String[] columnas) {
        LOGGER.debug("Validando zonas");

        String numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getColumna()];

        Double value;
        AvaluoPredioZona apz = new AvaluoPredioZona();

        AvaluoPredio ap = null;
        if (!this.banderaPredioNulo) {
            ap = this.validarNumeroPredial(numPredial);
            if (ap != null) {
                apz.setAvaluoPredioId(ap.getId());
                apz.setCodigoDepartamento(numPredial.substring(0, 2));
                apz.setCodigoMunicipio(numPredial.substring(2, 5));
                apz.setNumeroPredial(numPredial);
            }
        }

        apz.setDescripcion(columnas[EArchivoCargueDetallesAvaluo.DESCRIPCION_ZONA.getColumna()]);

        if (columnas[EArchivoCargueDetallesAvaluo.AREA.getColumna()] != null) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.AREA.getColumna()],
                EArchivoCargueDetallesAvaluo.AREA.getDescripcion());
            if (value != null) {
                apz.setArea(value);
            }
        }

        if (this.
            validarNulo(columnas[EArchivoCargueDetallesAvaluo.VALOR_UNITARIO_ZONA.getColumna()],
                EArchivoCargueDetallesAvaluo.VALOR_UNITARIO_ZONA.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.VALOR_UNITARIO_ZONA.
                getColumna()],
                EArchivoCargueDetallesAvaluo.VALOR_UNITARIO_ZONA.getDescripcion());
            if (value != null) {
                apz.setValorUnitarioTerreno(value);
            }
        }
        if (this.banderaErrorRegistro) {
            return false;
        } else {
            apz.setAvaluo(this.avaluo);
            apz.setValorTerreno(apz.getValorUnitarioTerreno() * apz.getArea());
            this.zonas.add(apz);
            return true;
        }

    }

    /**
     * Método para validar los campos del archivo cuando se cargan zonas
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
    private boolean validarAnexos(String[] columnas) {

        LOGGER.debug("Validando anexos");

        String numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getColumna()];

        Double value;
        AvaluoPredioAnexo anx = new AvaluoPredioAnexo();

        AvaluoPredio ap = null;
        if (!this.banderaPredioNulo) {
            ap = this.validarNumeroPredial(numPredial);
            if (ap != null) {
                anx.setAvaluoPredioId(ap.getId());
                anx.setCodigoDepartamento(numPredial.substring(0, 2));
                anx.setCodigoMunicipio(numPredial.substring(2, 5));
                anx.setNumeroPredial(numPredial);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.TIPO_ANEXO.getColumna()],
            EArchivoCargueDetallesAvaluo.TIPO_ANEXO.getDescripcion())) {

            String dom = this.validarDominio(columnas[EArchivoCargueDetallesAvaluo.TIPO_ANEXO.
                getColumna()],
                EAvaluoPredioAnexoTipo.class,
                EArchivoCargueDetallesAvaluo.TIPO_ANEXO.getDescripcion());
            if (dom != null) {
                anx.setTipo(dom);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.DECRIPCION_ANEXO.getColumna()],
            EArchivoCargueDetallesAvaluo.DECRIPCION_ANEXO.getDescripcion())) {
            anx.setDescripcion(columnas[EArchivoCargueDetallesAvaluo.DECRIPCION_ANEXO.getColumna()]);
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.FUENTE_ANEXO.getColumna()],
            EArchivoCargueDetallesAvaluo.FUENTE_ANEXO.getDescripcion())) {

            String dom = this.validarDominio(columnas[EArchivoCargueDetallesAvaluo.FUENTE_ANEXO.
                getColumna()],
                EAvaluoPredioAnexoFuente.class,
                EArchivoCargueDetallesAvaluo.FUENTE_ANEXO.getDescripcion());

            if (dom != null) {
                anx.setFuente(dom);
            }
        }

        if (this.
            validarNulo(columnas[EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_ANEXO.getColumna()],
                EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_ANEXO.getDescripcion())) {

            String dom = this.validarDominio(
                columnas[EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_ANEXO.getColumna()],
                EUnidadMedida.class,
                EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_ANEXO.getDescripcion());

            if (dom != null) {
                anx.setUnidadMedida(dom);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.MEDIDA_ANEXO.getColumna()],
            EArchivoCargueDetallesAvaluo.MEDIDA_ANEXO.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.MEDIDA_ANEXO.
                getColumna()],
                EArchivoCargueDetallesAvaluo.MEDIDA_ANEXO.getDescripcion());
            if (value != null) {
                anx.setMedida(value);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_ANEXO.getColumna()],
            EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_ANEXO.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_ANEXO.
                getColumna()],
                EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_ANEXO.getDescripcion());
            if (value != null) {
                anx.setValorUnidad(value);
            }
        }

        if (this.banderaErrorRegistro) {
            return false;
        } else {
            anx.setAvaluo(this.avaluo);
            anx.setValorTotal(anx.getValorUnidad() * anx.getMedida());
            this.anexos.add(anx);
            return true;
        }
    }

    /**
     * Método para validar los campos del archivo cuando se cargan predios
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
    private boolean validarPredios(String[] columnas) {

        LOGGER.debug("Validando anexos");

        String numPredial =
            columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL_PREDIO.getColumna()];

        Double value;
        AvaluoPredio predio = new AvaluoPredio();

        AvaluoPredio ap;
        if (!this.banderaPredioNulo) {
            ap = this.validarNumeroPredial(numPredial);
            if (ap != null) {
                predio = ap;
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.TIPO_AVALUO_PREDIO.getColumna()],
            EArchivoCargueDetallesAvaluo.TIPO_AVALUO_PREDIO.getDescripcion())) {

            String dom = this.validarDominioBD(
                columnas[EArchivoCargueDetallesAvaluo.TIPO_AVALUO_PREDIO.getColumna()],
                EDominio.AVALUO_PREDIO_TIPO_AVALUO,
                EArchivoCargueDetallesAvaluo.TIPO_AVALUO_PREDIO.getDescripcion());
            if (dom != null) {
                predio.setTipoAvaluoPredio(dom);
            }
        }

        Boolean matriculaAntigua = this.validarMatriculaAntigua(
            columnas[EArchivoCargueDetallesAvaluo.MATRICULA_ANTIGUA_PREDIO.getColumna()]);

        if (this.validarCirculoRegistral(
            columnas[EArchivoCargueDetallesAvaluo.CIRCULO_REGISTRAL_PREDIO.getColumna()],
            matriculaAntigua)) {
            predio.setCirculoRegistral(
                columnas[EArchivoCargueDetallesAvaluo.CIRCULO_REGISTRAL_PREDIO.getColumna()]);
        }

        value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.NUMERO_MATRICULA_PREDIO.
            getColumna()],
            EArchivoCargueDetallesAvaluo.NUMERO_MATRICULA_PREDIO.getDescripcion());
        if (value != null) {
            if (matriculaAntigua != null) {
                predio.setNumeroRegistro(value.toString());
            } else {
                this.agregarLineaRechazada(
                    "Debe ingresar la matricula antigua si va a registrar número de matricula");
            }
        } else {
            if (matriculaAntigua != null && !matriculaAntigua) {
                this.agregarLineaRechazada(
                    "Debe ingresar los datos completos de la matricula inmobiliaria");
            } else {
                predio.setNumeroRegistro("");
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.DIRECCION_PREDIO.getColumna()],
            EArchivoCargueDetallesAvaluo.DIRECCION_PREDIO.getDescripcion())) {
            predio.
                setDireccion(columnas[EArchivoCargueDetallesAvaluo.DIRECCION_PREDIO.getColumna()]);
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.DESTINO_PREDIO.getColumna()],
            EArchivoCargueDetallesAvaluo.DESTINO_PREDIO.getDescripcion())) {

            String dom = this.validarDominioBD(columnas[EArchivoCargueDetallesAvaluo.DESTINO_PREDIO.
                getColumna()],
                EDominio.PREDIO_DESTINO,
                EArchivoCargueDetallesAvaluo.DESTINO_PREDIO.getDescripcion());
            if (dom != null) {
                predio.setDestino(dom);
            }
        }

        if (this.validarNulo(
            columnas[EArchivoCargueDetallesAvaluo.TIPO_INMUEBLE_PREDIO.getColumna()],
            EArchivoCargueDetallesAvaluo.TIPO_INMUEBLE_PREDIO.getDescripcion())) {

            String dom = this.validarDominioBD(
                columnas[EArchivoCargueDetallesAvaluo.TIPO_INMUEBLE_PREDIO.getColumna()],
                EDominio.PREDIO_TIPO_INMUEBLE,
                EArchivoCargueDetallesAvaluo.TIPO_INMUEBLE_PREDIO.getDescripcion());
            if (dom != null) {
                predio.setTipoInmueble(dom);
            }
        }

        this.validarPrediosDuplicados(numPredial);
        //this.validarPrediosColindantes(numPredial);

        if (this.banderaErrorRegistro) {
            return false;
        } else {
            predio.setAvaluo(this.avaluo);
            this.predios.add(predio);
            return true;
        }
    }

    /**
     * Método para validar los campos del archivo cuando se cargan zonas
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
    private boolean validarCultivos(String[] columnas) {

        LOGGER.debug("Validando cultivos");
        String numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getColumna()];

        Double value = null;
        AvaluoPredioCultivo ctv = new AvaluoPredioCultivo();

        AvaluoPredio ap = null;
        if (!this.banderaPredioNulo) {
            ap = this.validarNumeroPredial(numPredial);
            if (ap != null) {
                ctv.setAvaluoPredioId(ap.getId());
                ctv.setCodigoDepartamento(numPredial.substring(0, 2));
                ctv.setCodigoMunicipio(numPredial.substring(2, 5));
                ctv.setNumeroPredial(numPredial);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.TIPO_CULTIVO.getColumna()],
            EArchivoCargueDetallesAvaluo.TIPO_CULTIVO.getDescripcion())) {
            ctv.setTipo(columnas[EArchivoCargueDetallesAvaluo.TIPO_CULTIVO.getColumna()]);
        }
        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.EDAD_CULTIVO.getColumna()],
            EArchivoCargueDetallesAvaluo.EDAD_CULTIVO.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.EDAD_CULTIVO.
                getColumna()],
                EArchivoCargueDetallesAvaluo.EDAD_CULTIVO.getDescripcion());
            if (value != null) {
                ctv.setEdad(value);
            }
        }

        if (this.
            validarNulo(columnas[EArchivoCargueDetallesAvaluo.DESCRIPCION_CULTIVO.getColumna()],
                EArchivoCargueDetallesAvaluo.DESCRIPCION_CULTIVO.getDescripcion())) {
            ctv.setDescripcion(columnas[EArchivoCargueDetallesAvaluo.DESCRIPCION_CULTIVO.
                getColumna()]);
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.ASPECTO_TECNICO_CULTIVO.
            getColumna()],
            EArchivoCargueDetallesAvaluo.ASPECTO_TECNICO_CULTIVO.getDescripcion())) {

            String dom = this.validarDominio(
                columnas[EArchivoCargueDetallesAvaluo.ASPECTO_TECNICO_CULTIVO.getColumna()],
                ECultivoAspectoTecnico.class,
                EArchivoCargueDetallesAvaluo.ASPECTO_TECNICO_CULTIVO.getDescripcion());

            if (dom != null) {
                ctv.setAspectoTecnico(dom);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.MEDIDA_CULTIVO.getColumna()],
            EArchivoCargueDetallesAvaluo.MEDIDA_CULTIVO.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.MEDIDA_CULTIVO.
                getColumna()],
                EArchivoCargueDetallesAvaluo.MEDIDA_CULTIVO.getDescripcion());
            if (value != null) {
                ctv.setCantidad(value);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_CULTIVO.
            getColumna()],
            EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_CULTIVO.getDescripcion())) {

            String dom = this.validarDominio(
                columnas[EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_CULTIVO.getColumna()],
                EUnidadMedida.class,
                EArchivoCargueDetallesAvaluo.UNIDAD_MEDIDA_CULTIVO.getDescripcion());

            if (dom != null) {
                ctv.setUnidadMedida(dom);
            }
        }

        if (this.validarNulo(
            columnas[EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_CULTIVO.getColumna()],
            EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_CULTIVO.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_CULTIVO.
                getColumna()],
                EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_CULTIVO.getDescripcion());
            if (value != null) {
                ctv.setValorUnidad(value);
            }
        }

        if (this.banderaErrorRegistro) {
            return false;
        } else {
            ctv.setAvaluo(this.avaluo);
            ctv.setValorTotal(ctv.getCantidad() * ctv.getValorUnidad());
            this.cultivos.add(ctv);
            return true;
        }
    }

    /**
     * Método para validar los campos del archivo cuando se cargan construcciones
     *
     * @author felipe.cadena
     * @param columnas
     * @return
     */
//TODO :: Se deben completar las validaciones cuando se desarrolle el caso de uso para cargar las construcciones.
    private boolean validarConstrucciones(String[] columnas) {

        LOGGER.debug("Validando construcciones");
        int numeroColumnas = EArchivoCarguePrediosAvaluos.values().length;

        if (columnas[0] != null && columnas.length == numeroColumnas) {
            return true;
        } else {

            String causal = "No tiene los " + numeroColumnas + " campos requeridos para la carga.";

            this.agregarLineaRechazada(causal);
            return false;
        }

    }

    /**
     * Método para validar los campos del archivo cuando se cargan zonas
     *
     * @author felipe.cadena
     * @param linea
     * @param registro
     * @param columnas
     * @return
     */
    private boolean validarMaquinaria(String[] columnas) {

        LOGGER.debug("Validando maqiuinaria");
        String numPredial = columnas[EArchivoCargueDetallesAvaluo.NUMERO_PREDIAL.getColumna()];
        String causaRechazo = "";

        Double value = null;
        AvaluoPredioMaquinaria mqr = new AvaluoPredioMaquinaria();

        AvaluoPredio ap = null;
        if (!this.banderaPredioNulo) {
            ap = this.validarNumeroPredial(numPredial);
            if (ap != null) {
                mqr.setAvaluoPredioId(ap.getId());
                mqr.setCodigoDepartamento(numPredial.substring(0, 2));
                mqr.setCodigoMunicipio(numPredial.substring(2, 5));
                mqr.setNumeroPredial(numPredial);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.DESCRIPCION_MAQUINARIA.
            getColumna()],
            EArchivoCargueDetallesAvaluo.DESCRIPCION_MAQUINARIA.getDescripcion())) {
            mqr.setDescripcion(columnas[EArchivoCargueDetallesAvaluo.DESCRIPCION_MAQUINARIA.
                getColumna()]);
        }

        mqr.setEspecificacion(columnas[EArchivoCargueDetallesAvaluo.ESPECIFICACION_MAQUINARIA.
            getColumna()]);

        if (this.
            validarNulo(columnas[EArchivoCargueDetallesAvaluo.CANTIDAD_MAQUINARIA.getColumna()],
                EArchivoCargueDetallesAvaluo.CANTIDAD_MAQUINARIA.getDescripcion())) {
            value = this.validarNumerico(columnas[EArchivoCargueDetallesAvaluo.CANTIDAD_MAQUINARIA.
                getColumna()],
                EArchivoCargueDetallesAvaluo.CANTIDAD_MAQUINARIA.getDescripcion());
            if (value != null) {
                mqr.setCantidad(value);
            }
        }

        if (this.validarNulo(columnas[EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_MAQUINARIA.
            getColumna()],
            EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_MAQUINARIA.getDescripcion())) {
            value = this.validarNumerico(
                columnas[EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_MAQUINARIA.getColumna()],
                EArchivoCargueDetallesAvaluo.VALOR_UNIDAD_MAQUINARIA.getDescripcion());
            if (value != null) {
                mqr.setValorUnidad(value);
            }
        }

        if (this.banderaErrorRegistro) {
            return false;
        } else {
            mqr.setAvaluo(this.avaluo);
            this.maquinarias.add(mqr);
            mqr.setValorTotal(mqr.getCantidad() * mqr.getValorUnidad());
            return true;
        }
    }

    /**
     * Método para guardar las zonas que se aceptaron el la validación del archivo.
     *
     * @author felipe.cadena
     */
    public void guardarDetallesAceptados() {

        if (this.tipoDetalle == EAvaluoDetalle.PREDIOS) {
            for (AvaluoPredio detalle : this.predios) {
                detalle.setFechaLog(new Date());
                detalle.setUsuarioLog(this.usuario.getLogin());
                detalle.setValorCotizacion(0D);
                detalle.setValorCotizacionAjustado(0D);
                this.getAvaluosService().guardarActualizarAvaluoPredio(detalle);
            }
        } else {

            Boolean estadoDetalleDB = this.getAvaluosService()
                .calcularDetallesPreviosAvaluo(this.avaluo.getId(), tipoDetalle.getCodigo());

            this.determinarDetallesPredios();

            Boolean datosExistentes = this.validarDatosExistentes(estadoDetalleDB, tipoDetalle.
                getNombre());

            if (datosExistentes) {

                this.getAvaluosService().eliminarDetallesPreviosAvaluo(
                    this.avaluo.getId(), tipoDetalle.getCodigo());

                if (this.tipoDetalle == EAvaluoDetalle.ZONAS_ECONOMICAS) {
                    for (AvaluoPredioZona detalle : this.zonas) {
                        this.getAvaluosService().guardarActualizarAvaluoPredioZona(detalle,
                            this.usuario);
                    }
                }
                if (this.tipoDetalle == EAvaluoDetalle.ANEXOS) {
                    for (AvaluoPredioAnexo detalle : this.anexos) {
                        this.getAvaluosService().guardarActualizarAvaluoPredioAnexo(detalle,
                            this.usuario);
                    }
                }
                if (this.tipoDetalle == EAvaluoDetalle.MAQUINARIA) {
                    for (AvaluoPredioMaquinaria detalle : this.maquinarias) {
                        this.getAvaluosService().guardarActualizarAvaluoPredioMaquinaria(detalle,
                            this.usuario);
                    }
                }
                if (this.tipoDetalle == EAvaluoDetalle.CULTIVOS) {
                    for (AvaluoPredioCultivo detalle : this.cultivos) {
                        this.getAvaluosService().guardarActualizarAvaluoPredioCultivo(detalle,
                            this.usuario);
                    }
                }
                if (this.tipoDetalle == EAvaluoDetalle.CONSTRUCCIONES) {
                    for (AvaluoPredioConstruccion detalle : this.construcciones) {
                        this.getAvaluosService().guardarActualizarAvaluoPredioConstruccion(detalle,
                            this.usuario);
                    }
                }
            }
        }
    }

    /**
     * Método para determinar el tipo de de carga de datos que se debe realizar dependiento los
     * detalles que hay en la base de datos.
     *
     * @author felipe.cadena
     * @param estadoDB
     * @param tipoDetalle
     * @return
     */
    private Boolean validarDatosExistentes(Boolean estadoDB, String tipoDetalle) {
        RequestContext contextoSesion = RequestContext.getCurrentInstance();
        Boolean permitirGuardado;
        if (estadoDB == null && this.banderaTodosPredioNulo != null) {
            permitirGuardado = true;
        } else {
            if (this.banderaTodosPredioNulo == null) {
                this.addMensajeError("No se pueden almacenar los datos de " + tipoDetalle +
                     " la información de predios debe ser consistente para asociarla " +
                     "al sec_radicado o al número predial");
                permitirGuardado = false;
                contextoSesion.addCallbackParam("error", "error");

            } else if (this.banderaTodosPredioNulo.equals(estadoDB) &&
                 this.banderaTodosPredioNulo.equals(true)) {
                this.addMensajeError("No se pueden almacenar los datos de " + tipoDetalle +
                     " asociadas a los predios porque ya existe información asociada al" +
                     " sec_radicado directamente");
                permitirGuardado = false;
                contextoSesion.addCallbackParam("error", "error");
            } else if (this.banderaTodosPredioNulo.equals(estadoDB) &&
                 this.banderaTodosPredioNulo.equals(false)) {
                this.addMensajeError("No se pueden almacenar los datos de " + tipoDetalle +
                     " asociadas al sec_radicado porque ya existe información asociada" +
                     " a los predios del avalúo");
                permitirGuardado = false;
                contextoSesion.addCallbackParam("error", "error");
            } else {
                permitirGuardado = true;
            }
        }

        return permitirGuardado;
    }

    /**
     * Método para determinar si los detalles cargados esten relacionados todos a predios o todos a
     * avalúos
     *
     * @author felipe.cadena
     */
    private void determinarDetallesPredios() {

        boolean predio = true;
        boolean noPredio = true;

        if (this.tipoDetalle == EAvaluoDetalle.ZONAS_ECONOMICAS) {
            for (AvaluoPredioZona apz : this.zonas) {
                if (apz.getAvaluoPredioId() == null) {
                    predio = false;
                } else {
                    noPredio = false;
                }
            }
        }
        if (this.tipoDetalle == EAvaluoDetalle.ANEXOS) {
            for (AvaluoPredioAnexo anx : this.anexos) {
                if (anx.getAvaluoPredioId() == null) {
                    predio = false;
                } else {
                    noPredio = false;
                }
            }
        }
        if (this.tipoDetalle == EAvaluoDetalle.CONSTRUCCIONES) {
            for (AvaluoPredioConstruccion cns : this.construcciones) {
                if (cns.getAvaluoPredioId() == null) {
                    predio = false;
                } else {
                    noPredio = false;
                }
            }
        }
        if (this.tipoDetalle == EAvaluoDetalle.CULTIVOS) {
            for (AvaluoPredioCultivo ctv : this.cultivos) {
                if (ctv.getAvaluoPredioId() == null) {
                    predio = false;
                } else {
                    noPredio = false;
                }
            }
        }
        if (this.tipoDetalle == EAvaluoDetalle.MAQUINARIA) {
            for (AvaluoPredioMaquinaria mqn : this.maquinarias) {
                if (mqn.getAvaluoPredioId() == null) {
                    predio = false;
                } else {
                    noPredio = false;
                }
            }
        }

        if (predio) {
            this.banderaTodosPredioNulo = false;
        }
        if (noPredio) {
            this.banderaTodosPredioNulo = true;
        }
        if (!noPredio && !predio) {
            this.banderaTodosPredioNulo = null;
        }
    }

    /**
     * Método para agregar linea de información rechazada
     *
     * @author felipe.cadena
     * @param causa
     */
    private void agregarLineaRechazada(String causa) {
        LOGGER.debug("Agregando linea rechazada...");

        this.causasRechazoRegistro += causa;
        this.causasRechazoRegistro += "; ";
        this.banderaErrorRegistro = true;

    }

    /**
     * Método para agregar la causa definitiva del error en el registro
     *
     * @author felipe.cadena
     */
    private void agregarCausaDefinitiva() {
        Object[] lr = {this.linea, this.registro, this.causasRechazoRegistro};
        this.causasRechazo.add(lr);
    }

    /**
     * Método para para reiniciar las listas de detalles
     *
     * @author felipe.cadena
     */
    public void reiniciarListas() {

        this.zonas = new ArrayList<AvaluoPredioZona>();
        this.cultivos = new ArrayList<AvaluoPredioCultivo>();
        this.maquinarias = new ArrayList<AvaluoPredioMaquinaria>();
        this.construcciones = new ArrayList<AvaluoPredioConstruccion>();
        this.anexos = new ArrayList<AvaluoPredioAnexo>();
        this.predios = new ArrayList<AvaluoPredio>();
        this.causasRechazo = new ArrayList<Object[]>();

        this.banderaArchivoCargado = false;
        this.banderaArchivoValidado = false;
    }
}
