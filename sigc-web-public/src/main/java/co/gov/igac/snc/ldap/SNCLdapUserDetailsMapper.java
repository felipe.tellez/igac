package co.gov.igac.snc.ldap;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;

/**
 *
 * @author juan.mendez
 *
 */
public class SNCLdapUserDetailsMapper extends LdapUserDetailsMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(LdapUserDetailsMapper.class);

    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public SNCLdapUserDetailsMapper() {
    }

    /**
     *
     */
    // @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx, String username,
        Collection<? extends GrantedAuthority> authorities) {
        LdapUserDetails ldapUserDetails = (LdapUserDetails) super.mapUserFromContext(ctx, username,
            authorities);

        SNCUserDetails sncDetails = new SNCUserDetails(ctx.getAttributes(), ldapUserDetails);
        return sncDetails;
    }

}
