/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EAvaluoTipoAvaluo;
import co.gov.igac.snc.persistence.util.EComponenteConstruccionCom;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipificacion;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipoCalificacion;
import co.gov.igac.snc.persistence.util.EUnidadPisoUbicacion;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.CalificacionConstruccionTreeNode;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para la carga manual de la ficha matriz
 *
 * @author felipe.cadena
 */
@Component("cargaFichaMatriz")
@Scope("session")
public class CargaFichaMatrizMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -7550906157281308499L;

    private static final String separadorCarpeta = System.getProperty("file.separator");

    private static final Logger LOGGER = LoggerFactory.getLogger(CargaFichaMatrizMB.class);
    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * lista de fichaMatriz
     */
    private List<PFichaMatriz> listaFichaMatriz;

    /**
     * Ficha matriz seleccionaa paa editar o eliminar
     */
    private PFichaMatriz fichaMatriz;

    /**
     * Predio asociado a la ficha matriz seleccionada
     */
    public PPredio predioSeleccionado;

    public PUnidadConstruccion unidadConstruccionSeleccionada;

    /**
     * Usuario autenticado.
     */
    private UsuarioDTO usuario;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * Lista de departamentos
     */
    private List<SelectItem> departamentos;

    /**
     * Lista de municipios
     */
    private List<SelectItem> municipios;

    /**
     * lista en la que se guardan las PFotos que se deben eliminar porque fueron reemplazadas al
     * modificar una unidad de construcción
     */
    private List<PFoto> listaPFotosABorrar;

    /**
     * Determina si se permite la creacion de una ficha
     */
    public boolean activarCreacion;

    /**
     * Variable usada para almacenar las direcciones del ppredio
     */
    private List<PPredioDireccion> predioSeleccionadoDirecciones;

    /**
     * Variable usada para almacenar la dirección para edicion
     */
    private PPredioDireccion direccionSeleccionada;

    /**
     * Variable usada como validadora de direcciones, cuando se quiere eliminar la ultima dirección
     * o cuando se quiere eliminar la dirección principal.
     */
    private boolean validacionDireccionBool;
    /**
     * Variable booleana usada al momento de visualizar los datos de una construcción como solo
     * lectura o en su edición.
     */
    private boolean datosConstruccionSoloLectura;

    /**
     * Variables para el manejo de las conversiones de áreas de terreno en la ficha matriz.
     */
    private Integer areaTotalTerrenoComunHa;
    private Double areaTotalTerrenoComunM2;
    private Integer areaTotalTerrenoPrivadaHa;
    private Double areaTotalTerrenoPrivadaM2;
    private Integer areaTotalTerrenoHa;
    private Double areaTotalTerrenoM2;
    private Double areaTotalConstruidaComunM2;
    private Double areaTotalConstruidaPrivadaM2;
    private Double areaTotalConstruidaM2;

    /**
     * Variable que almacena en nuevo numero de registro de matricula.
     */
    private String numeroRegistroInfoMatriculaNueva;

    /**
     * Lista de circulos registrales del municipio.
     */
    private List<SelectItem> itemListCirculosRegistrales = new ArrayList<SelectItem>();

    /**
     * Variable que almacena el codigo del circulo registral seleccionado al momento de un registro
     * de matrícula nueva.
     */
    private String circuloRegistralSeleccionado;

    /**
     * Variable usada para distinguir si una direccion es nueva o esta siendo modificada
     */
    private Boolean editModeBool = false;
    /**
     * Variable usada para guardar la version normalizada de una direccion nueva
     */
    private String direccionNormalizada;
    /**
     * Variable usada para guardar una direccion nueva
     */
    private String direccion;

    /**
     * variable que guarda el código postal de una dirección nueva
     */
    private String codigoPostalDireccion;

    /**
     * Variable usada para saber si se acepta la version normalizada de una direccion nueva
     */
    private Boolean normalizadaBool;
    /**
     * Variable usada para saber si una direccion nueva es principal
     */
    private Boolean principalBool;

    /**
     * Variable que almacena una referencia cartografica seleccionada.
     */
    private PReferenciaCartografica referenciaCartograficaSeleccionada;

    /**
     * Variable PPredioZona usada para almacenar las zonas homogéneas del PPredio
     */
    private List<PPredioZona> zonasHomogeneas;

    /**
     * Valor del Avaluo total del terreno Comun.
     */
    private Double avaluoTotalTerrenoComun;

    /**
     * PFichaMatrizTorre usada para edición, eliminación y multiplicación de torres.
     */
    private PFichaMatrizTorre fichaMatrizTorreSeleccionada;

    /**
     * lista con los items del combo "descripción construccion"
     */
    private ArrayList<SelectItem> descripcionConstruccionAnexoItemList;

    /**
     * PFichaMatrizTorre usada para hacer una replica de la PFichaMatrizTorre seleccionada.
     */
    private PFichaMatrizTorre fichaMatrizTorreReplica;
    /**
     * Variable usada para llevar un seguimiento de los numeros prediales asignados por torre y por
     * piso
     */
    private int numerosPredialesGeneradosPorPiso[][];

    /**
     * Variable usada para llevar un seguimiento de los numeros prediales asignados por torre y por
     * sotano
     */
    private int numerosPredialesGeneradosPorSotano[][];

    /**
     * Variables para controlar las validaciones de unidades en la generación de números prediales
     */
    private Long unidadesTotales;
    private Long unidadesRestantes;
    private Long unidadesAsignadas;

    /**
     * Variable usada como contador para las unidades faltantes al momento de generar los números
     * prediales
     */
    private Long conteoUnidad;

    /**
     * Variable usada para almacenar los números prediales generados en una condición de propiedad
     * condominio.
     */
    private int numerosPredialesGeneradosCondominio[];

    /**
     * Variable booleana para saber si se han seleccionado todos los predios generados en la ficha
     * matriz.
     */
    private boolean selectedAllBool;
    /**
     * Variable usada para almacenar los predios seleccionados en la ficha matriz para su edición.
     */
    private PFichaMatrizPredio[] fichaMatrizPrediosSeleccionados;

    /**
     * Variable usada para editar el coeficiente de copropiedad de un registro de la tabla de
     * numeros prediales resultantes.
     */
    private Double coeficienteCopropiedad;

    /**
     * Variable usada para almacenar los predios (PfichaMatrizPredios) generados en la ficha matriz.
     */
    private List<PFichaMatrizPredio> fichaMatrizPrediosResultantes;

    /**
     * Variable que almacena la lista de circulos registrales.
     */
    private List<CirculoRegistral> listCirculosRegistrales;

    private boolean vistaCompletaUC;

    /**
     * Variable usada para saber si se van a actualizar las áreas de la ficha matriz.
     */
    private boolean actualizarAreasFichaMatrizBool;

    /**
     * Variables usadas para manejo de construcciones nuevas y vigentes.
     */
    private List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas;
    private PUnidadConstruccion[] unidadConstruccionConvencionalVigentesSeleccionadas;
    private PUnidadConstruccion[] unidadConstruccionNoConvencionalVigentesSeleccionadas;

    /**
     * uso de construcción seleccionado en el combo
     */
    private Long selectedUsoUnidadConstruccionId;

    /**
     * uso de construcción como objeto para obtener valores necesarios para otros campos
     */
    private UsoConstruccion selectedUsoUnidadConstruccion;

    /**
     * Zona agregada por el usuario
     */
    private PPredioZona zonaSeleccionada;

    /**
     * bandera que dice si la unidad de construcción que se está creando o modificando es un anexo
     */
    private boolean isAnexoCurrentOrNewUnidadConstruccion;

    /**
     * Variable booleana que indica si la unidad del modelo de construcción que se está creando o
     * modificando es un anexo.
     */
    private boolean isAnexoONuevaUnidadDelModelo;

    /**
     * Arbol donde se muestran los componentes de un modelo de construcción.
     */
    private TreeNode treeComponentesConstruccionParaModelo;

    /**
     * los tres árboles que se arman para los tipos de calificación de la unidad de construcción
     * Hubo que manejar 3 aparte y que se creen al principio para que no se generen errores por
     * updates
     */
    private TreeNode treeComponentesConstruccion1;
    private TreeNode treeComponentesConstruccion2;
    private TreeNode treeComponentesConstruccion3;

    /**
     * árbol que se arma por defecto
     */
    private TreeNode treeComponentesConstruccion;

    /**
     * Variable para almacenar la PFmModeloConstruccion seleccionada.
     */
    private PFmModeloConstruccion unidadDelModeloDeConstruccionSeleccionada;
    /**
     * Variable que almacena el modelo de unidad de contrucción seleccionado.
     */
    private PFichaMatrizModelo modeloUnidadDeConstruccionSeleccionado;

    /**
     * Variable {@link Boolean} para habilitar un mensaje de validación cuando se haya seleccionado
     * una {@link }
     */
    private boolean validarCalificacionComponenteBool;

    /**
     * Dimensión para las construcciones no convencionales
     */
    private Double areaNoConvencional;

    /**
     * calificacion anexo seleccionado
     */
    private CalificacionAnexo selectedCalificacionAnexo;

    /**
     * lo que en la interfaz se llama "tipo", pero en realidad corresponde a los puntos de la
     * calificación anexo
     */
    private int tipoDescripcionConstruccion;
    /**
     * Lista en la que se guardan los PFotos que se deben insertar en la BD al guardar la unidad de
     * construcción
     */
    private List<PFoto> pFotosList;
    /**
     * lista con todos los items del combo de usos de construcción
     */
    private List<SelectItem> usosUnidadConstruccionItemList;

    /**
     * lista con todos los posibles usos de construcción (tabla USO_CONSTRUCCION)
     */
    private List<VUsoConstruccionZona> usosConstruccionList;

    /**
     * Variable usada para almacenar el id de la calificacionAnexo tanto de unidades de construcción
     * como de unidades del modelo de construcción.
     */
    private Long calificacionAnexoIdSelected;
    /**
     * identifica si la unidad de construcción que se va a guardar es nueva
     */
    private boolean esNuevaUnidadConstruccion;

    /**
     * Puntaje de la construcción al iniciar la edición
     */
    private int puntajeAnterior;

    /**
     * ruta web temporal de la foto del anexo nuevo o modificado
     */
    private String rutaWebTemporalFotoAnexo;

    /**
     * contiene la ruta de la imagen por defecto cuando no existen fotografias asociadas
     */
    private String rutaWebImagenBase = "/imagenesBase/uploadIcon.png";

    /**
     * Lista de elementos calificación construcción.
     */
    private List<CalificacionConstruccion> listaElementosCalificacionConstruccion;

    /**
     * determina visualizar boton aceptar
     */
    private boolean banderaBotonAceptarCargarFoto;
    /**
     * determina si existe una imagen para visualizar
     */
    private boolean existeImagen;

    /**
     * Contiene los datos de la PFoto
     */
    private PFoto currentPFoto;

    /**
     * rutas temporales (local y web) de la foto que se carga
     */
    private String rutaTemporalFoto, rutaWebTemporalFoto;
    /**
     * determina visualizar boton aceptar
     */
    private boolean errorConfirmarDatosCargarFotografia;

    private String mensajeFotos;

    private CalificacionConstruccionTreeNode currentCalificacionConstruccionNode;

    /**
     * ruta local de la fotografía del anexo - o construcción no convencional- que se adiciona o se
     * modifica
     */
    private String rutaLocalFotoAnexo;

    /**
     * Año vigencia de la zona
     */
    private int anioZona;

    /**
     * Variable que indica el número de torres a multiplicar
     */
    private Integer numeroTorresMultiplicar;

    /**
     * Determina si la toree esta en modo edicion
     */
    private boolean editarTorre;

    // ---------------Getters and Setters----------------------//
    public List<PFichaMatriz> getListaFichaMatriz() {
        return listaFichaMatriz;
    }

    public void setListaFichaMatriz(List<PFichaMatriz> listaFichaMatriz) {
        this.listaFichaMatriz = listaFichaMatriz;
    }

    public boolean isEditarTorre() {
        return editarTorre;
    }

    public void setEditarTorre(boolean editarTorre) {
        this.editarTorre = editarTorre;
    }

    public int getAnioZona() {
        return anioZona;
    }

    public void setAnioZona(int anioZona) {
        this.anioZona = anioZona;
    }

    public PPredioZona getZonaSeleccionada() {
        return zonaSeleccionada;
    }

    public void setZonaSeleccionada(PPredioZona zonaSeleccionada) {
        this.zonaSeleccionada = zonaSeleccionada;
    }

    public Integer getNumeroTorresMultiplicar() {
        return numeroTorresMultiplicar;
    }

    public void setNumeroTorresMultiplicar(Integer numeroTorresMultiplicar) {
        this.numeroTorresMultiplicar = numeroTorresMultiplicar;
    }

    public String getRutaLocalFotoAnexo() {
        return rutaLocalFotoAnexo;
    }

    public void setRutaLocalFotoAnexo(String rutaLocalFotoAnexo) {
        this.rutaLocalFotoAnexo = rutaLocalFotoAnexo;
    }

    public boolean isBanderaBotonAceptarCargarFoto() {
        return banderaBotonAceptarCargarFoto;
    }

    public CalificacionConstruccionTreeNode getCurrentCalificacionConstruccionNode() {
        return currentCalificacionConstruccionNode;
    }

    public void setCurrentCalificacionConstruccionNode(
        CalificacionConstruccionTreeNode currentCalificacionConstruccionNode) {
        this.currentCalificacionConstruccionNode = currentCalificacionConstruccionNode;
    }

    public void setBanderaBotonAceptarCargarFoto(boolean banderaBotonAceptarCargarFoto) {
        this.banderaBotonAceptarCargarFoto = banderaBotonAceptarCargarFoto;
    }

    public boolean isExisteImagen() {
        return existeImagen;
    }

    public void setExisteImagen(boolean existeImagen) {
        this.existeImagen = existeImagen;
    }

    public PFoto getCurrentPFoto() {
        return currentPFoto;
    }

    public void setCurrentPFoto(PFoto currentPFoto) {
        this.currentPFoto = currentPFoto;
    }

    public String getRutaTemporalFoto() {
        return rutaTemporalFoto;
    }

    public void setRutaTemporalFoto(String rutaTemporalFoto) {
        this.rutaTemporalFoto = rutaTemporalFoto;
    }

    public String getRutaWebTemporalFoto() {
        return rutaWebTemporalFoto;
    }

    public void setRutaWebTemporalFoto(String rutaWebTemporalFoto) {
        this.rutaWebTemporalFoto = rutaWebTemporalFoto;
    }

    public boolean isErrorConfirmarDatosCargarFotografia() {
        return errorConfirmarDatosCargarFotografia;
    }

    public void setErrorConfirmarDatosCargarFotografia(boolean errorConfirmarDatosCargarFotografia) {
        this.errorConfirmarDatosCargarFotografia = errorConfirmarDatosCargarFotografia;
    }

    public String getMensajeFotos() {
        return mensajeFotos;
    }

    public void setMensajeFotos(String mensajeFotos) {
        this.mensajeFotos = mensajeFotos;
    }

    public boolean isDatosConstruccionSoloLectura() {
        return datosConstruccionSoloLectura;
    }

    public void setDatosConstruccionSoloLectura(boolean datosConstruccionSoloLectura) {
        this.datosConstruccionSoloLectura = datosConstruccionSoloLectura;
    }

    public List<CalificacionConstruccion> getListaElementosCalificacionConstruccion() {
        return listaElementosCalificacionConstruccion;
    }

    public void setListaElementosCalificacionConstruccion(
        List<CalificacionConstruccion> listaElementosCalificacionConstruccion) {
        this.listaElementosCalificacionConstruccion = listaElementosCalificacionConstruccion;
    }

    public List<VUsoConstruccionZona> getUsosConstruccionList() {
        return usosConstruccionList;
    }

    public void setUsosConstruccionList(List<VUsoConstruccionZona> usosConstruccionList) {
        this.usosConstruccionList = usosConstruccionList;
    }

    public Long getCalificacionAnexoIdSelected() {
        return calificacionAnexoIdSelected;
    }

    public void setCalificacionAnexoIdSelected(Long calificacionAnexoIdSelected) {
        this.calificacionAnexoIdSelected = calificacionAnexoIdSelected;
    }

    public boolean isEsNuevaUnidadConstruccion() {
        return esNuevaUnidadConstruccion;
    }

    public void setEsNuevaUnidadConstruccion(boolean esNuevaUnidadConstruccion) {
        this.esNuevaUnidadConstruccion = esNuevaUnidadConstruccion;
    }

    public int getPuntajeAnterior() {
        return puntajeAnterior;
    }

    public void setPuntajeAnterior(int puntajeAnterior) {
        this.puntajeAnterior = puntajeAnterior;
    }

    public String getRutaWebTemporalFotoAnexo() {
        return rutaWebTemporalFotoAnexo;
    }

    public void setRutaWebTemporalFotoAnexo(String rutaWebTemporalFotoAnexo) {
        this.rutaWebTemporalFotoAnexo = rutaWebTemporalFotoAnexo;
    }

    public String getRutaWebImagenBase() {
        return rutaWebImagenBase;
    }

    public void setRutaWebImagenBase(String rutaWebImagenBase) {
        this.rutaWebImagenBase = rutaWebImagenBase;
    }

    public boolean isValidarCalificacionComponenteBool() {
        return validarCalificacionComponenteBool;
    }

    public void setValidarCalificacionComponenteBool(boolean validarCalificacionComponenteBool) {
        this.validarCalificacionComponenteBool = validarCalificacionComponenteBool;
    }

    public Double getAreaNoConvencional() {
        return areaNoConvencional;
    }

    public void setAreaNoConvencional(Double areaNoConvencional) {
        this.areaNoConvencional = areaNoConvencional;
    }

    public ArrayList<SelectItem> getDescripcionConstruccionAnexoItemList() {
        return descripcionConstruccionAnexoItemList;
    }

    public void setDescripcionConstruccionAnexoItemList(
        ArrayList<SelectItem> descripcionConstruccionAnexoItemList) {
        this.descripcionConstruccionAnexoItemList = descripcionConstruccionAnexoItemList;
    }

    /**
     * es el getter del this.selectedCalificacionAnexo, pero además, arma la lista de items para el
     * combo. Se hizo aquí porque del modo en que está armada la página hacía primero este getter
     * que el de la lista de items del combo descripcionConstruccionAnexoItemList. Antes se armaba
     * en el getter de la lista, pero el selectedCalificacionAnexo quedaba con el valor anterior
     */
    public CalificacionAnexo getSelectedCalificacionAnexo() {

        List<CalificacionAnexo> calificacionAnexoList;
        SelectItem comboItem;
        boolean isFirst, useFirst;
        Long unidadConstruccionCalificacionAnexoId = -1l;

        this.descripcionConstruccionAnexoItemList = new ArrayList<SelectItem>();
        isFirst = true;
        useFirst = false;

        calificacionAnexoList = this.getConservacionService()
            .obtenerCalificacionesAnexoPorIdUsoConstruccion(
                this.selectedUsoUnidadConstruccionId);

        // El dato calificacionAnexoIdSelected es igual a this.unidadConstruccionSeleccionada.getCalificacionAnexoId()
        // o para el caso de modelos de construcción a this.unidadDelModeloDeConstruccionSeleccionada.getCalificacionAnexoId()
        // D: se supone que si es 'anexo' debería tener este dato, pero por si
        // acaso...
        if (this.calificacionAnexoIdSelected != null) {
            unidadConstruccionCalificacionAnexoId = this.calificacionAnexoIdSelected;
        } else {
            useFirst = true;
        }

        for (CalificacionAnexo ca : calificacionAnexoList) {
            comboItem = new SelectItem(ca, ca.getDescripcion());
            this.descripcionConstruccionAnexoItemList.add(comboItem);

            if (useFirst && isFirst) {
                this.selectedCalificacionAnexo = ca;
                isFirst = false;
            } else if (!useFirst) {
                if (ca.getId().longValue() == unidadConstruccionCalificacionAnexoId
                    .longValue()) {
                    this.selectedCalificacionAnexo = ca;
                }
            }
        }
        return this.selectedCalificacionAnexo;
    }

    public void setSelectedCalificacionAnexo(CalificacionAnexo selectedCalificacionAnexo) {
        this.selectedCalificacionAnexo = selectedCalificacionAnexo;
    }

    /**
     * Éste dato se obtiene del CalificacionAnexo seleccionado
     *
     * @return
     */
    public int getTipoDescripcionConstruccion() {
        this.tipoDescripcionConstruccion = (int) getSelectedCalificacionAnexo()
            .getPuntos();
        return this.tipoDescripcionConstruccion;
    }

    public void setTipoDescripcionConstruccion(int tipoDescripcionConstruccion) {
        this.tipoDescripcionConstruccion = tipoDescripcionConstruccion;
    }

    public List<PFoto> getpFotosList() {
        return pFotosList;
    }

    public void setpFotosList(List<PFoto> pFotosList) {
        this.pFotosList = pFotosList;
    }

    public List<SelectItem> getUsosUnidadConstruccionItemList() {
        return usosUnidadConstruccionItemList;
    }

    /**
     * OJO: el maldito jsf acepta armar SelectItem con cualquier clase de objeto, pero al hacer la
     * selección, NO FUNCIONA!!. Por lo tanto hay que armarlo con tipo Java simple y hacer doble
     * trabajo de búsqueda.
     *
     * modified by javier.aponte: Se concatena el id del uso de la unidad de construcción con el
     * nombre en el label del SelectItem
     *
     * @author pedro.garcia
     * @modified javier.aponte
     * @modified felipe.cadena - se filtra el tipo de uso de unidad para rectificaciones y
     * complemetaciones
     * @param usosConstruccionList
     */
    public void setUsosUnidadConstruccionItemList(
        List<VUsoConstruccionZona> usosConstruccionList) {

        for (VUsoConstruccionZona uc : usosConstruccionList) {
            this.usosUnidadConstruccionItemList.add(new SelectItem(uc.getId(),
                uc.getId() + "-" + uc.getNombre()));

        }

    }

    public boolean isActualizarAreasFichaMatrizBool() {
        return actualizarAreasFichaMatrizBool;
    }

    public void setActualizarAreasFichaMatrizBool(boolean actualizarAreasFichaMatrizBool) {
        this.actualizarAreasFichaMatrizBool = actualizarAreasFichaMatrizBool;
    }

    public PFichaMatrizModelo getModeloUnidadDeConstruccionSeleccionado() {
        return modeloUnidadDeConstruccionSeleccionado;
    }

    public void setModeloUnidadDeConstruccionSeleccionado(
        PFichaMatrizModelo modeloUnidadDeConstruccionSeleccionado) {
        this.modeloUnidadDeConstruccionSeleccionado = modeloUnidadDeConstruccionSeleccionado;
    }

    public List<PFoto> getListaPFotosABorrar() {
        return listaPFotosABorrar;
    }

    public void setListaPFotosABorrar(List<PFoto> listaPFotosABorrar) {
        this.listaPFotosABorrar = listaPFotosABorrar;
    }

    public List<CirculoRegistral> getListCirculosRegistrales() {
        return listCirculosRegistrales;
    }

    public void setListCirculosRegistrales(List<CirculoRegistral> listCirculosRegistrales) {
        this.listCirculosRegistrales = listCirculosRegistrales;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalVigentes() {
        return unidadConstruccionConvencionalVigentes;
    }

    public void setUnidadConstruccionConvencionalVigentes(
        List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes) {
        this.unidadConstruccionConvencionalVigentes = unidadConstruccionConvencionalVigentes;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalVigentes() {
        return unidadConstruccionNoConvencionalVigentes;
    }

    public void setUnidadConstruccionNoConvencionalVigentes(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes) {
        this.unidadConstruccionNoConvencionalVigentes = unidadConstruccionNoConvencionalVigentes;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevas() {
        return unidadConstruccionConvencionalNuevas;
    }

    public void setUnidadConstruccionConvencionalNuevas(
        List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas) {
        this.unidadConstruccionConvencionalNuevas = unidadConstruccionConvencionalNuevas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevas() {
        return unidadConstruccionNoConvencionalNuevas;
    }

    public void setUnidadConstruccionNoConvencionalNuevas(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas) {
        this.unidadConstruccionNoConvencionalNuevas = unidadConstruccionNoConvencionalNuevas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevasNoCanceladas() {
        return unidadConstruccionConvencionalNuevasNoCanceladas;
    }

    public void setUnidadConstruccionConvencionalNuevasNoCanceladas(
        List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas) {
        this.unidadConstruccionConvencionalNuevasNoCanceladas =
            unidadConstruccionConvencionalNuevasNoCanceladas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevasNoCanceladas() {
        return unidadConstruccionNoConvencionalNuevasNoCanceladas;
    }

    public void setUnidadConstruccionNoConvencionalNuevasNoCanceladas(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas) {
        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            unidadConstruccionNoConvencionalNuevasNoCanceladas;
    }

    public PUnidadConstruccion[] getUnidadConstruccionConvencionalVigentesSeleccionadas() {
        return unidadConstruccionConvencionalVigentesSeleccionadas;
    }

    public void setUnidadConstruccionConvencionalVigentesSeleccionadas(
        PUnidadConstruccion[] unidadConstruccionConvencionalVigentesSeleccionadas) {
        this.unidadConstruccionConvencionalVigentesSeleccionadas =
            unidadConstruccionConvencionalVigentesSeleccionadas;
    }

    public PUnidadConstruccion[] getUnidadConstruccionNoConvencionalVigentesSeleccionadas() {
        return unidadConstruccionNoConvencionalVigentesSeleccionadas;
    }

    public void setUnidadConstruccionNoConvencionalVigentesSeleccionadas(
        PUnidadConstruccion[] unidadConstruccionNoConvencionalVigentesSeleccionadas) {
        this.unidadConstruccionNoConvencionalVigentesSeleccionadas =
            unidadConstruccionNoConvencionalVigentesSeleccionadas;
    }

    public List<PFichaMatrizPredio> getFichaMatrizPrediosResultantes() {
        return fichaMatrizPrediosResultantes;
    }

    public void setFichaMatrizPrediosResultantes(
        List<PFichaMatrizPredio> fichaMatrizPrediosResultantes) {
        this.fichaMatrizPrediosResultantes = fichaMatrizPrediosResultantes;
    }

    public PFichaMatriz getFichaMatriz() {
        return fichaMatriz;
    }

    public void setFichaMatriz(PFichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public List<SelectItem> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<SelectItem> departamentos) {
        this.departamentos = departamentos;
    }

    public List<SelectItem> getMunicipios() {

        return municipios;
    }

    public void setMunicipios(List<SelectItem> municipios) {
        this.municipios = municipios;
    }

    public boolean isActivarCreacion() {
        return activarCreacion;
    }

    public void setActivarCreacion(boolean activarCreacion) {
        this.activarCreacion = activarCreacion;
    }

    public PPredio getPredioSeleccionado() {
        return predioSeleccionado;
    }

    public void setPredioSeleccionado(PPredio predioSeleccionado) {
        this.predioSeleccionado = predioSeleccionado;
    }

    public List<PPredioDireccion> getPredioSeleccionadoDirecciones() {
        return predioSeleccionadoDirecciones;
    }

    public void setPredioSeleccionadoDirecciones(
        List<PPredioDireccion> predioSeleccionadoDirecciones) {
        this.predioSeleccionadoDirecciones = predioSeleccionadoDirecciones;
    }

    public boolean isValidacionDireccionBool() {
        return validacionDireccionBool;
    }

    public void setValidacionDireccionBool(boolean validacionDireccionBool) {
        this.validacionDireccionBool = validacionDireccionBool;
    }

    public PPredioDireccion getDireccionSeleccionada() {
        return direccionSeleccionada;
    }

    public void setDireccionSeleccionada(PPredioDireccion direccionSeleccionada) {
        this.direccionSeleccionada = direccionSeleccionada;
    }

    public Integer getAreaTotalTerrenoComunHa() {
        return areaTotalTerrenoComunHa;
    }

    public void setAreaTotalTerrenoComunHa(Integer areaTotalTerrenoComunHa) {
        this.areaTotalTerrenoComunHa = areaTotalTerrenoComunHa;
    }

    public Double getAreaTotalTerrenoComunM2() {
        return areaTotalTerrenoComunM2;
    }

    public void setAreaTotalTerrenoComunM2(Double areaTotalTerrenoComunM2) {
        this.areaTotalTerrenoComunM2 = areaTotalTerrenoComunM2;
    }

    public Integer getAreaTotalTerrenoPrivadaHa() {
        return areaTotalTerrenoPrivadaHa;
    }

    public void setAreaTotalTerrenoPrivadaHa(Integer areaTotalTerrenoPrivadaHa) {
        this.areaTotalTerrenoPrivadaHa = areaTotalTerrenoPrivadaHa;
    }

    public Double getAreaTotalTerrenoPrivadaM2() {
        return areaTotalTerrenoPrivadaM2;
    }

    public void setAreaTotalTerrenoPrivadaM2(Double areaTotalTerrenoPrivadaM2) {
        this.areaTotalTerrenoPrivadaM2 = areaTotalTerrenoPrivadaM2;
    }

    public Integer getAreaTotalTerrenoHa() {
        return areaTotalTerrenoHa;
    }

    public void setAreaTotalTerrenoHa(Integer areaTotalTerrenoHa) {
        this.areaTotalTerrenoHa = areaTotalTerrenoHa;
    }

    public Double getAreaTotalTerrenoM2() {
        return areaTotalTerrenoM2;
    }

    public void setAreaTotalTerrenoM2(Double areaTotalTerrenoM2) {
        this.areaTotalTerrenoM2 = areaTotalTerrenoM2;
    }

    public Double getAreaTotalConstruidaComunM2() {
        return areaTotalConstruidaComunM2;
    }

    public void setAreaTotalConstruidaComunM2(Double areaTotalConstruidaComunM2) {
        this.areaTotalConstruidaComunM2 = areaTotalConstruidaComunM2;
    }

    public Double getAreaTotalConstruidaPrivadaM2() {
        return areaTotalConstruidaPrivadaM2;
    }

    public void setAreaTotalConstruidaPrivadaM2(Double areaTotalConstruidaPrivadaM2) {
        this.areaTotalConstruidaPrivadaM2 = areaTotalConstruidaPrivadaM2;
    }

    public Double getAreaTotalConstruidaM2() {
        return areaTotalConstruidaM2;
    }

    public void setAreaTotalConstruidaM2(Double areaTotalConstruidaM2) {
        this.areaTotalConstruidaM2 = areaTotalConstruidaM2;
    }

    public String getNumeroRegistroInfoMatriculaNueva() {
        return numeroRegistroInfoMatriculaNueva;
    }

    public void setNumeroRegistroInfoMatriculaNueva(String numeroRegistroInfoMatriculaNueva) {
        this.numeroRegistroInfoMatriculaNueva = numeroRegistroInfoMatriculaNueva;
    }

    public List<SelectItem> getItemListCirculosRegistrales() {
        return itemListCirculosRegistrales;
    }

    public void setItemListCirculosRegistrales(List<SelectItem> itemListCirculosRegistrales) {
        this.itemListCirculosRegistrales = itemListCirculosRegistrales;
    }

    public String getCirculoRegistralSeleccionado() {
        return circuloRegistralSeleccionado;
    }

    public void setCirculoRegistralSeleccionado(String circuloRegistralSeleccionado) {
        this.circuloRegistralSeleccionado = circuloRegistralSeleccionado;
    }

    public Boolean isEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(Boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public String getDireccionNormalizada() {
        return direccionNormalizada;
    }

    public void setDireccionNormalizada(String direccionNormalizada) {
        this.direccionNormalizada = direccionNormalizada;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostalDireccion() {
        return codigoPostalDireccion;
    }

    public void setCodigoPostalDireccion(String codigoPostalDireccion) {
        this.codigoPostalDireccion = codigoPostalDireccion;
    }

    public Boolean isNormalizadaBool() {
        return normalizadaBool;
    }

    public void setNormalizadaBool(Boolean normalizadaBool) {
        this.normalizadaBool = normalizadaBool;
    }

    public Boolean isPrincipalBool() {
        return principalBool;
    }

    public void setPrincipalBool(Boolean principalBool) {
        this.principalBool = principalBool;
    }

    public PReferenciaCartografica getReferenciaCartograficaSeleccionada() {
        return referenciaCartograficaSeleccionada;
    }

    public void setReferenciaCartograficaSeleccionada(
        PReferenciaCartografica referenciaCartograficaSeleccionada) {
        this.referenciaCartograficaSeleccionada = referenciaCartograficaSeleccionada;
    }

    public List<PPredioZona> getZonasHomogeneas() {
        return zonasHomogeneas;
    }

    public void setZonasHomogeneas(List<PPredioZona> zonasHomogeneas) {
        this.zonasHomogeneas = zonasHomogeneas;
    }

    public Double getAvaluoTotalTerrenoComun() {
        return avaluoTotalTerrenoComun;
    }

    public void setAvaluoTotalTerrenoComun(Double avaluoTotalTerrenoComun) {
        this.avaluoTotalTerrenoComun = avaluoTotalTerrenoComun;
    }

    public PFichaMatrizTorre getFichaMatrizTorreSeleccionada() {
        return fichaMatrizTorreSeleccionada;
    }

    public void setFichaMatrizTorreSeleccionada(PFichaMatrizTorre fichaMatrizTorreSeleccionada) {
        this.fichaMatrizTorreSeleccionada = fichaMatrizTorreSeleccionada;
    }

    public PFichaMatrizTorre getFichaMatrizTorreReplica() {
        return fichaMatrizTorreReplica;
    }

    public void setFichaMatrizTorreReplica(PFichaMatrizTorre fichaMatrizTorreReplica) {
        this.fichaMatrizTorreReplica = fichaMatrizTorreReplica;
    }

    public int[][] getNumerosPredialesGeneradosPorPiso() {
        return numerosPredialesGeneradosPorPiso;
    }

    public void setNumerosPredialesGeneradosPorPiso(int[][] numerosPredialesGeneradosPorPiso) {
        this.numerosPredialesGeneradosPorPiso = numerosPredialesGeneradosPorPiso;
    }

    public int[][] getNumerosPredialesGeneradosPorSotano() {
        return numerosPredialesGeneradosPorSotano;
    }

    public void setNumerosPredialesGeneradosPorSotano(int[][] numerosPredialesGeneradosPorSotano) {
        this.numerosPredialesGeneradosPorSotano = numerosPredialesGeneradosPorSotano;
    }

    public Long getUnidadesTotales() {
        return unidadesTotales;
    }

    public void setUnidadesTotales(Long unidadesTotales) {
        this.unidadesTotales = unidadesTotales;
    }

    public Long getUnidadesRestantes() {
        return unidadesRestantes;
    }

    public void setUnidadesRestantes(Long unidadesRestantes) {
        this.unidadesRestantes = unidadesRestantes;
    }

    public Long getUnidadesAsignadas() {
        return unidadesAsignadas;
    }

    public void setUnidadesAsignadas(Long unidadesAsignadas) {
        this.unidadesAsignadas = unidadesAsignadas;
    }

    public Long getConteoUnidad() {
        return conteoUnidad;
    }

    public void setConteoUnidad(Long conteoUnidad) {
        this.conteoUnidad = conteoUnidad;
    }

    public int[] getNumerosPredialesGeneradosCondominio() {
        return numerosPredialesGeneradosCondominio;
    }

    public void setNumerosPredialesGeneradosCondominio(int[] numerosPredialesGeneradosCondominio) {
        this.numerosPredialesGeneradosCondominio = numerosPredialesGeneradosCondominio;
    }

    public boolean isSelectedAllBool() {
        return selectedAllBool;
    }

    public void setSelectedAllBool(boolean selectedAllBool) {
        this.selectedAllBool = selectedAllBool;
    }

    public PFichaMatrizPredio[] getFichaMatrizPrediosSeleccionados() {
        return fichaMatrizPrediosSeleccionados;
    }

    public void setFichaMatrizPrediosSeleccionados(
        PFichaMatrizPredio[] fichaMatrizPrediosSeleccionados) {
        this.fichaMatrizPrediosSeleccionados = fichaMatrizPrediosSeleccionados;
    }

    public Double getCoeficienteCopropiedad() {
        return coeficienteCopropiedad;
    }

    public void setCoeficienteCopropiedad(Double coeficienteCopropiedad) {
        this.coeficienteCopropiedad = coeficienteCopropiedad;
    }

    public boolean isVistaCompletaUC() {
        return vistaCompletaUC;
    }

    public void setVistaCompletaUC(boolean vistaCompletaUC) {
        this.vistaCompletaUC = vistaCompletaUC;
    }

    public PUnidadConstruccion getUnidadConstruccionSeleccionada() {
        return unidadConstruccionSeleccionada;
    }

    public void setUnidadConstruccionSeleccionada(PUnidadConstruccion unidadConstruccionSeleccionada) {
        this.unidadConstruccionSeleccionada = unidadConstruccionSeleccionada;
    }

    public Long getSelectedUsoUnidadConstruccionId() {
        return selectedUsoUnidadConstruccionId;
    }

    public void setSelectedUsoUnidadConstruccionId(Long selectedUsoUnidadConstruccionId) {
        this.selectedUsoUnidadConstruccionId = selectedUsoUnidadConstruccionId;
    }

    public UsoConstruccion getSelectedUsoUnidadConstruccion() {
        return selectedUsoUnidadConstruccion;
    }

    public void setSelectedUsoUnidadConstruccion(UsoConstruccion selectedUsoUnidadConstruccion) {
        this.selectedUsoUnidadConstruccion = selectedUsoUnidadConstruccion;
    }

    public boolean isIsAnexoCurrentOrNewUnidadConstruccion() {
        return isAnexoCurrentOrNewUnidadConstruccion;
    }

    public void setIsAnexoCurrentOrNewUnidadConstruccion(
        boolean isAnexoCurrentOrNewUnidadConstruccion) {
        this.isAnexoCurrentOrNewUnidadConstruccion = isAnexoCurrentOrNewUnidadConstruccion;
    }

    public boolean isIsAnexoONuevaUnidadDelModelo() {
        return isAnexoONuevaUnidadDelModelo;
    }

    public void setIsAnexoONuevaUnidadDelModelo(boolean isAnexoONuevaUnidadDelModelo) {
        this.isAnexoONuevaUnidadDelModelo = isAnexoONuevaUnidadDelModelo;
    }

    public TreeNode getTreeComponentesConstruccionParaModelo() {
        return treeComponentesConstruccionParaModelo;
    }

    public void setTreeComponentesConstruccionParaModelo(
        TreeNode treeComponentesConstruccionParaModelo) {
        this.treeComponentesConstruccionParaModelo = treeComponentesConstruccionParaModelo;
    }

    public TreeNode getTreeComponentesConstruccion1() {
        return treeComponentesConstruccion1;
    }

    public void setTreeComponentesConstruccion1(TreeNode treeComponentesConstruccion1) {
        this.treeComponentesConstruccion1 = treeComponentesConstruccion1;
    }

    public TreeNode getTreeComponentesConstruccion2() {
        return treeComponentesConstruccion2;
    }

    public void setTreeComponentesConstruccion2(TreeNode treeComponentesConstruccion2) {
        this.treeComponentesConstruccion2 = treeComponentesConstruccion2;
    }

    public TreeNode getTreeComponentesConstruccion3() {
        return treeComponentesConstruccion3;
    }

    public void setTreeComponentesConstruccion3(TreeNode treeComponentesConstruccion3) {
        this.treeComponentesConstruccion3 = treeComponentesConstruccion3;
    }

    public TreeNode getTreeComponentesConstruccion() {
        return treeComponentesConstruccion;
    }

    public void setTreeComponentesConstruccion(TreeNode treeComponentesConstruccion) {
        this.treeComponentesConstruccion = treeComponentesConstruccion;
    }

    public PFmModeloConstruccion getUnidadDelModeloDeConstruccionSeleccionada() {
        return unidadDelModeloDeConstruccionSeleccionada;
    }

    public void setUnidadDelModeloDeConstruccionSeleccionada(
        PFmModeloConstruccion unidadDelModeloDeConstruccionSeleccionada) {
        this.unidadDelModeloDeConstruccionSeleccionada = unidadDelModeloDeConstruccionSeleccionada;
    }

    // ---------------Metodos----------------------//
    @PostConstruct
    public void init() {

        LOGGER.debug("on CargaFichaMatrizMB#init ");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.datosConstruccionSoloLectura = true;
        this.direccionSeleccionada = new PPredioDireccion();

        inicializaDeptos();
        this.activarCreacion = false;

        // se cargan las fichas asociadas al usuario.
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.listaFichaMatriz = this.getConservacionService().
            buscarFichasEnProcesoDeCarga(this.usuario.getLogin());
        this.direccionSeleccionada = new PPredioDireccion();

        this.vistaCompletaUC = true;

    }

    /**
     * Método para usar los deptos para la busqueda de ficha
     *
     * @author felipe.cadena
     */
    public void inicializaDeptos() {

        this.municipios = new ArrayList<SelectItem>();
        this.departamentos = new ArrayList<SelectItem>();

        this.departamentos.add(new SelectItem(null, "Seleccionar ... "));
        this.municipios.add(new SelectItem(null, "Seleccionar ... "));

        List<Departamento> deptos = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
            this.usuario.getCodigoEstructuraOrganizacional());

        for (Departamento departamento : deptos) {
            this.departamentos.add(new SelectItem(departamento.getCodigo(),
                departamento.getNombre()));
        }
    }

    /**
     * Método para usar los municipios para la busqueda de ficha
     *
     * @author felipe.cadena
     */
    public void inicializaMunicipios() {
        List<Municipio> muns = this.getGeneralesService().getCacheMunicipiosByDepartamento(
            this.datosConsultaPredio.getDepartamentoId());
        this.datosConsultaPredio.setNumeroPredialS1("");
        this.datosConsultaPredio.setNumeroPredialS2("");
        this.datosConsultaPredio.setMunicipioId(null);
        this.inicializarNumeroPredial();

        this.municipios = new ArrayList<SelectItem>();
        this.municipios.add(new SelectItem(null, "Seleccionar ... "));
        for (Municipio municipio : muns) {
            this.municipios.add(new SelectItem(municipio.getCodigo(),
                municipio.getNombre()));
        }
    }

    /**
     * Inicializa las dos primeras posiciones del numero predial dependiendo del departamento y
     * municipios seleccionados
     *
     *
     * @author felipe.cadena
     */
    public void inicializarNumeroPredial() {

        LOGGER.debug("Departamento " + this.datosConsultaPredio.getDepartamentoId());
        LOGGER.debug("Municipio " + this.datosConsultaPredio.getMunicipioId());

        if (this.datosConsultaPredio.getDepartamentoId() == null) {
            this.datosConsultaPredio.setNumeroPredialS1("");
        } else {
            this.datosConsultaPredio.
                setNumeroPredialS1(this.datosConsultaPredio.getDepartamentoId());
        }

        if (this.datosConsultaPredio.getMunicipioId() == null) {
            this.datosConsultaPredio.setNumeroPredialS2("");
        } else {
            this.datosConsultaPredio.setNumeroPredialS2(this.datosConsultaPredio.getMunicipioId().
                substring(2));
        }

    }

    // --------------------------------------------------- //
    /**
     * Método que realiza una validaciones sobre las {@link PPredioDireccion} del {@link PPredio}
     * para no mostrar en pantalla las que se encuentran en estado cancelado.
     *
     * @author felipe.cadena
     */
    public void actualizarListaDeDirecciones() {

        this.validacionDireccionBool = false;
        this.predioSeleccionadoDirecciones = new ArrayList<PPredioDireccion>();
        if (this.predioSeleccionado != null &&
             this.predioSeleccionado.getPPredioDireccions() != null &&
             !this.predioSeleccionado.getPPredioDireccions().isEmpty()) {
            for (PPredioDireccion ppd : this.predioSeleccionado
                .getPPredioDireccions()) {
                if (ppd.getCancelaInscribe() == null ||
                     !ppd.getCancelaInscribe().equals(
                        EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    if (!this.predioSeleccionadoDirecciones.contains(ppd)) {
                        this.predioSeleccionadoDirecciones.add(ppd);
                    }
                }
            }
        }
        if (this.predioSeleccionadoDirecciones.size() == 1) {
            this.setValidacionDireccionBool(true);
        }
    }

    /**
     * Metodo para eliminar una ficha en proceso seleccionada
     *
     * @author felipe.cadena
     */
    public void eliminarFicha() {

        int numeroErrores = 0;
        List l;

        this.predioSeleccionado = this.getConservacionService().
            obtenerPPredioDatosFichaMatriz(this.fichaMatriz.getPPredio().getId());
        Object[] result = this.getConservacionService().reversarProyeccionFichaMigrada(
            this.predioSeleccionado.getId());

        if (result != null) {
            for (Object o : result) {
                l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!((Object[]) obje)[0].equals("0")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + ((Object[]) obje)[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al eliminar la ficha");

                    }
                }
            }
        }
        this.init();
    }

    /**
     * Realiza las validaciones necesarias antes de aplicar la ficha cargada
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarFichaCargada() {
        boolean valido = true;
        int nUnidadesRegistradas;
        Long nUnidades;
        RequestContext context = RequestContext.getCurrentInstance();

        //validar zonas
        if (this.zonasHomogeneas != null && !this.zonasHomogeneas.isEmpty()) {
            int anioVigencia = 0;
            int anioVigenciaAux;
            for (PPredioZona zona : this.zonasHomogeneas) {
                if (zona.getCancelaInscribe() != null &&
                     zona.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo())) {
                    anioVigenciaAux = zona.getVigencia().getYear() + 1900;
                    if (anioVigencia != 0 && anioVigencia != anioVigenciaAux) {
                        this.addMensajeError(
                            "Las zonas insertadas debe ser del mismo año de vigencia.");
                        context.addCallbackParam("error", "error");
                        return false;
                    }
                    anioVigencia = anioVigenciaAux;
                }
            }
        }

        //validar coeficiente de coopropiedad
        if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
            double sumaCoeficientes = 0d;
            for (PFichaMatrizPredio predio : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (!(predio.getCoeficiente() > 0d)) {
                    this.addMensajeError(
                        "Los coeficientes de copropiedad deben ser mayores de cero.");
                    context.addCallbackParam("error", "error");
                    return false;
                }
                sumaCoeficientes += predio.getCoeficiente();

            }

            if (sumaCoeficientes > 1 + 0.000000001 || sumaCoeficientes < 1 - 0.000000001) {
                this.addMensajeError(
                    "Los coeficientes de copropiedad deben sumar exactamente 1 suma ." +
                    sumaCoeficientes);
                context.addCallbackParam("error", "error");
                return false;
            }
        }

        //validadcion de unidades fichamatriz
        if (this.predioSeleccionado.isCondominio()) {
            nUnidadesRegistradas = this.fichaMatriz.getPFichaMatrizPredios().size();
            nUnidades = this.fichaMatriz.getTotalUnidadesPrivadas();

            if (nUnidades > nUnidadesRegistradas) {
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La cantidad de unidades supera el total de unidades registradas : " +
                    nUnidadesRegistradas);
                this.fichaMatriz.setTotalUnidadesPrivadas(0L);
                return false;

            } else if (nUnidades < nUnidadesRegistradas) {
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La cantidad de unidades es menor al total de unidades registradas : " +
                    nUnidadesRegistradas);
                this.fichaMatriz.setTotalUnidadesPrivadas(0L);
                return false;
            }
        } else {

            nUnidadesRegistradas = this.fichaMatriz.getPFichaMatrizPredios().size();
            nUnidades = ((this.fichaMatriz.getTotalUnidadesPrivadas() + this.fichaMatriz.
                getTotalUnidadesSotanos()));

            if (nUnidades > nUnidadesRegistradas) {
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La cantidad de unidades supera el total de unidades registradas : " +
                    nUnidadesRegistradas);
                return false;

            } else if (nUnidades < nUnidadesRegistradas) {
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La cantidad de unidades es menor al total de unidades registradas : " +
                    nUnidadesRegistradas);
                return false;
            }
        }

        return valido;
    }

    /**
     * Metodo para aplicar los cambios hachos en la ficha proyectada
     *
     * @author felipe.cadena
     */
    public void aplicarFichaProyectada() {

        int numeroErrores = 0;
        List l;

        //confirmar zonas
        if (this.zonasHomogeneas != null && !this.zonasHomogeneas.isEmpty()) {
            for (PPredioZona zona : this.zonasHomogeneas) {
                if (zona.getCancelaInscribe() != null &&
                     zona.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo())) {
                    zona.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    zona = this.getConservacionService().guardarActualizarZona(zona);
                }
            }
        }
        // Se calculan y guardan los avaluos relacionados a la ficha migrada.
        this.guardarAvaluosFicha();

        //se aplica la ficha       
        Object[] result = this.getConservacionService().confirmarProyeccionFichaMigrada(
            this.predioSeleccionado.getId());

        if (result != null) {
            for (Object o : result) {
                l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!((Object[]) obje)[0].equals("0")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + ((Object[]) obje)[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al aplicar ficha");

                    }
                }
            }
        }
        this.init();
        this.addMensajeWarn("TEST");
    }

    /**
     * Genera la plantilla inicial de una ficha matriz
     *
     * @author felipe.cadena
     */
    public void generarFicha() {

        int numeroErrores = 0;
        List l;
        Object[] result = this.getConservacionService().
            proyectarFichaMigrada(this.datosConsultaPredio.getNumeroPredial(),
                new Date(),
                this.usuario.getLogin());

        if (result != null) {
            for (Object o : result) {
                l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!((Object[]) obje)[0].equals("0")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + ((Object[]) obje)[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al generar la ficha");
                        return;

                    }
                }
            }
        }

        this.fichaMatriz = this.getConservacionService().buscarFichaPorNumeroPredialProyectada(
            this.datosConsultaPredio.getNumeroPredial());
        this.fichaMatriz.setTotalUnidadesPrivadas(0L);
        this.fichaMatriz = this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);
        this.predioSeleccionado = this.getConservacionService().
            obtenerPPredioDatosFichaMatriz(this.fichaMatriz.getPPredio().getId());

        this.zonasHomogeneas = this.inicializarZonas(this.predioSeleccionado.getPPredioZonas());

        //se calculan los avaluos de la zonas.
        try {
            this.actualizarAvaluosZonas();
        } catch (Exception e) {
            LOGGER.error("ERROR: " + e.getMessage());
            this.addMensajeError(
                "No fue posible realizar la liquidación de los avalúos, por favor verifique con el administrador.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            // No fue posible realizar la liquidación de los avalúos debido a
            // que no se pudo obtener en base de datos los valores de unidad de
            // terreno, por tal razón se debe reversar la proyección
            this.eliminarFicha();
            return;
        }

        this.init();
    }

    /**
     * Carga los datos de la ficha cuando se selecciona
     *
     * @author felipe.cadena
     */
    public void cargarFichaSeleccionada() {
        this.predioSeleccionado = this.getConservacionService().
            obtenerPPredioDatosFichaMatriz(this.fichaMatriz.getPPredio().getId());

        this.predioSeleccionado.setTramite(null);

        this.unidadConstruccionNoConvencionalNuevasNoCanceladas = this.predioSeleccionado.
            getPUnidadesConstruccionNoConvencionalNuevas();
        this.unidadConstruccionConvencionalNuevasNoCanceladas = this.predioSeleccionado.
            getPUnidadesConstruccionConvencionalNuevas();
        this.predioSeleccionadoDirecciones = this.predioSeleccionado.getPPredioDireccions();
        this.zonasHomogeneas = this.inicializarZonas(this.predioSeleccionado.getPPredioZonas());
        this.zonaSeleccionada = new PPredioZona();
        this.actualizarAreas();
        this.calcularAvaluoTerrenoComun();
    }

    /**
     * Carga las zonas de la ultima vigencia asociada el predio de la ficha matriz
     *
     * @author felipe.cadena
     * @param zonasBase
     * @return
     */
    public List<PPredioZona> inicializarZonas(List<PPredioZona> zonasBase) {
        List<PPredioZona> zonasFinales = new LinkedList<PPredioZona>();

        if (zonasBase == null || zonasBase.isEmpty()) {
            Calendar c = Calendar.getInstance();
            this.anioZona = c.get(Calendar.YEAR);
            return zonasFinales;
        }

        this.anioZona = zonasBase.get(0).getVigencia().getYear();
        for (PPredioZona pPredioZona : zonasBase) {
            if (pPredioZona.getVigencia().getYear() > this.anioZona) {
                this.anioZona = pPredioZona.getVigencia().getYear();
            }
        }

        for (PPredioZona pPredioZona : zonasBase) {
            if (pPredioZona.getVigencia().getYear() == this.anioZona) {
                zonasFinales.add(pPredioZona);
            }
        }

        this.anioZona += 1900;
        return zonasFinales;
    }

    /**
     * Determina si el predio tiene ficha predial.
     *
     * @author felipe.cadena
     */
    public void verificarExistenciaFicha() {

        PFichaMatriz pfm;
        FichaMatriz fm;
        List<Predio> prediosFicha;

        this.datosConsultaPredio.assembleNumeroPredialFromSegments();

        //validacion campos numero predial
        if (this.datosConsultaPredio.getNumeroPredial().length() < 22) {
            this.activarCreacion = false;
            this.addMensajeError("Debe ingresar el número completo");
            return;
        }

        //validacion condicion de propiedad
        if (!this.datosConsultaPredio.getNumeroPredialS9().equals(EPredioCondicionPropiedad.CP_8.
            getCodigo()) &&
             !this.datosConsultaPredio.getNumeroPredialS9().equals(EPredioCondicionPropiedad.CP_9.
                getCodigo())) {
            this.activarCreacion = false;
            this.addMensajeError("La condición de propiedad debe ser 8 o 9");
            return;
        }

        //validacion existencia de predios
        prediosFicha = this.getConservacionService().buscarPrediosPorRaizNumPredial(
            this.datosConsultaPredio.getNumeroPredial());

        if (prediosFicha == null || prediosFicha.isEmpty()) {
            this.activarCreacion = false;
            this.addMensajeError("No existen predios para este numero predial");
            return;
        }

        //validacion ficha existente proyectada
        pfm = this.getConservacionService().
            buscarFichaPorNumeroPredialProyectada(this.datosConsultaPredio.getNumeroPredial());

        if (pfm != null) {
            this.activarCreacion = false;
            this.addMensajeError("Ya existe una ficha matriz en proceso para este número predial");
            return;
        }

        // validacion ficha en firme
        fm = this.getConservacionService().obtenerFichaMatrizOrigen(this.datosConsultaPredio.
            getNumeroPredial());
        if (fm == null) {
            this.activarCreacion = true;
        } else {
            this.activarCreacion = false;
            this.addMensajeError("Ya existe una ficha matriz para este predio");
        }

    }

    // --------------------------------------------------- //
    /**
     * Método que carga los datos de una matrícula inmobiliaria para su modificación.
     *
     * @author david.cifuentes
     */
    public void cargarMatricula() {

        // Se buscan los circulos registrales filtrados por municipio
        this.listCirculosRegistrales = new ArrayList<CirculoRegistral>();
        if (this.predioSeleccionado != null) {
            String codigoMunicipio = this.predioSeleccionado.getMunicipio()
                .getCodigo();
            this.listCirculosRegistrales = this.getGeneralesService()
                .getCacheCirculosRegistralesPorMunicipio(
                    codigoMunicipio);
        }
        if (!this.listCirculosRegistrales.isEmpty()) {
            SelectItem nombre;
            for (CirculoRegistral circulo : this.listCirculosRegistrales) {
                nombre = new SelectItem();
                nombre.setValue(circulo.getCodigo());
                nombre.setLabel(circulo.getNombre());
                this.itemListCirculosRegistrales.add(nombre);
            }
        }

        if (this.predioSeleccionado.getNumeroRegistro() != null) {
            this.numeroRegistroInfoMatriculaNueva = this.predioSeleccionado
                .getNumeroRegistro();
        } else {
            this.numeroRegistroInfoMatriculaNueva = "";
        }

        // Set del circulo registral
        if (this.predioSeleccionado.getCirculoRegistral() != null) {
            for (SelectItem i : this.itemListCirculosRegistrales) {
                if (this.predioSeleccionado.getCirculoRegistral().getCodigo() != null &&
                     this.predioSeleccionado.getCirculoRegistral()
                        .getCodigo().equals(i.getValue())) {
                    this.circuloRegistralSeleccionado = this.predioSeleccionado
                        .getCirculoRegistral().getCodigo();
                }
            }
        }
    }

    // --------------------------------------------------- //
    /**
     * Método usado para reiniciar algunas variables cuando se usa la pantalla de edición de
     * direcciones y referencias.
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 13-08-2013 cambio de nombre del método porque no corresponde con el
     * estándar
     */
    public void entrarAModoEdicionDyR() {
        // Direcciones
        if (this.direccionSeleccionada != null && this.editModeBool) {
            this.direccion = this.direccionSeleccionada.getDireccion();
            this.normalizadaBool = false;
            this.direccionNormalizada = "";
            this.codigoPostalDireccion = this.direccionSeleccionada.getCodigoPostal();
            this.principalBool = this.direccionSeleccionada.getPrincipal().equals(
                ESiNo.SI.getCodigo());
        }
        // Referencias
        if (this.referenciaCartograficaSeleccionada == null ||
             !this.editModeBool) {
            this.referenciaCartograficaSeleccionada = new PReferenciaCartografica();
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida que la torre seleccionada sea la última de las torres creadas. Tambien es
     * usado para saber si la torre permite ser eliminada si no se ha creado el primer
     * pFichaMatrizPredio.
     *
     * @author david.cifuentes
     */
    public boolean validarUltimaTorreCreada(PFichaMatrizTorre torreSeleccionada) {

        boolean valido = false;

        // Si no hay predios creados, no importa el número de la torre, pues
        // estas se reenumeran.
        if (this.fichaMatriz.getPFichaMatrizPredios() == null ||
             this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {
            valido = true;
        } else {
            // Validar que sea la última torre.
            Long numeroTorre = torreSeleccionada.getTorre();
            Long mayor = 0L;
            for (PFichaMatrizTorre pfmt : this.fichaMatriz
                .getPFichaMatrizTorres()) {
                if (Long.valueOf(pfmt.getTorre()) > Long.valueOf(mayor)) {
                    mayor = pfmt.getTorre();
                }
            }
            if (Long.valueOf(mayor) == Long.valueOf(numeroTorre)) {
                valido = true;
            }
        }
        return valido;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza el proceso de eliminación de una torre, reubicando los números de las
     * torres.
     *
     * @author fabio.navarrete
     */
    public void eliminarTorre() {
        try {

            // && !(validarExistenPrediosCreadosEnTorre(this.fichaMatrizTorreSeleccionada)
            //			|| validarExistenSotanosCreadosEnTorre(this.fichaMatrizTorreSeleccionada)
            if (this.validarUltimaTorreCreada(this.fichaMatrizTorreSeleccionada)) {

                // Actualiza el total de unidades privadas en la ficha matriz
                this.fichaMatriz.setTotalUnidadesPrivadas(this.fichaMatriz
                    .getTotalUnidadesPrivadas() -
                     this.fichaMatrizTorreSeleccionada.getUnidades());

                // Actualiza el total de unidades privadas en la ficha matriz
                this.fichaMatriz.setTotalUnidadesSotanos(this.fichaMatriz
                    .getTotalUnidadesSotanos() -
                     this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos());

                this.fichaMatriz.getPFichaMatrizTorres().remove(
                    this.fichaMatrizTorreSeleccionada);
                Long numTorreSeleccionada = this.fichaMatrizTorreSeleccionada
                    .getTorre();

                this.getConservacionService().eliminarPFichaMatrizTorre(
                    this.fichaMatrizTorreSeleccionada);

                for (PFichaMatrizTorre pfmt : this.fichaMatriz
                    .getPFichaMatrizTorres()) {
                    if (pfmt.getTorre().compareTo(numTorreSeleccionada) > 0) {
                        pfmt.setTorre(pfmt.getTorre() - 1);
                    }
                }
                this.fichaMatriz.setPFichaMatrizTorres(this
                    .getConservacionService()
                    .guardarActualizarPFichaMatrizTorres(
                        this.fichaMatriz.getPFichaMatrizTorres()));

                this.guardarFichaMatriz();
                this.addMensajeInfo("Se eliminó la torre satisfactoriamente.");

            } else {
                this.addMensajeError(
                    "Debido a que ya se han creado predios en las torres, sólo se puede eliminar la última torre creada, siempre y cuando no se hayan creado predios para ésta.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que prepara la inserción de una construcción convencional nueva.
     *
     * @author fabio.navarrete
     */
    public void setupAdicionarFichaMatrizTorre() {
        this.fichaMatrizTorreSeleccionada = new PFichaMatrizTorre();
        this.fichaMatrizTorreReplica = null;
        Long numTorre = Long.valueOf(0);
        for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {
            if (pfmt.getTorre().compareTo(numTorre) > 0) {
                numTorre = pfmt.getTorre();
            }
        }
        this.fichaMatrizTorreSeleccionada.setTorre(++numTorre);

        // Actualización de matriz de números prediales generados
        this.cargarMatrizNumerosPrediales();
    }

    /**
     * Método que se encarga de multiplicar una torre
     *
     * @author fabio.navarrete
     */
    public void multiplicarTorre() {
        try {
            if (numeroTorresMultiplicar != null && numeroTorresMultiplicar > 0) {
                List<PFichaMatrizTorre> PFichaMatrizTorres = new ArrayList<PFichaMatrizTorre>();

                Long numTorre = Long.valueOf(0);
                for (PFichaMatrizTorre pfmt : this.fichaMatriz
                    .getPFichaMatrizTorres()) {
                    if (pfmt.getTorre().compareTo(numTorre) > 0) {
                        numTorre = pfmt.getTorre();
                    }
                }

                for (int i = 0; i < this.numeroTorresMultiplicar; i++) {
                    numTorre++;
                    PFichaMatrizTorre pfmt = new PFichaMatrizTorre(
                        this.fichaMatrizTorreSeleccionada);
                    pfmt.setId(null);
                    pfmt.setTorre(numTorre);
                    pfmt.setUsuarioLog(MenuMB.getMenu().getUsuarioDto()
                        .getLogin());
                    pfmt.setFechaLog(new Date());
                    PFichaMatrizTorres.add(pfmt);
                }
                this.fichaMatriz.setTotalUnidadesPrivadas(this.fichaMatriz
                    .getTotalUnidadesPrivadas() +
                     this.fichaMatrizTorreSeleccionada.getUnidades() *
                     this.numeroTorresMultiplicar);

                this.fichaMatriz.setTotalUnidadesSotanos(this.fichaMatriz
                    .getTotalUnidadesSotanos() +
                     this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos() *
                     this.numeroTorresMultiplicar);

                PFichaMatrizTorres = this
                    .getConservacionService()
                    .guardarActualizarPFichaMatrizTorres(PFichaMatrizTorres);
                this.fichaMatriz.getPFichaMatrizTorres().addAll(
                    PFichaMatrizTorres);
                this.guardarFichaMatriz();
                this.addMensajeInfo("Torre multiplicada satisfactoriamente");

                // Inicialización de variables para la creación de los
                // números prediales
                this.unidadesTotales = 0L;
                this.unidadesAsignadas = 0L;
                this.unidadesRestantes = 0L;
                this.conteoUnidad = 0L;
                this.cargarMatrizNumerosPrediales();
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Por favor ingrese la cantidad de torres a multiplicar.");
            }
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al multiplicar la torre");

        }
    }

    /**
     * Método que se llama en la ventana que modifica los datos de la torre. Se encarga de almacenar
     * los datos de la torre seleccionada
     *
     * @author fabio.navarrete
     */
    public void aceptarTorre() {

        int nUnidadesRegistradas;
        Long nUnidades;
        try {

            if (this.fichaMatrizTorreSeleccionada.getUnidades() == null) {
                this.fichaMatrizTorreSeleccionada.setUnidades(0L);
            }
            if (this.fichaMatrizTorreSeleccionada.getUnidadesSotanos() == null) {
                this.fichaMatrizTorreSeleccionada.setUnidadesSotanos(0L);
            }

            nUnidadesRegistradas = this.fichaMatriz.getPFichaMatrizPredios().size();
            nUnidades = ((this.fichaMatriz.getTotalUnidadesPrivadas() + this.fichaMatriz.
                getTotalUnidadesSotanos()) +
                 (this.fichaMatrizTorreSeleccionada.getUnidades() +
                this.fichaMatrizTorreSeleccionada.getUnidadesSotanos()));

            // refs:#14461 :: david.cifuentes :: Se coloca la torre como si
            // fuera original, de manera que se mantenga el mismo comportamiento
            // que una torre en firme en la actividad de modificación
            // alfanumérica :: 26/10/2015
            this.fichaMatrizTorreSeleccionada.setOriginalTramite(ESiNo.SI
                .getCodigo());

            this.editarTorre = true;

            if (nUnidades > nUnidadesRegistradas) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La cantidad de unidades supera el total de unidades registradas : " +
                    nUnidadesRegistradas);
                return;

            } else if (nUnidades < nUnidadesRegistradas) {
                this.addMensajeWarn(
                    "La cantidad de unidades es menor al total de unidades registradas : " +
                    nUnidadesRegistradas);
            }

            if (validarUnidadesFichaMatriz()) {

                boolean cambioValores = false;
                if (this.fichaMatrizTorreReplica == null) {
                    cambioValores = true;
                } else if (this.fichaMatrizTorreReplica != null &&
                    (Long.valueOf(this.fichaMatrizTorreSeleccionada.getPisos()) != Long
                    .valueOf(this.fichaMatrizTorreReplica.getPisos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getSotanos()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getSotanos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getUnidades()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getUnidades()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getUnidadesSotanos()))) {
                    cambioValores = true;
                }

                if (cambioValores) {
                    this.fichaMatrizTorreSeleccionada
                        .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo());
                    this.fichaMatrizTorreSeleccionada.setUsuarioLog(MenuMB
                        .getMenu().getUsuarioDto().getLogin());
                    this.fichaMatrizTorreSeleccionada.setFechaLog(new Date());
                    this.fichaMatrizTorreSeleccionada
                        .setPFichaMatriz(this.fichaMatriz);
                    this.fichaMatrizTorreSeleccionada = this
                        .getConservacionService()
                        .guardarActualizarPFichaMatrizTorre(
                            this.fichaMatrizTorreSeleccionada);
                    if (!this.fichaMatriz.getPFichaMatrizTorres().contains(
                        this.fichaMatrizTorreSeleccionada)) {
                        this.fichaMatriz.getPFichaMatrizTorres().add(
                            fichaMatrizTorreSeleccionada);
                    }

                    // Suma de unidades privadas y unidades de sotano de las torres
                    // de la ficha matriz
                    Long totalUnidadesPrivadas = 0L;
                    Long totalUnidadesSotanos = 0L;
                    for (PFichaMatrizTorre pft : this.fichaMatriz
                        .getPFichaMatrizTorres()) {
                        totalUnidadesPrivadas += pft.getUnidades();
                        totalUnidadesSotanos += pft.getUnidadesSotanos();
                    }
                    this.fichaMatriz
                        .setTotalUnidadesPrivadas(totalUnidadesPrivadas);
                    this.fichaMatriz.setTotalUnidadesSotanos(totalUnidadesSotanos);

                    // Guardar la ficha matriz
                    this.guardarFichaMatriz();

                    // Inicialización de variables para la creación de los
                    // números prediales
                    this.unidadesTotales = 0L;
                    this.unidadesAsignadas = 0L;
                    this.unidadesRestantes = 0L;
                    this.conteoUnidad = 0L;
                    this.cargarMatrizNumerosPrediales();
                    this.fichaMatrizTorreReplica = null;

                    this.addMensajeInfo("Datos de la torre almacenados satisfactoriamente");
                }
            } else {

                if (this.fichaMatrizTorreReplica != null) {
                    if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                         !this.fichaMatriz.getPFichaMatrizTorres()
                            .isEmpty()) {
                        PFichaMatrizTorre pfmtAuxRemove = new PFichaMatrizTorre();
                        for (PFichaMatrizTorre pfmt : this.fichaMatriz
                            .getPFichaMatrizTorres()) {
                            if (pfmt.getId().longValue() == this.fichaMatrizTorreReplica
                                .getId().longValue()) {
                                pfmtAuxRemove = pfmt;
                            }
                        }
                        this.fichaMatriz.getPFichaMatrizTorres().remove(
                            pfmtAuxRemove);
                        this.fichaMatriz.getPFichaMatrizTorres().add(
                            this.fichaMatrizTorreReplica);
                    }
                }

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al almacenar los datos de la torre");
        }
    }

    // --------------------------------------------------- //
    /**
     * Método que actualiza la {@link PPredioDireccion} de un {@link PPredio} marcada como
     * principal, deseleccionando la anterior.
     *
     * @author david.cifuentes
     */
    public void cambiarPrincipal() {
        if (this.direccionSeleccionada != null) {

            for (PPredioDireccion pd : this.predioSeleccionado
                .getPPredioDireccions()) {
                if (pd.getId().equals(this.direccionSeleccionada.getId())) {
                    pd.setPrincipal(ESiNo.SI.getCodigo());
                } else {
                    pd.setPrincipal(ESiNo.NO.getCodigo());
                }
            }
        }
        try {
            this.getConservacionService().actualizarPPredioDirecciones(
                this.predioSeleccionado.getPPredioDireccions());
            this.predioSeleccionadoDirecciones = this.predioSeleccionado.getPPredioDireccions();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(e);
        }
    }

    // --------------------------------------------------- //
    /**
     * Método que reinicia las variables usadas en la pantalla de gestión de ubicación del predio.
     *
     * @author david.cifuentes
     */
    public void reiniciarVariables() {
        this.direccion = "";
        this.principalBool = false;
        this.direccionNormalizada = "";
        this.normalizadaBool = false;
        this.editModeBool = false;
        this.referenciaCartograficaSeleccionada = new PReferenciaCartografica();
        if (this.direccionSeleccionada == null) {
            this.direccionSeleccionada = new PPredioDireccion();
        }
    }

    public String cerrar() {
        this.tareasPendientesMB.init();
        return "index";
    }

    /**
     * Método que acepta el total de unidades privadas para el condominio
     */
    public void aceptarUnidadesPrivadasCondominio() {

        boolean validateUnidades = validarUnidadesFichaMatriz();

        if (validateUnidades) {
            this.guardarFichaMatriz();
            // Inicialización de variables para la creación de los
            // números prediales
            this.unidadesTotales = 0L;
            this.unidadesAsignadas = 0L;
            this.unidadesRestantes = 0L;
            this.conteoUnidad = 0L;
            this.cargarMatrizNumerosPrediales();
            this.addMensajeInfo("Se guardaron las unidades satisfactoriamente");
        }
    }

    /**
     * Se encarga de guardar la ficha matriz que se encuentra en edición
     *
     * @author fabio.navarrete
     */
    public void guardarFichaMatriz() {
        try {
            this.fichaMatriz.setFechaLog(new Date());
            this.fichaMatriz.setUsuarioLog(MenuMB.getMenu().getUsuarioDto()
                .getLogin());
            this.fichaMatriz = this.getConservacionService()
                .guardarActualizarPFichaMatriz(this.fichaMatriz);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida la edición de unidades en la ficha matriz.
     *
     * @author david.cifuentes
     */
    public boolean validarUnidadesFichaMatriz() {
        boolean validate = false;

        if (this.predioSeleccionado.isEsPredioEnPH()) {

            validate = true;

            PFichaMatrizTorre torreSeleccionada = null;
            if (this.fichaMatrizTorreReplica == null) {
                torreSeleccionada = this.fichaMatrizTorreSeleccionada;
            } else {
                torreSeleccionada = this.fichaMatrizTorreReplica;
            }

            // Validaciones para PH
            if (torreSeleccionada.getPisos() == null) {
                torreSeleccionada.setPisos(0L);
            }
            if (torreSeleccionada.getSotanos() == null) {
                torreSeleccionada.setSotanos(0L);
            }
            if (torreSeleccionada.getUnidades() == null) {
                this.fichaMatrizTorreSeleccionada.setUnidades(0L);
            }
            if (torreSeleccionada.getUnidadesSotanos() == null) {
                torreSeleccionada.setUnidadesSotanos(0L);
            }

            // Si ya se han creado predios, para la torre seleccionada no se
            // puede realizar edición sobre las unidades.
            if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                 !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                // Se verifica si el valor que se quiere actualizar es el de las
                // unidades privadas o el de los sotanos, y posterior a ello se
                // valida que no hayan unidades creadas.
                if (verificarCambioValorUnidadesParaTorre(true)) {
                    if (validarExistenPrediosCreadosEnTorre(torreSeleccionada)) {
                        this.addMensajeError(
                            "No se puede realizar edición o eliminación sobre las unidades privadas de la torre seleccionada pues para ésta ya se han creado predios. Para editar o eliminar las unidades de la torre seleccionada por favor elimine sus predios asociados.");
                        validate = false;
                    }
                }

                if (verificarCambioValorUnidadesParaTorre(false)) {
                    if (validarExistenSotanosCreadosEnTorre(torreSeleccionada)) {
                        this.addMensajeError(
                            "No se puede realizar edición o eliminación sobre los sótanos de la torre seleccionada pues para ésta ya se han creado predios. Para editar o eliminar las unidades de la torre seleccionada por favor elimine sus predios asociados.");
                        validate = false;
                    }
                }
            }

            if (torreSeleccionada.getPisos() < 1 &&
                 torreSeleccionada.getSotanos() < 1) {
                this.addMensajeError(
                    "Por favor ingrese un valor para el total de pisos o sotanos de la torre.");
                validate = false;
            }

            if (torreSeleccionada.getPisos() > 0) {
                if (torreSeleccionada.getUnidades() < 1) {
                    this.
                        addMensajeError("Por favor ingrese un valor para las unidades de la torre.");
                    validate = false;
                }
            }

            if (torreSeleccionada.getSotanos() > 0) {
                if (torreSeleccionada.getUnidadesSotanos() < 1) {
                    this.addMensajeError("Por favor ingrese un valor para las unidades del sotano.");
                    validate = false;
                }
            }
        }

        return validate;
    }

    // ---------------------------------------------------- //
    /**
     * Método para verificar si se ha cambiado el valor de las unidades privadas al momento de
     * editar la torre. Si el parametro enviado es true, se realiza la validacion sobre los pisos y
     * unidades privadas de la torre, por el contrario si es false se realiza la validacion sobre
     * los sotanos y unidades de sotanos.
     *
     * @author david.cifuentes
     * @param pisoOSotano
     * @return
     */
    public boolean verificarCambioValorUnidadesParaTorre(boolean pisoOSotano) {
        boolean cambia = false;
        if (this.fichaMatrizTorreReplica != null &&
             this.fichaMatrizTorreSeleccionada != null) {

            if (pisoOSotano) {
                if (this.fichaMatrizTorreReplica.getPisos() != this.fichaMatrizTorreSeleccionada
                    .getPisos() ||
                     this.fichaMatrizTorreReplica.getUnidades() != this.fichaMatrizTorreSeleccionada
                    .getUnidades()) {
                    cambia = true;
                }
            } else {
                if (this.fichaMatrizTorreReplica.getSotanos() != this.fichaMatrizTorreSeleccionada
                    .getSotanos() ||
                     this.fichaMatrizTorreReplica.getUnidadesSotanos() !=
                    this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos()) {
                    cambia = true;
                }
            }
        }
        return cambia;
    }

    // ---------------------------------------------------- //
    /**
     * Método que verifica si se han creado predios para una torre seleccionadoa.
     *
     * @author david.cifuentes
     */
    public boolean validarExistenPrediosCreadosEnTorre(
        PFichaMatrizTorre torreSeleccionada) {
        boolean existen = false;

        if (torreSeleccionada != null) {
            // Validación de pisos de la torre
            for (int piso = 0; piso < torreSeleccionada.getPisos(); piso++) {
                if (this.numerosPredialesGeneradosPorPiso[(torreSeleccionada
                    .getTorre().intValue())][piso] > 0) {
                    existen = true;
                    break;
                }
            }
        }
        return existen;
    }

    // ---------------------------------------------------- //
    /**
     * Método que verifica si se han creado sotanos para una torre seleccionadoa.
     *
     * @author david.cifuentes
     */
    public boolean validarExistenSotanosCreadosEnTorre(
        PFichaMatrizTorre torreSeleccionada) {
        boolean existen = false;

        if (torreSeleccionada != null) {
            // Validación de sotanos de la torre
            for (int sotano = 0; sotano <= torreSeleccionada.getSotanos(); sotano++) {
                int lenSotanosActuales = this.numerosPredialesGeneradosPorSotano[torreSeleccionada
                    .getTorre().intValue()].length;
                if (lenSotanosActuales > torreSeleccionada.getSotanos() &&
                     this.numerosPredialesGeneradosPorSotano[torreSeleccionada
                        .getTorre().intValue()][sotano] > 0) {
                    existen = true;
                    break;
                }
            }
        }
        return existen;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que carga la matriz de números prediales utilizada para llevar un seguimiento de los
     * números prediales que se han generado,los que están por generar, y controlar las unidades que
     * se pueden eliminar.
     *
     * @author david.cifuentes
     */
    public void cargarMatrizNumerosPrediales() {

        int prediosCreados = 0;
        this.numerosPredialesGeneradosPorPiso = null;
        this.numerosPredialesGeneradosPorSotano = null;

        try {
            if (!this.predioSeleccionado.isEsPredioEnPH()) {

                // CONDOMINIO
                if (this.fichaMatriz.getTotalUnidadesPrivadas() != null &&
                     this.fichaMatriz.getTotalUnidadesPrivadas() > 0L) {

                    // Se crea el arreglo con el tamaño de la cantidad de
                    // unidades privadas que va a tener el condominio.
                    this.numerosPredialesGeneradosCondominio = new int[this.fichaMatriz.
                        getTotalUnidadesPrivadas()
                        .intValue() + 1];

                    // Se calcula el total de los predios creados.
                    if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                        prediosCreados = this.fichaMatriz
                            .getPFichaMatrizPredios().size();
                    }
                    // Se inicializan los campos del array correspondientes a
                    // los predios ya creados.
                    for (int i = 1; i <= prediosCreados; i++) {
                        this.numerosPredialesGeneradosCondominio[i] += 1;
                    }
                    this.conteoUnidad = Long.valueOf(prediosCreados);
                }

            } else if (this.predioSeleccionado.isEsPredioEnPH()) {

                // TIPO DE INSCRIPCIÓN - PH
                Long pisoMayor = 0L, sotanoMayor = 0L;
                Integer torre, pisoOSotano, esSotano;

                int torres = this.fichaMatriz.getTotalTorres();

                // Inicializar
                if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                     !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                    for (PFichaMatrizTorre pfmt : this.fichaMatriz
                        .getPFichaMatrizTorres()) {
                        if (pfmt.getUnidades() != null &&
                             pfmt.getUnidades() > pisoMayor) {
                            pisoMayor = pfmt.getUnidades();
                        }
                        if (pfmt.getUnidadesSotanos() != null &&
                             pfmt.getUnidadesSotanos() > sotanoMayor) {
                            sotanoMayor = pfmt.getUnidadesSotanos();
                        }
                    }
                }
                if (torres > 0 && pisoMayor > 0) {
                    this.numerosPredialesGeneradosPorPiso = new int[torres + 1][pisoMayor
                        .intValue() + 1];
                    this.numerosPredialesGeneradosPorSotano = new int[torres + 1][sotanoMayor
                        .intValue() + 1];
                }

                if (this.numerosPredialesGeneradosPorPiso != null ||
                     this.numerosPredialesGeneradosPorSotano != null) {
                    // Cargar Matriz
                    if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                         !this.fichaMatriz.getPFichaMatrizPredios()
                            .isEmpty()) {
                        for (PFichaMatrizPredio pfmp : this.fichaMatriz
                            .getPFichaMatrizPredios()) {
                            // torre
                            torre = Integer.valueOf(pfmp.getNumeroPredial()
                                .substring(22, 24));
                            // Piso
                            pisoOSotano = Integer.valueOf(pfmp
                                .getNumeroPredial().substring(24, 26));
                            // Si es o no sotano.
                            esSotano = Integer.valueOf(pfmp.getNumeroPredial()
                                .substring(24, 25));
                            if (esSotano.toString().equals("9")) {
                                int pisoOSotanoTranspuesto = 100 - pisoOSotano.intValue();
                                this.numerosPredialesGeneradosPorSotano[torre][pisoOSotanoTranspuesto] +=
                                    1;
                            } else {
                                this.numerosPredialesGeneradosPorPiso[torre][pisoOSotano] += 1;
                            }
                        }
                    }

                    LOGGER.debug(this
                        .imprimirMatriz(this.numerosPredialesGeneradosPorPiso));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            LOGGER.debug("Error al cargar los números prediales");
        }
    }

    /**
     * Método para imprimir en consola la matriz de números prediales generados.
     *
     * @author david.cifuentes
     */
    public String imprimirMatriz(int[][] a) {
        String matriz = "";
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                matriz += "+" + a[i][j];
            }
            matriz += "----";
        }
        return matriz;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que carga el coeficiente de copropiedad del PPredioSeleccionado.
     *
     * @author david.cifuentes
     */
    public void cargarCoeficiente() {
        //this.seleccionarPrediosPorCheck();

        if (this.fichaMatrizPrediosSeleccionados.length == 0) {
            this.addMensajeError("Debe seleccionar por lo menos un predio");
            return;
        }

        if (fichaMatrizPrediosSeleccionados != null &&
             fichaMatrizPrediosSeleccionados[0] != null &&
             fichaMatrizPrediosSeleccionados[0].getCoeficiente() != null) {
            this.coeficienteCopropiedad = fichaMatrizPrediosSeleccionados[0]
                .getCoeficiente();
        } else {
            this.coeficienteCopropiedad = 0.0D;
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de todos los predios generados en la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void seleccionarTodosPrediosGenerados() {
        this.fichaMatrizPrediosResultantes = this.fichaMatriz
            .getPFichaMatrizPredios();
        for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
            pfmp.setSelected(this.selectedAllBool);
        }
        if (this.selectedAllBool) {
            this.fichaMatrizPrediosSeleccionados = new PFichaMatrizPredio[this.fichaMatriz
                .getPFichaMatrizPredios().size()];
            for (int i = 0; i < this.fichaMatriz.getPFichaMatrizPredios()
                .size(); i++) {
                fichaMatrizPrediosSeleccionados[i] = this.fichaMatriz
                    .getPFichaMatrizPredios().get(i);
            }
        } else {
            this.fichaMatrizPrediosSeleccionados = null;
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado para realizar el cargue de los predios seleccionados mediante el checkBox.
     *
     * @author david.cifuentes
     */
    public void seleccionarPrediosPorCheck() {
        List<PFichaMatrizPredio> auxListaPredios = new ArrayList<PFichaMatrizPredio>();
        for (PFichaMatrizPredio pfmp : this.fichaMatriz
            .getPFichaMatrizPredios()) {
            if (pfmp.isSelected()) {
                auxListaPredios.add(pfmp);
            }
        }
        this.fichaMatrizPrediosSeleccionados = new PFichaMatrizPredio[auxListaPredios
            .size()];
        for (int i = 0; i < auxListaPredios.size(); i++) {
            fichaMatrizPrediosSeleccionados[i] = auxListaPredios.get(i);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de uno de los predios generados mediante la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void updateSelectedPrediosGenerados() {
        this.fichaMatrizPrediosResultantes = this.fichaMatriz
            .getPFichaMatrizPredios();
        this.seleccionarPrediosPorCheck();
    }

    // ------------------------------------------------------------- //
    /**
     * Método encargado de guardar el PPredio.
     *
     * @david.cifuentes
     */
    /*
     * @modified by leidy.gonzalez :: 12444:: 14-05-2015 Se ingresa que al actualizar el predio
     * seleccionado este tenga el valor que se le ingreso a la ficha matriz en el momento que fue
     * creada.
     */
    public void guardarPPredio() {

        try {
            this.predioSeleccionado.setTramite(this.fichaMatriz.getPPredio().getTramite());
            this.getConservacionService().actualizarPPredio(this.predioSeleccionado);

            this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(e);
        }
    }

    // --------------------------------------------------- //
    /**
     * Guarda la matrícula inmobiliaria actualizada
     *
     * @author david.cifuentes
     */
    public void guardarMatriculaInmobiliaria() {

        try {
            if (this.numeroRegistroInfoMatriculaNueva != null &&
                 !this.numeroRegistroInfoMatriculaNueva.trim().isEmpty() &&
                 this.circuloRegistralSeleccionado != null &&
                 !this.circuloRegistralSeleccionado.trim().isEmpty()) {

                if (this.predioSeleccionado.getNumeroRegistro() != null) {
                    this.predioSeleccionado
                        .setNumeroRegistroAnterior(this.predioSeleccionado
                            .getNumeroRegistro());
                }

                this.predioSeleccionado
                    .setNumeroRegistro(this.numeroRegistroInfoMatriculaNueva);

                // Set del circulo registral
                if (this.circuloRegistralSeleccionado != null) {
                    for (CirculoRegistral cr : listCirculosRegistrales) {
                        if (this.circuloRegistralSeleccionado.equals(cr
                            .getCodigo())) {
                            this.predioSeleccionado.setCirculoRegistral(cr);
                        }
                    }
                }
                this.getConservacionService().guardarProyeccion(this.usuario,
                    this.predioSeleccionado);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            LOGGER.debug("Error guardando la matricula inmobiliaria.");
        }
    }

    /**
     * @author felipe.cadena
     */
    public void guardarNombrePHCondominio() {
        // Guardar la proyección del predio seleccionado.
        this.getConservacionService().actualizarPPredio(
            this.predioSeleccionado);
    }

    // --------------------------------------------------- //
    /**
     * action del botón "generar" en la ventana en la que se adiciona una dirección para el predio.
     * Metodo que normaliza el valor de una dirección.
     *
     * @author david.cifuentes
     */
    public void normalizarDireccion() {
        this.direccionNormalizada = this.getConservacionService()
            .normalizarDireccion(this.direccion);
    }

    public void cerrarGestionDetallesUC() {

        this.listaPFotosABorrar = null;
    }

    // --------------------------------------------------- /
    /**
     * Método que realiza diferentes validaciones para una {@link PPredioDireccion} que se desea
     * actualizar.
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 13-08-2013 el método se convierte en privado. Se corrige validación
     * mal hecha sobre el código postal
     */
    private boolean validarActualizarDireccion() {
        boolean validate = true;

        //se valida el codigo postal
        RequestContext context = RequestContext.getCurrentInstance();
        if (this.codigoPostalDireccion != null && !this.codigoPostalDireccion.isEmpty()) {

            if (this.codigoPostalDireccion.length() < 6) {
                this.addMensajeError("El Código postal debe tener por lo menos 6 digitos");
                context.addCallbackParam("error", "error");
                return false;
            }
            String initCodigoPostal = this.codigoPostalDireccion.substring(0, 2);

            if (!this.predioSeleccionado.getDepartamento().getCodigo().equals(initCodigoPostal)) {
                this.addMensajeError(
                    "Código postal no corresponde con el código del departamento seleccionado");
                context.addCallbackParam("error", "error");
                return false;
            }

        }

        if (this.getDireccion() != null && !this.direccion.trim().isEmpty()) {

            // Validar que no se repita la dirección.
            if (this.predioSeleccionadoDirecciones != null &&
                 this.predioSeleccionadoDirecciones.size() > 0) {

                // Si es una nueva dirección verificar en las que están que no
                // se encuentre repetida
                if (!this.editModeBool) {
                    for (PPredioDireccion ppd : this.predioSeleccionadoDirecciones) {
                        if (this.direccion.equals(ppd.getDireccion())) {

                            this.addMensajeError(
                                "La dirección que se quiere ingresar ya se encuentra registrada, por favor verifiquela.");

                            context.addCallbackParam("error", "error");
                            this.reiniciarVariables();
                            validate = false;
                        }
                    }
                }
            }
        } else {
            this.addMensajeError("Ingrese un valor para la dirección.");
            context.addCallbackParam("error", "error");
            this.reiniciarVariables();
            validate = false;
        }
        return validate;
    }

    // --------------------------------------------------- /
    /**
     * Método que actualiza una lista de {@link PPredioDireccion} de un {@link PPredio}
     *
     * @author david.cifuentes
     */
    public void actualizarDireccion() {

        PPredioDireccion nuevaDireccion = new PPredioDireccion();

        if (validarActualizarDireccion()) {
            this.direccionSeleccionada.setPPredio(this.predioSeleccionado);

            // ACTUALIZACIÓN
            if (this.editModeBool && this.direccionNormalizada != null) {

                this.direccionSeleccionada.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
                    getCodigo());
                // Hago el set de la direccion dependiendo si se acepta o no la
                // normalizada.
                if (this.normalizadaBool) {
                    this.direccionSeleccionada
                        .setDireccion(this.direccionNormalizada);
                } else {
                    this.direccionSeleccionada
                        .setDireccion(this.direccion);
                }

                this.direccionSeleccionada.setCodigoPostal(this.codigoPostalDireccion);

                // Se selecciona a la direccion nueva como principal
                if (this.principalBool) {
                    for (PPredioDireccion pPredioDir : this.predioSeleccionado
                        .getPPredioDireccions()) {
                        if (pPredioDir.getPrincipal().equals(
                            ESiNo.SI.getCodigo())) {
                            pPredioDir.setPrincipal(ESiNo.NO.getCodigo());
                            this.getConservacionService().guardarProyeccion(
                                this.usuario, pPredioDir);
                        }
                    }
                    this.direccionSeleccionada.setPrincipal(ESiNo.SI
                        .getCodigo());
                } else {
                    this.direccionSeleccionada.setPrincipal(ESiNo.NO
                        .getCodigo());
                }

                this.direccionSeleccionada
                    .setPPredio(this.predioSeleccionado);
                this.direccionSeleccionada.setFechaLog(new Date(System
                    .currentTimeMillis()));
                this.direccionSeleccionada.setUsuarioLog(this.usuario
                    .getLogin());
            } else {

                // Hago el set de la direccion dependiendo si se acepta o no la
                // normalizada.
                if (this.normalizadaBool) {
                    nuevaDireccion.setDireccion(this.direccionNormalizada);
                } else {
                    nuevaDireccion.setDireccion(this.direccion);
                }

                if (this.codigoPostalDireccion != null) {
                    nuevaDireccion.setCodigoPostal(this.codigoPostalDireccion);
                }

                // Selecciono a la direccion nueva como principal
                if (this.principalBool) {
                    nuevaDireccion.setPrincipal(ESiNo.SI.getCodigo());
                    for (PPredioDireccion pPredioDir : this.predioSeleccionado
                        .getPPredioDireccions()) {
                        if (pPredioDir.getPrincipal().equals(
                            ESiNo.SI.getCodigo())) {
                            pPredioDir.setPrincipal(ESiNo.NO.getCodigo());
                            this.getConservacionService().guardarPPredioDireccion(nuevaDireccion);
                        }
                    }
                } else {
                    nuevaDireccion.setPrincipal(ESiNo.NO.getCodigo());
                }
                nuevaDireccion.setPPredio(this.predioSeleccionado);
                nuevaDireccion
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                nuevaDireccion
                    .setFechaLog(new Date(System.currentTimeMillis()));
                nuevaDireccion.setUsuarioLog(this.usuario.getLogin());

            }

            if (this.predioSeleccionadoDirecciones == null ||
                 this.predioSeleccionadoDirecciones.isEmpty()) {

                // Validar que si es la primera debe ser principal
                if (this.editModeBool) {
                    this.direccionSeleccionada.setPrincipal(ESiNo.SI
                        .getCodigo());
                } else {
                    nuevaDireccion.setPrincipal(ESiNo.SI.getCodigo());
                }
            }

            // Actualizacion de la direccion
            try {

                if (this.editModeBool) {
                    // Modificación de una PPredioDireccion a la lista de las
                    // PPredioDirecciones del predio.
                    this.getConservacionService().
                        guardarPPredioDireccion(this.direccionSeleccionada);
                } else {
                    // Adicion de la PPredioDireccion a la lista de las
                    // PPredioDirecciones del predio.

                    nuevaDireccion = this.getConservacionService().guardarPPredioDireccion(
                        nuevaDireccion);
                    this.predioSeleccionado.getPPredioDireccions().add(
                        nuevaDireccion);
                }
                this.actualizarListaDeDirecciones();
                this.reiniciarVariables();

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                this.addMensajeError("Error al guardar la dirección.");
            }
        }
    }

    // ---------------------------------------------------------------------------
    /**
     * Método que se encarga de actualizar el arbol de componentes mostrado en la pantalla cada vez
     * que se cambie el tipo de calificación de la unidad del modelo de construcción.
     *
     * @author david.cifuentes
     */
    public void actualizarArbolDeComponentes() {

        for (VUsoConstruccionZona uc : this.usosConstruccionList) {
            if (uc.getId().longValue() == selectedUsoUnidadConstruccionId
                .longValue()) {
                this.selectedUsoUnidadConstruccion = this.getGeneralesService()
                    .getCacheUsoConstruccionById(uc.getId());
                break;
            }
        }
        if (this.selectedUsoUnidadConstruccion != null) {
            if (this.unidadConstruccionSeleccionada != null) {
                this.unidadConstruccionSeleccionada
                    .setTipoCalificacion(EUnidadConstruccionTipoCalificacion
                        .getCodigoByNombre(this.selectedUsoUnidadConstruccion
                            .getDestinoEconomico()));
            }
            if (this.unidadDelModeloDeConstruccionSeleccionada != null) {
                this.unidadDelModeloDeConstruccionSeleccionada
                    .setTipoCalificacion(EUnidadConstruccionTipoCalificacion
                        .getCodigoByNombre(this.selectedUsoUnidadConstruccion
                            .getDestinoEconomico()));
            }
        }

        if (this.selectedUsoUnidadConstruccion != null &&
             this.selectedUsoUnidadConstruccion.getDestinoEconomico() != null) {

            if (!this.selectedUsoUnidadConstruccion.getDestinoEconomico()
                .equals(EUnidadConstruccionTipoCalificacion.ANEXO
                    .getCodigo())) {

                this.isAnexoCurrentOrNewUnidadConstruccion = false;

                if (this.selectedUsoUnidadConstruccion.getDestinoEconomico()
                    .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL
                        .getCodigo())) {

                    this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion2;
                    this.treeComponentesConstruccion = this.treeComponentesConstruccion2;

                } else if (this.selectedUsoUnidadConstruccion
                    .getDestinoEconomico().equals(
                        EUnidadConstruccionTipoCalificacion.INDUSTRIAL
                            .getCodigo())) {

                    this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion3;
                    this.treeComponentesConstruccion = this.treeComponentesConstruccion3;

                } else if (this.selectedUsoUnidadConstruccion
                    .getDestinoEconomico().equals(
                        EUnidadConstruccionTipoCalificacion.RESIDENCIAL
                            .getCodigo())) {

                    this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion1;
                    this.treeComponentesConstruccion = this.treeComponentesConstruccion1;

                }

                if (this.unidadConstruccionSeleccionada != null) {

                    this.unidadConstruccionSeleccionada.setAnioConstruccion(this.getAnoActual());
                }
                if (this.unidadDelModeloDeConstruccionSeleccionada != null) {
                    this.unidadDelModeloDeConstruccionSeleccionada.setAnioConstruccion(this.
                        getAnoActual());
                }

                /*
                 * Pone por defecto los mas altos valores
                 */
                for (int i = 0; i < this.treeComponentesConstruccion
                    .getChildCount(); i++) {
                    TreeNode tn = this.treeComponentesConstruccion
                        .getChildren().get(i);
                    for (int j = 0; j < tn.getChildCount(); j++) {
                        DefaultTreeNode dtn = (DefaultTreeNode) tn
                            .getChildren().get(j);
                        CalificacionConstruccionTreeNode cctn =
                            (CalificacionConstruccionTreeNode) dtn
                                .getData();

                        if (cctn.getDetallesCalificacionItemList().size() > 0) {
                            CalificacionConstruccion calificacionConstruccion =
                                new CalificacionConstruccion();
                            SelectItem si = cctn
                                .getDetallesCalificacionItemList()
                                .get(cctn.getDetallesCalificacionItemList()
                                    .size() - 1);
                            String[] objectAttributes = si
                                .getValue()
                                .toString()
                                .split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

                            calificacionConstruccion.setId(new Long(
                                objectAttributes[0]));
                            calificacionConstruccion
                                .setComponente(objectAttributes[1]);
                            calificacionConstruccion
                                .setElemento(objectAttributes[2]);
                            calificacionConstruccion
                                .setDetalleCalificacion(objectAttributes[3]);
                            calificacionConstruccion.setPuntos(new Integer(
                                objectAttributes[4]).intValue());

                            cctn.setSelectedDetalle(calificacionConstruccion);
                        }
                    }
                }
                /*
                 * Poner los componentes más altos para el modelo de construcción
                 */
                this.setupComponentesMayorValorUnidadDelModelo();
            } else {
                //this.unidadConstruccionSeleccionada.setAnioConstruccion(null);
                this.isAnexoCurrentOrNewUnidadConstruccion = true;
            }
        }

    }

    /**
     * Listener del fileupload que permite cargar imágenes en la gestión de unidades de
     * construcción. (metodo validador de carga de imagenes)
     *
     * @author juan.agudelo
     */
    public void imagenUpload(FileUploadEvent eventoCarga) {

        File f1;
        IDocumentosService servicioDocumental;

        DocumentoVO documentoVo;

        //D: se carga el archivo en la carpeta temporal del sistema
        f1 = this.copiarArchivoUploaded(eventoCarga, false);
        this.rutaTemporalFoto = UtilidadesWeb.obtenerRutaTemporalArchivos() + separadorCarpeta + f1.
            getName();

        //D: se carga el archivo en la ruta web de previsualización
        servicioDocumental = DocumentalServiceFactory.getService();
        documentoVo = servicioDocumental.cargarDocumentoWorkspacePreview(this.rutaTemporalFoto);
        if (documentoVo != null) {
            this.rutaWebTemporalFoto = documentoVo.getUrlPublico();
        }

        this.existeImagen = true;
    }

    // ---------------------------------------------------------- //
    /**
     * Copia el archivo seleccionado en una carpeta temporal
     *
     * @param eventoCarga
     * @param toWebTemp indica que el archivo se va a cargar en la ruta web de imágenes temporales
     * @return
     */
    /*
     * modified pedro.garcia 07-05-2013 se usa método de UtilidadesWeb para carga del archivo
     */
    private File copiarArchivoUploaded(FileUploadEvent eventoCarga,
        boolean toWebTemp) {

        File archivoTemporal = null;
        Object[] cargaArchivo;

        cargaArchivo = new Object[3];
        if (!toWebTemp) {
            cargaArchivo = UtilidadesWeb.cargarArchivoATemp(eventoCarga, true);
        }

        if (cargaArchivo[0] == null) {
            LOGGER.error("ERROR: " + cargaArchivo[2]);
            this.addMensajeError((String) cargaArchivo[1]);
        } else {
            archivoTemporal = (File) cargaArchivo[0];
        }
        return archivoTemporal;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * metodo action del botón "guardar" de la ventana de cargue de fotos
     *
     * @author juan.agudelo
     */
    public void guardarFoto() {

        // this.rutaWebImagenBase = rutaMostrar;
        // this.existeImagen = false;
        this.existeImagen = true;
        this.errorConfirmarDatosCargarFotografia = true;
        this.mensajeFotos = "La imagen fue cargada con exito!";

    }

    // --------------------------------------------------- //
    /**
     * Devuelve la cadena que debe quedar como "tipo" de la foto del componente.
     *
     * @return
     */
    /*
     * @modified pedro.garcia 08-05-2012 se tiene en cuenta ahora el tipo de foto para los anexos
     */
    private String getTipoFotoPorComponenteConstruccion() {

        String answer = "";

        if (!this.selectedUsoUnidadConstruccion.getDestinoEconomico()
            .equals(EUnidadConstruccionTipoCalificacion.ANEXO.getNombre())) {

            String componente = this.currentCalificacionConstruccionNode
                .getCalificacionConstruccion().getComponente();

            if (componente
                .equals(EComponenteConstruccionCom.ACABADOS_PRINCIPALES.getCodigo())) {
                answer = EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo();
            } else if (componente.equals(EComponenteConstruccionCom.BANIO.getCodigo())) {
                answer = EFotoTipo.BANIO.getCodigo();
            } else if (componente.equals(EComponenteConstruccionCom.COCINA.getCodigo())) {
                answer = EFotoTipo.COCINA.getCodigo();
            } else if (componente
                .equals(EComponenteConstruccionCom.COMPLEMENTO_INDUSTRIA
                    .getCodigo())) {
                answer = EFotoTipo.COMPLEMENTO_INDUSTRIA.getCodigo();
            } else if (componente
                .equals(EComponenteConstruccionCom.ESTRUCTURA
                    .getCodigo())) {
                answer = EFotoTipo.ESTRUCTURA.getCodigo();
            }
        } else {
            answer = EFotoTipo.ANEXO.getCodigo();
        }

        return answer;
    }

    // -----------------------------------------------------------------------------
    /**
     * Crea un nuevo documento con la ruta de la imagen en alfresco
     */
    private Documento nuevoDocumento() {

        Documento doc = new Documento();

        doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setPredioId(this.predioSeleccionado.getId());
        //doc.setTramiteId(this.tramite.getId());
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setFechaRadicacion(new Date(System.currentTimeMillis()));
        doc.setUsuarioLog(this.usuario.getLogin());
        doc.setFechaLog(new Date(System.currentTimeMillis()));
        TipoDocumento td = new TipoDocumento();
        td.setId(Constantes.TIPO_DOCUMENTO_FOTOGRAFIA);
        doc.setTipoDocumento(td);
        return doc;
    }

    /**
     * Action del botón "aceptar" en la ventana de confirmación de upload de foto. Método para
     * terminar la carga de una foto.
     *
     * @author juan.agudelo
     */
    /*
     * @modified by fredy.wilches @modified pedro.garcia Cambio de nombre del método porque no decía
     * lo que hace. Las fotos no se guardan en este punto, sino cuando se guarda la unidad de
     * construcción.
     */
    public void terminarCargaFoto() {

        String tipoFoto;
        Documento documento;

        //D: setear valores a la nueva PFoto
        this.currentPFoto.setFechaLog(new Date(System.currentTimeMillis()));
        this.currentPFoto.setUsuarioLog(this.usuario.getLogin());
        this.currentPFoto.setPPredio(this.predioSeleccionado);

        tipoFoto = this.getTipoFotoPorComponenteConstruccion();
        //D: es por el tipo de foto que se diferencian las fotos de cada componente-elemento
        this.currentPFoto.setTipo(tipoFoto);
        //this.currentPFoto.setTramiteId(this.tramite.getId());

        if (!this.isAnexoCurrentOrNewUnidadConstruccion) {
            //D: le asigna al nodo actual la ruta del archivo local para que se pueda ver la miniatura de la foto en el árbol
            this.currentCalificacionConstruccionNode.setRutaArchivoLocal(this.rutaTemporalFoto);
            this.currentCalificacionConstruccionNode.setUrlArchivoWebTemporal(
                this.rutaWebTemporalFoto);

        } else {
            this.rutaWebTemporalFotoAnexo = this.rutaWebTemporalFoto;
            this.rutaLocalFotoAnexo = this.rutaTemporalFoto;
        }

        //D: se crea el Documento que irá asociado a la PFoto
        documento = nuevoDocumento();
        documento.setArchivo(UtilidadesWeb.obtenerNombreSinTemporalArchivo(this.rutaTemporalFoto));
        documento.setRutaArchivoLocal(this.rutaTemporalFoto);

        this.currentPFoto.setDocumento(documento);
        this.currentPFoto.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);

        adicionarPFotoALista();

        this.existeImagen = false;
        this.errorConfirmarDatosCargarFotografia = false;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * adiciona una PFoto -la actual- a la lista que se va a guardar. Revisa si, por nombre de
     * componente, ya existe una foto para éste, en cuyo caso la elimina.
     *
     * Al guardar, primero se borran todos los registros de PFoto y luego se insertan los nuevos
     *
     * @author pedro.garcia
     */
    private void adicionarPFotoALista() {

        String currentPFotoComponenteName, tempPFotoComponenteName;
        String[] descripcionPartida;

        descripcionPartida = this.currentPFoto.getDescripcion().split(
            Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);
        currentPFotoComponenteName = descripcionPartida[0];

        if (this.pFotosList == null) {
            this.pFotosList = new ArrayList<PFoto>();
        }

        for (PFoto tempPFoto : this.pFotosList) {
            descripcionPartida = tempPFoto.getDescripcion().split(
                Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);
            tempPFotoComponenteName = descripcionPartida[0];
            if (tempPFotoComponenteName.equals(currentPFotoComponenteName)) {

                //D: si la nueva PFoto reemplaza a una anterior, se debe, al terminar la operación de 
                //creación o modificación de la unidad de construcción, borrar la que existía (en bd y en alfresco)
                if (this.listaPFotosABorrar == null) {
                    this.listaPFotosABorrar = new ArrayList<PFoto>();
                }
                this.listaPFotosABorrar.add(tempPFoto);
                this.pFotosList.remove(tempPFoto);
                if (!this.esNuevaUnidadConstruccion) {
                    this.currentPFoto
                        .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                }
                break;
            }
        }

        DocumentoTramiteDTO dtDto = DocumentoTramiteDTO.crearArgumentoCargarDocumentoPredioFoto(
            this.currentPFoto.getDocumento().getRutaArchivoLocal(),
            new Date(),
            this.getPredioSeleccionado().getNumeroPredial());

        dtDto.setTipoDocumento(ETipoDocumento.FOTOGRAFIA.getId().toString());
        dtDto.setNombreDepartamento(this.getPredioSeleccionado().getDepartamento().getNombre());
        dtDto.setNombreMunicipio(this.getPredioSeleccionado().getMunicipio().getNombre());
        //dtDto.setIdTramite(0);

        Documento d = this.getGeneralesService().actualizarDocumentoYSubirArchivoAlfresco(usuario,
            dtDto, this.currentPFoto.getDocumento());
        this.currentPFoto.setDocumento(d);
        this.pFotosList.add(this.currentPFoto);
        this.getConservacionService().actualizarPFotosUnidadConstruccion(this.pFotosList);

    }

    // -------------------------------------------------------------------------- //
    /**
     * Método que inicializa el arbol de componentes de una unidad del modelo con los valores más
     * altos.
     *
     * @author david.cifuentes
     */
    public void setupComponentesMayorValorUnidadDelModelo() {
        for (int i = 0; i < this.treeComponentesConstruccionParaModelo
            .getChildCount(); i++) {
            TreeNode tn = this.treeComponentesConstruccionParaModelo
                .getChildren().get(i);
            for (int j = 0; j < tn.getChildCount(); j++) {
                DefaultTreeNode dtn = (DefaultTreeNode) tn.getChildren().get(j);
                CalificacionConstruccionTreeNode cctn = (CalificacionConstruccionTreeNode) dtn
                    .getData();

                if (cctn.getDetallesCalificacionItemList().size() > 0) {
                    CalificacionConstruccion calificacionConstruccion =
                        new CalificacionConstruccion();

                    //Ahora se cambio y se quiere que aparezca seleccionado siempre el valor por
                    //defecto de seleccione javier.aponte 08-07-2013
                    // Mofified by fredy.wilches 20140201 para que no aparezca por defecto Seleccione, sino el primer elemento con valor de la lista
//					SelectItem si = cctn.getDetallesCalificacionItemList().get(
//							cctn.getDetallesCalificacionItemList().size() - 1);
                    SelectItem si = cctn.getDetallesCalificacionItemList().size() > 1 ? cctn.
                        getDetallesCalificacionItemList().get(1) : cctn.
                        getDetallesCalificacionItemList().get(0);

                    String[] objectAttributes = si
                        .getValue()
                        .toString()
                        .split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

                    calificacionConstruccion
                        .setId(new Long(objectAttributes[0]));
                    calificacionConstruccion.setComponente(objectAttributes[1]);
                    calificacionConstruccion.setElemento(objectAttributes[2]);
                    calificacionConstruccion
                        .setDetalleCalificacion(objectAttributes[3]);
                    calificacionConstruccion.setPuntos(new Integer(
                        objectAttributes[4]).intValue());

                    cctn.setSelectedDetalle(calificacionConstruccion);
                }
            }
        }
    }

    //----------------------------------------------------------------------------- //
    /**
     * Método que devuelve un valor {@link Integer} con el valor del año actual.
     *
     * @return
     */
    private Integer getAnoActual() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return new Integer(sdf.format(new Date()));
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asigna el coeficiente de copropiedad a los fichaMatrizPredios seleccionados.
     *
     * * @david.cifuentes
     */
    public void asignarCoeficientes() {
        List<PFichaMatrizPredio> aux = new ArrayList<PFichaMatrizPredio>();

        if (this.coeficienteCopropiedad == null ||
             this.coeficienteCopropiedad < 0 ||
             this.coeficienteCopropiedad > 1) {
            this.addMensajeWarn(
                "El coeficiente ingresado no es válido, debe ser un número entre 0 y 1.");
            return;
        } else {
            if (this.fichaMatrizPrediosSeleccionados != null) {
                for (int i = 0; i < this.fichaMatrizPrediosSeleccionados.length; i++) {
                    this.fichaMatrizPrediosSeleccionados[i]
                        .setCoeficiente(this.coeficienteCopropiedad);
                    aux.add(this.fichaMatrizPrediosSeleccionados[i]);
                }
            }
        }
        this.coeficienteCopropiedad = 0D;
        this.getConservacionService().guardarPrediosFichaMatriz(aux);
        this.fichaMatrizPrediosSeleccionados = null;
        Double sumaCoeficientes = 0D;
        Double uno = 1D;
        for (PFichaMatrizPredio pfmp : this.fichaMatriz
            .getPFichaMatrizPredios()) {
            if (pfmp.getCoeficiente().equals(0D)) {
                this.addMensajeWarn(
                    "Algunos de los predios tienen coeficiente en cero, el valor debe ser entre 0 y 1 ");
                return;
            }
            sumaCoeficientes += pfmp.getCoeficiente();
        }

        String mensajeMayor, mensajeMenor;
        if (sumaCoeficientes > 1 + 0.000000001 || sumaCoeficientes < 1 - 0.000000001) {

            mensajeMayor = (sumaCoeficientes > 1) ? " mayor " : "";
            mensajeMenor = (sumaCoeficientes < 1) ? " menor " : "";
            this.addMensajeWarn("Los coeficientes fueron almacenados pero la suma total de ellos " +
                sumaCoeficientes + " es" +
                 mensajeMayor + mensajeMenor + " a 1.");
            this.addMensajeWarn("Asegúrese de revisar los datos ingresados!");
            return;
        } else {
            this.addMensajeInfo("Coeficientes almacenados!");
        }
    }

    // -------------------------------------------------------------------------
    // //
    /**
     * Método que recupera el modelo de construcción en la selección del modelo de construcción.
     * Nota: Hubo la necesidad de hacerlo de ésta manera.
     *
     * @author david.cifuentes
     * @param event
     */
    public void onSelectModeloRow(SelectEvent event) {
        this.modeloUnidadDeConstruccionSeleccionado = (PFichaMatrizModelo) event
            .getObject();
    }

    // ---------------------------------------------------------------------------
    /**
     * Método para la desSeleccion del modelo de construcción.
     *
     * @author david.cifuentes
     * @param event
     */
    public void onUnselectModeloRow(UnselectEvent event) {
        this.modeloUnidadDeConstruccionSeleccionado = null;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * acción del botón 'guardar cambios' en la ventane de detalles de la unidad de construcción.
     * Guarda en la tabla P_UNIDAD_CONSTRUCCION_COMP los valores de la tabla de componente,
     * elemento, detalle, puntos Guarda las fotos de los componentes de la unidad de construcción
     * nueva o modificada.
     *
     * @author pedro.garcia
     */
    /*
     * @modified by fabio.navarrete @modified pedro.garcia 08-05-2013 orden, eliminación de
     * chambonadas
     */
    public void guardarDatosUnidadConstruccionYComponentes() {

        boolean isAnexo = true;
        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        List<PUnidadConstruccionComp> newUnidadesConstruccionComp;
        PUnidadConstruccionComp toInsert;
        CalificacionConstruccionTreeNode cctnTempL1, cctnTempL2;
        CalificacionConstruccion selectedDetalle;
        String currentComponenteName, currentElementoName;
        Date currentDate;
        Dominio valorDominio;

        //D: valida número de habitaciones mínimo para una construcción residencial
        if (this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
            if (this.unidadConstruccionSeleccionada.getTotalHabitaciones() == null ||
                 this.unidadConstruccionSeleccionada.getTotalHabitaciones() == 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La construcción por ser residencial debe tener al menos una habitación");
                return;
            }
        }

        //D: verifica si se trata de un anexo
        if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
            .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) ||
             this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
                EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) ||
             this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
                EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {

            isAnexo = false;
        }

        //D: valida condiciones de baños
        if (!isAnexo) {
            if (this.isSinBano() &&
                 this.unidadConstruccionSeleccionada.getTotalBanios() != 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La construcción no tiene baños, así que el total de baños debe ser 0");
                return;
            }
            if (this.isCalificaBano() && !this.isSinBano() &&
                 this.unidadConstruccionSeleccionada.getTotalBanios() == 0) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La construcción tiene baños, así que el total de baños debe ser diferente de 0");
                return;
            }
        }

        //D: seteo de algunos valores que dependen del tipo de construcción
        if (this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
            EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
            this.unidadConstruccionSeleccionada.setTipificacion(
                EUnidadConstruccionTipificacion.NA.getCodigo());
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
            EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
            this.unidadConstruccionSeleccionada.setTipificacion(
                EUnidadConstruccionTipificacion.NA.getCodigo());
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
            this.unidadConstruccionSeleccionada.setTipificacion(
                EUnidadConstruccionTipificacion.ANY.getCodigoByValor(
                    this.getTipificacionUnidadConstruccion()));

        } else {
            this.unidadConstruccionSeleccionada
                .setTipoConstruccion(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo());
            this.unidadConstruccionSeleccionada.setTotalPuntaje(Double.valueOf(0));
            this.unidadConstruccionSeleccionada
                .setTipificacion(EUnidadConstruccionTipificacion.NA.getCodigo());
            this.unidadConstruccionSeleccionada.setPisoUbicacion("");
            if (this.selectedCalificacionAnexo != null) {
                this.unidadConstruccionSeleccionada
                    .setCalificacionAnexoId(this.selectedCalificacionAnexo.getId());
                this.unidadConstruccionSeleccionada
                    .setDescripcion(this.selectedCalificacionAnexo.getDescripcion());
            }
        }

        try {
            // Set del valor correspondiente del dominio para la tipificación
            valorDominio = this.getGeneralesService().
                buscarValorDelDominioPorDominioYCodigo(EDominio.UNIDAD_CONSTRUCION_TIPIFICA,
                    this.unidadConstruccionSeleccionada.getTipificacion());

            if (valorDominio != null) {
                this.unidadConstruccionSeleccionada.setTipificacionValor(valorDominio.getValor());
            }

            if (!isAnexo) {
                this.unidadConstruccionSeleccionada
                    .setTipoConstruccion(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());

                this.unidadConstruccionSeleccionada.setTotalPuntaje(Double
                    .valueOf(this.getTotalPuntosConstruccion()));

            } else {
                this.unidadConstruccionSeleccionada.setTotalPuntaje(Double
                    .valueOf(this.tipoDescripcionConstruccion));
            }

            this.unidadConstruccionSeleccionada
                .setUsoConstruccion(this.selectedUsoUnidadConstruccion);

            // D: si se creó una nueva unidad de construcción, se guarda en la bd y se obtiene el 
            // id para poder guardar los datos en la tabla relacionada. 
            // Si no tiene 'unidad' se supone que es nueva
            if (this.unidadConstruccionSeleccionada.getUnidad() != null) {
                this.unidadConstruccionSeleccionada.setUnidad(this.unidadConstruccionSeleccionada.
                    getUnidad().toUpperCase());
            }

            LOGGER.debug("Almacenando datos de la unidad de construcción...");
            this.unidadConstruccionSeleccionada.setPPredio(this.predioSeleccionado);

            //campos default para los anexos
            if (isAnexo) {
                this.unidadConstruccionSeleccionada.setTotalPisosConstruccion(1);
                this.unidadConstruccionSeleccionada.setTotalPisosUnidad(1);
            }

//TODO:: felipe.cadena :: definir el origen de estos campos
            Double valorUnidad = this.getConservacionService().
                obtenerValorUnidadConstruccionPorProcedimientoDB(this.predioSeleccionado.
                    getNumeroPredial().substring(0, 7),
                    this.predioSeleccionado.getSector(),
                    this.predioSeleccionado.getDestino(),
                    this.unidadConstruccionSeleccionada.getUsoConstruccion().getIdConFormato(),
                    this.obtenerZonaFisicaMayorArea(),
                    this.unidadConstruccionSeleccionada.getTotalPuntaje().longValue(),
                    this.predioSeleccionado.getId());
            if (valorUnidad == null) {
                valorUnidad = 0.0;
            }

            this.unidadConstruccionSeleccionada.setValorM2Construccion(valorUnidad);
            this.unidadConstruccionSeleccionada.setAvaluo(valorUnidad *
                this.unidadConstruccionSeleccionada.getAreaConstruida());
            this.unidadConstruccionSeleccionada.setValorM2Construccion(0d);
            this.unidadConstruccionSeleccionada.setTipoDominio("PRIVADO");
            this.unidadConstruccionSeleccionada.setCancelaInscribe(
                EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            Long idUnidad = this.unidadConstruccionSeleccionada.getId();
            PUnidadConstruccion auxPUnidadConstruccion = (PUnidadConstruccion) this
                .getConservacionService().guardarProyeccion(this.usuario,
                    this.unidadConstruccionSeleccionada);
            LOGGER.debug("... Datos de la unidad de construcción almacenados.");
            this.predioSeleccionado.getPUnidadConstruccions().add(auxPUnidadConstruccion);
            if (idUnidad == null) {
                if (isAnexo) {
                    this.unidadConstruccionNoConvencionalNuevasNoCanceladas.add(
                        auxPUnidadConstruccion);
                } else {
                    this.unidadConstruccionConvencionalNuevasNoCanceladas.
                        add(auxPUnidadConstruccion);
                }
            } else {
                if (isAnexo) {

                    for (int i = 0; i < this.unidadConstruccionNoConvencionalNuevasNoCanceladas.
                        size(); i++) {
                        PUnidadConstruccion puc =
                            this.unidadConstruccionNoConvencionalNuevasNoCanceladas.get(i);
                        if (puc.getId().equals(idUnidad)) {
                            this.unidadConstruccionNoConvencionalNuevasNoCanceladas.remove(puc);
                            break;
                        }
                    }
                    this.unidadConstruccionNoConvencionalNuevasNoCanceladas.add(
                        auxPUnidadConstruccion);
                } else {
                    for (int i = 0; i < this.unidadConstruccionConvencionalNuevasNoCanceladas.size();
                        i++) {
                        PUnidadConstruccion puc =
                            this.unidadConstruccionConvencionalNuevasNoCanceladas.get(i);
                        if (puc.getId().equals(idUnidad)) {
                            this.unidadConstruccionConvencionalNuevasNoCanceladas.remove(puc);
                            break;
                        }
                    }
                    this.unidadConstruccionConvencionalNuevasNoCanceladas.
                        add(auxPUnidadConstruccion);
                }
            }
            this.unidadConstruccionSeleccionada.setId(auxPUnidadConstruccion.getId());

            // D: ... si es anexo (no convencional), puede que antes haya sido
            // convencional, lo que implicaría que existan registros en la tabla
            // P_UNIDAD_CONSTRUCCION_COMP. Se revisa si hay y se borran.
            this.getConservacionService()
                .borrarPUnidadConstruccionCompRegistros(
                    this.unidadConstruccionSeleccionada.getId());

            // D: si no es un anexo, se guardan los registros correspondientes en la tabla
            //    P_UNIDAD_CONSTRUCCION_COMP
            if (!isAnexo) {
                currentDate = new Date();
                newUnidadesConstruccionComp = new ArrayList<PUnidadConstruccionComp>();
                calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion
                    .getChildren();
                for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {

                    cctnTempL1 = (CalificacionConstruccionTreeNode) currentComponenteNode
                        .getData();
                    currentComponenteName = cctnTempL1
                        .getCalificacionConstruccion().getComponente();

                    calificacionConstruccionNodesLevel2 = currentComponenteNode
                        .getChildren();
                    for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                        cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode
                            .getData();
                        currentElementoName = cctnTempL2
                            .getCalificacionConstruccion()
                            .getElemento();
                        selectedDetalle = cctnTempL2.getSelectedDetalle();

                        // D: armar el objeto que se va a insertar...
                        toInsert = new PUnidadConstruccionComp();
                        toInsert.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);
                        toInsert.setComponente(currentComponenteName);
                        toInsert.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo());
                        toInsert.setElementoCalificacion(currentElementoName);
                        toInsert.setDetalleCalificacion(selectedDetalle
                            .getDetalleCalificacion());
                        toInsert.setPuntos(new Double(selectedDetalle
                            .getPuntos()));
                        toInsert.setFechaLog(currentDate);
                        toInsert.setUsuarioLog(this.usuario.getLogin());

                        newUnidadesConstruccionComp.add(toInsert);
                    }
                }

                // D: se insertan o modifican los registros de la tabla
                // P_UNIDAD_CONSTRUCCION_COMP
                LOGGER.
                    debug("insertando o actualizando registros en P_UNIDAD_CONSTRUCCION_COMP ...");

                newUnidadesConstruccionComp = this
                    .getConservacionService()
                    .insertarPUnidadConstruccionCompRegistros(
                        newUnidadesConstruccionComp);
                this.unidadConstruccionSeleccionada
                    .setPUnidadConstruccionComps(newUnidadesConstruccionComp);

                LOGGER.debug(
                    "... registros insertados o actualizados en P_UNIDAD_CONSTRUCCION_COMP.");
            }

            // D: se actualiza la unidad de construcción
            LOGGER.debug("modificando unidad de construcción ...");
            this.getConservacionService().guardarProyeccion(this.usuario,
                this.unidadConstruccionSeleccionada);
            LOGGER.debug("UC modificada");

            // N: OJO con esto. NO es necesario hacerlo, y si se hace causa
            // errores this.unidadConstruccionSeleccionada = null;
            this.addMensajeInfo("Unidad de construcción guardada exitosamente.");

            // D: si es una nueva unidad de construcción no se tenía el id de
            // esta sino hasta que se creó aquí antes, por lo que hay que definirla en las PFoto. 
            // Si la unidad de construcción no es nueva, y se adicionaron PFotos también se les 
            // debe asignar ese dato. Se hace aquí para centralizar esa operación
            // ======================================= //
            // Guardar fotos pendientes por guardar
            //D: si se adicionó o modificó alguna foto para la unidad de construcción
            if (this.pFotosList != null && !this.pFotosList.isEmpty()) {

                for (PFoto pfoto : this.pFotosList) {
                    if (null == pfoto.getPUnidadConstruccion() ||
                         null == pfoto.getPUnidadConstruccion().getId()) {
                        pfoto.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);
                    }

                    //D: al llamar a este método se crea o modifica el Documento asociado, y se
                    //  crea o modifica la PFoto.
                    //D: si el archivo local del Documento de la PFoto está vacío quiere decir que no fue cargado
                    //  como foto, es decir que no es nueva foto, y por lo tanto no se debe guardar
                    if (null != pfoto.getDocumento().getRutaArchivoLocal() &&
                         !pfoto.getDocumento().getRutaArchivoLocal().isEmpty()) {
                        pfoto = this.getConservacionService().guardarPFoto(pfoto, this.usuario);
                    }
                }
            }

            //D: eliminar las PFotos modificadas. Para borrar los registros antiguos y los archivos
            //   correspondientes en el gestor documental
            if (this.listaPFotosABorrar != null && !this.listaPFotosABorrar.isEmpty()) {
                this.getConservacionService().borrarPFotos(this.listaPFotosABorrar);
            }

            // Una vez ejecutado el procedimiento que calcula los avalúos
            // para el predio y sus unidades de construcción, se realiza la
            // consulta de la unidad de construcción
            // para actualizar la tabla de unidades de contrucción con el valor
            // del avalúo para dicha unidad.
            PUnidadConstruccion puc = this.getConservacionService()
                .buscarUnidadDeConstruccionPorSuId(
                    this.unidadConstruccionSeleccionada.getId());
            this.unidadConstruccionSeleccionada.setAvaluo(puc.getAvaluo());

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error interno al guardar la unidad de construcción.");
        }

        this.listaPFotosABorrar = null;
        this.actualizarAreas();
    }

    /**
     * action del command link de foto que aparece en cada componente de construcción de una
     * construcción convencional, y en la foto de una construcción anexo
     *
     * @author pedro.garcia
     */
    public void initCargarFoto() {

        String rutaTemp;
        this.currentPFoto = new PFoto();
        this.currentPFoto.setFecha(new Date());

        // D: se hace aquí siempre. Si resulta que es modificación de la foto,
        // más adelante se valida aquello y se cambia el valor
        this.currentPFoto.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

        //D: es por el campo 'descripcion' que se distinguen las fotos de componentes una unidad de construcción.
        if (!this.isAnexoCurrentOrNewUnidadConstruccion) {
            this.currentPFoto.setDescripcion(this.currentCalificacionConstruccionNode.
                getCalificacionConstruccion().getComponente());
        } else {
            this.currentPFoto.setDescripcion(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo());
            //" - " + this.selectedUsoUnidadConstruccion.getNombre());
        }

        rutaTemp = "";
        if (this.currentCalificacionConstruccionNode != null &&
            null != this.currentCalificacionConstruccionNode.getUrlArchivoWebTemporal()) {
            rutaTemp = this.currentCalificacionConstruccionNode.getUrlArchivoWebTemporal();
        }

        // Revisar si la imagen que se tiene para esta calificación construcción
        // es diferente de la imagen base, de ser así mostrarla.
        this.existeImagen = false;
        this.existeImagen =
            ((!rutaTemp.isEmpty() &&
            rutaTemp.compareToIgnoreCase(this.rutaWebImagenBase) != 0 &&
             !this.isAnexoCurrentOrNewUnidadConstruccion) ||
             (null != this.rutaWebTemporalFotoAnexo && !this.rutaWebTemporalFotoAnexo.isEmpty() &&
             this.rutaWebTemporalFotoAnexo.compareToIgnoreCase(this.rutaWebImagenBase) != 0 &&
             this.isAnexoCurrentOrNewUnidadConstruccion)) ?
                true : false;

        if (this.isAnexoCurrentOrNewUnidadConstruccion) {
            this.rutaWebTemporalFoto = this.rutaWebTemporalFotoAnexo;
        } else {
            this.rutaWebTemporalFoto = rutaTemp;
        }

        this.banderaBotonAceptarCargarFoto = false;
        this.errorConfirmarDatosCargarFotografia = false;
        this.setMensajeFotos("La imagen no tiene la información requerida");

    }

    // --------------------------------------------------------------------------------------------------
    /**
     *
     * @author felipe.cadena
     *
     *
     * Método que cancela una PUnidad de construccion de la ficha matriz y actualiza las areas y los
     * avalúos correspondientes a la ficha matriz.
     */
    public void cancelarPUnidadConstruccionFichaMatriz() {

        try {

            if (this.unidadConstruccionSeleccionada.getTipoConstruccion().
                equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                this.unidadConstruccionConvencionalNuevasNoCanceladas.remove(
                    this.unidadConstruccionSeleccionada);
            } else {
                this.unidadConstruccionNoConvencionalNuevasNoCanceladas.remove(
                    this.unidadConstruccionSeleccionada);
            }

            this.predioSeleccionado.getPUnidadConstruccions().remove(
                this.unidadConstruccionSeleccionada);
            this.getConservacionService().eliminarPunidadConstruccion(
                this.unidadConstruccionSeleccionada);

        } catch (Exception e) {
            LOGGER.error("error en cancelarPUnidadConstruccionFichaMatriz: " + e.getMessage());
        }

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que valida si los detalles de calificación construcción seleccionados requieren de una
     * validación, de ser así se debe mostrar un mensaje para que sea verificado el valor de la
     * selección.
     *
     * @author david.cifuentes
     */
    public void validacionDetalleCalificacionConstruc() {

        this.validarCalificacionComponenteBool = false;
        String message = "Revisar detalle de calificación.";

        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        CalificacionConstruccionTreeNode cctnTempL2;
        if (this.unidadConstruccionSeleccionada.getTipoCalificacion().equals(
            EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
            .equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL
                .getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
        } else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
            .equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL
                .getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
        }

        calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion
            .getChildren();

        boolean dynamicRefresh;
        CalificacionConstruccion ccDetalle = null;

        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            calificacionConstruccionNodesLevel2 = currentComponenteNode
                .getChildren();
            dynamicRefresh = false;
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode
                    .getData();
                // Si existe una calificacionConstruccion seleccionada que
                // requiera validación, se muestra el mensaje de
                // verificación.
                if (cctnTempL2.getSelectedDetalle() != null &&
                     ESiNo.SI.getCodigo().equals(
                        cctnTempL2.getSelectedDetalle().getValidar())) {
                    this.validarCalificacionComponenteBool = true;
                    this.addMensajeWarn(message);
                    return;
                }

                if (dynamicRefresh) {
                    for (SelectItem si : cctnTempL2
                        .getDetallesCalificacionItemList()) {
                        si.setDisabled(true);
                    }
                } else {
                    for (SelectItem si : cctnTempL2
                        .getDetallesCalificacionItemList()) {
                        si.setDisabled(false);
                    }
                }

                // Modificación dinámica del arbol para valores particulares
                // del detalle de calificación.
                if (!dynamicRefresh &&
                     cctnTempL2.getCalificacionConstruccion() != null &&
                     cctnTempL2.getCalificacionConstruccion()
                        .getElemento() != null &&
                     cctnTempL2.getCalificacionConstruccion()
                        .getElemento().equals("TAMAÑO")) {

                    ccDetalle = cctnTempL2.getSelectedDetalle();
                    if (ccDetalle.getDetalleCalificacion() != null &&
                         (ccDetalle.getDetalleCalificacion().equals(
                            "SIN BAÑO") || ccDetalle
                            .getDetalleCalificacion().equals(
                                "SIN COCINA"))) {
                        dynamicRefresh = true;
                    }
                }
            }
        }
    }

    /**
     * @author pedro.garcia
     */
    public int getTotalPuntosConstruccion() {

        if (this.unidadConstruccionSeleccionada == null) {
            return 0;
        }
        if (this.unidadConstruccionSeleccionada.getTipoCalificacion() == null) {
            return 0;
        }

        int answer = 0;
        this.validarCalificacionComponenteBool = false;
        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        CalificacionConstruccionTreeNode cctnTempL2;

        if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
            .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
        } else if (this.unidadConstruccionSeleccionada
            .getTipoCalificacion().equals(
                EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
        } else if (this.unidadConstruccionSeleccionada
            .getTipoCalificacion().equals(
                EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
            this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
        }

        calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion
            .getChildren();
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            calificacionConstruccionNodesLevel2 = currentComponenteNode
                .getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode
                    .getData();
                answer += (cctnTempL2.getSelectedDetalle() == null) ? 0 :
                     cctnTempL2.getSelectedDetalle().getPuntos();
            }
        }
        return answer;

    }

    // ---------------------------------------------------------- //
    /**
     * Método que verifica si la construcción debe o no tener baño.
     *
     * @return
     */
    public boolean isSinBano() {
        List<TreeNode> calificacionConstruccionNodesLevel2;
        CalificacionConstruccionTreeNode cctnTempL2;
        if (treeComponentesConstruccion == null || treeComponentesConstruccion.getChildren() == null) {
            // Por migracion puede no haber componentes
            return false;
        }
        for (TreeNode currentComponenteNode : treeComponentesConstruccion.getChildren()) {
            calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
                if (((CalificacionConstruccionTreeNode) currentComponenteNode.getData()).
                    getCalificacionConstruccion().getComponente().equals("BAÑO") &&
                     cctnTempL2.getCalificacionConstruccion().getElemento().equals("TAMAÑO")) {
                    if (cctnTempL2.getSelectedDetalle().getDetalleCalificacion().equals("SIN BAÑO")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    // ---------------------------------------------------------- //
    /**
     *
     * @return
     */
    public boolean isCalificaBano() {
        List<TreeNode> calificacionConstruccionNodesLevel2;
        CalificacionConstruccionTreeNode cctnTempL2;
        if (treeComponentesConstruccion == null || treeComponentesConstruccion.getChildren() == null) {
            // Por migracion puede no haber componentes
            return false;
        }
        for (TreeNode currentComponenteNode : treeComponentesConstruccion.getChildren()) {
            calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
                if (((CalificacionConstruccionTreeNode) currentComponenteNode.getData()).
                    getCalificacionConstruccion().getComponente().equals("BAÑO") &&
                     cctnTempL2.getCalificacionConstruccion().getElemento().equals("TAMAÑO")) {
                    return true;
                }
            }
        }
        return false;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * retorna la tipificación de la unidad de construcción dependiendo del puntaje total y de la
     * clasificación correspondiente en la tabla TIPIFICACION_UNIDAD_CONST
     *
     * @author pedro.garcia
     */
    public String getTipificacionUnidadConstruccion() {
        TipificacionUnidadConst tuc = this.getConservacionService()
            .obtenerTipificacionUnidadConstrPorPuntos(
                this.getTotalPuntosConstruccion());
        return tuc.getTipificacion();
    }

    /**
     * action del botón "Editar unidad de construcción". Este método también se llama desde el
     * método crearNuevaUnidadConstruccion
     *
     * @author pedro.garcia
     */
    public void initGestionDetalleUnidadConstruccion() {

        // ----- init CU-NP-CO-121
        TreeNode calificacionConstruccionNodesLevel1 = null;

        this.usosUnidadConstruccionItemList = new ArrayList<SelectItem>();
        if (this.usosConstruccionList == null || this.usosConstruccionList.isEmpty()) {
            this.usosConstruccionList = this.getConservacionService()
                .obtenerVUsosConstruccionZona(
                    this.predioSeleccionado.getNumeroPredial().substring(0, 7));
        }
        this.setUsosUnidadConstruccionItemList(this.usosConstruccionList);

        //D: si la unidad de construcción tiene un uso de construcción asignado, se le asignan esos 
        //   valores a las variables de clase correspondientes
        if (this.unidadConstruccionSeleccionada.getUsoConstruccion() != null) {
            this.selectedUsoUnidadConstruccionId = this.unidadConstruccionSeleccionada
                .getUsoConstruccion().getId();
            this.selectedUsoUnidadConstruccion = this.unidadConstruccionSeleccionada
                .getUsoConstruccion();
        } // ... si no, se le asigna 0l 
        else {
            if (this.usosUnidadConstruccionItemList.size() > 0 &&
                 !this.usosUnidadConstruccionItemList.get(0).getLabel()
                    .equals(this.DEFAULT_COMBOS)) {
                this.usosUnidadConstruccionItemList.
                    add(0, new SelectItem(null, this.DEFAULT_COMBOS));
            }
            this.selectedUsoUnidadConstruccionId = 0l;

        }

        // Set del id de la calificación anexo
        if (this.unidadConstruccionSeleccionada.getCalificacionAnexoId() != null) {
            this.calificacionAnexoIdSelected = this.unidadConstruccionSeleccionada.
                getCalificacionAnexoId();
        }

        // D: armar el TreeNode para mostrar los componentes de construcción
        // D: se arman los tres posibles desde el principio porque si se intenta
        // hacer que en el momento en que cambie el combo se arme y se refresque el nuevo árbol, no
        // funciona (se hace el refresco por cada línea de la tabla que muestra el árbol, y eso daña los
        // valores de la columna "puntos")
        this.treeComponentesConstruccion2 =
             armarArbolCalificacionConstruccion(EUnidadConstruccionTipoCalificacion.COMERCIAL
                .getCodigo());
        this.treeComponentesConstruccion3 = armarArbolCalificacionConstruccion(
            EUnidadConstruccionTipoCalificacion.INDUSTRIAL
                .getCodigo());
        this.treeComponentesConstruccion1 = armarArbolCalificacionConstruccion(
            EUnidadConstruccionTipoCalificacion.RESIDENCIAL
                .getCodigo());

        // N: OJO con esto. NO es necesario hacerlo, y si se hace causa errores
        // this.selectedCalificacionAnexo = null;
        // D: como este método también se llama desde el método
        // crearNuevaUnidadConstruccion, se verifica que no se trate de una
        // nueva unidad de construcción antes de hacer la consulta de las PFoto
        // asociadas a esta unidad de construcción
        if (!this.esNuevaUnidadConstruccion) {

            CalificacionConstruccionTreeNode cctnTemp;
            String tempComponenteNameNode, tempContainedComponenteNamePFoto;

            // ========================================== //
            // Se fuerza la consulta de la unidad de construcción y sus
            // componentes.
            this.unidadConstruccionSeleccionada = this.getConservacionService()
                .buscarUnidadDeConstruccionPorSuId(
                    this.unidadConstruccionSeleccionada.getId());
            // ========================================== //

            //D: se cargan las PFotos de la PUnidadConstruccion
            this.pFotosList = this.getConservacionService()
                .buscarPFotosUnidadConstruccion(this.unidadConstruccionSeleccionada.getId(), true);

            if (!this.unidadConstruccionSeleccionada.getTipoCalificacion()
                .equals(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo())) {

                this.isAnexoCurrentOrNewUnidadConstruccion = false;

                if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
                    .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
                    calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion2;
                } else if (this.unidadConstruccionSeleccionada
                    .getTipoCalificacion().equals(
                        EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
                    calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion3;
                } else if (this.unidadConstruccionSeleccionada
                    .getTipoCalificacion().equals(
                        EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
                    calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion1;
                }

                if (this.pFotosList != null && !this.pFotosList.isEmpty()) {

                    // D: ... luego de tener estos datos hay que recorrer el árbol y
                    // asignarle a cada nodo los datos de foto que se obtuvieron
                    for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1
                        .getChildren()) {
                        cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode
                            .getData();
                        tempComponenteNameNode = cctnTemp
                            .getCalificacionConstruccion().getComponente();
                        for (PFoto currentPFoto_i : this.pFotosList) {
                            tempContainedComponenteNamePFoto = currentPFoto_i.getDescripcion();
                            if (tempContainedComponenteNamePFoto.contains(tempComponenteNameNode)) {
                                cctnTemp.setUrlArchivoWebTemporal(
                                    currentPFoto_i.getDocumento().getRutaArchivoWeb());
                                cctnTemp.setMostrarIconoSubirImagen(false);
                            }
                        }
                    }
                }
                // Validación para cuando se han asociado las construcciones y
                // dichas construcciones tienen sus componentes en lazy.
                if (this.unidadConstruccionSeleccionada
                    .getPUnidadConstruccionComps() == null ||
                     this.unidadConstruccionSeleccionada
                        .getPUnidadConstruccionComps().isEmpty()) {
                    this.unidadConstruccionSeleccionada = this.getConservacionService().
                        buscarUnidadDeConstruccionPorSuId(this.unidadConstruccionSeleccionada.
                            getId());
                }

                // Hacer un recorrido de los componentes de la unidad de
                // construcción seleccionada y setearselos a los combo box del arbol.
                cargarComponentesDeLaUnidadDeConstruccion(
                    calificacionConstruccionNodesLevel1,
                    this.unidadConstruccionSeleccionada.getTipoCalificacion());

                if (!this.selectedUsoUnidadConstruccion.getDestinoEconomico()
                    .equals(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo())) {

                    if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
                        .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL
                            .getCodigo())) {
                        this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
                    } else if (this.unidadConstruccionSeleccionada
                        .getTipoCalificacion().equals(
                            EUnidadConstruccionTipoCalificacion.INDUSTRIAL
                                .getCodigo())) {
                        this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
                    } else if (this.unidadConstruccionSeleccionada
                        .getTipoCalificacion().equals(
                            EUnidadConstruccionTipoCalificacion.RESIDENCIAL
                                .getCodigo())) {
                        this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
                    }
                    this.puntajeAnterior = this.getTotalPuntosConstruccion();
                }

            } //D: para la foto de la unidad de construcción que es ANEXO. 
            //  La lista de PFotos, si tiene elementos, debe tener 1 sola
            else {
                this.isAnexoCurrentOrNewUnidadConstruccion = true;
                if (this.pFotosList != null && !this.pFotosList.isEmpty()) {
                    this.rutaWebTemporalFotoAnexo = this.pFotosList.get(0).getDocumento().
                        getRutaArchivoWeb();
                } else {
                    this.rutaWebTemporalFotoAnexo = this.rutaWebImagenBase;
                }
            }
        } else {
            this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion1;
            actualizarArbolDeComponentes();
        }
        // Cargar piso de la unidad.
        cargarPisoUnidadConstruccion();
    }

    // ---------------------------------------------------------- //
    /**
     * Método que carga el piso de la unidad en el combo box de pisos en la pantalla de
     * gestionDetalleUnidadContruccion.
     *
     * @author david.cifuentes
     */
    public void cargarPisoUnidadConstruccion() {

        // Si se tiene la ubicación del piso se carga.
        if (this.unidadConstruccionSeleccionada == null ||
             this.unidadConstruccionSeleccionada.getPisoUbicacion() == null ||
             this.unidadConstruccionSeleccionada.getPisoUbicacion()
                .trim().isEmpty()) {

            if (this.predioSeleccionado != null &&
                 this.predioSeleccionado.isEsPredioEnPH()) {
                // Si no se tiene la ubicación del piso se carga
                // basandose en el número predial.
                if (this.predioSeleccionado.getPiso() != null &&
                     !this.predioSeleccionado.getPiso().trim().isEmpty() &&
                     this.predioSeleccionado.getNumeroUnidad() != null &&
                     !this.predioSeleccionado.getNumeroUnidad().trim()
                        .isEmpty()) {

                    String pisoUbicacion = this.predioSeleccionado.getPiso();
                    String unidad = this.predioSeleccionado.getNumeroUnidad();

                    if (pisoUbicacion.substring(0, 1).equals("9") || pisoUbicacion.substring(0, 1).
                        equals("8")) {
                        int numeroSotano = 100 - Integer.valueOf(pisoUbicacion);
                        // Es un sotano
                        if (numeroSotano < 10) {
                            pisoUbicacion = "ST-0".concat("" + numeroSotano);
                        } else {
                            pisoUbicacion = "ST-".concat("" + numeroSotano);
                        }
                    } else {
                        // Es un piso
                        if (pisoUbicacion.equals("00")) {
                            pisoUbicacion = EUnidadPisoUbicacion.PISO_1.getCodigo();
                        } else {
                            pisoUbicacion = "PS-".concat(pisoUbicacion);
                        }
                    }

                    for (EUnidadPisoUbicacion e : EUnidadPisoUbicacion.values()) {
                        if (e.getCodigo().equals(pisoUbicacion)) {
                            pisoUbicacion = e.getCodigo();
                            break;
                        }
                    }
                    this.unidadConstruccionSeleccionada
                        .setPisoUbicacion(pisoUbicacion);

                } else {
                    // Se toma por defecto el piso 1
                    this.unidadConstruccionSeleccionada
                        .setPisoUbicacion(EUnidadPisoUbicacion.PISO_1
                            .getCodigo());
                }
            }
        }
    }

    // ------------------------------------------------------------------- //
    /**
     * Método que carga los componentes para el arbol de la {@link PUnidadConstruccion}
     * seleccionada.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unused")
    public void cargarComponentesDeLaUnidadDeConstruccion(
        TreeNode calificacionConstruccionNodesLevel1,
        String tipoCalificacion) {

        this.treeComponentesConstruccion = null;

        /**
         * 1. Recorrer la lista de componentes de la unidad de construcción. 2.Para el componente n
         * de la lista hallar el componente, detalleCalificación, elemento y recuperar la
         * calificacionConstruccion de éste. 3. Con esta calificacionConstruccion recorrer el
         * TreeNode que se recibe como parametro, y al encontrar el nodo, setear los datos
         * recuperados de la unidad de construcción. 4. Reasignar el TreeNode actualizado con el
         * detalle de la unidad del modelo de construcción.
         */
        if (calificacionConstruccionNodesLevel1 != null &&
             calificacionConstruccionNodesLevel1.getChildren() != null &&
             calificacionConstruccionNodesLevel1.getChildren().size() > 0) {

            String componente, detalleCalificacion, elementoCalificacion;
            CalificacionConstruccion auxCalificacionConstruccion, calificacionConstruccionDelComponente;
            if (this.unidadConstruccionSeleccionada
                .getPUnidadConstruccionComps() != null &&
                 !this.unidadConstruccionSeleccionada
                    .getPUnidadConstruccionComps().isEmpty()) {

                for (PUnidadConstruccionComp pucc : this.unidadConstruccionSeleccionada
                    .getPUnidadConstruccionComps()) {
                    componente = pucc.getComponente();
                    elementoCalificacion = pucc.getElementoCalificacion();
                    detalleCalificacion = pucc.getDetalleCalificacion();
                    calificacionConstruccionDelComponente = buscarCalificacionConstruccion(
                        componente, elementoCalificacion,
                        detalleCalificacion);

                    if (calificacionConstruccionDelComponente != null) {
                        for (TreeNode tn : calificacionConstruccionNodesLevel1
                            .getChildren()) {

                            // Leer el nodo y comparar con el valor de
                            // componente.
                            tn.setExpanded(true);

                            auxCalificacionConstruccion = ((CalificacionConstruccionTreeNode) tn
                                .getData()).getCalificacionConstruccion();
                            if (auxCalificacionConstruccion != null &&
                                 auxCalificacionConstruccion
                                    .getComponente() != null &&
                                 auxCalificacionConstruccion
                                    .getComponente().equals(componente)) {

                                // Cuando encuentre el componente en el arbol,
                                // se recorren las calificaciones del componente
                                // y se hace el set del detalle a cada una.
                                int aux = 0;
                                int puntos;
                                for (TreeNode tnElemento : tn.getChildren()) {
                                    tnElemento.setExpanded(true);
                                    if (((CalificacionConstruccionTreeNode) tnElemento
                                        .getData())
                                        .getCalificacionConstruccion()
                                        .getElemento()
                                        .equals(elementoCalificacion)) {

                                        aux++;

                                        // Se actualizan los puntos de
                                        // calificacion para el elemento
                                        // CalificacionConstruccionDelComponente.
                                        puntos = getPuntosPorCalificacionConstruccion(
                                            calificacionConstruccionDelComponente,
                                            tipoCalificacion);
                                        calificacionConstruccionDelComponente
                                            .setPuntos(puntos);

                                        // Se setea el objeto calificacion
                                        // construccion de la unidad al
                                        // selected.
                                        ((CalificacionConstruccionTreeNode) tnElemento
                                            .getData())
                                            .setSelectedDetalle(
                                                calificacionConstruccionDelComponente);
                                        LOGGER.debug("SETEANDO " +
                                             calificacionConstruccionDelComponente);
                                        // break;
                                    }
                                }
                                // break;
                            }
                        }
                    }
                }
                // Reasignación del TreeNode.
                this.treeComponentesConstruccion = calificacionConstruccionNodesLevel1;
                mostrarPuntos(this.treeComponentesConstruccion);
            } else {
                this.actualizarArbolDeComponentes();
            }
        }

    }

    // ---------------------------------------------------------- //
    /**
     * Método para mostrar el puntaje del arbol de componentes de la c construcción.
     *
     * @param treeComponentesConstruccion
     */
    public void mostrarPuntos(TreeNode treeComponentesConstruccion) {

        int answer = 0;

        List<TreeNode> calificacionConstruccionNodesLevel2;
        CalificacionConstruccionTreeNode cctnTempL2;

        for (TreeNode currentComponenteNode : treeComponentesConstruccion
            .getChildren()) {
            calificacionConstruccionNodesLevel2 = currentComponenteNode
                .getChildren();
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode
                    .getData();
                answer += (cctnTempL2.getSelectedDetalle() == null) ? 0 :
                     cctnTempL2.getSelectedDetalle().getPuntos();
                LOGGER.debug("DETALLE " + cctnTempL2.getSelectedDetalle());
            }
        }
        LOGGER.debug("TOTAL " + answer);

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * calcula ese valor dependiendo del árbol que se esté trabajando
     *
     * @author pedro.garcia
     * @param calificacionConstruccion
     * @param tipoCalificacion
     * @return
     */
    private int getPuntosPorCalificacionConstruccion(
        CalificacionConstruccion calificacionConstruccion,
        String tipoCalificacion) {

        int answer = -1;

        if (tipoCalificacion
            .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL
                .getCodigo())) {
            answer = calificacionConstruccion.getPuntosComercial().intValue();
        } else if (tipoCalificacion
            .equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL
                .getCodigo())) {
            answer = calificacionConstruccion.getPuntosIndustrial().intValue();
        } else if (tipoCalificacion
            .equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL
                .getCodigo())) {
            answer = calificacionConstruccion.getPuntosResidencial().intValue();
        }

        return answer;
    }

    // ----------------------------------------------------------------------------------------
    /**
     * Método que busca el objeto {@link CalificacionConstruccion} que tenga los paramentros
     * ingresados, y que se encuentre en la lista listaElementosCalificacionConstruccion.
     *
     * @return
     *
     * @author david.cifuentes
     */
    public CalificacionConstruccion buscarCalificacionConstruccion(
        String componente, String elementoCalificacion,
        String detalleCalificacion) {

        CalificacionConstruccion ccanswer = null;

        // Verificar que la lista de elementos calificacion construccion no esté
        // vacia.
        if (this.listaElementosCalificacionConstruccion == null ||
             this.listaElementosCalificacionConstruccion.isEmpty()) {

            listaElementosCalificacionConstruccion = this
                .getConservacionService()
                .obtenerTodosCalificacionConstruccion();
        }
        // Búsqueda del elemento calificación construcción.
        if (this.listaElementosCalificacionConstruccion != null &&
             !this.listaElementosCalificacionConstruccion.isEmpty()) {

            if (componente != null && elementoCalificacion != null &&
                 detalleCalificacion != null) {
                for (CalificacionConstruccion cc : this.listaElementosCalificacionConstruccion) {
                    if (cc.getComponente().equals(componente) &&
                         cc.getElemento().equals(elementoCalificacion) &&
                         cc.getDetalleCalificacion().equals(
                            detalleCalificacion)) {
                        ccanswer = cc;
                        break;
                    }
                }
            }
        }
        return ccanswer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @param calificacionConstruccionElements lista de objetos de tipo CalificacionConstruccion
     * obtenida de la consulta a la BD
     *
     * OJO: por el modo en que se arma el árbol (adicionando objetos que se traen de la consulta)
     * hay que hacer la consulta cada vez que se vaya a armar cada uno de los árboles. Si no se hace
     * los árboles quedan con los datos "pegados" del primero
     */
    private TreeNode armarArbolCalificacionConstruccion(String tipoCalificacion) {

        // D: obtener los registros de la tabla que sirve para armar el árbol
        List<CalificacionConstruccion> calificacionConstruccionElements = this
            .getConservacionService()
            .obtenerTodosCalificacionConstruccion();

        TreeNode answer;
        answer = new DefaultTreeNode("root" + tipoCalificacion, null);

        List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
        TreeNode componenteToAdd;
        String currentComponenteName, tempComponenteName;
        String currentElementoName, tempElementoName;
        CalificacionConstruccion incompleteToAdd;
        CalificacionConstruccionTreeNode ccNode, cctnTemp, cctnTempL2;
        int puntosPorCalificacionConstruccion;
        boolean addedNode = false;

        // D: armado de la columna "componente construcción"
        currentComponenteName = "";
        for (CalificacionConstruccion comp : calificacionConstruccionElements) {
            tempComponenteName = comp.getComponente();

            // D: no se adicionan como nodos los "componente" que no tengan por
            // lo menos un "elemento" cuyos puntos sean >= 0
            puntosPorCalificacionConstruccion = getPuntosPorCalificacionConstruccion(
                comp, tipoCalificacion);

            if (tempComponenteName.compareToIgnoreCase(currentComponenteName) != 0) {

                if (puntosPorCalificacionConstruccion >= 0 &&
                     !(tipoCalificacion
                        .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL
                            .getCodigo()) && tempComponenteName
                        .equals(EComponenteConstruccionCom.COMPLEMENTO_INDUSTRIA.getCodigo()))) {
                    incompleteToAdd = new CalificacionConstruccion();
                    incompleteToAdd.setComponente(tempComponenteName);
                    ccNode = new CalificacionConstruccionTreeNode();
                    ccNode.setCalificacionConstruccion(incompleteToAdd);
                    ccNode.setMostrarIconoSubirImagen(true);
                    //ccNode.setRutaArchivoLocal(this.rutaWebImagenBase);
                    ccNode.setUrlArchivoWebTemporal(this.rutaWebImagenBase);
                    componenteToAdd = new DefaultTreeNode("componente", ccNode,
                        answer);

                    addedNode = true;
                } else {
                    addedNode = false;
                }

                currentComponenteName = tempComponenteName;
            } else {
                if (puntosPorCalificacionConstruccion >= 0 &&
                     !(tipoCalificacion
                        .equals(EUnidadConstruccionTipoCalificacion.COMERCIAL
                            .getCodigo()) && tempComponenteName
                        .equals(EComponenteConstruccionCom.COMPLEMENTO_INDUSTRIA.getCodigo()))) {
                    if (!addedNode) {
                        incompleteToAdd = new CalificacionConstruccion();
                        incompleteToAdd.setComponente(tempComponenteName);
                        ccNode = new CalificacionConstruccionTreeNode();
                        ccNode.setCalificacionConstruccion(incompleteToAdd);
                        ccNode.setMostrarIconoSubirImagen(true);
                        //ccNode.setRutaArchivoLocal(this.rutaWebImagenBase);
                        ccNode.setUrlArchivoWebTemporal(this.rutaWebImagenBase);
                        componenteToAdd = new DefaultTreeNode("componente",
                            ccNode, answer);

                        addedNode = true;
                    }
                }
            }
        }

        // D: armado de los elementos de la columna "elemento calificación"
        currentComponenteName = "";
        currentElementoName = "";
        calificacionConstruccionNodesLevel1 = answer.getChildren();
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode
                .getData();
            tempComponenteName = cctnTemp.getCalificacionConstruccion()
                .getComponente();
            currentComponenteNode.setExpanded(true);
            if (tempComponenteName.compareToIgnoreCase(currentComponenteName) != 0) {
                for (CalificacionConstruccion comp : calificacionConstruccionElements) {
                    tempElementoName = comp.getElemento();
                    if (tempElementoName
                        .compareToIgnoreCase(currentElementoName) != 0 &&
                         comp.getComponente().compareToIgnoreCase(
                            tempComponenteName) == 0) {

                        puntosPorCalificacionConstruccion = getPuntosPorCalificacionConstruccion(
                            comp, tipoCalificacion);

                        // D: no se adicionan los "elemento" que no apliquen
                        // para el "componente";
                        // es decir, que tengan puntos < 0
                        if (puntosPorCalificacionConstruccion >= 0) {
                            incompleteToAdd = new CalificacionConstruccion();
                            incompleteToAdd.setElemento(tempElementoName);
                            ccNode = new CalificacionConstruccionTreeNode();
                            ccNode.setCalificacionConstruccion(incompleteToAdd);
                            componenteToAdd = new DefaultTreeNode("elemento",
                                ccNode, currentComponenteNode);
                        }

                        currentElementoName = tempElementoName;
                    }
                }
                currentComponenteName = tempComponenteName;
                currentElementoName = "";
            }
        }
        // D: adición de los combos a cada elemento de nivel 2
        ArrayList<CalificacionConstruccion> comboelements;
        for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
            cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode
                .getData();
            currentComponenteName = cctnTemp.getCalificacionConstruccion()
                .getComponente();
            calificacionConstruccionNodesLevel2 = currentComponenteNode
                .getChildren();
            currentComponenteNode.setExpanded(true);
            for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
                cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode
                    .getData();
                currentElementoName = cctnTempL2.getCalificacionConstruccion()
                    .getElemento();
                comboelements = new ArrayList<CalificacionConstruccion>();
                currentElementoNode.setExpanded(true);
                for (CalificacionConstruccion comp : calificacionConstruccionElements) {
                    tempComponenteName = comp.getComponente();
                    tempElementoName = comp.getElemento();
                    if (currentComponenteName
                        .compareToIgnoreCase(tempComponenteName) == 0 &&
                         currentElementoName
                            .compareToIgnoreCase(tempElementoName) == 0) {
                        comboelements.add(comp);
                    }
                }
                // cctnTempL2.setTipoCalificacion(this.currentUnidadConstruccion.getTipoCalificacion());
                cctnTempL2.setTipoCalificacion(tipoCalificacion);

                // D: se cambian los elementos mismos (de nivel 2)
                // adicionándoles el combo
                cctnTempL2.armarComboDetalleCalificacion(comboelements);
            }
        }

        return answer;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que inicializa una unidad de contrucción.
     */
    public void setupAdicionConstruccion() {

        this.unidadConstruccionSeleccionada = new PUnidadConstruccion();
        this.esNuevaUnidadConstruccion = true;
        this.datosConstruccionSoloLectura = false;
        this.initGestionDetalleUnidadConstruccion();
    }

    /**
     * Método que elimina una {@link PPredioDireccion} seleccionada.
     *
     * @author david.cifuentes
     */
    public void eliminarDireccion() {

        if (this.direccionSeleccionada != null) {

            this.predioSeleccionado.getPPredioDireccions().remove(
                this.direccionSeleccionada);
            this.getConservacionService().eliminarPPredioDireccion(this.direccionSeleccionada);
            this.predioSeleccionadoDirecciones.remove(this.direccionSeleccionada);
            //this.actualizarListaDeDirecciones();
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que prepara los datos necesarios para multiplicar una torre
     *
     * @author fabio.navarrete
     */
    public void setupMultiplicarFichaMatrizTorre() {
        this.numeroTorresMultiplicar = null;
    }

    /**
     * Metodo para calcular las areas de la ficha predial
     *
     *
     * @author felipe.cadena
     */
    public void actualizarAreas() {

        Double areaConstruidaComun = 0.0;
        if (this.predioSeleccionado.getPUnidadConstruccions() != null) {
            for (PUnidadConstruccion puc : this.predioSeleccionado.getPUnidadConstruccions()) {
                areaConstruidaComun += puc.getAreaConstruida();
            }
        }
        this.fichaMatriz.setAreaTotalConstruidaComun(areaConstruidaComun);
        this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);

        if (this.predioSeleccionado.isPredioRural()) {
            this.areaTotalTerrenoM2 = this.fichaMatriz.getAreaTotalTerreno() % 10000;
            this.areaTotalTerrenoPrivadaM2 = this.fichaMatriz.getAreaTotalTerrenoPrivada() % 10000;
            this.areaTotalTerrenoComunM2 = this.fichaMatriz.getAreaTotalTerrenoComun() % 10000;
            this.areaTotalTerrenoHa = (this.fichaMatriz.getAreaTotalTerreno()).intValue() / 10000;
            this.areaTotalTerrenoPrivadaHa = (this.fichaMatriz.getAreaTotalTerrenoPrivada()).
                intValue() / 10000;
            this.areaTotalTerrenoComunHa =
                (this.fichaMatriz.getAreaTotalTerrenoComun()).intValue() / 10000;
        } else {
            this.areaTotalTerrenoM2 = this.fichaMatriz.getAreaTotalTerreno();
            this.areaTotalTerrenoPrivadaM2 = this.fichaMatriz.getAreaTotalTerrenoPrivada();
            this.areaTotalTerrenoComunM2 = this.fichaMatriz.getAreaTotalTerrenoComun();
        }

        this.areaTotalConstruidaM2 = this.fichaMatriz.getAreaTotalConstruida();
        this.areaTotalConstruidaPrivadaM2 = this.fichaMatriz.getAreaTotalConstruidaPrivada();
        this.areaTotalConstruidaComunM2 = this.fichaMatriz.getAreaTotalConstruidaComun();

    }

    /**
     *
     *
     * Guarda la zona agregada
     *
     * @author felipe.cadena
     */
    public void guardarZona() {

        Calendar vigencia = Calendar.getInstance();
        int anioactual = vigencia.get(Calendar.YEAR);

        if (this.zonaSeleccionada.getZonaFisica().length() != 2 || this.zonaSeleccionada.
            getZonaGeoeconomica().length() != 2) {
            this.addMensajeError(
                "El valor de la zona fisica y la zona geoeconómica debe ser de dos digitos");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        if (this.anioZona > anioactual || this.anioZona < 1500) {
            this.addMensajeError("El valor del año de la vigencia no puede ser mayor al actual.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
        vigencia.set(Calendar.DAY_OF_MONTH, 1);
        vigencia.set(Calendar.MONTH, 0);
        vigencia.set(Calendar.YEAR, this.anioZona);

        this.zonaSeleccionada.setFechaLog(new Date());
        this.zonaSeleccionada.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());

        this.zonaSeleccionada.setVigencia(vigencia.getTime());
        this.zonaSeleccionada.setUsuarioLog(this.usuario.getLogin());
        this.zonaSeleccionada.setPPredio(this.predioSeleccionado);
        this.zonaSeleccionada.setAvaluo(this.zonaSeleccionada.getArea() * this.zonaSeleccionada.
            getValorM2Terreno());
        this.zonaSeleccionada.setAvaluo(this.zonaSeleccionada.getArea() * this.zonaSeleccionada.
            getValorM2Terreno());
        this.zonaSeleccionada = this.getConservacionService().guardarActualizarZona(
            this.zonaSeleccionada);

        this.predioSeleccionado.getPPredioZonas().add(this.zonaSeleccionada);

        boolean contain = false;
        for (PPredioZona zona : this.zonasHomogeneas) {
            if (zona.getId().equals(this.zonaSeleccionada.getId())) {
                contain = true;
                break;
            }
        }
        if (!contain) {
            this.zonasHomogeneas.add(this.zonaSeleccionada);
        }

        if (this.predioSeleccionado.isCondominio()) {
            this.calcularAreaCondominios();
        }

    }

    /**
     * Metodo para eliminar zona
     *
     * @author felipe.cadena
     */
    public void eliminarZona() {
        this.getConservacionService().eliminarZona(this.zonaSeleccionada);
        this.predioSeleccionado.getPPredioZonas().remove(this.zonaSeleccionada);
        this.zonasHomogeneas.remove(this.zonaSeleccionada);
        if (this.predioSeleccionado.isCondominio()) {
            this.calcularAreaCondominios();
        }
    }

    /**
     * Metodo para eliminar zona
     *
     * @author felipe.cadena
     */
    public void stupAdicionarZona() {
        this.zonaSeleccionada = new PPredioZona();
    }

    /**
     * Calcula el area comun de los condominios basado en las zonas ingresadas.
     *
     * @author felipe.cadena
     */
    private void calcularAreaCondominios() {

        Double areaComun = 0d;
        for (PPredioZona zona : this.zonasHomogeneas) {
            areaComun += zona.getArea();
        }

        this.fichaMatriz.setAreaTotalTerrenoComun(Utilidades.redondearNumeroADosCifrasDecimales(
            areaComun));
        this.actualizarAreas();
        this.calcularAvaluoTerrenoComun();
    }

    /**
     * Calcula el avaluo del terreno comun
     *
     * @author felipe.cadena
     *
     */
    public void calcularAvaluoTerrenoComun() {
        this.avaluoTotalTerrenoComun = 0d;

        for (PPredioZona zona : this.zonasHomogeneas) {
            this.avaluoTotalTerrenoComun += zona.getAvaluo();
        }
    }

    /**
     * Actualiza las unidades ingresaaspor el usuario
     *
     * @author felipe.cadena
     *
     */
    public void guardarUnidadesCondominio() {
        if (this.fichaMatriz.getTotalUnidadesPrivadas() == null) {
            this.fichaMatriz.setTotalUnidadesPrivadas(0L);
        }

        int nUnidadesRegistradas = this.fichaMatriz.getPFichaMatrizPredios().size();
        Long nUnidades = this.fichaMatriz.getTotalUnidadesPrivadas();

        if (nUnidades > nUnidadesRegistradas) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "La cantidad de unidades supera el total de unidades registradas : " +
                nUnidadesRegistradas);
            this.fichaMatriz.setTotalUnidadesPrivadas(0L);
            return;

        } else if (nUnidades < nUnidadesRegistradas) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "La cantidad de unidades es menor al total de unidades registradas : " +
                nUnidadesRegistradas);
            this.fichaMatriz.setTotalUnidadesPrivadas(0L);
            return;
        }

        this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);
    }

    // ------------------------------------------------------------- //
    /**
     * Método que se encarga de hacer una replica de la torre seleccionada.
     *
     * @author david.cifuentes
     */
    public void setupTorreReplica() {
//		if (this.fichaMatrizTorreSeleccionada != null
//				&& this.fichaMatrizTorreSeleccionada.getId() != null) {
//			this.fichaMatrizTorreReplica = (PFichaMatrizTorre) this.fichaMatrizTorreSeleccionada
//					.clone();
//		}

        this.editarTorre = true;
        this.fichaMatriz.setTotalUnidadesPrivadas(this.fichaMatriz.getTotalUnidadesPrivadas() -
            this.fichaMatrizTorreSeleccionada.getUnidades());
        this.fichaMatriz.setTotalUnidadesSotanos(this.fichaMatriz.getTotalUnidadesSotanos() -
            this.fichaMatrizTorreSeleccionada.getUnidadesSotanos());

    }

    /**
     * Metodo para calcular y guardar el avaluo relacionado al predio de la ficha.
     *
     * @author felipe.cadena
     */
    public void guardarAvaluosFicha() {

        //valor contrucciones
        Double valorConstrucciones = 0d;
        if (this.unidadConstruccionConvencionalNuevas != null) {
            for (PUnidadConstruccion puc : this.unidadConstruccionConvencionalNuevas) {
                valorConstrucciones += puc.getAvaluo();
            }
        }
        if (this.unidadConstruccionNoConvencionalNuevas != null) {
            for (PUnidadConstruccion puc : this.unidadConstruccionNoConvencionalNuevas) {
                valorConstrucciones += puc.getAvaluo();
            }
        }

        //valor terreno privado
        double[] avaluosPredios = this.getConservacionService().
            calcularSumaAvaluosTotalesDePrediosMigradosFichaMatriz(this.fichaMatriz.getId());

        PPredioAvaluoCatastral ppac = new PPredioAvaluoCatastral();
        ppac.setPPredio(this.predioSeleccionado);
        ppac.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
        ppac.setFechaInscripcionCatastral(new Date());
        ppac.setFechaLog(new Date());
        ppac.setUsuarioLog(this.usuario.getLogin());
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DATE, 1);
        ppac.setVigencia(c.getTime());
        ppac.setAutoavaluo(ESiNo.NO.getCodigo());

        ppac.setValorConstruccionComun(valorConstrucciones); // Son las construcciones de la ficha matriz A+B
        ppac.setValorTerrenoComun(this.avaluoTotalTerrenoComun); // Se calcula a partir de las zonas

        ppac.setValorTerrenoPrivado(avaluosPredios[0]); // Suma de todos los avaluos de las unidades de la ficha para la última vigencia (En los PH debería ser cero, en condominios no necesariamente)
        ppac.setValorTotalConsConvencional(0.0); // Ajusto A
        ppac.setValorTotalConsNconvencional(0.0); // Ajusto B

        ppac.setValorTotalConstruccionPrivada(avaluosPredios[1] + avaluosPredios[2]); // Suma de avaluos de UC + UNC de todas las unidades
        ppac.setValorTerreno(ppac.getValorTerrenoComun() + ppac.getValorTerrenoPrivado());
        ppac.setValorTotalConstruccion(ppac.getValorConstruccionComun() + ppac.
            getValorTotalConstruccionPrivada());

        ppac.setValorTotalAvaluoCatastral(ppac.getValorTerreno() + ppac.getValorTotalConstruccion());
        ppac.setValorTotalAvaluoVigencia(ppac.getValorTerreno() + ppac.getValorTotalConstruccion());

        //felipe.cadena::#16188::02-03-2016::Se debe consultar el avaluo si ya 
        //existe debido a que hay una restriccion de unicidad con predio y vigencia 
        List<PPredioAvaluoCatastral> ppacAnteriores = this.getConservacionService().
            obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());

        for (PPredioAvaluoCatastral pPredioAvaluoCatastral : ppacAnteriores) {
            if (pPredioAvaluoCatastral.getVigencia().getYear() == ppac.getVigencia().getYear()) {
                ppac.setId(pPredioAvaluoCatastral.getId());
                break;
            }
        }

        ppac = this.getConservacionService().actualizarPPredioAvaluoCatastral(ppac);

    }

    /**
     * Calcula el valor de las areas de las zonas basado en las tablas de valor asociadas
     *
     * @author felipe.cadena
     *
     * @throws Exception
     *
     */
    /*
     * @modified david.cifuentes :: Ajustes para los casos en que no se puede obtener el valor de la
     * unidad de terreno
     *
     */
    public void actualizarAvaluosZonas() throws Exception {

        Double valorUnidadTerreno = 0.0;

        for (PPredioZona ppz : this.zonasHomogeneas) {
            valorUnidadTerreno = this.getConservacionService().
                obtenerValorUnidadTerreno(this.predioSeleccionado.getNumeroPredial().substring(0, 7),
                    this.predioSeleccionado.getDestino(),
                    ppz.getZonaGeoeconomica(), this.predioSeleccionado.getId());
            if (valorUnidadTerreno != null && valorUnidadTerreno.intValue() == -1) {
                throw (new Exception(
                    "Error al obtener el valor de la unidad de terreno para las zonas"));
            }

            if (this.predioSeleccionado.getTipoAvaluo().equals(EAvaluoTipoAvaluo.RURAL.getCodigo())) {
                ppz.setValorM2Terreno(valorUnidadTerreno / 10000);
                valorUnidadTerreno = (double) Math.round(valorUnidadTerreno / 10000);
            } else {
                ppz.setValorM2Terreno(valorUnidadTerreno);
            }

            ppz.setAvaluo(valorUnidadTerreno * ppz.getArea());
            ppz = this.getConservacionService().guardarActualizarZona(ppz);
        }
    }

    /**
     * Retorna la zona de mayor area relacionada al predio de la ficha matriz
     *
     * @author felipe.cadena
     * @return
     */
    public String obtenerZonaFisicaMayorArea() {
        String zonaMayor = "";
        Double area = 0.0;

        if (this.zonasHomogeneas != null && !this.zonasHomogeneas.isEmpty()) {

            for (PPredioZona ppz : this.zonasHomogeneas) {

                if (ppz.getArea() > area) {
                    zonaMayor = ppz.getZonaFisica();
                }
            }
        }

        return zonaMayor;
    }

}
