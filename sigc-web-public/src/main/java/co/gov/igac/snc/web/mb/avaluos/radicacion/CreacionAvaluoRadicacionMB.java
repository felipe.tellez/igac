package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.AdministracionInfoContactoDePrediosMB;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.AdministracionPrediosAvaluosMB;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.CarguePrediosDeAvaluoDesdeArchivoMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed Bean asociado al caso de uso CU-SA-AC-048 Crear Avalúo Asociado a la Radicación
 *
 * @author christian.rodriguez
 *
 */
@Component("crearAvaluoRadicacion")
@Scope("session")
public class CreacionAvaluoRadicacionMB extends SNCManagedBean {

    private static final long serialVersionUID = -8892740765731101282L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreacionAvaluoRadicacionMB.class);

    /**
     * MB para CU-SA-AC-46
     */
    @Autowired
    private AdministracionPrediosAvaluosMB adminPrediosMB;

    /**
     * MB para CU-SA-AC-139
     */
    @Autowired
    private CarguePrediosDeAvaluoDesdeArchivoMB carguePrediosAvaluosMB;

    /**
     * MB para CU-SA-AC-109
     */
    @Autowired
    private AdministracionInfoContactoDePrediosMB administracionInfoContactoDePrediosMB;

    /**
     * Usuario loggeado en la sesión
     */
    private UsuarioDTO usuario;

    /**
     * Solicitud seleccioanda, es la que se selecciona desde el arbol de procesos
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Contiene los datos de la territorial asociada a la solicitud
     */
    private EstructuraOrganizacional territorialUsuario;

    /**
     * Lista con los avalúos asociados a la solicitud seleccionada
     */
    private List<Avaluo> avaluosSolicitud;

    /**
     * Lista con los predios de la solicitud seleccionada
     */
    private List<SolicitudPredio> prediosSolicitudSeleccionada;

    /**
     * Lista con los predios sin avalúo asociados a la solicitud seleccionada
     */
    private List<SolicitudPredio> prediosDisponiblesSolicitud;

    /**
     * Lista con los predios seleccionados para asociar al avaluo
     */
    private SolicitudPredio[] prediosAsociarAvaluo;

    /**
     * Lista con los predios seleccionados para desasociar del avaluo
     */
    private AvaluoPredio[] prediosDesasociarAvaluo;

    /**
     * Lista con los contactos asociados a la solicitud seleccionada
     */
    private List<SolicitanteSolicitud> contactosSolicitud;

    /**
     * Solicitante asociado a la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Contiene el avalúo seleccionado de la tabla de avalúos creados
     */
    private Avaluo avaluoSeleccionado;

    /**
     * Lista con los avalúos seleccionados
     */
    private Avaluo[] avaluosSeleccionados;

    /**
     * Atributo que contiene el label que va a mostrar el botón de guardar dependiendo de en que
     * pestaña este el usuario
     */
    private String labelBotonGuardar;

    /**
     * Atributos usados para manejar el label del botón de crear sec radicado dependiendo de la
     * pestaña en la que este el usuario
     */
    private final String ID_TAB_ASOCIAR_PREDIOS = "asociarPredios";
    private final String LABEL_BTN_ASOCIAR_PREDIOS = "Asociar predios y guardar";
    private final String LABEL_BTN_DESASOCIAR_PREDIOS = "Desasociar predios y guardar";

    /**
     * Bandera que determina si se muestran todas las columnas de la tabla de predios disponibles o
     * se muestran solo las principales
     */
    private boolean banderaTablaExpandidaPredio;

    /**
     * Determina si se muestra o no la pestaña para desasociar predios del avaluo
     */
    private boolean banderaEsActualizacionAvaluo;

    /**
     * Determina si la solicitud seleccionada es de revisión o impugnación
     */
    private boolean banderaSolicitudDeRevisionImpugnacion;

    // ----------------------------Métodos SET y GET----------------------------
    public Solicitud getSolicitudSeleccionada() {
        return this.solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Avaluo> getAvaluosSolicitud() {
        return this.avaluosSolicitud;
    }

    public void setAvaluosSolicitud(List<Avaluo> avaluosSolicitud) {
        this.avaluosSolicitud = avaluosSolicitud;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public Avaluo[] getAvaluosSeleccionados() {
        return this.avaluosSeleccionados;
    }

    public void setAvaluosSeleccionados(Avaluo[] avaluosSeleccionados) {
        this.avaluosSeleccionados = avaluosSeleccionados;
    }

    public List<SolicitudPredio> getPrediosSolicitudSeleccionada() {
        return this.prediosSolicitudSeleccionada;
    }

    public void setPrediosSolicitudSeleccionada(List<SolicitudPredio> prediosSolicitudSeleccionada) {
        this.prediosSolicitudSeleccionada = prediosSolicitudSeleccionada;
    }

    public List<SolicitudPredio> getPrediosDisponiblesSolicitud() {
        return this.prediosDisponiblesSolicitud;
    }

    public void setPrediosDisponiblesSolicitud(List<SolicitudPredio> prediosDisponiblesSolicitud) {
        this.prediosDisponiblesSolicitud = prediosDisponiblesSolicitud;
    }

    public SolicitudPredio[] getPrediosAsociarAvaluo() {
        return this.prediosAsociarAvaluo;
    }

    public void setPrediosAsociarAvaluo(SolicitudPredio[] prediosAsociarAvaluo) {
        this.prediosAsociarAvaluo = prediosAsociarAvaluo;
    }

    public AvaluoPredio[] getPrediosDesasociarAvaluo() {
        return this.prediosDesasociarAvaluo;
    }

    public void setPrediosDesasociarAvaluo(AvaluoPredio[] prediosDesasociarAvaluo) {
        this.prediosDesasociarAvaluo = prediosDesasociarAvaluo;
    }

    public SolicitanteSolicitud getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public EstructuraOrganizacional getTerritorialUsuario() {
        return this.territorialUsuario;
    }

    public String getLabelBotonGuardar() {
        return this.labelBotonGuardar;
    }

    public void setTerritorialUsuario(EstructuraOrganizacional territorialSolicitud) {
        this.territorialUsuario = territorialSolicitud;
    }

    public boolean isBanderaTablaExpandidaPredio() {
        return this.banderaTablaExpandidaPredio;
    }

    public void setBanderaTablaExpandidaPredio(boolean banderaTablaExpandidaPredio) {
        this.banderaTablaExpandidaPredio = banderaTablaExpandidaPredio;
    }

    public boolean isBanderaEsActualizacionAvaluo() {
        return this.banderaEsActualizacionAvaluo;
    }

    public void setBanderaEsActualizacionAvaluo(boolean banderaEsActualizacionAvaluo) {
        this.banderaEsActualizacionAvaluo = banderaEsActualizacionAvaluo;
    }

    public boolean isBanderaSolicitudDeRevisionImpugnacion() {
        return this.banderaSolicitudDeRevisionImpugnacion;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio CreacionAvaluoRadicacionMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.extraerDatosArbolProcesos();
        this.cargarAvaluosSolicitud();
        this.cargarContactosSolicitud();
        this.extraerTerritorialUsuario();
        this.asignarBanderasTipoTramite();
        LOGGER.debug("Fin CreacionAvaluoRadicacionMB#init");
    }

    // --------------------------------------------------------------------------
    /**
     * Inicializa las banderas que marcan el tipo de tramite según la enumeración
     * {@link ETramiteTipoTramiteAvaluo}
     *
     * @author christian.rodriguez
     */
    private void asignarBanderasTipoTramite() {
        if (this.solicitudSeleccionada != null) {

            String tipoTramite = this.solicitudSeleccionada.getTipo();

            if (tipoTramite.equals(ETramiteTipoTramite.SOLICITUD_DE_REVISION.getCodigo()) ||
                 tipoTramite.equals(ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION
                    .getCodigo())) {

                this.banderaSolicitudDeRevisionImpugnacion = true;

            }
        }

    }

    // --------------------------------------------------------------------------
    private void iniciarDatosQuemados() {

        Long solicitudId = 43500L;
        this.solicitudSeleccionada = this.getTramiteService()
            .buscarSolicitudConSolicitantesSolicitudPorId(solicitudId);

    }

    // --------------------------------------------------------------------------
    /**
     * Método que carga los contactos asociados a la solicitud seleccionada
     *
     * @author christian.rodriguez
     */
    private void cargarContactosSolicitud() {
        if (this.solicitudSeleccionada != null) {
            this.contactosSolicitud = this.getTramiteService()
                .buscarSolicitantesSolicitudBySolicitudIdRelacion(
                    this.solicitudSeleccionada.getId(),
                    ESolicitanteSolicitudRelac.CONTACTO.toString());
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que recupera de la BD los avalúos asociados a la solicitud
     *
     * @author christian.rodriguez
     */
    private void cargarAvaluosSolicitud() {

        this.avaluosSolicitud = new ArrayList<Avaluo>();

        if (this.solicitudSeleccionada != null) {

            List<Avaluo> avaluos = this.getAvaluosService().consultarAvaluosPorSolicitudId(
                this.solicitudSeleccionada.getId());
            if (avaluos != null) {
                this.avaluosSolicitud.addAll(avaluos);
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Extrae los datos de ....
     *
     * @author christian.rodriguez
     */
    private void extraerDatosArbolProcesos() {
        this.iniciarDatosQuemados();

        this.solicitante = this
            .getTramiteService()
            .buscarSolicitantesSolicitudBySolicitudIdRelacion(
                this.solicitudSeleccionada.getId(),
                ESolicitanteSolicitudRelac.PROPIETARIO.toString()).get(0);
    }

    // --------------------------------------------------------------------------
    /**
     * Extrae los datos de la territorial asociada al usuario
     *
     * @author christian.rodriguez
     */
    private void extraerTerritorialUsuario() {
        if (this.solicitudSeleccionada != null) {

            String codigoTerritorial = this.usuario.getCodigoEstructuraOrganizacional();
            this.territorialUsuario = this.getGeneralesService().obtenerTerritorialPorCodigo(
                codigoTerritorial);
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que carga los predios asociados a la solicitud
     *
     * @author christian.rodriguez
     */
    private void cargarPrediosDisponiblesSolicitud() {

        this.prediosDisponiblesSolicitud = this.getTramiteService()
            .buscarSolicitudPredioSinAvaluoPorSolicitudId(this.solicitudSeleccionada.getId());

    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga los valores necesarios para asociar o desasociar predios de un avaluo
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaAsocDeasocPredio() {

        this.banderaEsActualizacionAvaluo = true;

        this.cargarPrediosDisponiblesSolicitud();

        this.labelBotonGuardar = this.LABEL_BTN_ASOCIAR_PREDIOS;

    }

    // --------------------------------------------------------------------------
    /**
     * Listener que prepara la creación de un avaluo
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaCrearSecRadicado() {

        if (!banderaSolicitudDeRevisionImpugnacion) {

            this.avaluoSeleccionado = new Avaluo();

            this.banderaEsActualizacionAvaluo = false;

            this.cargarPrediosDisponiblesSolicitud();

            this.labelBotonGuardar = this.LABEL_BTN_ASOCIAR_PREDIOS;
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError(
                "No puede agregar mas de un Sec. Radicado a un trámite de este tipo.");
            context.addCallbackParam("error", "error");
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Reincializa las variables usadas en las tablas de asociar y desasociar predios
     *
     * @author christian.rodriguez
     */
    private void reinicializarVariablesdeSeleccion() {
        this.prediosAsociarAvaluo = new SolicitudPredio[]{};
        this.prediosDesasociarAvaluo = new AvaluoPredio[]{};
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que prepara la asociación y desasociación de predios de un avaluo
     *
     * @author christian.rodriguez
     */
    public void cargarAsociarDesasociarPredios() {

        this.reinicializarVariablesdeSeleccion();

        this.cargarPrediosDisponiblesSolicitud();

        this.labelBotonGuardar = this.LABEL_BTN_ASOCIAR_PREDIOS;

    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga los datos necesarios por el managedBean de administración de predios de la
     * solicitud
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaAdminPredios() {

        this.prediosSolicitudSeleccionada = this.getTramiteService()
            .buscarSolicitudesPredioBySolicitudId(this.solicitudSeleccionada.getId());

        this.adminPrediosMB.cargarVariablesExternas(this.solicitudSeleccionada, this.solicitante,
            this.prediosSolicitudSeleccionada, false, this.usuario);

        this.carguePrediosAvaluosMB.cargarVariablesExternas(this.solicitudSeleccionada,
            this.solicitante, this.usuario, this.prediosSolicitudSeleccionada);

        this.administracionInfoContactoDePrediosMB.cargarVariablesExternas(false, this.solicitante,
            this.solicitudSeleccionada, this.contactosSolicitud, this.usuario);
    }

    // --------------------------------------------------------------------------
    /**
     * Método encargado de registrar/modificar el sec radicado. Valida que se hayn seleccionado
     * predios y que todos los predios seleccionados tengan la misma condición jurídica
     *
     * @author christian.rodriguez
     */
    public void crearActualizarSecRadicado() {

        if (this.avaluoSeleccionado != null) {

            RequestContext context = RequestContext.getCurrentInstance();

            if (this.avaluoSeleccionado.getTipoAvaluo() != null) {

                if ((this.prediosAsociarAvaluo != null && this.prediosAsociarAvaluo.length > 0) ||
                     (this.prediosDesasociarAvaluo != null && this.prediosDesasociarAvaluo.length >
                    0)) {

                    if (this.validarCondicionJuridicaPrediosSeleccionados()) {

                        boolean hayError = false;

                        if (!this.banderaEsActualizacionAvaluo) {
                            this.avaluoSeleccionado = this.getAvaluosService()
                                .crearAvaluoRadicacion(this.avaluoSeleccionado,
                                    this.solicitudSeleccionada, this.solicitante,
                                    Arrays.asList(this.prediosAsociarAvaluo), this.usuario);
                        } else {

                            if (this.prediosDesasociarAvaluo != null &&
                                 this.prediosDesasociarAvaluo.length > 0) {

                                if (this.avaluoSeleccionado.getAvaluoPredios().size() ==
                                    this.prediosDesasociarAvaluo.length) {
                                    hayError = true;
                                }
                            }

                            if (!hayError) {

                                this.avaluoSeleccionado = this.getAvaluosService()
                                    .actualizarAvaluoRadicacion(this.avaluoSeleccionado,
                                        this.solicitudSeleccionada,
                                        Arrays.asList(this.prediosAsociarAvaluo),
                                        Arrays.asList(this.prediosDesasociarAvaluo),
                                        this.usuario);

                            } else {

                                this.addMensajeError(
                                    "No puede eliminar todos los predios del avalúo.");
                                context.addCallbackParam("error", "error");
                            }
                        }

                        if (this.avaluoSeleccionado != null &&
                             this.avaluoSeleccionado.getId() != null && !hayError) {

                            this.cargarAvaluosSolicitud();
                            this.cargarPrediosDisponiblesSolicitud();

                            this.addMensajeInfo("Sec radicado " +
                                 avaluoSeleccionado.getSecRadicado() +
                                 " guardado satisfactóriamente.");

                        } else {
                            this.addMensajeError("No fue posible guardar el Sec. Radicado.");
                            context.addCallbackParam("error", "error");
                        }

                    } else {

                        this.addMensajeError(
                            "Debe seleccionar predios que tengan la misma condición " +
                             "jurídica para un Sec. Radicado.");
                        context.addCallbackParam("error", "error");
                    }

                } else {
                    this.addMensajeError(
                        "Debe seleccionar como mínimo un Predio a asociar/desasociar " +
                         "para Guardar el Sec Radicado.");
                    context.addCallbackParam("error", "error");
                }

            } else {
                this.addMensajeError("Debe seleccionar el Tipo de Avalúo a crear.");
                context.addCallbackParam("error", "error");
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Valida que todos los predios seleccionados tienen la misma condición jurídica. Úincamente se
     * valida cuando se están adicionado predios al avalúo
     *
     * @author christian.rodriguez
     * @return true si todos los predios tienen la misma condición jurídica, false en otro caso
     */
    private boolean validarCondicionJuridicaPrediosSeleccionados() {

        if (this.prediosAsociarAvaluo != null && this.prediosAsociarAvaluo.length > 0) {

            String condicionJuridicaAnt = this.prediosAsociarAvaluo[0].getCondicionJuridica();

            for (SolicitudPredio predio : this.prediosAsociarAvaluo) {

                if (!condicionJuridicaAnt.equalsIgnoreCase(predio.getCondicionJuridica())) {
                    return false;
                }
                condicionJuridicaAnt = predio.getCondicionJuridica();
            }
        }
        return true;
    }

    // --------------------------------------------------------------------------
    /**
     * Listeners que se activa al cambiar de pestaña en la ventana de administración de avalúos
     *
     * @author christian.rodriguez
     * @param event evento del cambio de pestaña
     */
    public void onTabChangeAsoDesPrediosTV(TabChangeEvent event) {
        this.reinicializarVariablesdeSeleccion();

        String tabId = event.getTab().getId();

        if (tabId.equals(ID_TAB_ASOCIAR_PREDIOS)) {
            this.labelBotonGuardar = this.LABEL_BTN_ASOCIAR_PREDIOS;
        } else {
            this.labelBotonGuardar = this.LABEL_BTN_DESASOCIAR_PREDIOS;
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listeners que se activa al cerrar el dialogo de adminsitración de avaluo
     *
     * @author christian.rodriguez
     * @param event evento del cambio de pestaña
     */
    public void onCloseAdminAvaluoDialog(CloseEvent event) {
        this.reinicializarVariablesdeSeleccion();
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que se ejecuta al seleccionar la opción de eliminar avalúo en la tabla de avalúos
     * asocaidos a la solicitud
     *
     * @author christian.rodriguez
     */
    public void eliminarSecRadicado() {

        if (this.avaluosSeleccionados != null && this.avaluosSeleccionados.length > 0) {

            boolean avaluoEliminado;

            for (Avaluo avaluo : this.avaluosSeleccionados) {

                if (avaluo.getTramite().getProcesoInstanciaId() == null) {

                    avaluoEliminado = this.getAvaluosService().eliminarAvaluoRadicacion(avaluo,
                        this.solicitudSeleccionada.getId());

                    if (avaluoEliminado) {

                        this.addMensajeInfo("Sec. Radicado " + avaluo.getSecRadicado() +
                             " eliminado exitosamente." +
                             " Algunos Sec.Radicado pueden haber sido re enumerados.");

                    } else {
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");

                        this.addMensajeError("No fue posible eliminar el Sec. Radicado " +
                             avaluo.getSecRadicado() + ".");
                    }

                } else {

                    this.addMensajeError("No fue posible eliminar el Sec. Radicado " +
                         avaluo.getSecRadicado() +
                         " porque ya se encuentra en otro estado del proceso.");

                }

            }

            this.cargarAvaluosSolicitud();
            this.cargarPrediosDisponiblesSolicitud();

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeError("Debe seleccionar al menos un avalúo para eliminar.");
        }

    }

    // --------------------------------------------------------------------------
    /**
     * Listener del botón de avanzar avalúos. Crea los procesos para los avalúos seleccionados y
     * actualiza los sec radicados de los avalúos que aún no se hayan avanzado
     *
     * @author christian.rodriguez
     */
    public void avanzarAvaluos() {
        if (this.avaluosSeleccionados != null && this.avaluosSeleccionados.length > 0) {

            boolean avaluosAvanzados = this.getAvaluosService().avanzarAvaluosRadicacion(
                Arrays.asList(this.avaluosSeleccionados), this.solicitudSeleccionada.getId());

            if (avaluosAvanzados) {

                this.cargarAvaluosSolicitud();

                this.addMensajeInfo(
                    "Sec.Radicados registrados satisfactoriamente en el gestor de procesos." +
                     " Algunos Sec.Radicado pueden haber sido re enumerados.");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeError(
                    "Hubo errores al avanzar los Sec.Radicados. Intentelo nuevamente.");
            }

        }
    }

    // --------------------------------------------------------------------------
    public String cerrar() {

        LOGGER.debug("iniciando CreacionAvaluoRadicacionMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("finalizando CreacionAvaluoRadicacionMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

}
