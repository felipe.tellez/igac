package co.gov.igac.snc.web.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.util.Constantes;

/**
 * Conversor para obtener de cualquier dominio a partir del Código su respectivo Valor
 *
 * @author fredy.wilches
 *
 */
//TODO :: fredy.wilches :: 17-05-2012 :: no se entiende para qué es la clase :: pedro.garcia
public class CalendarConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CalendarConverter.class);

    @Override
    public Object getAsObject(FacesContext paramFacesContext,
        UIComponent paramUIComponent, String paramString) {

        return paramString;
    }

    @Override
    public String getAsString(FacesContext paramFacesContext,
        UIComponent paramUIComponent, Object paramObject) {
        SimpleDateFormat sdf =
            new SimpleDateFormat(Constantes.FORMATO_FECHA);
        return sdf.format(((Calendar) paramObject).getTime());
    }

}
