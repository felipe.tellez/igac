package co.gov.igac.snc.web.mb.conservacion.reportes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.persistence.entity.generales.Grupo;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaParametrizacionReporte;
import co.gov.igac.snc.util.RepUsuarioRolReporteAgrupadoDTO;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * @description Managed bean para gestionar la parametrización de los diferentes tipos de reportes y
 * la asignación de permisos a los usuarios para cada uno de ellos.
 *
 * @cu CU-NP-CO-262 Parametrizar Usuarios de Reportes
 *
 * @version 1.0
 *
 * @author david.cifuentes
 */
@Component("parametrizacionUsuarioReporte")
@Scope("session")
public class ParametrizacionUsuariosReportesMB extends SNCManagedBean implements
    Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = 6460179644515118053L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ParametrizacionUsuariosReportesMB.class);

    /**
     * ----------------------------------
     */
    /**
     * ----------- SERVICIOS ------------
     */
    /**
     * ----------------------------------
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * ----------------------------------
     */
    /**
     * ----------- VARIABLES ------------
     */
    /**
     * ----------------------------------
     */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable usada para almacenar el código de la categoría del reporte
     */
    public String categoriaReporte;

    /**
     * Variable usada para almacenar el cdigo del tipo de reporte seleccionado
     */
    private Long tipoReporteId;

    /**
     * Lista de select items de los tipos de reportes
     */
    private List<SelectItem> tipoReportesItemList;

    /**
     * Variable usada para capturar el código de una territorial seleccionada de la lista de
     * territoriales.
     */
    private String territorialSeleccionadaCodigo;

    /**
     * Lista de select items de las territoriales
     */
    private ArrayList<SelectItem> territorialesItemList;

    /**
     * Variable usada para capturar el código de una UOC seleccionada de la lista de UOCs de la
     * territorial.
     */
    private String unidadOperativaSeleccionadaCodigo;

    /**
     * Lista de select items de las UOC.
     */
    private List<SelectItem> unidadesOperativasCatastroItemList;

    /**
     * Variable que almacena el código del departamento seleccionado
     */
    private String departamentoSeleccionadoCodigo;

    /**
     * Lista de select items de los departamentos parametrizados
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Variable que almacena el código del municipio seleccionado
     */
    private String municipioSeleccionadoCodigo;

    /**
     * Lista de select items de los municipios parametrizados
     */
    private List<SelectItem> municipiosItemList;

    /**
     * Variable que almacena el login del funcionario seleccionado
     */
    private String rolSeleccionado;

    /**
     * Lista de select items de los funcionarios parametrizados
     */
    private List<SelectItem> rolesItemList;

    /**
     * Variable que almacena el login del funcionario seleccionado
     */
    private String funcionarioSeleccionado;

    /**
     * Lista de select items de los funcionarios parametrizados
     */
    private List<SelectItem> funcionariosItemList;

    /**
     * Variable que almacena el estado seleccionado para una parametrización
     */
    private String estadoSeleccionado;

    /**
     * Lista de parametrizaciones resultado de una búsqueda por filtros
     */
    private List<RepUsuarioRolReporteAgrupadoDTO> parametrizacionesExistentes;

    /**
     * Variable usada para almacenar una parametrización seleccionada
     */
    private RepUsuarioRolReporteAgrupadoDTO parametrizacionSeleccionada;

    /**
     * Variable usada para controlar el cargue de una pantalla en modo de edición o creación de
     * parametrizaciones.
     */
    private boolean editMode;

    /**
     * Variable que tiene los campos de consulta de la parametrización
     */
    private FiltroDatosConsultaParametrizacionReporte datosConsultaParametrizacion;

    /**
     * Variable usada para almacenar el código de la categoría del reporte en la pantalla de detalle
     * de parametrización
     */
    private String categoriaReporteParametrizando;

    /**
     * Variables usada para la administración del pickList para cargar los tipos de reporte
     */
    private DualListModel<RepReporte> tipoReportePickList;
    private List<RepReporte> sourceTipoReporteParametrizacion;
    private List<RepReporte> targetTipoReporteParametrizacion;

    /**
     * Variables usada para la administración del pickList para cargar las territoriales
     */
    private DualListModel<EstructuraOrganizacional> territorialPickList;
    private List<EstructuraOrganizacional> sourceTerritorialParametrizacion;
    private List<EstructuraOrganizacional> targetTerritorialParametrizacion;

    /**
     * Variables usada para la administración del pickList para cargar las UOCs
     */
    private DualListModel<EstructuraOrganizacional> uocPickList;
    private List<EstructuraOrganizacional> sourceUOCParametrizacion;
    private List<EstructuraOrganizacional> targetUOCParametrizacion;

    /**
     * Variables usada para la administración del pickList para cargar los departamentos
     */
    private DualListModel<Departamento> departamentoPickList;
    private List<Departamento> sourceDepartamentoParametrizacion;
    private List<Departamento> targetDepartamentoParametrizacion;

    /**
     * Variables usada para la administración del pickList para cargar los municipios de reporte
     */
    private DualListModel<Municipio> municipioPickList;
    private List<Municipio> sourceMunicipioParametrizacion;
    private List<Municipio> targetMunicipioParametrizacion;

    /**
     * Variables usada para la administración del pickList para cargar los roles de reporte
     */
    private DualListModel<String> rolPickList;
    private List<String> sourceRolParametrizacion;
    private List<String> targetRolParametrizacion;

    /**
     * Variables usada para la administración del pickList para cargar los usuarios de reporte
     */
    private DualListModel<UsuarioDTO> usuarioPickList;
    private List<UsuarioDTO> sourceUsuarioParametrizacion;
    private List<UsuarioDTO> targetUsuarioParametrizacion;

    /**
     * Variable que almacena el estado seleccionado para una parametrización
     */
    private String estadoReporteParametrizando;

    /**
     * Variables para almacenar los permisos de generación y consulta del reporte
     */
    private boolean consultarBoolParametrizando;
    private boolean generarBoolParametrizando;
    private boolean estadoPermisoConsultaBool;

    /**
     * Variable que almacena los valores de los roles de LDAP parametrizados para el SNC
     */
    private List<String> rolesSNC;

    /**
     * Variables usadas para determinar que tipo de usuario se encuentra logueado y de esta manera
     * cargar los diferentes menus y opciones. 1 - UOC 2 - Territorial 3 - Sede central
     */
    private int tipoUsuario;

    private Map<String, Boolean> lideresTecnicos;

    /**
     * Mapa para mantener los roles asociados a los usuarios que se pierden por el componente
     * picklist
     */
    private Map<String, String[]> rolesUsuarios = new HashMap<String, String[]>();

    private boolean usuarioDelegado;
    
    private boolean usuarioHabilitado;

    private List<Municipio> municipiosDelegados;
    
    private List<Municipio> municipiosHabilitados;

    /**
     * ----------------------------------
     */
    /**
     * -------------- INIT --------------
     */
    /**
     * ----------------------------------
     */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - ParametrizacionUsuariosReportesMB");

        try {
            this.usuario = MenuMB.getMenu().getUsuarioDto();
            this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
            this.usuarioHabilitado = MenuMB.getMenu().isUsuarioHabilitado();
            this.municipiosDelegados = this.getGeneralesService().obtenerMunicipiosDelegados();
            this.municipiosHabilitados = this.getGeneralesService().obtenerMunicipiosHabilitados();

            // Inicialización de variables
            this.inicializar();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(
                "Ocurrió un error en el cargue de la informaciòn de parametrización.");
        }
    }

    /**
     * ----------------------------------
     */
    /**
     * ------- GETTERS Y SETTERS --------
     */
    /**
     * ----------------------------------
     */
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public boolean isUsuarioHabilitado() {
        return usuarioHabilitado;
    }

    public void setUsuarioHabilitado(boolean usuarioHabilitado) {
        this.usuarioHabilitado = usuarioHabilitado;
    }
    
    public List<Municipio> getMunicipiosDelegados() {
        return municipiosDelegados;
    }

    public void setMunicipiosDelegados(List<Municipio> municipiosDelegados) {
        this.municipiosDelegados = municipiosDelegados;
    }

    public String getCategoriaReporte() {
        return categoriaReporte;
    }

    public void setCategoriaReporte(String categoriaReporte) {
        this.categoriaReporte = categoriaReporte;
    }

    public Long getTipoReporteId() {
        return tipoReporteId;
    }

    public void setTipoReporteId(Long tipoReporteId) {
        this.tipoReporteId = tipoReporteId;
    }

    public List<SelectItem> getTipoReportesItemList() {
        return tipoReportesItemList;
    }

    public void setTipoReportesItemList(List<SelectItem> tipoReportesItemList) {
        this.tipoReportesItemList = tipoReportesItemList;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return territorialesItemList;
    }

    public void setTerritorialesItemList(ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public String getUnidadOperativaSeleccionadaCodigo() {
        return unidadOperativaSeleccionadaCodigo;
    }

    public void setUnidadOperativaSeleccionadaCodigo(
        String unidadOperativaSeleccionadaCodigo) {
        this.unidadOperativaSeleccionadaCodigo = unidadOperativaSeleccionadaCodigo;
    }

    public DualListModel<EstructuraOrganizacional> getTerritorialPickList() {
        return territorialPickList;
    }

    public void setTerritorialPickList(DualListModel<EstructuraOrganizacional> territorialPickList) {
        this.territorialPickList = territorialPickList;
    }

    public boolean isEstadoPermisoConsultaBool() {
        return estadoPermisoConsultaBool;
    }

    public void setEstadoPermisoConsultaBool(boolean estadoPermisoConsultaBool) {
        this.estadoPermisoConsultaBool = estadoPermisoConsultaBool;
    }

    public List<EstructuraOrganizacional> getSourceTerritorialParametrizacion() {
        return sourceTerritorialParametrizacion;
    }

    public void setSourceTerritorialParametrizacion(
        List<EstructuraOrganizacional> sourceTerritorialParametrizacion) {
        this.sourceTerritorialParametrizacion = sourceTerritorialParametrizacion;
    }

    public List<String> getRolesSNC() {
        return rolesSNC;
    }

    public void setRolesSNC(List<String> rolesSNC) {
        this.rolesSNC = rolesSNC;
    }

    public DualListModel<Departamento> getDepartamentoPickList() {
        return departamentoPickList;
    }

    public void setDepartamentoPickList(
        DualListModel<Departamento> departamentoPickList) {
        this.departamentoPickList = departamentoPickList;
    }

    public List<Departamento> getSourceDepartamentoParametrizacion() {
        return sourceDepartamentoParametrizacion;
    }

    public void setSourceDepartamentoParametrizacion(
        List<Departamento> sourceDepartamentoParametrizacion) {
        this.sourceDepartamentoParametrizacion = sourceDepartamentoParametrizacion;
    }

    public DualListModel<String> getRolPickList() {
        return rolPickList;
    }

    public void setRolPickList(DualListModel<String> rolPickList) {
        this.rolPickList = rolPickList;
    }

    public List<String> getSourceRolParametrizacion() {
        return sourceRolParametrizacion;
    }

    public void setSourceRolParametrizacion(
        List<String> sourceRolParametrizacion) {
        this.sourceRolParametrizacion = sourceRolParametrizacion;
    }

    public List<String> getTargetRolParametrizacion() {
        return targetRolParametrizacion;
    }

    public void setTargetRolParametrizacion(
        List<String> targetRolParametrizacion) {
        this.targetRolParametrizacion = targetRolParametrizacion;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public DualListModel<UsuarioDTO> getUsuarioPickList() {
        return usuarioPickList;
    }

    public void setUsuarioPickList(DualListModel<UsuarioDTO> usuarioPickList) {
        this.usuarioPickList = usuarioPickList;
    }

    public List<UsuarioDTO> getSourceUsuarioParametrizacion() {
        return sourceUsuarioParametrizacion;
    }

    public void setSourceUsuarioParametrizacion(
        List<UsuarioDTO> sourceUsuarioParametrizacion) {
        this.sourceUsuarioParametrizacion = sourceUsuarioParametrizacion;
    }

    public List<UsuarioDTO> getTargetUsuarioParametrizacion() {
        return targetUsuarioParametrizacion;
    }

    public void setTargetUsuarioParametrizacion(
        List<UsuarioDTO> targetUsuarioParametrizacion) {
        this.targetUsuarioParametrizacion = targetUsuarioParametrizacion;
    }

    public List<Departamento> getTargetDepartamentoParametrizacion() {
        return targetDepartamentoParametrizacion;
    }

    public void setTargetDepartamentoParametrizacion(
        List<Departamento> targetDepartamentoParametrizacion) {
        this.targetDepartamentoParametrizacion = targetDepartamentoParametrizacion;
    }

    public DualListModel<Municipio> getMunicipioPickList() {
        return municipioPickList;
    }

    public void setMunicipioPickList(DualListModel<Municipio> municipioPickList) {
        this.municipioPickList = municipioPickList;
    }

    public List<Municipio> getSourceMunicipioParametrizacion() {
        return sourceMunicipioParametrizacion;
    }

    public void setSourceMunicipioParametrizacion(
        List<Municipio> sourceMunicipioParametrizacion) {
        this.sourceMunicipioParametrizacion = sourceMunicipioParametrizacion;
    }

    public List<Municipio> getTargetMunicipioParametrizacion() {
        return targetMunicipioParametrizacion;
    }

    public void setTargetMunicipioParametrizacion(
        List<Municipio> targetMunicipioParametrizacion) {
        this.targetMunicipioParametrizacion = targetMunicipioParametrizacion;
    }

    public List<EstructuraOrganizacional> getTargetTerritorialParametrizacion() {
        return targetTerritorialParametrizacion;
    }

    public void setTargetTerritorialParametrizacion(
        List<EstructuraOrganizacional> targetTerritorialParametrizacion) {
        this.targetTerritorialParametrizacion = targetTerritorialParametrizacion;
    }

    public DualListModel<EstructuraOrganizacional> getUocPickList() {
        return uocPickList;
    }

    public void setUocPickList(DualListModel<EstructuraOrganizacional> uocPickList) {
        this.uocPickList = uocPickList;
    }

    public List<EstructuraOrganizacional> getSourceUOCParametrizacion() {
        return sourceUOCParametrizacion;
    }

    public void setSourceUOCParametrizacion(
        List<EstructuraOrganizacional> sourceUOCParametrizacion) {
        this.sourceUOCParametrizacion = sourceUOCParametrizacion;
    }

    public List<EstructuraOrganizacional> getTargetUOCParametrizacion() {
        return targetUOCParametrizacion;
    }

    public void setTargetUOCParametrizacion(
        List<EstructuraOrganizacional> targetUOCParametrizacion) {
        this.targetUOCParametrizacion = targetUOCParametrizacion;
    }

    public String getCategoriaReporteParametrizando() {
        return categoriaReporteParametrizando;
    }

    public void setCategoriaReporteParametrizando(
        String categoriaReporteParametrizando) {
        this.categoriaReporteParametrizando = categoriaReporteParametrizando;
    }

    public List<SelectItem> getUnidadesOperativasCatastroItemList() {
        return unidadesOperativasCatastroItemList;
    }

    public void setUnidadesOperativasCatastroItemList(
        List<SelectItem> unidadesOperativasCatastroItemList) {
        this.unidadesOperativasCatastroItemList = unidadesOperativasCatastroItemList;
    }

    public String getDepartamentoSeleccionadoCodigo() {
        return departamentoSeleccionadoCodigo;
    }

    public void setDepartamentoSeleccionadoCodigo(
        String departamentoSeleccionadoCodigo) {
        this.departamentoSeleccionadoCodigo = departamentoSeleccionadoCodigo;
    }

    public boolean isConsultarBoolParametrizando() {
        return consultarBoolParametrizando;
    }

    public void setConsultarBoolParametrizando(boolean consultarBoolParametrizando) {
        this.consultarBoolParametrizando = consultarBoolParametrizando;
    }

    public boolean isGenerarBoolParametrizando() {
        return generarBoolParametrizando;
    }

    public void setGenerarBoolParametrizando(boolean generarBoolParametrizando) {
        this.generarBoolParametrizando = generarBoolParametrizando;
    }

    public String getEstadoReporteParametrizando() {
        return estadoReporteParametrizando;
    }

    public void setEstadoReporteParametrizando(String estadoReporteParametrizando) {
        this.estadoReporteParametrizando = estadoReporteParametrizando;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public String getMunicipioSeleccionadoCodigo() {
        return municipioSeleccionadoCodigo;
    }

    public void setMunicipioSeleccionadoCodigo(String municipioSeleccionadoCodigo) {
        this.municipioSeleccionadoCodigo = municipioSeleccionadoCodigo;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public List<SelectItem> getRolesItemList() {
        return rolesItemList;
    }

    public void setRolesItemList(List<SelectItem> rolesItemList) {
        this.rolesItemList = rolesItemList;
    }

    public String getFuncionarioSeleccionado() {
        return funcionarioSeleccionado;
    }

    public void setFuncionarioSeleccionado(String funcionarioSeleccionado) {
        this.funcionarioSeleccionado = funcionarioSeleccionado;
    }

    public List<SelectItem> getFuncionariosItemList() {
        return funcionariosItemList;
    }

    public void setFuncionariosItemList(List<SelectItem> funcionariosItemList) {
        this.funcionariosItemList = funcionariosItemList;
    }

    public String getEstadoSeleccionado() {
        return estadoSeleccionado;
    }

    public void setEstadoSeleccionado(String estadoSeleccionado) {
        this.estadoSeleccionado = estadoSeleccionado;
    }

    public List<RepReporte> getSourceTipoReporteParametrizacion() {
        return sourceTipoReporteParametrizacion;
    }

    public void setSourceTipoReporteParametrizacion(
        List<RepReporte> sourceTipoReporteParametrizacion) {
        this.sourceTipoReporteParametrizacion = sourceTipoReporteParametrizacion;
    }

    public List<RepReporte> getTargetTipoReporteParametrizacion() {
        return targetTipoReporteParametrizacion;
    }

    public void setTargetTipoReporteParametrizacion(
        List<RepReporte> targetTipoReporteParametrizacion) {
        this.targetTipoReporteParametrizacion = targetTipoReporteParametrizacion;
    }

    public List<RepUsuarioRolReporteAgrupadoDTO> getParametrizacionesExistentes() {
        return parametrizacionesExistentes;
    }

    public void setParametrizacionesExistentes(
        List<RepUsuarioRolReporteAgrupadoDTO> parametrizacionesExistentes) {
        this.parametrizacionesExistentes = parametrizacionesExistentes;
    }

    public DualListModel<RepReporte> getTipoReportePickList() {
        return tipoReportePickList;
    }

    public void setTipoReportePickList(DualListModel<RepReporte> tipoReportePickList) {
        this.tipoReportePickList = tipoReportePickList;
    }

    public RepUsuarioRolReporteAgrupadoDTO getParametrizacionSeleccionada() {
        return parametrizacionSeleccionada;
    }

    public void setParametrizacionSeleccionada(
        RepUsuarioRolReporteAgrupadoDTO parametrizacionSeleccionada) {
        this.parametrizacionSeleccionada = parametrizacionSeleccionada;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * --------------------------------
     */
    /**
     * ----------- MÉTODOS ------------
     */
    /**
     * --------------------------------
     */
    /**
     * Método que realiza la inicialización de las variables de la búsqueda de parametrizaciones de
     * usuario
     *
     * @author david.cifuentes
     */
    public void inicializar() {

        this.territorialesItemList = new ArrayList<SelectItem>();
        this.unidadesOperativasCatastroItemList = new ArrayList<SelectItem>();
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList = new ArrayList<SelectItem>();
        this.rolesItemList = new ArrayList<SelectItem>();
        this.funcionariosItemList = new ArrayList<SelectItem>();
        this.parametrizacionesExistentes = new ArrayList<RepUsuarioRolReporteAgrupadoDTO>();
        this.tipoReportesItemList = new ArrayList<SelectItem>();

        this.tipoReporteId = null;
        this.territorialSeleccionadaCodigo = null;
        this.unidadOperativaSeleccionadaCodigo = null;
        this.departamentoSeleccionadoCodigo = null;
        this.municipioSeleccionadoCodigo = null;
        this.rolSeleccionado = null;
        this.funcionarioSeleccionado = null;
        this.estadoSeleccionado = null;
        this.categoriaReporte = null;

        this.parametrizacionSeleccionada = new RepUsuarioRolReporteAgrupadoDTO();

        // Tipo de usuario
        if (this.usuario.getCodigoUOC() != null) {
            // Usuario UOC
            this.tipoUsuario = 1;
        } else if (this.usuario.getCodigoTerritorial() != null &&
             !this.usuario.getCodigoTerritorial().equals(Constantes.DIRECCION_GENERAL_CODIGO)) {
            // Usuario Territorial
            this.tipoUsuario = 2;
        } else {
            // Sede Central
            this.tipoUsuario = 3;
        }

        // ----------------------------- //
        // --- Menú de territoriales --- //
        // ----------------------------- //
        switch (this.tipoUsuario) {
            case 3:
                // Sede central
                for (EstructuraOrganizacional territorial : this
                    .getGeneralesService().getCacheTerritoriales()) {
                    this.territorialesItemList.add(new SelectItem(territorial
                        .getCodigo(), territorial.getNombre()));
                }
                break;
            default:
                // Territorial y UOC
                EstructuraOrganizacional territorial = this.buscarTerritorial(this.usuario.
                    getCodigoTerritorial());
                this.territorialesItemList.add(new SelectItem(territorial
                    .getCodigo(), territorial.getNombre()));
                break;
        }

        // ----------------------------- //
        // ------- Menú de roles ------- //
        // ----------------------------- //
        this.rolesSNC = new ArrayList<String>();
        List<Grupo> rolesSNCTemp = this.getGeneralesService()
            .consultarGrupoPorEstadoActivo(ESiNo.SI.getCodigo());

        if (rolesSNCTemp != null && !rolesSNCTemp.isEmpty()) {
            for (Grupo g : rolesSNCTemp) {
                this.rolesSNC.add(g.getNombre());
            }
        }

        if (this.usuario.getCodigoUOC() != null) {
            this.territorialSeleccionadaCodigo = this.usuario.getCodigoTerritorial();
            this.unidadOperativaSeleccionadaCodigo = this.usuario.getCodigoUOC();
        } else {
            this.territorialSeleccionadaCodigo = this.usuario.getCodigoTerritorial();
        }

        this.cargarRoles();
        this.cargarListaUOC();
        this.cargarDepartamentos();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar la categoría de un reporte y realiza el cargue de los
     * tipos de reporte de esa categoría
     *
     * @author david.cifuentes
     */
    public void cambioCategoriaReporte() {

        this.tipoReportesItemList.clear();

        if (this.categoriaReporte != null) {
            List<RepReporte> repReportes = this.getGeneralesService()
                .buscarRepReportesPorCategoria(this.categoriaReporte);

            if (repReportes != null) {
                for (RepReporte repReporte : repReportes) {
                    this.tipoReportesItemList.add(new SelectItem(repReporte
                        .getId(), repReporte.getNombre()));
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @author david.cifuentes
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {
        if (codigoTerritorial != null) {
            EstructuraOrganizacional estructuraOrganizacional, answer = null;
            for (EstructuraOrganizacional eo : this.getGeneralesService()
                .buscarTodasTerritoriales()) {
                if (eo.getCodigo().equals(codigoTerritorial)) {
                    answer = eo;
                    break;
                }
            }
            if (answer != null) {
                estructuraOrganizacional = this
                    .getGeneralesService()
                    .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
                if (estructuraOrganizacional != null) {
                    return estructuraOrganizacional;
                } else {
                    return answer;
                }
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el llamado a los métodos de cargue de las UOCs y los departamentos, y
     * realiza el set de la UOC seleccionada.
     *
     * @author david.cifuentes
     * @return
     */
    public void cargarUOC() {

        this.unidadesOperativasCatastroItemList.clear();
        if (this.territorialSeleccionadaCodigo != null) {
            this.cargarListaUOC();
            this.cargarUOCSeleccionada();
            this.cargarDepartamentos();
            this.cargarRoles();
        } else {
            this.rolesItemList = null;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el set por default de la UOC del usuario.
     *
     * @author david.cifuentes
     */
    public void cargarUOCSeleccionada() {
        if (this.usuario.isUoc()) {
            this.unidadOperativaSeleccionadaCodigo = this.usuario
                .getCodigoUOC();
        } else {
            this.unidadOperativaSeleccionadaCodigo = null;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Mètodo que realiza el cargue de unidades operativas de catastro cuando se ha seleccionado una
     * territorial
     *
     * @author david.cifuentes
     */
    public void cargarListaUOC() {
        EstructuraOrganizacional territorial = this.buscarTerritorial(
            this.territorialSeleccionadaCodigo);

        if (!territorial.isDireccionGeneral()) {

            List<EstructuraOrganizacional> unidadesOperativasCatastro = this
                .getGeneralesService()
                .getCacheEstructuraOrganizacionalPorTipoYPadre(
                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                    this.territorialSeleccionadaCodigo);

            if (unidadesOperativasCatastro != null) {

                if (this.tipoUsuario == 1) {
                    for (EstructuraOrganizacional eo : unidadesOperativasCatastro) {
                        if (eo.getCodigo().equals(this.usuario.getCodigoUOC())) {
                            this.unidadesOperativasCatastroItemList.add(new SelectItem(
                                eo.getCodigo(), eo.getNombre()));
                        }
                    }
                } else {
                    for (EstructuraOrganizacional eo : unidadesOperativasCatastro) {
                        this.unidadesOperativasCatastroItemList.add(new SelectItem(
                            eo.getCodigo(), eo.getNombre()));
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de la lista de {@link Departamento}
     *
     * @author david.cifuentes
     */
    public void cargarDepartamentos() {

        this.departamentosItemList.clear();

        if (this.territorialSeleccionadaCodigo != null &&
            !this.territorialSeleccionadaCodigo.equals(Constantes.DIRECCION_GENERAL_CODIGO)) {

            List<Departamento> departamentosList = null;

            if (this.unidadOperativaSeleccionadaCodigo != null ||
                 this.tipoUsuario == 1) {

                String uocCodigo = null;
                if (this.unidadOperativaSeleccionadaCodigo != null) {
                    uocCodigo = this.unidadOperativaSeleccionadaCodigo;
                } else {
                    uocCodigo = this.usuario.getCodigoUOC();
                }

                departamentosList = this.getGeneralesService()
                    .getCacheDepartamentosPorTerritorial(
                        uocCodigo);
            } else {
                departamentosList = this.getGeneralesService()
                    .getCacheDepartamentosPorTerritorial(
                        this.territorialSeleccionadaCodigo);
            }

            if (departamentosList != null) {
                for (Departamento departamento : departamentosList) {
                    this.departamentosItemList.add(new SelectItem(departamento
                        .getCodigo(), departamento.getNombre()));
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la actualización de los municipios al seleccionar un departamento.
     *
     * @author david.cifuentes
     */
    public void cargarMunicipios() {

        this.municipiosItemList.clear();

        if (this.departamentoSeleccionadoCodigo != null) {
            List<Municipio> municipiosList;
            if (this.usuarioHabilitado) {
                municipiosList = this.getGeneralesService()
                        .getCacheMunicipiosPorCodigoEstructuraOrganizacional(territorialSeleccionadaCodigo);
            } else {
                municipiosList = this.getGeneralesService()
                        .getCacheMunicipiosByDepartamento(
                                this.departamentoSeleccionadoCodigo);
            }

            if (this.tipoUsuario == 1) {
                EstructuraOrganizacional uoc = this.buscarTerritorial(this.usuario.getCodigoUOC());
                if (uoc != null) {
                    List<EstructuraOrganizacional> uocList =
                        new ArrayList<EstructuraOrganizacional>();
                    uocList.add(uoc);
                    municipiosList = this.filtrarMunicipioPorUOC(municipiosList, uocList);
                }
            }

            if (municipiosList != null) {
                for (Municipio municipio : municipiosList) {
                    
                    if (!this.usuarioDelegado) {
                        boolean delegado = false;
                        for (Municipio mun : this.municipiosDelegados) {
                            if (mun.getCodigo().equals(municipio.getCodigo())) {
                                delegado = true;
                            }
                        }
                        if (!delegado) {
                            this.municipiosItemList.add(new SelectItem(municipio
                                    .getCodigo(), municipio.getNombre()));
                        }
                    } else {
                        this.municipiosItemList.add(new SelectItem(municipio
                                .getCodigo(), municipio.getNombre()));
                    }

                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la actualización de los roles al seleccionar una territorial.
     *
     * @author david.cifuentes
     */
    public void cargarRoles() {

        this.rolesItemList = new ArrayList<SelectItem>();
        List<SelectItem> rolesItemListTemp = new ArrayList<SelectItem>();
        List<String> rolesListTemp = new ArrayList<String>();
        this.rolSeleccionado = null;

        if (this.territorialSeleccionadaCodigo != null) {

            Map<String, List<SelectItem>> rolesEstructuraOrganizacional =
                new HashMap<String, List<SelectItem>>();

            if (this.usuario.isUoc()) {
                if (this.unidadOperativaSeleccionadaCodigo != null) {
                    EstructuraOrganizacional uoc = this.buscarTerritorial(
                        this.unidadOperativaSeleccionadaCodigo);

                    if (uoc != null) {
                        rolesEstructuraOrganizacional = this
                            .cargarRolesEstructuraOrganizacional(
                                new EstructuraOrganizacional(
                                    this.unidadOperativaSeleccionadaCodigo,
                                    uoc.getNombre(), null, null,
                                    null), rolesEstructuraOrganizacional,
                                false);

                        if (rolesEstructuraOrganizacional != null &&
                             rolesEstructuraOrganizacional.get(
                                this.unidadOperativaSeleccionadaCodigo) != null &&
                             !rolesEstructuraOrganizacional.get(
                                this.unidadOperativaSeleccionadaCodigo).isEmpty()) {
                            rolesItemListTemp.addAll(rolesEstructuraOrganizacional
                                .get(this.unidadOperativaSeleccionadaCodigo));
                        }
                    }
                }
            } else {

                List<EstructuraOrganizacional> listaTerritorialYUOC =
                    new ArrayList<EstructuraOrganizacional>();
                boolean esTerritorial = false;

                EstructuraOrganizacional territorial = this.buscarTerritorial(
                    this.territorialSeleccionadaCodigo);
                if (territorial != null) {
                    listaTerritorialYUOC.add(territorial);
                }

                List<EstructuraOrganizacional> UOCs = this
                    .getGeneralesService()
                    .getCacheEstructuraOrganizacionalPorTipoYPadre(
                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                        this.territorialSeleccionadaCodigo);
                if (UOCs != null && !UOCs.isEmpty()) {
                    listaTerritorialYUOC.addAll(UOCs);
                }

                for (EstructuraOrganizacional eo : listaTerritorialYUOC) {
                    if (eo != null) {
                        if (eo.getCodigo().equals(this.territorialSeleccionadaCodigo)) {
                            esTerritorial = true;
                        } else {
                            esTerritorial = false;
                        }
                        rolesEstructuraOrganizacional = this
                            .cargarRolesEstructuraOrganizacional(
                                new EstructuraOrganizacional(
                                    eo.getCodigo(),
                                    eo.getNombre(), null, null,
                                    null), rolesEstructuraOrganizacional,
                                esTerritorial);

                        if (rolesEstructuraOrganizacional != null &&
                             rolesEstructuraOrganizacional.get(eo.getCodigo()) != null &&
                             !rolesEstructuraOrganizacional.get(eo.getCodigo()).isEmpty()) {
                            for (SelectItem si : rolesEstructuraOrganizacional.get(eo.getCodigo())) {
                                if (!rolesListTemp.contains(si.getValue().toString())) {
                                    rolesItemListTemp.add(si);
                                    rolesListTemp.add(si.getValue().toString());
                                }
                            }
                        }
                    }
                }
            }
        }

        if (rolesItemListTemp != null) {
            Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem s1, SelectItem s2) {
                    return s1.getValue().toString().compareTo(s2.getValue().toString());
                }
            };
            Collections.sort(rolesItemListTemp, comparator);
            this.rolesItemList = rolesItemListTemp;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de roles de una estructura organizacional seleccionada
     *
     * @param estructuraOrganizacional
     * @author david.cifuentes
     * @return
     */
    public Map<String, List<SelectItem>> cargarRolesEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacional,
        Map<String, List<SelectItem>> rolesEstructuraOrganizacional,
        boolean esTerritorial) {

        List<SelectItem> rolesTerritorial = new ArrayList<SelectItem>();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();

        this.rolesItemList = new ArrayList<SelectItem>();
        List<String> roles = new ArrayList<String>();

        String nombreUOC = estructuraOrganizacional.getNombre().replace(' ', '_');

        if (this.usuario.getCodigoTerritorial() != null &&
            !this.usuario.getCodigoTerritorial().equals(Constantes.DIRECCION_GENERAL_CODIGO)) {
            if (!esTerritorial) {
                usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(this.usuario.
                    getDescripcionTerritorial(), nombreUOC);
            } else {
                usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(
                    estructuraOrganizacional.getNombre(), null);
            }
        } else {
            usuarios = this.getTramiteService().buscarTodosFuncionarios();
        }

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para " +
                 estructuraOrganizacional.getNombre());
            return null;
        }

        for (UsuarioDTO user : usuarios) {
            if (user.getRoles() != null) {
                for (String rol : user.getRoles()) {

                    if (!roles.contains(rol) && this.containsRol(rol)) {
                        roles.add(rol);
                    }

                    if (roles != null) {
                        Collections.sort(roles);
                    }
                }
            }
        }

        if (roles != null) {
            for (String rol : roles) {
                rolesTerritorial.add(new SelectItem(rol, rol));
            }
            rolesEstructuraOrganizacional.
                put(estructuraOrganizacional.getCodigo(), rolesTerritorial);
        }
        return rolesEstructuraOrganizacional;
    }

    // ----------------------------------------------------- //
    /**
     * Método encargado de cargar los funcionarios en una lista de de SelecItem para sede central
     *
     * @author david.cifuentes
     * @return
     */
    public List<SelectItem> cargarRolesSedeCentral() {

        List<SelectItem> rolesTerritorial = new ArrayList<SelectItem>();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();

        this.rolesItemList = new ArrayList<SelectItem>();
        List<String> roles = new ArrayList<String>();

        usuarios = this.getTramiteService().buscarTodosFuncionarios();

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para sede central");
            return null;
        }

        for (UsuarioDTO user : usuarios) {
            if (user.getRoles() != null) {
                for (String rol : user.getRoles()) {

                    if (!roles.contains(rol) && this.containsRol(rol)) {
                        roles.add(rol);
                    }

                    if (roles != null) {
                        Collections.sort(roles);
                    }
                }
            }
        }

        if (roles != null) {
            for (String rol : roles) {
                rolesTerritorial.add(new SelectItem(rol, rol));
            }
        }
        return rolesTerritorial;
    }

    // ----------------------------------------------------- //
    /**
     * Método encargado de cargar los funcionarios en una lista de de SelecItem asociados a la
     * territorial seleccionada.
     *
     * @author david.cifuentes
     */
    public void cargarFuncionarios() {

        this.funcionariosItemList.clear();

        if (this.rolSeleccionado != null && !this.rolSeleccionado.isEmpty()) {

            List<UsuarioDTO> funcionarios;
            String codigoUOSeleccionada = "";

            if (this.territorialSeleccionadaCodigo != null) {

                if (this.unidadOperativaSeleccionadaCodigo != null) {

                    List<EstructuraOrganizacional> unidadesOperativasCatastro = this
                        .getGeneralesService()
                        .getCacheEstructuraOrganizacionalPorTipoYPadre(
                            EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                            this.territorialSeleccionadaCodigo);

                    for (EstructuraOrganizacional eo : unidadesOperativasCatastro) {
                        if (eo.getCodigo().equals(
                            this.unidadOperativaSeleccionadaCodigo)) {
                            codigoUOSeleccionada = eo.getNombre();
                            break;
                        }
                    }
                    codigoUOSeleccionada = codigoUOSeleccionada.toUpperCase().replace(" ", "_");

                } else {
                    EstructuraOrganizacional territorial = this
                        .buscarTerritorial(this.territorialSeleccionadaCodigo);

                    codigoUOSeleccionada = territorial.getNombre()
                        .toUpperCase();
                }
                codigoUOSeleccionada = Utilidades.delTildes(codigoUOSeleccionada);

            } else {
                codigoUOSeleccionada = this.usuario
                    .getDescripcionEstructuraOrganizacional();
            }

            funcionarios = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(codigoUOSeleccionada,
                    ERol.valueOf(this.rolSeleccionado));

            if (funcionarios != null && !funcionarios.isEmpty()) {
                for (UsuarioDTO funcionario : funcionarios) {
                    this.funcionariosItemList.add(new SelectItem(funcionario
                        .getLogin(), funcionario.getNombreCompleto()));
                }
            }

            Collections.sort(this.funcionariosItemList,
                new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem sItem1, SelectItem sItem2) {
                    String sItem1Label = sItem1.getLabel();
                    String sItem2Label = sItem2.getLabel();
                    return (sItem1Label.compareToIgnoreCase(sItem2Label));
                }
            });
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la búsqueda de las parametrizaciones de reportes para un usuario con base
     * en los filtros de búsqueda establecidos.
     *
     * @author david.cifuentes
     */
    public void buscar() {
        try {
            this.cargarParametrosConsulta();

            List<RepUsuarioRolReporte> parametrizacionesConsulta = this.getConservacionService()
                .buscarParametrizacionReporteUsuario(this.datosConsultaParametrizacion);

            if (parametrizacionesConsulta == null ||
                 parametrizacionesConsulta.isEmpty()) {
                this.parametrizacionesExistentes = new ArrayList<RepUsuarioRolReporteAgrupadoDTO>();
                this.addMensajeWarn(
                    "No se encontraron parametrizaciones de reportes a usuarios que cumplan con estos filtros de búsqueda.");
            } else {
                // Se requiere realizar una agrupación de las parametrizaciones
                // para no tener que mostrar información redundante.
                this.agruparParametrizacionesPorUsuario(parametrizacionesConsulta);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(
                "Ocurrió un error en la búsqueda de las parametrizaciones de reportes a usuarios.");
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza guarda en una variable {@link FiltroDatosConsultaParametrizacionReporte}
     * los parámetros de búsqueda.
     *
     * @author david.cifuentes
     */
    public void cargarParametrosConsulta() {

        if (this.territorialSeleccionadaCodigo == null || this.territorialSeleccionadaCodigo.
            isEmpty()) {
            this.territorialSeleccionadaCodigo = this.usuario.getCodigoTerritorial();
        }

        if (this.unidadOperativaSeleccionadaCodigo == null ||
            this.unidadOperativaSeleccionadaCodigo.isEmpty()) {
            this.unidadOperativaSeleccionadaCodigo = this.usuario.getCodigoUOC();
        }

        this.datosConsultaParametrizacion = new FiltroDatosConsultaParametrizacionReporte();
        this.datosConsultaParametrizacion.setTipoReporteId(this.tipoReporteId);
        //this.datosConsultaParametrizacion.setTerritorialCodigo(this.territorialSeleccionadaCodigo);
        //this.datosConsultaParametrizacion.setUOCCodigo(this.unidadOperativaSeleccionadaCodigo);
        this.datosConsultaParametrizacion.setDepartamentoCodigo(this.departamentoSeleccionadoCodigo);
        this.datosConsultaParametrizacion.setMunicipioCodigo(this.municipioSeleccionadoCodigo);
        this.datosConsultaParametrizacion.setRol(this.rolSeleccionado);
        this.datosConsultaParametrizacion.setFuncionario(this.funcionarioSeleccionado);

        if (this.unidadOperativaSeleccionadaCodigo != null &&
             !this.unidadOperativaSeleccionadaCodigo.isEmpty()) {
            this.datosConsultaParametrizacion.
                setEntidadCreacion(this.unidadOperativaSeleccionadaCodigo);
        } else {
            if (!Constantes.DIRECCION_GENERAL_CODIGO.equals(this.territorialSeleccionadaCodigo)) {
                this.datosConsultaParametrizacion.
                    setEntidadCreacion(this.usuario.getCodigoTerritorial());
            }
        }

        this.datosConsultaParametrizacion.setEstado(this.estadoSeleccionado);
        this.datosConsultaParametrizacion.setCategoria(this.categoriaReporte);
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la agrupación de la lista de parametrizaciones tipo
     * {@link RepUsuarioRolReporte} en una lista de parametrizaciones agrupadas por usuario.
     *
     * @param parametrizacionesConsulta
     * @author david.cifuentes
     */
    private void agruparParametrizacionesPorUsuario(
        List<RepUsuarioRolReporte> parametrizacionesConsulta) {

        this.parametrizacionesExistentes = new ArrayList<RepUsuarioRolReporteAgrupadoDTO>();

        for (RepUsuarioRolReporte rurr : parametrizacionesConsulta) {

            // Creación de la llave
            RepUsuarioRolReporteAgrupadoDTO usuarioReporteAgrupado =
                new RepUsuarioRolReporteAgrupadoDTO();
            usuarioReporteAgrupado.setUsuarioAsignado(rurr.getUsuarioAsignado());
            usuarioReporteAgrupado.setTipoReporteId(rurr.getRepReporte().getId());
            usuarioReporteAgrupado.setGenerar(rurr.isPermisoGenerar());
            usuarioReporteAgrupado.setConsultar(rurr.isPermisoConsultar());

            // Agrupación por nombre funcionario - tipo reporte y permisos
            if (this.compararLlaveAgrupacion(usuarioReporteAgrupado)) {
                // Existe la llave, es decir se adicionan los atributos
                this.parametrizacionSeleccionada.registrarRolAsignado(rurr.getRolAsignado());
                this.parametrizacionSeleccionada.registrarDepartamentos(rurr.
                    getRepTerritorialReportes());
                this.parametrizacionSeleccionada.registrarMunicipios(rurr.
                    getRepTerritorialReportes());
                this.parametrizacionSeleccionada.registrarTerritoriales(rurr.
                    getRepTerritorialReportes());
                this.parametrizacionSeleccionada.registrarUOCs(rurr.getRepTerritorialReportes());
                this.parametrizacionSeleccionada.registrarIds(rurr.getId());
            } else {
                // No existe llave
                usuarioReporteAgrupado.setCategoria(rurr.getRepReporte().getCategoria());
                usuarioReporteAgrupado.setNombreTipoReporte(rurr.getRepReporte().getNombre());
                usuarioReporteAgrupado.setEstado(rurr.getEstado());
                usuarioReporteAgrupado.setFechaLog(rurr.getFechaLog());
                usuarioReporteAgrupado.setId(rurr.getId());
                usuarioReporteAgrupado.setUsuarioLog(rurr.getUsuarioLog());
                usuarioReporteAgrupado.setRolAsignado(rurr.getRolAsignado());
                usuarioReporteAgrupado.registrarRolAsignado(rurr.getRolAsignado());
                usuarioReporteAgrupado.registrarDepartamentos(rurr.getRepTerritorialReportes());
                usuarioReporteAgrupado.registrarMunicipios(rurr.getRepTerritorialReportes());
                usuarioReporteAgrupado.registrarTerritoriales(rurr.getRepTerritorialReportes());
                usuarioReporteAgrupado.registrarUOCs(rurr.getRepTerritorialReportes());
                usuarioReporteAgrupado.registrarIds(rurr.getId());
                usuarioReporteAgrupado.determinarEdicion(rurr.getEntidadCreacion(), this.usuario);

                this.parametrizacionesExistentes.add(usuarioReporteAgrupado);
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la comparación de existencia de un {@link RepUsuarioRolReporteAgrupadoDTO}
     * contra la lista de parametrizaciones existentes, basado en la llave nombre funcionario, tipo
     * reporte id y permisos de una parametrización.
     *
     * @author david.cifuentes
     *
     * @param usuarioReporteAgrupado
     * @return
     */
    public boolean compararLlaveAgrupacion(
        RepUsuarioRolReporteAgrupadoDTO usuarioReporteAgrupado) {

        for (RepUsuarioRolReporteAgrupadoDTO rurra : this.parametrizacionesExistentes) {
            if (usuarioReporteAgrupado.getUsuarioAsignado() != null &&
                 usuarioReporteAgrupado.getTipoReporteId() != null &&
                 usuarioReporteAgrupado.getUsuarioAsignado().equals(rurra.getUsuarioAsignado()) &&
                 usuarioReporteAgrupado.getTipoReporteId().longValue() == rurra.getTipoReporteId().
                longValue() &&
                 usuarioReporteAgrupado.isConsultar() == rurra.isConsultar() &&
                 usuarioReporteAgrupado.isGenerar() == rurra.isGenerar()) {
                this.parametrizacionSeleccionada = rurra;
                return true;
            }
        }
        this.parametrizacionSeleccionada = new RepUsuarioRolReporteAgrupadoDTO();
        return false;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la inicialización de la pantalla de detalle de parametrización para la
     * creación de nuevas parametrizaciones.
     *
     * @author david.cifuentes
     */
    public void inicializarParametrizacion() {

        // Categoría del reporte
        this.categoriaReporteParametrizando = null;

        // Roles
        this.sourceRolParametrizacion = new ArrayList<String>();
        this.targetRolParametrizacion = new ArrayList<String>();

        EstructuraOrganizacional territorialUsuario = this.buscarTerritorial(this.usuario.
            getCodigoTerritorial());

        if (this.usuario.getCodigoTerritorial() != null) {

            Map<String, List<SelectItem>> rolesEstructuraOrganizacional =
                new HashMap<String, List<SelectItem>>();

            // Roles para la UOC
            if (this.usuario.isUoc()) {

                EstructuraOrganizacional uocUsuario = this.buscarTerritorial(this.usuario.
                    getCodigoUOC());

                if (uocUsuario != null) {
                    rolesEstructuraOrganizacional = this
                        .cargarRolesEstructuraOrganizacional(
                            new EstructuraOrganizacional(
                                this.usuario.getCodigoUOC(),
                                uocUsuario.getNombre(), null, null,
                                null), rolesEstructuraOrganizacional,
                            false);

                    if (rolesEstructuraOrganizacional != null) {
                        for (SelectItem rol : rolesEstructuraOrganizacional
                            .get(this.usuario.getCodigoUOC())) {
                            if (this.containsRol(rol.getValue().toString())) {
                                this.sourceRolParametrizacion.add((String) rol.getValue());
                            }
                        }
                    }
                }
            } else {

                List<EstructuraOrganizacional> listaTerritorialYUOC =
                    new ArrayList<EstructuraOrganizacional>();
                boolean esTerritorial = false;

                EstructuraOrganizacional territorial = this.buscarTerritorial(this.usuario.
                    getCodigoTerritorial());
                if (territorial != null) {
                    listaTerritorialYUOC.add(territorial);
                }

                List<EstructuraOrganizacional> UOCs = this
                    .getGeneralesService()
                    .getCacheEstructuraOrganizacionalPorTipoYPadre(
                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                        this.usuario.getCodigoTerritorial());

                if (UOCs != null && !UOCs.isEmpty()) {
                    listaTerritorialYUOC.addAll(UOCs);
                }

                for (EstructuraOrganizacional eo : listaTerritorialYUOC) {
                    if (eo != null) {
                        if (eo.getCodigo().equals(this.usuario.getCodigoTerritorial())) {
                            esTerritorial = true;
                        } else {
                            esTerritorial = false;
                        }
                        rolesEstructuraOrganizacional = this
                            .cargarRolesEstructuraOrganizacional(
                                new EstructuraOrganizacional(
                                    eo.getCodigo(),
                                    eo.getNombre(), null, null,
                                    null), rolesEstructuraOrganizacional,
                                esTerritorial);

                        if (rolesEstructuraOrganizacional != null &&
                             rolesEstructuraOrganizacional.get(eo.getCodigo()) != null &&
                             !rolesEstructuraOrganizacional.get(eo.getCodigo()).isEmpty()) {

                            for (SelectItem si : rolesEstructuraOrganizacional.get(eo.getCodigo())) {
                                if (this.containsRol(si.getValue().toString()) &&
                                     !this.sourceRolParametrizacion.contains(si.getValue().
                                        toString())) {
                                    this.sourceRolParametrizacion.add((String) si.getValue());
                                }
                            }
                            Collections.sort(this.sourceRolParametrizacion);
                        }
                    }
                }
            }
        } else {
            List<SelectItem> rolesSedeCentral = this.cargarRolesSedeCentral();
            if (rolesSedeCentral != null && !rolesSedeCentral.isEmpty()) {
                for (SelectItem rol : rolesSedeCentral) {
                    if (this.containsRol(rol.getValue().toString())) {
                        this.sourceRolParametrizacion.add((String) rol.getValue());
                    }
                }
            }
        }

        this.rolPickList = new DualListModel<String>(this.sourceRolParametrizacion,
            this.targetRolParametrizacion);

        // Usuarios
        this.sourceUsuarioParametrizacion = new ArrayList<UsuarioDTO>();
        this.targetUsuarioParametrizacion = new ArrayList<UsuarioDTO>();
        this.usuarioPickList = new DualListModel<UsuarioDTO>(this.sourceUsuarioParametrizacion,
            this.targetUsuarioParametrizacion);

        // Estado y permisos
        this.estadoReporteParametrizando = null;
        this.consultarBoolParametrizando = false;
        this.generarBoolParametrizando = false;

        // Tipos de reporte
        this.sourceTipoReporteParametrizacion = new ArrayList<RepReporte>();
        this.targetTipoReporteParametrizacion = new ArrayList<RepReporte>();
        this.tipoReportePickList = new DualListModel<RepReporte>(
            this.sourceTipoReporteParametrizacion,
            this.targetTipoReporteParametrizacion);

        // Territoriales
        if (territorialUsuario != null &&
             !territorialUsuario.isDireccionGeneral()) {
            this.sourceTerritorialParametrizacion = new ArrayList<EstructuraOrganizacional>();
            this.sourceTerritorialParametrizacion.add(territorialUsuario);
        } else {
            // Para sede central se cargan todas las territoriales
            this.sourceTerritorialParametrizacion = this.getGeneralesService().
                getCacheTerritoriales();
            //Se descarta la territorial de ANTIQUIA según control de calidad 32111
            for (EstructuraOrganizacional estructuraOrganizacional
                : sourceTerritorialParametrizacion) {
                if (estructuraOrganizacional.getCodigo().equals("6000")) {
                    sourceTerritorialParametrizacion.remove(estructuraOrganizacional);
                    break;
                }
            }
        }

        this.targetTerritorialParametrizacion = new ArrayList<EstructuraOrganizacional>();
        this.territorialPickList = new DualListModel<EstructuraOrganizacional>(
            this.sourceTerritorialParametrizacion,
            this.targetTerritorialParametrizacion);

        // UOC
        this.sourceUOCParametrizacion = new ArrayList<EstructuraOrganizacional>();
        this.targetUOCParametrizacion = new ArrayList<EstructuraOrganizacional>();
        this.uocPickList =
            new DualListModel<EstructuraOrganizacional>(this.sourceUOCParametrizacion,
                this.targetUOCParametrizacion);

        // Departamentos
        this.sourceDepartamentoParametrizacion = new ArrayList<Departamento>();
        this.targetDepartamentoParametrizacion = new ArrayList<Departamento>();
        this.departamentoPickList = new DualListModel<Departamento>(
            this.sourceDepartamentoParametrizacion,
            this.targetDepartamentoParametrizacion);

        // Municipios
        this.sourceMunicipioParametrizacion = new ArrayList<Municipio>();
        this.targetMunicipioParametrizacion = new ArrayList<Municipio>();
        this.municipioPickList = new DualListModel<Municipio>(this.sourceMunicipioParametrizacion,
            this.targetMunicipioParametrizacion);
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar la categoría de un reporte y realiza el cargue de los
     * tipos de reporte en la pantalla de detalle de parametrización
     *
     * @author david.cifuentes
     */
    public void cambioCategoriaParametrizando() {

        this.sourceTipoReporteParametrizacion.clear();
        this.targetTipoReporteParametrizacion.clear();

        if (this.categoriaReporteParametrizando != null) {

            List<RepReporte> repReportes = this.getGeneralesService()
                .buscarRepReportesPorCategoria(this.categoriaReporteParametrizando);

            if (repReportes != null) {
                this.sourceTipoReporteParametrizacion.addAll(repReportes);
            }
        }

        this.tipoReportePickList = new DualListModel<RepReporte>(
            this.sourceTipoReporteParametrizacion,
            this.targetTipoReporteParametrizacion);
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al asignar un rol en el picklist de parametrización, realiza el cargue
     * de los usuarios en la pantalla de detalle de parametrización
     *
     * @author david.cifuentes
     */
    public void cambioRolesParametrizando() {

        this.sourceUsuarioParametrizacion.clear();
        this.targetUsuarioParametrizacion.clear();

        this.sourceRolParametrizacion = this.rolPickList.getSource();
        this.targetRolParametrizacion = this.rolPickList.getTarget();

        if (this.targetRolParametrizacion != null && !this.targetRolParametrizacion.isEmpty()) {

            for (String rol : this.targetRolParametrizacion) {

                if (rol != null) {

                    List<UsuarioDTO> funcionarios = null;
                    String nombreEstructuraOrganizacionalSeleccionada = null;
                    String codigoEstructuraOrganizacionalSeleccionada = null;

                    if (this.usuario.getCodigoTerritorial() != null &&
                        !this.usuario.getCodigoTerritorial().equals(
                            Constantes.DIRECCION_GENERAL_CODIGO)) {

                        if (this.usuario.getCodigoUOC() != null) {

                            EstructuraOrganizacional uoc = this.buscarTerritorial(this.usuario.
                                getCodigoUOC());
                            nombreEstructuraOrganizacionalSeleccionada = uoc.getNombre();
                            nombreEstructuraOrganizacionalSeleccionada =
                                nombreEstructuraOrganizacionalSeleccionada.toUpperCase().replace(
                                    "UOC ", "UOC_");
                            codigoEstructuraOrganizacionalSeleccionada = this.usuario.getCodigoUOC();
                        } else {
                            EstructuraOrganizacional territorial = this.buscarTerritorial(
                                this.usuario.getCodigoTerritorial());
                            nombreEstructuraOrganizacionalSeleccionada = territorial.getNombre()
                                .toUpperCase();
                            codigoEstructuraOrganizacionalSeleccionada = this.usuario.
                                getCodigoTerritorial();
                        }

                        nombreEstructuraOrganizacionalSeleccionada = Utilidades.delTildes(
                            nombreEstructuraOrganizacionalSeleccionada);

                    } else {
                        nombreEstructuraOrganizacionalSeleccionada = this.usuario
                            .getDescripcionEstructuraOrganizacional();
                        codigoEstructuraOrganizacionalSeleccionada = this.usuario.
                            getDescripcionEstructuraOrganizacional();
                    }

                    if (nombreEstructuraOrganizacionalSeleccionada != null && !"SEDE_CENTRAL".
                        equals(nombreEstructuraOrganizacionalSeleccionada)) {

                        ERol rolSeleccionado = null;

                        boolean rolExiste = false;
                        for (ERol e : ERol.values()) {
                            if (e.getRol().toUpperCase().equals(rol.toUpperCase())) {
                                rolExiste = true;
                                rolSeleccionado = e;
                                break;
                            }
                        }

                        if (rolExiste) {

                            funcionarios = this.getTramiteService()
                                .buscarFuncionariosPorRolYTerritorial(
                                    nombreEstructuraOrganizacionalSeleccionada,
                                    rolSeleccionado);

                            // busqueda de los funcionarios de la UOC en caso de que
                            // el codigo de la estructura organizacional sea el de
                            // una territorial
                            List<EstructuraOrganizacional> unidadesOperativasCatastro = this
                                .getGeneralesService()
                                .getCacheEstructuraOrganizacionalPorTipoYPadre(
                                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                                    codigoEstructuraOrganizacionalSeleccionada);

                            if (unidadesOperativasCatastro != null && !unidadesOperativasCatastro.
                                isEmpty()) {

                                if (funcionarios == null) {
                                    funcionarios = new ArrayList<UsuarioDTO>();
                                }
                                for (EstructuraOrganizacional eo : unidadesOperativasCatastro) {
                                    List<UsuarioDTO> funcionariosUOC = this.getTramiteService()
                                        .buscarFuncionariosPorRolYEstructuraOrganizacional(
                                            Utilidades.delTildes(eo.getNombre().toUpperCase().
                                                replace(" ", "_")),
                                            ERol.valueOf(rol));
                                    if (funcionariosUOC != null && !funcionariosUOC.isEmpty()) {
                                        funcionarios.addAll(funcionariosUOC);
                                    }
                                }
                            }
                        }

                    } else {
                        List<UsuarioDTO> funcionariosAll = this.getTramiteService().
                            buscarTodosFuncionarios();

                        if (funcionariosAll != null && !funcionariosAll.isEmpty()) {
                            funcionarios = new ArrayList<UsuarioDTO>();

                            for (UsuarioDTO funcionario : funcionariosAll) {
                                if (funcionario.getRoles() != null) {
                                    List<String> rolesFuncionario = Arrays.asList(funcionario.
                                        getRoles());
                                    if (rolesFuncionario != null && rolesFuncionario.contains(rol)) {
                                        funcionarios.add(funcionario);
                                    }
                                }
                            }
                        }
                    }

                    if (funcionarios != null && !funcionarios.isEmpty()) {
                        this.sourceUsuarioParametrizacion.addAll(funcionarios);
                    }

                    Collections.sort(this.sourceUsuarioParametrizacion,
                        new Comparator<UsuarioDTO>() {
                        @Override
                        public int compare(UsuarioDTO sItem1, UsuarioDTO sItem2) {
                            String sItem1Label = sItem1.getLogin();
                            String sItem2Label = sItem2.getLogin();
                            return (sItem1Label.compareToIgnoreCase(sItem2Label));
                        }
                    });
                }
            }
        }

        for (UsuarioDTO user : this.sourceUsuarioParametrizacion) {
            this.rolesUsuarios.put(user.getLogin(), user.getRoles());
        }
        this.usuarioPickList = new DualListModel<UsuarioDTO>(this.sourceUsuarioParametrizacion,
            this.targetUsuarioParametrizacion);
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al asignar una territorial en el picklist de parametrización, realiza
     * el cargue de las UOC y Departamentos en la pantalla de detalle de parametrización
     *
     * @author david.cifuentes
     */
    public void cambioTerritorialParametrizando() {

        this.sourceUOCParametrizacion.clear();
        this.targetUOCParametrizacion.clear();

        this.sourceTerritorialParametrizacion = this.territorialPickList.getSource();
        this.targetTerritorialParametrizacion = this.territorialPickList.getTarget();

        this.sourceDepartamentoParametrizacion.clear();
        this.targetDepartamentoParametrizacion.clear();

        if (this.targetTerritorialParametrizacion != null && !this.targetTerritorialParametrizacion.
            isEmpty()) {

            for (EstructuraOrganizacional territorial : this.targetTerritorialParametrizacion) {

                if (territorial != null) {

                    // Cargar UOCs
                    if (!territorial.isDireccionGeneral()) {

                        List<EstructuraOrganizacional> unidadesOperativasCatastro = null;
                        if (this.tipoUsuario == 1) {
                            EstructuraOrganizacional uoc = this.buscarTerritorial(this.usuario.
                                getCodigoUOC());
                            if (uoc != null) {
                                unidadesOperativasCatastro =
                                    new ArrayList<EstructuraOrganizacional>();
                                unidadesOperativasCatastro.add(uoc);
                            }
                        } else {
                            unidadesOperativasCatastro = this
                                .getGeneralesService()
                                .getCacheEstructuraOrganizacionalPorTipoYPadre(
                                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                                    territorial.getCodigo());
                        }

                        if (unidadesOperativasCatastro != null) {
                            for (EstructuraOrganizacional uoc : unidadesOperativasCatastro) {
                                this.sourceUOCParametrizacion.add(uoc);
                            }
                        }
                    }

                    // Cargar Departamentos
                    List<Departamento> departamentosList = null;
                    if (this.tipoUsuario == 1) {
                        EstructuraOrganizacional uoc = this.buscarTerritorial(this.usuario.
                            getCodigoUOC());
                        if (uoc != null) {
                            departamentosList = this.getGeneralesService()
                                .getCacheDepartamentosPorTerritorial(uoc.getCodigo());
                        }
                    } else {
                        departamentosList = this.getGeneralesService()
                            .getCacheDepartamentosPorTerritorial(territorial.getCodigo());
                    }

                    if (departamentosList != null) {
                        for (Departamento departamento : departamentosList) {
                            this.sourceDepartamentoParametrizacion.add(departamento);
                        }
                    }
                }
            }
        }

        this.uocPickList =
            new DualListModel<EstructuraOrganizacional>(this.sourceUOCParametrizacion,
                this.targetUOCParametrizacion);

        this.departamentoPickList = new DualListModel<Departamento>(
            this.sourceDepartamentoParametrizacion,
            this.targetDepartamentoParametrizacion);

        this.cambioDepartamentoParametrizando();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al asignar una UOC en el picklist de parametrización, realiza el nuevo
     * cargue de Departamentos en la pantalla de detalle de parametrización
     *
     * @author david.cifuentes
     */
    public void cambioUOCParametrizando() {

        this.sourceUOCParametrizacion = this.uocPickList.getSource();
        this.targetUOCParametrizacion = this.uocPickList.getTarget();

        this.sourceDepartamentoParametrizacion.clear();
        this.targetDepartamentoParametrizacion.clear();

        if (this.targetUOCParametrizacion != null && !this.targetUOCParametrizacion.isEmpty()) {

            // Filtro por UOC
            for (EstructuraOrganizacional uoc : this.targetUOCParametrizacion) {

                if (uoc != null) {

                    // Cargar Departamentos
                    List<Departamento> departamentosList = this.getGeneralesService()
                        .getCacheDepartamentosByCodigoUOC(uoc.getCodigo());

                    if (departamentosList != null) {
                        for (Departamento departamento : departamentosList) {
                            if (!this.sourceDepartamentoParametrizacion.contains(departamento)) {
                                this.sourceDepartamentoParametrizacion.add(departamento);
                            }
                        }
                    }
                }
            }
        } else if (this.targetTerritorialParametrizacion != null &&
            !this.targetTerritorialParametrizacion.isEmpty()) {

            // Filtro por Departamento
            for (EstructuraOrganizacional territorial : this.targetTerritorialParametrizacion) {

                if (territorial != null) {

                    // Cargar Departamentos
                    List<Departamento> departamentosList = null;
                    if (this.tipoUsuario == 1) {
                        departamentosList = this.getGeneralesService()
                            .getCacheDepartamentosPorTerritorial(this.usuario.getCodigoUOC());
                    } else {
                        departamentosList = this.getGeneralesService()
                            .getCacheDepartamentosPorTerritorial(territorial.getCodigo());
                    }

                    if (departamentosList != null) {
                        for (Departamento departamento : departamentosList) {
                            this.sourceDepartamentoParametrizacion.add(departamento);
                        }
                    }

                }
            }
        }

        this.departamentoPickList = new DualListModel<Departamento>(
            this.sourceDepartamentoParametrizacion,
            this.targetDepartamentoParametrizacion);

        this.cambioDepartamentoParametrizando();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al asignar un departamento en el picklist de parametrización, realiza
     * el cargue de los municipios respectivamente
     *
     * @author david.cifuentes
     */
    public void cambioDepartamentoParametrizando() {

        this.sourceMunicipioParametrizacion.clear();
        this.targetMunicipioParametrizacion.clear();

        this.sourceDepartamentoParametrizacion = this.departamentoPickList.getSource();
        this.targetDepartamentoParametrizacion = this.departamentoPickList.getTarget();

        this.sourceUOCParametrizacion = this.uocPickList.getSource();
        this.targetUOCParametrizacion = this.uocPickList.getTarget();

        if (this.targetDepartamentoParametrizacion != null && !targetDepartamentoParametrizacion.
            isEmpty()) {
            for (Departamento departamento : this.targetDepartamentoParametrizacion) {

                List<Municipio> municipiosList = this.getGeneralesService()
                    .getCacheMunicipiosByDepartamento(departamento.getCodigo());

                for (Municipio mcpio : municipiosList) {
                    if (mcpio.getCodigo().equals("76001")) {
                        municipiosList.remove(mcpio);
                        break;
                    }
                }

                if (municipiosList != null) {
                    if ((this.targetUOCParametrizacion != null &&
                         !this.targetUOCParametrizacion.isEmpty()) ||
                         this.tipoUsuario == 1) {

                        List<Municipio> municipiosFilter = null;
                        if (this.tipoUsuario == 1) {
                            EstructuraOrganizacional uoc = this.buscarTerritorial(this.usuario.
                                getCodigoUOC());
                            if (uoc != null) {
                                List<EstructuraOrganizacional> uocList =
                                    new ArrayList<EstructuraOrganizacional>();
                                uocList.add(uoc);
                                municipiosFilter = this.filtrarMunicipioPorUOC(municipiosList,
                                    uocList);
                            }
                        } else {
                            municipiosFilter = this.filtrarMunicipioPorUOC(municipiosList,
                                this.targetUOCParametrizacion);
                        }

                        // Excluir los municipios que no pertenezcan a las UOCs seleccionadas
                        if (municipiosFilter != null && !municipiosFilter.isEmpty()) {
                            this.sourceMunicipioParametrizacion.addAll(municipiosFilter);
                        }
                    } else {
                        this.sourceMunicipioParametrizacion.addAll(municipiosList);
                    }
                }
            }
        }
        //Se comenta codigo debido a que las delegadas tambien pueden realizar la parametrizacion
        /* List<Municipio> finalMuns = new ArrayList<Municipio>(); /*for (Municipio municipio :
         * this.sourceMunicipioParametrizacion) { if (!this.usuarioDelegado) { boolean delegado =
         * false; for (Municipio mun : this.municipiosDelegados) { if
         * (mun.getCodigo().equals(municipio.getCodigo())) { delegado = true; } } if (!delegado) {
         * finalMuns.add(municipio); } }
        } */

        this.municipioPickList = new DualListModel<Municipio>(this.sourceMunicipioParametrizacion,
            this.targetMunicipioParametrizacion);
    }

    /**
     * Método que realiza la intersección de municipios de una UOC
     *
     * @param municipios
     * @param UOCs
     * @return
     */
    private List<Municipio> filtrarMunicipioPorUOC(
        List<Municipio> municipios,
        List<EstructuraOrganizacional> UOCs) {

        List<Municipio> municipiosFilter = new ArrayList<Municipio>();
        List<Municipio> municipiosUOC = null;

        for (EstructuraOrganizacional eo : UOCs) {
            municipiosUOC = this.getGeneralesService().
                findMunicipiosbyCodigoEstructuraOrganizacional(eo.getCodigo());
            if (municipiosUOC != null && !municipiosUOC.isEmpty()) {
                for (Municipio m : municipiosUOC) {
                    if (municipios.contains(m)) {
                        municipiosFilter.add(m);
                    }
                }
            }
        }

        return municipiosFilter;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de la pantalla de detalle de parametrización para la edición de
     * una parametrización existente.
     *
     * @author david.cifuentes
     */
    public void cargarParametrizacion() {

        if (this.parametrizacionSeleccionada != null &&
             (this.parametrizacionSeleccionada.getId() != null ||
             this.parametrizacionSeleccionada.getIdsParametrizaciones() != null)) {

            this.inicializarParametrizacion();

            // Categoría seleccionada
            this.categoriaReporteParametrizando = this.parametrizacionSeleccionada.getCategoria();
            this.cambioCategoriaParametrizando();

            // Tipos de reporte
            List<RepReporte> reportesSeleccionados = new ArrayList<RepReporte>();
            for (RepReporte tipoReporte : this.sourceTipoReporteParametrizacion) {
                if (tipoReporte.getId().longValue() == this.parametrizacionSeleccionada.
                    getTipoReporteId().longValue()) {
                    reportesSeleccionados.add(tipoReporte);
                }
            }
            if (!reportesSeleccionados.isEmpty()) {
                this.sourceTipoReporteParametrizacion.removeAll(reportesSeleccionados);
                this.targetTipoReporteParametrizacion.addAll(reportesSeleccionados);
                this.tipoReportePickList = new DualListModel<RepReporte>(
                    this.sourceTipoReporteParametrizacion,
                    this.targetTipoReporteParametrizacion);
            }

            // Roles
            this.sourceRolParametrizacion.removeAll(this.parametrizacionSeleccionada.
                getRolesAsignados());
            this.targetRolParametrizacion.addAll(this.parametrizacionSeleccionada.
                getRolesAsignados());
            this.rolPickList = new DualListModel<String>(this.sourceRolParametrizacion,
                this.targetRolParametrizacion);
            this.cambioRolesParametrizando();

            // Usuarios
            List<UsuarioDTO> usuariosSeleccionados = new ArrayList<UsuarioDTO>();
            for (UsuarioDTO u : this.sourceUsuarioParametrizacion) {
                if (u.getLogin().equals(parametrizacionSeleccionada.getUsuarioAsignado())) {
                    usuariosSeleccionados.add(u);
                }
            }
            if (!usuariosSeleccionados.isEmpty()) {
                this.sourceUsuarioParametrizacion.removeAll(usuariosSeleccionados);
                this.targetUsuarioParametrizacion.addAll(usuariosSeleccionados);
                this.usuarioPickList = new DualListModel<UsuarioDTO>(
                    this.sourceUsuarioParametrizacion,
                    this.targetUsuarioParametrizacion);
            }

            // Territoriales
            this.sourceTerritorialParametrizacion.removeAll(this.parametrizacionSeleccionada.
                getTerritoriales());
            this.targetTerritorialParametrizacion.addAll(this.parametrizacionSeleccionada.
                getTerritoriales());
            this.territorialPickList = new DualListModel<EstructuraOrganizacional>(
                this.sourceTerritorialParametrizacion, this.targetTerritorialParametrizacion);
            this.cambioTerritorialParametrizando();

            // UOC
            this.sourceUOCParametrizacion.removeAll(this.parametrizacionSeleccionada.getUOCs());
            this.targetUOCParametrizacion.addAll(this.parametrizacionSeleccionada.getUOCs());
            this.uocPickList = new DualListModel<EstructuraOrganizacional>(
                this.sourceUOCParametrizacion, this.targetUOCParametrizacion);
            this.cambioUOCParametrizando();

            // Departamentos
            this.sourceDepartamentoParametrizacion.removeAll(this.parametrizacionSeleccionada.
                getDepartamentos());
            this.targetDepartamentoParametrizacion.addAll(this.parametrizacionSeleccionada.
                getDepartamentos());
            this.departamentoPickList = new DualListModel<Departamento>(
                this.sourceDepartamentoParametrizacion, this.targetDepartamentoParametrizacion);
            this.cambioDepartamentoParametrizando();

            // Municipios
            this.sourceMunicipioParametrizacion.removeAll(this.parametrizacionSeleccionada.
                getMunicipios());
            this.targetMunicipioParametrizacion.addAll(this.parametrizacionSeleccionada.
                getMunicipios());
            this.municipioPickList = new DualListModel<Municipio>(
                this.sourceMunicipioParametrizacion, this.targetMunicipioParametrizacion);

            // Permisos y estado
            this.estadoReporteParametrizando = this.parametrizacionSeleccionada.getEstado();
            this.consultarBoolParametrizando = this.parametrizacionSeleccionada.isConsultar();
            this.generarBoolParametrizando = this.parametrizacionSeleccionada.isGenerar();
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la eliminación de una parametrización existente.
     *
     * @author david.cifuentes
     */
    public void eliminarParametrizacion() {

        try {

            if (this.parametrizacionSeleccionada != null &&
                 this.parametrizacionSeleccionada.getIdsParametrizaciones() != null &&
                 !this.parametrizacionSeleccionada.getIdsParametrizaciones().isEmpty()) {

                if (!this.editMode) {

                    boolean validoEliminar = true;

                    List<RepReporteEjecucionUsuario> subscripciones = this.getConservacionService().
                        buscarSubscripcionPorUsuario(this.parametrizacionSeleccionada.
                            getUsuarioAsignado());

                    if (subscripciones != null && !subscripciones.isEmpty()) {
                        List<Long> ids = new ArrayList<Long>();
                        for (RepReporteEjecucionUsuario subs : subscripciones) {
                            ids.add(subs.getReporteEjecucionId());
                        }
                        List<RepReporteEjecucion> ejecuciones = this.getConservacionService().
                            buscarReportesPorIds(ids);

                        for (RepReporteEjecucion ejecucion : ejecuciones) {
                            if (ejecucion.getRepReporte().getId().
                                equals(this.parametrizacionSeleccionada.getTipoReporteId())) {
                                validoEliminar = false;
                                break;
                            }
                        }
                    }

                    if (!validoEliminar) {
                        this.addMensajeError(ECodigoErrorVista.PARAMS_REPORTES_002.toString());
                        LOGGER.info(ECodigoErrorVista.PARAMS_REPORTES_002.getMensajeTecnico());
                        return;

                    }
                }

                // Se eliminan los RepUsuarioRolReporte asociados a la agrupación
                this.getConservacionService()
                    .eliminarRepUsuarioRolReporte(this.parametrizacionSeleccionada
                        .getIdsParametrizaciones());
                this.parametrizacionesExistentes.remove(this.parametrizacionSeleccionada);
                this.parametrizacionSeleccionada = null;

                if (!this.editMode) {
                    this.addMensajeInfo(
                        "Se eliminó la parametrización del usuario satisfactoriamente.");
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Ocurrió un error al eliminar la parametrización.");
            return;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que guardar los valores seleccionados de una parametrización.
     *
     * @author david.cifuentes
     */
    public void guardarParametrizacion() {

        // Set de los picklist
        this.sourceTipoReporteParametrizacion = this.tipoReportePickList.getSource();
        this.targetTipoReporteParametrizacion = this.tipoReportePickList.getTarget();
        this.sourceRolParametrizacion = this.rolPickList.getSource();
        this.targetRolParametrizacion = this.rolPickList.getTarget();
        //this.sourceUsuarioParametrizacion = this.usuarioPickList.getSource();
        this.targetUsuarioParametrizacion = this.usuarioPickList.getTarget();
        this.sourceTerritorialParametrizacion = this.territorialPickList.getSource();
        this.targetTerritorialParametrizacion = this.territorialPickList.getTarget();
        this.sourceUOCParametrizacion = this.uocPickList.getSource();
        this.targetUOCParametrizacion = this.uocPickList.getTarget();
        this.sourceDepartamentoParametrizacion = this.departamentoPickList.getSource();
        this.targetDepartamentoParametrizacion = this.departamentoPickList.getTarget();
        this.sourceMunicipioParametrizacion = this.municipioPickList.getSource();
        this.targetMunicipioParametrizacion = this.municipioPickList.getTarget();

        //Auxiliar para eliminacion de parametrizaciones al editar
        RepUsuarioRolReporteAgrupadoDTO parametrizacionAEliminar =
            new RepUsuarioRolReporteAgrupadoDTO();
        if (this.editMode) {
            parametrizacionAEliminar.setIdsParametrizaciones(this.parametrizacionSeleccionada.
                getIdsParametrizaciones());
        }

        if (this.validacionParametrizacion()) {

            //restaurar roles usuarios
            for (UsuarioDTO user : this.targetUsuarioParametrizacion) {
                user.setRoles(this.rolesUsuarios.get(user.getLogin()));
            }

            try {
                RepUsuarioRolReporte parametrizacionUsuarioReporte = null;
                RepTerritorialReporte parametrizacionTerritorialReporte = null;
                List<RepUsuarioRolReporte> listaParametrizacionesUsuarioReporte =
                    new ArrayList<RepUsuarioRolReporte>();
                List<RepTerritorialReporte> listaParametrizacionesTerritorialReporte =
                    new ArrayList<RepTerritorialReporte>();

                // Creación de parametrizaciones teniendo encuenta las tuplas (Rol-Usuario-TipoReoporte)
                for (String rol : this.targetRolParametrizacion) {
                    for (UsuarioDTO usuario : this.targetUsuarioParametrizacion) {
                        for (RepReporte reporte : this.targetTipoReporteParametrizacion) {

                            boolean existeRol = false;

                            for (String rolUsuario : usuario.getRoles()) {
                                if (rol.equals(rolUsuario)) {
                                    existeRol = true;
                                    break;
                                }
                            }

                            if (existeRol) {
                                parametrizacionUsuarioReporte = new RepUsuarioRolReporte();

                                if (this.generarBoolParametrizando) {
                                    parametrizacionUsuarioReporte.setGenerar(ESiNo.SI.getCodigo());
                                    parametrizacionUsuarioReporte.setConsultar(ESiNo.SI.getCodigo());
                                } else if (this.consultarBoolParametrizando) {
                                    parametrizacionUsuarioReporte.setConsultar(ESiNo.SI.getCodigo());
                                    parametrizacionUsuarioReporte.setGenerar(ESiNo.NO.getCodigo());
                                }

                                parametrizacionUsuarioReporte.setEstado(
                                    this.estadoReporteParametrizando);
                                parametrizacionUsuarioReporte.setFechaLog(new Date(System.
                                    currentTimeMillis()));
                                parametrizacionUsuarioReporte.setRepReporte(reporte);
                                parametrizacionUsuarioReporte.setRolAsignado(rol);
                                parametrizacionUsuarioReporte.setUsuarioAsignado(usuario.getLogin());
                                if (this.usuario.getCodigoUOC() != null && !this.usuario.
                                    getCodigoUOC().isEmpty()) {
                                    parametrizacionUsuarioReporte.setEntidadCreacion(this.usuario.
                                        getCodigoUOC());
                                } else {
                                    parametrizacionUsuarioReporte.setEntidadCreacion(this.usuario.
                                        getCodigoTerritorial());
                                }
                                parametrizacionUsuarioReporte.setUsuarioLog(this.usuario.getLogin());

                                listaParametrizacionesUsuarioReporte.add(
                                    parametrizacionUsuarioReporte);

                            }
                        }
                    }
                }

                // Municipios
                if (this.targetMunicipioParametrizacion != null &&
                    !this.targetMunicipioParametrizacion.isEmpty()) {
                    for (Municipio municipio : this.targetMunicipioParametrizacion) {

                        parametrizacionTerritorialReporte = new RepTerritorialReporte();
                        parametrizacionTerritorialReporte.setMunicipio(municipio);
                        parametrizacionTerritorialReporte.setDepartamento(
                            this.getGeneralesService().getCacheDepartamentoByCodigo(municipio.
                                getCodigo().substring(0, 2)));
                        EstructuraOrganizacional eo = this.getGeneralesService().
                            obtenerEstructuraOrganizacionalPorMunicipioCodigo(municipio.getCodigo());

                        if (EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL.getTipo().equals(
                            eo.getTipo())) {
                            parametrizacionTerritorialReporte.setUOC(eo);
                            parametrizacionTerritorialReporte.setTerritorial(this.buscarTerritorial(
                                eo.getEstructuraOrganizacionalCod()));
                        } else {
                            parametrizacionTerritorialReporte.setTerritorial(eo);
                            parametrizacionTerritorialReporte.setUOC(null);
                        }
                        parametrizacionTerritorialReporte.setFechaLog(new Date(System.
                            currentTimeMillis()));
                        parametrizacionTerritorialReporte.setUsuarioLog(this.usuario.getLogin());

                        listaParametrizacionesTerritorialReporte.add(
                            parametrizacionTerritorialReporte);
                    }
                }

                if (!listaParametrizacionesUsuarioReporte.isEmpty() &&
                    !listaParametrizacionesTerritorialReporte.isEmpty()) {

                    List<RepUsuarioRolReporte> parametrizacionesCreadas =
                        new ArrayList<RepUsuarioRolReporte>();

                    for (RepUsuarioRolReporte reporteUsuario : listaParametrizacionesUsuarioReporte) {
                        // Clone del reporteUsuario
                        RepUsuarioRolReporte reporteUsuarioClone =
                            (RepUsuarioRolReporte) reporteUsuario.clone();
                        reporteUsuarioClone.setId(null);

                        for (RepTerritorialReporte reporteTerritorial
                            : listaParametrizacionesTerritorialReporte) {
                            // Asignación del clone de reporteUsuario para que siempre se tome como registro nuevo
                            reporteTerritorial.setRepUsuarioRolReporte(reporteUsuarioClone);
                            reporteTerritorial.setId(null);
                        }

                        // Clone de la lista de reportesTerritorial
                        List<RepTerritorialReporte> reportesTerritorialClone =
                            new ArrayList<RepTerritorialReporte>(
                                listaParametrizacionesTerritorialReporte.size());
                        for (RepTerritorialReporte rtr : listaParametrizacionesTerritorialReporte) {
                            reportesTerritorialClone.add((RepTerritorialReporte) rtr.clone());
                        }
                        reporteUsuarioClone.setRepTerritorialReportes(reportesTerritorialClone);

                        if (!this.editMode) {
                            if (this.validarExistenciaPrevia(reporteUsuarioClone)) {
                                this.addErrorCallback();
                                this.addMensajeError(ECodigoErrorVista.PARAMS_REPORTES_001.
                                    getMensajeUsuario(reporteUsuarioClone.getUsuarioAsignado(),
                                        reporteUsuarioClone.getRepReporte().getNombre()));
                                LOGGER.info(ECodigoErrorVista.PARAMS_REPORTES_001.
                                    getMensajeTecnico());
                                return;
                            }
                        }

                        // Guardar parametrización para un usuario reporte
                        RepUsuarioRolReporte parametrizacionCreada =
                            this.getConservacionService().guardarRepUsuarioRolReporte(
                                reporteUsuarioClone);

                        if (parametrizacionCreada != null && parametrizacionCreada.getId() != null) {

                            RepUsuarioRolReporte parametrizacionBusqueda = this.
                                getConservacionService()
                                .buscarParametrizacionReporteUsuarioCompletaPorId(
                                    parametrizacionCreada.getId());
                            parametrizacionesCreadas.add(parametrizacionBusqueda);
                        }
                    }
                    if (!parametrizacionesCreadas.isEmpty()) {
                        this.agruparParametrizacionesPorUsuario(parametrizacionesCreadas);
                    }

                    this.addMensajeInfo("Se guardó la parametrización satisfactoriamente.");
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Ocurrió un error al guardar la parametrización.");
                return;
            }

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        if (this.editMode) {
            this.parametrizacionSeleccionada.setIdsParametrizaciones(parametrizacionAEliminar.
                getIdsParametrizaciones());
            this.eliminarParametrizacion();
            this.buscar();
            this.editMode = false;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza las validaciones sobre las parametrizaciones previo a guardar los valores
     * seleccionados.
     *
     * @author david.cifuentes
     */
    public boolean validacionParametrizacion() {

        // Categoría
        if (this.categoriaReporteParametrizando == null) {
            this.addMensajeError("Debe seleccionar una categoría para asignar la parametrización");
            return false;
        }
        // Tipo de reporte
        if (this.targetTipoReporteParametrizacion != null &&
             this.targetTipoReporteParametrizacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar como mínimo un tipo de reporte para asignar la parametrización");
            return false;
        }
        // Rol
        if (this.targetRolParametrizacion != null &&
             this.targetRolParametrizacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar como mínimo un rol para asignar la parametrización");
            return false;
        }
        // Usuario
        if (this.targetUsuarioParametrizacion != null &&
             this.targetUsuarioParametrizacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar como mínimo un usuario para asignar la parametrización");
            return false;
        }
        // Territorial
        if (this.targetTerritorialParametrizacion != null &&
             this.targetTerritorialParametrizacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar como mínimo una territorial para asignar la parametrización");
            return false;
        }
        // Departamento
        if (this.targetDepartamentoParametrizacion != null &&
             this.targetDepartamentoParametrizacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar como mínimo un departamento para asignar la parametrización");
            return false;
        }
        // Municipio
        if (this.targetMunicipioParametrizacion != null &&
             this.targetMunicipioParametrizacion.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar como mínimo un municipio para asignar la parametrización");
            return false;
        }
        // Estado
        if (this.estadoReporteParametrizando == null ||
             this.estadoReporteParametrizando.isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar el estado para los permisos a asignar en la parametrización");
            return false;
        }
        // Permisos
        if (!this.consultarBoolParametrizando && !this.generarBoolParametrizando) {
            this.addMensajeError(
                "Debe seleccionar al menos un permiso de Consulta o Generación para asignar en la parametrización");
            return false;
        }

        return true;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el set del permiso de consulta, en caso de que se otorgue el permiso de
     * generar.
     *
     * @author david.cifuentes
     */
    public void cambiarPermisos() {
        // Permisos
        if (this.generarBoolParametrizando) {
            this.consultarBoolParametrizando = true;
            this.estadoPermisoConsultaBool = true;
        } else {
            this.estadoPermisoConsultaBool = false;
        }
    }

    /**
     * Determina si existe lideres tecnicos para determinadas UOCS
     *
     * @author felipe.cadena
     */
    private void inicializarLideresTecnicos() {

        this.lideresTecnicos = new HashMap<String, Boolean>();
        List<EstructuraOrganizacional> uocs = this
            .getGeneralesService()
            .getCacheEstructuraOrganizacionalPorTipoYPadre(
                EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                this.usuario.getCodigoTerritorial());

        for (EstructuraOrganizacional uoc : uocs) {

            List<UsuarioDTO> funcionarios = this.getGeneralesService()
                .obtenerFuncionarioTerritorialYRol(uoc.getCodigo(),
                    ERol.LIDER_TECNICO);

            if (funcionarios != null && !funcionarios.isEmpty()) {
                this.lideresTecnicos.put(uoc.getCodigo(), true);
            } else {
                this.lideresTecnicos.put(uoc.getCodigo(), false);
            }

        }
    }

    /**
     * Determina si uo de los roles ya existe en al lista de roles del SNC
     *
     * @author felipe.cadena
     * @param rol
     * @return
     */
    private boolean containsRol(String rol) {

        for (String str : this.rolesSNC) {
            if (str.toUpperCase().equals(rol)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Valida si ya existe una parametrizacion con las mismas caracteristicas
     *
     * @author felipe.cadena
     *
     * @param parametrizacion
     * @return
     */
    private boolean validarExistenciaPrevia(RepUsuarioRolReporte parametrizacion) {

        FiltroDatosConsultaParametrizacionReporte criteriosBusqueda =
            new FiltroDatosConsultaParametrizacionReporte();
        criteriosBusqueda.setCategoria(parametrizacion.getRepReporte().getCategoria());
        criteriosBusqueda.setFuncionario(parametrizacion.getUsuarioAsignado());
        criteriosBusqueda.setRol(parametrizacion.getRolAsignado());
        criteriosBusqueda.setTipoReporteId(parametrizacion.getRepReporte().getId());
//        criteriosBusqueda.setGenerar(parametrizacion.getGenerar());
//        criteriosBusqueda.setConsultar(parametrizacion.getConsultar());

        for (RepTerritorialReporte reps : parametrizacion.getRepTerritorialReportes()) {

            criteriosBusqueda.setMunicipioCodigo(reps.getMunicipio().getCodigo());
            criteriosBusqueda.setDepartamentoCodigo(reps.getDepartamento().getCodigo());

            List<RepUsuarioRolReporte> parametrizacionesConsulta = this.getConservacionService()
                .buscarParametrizacionReporteUsuario(criteriosBusqueda);

            if (!parametrizacionesConsulta.isEmpty()) {
                return true;
            }

        }

        return false;
    }

    // ----------------------------------------------------- //
    /**
     * Método cerrar que forza realiza un llamado al método que remueve los managed bean de la
     * sesión y forza el init de tareasPendientesMB.
     *
     * @return
     */
    public String cerrar() {
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    // Fin de la clase
}
