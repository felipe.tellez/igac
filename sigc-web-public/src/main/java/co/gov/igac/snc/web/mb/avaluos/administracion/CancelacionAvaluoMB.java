package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.avaluos.radicacion.ConsultaSolicitudesAvaluosEnProcesoMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB para el caso de uso CU-SA-AC-105 Cancelar Avalúo Comercial
 *
 * @author felipe.cadena
 */
@Component("cancelarAvaluo")
@Scope("session")
public class CancelacionAvaluoMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -6024068512929698803L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(CancelacionAvaluoMB.class);
    /**
     * Avalúo seleccionado para la cancelacón
     */
    private Avaluo avaluoSeleccionado;
    /**
     * Justificación por la cual se realiza la cancelación
     */
    private String justificacionCancelacion;
    /**
     * Descripción de la justificación en caso de que el tipo de justificación sea 'Otra
     * Justificación'
     */
    private String descripcionJustificacion;
    /**
     * Bandera que activa el campo descripción cuando el tipo de justificación sea 'Otra
     * Justificación'
     */
    private Boolean banderaOtraJustificacion;
    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    @Autowired
    private ConsultaSolicitudesAvaluosEnProcesoMB consultaSolicitudesAvaluosEnProcesoMB;

    // --------getters-setters----------------
    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public Boolean getBanderaOtraJustificacion() {
        return this.banderaOtraJustificacion;
    }

    public void setBanderaOtraJustificacion(Boolean banderaOtraJustificacion) {
        this.banderaOtraJustificacion = banderaOtraJustificacion;
    }

    public String getJustificacionCancelacion() {
        return this.justificacionCancelacion;
    }

    public void setJustificacionCancelacion(String justificacionCancelacion) {
        this.justificacionCancelacion = justificacionCancelacion;
    }

    public String getDescripcionJustificacion() {
        return this.descripcionJustificacion;
    }

    public void setDescripcionJustificacion(String descripcionJustificacion) {
        this.descripcionJustificacion = descripcionJustificacion;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on CancelacionAvaluoMB init");
        this.iniciarVariables();

    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

    }

    /**
     * Método para realizar el proceso de cancelación del avalúo
     *
     * @author felipe.cadena
     */
    public void cancelarAvaluo() {
        LOGGER.debug("Cancelando avaluo ...");

        if (this.banderaOtraJustificacion) {
            this.avaluoSeleccionado = this.getAvaluosService().
                cancelarAvaluo(this.avaluoSeleccionado,
                    this.justificacionCancelacion,
                    this.descripcionJustificacion,
                    this.usuario);
        } else {
            this.avaluoSeleccionado = this.getAvaluosService().
                cancelarAvaluo(this.avaluoSeleccionado,
                    this.justificacionCancelacion,
                    null,
                    this.usuario);
        }

        this.consultaSolicitudesAvaluosEnProcesoMB.buscarAvaluos();

        LOGGER.debug("Avaluo cancelado");
    }

    /**
     * Método que el actualiza el valor de la bandera banderaOtraJustificacion según el tipo de
     * Justificación seleccionada por el usuario.
     *
     * @author felipe.cadena
     */
    public void listenerTipoDescripcion() {

        LOGGER.debug("Actualizando bandera ... " + this.justificacionCancelacion);

        if (this.justificacionCancelacion.equals(Constantes.OTRA_JUSTIFICACION)) {
            this.banderaOtraJustificacion = true;
        } else {
            this.banderaOtraJustificacion = false;
        }
    }
}
