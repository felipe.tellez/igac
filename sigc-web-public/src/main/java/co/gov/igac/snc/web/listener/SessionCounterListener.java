package co.gov.igac.snc.web.listener;

import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.ldap.SNCUserDetails;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.util.EMotivoLogout;
import co.gov.igac.snc.web.locator.SNCServiceLocator;
import co.gov.igac.snc.web.util.EAtributosSesion;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextImpl;

public class SessionCounterListener implements HttpSessionListener {

    private static Log LOGGER = LogFactory.getLog(SessionCounterListener.class);
    @Autowired
    private IContextListener contexto;
    private static int totalActiveSessions;

    public static int getTotalActiveSession() {
        return totalActiveSessions;
    }

    @Override
    public void sessionCreated(HttpSessionEvent arg0) {
        totalActiveSessions++;
        LOGGER.info("sessionCreated - add one session into counter");

        printCounter(arg0);
    }

    /**
     * Metodo que se ejecuta cuando la sesion se destruye, se registra la salida del usuario del
     * sistema, si no se trata de un usuario recurrente.
     *
     * @param arg0
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent arg0) {
        totalActiveSessions--;
        LOGGER.info("sessionDestroyed - deduct one session from counter");

        Boolean usuarioRecurrente = (Boolean) arg0.getSession().
            getAttribute(EAtributosSesion.USUARIO_RECURRENTE.getValor());
        String idSesion = arg0.getSession().getId();

        //Si el usuario es recurrente no se guarda registro de acceso
        if (usuarioRecurrente == null) {
            SecurityContextImpl securityContext = (SecurityContextImpl) arg0.getSession().
                getAttribute(EAtributosSesion.SPRING_SECURITY_CONTEXT.getValor());

            if (securityContext != null) {
                SNCUserDetails user = (SNCUserDetails) securityContext.getAuthentication().
                    getPrincipal();

                String usuario = user.getUsername();

                //Se cierra la conexion del usuario
                ConexionUsuario cu = this.getGeneralesService().obtenerConexionUsuarioPorSesionId(
                    idSesion);
                if (cu != null) {
                    this.getGeneralesService().eliminarConexionUsuario(cu);
                }

                this.getGeneralesService().registrarLogout(usuario, EMotivoLogout.CIERRE_SESION.
                    getValor());
                LOGGER.info("sesión destruida de usuario: " + user.getUsername());
            }
        }

        printCounter(arg0);
    }

    /**
     * Se necesita obtener el servicio para persistir el registro.
     *
     * @author felipe.cadena
     * @return
     */
    private IGenerales getGeneralesService() {
        IGenerales service = null;
        try {
            service = SNCServiceLocator.getInstance(contexto).getGeneralesService();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return service;
    }

    private void printCounter(HttpSessionEvent sessionEvent) {
        //HttpSession session = sessionEvent.getSession();
        LOGGER.info("totalActiveSessions:" + totalActiveSessions);
    }
}
