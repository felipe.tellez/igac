package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteMotivoDevolucion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el caso de uso de revisar trámites devueltos
 *
 * @author javier.aponte
 */
@Component("tramiteDevuelto")
@Scope("session")
public class TramiteDevueltoMB extends ProcessFlowManager {

    private static final long serialVersionUID = 994330951933545731L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TramiteDevueltoMB.class);

    /**
     * Variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena el trámite que se devolvió
     */
    private Tramite tramite;

    // **************** SERVICIOS ********************
    /**
     * Managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Managed bean para pintar los trámites de una comisión y permitir el retiro de éstos
     */
    @Autowired
    private TramitesDeComisionMB tramitesDeComisionMB;

    /**
     * Managed bean para usar métodos generales de web
     */
    @Autowired
    private GeneralMB generalMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Variable que almacena el número predial
     */
    private String numeroPredial;

    /**
     * Id del trámite que se haya seleccionado de la tabla que se despliega al dar click en una
     * actividad del árbol de tareas
     */
    private long idTramiteDeTrabajo;

    /**
     * Variable para manejar el estado del trámite
     */
    private TramiteEstado estadoTramite;

    /**
     * Contador de los documentos asociados al Tramite Seleccionado
     */
    private int countDocumentosTramiteSeleccionado;

    /**
     * Variable booleana para saber si se van a ver los documentos asociados o si se va a solicitar
     * nuevos Documentos
     */
    private boolean verSolicitudDocsBool;

    // **************** BANDERAS DE EDICION****************
    /**
     * Variable que almacena si mostrar o no, la opción de aceptar devolución
     */
    private boolean mostrarAceptaDevolucion;

    private boolean activarBotonSolicitarDocumentos;

    private boolean activarBotonReclasificarTramite;

    private boolean activarBotonDesasignarTramite;

    private boolean activarBotonDevolverAEjecutor;

    private boolean activarBotonAceptar;

    private boolean activarSeleccionSolucionadosOtrosMotivos;

    //-----------   banderas del reporte de memorando de comisión ------------------
    /**
     * Bandera que indica si el documento ha sido radicado
     */
    private boolean banderaDocumentoRadicado;

    private boolean generoMemorandoRetiroTramiteComision;

    /**
     * banderas de acciones realizadas. Sirven para determinar qué hacer al mover el proceso
     */
    private boolean devolvioTramite;

    private boolean solicitoDocumentos;

    private boolean reclasificoTramite;

    private boolean reclasificoTramiteConComision;

    private boolean desasignoTramite;

    private boolean desasignoTramiteConComision;

    private boolean solucionadosOtrosMotivos;

    //--------------   otras banderas   -------------------------
    /**
     * indica si se trata de un trámite de terreno cuya comisión habís sido cancelada. <br />
     * Se usa porque al devolver el trámite al ejecutor, si era de este tipo, la actividad a la que
     * se mueve es diferente
     *
     */
    private boolean tramiteConComisionCancelada;

    /**
     * Variable que almacena el número de radicado del memorando de retiro de trámite de comisión
     */
    private String numeroRadicado;

    /**
     * Variable que almacena la decisión de aceptar o no, cada uno de los motivos de devolución, se
     * muestra sólo si hay más de un motivo de devolución
     */
    private Map<String, String> aceptaMotivoDevolucion;

    /**
     * Lista que almacena los motivos de devolución aceptados por el responsable de conservación
     */
    private List<String> motivosDevolucionAceptados;

    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentActivity;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando retirar trámite de comisión
     */
    private ReporteDTO reporteMemorandoRetirarTramiteComision;

    /**
     * Variable que contiene la comisión asociada al trámite en el caso de que este esté comisionado
     */
    private VComision comisionSeleccionada;

    /**
     * Lista de usuarios con el rol de coordinador territorial.
     */
    private List<UsuarioDTO> coordinadoresTerritorial;

    /**
     * nombre de la estructura organizacional correspondiente
     */
    private String estructuraOrgSegunMnicipioTramite;

//-----------------------------------MÉTODOS--------------------------------------------------------
    public boolean isTramiteConComisionCancelada() {
        return this.tramiteConComisionCancelada;
    }

    public void setTramiteConComisionCancelada(boolean tramiteConComisionCancelada) {
        this.tramiteConComisionCancelada = tramiteConComisionCancelada;
    }

//--------------------------------------------------------------------------------------------------    
    @PostConstruct
    public void init() {
        LOGGER.debug("init EstableceProcedenciaSolicitudMB");

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "tramiteDevuelto");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.solicitoDocumentos = false;
        this.devolvioTramite = false;
        this.reclasificoTramite = false;
        this.reclasificoTramiteConComision = false;
        this.desasignoTramite = false;
        this.desasignoTramiteConComision = false;
        this.solucionadosOtrosMotivos = false;

        this.activarBotonAceptar = false;
        this.activarBotonDesasignarTramite = false;
        this.activarBotonDevolverAEjecutor = false;
        this.activarBotonReclasificarTramite = false;
        this.activarBotonSolicitarDocumentos = false;
        this.activarSeleccionSolucionadosOtrosMotivos = false;

        this.banderaDocumentoRadicado = false;

        this.generoMemorandoRetiroTramiteComision = false;

        try {

            if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
                this.addMensajeError(
                    "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
                return;
            }
            this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();
            //D: se obtiene el id del trámite
            this.idTramiteDeTrabajo =
                this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();

            //D: se consulta el trámite el BD
            this.tramite = this.getTramiteService().
                consultarTramiteDevuelto(this.idTramiteDeTrabajo);

            //felipe.cadena::15-09-2016::#19450::Reclamar actividad por parte del usuario autenticado        
            if (!this.reclamarActividad(this.tramite)) {
                this.cerrarPaginaPrincipal();
            }

            // Consulta los documentos aportados por el usuario
            if (this.tramite.getTramiteDocumentacions() != null && !this.tramite.
                getTramiteDocumentacions().isEmpty()) {
                this.countDocumentosTramiteSeleccionado = this.tramite.getTramiteDocumentacions().
                    size();
            }

            this.estadoTramite = this.getTramiteService().buscarTramiteEstadoVigentePorTramiteId(
                this.tramite.getId());

            this.aceptaMotivoDevolucion = new HashMap<String, String>();

            if (this.estadoTramite.getMotivo() != null) {

                String motivo[] = this.estadoTramite.getMotivo().split(
                    Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);

                //Si hay más de un motivo de devolución se muestra la parte de acepta devolución
                this.mostrarAceptaDevolucion = false;
                if (motivo.length > 1) {
                    this.mostrarAceptaDevolucion = true;

                    if (this.estadoTramite.getMotivo().contains(ETramiteMotivoDevolucion.OTRO.
                        getMotivo())) {
                        this.aceptaMotivoDevolucion.put(ETramiteMotivoDevolucion.OTRO.getMotivo(),
                            ETramiteMotivoDevolucion.OTRO.getMotivo());
                    }

                    if (this.estadoTramite.getMotivo().contains(
                        ETramiteMotivoDevolucion.POR_DOCUMENTOS.getMotivo())) {
                        this.aceptaMotivoDevolucion.put(ETramiteMotivoDevolucion.POR_DOCUMENTOS.
                            getMotivo(), ETramiteMotivoDevolucion.POR_DOCUMENTOS.getMotivo());
                    }

                    if (this.estadoTramite.getMotivo().contains(
                        ETramiteMotivoDevolucion.POR_RECLASIFICAR.getMotivo())) {
                        this.aceptaMotivoDevolucion.put(ETramiteMotivoDevolucion.POR_RECLASIFICAR.
                            getMotivo(), ETramiteMotivoDevolucion.POR_RECLASIFICAR.getMotivo());
                    }
                } else {

                    if (this.estadoTramite.getMotivo().contains(
                        ETramiteMotivoDevolucion.POR_RECLASIFICAR.getMotivo())) {
                        this.activarBotonReclasificarTramite = true;
                    }

                    if (this.estadoTramite.getMotivo().contains(
                        ETramiteMotivoDevolucion.POR_DOCUMENTOS.getMotivo())) {
                        this.activarBotonSolicitarDocumentos = true;
                    }

                    if (this.estadoTramite.getMotivo().contains(ETramiteMotivoDevolucion.OTRO.
                        getMotivo())) {
                        this.activarSeleccionSolucionadosOtrosMotivos = true;
                        //ya se desasignó el trámite
                        if (this.tramite.getFuncionarioEjecutor() == null) {
                            this.activarBotonAceptar = true;
                            this.desasignoTramite = true;
                        }

                    }
                }
            }

            if (this.tramite.getFuncionarioEjecutor() != null &&
                 this.tramite.getNombreFuncionarioEjecutor() != null) {
                this.activarBotonDesasignarTramite = true;
                this.activarBotonDevolverAEjecutor = true;
            }

            // Cargue de los cordinadores de la territorial.
            this.coordinadoresTerritorial = (List<UsuarioDTO>) this
                .getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.COORDINADOR);

            //D: definición de la estructura organizacional
            if (this.tramite.isQuinta()) {
                this.estructuraOrgSegunMnicipioTramite = this.getGeneralesService()
                    .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                        this.tramite.getMunicipio().getCodigo());
            } else {
                this.estructuraOrgSegunMnicipioTramite = this.getGeneralesService()
                    .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                        this.tramite.getPredio().getMunicipio().getCodigo());
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(e);
        }
    }

//--------------------------------------------------------------------------------------------------
    public String getNumeroPredial() {
        return this.numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public TramiteEstado getEstadoTramite() {
        return this.estadoTramite;
    }

    public void setEstadoTramite(TramiteEstado estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public boolean getVerSolicitudDocsBool() {
        return this.verSolicitudDocsBool;
    }

    public void setVerSolicitudDocsBool(boolean verSolicitudDocsBool) {
        this.verSolicitudDocsBool = verSolicitudDocsBool;
    }

    public boolean isMostrarAceptaDevolucion() {
        return mostrarAceptaDevolucion;
    }

    public void setMostrarAceptaDevolucion(boolean mostrarAceptaDevolucion) {
        this.mostrarAceptaDevolucion = mostrarAceptaDevolucion;
    }

    public boolean isActivarBotonSolicitarDocumentos() {
        return activarBotonSolicitarDocumentos;
    }

    public void setActivarBotonSolicitarDocumentos(
        boolean activarBotonSolicitarDocumentos) {
        this.activarBotonSolicitarDocumentos = activarBotonSolicitarDocumentos;
    }

    public boolean isActivarBotonReclasificarTramite() {
        return this.activarBotonReclasificarTramite;
    }

    public void setActivarBotonReclasificarTramite(
        boolean activarBotonReclasificarTramite) {
        this.activarBotonReclasificarTramite = activarBotonReclasificarTramite;
    }

    public boolean isActivarBotonDesasignarTramite() {
        return this.activarBotonDesasignarTramite;
    }

    public void setActivarBotonDesasignarTramite(
        boolean activarBotonDesasignarTramite) {
        this.activarBotonDesasignarTramite = activarBotonDesasignarTramite;
    }

    public boolean isActivarBotonDevolverAEjecutor() {
        return this.activarBotonDevolverAEjecutor;
    }

    public void setActivarBotonDevolverAEjecutor(
        boolean activarBotonDevolverAEjecutor) {
        this.activarBotonDevolverAEjecutor = activarBotonDevolverAEjecutor;
    }

    public boolean isActivarBotonAceptar() {
        return activarBotonAceptar;
    }

    public void setActivarBotonAceptar(boolean activarBotonAceptar) {
        this.activarBotonAceptar = activarBotonAceptar;
    }

    public boolean isActivarSeleccionSolucionadosOtrosMotivos() {
        return activarSeleccionSolucionadosOtrosMotivos;
    }

    public void setActivarSeleccionSolucionadosOtrosMotivos(
        boolean activarSeleccionSolucionadosOtrosMotivos) {
        this.activarSeleccionSolucionadosOtrosMotivos = activarSeleccionSolucionadosOtrosMotivos;
    }

    public boolean isSolucionadosOtrosMotivos() {
        return solucionadosOtrosMotivos;
    }

    public void setSolucionadosOtrosMotivos(boolean solucionadosOtrosMotivos) {
        this.solucionadosOtrosMotivos = solucionadosOtrosMotivos;
    }

    public int getCountDocumentosTramiteSeleccionado() {
        return countDocumentosTramiteSeleccionado;
    }

    public void setCountDocumentosTramiteSeleccionado(
        int countDocumentosTramiteSeleccionado) {
        this.countDocumentosTramiteSeleccionado = countDocumentosTramiteSeleccionado;
    }

    public ReporteDTO getReporteMemorandoRetirarTramiteComision() {
        return reporteMemorandoRetirarTramiteComision;
    }

    public void setReporteMemorandoRetirarTramiteComision(
        ReporteDTO reporteMemorandoRetirarTramiteComision) {
        this.reporteMemorandoRetirarTramiteComision = reporteMemorandoRetirarTramiteComision;
    }

    public boolean isBanderaDocumentoRadicado() {
        return banderaDocumentoRadicado;
    }

    public void setBanderaDocumentoRadicado(boolean banderaDocumentoRadicado) {
        this.banderaDocumentoRadicado = banderaDocumentoRadicado;
    }

    public boolean isGeneroMemorandoRetiroTramiteComision() {
        return generoMemorandoRetiroTramiteComision;
    }

    public void setGeneroMemorandoRetiroTramiteComision(
        boolean generoMemorandoRetiroTramiteComision) {
        this.generoMemorandoRetiroTramiteComision = generoMemorandoRetiroTramiteComision;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public Map<String, String> getAceptaMotivoDevolucion() {
        return aceptaMotivoDevolucion;
    }

    public void setAceptaMotivoDevolucion(Map<String, String> aceptaMotivoDevolucion) {
        this.aceptaMotivoDevolucion = aceptaMotivoDevolucion;
    }

    public List<String> getMotivosDevolucionAceptados() {
        return motivosDevolucionAceptados;
    }

    public void setMotivosDevolucionAceptados(
        List<String> motivosDevolucionAceptados) {
        this.motivosDevolucionAceptados = motivosDevolucionAceptados;
    }

//--------------------------------------------MÉTODOS-----------------------------------------------
//--------------------------------------------------------------------------------------------------
    /**
     * Método encargado de generar el memorando de retiro del trámite de la comisión
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 11/08/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarMemorandoRetirarTramiteComision() {

        FirmaUsuario firma = null;
        String documentoFirma;

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.MEMORANDO_RETIRO_TRAMITE_COMISION;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", String.valueOf(this.tramite.getId()));
        if (this.numeroRadicado != null) {
            parameters.put("NUMERO_RADICADO", this.numeroRadicado);
        }

        if (this.usuario != null && this.usuario.getNombreCompleto() != null) {

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                this.usuario.getNombreCompleto());

            if (firma != null && this.usuario.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                parameters.put("FIRMA_USUARIO", documentoFirma);
            } else {
                this.addMensajeInfo("El tramite no tiene firma de usuario asociada.");
            }
        }

        this.reporteMemorandoRetirarTramiteComision = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);
        this.generoMemorandoRetiroTramiteComision = true;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en Alfresco y
     * en la base de datos mediante el llamado al método guardar documento de la interfaz de
     * ITramite. Posteriormente le asocia el documento al trámite_documento creado al principio y
     * guarda el tramite_documento en la base de datos
     *
     * @author javier.aponte
     */
    public void guardarMemorandoRetirarTramiteComision() {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Documento documento = new Documento();

        tramiteDocumento.setTramite(this.tramite);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.MEMORANDO_PARA_RETIRAR_TRAMITE_DE_COMISION
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.reporteMemorandoRetirarTramiteComision != null) {
                documento.setArchivo(this.reporteMemorandoRetirarTramiteComision.
                    getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(this.tramite.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            //v1.1.7
            documento.setNumeroRadicacion(this.numeroRadicado);
            documento.setFechaRadicacion(new Date());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.usuario, documento);

            if (documento != null && documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                tramiteDocumento = this.getTramiteService().actualizarTramiteDocumento(
                    tramiteDocumento);
            }

        } catch (Exception e) {
            LOGGER.error(
                "Ocurrió un error al guardar el memorando de retiro de trámite de la comisión", e.
                    getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    public boolean isDevueltoPorReclasificarYPorDocumentos() {
        if (this.estadoTramite != null && this.estadoTramite.getMotivo() != null) {
            if (this.estadoTramite.getMotivo().contains(ETramiteMotivoDevolucion.OTRO.getMotivo())) {
                return true;
            }
        }
        return false;
    }

//------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado en el botón de solicitar documentos
     *
     * @return
     * @author javier.aponte
     */
    public String solicitarDocumentos() {

        this.solicitoDocumentos = true;

        forwardThisProcess();

        return this.cerrarPaginaPrincipal();
    }

//------------------------------------------------------------------------------------------------	
    /**
     * Acción de la confirmación para solicitar documentos cuando este está en una comisión no
     * cancelada.
     *
     * @author javier.aponte
     */
    public void solicitarDocumentosConComisionNoCancelada() {

        this.solicitoDocumentos = true;

        this.generarMemorandoRetirarTramiteComision();
    }
//-------------------------------------------------------------------------------------------------

    /**
     * Action del botón "Reclasificar Trámite", si el trámite es de oficina lo clasifica como
     * terreno, si el trámite es de terreno y ya está comisionado, elimina el trámite de la
     * comisión, en caso de que la comisión sólo tenga el trámite que se retira entonces se cancela
     * la comisión
     *
     * @author javier.aponte
     */
    /*
     * @modified pedro.garcia @modified leidy.gonzalez. Si el trámite es de terreno lo clasifica
     * como oficina
     */
    public void reclasificarTramite() {

        boolean error = false;
        if (ETramiteClasificacion.OFICINA.toString().equals(this.tramite.getClasificacion())) {
            this.tramite.setClasificacion(ETramiteClasificacion.TERRENO.toString());
            this.reclasificoTramite = true;
        }//v1.1.20
        else if (ETramiteClasificacion.TERRENO.toString().equals(this.tramite.getClasificacion())) {

            this.tramite.setClasificacion(ETramiteClasificacion.OFICINA.toString());
            this.reclasificoTramite = true;

            try {
                if (this.tramite.isTramiteComisionado()) {

                    if (this.tramite.getComisionTramite() != null &&
                        this.tramite.getComisionTramite().getComision() != null) {

                        if (!this.generoMemorandoRetiroTramiteComision) {
                            //Se retira el trámite de la comisión
                            this.retirarTramitesDeComision();
                            this.enviarCorreoAEjecutorYCoordinador();
                        }

                    }

                }

            } catch (Exception e) {
                LOGGER.debug("Error en TramiteDevueltoMB-reclasificar tramite " + e.getMessage());
                error = true;
            }

        }

        if (!error) {

            this.activarBotonAceptar = true;
            this.activarBotonReclasificarTramite = false;
            if (this.motivosDevolucionAceptados != null && !this.motivosDevolucionAceptados.
                isEmpty()) {
                if (this.motivosDevolucionAceptados.contains(
                    ETramiteMotivoDevolucion.POR_DOCUMENTOS.getMotivo())) {
                    this.activarBotonSolicitarDocumentos = true;
                }
                if (!this.activarBotonSolicitarDocumentos &&
                    this.motivosDevolucionAceptados.contains(ETramiteMotivoDevolucion.OTRO.
                        getMotivo())) {
                    this.activarSeleccionSolucionadosOtrosMotivos = true;
                }
            }

            this.getTramiteService().actualizarTramite(this.tramite, this.usuario);
            this.addMensajeInfo("El trámite se ha reclasificado exitosamente");

        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método encargado de retirar el trámite seleccionado de la comisión
     *
     * @author javier.aponte
     */
    private void retirarTramitesDeComision() {

        Tramite[] tramites = new Tramite[1];
        tramites[0] = this.tramite;

        Comision comision = this.tramite.getComisionTramite().getComision();

        List<Tramite> tramitesAsociadosComision = this.getTramiteService().buscarTramitesDeComision(
            comision.getId(), comision.getTipo());

        this.comisionSeleccionada = this.getTramiteService().getVComisionById(this.tramite.
            getComisionTramite().getComision().getId());

        this.tramitesDeComisionMB.setSelectedTramitesComision(tramites);
        this.tramitesDeComisionMB.setTramitesAsociadosComision(tramitesAsociadosComision);
        this.tramitesDeComisionMB.setSelectedVComision(comisionSeleccionada);
        this.tramitesDeComisionMB.setLoggedInUser(this.usuario);
        this.tramitesDeComisionMB.retirarTramitesComision();

        //Se toma el trámite con los cambios que se hicieron al retirar el trámite de la comisión
        this.tramite = this.tramitesDeComisionMB.getSelectedTramitesComision()[0];

    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Desasignar trámite", es decir, elimina el ejecutor que el trámite tiene
     * asignado
     *
     * @author javier.aponte
     */
    public void desasignarEjecutorDeTramite() {

        this.tramite.setFuncionarioEjecutor(null);
        this.tramite.setNombreFuncionarioEjecutor(null);
        this.getTramiteService().actualizarTramite(this.tramite, this.usuario);
        this.addMensajeInfo("El trámite se ha desasignado exitosamente");

        this.desasignoTramite = true;
        this.activarBotonDesasignarTramite = false;
        this.activarBotonDevolverAEjecutor = false;
        this.activarBotonAceptar = true;

        //Si el trámite está comisionado se retira el trámite de la comisión
        //y se envía correo a ejecutor y al coordinador
        if (this.tramite.isTramiteComisionado()) {
            this.retirarTramitesDeComision();
            this.enviarCorreoAEjecutorYCoordinador();
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Devolver a ejecutor" de la pantalla trámite devuelto, avanza el proceso
     * para que el trámite ahora le aparezca al ejecutor, porque se devuelve ya que el responsable
     * no acepta los motivos por los cuales el ejecutor le devolvió el trámite
     *
     * @author javier.aponte
     */
    public String devolverAEjecutor() {

        //D: indica que tomó esta acción
        this.devolvioTramite = true;

        forwardThisProcess();

        //D: CC-NO-CO-034  si el trámite estaba en una comisión que fue suspendida o aplazada, 
        // además de moverlo a la actividad correspondiente, se debe suspender la actividad en el bpm
        if (this.tramite.isTramiteComisionado()) {
            String estadoComision;
            if (this.tramite.getComisionTramite() != null &&
                this.tramite.getComisionTramite().getComision() != null) {
                estadoComision = this.tramite.getComisionTramite().getComision().getEstado();
                if (estadoComision.equals(EComisionEstado.APLAZADA.getCodigo()) ||
                    estadoComision.equals(EComisionEstado.SUSPENDIDA.getCodigo())) {
                    suspenderProcesoTramite();
                }
                //javier.aponte CC-NO-CO-034 Si la comisión está cancelada entonces elimina las relaciones de comisión trámite
                // y establece el trámite en comisionado NO
                if (this.isTramiteConComisionCancelada()) {
                    this.tramite = this.generalMB.
                        eliminarRelacionComisionTramite(this.tramite, this.usuario);
                }
            }
        }

        return this.cerrarPaginaPrincipal();
    }

//-------------------------------------------------------------------------------------------------
    /**
     * action del botón "aceptar". puede llegar habiendo reclasificado el trámite y desasignado el
     * ejecutor, o solo reclasificado
     *
     * @author javier.aponte
     */
    public String aceptarYAvanzarProceso() {

        this.activarBotonAceptar = false;
        this.forwardThisProcess();

        return this.cerrarPaginaPrincipal();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * método que se llama al dar click en el botón "radicar memorando de retiro trámite de
     * comisión" de la ventana "memorando de retiro de trámite de comisión"
     *
     * @author javier.aponte
     * @modified leidy.gonzalez
     */
    public void radicarMemorandoRetirarTramiteComision() {
        this.numeroRadicado = this.obtenerNumeroRadicado();
        if (this.numeroRadicado != null) {
            LOGGER.debug("Número Radicado: " + this.numeroRadicado);

            //1. Se genera el memorando de retiro de trámite de la comisión
            this.generarMemorandoRetirarTramiteComision();

            //2. Se guarda en el memorando en alfresco
            this.guardarMemorandoRetirarTramiteComision();
            this.banderaDocumentoRadicado = true;

            if (this.desasignoTramiteConComision) {
                //3. Se desasigna el ejecutor del trámite
                this.desasignarEjecutorDeTramite();

            } else if (this.reclasificoTramiteConComision) {
                //3. Se reclasifica el trámite
                this.reclasificarTramite();
            } else if (this.solicitoDocumentos) {
                this.retirarTramitesDeComision();
                this.enviarCorreoAEjecutorYCoordinador();
                this.activarBotonAceptar = true;
                this.activarBotonSolicitarDocumentos = false;
                this.activarBotonDevolverAEjecutor = false;
            }
        } else {
            this.addMensajeError(
                "Ocurrió un error al radicar el memorando de retiro de trámite de comisión, " +
                " por favor comuníquese con el administrador");
            this.activarBotonAceptar = false;
        }
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private String obtenerNumeroRadicado() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        UsuarioDTO ejecutorSeleccionado = this.getGeneralesService().getCacheUsuario(this.tramite.
            getFuncionarioEjecutor());

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.MEMORANDO_PARA_RETIRAR_TRAMITE_DE_COMISION.
            getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Memorando retirar trámite de comisión"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(ejecutorSeleccionado.getCodigoEstructuraOrganizacional()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(ejecutorSeleccionado.getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(ejecutorSeleccionado.getDireccionEstructuraOrganizacional()); // Direccion destino
        parametros.add(ejecutorSeleccionado.getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(ejecutorSeleccionado.getIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);
        //ultimo paso: llamar procedimiento recibirRadicado (Servicio de correspondencia para crear tramites)
        this.getTramiteService().recibirRadicado(numeroRadicado, this.usuario.getLogin());

        return numeroRadicado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método encargado de desasignar el ejecutor del trámite, cuando el trámite está comisionado,
     * por lo tanto se debe generar el memorando de retiro del trámite de la comisión y retirar el
     * trámite de la comisión
     *
     * @author javier.aponte
     */
    public void desasignarEjecutorDeTramiteConComisionNoCancelada() {

        this.generarMemorandoRetirarTramiteComision();
        this.desasignoTramiteConComision = true;

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método encargado de enviar correo al ejecutor y al coordinador, informandoles que se retira
     * el trámite de la comisión
     *
     * @author javier.aponte
     */
    public void enviarCorreoAEjecutorYCoordinador() {

        List<String> destinatariosList;

        // Se ajustan y setean los parámetros referentes al retiro del trámite de la comisión
        // envio de correo electrónico.
        try {

            destinatariosList = obtenerCorreosEjecutoresDeComisionSeleccionada();

            // Coordinadores de conservación de la UOC.
            if (this.coordinadoresTerritorial != null &&
                 !this.coordinadoresTerritorial.isEmpty()) {
                for (UsuarioDTO coordinador : this.coordinadoresTerritorial) {
                    if (coordinador != null && coordinador.getLogin() != null &&
                         !coordinador.getLogin().trim().isEmpty()) {
                        destinatariosList.add(coordinador.getEmail());
                    }
                }
            }

            String[] destinatarios = destinatariosList
                .toArray(new String[destinatariosList.size()]);

            // Remitente
            String remitente = this.usuario.getEmail();

            // Asunto y el contenido del correo electrónico.
            String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_RETIRO_TRAMITE_DE_COMISON;

            // Plantilla de contenido
            String codigoPlantillaContenido =
                EPlantilla.MENSAJE_COMUNICACION_RETIRO_TRAMITE_DE_COMISON
                    .getCodigo();

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaAsunto = new String[2];
            parametrosPlantillaAsunto[0] = this.tramite.getNumeroRadicacion();
            parametrosPlantillaAsunto[1] = this.comisionSeleccionada.getNumero();

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaContenido = new String[3];
            parametrosPlantillaContenido[0] = this.tramite.getNumeroRadicacion();
            parametrosPlantillaContenido[1] = this.comisionSeleccionada.getNumero();
            parametrosPlantillaContenido[2] = this.usuario.getNombreCompleto();

            // Listado de archivos adjuntos
            String[] adjuntos = new String[]{this.reporteMemorandoRetirarTramiteComision
                .getRutaCargarReporteAAlfresco()};

            // Nombre de los archivos adjuntos
            String[] titulosAdjuntos = new String[]{
                ConstantesComunicacionesCorreoElectronico.ARCHIVO_ADJUNTO_RETIRO_TRAMITE_DE_COMISION};

            // Cargar parámetros en el managed bean del componente.
            VisualizacionCorreoActualizacionMB visualCorreoMB =
                (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                    .getManagedBean("visualizacionCorreoActualizacion");
            visualCorreoMB.inicializarParametros(asunto, destinatarios,
                remitente, adjuntos, codigoPlantillaContenido,
                parametrosPlantillaAsunto, parametrosPlantillaContenido, titulosAdjuntos);

            // Envio de correo electrónico
            visualCorreoMB.enviarCorreo();
        } catch (Exception e) {
            LOGGER.error("Error al enviar el correo del retiro del trámite de la comisión " +
                 this.comisionSeleccionada.getNumero() + ".");
            LOGGER.debug(e.getMessage());
            this.addMensajeError(
                "Error al enviar el correo de retiro del trámite con número de radicación: " +
                this.tramite.getNumeroRadicacion() +
                 " de la comisión " + this.comisionSeleccionada.getNumero() + ".");
        }

    }

    /**
     * arma una lista con las direcciones de correo de los ejecutores relacionados con la comisión
     * actualmente seleccionada
     *
     * @autor pedro.garcia
     * @return
     */
    private ArrayList<String> obtenerCorreosEjecutoresDeComisionSeleccionada() {

        ArrayList<String> answer = new ArrayList<String>();
        String[] ejecutores;
        String emailIGAC;
        UsuarioDTO funcionarioEjecutor;

        if (this.comisionSeleccionada == null) {
            List<ComisionTramite> comisionesTramite =
                this.getTramiteService().buscarComisionesTramitePorIdTramite(this.tramite.getId());

            if (comisionesTramite != null && !comisionesTramite.isEmpty()) {
                this.comisionSeleccionada = this.getTramiteService().getVComisionById(
                    comisionesTramite.get(0).getComision().getId());
            }
        }

        if (this.comisionSeleccionada.getFuncionarioEjecutor() != null) {
            ejecutores = this.comisionSeleccionada.getFuncionarioEjecutor().
                split(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
            for (String currentEjecutor : ejecutores) {
                funcionarioEjecutor = this.getGeneralesService().getCacheUsuario(currentEjecutor);
                emailIGAC = funcionarioEjecutor.getEmail();
                answer.add(emailIGAC);
            }
        }

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * No es necesario validar nada
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    /*
     * @modified pedro.garcia 06-12-2013 CC-NP-CO-034
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO message = new ActivityMessageDTO();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();

        String transitionName = "";

        UsuarioDTO usuarioDestinoActividad = null;

        message.setUsuarioActual(this.usuario);

        message.setActivityId(this.currentActivity.getId());

        //D: la prioridad la tiene la solicitud de documentos
        if (this.solicitoDocumentos) {
            transitionName = ProcesoDeConservacion.ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE;
            usuarioDestinoActividad = this.usuario;
            usuariosActividad.add(usuarioDestinoActividad);
            solicitudCatastral.setUsuarios(usuariosActividad);
            this.solicitoDocumentos = false;
        } //D: si deasigna ejecutor, se da prioridad a eso y se envía a asignación de ejecutor
        else if (this.desasignoTramite) {
            transitionName = ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES;
            usuarioDestinoActividad = this.usuario;
            usuariosActividad.add(usuarioDestinoActividad);
            solicitudCatastral.setUsuarios(usuariosActividad);
            this.desasignoTramite = false;
        } else if (this.reclasificoTramite) {
            if (this.tramite.getClasificacion().equalsIgnoreCase(ETramiteClasificacion.TERRENO.
                toString())) {

                if (this.tramite.isRevisionAvaluo() ||
                     this.tramite.isCuarta()) {
                    transitionName = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
                } else {
                    transitionName = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                }

                solicitudCatastral.setUsuarios(this.getTramiteService().
                    buscarFuncionariosPorRolYTerritorial(
                        this.estructuraOrgSegunMnicipioTramite, ERol.RESPONSABLE_CONSERVACION));
            } else if (this.tramite.getClasificacion().equalsIgnoreCase(
                ETramiteClasificacion.OFICINA.toString())) {
                transitionName = ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(this.tramite.
                    getFuncionarioEjecutor());
                usuariosActividad.add(usuarioDestinoActividad);
                solicitudCatastral.setUsuarios(usuariosActividad);

            }
            this.reclasificoTramite = false;
        } //D: si devolvió el trámite se supone que nada más pudo haber hecho en la página
        else if (this.devolvioTramite) {

            //D: CC-NP-CO-034 si el trámite pertenecía a una comisión que había sido cancelada.
            //  Se envía a la actividad comisionar trámites de terreno
            if (this.tramiteConComisionCancelada) {
                if (this.tramite.isRevisionAvaluo() ||
                     this.tramite.isCuarta()) {
                    transitionName = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
                } else {
                    transitionName = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                }

                usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.estructuraOrgSegunMnicipioTramite, ERol.RESPONSABLE_CONSERVACION);
            } else {

                if (this.tramite.getClasificacion().equalsIgnoreCase(ETramiteClasificacion.TERRENO.
                    toString()) &&
                     this.tramite.getComisionTramites() != null &&
                     this.tramite.getComisionTramite().getComision() != null) {
                    Comision relatedComision = this.tramite.getComisionTramite().getComision();

                    Date currentDate = new Date(System.currentTimeMillis());

                    if (relatedComision.getFechaFin() != null &&
                         UtilidadesWeb.calcularNumeroDiasEntreFechas(
                            relatedComision.getFechaFin(), currentDate) > 0) {
                        //La comisión ya finalizó, se retirará el trámite de la comisión
                        this.retirarTramitesDeComision();

                        //Se enviará a la actividad comisionar trámites de terreno
                        if (this.tramite.isRevisionAvaluo() ||
                             this.tramite.isCuarta()) {
                            transitionName = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
                        } else {
                            transitionName = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                        }

                        usuariosActividad = this.getTramiteService().
                            buscarFuncionariosPorRolYTerritorial(
                                this.estructuraOrgSegunMnicipioTramite,
                                ERol.RESPONSABLE_CONSERVACION);
                    } else {
                        //D: la cadena de transición es la misma en este caso y cuando reclasifica de terreno a oficina
                        transitionName =
                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                        usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                            this.tramite.getFuncionarioEjecutor());
                        usuariosActividad.add(usuarioDestinoActividad);
                    }

                } else {
                    //D: la cadena de transición es la misma en este caso y cuando reclasifica de terreno a oficina
                    transitionName = ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                    usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                        this.tramite.getFuncionarioEjecutor());
                    usuariosActividad.add(usuarioDestinoActividad);

                    // tramite comisionado y comisi{on aplazada o suspendida => se suspende actividad del bpm
                    //TODO :: pedro.garcia refs #5922 :: pendiente definir si es que se deben suspender las actividades para el caso de comisión aplzada o suspendida
                }

            }
            solicitudCatastral.setUsuarios(usuariosActividad);
            this.devolvioTramite = false;
        }

        message.setTransition(transitionName);
        solicitudCatastral.setTransicion(transitionName);

        message.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(message);

        if (this.getMessage().getComment() != null) {
            message.setComment(this.getMessage().getComment());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * actualiza el trámite
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        if (this.reclasificoTramite || this.desasignoTramite) {
            this.getTramiteService().actualizarTramite(this.tramite, this.usuario);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * en el caso en que el llamado al BPM se debe hacer desde un botón que no está definido en
     * alguno de los template de procesos (porque por ejemplo hay al mismo tiempo dos botones que
     * mueven la actividad a diferentes transiciones), se debe hacer el llamado a los métodos como
     * si se estuviera implementando alguno de los métodos que avanzan el proceso del ProcessMB
     *
     * @author pedro.garcia
     */
    private void forwardThisProcess() {

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();

            this.forwardProcess();

        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     *
     * @author javier.aponte
     */
    public String cerrarPaginaPrincipal() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("tramiteDevuelto");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método encargado de activar los botones de los motivos de devolución aceptados por el
     * responsable de conservación
     *
     * @author javier.aponte
     */
    public void activarBotonesMotivosAceptados() {

        this.activarBotonReclasificarTramite = false;
        this.activarBotonSolicitarDocumentos = false;
        this.activarSeleccionSolucionadosOtrosMotivos = false;

        if (this.estadoTramite.getMotivo().contains(ETramiteMotivoDevolucion.POR_RECLASIFICAR.
            getMotivo()) &&
             this.motivosDevolucionAceptados.contains(ETramiteMotivoDevolucion.POR_RECLASIFICAR.
                getMotivo()) &&
             !this.reclasificoTramite) {
            this.activarBotonReclasificarTramite = true;
            if (this.activarBotonDesasignarTramite) {
                this.activarBotonDevolverAEjecutor = true;
            }
        }

        if (this.estadoTramite.getMotivo().contains(ETramiteMotivoDevolucion.POR_DOCUMENTOS.
            getMotivo()) &&
             this.motivosDevolucionAceptados.contains(ETramiteMotivoDevolucion.POR_DOCUMENTOS.
                getMotivo())) {
            this.activarBotonSolicitarDocumentos = true;
        }

        if (this.estadoTramite.getMotivo().contains(ETramiteMotivoDevolucion.OTRO.getMotivo())) {
            if (this.motivosDevolucionAceptados != null && !this.motivosDevolucionAceptados.
                isEmpty()) {
                if (this.motivosDevolucionAceptados.contains(ETramiteMotivoDevolucion.OTRO.
                    getMotivo())) {
                    if (!this.activarBotonReclasificarTramite &&
                        !this.activarBotonSolicitarDocumentos) {
                        this.activarSeleccionSolucionadosOtrosMotivos = true;
                    }
                }
            } else {
                this.activarSeleccionSolucionadosOtrosMotivos = true;
            }
        }

        if (this.solucionadosOtrosMotivos && this.activarBotonDesasignarTramite) {
            this.activarBotonDevolverAEjecutor = true;
        }

        //Si el usuario selecciono que acepta los motivos de devolución por reclasificar y por documentos,
        //entonces primero debe reclasificar y luego si puede solicitar los documentos
        if (this.activarBotonReclasificarTramite &&
             this.activarBotonSolicitarDocumentos) {
            this.activarBotonSolicitarDocumentos = false;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Acción de la confirmación para reclasificar un trámite cuando este está en una comisión no
     * cancelada.
     *
     * @author pedro.garcia
     */
    public void reclasificarTramiteConComisionNoCancelada() {

        this.generarMemorandoRetirarTramiteComision();

        this.reclasificoTramiteConComision = true;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * maneja la suspensión del proceso del trámite
     *
     * @author pedro.garcia
     */
    private boolean suspenderProcesoTramite() {

        boolean answer = false;

        List<Actividad> actividades = null;
        Actividad actividad;
        String idTramite, nombreActividad;
        Map<EParametrosConsultaActividades, String> filtros;

        idTramite = this.tramite.getId().toString();
        filtros =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        filtros.put(EParametrosConsultaActividades.ID_TRAMITE, idTramite);
        filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);

        try {
            actividades = this.getProcesosService().consultarListaActividades(filtros);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error consultando la lista de actividades para el trámite " + idTramite);
            return answer;
        }

        if (actividades != null && !actividades.isEmpty()) {

            if (actividades.size() > 1) {
                LOGGER.warn("La lista de actividades para el trámite " + idTramite +
                    " tiene más de un elemento. Se toma la primera actividad para hacer el movimiento del trámite por cancelación de la comisión.");
            }

            actividad = actividades.get(0);
            nombreActividad = actividad.getNombre();

            //D: la actividad en la que está debería ser 'revisar trámite asignado' porque antes de 
            //  llamar a este método se movió a esa.
            if (nombreActividad.
                equals(ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {

                ComisionEstado ce;
                //N: el último estado de la comisión debería ser 'aplazada' o 'suspendida'
                ce = this.getTramiteService().consultarUltimoCambioEstadoComision(
                    this.tramite.getComisionTramite().getComision().getId());
                if (ce == null) {
                    return answer;
                }

                Calendar fechaReanudacion, fechaSuspensionHasta;

                //D: si la suspensión a aplazamiento es a término indefinido o no
                if (ce.getFechaFin() == null) {
                    fechaReanudacion = null;
                } else {
                    fechaReanudacion = Calendar.getInstance();
                    fechaReanudacion.setTime(ce.getFechaInicio());
                }

                //D: suspender el proceso del trámite
                fechaSuspensionHasta = null;
                fechaSuspensionHasta = this.getProcesosService().
                    suspenderActividad(actividad.getId(), fechaReanudacion,
                        "Por suspensión o aplazamiento de la comisión");
                if (fechaSuspensionHasta != null) {
                    answer = true;
                }
            } else {
                LOGGER.warn(
                    "El trámite que se iba a suspender no está en la actividad que debía estar: " +
                     idTramite + " :: " +
                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);
            }
        }

        return answer;
    }

    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author felipe.cadena
     */
    private boolean reclamarActividad(Tramite tramiteReclamar) {
        Actividad actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        List<Actividad> actividades;

        if (actividad == null) {
            actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (actividades != null && !actividades.isEmpty()) {
                for (Actividad actTemp : actividades) {
                    if (actTemp.getIdObjetoNegocio() == tramiteReclamar.getId()) {
                        actividad = actTemp;
                        break;
                    }
                }
            }
        }

        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.usuario.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    if (tramiteReclamar.getNumeroRadicacion() != null && !tramiteReclamar.
                        getNumeroRadicacion().isEmpty()) {
                        this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                            tramiteReclamar.getNumeroRadicacion()));
                    } else {
                        this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA_NO_RAD.
                            getMensajeUsuario(tramiteReclamar.getSolicitud().getNumero()));
                    }
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.usuario);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                if (tramiteReclamar.getNumeroRadicacion() != null && !tramiteReclamar.
                    getNumeroRadicacion().isEmpty()) {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                        tramiteReclamar.getNumeroRadicacion()));
                } else {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA_NO_RAD.getMensajeUsuario(
                        tramiteReclamar.getSolicitud().getNumero()));
                }
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }
    }

//end of class    
}
