package co.gov.igac.snc.web.mb.tramite.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Clase que hace de MB para aceptar o rechazar un recurso de apelación
 *
 * @author juan.agudelo
 *
 */
@Component("registrarAceptacionRechazoRA")
@Scope("session")
public class RegistrarAceptacionRechazoRAMB extends SNCManagedBean implements
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistrarAceptacionRechazoRAMB.class);

    /**
     * Trámite seleccionado del arbol de recursos de apelación del menu de tareas del director de
     * territorial
     */
    private Tramite tramiteSeleccionado;

    /**
     * Almacena el valor de la seleccion de aprobación o rechazo
     */
    private int aprobarSeleccion;

    /**
     * Almacena las caracteristicas del trámite en el estado actual
     */
    private TramiteEstado tramiteEstadoActual;

    /**
     * Bandera de visualización del boton guardar
     */
    private boolean banderaBotonGuardar;

    /**
     * Usuario del sistema
     */
    private UsuarioDTO usuario;

    /**
     * Solicitud temporal pero debe ser la del tramite
     */
    private Solicitud solicitud;

    /**
     * Lista con los documentos aportados al trámite
     */
    private List<TramiteDocumentacion> tramiteDocumentosAportados;

    /**
     * Lista con los documentos faltantes en el trámite
     */
    private List<TramiteDocumentacion> tramiteDocumentosFaltantes;

    // ------------- methods ------------------------------------
    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public List<TramiteDocumentacion> getTramiteDocumentosAportados() {
        return tramiteDocumentosAportados;
    }

    public void setTramiteDocumentosAportados(
        List<TramiteDocumentacion> tramiteDocumentosAportados) {
        this.tramiteDocumentosAportados = tramiteDocumentosAportados;
    }

    public List<TramiteDocumentacion> getTramiteDocumentosFaltantes() {
        return tramiteDocumentosFaltantes;
    }

    public void setTramiteDocumentosFaltantes(
        List<TramiteDocumentacion> tramiteDocumentosFaltantes) {
        this.tramiteDocumentosFaltantes = tramiteDocumentosFaltantes;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public int getAprobarSeleccion() {
        return aprobarSeleccion;
    }

    public void setAprobarSeleccion(int aprobarSeleccion) {
        this.aprobarSeleccion = aprobarSeleccion;
    }

    public TramiteEstado getTramiteEstadoActual() {
        return tramiteEstadoActual;
    }

    public void setTramiteEstadoActual(TramiteEstado tramiteEstadoActual) {
        this.tramiteEstadoActual = tramiteEstadoActual;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("RegistrarAceptacionRechazoRAMB#init");

        this.usuario = new UsuarioDTO();
        this.banderaBotonGuardar = false;
        this.tramiteEstadoActual = new TramiteEstado();
        this.tramiteDocumentosAportados = new ArrayList<TramiteDocumentacion>();
        this.tramiteDocumentosFaltantes = new ArrayList<TramiteDocumentacion>();

        // TODO debe ser el usuario del sistema
        this.usuario.setLogin("juan.agudelo");

        // TODO debe ser el tramite seleccionado desde el arbol de acepatacion o
        // rechazo de recursos de apelación
        Long tramiteId = 135L;
        this.tramiteSeleccionado = this.getTramiteService()
            .buscarTramiteSolicitudPorId(tramiteId);

        this.solicitud = new Solicitud();

        this.aprobarSeleccion = 0;
        //TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir este método :: juan.agudelo
        /* this.tramiteEstadoActual .setEstado(ETramiteEstado.RESOLVIENDO_RECURSO_DE_APELACION
						.getCodigo()); */
        if (this.tramiteSeleccionado.getTramiteDocumentacions().size() > 0) {
            for (TramiteDocumentacion td : this.tramiteSeleccionado
                .getTramiteDocumentacions()) {
                if (td.getAportado().equals(ESiNo.SI.getCodigo())) {
                    this.tramiteDocumentosAportados.add(td);
                } else if (td.getAportado().equals(ESiNo.NO.getCodigo())) {
                    this.tramiteDocumentosFaltantes.add(td);
                }
            }
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para guardar los datos de aceptación o rechazo del reocurso de apelacion al pulsar el
     * boton guardar
     */
    public void guardar() {

        if (this.tramiteEstadoActual.getMotivo().length() > Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO) {
            this.addMensajeError("El tamaño del texto del motivo no debe ser superior a " +
                Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO + " caracteres");
            return;
        }

        this.tramiteEstadoActual.setTramite(this.tramiteSeleccionado);
        this.tramiteEstadoActual.setFechaInicio(new Date(System
            .currentTimeMillis()));

        try {
            @SuppressWarnings("unused")
            //TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir este método :: javier.aponte
            /* boolean answer =
             * remoteTramite.actualizarTramiteEstado( this.tramiteEstadoActual, usuario); */
            String mensaje = "Recurso procesado satisfactoriamente!";
            this.addMensajeInfo(mensaje);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            String mensaje = "Se presento un error al procesar el recurso!";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para actualizar la seleccion del radio button
     */
    public void seleccionar() {

        if (this.aprobarSeleccion == 0) {
            //TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir este método :: juan.agudelo
            /* this.tramiteEstadoActual .setEstado(ETramiteEstado.RESOLVIENDO_RECURSO_DE_APELACION
							.getCodigo()); */
        } else if (this.aprobarSeleccion == 1) {
            //TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir este método :: juan.agudelo
            /* this.tramiteEstadoActual
             * .setEstado(ETramiteEstado.EN_ESPERA_DE_LA_NOTIFICACION_DE_LA_NEGACION_DEL_RECURSO_DE_APELACION
							.getCodigo()); */
            this.tramiteSeleccionado.setEstado(ETramiteEstado.FINALIZADO
                .getCodigo());
        }

    }

}
