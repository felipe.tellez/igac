package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Managed Bean para el caso de uso de entregar resultado de levantamiento topógrafico
 *
 * @cu CU-NP-FA-211
 * @author javier.aponte
 *
 */
@Component("entregarResultadoDeLevantamientoTopografico")
@Scope("session")
public class EntregaResultadoDeLevantamientoTopograficoMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private String tiempoProgramado;
    private String trabajoProgramado;
    private String inconvenientesAdministrativos;
    private String resumenActividades;
    private String inconvenientesHumano;
    private String inconvenientesTecnico;
    private String inconvenientesLogistico;
    private String inconvenientesSocial;

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
    }

    /*
     * Getters y Setters
     *
     */
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getTiempoProgramado() {
        return tiempoProgramado;
    }

    public void setTiempoProgramado(String tiempoProgramado) {
        this.tiempoProgramado = tiempoProgramado;
    }

    public String getTrabajoProgramado() {
        return trabajoProgramado;
    }

    public void setTrabajoProgramado(String trabajoProgramado) {
        this.trabajoProgramado = trabajoProgramado;
    }

    public String getInconvenientesAdministrativos() {
        return inconvenientesAdministrativos;
    }

    public void setInconvenientesAdministrativos(String inconvenientesAdministrativos) {
        this.inconvenientesAdministrativos = inconvenientesAdministrativos;
    }

    public String getResumenActividades() {
        return resumenActividades;
    }

    public void setResumenActividades(String resumenActividades) {
        this.resumenActividades = resumenActividades;
    }

    public String getInconvenientesHumano() {
        return inconvenientesHumano;
    }

    public void setInconvenientesHumano(String inconvenientesHumano) {
        this.inconvenientesHumano = inconvenientesHumano;
    }

    public String getInconvenientesTecnico() {
        return inconvenientesTecnico;
    }

    public void setInconvenientesTecnico(String inconvenientesTecnico) {
        this.inconvenientesTecnico = inconvenientesTecnico;
    }

    public String getInconvenientesLogistico() {
        return inconvenientesLogistico;
    }

    public void setInconvenientesLogistico(String inconvenientesLogistico) {
        this.inconvenientesLogistico = inconvenientesLogistico;
    }

    public String getInconvenientesSocial() {
        return inconvenientesSocial;
    }

    public void setInconvenientesSocial(String inconvenientesSocial) {
        this.inconvenientesSocial = inconvenientesSocial;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

}
