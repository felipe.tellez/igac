package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaRecursoHidrico;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.CarguePrediosDeAvaluoDesdeArchivoMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 *
 * @author javier.aponte
 *
 */
@Component("registrarEvidenciasDeSocializacion")
@Scope("session")
public class RegistraEvidenciasDeSocializacionMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistraEvidenciasDeSocializacionMB.class);

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private UsuarioDTO usuario;

    /**
     * Variable usada en el pickList
     */
    private DualListModel<RecursoHumano> funcionariosYContratistas;
    private List<RecursoHumano> sourceFuncionariosYContratistas;
    private List<RecursoHumano> targetFuncionariosYContratistas;

    private boolean banderaAgregarActa;

    private Actualizacion currentActualizacion;

    private ActualizacionEvento actualizacionEventoSeleccionado;

    private List<ActualizacionEvento> actualizacionEventos;

    private EventoAsistente eventoAsistenteSeleccionado;

    private List<EventoAsistente> eventoAsistentes;

    /**
     * Almacena el archivo cargado
     */
    private File archivoResultado;

    /**
     * Bandera que indica si el archivo se cargo correctamente
     */
    private boolean banderaArchivoCargado;

    /**
     * Bandera que indica si el archivo se valido correctamente
     */
    private boolean banderaArchivoValidado;

    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

//		this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();
//		
//		Long idActualizacion = this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();
        Long idActualizacion = 450L;

        this.currentActualizacion = this.getActualizacionService().recuperarActualizacionPorId(
            idActualizacion);

        this.actualizacionEventoSeleccionado = new ActualizacionEvento();

        this.actualizacionEventoSeleccionado.setUsuarioLog(this.usuario.getLogin());
        this.actualizacionEventoSeleccionado.setFechaLog(new Date(System.currentTimeMillis()));
        this.actualizacionEventoSeleccionado.setActualizacion(this.currentActualizacion);

        this.actualizacionEventos = this.currentActualizacion.getActualizacionEventos();

        this.eventoAsistentes = new ArrayList<EventoAsistente>();

        this.eventoAsistenteSeleccionado = new EventoAsistente();

        this.sourceFuncionariosYContratistas = this.getActualizacionService().
            getRecursoHumanoPorActualizacionId(idActualizacion);

        this.targetFuncionariosYContratistas = new ArrayList<RecursoHumano>();
        funcionariosYContratistas = new DualListModel<RecursoHumano>(
            this.sourceFuncionariosYContratistas,
            this.targetFuncionariosYContratistas);

    }

    public void agregarAsistenteInvitado() {

        this.eventoAsistenteSeleccionado.
            setActualizacionEvento(this.actualizacionEventoSeleccionado);

        this.eventoAsistenteSeleccionado.setFechaLog(new Date(System.currentTimeMillis()));
        this.eventoAsistenteSeleccionado.setUsuarioLog(this.usuario.getLogin());

        this.eventoAsistentes.add(this.eventoAsistenteSeleccionado);

        this.eventoAsistenteSeleccionado = new EventoAsistente();

    }

    public void eliminarInvitado() {

        this.eventoAsistentes.remove(this.eventoAsistenteSeleccionado);

    }

    public void finalizar() {

        //OJO: hay necesidad de hacer esto aquí porque se no se usa el template que llama a ProcessMB.forwardProcess
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("registrarEvidenciasDeSocializacion");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();
    }

    /*
     * Getters y Setters
     */
    public Actualizacion getCurrentActualizacion() {
        return currentActualizacion;
    }

    public void setCurrentActualizacion(Actualizacion currentActualizacion) {
        this.currentActualizacion = currentActualizacion;
    }

    public DualListModel<RecursoHumano> getFuncionariosYContratistas() {
        return funcionariosYContratistas;
    }

    public void setFuncionariosYContratistas(
        DualListModel<RecursoHumano> funcionariosYContratistas) {
        this.funcionariosYContratistas = funcionariosYContratistas;
    }

    public boolean isBanderaAgregarActa() {
        return banderaAgregarActa;
    }

    public void setBanderaAgregarActa(boolean banderaAgregarActa) {
        this.banderaAgregarActa = banderaAgregarActa;
    }

    public ActualizacionEvento getActualizacionEventoSeleccionado() {
        return actualizacionEventoSeleccionado;
    }

    public void setActualizacionEventoSeleccionado(
        ActualizacionEvento actualizacionEventoSeleccionado) {
        this.actualizacionEventoSeleccionado = actualizacionEventoSeleccionado;
    }

    public EventoAsistente getEventoAsistenteSeleccionado() {
        return eventoAsistenteSeleccionado;
    }

    public void setEventoAsistenteSeleccionado(
        EventoAsistente eventoAsistenteSeleccionado) {
        this.eventoAsistenteSeleccionado = eventoAsistenteSeleccionado;
    }

    public List<EventoAsistente> getEventoAsistentes() {
        return eventoAsistentes;
    }

    public void setEventoAsistentes(List<EventoAsistente> eventoAsistentes) {
        this.eventoAsistentes = eventoAsistentes;
    }

    public boolean isBanderaArchivoCargado() {
        return banderaArchivoCargado;
    }

    public void setBanderaArchivoCargado(boolean banderaArchivoCargado) {
        this.banderaArchivoCargado = banderaArchivoCargado;
    }

    public boolean isBanderaArchivoValidado() {
        return banderaArchivoValidado;
    }

    public void setBanderaArchivoValidado(boolean banderaArchivoValidado) {
        this.banderaArchivoValidado = banderaArchivoValidado;
    }

    public List<ActualizacionEvento> getActualizacionEventos() {
        return actualizacionEventos;
    }

    public void setActualizacionEventos(
        List<ActualizacionEvento> actualizacionEventos) {
        this.actualizacionEventos = actualizacionEventos;
    }

    public void agregarActaDeSocializacion() {

        if (!banderaAgregarActa) {
            this.banderaAgregarActa = true;
        }

    }
    // ---------------------------------------------------------------------- //

    /**
     * Listener que se encarga de realizar la carga del archivo desde el componente de primefaces
     * fileupload
     *
     * @author javier.aponte
     * @param eventoCarga evento enviado por el componente FileUpload
     */
    public void cargarArchivo(FileUploadEvent eventoCarga) {

        this.banderaArchivoValidado = false;

        Object[] cargaArchivo = UtilidadesWeb.cargarArchivo(eventoCarga, this.getContexto());

        if (cargaArchivo[0] != null) {

            this.archivoResultado = (File) cargaArchivo[0];

            this.banderaArchivoCargado = true;

            String mensaje = "Documento cargado satisfactoriamente.";
            this.addMensajeInfo(mensaje);

        } else {

            this.banderaArchivoCargado = false;

            LOGGER.error("ERROR: " + cargaArchivo[2]);
            String mensaje = (String) cargaArchivo[1];
            this.addMensajeError(mensaje);
        }
    }

    /**
     * Método que elimina el objeto actulizacion evento seleccionado de la lista de actualización
     * eventos
     */
    public void eliminarActualizacionEvento() {

        if (this.actualizacionEventos.contains(this.actualizacionEventoSeleccionado)) {
            this.actualizacionEventos.remove(this.actualizacionEventoSeleccionado);
        }

    }
}
