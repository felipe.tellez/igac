package co.gov.igac.snc.web.mb.conservacion.reportes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import co.gov.igac.snc.persistence.entity.conservacion.*;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.*;
import co.gov.igac.snc.util.*;
import co.gov.igac.snc.web.util.*;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.jobs.ConsultarReportesBDRunnableJob;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;

/**
 * @description Managed bean para gestionar la consulta y generación de
 * diferentes tipos de reportes de las radicaciones.
 *
 * @cu CU-NP-CO-250 Generar reporte de estadisticos
 *
 * @version 1.0
 *
 * @author felipe.cadena
 */
@Component("gestionReporteEstadisticos")
@Scope("session")
public class GestionReportesEstadisticosMB extends SNCManagedBean implements
    Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -2005846748485473205L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(GestionReportesRadicacionMB.class);

    /**
     * ----------------------------------
     */
    /**
     * ----------- SERVICIOS ------------
     */
    /**
     * ----------------------------------
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * ----------------------------------
     */
    /**
     * ----------- CONSTANTES -----------
     */
    /**
     * ----------------------------------
     */
    private static final BigDecimal TAMANO_MAX_DESCARGAS = new BigDecimal(2048);
    //DISTRIBUCIONES DE RANGOS PERSONALIZADOS
    private static final int DIS_R_SUPERFICIE = 0;
    private static final int DIS_U_SUPERFICIE = 1;
    private static final int DIS_C_SUPERFICIE = 2;
    private static final int DIS_R_AVALUO = 3;
    private static final int DIS_U_AVALUO = 4;
    private static final int DIS_C_AVALUO = 5;

    /**
     * ----------------------------------
     */
    /**
     * ----------- VARIABLES ------------
     */
    /**
     * ----------------------------------
     */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Lista que almacena los reportes que han sido generados y cumplen con los
     * criterios de búsqueda
     */
    private List<RepReporteEjecucion> reportesRadicacionGenerados;

    /**
     * Variable que almacena la parametrización del usuario la cual es usada
     * para armar los menus de consulta
     */
    private List<RepUsuarioRolReporte> parametrizacionUsuario;

    /**
     * Variable usada para capturar el código de una territorial seleccionada de
     * la lista de territoriales.
     */
    private String territorialSeleccionadaCodigo;

    /**
     * Variable que almacena la territorial seleccionada.
     */
    private EstructuraOrganizacional territorialSeleccionada;

    /**
     * Lista de unidades operativas de catastro
     */
    private List<EstructuraOrganizacional> unidadesOperativasCatastroObj;

    /**
     * Lista de select items de las UOC.
     */
    private List<SelectItem> unidadesOperativasCatastroItemList;

    /**
     * Lista de select items de las territoriales
     */
    private ArrayList<SelectItem> territorialesItemList;

    /**
     * Variable usada para capturar el código de una UOC seleccionada de la
     * lista de UOCs de la territorial.
     */
    private String unidadOperativaSeleccionadaCodigo;

    /**
     * Variable que almacena la unidad operativa seleccionada.
     */
    private EstructuraOrganizacional unidadOperativaSeleccionada;

    /**
     * Variable usada para almacenar el còdigo del tipo de reporte seleccionado
     */
    private Long tipoReporteId;

    /**
     * Lista de select items de los tipos de reportes
     */
    private List<SelectItem> tipoReportesItemList;

    /**
     * Variable usada para almacenar el reporte seleccionado
     */
    private RepReporte repReporteSeleccionado;

    /**
     * Variable que almacena el formato seleccionado para la busqueda
     */
    private String formatoSeleccionado;

    /**
     * Variable que almacena el código del departamento seleccionado
     */
    private String departamentoSeleccionadoCodigo;

    /**
     * Lista de select items de los departamentos parametrizados
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Variable que almacena el código del municipio seleccionado
     */
    private String municipioSeleccionadoCodigo;

    /**
     * Lista de select items de los municipios parametrizados
     */
    private List<SelectItem> municipiosItemList;

    /**
     * Fechas de inicio y fin para la generación o consulta del informe
     */
    private Date fechaInicio;
    private Date fechaFin;
    private Date minDate;
    private Date maxDate;

    /**
     * Variable que almacena el login del funcionario seleccionado
     */
    private String rolSeleccionado;

    /**
     * Lista de select items de los funcionarios parametrizados
     */
    private List<SelectItem> rolesItemList;

    /**
     * Variable que almacena el login del funcionario seleccionado
     */
    private String funcionarioSeleccionado;

    /**
     * Lista de select items de los funcionarios parametrizados
     */
    private List<SelectItem> funcionariosItemList;

    /**
     * Variable que almacena el nombre del subproceso seleccionado
     */
    private EConservacionSubproceso subprocesoSeleccionado;

    /**
     * Lista de select items de los subprocesos de Conservación
     */
    private List<SelectItem> subprocesosItemList;

    /**
     * Variable que almacena el nombre de la actividad seleccionada
     */
    private EConservacionSubprocesoActividad actividadSeleccionada;

    /**
     * Lista de select items de las actividades del subproceso de conservación
     * seleccionado
     */
    private List<SelectItem> actividadesItemList;

    /**
     * Variable que almacena el nombre de la clasificación seleccionada
     */
    private String clasificacionSeleccionada;

    /**
     * Lista de select items de las clasificaciones de un trámite
     */
    private List<SelectItem> clasificacionesItemList;

    /**
     * Variable que almacena el rango de los días transcurridos al realizar la
     * consulta o generación de reportes de radicación.
     */
    private String rangoDiasTranscurridosSeleccionado;

    /**
     * Variable usada para almacenar el código único del reporte
     */
    private String codigoReporte;

    /**
     * Variables usadas para almacenar el año y consecutivo del reporte
     */
    private String anioReporteCod;
    private String codigoReporteCod;

    /**
     * Variable utilizada para habilitar la opción de generar reporte de
     * radicación
     */
    private boolean habilitarGenerarReporteBool;

    /**
     * Variable que tiene los campos de consulta de la parametrización
     */
    private FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroGenerarReportes datosConsultaPredio;

    /**
     * Variable para seleccionar todos los reportes que se encuentran en la
     * table de resultados de búsqueda
     */
    private boolean seleccionarTodosBool;

    /**
     * Variables para habilitar la busqueda y generación de reportes según la
     * parametrización del usuario.
     */
    private boolean permisoBuscarReporteBool;
    private boolean permisoGenerarReporteBool;

    /**
     * Variable que almacena el reporte seleccionado a visualizar
     */
    private RepReporteEjecucion reporteVisualizacionSeleccionado;

    /**
     * Variable para setear el Documento del reporte seleccionado para su
     * visualización
     */
    private String rutaPrevisualizacionReporte;

    /**
     * Variable de tipo streamed content que contiene el archivo con los
     * reportes seleccionados para la descarga.
     */
    private StreamedContent reportesDescarga;

    /**
     * Variable que almacena el histórico de las previsualizaciones vistas por
     * el usuario.
     */
    private Map<Long, String> listaRutasPrevisualizacionesReportes;

    /**
     * Variable que almacena la información del reporte a generar.
     */
    private RepReporteEjecucion repReporteEjecucion;

    /**
     * Reporte a ser visualizado con las caracteristicas ingresadas por el
     * usuario
     */
    private RepReporteEjecucionDetalle repReporteEjecucionDetalle;

    /**
     * Variable usada para la consulta de jobs en la base de datos
     */
    private ConsultarReportesBDRunnableJob consultarReportesBDRunnableJob;

    /**
     * Variable usada para habilitar o deshabilitar campos de consulta según el
     * tipo de reporte seleccionado
     */
    private boolean deshabilitarCamposConsultaBool;

    /**
     * Determina si se deben dehabilitar los campos posteriores al codigo de manzana
     */
    private boolean deshabilitarPostManzana;

    /**
     * Rangos disponibles para reportes
     */
    private List<SelectItem> rangosDisponibles;
    /**
     * Rango seleccionado
     */
    private String rangoSeleccionado;

    /**
     * Objeto que contiene las partes del número predial inicial cuando la
     * consulta es por rango
     */
    private NumeroPredialPartes numeroPredialPartesInicial;

    /**
     * Objeto que contiene las partes del número predial final cuando la
     * consulta es por rango
     */
    private NumeroPredialPartes numeroPredialPartesFinal;

    /**
     * Opciones de fechas de corte
     */
    private List<SelectItem> fechasCorte;

    /**
     * fecha de corte seleccionada
     */
    private String fechaCorteSeleccionada;

    /**
     * Variable item list con los valores de los años en los que se puede generar el reporte
     */
    private List<SelectItem> anoReporteItemList;

    /**
     * Variable que contiene el Dominio en BD de los años en los que se puede generar el reporte
     */
    List<Dominio> dominiosAnoReporte;

    /**
     * fecha de corte seleccionada
     */
    private String anioSeleccionado;

    /**
     * Determina los rangos habilitados para personalizacion
     */
    private boolean[] rangosHabilitados;

    /**
     * Listas para los rangos personalizados
     */
    private List<List<RangoReporte>> listasRangoReporte;

    /**
     * Rango seleccionado para edicion
     */
    private RangoReporte rangoReporteSeleccionado;

    /**
     * Rango catagoria seleccionado para edicion
     */
    private int rangoCategoriaSeleccionado;
    
    /**
     * Determina si se va a ingresar un rango personalizado
     */
    private boolean rangoPersonalizado;
    
    /**
     * Determina si se va a editar un rango existente
     */
    private boolean banderaEdicionRango;

    /**
     * Condiciones para determinar si los campos son habilitados o requeridos
     */
    private Map<String, Boolean[]> condicionesCampos;

    /**
     * Condiciones para todos los tipos de reportes
     */
    private ProcesaParametrosReporte condicionesTotalesCampos;

    /**
     * Variable usada para almacenar los usuarios asociados a un reporte
	 */
    private List<RepReporteEjecucionUsuario> reportesEjecucionUsuarios;

    /**
     * Lista con los reportes asociados a un usuario Finalizados
     */
    private List<RepReporteEjecucion> reportesUsuario;

    /**
     * Rango para habilitar
     */
    private int indexRangoHabilitado;

    private boolean nacional;
    private String tipoAgrupacion;
    private boolean incluirUocs;

    /**
     * fecha de corte seleccionada
     */
    private String tipo;


    private boolean busquedaPorCodigo = false;



    /**
     * ----------------------------------
     */
    /**
     * ------- GETTERS Y SETTERS --------
     */


    public Map<String, Boolean[]> getCondicionesCampos() {
        return condicionesCampos;
    }

    public void setCondicionesCampos(Map<String, Boolean[]> condicionesCampos) {
        this.condicionesCampos = condicionesCampos;
    }

    public String getFechaCorteSeleccionada() {
        return fechaCorteSeleccionada;
    }

    public void setFechaCorteSeleccionada(String fechaCorteSeleccionada) {
        this.fechaCorteSeleccionada = fechaCorteSeleccionada;
    }

    public boolean isDeshabilitarPostManzana() {
        return deshabilitarPostManzana;
    }

    public void setDeshabilitarPostManzana(boolean deshabilitarPostManzana) {
        this.deshabilitarPostManzana = deshabilitarPostManzana;
    }

    public int getIndexRangoHabilitado() {
        return indexRangoHabilitado;
    }

    public void setIndexRangoHabilitado(int indexRangoHabilitado) {
        if(indexRangoHabilitado>5){
            this.rangosHabilitados[indexRangoHabilitado-6] = false;
            this.listasRangoReporte.get(indexRangoHabilitado-6).clear();
        }else{
            this.rangosHabilitados[indexRangoHabilitado] = true;
        }
        this.indexRangoHabilitado = indexRangoHabilitado;
    }

    public boolean isRangoPersonalizado() {
        return rangoPersonalizado;
    }

    public void setRangoPersonalizado(boolean rangoPersonalizado) {
        this.rangoPersonalizado = rangoPersonalizado;
    }

    public boolean isBanderaEdicionRango() {
        return banderaEdicionRango;
    }

    public void setBanderaEdicionRango(boolean banderaEdicionRango) {
        this.banderaEdicionRango = banderaEdicionRango;
    }

    public boolean isNacional() {
        return nacional;
    }

    public void setNacional(boolean nacional) {
        this.nacional = nacional;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTipoAgrupacion() {
        return tipoAgrupacion;
    }

    public void setTipoAgrupacion(String tipoAgrupacion) {
        this.tipoAgrupacion = tipoAgrupacion;
    }

    public boolean isIncluirUocs() {
        return incluirUocs;
    }

    public void setIncluirUocs(boolean incluirUocs) {
        this.incluirUocs = incluirUocs;
    }

    public List<List<RangoReporte>> getListasRangoReporte() {
        return listasRangoReporte;
    }

    public void setListasRangoReporte(List<List<RangoReporte>> listasRangoReporte) {
        this.listasRangoReporte = listasRangoReporte;
    }

    public int getRangoCategoriaSeleccionado() {
        return rangoCategoriaSeleccionado;
    }

    public void setRangoCategoriaSeleccionado(int rangoCategoriaSeleccionado) {
        this.rangoCategoriaSeleccionado = rangoCategoriaSeleccionado;
    }

    public RangoReporte getRangoReporteSeleccionado() {
        return rangoReporteSeleccionado;
    }

    public void setRangoReporteSeleccionado(RangoReporte rangoReporteSeleccionado) {
        this.rangoReporteSeleccionado = rangoReporteSeleccionado;
    }

    public boolean[] getRangosHabilitados() {
        return rangosHabilitados;
    }

    public void setRangosHabilitados(boolean[] rangosHabilitados) {
        this.rangosHabilitados = rangosHabilitados;
    }

    public String getFechaCorteActual() {
        return EFechaCorteReporte.ACTUAL.getCodigo();
    }

    public String getAnioSeleccionado() {
        return anioSeleccionado;
    }

    public void setAnioSeleccionado(String anioSeleccionado) {
        this.anioSeleccionado = anioSeleccionado;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<RepUsuarioRolReporte> getParametrizacionUsuario() {
        return parametrizacionUsuario;
    }

    public void setParametrizacionUsuario(
        List<RepUsuarioRolReporte> parametrizacionUsuario) {
        this.parametrizacionUsuario = parametrizacionUsuario;
    }

    public String getAnioReporteCod() {
        return anioReporteCod;
    }

    public void setAnioReporteCod(String anioReporteCod) {
        this.anioReporteCod = anioReporteCod;
    }

    public String getCodigoReporteCod() {
        return codigoReporteCod;
    }

    public void setCodigoReporteCod(String codigoReporteCod) {
        this.codigoReporteCod = codigoReporteCod;
    }

    public Long getTipoReporteId() {
        return tipoReporteId;
    }

    public void setTipoReporteId(Long tipoReporteId) {
        this.tipoReporteId = tipoReporteId;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public EstructuraOrganizacional getTerritorialSeleccionada() {
        return territorialSeleccionada;
    }

    public void setTerritorialSeleccionada(
        EstructuraOrganizacional territorialSeleccionada) {
        this.territorialSeleccionada = territorialSeleccionada;
    }

    public RepReporteEjecucionDetalle getRepReporteEjecucionDetalle() {
        return repReporteEjecucionDetalle;
    }

    public void setRepReporteEjecucionDetalle(
        RepReporteEjecucionDetalle repReporteEjecucionDetalle) {
        this.repReporteEjecucionDetalle = repReporteEjecucionDetalle;
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public List<RepReporteEjecucion> getReportesRadicacionGenerados() {
        return reportesRadicacionGenerados;
    }

    public void setReportesRadicacionGenerados(
        List<RepReporteEjecucion> reportesRadicacionGenerados) {
        this.reportesRadicacionGenerados = reportesRadicacionGenerados;
    }

    public boolean isDeshabilitarCamposConsultaBool() {
        return deshabilitarCamposConsultaBool;
    }

    public void setDeshabilitarCamposConsultaBool(
        boolean deshabilitarCamposConsultaBool) {
        this.deshabilitarCamposConsultaBool = deshabilitarCamposConsultaBool;
    }

    public String getRutaPrevisualizacionReporte() {
        return rutaPrevisualizacionReporte;
    }

    public void setRutaPrevisualizacionReporte(String rutaPrevisualizacionReporte) {
        this.rutaPrevisualizacionReporte = rutaPrevisualizacionReporte;
    }

    public Map<Long, String> getListaRutasPrevisualizacionesReportes() {
        return listaRutasPrevisualizacionesReportes;
    }

    public void setListaRutasPrevisualizacionesReportes(
        Map<Long, String> listaRutasPrevisualizacionesReportes) {
        this.listaRutasPrevisualizacionesReportes = listaRutasPrevisualizacionesReportes;
    }

    public boolean isSeleccionarTodosBool() {
        return seleccionarTodosBool;
    }

    public void setSeleccionarTodosBool(boolean seleccionarTodosBool) {
        this.seleccionarTodosBool = seleccionarTodosBool;
    }

    public StreamedContent getReportesDescarga() {
        return reportesDescarga;
    }

    public void setReportesDescarga(StreamedContent reportesDescarga) {
        this.reportesDescarga = reportesDescarga;
    }

    public boolean isHabilitarGenerarReporteBool() {
        return habilitarGenerarReporteBool;
    }

    public void setHabilitarGenerarReporteBool(boolean habilitarGenerarReporteBool) {
        this.habilitarGenerarReporteBool = habilitarGenerarReporteBool;
    }

    public RepReporteEjecucion getReporteVisualizacionSeleccionado() {
        return reporteVisualizacionSeleccionado;
    }

    public void setReporteVisualizacionSeleccionado(
        RepReporteEjecucion reporteVisualizacionSeleccionado) {
        this.reporteVisualizacionSeleccionado = reporteVisualizacionSeleccionado;
    }

    public EConservacionSubproceso getSubprocesoSeleccionado() {
        return subprocesoSeleccionado;
    }

    public void setSubprocesoSeleccionado(EConservacionSubproceso subprocesoSeleccionado) {
        this.subprocesoSeleccionado = subprocesoSeleccionado;
    }

    public String getCodigoReporte() {
        return codigoReporte;
    }

    public void setCodigoReporte(String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }

    public EConservacionSubprocesoActividad getActividadSeleccionada() {
        return actividadSeleccionada;
    }

    public void setActividadSeleccionada(
        EConservacionSubprocesoActividad actividadSeleccionada) {
        this.actividadSeleccionada = actividadSeleccionada;
    }

    public boolean isPermisoBuscarReporteBool() {
        return permisoBuscarReporteBool;
    }

    public void setPermisoBuscarReporteBool(boolean permisoBuscarReporteBool) {
        this.permisoBuscarReporteBool = permisoBuscarReporteBool;
    }

    public boolean isPermisoGenerarReporteBool() {
        return permisoGenerarReporteBool;
    }

    public void setPermisoGenerarReporteBool(boolean permisoGenerarReporteBool) {
        this.permisoGenerarReporteBool = permisoGenerarReporteBool;
    }

    public FiltroDatosConsultaReportesRadicacion getDatosConsultaReportesRadicacion() {
        return datosConsultaReportesRadicacion;
    }

    public void setDatosConsultaReportesRadicacion(
        FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion) {
        this.datosConsultaReportesRadicacion = datosConsultaReportesRadicacion;
    }

    public String getRangoDiasTranscurridosSeleccionado() {
        return rangoDiasTranscurridosSeleccionado;
    }

    public void setRangoDiasTranscurridosSeleccionado(
        String rangoDiasTranscurridosSeleccionado) {
        this.rangoDiasTranscurridosSeleccionado = rangoDiasTranscurridosSeleccionado;
    }

    public String getClasificacionSeleccionada() {
        return clasificacionSeleccionada;
    }

    public void setClasificacionSeleccionada(
        String clasificacionSeleccionada) {
        this.clasificacionSeleccionada = clasificacionSeleccionada;
    }

    public List<SelectItem> getClasificacionesItemList() {
        return clasificacionesItemList;
    }

    public void setClasificacionesItemList(List<SelectItem> clasificacionesItemList) {
        this.clasificacionesItemList = clasificacionesItemList;
    }

    public List<SelectItem> getActividadesItemList() {
        return actividadesItemList;
    }

    public void setActividadesItemList(List<SelectItem> actividadesItemList) {
        this.actividadesItemList = actividadesItemList;
    }

    public List<SelectItem> getSubprocesosItemList() {
        return subprocesosItemList;
    }

    public void setSubprocesosItemList(List<SelectItem> subprocesosItemList) {
        this.subprocesosItemList = subprocesosItemList;
    }

    public List<SelectItem> getRolesItemList() {
        return rolesItemList;
    }

    public void setRolesItemList(List<SelectItem> rolesItemList) {
        this.rolesItemList = rolesItemList;
    }

    public List<SelectItem> getTipoReportesItemList() {
        return tipoReportesItemList;
    }

    public void setTipoReportesItemList(List<SelectItem> tipoReportesItemList) {
        this.tipoReportesItemList = tipoReportesItemList;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFuncionarioSeleccionado() {
        return funcionarioSeleccionado;
    }

    public void setFuncionarioSeleccionado(String funcionarioSeleccionado) {
        this.funcionarioSeleccionado = funcionarioSeleccionado;
    }

    public List<SelectItem> getFuncionariosItemList() {
        return funcionariosItemList;
    }

    public void setFuncionariosItemList(List<SelectItem> funcionariosItemList) {
        this.funcionariosItemList = funcionariosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public String getFormatoSeleccionado() {
        return formatoSeleccionado;
    }

    public void setFormatoSeleccionado(String formatoSeleccionado) {
        this.formatoSeleccionado = formatoSeleccionado;
    }

    public List<EstructuraOrganizacional> getUnidadesOperativasCatastroObj() {
        return unidadesOperativasCatastroObj;
    }

    public List<SelectItem> getUnidadesOperativasCatastroItemList() {
        return unidadesOperativasCatastroItemList;
    }

    public String getUnidadOperativaSeleccionadaCodigo() {
        return unidadOperativaSeleccionadaCodigo;
    }

    public void setUnidadOperativaSeleccionadaCodigo(
        String unidadOperativaSeleccionadaCodigo) {
        this.unidadOperativaSeleccionadaCodigo = unidadOperativaSeleccionadaCodigo;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return this.territorialesItemList;
    }

    public void setTerritorialesItemList(
        ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public RepReporte getRepReporteSeleccionado() {
        return repReporteSeleccionado;
    }

    public void setRepReporteSeleccionado(RepReporte repReporteSeleccionado) {
        this.repReporteSeleccionado = repReporteSeleccionado;
    }

    public RepReporteEjecucion getRepReporteEjecucion() {
        return repReporteEjecucion;
    }

    public void setRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion) {
        this.repReporteEjecucion = repReporteEjecucion;
    }

    public EstructuraOrganizacional getUnidadOperativaSeleccionada() {
        return unidadOperativaSeleccionada;
    }

    public String getDepartamentoSeleccionadoCodigo() {
        return departamentoSeleccionadoCodigo;
    }

    public void setDepartamentoSeleccionadoCodigo(
        String departamentoSeleccionadoCodigo) {
        if("null".equals(departamentoSeleccionadoCodigo)){
            departamentoSeleccionadoCodigo = null;
        }
        this.departamentoSeleccionadoCodigo = departamentoSeleccionadoCodigo;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public String getMunicipioSeleccionadoCodigo() {
        return municipioSeleccionadoCodigo;
    }

    public void setMunicipioSeleccionadoCodigo(String municipioSeleccionadoCodigo) {
        this.municipioSeleccionadoCodigo = municipioSeleccionadoCodigo;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setUnidadesOperativasCatastroObj(
        List<EstructuraOrganizacional> unidadesOperativasCatastroObj) {
        this.unidadesOperativasCatastroObj = unidadesOperativasCatastroObj;
    }

    public void setUnidadesOperativasCatastroItemList(
        List<SelectItem> unidadesOperativasCatastroItemList) {
        this.unidadesOperativasCatastroItemList = unidadesOperativasCatastroItemList;
    }

    public void setUnidadOperativaSeleccionada(
        EstructuraOrganizacional unidadOperativaSeleccionada) {
        this.unidadOperativaSeleccionada = unidadOperativaSeleccionada;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public List<SelectItem> getRangosDisponibles() {
        return rangosDisponibles;
    }

    public void setRangosDisponibles(List<SelectItem> rangosDisponibles) {
        this.rangosDisponibles = rangosDisponibles;
    }

    public String getRangoSeleccionado() {
        return rangoSeleccionado;
    }

    public void setRangoSeleccionado(String rangoSeleccionado) {
        this.rangoPersonalizado = rangoSeleccionado.
            equals(ERangoReporte.PERSONALIZADOS.toString());
        this.rangoSeleccionado = rangoSeleccionado;  
    }

    public NumeroPredialPartes getNumeroPredialPartesInicial() {
        return numeroPredialPartesInicial;
    }

    public void setNumeroPredialPartesInicial(NumeroPredialPartes numeroPredialPartesInicial) {
        this.numeroPredialPartesInicial = numeroPredialPartesInicial;
    }

    public NumeroPredialPartes getNumeroPredialPartesFinal() {
        return numeroPredialPartesFinal;
    }

    public void setNumeroPredialPartesFinal(NumeroPredialPartes numeroPredialPartesFinal) {
        this.numeroPredialPartesFinal = numeroPredialPartesFinal;
    }

    public List<SelectItem> getFechasCorte() {
        return fechasCorte;
    }

    public void setFechasCorte(List<SelectItem> fechasCorte) {
        this.fechasCorte = fechasCorte;
    }

    public List<SelectItem> getAnoReporteItemList() {
        return anoReporteItemList;
    }

    public void setAnoReporteItemList(List<SelectItem> anoReporteItemList) {
        this.anoReporteItemList = anoReporteItemList;
    }

    public boolean isBusquedaPorCodigo() {
        return busquedaPorCodigo;
    }

    public void setBusquedaPorCodigo(boolean busquedaPorCodigo) {
        this.busquedaPorCodigo = busquedaPorCodigo;
    }

    public FiltroGenerarReportes getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroGenerarReportes datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    /**
     * ----------------------------------
     */
    /**
     * -------------- INIT --------------
     */
    /**
     * ----------------------------------
     */
    @PostConstruct
    public void init() {
        try {
            this.usuario = MenuMB.getMenu().getUsuarioDto();

            // Consulta de parametrización del usuario
            this.parametrizacionUsuario = this.getConservacionService()
                .buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
                    this.usuario, ERepReporteCategoria.REPORTES_ESTADISTICOS.getCategoria());

            // Inicialización de variables
            this.territorialesItemList = new ArrayList<SelectItem>();
            this.unidadesOperativasCatastroItemList = new ArrayList<SelectItem>();
            this.departamentosItemList = new ArrayList<SelectItem>();
            this.municipiosItemList = new ArrayList<SelectItem>();
            this.tipoReportesItemList = new ArrayList<SelectItem>();
            this.rolesItemList = new ArrayList<SelectItem>();
            this.funcionariosItemList = new ArrayList<SelectItem>();

            // Cargue de menus
            this.cargarTiposDeReporte();
            this.cargarTerritoriales();
            this.cargarUOC();
            this.cargarSubprocesosConservacion();
            this.cargarClasificacion();
            this.cargaRangosYRestricciones();
            this.cargarAnoReporte();

            // Inicialización de una fecha mínima y máxima para el intervalo de consulta
            Calendar hoy = Calendar.getInstance();
            hoy.add(Calendar.DAY_OF_YEAR, -1);
            this.maxDate = hoy.getTime();

            SimpleDateFormat sm = new SimpleDateFormat("dd-mm-yyyy");
            this.minDate = sm.parse("01-01-2014");

            // Aunque la búsqueda se establecio que estaría habilitada se crea
            // la variable para que consuma las tablas de parametrización si se
            // quiere.
            this.permisoBuscarReporteBool = true;

            // Lista de previsualizaciones consultadas
            this.numeroPredialPartesFinal = new NumeroPredialPartes();
            this.numeroPredialPartesInicial = new NumeroPredialPartes();
            this.listaRutasPrevisualizacionesReportes = new HashMap<Long, String>();
            this.reportesRadicacionGenerados = new ArrayList<RepReporteEjecucion>();
            this.consultarReportesBDRunnableJob = new ConsultarReportesBDRunnableJob();
            this.codigoReporte = null;
            this.codigoReporteCod = null;
            this.anioReporteCod = null;
            this.rangosHabilitados = new boolean[6];
            this.fechaCorteSeleccionada = null;
            this.anioSeleccionado = null;
            Arrays.fill(this.rangosHabilitados, false);

            //cargar configuracion de campos
            cargarConfigCampos();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en el cargue de la parametrización del usuario.");
        }
    }

    /**
     * --------------------------------
     */
    /**
     * ----------- MÉTODOS ------------
     */
    /**
     * --------------------------------
     */
    /**
     * Metodo para inicializar las opciones de seleccion de rangos ademas de las
     * restricciones que aplican sobre ellos
     *
     * @author felipe.cadena
     */
    private void cargaRangosYRestricciones() {
        this.rangosDisponibles = new ArrayList<SelectItem>();
        this.fechasCorte = new ArrayList<SelectItem>();

        //inicializar rangos
        for (ERangoReporte err : ERangoReporte.values()) {
            this.rangosDisponibles.add(new SelectItem(err.getCodigo().toString(), err.getLabel()));
        }

        //fechas
        this.fechaCorteSeleccionada = EFechaCorteReporte.ACTUAL.getCodigo();
        for (EFechaCorteReporte err : EFechaCorteReporte.values()) {
            this.fechasCorte.add(new SelectItem(err.getCodigo(), err.getNombre()));
        }

        //inicializar listas de rangos
        this.listasRangoReporte = new ArrayList<List<RangoReporte>>();
        for (int i = 0; i < 6; i++) {
            this.listasRangoReporte.add(new ArrayList<RangoReporte>());
        }

        //años disponibles desde 2014 hasta año anterior al actual
        Calendar c = Calendar.getInstance();
        this.getGeneralMB().setAnioInicialRango(2014);
        this.getGeneralMB().setAnioFinalRango(c.get(Calendar.YEAR) - 1);


    }


    /**
     * Carga la configuracion de los campos asociados a los reportes estadisticos
     *
     * @author felipe.cadena
     */
    private void cargarConfigCampos(){

        List<RepConfigParametroReporte> lista = this.getConservacionService().
                consultaConfiguracionReportePorCat(ERepReporteCategoria.REPORTES_ESTADISTICOS.getCategoria());

        this.condicionesTotalesCampos = new ProcesaParametrosReporte(lista);

        this.condicionesCampos = this.condicionesTotalesCampos.getMapDefault();


    }

    /**
     * Actualiza la configuracion de los campos
     *
     * @author felipe.cadena
     */
    private void updateConfigCampos(Long idReporte){

        if(idReporte == null){
            this.condicionesCampos = this.condicionesTotalesCampos.getMapDefault();
        }else{
            this.condicionesCampos = this.condicionesTotalesCampos.getMapForTipoReporte(idReporte);
        }

    }

    /**
     * Método que realiza la inicialización de los menus en pantalla
     *
     * @author felipe.cadena
     */
    public void cargarTiposDeReporte() {

        if (this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()) {

            List<String> tipoReportesItemListTemp = new ArrayList<String>();

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                // Tipos de reporte
                if (repUsuarioRol.getRepReporte() != null) {

                    if (!tipoReportesItemListTemp.contains(repUsuarioRol.getRepReporte().getNombre())) {
                        this.tipoReportesItemList.add(new SelectItem(
                            repUsuarioRol.getRepReporte().getId(),
                            repUsuarioRol.getRepReporte().getNombre()));
                        tipoReportesItemListTemp.add(repUsuarioRol.getRepReporte().getNombre());
                    }
                }
            }

            Collections.sort(this.tipoReportesItemList, comparatorSI);
            //this.tipoReportePorCategoria.add(0, new SelectItem("", this.DEFAULT_COMBOS));
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar una territorial para la carga de
     * unidades operativas.
     *
     * @author felipe.cadena
     */
    public void cargarTerritoriales() {

        this.territorialesItemList.clear();
        List<String> territorialesItemListTemp = new ArrayList<String>();

        if (this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepTerritorialReportes() != null) {
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {
                        if (rtr.getTerritorial() != null) {
                            SelectItem territorial = new SelectItem(
                                rtr.getTerritorial().getCodigo(),
                                rtr.getTerritorial().getNombre());

                            if (!territorialesItemListTemp.contains(rtr.getTerritorial().getCodigo())) {
                                this.territorialesItemList.add(territorial);
                                territorialesItemListTemp.add(rtr.getTerritorial().getCodigo());
                            }

                        }
                    }
                }
            }
        }
        this.revisarPermisoGenerarReporte();

        this.cargarUOC();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar una territorial para la carga de
     * unidades operativas.
     *
     * @author felipe.cadena
     */
    public void cargarUOC() {
        if (this.territorialSeleccionadaCodigo != null) {
            this.territorialSeleccionada = this.buscarTerritorial(this.territorialSeleccionadaCodigo);
            this.unidadOperativaSeleccionadaCodigo = null;

            if (this.territorialSeleccionada != null) {
                this.revisarPermisoGenerarReporte();
                this.cargarListaUOC();
                this.cargarUOCSeleccionada();
                this.cargarDepartamentos();
                this.cargarRoles();
            }
        } else {
            this.rolesItemList = null;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la actualización de los roles al seleccionar una
     * territorial.
     *
     * @author felipe.cadena
     */
    public void cargarRoles() {

        this.rolesItemList = new ArrayList<SelectItem>();
        List<SelectItem> rolesItemListTemp = new ArrayList<SelectItem>();
        List<String> rolesListTemp = new ArrayList<String>();
        this.rolSeleccionado = null;

        if (this.territorialSeleccionadaCodigo != null) {

            Map<String, List<SelectItem>> rolesEstructuraOrganizacional = new HashMap<String, List<SelectItem>>();

            if (this.unidadOperativaSeleccionadaCodigo != null) {
                EstructuraOrganizacional uoc = this.buscarTerritorial(this.unidadOperativaSeleccionadaCodigo);

                if (uoc != null) {
                    rolesEstructuraOrganizacional = this
                        .cargarRolesEstructuraOrganizacional(
                            new EstructuraOrganizacional(
                                this.unidadOperativaSeleccionadaCodigo,
                                uoc.getNombre(), this.territorialSeleccionadaCodigo, null,
                                null), rolesEstructuraOrganizacional,
                            false);

                    if (rolesEstructuraOrganizacional != null
                        && rolesEstructuraOrganizacional.get(this.unidadOperativaSeleccionadaCodigo) != null
                        && !rolesEstructuraOrganizacional.get(this.unidadOperativaSeleccionadaCodigo).isEmpty()) {
                        rolesItemListTemp.addAll(rolesEstructuraOrganizacional
                            .get(this.unidadOperativaSeleccionadaCodigo));
                    }
                }
            } else {

                List<EstructuraOrganizacional> listaTerritorialYUOC = new ArrayList<EstructuraOrganizacional>();
                boolean esTerritorial = false;

                EstructuraOrganizacional territorial = this.buscarTerritorial(this.territorialSeleccionadaCodigo);
                if (territorial != null) {
                    listaTerritorialYUOC.add(territorial);
                }

                List<EstructuraOrganizacional> UOCs = this
                    .getGeneralesService()
                    .getCacheEstructuraOrganizacionalPorTipoYPadre(
                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                        this.territorialSeleccionadaCodigo);
                if (UOCs != null && !UOCs.isEmpty()) {
                    listaTerritorialYUOC.addAll(UOCs);
                }

                for (EstructuraOrganizacional eo : listaTerritorialYUOC) {
                    if (eo != null) {
                        if (eo.getCodigo().equals(this.territorialSeleccionadaCodigo)) {
                            esTerritorial = true;
                        } else {
                            esTerritorial = false;
                        }
                        rolesEstructuraOrganizacional = this
                            .cargarRolesEstructuraOrganizacional(
                                new EstructuraOrganizacional(
                                    eo.getCodigo(),
                                    eo.getNombre(), this.territorialSeleccionadaCodigo, null,
                                    null), rolesEstructuraOrganizacional,
                                esTerritorial);

                        if (rolesEstructuraOrganizacional != null
                            && rolesEstructuraOrganizacional.get(eo.getCodigo()) != null
                            && !rolesEstructuraOrganizacional.get(eo.getCodigo()).isEmpty()) {
                            for (SelectItem si : rolesEstructuraOrganizacional.get(eo.getCodigo())) {
                                if (!rolesListTemp.contains(si.getValue().toString())) {
                                    rolesItemListTemp.add(si);
                                    rolesListTemp.add(si.getValue().toString());
                                }
                            }
                        }
                    }
                }
            }
        }

        if (rolesItemListTemp != null) {
            Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem s1, SelectItem s2) {
                    return s1.getValue().toString().compareTo(s2.getValue().toString());
                }
            };
            Collections.sort(rolesItemListTemp, comparator);
            this.rolesItemList = rolesItemListTemp;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de roles de una estructura organizacional
     * seleccionada
     *
     * @param estructuraOrganizacional
     * @author felipe.cadena
     * @return
     */
    public Map<String, List<SelectItem>> cargarRolesEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacional,
        Map<String, List<SelectItem>> rolesEstructuraOrganizacional,
        boolean esTerritorial) {

        List<SelectItem> rolesTerritorial = new ArrayList<SelectItem>();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        String nombreEO;

        this.rolesItemList = new ArrayList<SelectItem>();
        List<String> roles = new ArrayList<String>();

        nombreEO = this.getGeneralesService().obtenerCodigoHomologadoNombreTerritorial(estructuraOrganizacional.getCodigo());
        if (nombreEO == null || estructuraOrganizacional.getCodigo().equals(nombreEO)) {
            nombreEO = estructuraOrganizacional.getNombre().replace(' ', '_');
        }

        if (estructuraOrganizacional != null && estructuraOrganizacional.getCodigo() != null) {
            if (!esTerritorial) {
                EstructuraOrganizacional territorial = buscarTerritorial(estructuraOrganizacional.getTipo());
                String nombreTerritorialCodHom = this.getGeneralesService().obtenerCodigoHomologadoNombreTerritorial(territorial.getCodigo());
                if (!estructuraOrganizacional.getTipo().equals(nombreTerritorialCodHom)) {
                    nombreTerritorialCodHom = territorial.getNombre().replace(' ', '_');
                    usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(nombreTerritorialCodHom, nombreEO);
                }
            } else {
                usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(nombreEO, null);
            }
        }

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para "
                + estructuraOrganizacional.getNombre());
            return null;
        }

        for (UsuarioDTO user : usuarios) {
            if (user.getRoles() != null) {
                for (String rol : user.getRoles()) {

                    if (!roles.contains(rol)) {
                        roles.add(rol);
                    }

                    if (roles != null) {
                        Collections.sort(roles);
                    }
                }
            }
        }

        if (roles != null) {
            for (String rol : roles) {
                rolesTerritorial.add(new SelectItem(rol, rol));
            }
            rolesEstructuraOrganizacional.put(estructuraOrganizacional.getCodigo(), rolesTerritorial);
        }
        return rolesEstructuraOrganizacional;
    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como
     * parámetro para su búsqueda
     *
     * @author felipe.cadena
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {
        if (codigoTerritorial != null) {
            EstructuraOrganizacional estructuraOrganizacional, answer = null;
            for (EstructuraOrganizacional eo : this.getGeneralesService()
                .buscarTodasTerritoriales()) {
                if (eo.getCodigo().equals(codigoTerritorial)) {
                    answer = eo;
                    break;
                }
            }
            if (answer != null) {
                estructuraOrganizacional = this
                    .getGeneralesService()
                    .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
                if (estructuraOrganizacional != null) {
                    return estructuraOrganizacional;
                } else {
                    return answer;
                }
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Mètodo que realiza el cargue de unidades operativas de catastro cuando se
     * ha seleccionado una territorial
     *
     * @author felipe.cadena
     */
    public void cargarListaUOC() {

        if (this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()) {

            this.unidadesOperativasCatastroItemList.clear();
            List<String> unidadesOperativasCatastroItemListTemp = new ArrayList<String>();

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepTerritorialReportes() != null) {
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        // UOC
                        if (rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)) {
                            if (rtr.getUOC() != null) {
                                SelectItem uoc = new SelectItem(rtr.getUOC().getCodigo(),
                                    rtr.getUOC().getNombre());

                                if (!unidadesOperativasCatastroItemListTemp.contains(rtr.getUOC().getCodigo())) {
                                    this.unidadesOperativasCatastroItemList.add(uoc);
                                    unidadesOperativasCatastroItemListTemp.add(rtr.getUOC().getCodigo());
                                }
                            } else {
                                this.unidadesOperativasCatastroObj = this.getGeneralesService()
                                    .getCacheEstructuraOrganizacionalPorTipoYPadre(EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                                        this.territorialSeleccionadaCodigo);

                                this.unidadesOperativasCatastroItemList.clear();

                                for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
                                    this.unidadesOperativasCatastroItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el set por default de la UOC del usuario.
     *
     * @author felipe.cadena
     */
    public void cargarUOCSeleccionada() {
        if (this.usuario.isUoc()) {
            this.unidadOperativaSeleccionada = null;

            if (this.unidadesOperativasCatastroObj != null) {

                this.unidadOperativaSeleccionadaCodigo = this.usuario.getCodigoUOC();

                for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
                    if (eo.getCodigo().equals(
                        this.unidadOperativaSeleccionadaCodigo)) {
                        this.unidadOperativaSeleccionada = eo;
                        break;
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de la lista de {@link Departamento} respecto
     * a la parametrización del usuario
     *
     * @author felipe.cadena
     */
    public void cargarDepartamentos() {

        this.departamentosItemList.clear();
        List<String> departamentosListTemp = new ArrayList<String>();

        this.cargarRoles();

        if (this.tipoReporteId != null
            && this.territorialSeleccionadaCodigo != null
            && this.parametrizacionUsuario != null
            && !this.parametrizacionUsuario.isEmpty()) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepReporte().getId().longValue() == this.tipoReporteId.longValue()
                    && repUsuarioRol.getRepTerritorialReportes() != null) {

                    // Se filtran los RepTerritorialReporte de la territorial seleccionada
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        if (rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)) {

                            // Si el RepTerritorialReporte tiene el campo de código departamento 
                            // vacío quiere decir que se tiene permisos para todos los departamentos
                            // de esa territorial
                            if (rtr.getDepartamento() == null) {

                                List<Departamento> departamentosList = this.getGeneralesService()
                                    .getCacheDepartamentosPorTerritorial(
                                        this.territorialSeleccionada.getCodigo());

                                if (departamentosList != null && !departamentosList.isEmpty()) {
                                    this.departamentosItemList.clear();
                                    for (Departamento departamento : departamentosList) {
                                        if (!departamentosListTemp.contains(departamento.getCodigo())) {
                                            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                                                departamento.getNombre()));
                                            departamentosListTemp.add(departamento.getCodigo());
                                        }
                                    }
                                }
                                break;
                            } else if (!departamentosListTemp.contains(rtr.getDepartamento().getCodigo())) {
                                this.departamentosItemList.add(new SelectItem(rtr.getDepartamento().getCodigo(),
                                    rtr.getDepartamento().getNombre()));
                                departamentosListTemp.add(rtr.getDepartamento().getCodigo());
                            }
                        }
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar el tipo de reporte a buscar, éste
     * principalmente recarga la lista de los menus que dependen del tipo de
     * reporte
     *
     * @author felipe.cadena
     */
    public void cambioTipoReporte() {


        this.updateConfigCampos(this.tipoReporteId);

        if (this.tipoReporteId != null
            && !this.parametrizacionUsuario.isEmpty()) {
            for (RepUsuarioRolReporte urr : this.parametrizacionUsuario) {
                if (urr.getRepReporte() != null
                    && urr.getRepReporte().getId() != null
                    && urr.getRepReporte().getId().longValue() == this.tipoReporteId.longValue()) {
                    this.repReporteSeleccionado = urr.getRepReporte();
                    break;
                }
            }

            this.deshabilitarCamposConsultaBool = false;
            // Verificar la si se deben deshabilitar campos de consulta según tipo de reporte
            for (ERepReporteConsultaEspecialParametrizacion rpce : ERepReporteConsultaEspecialParametrizacion.values()) {
                if (rpce.getId().longValue() == this.tipoReporteId.longValue()) {
                    this.deshabilitarCamposConsultaBool = true;
                    break;
                }
            }

            if(this.obtenerNombreReporte(this.tipoReporteId).equals("Estadísticas por manzana o sector")){
                this.deshabilitarPostManzana = true;

            }else{
                this.deshabilitarPostManzana = false;
            }
        }

        // Inicialización de menús que dependen del tipo de informe
        this.territorialesItemList.clear();
        this.unidadesOperativasCatastroItemList.clear();
        this.departamentosItemList.clear();
        this.municipiosItemList.clear();

        this.departamentoSeleccionadoCodigo = null;
        this.fechaCorteSeleccionada = null;
        this.anioSeleccionado = null;

        this.cargarTerritoriales();
        this.revisarPermisoGenerarReporte();
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la actualización de los municipios al seleccionar un
     * departamento.
     *
     * @author felipe.cadena
     */
    public void cargarMunicipios() {

        this.municipiosItemList.clear();
        this.municipioSeleccionadoCodigo = null;


        if (this.departamentoSeleccionadoCodigo != null && !this.departamentoSeleccionadoCodigo.isEmpty() ) {
            this.numeroPredialPartesInicial.setDepartamentoCodigo(this.departamentoSeleccionadoCodigo.substring(0,2));
            this.numeroPredialPartesFinal.setDepartamentoCodigo(this.departamentoSeleccionadoCodigo.substring(0,2));


            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepReporte().getId().longValue() == this.tipoReporteId.longValue()
                    && repUsuarioRol.getRepTerritorialReportes() != null) {

                    // Se filtran los RepTerritorialReporte de la territorial seleccionada
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        if (rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)
                            && rtr.getDepartamento().getCodigo().equals(this.departamentoSeleccionadoCodigo)) {

                            // Si el RepTerritorialReporte tiene el campo de código municipio 
                            // vacío quiere decir que se tiene permisos para todos los municipios
                            // del departamento
                            if (rtr.getMunicipio() == null) {

                                List<Municipio> municipiosList = this.getGeneralesService()
                                    .getCacheMunicipiosByDepartamento(this.departamentoSeleccionadoCodigo);

                                if (municipiosList != null && !municipiosList.isEmpty()) {
                                    this.municipiosItemList.clear();
                                    for (Municipio municipio : municipiosList) {
                                        this.municipiosItemList.add(new SelectItem(municipio
                                            .getCodigo(), municipio.getNombre()));
                                    }
                                }
                                break;
                            } else {
                                SelectItem mun = new SelectItem(
                                        rtr.getMunicipio().getCodigo(),
                                        rtr.getMunicipio().getNombre());
                                if(!UtilidadesWeb.containsSelectItemByCode(municipiosItemList, mun)){
                                    this.municipiosItemList.add(mun);
                                }
                            }
                        }
                    }
                }
            }
        } else{
            this.numeroPredialPartesInicial.setDepartamentoCodigo(null);
            this.numeroPredialPartesFinal.setDepartamentoCodigo(null);
            this.onSelectMunicipio();
        }
        this.revisarPermisoGenerarReporte();
    }


    /**
     * Acutaliza datos al seleccionar municipios
     *
     * @author felipe.cadena
     */
    public void onSelectMunicipio(){

        if(this.municipioSeleccionadoCodigo.equals("null")){
            this.municipioSeleccionadoCodigo = null;
        }

        if (this.municipioSeleccionadoCodigo != null) {
            this.numeroPredialPartesInicial.setMunicipioCodigo(this.municipioSeleccionadoCodigo.substring(2,5));
            this.numeroPredialPartesFinal.setMunicipioCodigo(this.municipioSeleccionadoCodigo.substring(2,5));
            this.tipoAgrupacion = ESiNo.SI.getCodigo();

        }else{
            this.numeroPredialPartesInicial.setMunicipioCodigo(null);
            this.numeroPredialPartesFinal.setMunicipioCodigo(null);
        }
    }


    // ----------------------------------------------------- //
    /**
     * Método encargado de cargar los funcionarios en una lista de de SelecItem
     * asociados a la territorial del usuario en sesion.
     */
    public void cargarFuncionarios() {

        this.funcionariosItemList.clear();
        if (this.rolSeleccionado != null && !this.rolSeleccionado.isEmpty()) {
            List<UsuarioDTO> funcionarios;
            String codigoUOSeleccionada;

            if (this.territorialSeleccionada != null) {
                if (this.unidadOperativaSeleccionada != null) {
                    codigoUOSeleccionada = this.unidadOperativaSeleccionada
                        .getNombre();
                    codigoUOSeleccionada = codigoUOSeleccionada.toUpperCase()
                        .replace("UOC ", "UOC_");
                } else {
                    codigoUOSeleccionada = this.territorialSeleccionada.getNombre()
                        .toUpperCase();
                }

                codigoUOSeleccionada = Utilidades.delTildes(codigoUOSeleccionada);

            } else {
                codigoUOSeleccionada = this.usuario
                    .getDescripcionEstructuraOrganizacional();
            }

            funcionarios = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(codigoUOSeleccionada,
                    ERol.valueOf(this.rolSeleccionado));

            if (funcionarios != null && !funcionarios.isEmpty()) {
                for (UsuarioDTO funcionario : funcionarios) {
                    this.funcionariosItemList.add(new SelectItem(funcionario
                        .getLogin(), funcionario.getNombreCompleto()));
                }
            }

            Collections.sort(this.funcionariosItemList,
                new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem sItem1, SelectItem sItem2) {
                    String sItem1Label = sItem1.getLabel();
                    String sItem2Label = sItem2.getLabel();
                    return (sItem1Label.compareToIgnoreCase(sItem2Label));
                }
            });
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue del menú de subprocesos de Conservación.
     *
     * @author felipe.cadena
     */
    public void cargarSubprocesosConservacion() {

        this.subprocesosItemList = new ArrayList<SelectItem>();

        for (EConservacionSubproceso cs : EConservacionSubproceso.values()) {
            this.subprocesosItemList.add(new SelectItem(cs, cs.getNombre()));
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método encargado de cargar las actividades en process de un subproceso
     * seleccionado
     *
     * @author felipe.cadena
     */
    public void cargarActividades() {
        this.actividadesItemList = new ArrayList<SelectItem>();
        if (this.subprocesoSeleccionado != null) {
            for (EConservacionSubprocesoActividad csa : EConservacionSubprocesoActividad.values()) {
                if (csa.getSubproceso().equals(this.subprocesoSeleccionado)) {
                    SelectItem actividad = new SelectItem(csa, csa.getNombreActividad());
                    if (!this.actividadesItemList.contains(actividad)) {
                        this.actividadesItemList.add(actividad);
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de las posibles clasificaciones de un
     * trámite.
     *
     * @author felipe.cadena
     */
    public void cargarClasificacion() {

        this.clasificacionesItemList = new ArrayList<SelectItem>();

        for (ETramiteClasificacion tc : ETramiteClasificacion.values()) {
            this.clasificacionesItemList.add(new SelectItem(tc.name(), tc.name()));
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza la búsqueda de los reportes de radicación generados
     * con base en los parámetros seleccionados.
     *
     * @author felipe.cadena
     */
    public void cargarParametrosConsulta() {

        this.datosConsultaPredio = new FiltroGenerarReportes();
        this.datosConsultaPredio.setCodigoTipoReporte(this.tipoReporteId.toString());
        this.datosConsultaPredio.setCodigoTipoInforme(Long.valueOf(this.tipoReporteId.toString()));
        this.datosConsultaPredio.setFormato(this.formatoSeleccionado);
        this.datosConsultaPredio.setTerritorialId(this.territorialSeleccionadaCodigo);
        //this.datosConsultaPredio.set(this.unidadOperativaSeleccionadaCodigo);
        this.datosConsultaPredio.setDepartamentoId(this.departamentoSeleccionadoCodigo);
        this.datosConsultaPredio.setMunicipioId(this.municipioSeleccionadoCodigo);
        this.datosConsultaPredio.setTipoDeFechaCorte(this.fechaCorteSeleccionada);
        this.datosConsultaPredio.setAnoReporte(this.anioSeleccionado);

        if(this.nacional){
            this.datosConsultaPredio.setNacional(ESiNo.SI.getCodigo());
        }else {
            this.datosConsultaPredio.setNacional(ESiNo.NO.getCodigo());
        }

        if(this.incluirUocs){
            this.datosConsultaPredio.setIncluirUocs(ESiNo.SI.getCodigo());
        }else {
            this.datosConsultaPredio.setIncluirUocs(ESiNo.NO.getCodigo());
        }

        if(this.tipoAgrupacion !=null
                && !this.tipoAgrupacion.trim().isEmpty()){
            this.datosConsultaPredio.setConsolidado(this.tipoAgrupacion);
        }



        if (this.codigoReporteCod != null
            && !this.codigoReporteCod.isEmpty()
            && this.anioReporteCod != null
            && !this.codigoReporteCod.isEmpty()) {
            this.codigoReporte = "REP-EST-" + this.codigoReporteCod + "-" + this.anioReporteCod;
            this.datosConsultaReportesRadicacion.setCodigoReporte(this.codigoReporte);
        }
        if (this.actividadSeleccionada != null) {
            this.datosConsultaReportesRadicacion.setActividad(this.actividadSeleccionada.getNombreActividad());
        }
        if (this.subprocesoSeleccionado != null) {
            this.datosConsultaReportesRadicacion.setSubproceso(this.subprocesoSeleccionado.getNombre());
        }


        // completa numeros prediales para la consulta
        this.datosConsultaPredio.setNumeroPredialPartesInicial(this.numeroPredialPartesInicial);
        this.datosConsultaPredio.setNumeroPredialPartesFinal(this.numeroPredialPartesFinal);
        this.datosConsultaPredio.setNumeroPredial(UtilidadesWeb.completarNumeroPredialConDigito(this.numeroPredialPartesInicial, 0));
        this.datosConsultaPredio.setNumeroPredialFinal(UtilidadesWeb.completarNumeroPredialConDigito(this.numeroPredialPartesFinal, 9));

    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza la búsqueda de los reportes estadisticos generados con
     * base en los parámetros seleccionados.
     *
     * @author felipe.cadena
     */
    public void buscarReporte() {
        this.habilitarGenerarReporteBool = true;

        if(this.codigoReporteCod != null && !this.codigoReporteCod.isEmpty()){
            this.buscarReportePorCodigo();
            return;
        }


        if(this.municipioSeleccionadoCodigo != null && ("null".equals(this.municipioSeleccionadoCodigo)
                || this.municipioSeleccionadoCodigo.isEmpty())
                ){
            this.municipioSeleccionadoCodigo = null;
        }

        if(this.tipoReporteId == null || this.tipoReporteId.equals(0L)){
            this.addMensajeError(ECodigoErrorVista.REPORTES_EST_008.getMensajeUsuario());
            return;
        }

        try {
            // Validación de fechas
            if (this.fechaInicio != null && this.fechaFin != null && this.fechaFin.before(this.fechaInicio)) {
                this.addMensajeError("La fecha de inicio es mayor a la fecha fin, por favor revise.");
                return;
            }

            this.cargarParametrosConsulta();

            /*this.reportesRadicacionGenerados = this.getConservacionService()
                .buscarReportesRadicacionGenerados(this.datosConsultaReportesRadicacion);*/

            this.reportesRadicacionGenerados = this.getConservacionService()
                    .buscarReportePorFiltrosBasicos(
                            this.datosConsultaPredio,
                            this.departamentoSeleccionadoCodigo,
                            this.municipioSeleccionadoCodigo,
                            this.territorialSeleccionadaCodigo, this.unidadOperativaSeleccionadaCodigo);
            if (this.reportesRadicacionGenerados == null
                || this.reportesRadicacionGenerados.isEmpty()) {
                this.addMensajeWarn("No se encontraron reportes que cumplan con estos filtros de búsqueda.");
            }else{
                this.reportesRadicacionGenerados = this.determinarAsociados(this.reportesRadicacionGenerados);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en la búsqueda de los reportes generados.");
        }
    }


    /**
     * Método que realiza la búsqueda de los reportes estadisticos generados con
     * base en los parámetros seleccionados.
     *
     * @author felipe.cadena
     */
    public void buscarReportePorCodigo() {

        this.habilitarGenerarReporteBool = true;

        try {

            if(this.anioReporteCod == null || this.anioReporteCod.isEmpty()){
                this.codigoReporte = "REP-EST-" + this.codigoReporteCod + "-%";
            }else{
                this.codigoReporte = "REP-EST-" + this.codigoReporteCod + "-" + this.anioReporteCod;
            }

            this.reportesRadicacionGenerados = this.getConservacionService()
                    .buscarReportesPorCodigo(this.codigoReporte);
            if (this.reportesRadicacionGenerados == null
                    || this.reportesRadicacionGenerados.isEmpty()) {
                this.addMensajeWarn("No se encontraron reportes que cumplan con estos filtros de búsqueda.");
            }else{
                this.reportesRadicacionGenerados = this.determinarAsociados(this.reportesRadicacionGenerados);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en la búsqueda de los reportes generados.");
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza el llamado al job para generar el reporte de
     * radicación con base en los parámetros seleccionados.
     *
     * @ref ConsultaReportesPredialesMB.generarReporte - Leidy Gonzalez
     * @author felipe.cadena
     */
    public void generarReporte() {

        try {

            if(this.condicionesCampos.get("MUNICIPIO")[1]){
                if (this.municipioSeleccionadoCodigo == null || this.municipioSeleccionadoCodigo.equals("null")
                        || this.municipioSeleccionadoCodigo.isEmpty()) {
                    this.addMensajeError("Municipio: Error de validación: se necesita un valor.");
                    return;
                }
            }

            if (this.reportesRadicacionGenerados == null || this.reportesRadicacionGenerados.isEmpty()) {
                this.reportesRadicacionGenerados = new ArrayList<RepReporteEjecucion>();
            }

            if(!this.validarNumerosPrediales() || !this.validarPermisoNacional()){
                return;
            }

            this.generarCodigoReporte();
            this.repReporteEjecucion = this.actualizarReporteEjecucion();
            this.repReporteEjecucionDetalle = this.actualizarReporteEjecucionDetalle(this.repReporteEjecucion);

            // Generación del reporte
            if (this.repReporteEjecucion != null) {

                this.getConservacionService().generarReporteEstadistico(this.repReporteEjecucion);

                //this.consultarReportesBDRunnableJob.run();
                this.reportesRadicacionGenerados.add(this.repReporteEjecucion);

                this.addMensajeInfo("Se inició la generación del reporte, su código es: " + this.codigoReporte);
            }

            this.actualizarTabla();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en la generación del reporte.");
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza el set de los valores seleccionados en los filtros,
     * para generar el reporte asociado
     *
     * @ref ConsultaReportesPredialesMB.actualizarReporteEjecucion - Leidy
     * Gonzalez
     * @author felipe.cadena
     */
    public RepReporteEjecucion actualizarReporteEjecucion() {

        this.repReporteEjecucion = new RepReporteEjecucion();

        if (this.datosConsultaPredio != null) {

            if (this.datosConsultaPredio.getDepartamentoId() != null
                && !this.datosConsultaPredio.getDepartamentoId().isEmpty()) {
                this.repReporteEjecucion.setDepartamentoCodigo(this.datosConsultaPredio.getDepartamentoId());
            }

            if (this.datosConsultaPredio.getMunicipioId() != null
                && !this.datosConsultaPredio.getMunicipioId().isEmpty()) {
                this.repReporteEjecucion.setMunicipioCodigo(this.datosConsultaPredio.getMunicipioId() );
            }

            this.repReporteEjecucion.setFechaInicio(new Date(System.currentTimeMillis()));

            if (this.repReporteSeleccionado != null) {
                this.repReporteEjecucion
                    .setRepReporte(this.repReporteSeleccionado);
            }

            if (this.territorialSeleccionada != null) {
                this.repReporteEjecucion.setTerritorial(this.territorialSeleccionada);

            }
            if (this.unidadOperativaSeleccionadaCodigo != null) {
                this.unidadOperativaSeleccionada = this.buscarTerritorial(this.unidadOperativaSeleccionadaCodigo);
                this.repReporteEjecucion.setUOC(this.unidadOperativaSeleccionada);
            }

            if (this.codigoReporte != null && !this.codigoReporte.isEmpty()) {
                this.repReporteEjecucion.setCodigoReporte(this.codigoReporte);
            }

            if (this.datosConsultaPredio.getFormato() != null) {
                this.repReporteEjecucion.setFormato(this.datosConsultaPredio.getFormato());
            }

            if(this.fechaCorteSeleccionada!=null &&
                    !this.fechaCorteSeleccionada.trim().isEmpty()){
                this.repReporteEjecucion.setFechaCorte(this.fechaCorteSeleccionada);
            }

            if(this.anioSeleccionado!=null && !this.anioSeleccionado.isEmpty()){
                this.repReporteEjecucion.setVigencia(this.anioSeleccionado);
            }


            this.repReporteEjecucion.setEstado(ERepReporteEjecucionEstado.INICIADO.getCodigo());
            this.repReporteEjecucion.setUsuario(this.usuario.getLogin());
            this.repReporteEjecucion.setUsuarioLog(this.usuario.getLogin());
            this.repReporteEjecucion.setFechaLog(new Date());
            //TODO: Definir valores para Reporte vacio
            this.repReporteEjecucion.setReporteVacio("I");
            
            this.repReporteEjecucion = this.getConservacionService().actualizarRepReporteEjecucion(
                this.repReporteEjecucion);

            if(this.listasRangoReporte!=null && !this.listasRangoReporte.isEmpty()){
                this.guardarRangos(this.listasRangoReporte, this.repReporteEjecucion);
            }
        }
        return this.repReporteEjecucion;
    }

    /**
     * Guarda los rangos personalizados asociados al reporte
     *
     * @author felipe.cadena
     * @param rangos
     * @param reporte
     */
    public void guardarRangos(List<List<RangoReporte>> rangos, RepReporteEjecucion reporte){

        List<RangoReporte> rangosAcumulados = new ArrayList<RangoReporte>();
        for (List<RangoReporte> rrList:rangos) {
            for (RangoReporte rr : rrList) {
                rr.setReporteEjecucionId(reporte.getId());
            }
            rangosAcumulados.addAll(rrList);
        }

        if(!rangosAcumulados.isEmpty()) {
            this.getConservacionService().guardarRangoReporteMultiple(rangosAcumulados);
        }

    }

    /**
     * Metodo para actualizar los datos del reporte con los detalles ingresados
     * por el usuario
     *
     * @ref ConsultaReportesPredialesMB.actualizarReporteEjecucionDetalle -
     * Leidy Gonzalez
     * @author felipe.cadena
     */
    public RepReporteEjecucionDetalle actualizarReporteEjecucionDetalle(
        RepReporteEjecucion repReporteEjecucion) {

        if (repReporteEjecucion != null) {

            this.repReporteEjecucionDetalle = new RepReporteEjecucionDetalle();

            this.repReporteEjecucionDetalle
                .setRepReporteEjecucion(repReporteEjecucion);

            if (repReporteEjecucion.getMunicipioCodigo() != null
                && !repReporteEjecucion.getMunicipioCodigo().isEmpty()) {
                this.repReporteEjecucionDetalle
                    .setMunicipioCodigo(repReporteEjecucion
                        .getMunicipioCodigo());
            }

            if (repReporteEjecucion.getDepartamentoCodigo() != null
                && !repReporteEjecucion.getDepartamentoCodigo().isEmpty()) {

                this.repReporteEjecucionDetalle
                    .setDepartamentoCodigo(repReporteEjecucion
                        .getDepartamentoCodigo());
            }

            if (repReporteEjecucion.getTerritorial() != null) {

                this.repReporteEjecucionDetalle
                    .setTerritorialCodigo(repReporteEjecucion
                        .getTerritorial().getCodigo());
            }


            if(this.fechaCorteSeleccionada !=null
                    && !this.fechaCorteSeleccionada.trim().isEmpty()){
                this.repReporteEjecucionDetalle.setFechaCorte(this.fechaCorteSeleccionada);
            }

            if(this.anioSeleccionado !=null
                    && !this.anioSeleccionado.trim().isEmpty()){
                this.repReporteEjecucionDetalle.setVigencia(this.anioSeleccionado);
            }

            if(this.rangoSeleccionado !=null
                    && !this.rangoSeleccionado.trim().isEmpty()){
                this.repReporteEjecucionDetalle.setTipoRango(Integer.valueOf(this.rangoSeleccionado));
            }

            if(this.nacional){
                this.repReporteEjecucionDetalle.setNacional(ESiNo.SI.getCodigo());
            }else {
                this.repReporteEjecucionDetalle.setNacional(ESiNo.NO.getCodigo());
            }

            if(this.incluirUocs){
                this.repReporteEjecucionDetalle.setIncluirUocs(ESiNo.SI.getCodigo());
            }else {
                this.repReporteEjecucionDetalle.setIncluirUocs(ESiNo.NO.getCodigo());
            }

            if(this.tipoAgrupacion !=null
                    && !this.tipoAgrupacion.trim().isEmpty()){
                this.repReporteEjecucionDetalle.setConsolidado(this.tipoAgrupacion);
            }


            if(this.datosConsultaPredio.getNumeroPredial() != null ||
                    !this.datosConsultaPredio.getNumeroPredial().isEmpty()){
                this.repReporteEjecucionDetalle.setNumeroPredialDesde(this.datosConsultaPredio.getNumeroPredial());
            }

            if(this.datosConsultaPredio.getNumeroPredialFinal() != null ||
                    !this.datosConsultaPredio.getNumeroPredialFinal().isEmpty()){
                this.repReporteEjecucionDetalle.setNumeroPredialHasta(this.datosConsultaPredio.getNumeroPredialFinal());
            }

            this.repReporteEjecucionDetalle.setUsuarioLog(this.usuario.getLogin());
            this.repReporteEjecucionDetalle.setFechaLog(new Date());


            this.repReporteEjecucionDetalle = this.getConservacionService()
                .actualizarRepReporteEjecucionDetalle(
                    this.repReporteEjecucionDetalle);
        }
        return this.repReporteEjecucionDetalle;
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que empaqueta los reportes seleccionados para que puedan ser
     * descargados
     *
     * @author felipe.cadena
     */
    public void exportarReportes() {

        List<RepReporteEjecucion> reportesSeleccionados = new ArrayList<RepReporteEjecucion>();
        List<String> rutaReportesADescargar = new ArrayList<String>();

        // Cargar reportes seleccionados
        if (this.reportesRadicacionGenerados != null) {
            for (RepReporteEjecucion rre : this.reportesRadicacionGenerados) {
                if (rre.isSelected()) {
                    reportesSeleccionados.add(rre);
                }
            }
        }

        if (reportesSeleccionados.isEmpty()) {
            this.addMensajeWarn("Por favor seleccione al menos un reporte a exportar.");
            this.addErrorCallback();
            return;
        }

        if (this.validarExportarReportes(reportesSeleccionados)) {

            for (RepReporteEjecucion rre : reportesSeleccionados) {

                if (rre.getRutaReporteGenerado() != null
                    && !rre.getRutaReporteGenerado().trim().isEmpty()) {

                    ReporteDTO documentoDTO = this.reportsService.consultarReporteDeGestorDocumental(rre.getRutaReporteGenerado());

                    if (documentoDTO != null && documentoDTO.getRutaCargarReporteAAlfresco() != null) {
                        rutaReportesADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());
                    }
                }
            }

            try {
                if (!rutaReportesADescargar.isEmpty()) {

                    String rutaZipDocumentos = this.getGeneralesService().crearZipAPartirDeRutaDeDocumentos(rutaReportesADescargar);

                    if (rutaZipDocumentos != null) {
                        InputStream stream;
                        File archivoReportesDescarga = new File(rutaZipDocumentos);
                        if (!archivoReportesDescarga.exists()) {
                            LOGGER.error("Error el archivo de origen no existe");
                            this.addMensajeError("Ocurrió un error al descargar los archivos.");
                            return;
                        }

                        try {
                            stream = new FileInputStream(archivoReportesDescarga);
                            this.reportesDescarga = new DefaultStreamedContent(stream,
                                Constantes.TIPO_MIME_ZIP, generarNombreZip() + ".zip");
                        } catch (FileNotFoundException e) {
                            LOGGER.error("Archivo no encontrado, no se pudieron descargar los reportes seleccionados. "+e.getMessage());
                        }

                    }
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en la búsqueda de los reportes generados.");
            }
        } else {
            this.addErrorCallback();
            return;
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que valida los tamaños de archivos a exportar previo a generar un
     * .zip para estos.
     *
     * @author felipe.cadena
     */
    public String generarNombreZip() {
        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
        return "Reportes_tramite".concat(fechaHora.format(new Date())).trim();
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que valida los tamaños de archivos a exportar previo a generar un
     * .zip para estos.
     *
     * @author felipe.cadena
     */
    /*
	 * @modified by leidy.gonzalez:: #21465
     */
    public boolean validarExportarReportes(List<RepReporteEjecucion> reportes) {

        BigDecimal sumaTamanioReportes = new BigDecimal(0);
        int resultado;

        for (RepReporteEjecucion rre : reportes) {
            // Estado de los reportes
            if (!ERepReporteEjecucionEstado.FINALIZADO.getCodigo().equals(rre.getEstado())) {
                this.addMensajeError("Uno de los reportes seleccionados, no se encuentra finalizado y no puede ser exportado");
                return false;
            }

            if (rre.getValorTamano() != null) {

                sumaTamanioReportes = sumaTamanioReportes.add(rre.getValorTamano());
            }

        }

        resultado = TAMANO_MAX_DESCARGAS.compareTo(sumaTamanioReportes);

        // Tamaño del archivo
        if (resultado == -1) {
            this.addMensajeError("La suma del tamaño de los archivos supera el tamaño permitido para un .zip, por favor verifique");
            return false;
        }
        return true;
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que actualiza la consulta de los reportes generados.
     *
     * @author felipe.cadena
     */
    public void actualizarTabla() {
        this.buscarReporte();
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que genera el código del reporte con base en el tipo de reporte,
     * un consecutivo y el año actual.
     *
     * @author felipe.cadena
     */
    public void generarCodigoReporte() {

        Object[] numeracionReportePredial = this.getGeneralesService()
            .generarNumeracion(ENumeraciones.NUMERACION_ANIO_CONS_REP_EST,
                this.usuario.getCodigoEstructuraOrganizacional(), "", "", 0);

        this.codigoReporte = (String) numeracionReportePredial[1];

        String codStart = this.codigoReporte.substring(0,12);

        String codeEnd = this.codigoReporte.substring(13);

        this.codigoReporte = codStart+codeEnd;

    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de todos los reportes que se encuentran en
     * la tabla de resultados de búsqueda
     *
     * @author felipe.cadena
     */
    public void seleccionarTodosReportes() {
        if (this.reportesRadicacionGenerados != null) {
            for (RepReporteEjecucion rre : this.reportesRadicacionGenerados) {
                if(rre.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO.getCodigo())){
                    rre.setSelected(this.seleccionarTodosBool);
                }
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que construye la previsualizacion del documento del reporte
     * {@link RepReporteEjecucion}
     *
     * @author felipe.cadena
     */
    public void cargarPrevisualizacionReporte() {

        this.rutaPrevisualizacionReporte = new String();

        if (this.reporteVisualizacionSeleccionado != null
                && !this.reporteVisualizacionSeleccionado.getRutaReporteGenerado().isEmpty()) {

            // Consultar archivo de previsualización
            try {

                // Loading an existing PDF document
                String ruta = this.getConservacionService()
                        .descargarOficioDeGestorDocumentalATempLocalMaquina(
                                this.reporteVisualizacionSeleccionado
                                        .getRutaReporteGenerado());

                File file = new File(ruta);

                PDDocument documento = PDDocument.load(file);

                // Instantiating Splitter class
                Splitter splitter = new Splitter();

                // splitting the pages of a PDF document
                List<PDDocument> Pages = splitter.split(documento);

                // Creating an iterator
                Iterator<PDDocument> iterator = Pages.listIterator();

                // Saving each page as an individual document
                int i = 0;
                while (iterator.hasNext()) {
                    if (i == 0) {

                        PDDocument pd = iterator.next();

                        pd.save(ruta);

                        File filePrimeraPagina = new File(ruta);

                        this.rutaPrevisualizacionReporte = this.getGeneralesService().
                                publicarArchivoEnWeb(filePrimeraPagina.getPath());

                        break;

                    } else {

                        break;
                    }

                }
                documento.close();

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en la carga de la previsualización.");
            }
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza la revisión de los permisos de generación del tipo de
     * reporte seleccionado para habilitar el botón de generar reporte.
     *
     * @author felipe.cadena
     *
     * @return
     */
    public void revisarPermisoGenerarReporte() {
        this.permisoGenerarReporteBool = false;
        int sinPermisoGenerar = 0;

        if (this.tipoReporteId != null
            && this.territorialSeleccionadaCodigo != null
            && !this.territorialSeleccionadaCodigo.isEmpty()) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepReporte().getId().longValue() == this.tipoReporteId.longValue()
                    && repUsuarioRol.getRepTerritorialReportes() != null) {

                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        if (rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)) {

                            if (rtr.getDepartamento() != null) {
                                if (this.departamentoSeleccionadoCodigo != null
                                    && rtr.getDepartamento().getCodigo().equals(this.departamentoSeleccionadoCodigo)
                                    && !repUsuarioRol.isPermisoGenerar()) {
                                    sinPermisoGenerar++;
                                    break;
                                } else {
                                    this.permisoGenerarReporteBool = true;
                                    return;
                                }
                            } else if (!repUsuarioRol.isPermisoGenerar()) {
                                sinPermisoGenerar++;
                                break;
                            } else {
                                this.permisoGenerarReporteBool = true;
                                return;
                            }
                        }
                    }
                }
            }

            if (sinPermisoGenerar > 0) {
                this.permisoGenerarReporteBool = true;
            }
        }
    }

    /**
     * Metodo para la personalizacion de rangos desde la pantalla de parametros
     *
     *
     * @author felipe.cadena
     */
    public void personalizarRangos() {
        for(List<RangoReporte> categoria : this.listasRangoReporte){
            if(!categoria.isEmpty() && categoria.size()<2){
                this.addMensajeError(ECodigoErrorVista.REPORTES_EST_005.getMensajeUsuario());
                this.addErrorCallback();
                return;
            }
        }
    }

    /**
     * Valida el nuevo rango ingresado
     *
     * @author felipe.cadena
     */
    public boolean validarRango(RangoReporte rango){

        List<RangoReporte> listaRangos = this.listasRangoReporte.get(this.rangoCategoriaSeleccionado);
        double coberturaMin = 0;
        double coberturaMax = 0;


        //validar maximo de rangos
        if(listaRangos.size()>=30){
            this.addMensajeError(ECodigoErrorVista.REPORTES_EST_007.getMensajeUsuario());
            this.addErrorCallback();
            return false;
        }

        if(rango.getValorFinal()<=rango.getValorInicial()){
            this.addMensajeError(ECodigoErrorVista.REPORTES_EST_003.getMensajeUsuario());
            this.addErrorCallback();
            return false;
        }

        //No se realizan mas validaciones para el rango inicial
        if(listaRangos.isEmpty()){
            return true;
        }

        int k = 0;
        for (RangoReporte rp : listaRangos) {
            if(k==0){
                coberturaMin = rp.getValorInicial();
                coberturaMax = rp.getValorFinal();
            }else{
                if(rp.getValorInicial()<coberturaMin){
                    coberturaMin = rp.getValorInicial();
                }
                if(rp.getValorFinal()>coberturaMax){
                    coberturaMax = rp.getValorFinal();
                }
            }
            k++;
        }

        //validar rangos superpuestos se considera el limite inferior de los intervalos cerrados y el superior abierto
        if((rango.getValorInicial() >= coberturaMin && rango.getValorInicial() < coberturaMax) ||
            (rango.getValorFinal() > coberturaMin && rango.getValorFinal() <= coberturaMax)){
            this.addMensajeError(ECodigoErrorVista.REPORTES_EST_004.getMensajeUsuario());
            this.addErrorCallback();
            return false;
        }

        //validar rangos continuos
        if((rango.getValorInicial() - coberturaMax != 0) && (coberturaMin - rango.getValorFinal() != 0)){
            this.addMensajeError(ECodigoErrorVista.REPORTES_EST_006.getMensajeUsuario());
            this.addErrorCallback();
            return false;
        }

        return true;

    }

    /**
     * Metodo para agregar un nuevo rango personalizado
     *
     *
     * @author felipe.cadena        
     */
    public void guardarNuevoRango() {

        if(this.banderaEdicionRango){
            return;
        }

        if(!validarRango(this.rangoReporteSeleccionado)){
            return;
        }

        switch (this.rangoCategoriaSeleccionado) {
            case GestionReportesEstadisticosMB.DIS_R_SUPERFICIE:
                this.rangoReporteSeleccionado.setTipo(ETipoRangoReporte.SUPERFICIE.getCodigo());
                this.rangoReporteSeleccionado.setZona(EDistribucionRangoReporte.RURAL.getCodigo());
                break;
            case GestionReportesEstadisticosMB.DIS_U_SUPERFICIE:
                this.rangoReporteSeleccionado.setTipo(ETipoRangoReporte.SUPERFICIE.getCodigo());
                this.rangoReporteSeleccionado.setZona(EDistribucionRangoReporte.URBANO.getCodigo());
                break;
            case GestionReportesEstadisticosMB.DIS_C_SUPERFICIE:
                this.rangoReporteSeleccionado.setTipo(ETipoRangoReporte.SUPERFICIE.getCodigo());
                this.rangoReporteSeleccionado.setZona(EDistribucionRangoReporte.CORREGIMIENTO.getCodigo());
                break;
            case GestionReportesEstadisticosMB.DIS_R_AVALUO:
                this.rangoReporteSeleccionado.setTipo(ETipoRangoReporte.AVALUO.getCodigo());
                this.rangoReporteSeleccionado.setZona(EDistribucionRangoReporte.RURAL.getCodigo());
                break;
            case GestionReportesEstadisticosMB.DIS_U_AVALUO:
                this.rangoReporteSeleccionado.setTipo(ETipoRangoReporte.AVALUO.getCodigo());
                this.rangoReporteSeleccionado.setZona(EDistribucionRangoReporte.URBANO.getCodigo());
                break;
            default:
                this.rangoReporteSeleccionado.setTipo(ETipoRangoReporte.AVALUO.getCodigo());
                this.rangoReporteSeleccionado.setZona(EDistribucionRangoReporte.CORREGIMIENTO.getCodigo());
                break;
        }
        
        this.rangoReporteSeleccionado.setFechaLog(new Date());
        this.rangoReporteSeleccionado.setUsuarioLog(this.usuario.getLogin());
        this.listasRangoReporte.get(this.rangoCategoriaSeleccionado).add(this.rangoReporteSeleccionado);

    }
    
    /**
     * Inicializa los datos necesarios para agregar un nuevo rango personalizado
     * 
     * @author felipe.cadena
     */
    public void inicializarNuevoRango(){
        this.rangoReporteSeleccionado = new RangoReporte();
        this.banderaEdicionRango = false;
    }
    
     /**
     * Metodo para eliminar rangos personalizados existentes
     *
     *
     * @author felipe.cadena
     */
    public void eliminarRangoPersonalizado() {
          this.listasRangoReporte.get(this.rangoCategoriaSeleccionado).
              remove(this.rangoReporteSeleccionado);
    }



    /** --------------------------------------------------------------------- */
    /**
     * Método que asocia un reporte a el usuario autenticado
     *
     * @author felipe.cadena
     */
    public void asociarReporteAUsuario(){

                    if(this.asociaReporteEjecucionUsuario(this.reporteVisualizacionSeleccionado) != null){

                        this.addMensajeInfo(ECodigoErrorVista.REPORTES_EST_001.getMensajeUsuario(
                                reporteVisualizacionSeleccionado.getCodigoReporte(), this.usuario.getLogin()));
                        this.reporteVisualizacionSeleccionado.setAsociado(true);


                    }
    }

    /**
     * Determina si los reportes dados estan asociados al usuario autenticado
     *
     *
     * @param reportes
     * @return
     */
    private List<RepReporteEjecucion> determinarAsociados(List<RepReporteEjecucion> reportes) {
        this.reportesEjecucionUsuarios = this.getConservacionService().
                buscarSubscripcionPorUsuario(this.usuario.getLogin());

        for (RepReporteEjecucion rre:reportes) {
            rre.setAsociado(false);
            for (RepReporteEjecucionUsuario rreu:this.reportesEjecucionUsuarios) {

                if(rre.getId().equals(rreu.getReporteEjecucionId())){
                    rre.setAsociado(true);
                    break;
                }

            }

        }
        return reportes;
    }

    /**
     * Prepara el objeto para la asociacion del usuario al reporte
     *
     *
     * @author felipe.cadena
     * @param repReporteEjecucion
     * @return
     */
    public RepReporteEjecucionUsuario asociaReporteEjecucionUsuario(
            RepReporteEjecucion repReporteEjecucion) {

        RepReporteEjecucionUsuario result = new RepReporteEjecucionUsuario();

        if (repReporteEjecucion != null) {

            result.setUsuarioGenera(this.usuario.getLogin());
            result.setReporteEjecucionId(repReporteEjecucion.getId());
            result.setUsuarioLog(repReporteEjecucion.getUsuario());
            result.setFechaLog(new Date());

            result = this.getConservacionService()
                    .actualizarRepReporteEjecucionUsuario(result);

            if (result == null) {
                this.addMensajeError(ECodigoErrorVista.REPORTES_EST_001.toString());
                LOGGER.error(ECodigoErrorVista.REPORTES_EST_001.getMensajeTecnico());
                return null;
            }
        }

        return result;
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros
     * del dominio AÑO_REPORTE
     * @author felipe.cadena
     * @return
     */
    public List<SelectItem> cargarAnoReporte() {

        this.anoReporteItemList = new ArrayList<SelectItem>();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String anioActual = String.valueOf(year);

        this.dominiosAnoReporte = this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.ANIO_REPORTE);

        for (Dominio dominio : this.dominiosAnoReporte) {
            this.anoReporteItemList.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
        }

        anoReporteItemList.add(0, new SelectItem(null,"Seleccionar..."));
        anioSeleccionado = anioActual;

        return anoReporteItemList;


    }


    /**
     * Actualiza los campos cuando se quiere buscar por codigo de reporte
     * @author felipe.cadena
     * @return
     */
    public void listenerBusquedaPorCodigo() {

        if(this.codigoReporteCod != null && !this.codigoReporteCod.isEmpty()) {
            this.condicionesCampos = this.condicionesTotalesCampos.getMapDefault();
            this.busquedaPorCodigo = true;
        }else{
            this.busquedaPorCodigo = false;
        }
    }


    /**
     * Método encargado de verificar la fecha de corte
     * en la que se genero el reporte
     * @author felipe.cadena
     */
    public void verificarAnoReporte(){

        int year = Calendar.getInstance().get(Calendar.YEAR);
        String anioActual = String.valueOf(year);

        if(this.fechaCorteSeleccionada != null
                && !this.fechaCorteSeleccionada.trim().isEmpty()){

            if ("2".equals(this.fechaCorteSeleccionada)
                    || "3".equals(this.fechaCorteSeleccionada)) {

                if (this.dominiosAnoReporte != null) {

                    this.anoReporteItemList = new ArrayList<SelectItem>();

                    for (Dominio dominio : this.dominiosAnoReporte) {

                        if (dominio.getCodigo() != null
                                && !anioActual.equals(dominio.getCodigo())) {

                            this.anoReporteItemList.add(new SelectItem(dominio.getCodigo(),
                                    dominio.getValor()));
                        }
                    }
                }

                if ("3".equals(this.fechaCorteSeleccionada)){
                    this.anoReporteItemList.add(new SelectItem(anioActual, anioActual));
                }
                anoReporteItemList.add(0, new SelectItem(null,"Seleccionar..."));
                anioSeleccionado = null;
            }
            else{
                this.anoReporteItemList = new ArrayList<SelectItem>();
                this.anoReporteItemList.add(new SelectItem(anioActual, anioActual));
                anioSeleccionado = anioActual;
            }

        }

    }

    /**
     * Valida si el numero predial esta diligenciado por los menos hasta manzana
     *
     * @author felipe.cadena
     *
     * @return
     */
    private boolean validarNumerosPrediales() {

        if (this.condicionesCampos.get("NO_PREDIAL_INICIAL")[1] ||
                this.condicionesCampos.get("NO_PREDIAL_FINAL")[1]) {

            if (this.datosConsultaPredio.getNumeroPredialFinal().length() < 17 ||
                    this.datosConsultaPredio.getNumeroPredial().length() < 17) {

                this.addMensajeError(ECodigoErrorVista.REPORTES_EST_009.toString());
                return false;
            }

        }
        return true;
    }

    /**
     * Valida si el usuario autenticado tiene permisos para genrar un reporte de nivel nacional
     *
     * @author felipe.cadena
     *
     * @return
     */
    private boolean validarPermisoNacional(){

        if(!this.nacional){
            return true;
        }

        if(!this.getGeneralesService().determinarReporteNacional(this.usuario.getLogin(),
                this.tipoReporteId)){

            this.addMensajeError(ECodigoErrorVista.REPORTES_EST_010.toString());
            return false;

        }

        return true;
    }

    /**
     * Retorna el nombre del reporte asociado al id dado
     *
     * @author felipe.cadena
     *
     * @param idReporte
     * @return
     */
    private String obtenerNombreReporte(Long idReporte){

        String nombre = null;

        for (SelectItem si : this.tipoReportesItemList) {

            if(si.getValue().equals(idReporte)){
                return si.getLabel();
            }

        }

        return nombre;
    }


    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método cerrar que forza realiza un llamado al método que remueve los
     * managed bean de la sesión y forza el init de tareasPendientesMB.
     *
     * @return
     */
    public String cerrar() {
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    Comparator<SelectItem> comparatorSI = new Comparator<SelectItem>() {
        @Override
        public int compare(SelectItem s1, SelectItem s2) {
            return s1.getLabel().compareTo(s2.getLabel());
        }
    };

    // Fin de la clase
}
