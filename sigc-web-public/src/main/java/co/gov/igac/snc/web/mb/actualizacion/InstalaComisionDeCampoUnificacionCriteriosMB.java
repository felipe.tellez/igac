package co.gov.igac.snc.web.mb.actualizacion;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 *
 *
 * @author javier.aponte
 *
 */
@Component("instalaComisionDeCampoUnificacionCriterios")
@Scope("session")
public class InstalaComisionDeCampoUnificacionCriteriosMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 3347869109786075919L;

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private UsuarioDTO usuario;

    private Actualizacion currentActualizacion;

    private boolean banderaAgregarAsistente;

    private ActualizacionEvento actualizacionEventoSeleccionado;

    private EventoAsistente asistenteInvitadoSeleccionado;

    private List<EventoAsistente> asistentesInvitados;

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

//		this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();
//		
//		Long idActualizacion = this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();
        Long idActualizacion = 250L;

        this.currentActualizacion = this.getActualizacionService().recuperarActualizacionPorId(
            idActualizacion);

    }

    public void mostrarAgregarAsistente() {
        if (banderaAgregarAsistente == false) {
            this.banderaAgregarAsistente = true;
        } else {
            this.banderaAgregarAsistente = false;
        }
    }

    public void agregarAsistenteInvitado() {
        this.asistenteInvitadoSeleccionado.setActualizacionEvento(
            this.actualizacionEventoSeleccionado);

        this.asistenteInvitadoSeleccionado = this.getActualizacionService().
            guardarYactualizarEventoAsistente(asistenteInvitadoSeleccionado);

        this.asistentesInvitados.add(asistenteInvitadoSeleccionado);

        this.asistenteInvitadoSeleccionado = new EventoAsistente();
    }

    public Actualizacion getCurrentActualizacion() {
        return currentActualizacion;
    }

    public void setCurrentActualizacion(Actualizacion currentActualizacion) {
        this.currentActualizacion = currentActualizacion;
    }

    public EventoAsistente getAsistenteInvitadoSeleccionado() {
        return asistenteInvitadoSeleccionado;
    }

    public void setAsistenteInvitadoSeleccionado(
        EventoAsistente asistenteInvitadoSeleccionado) {
        this.asistenteInvitadoSeleccionado = asistenteInvitadoSeleccionado;
    }

    public boolean isBanderaAgregarAsistente() {
        return banderaAgregarAsistente;
    }

    public void setBanderaAgregarAsistente(boolean banderaAgregarAsistente) {
        this.banderaAgregarAsistente = banderaAgregarAsistente;
    }

    public List<EventoAsistente> getAsistentesInvitados() {
        return asistentesInvitados;
    }

    public void setAsistentesInvitados(List<EventoAsistente> asistentesInvitados) {
        this.asistentesInvitados = asistentesInvitados;
    }

    public ActualizacionEvento getActualizacionEventoSeleccionado() {
        return actualizacionEventoSeleccionado;
    }

    public void setActualizacionEventoSeleccionado(
        ActualizacionEvento actualizacionEventoSeleccionado) {
        this.actualizacionEventoSeleccionado = actualizacionEventoSeleccionado;
    }

}
