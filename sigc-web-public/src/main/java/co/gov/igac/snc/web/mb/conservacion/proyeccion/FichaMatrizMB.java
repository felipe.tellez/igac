package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.*;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.*;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.ValidacionProyeccionDelTramiteDTO;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ESeccionEjecucion;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author david.cifuentes
 */
@Component("fichaMatriz")
@Scope("session")
public class FichaMatrizMB extends SNCManagedBean {

    private static final long serialVersionUID = 7802508813424834006L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(FichaMatrizMB.class);

    // ================= SERVICIOS ================ //
    @Autowired
    private ProyectarConservacionMB proyectarConservacionMB;
    @Autowired
    private EntradaProyeccionMB entradaProyecionMB;
    // ================= VARIABLES ================ //

    /**
     * Tramite seleccionado.
     */
    private Tramite tramite;

    /**
     * Usuario
     */
    private UsuarioDTO usuarioFichaMatriz;

    /**
     * PFichaMatriz usada en la interfaz gráfica
     */
    private PFichaMatriz fichaMatriz;

    /**
     * PFichaMatrizTorre usada para edición, eliminación y multiplicación de torres.
     */
    private PFichaMatrizTorre fichaMatrizTorreSeleccionada;

    /**
     * PFichaMatrizTorre usada para hacer una replica de la PFichaMatrizTorre seleccionada.
     */
    private PFichaMatrizTorre fichaMatrizTorreReplica;

    /**
     * PFichaMatriz usada para hacer una replica de la PFichaMatriz seleccionada.
     */
    private PFichaMatriz fichaMatrizReplica;

    /**
     * Predio del tramite de desenglobe.
     */
    private PPredio predioDesenglobe;

    /**
     * Variable que indica el número de torres a multiplicar
     */
    private Integer numeroTorresMultiplicar;

    /**
     * Variable PPredioZona usada para almacenar las zonas homogéneas del PPredio
     */
    private List<PPredioZona> zonasHomogeneas;

    /**
     * Variable bandera para saber si el predio es rural o urbano
     */
    private boolean predioRuralBool;

    /**
     * Valor del Avaluo total del terreno Comun.
     */
    private Double avaluoTotalTerrenoComun;

    /**
     * Variable usada para asignar la PUnidadConstrucción seleccionada para ver detalle, realizar
     * ediciones, inserciones o eliminaciones.
     */
    private PUnidadConstruccion unidadConstruccionSeleccionada;

    /**
     * Variable usada para almacenar los predios (PfichaMatrizPredios) generados en la ficha matriz.
     */
    private List<PFichaMatrizPredio> fichaMatrizPrediosResultantes;

    /**
     * Variable usada para almacenar los predios seleccionados en la ficha matriz para su edición.
     */
    private PFichaMatrizPredio[] fichaMatrizPrediosSeleccionados;

    /**
     * Variable usada para editar el coeficiente de copropiedad de un registro de la tabla de
     * numeros prediales resultantes.
     */
    private Double coeficienteCopropiedad;

    /**
     * Variable usada para visualizar la sumatoria del coeficiente de copropiedad de los predios
     * seleccionados para el tramite de englobe virtual.
     */
    private Double sumatoriaCoefCopropiedad;

    /**
     * Variable usada para saber si al momento de adicionar un fichaMatrizPredio se está adicionando
     * para un piso o para un sotano.
     */
    private String ejeVialHaciaAbajo;

    /**
     * Variable usada para almacenar el numero de una torre seleccionada, al momento de generar un
     * número predial.
     */
    private String torreSeleccionada;

    /**
     * Variable usada para almacenar el numero de una unidad inicial momento de modificar un número
     * predial.
     */
    private int unidadInicialSeleccionada;

    /**
     * Variable usada para almacenar el numero de una unidad inicial momento de modificar un número
     * predial.
     */
    private String unidadInicialSelec;

    /**
     * Variable usada para almacenar el numero de una unidad final momento de modificar un número
     * predial.
     */
    private String unidadFinalSeleccionada;

    /**
     * Lista de torres de la ficha Matriz.
     */
    private List<SelectItem> itemListTorres = new ArrayList<SelectItem>();

    /**
     * Lista de unidades iniciales de las fichas Matrices seleccionadas.
     */
    private List<SelectItem> itemListUnidadesIniciales = new ArrayList<SelectItem>();

    /**
     * Lista de los pisos de una torre seleccionada de la ficha Matriz.
     */
    private ArrayList<SelectItem> itemListPisos;

    /**
     * Lista de los sotanos de una torre seleccionada de la ficha Matriz.
     */
    private ArrayList<SelectItem> itemListSotanos;

    /**
     * Variable booleana usada para saber si existen sotanos para una torre.
     */
    private boolean sotanosBool;

    /**
     * Variable que almacena el numero del piso o del sotano seleccionado
     */
    private String pisoOSotanoSeleccionado;

    /**
     * Variable usada para llevar un conteo del número de unidades que debe tener un piso en la
     * generacion de numeros preduales.
     */
    private Long unidad;

    /**
     * Variable usada como contador para las unidades faltantes al momento de generar los números
     * prediales
     */
    private Long conteoUnidad;

    /**
     * variable usada para almacenar el último número predial disponible de una manzana.
     */
    private String numeroPredialDisponible;

    /**
     * Variable usada para llevar un seguimiento de los numeros prediales asignados por torre y por
     * piso
     */
    private int numerosPredialesGeneradosPorPiso[][];

    /**
     * Variable usada para llevar un seguimiento de los numeros prediales asignados por torre y por
     * sotano
     */
    private int numerosPredialesGeneradosPorSotano[][];

    /**
     * Variables booleanas para habilitar o no los campos de sotanos y pisos.
     */
    private boolean pisoOSotanoBool;
    private boolean unicaUnidadBool;
    private boolean torreCargadaBool;

    /**
     * Variables para controlar las validaciones de unidades en la generación de números prediales
     */
    private Long unidadesTotales;
    private Long unidadesRestantes;
    private Long unidadesAsignadas;

    /**
     * Variables usadas para controlar la generación de sótanos y unidades en PHs y Condominios.
     */
    private int sotanosRestantesPorTorrePH[];
    private int unidadesRestantesPorTorrePH[];
    private int sotanosAsignadosPorTorrePH[];
    private int unidadesAsignadasPorTorrePH[];
    private int unidadesRestantesCondominio;
    private int sotanosRestantesCondominio;

    private boolean vistaCompletaUC;

    /**
     * Variables usadas para manejo de construcciones nuevas y vigentes.
     */
    private List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas;
    private PUnidadConstruccion[] unidadConstruccionConvencionalVigentesSeleccionadas;
    private PUnidadConstruccion[] unidadConstruccionNoConvencionalVigentesSeleccionadas;

    /**
     * Variables para el manejo de las conversiones de áreas de terreno en la ficha matriz.
     */
    private Integer areaTotalTerrenoComunHa;
    private Double areaTotalTerrenoComunM2;
    private Integer areaTotalTerrenoPrivadaHa;
    private Double areaTotalTerrenoPrivadaM2;
    private Integer areaTotalTerrenoHa;
    private Double areaTotalTerrenoM2;
    private Double areaTotalConstruidaComunM2;
    private Double areaTotalConstruidaPrivadaM2;
    private Double areaTotalConstruidaM2;

    /**
     * Variable usada para almacenar los números prediales generados en una condición de propiedad
     * condominio.
     */
    private int numerosPredialesGeneradosCondominio[];

    /**
     * Lista de ValidacionProyeccionDelTramiteDTO que almacena los datos de las validaciones del
     * trámite.
     */
    private List<ValidacionProyeccionDelTramiteDTO> listValidacionDelTramite;

    /**
     * Variable que contiene el valor del avaluo total
     */
    private BigDecimal avaluoTotal;

    /**
     * Variable booleana para saber si se han seleccionado todos los predios generados en la ficha
     * matriz.
     */
    private boolean selectedAllBool;

    /**
     * Variable usada para registrar las nuevas unidades privadas del condominio para desenglobes en
     * etapas.
     */
    private long unidadesNuevasCondominioEtapas;

    /**
     * Variable usada para registrar las nuevas unidades privadas del condominio para englobe
     * virtual.
     */
    private long unidadesNuevasEV;

    /**
     * Variable usada para habilitar el botón de modificar número predial en los predios de la ficha
     * matriz provenientes de migración.
     */
    private boolean modificarPrediosMigracionBool;

    /**
     * Variable usada para almacenar la lista de los {@link PFichaMatrizPredio} cancelados
     */
    private List<PFichaMatrizPredio> pFichaMatrizPrediosCancelados;

    /**
     * Variable usada para almacenar los predios seleccionados en la activación de predios para PH
     * por etapas.
     */
    private PFichaMatrizPredio[] prediosInactivosSeleccionados;

    /**
     * Variable booleana usada para seleccionar todos los predios de la pantalla de activar predios.
     */
    private boolean selectedAllActivarPrediosBool;

    /**
     * Variable usada para saber si se está creando o editando una torre
     */
    private boolean editModeTorre;

    /**
     * Variables usadas para el manejo de construcciones originales del predio
     */
    private List<UnidadConstruccion> unidadConstruccionConvencionalPredioOriginal;
    private List<UnidadConstruccion> unidadConstruccionNoConvencionalPredioOriginal;
    private UnidadConstruccion[] unidadConstruccionConvencionalPredioOriginalSeleccionadas;
    private UnidadConstruccion[] unidadConstruccionNoConvencionalPredioOriginalSeleccionadas;

    /**
     * Variables para englobe virtual
     */
    private List<SelectItem> tiposCondicionEV;

    private boolean banderaCondominio;

    private boolean banderaPH;

    private boolean mantieneFicha;

    /**
     * Variable usada para almacenar al generar el predio, la condicion del predio selecionada
     */
    private String condicionPredioSeleccionada;

    /**
     * Listas englobe virtual
     */
    private PUnidadConstruccion[] unidadConstruccionConvencionalSelEV;
    private PUnidadConstruccion[] unidadConstruccionNoConvencionalSelEV;

    /**
     * Variable booleana usada para saber si el predio es condicion 8.
     */
    private boolean predioCondProp8;

    /**
     * Variable booleana usada para saber si el predio es condicion 9.
     */
    private boolean predioCondProp9;

    /**
     * Variable usada para almacenar el numero de torre inicial al momento de multiplicar una torre
     */
    private long numeroTorreInicial;

    /**
     * PFichaMatriz usada para cualdo la torre es en firme para consultar de que ficha matriz fue
     * creada.
     */
    private FichaMatriz fichaMatrizOriginalTorreSeleccionada;

    private boolean activaPisoSotano;

    /**
     * Variable usada para almacenar (PfichaMatriz) en la ficha matriz canceladas y los que se
     * mantienen, para el tramite.
     */
    private List<PFichaMatriz> fichasMatricesTramite;

    /**
     * Variable usada para almacenar (PfichaMatriz) de los condominios en la ficha matriz canceladas
     * y los que se mantienen, para el tramite.
     */
    private List<PFichaMatriz> fichasMatrizCondominio;

    /**
     * Variable que almacena el número predial para filtrar en la tabla PFichaMatrizPredios
     */
    private String numeroPredialBusquedaPFichaMatrizPredios;

    /**
     * Variable booleana usada para habilitar boton aceptar al modificar numeros prediales.
     */
    private boolean banderaAceptarNumPredial;

    /**
     * Variable booleana usada para habilitar boton aceptar en el calculo de coeficientes de
     * copropiedad.
     */
    private boolean banderaAceptarCoefCoprop;

    /**
     * Variable para cargar la direccion principal de la ficha Matriz.
     */
    private PPredioDireccion direccionPrincipalFM;

    /**
     * Variable para cargar la direccion pribncipal de los nuevos numeros prediales generales.
     */
    private PPredioDireccion direccionPredioGenerado;

    private long undInicialCondominio = 0;

    // ================= INIT ================ //
    @PostConstruct
    public void init() {
        try {

            // Usuario
            this.usuarioFichaMatriz = this.proyectarConservacionMB.getUsuario();
            // Trámite
            this.tramite = this.proyectarConservacionMB.getTramite();

            //felipe.cadena:: carga la ficha matriz cuando son tramites de rectificacion de ficha matriz
            if (this.tramite == null) {
                return;
            }
            if (this.tramite.isRectificacionMatriz() ||
                 this.tramite.isTerceraMasiva() ||
                 this.tramite.isCancelacionMasiva() ||
                 this.tramite.isQuintaMasivo()) {
                this.fichaMatriz = this.getConservacionService()
                    .buscarPFichaMatrizPorPredioId(
                        tramite.getPredio().getId());
            }

            // javier.aponte:: carga la ficha matriz cuano son tramites de
            // tercera masiva
            if (this.tramite.isTerceraMasiva()) {
                // Cargar las unidades de construcción vigentes
                this.cargarUnidadesConstruccionVigentesTerceraMasiva();
            }

            if (tramite.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) &&
                 tramite.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) &&
                 tramite.getSubtipo().equals(
                    EMutacion2Subtipo.DESENGLOBE.getCodigo()) ||
                 this.tramite.isRectificacionMatriz() ||
                 this.tramite.isTerceraMasiva() ||
                 this.tramite.isCancelacionMasiva() ||
                 this.tramite.isQuintaMasivo()) {

                // Predio
                this.predioDesenglobe = this.proyectarConservacionMB
                    .getPredioSeleccionado();

                if (this.predioDesenglobe != null &&
                     !tramite.isEnglobeVirtual()) {

                    // Consultar ficha matriz
                    proyectarConservacionMB.consultarFichaMatriz();

                    if (this.getFichaMatriz() != null) {

                        if (this.fichaMatriz.getTotalUnidadesPrivadas() == null) {
                            this.fichaMatriz.setTotalUnidadesPrivadas(0L);
                            this.proyectarConservacionMB
                                .setFichaMatrizSeleccionada(this.fichaMatriz);
                        }

                        this.modificarPrediosMigracionBool = false;

                        // ZONAS HOMOGÉNEAS DEL PREDIO
                        consultarZonas();

                        // Bandera que determinaa si un predio es rural o urbano
                        this.predioRuralBool = this.predioDesenglobe
                            .isPredioRural();

                        // Inicialización de variables para la creación de los
                        // números prediales
                        this.unidadesTotales = 0L;
                        this.unidadesAsignadas = 0L;
                        this.unidadesRestantes = 0L;
                        this.conteoUnidad = 0L;
                        this.cargarMatrizNumerosPrediales();

                        // Método que hace la conversión de áreas de terreno en
                        // la ficha matriz. (Ha / M2)
                        this.convertirAreasAHaM2();

                        // manejo de unidades de construcción
                        if (this.tramite.isRectificacionMatriz()) {
                            this.setupUnidadesDeConstruccionRectMatriz();
                        } else if (this.tramite.isTerceraMasiva()) {
                            this.setupUnidadesDeConstruccionTerceraMasiva();
                        } else {
                            this.setupUnidadesDeConstruccion();
                        }

                        // manejo de modelos de construcción
                        this.setupModelosDeConstruccion();

                        // calcular valor total avaluo
                        this.calcularAvaluoTotal();

                        // Actualizar áreas de la ficha matriz
                        this.actualizarTotalesAreasEnFichaMatriz();

                        // Se verifican los totales de la ficha matriz
                        if (!tramite.isEnglobeVirtual()) {
                            this.verificarTorresEnFirme();
                        }

                        // Inicializar una replica de la ficha matriz para 
                        // conservar la original en ciertas comparaciones
                        this.setupFichaMatrizReplica();

                        // Verificar que todos los PFichaMatrizPredio se
                        // encuentren asociados a un predio activo
                        this.cargarEstadoPFichaMatrizPredios();
                    }

                    // Cargue de direcciones
                    this.proyectarConservacionMB.actualizarListaDeDirecciones();
                }
                this.vistaCompletaUC = true;
            }

            if (tramite.isEnglobeVirtual()) {

                //No inicializa nada si el predio sellecionado no es una ficha matriz
                if (this.getProyectarConservacionMB().getPredioSeleccionado() == null ||
                    !this.getProyectarConservacionMB().getPredioSeleccionado().
                        isEsPredioFichaMatriz()) {
                    return;
                }

                this.banderaAceptarCoefCoprop = false;
                this.banderaCondominio = false;
                this.banderaPH = false;
                this.mantieneFicha = false;
                this.tiposCondicionEV = new ArrayList<SelectItem>();
                this.banderaAceptarNumPredial = false;

                if (this.getProyectarConservacionMB().getPredioSeleccionado() != null) {
                    this.fichaMatriz = this.getConservacionService()
                        .buscarPFichaMatrizPorPredioId(this.getProyectarConservacionMB().
                            getPredioSeleccionado().getId());
                    this.proyectarConservacionMB.setFichaMatrizSeleccionada(this.fichaMatriz);
                }

                this.cargarMatrizNumerosPrediales();

                inicializaMenuCondicionEV();

                if (!this.entradaProyecionMB.getNuevaFichaMatriz().isEmpty() &&
                     this.entradaProyecionMB.getNuevaFichaMatriz().equals(ESiNo.NO.getCodigo())) {
                    this.mantieneFicha = true;
                } else {
                    this.mantieneFicha = false;
                }

                if (this.tramite.isTipoInscripcionCondominio() ||
                     (this.proyectarConservacionMB.getFichaMatrizSeleccionada() != null &&
                     this.proyectarConservacionMB.getFichaMatrizSeleccionada().getPPredio().
                        isMixto())) {

                    this.consultarFichasMatrizCondominio();

                    if (this.mantieneFicha) {

                        FichaMatriz fichaMatrizOriginalMantiene = this.getConservacionService().
                            obtenerFichaMatrizPorId(this.fichaMatriz.getId());

                        if (fichaMatrizOriginalMantiene != null) {
                            this.undInicialCondominio = fichaMatrizOriginalMantiene.
                                getTotalUnidadesPrivadas();
                        }

                    }

                }

                this.modificarPrediosMigracionBool = false;

                // Método que hace la conversión de áreas de terreno en
                // la ficha matriz. (Ha / M2)
                this.convertirAreasAHaM2();

                // manejo de unidades de construcción
                this.setupUnidadesDeConstruccion();

                // manejo de modelos de construcción
                this.setupModelosDeConstruccion();

                // calcular valor total avaluo
                this.calcularAvaluoTotal();

                // Actualizar áreas de la ficha matriz
                this.actualizarTotalesAreasEnFichaMatriz();

                // Inicializar una replica de la ficha matriz para 
                // conservar la original en ciertas comparaciones
                this.setupFichaMatrizReplica();

                // Verificar que todos los PFichaMatrizPredio se
                // encuentren asociados a un predio activo
                this.cargarEstadoPFichaMatrizPredios();

                // ZONAS HOMOGÃNEAS DEL PREDIO
                consultarZonas();

                // Cargue de direcciones
                this.proyectarConservacionMB.actualizarListaDeDirecciones();

                if (this.fichaMatriz != null &&
                     this.fichaMatriz.getPFichaMatrizTorres() != null &&
                     !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                    this.verificarTorresEnFirme();
                    Collections.sort(this.fichaMatriz.getPFichaMatrizTorres());
                }

                getFichaMatrizPredios();

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Error al cargar la ficha matriz.");
        }
    }

    public PPredioDireccion getDireccionPredioGenerado() {
        return direccionPredioGenerado;
    }

    public boolean isBanderaAceptarCoefCoprop() {
        return banderaAceptarCoefCoprop;
    }

    // ================= GETTERS Y SETTERS ================ //
    public void setBanderaAceptarCoefCoprop(boolean banderaAceptarCoefCoprop) {
        this.banderaAceptarCoefCoprop = banderaAceptarCoefCoprop;
    }

    public void setDireccionPredioGenerado(PPredioDireccion direccionPredioGenerado) {
        this.direccionPredioGenerado = direccionPredioGenerado;
    }

    public PPredioDireccion getDireccionPrincipalFM() {
        return direccionPrincipalFM;
    }

    public void setDireccionPrincipalFM(PPredioDireccion direccionPrincipalFM) {
        this.direccionPrincipalFM = direccionPrincipalFM;
    }

    public boolean isBanderaAceptarNumPredial() {
        return banderaAceptarNumPredial;
    }

    public void setBanderaAceptarNumPredial(boolean banderaAceptarNumPredial) {
        this.banderaAceptarNumPredial = banderaAceptarNumPredial;
    }

    public List<PFichaMatriz> getFichasMatrizCondominio() {
        return fichasMatrizCondominio;
    }

    public void setFichasMatrizCondominio(List<PFichaMatriz> fichasMatrizCondominio) {
        this.fichasMatrizCondominio = fichasMatrizCondominio;
    }

    public String getNumeroPredialBusquedaPFichaMatrizPredios() {
        return numeroPredialBusquedaPFichaMatrizPredios;
    }

    public void setNumeroPredialBusquedaPFichaMatrizPredios(
        String numeroPredialBusquedaPFichaMatrizPredios) {
        this.numeroPredialBusquedaPFichaMatrizPredios = numeroPredialBusquedaPFichaMatrizPredios;
    }

    public List<PFichaMatriz> getFichasMatricesTramite() {
        return fichasMatricesTramite;
    }

    public void setFichasMatricesTramite(List<PFichaMatriz> fichasMatricesTramite) {
        this.fichasMatricesTramite = fichasMatricesTramite;
    }

    public boolean isActivaPisoSotano() {
        return activaPisoSotano;
    }

    public void setActivaPisoSotano(boolean activaPisoSotano) {
        this.activaPisoSotano = activaPisoSotano;
    }

    public FichaMatriz getFichaMatrizOriginalTorreSeleccionada() {
        return fichaMatrizOriginalTorreSeleccionada;
    }

    public void setFichaMatrizOriginalTorreSeleccionada(
        FichaMatriz fichaMatrizOriginalTorreSeleccionada) {
        this.fichaMatrizOriginalTorreSeleccionada = fichaMatrizOriginalTorreSeleccionada;
    }

    public long getNumeroTorreInicial() {
        return numeroTorreInicial;
    }

    public void setNumeroTorreInicial(long numeroTorreInicial) {
        this.numeroTorreInicial = numeroTorreInicial;
    }

    public PPredio getPredioDesenglobe() {
        return predioDesenglobe;
    }

    public void setPredioDesenglobe(PPredio predioDesenglobe) {
        this.predioDesenglobe = predioDesenglobe;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public PUnidadConstruccion[] getUnidadConstruccionConvencionalSelEV() {
        return unidadConstruccionConvencionalSelEV;
    }

    public void setUnidadConstruccionConvencionalSelEV(
        PUnidadConstruccion[] unidadConstruccionConvencionalSelEV) {
        this.unidadConstruccionConvencionalSelEV = unidadConstruccionConvencionalSelEV;
    }

    public PUnidadConstruccion[] getUnidadConstruccionNoConvencionalSelEV() {
        return unidadConstruccionNoConvencionalSelEV;
    }

    public void setUnidadConstruccionNoConvencionalSelEV(
        PUnidadConstruccion[] unidadConstruccionNoConvencionalSelEV) {
        this.unidadConstruccionNoConvencionalSelEV = unidadConstruccionNoConvencionalSelEV;
    }

    public PFichaMatriz getFichaMatriz() {
        this.fichaMatriz = this.proyectarConservacionMB
            .getFichaMatrizSeleccionada();
        return this.fichaMatriz;
    }

    public void setFichaMatriz(PFichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    public PFichaMatrizTorre getFichaMatrizTorreSeleccionada() {
        return fichaMatrizTorreSeleccionada;
    }

    public void setFichaMatrizTorreSeleccionada(
        PFichaMatrizTorre fichaMatrizTorreSeleccionada) {
        this.fichaMatrizTorreSeleccionada = fichaMatrizTorreSeleccionada;
    }

    public Integer getNumeroTorresMultiplicar() {
        return numeroTorresMultiplicar;
    }

    public void setNumeroTorresMultiplicar(Integer numeroTorresMultiplicar) {
        this.numeroTorresMultiplicar = numeroTorresMultiplicar;
    }

    public PUnidadConstruccion getUnidadConstruccionSeleccionada() {
        return unidadConstruccionSeleccionada;
    }

    public void setUnidadConstruccionSeleccionada(
        PUnidadConstruccion unidadConstruccionSeleccionada) {
        this.unidadConstruccionSeleccionada = unidadConstruccionSeleccionada;
    }

    public List<ValidacionProyeccionDelTramiteDTO> getListValidacionDelTramite() {
        return listValidacionDelTramite;
    }

    public void setListValidacionDelTramite(
        List<ValidacionProyeccionDelTramiteDTO> listValidacionDelTramite) {
        this.listValidacionDelTramite = listValidacionDelTramite;
    }

    public List<PPredioZona> getZonasHomogeneas() {
        return zonasHomogeneas;
    }

    public void setZonasHomogeneas(List<PPredioZona> zonasHomogeneas) {
        this.zonasHomogeneas = zonasHomogeneas;
    }

    public boolean isPredioRuralBool() {
        return predioRuralBool;
    }

    public void setPredioRuralBool(boolean predioRuralBool) {
        this.predioRuralBool = predioRuralBool;
    }

    public Integer getAreaTotalTerrenoComunHa() {
        return areaTotalTerrenoComunHa;
    }

    public void setAreaTotalTerrenoComunHa(Integer areaTotalTerrenoComunHa) {
        this.areaTotalTerrenoComunHa = areaTotalTerrenoComunHa;
    }

    public int[] getSotanosAsignadosPorTorrePH() {
        return sotanosAsignadosPorTorrePH;
    }

    public void setSotanosAsignadosPorTorrePH(int[] sotanosAsignadosPorTorrePH) {
        this.sotanosAsignadosPorTorrePH = sotanosAsignadosPorTorrePH;
    }

    public int[] getUnidadesAsignadasPorTorrePH() {
        return unidadesAsignadasPorTorrePH;
    }

    public void setUnidadesAsignadasPorTorrePH(int[] unidadesAsignadasPorTorrePH) {
        this.unidadesAsignadasPorTorrePH = unidadesAsignadasPorTorrePH;
    }

    public Double getAreaTotalTerrenoComunM2() {
        return areaTotalTerrenoComunM2;
    }

    public int[] getNumerosPredialesGeneradosCondominio() {
        return numerosPredialesGeneradosCondominio;
    }

    public void setNumerosPredialesGeneradosCondominio(
        int[] numerosPredialesGeneradosCondominio) {
        this.numerosPredialesGeneradosCondominio = numerosPredialesGeneradosCondominio;
    }

    public void setAreaTotalTerrenoComunM2(Double areaTotalTerrenoComunM2) {
        this.areaTotalTerrenoComunM2 = areaTotalTerrenoComunM2;
    }

    public boolean isSelectedAllBool() {
        return selectedAllBool;
    }

    public void setSelectedAllBool(boolean selectedAllBool) {
        this.selectedAllBool = selectedAllBool;
    }

    public Integer getAreaTotalTerrenoPrivadaHa() {
        return areaTotalTerrenoPrivadaHa;
    }

    public void setAreaTotalTerrenoPrivadaHa(Integer areaTotalTerrenoPrivadaHa) {
        this.areaTotalTerrenoPrivadaHa = areaTotalTerrenoPrivadaHa;
    }

    public Double getAreaTotalTerrenoPrivadaM2() {
        return areaTotalTerrenoPrivadaM2;
    }

    public void setAreaTotalTerrenoPrivadaM2(Double areaTotalTerrenoPrivadaM2) {
        this.areaTotalTerrenoPrivadaM2 = areaTotalTerrenoPrivadaM2;
    }

    public PFichaMatrizTorre getFichaMatrizTorreReplica() {
        return fichaMatrizTorreReplica;
    }

    public void setFichaMatrizTorreReplica(
        PFichaMatrizTorre fichaMatrizTorreReplica) {
        this.fichaMatrizTorreReplica = fichaMatrizTorreReplica;
    }

    public int[] getSotanosRestantesPorTorrePH() {
        return sotanosRestantesPorTorrePH;
    }

    public void setSotanosRestantesPorTorrePH(int[] sotanosRestantesPorTorrePH) {
        this.sotanosRestantesPorTorrePH = sotanosRestantesPorTorrePH;
    }

    public int[] getUnidadesRestantesPorTorrePH() {
        return unidadesRestantesPorTorrePH;
    }

    public void setUnidadesRestantesPorTorrePH(int[] unidadesRestantesPorTorrePH) {
        this.unidadesRestantesPorTorrePH = unidadesRestantesPorTorrePH;
    }

    public int getUnidadesRestantesCondominio() {
        return unidadesRestantesCondominio;
    }

    public void setUnidadesRestantesCondominio(int unidadesRestantesCondominio) {
        this.unidadesRestantesCondominio = unidadesRestantesCondominio;
    }

    public int getSotanosRestantesCondominio() {
        return sotanosRestantesCondominio;
    }

    public void setSotanosRestantesCondominio(int sotanosRestantesCondominio) {
        this.sotanosRestantesCondominio = sotanosRestantesCondominio;
    }

    public PFichaMatriz getFichaMatrizReplica() {
        return fichaMatrizReplica;
    }

    public void setFichaMatrizReplica(
        PFichaMatriz fichaMatrizReplica) {
        this.fichaMatrizReplica = fichaMatrizReplica;
    }

    public Integer getAreaTotalTerrenoHa() {
        return areaTotalTerrenoHa;
    }

    public void setAreaTotalTerrenoHa(Integer areaTotalTerrenoHa) {
        this.areaTotalTerrenoHa = areaTotalTerrenoHa;
    }

    public Double getAreaTotalTerrenoM2() {
        return areaTotalTerrenoM2;
    }

    public void setAreaTotalTerrenoM2(Double areaTotalTerrenoM2) {
        this.areaTotalTerrenoM2 = areaTotalTerrenoM2;
    }

    public Double getAvaluoTotalTerrenoComun() {
        return avaluoTotalTerrenoComun;
    }

    public void setAvaluoTotalTerrenoComun(Double avaluoTotalTerrenoComun) {
        this.avaluoTotalTerrenoComun = avaluoTotalTerrenoComun;
    }

    public UnidadConstruccion[] getUnidadConstruccionConvencionalPredioOriginalSeleccionadas() {
        return unidadConstruccionConvencionalPredioOriginalSeleccionadas;
    }

    public void setUnidadConstruccionConvencionalPredioOriginalSeleccionadas(
        UnidadConstruccion[] unidadConstruccionConvencionalPredioOriginalSeleccionadas) {
        this.unidadConstruccionConvencionalPredioOriginalSeleccionadas =
            unidadConstruccionConvencionalPredioOriginalSeleccionadas;
    }

    public Double getAreaTotalConstruidaComunM2() {
        return areaTotalConstruidaComunM2;
    }

    public void setAreaTotalConstruidaComunM2(Double areaTotalConstruidaComunM2) {
        this.areaTotalConstruidaComunM2 = areaTotalConstruidaComunM2;
    }

    public List<UnidadConstruccion> getUnidadConstruccionNoConvencionalPredioOriginal() {
        return unidadConstruccionNoConvencionalPredioOriginal;
    }

    public void setUnidadConstruccionNoConvencionalPredioOriginal(
        List<UnidadConstruccion> unidadConstruccionNoConvencionalPredioOriginal) {
        this.unidadConstruccionNoConvencionalPredioOriginal =
            unidadConstruccionNoConvencionalPredioOriginal;
    }

    public Double getAreaTotalConstruidaPrivadaM2() {
        return areaTotalConstruidaPrivadaM2;
    }

    public void setAreaTotalConstruidaPrivadaM2(
        Double areaTotalConstruidaPrivadaM2) {
        this.areaTotalConstruidaPrivadaM2 = areaTotalConstruidaPrivadaM2;
    }

    public UnidadConstruccion[] getUnidadConstruccionNoConvencionalPredioOriginalSeleccionadas() {
        return unidadConstruccionNoConvencionalPredioOriginalSeleccionadas;
    }

    public void setUnidadConstruccionNoConvencionalPredioOriginalSeleccionadas(
        UnidadConstruccion[] unidadConstruccionNoConvencionalPredioOriginalSeleccionadas) {
        this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas =
            unidadConstruccionNoConvencionalPredioOriginalSeleccionadas;
    }

    public Double getAreaTotalConstruidaM2() {
        return areaTotalConstruidaM2;
    }

    public void setAreaTotalConstruidaM2(Double areaTotalConstruidaM2) {
        this.areaTotalConstruidaM2 = areaTotalConstruidaM2;
    }

    public List<PFichaMatrizPredio> getFichaMatrizPrediosResultantes() {
        return fichaMatrizPrediosResultantes;
    }

    public void setFichaMatrizPrediosResultantes(
        List<PFichaMatrizPredio> fichaMatrizPrediosResultantes) {
        this.fichaMatrizPrediosResultantes = fichaMatrizPrediosResultantes;
    }

    public PFichaMatrizPredio[] getFichaMatrizPrediosSeleccionados() {
        return fichaMatrizPrediosSeleccionados;
    }

    public void setFichaMatrizPrediosSeleccionados(
        PFichaMatrizPredio[] fichaMatrizPrediosSeleccionados) {
        this.fichaMatrizPrediosSeleccionados = fichaMatrizPrediosSeleccionados;
    }

    public Double getCoeficienteCopropiedad() {
        return coeficienteCopropiedad;
    }

    public void setCoeficienteCopropiedad(Double coeficienteCopropiedad) {
        this.coeficienteCopropiedad = coeficienteCopropiedad;
    }

    public String getEjeVialHaciaAbajo() {
        return ejeVialHaciaAbajo;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalVigentes() {
        return unidadConstruccionConvencionalVigentes;
    }

    public void setUnidadConstruccionConvencionalVigentes(
        List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes) {
        this.unidadConstruccionConvencionalVigentes = unidadConstruccionConvencionalVigentes;
    }

    public BigDecimal getAvaluoTotal() {
        return avaluoTotal;
    }

    public void setAvaluoTotal(BigDecimal avaluoTotal) {
        this.avaluoTotal = avaluoTotal;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalVigentes() {
        return unidadConstruccionNoConvencionalVigentes;
    }

    public void setUnidadConstruccionNoConvencionalVigentes(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes) {
        this.unidadConstruccionNoConvencionalVigentes = unidadConstruccionNoConvencionalVigentes;
    }

    public boolean isVistaCompletaUC() {
        return vistaCompletaUC;
    }

    public void setVistaCompletaUC(boolean vistaCompletaUC) {
        this.vistaCompletaUC = vistaCompletaUC;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevas() {
        return unidadConstruccionConvencionalNuevas;
    }

    public void setUnidadConstruccionConvencionalNuevas(
        List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas) {
        this.unidadConstruccionConvencionalNuevas = unidadConstruccionConvencionalNuevas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevas() {
        return unidadConstruccionNoConvencionalNuevas;
    }

    public void setUnidadConstruccionNoConvencionalNuevas(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas) {
        this.unidadConstruccionNoConvencionalNuevas = unidadConstruccionNoConvencionalNuevas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevasNoCanceladas() {
        return unidadConstruccionConvencionalNuevasNoCanceladas;
    }

    public void setUnidadConstruccionConvencionalNuevasNoCanceladas(
        List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas) {
        this.unidadConstruccionConvencionalNuevasNoCanceladas =
            unidadConstruccionConvencionalNuevasNoCanceladas;
    }

    public PFichaMatrizPredio[] getPrediosInactivosSeleccionados() {
        return prediosInactivosSeleccionados;
    }

    public void setPrediosInactivosSeleccionados(
        PFichaMatrizPredio[] prediosInactivosSeleccionados) {
        this.prediosInactivosSeleccionados = prediosInactivosSeleccionados;
    }

    public boolean isModificarPrediosMigracionBool() {
        return modificarPrediosMigracionBool;
    }

    public void setModificarPrediosMigracionBool(
        boolean modificarPrediosMigracionBool) {
        this.modificarPrediosMigracionBool = modificarPrediosMigracionBool;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevasNoCanceladas() {
        return unidadConstruccionNoConvencionalNuevasNoCanceladas;
    }

    public void setUnidadConstruccionNoConvencionalNuevasNoCanceladas(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas) {
        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            unidadConstruccionNoConvencionalNuevasNoCanceladas;
    }

    public PUnidadConstruccion[] getUnidadConstruccionConvencionalVigentesSeleccionadas() {
        return unidadConstruccionConvencionalVigentesSeleccionadas;
    }

    public void setUnidadConstruccionConvencionalVigentesSeleccionadas(
        PUnidadConstruccion[] unidadConstruccionConvencionalVigentesSeleccionadas) {
        this.unidadConstruccionConvencionalVigentesSeleccionadas =
            unidadConstruccionConvencionalVigentesSeleccionadas;
    }

    public PUnidadConstruccion[] getUnidadConstruccionNoConvencionalVigentesSeleccionadas() {
        return unidadConstruccionNoConvencionalVigentesSeleccionadas;
    }

    public void setUnidadConstruccionNoConvencionalVigentesSeleccionadas(
        PUnidadConstruccion[] unidadConstruccionNoConvencionalVigentesSeleccionadas) {
        this.unidadConstruccionNoConvencionalVigentesSeleccionadas =
            unidadConstruccionNoConvencionalVigentesSeleccionadas;
    }

    public void setEjeVialHaciaAbajo(String ejeVialHaciaAbajo) {
        this.ejeVialHaciaAbajo = ejeVialHaciaAbajo;
    }

    public String getTorreSeleccionada() {
        return torreSeleccionada;
    }

    public void setTorreSeleccionada(String torreSeleccionada) {
        this.torreSeleccionada = torreSeleccionada;
    }

    public int getUnidadInicialSeleccionada() {
        return unidadInicialSeleccionada;
    }

    public void setUnidadInicialSeleccionada(int unidadInicialSeleccionada) {
        this.unidadInicialSeleccionada = unidadInicialSeleccionada;
    }

    public String getUnidadInicialSelec() {
        return unidadInicialSelec;
    }

    public void setUnidadInicialSelec(String unidadInicialSelec) {
        this.unidadInicialSelec = unidadInicialSelec;
    }

    public String getUnidadFinalSeleccionada() {
        return unidadFinalSeleccionada;
    }

    public void setUnidadFinalSeleccionada(String unidadFinalSeleccionada) {
        this.unidadFinalSeleccionada = unidadFinalSeleccionada;
    }

    public List<SelectItem> getItemListTorres() {
        return itemListTorres;
    }

    public void setItemListTorres(List<SelectItem> itemListTorres) {
        this.itemListTorres = itemListTorres;
    }

    public List<SelectItem> getItemListUnidadesIniciales() {
        return itemListUnidadesIniciales;
    }

    public void setItemListUnidadesIniciales(List<SelectItem> itemListUnidadesIniciales) {
        this.itemListUnidadesIniciales = itemListUnidadesIniciales;
    }

    public String getPisoOSotanoSeleccionado() {
        return pisoOSotanoSeleccionado;
    }

    public void setPisoOSotanoSeleccionado(String pisoOSotanoSeleccionado) {
        this.pisoOSotanoSeleccionado = pisoOSotanoSeleccionado;
    }

    public ArrayList<SelectItem> getItemListPisos() {
        return itemListPisos;
    }

    public void setItemListPisos(ArrayList<SelectItem> itemListPisos) {
        this.itemListPisos = itemListPisos;
    }

    public ArrayList<SelectItem> getItemListSotanos() {
        return itemListSotanos;
    }

    public void setItemListSotanos(ArrayList<SelectItem> itemListSotanos) {
        this.itemListSotanos = itemListSotanos;
    }

    public boolean isSotanosBool() {
        return sotanosBool;
    }

    public void setSotanosBool(boolean sotanosBool) {
        this.sotanosBool = sotanosBool;
    }

    public Long getUnidad() {
        return unidad;
    }

    public void setUnidad(Long unidad) {
        this.unidad = unidad;
    }

    public UsuarioDTO getUsuarioFichaMatriz() {
        return usuarioFichaMatriz;
    }

    public void setUsuarioFichaMatriz(UsuarioDTO usuarioFichaMatriz) {
        this.usuarioFichaMatriz = usuarioFichaMatriz;
    }

    public List<PFichaMatrizPredio> getpFichaMatrizPrediosCancelados() {
        return pFichaMatrizPrediosCancelados;
    }

    public void setpFichaMatrizPrediosCancelados(
        List<PFichaMatrizPredio> pFichaMatrizPrediosCancelados) {
        this.pFichaMatrizPrediosCancelados = pFichaMatrizPrediosCancelados;
    }

    public void setConteoUnidad(Long conteoUnidad) {
        this.conteoUnidad = conteoUnidad;
    }

    public Long getConteoUnidad() {
        return conteoUnidad;
    }

    public String getNumeroPredialDisponible() {
        return numeroPredialDisponible;
    }

    public void setNumeroPredialDisponible(String numeroPredialDisponible) {
        this.numeroPredialDisponible = numeroPredialDisponible;
    }

    public int[][] getNumerosPredialesGeneradosPorPiso() {
        return numerosPredialesGeneradosPorPiso;
    }

    public void setNumerosPredialesGeneradosPorPiso(
        int[][] numerosPredialesGeneradosPorPiso) {
        this.numerosPredialesGeneradosPorPiso = numerosPredialesGeneradosPorPiso;
    }

    public List<UnidadConstruccion> getUnidadConstruccionConvencionalPredioOriginal() {
        return unidadConstruccionConvencionalPredioOriginal;
    }

    public void setUnidadConstruccionConvencionalPredioOriginal(
        List<UnidadConstruccion> unidadConstruccionConvencionalPredioOriginal) {
        this.unidadConstruccionConvencionalPredioOriginal =
            unidadConstruccionConvencionalPredioOriginal;
    }

    public void setNumerosPredialesGeneradosPorSotano(
        int numerosPredialesGeneradosPorSotano[][]) {
        this.numerosPredialesGeneradosPorSotano = numerosPredialesGeneradosPorSotano;
    }

    public boolean isEditModeTorre() {
        return editModeTorre;
    }

    public void setEditModeTorre(boolean editModeTorre) {
        this.editModeTorre = editModeTorre;
    }

    public int[][] getNumerosPredialesGeneradosPorSotano() {
        return numerosPredialesGeneradosPorSotano;
    }

    public boolean isPisoOSotanoBool() {
        return pisoOSotanoBool;
    }

    public void setPisoOSotanoBool(boolean pisoOSotanoBool) {
        this.pisoOSotanoBool = pisoOSotanoBool;
    }

    public Long getUnidadesTotales() {
        return unidadesTotales;
    }

    public void setUnidadesTotales(Long unidadesTotales) {
        this.unidadesTotales = unidadesTotales;
    }

    public Long getUnidadesRestantes() {
        return unidadesRestantes;
    }

    public void setUnidadesRestantes(Long unidadesRestantes) {
        this.unidadesRestantes = unidadesRestantes;
    }

    public Long getUnidadesAsignadas() {
        return unidadesAsignadas;
    }

    public void setUnidadesAsignadas(Long unidadesAsignadas) {
        this.unidadesAsignadas = unidadesAsignadas;
    }

    public boolean isSelectedAllActivarPrediosBool() {
        return selectedAllActivarPrediosBool;
    }

    public void setSelectedAllActivarPrediosBool(
        boolean selectedAllActivarPrediosBool) {
        this.selectedAllActivarPrediosBool = selectedAllActivarPrediosBool;
    }

    public void setUnicaUnidadBool(boolean unicaUnidadBool) {
        this.unicaUnidadBool = unicaUnidadBool;
    }

    public void setTorreCargadaBool(boolean torreCargadaBool) {
        this.torreCargadaBool = torreCargadaBool;
    }

    public boolean isTorreCargadaBool() {
        return torreCargadaBool;
    }

    public boolean isUnicaUnidadBool() {
        return unicaUnidadBool;
    }

    public Double getSumatoriaCoefCopropiedad() {
        return sumatoriaCoefCopropiedad;
    }

    public void setSumatoriaCoefCopropiedad(Double sumatoriaCoefCopropiedad) {
        this.sumatoriaCoefCopropiedad = sumatoriaCoefCopropiedad;
    }

    public List<SelectItem> getTiposCondicionEV() {
        return tiposCondicionEV;
    }

    public void setTiposCondicionEV(List<SelectItem> tiposCondicionEV) {
        this.tiposCondicionEV = tiposCondicionEV;
    }

    public boolean isBanderaCondominio() {
        return banderaCondominio;
    }

    public void setBanderaCondominio(boolean banderaCondominio) {
        this.banderaCondominio = banderaCondominio;
    }

    public boolean isBanderaPH() {
        return banderaPH;
    }

    public void setBanderaPH(boolean banderaPH) {
        this.banderaPH = banderaPH;
    }

    public String getCondicionPredioSeleccionada() {
        return condicionPredioSeleccionada;
    }

    public void setCondicionPredioSeleccionada(String condicionPredioSeleccionada) {
        this.condicionPredioSeleccionada = condicionPredioSeleccionada;
    }

    public EntradaProyeccionMB getEntradaProyecionMB() {
        return entradaProyecionMB;
    }

    public void setEntradaProyecionMB(EntradaProyeccionMB entradaProyecionMB) {
        this.entradaProyecionMB = entradaProyecionMB;
    }

    public boolean isMantieneFicha() {
        return mantieneFicha;
    }

    public void setMantieneFicha(boolean mantieneFicha) {
        this.mantieneFicha = mantieneFicha;
    }

    public boolean isPredioCondProp8() {
        return predioCondProp8;
    }

    public void setPredioCondProp8(boolean predioCondProp8) {
        this.predioCondProp8 = predioCondProp8;
    }

    public boolean isPredioCondProp9() {
        return predioCondProp9;
    }

    public void setPredioCondProp9(boolean predioCondProp9) {
        this.predioCondProp9 = predioCondProp9;
    }

    // =================== MÉTODOS =================//
    /**
     * Método validar si todos los predios selecconados se encuentra cancelados y de esta manera
     * habilitar la activación de predios
     *
     * @author david.cifuentes
     *
     * @return
     */
    public boolean isActivarPredios() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizPredios() != null) {
            if (this.tramite.isQuintaMasivo() && this.tramite.isQuintaOmitido()) {
                return true;
            }
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                    return true;
                }
            }
        }
        return false;
    }

    public long getTotalTorresFichaMatrizEtapas() {
        return this.getTorresExistentesFichaMatrizEtapas() + this.
            getTorresCanceladasFichaMatrizEtapas();
    }

    public long getTotalUndPrivadasFichaMatrizEtapas() {

        long totalUnidadesPrivadas = 0L;

        if (this.tramite.isEnglobeVirtual()) {
            totalUnidadesPrivadas = this.getUnidadesPrivadasExistentesFichaMatrizEtapas() + this.
                getUnidadesPrivadasCanceladasFichaMatrizEV();
        } else {
            totalUnidadesPrivadas = this.getUnidadesPrivadasExistentesFichaMatrizEtapas() + this.
                getUnidadesPrivadasCanceladasFichaMatrizEtapas();
        }
        return totalUnidadesPrivadas;
    }

    public long getTotalUndSotanosFichaMatrizEtapas() {

        long totalUnidadesSotanos = 0L;

        if (this.tramite.isEnglobeVirtual()) {
            totalUnidadesSotanos = this.getUnidadesSotanosExistentesFichaMatrizEtapas() + this.
                getUnidadesSotanosCanceladosFichaMatrizEV();
        } else {
            totalUnidadesSotanos = this.getUnidadesSotanosExistentesFichaMatrizEtapas() + this.
                getUnidadesSotanosCanceladosFichaMatrizEtapas();
        }
        return totalUnidadesSotanos;
    }

    public long getTorresCanceladasFichaMatrizEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizTorres() != null) {
            long torresCanceladas = 0;
            for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {
                if (this.tramite.isEnglobeVirtual()) {
                    if (EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado())) {
                        torresCanceladas++;
                    }
                } else {
                    if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmt.
                        getCancelaInscribe())) {
                        torresCanceladas++;
                    }
                }
            }
            return torresCanceladas;
        }
        return 0;
    }

    public long getUnidadesPrivadasCanceladasFichaMatrizEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizPredios() != null) {
            long undCanceladas = 0;
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (!pfmp.isSotano() &&
                     (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) {
                    undCanceladas++;
                }
            }
            return undCanceladas;
        }
        return 0;
    }

    public long getUnidadesPrivadasCanceladasFichaMatrizEV() {
        List<PFichaMatrizPredio> prediosFicha = new ArrayList<PFichaMatrizPredio>();

        if (this.fichaMatriz != null) {

            prediosFicha = this.getConservacionService().
                buscarFichaMatrizPredioPorPFichaMatrizId(this.fichaMatriz.getId());
        }

        if (prediosFicha != null && !prediosFicha.isEmpty()) {
            long undCanceladas = 0;
            for (PFichaMatrizPredio pfmp : prediosFicha) {
                if (!pfmp.isSotano() &&
                     (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) {
                    undCanceladas++;
                }
            }
            return undCanceladas;

        }
        return 0;
    }

    public long getUnidadesSotanosCanceladosFichaMatrizEV() {

        List<PFichaMatrizPredio> prediosFicha = new ArrayList<PFichaMatrizPredio>();

        if (this.fichaMatriz != null) {

            prediosFicha = this.getConservacionService().
                buscarFichaMatrizPredioPorPFichaMatrizId(this.fichaMatriz.getId());
        }

        if (prediosFicha != null && !prediosFicha.isEmpty()) {
            long sotanosCanceladas = 0;
            for (PFichaMatrizPredio pfmp : prediosFicha) {
                if (pfmp.isSotano() &&
                     (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) {
                    sotanosCanceladas++;
                }
            }
            return sotanosCanceladas;
        }
        return 0;
    }

    public long getUnidadesSotanosCanceladosFichaMatrizEtapas() {
        if (this.fichaMatriz != null) {
            long sotanosCanceladas = 0;
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (pfmp.isSotano() &&
                     (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) {
                    sotanosCanceladas++;
                }
            }
            return sotanosCanceladas;
        }
        return 0;
    }

    public long getTorresExistentesFichaMatrizEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizTorres() != null) {
            long torresExistentes = 0;
            for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {

                if (this.tramite.isEnglobeVirtual()) {

                    if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado())) {
                        torresExistentes++;
                    }

                } else {

                    if (ESiNo.SI.getCodigo().equals(pfmt.getOriginalTramite()) && (pfmt.
                        getCancelaInscribe() == null ||
                         EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pfmt.
                            getCancelaInscribe()) ||
                         EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pfmt.
                            getCancelaInscribe()))) {
                        torresExistentes++;
                    }
                }
            }
            return torresExistentes;
        }
        return 0;
    }

    public long getUnidadesPrivadasExistentesFichaMatrizEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizTorres() != null) {
            long undExistentes = 0;
            for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {
                if (tramite.isEnglobeVirtual()) {

                    if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado())) {
                        if (pfmt.getUnidades() != null) {
                            undExistentes = undExistentes + pfmt.getUnidades();
                        }
                    }

                } else if (ESiNo.SI.getCodigo().equals(pfmt.getOriginalTramite()) && (pfmt.
                    getCancelaInscribe() == null ||
                     EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pfmt.
                        getCancelaInscribe()) ||
                     EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pfmt.
                        getCancelaInscribe()))) {
                    if (pfmt.getUnidades() != null) {
                        undExistentes = undExistentes + pfmt.getUnidades();
                    }
                }
            }

            if (!tramite.isEnglobeVirtual()) {
                long canceladasSegunTorre = (validarExistenciaPrediosMigrados()) ? 0 : this.
                    getUnidadesPrivadasCanceladasFichaMatrizEtapas();
                return undExistentes - canceladasSegunTorre;
            } else {
                return undExistentes;
            }

        }
        return 0;
    }

    public long getUnidadesSotanosExistentesFichaMatrizEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizTorres() != null) {
            long undSotanosExistentes = 0;
            for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {

                if (tramite.isEnglobeVirtual()) {
                    if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado())) {

                        if (pfmt.getUnidadesSotanos() != null) {
                            undSotanosExistentes = undSotanosExistentes + pfmt.getUnidadesSotanos();
                        }
                    }
                } else if (ESiNo.SI.getCodigo().equals(pfmt.getOriginalTramite()) && (pfmt.
                    getCancelaInscribe() == null ||
                     EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pfmt.
                        getCancelaInscribe()) ||
                     EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pfmt.
                        getCancelaInscribe()))) {
                    if (pfmt.getUnidadesSotanos() != null) {
                        undSotanosExistentes = undSotanosExistentes + pfmt.getUnidadesSotanos();
                    }
                }
            }
            return undSotanosExistentes;
        }
        return 0;
    }

    /**
     * Son las unidades que vienen en firme, eceptuando las que han sido canceladas.
     *
     * @return
     */
    public long getUnidadesExistentesCondominioEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizPredios() != null) {
            long undExistentesCondominio = 0;
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if ((ESiNo.SI.getCodigo().equals(pfmp.getOriginalTramite()) &&
                     !(EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) &&
                     pfmp.getNumeroPredial().substring(21, 22).equals(
                        EPredioCondicionPropiedad.CP_8.getCodigo())) {
                    undExistentesCondominio++;
                }
            }
            return undExistentesCondominio;
        }
        return 0;
    }

    /**
     * Son las unidades que vienen en firme, eceptuando las que han sido canceladas.
     *
     * @return
     */
    public long getUnidadesExistentesCondominioEV() {

        if (this.fichaMatriz != null && (this.fichasMatrizCondominio != null ||
             !this.fichasMatrizCondominio.isEmpty())) {
            long undExistentesCondominio = 0;

            //Para los predios mixtos se verifica las unidades nuevas asociadas a la ficha matriz
            if (this.fichaMatriz.getPPredio().isMixto()) {

                if (this.fichaMatriz.getTotalUnidadesCondominio() != null) {

                    undExistentesCondominio = this.fichaMatriz.getTotalUnidadesCondominio();

                } else if (this.fichaMatriz.getTotalUnidadesPrivadas() != null) {

                    undExistentesCondominio = this.fichaMatriz.getTotalUnidadesPrivadas();
                }

            }

            if (!this.fichaMatriz.getPPredio().isMixto()) {
                for (PFichaMatriz pfm : this.fichasMatrizCondominio) {

                    if ((EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pfm.
                        getCancelaInscribe()) ||
                         EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pfm.
                            getCancelaInscribe())) &&
                         EPredioEstado.ACTIVO.getCodigo().equals(pfm.getEstado())) {

                        if (pfm.getTotalUnidadesCondominio() != null) {

                            undExistentesCondominio = pfm.getTotalUnidadesCondominio();

                        } else if (pfm.getTotalUnidadesPrivadas() != null) {

                            if (mantieneFicha && pfm.getTotalUnidadesNuevas() != null) {
                                undExistentesCondominio = pfm.getTotalUnidadesPrivadas() - pfm.
                                    getTotalUnidadesNuevas();
                            } else {
                                undExistentesCondominio = pfm.getTotalUnidadesPrivadas();
                            }

                        }

                    }

                }
            }

            return undExistentesCondominio;
        }
        return 0;
    }

    public long getTotalUnidadesCondominioEtapas() {
        return (Math.abs(this.getUnidadesExistentesCondominioEtapas() + this.
            getUnidadesEliminarCondominioEtapas())) + this.getUnidadesNuevasCondominioEtapas();
    }

    public long getTotalUnidadesCondominioEV() {

        return (Math.abs(this.getUnidadesExistentesCondominioEV() + this.
            getUnidadesEliminarCondominioEV())) + this.getUnidadesNuevasEV();

    }

    public long getUnidadesNuevasCondominioEtapas() {
        this.unidadesNuevasCondominioEtapas = 0;
        if (this.fichaMatriz != null && this.fichaMatriz.getTotalUnidadesNuevas() == null) {
            this.fichaMatriz.setTotalUnidadesNuevas(0L);
        }
        this.unidadesNuevasCondominioEtapas = this.fichaMatriz.getTotalUnidadesNuevas();
        return this.unidadesNuevasCondominioEtapas;
    }

    public long getUnidadesNuevasEV() {

        this.unidadesNuevasEV = 0;

        //Para los predios mixtos se verifica las unidades nuevas asociadas a la ficha matriz
        if (this.fichaMatriz.getPPredio().isMixto()) {

            if (this.fichaMatriz.getTotalUnidadesNuevas() == null) {
                this.fichaMatriz.setTotalUnidadesNuevas(0L);
            } else {
                this.unidadesNuevasEV = this.fichaMatriz.getTotalUnidadesNuevas();
            }

        }

        if (!this.fichaMatriz.getPPredio().isMixto()) {
            for (PFichaMatriz pfm : this.fichasMatrizCondominio) {

                if ((EProyeccionCancelaInscribe.MODIFICA.getCodigo().
                    equals(pfm.getCancelaInscribe()) ||
                     EProyeccionCancelaInscribe.INSCRIBE.getCodigo().
                        equals(pfm.getCancelaInscribe())) &&
                     EPredioEstado.ACTIVO.getCodigo().equals(pfm.getEstado())) {

                    if (pfm.getTotalUnidadesNuevas() != null) {

                        this.unidadesNuevasEV = pfm.getTotalUnidadesNuevas();
                    } else {
                        this.fichaMatriz.setTotalUnidadesNuevas(0L);
                    }

                }

            }
        }

        return this.unidadesNuevasEV;
    }

    public long getUnidadesEliminarCondominioEtapas() {
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizPredios() != null) {
            long undCanceladasCondominio = 0;
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.getCancelaInscribe()) ||
                     EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()) &&
                     pfmp.getNumeroPredial().substring(21, 22).equals(
                        EPredioCondicionPropiedad.CP_8.getCodigo())) {
                    undCanceladasCondominio++;
                }
            }
            return undCanceladasCondominio;
        }
        return 0;
    }

    public void setUnidadesNuevasCondominioEtapas(long unidadesNuevasCondominioEtapas) {
        this.fichaMatriz.setTotalUnidadesNuevas(unidadesNuevasCondominioEtapas);
        this.fichaMatriz.setTotalUnidadesPrivadas(this.getTotalUnidadesCondominioEtapas());
        this.unidadesNuevasCondominioEtapas = unidadesNuevasCondominioEtapas;
    }

    public long getUnidadesEliminarCondominioEV() {

        if (this.fichaMatriz != null && (this.fichasMatrizCondominio != null ||
             !this.fichasMatrizCondominio.isEmpty())) {
            long undCanceladasCondominio = 0;
            for (PFichaMatriz pfm : this.fichasMatrizCondominio) {

                if (EPredioEstado.CANCELADO.getCodigo().equals(pfm.getEstado())) {

                    if (pfm.getPFichaMatrizPredios() != null &&
                         !pfm.getPFichaMatrizPredios().isEmpty()) {

                        for (PFichaMatrizPredio pfmp : pfm.getPFichaMatrizPredios()) {

                            if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                                getCancelaInscribe()) &&
                                 EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {

                                if (undCanceladasCondominio == 0) {
                                    undCanceladasCondominio = pfm.getTotalUnidadesPrivadas();
                                } else if (undCanceladasCondominio != 0) {
                                    undCanceladasCondominio = undCanceladasCondominio + pfm.
                                        getTotalUnidadesPrivadas();
                                }

                            }

                        }

                    }

                }
            }
            return undCanceladasCondominio;
        }
        return 0;
    }

    public List<PFichaMatrizPredio> getFichaMatrizPredios() {

        List<PFichaMatrizPredio> pFichaMatrizPrediosTabla = new ArrayList<PFichaMatrizPredio>();

        if (this.fichaMatriz != null &&
             !this.tramite.isDesenglobeEtapas() && !this.tramite.isQuintaMasivo()) {

            return this.fichaMatriz.getPFichaMatrizPredios();

        } else {

            if (this.fichaMatriz != null &&
                 this.fichaMatriz.getPFichaMatrizPredios() != null) {

                for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {

                    if (this.tramite.isEnglobeVirtual()) {

                        if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                            pFichaMatrizPrediosTabla.add(pfmp);
                        }

                    } else if (!(EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe()) ||
                         EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) {
                        pFichaMatrizPrediosTabla.add(pfmp);
                    }
                }
                if (this.tramite.isEnglobeVirtual()) {
                    this.fichaMatriz.getPFichaMatrizPredios().clear();
                    this.fichaMatriz.getPFichaMatrizPredios().addAll(pFichaMatrizPrediosTabla);
                }
            }
            if (this.fichaMatriz != null &&
                 this.fichaMatriz.getPFichaMatrizPredios() != null) {

                return this.fichaMatriz.getPFichaMatrizPredios();
            } else {
                return pFichaMatrizPrediosTabla;
            }

        }

    }

    /**
     * Método que se llama en la ventana que modifica los datos de la torre. Se encarga de almacenar
     * los datos de la torre seleccionada
     *
     * @author fabio.navarrete
     */
    public void aceptarTorre() {
        try {

            if (validarUnidadesFichaMatriz(false)) {
                //Validacion para modificar Predios Resultantes cuando se modifica una torre en firme

                // Validar que la torre no se encuentre cancelada
                if (this.tramite.isDesenglobeEtapas()) {
                    if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(
                        this.fichaMatrizTorreSeleccionada.getCancelaInscribe())) {
                        this.addMensajeError(
                            "La torre se encuentra cancelada debido a que se cancelaron los predios y ésta se encontraba en firme, para modificarla debe primero reactivar los predios cancelados.");
                        this.reversarSeleccionTorre();
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    }
                }

                // Validación de cambio de valores
                boolean cambioValores = false;
                if (this.fichaMatrizTorreReplica == null) {
                    cambioValores = true;
                } else if (!this.tramite.isEnglobeVirtual() &&
                     this.fichaMatrizTorreReplica != null &&
                     (Long.valueOf(this.fichaMatrizTorreSeleccionada.getPisos()) != Long
                    .valueOf(this.fichaMatrizTorreReplica.getPisos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getSotanos()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getSotanos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getUnidades()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getUnidades()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getUnidadesSotanos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getTorre()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getTorre()))) {

                    cambioValores = true;

                } else if (this.tramite.isEnglobeVirtual() &&
                     this.fichaMatrizTorreReplica != null &&
                     (Long.valueOf(this.fichaMatrizTorreSeleccionada.getPisos()) != Long
                    .valueOf(this.fichaMatrizTorreReplica.getPisos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getSotanos()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getSotanos()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getUnidades()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getUnidades()) ||
                     Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos()) != Long
                        .valueOf(this.fichaMatrizTorreReplica
                            .getUnidadesSotanos()))) {

                    cambioValores = true;

                } else if (this.tramite.isEnglobeVirtual() &&
                     this.fichaMatrizTorreSeleccionada.getTorre() != null &&
                     this.fichaMatrizTorreReplica.getTorre() != null &&
                     (Long.valueOf(this.fichaMatrizTorreSeleccionada
                        .getTorre()) != Long.valueOf(this.fichaMatrizTorreReplica.getTorre()) ||
                     editModeTorre)) {

                    cambioValores = true;

                } else if (this.tramite.isEnglobeVirtual() &&
                     this.fichaMatrizTorreSeleccionada.getProvieneTorre() != null &&
                     this.fichaMatrizTorreSeleccionada.getProvieneTorre().getTorre() != null &&
                     this.fichaMatrizTorreReplica.getProvieneTorre().getTorre() != null &&
                    
                    (Long.valueOf(this.fichaMatrizTorreSeleccionada.getProvieneTorre().getTorre()) !=
                     Long.valueOf(this.fichaMatrizTorreReplica.getProvieneTorre().getTorre()) ||
                     editModeTorre)) {
                    cambioValores = true;
                }

                if (cambioValores) {

                    // Validar la repetición del número de la torre
                    if (this.tramite.isDesenglobeEtapas()) {
                        PFichaMatrizTorre torreCreada = null;
                        int countTorres = 0;
                        for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {
                            if (pfmt.getTorre() != null &&
                                 this.fichaMatrizTorreSeleccionada.getTorre() != null &&
                                 pfmt.getTorre().longValue() == this.fichaMatrizTorreSeleccionada.
                                getTorre().longValue()) {
                                torreCreada = pfmt;
                                countTorres++;
                            }
                            if (this.tramite.isEnglobeVirtual() &&
                                 pfmt.getProvieneTorre() != null &&
                                 this.fichaMatrizTorreSeleccionada.getTorre() != null &&
                                 EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado()) &&
                                 pfmt.getProvieneTorre().getTorre().longValue() ==
                                 this.fichaMatrizTorreSeleccionada.getTorre().longValue()) {
                                torreCreada = pfmt;
                                countTorres++;
                            }
                        }
                        if (countTorres == 1) {
                            if (torreCreada != null) {
                                boolean existeNumeroTorreBool = false;
                                if (this.fichaMatrizTorreSeleccionada.getId() == null) {
                                    existeNumeroTorreBool = true;
                                } else if (torreCreada.getId().longValue() !=
                                    this.fichaMatrizTorreReplica.getId().longValue()) {
                                    existeNumeroTorreBool = true;
                                }
                                if (existeNumeroTorreBool) {
                                    if (this.tramite.isEnglobeVirtual()) {

                                        if (fichaMatrizTorreReplica != null) {
                                            this.fichaMatrizTorreSeleccionada =
                                                this.fichaMatrizTorreReplica;
                                        }
                                        this.addMensajeError(
                                            "La torre asignada ya se encuentra asignada en la ficha matriz.");
                                        this.addErrorCallback();

                                        return;
                                    } else {
                                        this.addMensajeError(
                                            "El número de torre ya se encuentra registrado en el PH, por favor modifique o elimine la torre creada, o asigne un número diferente para la nueva torre.");
                                        RequestContext context = RequestContext.getCurrentInstance();
                                        context.addCallbackParam("error", "error");
                                        return;
                                    }

                                }
                            }
                        } else if (countTorres > 1) {
                            if (this.tramite.isEnglobeVirtual()) {
                                if (fichaMatrizTorreReplica != null) {
                                    this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                                }

                                this.addMensajeError(
                                    "La torre asignada ya se encuentra asignada en la ficha matriz.");
                                RequestContext context = RequestContext.getCurrentInstance();
                                context.addCallbackParam("error", "error");
                                return;
                            } else {
                                this.addMensajeError(
                                    "El número de torre ya se encuentra registrado en el PH, por favor modifique o elimine la torre creada, o asigne un número diferente para la nueva torre.");
                                RequestContext context = RequestContext.getCurrentInstance();
                                context.addCallbackParam("error", "error");
                                return;
                            }
                        }
                    }

                    if (this.tramite.isDesenglobeEtapas() || this.tramite.isQuintaMasivo()) {
                        if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.
                            getOriginalTramite())) {
                            FichaMatrizTorre fmtFirme = this.buscarTorreEnFirme(
                                this.fichaMatrizTorreSeleccionada);
                            if (fmtFirme != null &&
                                 (Long.valueOf(fmtFirme.getPisos()).longValue() > Long.valueOf(
                                this.fichaMatrizTorreSeleccionada.getPisos()).longValue() ||
                                 Long.valueOf(fmtFirme.getSotanos()).longValue() > Long.valueOf(
                                this.fichaMatrizTorreSeleccionada.getSotanos()).longValue() ||
                                 Long.valueOf(fmtFirme.getUnidades()).longValue() > Long.valueOf(
                                this.fichaMatrizTorreSeleccionada.getUnidades()).longValue() ||
                                 Long.valueOf(fmtFirme.getUnidadesSotanos()).longValue() > Long.
                                valueOf(this.fichaMatrizTorreSeleccionada.getUnidadesSotanos()).
                                longValue())) {

                                if (this.tramite.isEnglobeVirtual()) {

                                    this.addMensajeError(
                                        "El número de pisos debe ser mayor al total.");
                                    this.addMensajeError(
                                        "El número de unidades debe ser mayor al total de unidades (existentes y canceladas)â. .");
                                    this.reversarSeleccionTorre();
                                    RequestContext context = RequestContext.getCurrentInstance();
                                    context.addCallbackParam("error", "error");
                                    return;

                                } else {
                                    this.addMensajeError(
                                        "Debido a que la torre se encuentra en firme, sólo se pueden ingresar valores mayores a los que se encuentran registrados.");
                                    this.reversarSeleccionTorre();
                                    RequestContext context = RequestContext.getCurrentInstance();
                                    context.addCallbackParam("error", "error");
                                    return;
                                }
                            }

                        }
                    }

                    //Para los tramites de Englobe Virtual y que el predio de la ficha matriz a 
                    //modificar los predios sea un mixto
                    if (this.tramite.isEnglobeVirtual()) {

                        if (ESiNo.SI.getCodigo().equals(
                            this.fichaMatrizTorreSeleccionada.getOriginalTramite())) {

                            String torrePredio = new String();
                            String predAntesPredio = new String();
                            String predDespuesTorre = new String();
                            String predTorreCompl = new String();
                            String nuevoNumeroPredial = new String();
                            String numeroPredoFichaMatriz = new String();
                            String condPropPredio = new String();

                            //Consultar Torre Predios de la torre y actualizarlos
                            List<PFichaMatrizTorre> pFichaMatrizTorres = this.
                                getConservacionService().
                                obtenerFichaMatrizTorrePorFichaId(this.fichaMatriz.getId());

                            if (this.fichaMatriz.getPPredio() != null &&
                                 this.fichaMatriz.getPPredio().getNumeroPredial() != null &&
                                 !this.fichaMatriz.getPPredio().getNumeroPredial().isEmpty()) {

                                numeroPredoFichaMatriz = this.fichaMatriz.getPPredio().
                                    getNumeroPredial().substring(17, 21);

                            }

                            if (pFichaMatrizTorres != null &&
                                 !pFichaMatrizTorres.isEmpty()) {
                                //Se verifica dentro de la lista de torres la torre seleccionada
                                for (PFichaMatrizTorre pfmt : pFichaMatrizTorres) {
                                    if (this.fichaMatrizTorreSeleccionada != null &&
                                         this.fichaMatrizTorreSeleccionada.getProvieneTorre() !=
                                        null &&
                                         pfmt.getProvieneTorre() != null &&
                                         pfmt.getProvieneTorre().getId().longValue() ==
                                         this.fichaMatrizTorreSeleccionada.getProvieneTorre().
                                            getId().longValue() &&
                                         pfmt.getProvieneFichaMatriz() != null &&
                                         this.fichaMatrizTorreSeleccionada.getProvieneFichaMatriz().
                                            getId() != null &&
                                         pfmt.getProvieneFichaMatriz().getId() != null &&
                                         this.fichaMatrizTorreSeleccionada.getProvieneFichaMatriz().
                                            getId().
                                            equals(pfmt.getProvieneFichaMatriz().getId())) {

                                        if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                                             !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                                            for (PFichaMatrizPredio pfmp : this.fichaMatriz.
                                                getPFichaMatrizPredios()) {

                                                if (pfmp.getNumeroPredial() != null &&
                                                     !pfmp.getNumeroPredial().isEmpty() &&
                                                     !EPredioEstado.CANCELADO.getCodigo().equals(
                                                        pfmp.getEstado())) {

                                                    String torreMigrada = new String();
                                                    String pisoMigrado = new String();

                                                    if (pfmp.getNumeroPredialOriginal() != null &&
                                                         !pfmp.getNumeroPredialOriginal().isEmpty()) {

                                                        torrePredio = pfmp.
                                                            getNumeroPredialOriginal().substring(23,
                                                                24);
                                                        torreMigrada = pfmp.
                                                            getNumeroPredialOriginal().substring(22,
                                                                24);
                                                        pisoMigrado = pfmp.
                                                            getNumeroPredialOriginal().substring(24,
                                                                26);
                                                    }

                                                    //Se verifica si el predio a modificar pertenece a la misma torre que se selecciono
                                                    //Y si pertenecen a la misma ficha matriz
                                                    //Comparando el # de torre y si no es un predio migrado
                                                    if (torrePredio != null && !torrePredio.
                                                        isEmpty() &&
                                                         pfmt.getProvieneTorre() != null &&
                                                         pfmt.getProvieneTorre().getTorre().
                                                            toString().
                                                            equals(torrePredio) &&
                                                         !("00".equals(pisoMigrado) && "00".equals(
                                                        torreMigrada)) &&
                                                         pfmp.getNumeroPredialOriginal() != null &&
                                                         !pfmp.getNumeroPredialOriginal().isEmpty() &&
                                                         pfmp.getProvieneFichaMatriz() != null &&
                                                         pfmp.getProvieneFichaMatriz().getId() !=
                                                        null &&
                                                         pfmt.getProvieneFichaMatriz().getId().
                                                            equals(pfmp.getProvieneFichaMatriz().
                                                                getId())) {

                                                        predAntesPredio = pfmp.
                                                            getNumeroPredialOriginal().substring(0,
                                                                17);

                                                        if (this.fichaMatrizTorreSeleccionada.
                                                            getTorre().toString().length() == 1) {

                                                            predTorreCompl = Constantes.CERO + Long.
                                                                toString(
                                                                    this.fichaMatrizTorreSeleccionada.
                                                                        getTorre());
                                                        } else {

                                                            predTorreCompl = Long.toString(
                                                                this.fichaMatrizTorreSeleccionada.
                                                                    getTorre());
                                                        }

                                                        condPropPredio = pfmp.
                                                            getNumeroPredialOriginal().substring(21,
                                                                22);
                                                        predDespuesTorre = pfmp.
                                                            getNumeroPredialOriginal().substring(24,
                                                                30);

                                                        nuevoNumeroPredial =
                                                             predAntesPredio +
                                                            numeroPredoFichaMatriz + condPropPredio +
                                                            predTorreCompl + predDespuesTorre;

                                                        if (this.proyectarConservacionMB.
                                                            getpPrediosDesenglobe() != null) {

                                                            for (PPredio pPredioTemp
                                                                : this.proyectarConservacionMB.
                                                                    getpPrediosDesenglobe()) {

                                                                if (pPredioTemp.getNumeroPredial() !=
                                                                    null &&
                                                                    
                                                                    !pPredioTemp.getNumeroPredial().
                                                                        isEmpty() &&
                                                                     pfmp.getNumeroPredial().equals(
                                                                        pPredioTemp.
                                                                            getNumeroPredial())) {

                                                                    pPredioTemp.setEdificio(
                                                                        predTorreCompl);
                                                                    pPredioTemp.setNumeroPredial(
                                                                        nuevoNumeroPredial);
                                                                    pPredioTemp.setPredio(
                                                                        numeroPredoFichaMatriz);
                                                                    this.getConservacionService().
                                                                        actualizarPPredio(
                                                                            pPredioTemp);
                                                                    break;
                                                                }

                                                            }
                                                        }

                                                        pfmp.setNumeroPredial(nuevoNumeroPredial);
                                                        this.getConservacionService().
                                                            actualizarListaPFichaMatrizPredio(pfmp);

                                                    }

                                                }
                                            }

                                        }

                                    }
                                }

                            }

                        }

                    }

                    if (editModeTorre) {

                        if (this.tramite.isEnglobeVirtual()) {

                            editarTorresNuevasEV();

                        }

                        this.fichaMatriz.getPFichaMatrizTorres().remove(
                            this.fichaMatrizTorreSeleccionada);

                        this.fichaMatriz.getPFichaMatrizTorres().add(
                            this.fichaMatrizTorreSeleccionada);

                    } else {
                        if (ESiNo.SI.getCodigo().equals(
                            this.fichaMatrizTorreSeleccionada.getOriginalTramite())) {
                            this.fichaMatrizTorreSeleccionada
                                .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo());
                        } else {
                            this.fichaMatrizTorreSeleccionada
                                .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                    .getCodigo());
                        }

                        this.fichaMatrizTorreSeleccionada
                            .setEstado(EPredioEstado.ACTIVO
                                .getCodigo());
                        this.fichaMatrizTorreSeleccionada.setUsuarioLog(MenuMB
                            .getMenu().getUsuarioDto().getLogin());
                        this.fichaMatrizTorreSeleccionada.setFechaLog(new Date());
                        this.fichaMatrizTorreSeleccionada
                            .setPFichaMatriz(this.fichaMatriz);

                        if (tramite.isEnglobeVirtual()) {
                            this.fichaMatrizTorreSeleccionada.setOriginalTramite(ESiNo.NO.
                                getCodigo());
                        }

                        this.fichaMatrizTorreSeleccionada = this
                            .getConservacionService()
                            .guardarActualizarPFichaMatrizTorre(
                                this.fichaMatrizTorreSeleccionada);
                        if (!this.fichaMatriz.getPFichaMatrizTorres().contains(
                            this.fichaMatrizTorreSeleccionada)) {
                            this.fichaMatriz.getPFichaMatrizTorres().add(
                                fichaMatrizTorreSeleccionada);
                        }
                    }

                    // Suma de unidades privadas y unidades de sotano de las torres
                    // de la ficha matriz
                    Long totalUnidadesPrivadas = 0L;
                    Long totalUnidadesSotanos = 0L;
                    for (PFichaMatrizTorre pft : this.fichaMatriz
                        .getPFichaMatrizTorres()) {
                        totalUnidadesPrivadas += pft.getUnidades();
                        totalUnidadesSotanos += pft.getUnidadesSotanos();
                    }
                    this.fichaMatriz
                        .setTotalUnidadesPrivadas(totalUnidadesPrivadas);
                    this.fichaMatriz.setTotalUnidadesSotanos(totalUnidadesSotanos);

                    // Guardar la ficha matriz
                    this.guardarFichaMatriz();

                    // Inicialización de variables para la creación de los
                    // números prediales
                    this.unidadesTotales = 0L;
                    this.unidadesAsignadas = 0L;
                    this.unidadesRestantes = 0L;
                    this.conteoUnidad = 0L;
                    this.cargarMatrizNumerosPrediales();
                    this.fichaMatrizTorreReplica = null;
                    this.fichaMatrizTorreSeleccionada = null;
                    if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                         !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                        Collections.sort(this.fichaMatriz.getPFichaMatrizTorres());
                    }

                    if (this.tramite.isEnglobeVirtual()) {

                        getFichaMatrizPredios();

                    }

                    this.addMensajeInfo("Datos de la torre almacenados satisfactoriamente");
                }
            } else {
                this.reversarSeleccionTorre();
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error("Error al almacenar los datos de la torre");
        }
    }

    /**
     * Método que realiza la búsqueda de la FichaMatrizTorre en firme dada una PFichaMatrizTorre
     *
     * @author david.cifuentes
     * @param pFichaMatrizTorre
     * @return
     */
    public FichaMatrizTorre buscarTorreEnFirme(PFichaMatrizTorre pFichaMatrizTorre) {
        FichaMatriz fichaMatrizOriginal = this.getConservacionService().
            getFichaMatrizByNumeroPredialPredio(this.fichaMatriz.getPPredio().getNumeroPredial());
        if (fichaMatrizOriginal != null && fichaMatrizOriginal.getFichaMatrizTorres() != null) {
            for (FichaMatrizTorre fmt : fichaMatrizOriginal.getFichaMatrizTorres()) {
                if (fmt.getTorre() != null && pFichaMatrizTorre.getTorre() != null &&
                     fmt.getTorre().longValue() == pFichaMatrizTorre.getTorre().longValue()) {
                    return fmt;
                }
            }
        }
        return null;
    }

    /**
     * Método que edita la FichaMatrizTorre con sus FichaMatrizPredios, dada una PFichaMatrizTorre
     *
     * @author leidy.gonzalez
     * @return
     */
    public void editarTorresNuevasEV() {

        if (ESiNo.NO.getCodigo().equals(
            this.fichaMatrizTorreSeleccionada.getOriginalTramite()) ||
             this.fichaMatrizTorreSeleccionada.getOriginalTramite() == null) {

            String torrePredio = new String();
            String predAntesPredio = new String();
            String predDespuesTorre = new String();
            String predTorreCompl = new String();
            String nuevoNumeroPredial = new String();
            String numeroPredoFichaMatriz = new String();
            String condPropPredio = new String();

            //Consultar Torre Predios de la torre y actualizarlos
            List<PFichaMatrizTorre> pFichaMatrizTorres = this.getConservacionService().
                obtenerFichaMatrizTorrePorFichaId(this.fichaMatriz.getId());

            if (this.fichaMatriz.getPPredio() != null &&
                 this.fichaMatriz.getPPredio().getNumeroPredial() != null &&
                 !this.fichaMatriz.getPPredio().getNumeroPredial().isEmpty()) {

                numeroPredoFichaMatriz = this.fichaMatriz.getPPredio().
                    getNumeroPredial().substring(17, 21);

            }

            if (pFichaMatrizTorres != null &&
                 !pFichaMatrizTorres.isEmpty()) {
                //Se verifica dentro de la lista de torres la torre seleccionada
                for (PFichaMatrizTorre pfmt : pFichaMatrizTorres) {
                    if (pfmt.getTorre() != null &&
                         this.fichaMatrizTorreReplica != null &&
                         this.fichaMatrizTorreReplica.getTorre() != null &&
                         pfmt.getTorre().longValue() == this.fichaMatrizTorreReplica.getTorre().
                        longValue()) {

                        if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                             !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {

                                if (pfmp.getNumeroPredial() != null &&
                                     !pfmp.getNumeroPredial().isEmpty() &&
                                     !EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {

                                    torrePredio = pfmp.getNumeroPredial().substring(23, 24);

                                    //Se verifica si el predio a modificar pertenece a la misma torre que se selecciono
                                    //Y si pertenecen a la misma ficha matriz
                                    //Comparando el # de torre o si es migrado
                                    if (torrePredio != null && !torrePredio.isEmpty() &&
                                         (pfmt.getTorre().toString().equals(torrePredio))) {

                                        predAntesPredio = pfmp.getNumeroPredial().substring(0, 17);

                                        if (this.fichaMatrizTorreSeleccionada.getTorre().toString().
                                            length() == 1) {

                                            predTorreCompl = Constantes.CERO + Long.toString(
                                                this.fichaMatrizTorreSeleccionada.getTorre());
                                        } else {

                                            predTorreCompl = Long.toString(
                                                this.fichaMatrizTorreSeleccionada.getTorre());
                                        }

                                        condPropPredio = pfmp.getNumeroPredial().substring(21, 22);
                                        predDespuesTorre = pfmp.getNumeroPredial().substring(24, 30);

                                        nuevoNumeroPredial =
                                             predAntesPredio + numeroPredoFichaMatriz +
                                            condPropPredio + predTorreCompl + predDespuesTorre;

                                        if (this.proyectarConservacionMB.getpPrediosDesenglobe() !=
                                            null) {

                                            for (PPredio pPredioTemp : this.proyectarConservacionMB.
                                                getpPrediosDesenglobe()) {

                                                if (pPredioTemp.getNumeroPredial() != null &&
                                                     !pPredioTemp.getNumeroPredial().isEmpty() &&
                                                     pfmp.getNumeroPredial().equals(pPredioTemp.
                                                        getNumeroPredial())) {

                                                    pPredioTemp.setEdificio(predTorreCompl);
                                                    pPredioTemp.setNumeroPredial(nuevoNumeroPredial);
                                                    pPredioTemp.setPredio(numeroPredoFichaMatriz);
                                                    this.getConservacionService().actualizarPPredio(
                                                        pPredioTemp);
                                                }

                                            }
                                        }

                                        pfmp.setNumeroPredial(nuevoNumeroPredial);
                                        this.getConservacionService().
                                            actualizarListaPFichaMatrizPredio(pfmp);

                                    }

                                }
                            }

                        }

                    }
                }

            }

        }

    }

    /**
     * Método que reversa la selección de una torre a la que se han cambiado los valores de
     * unidades, pisos o sótanos
     *
     * @author david.cifuentes
     */
    private void reversarSeleccionTorre() {
        if (this.fichaMatrizTorreReplica != null) {
            if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                 !this.fichaMatriz.getPFichaMatrizTorres()
                    .isEmpty()) {
                PFichaMatrizTorre pfmtAuxRemove = new PFichaMatrizTorre();
                for (PFichaMatrizTorre pfmt : this.fichaMatriz
                    .getPFichaMatrizTorres()) {
                    if (pfmt.getId().longValue() == this.fichaMatrizTorreReplica
                        .getId().longValue()) {
                        pfmtAuxRemove = pfmt;
                    }
                }
                this.fichaMatriz.getPFichaMatrizTorres().remove(
                    pfmtAuxRemove);
                this.fichaMatriz.getPFichaMatrizTorres().add(
                    this.fichaMatrizTorreReplica);
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que se encarga de hacer una replica de la torre seleccionada.
     *
     * @author david.cifuentes
     */
    public void setupTorreReplica() {

        if (this.fichaMatrizTorreSeleccionada != null &&
             this.fichaMatrizTorreSeleccionada.getId() != null) {
            this.fichaMatrizTorreReplica = (PFichaMatrizTorre) this.fichaMatrizTorreSeleccionada
                .clone();

            PFichaMatrizTorre pFichaMatrizTorreEd =
                (PFichaMatrizTorre) this.fichaMatrizTorreSeleccionada
                    .clone();
            this.fichaMatrizTorreSeleccionada = pFichaMatrizTorreEd;

        }

        if (this.tramite.isEnglobeVirtual()) {
            this.numeroTorreInicial = 0;
            if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite()) &&
                 this.fichaMatrizTorreSeleccionada.getProvieneFichaMatriz() != null) {
                this.fichaMatrizOriginalTorreSeleccionada = this.getConservacionService().
                    obtenerFichaMatrizPorId(this.fichaMatrizTorreSeleccionada.
                        getProvieneFichaMatriz().getId());
            }
        }
        this.setEditModeTorre(true);
    }

    /**
     * Método que se encarga de hacer una replica de la ficha matriz
     *
     * @author david.cifuentes
     */
    public void setupFichaMatrizReplica() {
        if (this.fichaMatriz != null &&
             this.fichaMatriz.getId() != null) {
            this.fichaMatrizReplica = (PFichaMatriz) this.fichaMatriz.clone();
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Se encarga de guardar la ficha matriz que se encuentra en edición
     *
     * @author fabio.navarrete
     */
    public void guardarFichaMatriz() {
        try {

            this.fichaMatriz.setFechaLog(new Date());
            this.fichaMatriz.setUsuarioLog(MenuMB.getMenu().getUsuarioDto()
                .getLogin());
            this.fichaMatriz = this.getConservacionService()
                .guardarActualizarPFichaMatriz(this.fichaMatriz);

            if (tramite.isEnglobeVirtual()) {
                this.fichaMatriz = this.getConservacionService()
                    .buscarPFichaMatrizPorPredioId(this.getProyectarConservacionMB().
                        getPredioSeleccionado().getId());
            }

            this.verificarTorresEnFirme();

            this.proyectarConservacionMB
                .setFichaMatrizSeleccionada(this.fichaMatriz);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Método que realiza el set de las torres que están en firme en la ficha matriz.
     *
     * @author david.cifuentes
     */
    private void verificarTorresEnFirme() {
        if (this.tramite.isDesenglobeEtapas() || this.tramite.isQuintaMasivo()) {
            // Set de torres que se encuentran en firme
            if (fichaMatriz != null &&
                 fichaMatriz.getPFichaMatrizTorres() != null &&
                 !fichaMatriz.getPFichaMatrizTorres().isEmpty()) {

                for (PFichaMatrizTorre pfmt : fichaMatriz
                    .getPFichaMatrizTorres()) {

                    if (ESiNo.SI.getCodigo().equals(pfmt.getOriginalTramite()) &&
                         !(EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmt.
                            getCancelaInscribe()) ||
                         EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado()))) {
                        pfmt.setEstadoEtapas(EEstadoPFichaMatrizTorreEtapas.EN_FIRME.getEstado());
                    } else if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmt.
                        getCancelaInscribe()) ||
                         EPredioEstado.CANCELADO.getCodigo().equals(pfmt.getEstado())) {
                        pfmt.setEstadoEtapas(EEstadoPFichaMatrizTorreEtapas.CANCELADA.getEstado());
                    } else {
                        pfmt.setEstadoEtapas(EEstadoPFichaMatrizTorreEtapas.NUEVA.getEstado());
                    }
                }
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que se encarga de multiplicar una torre
     *
     * @author fabio.navarrete
     */
    public void multiplicarTorre() {
        try {

            if (this.tramite.isEnglobeVirtual()) {

                this.multiplicarTorreEV();

            } else {
                if (numeroTorresMultiplicar != null && numeroTorresMultiplicar > 0) {
                    List<PFichaMatrizTorre> PFichaMatrizTorres = new ArrayList<PFichaMatrizTorre>();

                    Long numTorre = Long.valueOf(0);
                    for (PFichaMatrizTorre pfmt : this.fichaMatriz
                        .getPFichaMatrizTorres()) {
                        if (pfmt.getTorre() != null &&
                             pfmt.getTorre().compareTo(numTorre) > 0) {
                            numTorre = pfmt.getTorre();
                        }
                    }

                    for (int i = 0; i < this.numeroTorresMultiplicar; i++) {
                        numTorre++;
                        PFichaMatrizTorre pfmt = new PFichaMatrizTorre(
                            this.fichaMatrizTorreSeleccionada);
                        pfmt.setId(null);
                        pfmt.setTorre(numTorre);
                        pfmt.setUsuarioLog(MenuMB.getMenu().getUsuarioDto()
                            .getLogin());
                        pfmt.setFechaLog(new Date());
                        pfmt.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                        PFichaMatrizTorres.add(pfmt);
                    }
                    this.fichaMatriz.setTotalUnidadesPrivadas(this.fichaMatriz
                        .getTotalUnidadesPrivadas() +
                         this.fichaMatrizTorreSeleccionada.getUnidades() *
                         this.numeroTorresMultiplicar);

                    this.fichaMatriz.setTotalUnidadesSotanos(this.fichaMatriz
                        .getTotalUnidadesSotanos() +
                         this.fichaMatrizTorreSeleccionada
                            .getUnidadesSotanos() *
                         this.numeroTorresMultiplicar);

                    PFichaMatrizTorres = this
                        .getConservacionService()
                        .guardarActualizarPFichaMatrizTorres(PFichaMatrizTorres);
                    this.fichaMatriz.getPFichaMatrizTorres().addAll(
                        PFichaMatrizTorres);
                    this.guardarFichaMatriz();
                    this.addMensajeInfo("Torre multiplicada satisfactoriamente");

                    // Inicialización de variables para la creación de los
                    // números prediales
                    this.unidadesTotales = 0L;
                    this.unidadesAsignadas = 0L;
                    this.unidadesRestantes = 0L;
                    this.conteoUnidad = 0L;
                    this.cargarMatrizNumerosPrediales();
                } else {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError("Por favor ingrese la cantidad de torres a multiplicar.");
                }
            }
            if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                 !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                Collections.sort(this.fichaMatriz.getPFichaMatrizTorres());
            }

        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al multiplicar la torre");

        }
    }

    /**
     * Método que se encarga de multiplicar una torre para un tramite de Englobe Virtual
     *
     * @author leidy.gonzalez
     */
    public void multiplicarTorreEV() {

        if (numeroTorreInicial <= 0) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("La torre inicial no puede tener el valor cero");
            return;
        }

        if (numeroTorresMultiplicar != null && numeroTorresMultiplicar > 0 &&
             numeroTorreInicial > 0) {

            List<PFichaMatrizTorre> PFichaMatrizTorres = new ArrayList<PFichaMatrizTorre>();
            long numeroTorreInicialCopia = numeroTorreInicial;

            //Se verifica si de acuerdo al numero de torre inicial ingresado ya existe una
            //torre creada
            for (int i = 0; i < this.numeroTorresMultiplicar; i++) {

                if (i != 0) {
                    numeroTorreInicial++;
                }

                for (PFichaMatrizTorre pfmt : this.fichaMatriz
                    .getPFichaMatrizTorres()) {

                    if (pfmt.getTorre() != null &&
                         pfmt.getTorre().equals(numeroTorreInicial)) {
                        numeroTorreInicial = numeroTorreInicialCopia;
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        this.addMensajeError("La torre  ya se encuentra asignada, verifique");
                        return;
                    } else if (pfmt.getProvieneTorre() != null &&
                         pfmt.getProvieneTorre().getTorre().equals(numeroTorreInicial)) {
                        numeroTorreInicial = numeroTorreInicialCopia;
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        this.addMensajeError("La torre  ya se encuentra asignada, verifique");
                        return;
                    }
                }
            }

            if (numeroTorresMultiplicar == null) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Por favor ingrese la cantidad de torres a multiplicar.");
                return;
            }

            if (numeroTorresMultiplicar <= 0) {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "La cantidad de torres a multiplicar no puede tener el valor cero.");
                return;
            }

            numeroTorreInicial = numeroTorreInicialCopia;
            //Se crean las nuevas torres
            for (int i = 0; i < this.numeroTorresMultiplicar; i++) {

                if (i != 0) {
                    numeroTorreInicial++;
                }
                PFichaMatrizTorre pfmt = new PFichaMatrizTorre(
                    this.fichaMatrizTorreSeleccionada);
                pfmt.setId(null);
                pfmt.setTorre(numeroTorreInicial);
                pfmt.setUsuarioLog(MenuMB.getMenu().getUsuarioDto()
                    .getLogin());
                pfmt.setFechaLog(new Date());
                pfmt.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                PFichaMatrizTorres.add(pfmt);
            }
            this.fichaMatriz.setTotalUnidadesPrivadas(this.fichaMatriz
                .getTotalUnidadesPrivadas() +
                 this.fichaMatrizTorreSeleccionada.getUnidades() *
                 this.numeroTorresMultiplicar);

            this.fichaMatriz.setTotalUnidadesSotanos(this.fichaMatriz
                .getTotalUnidadesSotanos() +
                 this.fichaMatrizTorreSeleccionada
                    .getUnidadesSotanos() *
                 this.numeroTorresMultiplicar);

            PFichaMatrizTorres = this
                .getConservacionService()
                .guardarActualizarPFichaMatrizTorres(PFichaMatrizTorres);
            this.fichaMatriz.getPFichaMatrizTorres().addAll(
                PFichaMatrizTorres);
            this.guardarFichaMatriz();
            this.addMensajeInfo("Torre multiplicada satisfactoriamente");

            // Inicialización de variables para la creación de los
            // números prediales
            this.unidadesTotales = 0L;
            this.unidadesAsignadas = 0L;
            this.unidadesRestantes = 0L;
            this.conteoUnidad = 0L;
            numeroTorresMultiplicar = 0;
            numeroTorreInicial = 0;
            this.cargarMatrizNumerosPrediales();

            if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                 !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                Collections.sort(this.fichaMatriz.getPFichaMatrizTorres());
            }
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Por favor ingrese la cantidad de torres a multiplicar.");
        }

    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones convecionales vigentes a las nuevas construcciones.
     */
    public void asociarConstruccionesConvencionalesVigentes() {
        if (unidadConstruccionConvencionalNuevas == null) {
            unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        }
        if (this.tramite.isTerceraMasiva()) {
            this.asociarConstruccionesVigentesTerceraMasiva(
                this.unidadConstruccionConvencionalVigentesSeleccionadas,
                this.unidadConstruccionConvencionalNuevas);
        } else {
            this.asociarConstruccionesVigentes(
                this.unidadConstruccionConvencionalVigentesSeleccionadas,
                this.unidadConstruccionConvencionalNuevas);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones no convecionales vigentes a las nuevas construcciones.
     */
    public void asociarConstruccionesNoConvencionalesVigentes() {
        if (unidadConstruccionNoConvencionalNuevas == null) {
            unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        }
        if (this.tramite.isTerceraMasiva()) {
            this.asociarConstruccionesVigentesTerceraMasiva(
                this.unidadConstruccionNoConvencionalVigentesSeleccionadas,
                this.unidadConstruccionNoConvencionalNuevas);
        } else {
            this.asociarConstruccionesVigentes(
                this.unidadConstruccionNoConvencionalVigentesSeleccionadas,
                this.unidadConstruccionNoConvencionalNuevas);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones vigentes a las nuevas construcciones.
     */
    public void asociarConstruccionesVigentes(
        PUnidadConstruccion[] pUnidadConstruccionSeleccionadas,
        List<PUnidadConstruccion> unidadConstruccionNuevas) {
        if (pUnidadConstruccionSeleccionadas.length <= 0) {
            this.addMensajeWarn("Debe seleccionar al menos una unidad de construccion a asociar");
        } else {
            try {

                List<PUnidadConstruccion> unidadesAAsociar = new ArrayList<PUnidadConstruccion>();

                // Remover las unidades seleccionadas de la lista actual
                if (this.unidadConstruccionConvencionalVigentes != null &&
                     this.unidadConstruccionConvencionalVigentesSeleccionadas != null &&
                     this.unidadConstruccionConvencionalVigentesSeleccionadas.length > 0) {
                    this.unidadConstruccionConvencionalVigentes
                        .removeAll(Arrays
                            .asList(this.unidadConstruccionConvencionalVigentesSeleccionadas));
                }

                if (this.unidadConstruccionNoConvencionalVigentes != null &&
                     this.unidadConstruccionNoConvencionalVigentesSeleccionadas != null &&
                     this.unidadConstruccionNoConvencionalVigentesSeleccionadas.length > 0) {
                    this.unidadConstruccionNoConvencionalVigentes
                        .removeAll(Arrays
                            .asList(this.unidadConstruccionNoConvencionalVigentesSeleccionadas));
                }

                // Consulta del predio base con sus construcciones.
                PPredio pPredioBase = this.getConservacionService()
                    .obtenerPPredioCompletoById(
                        this.tramite.getPredio().getId());

                // Consulta del predio asociado al ppredio
                Predio predioBase = this.getConservacionService()
                    .obtenerPredioPorId(this.tramite.getPredio().getId());

                if (this.predioDesenglobe != null && pPredioBase != null) {

                    // Mantiene nuúmero predial
                    if (this.predioDesenglobe.getId().longValue() == pPredioBase
                        .getId().longValue()) {

                        // Modificar las construcciones seleccionadas del predio
                        // base, para posteriormente asociarselas al predios
                        // seleccionado.
                        if (pUnidadConstruccionSeleccionadas != null &&
                             pUnidadConstruccionSeleccionadas.length > 0) {
                            for (PUnidadConstruccion puc : pUnidadConstruccionSeleccionadas) {
                                puc.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo());
                                puc.setUsuarioLog(this.usuarioFichaMatriz
                                    .getLogin());
                                puc.setFechaLog(new Date());
                                puc.setProvienePredio(predioBase);
                                puc.setProvieneUnidad(puc.getUnidad());
                            }
                            this.getConservacionService()
                                .guardarListaPUnidadConstruccion(
                                    Arrays.asList(pUnidadConstruccionSeleccionadas));
                        }

                    } else {

                        // Obtener una copia del arreglo de construcciones
                        // seleccionadas, para posteriormente modificarlas.
                        if (pUnidadConstruccionSeleccionadas != null &&
                             pUnidadConstruccionSeleccionadas.length > 0) {
                            for (PUnidadConstruccion puc : pUnidadConstruccionSeleccionadas) {
                                unidadesAAsociar.add((PUnidadConstruccion) puc
                                    .clone());
                            }
                        }

                        // Cancelar las construcciones seleccionadas del predio
                        // base, para posteriormente asociarselas al predios
                        // selcccionado.
                        if (pUnidadConstruccionSeleccionadas != null &&
                             pUnidadConstruccionSeleccionadas.length > 0) {
                            for (PUnidadConstruccion puc : pUnidadConstruccionSeleccionadas) {
                                puc.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA
                                    .getCodigo());
                                puc.setUsuarioLog(this.usuarioFichaMatriz
                                    .getLogin());
                                puc.setFechaLog(new Date());
                                puc.setProvienePredio(predioBase);
                            }
                            this.getConservacionService()
                                .guardarListaPUnidadConstruccion(
                                    Arrays.asList(pUnidadConstruccionSeleccionadas));
                        }

                        // Actualizar las unidades convencionales antes de
                        // asociarselas al predio seleccionado.
                        if (unidadesAAsociar.size() > 0) {

                            // Actualización de unidades convencionales
                            for (PUnidadConstruccion ucAsociar : unidadesAAsociar) {
                                ucAsociar.setPPredio(this.predioDesenglobe);
                                ucAsociar
                                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                        .getCodigo());
                                ucAsociar.setUsuarioLog(this.usuarioFichaMatriz
                                    .getLogin());
                                ucAsociar.setFechaLog(new Date());
                                ucAsociar.setId(null);
                                ucAsociar.setProvienePredio(predioBase);
                                ucAsociar.setProvieneUnidad(ucAsociar
                                    .getUnidad());
                            }

                            unidadesAAsociar = this.getConservacionService()
                                .guardarListaPUnidadConstruccion(
                                    unidadesAAsociar);

                            if (unidadesAAsociar != null) {
                                // Actualización de los componentes de las
                                // unidades actualizadas para que tengan la
                                // misma unidad
                                // de construccion padre.
                                for (PUnidadConstruccion puc : unidadesAAsociar) {
                                    if (puc.getPUnidadConstruccionComps() != null &&
                                         !puc.getPUnidadConstruccionComps()
                                            .isEmpty()) {
                                        for (PUnidadConstruccionComp pucc : puc
                                            .getPUnidadConstruccionComps()) {
                                            pucc.setPUnidadConstruccion(puc);
                                        }
                                    }
                                }
                                // Actualizar la unidad de construcción con los
                                // componentes actualizados.
                                unidadesAAsociar = this
                                    .getConservacionService()
                                    .guardarListaPUnidadConstruccion(
                                        unidadesAAsociar);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                this.addMensajeError(
                    "Error al intentar asociar las unidades al predio seleccionado.");
                LOGGER.debug(e.getMessage());
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones vigentes a las nuevas construcciones para trámites de
     * tercera masiva.
     *
     * @author javier.aponte
     */
    public void asociarConstruccionesVigentesTerceraMasiva(
        PUnidadConstruccion[] pUnidadConstruccionSeleccionadas,
        List<PUnidadConstruccion> unidadConstruccionNuevas) {
        if (pUnidadConstruccionSeleccionadas.length <= 0) {
            this.addMensajeWarn("Debe seleccionar al menos una unidad de construccion a asociar");
            return;
        }

        try {

            // Consulta del predio asociado al ppredio
            Predio predioBase = this.getConservacionService()
                .obtenerPredioPorId(this.tramite.getPredio().getId());

            if (pUnidadConstruccionSeleccionadas != null &&
                 pUnidadConstruccionSeleccionadas.length > 0) {
                for (PUnidadConstruccion puc : pUnidadConstruccionSeleccionadas) {
                    puc.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                    puc.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                    puc.setFechaLog(new Date());
                    puc.setProvienePredio(predioBase);
                    puc.setProvieneUnidad(puc.getUnidad());
                }
                this.getConservacionService().guardarListaPUnidadConstruccion(
                    Arrays.asList(pUnidadConstruccionSeleccionadas));
            }

        } catch (Exception e) {
            this.addMensajeError("Error al intentar asociar las unidades al predio seleccionado");
            LOGGER.debug(e.getMessage());
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que hace la conversión de áreas de terreno de la ficha matriz a (Ha/M2)
     *
     * @author david.cifuentes
     */
    public void convertirAreasAHaM2() {

        Double Ha = Constantes.HECTAREA;

        if (this.fichaMatriz == null) {
            this.proyectarConservacionMB.consultarFichaMatriz();
            this.fichaMatriz = this.proyectarConservacionMB.getFichaMatrizSeleccionada();
        }

        // ÁREA TERRENO
        if (this.predioRuralBool) {
            // El predio es rural, se muestran las áreas en Ha y M2
            if (this.fichaMatriz.getAreaTotalTerrenoComun() > Ha) {
                this.areaTotalTerrenoComunHa = (int) (this.fichaMatriz
                    .getAreaTotalTerrenoComun() / Ha);
                this.areaTotalTerrenoComunM2 = (this.fichaMatriz
                    .getAreaTotalTerrenoComun() % Ha);
            } else {
                this.areaTotalTerrenoComunHa = 0;
                this.areaTotalTerrenoComunM2 = this.fichaMatriz
                    .getAreaTotalTerrenoComun();
            }
            if (this.fichaMatriz.getAreaTotalTerrenoPrivada() > Ha) {
                this.areaTotalTerrenoPrivadaHa = (int) (this.fichaMatriz
                    .getAreaTotalTerrenoPrivada() / Ha);
                this.areaTotalTerrenoPrivadaM2 = (this.fichaMatriz
                    .getAreaTotalTerrenoPrivada() % Ha);
            } else {
                this.areaTotalTerrenoPrivadaHa = 0;
                this.areaTotalTerrenoPrivadaM2 = this.fichaMatriz
                    .getAreaTotalTerrenoPrivada();
            }
            // TOTALES TERRENO (Comun + Privada)
            this.areaTotalTerrenoHa = this.areaTotalTerrenoComunHa +
                 this.areaTotalTerrenoPrivadaHa;
            this.areaTotalTerrenoM2 = this.areaTotalTerrenoComunM2 +
                 this.areaTotalTerrenoPrivadaM2;
            if (areaTotalTerrenoM2 > Ha) {
                this.areaTotalTerrenoHa += 1;
                this.areaTotalTerrenoM2 = this.areaTotalTerrenoM2 % Ha;
            }
        } else {
            // El predio es urbano, todo se muestra en M2
            this.areaTotalTerrenoComunM2 = this.fichaMatriz
                .getAreaTotalTerrenoComun();
            this.areaTotalTerrenoPrivadaM2 = this.fichaMatriz
                .getAreaTotalTerrenoPrivada();

            // TOTAL TERRENO (Comun + Privada)
            this.areaTotalTerrenoM2 = this.areaTotalTerrenoComunM2 +
                 this.areaTotalTerrenoPrivadaM2;
        }

        // ÁREA CONSTRUIDA
        this.areaTotalConstruidaComunM2 = this.fichaMatriz
            .getAreaTotalConstruidaComun();
        this.areaTotalConstruidaPrivadaM2 = this.fichaMatriz
            .getAreaTotalConstruidaPrivada();
        // TOTAL CONSTRUIDA
        this.areaTotalConstruidaM2 = this.areaTotalConstruidaComunM2 +
             this.areaTotalConstruidaPrivadaM2;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza la consulta de las zonas homogeneas de un predio.
     *
     * @author david.cifuentes
     */
    public void consultarZonas() {
        if (this.tramite.isCancelacionMasiva()) {
            this.zonasHomogeneas = this.getConservacionService().
                buscarZonasHomogeneasPorPPredioIdYEstadoP(this.fichaMatriz.getPPredio().getId(),
                    null);
        } else {
            this.zonasHomogeneas = this.getConservacionService()
                .buscarZonasHomogeneasPorPPredioId(
                    this.fichaMatriz.getPPredio().getId());
        }
        if (this.zonasHomogeneas == null) {
            this.zonasHomogeneas = new ArrayList<PPredioZona>();
        }

        // Avaluo total del Terreno Común
        this.avaluoTotalTerrenoComun = 0D;
        if (this.zonasHomogeneas != null && this.zonasHomogeneas.size() > 0) {
            for (PPredioZona pz : this.zonasHomogeneas) {
                if (pz.getAvaluo() != null) {
                    this.avaluoTotalTerrenoComun += pz.getAvaluo();
                }
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que hace la conversión de áreas de terreno en la ficha matriz. (M2)
     *
     * @author david.cifuentes
     */
    public void convertirAreasAM2() {

        Double Ha = Constantes.HECTAREA;
        Double aux;

        if (predioRuralBool) {
            if (this.areaTotalTerrenoComunHa != null &&
                 this.areaTotalTerrenoComunHa > 0) {
                aux = 0D;
                aux = this.areaTotalTerrenoComunHa * Ha;
                if (this.areaTotalTerrenoComunM2 != null &&
                     this.areaTotalTerrenoComunM2 > 0D) {
                    aux += this.areaTotalTerrenoComunM2;
                }
                // Se redondea el área a dos cifras decimales refs#12022
                this.fichaMatriz.setAreaTotalTerrenoComun(Utilidades
                    .redondearNumeroADosCifrasDecimales(aux));
            }

            if (this.areaTotalTerrenoPrivadaHa != null &&
                 this.areaTotalTerrenoPrivadaHa > 0) {
                aux = 0D;
                aux = this.areaTotalTerrenoPrivadaHa * Ha;
                if (this.areaTotalTerrenoPrivadaM2 != null &&
                     this.areaTotalTerrenoPrivadaM2 > 0D) {
                    aux += this.areaTotalTerrenoPrivadaM2;
                }
                this.fichaMatriz.setAreaTotalTerrenoPrivada(Utilidades
                    .redondearNumeroADosCifrasDecimales(aux));
            }
        } else {
            if (this.areaTotalTerrenoComunM2 != null &&
                 this.areaTotalTerrenoComunM2 > 0D) {
                this.fichaMatriz
                    .setAreaTotalTerrenoComun(Utilidades
                        .redondearNumeroADosCifrasDecimales(this.areaTotalTerrenoComunM2));
            }
            if (this.areaTotalTerrenoPrivadaM2 != null &&
                 this.areaTotalTerrenoPrivadaM2 > 0D) {
                this.fichaMatriz
                    .setAreaTotalTerrenoPrivada(Utilidades
                        .redondearNumeroADosCifrasDecimales(this.areaTotalTerrenoPrivadaM2));
            }
        }
        this.fichaMatriz
            .setAreaTotalConstruidaComun(Utilidades
                .redondearNumeroADosCifrasDecimales(this.areaTotalConstruidaComunM2));
        this.fichaMatriz
            .setAreaTotalConstruidaPrivada(Utilidades
                .redondearNumeroADosCifrasDecimales(this.areaTotalConstruidaPrivadaM2));
        this.proyectarConservacionMB
            .setFichaMatrizSeleccionada(this.fichaMatriz);
    }

    // ------------------------------------------------------------- //
    /**
     * Método que actualiza los cambios de áreas en la interfaz.
     *
     * @author david.cifuentes
     */
    public void actualizarTotales() {

        // TOTALES TERRENO (Comun + Privada)
        if (this.predioRuralBool) {
            this.areaTotalTerrenoHa = this.areaTotalTerrenoComunHa +
                 this.areaTotalTerrenoPrivadaHa;
        }
        this.areaTotalTerrenoM2 = this.areaTotalTerrenoComunM2 +
             this.areaTotalTerrenoPrivadaM2;

        this.areaTotalConstruidaM2 = this.areaTotalConstruidaComunM2 +
             this.areaTotalConstruidaPrivadaM2;
    }

    // ------------------------------------------------------------- //
    /**
     * Método encargado de guardar el PPredio.
     *
     * @david.cifuentes
     */
    public void guardarPPredio() {

        try {
            this.getConservacionService().actualizarPPredio(
                this.predioDesenglobe);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método encargado de guardar las areas del predio.
     *
     * @david.cifuentes
     */
    public void guardarAreasPPredio() {

        try {
            if (this.fichaMatriz == null) {
                return;
            }
            if (this.fichaMatriz.getCancelaInscribe() == null) {
                this.fichaMatriz
                    .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                        .getCodigo());
            }
            // asignación del usuario y fecha
            this.fichaMatriz.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
            this.fichaMatriz.setFechaLog(new Date());
            // Método que hace la conversión de áreas de terreno en la
            // ficha matriz. (Ha / M2)
            this.convertirAreasAHaM2();

            // Guardar áreas de la ficha matriz.
            this.fichaMatriz = this.getConservacionService()
                .guardarActualizarPFichaMatriz(this.fichaMatriz);
            this.verificarTorresEnFirme();

            this.proyectarConservacionMB
                .setFichaMatrizSeleccionada(this.fichaMatriz);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(e);
            this.addMensajeInfo("Error actualizando las áreas.");
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asigna el coeficiente de copropiedad a los fichaMatrizPredios seleccionados.
     *
     * * @david.cifuentes
     */
    public void asignarCoeficientes() {
        List<PFichaMatrizPredio> aux = new ArrayList<PFichaMatrizPredio>();

        seleccionarPrediosPorCheck();

        if (this.coeficienteCopropiedad == null ||
             this.coeficienteCopropiedad < 0 ||
             this.coeficienteCopropiedad > 1) {
            this.addMensajeWarn(
                "El coeficiente ingresado no es válido, debe ser un número entre 0 y 1.");
            return;
        } else if (this.coeficienteCopropiedad == 1) {
            this.addMensajeWarn(
                "El coeficiente ingresado no es válido, debe ser número estríctamente menor a 1 y mayor a 0.");
            return;
        } else {
            if (this.fichaMatrizPrediosSeleccionados != null) {

                List<PPredio> prediosAsociadosFM = this.getConservacionService()
                    .buscarPPrediosPorTramiteId(this.tramite.getId());
                List<PPredio> pPrediosActualizar = new ArrayList<PPredio>();

                for (int i = 0; i < this.fichaMatrizPrediosSeleccionados.length; i++) {
                    this.fichaMatrizPrediosSeleccionados[i]
                        .setCoeficiente(this.coeficienteCopropiedad);
                    this.fichaMatrizPrediosSeleccionados[i].setFechaLog(new Date());

                    // #14068 :: CC-NP-CO-122 :: Se hace necesario identificar a
                    // cuales predios originales del trámite se les ha
                    // modificado el coeficiente de copropiedad para mostrar a
                    // estos en el texto motivo :: 24/08/2015
                    if (ESiNo.SI.getCodigo().equals(
                        this.fichaMatrizPrediosSeleccionados[i]
                            .getOriginalTramite())) {

                        if (this.tramite.isEnglobeVirtual()) {
                            //Solo se actualizan los que no tienen cancela inscribe en 'I'
                            if (!EProyeccionCancelaInscribe.INSCRIBE.getCodigo().
                                equals(this.fichaMatrizPrediosSeleccionados[i].getCancelaInscribe())) {
                                this.fichaMatrizPrediosSeleccionados[i]
                                    .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                                        .getCodigo());
                            }
                        } else {
                            this.fichaMatrizPrediosSeleccionados[i]
                                .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo());
                        }

                        // Actualización del valor de cancela_inscribe para el PPredio
                        if (prediosAsociadosFM != null) {
                            for (PPredio pp : prediosAsociadosFM) {
                                if (ESiNo.SI.getCodigo().equals(pp.getOriginalTramite()) &&
                                     pp.getNumeroPredial() != null &&
                                     pp.getNumeroPredial().equals(
                                        this.fichaMatrizPrediosSeleccionados[i].getNumeroPredial())) {
                                    pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.
                                        getCodigo());
                                    pp.setFechaLog(new Date());
                                    pPrediosActualizar.add(pp);
                                }
                            }
                        }
                    }

                    aux.add(this.fichaMatrizPrediosSeleccionados[i]);
                }
                if (!pPrediosActualizar.isEmpty()) {
                    for (PPredio pp : pPrediosActualizar) {
                        this.getConservacionService().actualizarPPredio(pp);
                    }
                }
            }
        }
        this.coeficienteCopropiedad = 0D;
        this.getConservacionService().guardarPrediosFichaMatriz(aux);
        this.fichaMatrizPrediosSeleccionados = null;
        Double sumaCoeficientes = 0D;
        boolean coeficienteCero = false;
        for (PFichaMatrizPredio pfmp : this.fichaMatriz
            .getPFichaMatrizPredios()) {
            if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.getCancelaInscribe())) {
                sumaCoeficientes += pfmp.getCoeficiente();
                if (pfmp.getCoeficiente().doubleValue() == 0) {
                    coeficienteCero = true;
                }
            }
        }

        String mensajeMayor, mensajeMenor;
        if (sumaCoeficientes > 1 + 0.000000001 ||
             sumaCoeficientes < 1 - 0.000000001) {
            mensajeMayor = (sumaCoeficientes > 1) ? " mayor " : "";
            mensajeMenor = (sumaCoeficientes < 1) ? " menor " : "";
            this.addMensajeWarn("Los coeficientes fueron almacenados pero la suma total de ellos " +
                 sumaCoeficientes +
                 " es" +
                 mensajeMayor +
                 mensajeMenor +
                 " a 1.");
            this.addMensajeWarn("Asegúrese de revisar los datos ingresados!");
            return;
        } else {
            if (coeficienteCero) {
                this.addMensajeWarn(
                    "Los coeficientes fueron almacenados correctamente, la suma de ellos es 1 pero existen predios con coeficiente de propiedad cero.");
            } else {
                this.addMensajeInfo("Coeficientes almacenados!");
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que carga el coeficiente de copropiedad del PPredioSeleccionado.
     *
     * @author david.cifuentes
     */
    public void cargarCoeficiente() {
        this.seleccionarPrediosPorCheck();

        //@modified by:: leidy.gonzalez:: #24584:: 25/07/2017
        //Valida la sumatoria de coeficientes de copropiedad de acuerdo a la cantidad
        // de predios seleccionados para los englobes virtuales
        this.sumatoriaCoefCopropiedad = 0.0D;

        if (this.tramite.isEnglobeVirtual()) {

            this.banderaAceptarCoefCoprop = false;

            if (this.fichaMatrizPrediosSeleccionados != null &&
                 this.fichaMatrizPrediosSeleccionados.length >= 1) {

                for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {
                    if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                        if (this.coeficienteCopropiedad != null) {
                            this.sumatoriaCoefCopropiedad += this.coeficienteCopropiedad;
                        } else {
                            this.coeficienteCopropiedad = fichaMatrizPrediosSeleccionados[0].
                                getCoeficiente();
                            this.sumatoriaCoefCopropiedad += pfmp.getCoeficiente();
                        }

                    }
                }

            }
        } else if (fichaMatrizPrediosSeleccionados != null &&
             fichaMatrizPrediosSeleccionados[0] != null &&
             fichaMatrizPrediosSeleccionados[0].getCoeficiente() != null) {
            this.coeficienteCopropiedad = fichaMatrizPrediosSeleccionados[0]
                .getCoeficiente();
        } else {
            this.coeficienteCopropiedad = 0.0D;
        }
    }

    /**
     * Método que carga el coeficiente de copropiedad del PPredioSeleccionado.
     *
     * @author david.cifuentes
     */
    public void calcularCoeficiente() {
        this.seleccionarPrediosPorCheck();

        //@modified by:: leidy.gonzalez:: #24584:: 25/07/2017
        //Valida la sumatoria de coeficientes de copropiedad de acuerdo a la cantidad
        // de predios seleccionados para los englobes virtuales
        this.sumatoriaCoefCopropiedad = 0.0D;

        if (this.tramite.isEnglobeVirtual()) {

            this.banderaAceptarCoefCoprop = true;

            if (this.fichaMatrizPrediosSeleccionados != null &&
                 this.fichaMatrizPrediosSeleccionados.length >= 1) {

                for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {
                    if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                        if (this.coeficienteCopropiedad != null) {
                            this.sumatoriaCoefCopropiedad += this.coeficienteCopropiedad;
                        } else {
                            this.coeficienteCopropiedad = fichaMatrizPrediosSeleccionados[0].
                                getCoeficiente();
                            this.sumatoriaCoefCopropiedad += pfmp.getCoeficiente();
                        }

                    }
                }

            }
        } else if (fichaMatrizPrediosSeleccionados != null &&
             fichaMatrizPrediosSeleccionados[0] != null &&
             fichaMatrizPrediosSeleccionados[0].getCoeficiente() != null) {
            this.coeficienteCopropiedad = fichaMatrizPrediosSeleccionados[0]
                .getCoeficiente();
        } else {
            this.coeficienteCopropiedad = 0.0D;
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que prepara los datos necesarios para multiplicar una torre
     *
     * @author fabio.navarrete
     */
    public void setupMultiplicarFichaMatrizTorre() {
        this.numeroTorresMultiplicar = null;

        if (this.tramite.isEnglobeVirtual()) {
            this.numeroTorreInicial = 0;
            if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite()) &&
                 this.fichaMatrizTorreSeleccionada.getProvieneFichaMatriz() != null) {
                this.fichaMatrizOriginalTorreSeleccionada = this.getConservacionService().
                    obtenerFichaMatrizPorId(this.fichaMatrizTorreSeleccionada.
                        getProvieneFichaMatriz().getId());
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza el proceso de eliminación de una torre, reubicando los números de las
     * torres.
     *
     * @author fabio.navarrete
     */
    public void eliminarTorre() {
        try {
            boolean validacionTorre = true;

            if (!this.tramite.isDesenglobeEtapas()) {
                validacionTorre = this
                    .validarUltimaTorreCreada(this.fichaMatrizTorreSeleccionada);
            } else if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.
                getOriginalTramite()) && this.validarExistenciaPrediosPorTorre(
                    this.fichaMatrizTorreSeleccionada)) {
                this.addMensajeError(
                    "No se pueden eliminar torres que se encuentren en firme y que tengan predios activos, para esto debe desactivar primero todos los predios de la torre.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            } else if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.
                getOriginalTramite()) && this.validarExistenciaPrediosMigrados()) {
                this.addMensajeError(
                    "No se pueden eliminar torres que se encuentren en firme y que tengan predios migrados activos.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

            if (validacionTorre) {
                // Recrear la matriz de predios para hacer las validaciones sobre las unidades de la torre
                // this.cargarMatrizNumerosPrediales();

                if (!(validarExistenPrediosCreadosEnTorre(this.fichaMatrizTorreSeleccionada) ||
                     validarExistenSotanosCreadosEnTorre(this.fichaMatrizTorreSeleccionada))) {
                    // Actualiza el total de unidades privadas en la ficha
                    // matriz
                    this.fichaMatriz.setTotalUnidadesPrivadas(this.fichaMatriz
                        .getTotalUnidadesPrivadas() -
                         this.fichaMatrizTorreSeleccionada.getUnidades());

                    // Actualiza el total de unidades privadas en la ficha
                    // matriz
                    this.fichaMatriz.setTotalUnidadesSotanos(this.fichaMatriz
                        .getTotalUnidadesSotanos() -
                         this.fichaMatrizTorreSeleccionada
                            .getUnidadesSotanos());

                    this.fichaMatriz.getPFichaMatrizTorres().remove(
                        this.fichaMatrizTorreSeleccionada);

                    Long numTorreSeleccionada = this.fichaMatrizTorreSeleccionada
                        .getTorre();

                    if (!ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.
                        getOriginalTramite())) {
                        this.getConservacionService()
                            .eliminarPFichaMatrizTorre(
                                this.fichaMatrizTorreSeleccionada);
                    } else {

                        if (this.tramite.isEnglobeVirtual()) {
                            this.fichaMatrizTorreSeleccionada
                                .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo());
                        } else {
                            this.fichaMatrizTorreSeleccionada
                                .setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA
                                    .getCodigo());
                        }

                        this.fichaMatrizTorreSeleccionada.setEstado(EPredioEstado.CANCELADO.
                            getCodigo());

                        this.getConservacionService().guardarActualizarPFichaMatrizTorre(
                            this.fichaMatrizTorreSeleccionada);
                    }
                    if (!this.tramite.isEnglobeVirtual()) {
                        for (PFichaMatrizTorre pfmt : this.fichaMatriz
                            .getPFichaMatrizTorres()) {
                            if (pfmt.getTorre() != null &&
                                 pfmt.getTorre().compareTo(numTorreSeleccionada) > 0) {
                                pfmt.setTorre(pfmt.getTorre() - 1);
                            }
                        }
                        this.fichaMatriz.setPFichaMatrizTorres(this
                            .getConservacionService()
                            .guardarActualizarPFichaMatrizTorres(
                                this.fichaMatriz.getPFichaMatrizTorres()));
                    }

                    this.guardarFichaMatriz();
                    this.addMensajeInfo("Se eliminó la torre satisfactoriamente.");

                    //@modified by leidy.gonzalez::#40839::18/10/2017
                    //Se eliminan los predios asociados a la torre, dejandolos en estado cancelado
//                    if (this.tramite.isEnglobeVirtual()) {
//
//                        List<PPredio> pPrediosProyectados = this.getConservacionService()
//                                .buscarPPrediosCompletosPorTramiteIdPHCondominio(
//                                        this.proyectarConservacionMB.getTramite().getId());
//                        List<PPredio> pPrediosTemp = new ArrayList<PPredio>();
//
//                        List<PFichaMatrizPredio> pFichaMatrizPredioTemp
//                                = new ArrayList<PFichaMatrizPredio>();
//
//                        if (this.fichaMatriz.getPFichaMatrizPredios() != null
//                                && !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {
//
//                            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
//
//                                Long torre = 0L;
//                                torre = Long.valueOf(pfmp.getNumeroPredial()
//                                        .substring(23, 24));
//
//                                Long torreProv = 0L;
//
//                                String torreMigrada = new String();
//                                String pisoMigrado = new String();
//
//                                if (pfmp.getNumeroPredialOriginal() != null
//                                        && !pfmp.getNumeroPredialOriginal().isEmpty()) {
//
//                                    torreProv = Long.valueOf(pfmp.getNumeroPredialOriginal()
//                                            .substring(23, 24));
//
//                                    torreMigrada = pfmp.getNumeroPredialOriginal().substring(22, 24);
//                                    pisoMigrado = pfmp.getNumeroPredialOriginal().substring(24, 26);
//                                }
//
//                                //Se valida que la torre seleccionada sea la misma de la lista de P_FICHA_MATRIZ_TORRE
//                                //ya sea de una torre en firme, nueva o migrada
//                                if (fichaMatrizTorreSeleccionada.getProvieneTorre() != null
//                                        && ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite())
//                                        && (fichaMatrizTorreSeleccionada.getTorre() == torre
//                                        || fichaMatrizTorreSeleccionada.getProvieneTorre().getTorre() == torreProv
//                                        || !("00".equals(pisoMigrado) && "00".equals(torreMigrada)))
//                                        && !EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
//
//                                    if (pfmp.getProvieneFichaMatriz().getId() != null
//                                            && fichaMatrizTorreSeleccionada.getProvieneFichaMatriz().getId().
//                                                    equals(pfmp.getProvieneFichaMatriz().getId())) {
//
//                                        pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
//                                                .getCodigo());
//                                        pfmp.setEstado(EPredioEstado.CANCELADO.getCodigo());
//                                        pFichaMatrizPredioTemp.add(pfmp);
//                                    }
//
//                                } else if (fichaMatrizTorreSeleccionada.getTorre() != null
//                                        && ESiNo.NO.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite())
//                                        && fichaMatrizTorreSeleccionada.getTorre() == torre
//                                        && (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pfmp.getCancelaInscribe())
//                                        && !EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()))) {
//
//                                    pFichaMatrizPredioTemp.add(pfmp);
//                                }
//
//                            }
//                            if (pFichaMatrizPredioTemp != null && !pFichaMatrizPredioTemp.isEmpty()) {
//
//                                //Valida que los preios a cancelar en P_FICHA_MATRIZ_PREDIO 
//                                //sean los mismos a eliminar P_predio
//                                for (PFichaMatrizPredio pfmp : pFichaMatrizPredioTemp) {
//
//                                    if (pPrediosProyectados != null && !pPrediosProyectados.isEmpty()) {
//
//                                        for (PPredio pPredioTemp : pPrediosProyectados) {
//
//                                            if (!EPredioEstado.CANCELADO.getCodigo().equals(pPredioTemp.getEstado())) {
//
//                                                Long torrePred = 0L;
//                                                torrePred = Long.valueOf(pfmp.getNumeroPredial()
//                                                        .substring(23, 24));
//
//                                                String torreMigradaPred = new String();
//                                                String pisoMigradoPred = new String();
//
//                                                if (pfmp.getNumeroPredialOriginal() != null
//                                                        && !pfmp.getNumeroPredialOriginal().isEmpty()) {
//
//                                                    torreMigradaPred = pfmp.getNumeroPredialOriginal().substring(22, 24);
//                                                    pisoMigradoPred = pfmp.getNumeroPredialOriginal().substring(24, 26);
//                                                }
//
//                                                if (pPredioTemp.getNumeroPredial() != null
//                                                        && !pPredioTemp.getNumeroPredial().isEmpty()
//                                                        && pfmp.getNumeroPredial().equals(pPredioTemp.getNumeroPredial())) {
//
//                                                    if (!ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite())
//                                                            && fichaMatrizTorreSeleccionada.getTorre() != null
//                                                            && fichaMatrizTorreSeleccionada.getTorre() == torrePred) {
//
//                                                        this.getConservacionService()
//                                                                .eliminarProyeccionPredio(pPredioTemp.getId());
//
//                                                    } else if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite())
//                                                            && fichaMatrizTorreSeleccionada.getProvieneTorre() != null
//                                                            && fichaMatrizTorreSeleccionada.getProvieneTorre().getTorre() != null
//                                                            && (fichaMatrizTorreSeleccionada.getProvieneTorre().getTorre() == torrePred
//                                                            || ("00".equals(pisoMigradoPred) && "00".equals(torreMigradaPred)))) {
//
//                                                        pPredioTemp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
//                                                                .getCodigo());
//                                                        pPredioTemp.setEstado(EPredioEstado.CANCELADO.getCodigo());
//                                                        pPrediosTemp.add(pPredioTemp);
//                                                    }
//                                                }
//                                            }
//
//                                        }
//                                    }
//                                }
//
//                                if (pPrediosTemp != null && !pPrediosTemp.isEmpty()
//                                        && ESiNo.SI.getCodigo().
//                                                equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite())) {
//
//                                    this.getConservacionService()
//                                            .guardarPPredios(pPrediosTemp);
//                                }
//
//                                if (ESiNo.SI.getCodigo().equals(this.fichaMatrizTorreSeleccionada.getOriginalTramite())) {
//                                    this.getConservacionService()
//                                            .guardarPrediosFichaMatriz(pFichaMatrizPredioTemp);
//                                } else {
//                                    this.getConservacionService()
//                                            .eliminarPrediosFichaMatriz(pFichaMatrizPredioTemp);
//                                    this.fichaMatriz.getPFichaMatrizPredios().removeAll(pFichaMatrizPredioTemp);
//
//                                }
//                            }
//
//                        }
//
//                    }
//
//                    if (this.tramite.isEnglobeVirtual()) {
//
//                        getFichaMatrizPredios();
//
//                    }
                } else {
                    this.addMensajeError(
                        "No se puede eliminar la torre seleccionada pues para ésta ya se han creado predios. Para eliminarla por favor elimine sus predios asociados.");

                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");
                }
                if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
                     !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                    Collections.sort(this.fichaMatriz.getPFichaMatrizTorres());
                }

            } else {
                this.addMensajeError(
                    "Debido a que ya se han creado predios en las torres, sólo se puede eliminar la última torre creada, siempre y cuando no se hayan creado predios para ésta.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que actualiza las listas de unidades de construcción nuevas - no canceladas
     */
    private void updateConstruccionesNuevasNoCanceladas() {

        this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
        for (PUnidadConstruccion puc : this.unidadConstruccionConvencionalNuevas) {
            if (puc.getCancelaInscribe() == null ||
                 !puc.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

                if (puc.getProvienePredio() != null ||
                     this.tramite.isTerceraMasiva() ||
                     this.tramite.isRectificacionMatriz() ||
                     ESiNo.SI.getCodigo().equals(puc.getOriginalTramite())) {
                    puc.setPertenecePredioProyectado(true);
                }
                this.unidadConstruccionConvencionalNuevasNoCanceladas.add(puc);

            }
        }

        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            new ArrayList<PUnidadConstruccion>();
        for (PUnidadConstruccion puc : this.unidadConstruccionNoConvencionalNuevas) {
            if (puc.getCancelaInscribe() == null ||
                 !puc.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

                if (puc.getProvienePredio() != null ||
                     this.tramite.isTerceraMasiva() ||
                     this.tramite.isRectificacionMatriz() ||
                     ESiNo.SI.getCodigo().equals(puc.getOriginalTramite())) {
                    puc.setPertenecePredioProyectado(true);
                }
                this.unidadConstruccionNoConvencionalNuevasNoCanceladas.add(puc);
            }
        }

        // Verificar unidades que permiten edición
        //javier.aponte :: CU-NP-CO-242 Para trámites de tercerea masivos se muestra la opción Editar y Eliminar para cada una 
        //de las construcciones asociadas. :: 16/07/2015
        if (!this.tramite.isTerceraMasiva()) {
            this.verificarEdicionUCMantiene();
        }

    }

    // ------------------------------------------------------------- //
    /**
     * Método que inicializa las unidades de construcción a mostrar
     *
     * @author fabio.navarrete
     */
    @SuppressWarnings("unchecked")
    public void setupUnidadesDeConstruccion() {

        // Inicializar variables de construcciones
        this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionConvencionalVigentes = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalVigentes = new ArrayList<PUnidadConstruccion>();

        try {
            // Consulta del predio base con sus construcciones
            // Cargue de las construcciones vigentes
            PPredio pPredioBase = this.getConservacionService()
                .obtenerPPredioCompletoById(
                    this.proyectarConservacionMB.getTramite()
                        .getPredio().getId());

            if (pPredioBase != null &&
                 pPredioBase.getPUnidadConstruccions() != null &&
                 !pPredioBase.getPUnidadConstruccions().isEmpty()) {

                // Construcciones convencionales
                if (pPredioBase.getPUnidadesConstruccionConvencional() != null &&
                     !pPredioBase.getPUnidadesConstruccionConvencional()
                        .isEmpty()) {
                    for (PUnidadConstruccion puc : pPredioBase
                        .getPUnidadesConstruccionConvencional()) {
                        if (puc.getProvienePredio() == null &&
                             !EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo().equals(
                                    puc.getCancelaInscribe()) &&
                             !EProyeccionCancelaInscribe.CANCELA
                                .getCodigo().equals(puc.getCancelaInscribe())) {

                            this.unidadConstruccionConvencionalVigentes.add(puc);
                        }
                    }
                }
                // Construcciones no convencionales
                if (pPredioBase.getPUnidadesConstruccionNoConvencional() != null &&
                     !pPredioBase
                        .getPUnidadesConstruccionNoConvencional()
                        .isEmpty()) {
                    for (PUnidadConstruccion puc : pPredioBase
                        .getPUnidadesConstruccionNoConvencional()) {
                        if (puc.getProvienePredio() == null &&
                             !EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo().equals(puc.getCancelaInscribe()) &&
                             !EProyeccionCancelaInscribe.CANCELA
                                .getCodigo().equals(puc.getCancelaInscribe())) {

                            this.unidadConstruccionNoConvencionalVigentes.add(puc);
                        }
                    }
                }
            }
            if(this.predioDesenglobe!=null) {
            this.predioDesenglobe = this.getConservacionService()
                .obtenerPPredioCompletoById(this.predioDesenglobe.getId());

//			if (this.predioDesenglobe != null
//					&& pPredioBase != null
//					&& this.predioDesenglobe.getId().longValue() == pPredioBase
//							.getId().longValue()) {
//
//				if (this.predioDesenglobe.getPUnidadConstruccions() != null
//						&& !this.predioDesenglobe.getPUnidadConstruccions()
//								.isEmpty()) {
//
//					if (this.predioDesenglobe
//							.getPUnidadesConstruccionConvencional() != null
//							&& !this.predioDesenglobe
//									.getPUnidadesConstruccionConvencional()
//									.isEmpty()) {
//
//						for (PUnidadConstruccion pucConv : this.predioDesenglobe
//								.getPUnidadesConstruccionConvencional()) {
//							if (pucConv.getCancelaInscribe() != null) {
//								if ((pucConv.getCancelaInscribe().equals(
//										EProyeccionCancelaInscribe.INSCRIBE
//												.getCodigo()) && pucConv
//										.getProvienePredio() == null)
//										|| (pucConv
//												.getCancelaInscribe()
//												.equals(EProyeccionCancelaInscribe.MODIFICA
//														.getCodigo()) && pucConv
//												.getProvienePredio() != null)) {
//
//									this.unidadConstruccionConvencionalNuevas
//											.add(pucConv);
//								}
//							}
//						}
//					}
//					if (this.predioDesenglobe
//							.getPUnidadesConstruccionNoConvencional() != null
//							&& !this.predioDesenglobe
//									.getPUnidadesConstruccionNoConvencional()
//									.isEmpty()) {
//
//						for (PUnidadConstruccion pucNoConv : this.predioDesenglobe
//								.getPUnidadesConstruccionNoConvencional()) {
//
//							if (pucNoConv.getCancelaInscribe() != null) {
//								if ((pucNoConv.getCancelaInscribe().equals(
//										EProyeccionCancelaInscribe.INSCRIBE
//												.getCodigo()) && pucNoConv
//										.getProvienePredio() == null)
//										|| (pucNoConv
//												.getCancelaInscribe()
//												.equals(EProyeccionCancelaInscribe.MODIFICA
//														.getCodigo()) && pucNoConv
//												.getProvienePredio() != null)) {
//
//									this.unidadConstruccionNoConvencionalNuevas
//											.add(pucNoConv);
//								}
//							}
//						}
//					}
//
//					// Verificar unidades que permiten edición
//					this.verificarEdicionUCMantiene();
//				}
//			} else {
            if (!this.predioDesenglobe.isEsPredioFichaMatriz()) {
                // Consultar las unidades de construcción nuevas de la ficha
                // matriz.
                if (this.fichaMatriz != null &&
                     this.fichaMatriz.getPPredio() != null) {
                    PPredio pPredioFichaMatriz = this
                        .getConservacionService()
                        .obtenerPPredioCompletoById(
                            this.fichaMatriz.getPPredio().getId());

                    if (pPredioFichaMatriz
                        .getPUnidadesConstruccionConvencional() != null &&
                         !pPredioFichaMatriz
                            .getPUnidadesConstruccionConvencional()
                            .isEmpty()) {

                        for (PUnidadConstruccion pucConv : pPredioFichaMatriz
                            .getPUnidadesConstruccionConvencional()) {

                            if (pucConv.getCancelaInscribe() != null &&
                                 (pucConv
                                    .getCancelaInscribe()
                                    .equals(EProyeccionCancelaInscribe.INSCRIBE
                                        .getCodigo()) || (pucConv
                                    .getCancelaInscribe()
                                    .equals(EProyeccionCancelaInscribe.MODIFICA
                                        .getCodigo()) && pucConv
                                    .getProvieneUnidad() != null))) {

                                this.unidadConstruccionConvencionalNuevas
                                    .add(pucConv);
                            }
                        }
                    }
                    if (pPredioFichaMatriz
                        .getPUnidadesConstruccionNoConvencional() != null &&
                         !pPredioFichaMatriz
                            .getPUnidadesConstruccionNoConvencional()
                            .isEmpty()) {

                        for (PUnidadConstruccion pucNoConv : pPredioFichaMatriz
                            .getPUnidadesConstruccionNoConvencional()) {
                            if (pucNoConv.getCancelaInscribe() != null &&
                                 (pucNoConv
                                    .getCancelaInscribe()
                                    .equals(EProyeccionCancelaInscribe.INSCRIBE
                                        .getCodigo()) || (pucNoConv
                                    .getCancelaInscribe()
                                    .equals(EProyeccionCancelaInscribe.MODIFICA
                                        .getCodigo()) && pucNoConv
                                    .getProvieneUnidad() != null))) {

                                this.unidadConstruccionNoConvencionalNuevas
                                    .add(pucNoConv);
                            }
                        }
                    }
                }
            } else if (this.predioDesenglobe.getPUnidadConstruccions() != null &&
                 !this.predioDesenglobe.getPUnidadConstruccions()
                    .isEmpty()) {

                if (this.predioDesenglobe
                    .getPUnidadesConstruccionConvencional() != null &&
                     !this.predioDesenglobe
                        .getPUnidadesConstruccionConvencional()
                        .isEmpty()) {

                    for (PUnidadConstruccion pucConv : this.predioDesenglobe
                        .getPUnidadesConstruccionConvencional()) {

                        if (pucConv.getCancelaInscribe() != null &&
                             (pucConv.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.INSCRIBE
                                    .getCodigo()) || pucConv
                                .getCancelaInscribe()
                                .equals(EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo()))) {

                            this.unidadConstruccionConvencionalNuevas
                                .add(pucConv);
                        }
                    }
                }
                if (this.predioDesenglobe
                    .getPUnidadesConstruccionNoConvencional() != null &&
                     !this.predioDesenglobe
                        .getPUnidadesConstruccionNoConvencional()
                        .isEmpty()) {

                    for (PUnidadConstruccion pucNoConv : this.predioDesenglobe
                        .getPUnidadesConstruccionNoConvencional()) {
                        if (pucNoConv.getCancelaInscribe() != null &&
                             (pucNoConv.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.INSCRIBE
                                    .getCodigo()) || pucNoConv
                                .getCancelaInscribe()
                                .equals(EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo()))) {

                            this.unidadConstruccionNoConvencionalNuevas
                                .add(pucNoConv);
                        }
                    }
                }

                // Debido a que no se tiene la manera explicita de saber si
                // una unidad es la misma que la del predio base, se realiza
                // un ordenamiento de las unidades proyectadas y se toma que
                // las primeras unidades son las del predio base, teniendo
                // encuenta la cantidad de unidades inicial.
                int cantidadUnidadesPredioBase;
                int i = 0;

                // Cargue de los id de las unidades construcción del predio
                // base
                List<UnidadConstruccion> unidadConstruccionPredioBase = this
                    .getConservacionService()
                    .obtenerUnidadesConstruccionsPorNumeroPredial(
                        this.tramite.getPredio().getNumeroPredial());

                if (unidadConstruccionPredioBase != null) {

                    cantidadUnidadesPredioBase = unidadConstruccionPredioBase
                        .size();
                    Collections.sort(this.predioDesenglobe
                        .getPUnidadConstruccions());
                    for (PUnidadConstruccion pu : this.predioDesenglobe
                        .getPUnidadConstruccions()) {
                        pu.setPertenecePredioProyectado(true);
                        i++;
                        if (i == cantidadUnidadesPredioBase) {
                            break;
                        }
                    }
                }
            }
        }
//			}

            this.updateConstruccionesNuevasNoCanceladas();
            this.proyectarConservacionMB.cargarConstruccionesAsociadasPredioOrigen();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void cargarConstruccionesAsociadasPredioOrigen() {

        if (this.getTramite().isSegundaDesenglobe()) {
            try {

                List<UnidadConstruccion> unidadConstruccionConvencionalPredioOriginalTemp =
                    new ArrayList<UnidadConstruccion>();
                List<UnidadConstruccion> unidadConstruccionNoConvencionalPredioOriginalTemp =
                    new ArrayList<UnidadConstruccion>();

                // Cargue de las construcciones Originales
                List<UnidadConstruccion> unidadesConstruccionPredioOrigen = this
                    .getConservacionService()
                    .obtenerUnidadesNoCanceladasByPredioId(
                        getTramite().getPredio().getId());

                if (unidadesConstruccionPredioOrigen != null &&
                     !unidadesConstruccionPredioOrigen.isEmpty()) {
                    for (UnidadConstruccion uc : unidadesConstruccionPredioOrigen) {
                        if (EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()
                            .equals(uc.getTipoConstruccion().trim()) &&
                             uc.getAnioCancelacion() == null) {

                            if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
                                unidadConstruccionConvencionalPredioOriginalTemp
                                    .add(uc);
                            }
                        }
                        if (EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()
                            .equals(uc.getTipoConstruccion().trim())) {
                            if (!this
                                .validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
                                unidadConstruccionNoConvencionalPredioOriginalTemp
                                    .add(uc);
                            }
                        }
                    }

                    setUnidadConstruccionConvencionalPredioOriginal(
                        unidadConstruccionConvencionalPredioOriginalTemp);
                    setUnidadConstruccionNoConvencionalPredioOriginal(
                        unidadConstruccionNoConvencionalPredioOriginalTemp);
                    setUnidadConstruccionConvencionalPredioOriginalSeleccionadas(null);
                    setUnidadConstruccionNoConvencionalPredioOriginalSeleccionadas(null);
                }
            } catch (Exception e) {
                LOGGER.error("Error realizando la búsqueda de las unidades del predio origen': " +
                     e.getMessage(), e);
            }
        }
    }

    /**
     * Método usado para validar el área asignada de una de las construcciones del predio origen, ha
     * sido asignada en su totalidad a los predios resultantes en el trámite de desenglobe
     *
     * @author david.cifuentes
     * @param unidadConstruccionOrigen
     * @return true - Si el área original ha sido asignada en su totalidad false - Si el parte del
     * área total aún se ha asignado
     */
    public boolean validarAreaCompletaAsignadaConstruccionOriginal(
        UnidadConstruccion unidadConstruccionOrigen) {

        // Se verifica si la unidad ha sido asociada o no, si no se ha asociado
        // se considera válida.
        List<PUnidadConstruccion> pucAsociadas = this.getConservacionService()
            .buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(
                this.getTramite().getId(), unidadConstruccionOrigen);

        if (unidadConstruccionOrigen != null) {

            Double areaAsignada = new Double(0);

            for (PUnidadConstruccion puc : pucAsociadas) {
                if (puc.getUnidad() != null &&
                     !EProyeccionCancelaInscribe.CANCELA.getCodigo()
                        .equals(puc.getCancelaInscribe()) &&
                     puc.getProvieneUnidad() != null &&
                     puc.getProvieneUnidad().equals(
                        unidadConstruccionOrigen.getUnidad())) {
                    areaAsignada += puc.getAreaConstruida();
                }
            }
            // Verificar si la suma de áreas asignadas completa la totalidad del
            // área de la construcción origen
            if (unidadConstruccionOrigen != null) {
                if (areaAsignada.doubleValue() >= unidadConstruccionOrigen
                    .getAreaConstruida().doubleValue()) {
                    return true;
                }
            }
        }
        return false;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que inicializa las unidades de construcción a mostrar para tramites de rectificacion
     * matriz
     *
     * @author felipe.cadena
     */
    public void setupUnidadesDeConstruccionRectMatriz() {

        // Inicializar variables de construcciones
        this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionConvencionalVigentes = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalVigentes = new ArrayList<PUnidadConstruccion>();

        try {
            // Consulta del predio base con sus construcciones
            // Cargue de las construcciones vigentes
            PPredio pPredioBase = this.getConservacionService()
                .obtenerPPredioCompletoById(
                    this.proyectarConservacionMB.getTramite()
                        .getPredio().getId());

            if (pPredioBase != null &&
                 pPredioBase.getPUnidadConstruccions() != null &&
                 !pPredioBase.getPUnidadConstruccions().isEmpty()) {

                Predio predioOriginal = this.getConservacionService().obtenerPredioConAvaluosPorId(
                    this.proyectarConservacionMB.getTramite()
                        .getPredio().getId());
                List<UnidadConstruccion> unidadesConvencionalesOriginales = predioOriginal.
                    getUnidadesConstruccionConvencional();
                List<UnidadConstruccion> unidadesNoConvencionalesOriginales = predioOriginal.
                    getUnidadesConstruccionNoConvencional();

                // Construcciones convencionales vigentes
                if (unidadesConvencionalesOriginales != null &&
                     !unidadesConvencionalesOriginales.isEmpty()) {
                    for (UnidadConstruccion unidadConstruccion : unidadesConvencionalesOriginales) {

                        if (unidadConstruccion.getAnioCancelacion() == null) {
                            PUnidadConstruccion puc = new PUnidadConstruccion(unidadConstruccion,
                                    this.proyectarConservacionMB.getUsuario().getLogin());
                            this.unidadConstruccionConvencionalVigentes
                                    .add(puc);
                        }

                    }
                }
                // Construcciones no convencionales vigentes
                if (unidadesNoConvencionalesOriginales != null &&
                     !unidadesNoConvencionalesOriginales.isEmpty()) {
                    for (UnidadConstruccion unidadConstruccion : unidadesNoConvencionalesOriginales) {

                        if (unidadConstruccion.getAnioCancelacion() == null) {
                            PUnidadConstruccion puc = new PUnidadConstruccion(unidadConstruccion,
                                    this.proyectarConservacionMB.getUsuario().getLogin());
                            this.unidadConstruccionNoConvencionalVigentes
                                    .add(puc);
                        }
                    }
                }

                // Construcciones convencionales nuevas
                this.unidadConstruccionConvencionalNuevasNoCanceladas = pPredioBase.
                    getPUnidadesConstruccionConvencional();
                // Construcciones no convencionales nuevas
                this.unidadConstruccionNoConvencionalNuevasNoCanceladas = pPredioBase.
                    getPUnidadesConstruccionNoConvencional();
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Método encargado de cargar las unidades de construcción vigentes asociadas a la ficha matriz
     * para los trámites de tercera masivos
     *
     * @author javier.aponte
     */
    public void cargarUnidadesConstruccionVigentesTerceraMasiva() {
        // Consulta del predio base con sus construcciones
        Predio predioBase = this.getConservacionService().getPredioFetchAvaluosByNumeroPredial(
            this.tramite.getPredio().getNumeroPredial());

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

        this.unidadConstruccionConvencionalVigentes = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalVigentes = new ArrayList<PUnidadConstruccion>();

        if (predioBase != null && predioBase.getUnidadConstruccions() != null &&
             !predioBase.getUnidadConstruccions().isEmpty()) {

            // Construcciones convencionales
            if (predioBase.getUnidadesConstruccionConvencional() != null &&
                 !predioBase.getUnidadesConstruccionConvencional().isEmpty()) {
                for (UnidadConstruccion puc : predioBase.getUnidadesConstruccionConvencional()) {

                    this.unidadConstruccionConvencionalVigentes.add(generalMB.
                        copiarUnidadConstruccionAPUnidadConstruccion(puc));

                }
            }
            // Construcciones no convencionales
            if (predioBase.getUnidadesConstruccionNoConvencional() != null &&
                 !predioBase.getUnidadesConstruccionNoConvencional().isEmpty()) {
                for (UnidadConstruccion puc : predioBase.getUnidadesConstruccionNoConvencional()) {

                    this.unidadConstruccionNoConvencionalVigentes.add(generalMB.
                        copiarUnidadConstruccionAPUnidadConstruccion(puc));

                }
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que inicializa las unidades de construcción a mostrar para los trámites de tercera
     * masiva
     *
     * @author javier.aponte
     */
    @SuppressWarnings("unchecked")
    public void setupUnidadesDeConstruccionTerceraMasiva() {

        // Inicializar variables de construcciones
        this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();

        try {
            // Consulta del predio base con sus construcciones
            // Cargue de las construcciones vigentes
            PPredio pPredioBase = this.getConservacionService().obtenerPPredioCompletoById(
                this.proyectarConservacionMB.getTramite().getPredio().getId());

            this.predioDesenglobe = this.getConservacionService().obtenerPPredioCompletoById(
                this.predioDesenglobe.getId());

            if (this.predioDesenglobe != null && pPredioBase != null &&
                 this.predioDesenglobe.getId().longValue() == pPredioBase.getId().longValue()) {

                if (this.predioDesenglobe.getPUnidadConstruccions() != null &&
                     !this.predioDesenglobe.getPUnidadConstruccions().isEmpty()) {

                    //Se adicionan las construcciones vigentes a las construcciones nuevas
                    if (this.predioDesenglobe.getPUnidadesConstruccionConvencional() != null &&
                         !this.predioDesenglobe.getPUnidadesConstruccionConvencional().isEmpty()) {

                        //Se adicionan las construcciones vigentes convencionales a las construcciones nuevas
                        for (PUnidadConstruccion pucConv : this.predioDesenglobe.
                            getPUnidadesConstruccionConvencional()) {

                            if (pucConv.getCancelaInscribe() == null ||
                                 (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pucConv.
                                    getCancelaInscribe()) &&
                                 pucConv.getProvienePredio() == null) ||
                                 EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pucConv.
                                    getCancelaInscribe())) {
                                this.unidadConstruccionConvencionalNuevas.add(pucConv);
                            }

                        }
                    }
                    if (this.predioDesenglobe.getPUnidadesConstruccionNoConvencional() != null &&
                         !this.predioDesenglobe.getPUnidadesConstruccionNoConvencional().isEmpty()) {

                        for (PUnidadConstruccion pucNoConv : this.predioDesenglobe.
                            getPUnidadesConstruccionNoConvencional()) {

                            //Se adicionan las construcciones vigentes no convencionales a las construcciones nuevas
                            if (pucNoConv.getCancelaInscribe() == null ||
                                 (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pucNoConv.
                                    getCancelaInscribe()) &&
                                 pucNoConv.getProvienePredio() == null) ||
                                 EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pucNoConv.
                                    getCancelaInscribe())) {
                                this.unidadConstruccionNoConvencionalNuevas.add(pucNoConv);
                            }
                        }
                    }
                }
            } //En este caso el predio seleccionado no es la ficha matriz
            else {
                if (!this.predioDesenglobe.isEsPredioFichaMatriz()) {
                    // Consultar las unidades de construcción nuevas de la ficha
                    // matriz.
                    if (this.fichaMatriz != null &&
                         this.fichaMatriz.getPPredio() != null) {
                        PPredio pPredioFichaMatriz = this
                            .getConservacionService()
                            .obtenerPPredioCompletoById(
                                this.fichaMatriz.getPPredio().getId());

                        if (pPredioFichaMatriz
                            .getPUnidadesConstruccionConvencional() != null &&
                             !pPredioFichaMatriz
                                .getPUnidadesConstruccionConvencional()
                                .isEmpty()) {

                            for (PUnidadConstruccion pucConv : pPredioFichaMatriz
                                .getPUnidadesConstruccionConvencional()) {

                                if (pucConv.getCancelaInscribe() != null &&
                                     (pucConv
                                        .getCancelaInscribe()
                                        .equals(EProyeccionCancelaInscribe.INSCRIBE
                                            .getCodigo()) || (pucConv
                                        .getCancelaInscribe()
                                        .equals(EProyeccionCancelaInscribe.MODIFICA
                                            .getCodigo()) && pucConv
                                        .getProvieneUnidad() != null))) {

                                    this.unidadConstruccionConvencionalNuevas
                                        .add(pucConv);
                                }
                            }
                        }
                        if (pPredioFichaMatriz
                            .getPUnidadesConstruccionNoConvencional() != null &&
                             !pPredioFichaMatriz
                                .getPUnidadesConstruccionNoConvencional()
                                .isEmpty()) {

                            for (PUnidadConstruccion pucNoConv : pPredioFichaMatriz
                                .getPUnidadesConstruccionNoConvencional()) {
                                if (pucNoConv.getCancelaInscribe() != null &&
                                     (pucNoConv
                                        .getCancelaInscribe()
                                        .equals(EProyeccionCancelaInscribe.INSCRIBE
                                            .getCodigo()) || (pucNoConv
                                        .getCancelaInscribe()
                                        .equals(EProyeccionCancelaInscribe.MODIFICA
                                            .getCodigo()) && pucNoConv
                                        .getProvieneUnidad() != null))) {

                                    this.unidadConstruccionNoConvencionalNuevas
                                        .add(pucNoConv);
                                }
                            }
                        }
                    }
                } //TODO :: javier.aponte :: Validar si este código se ejecuta en algún caso para las terceras masivas:: 16/07/2015
                else if (this.predioDesenglobe.getPUnidadConstruccions() != null &&
                     !this.predioDesenglobe.getPUnidadConstruccions().isEmpty()) {

                    //Adiciona a las unidades de construcción nuevas las unidades de construcción convencionales de la ficha matriz
                    if (this.predioDesenglobe.getPUnidadesConstruccionConvencional() != null &&
                         !this.predioDesenglobe.getPUnidadesConstruccionConvencional().isEmpty()) {

                        for (PUnidadConstruccion pucConv : this.predioDesenglobe.
                            getPUnidadesConstruccionConvencional()) {

                            if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pucConv.
                                getCancelaInscribe()) ||
                                 EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pucConv.
                                    getCancelaInscribe())) {

                                this.unidadConstruccionConvencionalNuevas.add(pucConv);

                            }
                        }
                    }

                    //Adiciona a las unidades de construcción nuevas las unidades de construcción no convencionales de la ficha matriz
                    if (this.predioDesenglobe.getPUnidadesConstruccionNoConvencional() != null &&
                         !this.predioDesenglobe.getPUnidadesConstruccionNoConvencional().isEmpty()) {

                        for (PUnidadConstruccion pucNoConv : this.predioDesenglobe.
                            getPUnidadesConstruccionNoConvencional()) {

                            if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pucNoConv.
                                getCancelaInscribe()) ||
                                 EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(pucNoConv.
                                    getCancelaInscribe())) {

                                this.unidadConstruccionNoConvencionalNuevas.add(pucNoConv);

                            }
                        }
                    }

                    // Debido a que no se tiene la manera explicita de saber si
                    // una unidad es la misma que la del predio base, se realiza
                    // un ordenamiento de las unidades proyectadas y se toma que
                    // las primeras unidades son las del predio base, teniendo
                    // encuenta la cantidad de unidades inicial.
                    int cantidadUnidadesPredioBase;
                    int i = 0;

                    // Cargue de los id de las unidades construcción del predio
                    // base
                    List<UnidadConstruccion> unidadConstruccionPredioBase =
                        this.getConservacionService().obtenerUnidadesConstruccionsPorNumeroPredial(
                            this.tramite.getPredio().getNumeroPredial());

                    if (unidadConstruccionPredioBase != null) {

                        cantidadUnidadesPredioBase = unidadConstruccionPredioBase.size();
                        Collections.sort(this.predioDesenglobe.getPUnidadConstruccions());
                        for (PUnidadConstruccion pu : this.predioDesenglobe.
                            getPUnidadConstruccions()) {
                            pu.setPertenecePredioProyectado(true);
                            i++;
                            if (i == cantidadUnidadesPredioBase) {
                                break;
                            }
                        }

                    }
                }
            }

            this.updateConstruccionesNuevasNoCanceladas();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que inicializa los modelos de construcción asociados a la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void setupModelosDeConstruccion() {
        ProyectarConservacionMB proyectarConservacionMB = (ProyectarConservacionMB) UtilidadesWeb
            .getManagedBean("proyectarConservacion");
        if (this.fichaMatriz != null &&
             this.fichaMatriz.getPFichaMatrizModelos() != null &&
             !this.fichaMatriz.getPFichaMatrizModelos().isEmpty()) {
            proyectarConservacionMB
                .setListModelosUnidadDeConstruccion(this.fichaMatriz
                    .getPFichaMatrizModelos());
        } else {
            List<PFichaMatrizModelo> nuevaListaModelos = new ArrayList<PFichaMatrizModelo>();
            proyectarConservacionMB
                .setListModelosUnidadDeConstruccion(nuevaListaModelos);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que elimina la PUnidadConstruccion seleccionada.
     *
     * @author fabio.navarrete
     */
    public void cancelarPUnidadConstruccion() {
        // si era null queda C
        // si era I se elimina
        // si era C se queda C No deberia darse
        // si era M queda C
        try {
            PUnidadConstruccion pUnidadConstruccionAux = (PUnidadConstruccion) this
                .getConservacionService().eliminarProyeccion(
                    MenuMB.getMenu().getUsuarioDto(),
                    this.unidadConstruccionSeleccionada);

            if (pUnidadConstruccionAux == null) {
                // Removerla de las unidades nuevas
                if (this.unidadConstruccionConvencionalNuevas
                    .contains(this.unidadConstruccionSeleccionada)) {
                    this.unidadConstruccionConvencionalNuevas
                        .remove(this.unidadConstruccionSeleccionada);
                } else if (this.unidadConstruccionNoConvencionalNuevas
                    .contains(this.unidadConstruccionSeleccionada)) {
                    this.unidadConstruccionNoConvencionalNuevas
                        .remove(this.unidadConstruccionSeleccionada);
                }

                this.predioDesenglobe = this.getConservacionService()
                    .obtenerPPredioCompletoById(
                        proyectarConservacionMB.getPredioSeleccionado()
                            .getId());

                //javier.aponte :: Para trámites de tercera se hace con la lista pPrediosTerceraRectificacionMasiva :: 28/07/2015
                if (this.tramite.isTerceraMasiva()) {
                    int idx = this.proyectarConservacionMB.getpPrediosTerceraRectificacionMasiva().
                        indexOf(this.predioDesenglobe);

                    if (idx >= 0) {
                        this.proyectarConservacionMB.getpPrediosTerceraRectificacionMasiva().remove(
                            idx);
                    }
                } else {
                    int idx = this.proyectarConservacionMB.getpPrediosDesenglobe()
                        .indexOf(this.predioDesenglobe);

                    if (idx >= 0) {
                        this.proyectarConservacionMB
                            .getpPrediosDesenglobe().remove(idx);
                    }
                }

                // Removerla del las pUnidadesConstruccion del predio.
                if (this.predioDesenglobe != null &&
                     this.predioDesenglobe.getPUnidadConstruccions() != null &&
                     this.predioDesenglobe.getPUnidadConstruccions()
                        .contains(this.unidadConstruccionSeleccionada)) {
                    this.predioDesenglobe.getPUnidadConstruccions().remove(
                        this.unidadConstruccionSeleccionada);
                    this.proyectarConservacionMB.getPredioSeleccionado()
                        .getPUnidadConstruccions()
                        .remove(this.unidadConstruccionSeleccionada);
                }

                //javier.aponte :: Para trámites de tercera se hace con la lista pPrediosTerceraRectificacionMasiva :: 28/07/2015
                if (this.tramite.isTerceraMasiva()) {
                    this.proyectarConservacionMB.getpPrediosTerceraRectificacionMasiva().add(
                        this.predioDesenglobe);
                } else {
                    this.proyectarConservacionMB.getpPrediosDesenglobe().add(this.predioDesenglobe);
                }
                this.addMensajeInfo("Unidad de construcción eliminada satisfactoriamente.");
            } else {
                this.unidadConstruccionSeleccionada
                    .setCancelaInscribe(pUnidadConstruccionAux
                        .getCancelaInscribe());
                this.addMensajeInfo("Unidad de construcción cancelada satisfactoriamente.");
            }

            if (this.tramite.isRectificacionMatriz()) {
                this.setupUnidadesDeConstruccionRectMatriz();
            } else if (this.tramite.isTerceraMasiva()) {
                this.setupUnidadesDeConstruccionTerceraMasiva();
            } else {
                this.setupUnidadesDeConstruccion();
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que inicializa una unidad de contrucción.
     */
    public void setupAdicionConstruccion() {
        this.proyectarConservacionMB.setTipoDominioComun(true);
        this.proyectarConservacionMB.crearNuevaUnidadConstruccion();
    }

    /**
     * Método que asocia las construcciones vigentes seleccionadas (convencionales y no
     * convencionales) a las nuevas.
     */
    public void asociarConstrucciones() {
        try {
            int cantidadConvencional = 0, cantidadNoConvencional = 0, cantidadUnidades;
            // LLamado al método que realiza el set de las unidades de
            // construcción seleccionadas.
            this.cargarUnidadesSeleccionadas();

            if (this.unidadConstruccionConvencionalVigentesSeleccionadas != null &&
                 this.unidadConstruccionConvencionalVigentesSeleccionadas.length > 0) {
                cantidadConvencional =
                    this.unidadConstruccionConvencionalVigentesSeleccionadas.length;
            }
            if (this.unidadConstruccionNoConvencionalVigentesSeleccionadas != null &&
                 this.unidadConstruccionNoConvencionalVigentesSeleccionadas.length > 0) {
                cantidadNoConvencional =
                    this.unidadConstruccionNoConvencionalVigentesSeleccionadas.length;
            }
            cantidadUnidades = cantidadNoConvencional + cantidadConvencional;

            if (cantidadUnidades < 1) {
                this.addMensajeWarn("Por favor seleccione las unidades a asociar.");
                return;
            } else {
                if (cantidadConvencional > 0) {
                    if (this.tramite.isTerceraMasiva()) {
                        if (this.unidadConstruccionConvencionalVigentesSeleccionadas != null &&
                             this.unidadConstruccionConvencionalVigentesSeleccionadas.length > 0 &&
                             this.unidadConstruccionConvencionalNuevas != null &&
                             !this.unidadConstruccionConvencionalNuevas.isEmpty()) {
                            for (PUnidadConstruccion puc
                                : this.unidadConstruccionConvencionalVigentesSeleccionadas) {
                                for (PUnidadConstruccion pucNuevas
                                    : this.unidadConstruccionConvencionalNuevas) {
                                    if (puc.getId().equals(pucNuevas.getId())) {
                                        this.addMensajeWarn(
                                            "Una de las construcciones convencionales seleccionadas ya se encuentra asociada");
                                        this.limpiarUnidadesConstruccionSeleccionadas();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    asociarConstruccionesConvencionalesVigentes();
                }
                if (cantidadNoConvencional > 0) {
                    if (this.tramite.isTerceraMasiva()) {
                        if (this.unidadConstruccionNoConvencionalVigentesSeleccionadas != null &&
                             this.unidadConstruccionNoConvencionalVigentesSeleccionadas.length > 0 &&
                             this.unidadConstruccionNoConvencionalNuevas != null &&
                             !this.unidadConstruccionNoConvencionalNuevas.isEmpty()) {
                            for (PUnidadConstruccion puc
                                : this.unidadConstruccionNoConvencionalVigentesSeleccionadas) {
                                for (PUnidadConstruccion pucNuevas
                                    : this.unidadConstruccionNoConvencionalNuevas) {
                                    if (puc.getId().equals(pucNuevas.getId())) {
                                        this.addMensajeWarn(
                                            "Una de las construcciones no convencionales seleccionadas ya se encuentra asociada");
                                        this.limpiarUnidadesConstruccionSeleccionadas();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    asociarConstruccionesNoConvencionalesVigentes();
                }
            }

            /// manejo de unidades de construcción
            if (this.tramite.isTerceraMasiva()) {
                this.setupUnidadesDeConstruccionTerceraMasiva();
            } else if (this.tramite.isRectificacionMatriz()) {
                this.setupUnidadesDeConstruccionRectMatriz();
            } else {
                this.setupUnidadesDeConstruccion();
            }

            this.proyectarConservacionMB.actualizarAvaluosYAreasFichaMatriz();

            this.limpiarUnidadesConstruccionSeleccionadas();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método encargado de limpiar las listas de unidades de construcción convencionales y no
     * convencionales vigentes seleccionadas
     *
     * @author javier.aponte
     */
    public void limpiarUnidadesConstruccionSeleccionadas() {

        this.unidadConstruccionConvencionalVigentesSeleccionadas = null;
        this.unidadConstruccionNoConvencionalVigentesSeleccionadas = null;

    }

    // ------------------------------------------------------------- //
    /**
     * Método que carga las unidades seleccionadas de la tabla de unidades de construcción vigentes.
     *
     * @author david.cifuentes
     */
    public void cargarUnidadesSeleccionadas() {

        List<PUnidadConstruccion> aux;

        if (this.unidadConstruccionConvencionalVigentes != null &&
             !this.unidadConstruccionConvencionalVigentes.isEmpty()) {

            aux = new ArrayList<PUnidadConstruccion>();
            for (PUnidadConstruccion uccv : this.unidadConstruccionConvencionalVigentes) {
                if (uccv.isSelected()) {
                    aux.add(uccv);
                }
            }
            if (aux.size() > 0) {
                this.unidadConstruccionConvencionalVigentesSeleccionadas =
                    new PUnidadConstruccion[aux
                        .size()];
                for (int i = 0; i < aux.size(); i++) {
                    this.unidadConstruccionConvencionalVigentesSeleccionadas[i] = aux
                        .get(i);
                }
            }
        }
        if (this.unidadConstruccionNoConvencionalVigentes != null &&
             !this.unidadConstruccionNoConvencionalVigentes.isEmpty()) {

            aux = new ArrayList<PUnidadConstruccion>();
            for (PUnidadConstruccion ucncv : this.unidadConstruccionNoConvencionalVigentes) {
                if (ucncv.isSelected()) {
                    aux.add(ucncv);
                }
            }
            if (aux.size() > 0) {
                this.unidadConstruccionNoConvencionalVigentesSeleccionadas =
                    new PUnidadConstruccion[aux
                        .size()];
                for (int i = 0; i < aux.size(); i++) {
                    this.unidadConstruccionNoConvencionalVigentesSeleccionadas[i] = aux
                        .get(i);
                }
            }
        }
    }

    // ------------------------------------------------------------- //
    // ------ MÉTODOS PARA LA GENERACIÓN DE NÚMEROS PREDIALES ------ //
    // ------------------------------------------------------------- //
    /**
     * Método que busca un número predial Disponible en la manzana del predio seleccionado
     *
     * @author david.cifuentes
     */
    public void buscarNumeroPredialDisponible() {
        try {
            this.numeroPredialDisponible = this.getConservacionService()
                .buscarUltimoNumeroPredialDisponible(
                    this.predioDesenglobe.getNumeroPredial());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Error al obtener el número predial disponible!");
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que carga la matriz de números prediales utilizada para llevar un seguimiento de los
     * números prediales que se han generado,los que están por generar, y controlar las unidades que
     * se pueden eliminar.
     *
     * @author david.cifuentes
     */
    public void cargarMatrizNumerosPrediales() {

        int prediosCreados = 0;
        this.numerosPredialesGeneradosPorPiso = null;
        this.numerosPredialesGeneradosPorSotano = null;

        try {
            if (this.tramite.isTipoInscripcionCondominio()) {

                // CONDOMINIO
                if (this.fichaMatriz != null &&
                     this.fichaMatriz.getTotalUnidadesPrivadas() != null &&
                     this.getFichaMatriz().getTotalUnidadesPrivadas() > 0L) {

                    // Se crea el arreglo con el tamaño de la cantidad de
                    // unidades privadas que va a tener el condominio.
                    this.numerosPredialesGeneradosCondominio = new int[this
                        .getFichaMatriz().getTotalUnidadesPrivadas()
                        .intValue() + 1];

                    // Se calcula el total de los predios creados.
                    if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                        if (!this.tramite.isDesenglobeEtapas()) {
                            prediosCreados = this.fichaMatriz
                                .getPFichaMatrizPredios().size();
                        } else {
                            prediosCreados = 0;
                            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                                if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                                    getCancelaInscribe())) {
                                    prediosCreados++;
                                }
                            }
                        }
                    }
                    // Se inicializan los campos del array correspondientes a
                    // los predios ya creados.
                    for (int i = 1; i <= prediosCreados; i++) {
                        this.numerosPredialesGeneradosCondominio[i] += 1;
                    }
                    this.conteoUnidad = Long.valueOf(prediosCreados);
                }

            } else if (this.tramite.isTipoInscripcionPH()) {

                // TIPO DE INSCRIPCIÓN - PH
                Long pisoMayor = 0L, sotanoMayor = 0L, torreMayor = 0L;
                Integer torre, pisoOSotano, esSotano;

                // Inicializar
                if (this.fichaMatriz != null &&
                     this.fichaMatriz.getPFichaMatrizTorres() != null &&
                     !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {
                    for (PFichaMatrizTorre pfmt : this.fichaMatriz
                        .getPFichaMatrizTorres()) {
                        if (pfmt.getPisos() != null &&
                             pfmt.getPisos() > pisoMayor) {
                            pisoMayor = pfmt.getPisos();
                        }
                        if (pfmt.getSotanos() != null &&
                             pfmt.getSotanos() > sotanoMayor) {
                            sotanoMayor = pfmt.getSotanos();
                        }
                        if (pfmt.getTorre() != null &&
                             pfmt.getTorre() > torreMayor) {
                            torreMayor = pfmt.getTorre();
                        }
                    }
                }
                if (torreMayor > 0 && pisoMayor > 0) {
                    this.numerosPredialesGeneradosPorPiso = new int[torreMayor
                        .intValue() + 1][pisoMayor.intValue() + 1];
                    this.numerosPredialesGeneradosPorSotano = new int[torreMayor
                        .intValue() + 1][sotanoMayor.intValue() + 1];
                }

                if (this.numerosPredialesGeneradosPorPiso != null ||
                     this.numerosPredialesGeneradosPorSotano != null) {
                    // Cargar Matriz
                    if (this.fichaMatriz != null &&
                         this.fichaMatriz.getPFichaMatrizPredios() != null &&
                         !this.fichaMatriz.getPFichaMatrizPredios()
                            .isEmpty()) {
                        for (PFichaMatrizPredio pfmp : this.fichaMatriz
                            .getPFichaMatrizPredios()) {
                            // torre
                            torre = Integer.valueOf(pfmp.getNumeroPredial()
                                .substring(22, 24));
                            // Piso
                            pisoOSotano = Integer.valueOf(pfmp
                                .getNumeroPredial().substring(24, 26));
                            // Si es o no sotano.
                            esSotano = Integer.valueOf(pfmp.getNumeroPredial()
                                .substring(24, 25));
                            if (esSotano.toString().equals("9")) {
                                int pisoOSotanoTranspuesto = 100 - pisoOSotano
                                    .intValue();
                                this.numerosPredialesGeneradosPorSotano[torre][pisoOSotanoTranspuesto] +=
                                    1;
                            } else {
                                if (torre != null && pisoOSotano != null) {
                                    this.numerosPredialesGeneradosPorPiso[torre][pisoOSotano] += 1;
                                }
                            }
                        }
                    }

                    LOGGER.debug(this
                        .imprimirMatriz(this.numerosPredialesGeneradosPorPiso));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error al cargar los números prediales");
        }
    }

    /**
     * Método que realiza el seteo de los números prediales originales de los predios que vienen en
     * firme.
     *
     * @author david.cifuentes
     */
    public void inicializarNumerosPredialesEtapas() {

        if (this.tramite.isDesenglobeEtapas() &&
             this.fichaMatriz.getPFichaMatrizPredios() != null) {
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (ESiNo.SI.getCodigo().equals(pfmp.getOriginalTramite()) && pfmp.
                    getNumeroPredialOriginal() != null) {
                    pfmp.setNumeroPredialOriginal(pfmp.getNumeroPredialOriginal());
                }
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que reinicia las variables de generación de números prediales
     *
     * @author david.cifuentes
     */
    public void reiniciarVariablesNumerosPrediales() {
        this.pisoOSotanoSeleccionado = "0";
        this.torreSeleccionada = "0";
        this.unidad = 0L;
    }

    // ---------------------------------------------------- //
    /**
     * Método que se encarga de realizar el llamado a los métodos que inicializan las variables
     * usadas al momento de adicionar un predio, y de buscar un número predial disponible.
     *
     * @author david.cifuentes
     */
    public void cargarPredios() {

        reiniciarVariablesNumerosPrediales();

        // Búsqueda de un número predial a asignar.
        if (this.numeroPredialDisponible == null ||
             this.numeroPredialDisponible.trim().isEmpty()) {
            buscarNumeroPredialDisponible();
        }
        // Búsqueda de un número predial a asignar.
        if (this.tramite.isEnglobeVirtual()) {
            this.predioCondProp8 = false;
            this.predioCondProp9 = false;

            Long primerProvieneFichaMatriz = 0L;
            Long provieneFichaMatriz = 0L;
            String primerCondicionNumeroPredial = new String();
            String primerTorreNumeroPredial = new String();
            String primerTorreNumPredialCobol = new String();
            String condPropPred = new String();
            String pisoPrimerPredio = new String();
            String piso = new String();
            String torreCobol = new String();
            boolean cobolPrimerPredio = false;
            boolean cobolPredio = false;

            //Carga Unidades Iniciales a modificar en Condominio
            this.cargarUnidadesInicialesDelCondominio();

            if (this.fichaMatrizPrediosSeleccionados != null &&
                 this.fichaMatrizPrediosSeleccionados.length > 0) {

                if (this.fichaMatrizPrediosSeleccionados[0].getProvieneFichaMatriz() != null) {

                    primerProvieneFichaMatriz =
                         this.fichaMatrizPrediosSeleccionados[0].getProvieneFichaMatriz().getId();
                }

                if (this.fichaMatrizPrediosSeleccionados[0].getNumeroPredialOriginal() != null &&
                     this.fichaMatrizPrediosSeleccionados[0].getNumeroPredialOriginal().
                        substring(21, 22) != null) {

                    primerCondicionNumeroPredial = this.fichaMatrizPrediosSeleccionados[0].
                        getNumeroPredialOriginal().substring(21, 22);
                }

                if (this.fichaMatrizPrediosSeleccionados[0].getNumeroPredial() != null &&
                     this.fichaMatrizPrediosSeleccionados[0].getNumeroPredial().substring(22, 24) !=
                    null) {

                    primerTorreNumeroPredial = this.fichaMatrizPrediosSeleccionados[0].
                        getNumeroPredial().substring(22, 24);

                }

                if (this.fichaMatrizPrediosSeleccionados[0].getNumeroPredialOriginal() != null) {

                    pisoPrimerPredio = this.fichaMatrizPrediosSeleccionados[0].
                        getNumeroPredialOriginal().substring(24, 26);
                    primerTorreNumPredialCobol = this.fichaMatrizPrediosSeleccionados[0].
                        getNumeroPredialOriginal().substring(22, 24);

                }

                if (pisoPrimerPredio != null && !pisoPrimerPredio.isEmpty() &&
                     primerTorreNumPredialCobol != null && !primerTorreNumPredialCobol.isEmpty() &&
                     ("00".equals(pisoPrimerPredio) || "00".equals(primerTorreNumPredialCobol))) {

                    cobolPrimerPredio = true;
                } else {
                    cobolPrimerPredio = false;
                }

                for (PFichaMatrizPredio pFichaMatrizPredio : this.fichaMatrizPrediosSeleccionados) {

                    if (pFichaMatrizPredio.getNumeroPredialOriginal() == null ||
                         ESiNo.NO.getCodigo().equals(pFichaMatrizPredio.getOriginalTramite())) {

                        this.addMensajeWarn("Los predios creados en el trámite son nuevos, " +
                             "no tienen la funcionalidad de modificar el número predial");
                        this.addErrorCallback();
                        return;

                    }

                    if (pFichaMatrizPredio.getNumeroPredialOriginal() != null) {

                        piso = pFichaMatrizPredio.getNumeroPredialOriginal().substring(24, 26);
                        torreCobol = pFichaMatrizPredio.getNumeroPredialOriginal().substring(22, 24);
                    }

                    if (torreCobol != null && !torreCobol.isEmpty() &&
                         piso != null && !piso.isEmpty() &&
                         ("00".equals(torreCobol) || "00".equals(piso))) {
                        cobolPredio = true;
                    } else {
                        cobolPredio = false;
                    }

                    if ((cobolPrimerPredio && !(cobolPredio)) ||
                         (!cobolPrimerPredio && cobolPredio)) {

                        this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_070
                            .getMensajeUsuario());
                        LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_070
                            .getMensajeTecnico());
                        this.addErrorCallback();
                        return;

                    }

                }

                for (PFichaMatrizPredio pFichaMatrizPredio : this.fichaMatrizPrediosSeleccionados) {

                    if (pFichaMatrizPredio.getNumeroPredialOriginal() != null &&
                         pFichaMatrizPredio.getNumeroPredialOriginal().substring(21, 22) != null &&
                         primerCondicionNumeroPredial != null &&
                         !pFichaMatrizPredio.getNumeroPredialOriginal().substring(21, 22).
                            equals(primerCondicionNumeroPredial)) {

                        this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_011
                            .getMensajeUsuario());
                        LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_011
                            .getMensajeTecnico());
                        this.addErrorCallback();
                        return;
                    }

                    if (pFichaMatrizPredio.getProvieneFichaMatriz() != null) {

                        provieneFichaMatriz =
                             pFichaMatrizPredio.getProvieneFichaMatriz().getId();

                        if (provieneFichaMatriz != primerProvieneFichaMatriz) {

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_010
                                .getMensajeUsuario());
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_010
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            return;

                        }
                    }

                    if (pFichaMatrizPredio.getNumeroPredial() != null &&
                         pFichaMatrizPredio.getNumeroPredial().substring(22, 24) != null &&
                         primerTorreNumeroPredial != null &&
                         !pFichaMatrizPredio.getNumeroPredial().substring(22, 24).
                            equals(primerTorreNumeroPredial)) {

                        this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_012
                            .getMensajeUsuario());
                        LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_012
                            .getMensajeTecnico());
                        this.addErrorCallback();
                        return;
                    }

                }

            }

            if (this.fichaMatrizPrediosSeleccionados != null) {
                for (PFichaMatrizPredio pFichaMatrizPredio1 : this.fichaMatrizPrediosSeleccionados) {

                    if (pFichaMatrizPredio1.getNumeroPredialOriginal() != null) {
                        condPropPred = pFichaMatrizPredio1.getNumeroPredialOriginal().substring(21,
                            22);
                    } else {
                        condPropPred = pFichaMatrizPredio1.getNumeroPredial().substring(21, 22);
                    }

                    if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condPropPred)) {
                        this.predioCondProp8 = true;
                        this.banderaAceptarNumPredial = false;
                        break;
                    }

                    if (EPredioCondicionPropiedad.CP_9.getCodigo().equals(condPropPred)) {
                        this.predioCondProp9 = true;
                        this.torreSeleccionada = primerTorreNumeroPredial;
                        this.banderaAceptarNumPredial = true;
                        break;
                    }
                }
            }

        }

        if (this.fichaMatriz.getPPredio().isMixto()) {
            cargarTorresDelPH();
            cargarUnidadesCondominio();
        }
        if (this.tramite.isTipoInscripcionPH()) {
            cargarTorresDelPH();
        } else if (this.tramite.isTipoInscripcionCondominio()) {
            cargarUnidadesCondominio();
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que carga las unidades y variables usadas para generar un número predial para el
     * condominio.
     *
     * @author david.cifuentes
     */
    public void cargarUnidadesCondominio() {

        this.torreSeleccionada = "0";
        this.pisoOSotanoSeleccionado = "0";
        this.setTorreCargadaBool(false);
        this.unidad = (this.unicaUnidadBool) ? 1L : 0L;

        this.unidadesTotales = 0L;
        this.unidadesRestantes = 0L;
        this.unidadesAsignadas = 0l;
        this.unidadesTotales = fichaMatriz.getTotalUnidadesPrivadas() +
             this.fichaMatriz.getTotalUnidadesSotanos();
        this.unidadesAsignadas = Long.valueOf(this.fichaMatriz
            .getPFichaMatrizPredios().size());
        this.unidadesRestantes = this.unidadesTotales - this.unidadesAsignadas;
        this.torreCargadaBool = false;
        this.ejeVialHaciaAbajo = ESiNo.NO.getCodigo();
    }

    // ---------------------------------------------------- //
    /**
     * Método que consulta las Torres de la ficha, y las almacena en un itemList.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unchecked")
    public void cargarTorresDelPH() {

        // Inicialización de variables
        this.pisoOSotanoBool = true;
        this.unidadesTotales = 0L;
        this.unidadesRestantes = 0L;
        this.unidadesAsignadas = 0L;
        this.unidad = (this.unicaUnidadBool) ? 1L : 0L;
        boolean torresIguales = true;
        Long auxCountSotanos = 0L;
        List<PFichaMatrizTorre> listaFichaMatrizTorres = new ArrayList<PFichaMatrizTorre>();
        SelectItem nombre, sel = new SelectItem();
        sel.setValue("0");
        sel.setLabel("Seleccionar...");
        this.itemListTorres = new ArrayList<SelectItem>();
        this.itemListTorres.add(sel);

        this.itemListPisos = new ArrayList<SelectItem>();
        this.itemListPisos.add(sel);
        this.itemListSotanos = new ArrayList<SelectItem>();
        this.itemListSotanos.add(sel);

        // Actualización de los valores de las unidades asignadas y restantes.
        if (this.fichaMatriz.getPFichaMatrizTorres() != null &&
             !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {

            for (PFichaMatrizTorre fmt : this.fichaMatriz
                .getPFichaMatrizTorres()) {
                this.unidadesTotales += fmt.getUnidades() +
                     fmt.getUnidadesSotanos();
            }
            if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                this.unidadesAsignadas = Long.valueOf(this.fichaMatriz.getPFichaMatrizPredios().
                    size());
            }
            this.unidadesRestantes = this.unidadesTotales -
                 this.unidadesAsignadas;
        }

        // Se realiza una copia de la lista de Torres, para crear los select
        // items de cada una.
        listaFichaMatrizTorres.addAll(this.fichaMatriz.getPFichaMatrizTorres());
        Collections.sort(listaFichaMatrizTorres);
        if (!listaFichaMatrizTorres.isEmpty()) {

            for (PFichaMatrizTorre fichaMatrizTorre : listaFichaMatrizTorres) {
                nombre = new SelectItem();

                if (this.tramite.isEnglobeVirtual()) {

                    if (fichaMatrizTorre.getTorre() != null &&
                         !EPredioEstado.CANCELADO.getCodigo().
                            equals(fichaMatrizTorre.getEstado())) {

                        nombre.setValue(fichaMatrizTorre.getTorre());
                        nombre.setLabel(fichaMatrizTorre.getTorre().toString());
                    }

                } else if (fichaMatrizTorre.getTorre() != null) {
                    nombre.setValue(fichaMatrizTorre.getTorre());
                    nombre.setLabel(fichaMatrizTorre.getTorre().toString());
                }

                if (this.fichaMatrizPrediosSeleccionados == null ||
                    this.fichaMatrizPrediosSeleccionados.length == 0) {
                    if (nombre != null && nombre.getValue() != null) {
                        this.itemListTorres.add(nombre);
                    }
                } else if (this.validarCargueTorresParaPrediosMigrados()) {

                    //@modified by leidy.gonzalez::#24523:: Valida que no se muestre el número de las torres 
                    //nuevas que se están creando en el trámite de englobe virtual.
                    if (this.tramite.isEnglobeVirtual()) {

                        if (!EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(
                            fichaMatrizTorre.getCancelaInscribe()) &&
                             !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(
                                fichaMatrizTorre.getCancelaInscribe()) &&
                             ESiNo.SI.getCodigo().equals(fichaMatrizTorre.getOriginalTramite())) {

                            if (nombre != null && nombre.getValue() != null) {
                                this.itemListTorres.add(nombre);
                            }

                        }
                    } else {
                        if (nombre != null && nombre.getValue() != null) {
                            this.itemListTorres.add(nombre);
                        }
                    }

                    this.activaPisoSotano = true;

                } else {
                    if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(fichaMatrizTorre.
                        getCancelaInscribe()) &&
                         ESiNo.SI.getCodigo().equals(fichaMatrizTorre.getOriginalTramite())) {
                        if (nombre != null && nombre.getValue() != null) {
                            this.itemListTorres.add(nombre);
                        }
                    }
                }

                if (fichaMatrizTorre.getSotanos() > 0L) {
                    auxCountSotanos++;
                }
            }

            Long auxPisos = this.fichaMatriz.getPFichaMatrizTorres().get(0)
                .getPisos();
            // Revisar si las torres son del mismo tamaÃ±o.
            for (PFichaMatrizTorre fichaMatrizTorre : listaFichaMatrizTorres) {
                if (!fichaMatrizTorre.getPisos().equals(auxPisos)) {
                    torresIguales = false;
                    break;
                }
            }

            if (!this.unicaUnidadBool &&
                 torresIguales &&
                 !(this.tramite.isDesenglobeEtapas() || this.tramite.isTerceraMasiva() ||
                this.tramite.isRectificacion() || this.tramite.isQuintaMasivo())) {
                SelectItem todas = new SelectItem();
                todas.setValue("TODAS");
                todas.setLabel("TODAS");
                this.itemListTorres.add(todas);
            }
        }
        this.setEjeVialHaciaAbajo(ESiNo.NO.getCodigo());
        this.sotanosBool = (auxCountSotanos > 0) ? true : false;
        this.activaPisoSotano = true;

        if (!this.sotanosBool && ESiNo.SI.getCodigo().equals(this.ejeVialHaciaAbajo)) {
            this.addMensajeWarn(
                "La torre seleccionada no tiene definida una cantidad para los sótanos, si desea asignar sótanos por favor edite la torre.");
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que marca como asignado el número predial en el arreglo correspondiente a condominio o
     * ph de numeros prediales generados.
     *
     * @author david.cifuentes
     */
    public void marcarNumeroPredialAsignado() {

        if (this.tramite.isTipoInscripcionCondominio()) {
            this.numerosPredialesGeneradosCondominio[this.conteoUnidad
                .intValue()] = 1;
        } else if (this.tramite.isTipoInscripcionPH()) {
            if (this.torreSeleccionada != null &&
                 this.pisoOSotanoSeleccionado != null) {

                int pisoOSotano = Integer.valueOf(this.pisoOSotanoSeleccionado);
                int torre = Integer.valueOf(this.torreSeleccionada);
                if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo())) {
                    this.numerosPredialesGeneradosPorSotano[torre][pisoOSotano] = this.conteoUnidad
                        .intValue();
                } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO.getCodigo())) {
                    this.numerosPredialesGeneradosPorPiso[torre][pisoOSotano] = this.conteoUnidad
                        .intValue();
                }
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que realiza el llamado a la generación de los números prediales respecto a su
     * condición de propiedad.
     *
     * @author david.cifuentes
     */
    public void crearNumerosPrediales() {

        // Validación de la cantidad de unidades restantes.
        actualizarUnidades();

        // Validación de unidades a crear
        if (this.unidad <= 0) {

            if (!this.tramite.isEnglobeVirtual()) {
                return;
            }
        } else if (this.unidad > this.unidadesRestantes) {
            String unidadesMsg = "restantes";
            if (this.tramite.isQuintaMasivo()) {
                unidadesMsg = "a crear";
            }
            this.addMensajeError(
                "Los números prediales que se quiere generar superan la cantidad de unidades " +
                unidadesMsg + ". ");
            this.addMensajeError("El total de unidades es: " +
                 this.unidadesTotales + ". Las unidades " + unidadesMsg + " son " +
                 this.unidadesRestantes + ".");
            return;
        }

        if (!this.tramite.isEnglobeVirtual()) {

            String condicionPropiedad = this.predioDesenglobe
                .getCondicionPropiedad();

            // Validación contra unidades restantes
            if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
                if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo()) && this.unidad.intValue() >
                    this.sotanosRestantesCondominio) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades de sótanos " +
                         "restantes que son: " + this.sotanosRestantesCondominio + ".");
                    return;
                } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO.getCodigo()) && this.unidad.
                    intValue() > this.unidadesRestantesCondominio) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades privadas " +
                         "restantes que son: " + this.unidadesRestantesCondominio + ".");
                    return;
                }
            } else if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo()) &&
                 this.torreSeleccionada != null &&
                 !this.torreSeleccionada.equals("TODAS")) {

                int torreSeleccionada = new Integer(this.torreSeleccionada);

                if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo()) && this.unidad.intValue() >
                    this.sotanosRestantesPorTorrePH[torreSeleccionada]) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades de sótanos restantes " +
                         "para la torre " + torreSeleccionada + " que son: " +
                        this.sotanosRestantesPorTorrePH[torreSeleccionada] + ".");
                    return;
                } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO.getCodigo()) && this.unidad.
                    intValue() > this.unidadesRestantesPorTorrePH[torreSeleccionada]) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades privadas restantes " +
                         "para la torre " + torreSeleccionada + " que son: " +
                        this.unidadesRestantesPorTorrePH[torreSeleccionada] + ".");
                    return;
                }
            }

            // Creación de números prediales
            if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8
                .getCodigo())) {
                crearNumerosPredialesCondominio();
                this.tramite.setTipoInscripcion(ETramiteTipoInscripcion.CONDOMINIO
                    .getCodigo());
            } else if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9
                .getCodigo())) {
                crearNumerosPredialesPH();
                this.tramite.setTipoInscripcion(ETramiteTipoInscripcion.PH
                    .getCodigo());
            }

        } else {
            //Valida creacion Numeros Prediales para tramites de Englobe Virtual
            crearNumerosPredialesEV();

            //Se verifica si la ficha matriz tien direccion, para asignarla a los nuevos predios
        }

        this.banderaPH = false;
        this.banderaCondominio = false;
        this.condicionPredioSeleccionada = null;

        this.proyectarConservacionMB.obligarGeografico();
    }

    // ---------------------------------------------------- //
    /**
     * Método que genera los números prediales para los condominios.
     *
     * @author david.cifuentes
     * @modified by leidy.gonzalez Se valida que se agregue la fecha de Inscripción catastral para
     * los condominios y PH generados del predio original
     */
    public void crearNumerosPredialesCondominio() {
        // v1.1.9
        PFichaMatrizPredio nuevoPFichaMatrizPredio;
        PPredio nuevoPPredio;
        List<PFichaMatrizPredio> prediosFMGenerados = new ArrayList<PFichaMatrizPredio>();
        List<PPredio> prediosGenerados = new ArrayList<PPredio>();
        StringBuilder nuevoNumeroPredial;
        StringBuilder parteFija = new StringBuilder();
        this.cargarMatrizNumerosPrediales();

        this.conteoUnidad = 0L;

        try {
            this.fichaMatrizPrediosResultantes = this.fichaMatriz.getPFichaMatrizPredios();
            if (this.fichaMatrizPrediosResultantes == null ||
                 this.fichaMatrizPrediosResultantes.size() <= 0) {
                this.fichaMatrizPrediosResultantes = new ArrayList<PFichaMatrizPredio>();
            }

            // PARTE FIJA DEL NÃMERO PREDIAL
            parteFija.append(this.predioDesenglobe.getNumeroPredial()
                .substring(0, 17));

            // PARTE VARIABLE DEL NÃMERO PREDIAL
            for (Long i = 1L; i <= this.unidad; i++) {

                // NUEVO NÃMERO PREDIAL
                nuevoNumeroPredial = new StringBuilder();
                nuevoNumeroPredial.append(parteFija);

                this.conteoUnidad += 1;

                // Consulta del predio base con sus construcciones.
                PPredio pPredioBase = this.getConservacionService()
                    .obtenerPPredioCompletoById(
                        this.tramite.getPredio().getId());

                if (pPredioBase == null) {
                    LOGGER.debug(
                        "Ocurrió un error al consultar el PPredioBase: FichaMatrizMB#crearNumerosPredialesCondominio");
                }

                // Si no se mantiene número predial
                // Terreno (4 posiciones): Debe ser corresponder al último
                // número de terreno + 1 que ha sido asignado en la manzana o
                // vereda
//				if (this.predioDesenglobe.getId().longValue() != pPredioBase
//						.getId().longValue()) {
//
//					nuevoNumeroPredial.append(this.numeroPredialDisponible
//							.substring(17, 21));
//
//				} else {
                // Terreno (4 posiciones): Debe corresponder al número del
                // terreno del predio que se está desenglobando
                nuevoNumeroPredial.append(this.predioDesenglobe
                    .getNumeroPredial().substring(17, 21));
//				}

                // Condición de Predio (1 posición): Se debe precargar el valor
                // 8.
                nuevoNumeroPredial.append(EPredioCondicionPropiedad.CP_8
                    .getCodigo());

                // No. Del Edificio o Torre (2 posiciones): Se deben cargar los
                // valores 00
                nuevoNumeroPredial.append("00");

                // No del piso dentro del edificio o torre (2 posiciones): Se
                // debe precargar con el valor de 00
                nuevoNumeroPredial.append("00");

                // No. de unidad en PH o mejora (4 posiciones): Debe
                // corresponder a un consecutivo
                // que se está generando de acuerdo al número de unidades que
                // tenga el condominio; empieza con 0001, 0002,â¦ y asÃ­
                // sucesivamente.
                int tempConteoUnidad = 0;
                if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                    int unidadMayor = 0;
                    for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                        Integer unidadAsignada = 0;

                        if (tramite.isEnglobeVirtual()) {
                            unidadAsignada = new Integer(pfmp.getNumeroPredial().substring(26, 30));
                            String condiPropiedad = pfmp.getNumeroPredial().substring(21, 22);

                            if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condiPropiedad)) {
                                if (unidadMayor <= unidadAsignada.intValue()) {
                                    unidadMayor = unidadAsignada.intValue();
                                }
                            }

                        } else {
                            unidadAsignada = new Integer(pfmp.getNumeroPredial().substring(26, 30));
                            if (unidadMayor <= unidadAsignada.intValue()) {
                                unidadMayor = unidadAsignada.intValue();
                            }
                        }
                    }
                    tempConteoUnidad = unidadMayor + i.intValue();
                }

                if (tempConteoUnidad < 10L) {
                    nuevoNumeroPredial.append("000" + String.valueOf(tempConteoUnidad));
                } else if (tempConteoUnidad < 100L) {
                    nuevoNumeroPredial.append("00" + String.valueOf(tempConteoUnidad));
                } else if (tempConteoUnidad < 1000L) {
                    nuevoNumeroPredial.append("0" + String.valueOf(tempConteoUnidad));
                } else {
                    nuevoNumeroPredial.append(String.valueOf(tempConteoUnidad));
                }

                marcarNumeroPredialAsignado();

                // SE ASIGNA EL NUEVO NÃMERO PREDIAL AL NUEVO OBJETO.
                // Se crea un nuevo PFichaMatrizPredio //
                nuevoPFichaMatrizPredio = new PFichaMatrizPredio();
                nuevoPFichaMatrizPredio.setNumeroPredial(nuevoNumeroPredial
                    .toString());

                nuevoPFichaMatrizPredio.setPFichaMatriz(this.fichaMatriz);
                nuevoPFichaMatrizPredio.setCoeficiente(0D);
                nuevoPFichaMatrizPredio
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                nuevoPFichaMatrizPredio.setOriginalTramite(ESiNo.NO.getCodigo());
                nuevoPFichaMatrizPredio.setUsuarioLog(this.usuarioFichaMatriz
                    .getLogin());
                nuevoPFichaMatrizPredio.setFechaLog(new Date(System
                    .currentTimeMillis()));

                prediosFMGenerados.add(nuevoPFichaMatrizPredio);

                // Se crea un nuevo PPredio //
                nuevoPPredio = new PPredio();
                nuevoPPredio
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                nuevoPPredio.setDepartamento(this.predioDesenglobe
                    .getDepartamento());
                nuevoPPredio.setMunicipio(this.predioDesenglobe.getMunicipio());
                nuevoPPredio.setZonaUnidadOrganica(this.predioDesenglobe
                    .getZonaUnidadOrganica());
                nuevoPPredio.setBarrioCodigo(this.predioDesenglobe
                    .getBarrioCodigo());
                nuevoPPredio.setSectorCodigo(this.predioDesenglobe
                    .getSectorCodigo());
                nuevoPPredio.setManzanaCodigo(this.predioDesenglobe
                    .getManzanaCodigo());
                nuevoPPredio.setCorregimientoCodigo(this.predioDesenglobe
                    .getCorregimientoCodigo());
                nuevoPPredio.setTramite(this.tramite);
                nuevoPPredio.setNumeroPredial(nuevoNumeroPredial.toString());
                nuevoPPredio.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                nuevoPPredio.setFechaLog(new Date(System.currentTimeMillis()));
                nuevoPPredio.setPredio(nuevoNumeroPredial.substring(17, 21));
                nuevoPPredio.setCondicionPropiedad(nuevoNumeroPredial
                    .substring(21, 22));
                nuevoPPredio.setEdificio(nuevoNumeroPredial.substring(22, 24));
                nuevoPPredio.setPiso(nuevoNumeroPredial.substring(24, 26));
                if (tempConteoUnidad < 10L) {
                    nuevoPPredio.setUnidad("000" + String.valueOf(tempConteoUnidad));
                } else if (tempConteoUnidad < 100L) {
                    nuevoPPredio.setUnidad("00" + String.valueOf(tempConteoUnidad));
                } else if (tempConteoUnidad < 1000L) {
                    nuevoPPredio.setUnidad("0" + String.valueOf(tempConteoUnidad));
                } else {
                    nuevoPPredio.setUnidad(String.valueOf(tempConteoUnidad));
                }

                nuevoPPredio.setTipo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                nuevoPPredio
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                nuevoPPredio.setNombre(this.proyectarConservacionMB
                    .getPredioSeleccionado().getNombre());
                nuevoPPredio.setTipoAvaluo(this.predioDesenglobe
                    .getTipoAvaluo());
                nuevoPPredio.setTipoCatastro(this.predioDesenglobe
                    .getTipoCatastro());

                nuevoPPredio.setEstado(EPredioEstado.ACTIVO.getCodigo());
                nuevoPPredio.setDestino(Constantes.CONSTANTE_CADENA_VACIA_DB);
                nuevoPPredio.setAreaTerreno(0D);
                nuevoPPredio.setAreaConstruccion(0D);
                nuevoPPredio.setOriginalTramite(ESiNo.NO.getCodigo());

                prediosGenerados.add(nuevoPPredio);

                this.unidadesAsignadas += 1L;
            }
            // Guardar PFichaMatrizPredios
            prediosFMGenerados = this.getConservacionService()
                .guardarPrediosFichaMatriz(prediosFMGenerados);

            // Guardar PPredios
            prediosGenerados = this.getConservacionService().guardarPPredios(
                prediosGenerados);

            // Se asocian los datos del predio base que deben persisitir..
            List<PPredio> listActualizarPpredio = new ArrayList<PPredio>();
            for (PPredio pp : prediosGenerados) {
                pp = asociarDatosBaseAlPPredio(pp);
                listActualizarPpredio.add(pp);
            }
            prediosGenerados = this.getConservacionService().guardarPPredios(
                listActualizarPpredio);

            this.fichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
                this.fichaMatriz.getPPredio().getId());

            this.proyectarConservacionMB
                .setFichaMatrizSeleccionada(this.fichaMatriz);

            if (this.tramite.isEnglobeVirtual()) {

                //Se agrega consulta la direccion principal de la ficha matriz
                this.consultarDireccionFM();
                //Se agrega direccion principal de la ficha matriz a los predios generados
                if (this.direccionPrincipalFM != null) {

                    this.asignarDireccionFMPrediosGenerados(prediosGenerados);
                }
            }

            if (this.tramite.isTipoInscripcionCondominio() ||
                 this.proyectarConservacionMB.getFichaMatrizSeleccionada().getPPredio().isMixto()) {

                this.consultarFichasMatrizCondominio();

            }

            this.getTotalUnidadesCondominioEV();

            actualizarUnidades();

            // Mensajes
            if (this.unidad > 1) {
                this.addMensajeInfo("Se generaron " + prediosFMGenerados.size() +
                     " números prediales!");
            } else {
                this.addMensajeInfo("Se generó " + this.unidad +
                     " número predial!");
            }
            if (this.unidadesRestantes != null && this.unidadesRestantes > 0) {
                this.addMensajeInfo("Faltan " + this.unidadesRestantes +
                     " unidades pendientes de generar su número predial");
            } else if (this.unidadesRestantes != null &&
                 this.unidadesRestantes.intValue() == 0) {
                this.addMensajeInfo("Se completó el total de números prediales");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(
                "No se pudieron generar los números prediales para el condominio, por favor intente nuevamente.");
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que genera los números prediales para los PH.
     *
     * @author david.cifuentes
     * @modified by leidy.gonzalez Se agrega a los nuevos predios PH la fecha de inscripción
     * catastral del predio original
     */
    public void crearNumerosPredialesPH() {

        Long cantidadTorres = 1L;
        Long cantidadPisoOSotano = 1L;
        Long numPisos = 0L;
        Long numSotanos = 0L;
        PFichaMatrizPredio nuevoPFichaMatrizPredio;
        PPredio nuevoPPredio;
        List<PFichaMatrizPredio> prediosFMGenerados = new ArrayList<PFichaMatrizPredio>();
        List<PPredio> prediosGenerados = new ArrayList<PPredio>();
        StringBuilder nuevoNumeroPredial;
        Vector<String> valorAuxiliar;
        String auxTodosLosPisos = "";
        StringBuilder parteFija = new StringBuilder();
        Long auxUnidadesACrear = 0L;

        // TamaÃ±os de las partes del número predial.
        int tamAuxiliar;
        final int terrenoTam = 4;
        final int torreTam = 2;
        final int pisoTam = 2;
        final int unidadTam = 4;

        try {
            String auxTorre = this.torreSeleccionada;
            this.fichaMatrizPrediosResultantes = this.fichaMatriz.getPFichaMatrizPredios();
            if (this.fichaMatrizPrediosResultantes == null ||
                 this.fichaMatrizPrediosResultantes.size() <= 0) {
                this.fichaMatrizPrediosResultantes = new ArrayList<PFichaMatrizPredio>();
            }

            // PARTE FIJA DEL NÃMERO PREDIAL
            parteFija.append(this.numeroPredialDisponible.substring(0, 17));

            if (auxTorre.equals("TODAS")) {
                cantidadTorres = Long.valueOf(this.fichaMatriz
                    .getPFichaMatrizTorres().size());
            }

            // PARTE VARIABLE DEL NÃMERO PREDIAL
            String pisoOSotanoSeleccionadoAux = this.pisoOSotanoSeleccionado;
            auxTodosLosPisos = this.pisoOSotanoSeleccionado;
            for (Long k = 1L; k <= cantidadTorres; k++) {
                if (auxTorre.equals("TODAS")) {
                    this.torreSeleccionada = String.valueOf(k);
                }
                if (pisoOSotanoSeleccionadoAux.equals("TODOS")) {
                    for (PFichaMatrizTorre pfmt : this.fichaMatriz
                        .getPFichaMatrizTorres()) {
                        if (pfmt.getTorre() != null &&
                             pfmt.getTorre().toString()
                                .equals(this.torreSeleccionada.toString())) {
                            numPisos = pfmt.getPisos();
                            numSotanos = pfmt.getSotanos();
                            break;
                        }
                    }
                    cantidadPisoOSotano = (this.ejeVialHaciaAbajo
                        .equals(ESiNo.SI.getCodigo())) ? numSotanos :
                         numPisos;
                }

                auxUnidadesACrear = cantidadPisoOSotano * this.unidad;
                if (this.unidadesRestantes < auxUnidadesACrear) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades restantes. ");
                    this.addMensajeError("El total de unidades es: " +
                         this.unidadesTotales +
                         ". Las unidades restantes son " +
                         this.unidadesRestantes + ".");
                    return;
                }

                Integer unidadesRestantesTorre = 0;
                int totalAsignados = 0;
//				int totalCancelados = 0;
                Long numerosAGenerar = 0L;

                this.cargarMatrizNumerosPrediales();

                if (!this.tramite.isDesenglobeEtapas() && !this.tramite.isQuintaMasivo()) {
                    // Calcula la suma de las unidades generadas en los
                    // sotanos y en los pisos para una torre seleccionada.
                    for (int x = 0; x < this.numerosPredialesGeneradosPorPiso[Integer
                        .valueOf(this.torreSeleccionada)].length - 1; x++) {
                        totalAsignados += this.numerosPredialesGeneradosPorPiso[Integer
                            .valueOf(this.torreSeleccionada)][x];
                        if (this.numerosPredialesGeneradosPorSotano[Integer
                            .valueOf(this.torreSeleccionada)].length > x) {
                            totalAsignados += this.numerosPredialesGeneradosPorSotano[Integer
                                .valueOf(this.torreSeleccionada)][x];
                        }
                    }
                } else {
//					totalCancelados = this.verificarPrediosCanceladosPorTorre(this.torreSeleccionada);
                    totalAsignados = verificarPrediosPisoAsignadosPorTorre(this.torreSeleccionada) +
                        verificarPrediosSotanoAsignadosPorTorre(this.torreSeleccionada);
                }
                numerosAGenerar = this.unidad;

                for (PFichaMatrizTorre pfmt : this.fichaMatriz
                    .getPFichaMatrizTorres()) {
                    if (pfmt.getTorre() != null &&
                         pfmt.getTorre().toString()
                            .equals(this.torreSeleccionada)) {
                        unidadesRestantesTorre = (int) (pfmt.getUnidades() +
                             pfmt.getUnidadesSotanos() - totalAsignados);
                        if (auxTodosLosPisos.equals("TODOS")) {
                            if (this.ejeVialHaciaAbajo.equals(ESiNo.SI
                                .getCodigo())) {
                                numerosAGenerar = pfmt.getSotanos();
                            } else {
                                numerosAGenerar = pfmt.getPisos() * this.unidad;
                            }
                        }
                        break;
                    }
                }

                if (numerosAGenerar > Long.valueOf(unidadesRestantesTorre)) {
                    this.addMensajeError(
                        "El número de unidades que desea crear supera las disponibles para esa torre, que son " +
                         unidadesRestantesTorre +
                         ". Por favor intente de nuevo.");
                    return;
                }

                for (Long j = 1L; j <= cantidadPisoOSotano; j++) {
                    if (auxTodosLosPisos.equals("TODOS")) {
                        this.pisoOSotanoSeleccionado = String.valueOf(j);
                    }
                    for (Long i = 1L; i <= this.unidad; i++) {

                        // NUEVO NÃMERO PREDIAL
                        nuevoNumeroPredial = new StringBuilder();
                        nuevoNumeroPredial.append(parteFija);

                        if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo())) {
                            if (this.numerosPredialesGeneradosPorSotano[Integer
                                .valueOf(this.torreSeleccionada)][Integer
                                .valueOf(this.pisoOSotanoSeleccionado)] == 0) {
                                this.conteoUnidad = i;
                            } else {
                                this.conteoUnidad = Long
                                    .valueOf(this.numerosPredialesGeneradosPorSotano[Integer
                                        .valueOf(this.torreSeleccionada)][Integer
                                        .valueOf(this.pisoOSotanoSeleccionado)]);
                                this.conteoUnidad += 1;
                            }
                        } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO
                            .getCodigo())) {
                            if (this.numerosPredialesGeneradosPorPiso[Integer
                                .valueOf(this.torreSeleccionada)][Integer
                                .valueOf(this.pisoOSotanoSeleccionado)] == 0) {
                                this.conteoUnidad = i;
                            } else {
                                this.conteoUnidad = Long
                                    .valueOf(this.numerosPredialesGeneradosPorPiso[Integer
                                        .valueOf(this.torreSeleccionada)][Integer
                                        .valueOf(this.pisoOSotanoSeleccionado)]);
                                this.conteoUnidad += 1;
                            }
                        }
                        tamAuxiliar = 0;
                        this.predioDesenglobe = this.proyectarConservacionMB
                            .getPredioSeleccionado();
                        // ES DESENGLOBE DE PROPIEDAD HORIZONTAL
                        if (this.predioDesenglobe != null &&
                             this.predioDesenglobe.getPredio() != null) {
                            tamAuxiliar = terrenoTam -
                                 this.predioDesenglobe.getPredio().length();
                        }

                        valorAuxiliar = new Vector<String>();
                        if (tamAuxiliar > 0) {
                            valorAuxiliar.setSize(tamAuxiliar);
                            Collections.fill(valorAuxiliar, "0");
                            nuevoNumeroPredial.append(StringUtils.join(
                                valorAuxiliar, "").concat(
                                    this.predioDesenglobe.getPredio()));
                        } else {
                            nuevoNumeroPredial.append(this.predioDesenglobe
                                .getPredio());
                        }

                        // Tipo de inscripción.
                        nuevoNumeroPredial.append(this.tramite
                            .getTipoInscripcion());

                        // Edificio o torre
                        if (this.torreSeleccionada != null) {
                            tamAuxiliar = torreTam -
                                 this.torreSeleccionada.length();
                            valorAuxiliar = new Vector<String>();
                            if (tamAuxiliar > 0) {
                                valorAuxiliar.setSize(tamAuxiliar);
                                Collections.fill(valorAuxiliar, "0");
                                nuevoNumeroPredial.append(StringUtils.join(
                                    valorAuxiliar, "").concat(
                                        this.torreSeleccionada));
                            } else {
                                nuevoNumeroPredial
                                    .append(this.torreSeleccionada);
                            }
                        } else {
                            nuevoNumeroPredial.append("00");
                        }

                        // Piso
                        if (this.pisoOSotanoSeleccionado != null) {

                            if (this.ejeVialHaciaAbajo.equals(ESiNo.NO
                                .getCodigo())) {
                                tamAuxiliar = pisoTam -
                                     this.pisoOSotanoSeleccionado.length();
                                valorAuxiliar = new Vector<String>();
                                if (tamAuxiliar > 0) {
                                    valorAuxiliar.setSize(tamAuxiliar);
                                    Collections.fill(valorAuxiliar, "0");
                                    nuevoNumeroPredial.append(StringUtils.join(
                                        valorAuxiliar, "").concat(
                                            this.pisoOSotanoSeleccionado));
                                } else {
                                    nuevoNumeroPredial
                                        .append(this.pisoOSotanoSeleccionado);
                                }
                            } else {
                                nuevoNumeroPredial.append(100 - Integer
                                    .valueOf(this.pisoOSotanoSeleccionado));
                            }
                        } else {
                            nuevoNumeroPredial.append("00");
                        }

                        // Unidad
                        tamAuxiliar = unidadTam - 1 -
                             this.conteoUnidad.toString().length();

                        nuevoNumeroPredial.append("0");
                        valorAuxiliar = new Vector<String>();
                        if (tamAuxiliar > 0) {
                            valorAuxiliar.setSize(tamAuxiliar);
                            Collections.fill(valorAuxiliar, "0");
                            nuevoNumeroPredial.append(StringUtils.join(
                                valorAuxiliar, "").concat(
                                    this.conteoUnidad.toString()));
                        } else {
                            nuevoNumeroPredial.append(this.conteoUnidad
                                .toString());
                        }

                        marcarNumeroPredialAsignado();

                        // SE ASIGNA EL NUEVO NÃMERO PREDIAL AL NUEVO OBJETO.
                        // Se crea el nuevo PFichaMatrizPredio //
                        nuevoPFichaMatrizPredio = new PFichaMatrizPredio();
                        nuevoPFichaMatrizPredio
                            .setNumeroPredial(nuevoNumeroPredial.toString());
                        nuevoPFichaMatrizPredio
                            .setPFichaMatriz(this.fichaMatriz);
                        nuevoPFichaMatrizPredio.setCoeficiente(0D);
                        nuevoPFichaMatrizPredio
                            .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo());
                        nuevoPFichaMatrizPredio
                            .setUsuarioLog(this.usuarioFichaMatriz
                                .getLogin());
                        nuevoPFichaMatrizPredio.setFechaLog(new Date(System
                            .currentTimeMillis()));
                        nuevoPFichaMatrizPredio.setOriginalTramite(ESiNo.NO.getCodigo());

                        prediosFMGenerados.add(nuevoPFichaMatrizPredio);

                        // --- Nuevo PPredio ---//
                        nuevoPPredio = new PPredio();
                        nuevoPPredio
                            .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo());
                        nuevoPPredio.setDepartamento(this.predioDesenglobe
                            .getDepartamento());
                        nuevoPPredio.setMunicipio(this.predioDesenglobe
                            .getMunicipio());
                        nuevoPPredio
                            .setZonaUnidadOrganica(this.predioDesenglobe
                                .getZonaUnidadOrganica());
                        nuevoPPredio.setBarrioCodigo(this.predioDesenglobe
                            .getBarrioCodigo());
                        nuevoPPredio.setSectorCodigo(this.predioDesenglobe
                            .getSectorCodigo());
                        nuevoPPredio.setManzanaCodigo(this.predioDesenglobe
                            .getManzanaCodigo());
                        nuevoPPredio
                            .setCorregimientoCodigo(this.predioDesenglobe
                                .getCorregimientoCodigo());
                        nuevoPPredio.setTramite(this.tramite);
                        nuevoPPredio.setNumeroPredial(nuevoNumeroPredial
                            .toString());
                        nuevoPPredio.setUsuarioLog(this.usuarioFichaMatriz
                            .getLogin());
                        nuevoPPredio.setFechaLog(new Date(System
                            .currentTimeMillis()));
                        nuevoPPredio.setPredio(nuevoNumeroPredial.substring(17,
                            21));
                        nuevoPPredio.setCondicionPropiedad(nuevoNumeroPredial
                            .substring(21, 22));
                        nuevoPPredio.setEdificio(nuevoNumeroPredial.substring(
                            22, 24));
                        nuevoPPredio.setPiso(nuevoNumeroPredial.substring(24,
                            26));
                        nuevoPPredio.setUnidad(nuevoNumeroPredial.substring(26,
                            30));
                        nuevoPPredio.setNombre(this.proyectarConservacionMB
                            .getPredioSeleccionado().getNombre());
                        nuevoPPredio
                            .setTipo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                        nuevoPPredio.setTipoAvaluo(this.predioDesenglobe
                            .getTipoAvaluo());
                        nuevoPPredio.setTipoCatastro(this.predioDesenglobe
                            .getTipoCatastro());

                        nuevoPPredio
                            .setEstado(EPredioEstado.ACTIVO.getCodigo());
                        nuevoPPredio
                            .setDestino(Constantes.CONSTANTE_CADENA_VACIA_DB);
                        nuevoPPredio.setAreaTerreno(0D);
                        nuevoPPredio.setAreaConstruccion(0D);

                        nuevoPPredio.setOriginalTramite(ESiNo.NO.getCodigo());

                        prediosGenerados.add(nuevoPPredio);

                        this.unidadesAsignadas += 1L;

                        // Código agregado para visualizar los datos de la
                        // matriz en consola.
                        LOGGER.debug("****** Predio creado ********* torre:" +
                             k +
                             " piso:" +
                             j +
                             " unidad: " +
                             i +
                             "*******Predios:" +
                             imprimirMatriz(this.numerosPredialesGeneradosPorPiso));
                    }
                }
            }
            // Guardar PFichaMatrizPredios
            prediosFMGenerados = this.getConservacionService()
                .guardarPrediosFichaMatriz(prediosFMGenerados);
            // Guardar PPredios
            prediosGenerados = this.getConservacionService().guardarPPredios(
                prediosGenerados);

            // Se asocian los datos del predio base que deben persistir.
            List<PPredio> listActualizarPpredio = new ArrayList<PPredio>();
            for (PPredio pp : prediosGenerados) {
                pp = asociarDatosBaseAlPPredio(pp);
                listActualizarPpredio.add(pp);
            }
            prediosGenerados = this.getConservacionService().guardarPPredios(
                listActualizarPpredio);

            this.fichaMatriz.getPFichaMatrizPredios().addAll(prediosFMGenerados);

            this.proyectarConservacionMB
                .setFichaMatrizSeleccionada(this.fichaMatriz);

            if (this.tramite.isEnglobeVirtual()) {
                //Se agrega consulta la direccion principal de la ficha matriz
                this.consultarDireccionFM();
                //Se agrega direccion principal de la ficha matriz a los predios generados
                if (this.direccionPrincipalFM != null) {

                    this.asignarDireccionFMPrediosGenerados(prediosGenerados);
                }
            }

            actualizarUnidades();
            Long generados = this.unidad * cantidadTorres * cantidadPisoOSotano;

            if (generados > 1) {
                this.addMensajeInfo("Se generaron " + prediosFMGenerados.size() +
                     " números prediales!");
            } else {
                this.addMensajeInfo("Se generó " + this.unidad +
                     " número predial!");
            }
            if (this.unidadesRestantes != null && this.unidadesRestantes > 0) {
                this.addMensajeInfo("Faltan " + this.unidadesRestantes +
                     " unidades pendientes de generar su número predial");
            } else if (this.unidadesRestantes != null &&
                 this.unidadesRestantes.intValue() == 0) {
                this.addMensajeInfo("Se completó el total de números prediales");
            }
            this.pisoOSotanoBool = false;
            this.unicaUnidadBool = false;
            this.torreCargadaBool = true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError("No se pudieron generar los números prediales!");
        }
    }

    /**
     * Método para imprimir en consola la matriz de números prediales generados.
     *
     * @author david.cifuentes
     */
    public String imprimirMatriz(int[][] a) {
        String matriz = "";
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                matriz += "+" + a[i][j];
            }
            matriz += "----";
        }
        return matriz;
    }

    // ---------------------------------------------------- //
    /**
     * Método que asocia al nuevo Predio los datos del predio base que deben persistir, tales como
     * referencias cartograficas, direcciones y propietarios.
     *
     * * @david.cifuentes
     */
    public PPredio asociarDatosBaseAlPPredio(PPredio nuevoPPredio) {

        if (this.predioDesenglobe != null) {

            // DIRECCIONES
            if (this.predioDesenglobe.getPPredioDireccions() != null &&
                 !this.predioDesenglobe.getPPredioDireccions().isEmpty()) {

                nuevoPPredio
                    .setPPredioDireccions(new ArrayList<PPredioDireccion>());
                PPredioDireccion nuevoPPredioDireccion;
                for (PPredioDireccion ppd : this.predioDesenglobe
                    .getPPredioDireccions()) {
                    // Set de la dirección
                    nuevoPPredioDireccion = (PPredioDireccion) ppd.clone();
                    nuevoPPredioDireccion.setId(null);
                    nuevoPPredioDireccion
                        .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo());
                    nuevoPPredioDireccion.setFechaLog(new Date(System
                        .currentTimeMillis()));
                    nuevoPPredioDireccion.setUsuarioLog(this.usuarioFichaMatriz
                        .getLogin());
                    nuevoPPredioDireccion.setPPredio(nuevoPPredio);
                    // Se asocia la dirección
                    nuevoPPredio.getPPredioDireccions().add(
                        nuevoPPredioDireccion);
                }
            }
            // REFERENCIAS CARTOGRÁFICAS
            if (this.predioDesenglobe.getPReferenciaCartograficas() != null &&
                 !this.predioDesenglobe.getPReferenciaCartograficas()
                    .isEmpty()) {

                nuevoPPredio
                    .setPReferenciaCartograficas(new ArrayList<PReferenciaCartografica>());
                PReferenciaCartografica nuevaPReferenciaCartografica;

                for (PReferenciaCartografica prc : this.predioDesenglobe
                    .getPReferenciaCartograficas()) {
                    // Set de la referencia cartográfica
                    nuevaPReferenciaCartografica = (PReferenciaCartografica) prc
                        .clone();
                    nuevaPReferenciaCartografica.setId(null);
                    nuevaPReferenciaCartografica
                        .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo());
                    nuevaPReferenciaCartografica.setFechaLog(new Date(System
                        .currentTimeMillis()));
                    nuevaPReferenciaCartografica
                        .setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                    nuevaPReferenciaCartografica.setPPredio(nuevoPPredio);
                    // Se asocia la dirección
                    nuevoPPredio.getPReferenciaCartograficas().add(
                        nuevaPReferenciaCartografica);
                }
            }

            // PROPIETARIOS Y POSEEDORES
            if (this.predioDesenglobe.getPPersonaPredios() != null &&
                 !this.predioDesenglobe.getPPersonaPredios().isEmpty()) {

                nuevoPPredio
                    .setPPersonaPredios(new ArrayList<PPersonaPredio>());
                PPersonaPredio nuevaPPersonaPredio;

                for (PPersonaPredio ppp : this.predioDesenglobe
                    .getPPersonaPredios()) {
                    // Set del propietario poseedor.
                    nuevaPPersonaPredio = (PPersonaPredio) ppp.clone();
                    nuevaPPersonaPredio.setId(null);
                    nuevaPPersonaPredio
                        .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo());
                    nuevaPPersonaPredio.setFechaLog(new Date(System
                        .currentTimeMillis()));
                    nuevaPPersonaPredio.setUsuarioLog(this.usuarioFichaMatriz
                        .getLogin());
                    nuevaPPersonaPredio.setPPredio(nuevoPPredio);
                    // Se asocia el propietario poseedor
                    nuevoPPredio.getPPersonaPredios().add(nuevaPPersonaPredio);
                }
            }
        }
        return nuevoPPredio;
    }

    // ---------------------------------------------------- //
    /**
     * Método que retira un fichaMatrizPredio eliminando su número predial.
     *
     * * @david.cifuentes
     */
    public void retirarNumeroPredial() {

        List<PFichaMatrizPredio> auxEliminar = new ArrayList<PFichaMatrizPredio>();
        List<PFichaMatrizPredio> auxDesactivar = new ArrayList<PFichaMatrizPredio>();

        List<String> auxNumerosPredialesAEliminar = new ArrayList<String>();
        List<String> auxNumerosPredialesADesactivar = new ArrayList<String>();

        if (this.validarRetirarNumeroPredial(true)) {

            for (int i = 0; i < this.fichaMatrizPrediosSeleccionados.length; i++) {
                if (this.tramite.isDesenglobeEtapas() &&
                     ESiNo.SI.getCodigo().equals(this.fichaMatrizPrediosSeleccionados[i].
                        getOriginalTramite())) {

                    this.fichaMatrizPrediosSeleccionados[i].setCancelaInscribe(
                        EProyeccionCancelaInscribe.CANCELA.getCodigo());
                    auxDesactivar.add(this.fichaMatrizPrediosSeleccionados[i]);
                    auxNumerosPredialesADesactivar.add(this.fichaMatrizPrediosSeleccionados[i].
                        getNumeroPredial());
                } else if (this.tramite.isQuintaMasivo()) {

                    // Verificar si el estado original del predio era reactivado
                    if (EPredioEstado.REACTIVADO.getCodigo().equals(
                        this.fichaMatrizPrediosSeleccionados[i].getEstado())) {
                        if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(
                            this.fichaMatrizPrediosSeleccionados[i].getCancelaInscribe())) {
                            auxEliminar.add(this.fichaMatrizPrediosSeleccionados[i]);
                            auxNumerosPredialesAEliminar.add(
                                this.fichaMatrizPrediosSeleccionados[i].getNumeroPredial());
                        } else if (EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(
                            this.fichaMatrizPrediosSeleccionados[i].getCancelaInscribe()) ||
                             this.fichaMatrizPrediosSeleccionados[i].getCancelaInscribe() == null) {
                            this.fichaMatrizPrediosSeleccionados[i].setCancelaInscribe(null);
                            this.fichaMatrizPrediosSeleccionados[i].setEstado(
                                EPredioEstado.CANCELADO.getCodigo());
                            auxDesactivar.add(this.fichaMatrizPrediosSeleccionados[i]);
                            auxNumerosPredialesADesactivar.add(
                                this.fichaMatrizPrediosSeleccionados[i].getNumeroPredial());
                        }
                        // Si el predio no era cancelado se verifica que el predio no este en firme.
                    } else if (ESiNo.SI.getCodigo().equals(this.fichaMatrizPrediosSeleccionados[i].
                        getOriginalTramite())) {

                        String condiPropiedad = this.fichaMatrizPrediosSeleccionados[i].
                            getNumeroPredial().substring(21, 22);
                        String msgCondicionPropiedad = "";
                        if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condiPropiedad)) {
                            msgCondicionPropiedad = "en el Condominio";
                        } else if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condiPropiedad)) {
                            msgCondicionPropiedad = "en el PH";
                        }
                        this.addMensajeError("Los predios ya existen " + msgCondicionPropiedad +
                            ", estos no se pueden eliminar debido a que están en firme.");
                    } else {
                        auxEliminar.add(this.fichaMatrizPrediosSeleccionados[i]);
                        auxNumerosPredialesAEliminar.add(this.fichaMatrizPrediosSeleccionados[i].
                            getNumeroPredial());
                    }
                } else {
                    auxEliminar.add(this.fichaMatrizPrediosSeleccionados[i]);
                    auxNumerosPredialesAEliminar.add(this.fichaMatrizPrediosSeleccionados[i].
                        getNumeroPredial());
                }
            }
            if (auxEliminar.size() > 0) {
                this.fichaMatriz.getPFichaMatrizPredios()
                    .removeAll(auxEliminar);

                // ELIMINACIÓN DE LOS PFichaMatrizPredio SELECCIONADOS DE LA
                // FICHA MATRIZ
                this.getConservacionService().eliminarPrediosFichaMatriz(
                    auxEliminar);
                // SE DESASOCIAN LOS PPredios DEL TRÁMITE
                retirarPrediosSeleccionados(auxNumerosPredialesAEliminar);
            }

            if (auxDesactivar.size() > 0) {
                this.fichaMatriz.getPFichaMatrizPredios()
                    .removeAll(auxDesactivar);

                // DESACTIVAR LOS PFichaMatrizPredio SELECCIONADOS QUE ESTÁN EN FIRME DE LA
                // FICHA MATRIZ
                this.getConservacionService().actualizarListaPFichaMatrizPredios(auxDesactivar);

                // SE DESASOCIAN LOS PPredios DEL TRÁMITE
                retirarPrediosSeleccionados(auxNumerosPredialesADesactivar);
            }

            if (auxDesactivar.size() > 0 || auxEliminar.size() > 0) {
                if (this.tramite.isTipoInscripcionCondominio() && this.tramite.isDesenglobeEtapas() &&
                    this.fichaMatriz.getPFichaMatrizPredios().size() == 0) {
                    this.addMensajeInfo(
                        "Se retiraron los números prediales satisfactoriamente, recuerde que el condominio debe tener al menos un predio.");
                } else {
                    this.addMensajeInfo("Se retiraron los números prediales satisfactoriamente!");
                }

                this.proyectarConservacionMB.obligarGeografico();
                this.fichaMatrizPrediosSeleccionados = null;
                this.selectedAllActivarPrediosBool = false;
                this.selectedAllBool = false;
            }
            this.fichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
                this.fichaMatriz.getPPredio().getId());
            this.verificarTorresEnFirme();
            this.proyectarConservacionMB.setFichaMatrizSeleccionada(this.fichaMatriz);
            this.proyectarConservacionMB.obligarGeografico();
        }
        this.cargarMatrizNumerosPrediales();
    }

    /**
     * Método para actualizar los predios que fueron desactivados de la ficha matriz.
     *
     * @author david.cifuentes
     * @param auxDesactivar
     */
    @SuppressWarnings("unused")
    private void desactivarPrediosSeleccionados(
        List<String> auxDesactivar) {

        List<PPredio> pPrediosDesenglobe = new ArrayList<PPredio>();
        List<PPredio> desactivarPredio = new ArrayList<PPredio>();

        if (auxDesactivar != null && !auxDesactivar.isEmpty()) {
            if (this.tramite.isSegundaDesenglobe()) {

                pPrediosDesenglobe = this.getConservacionService()
                    .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                        this.tramite.getId());

                for (String np : auxDesactivar) {
                    for (PPredio pp : pPrediosDesenglobe) {
                        if (pp.getNumeroPredialOriginal() != null && pp.getNumeroPredialOriginal().
                            equals(np) || pp.getNumeroPredial().equals(np)) {
                            pp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                            pp.setEstado(EPredioEstado.CANCELADO.getCodigo());
                            pp.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                            pp.setFechaLog(new Date());
                            desactivarPredio.add(pp);
                        }
                    }
                }
            } else if (this.tramite.isQuintaMasivo()) {
                retirarPrediosSeleccionados(auxDesactivar);
            }

            if (desactivarPredio != null && !desactivarPredio.isEmpty()) {
                this.getConservacionService().guardarPPredios(desactivarPredio);

                this.fichaMatriz = this.getConservacionService()
                    .buscarPFichaMatrizPorPredioId(
                        this.fichaMatriz.getPPredio().getId());
                this.proyectarConservacionMB
                    .setFichaMatrizSeleccionada(this.fichaMatriz);
            }
            this.verificarTorresEnFirme();
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que retira una lista de PPredios asociados al trámite dada una lista de números
     * prediales.
     *
     * @author david.cifuentes
     * @param auxNumerosPrediales
     *
     */
    public void retirarPrediosSeleccionados(List<String> auxNumerosPrediales) {

        List<PPredio> pPrediosDesenglobe = new ArrayList<PPredio>();
        List<PPredio> removePredio = new ArrayList<PPredio>();

        if (tramite.isSegundaDesenglobe() || tramite.isQuintaMasivo()) {
            pPrediosDesenglobe = this.getConservacionService()
                .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                    this.tramite.getId());

            if (auxNumerosPrediales != null && !auxNumerosPrediales.isEmpty()) {
                for (String np : auxNumerosPrediales) {
                    for (PPredio pp : pPrediosDesenglobe) {
                        if (!this.tramite.isDesenglobeEtapas() &&
                             pp.getNumeroPredial().equals(np) &&
                             pp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.INSCRIBE
                                    .getCodigo())) {
                            removePredio.add(pp);
                        } else if (this.tramite.isEnglobeVirtual()) {

                            if (pp.getNumeroPredial().equals(np) &&
                                 (pp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.MODIFICA
                                        .getCodigo()) ||
                                 pp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.INSCRIBE
                                        .getCodigo()))) {
                                removePredio.add(pp);
                            }

                        } else if (!this.tramite.isEnglobeVirtual() &&
                             this.tramite.isDesenglobeEtapas()) {

                            if (pp.getNumeroPredial().equals(np) &&
                                 pp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.MODIFICA
                                        .getCodigo())) {
                                removePredio.add(pp);
                            }
                        }
                    }
                }

                List<IProyeccionObject> pPrediosProyeccionRemover =
                    new ArrayList<IProyeccionObject>();

                for (IProyeccionObject objProy : removePredio) {
                    pPrediosProyeccionRemover.add(objProy);
                }

                this.getConservacionService().eliminarProyecciones(
                    this.usuarioFichaMatriz, pPrediosProyeccionRemover);

                this.fichaMatriz = this.getConservacionService()
                    .buscarPFichaMatrizPorPredioId(
                        this.fichaMatriz.getPPredio().getId());
                this.proyectarConservacionMB
                    .setFichaMatrizSeleccionada(this.fichaMatriz);
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida si los números prediales a retirar se pueden eliminar para trámites de
     * segunda desenglobe (PH y Condominios) y segundas masivas
     *
     * @author david.cifuentes
     * @param retirarBool Variable usada para mostrar los mensajes dependiendo si se quiere retirar
     * la unidad o sólo borrar el número predial en el caso de los migrados.
     * @return
     */
    @SuppressWarnings("unused")
    public boolean validarRetirarNumeroPredial(boolean retirarBool) {

        int numerosPredialesGenerados[][] = null;
        PFichaMatrizPredio ultimoCondominio, ultimoCondominioSeleccionado;
        boolean mismoPiso = true;
        PFichaMatrizPredio aux;
        String torre, pisoOSotano, ultimaUnidad, torreSel, pisoOSotanoSel, esSotano, esSotanoSel;

        // Se debe realizar el cargue de la matriz nuevamente antes y despues de
        // la eliminación, antes para validar sobre los datos persistidos y despues
        // para refrescar la matriz de predios.
        this.cargarMatrizNumerosPrediales();

        if (this.tramite.isQuintaMasivo()) {
            if (this.fichaMatrizPrediosSeleccionados.length == 1 &&
                this.fichaMatrizPrediosSeleccionados[0].getEstado() != null &&
                 this.fichaMatrizPrediosSeleccionados[0].getEstado().equals(
                    EPredioEstado.REACTIVADO.getCodigo())) {
                return true;
            }
        }

        if (this.tramite.isEnglobeVirtual()) {
            for (PFichaMatrizPredio pfmp : fichaMatrizPrediosSeleccionados) {
                if (ESiNo.SI.getCodigo().equals(pfmp.getOriginalTramite())) {
                    this.addMensajeError(
                        "Solo puede eliminar predios que han sido creados en el trámite.");
                    return false;
                }
            }
        }

        if (this.tramite.isTipoInscripcionCondominio() && this.tramite.isDesenglobeEtapas()) {
            return true;
        }

        if (this.fichaMatrizPrediosSeleccionados != null &&
             this.fichaMatrizPrediosSeleccionados.length > 0) {

            if ((this.fichaMatrizPrediosSeleccionados.length + this.
                getUnidadesPrivadasCanceladasFichaMatrizEtapas()) ==
                this.fichaMatrizPrediosResultantes.size()) {
                // Seleccionados todos los predios

                if (this.tramite.isTipoInscripcionPH() && this.tramite.isDesenglobeEtapas()) {
                    if (retirarBool) {
                        this.addMensajeWarn(
                            "No es posible retirar todos los predios para un desenglobe de etapas.");
                    } else {
                        this.addMensajeWarn(
                            "No es posible borrar todos los números prediales para un desenglobe de etapas.");
                    }
                    return false;
                } else {
                    return true;
                }
            } else if (this.fichaMatrizPrediosSeleccionados.length == 1) {
                // VALIDAR EL RETIRO DE LA ÃLTIMA UNIDAD ASIGNADA
                aux = this.fichaMatrizPrediosSeleccionados[0];
                if (this.tramite.isTipoInscripcionCondominio() && !this.tramite.isDesenglobeEtapas()) {

                    ultimoCondominio = buscarUltimaUnidadAsignadaCondominio(this.fichaMatriz.
                        getPFichaMatrizPredios());

                    if (ultimoCondominio != null) {
                        if (ultimoCondominio
                            .equals(this.fichaMatrizPrediosSeleccionados[0])) {
                            // Se recorre el arreglo de numeros prediales
                            // generados, y se desasigna la última que
                            // tenga un '1', que corresponde al último asignado.
                            for (int i = this.numerosPredialesGeneradosCondominio.length - 1; i > 0;
                                i--) {
                                if (this.numerosPredialesGeneradosCondominio[i] == 1) {
                                    this.numerosPredialesGeneradosCondominio[i] = 0;
                                    break;
                                }
                            }
                            return true;
                        } else {
                            if (retirarBool) {
                                this.addMensajeWarn(
                                    "Sólo se puede retirar el último número predial asignado.");
                            } else {
                                this.addMensajeWarn(
                                    "Sólo se puede borrar el último número predial modificado.");
                            }
                            return false;
                        }
                    } else {
                        this.addMensajeError("Error al validar el retiro de la unidad.");
                        return false;
                    }
                } else if (this.tramite.isTipoInscripcionPH()) {

                    // torre
                    torre = aux.getNumeroPredial().substring(22, 24);
                    // Piso
                    pisoOSotano = aux.getNumeroPredial().substring(24, 26);
                    // Unidad
                    ultimaUnidad = aux.getNumeroPredial().substring(26, 30);
                    //Es sotano
                    esSotano = aux.getNumeroPredial().substring(24, 26);

                    // Verificar si se va a realizar una eliminación de sotanos,
                    // de pisos o de la torre completa.
                    if (esSotano.substring(0, 1).equals("9")) {
                        numerosPredialesGenerados =
                            new int[this.numerosPredialesGeneradosPorSotano.length][this.numerosPredialesGeneradosPorSotano[0].length];
                        for (int i = 0; i < this.numerosPredialesGeneradosPorSotano.length; i++) {
                            for (int j = 0; j < this.numerosPredialesGeneradosPorSotano[i].length;
                                j++) {
                                numerosPredialesGenerados[i][j] =
                                    this.numerosPredialesGeneradosPorSotano[i][j];
                            }
                        }
                        ultimaUnidad = aux.getNumeroPredial().substring(27, 30);
                        int reajustePisoSotano = 100 - Integer
                            .valueOf(pisoOSotano);
                        pisoOSotano = "" + reajustePisoSotano;
                    } else if (!esSotano.substring(0, 1).equals("9")) {
                        numerosPredialesGenerados =
                            new int[this.numerosPredialesGeneradosPorPiso.length][this.numerosPredialesGeneradosPorPiso[0].length];
                        for (int i = 0; i < this.numerosPredialesGeneradosPorPiso.length; i++) {
                            for (int j = 0; j < this.numerosPredialesGeneradosPorPiso[i].length; j++) {
                                numerosPredialesGenerados[i][j] =
                                    this.numerosPredialesGeneradosPorPiso[i][j];
                            }
                        }
                    }

                    if (Integer.valueOf(ultimaUnidad) < Integer
                        .valueOf(numerosPredialesGenerados[Integer
                            .valueOf(torre)][Integer
                            .valueOf(pisoOSotano)])) {
                        if (retirarBool) {
                            this.addMensajeError(
                                "Sólo se puede retirar la ultima unidad asignada en la torre, o toda la torre.");
                        } else {
                            this.addMensajeError(
                                "Sólo se puede borrar el número predial de la ultima unidad asignada en la torre, o borrar todos los números prediales asignados en la torre.");
                        }
                        return false;
                    } else if (esSotano.substring(0, 1).equals("9")) {
                        this.numerosPredialesGeneradosPorSotano[Integer
                            .valueOf(torre)][Integer.valueOf(pisoOSotano)] -= 1;
                    } else if (!esSotano.substring(0, 1).equals("9")) {
                        this.numerosPredialesGeneradosPorPiso[Integer
                            .valueOf(torre)][Integer.valueOf(pisoOSotano)] -= 1;
                    }

                    numerosPredialesGenerados[Integer.valueOf(torre)][Integer
                        .valueOf(pisoOSotano)] -= 1;
                    return true;
                }
            } else {

                if (this.tramite.isTipoInscripcionCondominio()) {

                    // Copia de la lista de PFichaMatrizPredios de la ficha
                    // matriiz.
                    List<PFichaMatrizPredio> listaPFichaMatrizPredioTemporal =
                        new ArrayList<PFichaMatrizPredio>();
                    listaPFichaMatrizPredioTemporal.
                        addAll(this.fichaMatriz.getPFichaMatrizPredios());

                    // Copia de la lista de PFichaMatrizPredios seleccionados.
                    List<PFichaMatrizPredio> listaPrediosSeleccionadadosTemp =
                        new ArrayList<PFichaMatrizPredio>(
                            Arrays.asList(fichaMatrizPrediosSeleccionados));

                    for (PFichaMatrizPredio pfmp : fichaMatrizPrediosSeleccionados) {

                        // Búsqueda del último predio asignado a la ficha
                        // matriz.
                        ultimoCondominio = buscarUltimaUnidadAsignadaCondominio(
                            listaPFichaMatrizPredioTemporal);
                        // Búsqueda del último predio seleccionado.
                        ultimoCondominioSeleccionado = buscarUltimaUnidadAsignadaCondominio(
                            listaPrediosSeleccionadadosTemp);

                        if (ultimoCondominio != null &&
                             ultimoCondominioSeleccionado != null) {
                            // Si los últimos predios son iguales, se remueven
                            // de ambas listas, y se procede a seguir con la
                            // comparación del siguiente predio, de lo contrario
                            // se considera que los predios seleccionados no
                            // corresponden exactamente a los últimos generados.
                            if (ultimoCondominio
                                .equals(ultimoCondominioSeleccionado)) {

                                listaPrediosSeleccionadadosTemp
                                    .remove(ultimoCondominioSeleccionado);
                                listaPFichaMatrizPredioTemporal
                                    .remove(ultimoCondominio);
                            } else {
                                if (!this.tramite.isDesenglobeEtapas()) {
                                    if (retirarBool) {
                                        this.addMensajeWarn(
                                            "Sólo se pueden retirar las últimas unidades asignadas, por favor revise los números prediales seleccionados.");
                                    } else {
                                        this.addMensajeWarn(
                                            "Sólo se pueden borrar los últimos números prediales, por favor revise los números prediales seleccionados.");
                                    }
                                    return false;
                                } else if (fichaMatrizPrediosSeleccionados.length == 1) {
                                    if (retirarBool) {
                                        this.addMensajeWarn(
                                            "Sólo se pueden retirar las últimas unidades asignadas, por favor revise los números prediales seleccionados.");
                                    } else {
                                        this.addMensajeWarn(
                                            "Sólo se pueden borrar las últimas unidades asignadas, por favor revise los números prediales seleccionados.");
                                    }
                                    return false;
                                }
                            }
                        } else {
                            this.addMensajeError("Error al validar el retiro de las unidades.");
                            return false;
                        }
                    }

                    // Si llega hasta este punto, se considera que las unidades
                    // seleccionadas son las últimas generadas y se procede a
                    // actualizar la lista de números prediales asignados.
                    int countSelected = 0;
                    for (int i = this.numerosPredialesGeneradosCondominio.length - 1;
                        countSelected == this.numerosPredialesGeneradosCondominio.length; i--) {
                        if (this.numerosPredialesGeneradosCondominio[i] == 1) {
                            this.numerosPredialesGeneradosCondominio[i] = 0;
                            countSelected++;
                        }
                    }
                    return true;

                } else if (this.tramite.isTipoInscripcionPH()) {
                    torreSel = fichaMatrizPrediosSeleccionados[0]
                        .getNumeroPredial().substring(22, 24);

                    for (PFichaMatrizPredio pfmp : fichaMatrizPrediosSeleccionados) {

                        // Los seleccionados deben pertenecer a la misma torre
                        torre = pfmp.getNumeroPredial().substring(22, 24);
                        if (!torreSel.equals(torre)) {
                            if (retirarBool) {
                                this.addMensajeError(
                                    "Los números prediales a eliminar debe ser de la misma torre");
                            } else {
                                this.addMensajeError(
                                    "Los números prediales a borrar deben ser de la misma torre");
                            }
                            return false;
                        }
                    }
                    // Los seleccionados deben ser la torre completa o el
                    // piso completo
                    pisoOSotanoSel = fichaMatrizPrediosSeleccionados[0]
                        .getNumeroPredial().substring(24, 26);
                    for (PFichaMatrizPredio pfmp : fichaMatrizPrediosSeleccionados) {

                        pisoOSotano = pfmp.getNumeroPredial().substring(24, 26);

                        if (!pisoOSotanoSel.equals(pisoOSotano)) {
                            // No son del mismo piso
                            mismoPiso = false;
                            break;
                        }
                    }

                    // Mismo piso
                    if (mismoPiso) {
                        // Piso completo
                        if (pisoOSotanoSel.substring(0, 1).equals("9")) {
                            if (this.fichaMatrizPrediosSeleccionados.length ==
                                numerosPredialesGeneradosPorSotano[Integer
                                    .valueOf(torreSel)][100 - (Integer
                                .valueOf(pisoOSotanoSel))]) {
                                if (!this.tramite.isDesenglobeEtapas()) {
                                    return true;
                                } else {
                                    List<Integer> listaPisosOSotanos = new ArrayList<Integer>();
                                    listaPisosOSotanos.add(Integer.valueOf(pisoOSotanoSel));
                                    if (ESiNo.SI.getCodigo().equals(
                                        this.fichaMatrizPrediosSeleccionados[0].getOriginalTramite()) &&
                                        this.validarUltimosPisosTorre(listaPisosOSotanos, torreSel)) {
                                        if (retirarBool) {
                                            this.addMensajeError(
                                                "Está seleccionando todas las unidades en firme que conforman un piso. El piso " +
                                                pisoOSotanoSel +
                                                " no puede quedar sin predios asociados.");
                                        } else {
                                            this.addMensajeError(
                                                "Está seleccionando todas las unidades en firme que conforman un piso. El piso " +
                                                pisoOSotanoSel + " no números prediales asociados.");
                                        }
                                        return false;
                                    } else {
                                        return true;
                                    }
                                }

                            }
                        } else {
                            if (this.fichaMatrizPrediosSeleccionados.length ==
                                numerosPredialesGeneradosPorPiso[Integer
                                    .valueOf(torreSel)][Integer
                                .valueOf(pisoOSotanoSel)]) {
                                if (!this.tramite.isDesenglobeEtapas()) {
                                    return true;
                                } else {
                                    List<Integer> listaPisosOSotanos = new ArrayList<Integer>();
                                    listaPisosOSotanos.add(Integer.valueOf(pisoOSotanoSel));
                                    if (!this.validarUltimosPisosTorre(listaPisosOSotanos, torreSel)) {
                                        this.addMensajeError(
                                            "Sólo se pueden eliminar las últimas unidades generadas de la torre.");
                                        return false;
                                    }
                                    if (ESiNo.SI.getCodigo().equals(
                                        this.fichaMatrizPrediosSeleccionados[0].getOriginalTramite()) &&
                                         !this.validarPrediosAsignadosPorPisoTorre(
                                            listaPisosOSotanos, torreSel)) {
                                        if (retirarBool) {
                                            this.addMensajeError(
                                                "Está seleccionando todas las unidades en firme que conforman un piso. El piso " +
                                                Integer.parseInt(pisoOSotanoSel) +
                                                " no puede quedar sin predios asociados.");
                                        } else {
                                            this.addMensajeError(
                                                "Está seleccionando todas las unidades en firme que conforman un piso. El piso " +
                                                Integer.parseInt(pisoOSotanoSel) +
                                                " no puede quedar sin números prediales asociados.");
                                        }
                                        return false;
                                    }
                                    return true;
                                }
                            }
                        }
                    } else {

                        // Verificar últimos pisos
                        if (this.tramite.isDesenglobeEtapas()) {

                            List<Integer> listaPisosOSotanos = new ArrayList<Integer>();

                            for (PFichaMatrizPredio pfmp : fichaMatrizPrediosSeleccionados) {
                                pisoOSotano = pfmp.getNumeroPredial().substring(24, 26);

                                if (!listaPisosOSotanos.contains(pisoOSotano)) {
                                    listaPisosOSotanos.add(Integer.valueOf(pisoOSotano));
                                }
                            }

                            if (ESiNo.SI.getCodigo().equals(this.fichaMatrizPrediosSeleccionados[0].
                                getOriginalTramite()) && this.validarUltimosPisosTorre(
                                    listaPisosOSotanos, torreSel)) {
                                if (retirarBool) {
                                    this.addMensajeError(
                                        "Está seleccionando unidades en firme que conforman varios pisos. Recuerde que los pisos no puede quedar sin predios asociados.");
                                } else {
                                    this.addMensajeError(
                                        "Está seleccionando unidades en firme que conforman varios pisos. Recuerde que los pisos no puede quedar sin números prediales asociados.");
                                }
                                return false;
                            } else {
                                return true;
                            }
                        }

                        // Torre completa
                        Long totalTorre = 0L;
                        Long totalSotanos = 0L;
                        Long totalPisos = 0L;
                        for (int x = 0; x < this.numerosPredialesGeneradosPorPiso[Integer
                            .valueOf(torreSel)].length; x++) {
                            totalTorre += this.numerosPredialesGeneradosPorPiso[Integer
                                .valueOf(torreSel)][x];
                            totalPisos += this.numerosPredialesGeneradosPorPiso[Integer
                                .valueOf(torreSel)][x];
                            if (x < this.numerosPredialesGeneradosPorSotano[Integer
                                .valueOf(torreSel)].length) {
                                totalTorre += this.numerosPredialesGeneradosPorSotano[Integer
                                    .valueOf(torreSel)][x];
                                totalSotanos += this.numerosPredialesGeneradosPorSotano[Integer
                                    .valueOf(torreSel)][x];
                            }
                        }
                        if (this.fichaMatrizPrediosSeleccionados.length == totalTorre) {
                            return true;
                        }
                        // Totalidad de pisos
                        if (this.fichaMatrizPrediosSeleccionados.length == totalPisos) {
                            return true;
                        }
                        // Totalidad de sotanos
                        if (this.fichaMatrizPrediosSeleccionados.length == totalSotanos) {
                            return true;
                        }
                    }
                    // Validación para predios migrados
                    if (this.tramite.isDesenglobeEtapas() && this.fichaMatrizPrediosSeleccionados !=
                        null) {
                        boolean migrado = false;
                        int conteoMigrados = 0;
                        for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {
                            if (pfmp.getNumeroPredialOriginal() != null) {
                                String torreMigrada = pfmp.getNumeroPredialOriginal().substring(22,
                                    24);
                                String pisoMigrado = pfmp.getNumeroPredialOriginal().substring(24,
                                    26);
                                if ("00".equals(pisoMigrado) || "00".equals(torreMigrada)) {
                                    migrado = true;
                                    conteoMigrados++;
                                }
                            }
                        }
                        if (migrado && conteoMigrados == this.fichaMatrizPrediosSeleccionados.length) {
                            return true;
                        } else {
                            if (retirarBool) {
                                this.addMensajeWarn(
                                    "Existen predios migrados que han sido seleccionados, por favor retire individualmente dichos predios.");
                            } else {
                                this.addMensajeWarn(
                                    "Existen predios migrados que han sido seleccionados, por favor borre individualmente dichos números prediales.");
                            }
                            return false;
                        }

                    }
                    this.addMensajeError(
                        "Debe seleccionar todas las unidades de un piso, o la totalidad de la torre.");
                    return false;
                }

            }
        }
        return false;
    }

    /**
     * Método que valida si se han asignado predios a la lista de pisos enviada como parámetro para
     * verificar si una determinada lista de predios se puede eliminar o conforma la totalidad de
     * los predios del piso.
     *
     * @param listaPisosOSotanos
     * @param torreSel
     * @return
     */
    private boolean validarPrediosAsignadosPorPisoTorre(
        List<Integer> listaPisosOSotanos, String torreSel) {
        if (listaPisosOSotanos != null && listaPisosOSotanos.size() == 1 && torreSel != null &&
            numerosPredialesGeneradosPorPiso != null) {
            int prediosAsignados =
                numerosPredialesGeneradosPorPiso[Integer.valueOf(torreSel)][Integer.valueOf(
                listaPisosOSotanos.get(0))];
            if (prediosAsignados == this.fichaMatrizPrediosSeleccionados.length) {
                int pisosTorre =
                    numerosPredialesGeneradosPorPiso[Integer.valueOf(torreSel)].length - 1;
                if (pisosTorre == listaPisosOSotanos.get(0).intValue()) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Método que valida si la lista de pisos son o no los últimos de una torre enviada como
     * parámetro.
     *
     * @param listaPisosOSotanos
     * @param torreSel
     * @return
     */
    private boolean validarUltimosPisosTorre(List<Integer> listaPisosOSotanos,
        String torreSel) {
        if (listaPisosOSotanos != null && torreSel != null && numerosPredialesGeneradosPorPiso !=
            null) {
            int pisosTorre = numerosPredialesGeneradosPorPiso[Integer.valueOf(torreSel)].length - 1;
            int iterator = 0;
            for (int i = pisosTorre; i > 0; i--) {
                iterator++;
                if (listaPisosOSotanos.contains(i) && iterator >= listaPisosOSotanos.size()) {
                    return true;
                }
                if (iterator > listaPisosOSotanos.size()) {
                    break;
                }
            }
            return false;
        }
        return false;
    }

    // ---------------------------------------------------- //
    /**
     * Método que busca la última unidad asignada para un condominio y retorna su indice en el
     * arreglo de números prediales asignados.
     *
     * @author david.cifuentes
     */
    public PFichaMatrizPredio buscarUltimaUnidadAsignadaCondominio(
        List<PFichaMatrizPredio> listaPFichaMatrizPredio) {
        String numeroPredio = "0";
        String numeroUnidad = "0";
        Long mayorPredio = 0L;
        Long mayorUnidad = 0L;
        PFichaMatrizPredio ultimoPFichaMatrizPredio = null;
        for (PFichaMatrizPredio pfmp : listaPFichaMatrizPredio) {
            if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.getCancelaInscribe())) {
                numeroPredio = pfmp.getNumeroPredial().substring(17, 21);
                numeroUnidad = pfmp.getNumeroPredial().substring(26, 30);
                if (Long.valueOf(numeroPredio) >= mayorPredio) {
                    mayorPredio = Long.valueOf(numeroPredio);
                    if (Long.valueOf(numeroUnidad) >= mayorUnidad) {
                        mayorUnidad = Long.valueOf(numeroUnidad);
                        ultimoPFichaMatrizPredio = pfmp;
                    }
                }
            }
        }
        return ultimoPFichaMatrizPredio;
    }

    // ---------------------------------------------------- //
    /**
     * Método que consulta los pisos o sotanos de una torre y los almacena en un itemList.
     *
     * @author david.cifuentes
     */
    public void cargarPisoOSotano() {
        LOGGER.info("cargarPisoOSotano");
        // Se buscan los pisos y sotanos de la torre seleccionada.
        this.itemListPisos = new ArrayList<SelectItem>();
        this.itemListSotanos = new ArrayList<SelectItem>();
        Long numPisos = 0L;
        Long numSotanos = 0L;
        Long mayor;
        SelectItem numeracion;
        if (this.torreSeleccionada.equals("0")) {
            this.setTorreCargadaBool(true);
            return;
        }
        if ((this.tramite.isTipoInscripcionPH() || this.fichaMatriz.getPPredio().isMixto()) &&
             this.torreSeleccionada != null &&
             !this.torreSeleccionada.trim().isEmpty()) {
            // Para todas las torres
            if (!this.torreSeleccionada.equals("TODAS")) {
                for (PFichaMatrizTorre pfmt : this.fichaMatriz
                    .getPFichaMatrizTorres()) {
                    if (pfmt.getTorre() != null &&
                         pfmt.getTorre().toString()
                            .equals(this.torreSeleccionada.toString())) {
                        numPisos = pfmt.getPisos();
                        numSotanos = pfmt.getSotanos();
                        break;
                    }
                }

                mayor = (numPisos > numSotanos) ? numPisos : numSotanos;
                // Numeración de los pisos y sotanos
                for (Long i = 1L; i <= mayor; i++) {
                    numeracion = new SelectItem();
                    numeracion.setValue(i);
                    numeracion.setLabel(i.toString());
                    if (i <= numPisos) {
                        this.itemListPisos.add(numeracion);
                    }
                    if (i <= numSotanos) {
                        this.itemListSotanos.add(numeracion);
                    }
                }

                if (!this.unicaUnidadBool &&
                     !(this.tramite.isDesenglobeEtapas() || this.tramite.isTerceraMasiva() ||
                    this.tramite.isRectificacion() || this.tramite.isQuintaMasivo())) {
                    SelectItem todos = new SelectItem();
                    todos.setValue("TODOS");
                    todos.setLabel("TODOS");
                    this.itemListSotanos.add(todos);
                    this.itemListPisos.add(todos);
                }

                if (numSotanos > 0) {
                    this.sotanosBool = true;
                } else {
                    this.sotanosBool = false;
                }
            } else {

                // Todas las torres son iguales
                numPisos = this.fichaMatriz.getPFichaMatrizTorres().get(0)
                    .getPisos();
                numSotanos = this.fichaMatriz.getPFichaMatrizTorres().get(0)
                    .getSotanos();
                mayor = (numPisos > numSotanos) ? numPisos : numSotanos;
                // Numeración de los pisos y sotanos
                for (Long i = 1L; i <= mayor; i++) {
                    numeracion = new SelectItem();
                    numeracion.setValue(i);
                    numeracion.setLabel(i.toString());
                    if (i <= numPisos) {
                        this.itemListPisos.add(numeracion);
                    }
                    if (i <= numSotanos) {
                        this.itemListSotanos.add(numeracion);
                    }
                }

                if (!this.unicaUnidadBool &&
                     !(this.tramite.isDesenglobeEtapas() || this.tramite.isTerceraMasiva() ||
                    this.tramite.isRectificacion() || this.tramite.isQuintaMasivo())) {
                    SelectItem todos = new SelectItem();
                    todos.setValue("TODOS");
                    todos.setLabel("TODOS");
                    this.itemListSotanos.add(todos);
                    this.itemListPisos.add(todos);
                }

                if (numSotanos > 0) {
                    this.sotanosBool = true;
                } else {
                    this.sotanosBool = false;
                }
            }
        }
        this.setTorreCargadaBool(false);

        if (!this.sotanosBool && ESiNo.SI.getCodigo().equals(this.ejeVialHaciaAbajo)) {
            this.addMensajeWarn(
                "La torre seleccionada no tiene definida una cantidad para los sótanos, si desea asignar sótanos por favor edite la torre.");
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida si es posible o no asignar un sótano.
     *
     * @author david.cifuentes
     */
    public void validarAsignacionSotanos() {
        if (!this.sotanosBool && ESiNo.SI.getCodigo().equals(this.ejeVialHaciaAbajo)) {
            this.addMensajeWarn(
                "La torre seleccionada no tiene definida una cantidad para los sótanos, si desea asignar sótanos por favor edite la torre.");
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que actualiza el valor de las unidades restantes y asignadas.
     *
     * @author david.cifuentes
     */
    public void actualizarUnidades() {

        this.unidadesTotales = 0L;
        this.unidadesRestantes = 0L;
        this.unidadesAsignadas = 0L;

        int unidadesCreadasTemp = 0;
        int sotanosCreadosTem = 0;
        Long torreMayor = 0L;

        long totalUnidadesCon = 0;
        long totalUnidadesPh = 0;

        if (this.fichaMatriz.getPPredio().isMixto()) {
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {

                if (pfmp.getNumeroPredial().substring(21, 22).equals(EPredioCondicionPropiedad.CP_8.
                    getCodigo())) {
                    totalUnidadesCon++;
                } else if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                    totalUnidadesPh++;
                }

            }
        }

        if (!this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {

            for (PFichaMatrizTorre fmt : this.fichaMatriz.getPFichaMatrizTorres()) {
                if (this.tramite.isEnglobeVirtual()) {

                    if (!EPredioEstado.CANCELADO.getCodigo().equals(fmt.getEstado())) {

                        this.unidadesTotales += fmt.getUnidades();
                        this.unidadesTotales += fmt.getUnidadesSotanos();
                    }

                } else if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(fmt.
                    getCancelaInscribe())) {
                    this.unidadesTotales += fmt.getUnidades();
                    this.unidadesTotales += fmt.getUnidadesSotanos();
                }

                if (fmt.getTorre() != null && fmt.getTorre() > torreMayor) {
                    torreMayor = fmt.getTorre();
                }
            }

            if (!this.tramite.isDesenglobeEtapas()) {
                this.unidadesAsignadas = Long.valueOf(this.fichaMatriz.getPFichaMatrizPredios().
                    size());
                this.unidadesRestantes = this.unidadesTotales -
                     this.unidadesAsignadas;
            } else {
                if (this.fichaMatriz.getPPredio().isMixto()) {

                    this.unidadesAsignadas = totalUnidadesPh;

                } else if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                    for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                        if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                            this.unidadesAsignadas++;
                        }
                    }
                }
                this.unidadesRestantes = this.unidadesTotales -
                     this.unidadesAsignadas;
            }

            // Cargue de unidades restantes
            if (torreMayor > 0) {
                this.sotanosRestantesPorTorrePH = new int[torreMayor.intValue() + 1];
                this.unidadesRestantesPorTorrePH = new int[torreMayor.intValue() + 1];
                this.sotanosAsignadosPorTorrePH = new int[torreMayor.intValue() + 1];
                this.unidadesAsignadasPorTorrePH = new int[torreMayor.intValue() + 1];

                for (PFichaMatrizTorre fmt : this.fichaMatriz.getPFichaMatrizTorres()) {

                    int torre = 0;
                    if (fmt.getTorre() != null) {

                        torre = new Long(fmt.getTorre().intValue()).intValue();

                    } else if (fmt.getTorre() == null &&
                         fmt.getProvieneTorre() != null &&
                         fmt.getProvieneTorre().getTorre() != null) {

                        torre = new Long(fmt.getProvieneTorre().getTorre().intValue()).intValue();
                    }

                    this.unidadesRestantesPorTorrePH[torre] += fmt.getUnidades();
                    this.sotanosRestantesPorTorrePH[torre] += fmt.getUnidadesSotanos();
                    this.sotanosAsignadosPorTorrePH[torre] = 0;
                    this.unidadesAsignadasPorTorrePH[torre] = 0;
                }

                if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                    for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                        if (pfmp.getNumeroPredial() != null) {
                            int torre = new Long(pfmp.getNumeroPredial().substring(22, 24)).
                                intValue();

                            if (pfmp.isSotano()) {
                                this.sotanosRestantesPorTorrePH[torre] =
                                    this.sotanosRestantesPorTorrePH[torre] - 1;
                                this.sotanosAsignadosPorTorrePH[torre] =
                                    this.sotanosAsignadosPorTorrePH[torre] + 1;
                            } else {
                                this.unidadesRestantesPorTorrePH[torre] =
                                    this.unidadesRestantesPorTorrePH[torre] - 1;
                                this.unidadesAsignadasPorTorrePH[torre] =
                                    this.unidadesAsignadasPorTorrePH[torre] + 1;
                            }
                        }
                    }
                }
            }

        }
        if (this.fichaMatriz.getPPredio().getNumeroPredial().substring(21, 22).equals(
            EPredioCondicionPropiedad.CP_8.getCodigo()) ||
             (this.condicionPredioSeleccionada != null && !this.condicionPredioSeleccionada.
                isEmpty() &&
             this.condicionPredioSeleccionada.contains(EPredioCondicionPropiedad.CP_8.getCodigo()))) {

            if (this.fichaMatriz.getPPredio().isMixto()) {
                this.unidadesTotales = this.getTotalUnidadesCondominioEtapas();
                this.unidadesAsignadas = totalUnidadesCon;

                this.unidadesRestantesCondominio = (this.unidadesTotales.intValue() -
                    this.unidadesAsignadas.intValue());
                this.unidadesRestantes = Long.valueOf(this.unidadesRestantesCondominio);

            } else {

                this.unidadesTotales = this.fichaMatriz.getTotalUnidadesPrivadas() +
                     this.fichaMatriz.getTotalUnidadesSotanos();
                if (!this.tramite.isDesenglobeEtapas() && !this.tramite.isQuintaMasivo()) {
                    this.unidadesAsignadas = Long.valueOf(this.fichaMatriz.getPFichaMatrizPredios().
                        size());
                } else {
                    for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {

                        if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                            getCancelaInscribe()) &&
                             !EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                            this.unidadesAsignadas++;
                        }
                    }
                }

                this.unidadesRestantes = this.unidadesTotales - this.unidadesAsignadas;

                // Cargue de unidades restantes
                if (this.fichaMatriz.getPFichaMatrizPredios() != null) {
                    for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                        if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                            getCancelaInscribe()) &&
                             !EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                            if (pfmp.isSotano()) {
                                sotanosCreadosTem++;
                            } else {
                                unidadesCreadasTemp++;
                            }
                        }
                    }
                    this.unidadesRestantesCondominio = this.fichaMatriz.getTotalUnidadesPrivadas().
                        intValue() - unidadesCreadasTemp;
                    this.sotanosRestantesCondominio = this.fichaMatriz.getTotalUnidadesSotanos().
                        intValue() - sotanosCreadosTem;
                }
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que verifica si se han creado predios para una torre seleccionadoa.
     *
     * @author david.cifuentes
     */
    public boolean validarExistenPrediosCreadosEnTorre(
        PFichaMatrizTorre torreSeleccionada) {
        boolean existen = false;

        if (!this.tramite.isEnglobeVirtual()) {
            if (torreSeleccionada != null) {
                // Validación de pisos de la torre
                for (int piso = 0; piso < torreSeleccionada.getPisos(); piso++) {
                    if (torreSeleccionada.getTorre() != null &&
                         this.numerosPredialesGeneradosPorPiso[(torreSeleccionada.getTorre().
                            intValue())].length > piso &&
                         this.numerosPredialesGeneradosPorPiso[(torreSeleccionada.getTorre().
                            intValue())][piso] > 0) {
                        existen = true;
                        break;
                    }
                }
            }
        }

        return existen;
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida que la torre seleccionada sea la última de las torres creadas. Tambien es
     * usado para saber si la torre permite ser eliminada si no se ha creado el primer
     * pFichaMatrizPredio.
     *
     * @author david.cifuentes
     */
    public boolean validarUltimaTorreCreada(PFichaMatrizTorre torreSeleccionada) {

        boolean valido = false;

        // Si no hay predios creados, no importa el número de la torre, pues
        // estas se reenumeran.
        if (this.fichaMatriz.getPFichaMatrizPredios() == null ||
             this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {
            valido = true;
        } else {
            // Validar que sea la última torre.
            Long numeroTorre = torreSeleccionada.getTorre();
            Long mayor = 0L;
            for (PFichaMatrizTorre pfmt : this.fichaMatriz
                .getPFichaMatrizTorres()) {
                if (pfmt.getTorre() != null &&
                     Long.valueOf(pfmt.getTorre()) > Long.valueOf(mayor)) {
                    mayor = pfmt.getTorre();
                }
            }
            if (Long.valueOf(mayor) == Long.valueOf(numeroTorre)) {
                valido = true;
            }
        }
        return valido;
    }

    // ---------------------------------------------------- //
    /**
     * Método que verifica si se han creado sotanos para una torre seleccionadoa.
     *
     * @author david.cifuentes
     */
    public boolean validarExistenSotanosCreadosEnTorre(
        PFichaMatrizTorre torreSeleccionada) {
        boolean existen = false;

        if (!this.tramite.isEnglobeVirtual()) {
            if (torreSeleccionada != null) {
                // Validación de sotanos de la torre
                for (int sotano = 0; sotano <= torreSeleccionada.getSotanos(); sotano++) {
                    int lenSotanosActuales =
                        this.numerosPredialesGeneradosPorSotano[torreSeleccionada
                            .getTorre().intValue()].length;
                    if (lenSotanosActuales > torreSeleccionada.getSotanos() &&
                         this.numerosPredialesGeneradosPorSotano[torreSeleccionada
                            .getTorre().intValue()][sotano] > 0) {
                        existen = true;
                        break;
                    }
                }
            }
        }
        return existen;
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida la edición de unidades en la ficha matriz.
     *
     * @author david.cifuentes
     */
    public boolean validarUnidadesFichaMatriz(boolean generaUnidadesCondominio) {
        boolean validate = false;

        if (generaUnidadesCondominio) {

            if (!(this.tramite.isDesenglobeEtapas() || this.tramite.isQuintaMasivo())) {

                // Validaciones para condominio
                if (this.fichaMatriz.getTotalUnidadesPrivadas() > 0) {
                    validate = true;
                } else {
                    this.addMensajeError("Por favor ingrese un valor para las unidades privadas.");
                }

                // Si ya se han creado predios, no se pueden editar dichas unidades
                if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                     !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {
                    this.addMensajeWarn(
                        "No se puede realizar edición o eliminación sobre las unidades del condominio pues ya se han creado predios en la ficha matriz. Para editar o eliminar las unidades por favor elimine sus predios asociados.");
                    validate = false;
                }
            } else {

                // Validaciones para condominio
                if (this.getUnidadesNuevasCondominioEtapas() > 0) {
                    validate = true;
                } else {
                    this.addMensajeError("Por favor ingrese un valor para las unidades privadas.");
                }

            }

        } else {

            validate = true;

            PFichaMatrizTorre torreSeleccionada = null;
            if (this.fichaMatrizTorreSeleccionada != null) {
                torreSeleccionada = this.fichaMatrizTorreSeleccionada;
            }

            // Validaciones para PH
            if (torreSeleccionada.getPisos() == null) {
                torreSeleccionada.setPisos(0L);
            }
            if (torreSeleccionada.getSotanos() == null) {
                torreSeleccionada.setSotanos(0L);
            }
            if (torreSeleccionada.getUnidades() == null) {
                torreSeleccionada.setUnidades(0L);
            }
            if (torreSeleccionada.getUnidadesSotanos() == null) {
                torreSeleccionada.setUnidadesSotanos(0L);
            }

            if (torreSeleccionada.getTorre() != null &&
                 torreSeleccionada.getTorre() < 1) {
                this.addMensajeError(
                    "Por favor ingrese un valor mayor a cero para el número de la torre.");
                validate = false;
            }

            if (torreSeleccionada.getPisos() < 1 &&
                 torreSeleccionada.getSotanos() < 1) {
                this.addMensajeError(
                    "Por favor ingrese un valor para el total de pisos o sotanos de la torre.");
                validate = false;
            }

            if (torreSeleccionada.getPisos() > 0) {
                if (torreSeleccionada.getUnidades() < 1) {
                    this.
                        addMensajeError("Por favor ingrese un valor para las unidades de la torre.");
                    validate = false;
                }
            }

            if (torreSeleccionada.getSotanos() > 0) {
                if (torreSeleccionada.getUnidadesSotanos() < 1) {
                    this.addMensajeError("Por favor ingrese un valor para las unidades del sótano.");
                    validate = false;
                }
            }

            if (this.tramite.isEnglobeVirtual()) {

                if (torreSeleccionada.getSotanos() < 0 ||
                     torreSeleccionada.getUnidadesSotanos() < 0) {

                    this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                    this.addMensajeError(
                        "Por favor ingrese un valor para el total de pisos o sótanos de la torre.");
                    validate = false;

                }

                if (torreSeleccionada.getUnidadesSotanos() > 0) {
                    if (torreSeleccionada.getSotanos() < 1) {
                        this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                        this.addMensajeError(
                            "Por favor ingrese un valor para el total de sótano de la torre.");
                        validate = false;
                    }
                }

            } else {

                if (torreSeleccionada.getUnidadesSotanos() > 0) {
                    if (torreSeleccionada.getSotanos() < 1) {
                        this.addMensajeError(
                            "Por favor ingrese un valor válido para los sotanos, este no puede ser cero si hay unidades de sótano mayores a cero.");
                        validate = false;
                    }
                }
            }

            if (this.tramite.isEnglobeVirtual()) {
                if (torreSeleccionada.getSotanos() > 1) {
                    if (torreSeleccionada.getUnidadesSotanos() < 1) {
                        this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                        this.addMensajeError(
                            "Por favor ingrese un valor para el total unidades de sótanos de la torre.");
                        validate = false;
                    }
                }
            }

            if (torreSeleccionada.getPisos() > 99) {
                this.addMensajeError("El máximo número de  piso permitido es 99.");
                validate = false;
            }

            if (torreSeleccionada.getTorre() != null &&
                 torreSeleccionada.getTorre() > 99) {

                this.addMensajeError("El máximo número de  torre permitido es 99.");
                validate = false;

            }

            if (torreSeleccionada.getSotanos() > 99) {
                this.addMensajeError("El máximo número de  piso y sótanos para una torre es 99.");
                validate = false;
            }

            if (torreSeleccionada.getPisos() + torreSeleccionada.getSotanos() > 99) {

                this.addMensajeError("La suma máxima entre pisos y sótanos para una torre es 99.");
                validate = false;

            }

            if (torreSeleccionada.getPisos() > torreSeleccionada.getUnidades()) {
                this.
                    addMensajeError("Deben existir por lo menos una unidad privada para cada piso.");
                validate = false;
            }

            if (torreSeleccionada.getUnidadesSotanos() < torreSeleccionada.getSotanos()) {

                this.addMensajeError(
                    "Deben existir por lo menos una unidad de sótano para cada sótano.");
                validate = false;
            }

            if (this.tramite.isEnglobeVirtual() && editModeTorre &&
                 this.fichaMatrizTorreReplica != null) {

                if (this.fichaMatrizTorreReplica.getPisos() > torreSeleccionada.getPisos()) {
                    this.addMensajeError("El número de pisos debe ser mayor al total.");
                    validate = false;
                }

                if (this.fichaMatrizTorreReplica.getUnidades() > torreSeleccionada.getUnidades()) {
                    this.addMensajeError(
                        "El número de unidades debe ser mayor al total de unidades.");
                    validate = false;
                }

                if (this.fichaMatrizTorreReplica.getSotanos() > torreSeleccionada.getSotanos()) {

                    this.addMensajeError("El número de sótanos debe ser mayor al total.");
                    validate = false;
                }

                if (this.fichaMatrizTorreReplica.getUnidadesSotanos() > torreSeleccionada.
                    getUnidadesSotanos()) {

                    this.
                        addMensajeError("El número de unidades de sótanos debe ser mayor al total.");
                    validate = false;
                }

            }

            // Si ya se han creado predios, para la torre seleccionada no se
            // puede realizar edición sobre las unidades.
            if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                 !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                // Se verifica si el valor que se quiere actualizar es el de las
                // unidades privadas o el de los sotanos, y posterior a ello se
                // valida que no hayan unidades creadas.
                if (verificarCambioValorUnidadesParaTorre(true)) {
                    if (torreSeleccionada.getOriginalTramite() != null &&
                         !torreSeleccionada.getOriginalTramite().isEmpty() &&
                         !ESiNo.SI.getCodigo().equals(torreSeleccionada.getOriginalTramite())) {
                        if (validarExistenPrediosCreadosEnTorre(torreSeleccionada)) {
                            this.addMensajeError(
                                "No se puede realizar edición o eliminación sobre las unidades privadas de la torre seleccionada pues para ésta ya se han creado predios. Para editar o eliminar las unidades de la torre seleccionada por favor elimine sus predios asociados.");
                            validate = false;
                        }

                        // Verificación en predios migrados para quintas masivos
                        if (this.tramite.isQuintaMasivo()) {
                            if (this.numerosPredialesGeneradosPorPiso[0][0] > torreSeleccionada.
                                getUnidades()) {
                                this.addMensajeError(
                                    "No se puede realizar edición sobre las unidades privadas de la torre seleccionada debido a que hay predios sin asociar a una torre. Por favor asocie los predios a una torre e intente nuevamente la edición de las unidades privadas de la torre.");
                                validate = false;
                            }
                        }

                    } else if (!validarValoresPisosTorreEnFirme(torreSeleccionada)) {
                        validate = false;
                    }
                }

                if (verificarCambioValorUnidadesParaTorre(false)) {
                    if (!ESiNo.SI.getCodigo().equals(torreSeleccionada.getOriginalTramite())) {
                        if (validarExistenSotanosCreadosEnTorre(torreSeleccionada)) {
                            this.addMensajeError(
                                "No se puede realizar edición o eliminación sobre los sótanos de la torre seleccionada pues para ésta ya se han creado predios. Para editar o eliminar las unidades de la torre seleccionada por favor elimine sus predios asociados.");
                            validate = false;
                        }
                    } else if (!validarValoresSotanosTorreEnFirme(torreSeleccionada)) {
                        validate = false;
                    }
                }

                if (tramite.isEnglobeVirtual() &&
                     ESiNo.SI.getCodigo().equals(torreSeleccionada.getOriginalTramite())) {

                    FichaMatrizTorre fichaMatrizTorre = new FichaMatrizTorre();

                    if (this.fichaMatrizTorreReplica.getTorre() != null &&
                         this.fichaMatrizTorreReplica.getProvieneFichaMatriz() != null) {

                        fichaMatrizTorre = this.getConservacionService().
                            buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
                                this.fichaMatrizTorreReplica.getTorre(),
                                this.fichaMatrizTorreReplica.getProvieneFichaMatriz().getId());

                    } else if (this.fichaMatrizTorreReplica.getProvieneTorre() != null &&
                         this.fichaMatrizTorreReplica.getProvieneTorre().getTorre() != null) {

                        fichaMatrizTorre = this.getConservacionService().
                            buscarTorreEnFirmePorIdFichaMatrizYNumeroTorre(
                                this.fichaMatrizTorreReplica.getProvieneTorre().getTorre(),
                                this.fichaMatrizTorreReplica.getProvieneFichaMatriz().getId());
                    }

                    if (fichaMatrizTorre != null) {

                        if (fichaMatrizTorre.getPisos() != null &&
                             torreSeleccionada.getPisos() != null &&
                             torreSeleccionada.getPisos() < fichaMatrizTorre.getPisos()) {
                            this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                            this.addMensajeError(
                                "El número de pisos debe ser mayor al total(existentes y canceladas).");
                            validate = false;

                        }

                        if (fichaMatrizTorre.getUnidades() != null &&
                             torreSeleccionada.getUnidades() != null &&
                             torreSeleccionada.getUnidades() < fichaMatrizTorre.getUnidades()) {
                            this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                            this.addMensajeError(
                                "El número de unidades debe ser mayor al total de unidades (existentes y canceladas).");
                            validate = false;
                        }

                        if (fichaMatrizTorre.getSotanos() != null &&
                            torreSeleccionada.getSotanos() != null &&
                             torreSeleccionada.getSotanos() < fichaMatrizTorre.getSotanos()) {
                            this.fichaMatrizTorreSeleccionada = this.fichaMatrizTorreReplica;
                            this.addMensajeError(
                                "El número de sotanos debe ser mayor al total de sotanos.");
                            validate = false;
                        }
                    }
                }
            }
        }

        return validate;
    }

    /**
     * Método que realiza las validaciones de los valores de sótanos si una torre se encuentra en
     * firme
     *
     * @param torreSeleccionada
     * @return
     */
    private boolean validarValoresSotanosTorreEnFirme(
        PFichaMatrizTorre torreSeleccionada) {
        boolean valido = true;
        boolean existeValorSotanoBool = false;
        int valorOriginalSotano = 0;

        FichaMatriz fichaMatrizOriginal = this.getConservacionService().
            getFichaMatrizByNumeroPredialPredio(this.fichaMatriz.getPPredio().getNumeroPredial());
        if (fichaMatrizOriginal != null && fichaMatrizOriginal.getFichaMatrizTorres() != null) {
            for (FichaMatrizTorre fmt : fichaMatrizOriginal.getFichaMatrizTorres()) {
                if (fmt.getTorre() != null &&
                     fmt.getTorre().longValue() == torreSeleccionada.getTorre().longValue()) {
                    valorOriginalSotano = fmt.getSotanos().intValue();
                    existeValorSotanoBool = true;
                }
            }
            if (!existeValorSotanoBool) {
                valorOriginalSotano = this.numerosPredialesGeneradosPorSotano[torreSeleccionada.
                    getTorre().intValue()].length;
            }
        }
        if (torreSeleccionada != null &&
             this.numerosPredialesGeneradosPorSotano[torreSeleccionada
                .getTorre().intValue()] != null) {
            // Validación de sotanos de la torre
            if ((torreSeleccionada.getSotanos().intValue() + 1) < valorOriginalSotano) {
                this.addMensajeError(
                    "Debido a que la torre se encuentra en firme, sólo se pueden aumentar los valores de sótanos.");
                valido = false;
            }
        }
        return valido;
    }

    /**
     * Método que realiza las validaciones de los valores de piso si una torre se encuentra en firme
     *
     * @param torreSeleccionada
     * @return
     */
    private boolean validarValoresPisosTorreEnFirme(
        PFichaMatrizTorre torreSeleccionada) {
        boolean valido = true;
        boolean existeValorPisoBool = false;
        int valorOriginalPiso = 0;

        FichaMatriz fichaMatrizOriginal = this.getConservacionService().
            getFichaMatrizByNumeroPredialPredio(this.fichaMatriz.getPPredio().getNumeroPredial());
        if (fichaMatrizOriginal != null && fichaMatrizOriginal.getFichaMatrizTorres() != null) {
            for (FichaMatrizTorre fmt : fichaMatrizOriginal.getFichaMatrizTorres()) {
                if (fmt.getTorre() != null &&
                     fmt.getTorre().longValue() == torreSeleccionada.getTorre().longValue()) {
                    valorOriginalPiso = fmt.getPisos().intValue();
                    existeValorPisoBool = true;
                }
            }
            if (!existeValorPisoBool) {
                valorOriginalPiso = this.numerosPredialesGeneradosPorPiso[torreSeleccionada.
                    getTorre().intValue()].length;
            }
        }

        if (this.tramite.isEnglobeVirtual() && editModeTorre &&
             this.fichaMatrizTorreReplica != null) {

            if (this.fichaMatrizTorreReplica.getTorre() > torreSeleccionada.getTorre()) {
                this.numerosPredialesGeneradosPorPiso = new int[torreSeleccionada.getTorre()
                    .intValue() + 1][torreSeleccionada.getPisos().intValue() + 1];
            }
        }

        if (torreSeleccionada != null &&
             (torreSeleccionada.getTorre() != null ||
             (this.numerosPredialesGeneradosPorPiso != null &&
             this.numerosPredialesGeneradosPorPiso[torreSeleccionada
                .getTorre().intValue()] != null))) {
            // Validación de pisos de la torre
            if ((torreSeleccionada.getPisos().intValue() + 1) < valorOriginalPiso) {
                this.addMensajeError(
                    "Debido a que la torre se encuentra en firme, sólo se pueden aumentar los valores de pisos.");
                valido = false;
            }
        }
        return valido;
    }

    /**
     * Método que valida la existencia de predios no desactivados para la torre seleccionada, se usa
     * principalmente para torres que se encuentran en firme en un trámite de PH por etapas.
     *
     * @param fichaMatrizTorre
     * @return
     */
    private boolean validarExistenciaPrediosPorTorre(PFichaMatrizTorre fichaMatrizTorre) {

        // Validación por números prediales
        if (this.getFichaMatrizPredios() != null) {
            for (PFichaMatrizPredio pfmp : this.getFichaMatrizPredios()) {
                if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().
                    equals(pfmp.getCancelaInscribe())) {
                    if (pfmp.getNumeroPredialOriginal() != null &&
                         pfmp.getNumeroPredialOriginal().length() > 24 &&
                         fichaMatrizTorre.getTorre() != null &&
                         pfmp.getNumeroPredialOriginal().substring(22, 24).equals(fichaMatrizTorre.
                            getTorre().toString())) {
                        return true;
                    } else if (pfmp.getNumeroPredial() != null &&
                         pfmp.getNumeroPredial().length() > 24 &&
                         fichaMatrizTorre.getTorre() != null &&
                         pfmp.getNumeroPredial().substring(22, 24).equals(fichaMatrizTorre.
                            getTorre().toString())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Método que valida la existencia de predios migrados.
     *
     * @return
     */
    private boolean validarExistenciaPrediosMigrados() {

        if (this.getFichaMatrizPredios() != null) {
            for (PFichaMatrizPredio pfmp : this.getFichaMatrizPredios()) {
                if (pfmp.getNumeroPredialOriginal() != null) {
                    String torre = pfmp.getNumeroPredialOriginal().substring(22, 24);
                    String piso = pfmp.getNumeroPredialOriginal().substring(24, 26);
                    if ("00".equals(piso) || "00".equals(torre)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // ---------------------------------------------------- //
    /**
     * Método para verificar si se ha cambiado el valor de las unidades privadas al momento de
     * editar la torre. Si el parametro enviado es true, se realiza la validacion sobre los pisos y
     * unidades privadas de la torre, por el contrario si es false se realiza la validacion sobre
     * los sotanos y unidades de sotanos.
     *
     * @author david.cifuentes
     * @param pisoOSotano
     * @return
     */
    public boolean verificarCambioValorUnidadesParaTorre(boolean pisoOSotano) {
        boolean cambia = false;
        if (this.fichaMatrizTorreReplica != null &&
             this.fichaMatrizTorreSeleccionada != null) {

            if (pisoOSotano) {
                if (this.fichaMatrizTorreReplica.getPisos() != this.fichaMatrizTorreSeleccionada
                    .getPisos() ||
                     this.fichaMatrizTorreReplica.getUnidades() != this.fichaMatrizTorreSeleccionada
                    .getUnidades()) {
                    cambia = true;
                }
            } else {
                if (this.fichaMatrizTorreReplica.getSotanos() != this.fichaMatrizTorreSeleccionada
                    .getSotanos() ||
                     this.fichaMatrizTorreReplica.getUnidadesSotanos() !=
                    this.fichaMatrizTorreSeleccionada
                        .getUnidadesSotanos()) {
                    cambia = true;
                }
            }
        }
        return cambia;
    }

    // ---------------------------------------------------- //
    /**
     * Método que acepta el total de unidades privadas para el condominio
     */
    public void aceptarUnidadesPrivadasCondominio() {

        boolean validateUnidades = validarUnidadesFichaMatriz(true);

        if (validateUnidades) {
            this.guardarFichaMatriz();
            // Inicialización de variables para la creación de los
            // números prediales
            this.unidadesTotales = 0L;
            this.unidadesAsignadas = 0L;
            this.unidadesRestantes = 0L;
            this.conteoUnidad = 0L;
            this.cargarMatrizNumerosPrediales();
            this.setupFichaMatrizReplica();
            this.addMensajeInfo("Se guardaron las unidades satisfactoriamente");

            this.consultarFichasMatrizCondominio();
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que prepara la inserción de una construcción convencional nueva.
     *
     * @author fabio.navarrete
     */
    public void setupAdicionarFichaMatrizTorre() {
        this.fichaMatrizTorreSeleccionada = new PFichaMatrizTorre();
        this.fichaMatrizTorreReplica = null;
        if (!this.tramite.isDesenglobeEtapas()) {
            Long numTorre = Long.valueOf(0);
            for (PFichaMatrizTorre pfmt : this.fichaMatriz
                .getPFichaMatrizTorres()) {
                if (pfmt.getTorre() != null &&
                     pfmt.getTorre().compareTo(numTorre) > 0) {
                    numTorre = pfmt.getTorre();
                }
            }
            this.fichaMatrizTorreSeleccionada.setTorre(++numTorre);
        }

        // Actualización de matriz de números prediales generados
        this.cargarMatrizNumerosPrediales();

        this.setEditModeTorre(false);

    }

    // --------------------------------------------------------------- //
    /**
     * Método que valida que se haya generado los predios adecuadamente. Para PH al menos uno por
     * cada piso. Para Condominio uno por cada terreno.
     *
     * @author david.cifuentes
     */
    public boolean validarPrediosPorPisoYTorre() {

        this.cargarMatrizNumerosPrediales();
        if (this.tramite.isTipoInscripcionCondominio()) {

            if (this.fichaMatriz != null &&
                 this.fichaMatriz.getPFichaMatrizPredios() != null &&
                 !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                for (int predio = 1; predio <= this.fichaMatriz
                    .getPFichaMatrizPredios().size(); predio++) {
                    if (this.numerosPredialesGeneradosCondominio[predio] <= 0) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else if (this.tramite.isTipoInscripcionPH()) {
            if (this.fichaMatriz != null &&
                 this.fichaMatriz.getPFichaMatrizTorres() != null &&
                 !this.fichaMatriz.getPFichaMatrizTorres().isEmpty()) {

                for (int torre = 1; torre <= this.fichaMatriz
                    .getPFichaMatrizTorres().size(); torre++) {
                    // Validación de pisos de la torre
                    for (int piso = 1; piso <= this.fichaMatriz
                        .getPFichaMatrizTorres().get(torre - 1).getPisos(); piso++) {
                        if (this.numerosPredialesGeneradosPorPiso[torre][piso] <= 0) {
                            return false;
                        }
                    }
                    // Validación de sotanos de la torre
                    for (int sotano = 1; sotano <= this.fichaMatriz
                        .getPFichaMatrizTorres().get(torre - 1)
                        .getSotanos(); sotano++) {
                        if (this.numerosPredialesGeneradosPorSotano[torre][sotano] <= 0) {
                            return false;
                        }
                    }
                }
            } else {
                return false;
            }
        }
        return true;
    }

    // --------------------------------------------------------------- //
    // ----------------------- EVENTOS ------------------------------- //
    // --------------------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar un fichaMatrizPredio en la tabla de generar números
     * pediales.
     *
     * @param event
     */
    public void onUnSelectFichaMatrizPredioRow(UnselectEvent event) {
        LOGGER.debug("FichaMatrizMB#onUnSelectFichaMatrizPredioRow");
    }

    // ------------------------------------------------------------- //
    /**
     * Método que se ejecuta al desseleccionar un fichaMatrizPredio en la tabla de generar números
     * pediales.
     *
     * @param event
     */
    public void onSelectFichaMatrizPredioRow(SelectEvent event) {
        LOGGER.debug("FichaMatrizMB#onSelectFichaMatrizPredioRow");
    }

    /**
     * Llamado al método que realiza los cambios en las tablas la proyección.
     *
     * @modified felipe.cadena - 04-12-2013 - Se evitan las validaciones cuando es un desenglobe de
     * mejora.
     */
    public void validarGuardar() {

        // Actualización de áreas de la ficha matriz.
        this.actualizarTotalesAreasEnFichaMatriz();

        if (!this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
            boolean resultadoValidacion = this.validarSeccion();
            proyectarConservacionMB.getValidezSeccion().put(
                ESeccionEjecucion.FICHA_MATRIZ, resultadoValidacion);
        }
    }

    /**
     * Método que realiza una validación de las secciones que componen la ficha matriz.
     *
     * @author david.cifuentes
     * @return
     */
    public boolean validarSeccion() {

        boolean valido;
        // En la cancelacion de predio no se muestra esta seccion, excepto en
        // condiciones de propiedad 8 o 9 donde es de solo lectura.
        // Las validaciones para los englobes virtuales se realizan de manera independiente
        if (this.tramite.isCancelacionPredio() || this.tramite.isEnglobeVirtual()) {
            valido = true;
        } else {
            valido = true;
            this.listValidacionDelTramite = new ArrayList<ValidacionProyeccionDelTramiteDTO>();
            ValidacionProyeccionDelTramiteDTO validacionAdicional = null;

            // ============================================================ //
            // =========== VALIDACIONES PROPIAS DE FICHA MATRIZ =========== //
            // ============================================================ //
            if (this.fichaMatriz == null) {
                proyectarConservacionMB.consultarFichaMatriz();
            }
            if (this.predioDesenglobe == null) {
                this.predioDesenglobe = this.proyectarConservacionMB
                    .getPredioSeleccionado();
            }
            if (this.tramite == null) {
                this.tramite = this.proyectarConservacionMB.getTramite();
            }

            if (this.getFichaMatriz() != null &&
                 this.fichaMatriz.getTotalUnidadesPrivadas() != null &&
                 this.fichaMatriz.getTotalUnidadesSotanos() != null) {

                // ================================================== //
                // ============ ENUMERACIÓN DE LOS PREDIOS ========== //
                // ================================================== //
                // Se realiza una enumeración de los predios generados.
                this.enumerarPFichaMatrizPredios();

                // ================================================== //
                Long totalUnidadesFichaMatriz = this.fichaMatriz
                    .getTotalUnidadesPrivadas() +
                     this.fichaMatriz.getTotalUnidadesSotanos();

                // ================================================== //
                // ==== GENERALIDADES DE LA PROPIEDAD HORIZONTAL ==== //
                // ================================================== //
                // La cantidad de unidades a desenglobar debe ser mayor a uno.
                if (totalUnidadesFichaMatriz < 1) {
                    validacionAdicional = new ValidacionProyeccionDelTramiteDTO();
                    if (this.predioDesenglobe != null) {
                        validacionAdicional.setPredioId(this.predioDesenglobe
                            .getId());
                    }
                    validacionAdicional.setTramiteId(this.tramite.getId());
                    validacionAdicional
                        .setSeccion(ESeccionEjecucion.FICHA_MATRIZ
                            .getNombre());
                    validacionAdicional
                        .setMensajeError(
                            "La cantidad de unidades a desenglobar debe ser mayor a uno en la subsección de generalidades del PH o condominio");
                    this.listValidacionDelTramite.add(validacionAdicional);
                }
                // ================================================== //
                // ========= GENERACIÓN DE NÚMEROS PREDIALES ======== //
                // ================================================== //
                // Validar números prediales - Se deben generar la totalidad
                // de los números prediales, para torres, pisos, sotanos y
                // en general todas las unidades según sea el caso para PH o
                // condominio.

                if (this.fichaMatriz.getPFichaMatrizPredios() == null ||
                     this.fichaMatriz.getPFichaMatrizPredios().size() != totalUnidadesFichaMatriz) {

                    validacionAdicional = new ValidacionProyeccionDelTramiteDTO();
                    if (this.predioDesenglobe != null) {
                        validacionAdicional.setPredioId(this.predioDesenglobe
                            .getId());
                    }
                    validacionAdicional.setTramiteId(this.tramite.getId());
                    validacionAdicional
                        .setSeccion(ESeccionEjecucion.FICHA_MATRIZ
                            .getNombre());
                    validacionAdicional
                        .setMensajeError(
                            "Existe una inconsistencia con el total de predios que deben generarse a partir de la ficha matriz");
                    this.listValidacionDelTramite.add(validacionAdicional);
                }

                // Debe existir al menos un predio por piso, en cada una de las
                // torres
                if (!validarPrediosPorPisoYTorre()) {

                    validacionAdicional = new ValidacionProyeccionDelTramiteDTO();
                    if (this.predioDesenglobe != null) {
                        validacionAdicional.setPredioId(this.predioDesenglobe
                            .getId());
                    }
                    validacionAdicional.setTramiteId(this.tramite.getId());
                    validacionAdicional
                        .setSeccion(ESeccionEjecucion.FICHA_MATRIZ
                            .getNombre());
                    if (this.tramite.isTipoInscripcionPH()) {
                        validacionAdicional
                            .setMensajeError(
                                "Debe existir al menos una unidad por piso o sotano en todas los pisos y sotanos de las torres de la ficha matriz");
                    } else {
                        validacionAdicional
                            .setMensajeError("Faltan unidades por generar en la ficha matriz.");
                    }
                    this.listValidacionDelTramite.add(validacionAdicional);
                }

                // ================================================== //
                // =========== COEEFICIENTES DE COPROPIEDAD ========= //
                // ================================================== //
                // Todos los predios resultantes deben tener un coeficiente
                // de copropiedad.
                boolean aux = true;
                for (PFichaMatrizPredio pfmp : this.fichaMatriz
                    .getPFichaMatrizPredios()) {
                    if (pfmp.getCoeficiente() == null ||
                         pfmp.getCoeficiente() <= 0D) {
                        aux = false;
                        break;
                    }
                }
                if (!aux) {
                    validacionAdicional = new ValidacionProyeccionDelTramiteDTO();
                    if (this.predioDesenglobe != null) {
                        validacionAdicional.setPredioId(this.predioDesenglobe
                            .getId());
                    }
                    validacionAdicional.setTramiteId(this.tramite.getId());
                    validacionAdicional
                        .setSeccion(ESeccionEjecucion.FICHA_MATRIZ
                            .getNombre());
                    validacionAdicional
                        .setMensajeError(
                            "Existe uno o más predios generados a los cuales no se les ha asignado un coeficiente de copropiedad");
                    this.listValidacionDelTramite.add(validacionAdicional);
                }

            } else {
                // Debe existir una cantidad válida de predios a desenglobar.
                validacionAdicional = new ValidacionProyeccionDelTramiteDTO();
                if (this.predioDesenglobe != null) {
                    validacionAdicional.setPredioId(this.predioDesenglobe
                        .getId());
                }
                validacionAdicional.setTramiteId(this.tramite.getId());
                validacionAdicional.setSeccion(ESeccionEjecucion.FICHA_MATRIZ
                    .getNombre());
                validacionAdicional
                    .setMensajeError(
                        "No se ha ingresado un valor para las unidades en la sección de generalidades del PH o condominio");
                this.listValidacionDelTramite.add(validacionAdicional);
            }

            // Retornar resultado de validación
            if (this.listValidacionDelTramite != null &&
                 !this.listValidacionDelTramite.isEmpty()) {
                if (this.proyectarConservacionMB
                    .getListaValidacionesTramiteSegundaDesenglobe() != null) {
                    this.proyectarConservacionMB
                        .getListaValidacionesTramiteSegundaDesenglobe()
                        .addAll(this.listValidacionDelTramite);
                }

                valido = false;
            }
        }
        return valido;
    }

    /**
     * Método que realiza la validación únicamente para la ficha matriz que se encuentra como predio
     * seleccionado.
     *
     * @author david.cifuentes
     *
     */
    public void validarSeccionFichaMatriz() {

        boolean resultadoValidacion = true;

        if (this.fichaMatriz == null) {
            proyectarConservacionMB.consultarFichaMatriz();
        }
        if (this.predioDesenglobe == null) {
            this.predioDesenglobe = this.proyectarConservacionMB
                .getPredioSeleccionado();
        }
        if (this.tramite == null) {
            this.tramite = this.proyectarConservacionMB.getTramite();
        }

        if (this.getFichaMatriz() != null &&
             this.fichaMatriz.getTotalUnidadesPrivadas() != null &&
             this.fichaMatriz.getTotalUnidadesSotanos() != null) {

            Long totalUnidadesFichaMatriz = this.fichaMatriz
                .getTotalUnidadesPrivadas() +
                 this.fichaMatriz.getTotalUnidadesSotanos();

            // ================================================== //
            // ==== GENERALIDADES DE LA PROPIEDAD HORIZONTAL ==== //
            // ================================================== //
            // La cantidad de unidades a desenglobar debe ser mayor a uno.
            if (totalUnidadesFichaMatriz < 1) {
                this.addMensajeError(
                    "La cantidad de unidades a desenglobar debe ser mayor a uno en la subsección de generalidades del PH o condominio");
                resultadoValidacion = false;
            }
            // ================================================== //
            // ========= GENERACIÓN DE NÚMEROS PREDIALES ======== //
            // ================================================== //
            // Validar números prediales - Se deben generar la totalidad
            // de los números prediales, para torres, pisos, sotanos y
            // en general todas las unidades según sea el caso para PH o
            // condominio.

            if (this.fichaMatriz.getPFichaMatrizPredios() == null ||
                 this.fichaMatriz.getPFichaMatrizPredios().size() != totalUnidadesFichaMatriz) {

                this.addMensajeError(
                    "Existe una inconsistencia con el total de predios que deben generarse a partir de la ficha matriz");
                resultadoValidacion = false;
            } else {
                this.addMensajeInfo("Se validó la generación de la totalidad de los predios");
            }

            // Debe existir al menos un predio por piso, en cada una de las
            // torres
            if (!this.tramite.isRectificacionMatriz()) {
                if (!validarPrediosPorPisoYTorre()) {
//                    if (this.tramite.isTipoInscripcionPH()) {
//                        addMensajeError("Debe existir al menos una unidad por piso o sotano en todas los pisos y sotanos de las torres de la ficha matriz");
//                    } else {
//                        addMensajeError("Debe existir al menos una unidad por cada uno de los terrenos del condominio de la ficha matriz");
//                    }
//                    resultadoValidacion = false;
                } else {
                    this.addMensajeInfo("Todos los pisos de las torres tienen predios asignados");
                }
            }

            // ================================================== //
            // =========== COEEFICIENTES DE COPROPIEDAD ========= //
            // ================================================== //
            // Todos los predios resultantes deben tener un coeficiente
            // de copropiedad.
            boolean aux = true;
            for (PFichaMatrizPredio pfmp : this.fichaMatriz
                .getPFichaMatrizPredios()) {
                if (pfmp.getCoeficiente() == null ||
                     pfmp.getCoeficiente() <= 0D) {
                    aux = false;
                    break;
                }
            }
            if (!aux) {
                addMensajeError(
                    "Existe uno o más predios generados a los cuales no se les ha asignado un coeficiente de copropiedad");
                resultadoValidacion = false;
            } else {
                this.addMensajeInfo("Se validó el coeficiente de copropiedad para cada predio");
            }

        } else {
            // Debe existir una cantidad válida de predios a desenglobar.
            this.addMensajeError(
                "No se ha ingresado un valor para las unidades en la sección de generalidades del PH o condominio");
            resultadoValidacion = false;
        }

        proyectarConservacionMB.getValidezSeccion().put(
            ESeccionEjecucion.FICHA_MATRIZ, resultadoValidacion);
    }

    /**
     * Método que se ejecuta al desseleccionar un fichaMatrizPredio en la tabla de generar números
     * pediales.
     *
     * @param event
     */
    public void onSelectUnidadtesRow(SelectEvent event) {
        LOGGER.debug("FichaMatrizMB#onSelectUnidadtesRow");
    }

    /**
     * Método que calcula el valor total del avaluó para mutaciones de segunda desenglobe.
     *
     * @author juanfelipe.garcia
     *
     */
    public void calcularAvaluoTotal() {
        // sumatoria de los avaluos individuales de las construcciones
        // convencionales Vigentes
        BigDecimal avaluoTotalConstruccionesVigentes = new BigDecimal(0);
        if (this.unidadConstruccionConvencionalVigentes != null &&
             !this.unidadConstruccionConvencionalVigentes.isEmpty()) {
            for (PUnidadConstruccion punid : this.unidadConstruccionConvencionalVigentes) {
                if (punid.getCancelaInscribe() != null &&
                     !punid.getCancelaInscribe().equals(
                        EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    avaluoTotalConstruccionesVigentes = avaluoTotalConstruccionesVigentes
                        .add(new BigDecimal(punid.getAvaluo()));
                }
            }
        }

        // sumatoria de los avaluos individuales de las construcciones no
        // convencionales Vigentes
        BigDecimal avaluoTotalConstruccionesNoConvencionalVigentes = new BigDecimal(
            0);
        if (this.unidadConstruccionNoConvencionalVigentes != null &&
             !this.unidadConstruccionNoConvencionalVigentes.isEmpty()) {
            for (PUnidadConstruccion punid : this.unidadConstruccionNoConvencionalVigentes) {
                if (punid.getCancelaInscribe() != null &&
                     !punid.getCancelaInscribe().equals(
                        EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                    avaluoTotalConstruccionesNoConvencionalVigentes =
                        avaluoTotalConstruccionesNoConvencionalVigentes
                            .add(new BigDecimal(punid.getAvaluo()));
                }
            }
        }

        // sumatoria de los avaluos individualess de las construcciones
        // convencionales nuevas
        BigDecimal avaluoTotalConstruccionesConvencionalesNuevas = new BigDecimal(
            0);
        if (this.unidadConstruccionConvencionalNuevasNoCanceladas != null &&
             !this.unidadConstruccionConvencionalNuevasNoCanceladas.isEmpty()) {
            for (PUnidadConstruccion punid : this.unidadConstruccionConvencionalNuevasNoCanceladas) {
                avaluoTotalConstruccionesConvencionalesNuevas =
                    avaluoTotalConstruccionesConvencionalesNuevas
                        .add(new BigDecimal(punid.getAvaluo()));
            }
        }

        // sumatoria de los avaluos individuales de las construcciones no
        // convencionales nuevas
        BigDecimal avaluoTotalConstruccionesNoConvencionalesNuevas = new BigDecimal(
            0);
        if (this.unidadConstruccionNoConvencionalNuevasNoCanceladas != null &&
             !this.unidadConstruccionNoConvencionalNuevasNoCanceladas.isEmpty()) {
            for (PUnidadConstruccion punid : this.unidadConstruccionNoConvencionalNuevasNoCanceladas) {
                avaluoTotalConstruccionesNoConvencionalesNuevas =
                    avaluoTotalConstruccionesNoConvencionalesNuevas
                        .add(new BigDecimal(punid.getAvaluo()));
            }
        }

        // calcular el valor del terreno privado
        BigDecimal avaluoTerrenoPrivado = new BigDecimal(0);
        // TODO juanfelipe.garcia :: hacer el calculo del valor del avaluo
        // terreno privado

        this.fichaMatriz.getAreaTotalTerrenoPrivada();
        // calcular avalúo total
        this.avaluoTotal = new BigDecimal(0);
        // adicion de valores del avaluo del terreno
        if (this.avaluoTotalTerrenoComun != null) {
            this.avaluoTotal = this.avaluoTotal.add(new BigDecimal(
                this.avaluoTotalTerrenoComun));
        }

        this.avaluoTotal = this.avaluoTotal.add(avaluoTerrenoPrivado);
        // adicion de valores de avaluo construcciones
        this.avaluoTotal = this.avaluoTotal
            .add(avaluoTotalConstruccionesVigentes);
        this.avaluoTotal = this.avaluoTotal
            .add(avaluoTotalConstruccionesNoConvencionalVigentes);
        this.avaluoTotal = this.avaluoTotal
            .add(avaluoTotalConstruccionesConvencionalesNuevas);
        this.avaluoTotal = this.avaluoTotal
            .add(avaluoTotalConstruccionesNoConvencionalesNuevas);

    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza un recorrido de los predios creados a partir de la ficha matriz y los
     * enumera comenzando por los pisos, hacia arriba y siguiendo con los sotanos, de tal manera que
     * el número del último predio (incluyendo los sotanos) es igual a la cantidad total de los
     * predios de la ficha matriz.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void enumerarPFichaMatrizPredios() {

        // La enumeración de los predios se realiza desde el piso uno hacia
        // arriba y posteriormente los sotanos desde el primer sotan hacia
        // abajo.
        try {

            Double consecutivoUnidad = new Double(1);

            if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                 !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                // Se capturan los números prediales para ser ordenados
                HashMap<Long, String> numerosPrediales = new HashMap<Long, String>();
                for (PFichaMatrizPredio fmp : this.fichaMatriz
                    .getPFichaMatrizPredios()) {
                    numerosPrediales.put(fmp.getId(), fmp.getNumeroPredial());
                }

                // ----------------------------------------------- //
                // ---- Ordenamiento de los números prediales ---- //
                // ----------------------------------------------- //
                HashMap numerosPredialesResult = new LinkedHashMap();

                List listaIds = new ArrayList(numerosPrediales.keySet());

                List listaNumerosPrediales = new ArrayList(
                    numerosPrediales.values());

                TreeSet conjuntoOrdenado = new TreeSet(listaNumerosPrediales);

                Object[] arrayOrdenado = conjuntoOrdenado.toArray();

                for (int i = 0; i < arrayOrdenado.length; i++) {
                    numerosPredialesResult.put(listaIds
                        .get(listaNumerosPrediales
                            .indexOf(arrayOrdenado[i])),
                        arrayOrdenado[i]);
                }

                Iterator iteradorId = numerosPredialesResult.keySet()
                    .iterator();
                Long idPFichaMatrizPredio = null;
                while (iteradorId.hasNext()) {
                    idPFichaMatrizPredio = (Long) iteradorId.next();
                    for (PFichaMatrizPredio fmp : this.fichaMatriz
                        .getPFichaMatrizPredios()) {
                        if (fmp.getId().longValue() == idPFichaMatrizPredio) {
                            fmp.setConsecutivoUnidad(consecutivoUnidad);
                            break;
                        }
                    }
                    consecutivoUnidad++;
                }

                // Guardar la lista de los PFichaMatrizPredios con su nuevos
                // consecutivos.
                this.guardarFichaMatriz();
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error al enumerar los predios");
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de todos los predios generados en la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void seleccionarTodosPrediosGenerados() {
        List<PFichaMatrizPredio> prediosSeleccionados = new ArrayList<PFichaMatrizPredio>();
        this.fichaMatrizPrediosResultantes = this.fichaMatriz.getPFichaMatrizPredios();
        for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
            if (tramite.isEnglobeVirtual()) {

                if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado())) {
                    pfmp.setSelected(this.selectedAllBool);
                    prediosSeleccionados.add(pfmp);
                }

            } else if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                getCancelaInscribe())) {
                pfmp.setSelected(this.selectedAllBool);
                prediosSeleccionados.add(pfmp);
            }
        }
        if (this.selectedAllBool) {
            this.fichaMatrizPrediosSeleccionados = new PFichaMatrizPredio[prediosSeleccionados.
                size()];
            for (int i = 0; i < prediosSeleccionados.size(); i++) {
                fichaMatrizPrediosSeleccionados[i] = prediosSeleccionados.get(i);
            }
            // Habilitar botón de modificación de número predial sólo para predios provenientes de migración
            this.modificarPrediosMigracionBool = this.validarPrediosProvenientesMigracion();
        } else {
            this.fichaMatrizPrediosSeleccionados = null;
            this.modificarPrediosMigracionBool = false;
        }

    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de uno de los predios generados mediante la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void updateSelectedPrediosGenerados() {

        this.fichaMatrizPrediosResultantes = this.getFichaMatrizPredios();
        this.seleccionarPrediosPorCheck();

        this.modificarPrediosMigracionBool = false;

        //javier.aponte :: 15/07/2015 :: Se agrega la validación para trámites de tercera masivos
        if ((this.tramite.isDesenglobeEtapas() ||
             this.tramite.isTerceraMasiva() ||
             this.tramite.isRectificacion() ||
             this.tramite.isQuintaMasivo()) &&
             this.fichaMatrizPrediosSeleccionados != null &&
             this.fichaMatrizPrediosSeleccionados.length > 0) {
            this.unidad = new Long(this.fichaMatrizPrediosSeleccionados.length);

            // Habilitar botón de modificación de número predial sólo para predios provenientes de migración
            this.modificarPrediosMigracionBool = this.validarPrediosProvenientesMigracion();
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado para realizar el cargue de los predios seleccionados mediante el checkBox.
     *
     * @author david.cifuentes
     */
    public void seleccionarPrediosPorCheck() {
        List<PFichaMatrizPredio> auxListaPredios = new ArrayList<PFichaMatrizPredio>();
        for (PFichaMatrizPredio pfmp : this.getFichaMatrizPredios()) {

            if (tramite.isEnglobeVirtual()) {

                if (!EPredioEstado.CANCELADO.getCodigo().equals(pfmp.getEstado()) &&
                     pfmp.isSelected()) {
                    auxListaPredios.add(pfmp);
                }

            } else if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                getCancelaInscribe()) && pfmp.isSelected()) {
                auxListaPredios.add(pfmp);
            }
        }
        this.fichaMatrizPrediosSeleccionados = new PFichaMatrizPredio[auxListaPredios
            .size()];
        for (int i = 0; i < auxListaPredios.size(); i++) {
            fichaMatrizPrediosSeleccionados[i] = auxListaPredios.get(i);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado para verificar las unidades que permiten edición de la ficha matriz, las que
     * vienen asociadas al predio proyectado no la permiten
     *
     * @author david.cifuentes
     */
    public void verificarEdicionUCMantiene() {

        List<Long> idsUnidadesPredioBase = new ArrayList<Long>();
        // Cargue de los id de las unidades construcción del predio base
        List<UnidadConstruccion> unidadConstruccionPredioBase = this
            .getConservacionService()
            .obtenerUnidadesConstruccionsPorNumeroPredial(
                this.tramite.getPredio().getNumeroPredial());

        if (unidadConstruccionPredioBase != null &&
             !unidadConstruccionPredioBase.isEmpty()) {

            for (UnidadConstruccion uc : unidadConstruccionPredioBase) {
                idsUnidadesPredioBase.add(uc.getId().longValue());
            }

            if (!idsUnidadesPredioBase.isEmpty()) {

                // Verificar si el número predial se mantuvo o no
                if (this.fichaMatriz != null && this.fichaMatriz.getPPredio().getId().longValue() == this
                    .getTramite().getPredio().getId()) {
                    // se comentareo esta linea por redundancia cliclica con el
                    // branch felipe.cadena_9997
                    // this.updateConstruccionesNuevasNoCanceladas();

                    // Si se mantuvo los ids de las punidades n son iguales a
                    // los de las unidades iniciales
                    if (this.unidadConstruccionConvencionalNuevasNoCanceladas != null &&
                         !this.unidadConstruccionConvencionalNuevasNoCanceladas
                            .isEmpty()) {
                        for (PUnidadConstruccion pu
                            : this.unidadConstruccionConvencionalNuevasNoCanceladas) {
                            if (idsUnidadesPredioBase.contains(pu.getId()
                                .longValue())) {
                                pu.setPertenecePredioProyectado(true);
                            }
                        }
                    }
                    if (this.unidadConstruccionNoConvencionalNuevasNoCanceladas != null &&
                         !this.unidadConstruccionNoConvencionalNuevasNoCanceladas
                            .isEmpty()) {
                        for (PUnidadConstruccion pu
                            : this.unidadConstruccionNoConvencionalNuevasNoCanceladas) {
                            if (idsUnidadesPredioBase.contains(pu.getId()
                                .longValue())) {
                                pu.setPertenecePredioProyectado(true);
                            }
                        }
                    }
                }
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que realiza la asignación de torre y piso para predios provenientes de migración
     *
     * @author david.cifuentes
     */
    public void modificarNumerosPrediales() {

        try {
            Vector<String> valorAuxiliar;
            String torre = "";
            String piso = "";
            String unidad = "";
            String parteFija = "";
            List<PPredio> prediosModificados = new ArrayList<PPredio>();

            // TamaÃ±os de las partes del número predial.
            int tamAuxiliar;
            final int torreTam = 2;
            final int pisoTam = 2;
            final int unidadTam = 4;

            this.unidad = new Long(this.fichaMatrizPrediosSeleccionados.length);

            if (this.tramite.isEnglobeVirtual()) {

                if (this.predioCondProp9) {

                    if (this.torreSeleccionada == null ||
                         this.torreSeleccionada.isEmpty() ||
                         this.torreSeleccionada.equals(Constantes.CERO)) {
                        this.addMensajeError(
                            "Falta registrar datos obligatorios, como la torre seleccionada");
                        this.addErrorCallback();
                        this.cerrar();
                        return;
                    }

                    if (this.fichaMatrizPrediosSeleccionados != null) {

                        String torrePredSelOrig = new String();
                        String pisoPredSelOrig = new String();

                        for (PFichaMatrizPredio pFichaMatrizPredio
                            : this.fichaMatrizPrediosSeleccionados) {

                            if (pFichaMatrizPredio.getNumeroPredialOriginal() != null &&
                                 !pFichaMatrizPredio.getNumeroPredialOriginal().isEmpty()) {

                                torrePredSelOrig = pFichaMatrizPredio.getNumeroPredialOriginal().
                                    substring(22, 24);
                                pisoPredSelOrig = pFichaMatrizPredio.getNumeroPredialOriginal().
                                    substring(24, 26);
                            }

                            Long torreLong = Long.valueOf(this.torreSeleccionada);

                            if (ESiNo.SI.getCodigo().equals(pFichaMatrizPredio.getOriginalTramite()) &&
                                 pisoPredSelOrig != null && !pisoPredSelOrig.isEmpty() &&
                                 torrePredSelOrig != null && !torrePredSelOrig.isEmpty() &&
                                 !("00".equals(torrePredSelOrig) && "00".equals(pisoPredSelOrig))) {

                                String torreS = pFichaMatrizPredio.getNumeroPredialOriginal().
                                    substring(23, 24);
                                Long torreL = Long.valueOf(torreS);

                                if (torreL != null &&
                                     torreLong != torreL) {

                                    this.addMensajeError("El predio " + pFichaMatrizPredio.
                                        getNumeroPredial() +
                                        
                                        " no está en la misma torre que los predios que se encontraban asignados a la misma torre");
                                    this.addErrorCallback();
                                    this.cerrar();
                                    return;
                                }

                            }
                        }
                    }
                }

                if (this.predioCondProp8) {

                    if (this.unidadInicialSeleccionada <= 0) {
                        this.addMensajeError(
                            "Falta registrar datos obligatorios, como el numero de Unidad Inicial");
                        this.addErrorCallback();
                        this.cerrar();
                        return;
                    }
                }

                String fichaMatrizCondProp = new String();
                String fichaMatrizPredDesNumPredCompl = new String();
                String fichaMatrizPredAntesNumPred = new String();
                String unidadFinalPredio = new String();
                String nuevoPredio = new String();
                String temPredSelec = new String();
                String numeroPredFichaMatriz = new String();
                String fichaMatrizPiso = new String();
                int contador = 0;
                int contadorUnid = 0;

                numeroPredFichaMatriz = this.fichaMatriz.getPPredio().
                    getNumeroPredial().substring(17, 21);

                String unidInicialSelec = "";

                if (unidadInicialSelec.length() == 1) {

                    unidInicialSelec = "000" + unidadInicialSelec;

                } else if (unidadInicialSelec.length() == 2) {

                    unidInicialSelec = "00" + unidadInicialSelec;

                } else if (unidadInicialSelec.length() == 3) {

                    unidInicialSelec = "0" + unidadInicialSelec;

                } else if (unidadInicialSelec.length() == 4) {

                    unidInicialSelec = unidadInicialSelec;

                }

                String unidSelecTemp = "";
                int unidConv = 0;
                //Dependiendo de la cantidad de  predios seleccionados 
                // y el nuevo numero de unidad se editan los numeros prediales
                // de la ficha matriz
                for (PFichaMatrizPredio pFichaMPredioSelVal : this.fichaMatrizPrediosSeleccionados) {

                    contadorUnid++;

                    if (contadorUnid == 1) {
                        if (!unidInicialSelec.isEmpty()) {
                            unidSelecTemp = unidInicialSelec;
                        }

                    } else {
                        if (!unidSelecTemp.isEmpty()) {
                            unidConv = Integer.valueOf(unidSelecTemp);
                            unidConv++;
                        }

                    }

                    if (unidConv != 0) {
                        unidSelecTemp = String.valueOf(unidConv);
                    }

                    if (unidSelecTemp.length() == 1) {
                        unidSelecTemp = "000" + unidSelecTemp;
                    } else if (unidSelecTemp.length() == 2) {
                        unidSelecTemp = "00" + unidSelecTemp;
                    } else if (unidSelecTemp.length() == 3) {
                        unidSelecTemp = "0" + unidSelecTemp;
                    }

                    String numPredAntesFichSel = new String();
                    numPredAntesFichSel =
                         pFichaMPredioSelVal.getNumeroPredial().substring(0, 17);

                    String numPredDesFichSel = new String();

                    numPredDesFichSel =
                         pFichaMPredioSelVal.getNumeroPredial().substring(21, 26);

                    temPredSelec = new String();

                    temPredSelec = numPredAntesFichSel + numeroPredFichaMatriz + numPredDesFichSel +
                        unidSelecTemp;

                    List<PFichaMatrizPredio> prediosFicha = this.getConservacionService().
                        buscarFichaMatrizPredioPorPFichaMatrizId(this.fichaMatriz.getId());

                    for (PFichaMatrizPredio pfmpVal : prediosFicha) {

                        if (pfmpVal.getNumeroPredial() != null && !pfmpVal.getNumeroPredial().
                            isEmpty() &&
                             temPredSelec.equals(pfmpVal.getNumeroPredial())) {

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_060
                                .getMensajeUsuario(unidSelecTemp));
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_060
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            this.cerrar();
                            return;
                        }

                        if (pfmpVal.getNumeroPredialOriginal() != null && !pfmpVal.
                            getNumeroPredialOriginal().isEmpty() &&
                             temPredSelec.equals(pfmpVal.getNumeroPredialOriginal())) {

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_060
                                .getMensajeUsuario(this.unidadFinalSeleccionada));
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_060
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            this.cerrar();
                            return;
                        }

                    }

                }

                for (PFichaMatrizPredio pFichaMPredioSel : this.fichaMatrizPrediosSeleccionados) {

                    if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                         !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                        for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {

                            if (pFichaMPredioSel.getId().equals(pfmp.getId()) &&
                                 pfmp.getNumeroPredial() != null &&
                                 !pfmp.getNumeroPredial().isEmpty()) {

                                fichaMatrizPredAntesNumPred = new String();
                                fichaMatrizPredAntesNumPred =
                                     pfmp.getNumeroPredial().substring(0, 17);

                                fichaMatrizCondProp = new String();
                                fichaMatrizPredDesNumPredCompl = new String();
                                fichaMatrizPiso = new String();

                                fichaMatrizPredDesNumPredCompl =
                                     pfmp.getNumeroPredial().substring(21, 26);
                                fichaMatrizCondProp =
                                     pfmp.getNumeroPredial().substring(21, 22);

                                fichaMatrizPiso =
                                     pfmp.getNumeroPredial().substring(24, 26);

                                int intValueInicial = 0;
                                if (this.unidadInicialSelec == null || this.unidadInicialSelec.
                                    isEmpty()) {
                                    intValueInicial = this.fichaMatrizPrediosSeleccionados.length;
                                } else {
                                    intValueInicial = Integer.valueOf(this.unidadInicialSelec);
                                }

                                contador++;
                                if (this.predioCondProp8) {

                                    int intValueFinal = 0;

                                    if (contador == 1) {
                                        intValueFinal = intValueInicial;
                                    } else {
                                        int unFinTemp = Integer.valueOf(unidadFinalPredio);
                                        intValueFinal = (unFinTemp + 1);
                                    }

                                    String convIntValueFinal = String.valueOf(intValueFinal);

                                    unidadFinalPredio = new String();

                                    for (int i = 0; i <= 4; i++) {

                                        if (convIntValueFinal.length() == 4) {

                                            unidadFinalPredio = "000" + convIntValueFinal;
                                            break;
                                        } else if (unidadFinalPredio.length() == 4) {

                                            break;

                                        } else {

                                            if (i == 0) {
                                                unidadFinalPredio = '0' + convIntValueFinal;
                                            } else {
                                                unidadFinalPredio = '0' + unidadFinalPredio;
                                            }

                                        }
                                    }

                                    nuevoPredio = new String();

                                    nuevoPredio = fichaMatrizPredAntesNumPred +
                                        numeroPredFichaMatriz + fichaMatrizPredDesNumPredCompl +
                                        unidadFinalPredio;

                                    this.unidadInicialSelec = unidadFinalPredio;

                                } else if (this.predioCondProp9) {

                                    // Piso si es un piso migrado y el piso trae valor de 00
                                    if (this.pisoOSotanoSeleccionado != null &&
                                         pfmp.getNumeroPredial().substring(24, 26).equals("00")) {

                                        if (this.ejeVialHaciaAbajo.equals(ESiNo.NO
                                            .getCodigo())) {
                                            tamAuxiliar = pisoTam -
                                                 this.pisoOSotanoSeleccionado.length();
                                            valorAuxiliar = new Vector<String>();
                                            if (tamAuxiliar > 0) {
                                                valorAuxiliar.setSize(tamAuxiliar);
                                                Collections.fill(valorAuxiliar, "0");
                                                piso = StringUtils.join(valorAuxiliar, "").concat(
                                                    this.pisoOSotanoSeleccionado);
                                            } else {
                                                piso = this.pisoOSotanoSeleccionado;
                                            }
                                        } else {
                                            piso = "" + (100 - Integer.valueOf(
                                                this.pisoOSotanoSeleccionado));
                                        }

                                        fichaMatrizPredDesNumPredCompl =
                                             pfmp.getNumeroPredial().substring(21, 24) + piso;
                                    }

                                    unidadFinalPredio = new String();
                                    unidadFinalPredio = pfmp.getNumeroPredial().substring(26, 30);

                                    nuevoPredio = new String();

                                    if (this.pisoOSotanoSeleccionado != null &&
                                         !this.pisoOSotanoSeleccionado.isEmpty() &&
                                         this.torreSeleccionada != null &&
                                         !this.torreSeleccionada.isEmpty() &&
                                         !this.torreSeleccionada.equals(Constantes.CERO) &&
                                         !this.pisoOSotanoSeleccionado.equals(Constantes.CERO)) {

                                        if (this.pisoOSotanoSeleccionado.length() == 1) {
                                            this.pisoOSotanoSeleccionado = "0" +
                                                this.pisoOSotanoSeleccionado;
                                        }

                                        if (this.torreSeleccionada.length() == 1) {
                                            this.torreSeleccionada = "0" + this.torreSeleccionada;
                                        }

                                        nuevoPredio = fichaMatrizPredAntesNumPred +
                                            numeroPredFichaMatriz +
                                             fichaMatrizCondProp + torreSeleccionada +
                                            this.pisoOSotanoSeleccionado + unidadFinalPredio;

                                    } else if (this.torreSeleccionada != null &&
                                         !this.torreSeleccionada.isEmpty() &&
                                         !this.torreSeleccionada.equals(Constantes.CERO)) {

                                        if (this.torreSeleccionada.length() == 1) {
                                            this.torreSeleccionada = "0" + this.torreSeleccionada;
                                        }

                                        nuevoPredio = fichaMatrizPredAntesNumPred +
                                            numeroPredFichaMatriz +
                                             fichaMatrizCondProp + torreSeleccionada +
                                            fichaMatrizPiso + unidadFinalPredio;
                                    } else {
                                        nuevoPredio = fichaMatrizPredAntesNumPred +
                                            numeroPredFichaMatriz +
                                             fichaMatrizPredDesNumPredCompl + unidadFinalPredio;
                                    }

                                }

                                if (this.proyectarConservacionMB.getpPrediosDesenglobe() != null &&
                                     !this.proyectarConservacionMB.getpPrediosDesenglobe().isEmpty()) {

                                    for (PPredio pPredioTemp : this.proyectarConservacionMB.
                                        getpPrediosDesenglobe()) {

                                        if (pPredioTemp.getNumeroPredial() != null && !pPredioTemp.
                                            getNumeroPredial().isEmpty() &&
                                             pfmp.getNumeroPredial().equals(pPredioTemp.
                                                getNumeroPredial())) {

                                            if (this.predioCondProp9) {
                                                if (this.torreSeleccionada != null) {
                                                    if (this.torreSeleccionada.length() == 1) {
                                                        this.torreSeleccionada = "0" +
                                                            this.torreSeleccionada;
                                                        pPredioTemp.setEdificio(
                                                            this.torreSeleccionada);
                                                    } else {
                                                        pPredioTemp.setEdificio(
                                                            this.torreSeleccionada);
                                                    }
                                                }

                                                if (this.pisoOSotanoSeleccionado != null) {

                                                    if (this.pisoOSotanoSeleccionado.length() == 1) {

                                                        this.pisoOSotanoSeleccionado = "0" +
                                                            this.pisoOSotanoSeleccionado;
                                                        pPredioTemp.setPiso(
                                                            this.pisoOSotanoSeleccionado);

                                                    } else {
                                                        pPredioTemp.setPiso(
                                                            this.pisoOSotanoSeleccionado);
                                                    }
                                                }
                                            }

                                            pPredioTemp.setPredio(numeroPredFichaMatriz);
                                            pPredioTemp.setUnidad(unidadFinalPredio);
                                            pPredioTemp.setNumeroPredial(nuevoPredio);
                                            pPredioTemp.setCancelaInscribe(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                                            this.getConservacionService().actualizarPPredio(
                                                pPredioTemp);
                                        }

                                    }
                                }

                                pfmp.setNumeroPredial(nuevoPredio);
                                pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.
                                    getCodigo());
                                this.getConservacionService().
                                    actualizarListaPFichaMatrizPredio(pfmp);

                            }
                        }

                    }

                }

                this.cerrar();

            } else {

                this.cargarMatrizNumerosPrediales();

                if (this.fichaMatrizPrediosSeleccionados != null &&
                     this.fichaMatrizPrediosSeleccionados.length > 0 &&
                     EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                        this.predioDesenglobe.getCondicionPropiedad())) {

                    for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {

                        Long i = 1L;
                        // PARTE FIJA DEL NÃMERO PREDIAL
                        parteFija = pfmp.getNumeroPredial().substring(0, 22);

                        if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo())) {
                            if (this.numerosPredialesGeneradosPorSotano[Integer
                                .valueOf(this.torreSeleccionada)][Integer
                                .valueOf(this.pisoOSotanoSeleccionado)] == 0) {
                                this.conteoUnidad = i;
                            } else {
                                this.conteoUnidad = Long
                                    .valueOf(this.numerosPredialesGeneradosPorSotano[Integer
                                        .valueOf(this.torreSeleccionada)][Integer
                                        .valueOf(this.pisoOSotanoSeleccionado)]);
                                this.conteoUnidad += 1;
                            }
                        } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO
                            .getCodigo())) {
                            if (this.numerosPredialesGeneradosPorPiso[Integer
                                .valueOf(this.torreSeleccionada)][Integer
                                .valueOf(this.pisoOSotanoSeleccionado)] == 0) {
                                this.conteoUnidad = i;
                            } else {
                                this.conteoUnidad = Long
                                    .valueOf(this.numerosPredialesGeneradosPorPiso[Integer
                                        .valueOf(this.torreSeleccionada)][Integer
                                        .valueOf(this.pisoOSotanoSeleccionado)]);
                                this.conteoUnidad += 1;
                            }
                        }
                        i++;

                        // Edificio o torre
                        if (this.torreSeleccionada != null) {
                            tamAuxiliar = torreTam -
                                 this.torreSeleccionada.length();
                            valorAuxiliar = new Vector<String>();

                            if (tamAuxiliar > 0) {
                                valorAuxiliar.setSize(tamAuxiliar);
                                Collections.fill(valorAuxiliar, "0");
                                torre = StringUtils.join(
                                    valorAuxiliar, "").concat(
                                        this.torreSeleccionada);
                            } else {
                                torre = this.torreSeleccionada;
                            }
                        }

                        // Piso
                        if (this.pisoOSotanoSeleccionado != null) {

                            if (this.ejeVialHaciaAbajo.equals(ESiNo.NO
                                .getCodigo())) {
                                tamAuxiliar = pisoTam -
                                     this.pisoOSotanoSeleccionado.length();
                                valorAuxiliar = new Vector<String>();
                                if (tamAuxiliar > 0) {
                                    valorAuxiliar.setSize(tamAuxiliar);
                                    Collections.fill(valorAuxiliar, "0");
                                    piso = StringUtils.join(valorAuxiliar, "").concat(
                                        this.pisoOSotanoSeleccionado);
                                } else {
                                    piso = this.pisoOSotanoSeleccionado;
                                }
                            } else {
                                piso = "" + (100 - Integer.valueOf(this.pisoOSotanoSeleccionado));
                            }
                        }

                        // Unidad
                        tamAuxiliar = unidadTam - 1 - this.conteoUnidad.toString().length();

                        unidad = "0";
                        valorAuxiliar = new Vector<String>();
                        if (tamAuxiliar > 0) {
                            valorAuxiliar.setSize(tamAuxiliar);
                            Collections.fill(valorAuxiliar, "0");
                            unidad = unidad.concat(StringUtils.join(
                                valorAuxiliar, "").concat(
                                    this.conteoUnidad.toString()));
                        } else {
                            unidad.concat(this.conteoUnidad.toString());
                        }

                        String numeroPredial = new String(parteFija + torre + piso + unidad);

                        marcarNumeroPredialAsignado();

                        PPredio pPredio = this.getConservacionService().getPPredioByNumeroPredial(
                            pfmp.getNumeroPredial());
                        if (pPredio == null) {
                            this.addMensajeError(
                                "Ocurrio un error al modificar los números prediales");
                        }
                        pPredio.setNumeroPredial(numeroPredial);
                        pPredio.setEdificio(torre);
                        pPredio.setPiso(piso);
                        pPredio.setUnidad(unidad);
                        pPredio.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                        pPredio.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                        pPredio.setFechaLog(new Date(System.currentTimeMillis()));
                        prediosModificados.add(pPredio);

                        if (pfmp.getNumeroPredialOriginal() == null) {
                            pfmp.setNumeroPredialOriginal(new String(pfmp.getNumeroPredial()));
                        }
                        pfmp.setNumeroPredial(numeroPredial);

                        pfmp.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                        pfmp.setFechaLog(new Date(System.currentTimeMillis()));
                        pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());

                        this.unidadesAsignadas += 1L;
                    }
                }
                // Guardar PFichaMatrizPredios
                this.getConservacionService().guardarPrediosFichaMatriz(Arrays.asList(
                    this.fichaMatrizPrediosSeleccionados));
                // Guardar PPredios
                this.getConservacionService().guardarPPredios(prediosModificados);

                // Consultar la ficha matriz actualizada
                this.fichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
                    this.fichaMatriz.getPPredio().getId());
                this.verificarTorresEnFirme();

            }

            this.proyectarConservacionMB
                .setFichaMatrizSeleccionada(this.fichaMatriz);

            actualizarUnidades();

            this.addMensajeInfo("Se completó el total de números prediales");

            this.pisoOSotanoBool = false;
            this.unicaUnidadBool = false;
            this.torreCargadaBool = true;
            this.selectedAllBool = false;
            this.proyectarConservacionMB.obligarGeografico();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError("Ocurrio un error al modificar los números prediales");
        }
    }

    /**
     * Método usado para verificar si los {@link PFichaMatrizPredio} seleccionados provienen de
     * migración.
     *
     * @author david.cifuentes
     *
     * @return
     */
    private boolean validarPrediosProvenientesMigracion() {

        if (this.fichaMatrizPrediosSeleccionados != null) {
            for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {
                if (pfmp.getNumeroPredialOriginal() != null) {

                    String terreno = pfmp.getNumeroPredialOriginal().substring(17, 21);
                    String condicionPropiedad = pfmp.getNumeroPredialOriginal().substring(21, 22);
                    String torre = pfmp.getNumeroPredialOriginal().substring(22, 24);
                    String piso = pfmp.getNumeroPredialOriginal().substring(24, 26);

                    if ("9".equals(condicionPropiedad)) {

                        // Se define que los predios provenientes de migración no tienen
                        // información de torre ni edificio y que de cobol vienen con
                        // las unidades de terreno de 801 a 899 para condominios y 901 a
                        // 999 para PHs
                        if (this.tramite.isEnglobeVirtual() && (!("00".equals(piso) || "00".equals(
                            torre)))) {
                            return false;
                        } else if (!this.tramite.isEnglobeVirtual() && !("00".equals(piso) || "00".
                            equals(torre) ||
                             (Integer.parseInt(terreno) > 901 && Integer.parseInt(terreno) < 999))) {
                            return false;
                        }
                    } else if (!this.tramite.isEnglobeVirtual()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Método que revierte los cambios realizados a los números prediales modificados de los
     * {@link PFichaMatrizPredio} que provienen de migración
     *
     * @author david.cifuentes
     */
    public void borrarNumerosPrediales() {
        try {
            if (this.fichaMatrizPrediosSeleccionados != null) {

                if (this.tramite.isEnglobeVirtual()) {
                    if (this.tramite.isTipoInscripcionCondominio() ||
                         this.proyectarConservacionMB.getFichaMatrizSeleccionada().getPPredio().
                            isMixto()) {

                        for (PFichaMatrizPredio pFichaMPredioSelVal
                            : this.fichaMatrizPrediosSeleccionados) {

                            if (pFichaMPredioSelVal.getNumeroPredial() == null ||
                                 pFichaMPredioSelVal.getNumeroPredial().isEmpty()) {

                                this.addMensajeWarn(ECodigoErrorVista.MODIFICACION_PH_058
                                    .getMensajeUsuario(pFichaMPredioSelVal.
                                        getNumeroPredialOriginal()));
                                LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_058
                                    .getMensajeTecnico());
                                return;
                            }
                        }

                        for (PFichaMatrizPredio pFichaMPredioSel
                            : this.fichaMatrizPrediosSeleccionados) {

                            if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
                                 !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

                                for (PFichaMatrizPredio pfmp : this.fichaMatriz.
                                    getPFichaMatrizPredios()) {

                                    if (pfmp.getNumeroPredialOriginal() != null) {

                                        if (pFichaMPredioSel.getId().equals(pfmp.getId()) &&
                                             pfmp.getNumeroPredial() != null &&
                                             !pfmp.getNumeroPredial().isEmpty()) {

                                            if (this.proyectarConservacionMB.getpPrediosDesenglobe() !=
                                                null &&
                                                 !this.proyectarConservacionMB.
                                                    getpPrediosDesenglobe().isEmpty()) {

                                                for (PPredio pPredioTemp
                                                    : this.proyectarConservacionMB.
                                                        getpPrediosDesenglobe()) {

                                                    if (pPredioTemp.getNumeroPredial() != null &&
                                                        !pPredioTemp.getNumeroPredial().isEmpty() &&
                                                         pfmp.getNumeroPredial().equals(pPredioTemp.
                                                            getNumeroPredial())) {

                                                        pPredioTemp.setNumeroPredial(pfmp.
                                                            getNumeroPredialOriginal());
                                                        this.getConservacionService().
                                                            actualizarPPredio(pPredioTemp);
                                                    }

                                                }
                                            }

                                            pfmp.setNumeroPredial(pfmp.getNumeroPredialOriginal());
                                            this.getConservacionService().
                                                actualizarListaPFichaMatrizPredio(pfmp);

                                        }
                                    }
                                }
                            }

                        }
                        this.cerrar();
                    }
                } else if (this.validarRetirarNumeroPredial(false)) {
                    List<PPredio> prediosModificados = new ArrayList<PPredio>();
                    for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {
                        PPredio pp = this.getConservacionService().getPPredioByNumeroPredial(pfmp.
                            getNumeroPredial());
                        if (pp != null && pfmp.getNumeroPredialOriginal() != null) {
                            pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                            pp.setNumeroPredial(pfmp.getNumeroPredialOriginal());
                            pp.setEdificio(pfmp.getNumeroPredialOriginal().substring(22, 24));
                            pp.setPiso(pfmp.getNumeroPredialOriginal().substring(24, 26));
                            pp.setUnidad(pfmp.getNumeroPredialOriginal().substring(26, 30));
                            prediosModificados.add(pp);
                        }
                        pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                        if (pfmp.getNumeroPredialOriginal() != null) {
                            pfmp.setNumeroPredial(pfmp.getNumeroPredialOriginal());
                        }
                    }
                    this.getConservacionService().actualizarPFichaMatrizPredios(
                        this.fichaMatrizPrediosSeleccionados);
                    // Guardar PPredios
                    if (prediosModificados != null && !prediosModificados.isEmpty()) {
                        this.getConservacionService().guardarPPredios(prediosModificados);
                    }

                    this.fichaMatrizPrediosSeleccionados = null;
                    this.addMensajeInfo("Se borraron los números prediales satisfactoriamente.");
                    this.proyectarConservacionMB.obligarGeografico();

                    // Deseleccionar los predios para controlar el comportamiento de la activación de botones.
                    this.selectedAllBool = false;
                    this.seleccionarTodosPrediosGenerados();

                }
            }
            this.cargarMatrizNumerosPrediales();
        } catch (Exception e) {
            this.addMensajeError(
                "Error al cargar los predios cancelados del trámite, por favor intente nuevamente.");
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Método que realiza el cargue de los predios cancelados que se encontraban en firme en la
     * {@link FichaMatriz}, usado en PH por etapas.
     *
     * @author david.cifuentes
     */
    public void cargarPrediosDesactivados() {
        try {
            if (this.tramite.isQuintaMasivo() && this.tramite.isQuintaOmitido()) {
                //Se cargan los predios en estado 'CANCELADO' del tramite 
                this.pFichaMatrizPrediosCancelados = this.getConservacionService().
                    buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(
                        EPredioEstado.CANCELADO.getCodigo(), this.fichaMatriz.getId());

                // Si no se encontraron predios cancelados se buscan a partir de los históricos
                if (this.pFichaMatrizPrediosCancelados == null ||
                    this.pFichaMatrizPrediosCancelados.isEmpty()) {

                    // Consulta a partir de históricos
                    List<HFichaMatrizPredio> historicoPrediosCancelados = this.
                        getConservacionService().buscarHistoricoFichaMatrizPredioPorIdFichaYEstado(
                            EPredioEstado.CANCELADO.getCodigo(), this.fichaMatriz.getId());

                    if (historicoPrediosCancelados != null && !historicoPrediosCancelados.isEmpty()) {
                        this.crearProyeccionAPartirDeHistorico(historicoPrediosCancelados);
                    } else {
                        this.addMensajeWarn(
                            "No se encontraron predios cancelados para la ficha matriz.");
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    }
                }
            } else {
                // Cargue de los predios cancelados que se encontraban en firme
                this.pFichaMatrizPrediosCancelados = this.getConservacionService().
                    buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(
                        EProyeccionCancelaInscribe.CANCELA.getCodigo(), this.fichaMatriz.getId());
            }
        } catch (Exception e) {
            this.addMensajeError(
                "Error al cargar los predios cancelados del trámite, por favor intente nuevamente.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Método para seleccionar todos los predios de la pantalla de activar predios.
     *
     * @author david.cifuentes
     */
    public void seleccionarTodosPrediosInactivas() {
        if (this.pFichaMatrizPrediosCancelados != null) {
            for (PFichaMatrizPredio pfmp : this.pFichaMatrizPrediosCancelados) {
                pfmp.setSelected(this.selectedAllActivarPrediosBool);
            }

            if (this.selectedAllActivarPrediosBool) {
                this.prediosInactivosSeleccionados =
                    new PFichaMatrizPredio[this.pFichaMatrizPrediosCancelados.size()];
                for (int i = 0; i < this.pFichaMatrizPrediosCancelados.size(); i++) {
                    this.prediosInactivosSeleccionados[i] = this.pFichaMatrizPrediosCancelados.
                        get(i);
                }
            } else {
                this.prediosInactivosSeleccionados = null;
            }
        }
    }

    /**
     * Método para actualizar la selección de los predios en la pantalla de activar predios cuando
     * se selecciona sólo uno de ellos.
     *
     * @author david.cifuentes
     */
    public void updateSelectedPrediosInactivos() {
        List<PFichaMatrizPredio> auxListaPredios = new ArrayList<PFichaMatrizPredio>();
        for (PFichaMatrizPredio pfmp : this.pFichaMatrizPrediosCancelados) {
            if (pfmp.isSelected()) {
                auxListaPredios.add(pfmp);
            }
        }
        this.prediosInactivosSeleccionados = new PFichaMatrizPredio[auxListaPredios
            .size()];
        for (int i = 0; i < auxListaPredios.size(); i++) {
            this.prediosInactivosSeleccionados[i] = auxListaPredios.get(i);
        }
    }

    /**
     * Método que realiza la acitivación de {@link PFichaMatrizPredio} cancelados y que se
     * encontraban en firme.
     *
     * @author david.cifuentes
     */
    public void activarPredios() {
        try {
            if (this.prediosInactivosSeleccionados != null) {
                List<PPredio> pPrediosActivar = new ArrayList<PPredio>();
                List<String> torresActivar = new ArrayList<String>();
                for (PFichaMatrizPredio pfmp : this.prediosInactivosSeleccionados) {
                    if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.
                        getCancelaInscribe())) {
                        pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    } else {
                        pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                    }

                    pfmp.setEstado(EPredioEstado.REACTIVADO.getCodigo());
                    String numeroPredial = pfmp.getNumeroPredial();
                    PPredio pp = this.getConservacionService().getPPredioByNumeroPredial(
                        numeroPredial);

                    if (pp != null) {
                        pp.setEstado(EPredioEstado.ACTIVO.getCodigo());
                        pp.setFechaInscripcionCatastral(this.tramite.getFechaInscripcionCatastral());
                        pp.setFechaLog(new Date());
                        pp.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                        if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pp.
                            getCancelaInscribe())) {
                            pp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                        }
                        pPrediosActivar.add(pp);
                        if (!torresActivar.contains(pp.getNumeroEdificioTorre())) {
                            torresActivar.add(pp.getNumeroEdificioTorre());
                        }
                    }

                    if (pp == null && this.tramite.isQuintaMasivo()) {
                        pp = new PPredio();
                        pp.setEstado(EPredioEstado.ACTIVO.getCodigo());
                        pp.setFechaInscripcionCatastral(this.tramite.getFechaInscripcionCatastral());
                        pp.setFechaLog(new Date());
                        pp.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                        pp.setNumeroPredial(numeroPredial);
                        pp.setDepartamento(this.tramite.getPredio().getDepartamento());
                        pp.setMunicipio(this.tramite.getPredio().getMunicipio());
                        pp.setZonaUnidadOrganica(this.tramite.getPredio().getZonaUnidadOrganica());
                        pp.setBarrioCodigo(this.tramite.getPredio().getBarrioCodigo());
                        pp.setSectorCodigo(this.tramite.getPredio().getSectorCodigo());
                        pp.setManzanaCodigo(this.tramite.getPredio().getManzanaCodigo());
                        pp.setCorregimientoCodigo(this.tramite.getPredio().getCorregimientoCodigo());
                        pp.setTramite(this.tramite);
                        pp.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                        pp.setFechaLog(new Date(System.currentTimeMillis()));
                        pp.setPredio(numeroPredial.substring(17, 21));
                        pp.setCondicionPropiedad(numeroPredial.substring(21, 22));
                        pp.setEdificio(numeroPredial.substring(22, 24));
                        pp.setPiso(numeroPredial.substring(24, 26));
                        pp.setUnidad(numeroPredial.substring(26, 30));
                        pp.setTipo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                        pp.setDestino(this.tramite.getPredio().getDestino());
                        pp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                        pp.setNombre(this.proyectarConservacionMB.getPredioSeleccionado().
                            getNombre());
                        pp.setTipoAvaluo(this.tramite.getPredio().getTipoAvaluo());
                        pp.setTipoCatastro(this.tramite.getPredio().getTipoCatastro());
                        pp.setAreaTerreno(0D);
                        pp.setAreaConstruccion(0D);
                        pp.setFechaInscripcionCatastral(this.tramite.getPredio().
                            getFechaInscripcionCatastral());
                        pp.setOriginalTramite(ESiNo.NO.getCodigo());

                        asociarDatosBasePredioAPPredio(pp);

                        this.getConservacionService().actualizarPPredio(pp);
                    }
                }

                if (!pPrediosActivar.isEmpty()) {
                    if (!this.tramite.isQuintaMasivo()) {
                        this.activarTorres(torresActivar);
                    }
                    this.getConservacionService().guardarPPredios(pPrediosActivar);
                }

                this.getConservacionService().actualizarPFichaMatrizPredios(
                    this.prediosInactivosSeleccionados);

                this.pFichaMatrizPrediosCancelados = null;
                this.selectedAllActivarPrediosBool = false;
                this.selectedAllBool = false;

                // Consultar la ficha matriz actualizada
                this.fichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
                    this.fichaMatriz.getPPredio().getId());
                this.verificarTorresEnFirme();
                this.proyectarConservacionMB
                    .setFichaMatrizSeleccionada(this.fichaMatriz);

                this.addMensajeInfo("Se activaron los predios satisfactoriamente");
                this.proyectarConservacionMB.obligarGeografico();
            }
        } catch (Exception e) {
            this.addMensajeError(
                "Error al activar los predios seleccionados, por favor intente nuevamente.");
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Método que realiza la activación de torres, es decir le cambia el estado a modificar.
     *
     * @param torresActivar
     */
    public void activarTorres(List<String> torresActivar) {
        if (!torresActivar.isEmpty() && this.fichaMatriz.getPFichaMatrizTorres() != null) {
            for (String t : torresActivar) {
                Long torre = new Long(t);
                for (PFichaMatrizTorre pfmt : this.fichaMatriz.getPFichaMatrizTorres()) {
                    if (pfmt.getTorre().longValue() == torre.longValue() ||
                         (torre.longValue() == 0 && ESiNo.SI.getCodigo().equals(pfmt.
                        getOriginalTramite()))) {
                        pfmt.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                        this.getConservacionService().guardarActualizarPFichaMatrizTorre(pfmt);
                    }
                }
            }
            this.verificarTorresEnFirme();
        }
    }

    /**
     * Método que verifica la cantidad de predios cancelados de una torre específica
     *
     * @author david.cifuentes
     * @param torre
     * @return
     */
    public int verificarPrediosCanceladosPorTorre(String torre) {
        int prediosCancelados = 0;
        this.pFichaMatrizPrediosCancelados = this.getConservacionService()
            .buscarPFichaMatrizPredioPorIdFichaYValorCancelaInscribe(
                EProyeccionCancelaInscribe.CANCELA.getCodigo(),
                this.fichaMatriz.getId());

        if (this.pFichaMatrizPrediosCancelados != null) {
            Integer valorTorre = new Integer(torre);
            for (PFichaMatrizPredio pfmp : this.pFichaMatrizPrediosCancelados) {
                Integer valorTorreIterator = new Integer(pfmp.getNumeroPredial().substring(22, 24));
                if (valorTorre.intValue() == valorTorreIterator.intValue()) {
                    prediosCancelados++;
                }
            }
        }
        return prediosCancelados;
    }

    /**
     * Método que verifica la cantidad de predios cancelados que están en el piso de una torre
     * específica
     *
     * @author david.cifuentes
     * @param torre
     * @return
     */
    public int verificarPrediosPisoAsignadosPorTorre(String torre) {
        int prediosAsignados = 0;
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizPredios() != null) {
            Integer valorTorre = new Integer(torre);
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().
                    equals(pfmp.getCancelaInscribe()) && !pfmp.isSotano()) {
                    Integer valorTorreIterator = new Integer(pfmp.getNumeroPredial().substring(22,
                        24));
                    if (valorTorre.intValue() == valorTorreIterator.intValue()) {
                        prediosAsignados++;
                    }
                }
            }
        }
        return prediosAsignados;
    }

    /**
     * Método que verifica la cantidad de predios cancelados que son sótanos de una torre específica
     *
     * @author david.cifuentes
     * @param torre
     * @return
     */
    public int verificarPrediosSotanoAsignadosPorTorre(String torre) {
        int prediosAsignados = 0;
        if (this.fichaMatriz != null && this.fichaMatriz.getPFichaMatrizPredios() != null) {
            Integer valorTorre = new Integer(torre);
            for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().
                    equals(pfmp.getCancelaInscribe()) && pfmp.isSotano()) {
                    Integer valorTorreIterator = new Integer(pfmp.getNumeroPredial().substring(22,
                        24));
                    if (valorTorre.intValue() == valorTorreIterator.intValue()) {
                        prediosAsignados++;
                    }
                }
            }
        }
        return prediosAsignados;
    }

    /**
     * Método usado para validar si los predios seleccionados son migrados o no, para validar si una
     * torre se debe o no mostrar.
     *
     * @author david.cifuentes
     *
     * @return
     */
    private boolean validarCargueTorresParaPrediosMigrados() {

        if (this.fichaMatrizPrediosSeleccionados != null) {
            int migrados = 0;
            for (PFichaMatrizPredio pfmp : this.fichaMatrizPrediosSeleccionados) {
                if (pfmp.getNumeroPredialOriginal() != null) {
                    String condicionPropiedad = pfmp.getNumeroPredialOriginal().substring(21, 22);
                    String torreMigrada = pfmp.getNumeroPredialOriginal().substring(22, 24);
                    String pisoMigrado = pfmp.getNumeroPredialOriginal().substring(24, 26);
                    if ("9".equals(condicionPropiedad) && ("00".equals(pisoMigrado) || "00".equals(
                        torreMigrada))) {
                        migrados++;
                    }
                }
            }
            if (migrados == this.fichaMatrizPrediosSeleccionados.length) {
                return true;
            }
        }
        return false;
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones originales del predio que hayan sido seleccionadas
     * (convencionales y no convencionales) a las nuevas.
     *
     * @author david.cifuentes
     */
    public void asociarConstruccionesPredioOriginal() {
        try {
            int cantidadConvencional = 0, cantidadNoConvencional = 0, cantidadUnidades;
            // LLamado al método que realiza el set de las unidades de
            // construcción seleccionadas del predio original
            this.cargarUnidadesSeleccionadasPredioOriginal();

            if (this.unidadConstruccionConvencionalPredioOriginalSeleccionadas != null &&
                 this.unidadConstruccionConvencionalPredioOriginalSeleccionadas.length > 0) {
                cantidadConvencional =
                    this.unidadConstruccionConvencionalPredioOriginalSeleccionadas.length;
            }
            if (this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas != null &&
                 this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas.length > 0) {
                cantidadNoConvencional =
                    this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas.length;
            }
            cantidadUnidades = cantidadNoConvencional + cantidadConvencional;

            if (cantidadUnidades < 1) {
                this.addMensajeWarn("Por favor seleccione las unidades a asociar.");
                return;
            } else {
                if (cantidadNoConvencional > 0) {
                    asociarConstruccionesNoConvencionalesPredioOriginal();
                }
                if (cantidadConvencional > 0) {
                    asociarConstruccionesConvencionalesPredioOriginal();
                }
            }

            if(this.unidadConstruccionConvencionalPredioOriginal != null){
                for(UnidadConstruccion uc : this.unidadConstruccionConvencionalPredioOriginal){
                    uc.setSelected(false);
                }
            }
            if(this.unidadConstruccionNoConvencionalPredioOriginal != null){
                for(UnidadConstruccion uc : this.unidadConstruccionNoConvencionalPredioOriginal){
                    uc.setSelected(false);
                }
            }

            this.setupUnidadesDeConstruccion();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que carga las unidades seleccionadas de la tabla de unidades de construcciónn del
     * predio origen.
     *
     * @author david.cifuentes
     */
    public void cargarUnidadesSeleccionadasPredioOriginal() {

        List<UnidadConstruccion> aux;
        this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = null;
        this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = null;

        if (this.unidadConstruccionConvencionalPredioOriginal != null &&
             !this.unidadConstruccionConvencionalPredioOriginal.isEmpty()) {

            aux = new ArrayList<UnidadConstruccion>();
            for (UnidadConstruccion uccv : this.unidadConstruccionConvencionalPredioOriginal) {
                if (uccv.isSelected()) {
                    aux.add(uccv);
                }
            }
            if (aux.size() > 0) {
                this.unidadConstruccionConvencionalPredioOriginalSeleccionadas =
                    new UnidadConstruccion[aux.size()];
                for (int i = 0; i < aux.size(); i++) {
                    this.unidadConstruccionConvencionalPredioOriginalSeleccionadas[i] = aux.get(i);
                }
            }
        }
        if (this.unidadConstruccionNoConvencionalPredioOriginal != null &&
             !this.unidadConstruccionNoConvencionalPredioOriginal.isEmpty()) {

            aux = new ArrayList<UnidadConstruccion>();
            for (UnidadConstruccion ucncv : this.unidadConstruccionNoConvencionalPredioOriginal) {
                if (ucncv.isSelected()) {
                    aux.add(ucncv);
                }
            }
            if (aux.size() > 0) {
                this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas =
                    new UnidadConstruccion[aux.size()];
                for (int i = 0; i < aux.size(); i++) {
                    this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas[i] = aux.get(i);
                }
            }
        }
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones convecionales del predio original a las nuevas
     * construcciones.
     *
     * @author david.cifuentes
     */
    public void asociarConstruccionesConvencionalesPredioOriginal() {
        if (unidadConstruccionConvencionalNuevas == null) {
            unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        }
        this.asociarConstruccionesPredioOriginal(
            this.unidadConstruccionConvencionalPredioOriginalSeleccionadas,
            this.unidadConstruccionConvencionalNuevas);
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones no convencionales del predio original a las nuevas
     * construcciones.
     *
     * @author david.cifuentes
     */
    public void asociarConstruccionesNoConvencionalesPredioOriginal() {
        if (unidadConstruccionNoConvencionalNuevas == null) {
            unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        }
        this.asociarConstruccionesPredioOriginal(
            this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas,
            this.unidadConstruccionNoConvencionalNuevas);
    }

    // ------------------------------------------------------------- //
    /**
     * Método que asocia las construcciones originales del predio que han sido enviadas como
     * parámetro a las nuevas construcciones.
     *
     * @author david.cifuentes
     */
    public void asociarConstruccionesPredioOriginal(
        UnidadConstruccion[] unidadConstruccionSeleccionadas,
        List<PUnidadConstruccion> pUnidadConstruccionNuevas) {

        if (unidadConstruccionSeleccionadas.length <= 0) {
            this.addMensajeWarn("Debe seleccionar al menos una unidad de construccion a asociar");
        } else {
            try {

                GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

                List<PUnidadConstruccion> unidadesAAsociar = new ArrayList<PUnidadConstruccion>();

                // Consulta del predio base con sus construcciones.
                PPredio pPredioBase = this.getConservacionService()
                    .obtenerPPredioCompletoById(
                        this.tramite.getPredio().getId());

                // Consulta del predio asociado al ppredio
                Predio predioBase = this.getConservacionService()
                    .obtenerPredioPorId(this.tramite.getPredio().getId());

                if (this.predioDesenglobe != null && pPredioBase != null) {

                    // Únicamente se puede asociar la misma unidad origen al predio
                    if (unidadConstruccionSeleccionadas != null &&
                         !this.proyectarConservacionMB.getPredioSeleccionado().
                            getPUnidadConstruccions().isEmpty()) {

                        List<String> unidadesAsociadasPredio = new ArrayList<String>();
                        for (PUnidadConstruccion puc : this.proyectarConservacionMB.
                            getPredioSeleccionado().getPUnidadConstruccions()) {
                            if (puc.getProvieneUnidad() != null) {
                                if (!unidadesAsociadasPredio.contains(puc.getProvieneUnidad())) {
                                    unidadesAsociadasPredio.add(puc.getProvieneUnidad());
                                }
                            }
                        }
                        for (UnidadConstruccion uc : unidadConstruccionSeleccionadas) {
                            if (unidadesAsociadasPredio.contains(uc.getUnidad())) {
                                this.addMensajeError(
                                    "Sólo se puede asociar una vez la unidad original " + uc.
                                        getUnidad() + " al predio actual.");
                                return;
                            }
                        }
                    }

                    // Obtener una copia del arreglo de construcciones
                    // seleccionadas, para posteriormente modificarlas.
                    if (unidadConstruccionSeleccionadas != null &&
                         unidadConstruccionSeleccionadas.length > 0) {
                        for (UnidadConstruccion uc : unidadConstruccionSeleccionadas) {
                            unidadesAAsociar.add(generalMB.
                                copiarUnidadConstruccionAPUnidadConstruccion(uc));
                        }
                    }

                    // Actualizar las unidades convencionales antes de
                    // asociarselas al predio seleccionado.
                    if (unidadesAAsociar.size() > 0) {

                        // Actualización de unidades convencionales
                        for (PUnidadConstruccion ucAsociar : unidadesAAsociar) {

                            List<PUnidadConstruccion> auxUnidadesAsociar =
                                new ArrayList<PUnidadConstruccion>();
                            ucAsociar.setPPredio(this.proyectarConservacionMB.
                                getPredioSeleccionado());
                            ucAsociar
                                .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                    .getCodigo());
                            ucAsociar.setUsuarioLog(this.usuarioFichaMatriz
                                .getLogin());
                            ucAsociar.setFechaLog(new Date());
                            ucAsociar.setId(null);
                            ucAsociar.setProvienePredio(predioBase);
                            ucAsociar.setProvieneUnidad(ucAsociar.getUnidad());
                            ucAsociar.setUnidad(this.proyectarConservacionMB.
                                calcularUnidadConstruccionUnidad());

                            // El área se asocia con un valor de cero para
                            // ser modificada según el área que se quiera
                            // asignar como común
                            ucAsociar.setAreaConstruida(new Double(0));
                            ucAsociar.setAvaluo(new Double(0));
                            ucAsociar.setValorM2Construccion(new Double(0));

                            auxUnidadesAsociar.add(ucAsociar);
                            auxUnidadesAsociar = this.getConservacionService()
                                .guardarListaPUnidadConstruccion(
                                    auxUnidadesAsociar);

                            if (auxUnidadesAsociar != null) {
                                ucAsociar.setId(auxUnidadesAsociar.get(0).getId());
                            }
                        }

                        if (unidadesAAsociar != null) {
                            // Actualización de los componentes de las
                            // unidades actualizadas para que tengan la
                            // misma unidad
                            // de construccion padre.
                            for (PUnidadConstruccion puc : unidadesAAsociar) {
                                if (puc.getPUnidadConstruccionComps() != null &&
                                     !puc.getPUnidadConstruccionComps()
                                        .isEmpty()) {
                                    for (PUnidadConstruccionComp pucc : puc
                                        .getPUnidadConstruccionComps()) {
                                        pucc.setPUnidadConstruccion(puc);
                                    }
                                }
                            }
                            // Actualizar la unidad de construcción con los
                            // componentes actualizados.
                            unidadesAAsociar = this
                                .getConservacionService()
                                .guardarListaPUnidadConstruccion(
                                    unidadesAAsociar);
                        }
                    }

                    // Actualización de totales de áreas en la FM
                    if(this.fichaMatriz != null) {
                        this.actualizarTotalesAreasEnFichaMatriz();
                    }
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeWarn("Error actualizando las áreas en la ficha matriz.");
            }
        }
    }

    /**
     * Método que realiza la actualización de los totales de área de construcción y de terreno común
     * de la ficha matriz. Debe ser llamado en cada adición, modificación o eliminación de
     * construcciones en los predios del desenglobe o en la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void actualizarTotalesAreasEnFichaMatriz() {

        try {

            List<PPredio> prediosTramite = null;
            //No se realiza calculo si aun no se ha creado o consultado la ficha
            if (this.fichaMatriz == null) {
                return;
            }

            if (this.tramite.isTerceraMasiva()) {
                prediosTramite = this.proyectarConservacionMB.
                    getpPrediosTerceraRectificacionMasiva();
            } else {
                prediosTramite = this.proyectarConservacionMB.getpPrediosDesenglobe();
            }

            if (prediosTramite != null && !prediosTramite.isEmpty()) {

                Double areaTotalConstruidaComun = 0D;
                Double areaTotalConstruidaPrivada = 0D;
                Double areaTotalTerrenoComun = 0D;
                Double areaTotalTerrenoPrivada = 0D;

                for (PPredio pp : prediosTramite) {

                    if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pp.
                        getCancelaInscribe()) &&
                         !ETramiteEstado.CANCELADO.getCodigo().equals(pp.getEstado())) {

                        //Predio ficha matriz
                        if (pp.getId().longValue() == this.fichaMatriz.getPPredio().getId().
                            longValue()) {

                            if (this.tramite.isEnglobeVirtual() &&
                                this.getProyectarConservacionMB().getPredioSeleccionado() != null) {
                                if (pp.getId().equals(this.getProyectarConservacionMB().
                                    getPredioSeleccionado().getId())) {
                                    pp = this.getProyectarConservacionMB().getPredioSeleccionado();
                                }
                            }

                            areaTotalTerrenoComun += pp.getAreaTerreno();
                            if (pp.getPUnidadConstruccions() != null &&
                                 !pp.getPUnidadConstruccions().isEmpty()) {
                                for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                                    if (puc.getAnioCancelacion() == null &&
                                        !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.
                                            getCancelaInscribe())) {
                                        areaTotalConstruidaComun += puc.getAreaConstruida();
                                    }
                                }
                            }
                        } else {
                            if (!pp.isUnidadEnPH() && !EPredioEstado.CANCELADO.getCodigo().equals(
                                pp.getEstado())) {
                                areaTotalTerrenoPrivada += pp.getAreaTerreno();
                            }
                            //Predios desenglobe
                            if (pp.getPUnidadConstruccions() != null &&
                                 !pp.getPUnidadConstruccions().isEmpty()) {
                                for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                                    if (puc.getAnioCancelacion() == null &&
                                        !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.
                                            getCancelaInscribe())) {
                                        areaTotalConstruidaPrivada += puc.getAreaConstruida();
                                    }
                                }
                            }
                        }
                    }
                }

                if (EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.fichaMatriz.getPPredio().
                    getCondicionPropiedad())) {
                    areaTotalTerrenoPrivada = 0D;
                }

                // Ãreas
                this.fichaMatriz.setAreaTotalConstruidaComun(areaTotalConstruidaComun);
                this.fichaMatriz.setAreaTotalConstruidaPrivada(areaTotalConstruidaPrivada);
                this.fichaMatriz.setAreaTotalTerrenoComun(areaTotalTerrenoComun);
                this.fichaMatriz.setAreaTotalTerrenoPrivada(areaTotalTerrenoPrivada);
                if (!EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(this.fichaMatriz.
                    getCancelaInscribe()) &&
                     !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(this.fichaMatriz.
                        getCancelaInscribe())) {
                    this.fichaMatriz.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.
                        getCodigo());
                }

                // Método que hace la conversión de áreas de terreno en la ficha matriz 
                // para las variables que se visualizan en la interfaz (Ha / M2)
                this.convertirAreasAHaM2();

                this.fichaMatriz.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                this.fichaMatriz.setFechaLog(new Date());
                this.fichaMatriz.setEstado(EFichaMatrizEstado.ACTIVO.getCodigo());

                if (this.tramite.isEnglobeVirtual()) {
                    // Guardar áreas de la ficha matriz.
                    this.getConservacionService().guardarActualizarPFichaMatriz(this.fichaMatriz);
                    if (this.getProyectarConservacionMB().getPredioSeleccionado() != null) {
                        this.fichaMatriz = this.getConservacionService()
                            .buscarPFichaMatrizPorPredioId(this.getProyectarConservacionMB().
                                getPredioSeleccionado().getId());
                        this.proyectarConservacionMB.setFichaMatrizSeleccionada(this.fichaMatriz);
                    }
                } else {

                    // Guardar áreas de la ficha matriz.
                    this.fichaMatriz = this.getConservacionService().guardarActualizarPFichaMatriz(
                        this.fichaMatriz);

                    if (this.fichaMatriz != null) {
                        this.proyectarConservacionMB
                            .setFichaMatrizSeleccionada(this.fichaMatriz);
                    }

                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeWarn("Error actualizando las áreas en la ficha matriz.");
        }
    }

    /**
     * Método que crea la proyección de un predio a partir de sus historicos.
     *
     * @author david.cifuentes
     * @param historicoPrediosCancelados
     */
    private void crearProyeccionAPartirDeHistorico(
        List<HFichaMatrizPredio> historicoPrediosCancelados) {

        List<PFichaMatrizPredio> pFichaMatrizPrediosCancelados = new ArrayList<PFichaMatrizPredio>();
        List<PPredio> pPrediosCancelados = new ArrayList<PPredio>();

        for (HFichaMatrizPredio hfmp : historicoPrediosCancelados) {

            PFichaMatrizPredio pfmp = GeneralMB.copiarHFichaMatrizPredioAPFichaMatrizPredio(hfmp);
            pfmp.setEstado(EPredioEstado.CANCELADO.getCodigo());
            pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
            pfmp.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
            pfmp.setPFichaMatriz(this.fichaMatriz);
            pfmp.setOriginalTramite(ESiNo.NO.getCodigo());

            HPredio hPredio = this.getConservacionService()
                .buscarHPredioPorNumeroPredialYEstado(pfmp.getNumeroPredial(),
                    EPredioEstado.CANCELADO.getCodigo());

            if (hPredio != null) {
                PPredio pPredio = GeneralMB.copiarHPredioAPPredio(hPredio);
                pPredio.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                pPredio.setEstado(EPredioEstado.CANCELADO.getCodigo());
                pPredio.setOriginalTramite(ESiNo.SI.getCodigo());
                pPrediosCancelados.add(pPredio);
            }
            pFichaMatrizPrediosCancelados.add(pfmp);
        }

        if (pFichaMatrizPrediosCancelados != null) {
            // Guardar PFichaMatrizPredios
            this.pFichaMatrizPrediosCancelados = this.getConservacionService()
                .guardarPrediosFichaMatriz(pFichaMatrizPrediosCancelados);
            // Guardar PPredios
            pPrediosCancelados = this.getConservacionService().guardarPPredios(
                pPrediosCancelados);

            // Se asocian los datos históricos existentes del predio
            List<PPredio> listActualizarPpredio = new ArrayList<PPredio>();
            for (PPredio pp : pPrediosCancelados) {
                pp = asociarDatosBaseAlPPredio(pp);
                listActualizarPpredio.add(pp);
            }
            pPrediosCancelados = this.getConservacionService().guardarPPredios(
                listActualizarPpredio);
        }
    }

    /**
     * Método que verifica que todos los {@link PFichaMatrizPredio} proyectados se encuentren
     * asociados a un {@link PPredio} activo
     *
     * @author david.cifuentes
     */
    private void cargarEstadoPFichaMatrizPredios() {

        // Consultar todos los predios que se encuentran en firme
        List<Predio> prediosDesenglobe = this.getConservacionService()
            .obtenerPrediosPH(this.fichaMatriz.getPPredio().getNumeroPredial().substring(0, 21));
        List<String> prediosCancelados = new ArrayList<String>();

        // Hacer el set de los predios cancelados
        if (this.fichaMatriz != null &&
             this.fichaMatriz.getPFichaMatrizPredios() != null &&
             prediosDesenglobe != null) {

            for (Predio p : prediosDesenglobe) {
                if (EPredioEstado.CANCELADO.getCodigo().equals(p.getEstado())) {
                    prediosCancelados.add(p.getNumeroPredial());
                }
            }

            if (!prediosCancelados.isEmpty()) {

                boolean actualizarFichaBool = false;
                for (PFichaMatrizPredio pfmp : this.fichaMatriz.getPFichaMatrizPredios()) {
                    if (prediosCancelados.contains(pfmp.getNumeroPredial()) &&
                         pfmp.getEstado() == null) {
                        pfmp.setEstado(EPredioEstado.CANCELADO.getCodigo());
                        actualizarFichaBool = true;
                    }
                }
                if (actualizarFichaBool) {
                    this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);
                }
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que asocia al nuevo PPredio los datos del Predio base que deben persistir, tales como
     * referencias cartograficas, direcciones y propietarios.
     *
     * * @david.cifuentes
     */
    public PPredio asociarDatosBasePredioAPPredio(PPredio nuevoPPredio) {

        Predio pTemp = this.getConservacionService().getPredioByNumeroPredial(nuevoPPredio.
            getNumeroPredial());

        if (pTemp != null) {

            Predio predioBase = this.getConservacionService().buscarPredioCompletoPorPredioId(pTemp.
                getId());

            if (predioBase != null) {

                // DIRECCIONES
                if (predioBase.getPredioDireccions() != null &&
                     !predioBase.getPredioDireccions().isEmpty()) {

                    nuevoPPredio
                        .setPPredioDireccions(new ArrayList<PPredioDireccion>());
                    PPredioDireccion nuevoPPredioDireccion;
                    for (PredioDireccion pd : predioBase.getPredioDireccions()) {
                        // Set de la dirección
                        nuevoPPredioDireccion = new PPredioDireccion();
                        nuevoPPredioDireccion.setDireccion(pd.getDireccion());
                        nuevoPPredioDireccion.setCodigoPostal(pd.getCodigoPostal());
                        nuevoPPredioDireccion.setPrincipal(pd.getPrincipal());
                        nuevoPPredioDireccion.setReferencia(pd.getReferencia());
                        nuevoPPredioDireccion.setId(null);
                        nuevoPPredioDireccion
                            .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo());
                        nuevoPPredioDireccion.setFechaLog(new Date(System
                            .currentTimeMillis()));
                        nuevoPPredioDireccion.setUsuarioLog(this.usuarioFichaMatriz
                            .getLogin());
                        nuevoPPredioDireccion.setPPredio(nuevoPPredio);
                        // Se asocia la dirección
                        nuevoPPredio.getPPredioDireccions().add(
                            nuevoPPredioDireccion);
                    }
                }
                // REFERENCIAS CARTOGRÁFICAS
                if (predioBase.getReferenciaCartograficas() != null &&
                     !predioBase.getReferenciaCartograficas()
                        .isEmpty()) {

                    nuevoPPredio
                        .setPReferenciaCartograficas(new ArrayList<PReferenciaCartografica>());
                    PReferenciaCartografica nuevaPReferenciaCartografica;

                    for (ReferenciaCartografica rc : predioBase
                        .getReferenciaCartograficas()) {
                        // Set de la referencia cartográfica
                        nuevaPReferenciaCartografica = new PReferenciaCartografica();
                        nuevaPReferenciaCartografica.setFoto(rc.getFoto());
                        nuevaPReferenciaCartografica.setPlancha(rc.getPlancha());
                        nuevaPReferenciaCartografica.setVuelo(rc.getVuelo());
                        nuevaPReferenciaCartografica.setPPredio(nuevoPPredio);
                        nuevaPReferenciaCartografica.setId(null);
                        nuevaPReferenciaCartografica
                            .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo());
                        nuevaPReferenciaCartografica.setFechaLog(new Date(System
                            .currentTimeMillis()));
                        nuevaPReferenciaCartografica
                            .setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                        nuevaPReferenciaCartografica.setPPredio(nuevoPPredio);
                        // Se asocia la dirección
                        nuevoPPredio.getPReferenciaCartograficas().add(
                            nuevaPReferenciaCartografica);
                    }
                }

                // PROPIETARIOS Y POSEEDORES
                if (predioBase.getPersonaPredios() != null &&
                     !predioBase.getPersonaPredios().isEmpty()) {

                    nuevoPPredio
                        .setPPersonaPredios(new ArrayList<PPersonaPredio>());
                    PPersonaPredio nuevaPPersonaPredio;

                    for (PersonaPredio pp : predioBase
                        .getPersonaPredios()) {

                        if (pp.getPersona() != null &&
                             pp.getPersona().getNumeroIdentificacion() != null &&
                             pp.getPersona().getNumeroIdentificacion().length() > 1) {

                            List<PPersona> pPers = this.getConservacionService().
                                buscarPPersonasPorTipoNumeroIdentificacion(pp.getPersona().
                                    getTipoIdentificacion(), pp.getPersona().
                                        getNumeroIdentificacion());

                            if (pPers != null && !pPers.isEmpty()) {

                                // Set del propietario poseedor.
                                nuevaPPersonaPredio = new PPersonaPredio();
                                nuevaPPersonaPredio.setFechaInscripcionCatastral(pp.
                                    getFechaInscripcionCatastral());
                                nuevaPPersonaPredio.setParticipacion(pp.getParticipacion());

                                nuevaPPersonaPredio.setTipo(pp.getTipo());
                                nuevaPPersonaPredio
                                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                                        .getCodigo());
                                nuevaPPersonaPredio.setFechaLog(new Date(System
                                    .currentTimeMillis()));
                                nuevaPPersonaPredio.setUsuarioLog(this.usuarioFichaMatriz
                                    .getLogin());
                                nuevaPPersonaPredio.setPPredio(nuevoPPredio);

                                // Se asocia el propietario poseedor
                                PPersona nuevaPPersona = new PPersona();

                                nuevaPPersona.setTipoPersona(pPers.get(0).getTipoPersona());
                                nuevaPPersona.setTipoIdentificacion(pPers.get(0).
                                    getTipoIdentificacion());
                                nuevaPPersona.setNumeroIdentificacion(pPers.get(0).
                                    getNumeroIdentificacion());
                                nuevaPPersona.setDigitoVerificacion(pPers.get(0).
                                    getDigitoVerificacion());
                                nuevaPPersona.setPrimerNombre(pPers.get(0).getNombre());
                                nuevaPPersona.setSegundoNombre(pPers.get(0).getSegundoNombre());
                                nuevaPPersona.setPrimerApellido(pPers.get(0).getPrimerApellido());
                                nuevaPPersona.setSegundoApellido(pPers.get(0).getSegundoApellido());
                                nuevaPPersona.setRazonSocial(pPers.get(0).getRazonSocial());
                                nuevaPPersona.setSigla(pPers.get(0).getSigla());
                                nuevaPPersona.setDireccion(pPers.get(0).getDireccion());
                                nuevaPPersona.setDireccionPais(pPers.get(0).getDireccionPais());
                                nuevaPPersona.setDireccionDepartamento(pPers.get(0).
                                    getDireccionDepartamento());
                                nuevaPPersona.setDireccionMunicipio(pPers.get(0).
                                    getDireccionMunicipio());
                                nuevaPPersona.setTelefonoPrincipal(pPers.get(0).
                                    getTelefonoPrincipal());
                                nuevaPPersona.setTelefonoPrincipalExt(pPers.get(0).
                                    getTelefonoPrincipalExt());
                                nuevaPPersona.setTelefonoSecundario(pPers.get(0).
                                    getTelefonoSecundario());
                                nuevaPPersona.setTelefonoSecundarioExt(pPers.get(0).
                                    getTelefonoSecundarioExt());
                                nuevaPPersona.setTelefonoCelular(pPers.get(0).getTelefonoCelular());
                                nuevaPPersona.setFax(pPers.get(0).getFax());
                                nuevaPPersona.setFaxExt(pPers.get(0).getFaxExt());
                                nuevaPPersona.setCorreoElectronico(pPers.get(0).
                                    getCorreoElectronico());
                                nuevaPPersona.setCorreoElectronicoSecundario(pPers.get(0).
                                    getCorreoElectronicoSecundario());
                                nuevaPPersona.setEstadoCivil(pPers.get(0).getEstadoCivil());
                                nuevaPPersona.setCancelaInscribe(
                                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                                nuevaPPersona.setUsuarioLog(this.usuarioFichaMatriz.getLogin());
                                nuevaPPersona.setFechaLog(new Date());

                                nuevaPPersona = this.getConservacionService().actualizarPPersona(
                                    nuevaPPersona);

                                nuevaPPersonaPredio.setPPersona(nuevaPPersona);
                                nuevoPPredio.getPPersonaPredios().add(nuevaPPersonaPredio);
                            }
                        }
                    }
                }
            }
        }
        return nuevoPPredio;
    }

    /**
     * Metodo para inicializar datos asociados a tramites de moodificacion de PH
     *
     *
     * @author leidy.gonzalesz
     */
    public void inicializaMenuCondicionEV() {

        this.tiposCondicionEV = new ArrayList<SelectItem>();

        SelectItem item = new SelectItem(EPredioCondicionPropiedad.CP_9.getCodigo(),
            EPredioCondicionPropiedad.CP_9.getCodigo() + " - " + EPredioCondicionPropiedad.CP_9.
            getValor());
        this.tiposCondicionEV.add(item);
        item = new SelectItem(EPredioCondicionPropiedad.CP_8.getCodigo(),
            EPredioCondicionPropiedad.CP_8.getCodigo() + " - " + EPredioCondicionPropiedad.CP_8.
            getValor());
        this.tiposCondicionEV.add(item);

    }

    /**
     * Metodo para inicializar datos asociados a tramites de moodificacion de PH
     *
     *
     * @author leidy.gonzalesz
     */
    public void seleccionCondicionPredio() {

        if (this.condicionPredioSeleccionada != null && !this.condicionPredioSeleccionada.isEmpty()) {

            if (this.condicionPredioSeleccionada.
                contains(EPredioCondicionPropiedad.CP_8.getCodigo())) {
                this.banderaCondominio = true;
                this.banderaPH = false;
            } else if (this.condicionPredioSeleccionada.contains(EPredioCondicionPropiedad.CP_9.
                getCodigo())) {
                this.banderaPH = true;
                this.banderaCondominio = false;
            } else {
                this.banderaPH = false;
                this.banderaCondominio = false;
            }
        } else {
            this.banderaPH = false;
            this.banderaCondominio = false;
        }
    }

    /**
     * Metodo para crear numeros Prediales de Englobe Virtual
     *
     *
     * @author leidy.gonzalesz
     */
    public void crearNumerosPredialesEV() {

        if (this.mantieneFicha) {

            String condicionPropiedad = this.predioDesenglobe
                .getCondicionPropiedad();

            // Validación contra unidades restantes
            if (condicionPropiedad.
                equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {

                if (this.torreSeleccionada == null ||
                     this.torreSeleccionada.equals("0")) {

                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_013
                        .getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_013
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return;
                }

                if (this.pisoOSotanoSeleccionado == null ||
                     this.pisoOSotanoSeleccionado.equals("0")) {

                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_014
                        .getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_014
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return;
                }

            }

            if (this.unidad <= 0) {

                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_015
                    .getMensajeUsuario());
                LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_015
                    .getMensajeTecnico());
                this.addErrorCallback();
                return;
            }

            if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo()) &&
                 this.torreSeleccionada != null &&
                 !this.torreSeleccionada.equals("TODAS")) {

                int torreSeleccionada = new Integer(this.torreSeleccionada);

                if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo()) && this.unidad.intValue() >
                    this.sotanosRestantesPorTorrePH[torreSeleccionada]) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades de sótanos restantes " +
                         "para la torre " + torreSeleccionada + " que son: " +
                        this.sotanosRestantesPorTorrePH[torreSeleccionada] + ".");
                    return;
                } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO.getCodigo()) && this.unidad.
                    intValue() > this.unidadesRestantesPorTorrePH[torreSeleccionada]) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades privadas restantes " +
                         "para la torre " + torreSeleccionada + " que son: " +
                        this.unidadesRestantesPorTorrePH[torreSeleccionada] + ".");
                    return;
                }
            }

            // Creación de números prediales
            if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8
                .getCodigo())) {
                crearNumerosPredialesCondominio();
                this.tramite.setTipoInscripcion(ETramiteTipoInscripcion.CONDOMINIO
                    .getCodigo());
            } else if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9
                .getCodigo())) {
                crearNumerosPredialesPH();
                this.tramite.setTipoInscripcion(ETramiteTipoInscripcion.PH
                    .getCodigo());
            }

        } else {

            // Validación contra unidades restantes
            if (this.condicionPredioSeleccionada.
                equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {

                if (this.torreSeleccionada == null ||
                     this.torreSeleccionada.equals("0")) {

                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_013
                        .getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_013
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return;
                }

                if (this.pisoOSotanoSeleccionado == null ||
                     this.pisoOSotanoSeleccionado.equals("0")) {

                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_014
                        .getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_014
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return;
                }

            }

            if (this.unidad <= 0) {

                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_015
                    .getMensajeUsuario());
                LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_015
                    .getMensajeTecnico());
                this.addErrorCallback();
                return;
            }

            if (!this.condicionPredioSeleccionada.isEmpty() &&
                 this.condicionPredioSeleccionada.contains(EPredioCondicionPropiedad.CP_9.
                    getCodigo()) &&
                 this.torreSeleccionada != null &&
                 !this.torreSeleccionada.equals("TODAS")) {

                int torreSeleccionada = new Integer(this.torreSeleccionada);

                if (this.ejeVialHaciaAbajo.equals(ESiNo.SI.getCodigo()) && this.unidad.intValue() >
                    this.sotanosRestantesPorTorrePH[torreSeleccionada]) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades de sótanos restantes " +
                         "para la torre " + torreSeleccionada + " que son: " +
                        this.sotanosRestantesPorTorrePH[torreSeleccionada] + ".");
                    return;
                } else if (this.ejeVialHaciaAbajo.equals(ESiNo.NO.getCodigo()) && this.unidad.
                    intValue() > this.unidadesRestantesPorTorrePH[torreSeleccionada]) {
                    this.addMensajeError(
                        "Los números prediales que se quiere generar superan la cantidad de unidades privadas restantes " +
                         "para la torre " + torreSeleccionada + " que son: " +
                        this.unidadesRestantesPorTorrePH[torreSeleccionada] + ".");
                    return;
                }
            }

            // Creación de números prediales
            if (!this.condicionPredioSeleccionada.isEmpty() &&
                 this.condicionPredioSeleccionada.contains(EPredioCondicionPropiedad.CP_8.
                    getCodigo())) {

                crearNumerosPredialesCondominio();
                this.tramite.setTipoInscripcion(ETramiteTipoInscripcion.CONDOMINIO
                    .getCodigo());
            } else if (!this.condicionPredioSeleccionada.isEmpty() &&
                 this.condicionPredioSeleccionada.contains(EPredioCondicionPropiedad.CP_9.
                    getCodigo())) {

                crearNumerosPredialesPH();
                this.tramite.setTipoInscripcion(ETramiteTipoInscripcion.PH
                    .getCodigo());
            }
        }

    }

    /**
     * Método que consulta las Unidades de la ficha, y las almacena en un itemList.
     *
     * @author leidy.gonzalez
     */
    public void cargarUnidadesInicialesDelCondominio() {
        this.unidadInicialSelec = new String();
        this.unidadInicialSeleccionada = 0;
        this.unidadFinalSeleccionada = new String();

    }

    /**
     * Método que carga la Unidad final de la ficha de acuerdo a la unidad inicial seleccionada.
     *
     * @author leidy.gonzalez
     */
    public void cargarUnidadFinalesCond() {

        this.unidadInicialSelec = new String();
        this.unidadFinalSeleccionada = new String();
        int longSel = fichaMatrizPrediosSeleccionados.length;
        this.banderaAceptarNumPredial = false;

        if (this.unidadInicialSeleccionada >= 1) {

            this.unidadInicialSelec = String.valueOf(this.unidadInicialSeleccionada);

            int intValueFinal = 0;

            if (longSel > 1) {
                intValueFinal = this.unidadInicialSeleccionada +
                    (fichaMatrizPrediosSeleccionados.length - 1);
            } else if (longSel == 1) {
                intValueFinal = this.unidadInicialSeleccionada;
            }

            String convIntValueFinal = String.valueOf(intValueFinal);

            for (int i = 0; i < 4; i++) {
                if (convIntValueFinal.length() == 4) {

                    this.unidadFinalSeleccionada = "000" + convIntValueFinal;
                    break;
                } else if (this.unidadFinalSeleccionada.length() == 4) {
                    break;
                } else {

                    if (i == 0) {
                        this.unidadFinalSeleccionada = '0' + convIntValueFinal;
                    } else {
                        this.unidadFinalSeleccionada = '0' + this.unidadFinalSeleccionada;
                    }

                }
            }

            this.banderaAceptarNumPredial = true;

        } else {
            this.banderaAceptarNumPredial = false;
            this.addMensajeError("La unidad inicial debe tener un valor mayor a 0 ");
            this.cerrar();
            return;
        }
    }

    /**
     * Método que permite cerrar el dialogo de modificar numero predial limpiando los campos
     *
     * @author leidy.gonzalez
     */
    public void cerrar() {
        this.unidadInicialSelec = new String();
        this.unidadFinalSeleccionada = new String();
        this.unidadInicialSeleccionada = 0;
        this.fichaMatrizPrediosSeleccionados = new PFichaMatrizPredio[0];
        this.selectedAllBool = false;
        this.modificarPrediosMigracionBool = false;
        if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
             !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

            for (PFichaMatrizPredio pFichaMatrizPredio : this.fichaMatriz.getPFichaMatrizPredios()) {

                pFichaMatrizPredio.setSelected(this.selectedAllBool);
            }
        }
    }

    /**
     * Método que permite limpiar campos despues de salir de dialogo de asignar coeficiente
     *
     * @author leidy.gonzalez
     */
    public void limpiarCoeficiente() {
        this.fichaMatrizPrediosSeleccionados = null;
        this.sumatoriaCoefCopropiedad = null;
        this.coeficienteCopropiedad = null;
        this.selectedAllBool = false;
        this.fichaMatrizPrediosSeleccionados = new PFichaMatrizPredio[0];
        if (this.fichaMatriz.getPFichaMatrizPredios() != null &&
             !this.fichaMatriz.getPFichaMatrizPredios().isEmpty()) {

            for (PFichaMatrizPredio pFichaMatrizPredio : this.fichaMatriz.getPFichaMatrizPredios()) {

                pFichaMatrizPredio.setSelected(this.selectedAllBool);
            }
        }

        this.modificarPrediosMigracionBool = false;
        this.fichaMatrizPrediosSeleccionados = null;
    }

    /**
     * Método encargado de filtrar los PFichaMatrizPredio dependiendo de lo que el usuario ingrese
     * en el campo de búsqueda.
     *
     * @author leidy.gonzalez
     */
    public void filtrarPPrediosResultantesFichaMatrizPredio() {
        List<PFichaMatrizPredio> answer = new ArrayList<PFichaMatrizPredio>();

        List<PFichaMatrizPredio> pFichaMatrizPrediosCopia = new ArrayList<PFichaMatrizPredio>();
        pFichaMatrizPrediosCopia.addAll(this.fichaMatriz.getPFichaMatrizPredios());

        if (pFichaMatrizPrediosCopia != null && !pFichaMatrizPrediosCopia.isEmpty() &&
             this.numeroPredialBusquedaPFichaMatrizPredios != null &&
             !this.numeroPredialBusquedaPFichaMatrizPredios.isEmpty()) {

            answer = this.getConservacionService()
                .obtenerPFichaMatrizPredioContieneNumeroPredial(
                    this.numeroPredialBusquedaPFichaMatrizPredios);

        }

        if (answer != null && !answer.isEmpty()) {
            this.fichaMatriz.getPFichaMatrizPredios().clear();
            this.fichaMatriz.getPFichaMatrizPredios().addAll(answer);
        } else {
            this.fichaMatriz = this.getConservacionService()
                .buscarPFichaMatrizPorPredioId(this.getProyectarConservacionMB().
                    getPredioSeleccionado().getId());
            this.proyectarConservacionMB.setFichaMatrizSeleccionada(this.fichaMatriz);
        }

    }

    /**
     * Método encargado de consultar las PFichaMatrizPredio asociadas al tramite dependiendo si
     * mantiena o no la fichaMatriz
     *
     * @author leidy.gonzalez
     */
    public void consultarFichasMatrizCondominio() {

        this.fichasMatrizCondominio = new ArrayList<PFichaMatriz>();

        this.fichasMatricesTramite = this.getTramiteService()
            .obtenerFichasAsociadasATramite(this.tramite.getId());

        if (this.fichasMatricesTramite != null && !this.fichasMatricesTramite.isEmpty()) {
            for (PFichaMatriz fichasMatrizCond : this.fichasMatricesTramite) {

                if (EPredioCondicionPropiedad.CP_8.getCodigo().
                    equals(fichasMatrizCond.getPPredio().getNumeroPredial().substring(21, 22))) {

                    this.fichasMatrizCondominio.add(fichasMatrizCond);

                }

            }

        }
    }

    /**
     * Método encargado de insertar a los predios generados la direccion principal de la Ficha
     * Matriz
     *
     * @author leidy.gonzalez
     */
    public void consultarDireccionFM() {

        this.direccionPrincipalFM = this.getConservacionService().
            obtenerDireccionPrincipalFM(this.fichaMatriz.getPPredio().getNumeroPredial().
                substring(0, 21));
    }

    /**
     * Método encargado de insertar a los predios generados la direccion principal de la Ficha
     * Matriz
     *
     * @author leidy.gonzalez
     */
    public void consultarDireccionPredioGenerado(PPredio pPredio) {

        this.direccionPredioGenerado = new PPredioDireccion();

        this.direccionPredioGenerado = this.getConservacionService().
            obtenerDireccionPrincipalFichaMatrizId(pPredio.getId());
    }

    /**
     * Método encargado de insertar a los predios generados la direccion principal de la Ficha
     * Matriz
     *
     * @author leidy.gonzalez
     */
    public void asignarDireccionFMPrediosGenerados(List<PPredio> prediosGenerados) {

        for (PPredio pPredio : prediosGenerados) {

            //Verifica si el predio seleccionado ya tiene asignada una direccion principal
            this.consultarDireccionPredioGenerado(pPredio);

            if (this.direccionPredioGenerado == null) {

                PPredioDireccion direccionNueva = new PPredioDireccion();
                direccionNueva = (PPredioDireccion) this.direccionPrincipalFM.clone();
                direccionNueva.setPPredio(pPredio);
                direccionNueva.setId(null);

                direccionNueva = this.getConservacionService().guardarPPredioDireccion(
                    direccionNueva);

            } else if (this.direccionPredioGenerado != null &&
                 this.direccionPredioGenerado.getDireccion() != null &&
                !this.direccionPredioGenerado.getDireccion().isEmpty() &&
                 !this.direccionPredioGenerado.getDireccion().equals(this.direccionPrincipalFM.
                    getDireccion())) {

                for (PPredioDireccion predioDP : pPredio.getPPredioDireccions()) {

                    if (ESiNo.SI.getCodigo().equals(predioDP.getPrincipal())) {
                        predioDP.setDireccion(this.direccionPrincipalFM.getDireccion());

                        predioDP = this.getConservacionService().guardarPPredioDireccion(predioDP);
                    }
                }

            }

        }

    }

}
