/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tramite.ejecucion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.util.SNCManagedBean;
import org.primefaces.model.SortOrder;

/**
 * Managed bean para la ejecución de trámites Reutiliza metodos de asignacionTramites
 *
 * @author juan.agudelo
 */
@Component("ejecucionTramites")
@Scope("session")
public class EjecucionTramitesMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -9064304637726891681L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EjecucionTramitesMB.class);

    private List<String> prueba;

    public List<String> getPrueba() {
        return prueba;
    }

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites a los ya se les
     * ha asignado ejecutor, pero aún no han sido cambiados al siguiente estado del BPM
     */
    private LazyDataModel<Tramite> lazyAssignedTramites;

    /**
     * variable que guarda el valor del id de la territorial del usuario
     */
    private String territorialId;

    /**
     * Trámites seleccionados de la tabla de trámites sin ejecutor asignado
     */
    private Tramite[] selectedTramites;

    /**
     * is del trámite que se escohe para ver los detalles
     */
    private Long selectedTramiteId;

    /**
     * Tramite seleccionado para vizualizar el detalle del tramite
     */
    private Tramite tramite;

    /**
     * Predio(s) asociado(s) al tramite
     */
    private Predio predio;

    /**
     * Lista de tramite(s) asociado(s) a un tramite
     */
    private List<Tramite> tramitesAsociados;

    /**
     * Predio seleccionado desde evento de tabla
     */
    private Predio predioSeleccionado;

    /**
     * Datos del predio seleccionados desde el evento
     */
    private Predio predioDatos;

    /**
     * Predios para englobe
     */
    @SuppressWarnings("unused")
    private List<Predio> predios;

    /**
     * Id de predio utilizado en detalle tramite
     */
    private Long predioId;

    /**
     * Banderas de visualizacion de botones para solicitar otro documento
     */
    private boolean otroDoc;
    private boolean predioNoSeleccionado;
    private boolean adicionarDoc;
    private boolean generarOficioB;

    /**
     * Cadena modo de adquisición en solicitar otro documento
     */
    private String modoDeadquisicion;
    private String modoDeadquisicion2;

    /**
     * Lista de documentos asociados a un tramite
     */
    private List<TramiteDocumentacion> documentos;

    /**
     * Lista de documentos faltantes asociados a un tramite
     */
    private List<TramiteDocumentacion> documentosFaltantes;

    /**
     * Lista de documentos aportados asociados a un tramite
     */
    private List<TramiteDocumentacion> documentosAportados;

    /**
     * Lista de documentos adicionales aportados asociados a un tramite
     */
    private List<TramiteDocumentacion> documentosAdicionalesAportados;

    /** *
     * Documentacion asociada a un tramite
     */
    private TramiteDocumentacion tramiteDocumentacion;

    /**
     * Lista de items seleccionados para adquisicion
     */
    private List<SelectItem> adq;

    /**
     * Lista de docuemntos solicitados
     */
    private List<TramiteDocumentacion> documentosSoli;

    /**
     * Detalle de documento para solicitar un nuevo documento
     */
    private String detalleDocumento;

    /**
     * Evento eliminar de la lista de documentos adicionales solicitados
     */
    private SelectEvent eventoEliminar;

    /**
     * Lista de documentos del dominio TIPO_DOCUMENTO_CLASE
     */
    private List<Dominio> documentosDominio;

    /**
     * Ejecutor que accede al sistema
     */
    private String ejecutor;

    /**
     * Aceptar tramite si no tiene documentacion pendiente
     */
    private boolean aceptarTramiteDocuP;

    /**
     * Comision asociada a un tramite
     */
    private Comision comision;

    /**
     * Id de comision
     */
    private String comisionNumero;

    /**
     * lista de tramites paginados
     */
    private List<Tramite> rows;

    /**
     * predio numero Predial
     */
    private String numeroPredial;

    /**
     * Lista de predios englobe para detalle de predios
     */
    private List<Predio> listaPredio;

    /**
     * solicitud asociada a un tramite
     */
    private Solicitud solicitud;

    /**
     * tramite Id
     */
    private Long tramiteId;

    /**
     * tramiet seleccionado
     */
    private boolean tramiteS;

    /**
     * Tramite temporal
     */
    private Tramite tramiteTemp;

    /**
     * Motivo de la devolucion
     */
    private String motivoD;

    /**
     * Observación de la devolucion
     */
    private String observacionD;

    // ------------- methods ------------------------------------
    public void setPrueba(List<String> prueba) {
        this.prueba = prueba;
    }

    public String getMotivoD() {
        return motivoD;
    }

    public void setMotivoD(String motivoD) {
        this.motivoD = motivoD;
    }

    public String getObservacionD() {
        return observacionD;
    }

    public void setObservacionD(String observacionD) {
        this.observacionD = observacionD;
    }

    public Long getTramiteId() {
        return tramiteId;
    }

    public Long getPredioId() {
        return predioId;
    }

    public void setPredioId(Long predioId) {
        this.predioId = predioId;
    }

    public void setTramiteId(Long tramiteId) {
        this.tramiteId = tramiteId;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Predio getPredioDatos() {
        return predioDatos;
    }

    public void setPredioDatos(Predio predioDatos) {
        this.predioDatos = predioDatos;
    }

    public List<Predio> getListaPredio() {
        return listaPredio;
    }

    public void setListaPredio(List<Predio> listaPredio) {
        this.listaPredio = listaPredio;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getComisionNumero() {
        return comisionNumero;
    }

    public void setComisionNumero(String comisionNumero) {
        this.comisionNumero = comisionNumero;
    }

    public Comision getComision() {
        return comision;
    }

    public void setComision(Comision comision) {
        this.comision = comision;
    }

    public boolean isAceptarTramiteDocuP() {
        return aceptarTramiteDocuP;
    }

    public void setAceptarTramiteDocuP(boolean aceptarTramiteDocuP) {
        this.aceptarTramiteDocuP = aceptarTramiteDocuP;
    }

    public Tramite[] getSelectedTramites() {
        return selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public String getTerritorialId() {
        return territorialId;
    }

    public void setTerritorialId(String territorialId) {
        this.territorialId = territorialId;
    }

    public Predio getPredio() {
        return predio;
    }

    public Long getSelectedTramiteId() {
        return selectedTramiteId;
    }

    public void setSelectedTramiteId(Long selectedTramiteId) {
        this.selectedTramiteId = selectedTramiteId;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public LazyDataModel<Tramite> getLazyAssignedTramites() {
        return lazyAssignedTramites;
    }

    public void setLazyAssignedTramites(
        LazyDataModel<Tramite> lazyAssignedTramites) {
        this.lazyAssignedTramites = lazyAssignedTramites;
    }

    public boolean isGenerarOficioB() {
        return generarOficioB;
    }

    public void setGenerarOficioB(boolean generarOficioB) {
        this.generarOficioB = generarOficioB;
    }

    public boolean isAdicionarDoc() {
        return adicionarDoc;
    }

    public void setAdicionarDoc(boolean adicionarDoc) {
        this.adicionarDoc = adicionarDoc;
    }

    public List<TramiteDocumentacion> getDocumentosSoli() {
        return documentosSoli;
    }

    public void setDocumentosSoli(List<TramiteDocumentacion> documentosSoli) {
        this.documentosSoli = documentosSoli;
    }

    public String getDetalleDocumento() {
        return detalleDocumento;
    }

    public void setDetalleDocumento(String detalleDocumento) {
        this.detalleDocumento = detalleDocumento;
    }

    public boolean isOtroDoc() {
        return otroDoc;
    }

    public void setOtroDoc(boolean otroDoc) {
        this.otroDoc = otroDoc;
    }

    public List<SelectItem> getAdq() {
        return adq;
    }

    public void setAdq(List<SelectItem> adq) {
        this.adq = adq;
    }

    public String getModoDeadquisicion() {
        return modoDeadquisicion;
    }

    public void setModoDeadquisicion(String modoDeadquisicion) {
        this.modoDeadquisicion = modoDeadquisicion;
    }

    public String getModoDeadquisicion2() {
        return modoDeadquisicion2;
    }

    public void setModoDeadquisicion2(String modoDeadquisicion2) {
        this.modoDeadquisicion2 = modoDeadquisicion2;
    }

    public List<Tramite> getTramitesAsociados() {
        return tramitesAsociados;
    }

    public void setTramitesAsociados(List<Tramite> tramitesAsociados) {
        this.tramitesAsociados = tramitesAsociados;
    }

    public List<TramiteDocumentacion> getDocumentosFaltantes() {
        return documentosFaltantes;
    }

    public void setDocumentosFaltantes(
        List<TramiteDocumentacion> documentosFaltantes) {
        this.documentosFaltantes = documentosFaltantes;
    }

    public List<TramiteDocumentacion> getDocumentosAportados() {
        return documentosAportados;
    }

    public void setDocumentosAportados(
        List<TramiteDocumentacion> documentosAportados) {
        this.documentosAportados = documentosAportados;
    }

    public List<TramiteDocumentacion> getDocumentosAdicionalesAportados() {
        return documentosAdicionalesAportados;
    }

    public void setDocumentosAdicionalesAportados(
        List<TramiteDocumentacion> documentosAdicionalesAportados) {
        this.documentosAdicionalesAportados = documentosAdicionalesAportados;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    @Deprecated
    public void init() {
        LOGGER.debug("entra al init de EjecucionTramitesMB");

        LOGGER.debug(
            "SI VE ESTE MENSAJE, AVISE A JUAN AGUDELO, YA QUE ESTE INIT ESTÁ CON DATOS QUEMADOS!!!!!!!");

//TODO :: 08-03-2012 :: juan.agudelo :: OJO: jay que determinar si este mb se está usando efectivamente en algún lado
        // TODO::juan.agudelo::el id de la territorial debe haber sido pasado como parámetro
        // de alguna forma
        this.setTerritorialId("6368");
        this.ejecutor = "SENIOR2";
        this.tramiteS = true;

        poblar();
    }

    // --------------------------------------------------------------------------------------------------
    public void poblar() {

        int count;
        count = this.getTramiteService()
            .contarTramitesDeEjecutorAsignado(ejecutor);

        if (count > 0) {
            this.lazyAssignedTramites.setRowCount(count);
        } else {
            this.lazyAssignedTramites.setRowCount(0);
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public EjecucionTramitesMB() {

        this.lazyAssignedTramites = new LazyDataModel<Tramite>() {

            // Si existe mas de una comision en la DB el toma por defecto la
            // primera de la lista
            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();
                populateTramitesConEjecutorLazyly(answer, first, pageSize);

                return answer;

            }
        };
    }

    // --------------------------------------------------------------------------------------------------
    private void populateTramitesConEjecutorLazyly(List<Tramite> answer,
        int first, int pageSize) {

        rows = new ArrayList<Tramite>();

        // TODO: juan.agudelo OJO: valor quemado!!!
        this.ejecutor = "SENIOR2";
        int size;

        rows = this.getTramiteService().buscarTramitesDeEjecutorAsignado(
            this.territorialId, ejecutor, first, pageSize);

        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }

        // Este metodo carga los predios para ver detalles y se carga aca por
        // bug en la accion sobre listas o tablas dentro de trablas
        detallePredios();

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Detalle tramite
     */
    public void consultarDetalleDelTramite() {

        tramite = new Tramite();
        tramitesAsociados = new ArrayList<Tramite>();
        predioNoSeleccionado = false;
        predios = new ArrayList<Predio>();
        this.otroDoc = false;
        this.generarOficioB = true;
        this.adicionarDoc = true;
        this.documentosSoli = new ArrayList<TramiteDocumentacion>();
        this.documentos = new ArrayList<TramiteDocumentacion>();
        this.documentosAdicionalesAportados = new ArrayList<TramiteDocumentacion>();
        this.documentosAportados = new ArrayList<TramiteDocumentacion>();
        this.documentosFaltantes = new ArrayList<TramiteDocumentacion>();
        this.aceptarTramiteDocuP = true;

        this.tramite = this.getTramiteService()
            .buscarTramiteTramitesPorTramiteId(selectedTramiteId);
        obtenerTramitesAsociados(tramite);

        documentos = tramite.getTramiteDocumentacions();

        if (documentos != null) {

            for (int i = 0; i < documentos.size(); i++) {
                TramiteDocumentacion temp = documentos.get(i);

                if (temp.getAportado().equals(ESiNo.NO.getCodigo())) {
                    documentosFaltantes.add(temp);

                } else if (temp.getAdicional().equals(ESiNo.NO.getCodigo())) {
                    documentosAportados.add(temp);
                } else {
                    documentosAdicionalesAportados.add(temp);

                }
            }

            if (documentosFaltantes.size() > 0) {
                this.aceptarTramiteDocuP = false;
            }
        }
        LOGGER.debug("tipo tramite: " + tramite.getTipoTramite());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Obtiene los tramites asociados a un tramite
     */
    private void obtenerTramitesAsociados(Tramite tramite) {

        if (tramite != null) {
            if (tramite.getTramite() != null) {
                tramitesAsociados.add(tramite);
                obtenerTramitesAsociados(tramite.getTramite());
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Bandera de visualizacion de botones en solicitar otro documento
     */
    public void otroDocumentoTipo() {

        String valor = this.modoDeadquisicion;
        LOGGER.debug("" + "Valor: " + valor);
        if (valor.equals(Constantes.OTRO)) {
            this.otroDoc = true;
        } else {
            this.otroDoc = false;
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Datos iniciales de la ventana modal solicitarDocumentos
     */
    public void cargarSolicitarDocumentos() {
        modo();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Carga el menu de seleccion de tipo de documento en solicitar otro documento
     */
    public void modo() {

        adq = new ArrayList<SelectItem>();
        this.documentosDominio = new ArrayList<Dominio>();

        this.adq.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.documentosDominio = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.TIPO_DOCUMENTO_CLASE);
        for (Dominio d : documentosDominio) {
            this.adq.add(new SelectItem(d.getCodigo(), d.getValor()));
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion al ejecutar el boton adicionar documento
     */
    public void adicionarDocu() {

        String clase = "";
        TipoDocumento tipoDocumento = new TipoDocumento();
        tramiteDocumentacion = new TramiteDocumentacion();

        // Busqueda en tabla TIPO_DOCUMENTO
        if (modoDeadquisicion.equals(Constantes.OTRO)) {
            tipoDocumento.setNombre(modoDeadquisicion2);
        } else {
            tipoDocumento.setNombre(modoDeadquisicion);
        }

        clase = modoDeadquisicion;

        // Datos para la tabla TRAMITE_DOCUMENTACION
        tramiteDocumentacion.setTramite(tramite);
        tramiteDocumentacion.setAportado(ESiNo.NO.getCodigo());
        tramiteDocumentacion.setFechaAporte(null);
        tramiteDocumentacion.setFechaSolicitudDocumento(null);
        tramiteDocumentacion.setDocumentoSoporte(null);
        tramiteDocumentacion.setAdicional(ESiNo.SI.getCodigo());
        tramiteDocumentacion.setCantidadFolios(0);
        tramiteDocumentacion.setFechaTitulo(null);
        tramiteDocumentacion.setNumeroTitulo(null);
        // TODO confirmar oficina emisora
        tramiteDocumentacion.setOficinaEmisora(null);
        tramiteDocumentacion.setDepartamento(null);
        tramiteDocumentacion.setMunicipio(null);

        if (clase.equals(Constantes.OTRO)) {
            tramiteDocumentacion.setDetalle(modoDeadquisicion2 + " : " +
                 detalleDocumento);
        } else {
            tramiteDocumentacion.setDetalle(detalleDocumento);
        }

        tramiteDocumentacion.setRequerido(ESiNo.SI.getCodigo());

        // TODO juan.agudelo usar el usuario del sistema
        tramiteDocumentacion.setUsuarioLog("jgag");
        tramiteDocumentacion.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumentacion.setTipoDocumento(this.getGeneralesService()
            .getCacheTipoDocumentoPorClase(clase));

        LOGGER.debug("Documentos solicitados " + documentosSoli.size());

        if (documentosSoli.size() > 0) {
            Boolean repetido = false;
            for (int h = 0; h < documentosSoli.size(); h++) {

                if (tramiteDocumentacion
                    .getTipoDocumento()
                    .getNombre()
                    .equals(documentosSoli.get(h).getTipoDocumento()
                        .getNombre()) &&
                     tramiteDocumentacion
                        .getFechaTitulo()
                        .toString()
                        .regionMatches(
                            0,
                            documentosSoli.get(h).getFechaTitulo()
                                .toString(), 0, 19)) {
                    LOGGER.debug(
                        "El documento que desea ingresar ya se encuentra en la base de datos");
                    repetido = true;
                } else {
                    repetido = false;
                }
            }
            LOGGER.debug("repetido: " + repetido);

            if (repetido == false) {
                this.documentosSoli.add(tramiteDocumentacion);
            }
        } else {
            this.documentosSoli.add(tramiteDocumentacion);
        }

        this.detalleDocumento = new String();
        this.modoDeadquisicion = "";
        this.modoDeadquisicion2 = new String();
        this.otroDoc = false;

        if (documentosSoli.size() > 0) {
            this.generarOficioB = false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion de eliminar un documento de la lista de adicionar otro documento en solicitu de
     * documento
     */
    public void eliminarDocumentoList() {

        TramiteDocumentacion elim = (TramiteDocumentacion) eventoEliminar
            .getObject();

        if (documentosSoli.equals(0)) {
            LOGGER.debug("No existen documentos para eliminar");
        } else {
            for (int k = 0; k < documentosSoli.size(); k++) {
                if (elim.getTipoDocumento()
                    .getNombre()
                    .equals(documentosSoli.get(k).getTipoDocumento()
                        .getNombre()) &&
                     elim.getFechaLog().equals(
                        documentosSoli.get(k).getFechaLog())) {
                    documentosSoli.remove(k);
                }
            }
        }

        if (documentosSoli.size() > 0) {
            this.generarOficioB = false;
        } else {
            this.generarOficioB = true;
        }

        this.adicionarDoc = true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Control de visualizacion de botones en la seleccion de documento
     */
    public void eliminarDocu(SelectEvent event) {
        this.eventoEliminar = event;
        this.generarOficioB = true;
        this.adicionarDoc = false;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Control de visualizacion de botones en la des seleccion de documento
     */
    public void desEliminarDocu(UnselectEvent eventDesSeleccionar) {

        if (adicionarDoc == true) {
            this.adicionarDoc = false;
        } else {
            adicionarDoc = true;
        }

        this.generarOficioB = false;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion del boton generar oficio
     */
    public void generarOficio() {

        this.getTramiteService()
            .ingresarDocumentosAdicionalesRequeridos(documentosSoli);
        documentosSoli = new ArrayList<TramiteDocumentacion>();
        generarOficioB = true;
        consultarDetalleDelTramite();
        this.aceptarTramiteDocuP = false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Detalle comision
     */
    public void detalleComision() {

        comision = new Comision();

        if (rows != null && rows.size() > 0) {
            for (int c = 0; c < rows.size(); c++) {

                if (rows.get(c).getComisionTramites().get(0).getComision()
                    .getNumero().equals(comisionNumero)) {
                    comision = rows.get(c).getComisionTramites().get(0)
                        .getComision();
                }
            }
        }
    }

//--------------------------------------------------------------------------------------------------
//TODO::juan.agudelo::03-08-2011::DOCUMENTAR!!!. Cambiar nombre de método: debe ser el infinitivo de un verbo
    /**
     * Detalle predio
     */
    public void detallePredio() {

        predio = new Predio();

        if (rows != null && rows.size() > 0) {
            for (int c = 0; c < rows.size(); c++) {
                LOGGER.debug("Ehhhhh : " + predioId);
                LOGGER.debug("Ehhhhh2 : " +
                     rows.get(c).getTramitePredioEnglobes().get(0)
                        .getPredio().getId());

                if (rows.get(c).isSegundaEnglobe()) {
                    if (rows.get(c).getTramitePredioEnglobes().get(0)
                        .getPredio().getId().equals(predioId)) {
                        predio = rows.get(c).getTramitePredioEnglobes().get(0)
                            .getPredio();

                    } else {
                        if (rows.get(c).getPredio().getNumeroPredial()
                            .equals(predioId)) {
                            predio = rows.get(c).getPredio();
                        }
                    }
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Evento de selecion sobre la tabla de predios asociados a un tramite en detalle del tramite
     */
    public void seleccionarPredio(SelectEvent evento) {

        this.predioNoSeleccionado = true;
        this.predioSeleccionado = (Predio) evento.getObject();
        Long predioId = predioSeleccionado.getId();
        predioDatos = new Predio();

        for (int g = 0; g < listaPredio.size(); g++) {
            if (listaPredio.get(g).getId().equals(predioId)) {
                this.predioDatos = listaPredio.get(g);
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Evento de desselecion sobre la tabla de predios asociados a un tramite en detalle del tramite
     */
    public void deseleccionarPredio(UnselectEvent evento) {

        this.predioNoSeleccionado = false;
        this.predioSeleccionado = (Predio) evento.getObject();

    }

    public boolean isPredioNoSeleccionado() {
        return predioNoSeleccionado;
    }

    public void setPredioNoSeleccionado(boolean predioNoSeleccionado) {
        this.predioNoSeleccionado = predioNoSeleccionado;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Detalle predio
     */
    public void detallePredios() {

        listaPredio = new ArrayList<Predio>();

        if (rows != null && rows.size() > 0) {
            for (int c = 0; c < rows.size(); c++) {
                if (rows.get(c).getTramitePredioEnglobes() != null &&
                     rows.get(c).getTramitePredioEnglobes().size() > 0) {
                    for (int d = 0; d < rows.get(c).getTramitePredioEnglobes()
                        .size(); d++) {
                        listaPredio.add(rows.get(c).getTramitePredioEnglobes()
                            .get(d).getPredio());
                    }
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Detalle de la solicitud asociada a un tramite
     */
    public void detalleSolicitud() {

        this.solicitud = new Solicitud();

        if (rows != null && rows.size() > 0) {
            for (int h = 0; h < rows.size(); h++) {
                if (rows.get(h).getId().equals(tramiteId)) {
                    this.solicitud = rows.get(h).getSolicitud();
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Action ejecutada al dar click en el botón "Ver solicitud"
     */
    public void selectedTramites(SelectEvent traSel) {
        tramiteS = false;
        tramiteTemp = (Tramite) traSel.getObject();
        if (tramiteTemp != null) {
            tramiteId = tramiteTemp.getId();
        }
    }

    public boolean isTramiteS() {
        return tramiteS;
    }

    public void setTramiteS(boolean tramiteS) {

        this.tramiteS = tramiteS;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Tramite deseleccionado
     */
    public void selectedNoTramites(UnselectEvent traNoSel) {
        this.tramiteS = true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Devolver tramite
     */
    public void devolverTramite() {

        // TODO motivoD y observacionD cuando se encuentren definidos deben
        // almacenarse en el lugar indicado
        Tramite[] dev = new Tramite[1];
        Tramite tramiteEnvio;
        tramiteEnvio = tramiteTemp;

        //TODO juan.agudelo :: 13-09-11 :: revisar uso, ya no se debe consumir este método :: juan.agudelo
        /* tramiteEnvio
				.setEstado(ETramiteEstado.EN_ESTUDIO_DEL_RESPONSABLE_DE_CONSERVACION.getCodigo()); */
        dev[0] = tramiteEnvio;

        if (dev != null) {
            this.getTramiteService().actualizarTramites(dev);
        }
        dev[0] = null;
        this.tramiteS = true;
        poblar();
    }

    // end of class
}
