package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.LevantamientoAsignacion;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author javier.barajas
 * @cu 209 Exportar Informacion de Manzanas Creacion de metodos para consultar las manzanas a
 * exportar, incoporacion de funcion de xportar manzanas a shp dxf dwg
 *
 */
@Component("exportarInformacionDeManzanas")
@Scope("session")
public class ExportacionInformacionDeManzanasMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ExportacionInformacionDeManzanasMB.class);

    @Autowired
    private IContextListener contextoWeb;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Lista ActualizacionLevantamiento manzanas a exportar y lista de topografos
     */
    private List<ActualizacionLevantamiento> manzanasExportar;
    /**
     * Lista de seleccion por topografo
     */

    private List<LevantamientoAsignacion> listaTopografos;
    /**
     * Opcion de exportacion
     */

    private String seleccionExportacion;

    /**
     * Ruta del archivo a exportar
     */
    private String rutaArchivoExportar;

    /**
     * Datos del usuario de la aplacion
     */
    private UsuarioDTO usuario;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    private String manzanaCodigo;

    /**
     * Seleccion de datatable
     */
    private ActualizacionLevantamiento[] selectManzanas;
    /**
     * Seleccion de topografo
     */

    private ArrayList<SelectItem> selectTopografo;

    /**
     * Seleccion del topografo
     */
    private String selecionTopografo;

    /**
     * Variable para mostrar las manzanas a exportar
     */
    private boolean rendererManzanasExportar;

    /**
     * Variable de tipo streamed content que contiene el archivo con el .zip con la exportacion de
     * laz manzanas
     */
    private StreamedContent archivosADescargar;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 24/10/12

        // Obtener la actualizacióin del arbol de tareas.
        this.setActualizacionSeleccionada(this.getActualizacionService()
            .recuperarActualizacionPorId(450L));

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.moduleLayer = "ac";
        this.moduleTools = "ac";
        this.rendererManzanasExportar = false;
        this.selectTopografo = new ArrayList<SelectItem>();
        this.archivosADescargar = new DefaultStreamedContent();
        this.listaTopografos = this.getActualizacionService().
            buscarTopografosLevantamientoActualizacion(
                this.getActualizacionService().buscarActualizacionLevantamientoPorActualizacionId(
                    this.actualizacionSeleccionada.getId(), null));
        if (this.listaTopografos.size() > 0) {

            //TODO: Se agrega consulta del recurso humano por id por que no traia el recurso humano con la consulta anterior
            for (LevantamientoAsignacion la : this.listaTopografos) {

                try {
                    RecursoHumano rh = this.getActualizacionService().buscarRecursoHumanoporId(la.
                        getRecursoHumano().getId());
                    la.setRecursoHumano(rh);
                } catch (Exception e) {
                    LOGGER.debug(e.getMessage(), e);
                }

            }
            this.adicionarDatosVitem(this.selectTopografo, this.listaTopografos);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "No existen topografos para esta Actualización",
                "No existen topografos para esta Actualización"));
            LOGGER.warn(
                "No existen topografos para esta Actualización: this.listaTopografos.size<=0");
        }

    }

    // **************************************************************************************************************************
    // Getters y Setters 
    // **************************************************************************************************************************
    public void setActualizacionSeleccionada(Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public List<ActualizacionLevantamiento> getManzanasExportar() {
        return manzanasExportar;
    }

    public void setManzanasExportar(
        List<ActualizacionLevantamiento> manzanasExportar) {
        this.manzanasExportar = manzanasExportar;
    }

    public String getSeleccionExportacion() {
        return seleccionExportacion;
    }

    public void setSeleccionExportacion(String seleccionExportacion) {
        this.seleccionExportacion = seleccionExportacion;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public ActualizacionLevantamiento[] getSelectManzanas() {
        return selectManzanas;
    }

    public void setSelectManzanas(ActualizacionLevantamiento[] selectManzanas) {
        this.selectManzanas = selectManzanas;
    }

    public ArrayList<SelectItem> getSelectTopografo() {
        return selectTopografo;
    }

    public void setSelectTopografo(ArrayList<SelectItem> selectTopografo) {
        this.selectTopografo = selectTopografo;
    }

    public List<LevantamientoAsignacion> getListaTopografos() {
        return listaTopografos;
    }

    public void setListaTopografos(List<LevantamientoAsignacion> listaTopografos) {
        this.listaTopografos = listaTopografos;
    }

    public String getSelecionTopografo() {
        return selecionTopografo;
    }

    public void setSelecionTopografo(String selecionTopografo) {
        this.selecionTopografo = selecionTopografo;
    }

    public boolean isRendererManzanasExportar() {
        return rendererManzanasExportar;
    }

    public void setRendererManzanasExportar(boolean rendererManzanasExportar) {
        this.rendererManzanasExportar = rendererManzanasExportar;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getRutaArchivoExportar() {
        return rutaArchivoExportar;
    }

    public void setRutaArchivoExportar(String rutaArchivoExportar) {
        this.rutaArchivoExportar = rutaArchivoExportar;
    }

    public StreamedContent getArchivosADescargar() {

        return archivosADescargar;
    }

    public void setArchivosADescargar(StreamedContent archivosADescargar) {
        this.archivosADescargar = archivosADescargar;
    }

    // **************************************************************************************************************************
    // Funciones
    // **************************************************************************************************************************
    /**
     * Carga las manzanas a exportar
     *
     * @author javier.barajas
     */
    public void cargarManzanasaExportar() {
        this.rendererManzanasExportar = true;
        this.manzanasExportar = this.getActualizacionService().
            buscarActualizacionLevantamientoPorActualizacionId(
                this.actualizacionSeleccionada.getId(), Long.valueOf(this.selecionTopografo));

    }

    /**
     * Ejectua la funcion para exportar las manazanas seleccionadas
     *
     * @author javier.barajas
     */
    public void exportarManzana() {

        LOGGER.debug("Entro exportarManzana");
        try {
            this.rutaArchivoExportar = this.getActualizacionService().
                exportarManzanasaTopografos(this.manzanaCodigo, this.seleccionExportacion,
                    this.usuario);
            this.descargarArchivosaExportar();
            LOGGER.debug("Ruta Archivo de exportacion:" + this.rutaArchivoExportar);
        } catch (Exception e) {
            //LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error a generar el archivo de exportación");
        }
    }

    /**
     * Obtiene las manazanas segun la seleccion
     *
     * @author javier.barajas
     */
    public void obtenerManzanas() {
        int contador = 0;
        if (this.selectManzanas.length > 0) {
            if (this.selectManzanas.length == 1) {
                this.manzanaCodigo = this.selectManzanas[0].getIdentificador().toString();
            } else {
                for (ActualizacionLevantamiento al : this.selectManzanas) {
                    if (contador == 0) {
                        this.manzanaCodigo = al.getIdentificador().toString();
                    } else {
                        this.manzanaCodigo = this.manzanaCodigo + al.getIdentificador().toString();
                    }
                    if (this.selectManzanas.length - 1 != contador) {
                        this.manzanaCodigo = this.manzanaCodigo + ",";
                        contador++;
                    }
                }
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No ha seleccionado ninguna manzana",
                "No ha seleccionado ninguna manzana"));
            LOGGER.warn("No ha seleccionado ninguna manzana: this.selectManzanas.length<=0");
        }

    }

    /**
     * Obtiene la manzana seleccionada
     *
     * @param indentificadorManzana
     *
     * @author javier.barajas
     */
    public void obtenerUnaManzana(ActionEvent indentificadorManzana) {

        FacesMessage msg = new FacesMessage("Manzana Selecionada:" + indentificadorManzana.
            getSource().getClass().getName());
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    /**
     * Borra la manana del datatable
     *
     * @author javier.barajas
     */
    public void borrarManzana() {
        this.manzanaCodigo = null;
        FacesMessage msg = new FacesMessage("lo hizo");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Evento que lanza cuando se selecciona una tabla
     *
     * @param evento
     *
     * @author javier.barajas
     */
    public void rowSelected(org.primefaces.event.SelectEvent evento) {
        this.obtenerManzanas();
    }

    /**
     * Convierte una lista a lista selectitem
     *
     * @param lista
     * @param listTopogra
     *
     * @author javier.barajas
     */
    private void adicionarDatosVitem(List<SelectItem> lista,
        List<LevantamientoAsignacion> listTopogra) {

        for (LevantamientoAsignacion t : listTopogra) {
            //if(t.getRecursoHumano() != null)
            lista.add(new SelectItem(String.valueOf(t.getId()), t.getRecursoHumano().getNombre()));
        }
    }

    /**
     * Se lanza cuando se selecciona la opcion de exportar
     *
     * @author javier.barajas
     */
    public void rowSelectedExportar() {
        LOGGER.info("formato:" + this.seleccionExportacion);
        LOGGER.info("cod manzanas:" + this.manzanaCodigo);
        LOGGER.info("usuario:" + this.usuario.getNombreCompleto());
    }

    // ------------------------------------------------------------------------------------------
    /**
     * Funcion para descargar el archivo generado
     *
     * @author javier.barajas
     */
    public void descargarArchivosaExportar() {

        File archivoDocumentosAsociadosATramite = new File(this.rutaArchivoExportar);
        if (!archivoDocumentosAsociadosATramite.exists()) {
            LOGGER.warn("Error el archivo de origen no existe");
            this.addMensajeError("Ocurrió un error al descargar los archivos");
            return;
        }

        InputStream stream;

        try {
            stream = new FileInputStream(archivoDocumentosAsociadosATramite);
            this.setArchivosADescargar(new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_ZIP, "archivo_exportacion.zip"));
        } catch (FileNotFoundException e) {
            LOGGER.error(
                "Archivo no encontrado, no se puede descargar los documentos asociados al trámite",
                e);
        }

    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("exportarInformacionDeManzanas");
        this.tareasPendientesMB.init();
        //this.init();
        return "index";
    }

}
