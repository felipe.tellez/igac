/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.jobs;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

/**
 * Este Job realiza las siguientes operaciones: Consulta los jobs que enviaron a ejecución
 * geográfica y reportaron errores de Arcgis Server
 *
 * Envía los jobs encontrados para que intente volver a enviarlos. Para tal fin se utiliza una tarea
 * ReenviarJobTask. Dicha tarea se ejecuta en su propio hilo buscando mayor paralelismo. ( Los
 * threads son controlados por Spring )
 *
 *
 * @author andres.eslava
 *
 */
public class ConsultarJobsGeograficosErrorRunnableJob extends AbstractLocator implements Runnable,
    Serializable {

    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(
        ConsultarJobsGeograficosEsperandoRunnableJob.class);

    private TaskExecutor taskExecutor;

    private Integer numeroHoras;
    private Integer numeroJobs;
    private Integer numeroReintentos;

    public void setNumeroHoras(Integer numeroHoras) {
        this.numeroHoras = numeroHoras;
    }

    public void setNumeroJobs(Integer numeroJobs) {
        this.numeroJobs = numeroJobs;
    }

    public void setNumeroReintentos(Integer numeroReintentos) {
        this.numeroReintentos = numeroReintentos;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void run() {
        try {
            List<ProductoCatastralJob> jobsPendientes = this.getGeneralesService().
                obtenerJobsGeograficosError(numeroHoras, numeroJobs, numeroReintentos);
            if (jobsPendientes != null && jobsPendientes.size() > 0) {
                for (ProductoCatastralJob job : jobsPendientes) {
                    //envía los jobs a la cola (Dicha cola tiene un pool de instancias que se ejecutan en paralelo )
                    this.taskExecutor.execute(new ReenviarJobTask(job));
                }
            } else {
                LOGGER.warn("No hay jobs geográficos esperando");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
