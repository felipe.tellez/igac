package co.gov.igac.snc.web.mb.generales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.persistence.entity.generales.Componente;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.VPermisoGrupo;
import co.gov.igac.snc.persistence.util.EGrupoComponenteAmbito;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EMenuOpcionesCierreAnual;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;


/**
 * En este managed bean se administran los datos del usuario asociados a la sesión actual
 *
 * @author fredy.wilches
 *
 */
@Component("menu")
@Scope("session")
public class MenuMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -1678157305193873654L;
    //private Authentication authentication;
    private MenuModel model;
    private UsuarioDTO usuarioDto;
    private boolean usuarioDelegado;
    private boolean usuarioHabilitado;
    private String tema = "custom-theme1";
    private List<VPermisoGrupo> opcionesConPermiso = new ArrayList<VPermisoGrupo>();

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuMB.class);

    /**
     *
     */
    @PostConstruct
    public void init() {

        this.usuarioDto = obtenerDatosUsuarioAutenticado();
        Long idReportMenu = null;

        if (this.usuarioDto == null) {
            LOGGER.error("No se pudo localizar el usuario de la sesión");
            return;
        }
        if (this.usuarioDto.getDescripcionTerritorial() != null) {
            this.usuarioDelegado = this.usuarioDto.getDescripcionTerritorial().contains(
                Constantes.PREFIJO_DELEGADOS);
            this.usuarioHabilitado=this.usuarioDto.getDescripcionTerritorial().contains(
                Constantes.PREFIJO_HABILITADOS);
        } else {
            this.usuarioDelegado = false;
        }

        //Manejo del menu de reportes
        List<Componente> menusReportes = new ArrayList<Componente>();
        List<RepUsuarioRolReporte> reportesAsignados = this.getConservacionService()
            .buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
                this.usuarioDto, ERepReporteCategoria.REPORTES_RADICACION.getCategoria());
        if (reportesAsignados != null && !reportesAsignados.isEmpty()) {
            Componente c = this.getGeneralesService().
                obtenerComponentePorNombre(ERepReporteCategoria.REPORTES_RADICACION.
                    getNombreComponente());
            if (c != null) {
                menusReportes.add(c);
            }
        }

        reportesAsignados = this.getConservacionService()
            .buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
                this.usuarioDto, ERepReporteCategoria.REPORTES_ESTADISTICOS.getCategoria());
        if (reportesAsignados != null && !reportesAsignados.isEmpty()) {
            Componente c = this.getGeneralesService().
                obtenerComponentePorNombre(ERepReporteCategoria.REPORTES_ESTADISTICOS.
                    getNombreComponente());
            if (c != null) {
                menusReportes.add(c);
            }
        }

        reportesAsignados = this.getConservacionService()
            .buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
                this.usuarioDto, ERepReporteCategoria.REPORTES_PREDIALES.getCategoria());
        if (reportesAsignados != null && !reportesAsignados.isEmpty()) {
            Componente c = this.getGeneralesService().
                obtenerComponentePorNombre(ERepReporteCategoria.REPORTES_PREDIALES.
                    getNombreComponente());
            if (c != null) {
                menusReportes.add(c);
            }
        }

        if (!menusReportes.isEmpty()) {
            Componente c = this.getGeneralesService().
                obtenerComponentePorNombre("Consultas y reportes");
            if (c != null) {
                menusReportes.add(c);
            }
        }

        if (!menusReportes.isEmpty()) {
            Componente c = this.getGeneralesService().
                    obtenerComponentePorNombre("Reportes");
            if (c != null) {
                idReportMenu = c.getId();
                menusReportes.add(c);
            }
        }

        List<VPermisoGrupo> menu = this.getGeneralesService().getCacheMenu(this.usuarioDto.
                getRoles());

        // Manejo de menu de cierre para usuario administrador
        // Validación para saber si se el cierre se va a realizar
        // por parte de un usuario administrador

        List<Dominio> usuarioAdmin = this.getGeneralesService().buscarDominioPorNombreOrderByNombre(EDominio.USUARIO_ADMIN_CIERRE_ANIO);
        Submenu submenuAdminCierre = new Submenu();
        List<Componente> menusCierre = new ArrayList<Componente>();

        if(usuarioAdmin != null && !usuarioAdmin.isEmpty()) {
            if (this.usuarioDto.getLogin().equals(usuarioAdmin.get(0).getValor())) {
                for(EMenuOpcionesCierreAnual e : EMenuOpcionesCierreAnual.values()) {
                    Componente componente = this.getGeneralesService().obtenerComponentePorNombre(e.getNombre());
                    if (componente != null) {
                        menusCierre.add(componente);
                    }
                }
                if (!menusCierre.isEmpty()) {

                    submenuAdminCierre.setLabel("Administrador cierre");
                    submenuAdminCierre.setId("MENU_ADMIN");
                    for(Componente c : menusCierre) {
                        MenuItem item = new MenuItem();
                        item.setValue(c.getNombre());
                        item.setUrl(c.getUrl());
                        item.setId("OPCION" + c.getId());

                        VPermisoGrupo menuComp = new VPermisoGrupo(
                                BigDecimal.valueOf(3.0),
                                BigDecimal.valueOf(c.getId()),
                                c.getTipo(),
                                c.getNombre(),
                                c.getUrl(),
                                null,
                                BigDecimal.ONE,
                                BigDecimal.valueOf(102));
                        menuComp.setAmbito(EGrupoComponenteAmbito.TODOS.toString());
                        this.opcionesConPermiso.add(menuComp);
                        submenuAdminCierre.getChildren().add(item);
                    }
                }
            }
        }

        for (Componente comp : menusReportes) {
            if (comp.getActivo().equals(ESiNo.SI.getCodigo())) {

                boolean exists = false;
                for (VPermisoGrupo vPermisoGrupo : menu) {
                    if (vPermisoGrupo.getId().longValue() == comp.getId()) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    VPermisoGrupo menuComp = new VPermisoGrupo(
                        BigDecimal.valueOf(3.0),
                        BigDecimal.valueOf(comp.getId()),
                        comp.getTipo(),
                        comp.getNombre(),
                        comp.getUrl(),
                        null,
                        BigDecimal.ONE,
                        BigDecimal.valueOf(idReportMenu));
                    menuComp.setAmbito(EGrupoComponenteAmbito.TODOS.toString());
                    menu.add(menuComp);
                }
            }
        }

        this.model = new DefaultMenuModel();

        for (VPermisoGrupo p : menu) {
            if (!p.getTipo().equals("SISTEMA") && p.getNivel().intValue() == 2) {
                Submenu submenu = new Submenu();
                submenu.setLabel(p.getNombre());
                submenu.setId("MENU" + p.getId());
                adicionarItems(p.getId(), submenu, menu);
                this.model.addSubmenu(submenu);
            }
        }

        if (!menusCierre.isEmpty()) {
            this.model.addSubmenu(submenuAdminCierre);
        }

        MenuItem salir = new MenuItem();
        salir.setValue("Salir");
        salir.setUrl("/logout");
        salir.setId("salirID");
        this.model.addMenuItem(salir);

    }

    /**
     *
     * @param id
     * @param submenu
     * @param menu
     */
    private void adicionarItems(BigDecimal id, Submenu submenu, List<VPermisoGrupo> menu) {
        for (VPermisoGrupo p : menu) {
            if (id.equals(p.getPadreComponenteId())) {
                if (p.getTipo().equals("MENU")) {
                    Submenu submenu2 = new Submenu();
                    submenu2.setLabel(p.getNombre());
                    submenu2.setId("MENU" + p.getId());
                    submenu.getChildren().add(submenu2);
                    adicionarItems(p.getId(), submenu2, menu);
                } else {
                    // OPCION
                    if (p.getAmbito().equals(EGrupoComponenteAmbito.TODOS.toString()) ||
                        (p.getAmbito().equals(EGrupoComponenteAmbito.TERRITORIAL.toString()) &&
                        this.usuarioDto.isTerritorial()) ||
                        (p.getAmbito().equals(EGrupoComponenteAmbito.UOC.toString()) &&
                        this.usuarioDto.isUoc())) {
                        MenuItem item = new MenuItem();
                        item.setValue(p.getNombre());
                        item.setUrl(p.getUrl());
                        this.opcionesConPermiso.add(p);
                        item.setId("OPCION" + p.getId());
                        submenu.getChildren().add(item);
                    }
                }
            }
        }
    }

    public MenuModel getModel() {
        return this.model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public static MenuMB getMenu() {
        return (MenuMB) UtilidadesWeb.getManagedBean("menu");
    }

    public UsuarioDTO getUsuarioDto() {
        if (this.usuarioDto != null && !this.usuarioDto.getLogin().equals(this.
            obtenerDatosUsuarioAutenticado().getLogin())) {
            init();
            TareasPendientesMB mb = (TareasPendientesMB) UtilidadesWeb.getManagedBean(
                "tareasPendientes");
            mb.init();
        }
        return usuarioDto;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public boolean isUsuarioHabilitado() {
        return usuarioHabilitado;
    }

    public void setUsuarioHabilitado(boolean usuarioHabilitado) {
        this.usuarioHabilitado = usuarioHabilitado;
    }
    

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public boolean isRol(ERol rol) {
        for (String sRol : this.getUsuarioDto().getRoles()) {
            if (sRol.equals(rol.toString())) {
                return true;
            }
        }
        return false;
    }

    public List<VPermisoGrupo> getOpcionesConPermiso() {
        return opcionesConPermiso;
    }

    public void setOpcionesConPermiso(List<VPermisoGrupo> opcionesConPermiso) {
        this.opcionesConPermiso = opcionesConPermiso;
    }

}
