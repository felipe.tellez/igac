package co.gov.igac.snc.web.jobs;

import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * Job de prueba para validar la ejecución de los procesos disparados a través del sistema de cron
 *
 * @author juan.mendez
 *
 */
public class MyRunnableJob implements Runnable, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(MyRunnableJob.class);

    // @Resource private SpringService myService;
    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
