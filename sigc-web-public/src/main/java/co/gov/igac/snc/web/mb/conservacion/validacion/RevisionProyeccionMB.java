package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.swing.ImageIcon;

import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.DocumentoResultadoPPRedios;
import co.gov.igac.snc.util.EIdTabRevisionProyeccion;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.TreeMap;
import org.primefaces.context.RequestContext;

/**
 * Managed bean para la revisión de proyección en validación
 *
 * @author juan.agudelo
 */
/* ¨
 * @modified by leidy.gonzalez Se modifica init para btener la lista de actividades cuando la
 * activdad a mostrar es GenerarResolucion
 */
@Component("revisionProyeccion")
@Scope("session")
public class RevisionProyeccionMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -214953148147449428L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RevisionProyeccionMB.class);

    // --------------------------------- Servicios ----------------------------
    @Autowired
    private IContextListener contexto;

    @Autowired
    private InformacionCancelacionInscripcionMB informacionCancelacionInscripcionMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    // --------------------------------- Variables ----------------------------
    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * Trámite seleccionado
     */
    private Tramite tramiteSeleccionado;

    /**
     * Valor de desción para aprobar o rechazar el trámite en esta instancia
     */
    private String tramiteAprobado;

    /**
     * Motivo del rechazo del trámite
     */
    private String motivoRechazo;

    /**
     * Lista con los datos resumen de la cancelación o inscripción
     */
    private List<String[]> tablaResumenTramite;

    /**
     * Documento resultado y lista de PPredios asociados a la proyección del trámite
     */
    private DocumentoResultadoPPRedios drp;

    /**
     * Tramite con el contenido del texto de la resolucion
     */
    private TramiteTextoResolucion tramiteTextoResolucion;

    /**
     * Imagen incial del predio antes del trámite
     */
    private String imagenInicial;

    /**
     * Imagen del predio despues del trámite
     */
    private String imagenFinal;

    /**
     * Lista que contiene la información del histórico de obsevaciones del trámite
     */
    private List<TramiteEstado> tablaHistoricoObservaciones;

    /**
     * Observaciónes para devolver el trámite al ejecutor
     */
    private TramiteEstado observacionesDeDevolucionDeTramite;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de resolución
     */
    private ReporteDTO reporteResolucion;

    // ------------- Banderas de edicion --------------------------------------
    /**
     * Bandera que indica si el trámite no es aprobado
     */
    private boolean banderaTramiteDevuelto;

    /**
     * Bandera que indica si el funcionario es el coordinador
     */
    private boolean banderaUsuarioCoordinador;

    /**
     * Bandera que indica si el funcionario es el jefe de conservación
     */
    private boolean banderaUsuarioRConservacion;

    /**
     * Bandera asociada a la desición de aprobación
     */
    private boolean banderaAprobacionRevision;

    /**
     * Bandera para habilitar o deshabilitar el envio del ejecutor.
     */
    private boolean banderaHabilitarEnvioEjecutor;

    /**
     * Bandera que indica si la observación es de modificación temporal
     */
    private boolean banderaObservacionTemporal;

    /**
     * Bandera que indica si es necesario mostrar el visor GIS
     */
    private boolean banderaVerVisorGIS;

    private int anchoImagen;
    private int altoImagen;

    /**
     * Bandera para la pestaña comparativo geografico
     */
    private boolean tieneComparativoGeografico;

    // ------------------- Interfaces de Servicio ------------------------------
    /**
     * Acceso al arbol
     *
     * @author fredy.wilches
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private String activityId;
    /**
     * Actividad de procesos
     */
    private Actividad actividad;

    private List<Actividad> actividades;

    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;
    /**
     * Nombre de las variables necesarias para llamar al editor geográfico para mostrar el
     * comparativo geografico.
     */
    private String parametroEditorGeograficoControlCalidad;

    /**
     * Lista de usuarios con el rol de responsable de conservación
     */
    private List<UsuarioDTO> responsablesConservacion;

    // -------------------- Métodos --------------------------------------------
    public boolean isBanderaHabilitarEnvioEjecutor() {
        return banderaHabilitarEnvioEjecutor;
    }

    public void setBanderaHabilitarEnvioEjecutor(boolean banderaHabilitarEnvioEjecutor) {
        this.banderaHabilitarEnvioEjecutor = banderaHabilitarEnvioEjecutor;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public String getParametroEditorGeograficoControlCalidad() {
        return parametroEditorGeograficoControlCalidad;
    }

    public void setParametroEditorGeograficoControlCalidad(
        String parametroEditorGeograficoControlCalidad) {
        this.parametroEditorGeograficoControlCalidad = parametroEditorGeograficoControlCalidad;
    }

    public boolean isBanderaUsuarioCoordinador() {
        return this.banderaUsuarioCoordinador;
    }

    public boolean isBanderaAprobacionRevision() {
        return this.banderaAprobacionRevision;
    }

    public void setBanderaAprobacionRevision(boolean banderaAprobacionRevision) {
        this.banderaAprobacionRevision = banderaAprobacionRevision;
    }

    public boolean isBanderaObservacionTemporal() {
        return this.banderaObservacionTemporal;
    }

    public void setBanderaObservacionTemporal(boolean banderaObservacionTemporal) {
        this.banderaObservacionTemporal = banderaObservacionTemporal;
    }

    public void setBanderaUsuarioCoordinador(boolean banderaUsuarioCoordinador) {
        this.banderaUsuarioCoordinador = banderaUsuarioCoordinador;
    }

    public boolean isBanderaUsuarioRConservacion() {
        return this.banderaUsuarioRConservacion;
    }

    public void setBanderaUsuarioRConservacion(boolean banderaUsuarioJConservacion) {
        this.banderaUsuarioRConservacion = banderaUsuarioJConservacion;
    }

    public boolean isBanderaVerVisorGIS() {
        return this.banderaVerVisorGIS;
    }

    public void setBanderaVerVisorGIS(boolean banderaVerVisorGIS) {
        this.banderaVerVisorGIS = banderaVerVisorGIS;
    }

    public Tramite getTramiteSeleccionado() {
        return this.tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getImagenInicial() {
        return this.imagenInicial;
    }

    public List<TramiteEstado> getTablaHistoricoObservaciones() {
        return this.tablaHistoricoObservaciones;
    }

    public void setTablaHistoricoObservaciones(
        List<TramiteEstado> tablaHistoricoObservaciones) {
        this.tablaHistoricoObservaciones = tablaHistoricoObservaciones;
    }

    public void setImagenInicial(String imagenInicial) {
        this.imagenInicial = imagenInicial;
    }

    public String getImagenFinal() {
        return this.imagenFinal;
    }

    public void setImagenFinal(String imagenFinal) {
        this.imagenFinal = imagenFinal;
    }

    public String getTramiteAprobado() {
        return this.tramiteAprobado;
    }

    public List<String[]> getTablaResumenTramite() {
        return this.tablaResumenTramite;
    }

    public TramiteTextoResolucion getTramiteTextoResolucion() {
        return this.tramiteTextoResolucion;
    }

    public void setTramiteTextoResolucion(TramiteTextoResolucion tramiteTextoResolucion) {
        this.tramiteTextoResolucion = tramiteTextoResolucion;
    }

    public void setTablaResumenTramite(List<String[]> tablaResumenTramite) {
        this.tablaResumenTramite = tablaResumenTramite;
    }

    public void setTramiteAprobado(String tramiteAprobado) {
        this.tramiteAprobado = tramiteAprobado;
    }

    public boolean isBanderaTramiteDevuelto() {
        return this.banderaTramiteDevuelto;
    }

    public void setBanderaTramiteDevuelto(boolean banderaTramiteDevuelto) {
        this.banderaTramiteDevuelto = banderaTramiteDevuelto;
    }

    public String getMotivoRechazo() {
        return this.motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    public ReporteDTO getReporteResolucion() {
        return reporteResolucion;
    }

    public void setReporteResolucion(ReporteDTO reporteResolucion) {
        this.reporteResolucion = reporteResolucion;
    }

    public int getAnchoImagen() {
        return anchoImagen;
    }

    public void setAnchoImagen(int anchoImagen) {
        this.anchoImagen = anchoImagen;
    }

    public int getAltoImagen() {
        return altoImagen;
    }

    public void setAltoImagen(int altoImagen) {
        this.altoImagen = altoImagen;
    }

    public boolean isTieneComparativoGeografico() {
        return tieneComparativoGeografico;
    }

    public void setTieneComparativoGeografico(boolean tieneComparativoGeografico) {
        this.tieneComparativoGeografico = tieneComparativoGeografico;
    }

    //--------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("entra al init de RevisionProyeccionMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        if (this.actividad == null) {
            this.actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (this.actividades != null && !this.actividades.isEmpty()) {
                for (Actividad actTemp : this.actividades) {
                    this.actividad = actTemp;
                }
            }
        }

        this.activityId = this.actividad.getId();

        //felipe.cadena::#15723::07-01-2016::Se reclama manualmente la actividad desde process
        try {
            this.getProcesosService().reclamarActividad(activityId, this.usuario);
        } catch (ExcepcionSNC e) {
            //this.addMensajeError(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.toString());
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico() + e +
                this.actividad.getIdObjetoNegocio(), e);
        }

        this.tramiteSeleccionado = this.getTramiteService()
            .buscarTramiteTraerDocumentoResolucionPorIdTramite(
                this.actividad.getIdObjetoNegocio());

        List<TramiteDocumento> documentosPruebas = this.getTramiteService()
            .obtenerTramiteDocumentosDePruebasPorTramiteId(
                this.tramiteSeleccionado.getId());
        if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
            this.tramiteSeleccionado.setDocumentosPruebas(documentosPruebas);
        }
        //@modified by leidy.gonzalez:: #18312:: 25/07/2016:: Se unifica en un metodo la carga de 
        // datos de la proyeccion.
        cargarInformacionProyeccion();
    }

    //----------------------------------------------------------------------------------------
    /**
     * busca en los documentos del tramite si existen imagenes de edicion geografica si no los hay
     * baja la bandera para mostrar la pestaña de comparativo geografico
     *
     * @modified juanfelipe.garcia 27-05-2013 cambio en la Arquitectura documental
     */
    private void validarComparativoGeografico() {
        this.tieneComparativoGeografico = false;

        String imagenAntes = null;
        String imagenDespues = null;

        if (this.tramiteSeleccionado.isTramiteGeografico()) {
            List<Documento> documentosImagenes = this
                .getConservacionService().buscarImagenesTramite(
                    this.tramiteSeleccionado);

            if (documentosImagenes != null) {
                for (Documento d : documentosImagenes) {

                    if (d.getIdRepositorioDocumentos() != null && d.getArchivo() != null) {

                        String rutaPreview = this.getGeneralesService().
                            descargarArchivoAlfrescoYSubirAPreview(d.getIdRepositorioDocumentos());

                        if (d.getTipoDocumento()
                            .getId()
                            .equals(ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE
                                .getId())) {
                            imagenAntes = rutaPreview;

                        }
                        if (d.getTipoDocumento()
                            .getId()
                            .equals(ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE
                                .getId())) {
                            imagenDespues = rutaPreview;
                        }
                    }
                }
            }
            if (imagenAntes != null) {
                ImageIcon ii;
                java.net.URL imgURL;
                try {
                    imgURL = new URL(imagenAntes);
                    if (imgURL != null) {
                        ii = new ImageIcon(imgURL, "");
                        this.anchoImagen = ii.getIconWidth();
                        this.altoImagen = ii.getIconHeight();
                        double radio = (double) this.anchoImagen / this.altoImagen;
                        this.anchoImagen = 900;
                        this.altoImagen = (int) (this.anchoImagen / radio);
                    }
                } catch (MalformedURLException ex) {
                    LOGGER.error("error obteniendo la url de la imagen 'antes': " + ex.getMessage(),
                        ex);
                }
            }
        }
        if (imagenAntes != null && imagenDespues != null) {
            this.tieneComparativoGeografico = true;
        }

    }

// --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar de tab en ver detalle de proyección
     *
     * @modified juanfelipe.garcia 24-05-2013 cambio en la Arquitectura documental
     */
    public void cambioTab(TabChangeEvent tce) {

        if (tce != null) {
            if (tce.getTab()
                .getId()
                .equals(EIdTabRevisionProyeccion.INFORMACION_CANCELACION_INSCRIPCION
                    .getId())) {
                this.informacionCancelacionInscripcionMB
                    .setTramiteSeleccionado(this.tramiteSeleccionado);
                this.informacionCancelacionInscripcionMB.init();
            } else if (tce.getTab().getId()
                .equals(EIdTabRevisionProyeccion.TRAMITES.getId())) {

            } else if (tce.getTab().getId()
                .equals(EIdTabRevisionProyeccion.VER_PROYECCION.getId())) {
            } else if (tce.getTab().getId()
                .equals(EIdTabRevisionProyeccion.MOTIVACION.getId())) {
                this.tramiteTextoResolucion = this.getTramiteService()
                    .buscarTramiteTextoResolucionPorIdTramite(
                        this.tramiteSeleccionado.getId());
            } else if (tce.getTab().getId().equals(EIdTabRevisionProyeccion.VISOR_GIS.getId())) {

                if (this.tramiteSeleccionado.isTramiteGeografico()) {
                    List<Documento> documentosImagenes = this.getConservacionService().
                        buscarImagenesTramite(this.tramiteSeleccionado);
                    for (Documento d : documentosImagenes) {
                        String rutaPreview = this.getGeneralesService().
                            descargarArchivoAlfrescoYSubirAPreview(d.getIdRepositorioDocumentos());

                        if (d.getTipoDocumento().getId().equals(
                            ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE.getId())) {
                            this.imagenInicial = rutaPreview;
                        }
                        if (d.getTipoDocumento().getId().equals(
                            ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId())) {
                            this.imagenFinal = rutaPreview;
                        }
                    }
                    if (imagenInicial != null) {
                        ImageIcon ii;
                        java.net.URL imgURL;
                        try {
                            imgURL = new URL(this.imagenInicial);
                            if (imgURL != null) {
                                ii = new ImageIcon(imgURL, "");
                                this.anchoImagen = ii.getIconWidth();
                                this.altoImagen = ii.getIconHeight();
                                double radio = (double) this.anchoImagen / this.altoImagen;
                                this.anchoImagen = 900;
                                this.altoImagen = (int) (this.anchoImagen / radio);
                            }
                        } catch (MalformedURLException e) {
                            LOGGER.error(
                                "error obteniendo la url de la imagen inicial en RevisionProyeccionMB#cambioTab: " +
                                 e.getMessage(), e);
                        }
                    }
                }
            } else if (tce.getTab().getId()
                .equals(EIdTabRevisionProyeccion.VER_RESOLUCION.getId())) {

                if (this.reporteResolucion == null) {

                    // Generación del documento de la resolución
                    generarReporteResolucion();
                }
            }

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para inicializar los datos de la tabla de resumen de trámite
     */
    /**
     * @documented pedro.garcia
     * @modified pedro.garcia 02-08-2012 Se aplica esta nueva regla para el cálculo de los datos de
     * la tabla:
     *
     * OJO: esto aplica para los trámites que no son de segunda
     *
     * para propietario y predios cancelación ->(se tiene en cuenta) C, M, null inscripción ->(se
     * tiene en cuenta) I, M, null
     *
     * para área terreno, área construcción, avalúo cancelación ->(se revisa) Predio inscripción
     * ->(se revisa) PPredio
     */
    /*
     * @modified by juanfelipe.garcia :: 18-09-2014 :: refs#9475 se adición validación para tramites
     * de segunda con ficha matriz para no tener en cuenta el predio ficha matriz en el cálculo de
     * áreas y avalúos
     */
    private void cargarDatosTablaResumenTramite() {

        this.tablaResumenTramite = new ArrayList<String[]>();

        String[] cancelaciones = new String[6];
        String[] inscripciones = new String[6];

        cancelaciones[0] = "Cancelaciones";
        inscripciones[0] = "Inscripciones";

        int cantidadPrediosCancelados = 0;
        int cantidadPrediosInscritos = 0;

        double areaTerrenoPCancelados = 0.0;
        double areaTerrenoPInscritos = 0.0;

        double areaConstruccionPCancelados = 0.0;
        double areaConstruccionPInscritos = 0.0;

        double avaluoPCancelados = 0.0;
        double avaluoPInscritos = 0.0;

        int propietariosCancelados = 0;
        int propietariosInscritos = 0;

        Predio tempPredio = null;
        boolean rural = false;

        if (this.drp.getPprediosTramite() != null && !this.drp.getPprediosTramite().isEmpty()) {

            if (!this.tramiteSeleccionado.isSegunda()) {
                for (PPredio ppredio : this.drp.getPprediosTramite()) {

                    //D: obtención datos número propietarios
                    for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {

                        if (null == pppTemp.getCancelaInscribe()) {
                            propietariosInscritos++;
                            propietariosCancelados++;
                        } else {
                            if (pppTemp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                propietariosCancelados++;
                            } else if (pppTemp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                                propietariosInscritos++;
                            } else if (pppTemp.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                propietariosInscritos++;
                                propietariosCancelados++;
                            }
                        }
                    }

                    //D: obtención datos número predios
                    if (null == ppredio.getCancelaInscribe()) {
                        cantidadPrediosCancelados++;
                        cantidadPrediosInscritos++;
                    } else {
                        if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                            cantidadPrediosCancelados++;
                        } else if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                            cantidadPrediosInscritos++;
                        } else if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                            cantidadPrediosCancelados++;
                            cantidadPrediosInscritos++;
                        }
                    }

                    //D: obtención datos área terreno, área construcción, autoavalúo
                    //D: para los 'C' se toman los datos del Predio.
                    //N: esto sirve porque para estos trámites el id del PPredio es el mismo del Predio
                    tempPredio = this.getConservacionService().obtenerPredioPorId(ppredio.getId());
                    if (tempPredio != null) {
                        areaTerrenoPCancelados =
                            areaTerrenoPCancelados + tempPredio.getAreaTerreno();
                        areaConstruccionPCancelados =
                            areaConstruccionPCancelados + tempPredio.getAreaConstruccion();
                        if (tempPredio.getAvaluoCatastral() != null) {
                            avaluoPCancelados = avaluoPCancelados + tempPredio.getAvaluoCatastral();
                        }
                    }

                    //D: para los 'I' se toman los datos del PPredio
                    areaTerrenoPInscritos = areaTerrenoPInscritos + ppredio.getAreaTerreno();
                    if (ppredio.getCancelaInscribe() == null ||
                        (ppredio.getCancelaInscribe() != null &&
                        (ppredio.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.
                            getCodigo()) ||
                         ppredio.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.
                            getCodigo())))) {
                        areaConstruccionPInscritos =
                            areaConstruccionPInscritos + ppredio.getAreaConstruccion();
                    }
//TODO :: fredy.wilches :: verificar si es correcto que un pppredio en un trámite de quinta (u otro tipo de trámite)n tenga este campo en null
                    // avaluoPInscritos = avaluoPInscritos + (ppredio.getAvaluoCatastral()==null?0:ppredio.getAvaluoCatastral());

                    // #11138 :: david.cifuentes :: Ajuste en la consulta del
                    // avaluo de los predios inscritos para traer el avalúo de
                    // la última vigencia registrada.
                    double valorAvaluo = 0.0;
                    if (ppredio.getAvaluoCatastral() != null) {

                        List<PPredioAvaluoCatastral> avaluosPredio = this.getConservacionService().
                            obtenerListaPPredioAvaluoCatastralPorIdPPredio(ppredio.getId());
                        if (avaluosPredio != null &&
                             !avaluosPredio.isEmpty() &&
                             avaluosPredio.get(0) != null &&
                             avaluosPredio.get(0).getValorTotalAvaluoCatastral() != null) {
                            valorAvaluo = avaluosPredio.get(0).getValorTotalAvaluoCatastral();
                        } else {
                            valorAvaluo = ppredio.getAvaluoCatastral();
                        }
                    }
                    avaluoPInscritos = avaluoPInscritos + valorAvaluo;

                    if (ppredio.isPredioRural()) {
                        rural = true;
                    }
                }
            } //D: para los trámites de englobe o desenglobe aplican otras reglas para obtener los
            //   datos de la tabla resumen de proyección.
            else if (this.tramiteSeleccionado.isSegunda()) {

                //D: Reglas para calcular los datos para desenglobe.
                // Cancelación:
                // La cantidad de predios cancelados siempre es 1.
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'C' o 'M'
                // El área de terreno, área de construcción y avalúo corresponden al dato del predio original
                //
                // Inscripción:
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'I' o 'M'
                // El número de predios es la SUM de los PPredios marcados con 'I' o 'M'
                // El área de terreno, área de construcción y avalúo se obtienen de la SUM de esos
                // datos de los PPredio marcados con 'I' o 'M'
                if (this.tramiteSeleccionado.isSegundaDesenglobe()) {

                    //D: en desenglobe, el campro 'predio' de la tabla 'Tramite' guarda el predio inicial
                    tempPredio = this.tramiteSeleccionado.getPredio();
                    cantidadPrediosCancelados = 1;

                    if (tempPredio != null) {

                        areaTerrenoPCancelados = tempPredio.getAreaTerreno();
                        areaConstruccionPCancelados = tempPredio.getAreaConstruccion();
                        avaluoPCancelados = tempPredio.getAvaluoCatastral();

                        for (PPredio ppredio : this.drp.getPprediosTramite()) {

                            if (ppredio.getCancelaInscribe() == null ||
                                (ppredio.getCancelaInscribe() != null && (ppredio.
                                getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                ppredio.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.MODIFICA.getCodigo())) &&
                                 !ppredio.isEsPredioFichaMatriz())) {

                                if (!ppredio.isEsPredioFichaMatriz()) {
                                    cantidadPrediosInscritos++;
                                }
                                areaTerrenoPInscritos += ppredio.getAreaTerreno();
                                areaConstruccionPInscritos += ppredio.getAreaConstruccion();
//TODO :: fredy.wilches :: definir qué pasa cuando este campo llega en null :: pedro.garcia
                                if (ppredio.getAvaluoCatastral() != null) {
                                    avaluoPInscritos += ppredio.getAvaluoCatastral();
                                }
                            }

                            /* El conteo de los propietarios cancelados se debe hacer en el predio
                             * principal.
                             */
                            if (tempPredio.getId().longValue() == ppredio.getId().longValue()) {

                                if (ppredio.getCancelaInscribe() != null && ppredio.
                                    getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                    for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {
                                        if (null == pppTemp.getCancelaInscribe() || pppTemp.
                                            getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                            propietariosCancelados++;
                                        } else if (pppTemp.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                            propietariosCancelados++;
                                        }
                                    }
                                } else if (ppredio.getCancelaInscribe() == null || ppredio.
                                    getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                    ppredio.getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                    for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {
                                        if (null == pppTemp.getCancelaInscribe() || pppTemp.
                                            getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                            propietariosCancelados++;
                                            if (!ppredio.isEsPredioFichaMatriz()) {
                                                propietariosInscritos++;
                                            }
                                        } else if (pppTemp.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                            propietariosCancelados++;
                                        } else if (pppTemp.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                                            propietariosInscritos++;
                                        }
                                    }
                                }
                            } else {
                                for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {

                                    if (ppredio.getCancelaInscribe() == null || ppredio.
                                        getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                        ppredio.getCancelaInscribe().equals(
                                            EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

                                        if (null == pppTemp.getCancelaInscribe() || pppTemp.
                                            getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                                            pppTemp.getCancelaInscribe().equals(
                                                EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                                            if (!ppredio.isEsPredioFichaMatriz()) {
                                                propietariosInscritos++;
                                            }
                                        }
                                    }
                                }
                            }
                            if (ppredio.isPredioRural()) {
                                rural = true;
                            }
                        }
                    }

                } //D: Reglas para calcular los datos para englobe.
                // Cancelación:
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'C'
                // La cantidad de predios cancelados es la SUM de PPredios marcados con 'C' más -si
                // lo hay- el PPredio marcado con 'M' (hay 0 o 1. 1 si se mantiene el número predial)
                // El área de terreno, área de construcción y avalúo se obtienen de la SUM de esos
                // datos de los Predio originales
                //
                // Inscripción:
                // El número de propietarios es la SUM de los PPredioPersona marcados con 'I' o 'M'
                // El número de predios inscritos es siempre 1
                // El área de terreno, área de construcción y avalúo se obtienen del PPredio (que
                // puede estar marcado con 'I' o 'M')
                else if (this.tramiteSeleccionado.isSegundaEnglobe()) {

                    cantidadPrediosInscritos = 1;

                    for (PPredio ppredio : this.drp.getPprediosTramite()) {

                        if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.CANCELA.getCodigo()) ||
                            ppredio.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

                            cantidadPrediosCancelados++;
                        }

                        //D: debería se solo un PPredio y estar con 'M' o 'I'
                        if (ppredio.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                            ppredio.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

                            areaTerrenoPInscritos += ppredio.getAreaTerreno();
                            areaConstruccionPInscritos += ppredio.getAreaConstruccion();
                            avaluoPInscritos += ppredio.getAvaluoCatastral();
                        }

                        for (PPersonaPredio pppTemp : ppredio.getPPersonaPredios()) {

//TODO :: fredy.wilches :: 21-08-2012 :: por ahora él definió que se sumaran a los cancelados, pero es error que ese campo esté en null :: pedro.garcia
                            if (null == pppTemp.getCancelaInscribe()) {
                                propietariosCancelados++;
                                LOGGER.error(
                                    "El campo 'cancelaInscribe del PPersonaPredio con id " +
                                     pppTemp.getId() + " está en null");
                            } else {
                                if (pppTemp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                    propietariosCancelados++;
                                } else if (pppTemp.getCancelaInscribe().equals(
                                    EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) ||
                                    pppTemp.getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
                                    propietariosInscritos++;
                                }
                            }
                        }
                    }

                    //D: se obtienen los Predios relacionados con el trámite. Estos están en la tabla
                    //  Tramite_Detalle_Predio
                    ArrayList<TramiteDetallePredio> prediosOriginalesTPE;
                    prediosOriginalesTPE = (ArrayList<TramiteDetallePredio>) this.
                        getTramiteService().buscarPredioEnglobesPorTramite(
                            this.getTramiteSeleccionado().getId());

                    if (prediosOriginalesTPE != null) {
                        for (TramiteDetallePredio tdp : prediosOriginalesTPE) {
                            areaTerrenoPCancelados += tdp.getPredio().getAreaTerreno();
                            areaConstruccionPCancelados += tdp.getPredio().getAreaConstruccion();
                            avaluoPCancelados += tdp.getPredio().getAvaluoCatastral();
                        }
                    } else {
                        LOGGER.error("En desenglobe con id de trámite " +
                            this.getTramiteSeleccionado().getId() +
                            " dio que los predios originales son null");
                    }
                }
            }
        }

        cancelaciones[1] = cantidadPrediosCancelados + "";

        //rurales
        if (rural) {
            int[] areaTerreno = Utilidades.convertirAreaAformatoHectareas(areaTerrenoPCancelados);
            int[] areaConstruc = Utilidades.convertirAreaAformatoHectareas(
                areaConstruccionPCancelados);
            cancelaciones[2] = areaTerreno[0] + "  Ha  " + Utilidades.
                redondearNumeroADosCifrasDecimales(areaTerreno[1]) + " m2 ";
            cancelaciones[3] = areaConstruc[0] + "  Ha  " + areaConstruc[1] + " m2 ";
        } else {
            cancelaciones[2] =
                Utilidades.redondearNumeroADosCifrasDecimales(areaTerrenoPCancelados) + " m2";
            cancelaciones[3] = areaConstruccionPCancelados + " m2";
        }

        NumberFormat nf = new DecimalFormat("#.##");
        cancelaciones[4] = nf.format(avaluoPCancelados) + "";
        cancelaciones[5] = propietariosCancelados + "";

        inscripciones[1] = cantidadPrediosInscritos + "";

        //rurales
        if (rural) {
            int[] areaTerrenoP = Utilidades.convertirAreaAformatoHectareas(areaTerrenoPInscritos);
            int[] areaConstrucP = Utilidades.convertirAreaAformatoHectareas(
                areaConstruccionPInscritos);
            inscripciones[2] = areaTerrenoP[0] + " Ha  " + Utilidades.
                redondearNumeroADosCifrasDecimales(areaTerrenoP[1]) + " m2";
            inscripciones[3] = areaConstrucP[0] + " Ha  " + areaConstrucP[1] + " m2";
        } else {
            inscripciones[2] =
                Utilidades.redondearNumeroADosCifrasDecimales(areaTerrenoPInscritos) + " m2 " + "";
            inscripciones[3] = areaConstruccionPInscritos + " m2 " + "";
        }

        inscripciones[4] = nf.format(avaluoPInscritos) + "";
        inscripciones[5] = propietariosInscritos + "";

        //D: la fila de cancelaciones no se muestra cuando el trámite es de quinta omitido
        if (!this.tramiteSeleccionado.isQuintaOmitido()) {
            this.tablaResumenTramite.add(cancelaciones);
        }

        //D: la fila de inscripciones se muestra si el trámite no es de cancelación de predio
        if (!this.tramiteSeleccionado.isCancelacionPredio()) {
            this.tablaResumenTramite.add(inscripciones);
        }

    }

    /**
     * Método que verifica si dentro de la lista de predios del trámite se encuentra el predio ficha
     * matriz.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public long verificarIdPredioFichaMatriz() {
        long idPPredioFichaMatriz = 0;

        // Revisar si el predio del trámite tiene condición de
        // propiedad ocho o nueve, de ser así se debe restar el
        // predio ficha matriz del conteo total de predios
        // inscritos.
        List<PPredio> pPrediosDesenglobe = this.getConservacionService()
            .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                this.tramiteSeleccionado.getId());
        for (PPredio p : pPrediosDesenglobe) {
            if (p.isEsPredioFichaMatriz()) {
                idPPredioFichaMatriz = p.getId().longValue();
            }
        }
        return idPPredioFichaMatriz;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para guardar las observaciones de la proyección para devolverlo al funcionario
     * ejecutor
     */
    public boolean guardarObservaciones() {

        if (this.tramiteAprobado.equals(ESiNo.NO.getCodigo()) &&
             this.motivoRechazo != null && !this.motivoRechazo.isEmpty()) {

            actualizarObservaciones();

            String mensaje = "Las observaciones se guardaron exitosamente";
            this.addMensajeInfo(mensaje);
            return true;
        } else {

            String mensaje = null;
            if (this.tramiteAprobado.equals(ESiNo.SI.getCodigo())) {
                mensaje = "Usted está tratando de devolver el trámite" +
                     " pero el valor de decisión de aprobación es SI" +
                     ", por favor corrija los datos e intente nuevamente";
            } else if (this.tramiteAprobado.equals(ESiNo.NO.getCodigo()) &&
                 (this.motivoRechazo == null || this.motivoRechazo
                    .isEmpty())) {
                mensaje = "Usted está tratando de guardar una observación sobre el trámite pero" +
                     " el contenido de este es vacío" +
                     ", por favor corrija los datos e intente nuevamente";
            }
            this.addMensajeError(mensaje);
            return false;
        }

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para guardar las observaciones
     */
    public void guardarObservacionesTemp() {

        if (this.motivoRechazo != null &&
             this.motivoRechazo.trim().length() > 0) {
            try {
                String mensaje = "Las observaciones se guardaron exitosamente";
                actualizarObservaciones();
                this.addMensajeInfo(mensaje);
            } catch (Exception e) {
                String mensaje = "No se pudo realizar la operación";
                LOGGER.error(mensaje + " " + e.getMessage(), e);
                this.addMensajeError(mensaje);
            }
            this.banderaHabilitarEnvioEjecutor = true;
        } else if (!this.banderaAprobacionRevision) {
            String mensaje = "No se ingresó un motivo de rechazo a guardar";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para actualizar las observaciones de la proyección para devolverlo al funcionario
     * ejecutor
     *
     * @ modified felipe.cadena - 08.10.2013 - Se agrega funcionalidad para mantener una observacion
     * temporal.
     */
    public void actualizarObservaciones() {

        if (this.drp.getObservacionTemporal() == null) {
            if (this.observacionesDeDevolucionDeTramite == null) {
                this.observacionesDeDevolucionDeTramite = new TramiteEstado();
            }

            this.observacionesDeDevolucionDeTramite.setMotivo(this.motivoRechazo);
            this.observacionesDeDevolucionDeTramite
                .setTramite(this.tramiteSeleccionado);
            this.observacionesDeDevolucionDeTramite
                .setEstado(ETramiteEstado.ESPERANDO_CONFIRMAR_DEVOLUCION
                    .getCodigo());
            this.observacionesDeDevolucionDeTramite.setResponsable(usuario
                .getLogin());

            this.observacionesDeDevolucionDeTramite = this.getTramiteService()
                .actualizarTramiteEstado(
                    this.observacionesDeDevolucionDeTramite, this.usuario);
            this.drp.setObservacionTemporal(this.observacionesDeDevolucionDeTramite);
        } else {
            if (this.tramiteAprobado.equals(ESiNo.NO.getCodigo())) {

                if (this.motivoRechazo == null || "".equals(this.motivoRechazo)) {
                    this.addMensajeError("Si el trámite es rechazado debe ingresarse un motivo");
                    return;
                }
                this.drp.getObservacionTemporal().setMotivo(this.motivoRechazo);
                this.observacionesDeDevolucionDeTramite = this.getTramiteService()
                    .actualizarTramiteEstado(this.drp.getObservacionTemporal(),
                        this.usuario);
            } else {
                this.getTramiteService().eliminarTramiteEstado(observacionesDeDevolucionDeTramite);
            }
        }

        this.banderaTramiteDevuelto = true;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para guardar envíar al responsable de conservación
     */
    public String enviarARConservacion() {

        boolean validarDatosEnviar = validacionDatosEnvioProyeccion();

        if (validarDatosEnviar == true) {

            this.tramiteSeleccionado.setEstadoGdb(null);
            this.getTramiteService().actualizarTramite(this.tramiteSeleccionado);

            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setUsuarios(this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario
                        .getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION));

            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION);

            this.getProcesosService().avanzarActividad(this.usuario, activityId,
                solicitudCatastral);
            return this.cerrar();
        } else {
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que valida los datos ingresados al enviár una proyección
     */
    private boolean validacionDatosEnvioProyeccion() {
        //Se cambia cancela/inscribe de las unidades construccion y
        //de las zonas de los tramites de Englobe Virtual antes de Generar la Resolucion
        if (this.tramiteSeleccionado.isEnglobeVirtual()) {

            //Modifica Unidades Construccion de la Ficha Antigua
            List<PUnidadConstruccion> unidadConstruccionOriginalesEnglobeVirtual = this.
                getConservacionService()
                .buscarUnidConstOriginalesCanceladasPorTramite(
                    this.tramiteSeleccionado.getId());

            if (unidadConstruccionOriginalesEnglobeVirtual != null &&
                 !unidadConstruccionOriginalesEnglobeVirtual.isEmpty()) {

                for (PUnidadConstruccion pUnidadConstTemp
                    : unidadConstruccionOriginalesEnglobeVirtual) {

                    pUnidadConstTemp.setCancelaInscribe(null);

                }

                this.getConservacionService()
                    .guardarListaPUnidadConstruccion(unidadConstruccionOriginalesEnglobeVirtual);

            }

            //Modifica Zonas con cancela/inscribe en T
            List<PPredioZona> zonasTemp = this.getConservacionService().
                buscarZonasTempPorIdTramite(this.tramiteSeleccionado.getId());

            if (zonasTemp != null &&
                 !zonasTemp.isEmpty()) {

                for (PPredioZona zonaTemp : zonasTemp) {

                    zonaTemp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());

                }

                this.getConservacionService().guardarListaPPredioZona(zonasTemp);

            }

        }

        if (this.tramiteAprobado.equals(ESiNo.SI.getCodigo()) &&
             (this.motivoRechazo == null || (this.motivoRechazo != null && this.motivoRechazo
                .isEmpty()))) {
            return true;
        } else {

            String mensaje = null;
            if (this.tramiteAprobado.equals(ESiNo.NO.getCodigo())) {
                mensaje = "Usted está tratando de enviar el trámite" +
                     " pero el valor de decisión de aprobación es NO" +
                     ", por favor corrija los datos e intente nuevamente";
            } else if (this.tramiteAprobado.equals(ESiNo.SI.getCodigo()) &&
                 (this.motivoRechazo != null && !this.motivoRechazo
                    .isEmpty())) {
                mensaje = "Usted está tratando de enviar el trámite" +
                     " pero este contiene observaciones de devolución" +
                     ", por favor corrija los datos e intente nuevamente";
            }
            this.addMensajeError(mensaje);
            return false;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para guardar envíar generar resolución al funcionario responsable de conservación
     */
    public String enviarGenerarResolucion() {

        boolean validarDatosEnviar = validacionDatosEnvioProyeccion();

        //@modified by leidy.gonzalez::#40166::Determina los predios modificados en un tramite de
        //desenglobe por etapas
        if (this.tramiteSeleccionado.isDesenglobeEtapas()) {
            this.determinarPrediosModificadosDesenglobePorEtapas();
        }

        if (validarDatosEnviar == true) {

            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();

            this.tramiteSeleccionado.setEstadoGdb(null);
            this.getTramiteService().actualizarTramite(this.tramiteSeleccionado);

            if (this.tramiteSeleccionado.getSolicitud().isViaGubernativa() &&
                 (this.tramiteSeleccionado.getTipoTramite().equals(
                    ETramiteTipoTramite.RECURSO_DE_QUEJA.getCodigo()) || this.tramiteSeleccionado
                .getTipoTramite().equals(
                    ETramiteTipoTramite.RECURSO_DE_APELACION
                        .getCodigo()))) {

                solicitudCatastral.setUsuarios(this.getTramiteService()
                    .buscarFuncionariosPorRolYTerritorial(
                        this.usuario.getDescripcionTerritorial(),
                        ERol.DIRECTOR_TERRITORIAL));

            } else {
                solicitudCatastral
                    .setUsuarios(this
                        .getTramiteService()
                        .buscarFuncionariosPorRolYTerritorial(
                            this.usuario
                                .getDescripcionEstructuraOrganizacional(),
                            ERol.RESPONSABLE_CONSERVACION));
            }

            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION);

            this.getProcesosService().avanzarActividad(this.usuario, activityId,
                solicitudCatastral);
            return this.cerrar();
        } else {
            return null;
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para guardar envíar generar resolución al funcionario ejecutor
     */
    /*
     * modified by javier.aponte :: refs#16791 :: Cuando el trámite es devuelto, por el coordinador
     * o por el responsable de conservación, al ejecutor no debe registrar en el campo ESTADO_GDB de
     * la tabla trámite :: 21/04/2016
     */
    public String enviarEjecutor() {

        this.drp.getObservacionTemporal().setEstado(ETramiteEstado.DEVUELTO_REVISION_PROYECCION.
            getCodigo());
        this.drp.getObservacionTemporal().setMotivo(this.motivoRechazo);

        /*
         * javier.aponte :: refs#13943 :: En la tabla TRAMITE_ESTADO adicional al nombre del usuario
         * se debe marcar el rol que tiene en el momento de realizar la devolucion del tramite por
         * control de calidad :: 26/01/2016
         */
        if (this.banderaUsuarioCoordinador) {
            this.drp.getObservacionTemporal().setRolResponsable(ERol.COORDINADOR.toString());
        } else {
            this.drp.getObservacionTemporal().setRolResponsable(ERol.RESPONSABLE_CONSERVACION.
                toString());
        }

        this.observacionesDeDevolucionDeTramite = this.getTramiteService()
            .actualizarTramiteEstado(this.drp.getObservacionTemporal(),
                this.usuario);

        if (this.tramiteSeleccionado.getFuncionarioEjecutor() == null) {
            this.addMensajeError("El trámite no tiene usuario ejecutor");
            return null;
        }

        boolean resultado = guardarObservaciones();
        if (resultado) {

            this.getTramiteService().actualizarTramite(this.tramiteSeleccionado);

            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();

            usuarios.add(this.getGeneralesService().getCacheUsuario(
                this.tramiteSeleccionado.getFuncionarioEjecutor()));

            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setUsuarios(usuarios);

            solicitudCatastral
                .setTransicion(
                    ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA);

            this.getProcesosService().avanzarActividad(this.usuario, activityId,
                solicitudCatastral);
            return this.cerrar();
        } else {
            return null;
        }

    }

    public String cerrar() {
        this.tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean("infoCancelacionInscripcion");
        UtilidadesWeb.removerManagedBean("revisionProyeccion");
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Generar reporte resolución
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte #7249 se genera la resolución sin marca de agua 11/03/2014
     */
    public void generarReporteResolucion() {

        EReporteServiceSNC enumeracionReporte = this.getTramiteService().
            obtenerUrlReporteResoluciones(this.tramiteSeleccionado.getId());

        if (enumeracionReporte != null) {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID",
                String.valueOf(this.tramiteSeleccionado.getId()));
            parameters.put("COPIA_USO_RESTRINGIDO", "false");

            UsuarioDTO responsableConservacion = null;

            // Se consulta el responsable de conservación asociado a la territorial
            if (this.responsablesConservacion == null) {
                this.responsablesConservacion = (List<UsuarioDTO>) this
                    .getTramiteService().buscarFuncionariosPorRolYTerritorial(this.usuario.
                        getDescripcionEstructuraOrganizacional(),
                        ERol.RESPONSABLE_CONSERVACION);
            }

            // Se captura el nombre del responsable de conservación para 
            // posteriormente enviar como parámetro.
            // Para el caso en que no lo encuentre (Éste caso no se debería presentar)
            // el reporte controlará que no se muestren valores nulos, en éste caso
            // se mostrará unicamente el rol y la territorial.
            if (this.responsablesConservacion != null &&
                 !this.responsablesConservacion.isEmpty() &&
                 this.responsablesConservacion.get(0) != null) {
                responsableConservacion = this.responsablesConservacion.get(0);
            }

            //Siempre debe existir un responsable de conservación porque es quien firma el documento
            //en caso que haya no se debe generar el reporte
            if (responsableConservacion != null && responsableConservacion.getNombreCompleto() !=
                null) {
                parameters.put("NOMBRE_RESPONSABLE_CONSERVACION", responsableConservacion.
                    getNombreCompleto());
            }

            if (this.tramiteSeleccionado.isTipoTramiteViaGubernativaModificado() ||
                this.tramiteSeleccionado.isTipoTramiteViaGubModSubsidioApelacion() ||
                 this.tramiteSeleccionado.isRecursoApelacion() || this.tramiteSeleccionado.
                isViaGubernativa()) {

                if (!this.tramiteSeleccionado.getNumeroRadicacionReferencia().isEmpty()) {

                    Tramite tramiteReferencia = this.getTramiteService().
                        buscarTramitePorNumeroRadicacion(this.tramiteSeleccionado.
                            getNumeroRadicacionReferencia());

                    if (tramiteReferencia != null) {
                        parameters.put("NOMBRE_SUBREPORTE_RESUELVE", this.getTramiteService().
                            obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.getId()));
                    }
                }

            }

            this.reporteResolucion = reportsService.generarReporte(
                parameters, enumeracionReporte, this.usuario);
        } else {
            LOGGER.error("No se pudo obtener el url " +
                 "en el servidor de reportes asociada a la resolución ");
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que actualiza la visualización de botones dependiendo de la decisión de aprobación de
     * la revisión
     */
    public void cambioDecisionAprobacion() {

        if (this.tramiteAprobado.equals(ESiNo.SI.getCodigo())) {
            this.banderaAprobacionRevision = true;
        } else if (this.tramiteAprobado.equals(ESiNo.NO.getCodigo())) {
            this.banderaAprobacionRevision = false;
        }

        this.motivoRechazo = "";
    }

    /**
     * Constante comparador para regla de navegacion
     *
     * @author andres.eslava
     *
     */

    public String constanteComparativo() {

        return "VISOR_COMPARATIVO_GEOGRAFICO";

    }

    public boolean isRevisarProyeccion() {
        return this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);
    }

    public boolean isRevisarProyeccionResolucion() {
        return this.actividad.getNombre().equals(
            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION);
    }

    /**
     * @author leidy.gonzalez
     *
     * #18312:: Método que carga la informacion necesaria de la proyeccion del tramite para
     * visualizar en la pagina
     */
    public void cargarInformacionProyeccion() {

        this.drp = this.getGeneralesService()
            .buscarDocumentoResultadoPorTramiteId(this.tramiteSeleccionado.getId());

        this.tramiteAprobado = ESiNo.NO.getCodigo();
        cargarDatosTablaResumenTramite();

        this.tablaHistoricoObservaciones = this.drp.getHistoricoObservaciones();

        if (this.drp.getObservacionTemporal() != null) {
            this.motivoRechazo = this.drp.getObservacionTemporal().getMotivo();
        }

        if (this.drp.getHistoricoObservaciones() != null &&
             !this.drp.getHistoricoObservaciones().isEmpty() &&
             this.drp.getHistoricoObservaciones().get(0).getMotivo() != null &&
             this.drp.getHistoricoObservaciones().get(0).getMotivo()
                .contains(Constantes.PALABRA_CLAVE_TMP_HISTORICO_OBS)) {
            // TODO: fredy.wilches :: 29/05/2012 :: Condicion adicionada por
            // FGWL pero debe revisarse si es necesaria :: gabriel.agudelo

            this.motivoRechazo = this.drp
                .getHistoricoObservaciones()
                .get(0)
                .getMotivo()
                .replace(Constantes.PALABRA_CLAVE_TMP_HISTORICO_OBS.toString(), "");

            this.drp.getHistoricoObservaciones()
                .get(0)
                .setMotivo(
                    this.drp.getHistoricoObservaciones()
                        .get(this.drp.getHistoricoObservaciones()
                            .size() - 1)
                        .getMotivo()
                        .replace(
                            Constantes.PALABRA_CLAVE_TMP_HISTORICO_OBS
                                .toString(), ""));

            this.observacionesDeDevolucionDeTramite = this.drp
                .getHistoricoObservaciones().get(0);

        }

        String[] roles = this.usuario.getRoles();

        this.banderaTramiteDevuelto = false;

        for (String r : roles) {
            if (r.equals(ERol.COORDINADOR.toString())) {
                this.banderaUsuarioCoordinador = true;
            } else if (r.equals(ERol.RESPONSABLE_CONSERVACION.toString())) {
                this.banderaUsuarioRConservacion = true;
            }
        }

        this.banderaHabilitarEnvioEjecutor = false;
        this.banderaVerVisorGIS = this.getTramiteService().verVisorGISPorTipoTramite(
            this.tramiteSeleccionado.getTipoTramite(),
            this.tramiteSeleccionado.getClaseMutacion(), this.tramiteSeleccionado.getSubtipo());

        this.validarComparativoGeografico();

        //inicializar parametros editor geografico
        this.parametroEditorGeografico = this.usuario.getLogin() + "," + this.tramiteSeleccionado.
            getId() +
            "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class, this.actividad.
                getNombre());

        //inicializar parametros editor geografico control de calidad
        this.parametroEditorGeograficoControlCalidad = this.usuario.getLogin() + "," +
            this.tramiteSeleccionado.getId() +
            "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class, this.actividad.
                getNombre()) + "_CONTROL_CALIDAD";

    }

    /**
     * Determina los predios modificados en un tramite de desenglobe por etapas
     *
     * @author leidy.gonzalez
     */
    private void determinarPrediosModificadosDesenglobePorEtapas() {

        // Se consultan los predios para actualizar si fueron modificados o no
        List<PPredio> prediosTramite = new ArrayList<PPredio>();

        if (this.tramiteSeleccionado.isEnglobeVirtual()) {

            String fichaNueva =
                this.tramiteSeleccionado.obtenerInfoCampoAdicional(
                    EInfoAdicionalCampo.NUEVA_FICHA_MATRIZ_EV);

            if (fichaNueva != null && !fichaNueva.isEmpty()) {

                prediosTramite = this.getConservacionService()
                    .buscarPPrediosDifFichaMatrizCon0Y5NoCancelados(this.tramiteSeleccionado.getId());

            } else {

                String fichaMatrizResultante =
                    this.tramiteSeleccionado.obtenerInfoCampoAdicional(
                        EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV);

                if (fichaMatrizResultante != null && !fichaMatrizResultante.isEmpty()) {

                    PFichaMatriz pFichaMatriz = this.getConservacionService().
                        obtenerFichaMatrizTramiteEV(
                            fichaMatrizResultante);

                    if (pFichaMatriz != null) {
                        prediosTramite = this.getConservacionService()
                            .buscarPPrediosModPorTramiteId(this.tramiteSeleccionado.getId(),
                                pFichaMatriz.getId());
                    }
                }

            }

        } else {
            prediosTramite = this.getConservacionService()
                .buscarPPrediosModPorTramiteId(this.tramiteSeleccionado.getId(),
                    this.tramiteSeleccionado.getPredioId());
        }

        //Predios que fueron modificados la construccion
        TreeMap<Long, Boolean> prediosModificados = this
            .getConservacionService().obtenerPredioModificadoPorTamite(
                this.tramiteSeleccionado.getId(),
                Constantes.TABLA_UNIDAD_CONSTRUCCION_PROYECTADA);

        // Se valida si el número predial del predio
        // fue modificado y en ese
        // caso también se establece en original trámite en 'NO' para el
        // p_predio
        TreeMap<String, Boolean> prediosConNumeroPredialModificado = this
            .getConservacionService()
            .obtenerPredioConNumeroPredialModificadoPorTamite(
                this.tramiteSeleccionado.getId());

        Boolean modificado;

        for (PPredio predio : prediosTramite) {

            modificado = prediosModificados.get(predio.getId());
            if (modificado != null && modificado.booleanValue()) {
                predio.setOriginalTramite(ESiNo.NO.getCodigo());
                this.getConservacionService().guardarPPredio(predio);
            } else {
                modificado = prediosConNumeroPredialModificado.get(predio
                    .getId().toString());
                if (modificado != null && modificado.booleanValue()) {
                    predio.setOriginalTramite(ESiNo.NO.getCodigo());
                    this.getConservacionService().guardarPPredio(predio);
                }
            }

        }
    }

}
