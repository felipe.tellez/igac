package co.gov.igac.snc.web.jobs;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

/**
 * Representa la tarea programada para la aplicacion de cambios
 *
 * @author andres.eslava
 */
public class VerificarJobsAplicarCambiosRunnableJob extends AbstractLocator implements Runnable,
    Serializable {

    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(
        VerificarJobsPendientesSNCRunnableJob.class);

    private TaskExecutor taskExecutor;
    private Long numeroJobs;

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void setNumeroJobs(Long cantidadJobs) {
        this.numeroJobs = cantidadJobs;
    }

//--------------------------------------------------------------------------------------------------	
    /**
     * Verifica los jobs para aplicar cambios geograficos y envia la solicitud al servidor
     * geografico, en donde ningun tramite coincida en manzana-
     *
     * @author andres.eslava
     */
    @Override
    public void run() {
        try {
            if (this.getGeneralesService().confirmarEjecucionJobAplicarCambios()) {
                List<ProductoCatastralJob> jobsPendientes = null;
                try {
                    jobsPendientes = this.getGeneralesService().obtenerJobsEsperandoUnoPorManzana(
                        this.numeroJobs);
                } catch (Exception e) {
                    throw new Exception("Error obteniendo los jobs pendientes por ejecutar");
                }
                try {
                    if (jobsPendientes != null && jobsPendientes.size() > 0) {
                        for (ProductoCatastralJob job : jobsPendientes) {
                            this.taskExecutor.execute(new AplicarCambiosJobTask(job));
                        }
                    } else {
                        LOGGER.warn(
                            "No se obtuvieron jobs pendientes para aplicar cambios geograficos");
                    }
                } catch (Exception e) {
                    throw new Exception("Error enviando la aplicacion de cambios geografica");
                }
            } else {
                LOGGER.warn(
                    "No se esta ejecutando el job de aplicar cambios, parametro en la DB EJECUTA_JOB_APLICAR_CAMBIOS es NO");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
