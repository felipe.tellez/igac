/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.util.Date;
import java.util.List;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import co.gov.igac.snc.persistence.util.EMovimientoAvaluoTipo;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 *
 * Managed bean del CU-SA-AC-154 Solicitar suspensión cancelación o ampliación de tiempos
 *
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-154
 */
@Component("solicitudCanSusAmplTiempos")
@Scope("session")
public class SolicitudSuspencionCancelacionAmpliacionDeTiemposMB extends
    SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(SolicitudSuspencionCancelacionAmpliacionDeTiemposMB.class);

    // --------------------------------Atributos--------------------------------
    /**
     * Avaluo al cual se le va a modificar los tiempos
     */
    private Avaluo avaluo;

    /**
     * solicitud que se está realizando a nivel de datos se ve como un {@link AvaluoMovimiento}
     */
    private AvaluoMovimiento movimiento;

    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    // --------------------------------Banderas---------------------------------
    /**
     * Determina si se está haciendo una suspención
     */
    private boolean banderaEsSuspencion;

    /**
     * Determina si se está realizando una cancelación
     */
    private boolean banderaEsCancelacion;

    /**
     * Determina si se está haciendo una ampliación de tiempo
     */
    private boolean banderaEsAmpliacionTiempos;

    /**
     * Determina si la suspención es de tipo personal
     */
    private boolean banderaEsSuspencionTemporal;

    // ----------------------------Métodos SET y GET----------------------------
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public AvaluoMovimiento getMovimiento() {
        return this.movimiento;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsSuspencion() {
        return this.banderaEsSuspencion;
    }

    public boolean isBanderaEsCancelacion() {
        return this.banderaEsCancelacion;
    }

    public boolean isBanderaEsAmpliacionTiempos() {
        return this.banderaEsAmpliacionTiempos;
    }

    public boolean isBanderaEsSuspencionTemporal() {
        return this.banderaEsSuspencionTemporal;
    }

    public void setBanderaEsSuspencionTemporal(
        boolean banderaEsSuspencionTemporal) {
        this.banderaEsSuspencionTemporal = banderaEsSuspencionTemporal;
    }

    // ---------------------------------Métodos---------------------------------
    /**
     * Inicializa el objeto de tipo {@link AvaluoMovimiento} con los datos necesarios según el tipo
     * de movimiento a realizar
     *
     * @author christian.rodriguez
     */
    public void inicializarMovimiento() {

        this.movimiento = new AvaluoMovimiento();
        this.movimiento.setAvaluo(this.avaluo);

        if (this.banderaEsSuspencion) {
            this.movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.SUSPENCION
                .getCodigo());
            this.movimiento.setDiasAmpliacion(0L);
        }
        if (this.banderaEsCancelacion) {
            this.movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.CANCELACION
                .getCodigo());
            this.movimiento.setDiasAmpliacion(0L);
        }
        if (this.banderaEsAmpliacionTiempos) {
            this.movimiento.setTipoMovimiento(EMovimientoAvaluoTipo.AMPLIACION
                .getCodigo());
        }

        this.movimiento.setFecha(new Date());
        this.movimiento.setUsuarioMovimientoId(this.usuario.getLogin());

    }

    // -------------------------------------------------------------------------
    /**
     * Listener que prepara las bandera y datos necesarios para realizar una solicitud de suspención
     *
     * @author christian.rodriguez
     */
    public void cargarDialogoSuspencion() {
        this.banderaEsSuspencion = true;
        this.banderaEsCancelacion = false;
        this.banderaEsAmpliacionTiempos = false;
        this.banderaEsSuspencionTemporal = false;
        this.inicializarMovimiento();
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que prepara las bandera y datos necesarios para realizar una solicitud de
     * cancelación
     *
     * @author christian.rodriguez
     */
    public void cargarDialogoCancelacion() {
        this.banderaEsCancelacion = true;
        this.banderaEsSuspencion = false;
        this.banderaEsAmpliacionTiempos = false;
        this.banderaEsSuspencionTemporal = false;
        this.inicializarMovimiento();
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que prepara las bandera y datos necesarios para realizar una solicitud de ampliación
     * de tiempos
     *
     * @author christian.rodriguez
     */
    public void cargarDialogoAmpliacionTiempos() {
        this.banderaEsAmpliacionTiempos = true;
        this.banderaEsSuspencion = false;
        this.banderaEsCancelacion = false;
        this.banderaEsSuspencionTemporal = false;
        this.inicializarMovimiento();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que debe ser llamado para cargar los datos requeridos por este Caso de uso cuando es
     * llamado desde otros MB
     *
     * @author christian.rodriguez
     * @param secRadicado número sec radicado del avalúo
     */
    public void cargarDesdeOtrosMB(String secRadicado) {
        if (secRadicado != null && !secRadicado.isEmpty()) {
            this.avaluo = this.getAvaluosService()
                .consultarAvaluoPorSecRadicadoConAtributos(secRadicado);
        }
        this.usuario = MenuMB.getMenu().getUsuarioDto();
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón guardar del diálogo de creación de solicitudes de suspención, cancelación
     * y ampliamiento de tiempos para los tramites de avalúos
     *
     * @author christian.rodriguez
     */
    public void guardar() {
        if (this.movimiento != null) {

            if (this.movimiento.getTipoMovimiento().equalsIgnoreCase(
                EMovimientoAvaluoTipo.SUSPENCION.getCodigo()) &&
                 !this.banderaEsSuspencionTemporal) {
                this.movimiento.setFechaDesde(new Date());
                this.movimiento.setFechaHasta(null);
            }

            this.movimiento = this.getAvaluosService()
                .guardarActualizarAvaluoMovimiento(this.movimiento,
                    this.usuario);

            if (this.movimiento != null) {
                this.addMensajeInfo("Solicitud generada satisfactoriamente");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError("No se pudo generar la solicitud .");
                context.addCallbackParam("error", "error");
            }

            this.enviarNotificacionPorCorreoElectronico();
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Envia la notificación de la solicitud realizada a el director de la territorial o al
     * coordinador git dependiendo de si el avlauo es de cede central o no
     *
     * @author christian.rodriguez
     */
    private void enviarNotificacionPorCorreoElectronico() {

        EstructuraOrganizacional territorialAvaluo = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                String.valueOf(this.avaluo
                    .getEstructuraOrganizacionalCod()));

        List<UsuarioDTO> destinatarios = null;

        if (this.avaluo.getEstructuraOrganizacionalCod().equals(
            Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL)) {

            destinatarios = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    territorialAvaluo.getNombre(),
                    ERol.COORDINADOR_GIT_AVALUOS);

        } else {

            destinatarios = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    territorialAvaluo.getNombre(),
                    ERol.DIRECTOR_TERRITORIAL);
        }

        boolean notificacionExitosa = false;
        if (destinatarios != null) {

            notificacionExitosa = this
                .getAvaluosService()
                .enviarCorreoElectronicoSolicitudMovimientoAvaluo(
                    destinatarios.get(0), this.movimiento, this.usuario);
        }

        if (notificacionExitosa) {
            this.addMensajeInfo("Se envio exitosamente la notificación por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo enviar la notificación por correo electrónico.");
            context.addCallbackParam("error", "error");
        }
    }
}
