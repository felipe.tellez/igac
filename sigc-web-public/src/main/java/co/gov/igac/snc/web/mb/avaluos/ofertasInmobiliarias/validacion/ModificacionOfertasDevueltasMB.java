/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.validacion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOfertaOb;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.RegistroOfertaInmobiliariaMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-10 Modificar ofertas devueltas por contorl de calidad
 *
 * @author christian.rodriguez
 * @cu CU-TV-0F-10
 * @version 2.0
 *
 */
@Component("modOfertasDevueltas")
@Scope("session")
public class ModificacionOfertasDevueltasMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistroOfertaInmobiliariaMB.class);

    /**
     * Serial autogenerado
     */
    private static final long serialVersionUID = -6182205142379617591L;

    @Autowired
    private RegistroOfertaInmobiliariaMB registroOfertaInmobiliaria;

    @Autowired
    private IContextListener contextoWeb;

    @Autowired
    protected TareasPendientesMB tareasPendientesMB;

    private Actividad bpmActivityInstance;

    // --------Atributos-----------------
    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    private List<OfertaInmobiliaria> ofertasInmob;
    private OfertaInmobiliaria ofertaInmobSeleccionada;
    private List<ControlCalidadOfertaOb> ofertaSeleccionadaObs;

    /**
     * Id del Control de Calidad
     */
    private Long controlCalidadId;

    // ----------- Variables VISOR --------------------
    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Esta variable es para el parametro de las capas del mapa
     */
    private String moduleLayer;
    /**
     * Esta variable es para el parametro de las herramientas del mapa
     */
    private String moduleTools;
    /**
     * En esta variable se guardan los codigos de los predios asociados a la oferta para hacerles
     * zoom en el visor
     */
    private String prediosParaZoom;

    /**
     * Datos de la región de recolección
     */
    private Long regionId;

    private RegionCapturaOferta regionSeleccionada;

    /**
     * Usado para deshabilitar el botón de Enviar a control de calidad
     */
    private boolean banderaDeshabilitarBotones;

    // --------Getters y Setters---------
    public List<OfertaInmobiliaria> getOfertasInmob() {
        return ofertasInmob;
    }

    public void setOfertasInmob(List<OfertaInmobiliaria> ofertasInmob) {
        this.ofertasInmob = ofertasInmob;
    }

    public OfertaInmobiliaria getOfertaInmobSeleccionada() {
        return ofertaInmobSeleccionada;
    }

    public void setOfertaInmobSeleccionada(OfertaInmobiliaria ofertaInmobSeleccionada) {
        this.ofertaInmobSeleccionada = ofertaInmobSeleccionada;
    }

    public List<ControlCalidadOfertaOb> getOfertaSeleccionadaObs() {
        return ofertaSeleccionadaObs;
    }

    public void setOfertaSeleccionadaObs(List<ControlCalidadOfertaOb> ofertaSeleccionadaObs) {
        this.ofertaSeleccionadaObs = ofertaSeleccionadaObs;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public RegionCapturaOferta getRegionSeleccionada() {
        return regionSeleccionada;
    }

    public void setRegionSeleccionada(RegionCapturaOferta regionSeleccionada) {
        this.regionSeleccionada = regionSeleccionada;
    }

    public boolean isBanderaDeshabilitarBotones() {
        return banderaDeshabilitarBotones;
    }

    public void setBanderaDeshabilitarBotones(boolean banderaDeshabilitarBotones) {
        this.banderaDeshabilitarBotones = banderaDeshabilitarBotones;
    }

    // ------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on modOfertasDevueltas init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.inicializarVariablesVisorGIS();

        this.extraerRegionOfertasDelArbol();

        this.cargueDeOfertasInmobiliarias();

    }

    // ------------------------------------------------------------
    /**
     * Inicializa las variables requeridas por el VISOR GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        // se pone en of para que muestre la capa del mapa de ofertas
        // inmobiliarias
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        // no se pone en of porque en este caso de uso no hay permiso para
        // edicion
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";
    }

    // ------------------------------------------------------------
    /**
     * Define los criterios que tiene la consulta para traer las ofertas
     *
     * @author christian.rodriguez
     *
     * @modified rodrigo.hernandez 14-06-2012 Se cambia la función para cargar la lista de ofertas
     * de un recolector
     */
    private void cargueDeOfertasInmobiliarias() {

        this.ofertasInmob = this.getAvaluosService()
            .buscarOfertasDevueltasPorIdRecolectorIdControlCalidadIdRegion(
                this.regionSeleccionada.getAsignadoRecolectorId(), this.controlCalidadId,
                this.regionSeleccionada.getId());
    }

    /**
     * Extrae la información relacionada con la región de recolección según lo seleccionado en el
     * arbol de procesos
     *
     * @author christian.rodriguez
     */
    private void extraerRegionOfertasDelArbol() {

        this.bpmActivityInstance = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.regionId = Long.valueOf(this.bpmActivityInstance.getIdRegion());

        this.controlCalidadId = Long.valueOf(this.bpmActivityInstance.getIdCorrelacion());

        this.regionSeleccionada = this.getAvaluosService().buscarRegionCapturaOfertaPorId(
            this.regionId);

    }

    // ------------------------------------------------------------
    /**
     * Abre la página de registro/modificación de ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @modified rodrigo.hernandez se realiza llamado a metodo de RegistroOfertaInmobiliaria para
     * asignar region y recolector a la oferta q se vaya a crear
     *
     * @return le URL del caso de uso de registro de ofertas inmobiliarias
     */
    public String registrarModificarOferta() {

        this.registroOfertaInmobiliaria.init();

        this.registroOfertaInmobiliaria.setBanderaCreacionOfertaDesdeModOfertasDevueltas(true);

        this.registroOfertaInmobiliaria.precargaDatosCapturaOferta(null, this.usuario.getLogin());

        UtilidadesWeb.removerManagedBean("modOfertasDevueltas");

        return "/avaluos/ofertasInmob/registroOfertaInmobiliaria.jsf";
    }

    // ------------------------------------------------------------
    /**
     * Carga de base de datos las observaciones de control de calidad asociadas a la oferta
     * seleccionada
     *
     * @author christian.rodriguez
     */
    public void cargarObservacionesOfertaSeleccionada() {

        if (this.ofertaInmobSeleccionada != null) {

            Long ofertaID = this.ofertaInmobSeleccionada.getId();
            this.ofertaSeleccionadaObs = this.getAvaluosService().buscarObservacionesPorOfertaId(
                ofertaID);
        }

    }

    // ------------------------------------------------------------
    /**
     * Método encargado de mover las ofertas a control de calidad
     *
     * @author christian.rodriguez
     */
    public void enviarACalidad() {
        this.doDatabaseStatesUpdate();
        this.forwardProcess();
    }

    /**
     * Mueve el proceso a la siguiente transición (Modificar ofertas devueltas)
     *
     * @author christian.rodriguez
     */
    private void forwardProcess() {

        String idActividad = this.bpmActivityInstance.getId();

        ControlCalidad controlCalidadBPM = this.crearObjetoDeNegocio();

        try {

            this.getProcesosService().avanzarActividad(idActividad, controlCalidadBPM);
            this.addMensajeInfo("Ofertas enviadas a control de calidad");
            this.banderaDeshabilitarBotones = true;

        } catch (Exception ex) {

            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo enviar las ofertas a control de calidad");
            context.addCallbackParam("error", "error");

            LOGGER.error(
                "Error moviendo actividad con id " + idActividad + " a la " + "actividad " +
                 controlCalidadBPM.getTransicion() + " : " + ex.getMessage());

        }
    }

    /**
     * Actualiza el estado de las ofertas modificadas a CONTROL CALIDAD
     *
     * @author christian.rodriguez
     */
    private void doDatabaseStatesUpdate() {

        for (OfertaInmobiliaria oferta : this.ofertasInmob) {
            oferta.setEstado(EOfertaEstado.CONTROL_CALIDAD.getCodigo());
        }

        this.getAvaluosService().guardarActualizarListaOfertaInmobiliaria(this.ofertasInmob);

        List<ControlCalidadOferta> listaOfertasControlCalidadRecolector =
            new ArrayList<ControlCalidadOferta>();

        listaOfertasControlCalidadRecolector = this.getAvaluosService()
            .buscarOfertasControlDevueltasPorIdRecolectorIdControlCalidad(
                this.regionSeleccionada.getAsignadoRecolectorId(), this.controlCalidadId);

        for (ControlCalidadOferta controlCalidadOferta : listaOfertasControlCalidadRecolector) {
            controlCalidadOferta.setAprobado(null);
        }

        this.getAvaluosService().guardarActualizarListaControlCalidadOferta(
            listaOfertasControlCalidadRecolector);

    }

    /**
     * Crea el objeto de negocio para avanzar la actividad en el process
     *
     * @author christian.rodriguez
     * @return objeto de negocio con los valores necesarios para avanzar la actividad
     */
    private ControlCalidad crearObjetoDeNegocio() {

        ControlCalidad controlCalidadBPM = new ControlCalidad();

        Departamento departamento;
        Municipio municipio;
        DivisionAdministrativa departamentoBPM, municipioBPM;

        departamento = this.regionSeleccionada.getAreaCapturaOferta().getDepartamento();
        municipio = this.regionSeleccionada.getAreaCapturaOferta().getMunicipio();

        departamentoBPM = new DivisionAdministrativa(departamento.getCodigo(),
            departamento.getNombre());
        municipioBPM = new DivisionAdministrativa(municipio.getCodigo(), municipio.getNombre());

        controlCalidadBPM.setIdentificador(this.regionSeleccionada.getId());
        controlCalidadBPM.setIdRegion(String.valueOf(this.regionSeleccionada.getId()));
        controlCalidadBPM.setIdCorrelacion(this.controlCalidadId);
        controlCalidadBPM.setDepartamento(departamentoBPM);
        controlCalidadBPM.setMunicipio(municipioBPM);
        controlCalidadBPM.setTransicion(
            ProcesoDeControlCalidad.ACT_VALIDACION_REALIZAR_CONTROL_CALIDAD_MUESTRAS);
        controlCalidadBPM.setUsuarios(this.buscarInvestigadorMercadosTerritorial());
        controlCalidadBPM.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        controlCalidadBPM.setFuncionario(this.usuario.getNombreCompleto());

        return controlCalidadBPM;
    }

    /**
     * Buscar el investigador de mercado de la territorial del recolector
     *
     * @author christian.rodriguez
     * @return lista de usuarios de tipo investigador de mercado asociado a la territorial
     */
    private List<UsuarioDTO> buscarInvestigadorMercadosTerritorial() {

        List<UsuarioDTO> usuarios = this.getGeneralesService().obtenerFuncionarioTerritorialYRol(
            this.usuario.getCodigoEstructuraOrganizacional(), ERol.INVESTIGADOR_MERCADO);

        return usuarios;

    }

    // ------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la página, elimina los managed bean de registro de foertas y de
     * modificación de ofertas devueltas
     *
     * @author christian.rodriguez
     * @return destino de la navegación
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

}
