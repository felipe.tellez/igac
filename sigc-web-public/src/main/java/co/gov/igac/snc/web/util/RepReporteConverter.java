package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.util.Constantes;

/**
 * Converter para reemplazar un RepReporte.
 *
 * @author david.cifuentes
 *
 */
public class RepReporteConverter implements Converter {

    @Implement
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String objectAsString) {

        RepReporte answer;
        String[] objectAttributes;

        answer = new RepReporte();
        objectAttributes = objectAsString.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setId(new Long(objectAttributes[0]));
        answer.setCategoria(objectAttributes[1]);
        answer.setNombre(objectAttributes[2]);
        answer.setDescripcion(objectAttributes[3]);

        return answer;
    }

    @Implement
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        RepReporte objDT = (RepReporte) o;
        String answer = objDT.toString();
        return answer;
    }
}
