package co.gov.igac.snc.web.mb.generales;

import org.springframework.stereotype.Component;

import co.gov.igac.snc.web.util.SNCManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lorena.salamanca Este ManagedBean se invoca automáticamente cuando se genera un error en
 * el sistema
 */
@Component("errorHandler")
@RequestScoped
public class ErrorHandlerMB extends SNCManagedBean {

    public String getStatusCode() {
        String val = String.valueOf(
            (Integer) FacesContext.getCurrentInstance().getExternalContext().
                getRequestMap().get("javax.servlet.error.status_code"));
        return val;
    }

    public String getMessage() {
        String val = (String) FacesContext.getCurrentInstance().getExternalContext().
            getRequestMap().get("javax.servlet.error.message");
        return val;
    }

    public String getExceptionType() {
        String val = FacesContext.getCurrentInstance().getExternalContext().
            getRequestMap().get("javax.servlet.error.exception_type").toString();
        return val;
    }

    public String getException() {
        String val = (String) ((Exception) FacesContext.getCurrentInstance().getExternalContext().
            getRequestMap().get("javax.servlet.error.exception")).toString();
        return val;
    }

    public String getRequestURI() {
        return (String) FacesContext.getCurrentInstance().getExternalContext().
            getRequestMap().get("javax.servlet.error.request_uri");
    }

    public String getServletName() {
        return (String) FacesContext.getCurrentInstance().getExternalContext().
            getRequestMap().get("javax.servlet.error.servlet_name");
    }
}
