package co.gov.igac.snc.web.util;

import java.io.Serializable;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;

/**
 * Transporta el mensaje requerido para mover la actividad del proceso.
 *
 * @author fabio.navarrete
 *
 */
public class ActivityMessageDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3520057816348709194L;

    /**
     * Observaciones del mensaje.
     */
    private String comment;
    /**
     * Id de la Actividad.
     */
    private String activityId;

    /**
     * Objeto de negocio definido por equipo de procesos. Si se requiere usarlo hablar con los
     * encargados de procesos.
     */
    private SolicitudCatastral solicitudCatastral;

    /**
     * Identificador de la transición a la cual va el proceso.
     */
    private String transition;

    /**
     * Usuario al cual se asignará la siguiente actividad del proceso.
     *
     * @deprecated se usa el del objeto SolicitudCatastral
     */
    private String user;

    /**
     * Usuario que tiene asignada la actividad actualmente del proceso.
     */
    private UsuarioDTO usuarioActual;

    /**
     * Void Constructor
     */
    public ActivityMessageDTO() {
    }

    /**
     * full constructor
     *
     * @param comment
     * @param activityId
     * @param solicitudCatastral
     * @param transition
     * @param user
     * @param advancedActivity
     */
    public ActivityMessageDTO(String comment, String activityId,
        SolicitudCatastral solicitudCatastral, String transition,
        String user) {
        super();
        this.comment = comment;
        this.activityId = activityId;
        this.solicitudCatastral = solicitudCatastral;
        this.transition = transition;
        this.user = user;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public SolicitudCatastral getSolicitudCatastral() {
        return solicitudCatastral;
    }

    public void setSolicitudCatastral(SolicitudCatastral solicitudCatastral) {
        this.solicitudCatastral = solicitudCatastral;
    }

    public String getTransition() {
        return transition;
    }

    public void setTransition(String transicion) {
        this.transition = transicion;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String usuario) {
        this.user = usuario;
    }

    public UsuarioDTO getUsuarioActual() {
        return usuarioActual;
    }

    public void setUsuarioActual(UsuarioDTO usuarioActual) {
        this.usuarioActual = usuarioActual;
    }

}
