package co.gov.igac.snc.web.util;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.web.mb.generales.GeneralMB;

/**
 * Converter para traer el mostrar el código de un municipio concatenado con su nombre apartir del
 * código del municipio.
 *
 * @author david.cifuentes
 *
 */
public class MunicipioCodigoNombreConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(MunicipioCodigoNombreConverter.class);

    private GeneralMB generalMB = (GeneralMB) UtilidadesWeb
        .getManagedBean("general");

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1,
        String valorString) {

        try {
            return valorString;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " +
                 valorString);
            return valorString;
        }
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1,
        Object valorObjeto) {

        List<Municipio> municipios = this.generalMB.getMunicipios(valorObjeto
            .toString().substring(0, 2));
        String codigoNombreMunicipio = null;
        for (Municipio mcpio : municipios) {
            if (valorObjeto.equals(mcpio.getCodigo())) {
                codigoNombreMunicipio = mcpio.getCodigo3Digitos() + " - " +
                     mcpio.getNombre();
            }
        }
        try {
            return codigoNombreMunicipio;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " +
                 valorObjeto);
            return (String) valorObjeto;
        }
    }
}
