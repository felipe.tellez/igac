/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.util.List;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace las veces de managed bean para la consulta del detalle del bloqueo de una persona.
 *
 * El CU dice que recibe el número de identificación de la persona, pero aquí, por la interacción
 * con los otros CU se recibe el id de la persona directamente (lo cual hace más eficiente la
 * consulta); incluso se tiene el objeto Persona completo, por lo que se evita hacer la búsqueda de
 * esos datos.
 *
 * @author pedro.garcia
 */
/*
 * @modified juan.agudelo
 */
@Component("consultaBloqueoPersona")
@Scope("session")
public class ConsultaBloqueoPersonaMB extends SNCManagedBean {

    private static final long serialVersionUID = 8947642269056589533L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaBloqueoPersonaMB.class);

    /**
     * Objeto que tiene la info del bloqueo de la persona
     */
    private List<PersonaBloqueo> personaBloqueos;

    private Predio predioForPersonaBloqueo;
    private Persona persona;
    private String idPersona;

    private boolean existeBloqueoPersona;

    /**
     * url del documento de soporte del bloqueo de una persona
     */
    private String urlDocumentoBloqueo;

    //-------  methods   -----------------------------------------------------------------
    public String getUrlDocumentoBloqueo() {
        return this.urlDocumentoBloqueo;
    }

    public void setUrlDocumentoBloqueo(String urlDocumentoBloqueo) {
        this.urlDocumentoBloqueo = urlDocumentoBloqueo;
    }

    public List<PersonaBloqueo> getPersonaBloqueos() {
        return this.personaBloqueos;
    }

    public void setPersonaBloqueos(List<PersonaBloqueo> personaBloqueos) {
        this.personaBloqueos = personaBloqueos;
    }

    public Predio getPredioForPersonaBloqueo() {
        return this.predioForPersonaBloqueo;
    }

    public void setPredioForPersonaBloqueo(Predio predio) {
        this.predioForPersonaBloqueo = predio;
    }

    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(String idPersona) {

        this.idPersona = idPersona;
        if (idPersona != null) {
            this.init();
        }
    }

    public boolean isExisteBloqueoPersona() {
        return existeBloqueoPersona;
    }

    public void setExisteBloqueoPersona(boolean existeBloqueoPersona) {
        this.existeBloqueoPersona = existeBloqueoPersona;
    }

//--------------------------------------------------------------------------------------------------
    //@PostConstruct
    public void init() {
        LOGGER.debug("init de ConsultaBloqueoPersonaMB");

        ConsultaPropietariosMB mbConsPropietarios = (ConsultaPropietariosMB) UtilidadesWeb
            .getManagedBean("consultaPropietarios");

        this.predioForPersonaBloqueo = mbConsPropietarios.getPredio();
        this.persona = mbConsPropietarios.getPersona();

        if (this.persona != null) {
            this.personaBloqueos = this.getConservacionService()
                .obtenerPersonaBloqueosPorPersonaId(this.persona.getId());
            if (this.personaBloqueos != null && !this.personaBloqueos.isEmpty()) {

                this.existeBloqueoPersona = true;
            } else {
                this.existeBloqueoPersona = false;
            }
        }

    }

//--------------------------------------------------------------------------------------------------
    @PreDestroy
    public void kill() {
        LOGGER.debug("killing ConsultaBloqueoPersonaMB...");
    }

}
