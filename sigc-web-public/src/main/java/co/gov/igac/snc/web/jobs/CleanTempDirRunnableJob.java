package co.gov.igac.snc.web.jobs;

import java.io.File;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;

public class CleanTempDirRunnableJob implements Runnable, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(CleanTempDirRunnableJob.class);

    // @Resource private SpringService myService;
    @Override
    public void run() {
        try {
            FileUtils.cleanDirectory(new File(FileUtils.getTempDirectory().getAbsolutePath()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            service.limpiarWorkspacePreview();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
