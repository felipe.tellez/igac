package co.gov.igac.snc.web.util;

/**
 * Enumeracion para los posibles valores del los atributos de la sesion http.
 *
 * @author felipe.cadena
 */
public enum EAtributosSesion {

    ID_CONEXION("ID_CONEXION"),
    SPRING_SECURITY_CONTEXT("SPRING_SECURITY_CONTEXT"),
    SNC_DESKTOP("EDITOR SNC"),
    USUARIO_RECURRENTE("USUARIO_RECURRENTE");

    private String valor;

    private EAtributosSesion(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
