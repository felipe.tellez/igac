package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * ManagedBean para el caso de uso 082 de avalúos.CU-SA-AC-082 Administrar Pagos del Contrato del
 * Avaluador Externo {@link ProfesionalAvaluo}
 *
 * @author christian.rodriguez
 *
 */
@Component("adminPagosContratoAvaluador")
@Scope("session")
public class AdminPagosContratoAvaluadorExternoMB extends SNCManagedBean
    implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdminPagosContratoAvaluadorExternoMB.class);

    /**
     * Lista con los pagos proyectados del contrato y avaluador seleccionados
     */
    private List<ProfesionalContratoPago> pagosProyectados;

    /**
     * Lista con los pagos causados del contrato y avaluador seleccionados
     */
    private List<ProfesionalContratoPago> pagosCausados;

    /**
     * Avaluador seleccionado
     */
    private ProfesionalAvaluo avaluador;

    /**
     * Contrato del avaluador seleccionado
     */
    private ProfesionalAvaluosContrato contrato;

    /**
     * Pago seleccionado para edición ó adición
     */
    private ProfesionalContratoPago pagoSeleccionado;

    /**
     * Lista de avalúos sin proyección de pago
     */
    private List<Avaluo> avaluosPago;

    /**
     * Avalúo seleccionado para edición
     */
    private Avaluo avaluoSeleccionado;

    /**
     * Avalúo seleccionados para proyectar en el pago
     */
    private Avaluo[] avaluosSeleccionados;

    /**
     * Usuario actual
     */
    private UsuarioDTO usuario;

    // --------------------------------Banderas---------------------------------
    /**
     * Indica si se deben habilitar las opciones de edición o no
     */
    private boolean banderaModoSoloLectura;
    // ----------------------------Métodos SET y GET----------------------------

    public List<ProfesionalContratoPago> getPagosProyectados() {
        return this.pagosProyectados;
    }

    public List<ProfesionalContratoPago> getPagosCausados() {
        return this.pagosCausados;
    }

    public ProfesionalAvaluo getAvaluador() {
        return this.avaluador;
    }

    public ProfesionalAvaluosContrato getContrato() {
        return this.contrato;
    }

    public ProfesionalContratoPago getPagoSeleccionado() {
        return this.pagoSeleccionado;
    }

    public void setPagoSeleccionado(ProfesionalContratoPago pagoSeleccionado) {
        this.pagoSeleccionado = pagoSeleccionado;
    }

    public List<Avaluo> getAvaluosPago() {
        return this.avaluosPago;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public Avaluo[] getAvaluosSeleccionados() {
        return this.avaluosSeleccionados;
    }

    public void setAvaluosSeleccionados(Avaluo[] avaluosSeleccionados) {
        this.avaluosSeleccionados = avaluosSeleccionados;
    }

    public boolean isBanderaModoSoloLectura() {
        return this.banderaModoSoloLectura;
    }

    // ---------------------------------Métodos---------------------------------
    // --------------------------------------------------------------------------
    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado por otros managedBeans
     *
     * @author christian.rodriguez
     * @param contrato {@link ProfesionalAvaluosContrato } contrato del avaluador
     * @param idAvaluador id del avaluador {@link ProfesionalAvaluo} asociado al contrato
     */
    public void cargarDesdeOtrosMB(ProfesionalAvaluosContrato contrato,
        Long idAvaluador, boolean modoSoloLectura) {

        this.banderaModoSoloLectura = modoSoloLectura;

        this.contrato = this.getAvaluosService()
            .obtenerProfesionalAvaluoContratoPorIdConAtributos(
                contrato.getId());

        this.avaluador = this.getAvaluosService()
            .obtenerProfesionalAvaluoPorIdConAtributos(idAvaluador);

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.consultarPagosProyectadosCausados();

    }

    // --------------------------------------------------------------------------
    /**
     * Método que consulta los pagos proyectados y causados asociados al contrato
     *
     * @author christian.rodriguez
     * @param contrato
     */
    private void consultarPagosProyectadosCausados() {
        this.pagosCausados = this.getAvaluosService()
            .consultarProfesionalAvaluoContratoPagosCausadosPorIdContrato(
                this.contrato.getId());
        this.pagosProyectados = this
            .getAvaluosService()
            .consultarProfesionalAvaluoContratoPagosProyectadosPorIdContrato(
                this.contrato.getId());
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el dialogo para adición de un pago
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaProyeccionPago() {

        this.pagoSeleccionado = new ProfesionalContratoPago();
        this.consultarAvaluosSinPagos();
        this.avaluosSeleccionados = new Avaluo[0];
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el dialogo para edicion de un pago. Carga los avaluos disponibles para
     * asociar al pago, carga los avaluos asociados al pago y los marca como seleccionados
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaEdicionPago() {

        if (this.contrato != null && this.avaluador != null &&
             this.pagoSeleccionado != null) {

            this.consultarAvaluosSinPagos();

            List<Avaluo> avaluosAsociadoAPagoSeleccionado = this
                .cargarAvaluosAsociadosAlPago();

            // Se hace esto para que en la tabla aparezcan seleccionados los
            // pagos que ya están asociados a el pago
            this.avaluosPago.addAll(avaluosAsociadoAPagoSeleccionado);

            this.avaluosSeleccionados = (Avaluo[]) avaluosAsociadoAPagoSeleccionado
                .toArray(new Avaluo[0]);
        }
    }

    // --------------------------------------------------------------------------
    /**
     * método que consulta los avaluos asociados al pago seleccionado
     *
     * @author christian.rodriguez
     */
    private List<Avaluo> cargarAvaluosAsociadosAlPago() {
        if (this.pagoSeleccionado != null) {
            return this.getAvaluosService()
                .consultarAvaluosPorProfesionalContratoPagoId(
                    this.pagoSeleccionado.getId());
        }
        return null;
    }

    // ----------------------------------------------------------------------
    /**
     * Método que consulta los avaluos disponibles para asociar al pago
     *
     * @author christian.rodriguez
     *
     */
    private void consultarAvaluosSinPagos() {
        if (this.contrato != null && this.avaluador != null) {
            this.avaluosPago = this.getAvaluosService()
                .consultarAvaluosSinPagoPorContratoYAvaluador(
                    this.contrato.getId(), this.avaluador.getId());
        } else {
            this.avaluosPago = new ArrayList<Avaluo>();
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón de guardar. Guarda el pago que se está proyectando
     *
     * @author christian.rodriguez
     */
    public void guardarPago() {
        if (this.contrato != null && this.avaluador != null &&
             this.pagoSeleccionado != null) {

            if (this.avaluosSeleccionados.length != 0) {

                // Inicio Valores quemados
                this.pagoSeleccionado.setValor(0.0);
                this.pagoSeleccionado.setValorPagarPension(0.0);
                this.pagoSeleccionado.setValorPagarSalud(0.0);
                this.pagoSeleccionado.setValorPagado(0.0);
                // Fin Valores quemados

                this.pagoSeleccionado = this.getAvaluosService()
                    .guardarActualizarProfesionalContratoPago(
                        this.pagoSeleccionado, this.contrato,
                        Arrays.asList(this.avaluosSeleccionados), this.usuario);

                if (this.pagoSeleccionado != null) {

                    this.consultarPagosProyectadosCausados();

                    this.addMensajeInfo("Pago " +
                         this.pagoSeleccionado.getConsecutivoPago() +
                         " proyectado satisfactoriamente.");

                } else {
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");

                    this.addMensajeError("No fue posible proyectar el pago.");
                }
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeError(
                    "Debe seleccionar por lo menos un Sec. Radicado para asociar a la  " +
                     " Proyección de Pago,  intente nuevamente.");
            }
        }

    }

    public void onSelectAvaluo(SelectEvent event) {
        Avaluo avaluo = (Avaluo) event.getObject();
    }
}
