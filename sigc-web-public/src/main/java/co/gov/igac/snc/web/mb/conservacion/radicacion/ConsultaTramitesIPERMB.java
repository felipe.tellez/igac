package co.gov.igac.snc.web.mb.conservacion.radicacion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionada;
import co.gov.igac.snc.persistence.entity.i11n.InfInterrelacionadaRe;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesIper;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author fredy.wilches
 */
@Component("tramitesIperMB")
@Scope("session")
public class ConsultaTramitesIPERMB extends SNCManagedBean {

    private FiltroDatosConsultaTramitesIper datosConsultaPredio;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;

    /**
     * Orden apra la lista de Municipios
     */
    private EOrden ordenMunicipios = EOrden.CODIGO;

    /**
     * lista con los valores del combo de tipos de trámite que se permiten para el tipo de solicitud
     * creada
     */
    private ArrayList<SelectItem> tiposTramitePorSolicitud;

    /**
     * bandera para indicar si el subtipo de trámite es dato requerido
     */
    private boolean subtipoTramiteEsRequerido;
    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private List<InfInterrelacionadaRe> tramites;

    @PostConstruct
    public void init() {

        datosConsultaPredio = new FiltroDatosConsultaTramitesIper();
        this.departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorPais(
                Constantes.COLOMBIA);
        updateMunicipiosItemList();
        tiposTramitePorSolicitud = this.getTiposTramitePorSolicitud();

    }

    // GETTERS y SETTERS
    public List<SelectItem> getSubtipoClaseMutacionV() {
        return subtipoClaseMutacionV;
    }

    public List<InfInterrelacionadaRe> getTramites() {
        return tramites;
    }

    public void setTramites(List<InfInterrelacionadaRe> tramites) {
        this.tramites = tramites;
    }

    public FiltroDatosConsultaTramitesIper getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(
        FiltroDatosConsultaTramitesIper datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public boolean isSubtipoTramiteEsRequerido() {
        return subtipoTramiteEsRequerido;
    }

    public void setSubtipoTramiteEsRequerido(boolean subtipoTramiteEsRequerido) {
        this.subtipoTramiteEsRequerido = subtipoTramiteEsRequerido;
    }

    public void setTiposTramitePorSolicitud(
        ArrayList<SelectItem> tiposTramitePorSolicitud) {
        this.tiposTramitePorSolicitud = tiposTramitePorSolicitud;
    }

    public ArrayList<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    // LOGICA MB
    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.datosConsultaPredio.getDepartamentoId() != null) {
            this.municipios = new ArrayList<Municipio>();
            this.municipios = (ArrayList<Municipio>) this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.datosConsultaPredio.getDepartamentoId());

            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(this.municipios);
            } else {
                Collections.sort(this.municipios,
                    Municipio.getComparatorNombre());
            }

            for (Municipio municipio : this.municipios) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    this.municipiosItemList.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    this.municipiosItemList.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
            .add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    /**
     * listener del cambio en el combo de departamentos
     */
    public void onChangeDepartamento() {
        this.datosConsultaPredio.setMunicipioId(null);
        this.updateMunicipiosItemList();
        FacesMessage msg = new FacesMessage("departamento:" +
             this.datosConsultaPredio.getDepartamentoId());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Se consultan los tipos de tramites para el tipo de solicitud Tramite catastral
     *
     * @return
     */
    public ArrayList<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> answer = null;
        List<TipoSolicitudTramite> tiposTramite = null;

        tiposTramite = this.getTramiteService()
            .obtenerTiposDeTramitePorSolicitud("1"); //Tramites catastrales 

        if (tiposTramite != null && !tiposTramite.isEmpty()) {
            answer = new ArrayList<SelectItem>();
            for (TipoSolicitudTramite tst : tiposTramite) {
                answer.add(new SelectItem(tst.getTipoTramite(),
                    tst.getTipoTramiteValor()));
            }
        }
        return answer;
    }

    /**
     * Método listener para el cambio de tipo de trámite
     */
    public void cambioTipoTramite() {

        this.datosConsultaPredio.setClaseMutacion(null);
        this.datosConsultaPredio.setSubtipo(null);

    }

    public void cambioClaseMutacion() {

        // D: si es de segunda o quinta, se hace requerido escoger el subtipo de
        // trámite; y se pone el subtipo en el trámite seleccionado
        if (this.datosConsultaPredio.getClaseMutacion()
            .equals(EMutacionClase.SEGUNDA.getCodigo()) ||
             this.datosConsultaPredio.getClaseMutacion().equals(EMutacionClase.QUINTA.getCodigo())) {
            this.subtipoTramiteEsRequerido = true;
            this.subtipoClaseMutacionV = this.generalMB
                .getSubtipoClaseMutacionV(this.datosConsultaPredio.getClaseMutacion());
        } else {
            this.subtipoTramiteEsRequerido = false;
            this.datosConsultaPredio.setSubtipo(null);
        }
    }

    public boolean isMutacion() {
        if (this.datosConsultaPredio.getTipoTramite() != null && this.datosConsultaPredio.
            getTipoTramite().equals(ETramiteTipoTramite.MUTACION.getCodigo())) {
            return true;
        }
        return false;
    }

    public String cerrar() {
        UtilidadesWeb.removerManagedBean("tramitesIperMB");
        this.tareasPendientesMB.init();
        return "index";
    }

    public String buscar() {

        tramites = this.getInterrelacionService().getTramites(this.datosConsultaPredio);

        return null;
    }

}
