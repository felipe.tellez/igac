package co.gov.igac.snc.web.util;

/**
 *
 * @author juanfelipe.garcia
 */
public enum EActividadControlCalidad {
    DIGITALIZADOR("Digitalizador"),
    TOPOGRAFO("Topógrafo"),
    EJECUTOR("Ejecutor");

    private String nombre;

    EActividadControlCalidad(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
