package co.gov.igac.snc.web.mb.conservacion.productos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * ManagedBean para la consulta de la cantidad de registros prediales
 *
 * @author javier.aponte
 * @modified by leidy.gonzalez Variable que almacena el resultado de la cantidad de registros
 * prediales y la cantidad de registros de propietarios. Se agregan objetos para guardar valores de
 * numeros prediales
 */
@Component("cantidadRegistrosPrediales")
@Scope("session")
public class ConsultaCantidadDeRegistrosPredialesMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -3865342860839390196L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ConsultaCantidadDeRegistrosPredialesMB.class);

    /**
     * Variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable para almacenar los criterios de búsqueda de predios
     */
    private FiltroDatosConsultaPredio filtroDatosConsultaPredio;

    /**
     * Variable que almacena el resultado de la cantidad de registros prediales y la cantidad de
     * registros de propietarios
     */
    private List<BigDecimal[]> resultadoConsulta;

    /**
     * Variable que almacena el tipo de selección para realizar la consulta de cantidad de registros
     * prediales
     */
    private String tipoDeSeleccionConsultaPredio;

    /**
     * Objeto que contiene las partes del número predial
     */
    private NumeroPredialPartes numeroPredialPartes;

    /**
     * Objeto que contiene las partes del número predial inicial cuando la consulta es por rango
     */
    private NumeroPredialPartes numeroPredialPartesInicial;

    /**
     * Objeto que contiene las partes del número predial final cuando la consulta es por rango
     */
    private NumeroPredialPartes numeroPredialPartesFinal;

    public String getTipoDeSeleccionConsultaPredio() {
        return tipoDeSeleccionConsultaPredio;
    }

    public void setTipoDeSeleccionConsultaPredio(
        String tipoDeSeleccionConsultaPredio) {
        this.tipoDeSeleccionConsultaPredio = tipoDeSeleccionConsultaPredio;
    }

    public List<BigDecimal[]> getResultadoConsulta() {
        return resultadoConsulta;
    }

    public void setResultadoConsulta(List<BigDecimal[]> resultadoConsulta) {
        this.resultadoConsulta = resultadoConsulta;
    }

    public NumeroPredialPartes getNumeroPredialPartes() {
        return numeroPredialPartes;
    }

    public void setNumeroPredialPartes(NumeroPredialPartes numeroPredialPartes) {
        this.numeroPredialPartes = numeroPredialPartes;
    }

    public NumeroPredialPartes getNumeroPredialPartesInicial() {
        return numeroPredialPartesInicial;
    }

    public void setNumeroPredialPartesInicial(
        NumeroPredialPartes numeroPredialPartesInicial) {
        this.numeroPredialPartesInicial = numeroPredialPartesInicial;
    }

    public NumeroPredialPartes getNumeroPredialPartesFinal() {
        return numeroPredialPartesFinal;
    }

    public void setNumeroPredialPartesFinal(
        NumeroPredialPartes numeroPredialPartesFinal) {
        this.numeroPredialPartesFinal = numeroPredialPartesFinal;
    }

    // -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init ConsultaCantidadDeRegistrosPredialesMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.filtroDatosConsultaPredio = new FiltroDatosConsultaPredio();

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartesInicial = new NumeroPredialPartes();
        this.numeroPredialPartesFinal = new NumeroPredialPartes();

    }

    private void inicializarVariablesDePantalla() {

        this.numeroPredialPartes = new NumeroPredialPartes();
        this.numeroPredialPartesInicial = new NumeroPredialPartes();
        this.numeroPredialPartesFinal = new NumeroPredialPartes();
    }

    /**
     * Método encargado de consultar la cantidad de registros prediales que cumplen los criterios de
     * búsqueda
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se cambio modo de consulta de cantidad de registros prediales
     * para generar producto Catastral de acuerdo al control de cambio realizado.
     */
    public void consultarCantidadDeRegistrosPrediales() {

        this.resultadoConsulta = new ArrayList<BigDecimal[]>();

        Object[] resultado = new Object[2];
        BigDecimal[] resultados = new BigDecimal[2];

        List<Object> parametros = new ArrayList<Object>();

        String numeroPredial = "";
        String numeroPredialInicial = "";
        String numeroPredialFinal = "";

        if ((this.tipoDeSeleccionConsultaPredio != null && !this.tipoDeSeleccionConsultaPredio.
            trim().isEmpty() &&
             "1".equals(this.tipoDeSeleccionConsultaPredio)) ||
             this.tipoDeSeleccionConsultaPredio == null) {

            numeroPredial = this.numeroPredialPartes.assembleNumeroPredialFromSegments();

            if (numeroPredial == null || numeroPredial.isEmpty()) {

                this.addMensajeError("Debe ingresar el número predial completo");
                return;

            }

            parametros.add(numeroPredial);
            parametros.add(numeroPredial);

        } else if (this.tipoDeSeleccionConsultaPredio != null &&
            !this.tipoDeSeleccionConsultaPredio.trim().isEmpty() &&
             "2".equals(this.tipoDeSeleccionConsultaPredio)) {

            numeroPredialInicial = this.numeroPredialPartesInicial.
                assembleNumeroPredialFromSegments();

            if (numeroPredialInicial == null || numeroPredialInicial.isEmpty()) {

                this.addMensajeError("Debe ingresar el número predial inicial completo");
                return;

            }

            numeroPredialFinal = this.numeroPredialPartesFinal.assembleNumeroPredialFromSegments();

            if (numeroPredialFinal == null || numeroPredialFinal.isEmpty()) {

                this.addMensajeError("Debe ingresar el número predial final completo");
                return;

            }
            parametros.add(numeroPredialInicial);
            parametros.add(numeroPredialFinal);
        }

        resultado = this.getGeneralesService().contarRegistrosPrediales(parametros);

        //Cantidad de registros por propietario
        resultados[0] = this.getBigDecimal(resultado[1]);
        //Cantidad de registros por predio
        resultados[1] = this.getBigDecimal(resultado[0]);

        this.resultadoConsulta.add(resultados);

        numeroPredial = new String();
        numeroPredialFinal = new String();
        numeroPredialInicial = new String();
        inicializarVariablesDePantalla();

    }

    /**
     * Método encargado de convertir un objeto de tipo Object a un objeto de tipo BigDecimal
     *
     * @param value
     * @return
     */
    // tomado de: http://stackoverflow.com/questions/13175623/how-to-convert-a-list-object-to-bigdecimal-in-prepared-statement
    private BigDecimal getBigDecimal(Object value) {
        BigDecimal ret = null;
        if (value != null) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof String) {
                ret = new BigDecimal((String) value);
            } else if (value instanceof Number) {
                ret = new BigDecimal(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " +
                    value.getClass() + " into a BigDecimal.");
            }
        }
        return ret;
    }

//end of class
}
