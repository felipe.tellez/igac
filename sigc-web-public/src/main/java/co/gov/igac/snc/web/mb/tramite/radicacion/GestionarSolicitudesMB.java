package co.gov.igac.snc.web.mb.tramite.radicacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.CirculoRegistralMunicipio;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.ProcesoDeProductosCatastrales;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.comun.utilerias.Duration;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazoPredio;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.DeterminaTramitePH;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;
import co.gov.igac.snc.persistence.entity.tramite.Producto;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudAvisoRegistro;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRequisito;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EFichaMatrizEstado;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.EProductoCatastralEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ESolicitudFormaPeticion;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteFuente;
import co.gov.igac.snc.persistence.util.ETramiteRadicacionEspecial;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramitesNotificacion;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.conservacion.ConsultaTramiteMB;
import co.gov.igac.snc.web.mb.conservacion.ValidacionYAdicionNuevoTramiteMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.SimpleDateFormat;

@Component("gestionarSolicitudes")
@Scope("session")
/**
 * @autor fredy.wilches
 */
public class GestionarSolicitudesMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 3888863230536781383L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionarSolicitudesMB.class);

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    transient private StreamedContent fileTempDownload;

    /**
     * Objeto que contiene los campos filtro para buscar solicitudes
     */
    private VSolicitudPredio filtroBusquedaSolicitud;

    /**
     * Arreglo de las solicitudes resultado de una consulta por filtro
     */
    private List<VSolicitudPredio> solicitudes;

    /**
     * Tramites existentes de la solicitud
     */
    private List<Long> tramitesExistentes;

    /**
     * Solicitud que se encuentra seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Solicitud asociada a la solicitud que se encuentra seleccionada
     */
    private Solicitud solicitudAsociadaASolicitudSeleccionada;

    /**
     * Solicitante seleccionado de la solicitud
     */
    private SolicitanteSolicitud solicitanteSolicitudSeleccionado;

    /**
     * Solicitante seleccionado de la solicitud
     */
    private SolicitanteTramite solicitanteTramiteSeleccionado;

    /**
     * Solicitante del trámite seleccionado
     */
    private boolean solicitanteTramite;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Tipo de avaluo dependiendo del valor en el número predial
     */
    private String tipoAvaluoSeleccionado;

    /**
     * Lista de Documento agregados temporalmente al radicar un tramite
     */
    private List<Documento> documentosDelTramiteTemporales;

    /**
     * Lista de TramiteDocumentacion agregados temporalmente al radicar un tramite
     */
    private List<TramiteDocumentacion> tramiteDocumentacionTemporales;

    /**
     * Lista de SolicitudDocumentacion agregados temporalmente al radicar un tramite
     */
    private List<SolicitudDocumentacion> solicitudDocumentacionTemporales;

    /**
     * Tramite seleccionado
     */
    private Tramite tramiteSeleccionado;

    /**
     * Número de Solicitud utilizado para buscar en Correspondencia
     */
    private String numeroSolicitud;

    /**
     * Bandera que indica si el documento fue encontrado en correspondencia o no
     */
    private boolean banderaMostrarBotonImprimirConstanciaRadicacion;

    /**
     * Solicitante seleccionado temporal dado el problema de reconocimiento de mangedBeanParan en la
     * migración de primeFaces
     */
    private Solicitante solicitanteSeleccionadoTemp;

    /**
     * Trámites asociados a una solicitud
     */
    private List<Tramite> tramitesSolicitud;

    /**
     * lista con los valores del combo de tipos de trámite que se permiten para el tipo de solicitud
     * creada
     */
    private List<SelectItem> tiposTramitePorSolicitud;

    /**
     * Lista de documentos asociados a un tramite
     */
    private List<TramiteDocumentacion> documentacionTramite;

    /**
     * Arbol con los solicitantes de la solicitud
     */
    transient private TreeNode arbolSolicitantesSolicitud;

    /**
     * Lista que contiene los predios asociados a un trámite
     */
    private List<TramitePredioEnglobe> prediosTramite;

    /**
     * variable donde se cargan los resultados paginados de la consulta de solicitudes
     */
    private LazyDataModel<VSolicitudPredio> lazySolicitudes;

    /**
     * Lista que contiene los predios colindantes para englobe asociados al predio seleccionado
     */
    private List<Predio> listaPrediosColindantes;

    /**
     * Lista que contiene los predios colindantes para englobe asociados al predio seleccionado
     */
    private List<Predio> listaGlobalPrediosColindantes;

    /**
     * Objeto temporal de seleccion en la tabla predioTramite
     */
    private TramitePredioEnglobe selectedPredioTramite;

    /**
     * Objeto temporal de seleccion en la tabla predioColindantes
     */
    private Predio selectedPredioColindante;

    /**
     * Partes del número predial para mutaciones de segunda o quinta
     */
    private String numeroPredialParte1;
    private String numeroPredialParte2;
    private String numeroPredialParte3;
    private String numeroPredialParte4;
    private String numeroPredialParte5;
    private String numeroPredialParte6;
    private String numeroPredialParte7;
    private String numeroPredialParte8;
    private String numeroPredialParte9;
    private String numeroPredialParte10;
    private String numeroPredialParte11;
    private String numeroPredialParte12;
    private String numeroPredial;
    private String areaTerreno;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;
    private EOrden ordenDepartamentosTramite = EOrden.CODIGO;
    private EOrden ordenMunicipiosTramite = EOrden.CODIGO;

    /**
     * Orden para la listas de avisos
     */
    private EOrden ordenDepartamentosAvisos = EOrden.CODIGO;
    private EOrden ordenMunicipiosAvisos = EOrden.CODIGO;
    private EOrden ordenCirculosRegistralesAvisos = EOrden.CODIGO;

    /**
     * Orden para la listas de solicitante
     */
    private EOrden ordenDepartamentosSolicitante = EOrden.CODIGO;
    private EOrden ordenMunicipiosSolicitante = EOrden.CODIGO;
    private EOrden ordenPaisesSolicitante = EOrden.CODIGO;

    /**
     * bandera para indicar si el subtipo de trámite es dato requerido
     */
    private boolean subtipoTramiteEsRequerido;

    /**
     * Documento temporal de trámite
     */
    private TramiteDocumentacion documentoSeleccionado;

    /**
     * Documento temporal de ampliación de tiempos
     */
    private TramiteDocumentacion documentoAmpliacionTemp;

    /**
     * VSolicitudPredio temporal
     */
    private VSolicitudPredio sTemp;

    /**
     * Valor asociado al radio de seleccion de tipo de persona
     */
    private int tipoPersona;

    /**
     * Lista resultado de busqueda de solicitantes
     */
    private List<Solicitante> solicitantesRes;

    private FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante;

    /**
     * Lista de requisistos de documentación en el trámite seleccionado
     */
    private List<TramiteDocumentacion> documentacionRequerida;

    /**
     * Lista de documentación adicional presente en el trámite seleccionado
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * Bandera que determina la visualización de los datos del solicitante solo si este se ha
     * buscado primero en el sistema
     */
    private boolean banderaBusquedaSolicitanteRealizada;

    /**
     * Bandera que establece si ya se asigno un documentod e ampliacion de tiempo para el tramite
     */
    private boolean docAmpliarTiempoRecibido;
    /**
     * nombre temporal del archivo cargado
     */
    private String nombreTemporalArchivo;

    /**
     * Datos geográficos de trámite
     */
    private List<Departamento> dptosTramite;
    private List<Municipio> munisTramite;
    private Departamento departamentoTramite;
    private Municipio municipioTramite;
    private List<SelectItem> municipiosTramite;
    private List<SelectItem> departamentosTramite;

    /**
     * Datos geográficos del documento
     */
    private List<Departamento> dptosDocumento;
    private List<Municipio> munisDocumento;
    private Departamento departamentoDocumento;

    //--------------Variables para el reporte de constancia de radicación ----------------------------
    /**
     * Objeto que contiene los datos del reporte de constancia de radicación
     */
    private ReporteDTO reporteConstanciaRadicacion;

    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteSolicitudDocumentos;

    /**
     * Objeto que contiene los datos del reporte de aviso de registro de rechazo
     */
    private ReporteDTO reporteAvisoRegistroRechazo;

    /*
     * variable para mostrar el panel con la informacion del resultado de la busqueda
     */
    private boolean mostrarPanelResultadoConsulta;

    private boolean banderaDocumentosFaltantes;

//--------------Variables para el caso de uso CU-NP-CO-191 radicar derecho de petición y tutela --
    private boolean formaDePeticionDerechoDePeticion;

    private boolean formaDePeticionTutela;

    private boolean formaDeTramiteTramiteNuevo;

    private boolean formaDeTramiteTramiteRadicado;

    private boolean activarFormaDeTramite;

    private boolean activarRelacionarTramite;

    private boolean mostrarAceptarDerechoPeticionOTutela;

    /**
     * Variable que almacena el documento asociado a la solicitud que justifica el derecho de
     * petición o tutela
     */
    private SolicitudDocumentacion solicitudDocumentacion;

//------------------------------------------------------------------------------------------------
    /**
     * Variable utilizada para controlar lógica de documentos cuando se amplía el tiempo
     */
    private boolean ampliarTiempo;

//------------------------------------------------------------------------------------------------
//--------------Variables para los casos de uso de productos -------------------------------------
    /**
     * Variable para almacenar el docuemento soporte asociado al producto
     *
     * @author javier.aponte
     */
    private String documentoSoporteProducto;

    /**
     * Lista donde se almacenan los tipo de documento soporte
     */
    private TipoDocumento tipoDocumentoSoporteSeleccionado;

    /**
     * Documento soporte a adicionar
     */
    private Documento nuevoDocumentoSoporte;

    /**
     * Documento soporte seleccionado en la tabla de lista de documentos soporte
     */
    private Documento nuevoDocumentoSoporteSeleccionado;

    /**
     * Lista de los documentos soporte adicionados
     */
    private List<Documento> documentosSoportesAdicionados;

    /**
     * Lista de los productos asociados a la solicitud
     */
    private List<ProductoCatastral> productosCatastralesAsociados;

    /**
     * Producto catastral seleccionado
     */
    private ProductoCatastral productoCatastralSeleccionado;

    /**
     * Producto seleccionado
     */
    private Producto productoSeleccionado;

    /**
     * Variable que contiene el grupo producto seleccionado
     */
    private GrupoProducto grupoProductoSeleccionado;

    /**
     * Lista que contiene los productos por grupo
     */
    private List<SelectItem> productoPorGrupoV;

    /**
     * Variable booleana que indica si se va a editar un producto catastral seleccionado en la tabla
     * de lista de productos asociados a la solicitud
     */
    private boolean editarProductoCatastral;

    //Banderas de edición
    /**
     * Activar panel productos asociados a la solicitud
     */
    private boolean activarPanelProductosAsociados;

    /**
     * Activar opciones de editar y eliminar documentos soportes
     */
    private boolean activarOpcionesDocumentosSoportes;

    /**
     * Activar opciones de editar y eliminar productos asociados a la solicitud
     */
    private boolean activarOpcionesProductosAsociados;

    /**
     * Número de Documento Copia para editar la solicitud de productos catastrales
     */
    private String numeroDocumentoCopia;

    /**
     * Lista de las copias de los documentos soportes adicionados
     */
    private List<Documento> documentosCopia;

    /**
     * Copia Documento soporte a adicionar
     */
    private Documento documentoCopia;
//---------------------------------------------------------------------------------------------------

    private Municipio municipioDocumento;
    private List<SelectItem> municipiosDocumento;
    private List<SelectItem> departamentosDocumento;

    /**
     * Texto visualizado en el fundamento de la petición de un trámite que depende directamente del
     * tipo de solicitud
     */
    private String fundamentoPeticionText;

    /**
     * Label del caudro de texto visualizado en el fundamento de la petición de un trámite que
     * depende directamente del tipo de solicitud
     */
    private String fundamentoPeticionLabel;

    /**
     * Ruta del documento cargado
     */
    private String rutaMostrar;

    /**
     * Archivo a cargar en la carpeta temporal
     */
    private File archivoResultado;

    /**
     * Lista temporal de los tramites asociados a una solicitud
     */
    private ArrayList<Long> tramitesAlmacenados;

    private UsuarioDTO usuario;

    /**
     * Determina si el usuario autenticado se pertenece a una entidad delegada
     */
    private boolean usuarioDelegado;

    // ----------- rectificación/complementación
    /**
     * tipo de dato para rectificación o complementación seleccionado
     */
    private String tipoDatoRectifComplSeleccionado;

    /**
     * modelo del pick list de datos a modificar o complementar Se requieren dos listas: una fuente
     * y una destino
     */
    private DualListModel<DatoRectificar> datosRectifComplDLModel;

    private List<DatoRectificar> datosRectifComplSource;
    private List<DatoRectificar> datosRectifComplTarget;

    /**
     * lista con los DatoRectificar que se han seleccionado en el pick list
     */
    private List<DatoRectificar> selectedDatosRectifComplPL;

    /**
     * lista con los DatoRectificar de la tabla Datos por rectificar o complementar. </br>
     * Son los que se han escogido en el picklist y que ahora se pintan en la tabla
     */
    private List<DatoRectificar> selectedDatosRectifComplDataTable;

    /**
     * lista con los DatoRectificar que se han seleccionado en el data table de resumen
     */
    private DatoRectificar[] selectedDatosRectifComplDT;

    /**
     * Texto del boton rectificar/complementar
     */
    private String textoBotonRectificacionC;

    // ---------------------- DATOS RELACIONADOS A AVISOS ----------------------
    /**
     * Variable usada para manejar el rechazo-Aviso en proceso
     */
    private AvisoRegistroRechazo avisoRechazo;

    /**
     * Aviso rechazado que tiene un link para mostrar el detalle de todos los predios asociados
     */
    private AvisoRegistroRechazo selectedAvisoRechazado;

    /**
     * lista con los avisos rechazados relacionados con la solicitud actual
     */
    private List<AvisoRegistroRechazo> solicitudAvisoAvisosRechazados;

    /**
     * Datos geográficos del aviso
     */
    private Departamento departamentoAvisos;
    private Municipio municipioAvisos;
    private CirculoRegistral circuloRegistralAvisos;

    /**
     * Listas de select Items avisos
     */
    private List<SelectItem> municipiosAvisos;
    private List<SelectItem> departamentosAvisos;
    private List<SelectItem> circulosRegistralesAvisos;

    /**
     * Listas geográficas de Avisos
     */
    private List<Departamento> dptosAvisos;
    private List<Municipio> munisAvisos;
    private List<CirculoRegistral> circsRegistralesAvisos;

    // ---------------------- DATOS RELACIONADOS AL SOLICITANTE
    // ----------------------
    /**
     * Solicitante seleccionado en la busqueda de solicitudes
     */
    private Solicitante solicitanteSeleccionado;

    /**
     * Lista de solicitantes coincidentes
     */
    private List<Solicitante> solicitantesCoincidentes;

    /**
     * Solicitante temporal usado para definir de la lista de solicitantes coincidentes cual es el
     * correcto
     */
    private Solicitante solicitanteCoincidenteSeleccionado;

    /**
     * Solicitante que se selecciona para editar
     */
    private Solicitante solicitanteAEditar;

    /**
     * Datos geográficos del aviso
     */
    private Pais paisSolicitante;
    private Departamento departamentoSolicitante;
    private Municipio municipioSolicitante;

    /**
     * Listas de select Items avisos
     */
    private List<SelectItem> paisesSolicitante;
    private List<SelectItem> municipiosSolicitante;
    private List<SelectItem> departamentosSolicitante;

    /**
     * Listas geográficas de Avisos
     */
    private List<Pais> paissSolicitante;
    private List<Departamento> dptosSolicitante;
    private List<Municipio> munisSolicitante;

    // **************** SERVICIOS ********************
    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private ValidacionYAdicionNuevoTramiteMB validacionTramiteMB;

    // **************** BANDERAS DE EDICION
    /**
     * bandera que indica si una solicitud se encuentra en edición o es de lectura
     */
    private boolean editandoSolicitud = true;

    /**
     * bandera que indica si un trámite se encuentra en edición o es de lectura
     */
    private boolean editandoTramite;

    /**
     * Bandera que habilita la trabla de resultados de busqueda de solicitudes
     */
    private boolean banderaBuscarSolicitudes;

    /**
     * bandera que indica si un documento (requerido/adicional) se encuentra en edición o es de
     * lectura
     */
    private boolean editandoDocumentacion;

    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean tipoPersonaN;

    /**
     * Bandera de visualización de la tabla de resultado de busqueda de solicitantes
     */
    private boolean banderaBuscarSolicitantes;

    /**
     * Bandera de seleccion de solicitante para la busqueda de solicitudes
     */
    private boolean banderaSolicitanteSolicitudSeleccionado;

    /**
     * Bandera de seleccion de solicitud
     */
    private boolean banderaSolicitudSeleccionada;

    /**
     * Bandera que se activa con la creación de una solicitud
     */
    private boolean banderaSolicitudCreada;

    /**
     * Bandera para determinar si se muestra el panel de los tràmites asociados a la solicitud
     * asociada a la solicitud
     */
    private boolean banderaMostrarTramiteRadicadoDerechoPeticionOTutela;

    /**
     * Bandear que sirve para ocultar o mostrar los campos relacionados con el número de radicado al
     * crear un nuevo documento
     */
    private boolean banderaMostrarNumeroRadicado;

    /**
     * Bandera que se activa cuando se almacena un trámite
     */
    private boolean banderaTramitePAlmacenado;

    /**
     * Bandera que identifica si el documento fue digitalizado
     */
    private boolean banderaDocumentoDigital;

    /**
     * Bandera que identifica si el documento es adicional
     */
    private boolean banderaDocumentoAdicional;

//TODO :: juanfelipe.garcia :: documentar esta variable :: pedro.garcia    
    private boolean documentoSoporteEliminado;

    // ------------------------------------------
    /**
     * Criterio de busqueda si los tramites son de tipo via gubernativa o revocatoria
     */
    private String criterio;

    /**
     * Objeto usado como contenedor del numero predial para la consulta de tramites por via
     * gubernativa
     */
    private FiltroDatosConsultaTramitesNotificacion datosConsultaTramite;

    /**
     * Variable usada para la consulta de tramites via gubernativa por numero de resolucion
     */
    private String numResolucion;

    /**
     * Lista de Tramites de via gubernativa
     */
    private List<Tramite> tramitesGubernativaAll;

    /**
     * Arreglo de los tramites via gubernativa buscados por resolucion seleccionados
     */
    private Tramite[] selectedTramitesGubernativaBuscados;

    /**
     * Lista de resoluciones buscadas por el criterio resolucion en solicitudes de via gubernativa
     */
    private List<Tramite> listTramitesBuscadosPorCriterio;

    /**
     * Resoluciones buscadas por el número predial cuando el trámite es de Via Gubernativa
     */
    private List<String> listTramResolucion;

    /**
     * Variable usada para activar el botón de guardarTramiteSolicitud cuando se seleccione un
     * trámite en vía gubernativa
     */
    private Boolean selectedTramiteGubernativaBuscadoBool;

    /**
     * Tramite seleccionado de la busqueda de trámites buscados por resolución
     */
    private Tramite selectedTramiteGubernativaBuscado;

    /**
     * Trámite de via gubernativa a borrar de la lista
     */
    private Tramite tramiteDeleteSeleccionado;

    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;

    /**
     * variable usada para saber si un tramite tiene documentacion a digitalizar (true) o si esta
     * pendiente por enviar (false)
     */
    private boolean documentacionADigitalizarBool;

    /**
     * Lista de documentos aportados,digitalizables que no han sido cargados de un tramite
     */
    private List<TramiteDocumentacion> documentosADigitalizar;

    /**
     * Bandera de visualización para componentes de trámite cuando el tipo de solicitud es de avaluo
     * o auto avaluo
     */
    private boolean banderaTipoSolicitudAvaluo;

    /**
     * Bandera que determina si se selecciono una clase de mutación
     */
    private boolean banderaClaseDeMutacionSeleccionada;

    /**
     * Variable booleana usada como bandera para saber si se mueve el proceso a "Digitalizar" o a
     * "Pendiente por enviar"
     */
    private boolean caminoActividadDigitalizar;

    /**
     * Bandera que determina si el subtipo de la mutación de segunda es englobe
     */
    private boolean banderaSubtipoEnglobe;

    /**
     * Bandera para mostrar las resoluciones si se busca por numero predial
     */
    private Boolean numPredialBool;

    /**
     * Variable usada para guardar la resolucion de un trámite
     */
    private String resolucionViaGubernativa;

    /**
     * Valores para las relaciones del solicitante.
     */
    private List<SelectItem> relacionesSolicitante;

    /**
     * Variable usada para saber si la resolución de un trámite está en firme o en proyección para
     * las solicitudes de vía gubernativa.
     */
    private boolean resolucionTramiteEnProyeccion;

    /**
     * Bandera paa determinar si el archivo relacionado al docuemtno se esta reemplazando.
     */
    private boolean banderaNuevoArchivo;

    // ----------------------------------------------------------------------------------//
    // --------------------- Variables usadas para recibir documentos
    // -------------------//
    // ----------------------------------------------------------------------------------//
    /**
     * Variable usada como criterio de busqueda por número de solicitud
     */
    private String numSolicitudBusqueda;

    /**
     * Variable usada como criterio de busqueda por número de radicación
     */
    private String numRadicacionBusqueda;

    /**
     * Variable usada como criterio de busqueda por solicitante
     */
    private Solicitante solicitanteBusqueda;

    /**
     * Arreglo de todos Trámites con documentos faltantes
     */
    private List<Tramite> tramitesConDocumentosFaltantesCompleta;

    /**
     * Copia del arreglo de Trámites con documentos faltantes
     */
    private List<Tramite> tramitesConDocumentosFaltantes;

    /**
     * Variable booleana para habilitar un botón si hay un trámite seleccionado
     */
    private Boolean tramiteSeleccionadoBool;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * Variable booleana para mostrar o no el boton y la tabla de busqueda de solicitantes, no se
     * muestra si existen otros filtros, para el caso de asociar documentos, en donde existen
     * filtros adicionales
     */
    private boolean filtrosAdicionalesBool;

    /**
     * Variables para la ampliación de tiempos de la solicitud de documentos
     */
    private Date fechaComunicacionSolicitud;
    private Date fechaFinalizacionDeEspera;
    private Integer tiempoAmpliacion;
    private Date nuevaFechaFinalizacion;

    /**
     * Variable usada para almacenar la actividad a la que se va a mover el proceso
     */
    private String siguienteActividad;

    /**
     * Id de la actividad actual
     */
    private String activityId;

    /**
     * Actividad seleccionada
     */
    private Actividad actividadSeleccionada;

    /**
     * Objeto duración usado en el process cuando se solicita una ampliación de tiempos
     */
    private Duration duracion;

    /**
     * Archivo que se consulta para visualizar la solicitud de documentos
     */
    private TramiteDocumento solicitudDeDocumentos;

    /**
     * Variable usada para guardar la url de la solicitud de documentos en el servidor.
     */
    private String solicitudDocumentosUrlArchivo;

    /**
     * Variable usada para saber si se pudo cargar la solicitud de documentos.
     */
    private boolean solicitudDocumentosBool;

    /**
     * Variables usada para saber si ya se digitalizaron los documentos al recibir documentos y dar
     * paso a avanzar el proceso.
     */
    private boolean avanzarDeRecibirDocumentosBool;

    /**
     * variable usada para saber si se encontró o no una instancia del proceso asociada al trámite
     */
    private boolean actividadEncontradaBool;

    /**
     * Variable para hacer un backup del documento seleccionado.
     */
    private TramiteDocumentacion documentoReplica;

    /**
     * Variable para visualizar la fecha de radicación en tesorería dependiendo del tipo de trámite
     * y si el solicitante es tesorería.
     */
    private boolean mostrarFechaRadTesoreria;

    /**
     * Objeto que contiene los datos del reporte con marca de agua para visualizar la resolución de
     * un trámite.
     */
    private ReporteDTO resolucionWaterMark;

    /**
     * Variable para almacenar el documento al momento de realizar la consulta de una resolución por
     * número de resolución.
     */
    private Documento documentoResolucionVG;

    /**
     * variable booleana de control para validar el radicado
     */
    private boolean radicadoValidado;

    /**
     * Variable booleana que indica si el documento que se va a a guardar es un documento de derecho
     * de petición o tutela
     */
    private boolean documentoDerechoPeticionOTutela;

    /**
     * Lista de los predios selecionados cancelados
     */
    private List<String> prediosCancelados;

    /**
     * Bandera para determinar si un predio no tiene info geografica
     */
    private boolean banderaNoGeograficoEnglobe;

    /**
     * Bandera para determinar si ya se han terminado de radicar los avisos
     */
    private boolean banderaAvisosNoTerminados;

    /**
     * Bandera que indica si el predio es fiscal
     */
    private boolean banderaPredioFiscal;

    /**
     * Lista temporal de documentos asociados a un tramite
     */
    private List<TramiteDocumentacion> documentacionTramiteTmp;

    private List<Municipio> municipiosDelegados;

    /**
     * Bandera que habilita el command link de si existe o no un aviso de rechazo
     */
    private boolean existeRechazoPredio;
    // ------------- methods ------------------------------------

    /**
     * @author pedro.garcia
     */
    public List<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> answer = null;
        List<TipoSolicitudTramite> tiposTramite = null;
        SelectItem itemToAdd;

        if (this.solicitudSeleccionada.getTipo() != null) {
            tiposTramite = this.getTramiteService()
                .obtenerTiposDeTramitePorSolicitud(
                    this.solicitudSeleccionada.getTipo());
        }
        if (tiposTramite != null && !tiposTramite.isEmpty()) {
            answer = new ArrayList<SelectItem>();
            for (TipoSolicitudTramite tst : tiposTramite) {
                itemToAdd = new SelectItem(tst.getTipoTramite(),
                    tst.getTipoTramiteValor());
                answer.add(itemToAdd);
            }
        }
        this.tiposTramitePorSolicitud = answer;
        return this.tiposTramitePorSolicitud;
    }

    public boolean isExisteRechazoPredio() {
        return existeRechazoPredio;
    }

    public void setExisteRechazoPredio(boolean existeRechazoPredio) {
        this.existeRechazoPredio = existeRechazoPredio;
    }

    public void setTiposTramitePorSolicitud(List<SelectItem> tiposTramite) {
        this.tiposTramitePorSolicitud = tiposTramite;
    }

    public Departamento getDepartamentoDocumento() {
        return departamentoDocumento;
    }

    public List<Documento> getDocumentosCopia() {
        return documentosCopia;
    }

    public void setDocumentosCopia(List<Documento> documentosCopia) {
        this.documentosCopia = documentosCopia;
    }

    public void setDocumentoCopia(Documento documentoCopia) {
        this.documentoCopia = documentoCopia;
    }

    public Solicitante getSolicitanteCoincidenteSeleccionado() {
        return solicitanteCoincidenteSeleccionado;
    }

    public void setSolicitanteCoincidenteSeleccionado(
        Solicitante solicitanteCoincidenteSeleccionado) {
        this.solicitanteCoincidenteSeleccionado = solicitanteCoincidenteSeleccionado;
    }

    public List<Documento> getDocumentoCopia() {
        return documentosCopia;
    }

    public void setDocumentoCopia(List<Documento> documentoCopia) {
        this.documentosCopia = documentoCopia;
    }

    public void setDepartamentoDocumento(Departamento departamentoDocumento) {
        this.departamentoDocumento = departamentoDocumento;
    }

    public boolean isBanderaDocumentosFaltantes() {
        return banderaDocumentosFaltantes;
    }

    public void setBanderaDocumentosFaltantes(boolean banderaDocumentosFaltantes) {
        this.banderaDocumentosFaltantes = banderaDocumentosFaltantes;
    }

    public String getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(String areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public Municipio getMunicipioDocumento() {
        return municipioDocumento;
    }

    public void setMunicipioDocumento(Municipio municipioDocumento) {
        this.municipioDocumento = municipioDocumento;
    }

    public List<Solicitante> getSolicitantesCoincidentes() {
        return solicitantesCoincidentes;
    }

    public void setSolicitantesCoincidentes(
        List<Solicitante> solicitantesCoincidentes) {
        this.solicitantesCoincidentes = solicitantesCoincidentes;
    }

    public TramiteDocumentacion getDocumentoAmpliacionTemp() {
        return documentoAmpliacionTemp;
    }

    public void setDocumentoAmpliacionTemp(
        TramiteDocumentacion documentoAmpliacionTemp) {
        this.documentoAmpliacionTemp = documentoAmpliacionTemp;
    }

    public boolean isDocAmpliarTiempoRecibido() {
        return docAmpliarTiempoRecibido;
    }

    public void setDocAmpliarTiempoRecibido(boolean docAmpliarTiempoRecibido) {
        this.docAmpliarTiempoRecibido = docAmpliarTiempoRecibido;
    }

    public boolean isBanderaAvisosNoTerminados() {
        return banderaAvisosNoTerminados;
    }

    public void setBanderaAvisosNoTerminados(boolean banderaAvisosNoTerminados) {
        this.banderaAvisosNoTerminados = banderaAvisosNoTerminados;
    }

    public List<String> getPrediosCancelados() {
        return prediosCancelados;
    }

    public void setPrediosCancelados(List<String> prediosCancelados) {
        this.prediosCancelados = prediosCancelados;
    }

    public boolean isBanderaNoGeograficoEnglobe() {
        return banderaNoGeograficoEnglobe;
    }

    public void setBanderaNoGeograficoEnglobe(boolean banderaNoGeograficoEnglobe) {
        this.banderaNoGeograficoEnglobe = banderaNoGeograficoEnglobe;
    }

    public boolean isBanderaBuscarSolicitantes() {
        return this.banderaBuscarSolicitantes;
    }

    public void setConsultaPredioMB(ConsultaPredioMB consultaPredioMB) {
        this.consultaPredioMB = consultaPredioMB;
    }

    public boolean isDocumentoSoporteEliminado() {
        return documentoSoporteEliminado;
    }

    public void setDocumentoSoporteEliminado(boolean documentoSoporteEliminado) {
        this.documentoSoporteEliminado = documentoSoporteEliminado;
    }

    public List<Departamento> getDptosTramite() {
        return dptosTramite;
    }

    public void setDptosTramite(List<Departamento> dptosTramite) {
        this.dptosTramite = dptosTramite;
    }

    public List<Municipio> getMunisTramite() {
        return munisTramite;
    }

    public Departamento getDepartamentoTramite() {
        return departamentoTramite;
    }

    public boolean isEditandoDocumentacion() {
        return editandoDocumentacion;
    }

    public void setEditandoDocumentacion(boolean editandoDocumentacion) {
        this.editandoDocumentacion = editandoDocumentacion;
    }

    public boolean isBanderaBusquedaSolicitanteRealizada() {
        return banderaBusquedaSolicitanteRealizada;
    }

    public void setBanderaBusquedaSolicitanteRealizada(
        boolean banderaBusquedaSolicitanteRealizada) {
        this.banderaBusquedaSolicitanteRealizada = banderaBusquedaSolicitanteRealizada;
    }

    public boolean isBanderaDocumentoAdicional() {
        return banderaDocumentoAdicional;
    }

    public Solicitante getSolicitanteSeleccionadoTemp() {
        return solicitanteSeleccionadoTemp;
    }

    public void setSolicitanteSeleccionadoTemp(
        Solicitante solicitanteSeleccionadoTemp) {
        this.solicitanteSeleccionadoTemp = solicitanteSeleccionadoTemp;
    }

    public String getTextoBotonRectificacionC() {
        return this.textoBotonRectificacionC;
    }

    public void setTextoBotonRectificacionC(String textoBotonRectificacionC) {
        this.textoBotonRectificacionC = textoBotonRectificacionC;
    }

    public ReporteDTO getResolucionWaterMark() {
        return this.resolucionWaterMark;
    }

    public void setResolucionWaterMark(ReporteDTO resolucionWaterMark) {
        this.resolucionWaterMark = resolucionWaterMark;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public Documento getDocumentoResolucionVG() {
        return documentoResolucionVG;
    }

    public void setDocumentoResolucionVG(Documento documentoResolucionVG) {
        this.documentoResolucionVG = documentoResolucionVG;
    }

    public void setBanderaDocumentoAdicional(boolean banderaDocumentoAdicional) {
        this.banderaDocumentoAdicional = banderaDocumentoAdicional;
    }

    public boolean isBanderaTipoSolicitudAvaluo() {
        return banderaTipoSolicitudAvaluo;
    }

    public void setBanderaTipoSolicitudAvaluo(boolean banderaTipoSolicitudAvaluo) {
        this.banderaTipoSolicitudAvaluo = banderaTipoSolicitudAvaluo;
    }

    public void setDepartamentoTramite(Departamento departamentoTramite) {
        this.departamentoTramite = departamentoTramite;
    }

    public boolean isResolucionTramiteEnProyeccion() {
        return resolucionTramiteEnProyeccion;
    }

    public void setResolucionTramiteEnProyeccion(
        boolean resolucionTramiteEnProyeccion) {
        this.resolucionTramiteEnProyeccion = resolucionTramiteEnProyeccion;
    }

    public Municipio getMunicipioTramite() {
        return municipioTramite;
    }

    public void setMunicipioTramite(Municipio municipioTramite) {
        this.municipioTramite = municipioTramite;
    }

    public TramiteDocumentacion getDocumentoReplica() {
        return documentoReplica;
    }

    public void setDocumentoReplica(TramiteDocumentacion documentoReplica) {
        this.documentoReplica = documentoReplica;
    }

    public void setMunisTramite(List<Municipio> munisTramite) {
        this.munisTramite = munisTramite;
    }

    public List<TramiteDocumentacion> getDocumentacionTramite() {
        return documentacionTramite;
    }

    public boolean isBanderaDocumentoDigital() {
        return banderaDocumentoDigital;
    }

    public boolean isBanderaSubtipoEnglobe() {
        return banderaSubtipoEnglobe;
    }

    public void setBanderaSubtipoEnglobe(boolean banderaSubtipoEnglobe) {
        this.banderaSubtipoEnglobe = banderaSubtipoEnglobe;
    }

    public boolean isBanderaClaseDeMutacionSeleccionada() {
        return banderaClaseDeMutacionSeleccionada;
    }

    public void setBanderaClaseDeMutacionSeleccionada(
        boolean banderaClaseDeMutacionSeleccionada) {
        this.banderaClaseDeMutacionSeleccionada = banderaClaseDeMutacionSeleccionada;
    }

    public void setBanderaDocumentoDigital(boolean banderaDocumentoDigital) {
        this.banderaDocumentoDigital = banderaDocumentoDigital;
    }

    public void setDocumentacionTramite(
        List<TramiteDocumentacion> documentacionTramite) {
        this.documentacionTramite = documentacionTramite;
    }

    public List<TramiteDocumentacion> getDocumentacionRequerida() {
        return documentacionRequerida;
    }

    public void setDocumentacionRequerida(
        List<TramiteDocumentacion> documentacionRequerida) {
        this.documentacionRequerida = documentacionRequerida;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public boolean isBanderaMostrarNumeroRadicado() {
        return banderaMostrarNumeroRadicado;
    }

    public void setBanderaMostrarNumeroRadicado(boolean banderaMostrarNumeroRadicado) {
        this.banderaMostrarNumeroRadicado = banderaMostrarNumeroRadicado;
    }

    public boolean isBanderaTramitePAlmacenado() {
        return banderaTramitePAlmacenado;
    }

    public void setBanderaTramitePAlmacenado(boolean banderaTramitePAlmacenado) {
        this.banderaTramitePAlmacenado = banderaTramitePAlmacenado;
    }

    public Predio getSelectedPredioColindante() {
        return selectedPredioColindante;
    }

    public void setSelectedPredioColindante(Predio selectedPredioColindante) {
        this.selectedPredioColindante = selectedPredioColindante;
    }

    public String getFundamentoPeticionText() {
        return fundamentoPeticionText;
    }

    public void setFundamentoPeticionText(String fundamentoPeticionText) {
        this.fundamentoPeticionText = fundamentoPeticionText;
    }

    public String getFundamentoPeticionLabel() {
        return fundamentoPeticionLabel;
    }

    public void setFundamentoPeticionLabel(String fundamentoPeticionLabel) {
        this.fundamentoPeticionLabel = fundamentoPeticionLabel;
    }

    public void setBanderaSolicitudCreada(boolean banderaSolicitudCreada) {
        this.banderaSolicitudCreada = banderaSolicitudCreada;
    }

    public boolean isBanderaSolicitudCreada() {
        return banderaSolicitudCreada;
    }

    public boolean isBanderaMostrarTramiteRadicadoDerechoPeticionOTutela() {
        return banderaMostrarTramiteRadicadoDerechoPeticionOTutela;
    }

    public boolean isCaminoActividadDigitalizar() {
        return caminoActividadDigitalizar;
    }

    public void setCaminoActividadDigitalizar(boolean caminoActividadDigitalizar) {
        this.caminoActividadDigitalizar = caminoActividadDigitalizar;
    }

    public Boolean getNumPredialBool() {
        return numPredialBool;
    }

    public void setNumPredialBool(Boolean numPredialBool) {
        this.numPredialBool = numPredialBool;
    }

    public Boolean getSelectedTramiteGubernativaBuscadoBool() {
        return selectedTramiteGubernativaBuscadoBool;
    }

    public void setSelectedTramiteGubernativaBuscadoBool(
        Boolean selectedTramiteGubernativaBuscadoBool) {
        this.selectedTramiteGubernativaBuscadoBool = selectedTramiteGubernativaBuscadoBool;
    }

    public Tramite getSelectedTramiteGubernativaBuscado() {
        return selectedTramiteGubernativaBuscado;
    }

    public void setSelectedTramiteGubernativaBuscado(
        Tramite selectedTramiteGubernativaBuscado) {
        this.selectedTramiteGubernativaBuscado = selectedTramiteGubernativaBuscado;
    }

    public Tramite getTramiteDeleteSeleccionado() {
        return tramiteDeleteSeleccionado;
    }

    public String getTipoAvaluoSeleccionado() {
        return tipoAvaluoSeleccionado;
    }

    public void setTipoAvaluoSeleccionado(String tipoAvaluoSeleccionado) {
        this.tipoAvaluoSeleccionado = tipoAvaluoSeleccionado;
    }

    public void setTramiteDeleteSeleccionado(Tramite tramiteDeleteSeleccionado) {
        this.tramiteDeleteSeleccionado = tramiteDeleteSeleccionado;
    }

    public String getResolucionViaGubernativa() {
        return resolucionViaGubernativa;
    }

    public void setResolucionViaGubernativa(String resolucionViaGubernativa) {
        this.resolucionViaGubernativa = resolucionViaGubernativa;
    }

    public AvisoRegistroRechazo getSelectedAvisoRechazado() {
        return this.selectedAvisoRechazado;
    }

    public void setSelectedAvisoRechazado(
        AvisoRegistroRechazo selectedAvisoRechazado) {
        this.selectedAvisoRechazado = selectedAvisoRechazado;
    }

    public ReporteDTO getReporteAvisoRegistroRechazo() {
        return reporteAvisoRegistroRechazo;
    }

    public void setReporteAvisoRegistroRechazo(ReporteDTO reporteAvisoRegistroRechazo) {
        this.reporteAvisoRegistroRechazo = reporteAvisoRegistroRechazo;
    }

    /**
     * consulta los avisos rechazados de una solicitud
     *
     * @author pedro.garcia
     * @return
     */
    public List<AvisoRegistroRechazo> getSolicitudAvisoAvisosRechazados() {

        this.solicitudAvisoAvisosRechazados = this.getTramiteService()
            .obtenerInfoAvisosRechazadosPorSolicitud(
                this.solicitudSeleccionada.getNumero());

        return this.solicitudAvisoAvisosRechazados;
    }

    public void setSolicitudAvisoAvisosRechazados(
        List<AvisoRegistroRechazo> solicitudAvisoAvisosRechazados) {
        this.solicitudAvisoAvisosRechazados = solicitudAvisoAvisosRechazados;
    }

    // --------------------------------------------------------------------------------------------------
    public void setBanderaBuscarSolicitantes(boolean banderaBuscarSolicitantes) {
        this.banderaBuscarSolicitantes = banderaBuscarSolicitantes;
    }

    public DatoRectificar[] getSelectedDatosRectifComplDT() {
        return this.selectedDatosRectifComplDT;
    }

    public boolean isBanderaSolicitudSeleccionada() {
        return banderaSolicitudSeleccionada;
    }

    public void setBanderaSolicitudSeleccionada(
        boolean banderaSolicitudSeleccionada) {
        this.banderaSolicitudSeleccionada = banderaSolicitudSeleccionada;
    }

    public boolean isBanderaSolicitanteSolicitudSeleccionado() {
        return banderaSolicitanteSolicitudSeleccionado;
    }

    public void setBanderaSolicitanteSolicitudSeleccionado(
        boolean banderaSolicitanteSolicitudSeleccionado) {
        this.banderaSolicitanteSolicitudSeleccionado = banderaSolicitanteSolicitudSeleccionado;
    }

    public void setSelectedDatosRectifComplDT(
        DatoRectificar[] selectedDatosRectifComplDT) {
        this.selectedDatosRectifComplDT = selectedDatosRectifComplDT;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public Solicitante getSolicitanteSeleccionado() {
        return solicitanteSeleccionado;
    }

    public Solicitante getSolicitanteAEditar() {
        return solicitanteAEditar;
    }

    public void setSolicitanteAEditar(Solicitante solicitanteAEditar) {
        this.solicitanteAEditar = solicitanteAEditar;
    }

    public List<SelectItem> getSubtipoClaseMutacionV() {
        return subtipoClaseMutacionV;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public FiltroDatosConsultaSolicitante getFiltroDatosConsultaSolicitante() {
        return filtroDatosConsultaSolicitante;
    }

    public void setFiltroDatosConsultaSolicitante(
        FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante) {
        this.filtroDatosConsultaSolicitante = filtroDatosConsultaSolicitante;
    }

    public void setSolicitanteSeleccionado(Solicitante solicitanteSeleccionado) {
        this.solicitanteSeleccionado = solicitanteSeleccionado;
    }

    public TramiteDocumentacion getDocumentoSeleccionado() {
        return documentoSeleccionado;
    }

    public void setDocumentoSeleccionado(
        TramiteDocumentacion documentoSeleccionado) {
        this.documentoSeleccionado = documentoSeleccionado;
    }

    public List<Solicitante> getSolicitantesRes() {
        return solicitantesRes;
    }

    public void setSolicitantesRes(List<Solicitante> solicitantesRes) {
        this.solicitantesRes = solicitantesRes;
    }

    public boolean isTipoPersonaN() {
        return tipoPersonaN;
    }

    public void setTipoPersonaN(boolean tipoPersonaN) {
        this.tipoPersonaN = tipoPersonaN;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public List<DatoRectificar> getSelectedDatosRectifComplPL() {
        return this.selectedDatosRectifComplPL;
    }

    public void setSelectedDatosRectifComplPL(
        List<DatoRectificar> selectedDatosRectifCompl) {
        this.selectedDatosRectifComplPL = selectedDatosRectifCompl;
    }

    public List<DatoRectificar> getdatosRectifComplSource() {
        return this.datosRectifComplSource;
    }

    public StreamedContent getFileTempDownload() {
        controladorDescargaArchivos();
        return this.fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public ReporteDTO getReporteConstanciaRadicacion() {
        return this.reporteConstanciaRadicacion;
    }

    public void setReporteConstanciaRadicacion(ReporteDTO reporteConstanciaRadicacion) {
        this.reporteConstanciaRadicacion = reporteConstanciaRadicacion;
    }

    public ReporteDTO getReporteSolicitudDocumentos() {
        return reporteSolicitudDocumentos;
    }

    public void setReporteSolicitudDocumentos(ReporteDTO reporteSolicitudDocumentos) {
        this.reporteSolicitudDocumentos = reporteSolicitudDocumentos;
    }

    public void setdatosRectifComplSource(
        List<DatoRectificar> datosRectifComplSource) {
        this.datosRectifComplSource = datosRectifComplSource;
    }

    public List<DatoRectificar> getDatosRectifComplTarget() {
        return this.datosRectifComplTarget;
    }

    public void setDatosRectifComplTarget(
        List<DatoRectificar> datosRectifComplTarget) {
        this.datosRectifComplTarget = datosRectifComplTarget;
    }

    public String getTipoDatoRectifComplSeleccionado() {
        return this.tipoDatoRectifComplSeleccionado;
    }

    public void setTipoDatoRectifComplSeleccionado(
        String tipoDatoRectifComplSeleccionado) {
        this.tipoDatoRectifComplSeleccionado = tipoDatoRectifComplSeleccionado;
    }

    public boolean isEditandoSolicitud() {
        return editandoSolicitud;
    }

    public String getNumeroPredialParte1() {
        return numeroPredialParte1;
    }

    public void setNumeroPredialParte1(String numeroPredialParte1) {
        this.numeroPredialParte1 = numeroPredialParte1;
    }

    public String getNumeroPredialParte2() {
        return numeroPredialParte2;
    }

    public void setNumeroPredialParte2(String numeroPredialParte2) {
        this.numeroPredialParte2 = numeroPredialParte2;
    }

    public String getNumeroPredialParte12() {
        return numeroPredialParte12;
    }

    public void setNumeroPredialParte12(String numeroPredialParte12) {
        this.numeroPredialParte12 = numeroPredialParte12;
    }

    public String getNumeroPredialParte3() {
        return numeroPredialParte3;
    }

    public void setNumeroPredialParte3(String numeroPredialParte3) {
        this.numeroPredialParte3 = numeroPredialParte3;
    }

    public String getNumeroPredialParte4() {
        return numeroPredialParte4;
    }

    public EOrden getOrdenDepartamentos() {
        return ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public boolean isSubtipoTramiteEsRequerido() {
        return subtipoTramiteEsRequerido;
    }

    public void setSubtipoTramiteEsRequerido(boolean subtipoTramiteEsRequerido) {
        this.subtipoTramiteEsRequerido = subtipoTramiteEsRequerido;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public List<Departamento> getDptosAvisos() {
        return dptosAvisos;
    }

    public void setDptosAvisos(List<Departamento> dptos) {
        this.dptosAvisos = dptos;
    }

    public void setMunisAvisos(List<Municipio> municipios) {
        this.munisAvisos = municipios;
    }

    public void setNumeroPredialParte4(String numeroPredialParte4) {
        this.numeroPredialParte4 = numeroPredialParte4;
    }

    public String getNumeroPredialParte5() {
        return numeroPredialParte5;
    }

    public void setNumeroPredialParte5(String numeroPredialParte5) {
        this.numeroPredialParte5 = numeroPredialParte5;
    }

    public String getNumeroPredialParte6() {
        return numeroPredialParte6;
    }

    public void setNumeroPredialParte6(String numeroPredialParte6) {
        this.numeroPredialParte6 = numeroPredialParte6;
    }

    public String getNumeroPredialParte7() {
        return numeroPredialParte7;
    }

    public void setNumeroPredialParte7(String numeroPredialParte7) {
        this.numeroPredialParte7 = numeroPredialParte7;
    }

    public String getNumeroPredialParte8() {
        return numeroPredialParte8;
    }

    public void setNumeroPredialParte8(String numeroPredialParte8) {
        this.numeroPredialParte8 = numeroPredialParte8;
    }

    public String getNumeroPredialParte9() {
        return numeroPredialParte9;
    }

    public void setNumeroPredialParte9(String numeroPredialParte9) {
        this.numeroPredialParte9 = numeroPredialParte9;
    }

    public String getNumeroPredialParte10() {
        return numeroPredialParte10;
    }

    public void setNumeroPredialParte10(String numeroPredialParte10) {
        this.numeroPredialParte10 = numeroPredialParte10;
    }

    public String getNumeroPredialParte11() {
        return numeroPredialParte11;
    }

    public void setNumeroPredialParte11(String numeroPredialParte11) {
        this.numeroPredialParte11 = numeroPredialParte11;
    }

    public TramitePredioEnglobe getSelectedPredioTramite() {
        return this.selectedPredioTramite;
    }

    public void setSelectedPredioTramite(
        TramitePredioEnglobe selectedPredioTramite) {
        this.selectedPredioTramite = selectedPredioTramite;
    }

    public List<TramitePredioEnglobe> getPrediosTramite() {
        return this.prediosTramite;
    }

    public void setPrediosTramite(List<TramitePredioEnglobe> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public boolean isBanderaMostrarBotonImprimirConstanciaRadicacion() {
        return this.banderaMostrarBotonImprimirConstanciaRadicacion;
    }

    public void setBanderaMostrarBotonImprimirConstanciaRadicacion(
        boolean banderaMostrarBotonImprimirConstanciaRadicacion) {
        this.banderaMostrarBotonImprimirConstanciaRadicacion =
            banderaMostrarBotonImprimirConstanciaRadicacion;
    }

    public boolean isBanderaBuscarSolicitudes() {
        return banderaBuscarSolicitudes;
    }

    public List<VSolicitudPredio> getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(List<VSolicitudPredio> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public void setBanderaBuscarSolicitudes(boolean banderaBuscarSolicitudes) {
        this.banderaBuscarSolicitudes = banderaBuscarSolicitudes;
    }

    public VSolicitudPredio getFiltroBusquedaSolicitud() {
        return filtroBusquedaSolicitud;
    }

    public void setFiltroBusquedaSolicitud(
        VSolicitudPredio filtroBusquedaSolicitud) {
        this.filtroBusquedaSolicitud = filtroBusquedaSolicitud;
    }

    public List<Tramite> getTramitesSolicitud() {
        return tramitesSolicitud;
    }

    public void setTramitesSolicitud(List<Tramite> tramitesSolicitud) {
        this.tramitesSolicitud = tramitesSolicitud;
    }

    public void setEditandoSolicitud(boolean editandoSolicitud) {
        this.editandoSolicitud = editandoSolicitud;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }
    //v1.1.7

    public Solicitud getSolicitudAsociadaASolicitudSeleccionada() {
        return solicitudAsociadaASolicitudSeleccionada;
    }

    public void setSolicitudAsociadaASolicitudSeleccionada(
        Solicitud solicitudAsociadaASolicitudSeleccionada) {
        this.solicitudAsociadaASolicitudSeleccionada = solicitudAsociadaASolicitudSeleccionada;
    }

    public SolicitanteSolicitud getSolicitanteSolicitudSeleccionado() {
        return solicitanteSolicitudSeleccionado;
    }

    public void setSolicitanteSolicitudSeleccionado(
        SolicitanteSolicitud solicitanteSolicitudSeleccionado) {
        this.solicitanteSolicitudSeleccionado = solicitanteSolicitudSeleccionado;
    }

    public SolicitanteTramite getSolicitanteTramiteSeleccionado() {
        return this.solicitanteTramiteSeleccionado;
    }

    public void setSolicitanteTramiteSeleccionado(
        SolicitanteTramite solicitanteTramiteSeleccionado) {
        this.solicitanteTramiteSeleccionado = solicitanteTramiteSeleccionado;
    }

    public void setDatosRectifComplDLModel(
        DualListModel<DatoRectificar> datosRectifComplDLModel) {
        this.datosRectifComplDLModel = datosRectifComplDLModel;
    }

    public Tramite getTramiteSeleccionado() {
        return this.tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public boolean isEditandoTramite() {
        return editandoTramite;
    }

    public void setEditandoTramite(boolean editandoTramite) {
        this.editandoTramite = editandoTramite;
    }

    public String getCriterio() {
        return criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    public FiltroDatosConsultaTramitesNotificacion getDatosConsultaTramite() {
        return datosConsultaTramite;
    }

    public void setDatosConsultaTramite(
        FiltroDatosConsultaTramitesNotificacion datosConsultaTramite) {
        this.datosConsultaTramite = datosConsultaTramite;
    }

    public String getNumResolucion() {
        return numResolucion;
    }

    public void setNumResolucion(String numResolucion) {
        this.numResolucion = numResolucion;
    }

    public List<TramiteDocumentacion> getDocumentosADigitalizar() {
        return documentosADigitalizar;
    }

    public void setDocumentosADigitalizar(
        List<TramiteDocumentacion> documentosADigitalizar) {
        this.documentosADigitalizar = documentosADigitalizar;
    }

    public void setDocumentacionADigitalizarBool(
        boolean documentacionADigitalizarBool) {
        this.documentacionADigitalizarBool = documentacionADigitalizarBool;
    }

    public boolean isDocumentacionADigitalizarBool() {
        return documentacionADigitalizarBool;
    }

    public List<Tramite> getListTramitesBuscadosPorCriterio() {
        return listTramitesBuscadosPorCriterio;
    }

    public void setListTramitesBuscadosPorCriterio(
        List<Tramite> listTramitesBuscadosPorCriterio) {
        this.listTramitesBuscadosPorCriterio = listTramitesBuscadosPorCriterio;
    }

    public Tramite[] getSelectedTramitesGubernativaBuscados() {
        return selectedTramitesGubernativaBuscados;
    }

    public void setSelectedTramitesGubernativaBuscados(
        Tramite[] selectedTramitesGubernativaBuscados) {
        this.selectedTramitesGubernativaBuscados = selectedTramitesGubernativaBuscados;
    }

    public List<Tramite> getTramitesGubernativaAll() {
        return tramitesGubernativaAll;
    }

    public void setTramitesGubernativaAll(List<Tramite> tramitesGubernativaAll) {
        this.tramitesGubernativaAll = tramitesGubernativaAll;
    }

    public List<String> getListTramResolucion() {
        return listTramResolucion;
    }

    public void setListTramResolucion(List<String> listTramResolucion) {
        this.listTramResolucion = listTramResolucion;
    }

    public String getNumSolicitudBusqueda() {
        return numSolicitudBusqueda;
    }

    public void setNumSolicitudBusqueda(String numSolicitudBusqueda) {
        this.numSolicitudBusqueda = numSolicitudBusqueda;
    }

    public String getNumRadicacionBusqueda() {
        return numRadicacionBusqueda;
    }

    public void setNumRadicacionBusqueda(String numRadicacionBusqueda) {
        this.numRadicacionBusqueda = numRadicacionBusqueda;
    }

    public List<Tramite> getTramitesConDocumentosFaltantes() {
        return tramitesConDocumentosFaltantes;
    }

    public void setTramitesConDocumentosFaltantes(
        List<Tramite> tramitesConDocumentosFaltantes) {
        this.tramitesConDocumentosFaltantes = tramitesConDocumentosFaltantes;
    }

    public List<Tramite> getTramitesConDocumentosFaltantesCompleta() {
        return tramitesConDocumentosFaltantesCompleta;
    }

    public void setTramitesConDocumentosFaltantesCompleta(
        List<Tramite> tramitesConDocumentosFaltantesCompleta) {
        this.tramitesConDocumentosFaltantesCompleta = tramitesConDocumentosFaltantesCompleta;
    }

    public Boolean getTramiteSeleccionadoBool() {
        return tramiteSeleccionadoBool;
    }

    public void setTramiteSeleccionadoBool(Boolean tramiteSeleccionadoBool) {
        this.tramiteSeleccionadoBool = tramiteSeleccionadoBool;
    }

    public Date getFechaComunicacionSolicitud() {
        return fechaComunicacionSolicitud;
    }

    public void setFechaComunicacionSolicitud(Date fechaComunicacionSolicitud) {
        this.fechaComunicacionSolicitud = fechaComunicacionSolicitud;
    }

    public Date getFechaFinalizacionDeEspera() {
        return fechaFinalizacionDeEspera;
    }

    public void setFechaFinalizacionDeEspera(Date fechaFinalizacionDeEspera) {
        this.fechaFinalizacionDeEspera = fechaFinalizacionDeEspera;
    }

    public Integer getTiempoAmpliacion() {
        return tiempoAmpliacion;
    }

    public void setTiempoAmpliacion(Integer tiempoAmpliacion) {
        this.tiempoAmpliacion = tiempoAmpliacion;
    }

    public Date getNuevaFechaFinalizacion() {
        return nuevaFechaFinalizacion;
    }

    public void setNuevaFechaFinalizacion(Date nuevaFechaFinalizacion) {
        this.nuevaFechaFinalizacion = nuevaFechaFinalizacion;
    }

    public void setDatosConsultaPredio(
        FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setFiltrosAdicionalesBool(boolean filtrosAdicionalesBool) {
        this.filtrosAdicionalesBool = filtrosAdicionalesBool;
    }

    public boolean isFiltrosAdicionalesBool() {
        return filtrosAdicionalesBool;
    }

    public Solicitante getSolicitanteBusqueda() {
        return solicitanteBusqueda;
    }

    public void setSolicitanteBusqueda(Solicitante solicitanteBusqueda) {
        this.solicitanteBusqueda = solicitanteBusqueda;
    }

    public String getSiguienteActividad() {
        return siguienteActividad;
    }

    public void setSiguienteActividad(String siguienteActividad) {
        this.siguienteActividad = siguienteActividad;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Duration getDuracion() {
        return duracion;
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public List<Predio> getListaPrediosColindantes() {
        return listaPrediosColindantes;
    }

    public void setListaPrediosColindantes(List<Predio> listaPrediosColindantes) {
        this.listaPrediosColindantes = listaPrediosColindantes;
    }

    public void setDuracion(Duration duracion) {
        this.duracion = duracion;
    }

    public TramiteDocumento getSolicitudDeDocumentos() {
        return solicitudDeDocumentos;
    }

    public void setSolicitudDeDocumentos(TramiteDocumento solicitudDeDocumentos) {
        this.solicitudDeDocumentos = solicitudDeDocumentos;
    }

    public String getSolicitudDocumentosUrlArchivo() {
        return solicitudDocumentosUrlArchivo;
    }

    public void setSolicitudDocumentosUrlArchivo(
        String solicitudDocumentosUrlArchivo) {
        this.solicitudDocumentosUrlArchivo = solicitudDocumentosUrlArchivo;
    }

    public boolean isSolicitudDocumentosBool() {
        return solicitudDocumentosBool;
    }

    public void setSolicitudDocumentosBool(boolean solicitudDocumentosBool) {
        this.solicitudDocumentosBool = solicitudDocumentosBool;
    }

    public boolean isAvanzarDeRecibirDocumentosBool() {
        return avanzarDeRecibirDocumentosBool;
    }

    public void setAvanzarDeRecibirDocumentosBool(
        boolean avanzarDeRecibirDocumentosBool) {
        this.avanzarDeRecibirDocumentosBool = avanzarDeRecibirDocumentosBool;
    }

    public boolean isMostrarPanelResultadoConsulta() {
        return mostrarPanelResultadoConsulta;
    }

    public void setMostrarPanelResultadoConsulta(boolean mostrarPanelResultadoConsulta) {
        this.mostrarPanelResultadoConsulta = mostrarPanelResultadoConsulta;
    }

    public boolean isRadicadoValidado() {
        return radicadoValidado;
    }

    public void setRadicadoValidado(boolean radicadoValidado) {
        this.radicadoValidado = radicadoValidado;
    }

    // -----------------------------------------------------------------------------------------------
    public boolean isFormaDePeticionDerechoDePeticion() {
        return formaDePeticionDerechoDePeticion;
    }

    public void setFormaDePeticionDerechoDePeticion(
        boolean formaDePeticionDerechoDePeticion) {
        this.formaDePeticionDerechoDePeticion = formaDePeticionDerechoDePeticion;
    }

    public boolean isFormaDePeticionTutela() {
        return formaDePeticionTutela;
    }

    public void setFormaDePeticionTutela(boolean formaDePeticionTutela) {
        this.formaDePeticionTutela = formaDePeticionTutela;
    }

    public boolean isFormaDeTramiteTramiteNuevo() {
        return formaDeTramiteTramiteNuevo;
    }

    public void setFormaDeTramiteTramiteNuevo(boolean formaDeTramiteTramiteNuevo) {
        this.formaDeTramiteTramiteNuevo = formaDeTramiteTramiteNuevo;
    }

    public boolean isFormaDeTramiteTramiteRadicado() {
        return formaDeTramiteTramiteRadicado;
    }

    public void setFormaDeTramiteTramiteRadicado(
        boolean formaDeTramiteTramiteRadicado) {
        this.formaDeTramiteTramiteRadicado = formaDeTramiteTramiteRadicado;
    }

    public boolean isActivarFormaDeTramite() {
        return activarFormaDeTramite;
    }

    public void setActivarFormaDeTramite(boolean activarFormaDeTramite) {
        this.activarFormaDeTramite = activarFormaDeTramite;
    }

    public boolean isActivarRelacionarTramite() {
        return activarRelacionarTramite;
    }

    public void setActivarRelacionarTramite(boolean activarRelacionarTramite) {
        this.activarRelacionarTramite = activarRelacionarTramite;
    }

    public boolean isMostrarAceptarDerechoPeticionOTutela() {
        return mostrarAceptarDerechoPeticionOTutela;
    }

    public void setMostrarAceptarDerechoPeticionOTutela(
        boolean mostrarAceptarDerechoPeticionOTutela) {
        this.mostrarAceptarDerechoPeticionOTutela = mostrarAceptarDerechoPeticionOTutela;
    }

    public boolean isAmpliarTiempo() {
        return ampliarTiempo;
    }

    public void setAmpliarTiempo(boolean ampliarTiempo) {
        this.ampliarTiempo = ampliarTiempo;
    }

    public boolean isDocumentoDerechoPeticionOTutela() {
        return documentoDerechoPeticionOTutela;
    }

    public void setDocumentoDerechoPeticionOTutela(
        boolean documentoDerechoPeticionOTutela) {
        this.documentoDerechoPeticionOTutela = documentoDerechoPeticionOTutela;
    }

//--------------Variables para los casos de uso de productos -------------------------------------
    public String getDocumentoSoporteProducto() {
        return documentoSoporteProducto;
    }

    public void setDocumentoSoporteProducto(String documentoSoporteProducto) {
        this.documentoSoporteProducto = documentoSoporteProducto;
    }

    public TipoDocumento getTipoDocumentoSoporteSeleccionado() {
        return tipoDocumentoSoporteSeleccionado;
    }

    public void setTipoDocumentoSoporteSeleccionado(
        TipoDocumento tipoDocumentoSoporteSeleccionado) {
        this.tipoDocumentoSoporteSeleccionado = tipoDocumentoSoporteSeleccionado;
    }

    public Documento getNuevoDocumentoSoporte() {
        return nuevoDocumentoSoporte;
    }

    public void setNuevoDocumentoSoporte(Documento nuevoDocumentoSoporte) {
        this.nuevoDocumentoSoporte = nuevoDocumentoSoporte;
    }

    public Documento getNuevoDocumentoSoporteSeleccionado() {
        return nuevoDocumentoSoporteSeleccionado;
    }

    public void setNuevoDocumentoSoporteSeleccionado(
        Documento nuevoDocumentoSoporteSeleccionado) {
        this.nuevoDocumentoSoporteSeleccionado = nuevoDocumentoSoporteSeleccionado;
    }

    public List<Documento> getDocumentosSoportesAdicionados() {
        return documentosSoportesAdicionados;
    }

    public void setDocumentosSoportesAdicionados(
        List<Documento> documentosSoportesAdicionados) {
        this.documentosSoportesAdicionados = documentosSoportesAdicionados;
    }

    public List<ProductoCatastral> getProductosCatastralesAsociados() {
        return productosCatastralesAsociados;
    }

    public void setProductosCatastralesAsociados(
        List<ProductoCatastral> productosCatastralesAsociados) {
        this.productosCatastralesAsociados = productosCatastralesAsociados;
    }

    public ProductoCatastral getProductoCatastralSeleccionado() {
        return productoCatastralSeleccionado;
    }

    public void setProductoCatastralSeleccionado(
        ProductoCatastral productoCatastralSeleccionado) {
        this.productoCatastralSeleccionado = productoCatastralSeleccionado;
    }

    public Producto getProductoSeleccionado() {
        return productoSeleccionado;
    }

    public void setProductoSeleccionado(Producto productoSeleccionado) {
        this.productoSeleccionado = productoSeleccionado;
    }

    public GrupoProducto getGrupoProductoSeleccionado() {
        return grupoProductoSeleccionado;
    }

    public void setGrupoProductoSeleccionado(GrupoProducto grupoProductoSeleccionado) {
        this.grupoProductoSeleccionado = grupoProductoSeleccionado;
    }

    public List<SelectItem> getProductoPorGrupoV() {
        return productoPorGrupoV;
    }

    public void setProductoPorGrupoV(List<SelectItem> productoPorGrupoV) {
        this.productoPorGrupoV = productoPorGrupoV;
    }

    public boolean isEditarProductoCatastral() {
        return editarProductoCatastral;
    }

    public void setEditarProductoCatastral(boolean editarProductoCatastral) {
        this.editarProductoCatastral = editarProductoCatastral;
    }

    public boolean isActivarPanelProductosAsociados() {
        return activarPanelProductosAsociados;
    }

    public void setActivarPanelProductosAsociados(
        boolean activarPanelProductosAsociados) {
        this.activarPanelProductosAsociados = activarPanelProductosAsociados;
    }

    public boolean isActivarOpcionesDocumentosSoportes() {
        return activarOpcionesDocumentosSoportes;
    }

    public void setActivarOpcionesDocumentosSoportes(
        boolean activarOpcionesDocumentosSoportes) {
        this.activarOpcionesDocumentosSoportes = activarOpcionesDocumentosSoportes;
    }

    public boolean isActivarOpcionesProductosAsociados() {
        return activarOpcionesProductosAsociados;
    }

    public void setActivarOpcionesProductosAsociados(
        boolean activarOpcionesProductosAsociados) {
        this.activarOpcionesProductosAsociados = activarOpcionesProductosAsociados;
    }

    public boolean isBanderaPredioFiscal() {
        return banderaPredioFiscal;
    }

    public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
        this.banderaPredioFiscal = banderaPredioFiscal;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }
    //-----------------------------------------------------------------------------------------------

    /*
     * @modified by juanfelipe.garcia : 08-08-2013 adición de booleano para controlar el render en
     * pagina
     */
    @PostConstruct
    public void init() {

        this.documentosDelTramiteTemporales = new ArrayList<Documento>();
        this.tramiteDocumentacionTemporales = new ArrayList<TramiteDocumentacion>();
        this.solicitudDocumentacionTemporales = new ArrayList<SolicitudDocumentacion>();

        this.documentoSoporteEliminado = false;
        this.ampliarTiempo = false;
        this.radicadoValidado = false;
        this.mostrarPanelResultadoConsulta = false;
        this.filtroBusquedaSolicitud = new VSolicitudPredio();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        //datos para delegacion
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.municipiosDelegados = this.getGeneralesService().obtenerMunicipiosDelegados();

        this.solicitudSeleccionada = new Solicitud();
        this.banderaBuscarSolicitudes = false;
        this.banderaBuscarSolicitantes = false;
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.numPredialBool = false;
        this.banderaNoGeograficoEnglobe = false;
        this.banderaPredioFiscal = false;

        this.banderaSolicitanteSolicitudSeleccionado = false;
        this.docAmpliarTiempoRecibido = false;

        // Asigna el valor por defecto a persona natural en el inicio de la forma
        this.tipoPersona = 0;
        this.tipoPersonaN = true;

        // Es necesario el incializar los componentes de una nueva solicitud
        nuevaSolicitud();

        // Método usado para el cargue de los trámites con documentación
        // incompleta
        // éste método se usa cuado se elige la opción del menú
        // "Recibir documentos"
        recibirDocumentos();

        //lorena.salamanca:: Se cambian valores por defecto #2589
        this.banderaNuevoArchivo = false;

        this.banderaDocumentosFaltantes = false;

        this.documentoDerechoPeticionOTutela = false;

        this.banderaMostrarBotonImprimirConstanciaRadicacion = false;

        this.mostrarAceptarDerechoPeticionOTutela = false;

        this.existeRechazoPredio = false;

        //Consulta si para la solicitud existe 
    }

    //  -----------------------------------------------------------------------------------------------
    public DualListModel<DatoRectificar> getDatosRectifComplDLModel() {

        this.datosRectifComplSource = new ArrayList<DatoRectificar>();
        this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
        String naturaleza;

        if (this.tramiteSeleccionado != null &&
            this.tramiteSeleccionado.isRectificacion()) {
            naturaleza = EDatoRectificarNaturaleza.RECTIFICACION.geCodigo();
        } else {
            naturaleza = EDatoRectificarNaturaleza.COMPLEMENTACION.geCodigo();
        }

        if (this.tipoDatoRectifComplSeleccionado != null &&
            this.tipoDatoRectifComplSeleccionado.compareToIgnoreCase("") != 0 &&
            naturaleza != null && !naturaleza.isEmpty()) {
            this.datosRectifComplSource = this.getTramiteService()
                .obtenerDatosRectificarPorTipoYNaturaleza(
                    this.tipoDatoRectifComplSeleccionado, naturaleza);
        }

        //Se remueven algunos datos en caso de que el predio seleccionado sea fiscal y sean rectificaciones
        if (this.tramiteSeleccionado.getPredio() != null &&
            this.tramiteSeleccionado.getPredio().getTipoCatastro().equals(
                EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo()) &&
            EDatoRectificarNaturaleza.RECTIFICACION.geCodigo().equals(naturaleza)) {

            for (Iterator<DatoRectificar> it = this.datosRectifComplSource.iterator(); it.hasNext();) {
                DatoRectificar dt = it.next();
                if ("Foto".equals(dt.getNombre()) ||
                    "Zona Geoeconómica".equals(dt.getNombre()) ||
                    "Zona física".equals(dt.getNombre()) ||
                    "Detalle calificación".equals(dt.getNombre())) {
                    it.remove();
                }
            }
        }

        this.datosRectifComplDLModel = new DualListModel<DatoRectificar>(
            this.datosRectifComplSource, this.datosRectifComplTarget);

        return this.datosRectifComplDLModel;
    }

    // ******* MOSTRAR O NO **************
    /**
     * El tab contenido de una solicitud no debe mostrarse si esta se encuentra en edición
     */
    public boolean mostrarTabViewContenidoSolicitud() {
        return !this.editandoSolicitud;
    }

    /**
     * No debe mostrarse si es una solicitud de Avisos
     *
     * @return
     */
    public boolean mostrarEnlaceRepresentanteLegalApoderado() {
        return !this.solicitudSeleccionada.isSolicitudAvisos();
    }

    // TODO :: fredy.wilches :: 29-07-2011 :: Hacer la consulta lazy ::
    // fredy.wilches
    /**
     * Método invocado desde el boton de búsqueda de solicitudes de la lista
     */
    public void buscarSolicitudes() {
        boolean validarF = validarFiltro();
        if (validarF) {

            if (this.solicitanteSeleccionadoTemp != null &&
                this.solicitanteSeleccionadoTemp.getTipoIdentificacion() != null &&
                !this.solicitanteSeleccionadoTemp
                    .getTipoIdentificacion().isEmpty()) {

                this.filtroBusquedaSolicitud
                    .setTipoIdentificacion(this.solicitanteSeleccionadoTemp
                        .getTipoIdentificacion());
            }

            this.lazySolicitudes = new LazyDataModel<VSolicitudPredio>() {

                /**
                 *
                 */
                private static final long serialVersionUID = 5741091518050862306L;

                @Override
                public List<VSolicitudPredio> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<VSolicitudPredio> answer = new ArrayList<VSolicitudPredio>();
                    populateSolicitudesLazyly(answer, sortField, sortOrder, filters, first, pageSize);

                    return answer;
                }
            };

            poblar();

            this.banderaBuscarSolicitudes = true;
        } else {
            String mensaje = "La búsqueda debe contener como mínimo un parámetro";
            this.addMensajeError(mensaje);
        }
    }

    public LazyDataModel<VSolicitudPredio> getLazySolicitudes() {
        return lazySolicitudes;
    }

    public void setLazySolicitudes(
        LazyDataModel<VSolicitudPredio> lazySolicitudes) {
        this.lazySolicitudes = lazySolicitudes;
    }

    // ------------------------------------------------------------------------
    private void populateSolicitudesLazyly(List<VSolicitudPredio> answer,
        String sortField, SortOrder sortOrder, Map<String, String> filters, int first, int pageSize) {

        int size;
        this.solicitudes = this.getTramiteService()
            .buscarSolicitudesPaginadasPorFiltro(
                this.filtroBusquedaSolicitud, sortField, sortOrder.toString(), filters, first);
        if (this.solicitudes != null) {
            size = this.solicitudes.size();
            for (int i = 0; i < size; i++) {
                answer.add(this.solicitudes.get(i));
            }
        }
    }

    // ------------------------------------------------------------------------
    public void poblar() {

        int count;
        count = this.getTramiteService().contarSolicitudesPaginadasPorFiltro(
            this.filtroBusquedaSolicitud);

        if (count > 0) {
            this.lazySolicitudes.setRowCount(count);
        } else {
            this.lazySolicitudes.setRowCount(0);
        }

    }

    /**
     * Método que valida los parametros de la busqueda
     *
     * @return
     */
    public boolean validarFiltro() {
        if (!"".equals(this.filtroBusquedaSolicitud.getNumeroPredial()) &&
            this.filtroBusquedaSolicitud.getNumeroPredial() != null) {
            return true;
        } else if (!"".equals(this.filtroBusquedaSolicitud.getNumeroIdentificacion()) &&
            this.filtroBusquedaSolicitud.getNumeroIdentificacion() != null) {
            return true;
        } else if (this.filtroBusquedaSolicitud.getTipoIdentificacion() != null) {
            if (!"".equals(this.filtroBusquedaSolicitud.getTipoIdentificacion())) {
                return true;
            }
        } else if (!"".equals(this.filtroBusquedaSolicitud.getDigitoVerificacion()) &&
            this.filtroBusquedaSolicitud.getDigitoVerificacion() != null) {
            return true;
        } else if (!"".equals(this.filtroBusquedaSolicitud.getPrimerNombre()) &&
            this.filtroBusquedaSolicitud.getPrimerNombre() != null) {
            return true;
        } else if (!"".equals(this.filtroBusquedaSolicitud.getSegundoNombre()) &&
            this.filtroBusquedaSolicitud.getSegundoNombre() != null) {
            return true;
        } else if (!"".equals(this.filtroBusquedaSolicitud.getPrimerApellido()) &&
            this.filtroBusquedaSolicitud.getPrimerApellido() != null) {
            return true;
        } else {
            return !"".equals(this.filtroBusquedaSolicitud.getSegundoApellido()) &&
                this.filtroBusquedaSolicitud.getSegundoApellido() != null;
        }
        return false;
    }

    /**
     * Método invocado desde el boton para crear una nueva Solicitud
     */
    public String nuevaSolicitud() {
        this.solicitudSeleccionada = new Solicitud();
        this.tramitesAlmacenados = new ArrayList<Long>();
        this.editandoSolicitud = true;
        this.banderaSolicitudCreada = false;
        this.numeroSolicitud = "";
        this.resolucionTramiteEnProyeccion = false;

        this.solicitudSeleccionada.setFolios(1);
        this.solicitudSeleccionada.setAnexos(1);
        //version 1.1.5 - javier.aponte_productos
        boolean isResponsableAtencionUsuario = false;
        boolean isFuncionarioRadicador = false;

        for (String rol : this.usuario.getRoles()) {
            if (ERol.RESPONSABLE_ATENCION_USUARIO.getRol().equalsIgnoreCase(rol)) {
                isResponsableAtencionUsuario = true;
            }
        }

        for (String rol : this.usuario.getRoles()) {
            if (ERol.FUNCIONARIO_RADICADOR.getRol().equalsIgnoreCase(rol)) {
                isFuncionarioRadicador = true;
            }
        }

        Dominio d;

        if (isResponsableAtencionUsuario && !isFuncionarioRadicador) {
            this.solicitudSeleccionada.setTipo(ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo());
            d = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
                EDominio.SOLICITUD_TIPO, ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo());
        } else {
            this.solicitudSeleccionada.setTipo(ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
            d = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
                EDominio.SOLICITUD_TIPO, ESolicitudTipo.TRAMITE_CATASTRAL.getCodigo());
        }

        this.solicitudSeleccionada.setSistemaEnvio("01");
        this.solicitudSeleccionada.setAsunto(d.getValor());

        // Valor por default para el documento correspondencia
        this.solicitudSeleccionada.setTipoDocumentoCorrespondencia("999");

        this.reporteConstanciaRadicacion = null;
        this.banderaMostrarBotonImprimirConstanciaRadicacion = false;

        this.formaDePeticionDerechoDePeticion = false;
        this.formaDePeticionTutela = false;
        this.formaDeTramiteTramiteNuevo = false;
        this.formaDeTramiteTramiteRadicado = false;
        this.activarFormaDeTramite = false;
        this.activarRelacionarTramite = false;
        this.activarFormaDeTramite = false;
        this.activarRelacionarTramite = false;
        this.mostrarAceptarDerechoPeticionOTutela = false;
        this.solicitudDocumentacion = null;
        //v1.1.7
        this.documentoDerechoPeticionOTutela = false;
        this.banderaMostrarTramiteRadicadoDerechoPeticionOTutela = false;
        this.documentoSeleccionado = new TramiteDocumentacion();
        this.documentoSeleccionado.setTipoDocumento(new TipoDocumento());

        return "detalleSolicitudGeneral";

    }

    /**
     * Método invocado para buscar una solicitud pendiente en Correspondencia, trae datos y los
     * setea en la solicitud seleccionada
     */
    /*
     * @modified by juanfelipe.garcia :: 12-02-2014 :: #6585 Habilitar el botón Imprimir @modified
     * by felipe.cadena :: 19-02-2014 :: #6686 se realizan modificaciones para trabajar con avisos
     * @modified by juanfelipe.garcia :: 28-05-2014 :: #7760 ajuste para solicitudes encontradas en
     * correspondencia
     */
    public void buscarCorrespondencia() {

        this.solicitudSeleccionada = this.getTramiteService()
            .obtenerSolicitudInternaCorrespondencia(this.numeroSolicitud, this.usuario);
        this.tramitesAlmacenados = new ArrayList<Long>();

        this.tramitesExistentes = new ArrayList<Long>();
        for (Tramite t : this.solicitudSeleccionada.getTramites()) {
            this.tramitesExistentes.add(t.getId());
        }

        boolean existenTramitesAsociados = false;

        if (this.solicitudSeleccionada != null) {

            if (this.solicitudSeleccionada.getTramites() != null &&
                !this.solicitudSeleccionada.getTramites().isEmpty()) {

                for (Tramite tTemp : this.solicitudSeleccionada.getTramites()) {
                    tramitesAlmacenados.add(tTemp.getId());
                }
                if (!tramitesAlmacenados.isEmpty()) {
                    existenTramitesAsociados = true;
                }
            }
            if (this.solicitudSeleccionada.getId() != null) {
                this.editandoSolicitud = false;
                this.banderaSolicitudCreada = true;
                if (ESolicitudTipo.AVISOS.getCodigo().equals(this.solicitudSeleccionada.getTipo())) {
                    SolicitudAvisoRegistro avisos = this.solicitudSeleccionada.
                        getSolicitudAvisoRegistro();

                    List<SolicitanteSolicitud> sols = this.getTramiteService().
                        obtenerSolicitantesSolicitud(this.solicitudSeleccionada.getId());
                    this.solicitudSeleccionada.setSolicitanteSolicituds(sols);

                    for (AvisoRegistroRechazo arr : this.solicitudSeleccionada.
                        getSolicitudAvisoRegistro().getAvisoRegistroRechazos()) {
                        Long pid = arr.getFirst().getPredio().getId();
                        Predio predioAux = this.getConservacionService().
                            obtenerPredioConDatosUbicacionPorId(pid);
                        arr.getAvisoRegistroRechazoPredios().get(0).setPredio(predioAux);
                        this.existeRechazoPredio = true;

                    }

                    if (avisos.getAvisosPendientes() > 0) {
                        this.banderaAvisosNoTerminados = true;
                    }

                    List<CirculoRegistralMunicipio> crm;
                    crm = this.getGeneralesService().
                        getCirculoRegistralMunicipioByCodigoCirculoRegistral(
                            this.solicitudSeleccionada.getSolicitudAvisoRegistro().
                                getCirculoRegistral().getCodigo());
                    if (crm != null && !crm.isEmpty()) {
                        this.departamentoAvisos = crm.get(0).getMunicipio().getDepartamento();
                    }

                }
                //v1.1.7 
                if (this.solicitudSeleccionada.getAsunto().contains(
                    ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor()) ||
                    this.solicitudSeleccionada.getAsunto().contains(ESolicitudFormaPeticion.TUTELA.
                        getValor())) {
                    if (this.solicitudSeleccionada.getAsociadaSolicitudNumero() != null &&
                        !this.solicitudSeleccionada.getAsociadaSolicitudNumero().trim().isEmpty()) {

                        if (existenTramitesAsociados) {
                            this.generarReporteConstanciaRadicacion();
                            this.banderaMostrarBotonImprimirConstanciaRadicacion = true;
                        }

                        this.solicitudAsociadaASolicitudSeleccionada = this.getTramiteService().
                            obtenerSolicitudInternaCorrespondencia(this.solicitudSeleccionada.
                                getAsociadaSolicitudNumero(), this.usuario);
                        this.banderaMostrarTramiteRadicadoDerechoPeticionOTutela = true;
                    } else if (this.solicitudSeleccionada.getTramites() != null &&
                        !this.solicitudSeleccionada.getTramites().isEmpty()) {
                        if (existenTramitesAsociados) {
                            this.generarReporteConstanciaRadicacion();
                            this.banderaMostrarBotonImprimirConstanciaRadicacion = true;
                        }
                    } else {
                        this.activarRelacionarTramite = true;
                    }
                } //version 1.1.5 - javier.aponte_productos
                //Esto no se hace para las solicitudes de productos
                else if (!this.solicitudSeleccionada.isSolicitudProductosCatastrales()) {
                    if (existenTramitesAsociados) {
                        this.generarReporteConstanciaRadicacion();
                        this.banderaMostrarBotonImprimirConstanciaRadicacion = true;
                    }
                }

                this.addMensajeInfo("Solicitud encontrada en SNC");
            } else {
                if (this.solicitudSeleccionada.getSolicitanteSolicituds() != null &&
                    !this.solicitudSeleccionada.getSolicitanteSolicituds().isEmpty() &&
                    this.solicitudSeleccionada.getSolicitanteSolicituds().get(0).getId() == null) {

                    if (this.solicitudSeleccionada.getSolicitanteSolicituds().get(0).
                        getSolicitante() != null) {
                        String nombre =
                            this.solicitudSeleccionada.getSolicitanteSolicituds().get(0).
                                getSolicitante().getNombreCompleto();
                        String observaciones = this.solicitudSeleccionada.getObservaciones();
                        observaciones = observaciones.concat(" " + "SOLICITANTE: " + nombre);
                        this.solicitudSeleccionada.setObservaciones(observaciones);
                    }

                    this.solicitudSeleccionada.getSolicitanteSolicituds().clear();

                }
                this.addMensajeInfo("Solicitud encontrada en Correspondencia");
            }

        } else {
            this.editandoSolicitud = true;
            this.solicitudSeleccionada = new Solicitud();
            this.addMensajeError("La solicitud buscada no existe");
        }
    }

    // TODO :: fredy.wilches :: 29-07-2011 :: Obtener los solicitantes de una
    // solicitud de forma ordenada para armar el respectivo arbol ::
    // fredy.wilches
    public TreeNode getArbolSolicitantesSolicitud() {
        if (this.arbolSolicitantesSolicitud == null) {
            this.arbolSolicitantesSolicitud = new DefaultTreeNode("root", null);
        }
        return this.arbolSolicitantesSolicitud;
    }

    public void nuevoSolicitanteSolicitud() {
        this.solicitanteSolicitudSeleccionado = new SolicitanteSolicitud();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método invocado desde el botón para crear un nuevo trámite Solicitud. Inicializa las
     * variables que se necesitan para manejar el nuevo
     */
    public void initNuevoTramiteSolicitud() {

        this.subtipoTramiteEsRequerido = false;
        this.tramiteSeleccionado = new Tramite();
        this.tramiteSeleccionado.setComisionado(ESiNo.NO.getCodigo());
        this.tramiteSeleccionado.setFuente(Constantes.FUENTE_DEFECTO);
        this.tramitesSolicitud = new ArrayList<Tramite>();
        this.editandoTramite = true;
        this.banderaTramitePAlmacenado = false;
        this.banderaClaseDeMutacionSeleccionada = false;
        this.banderaSubtipoEnglobe = false;
        this.municipioTramite = null;
        this.departamentoTramite = null;
        this.updateDepartamentosTramite();
        this.updateMunicipiosTramite();
        this.datosConsultaTramite = new FiltroDatosConsultaTramitesNotificacion();
        this.listTramResolucion = new ArrayList<String>();
        this.listTramitesBuscadosPorCriterio = new ArrayList<Tramite>();
        this.tramitesGubernativaAll = new ArrayList<Tramite>();
        this.documentacionADigitalizarBool = false;
        this.mostrarFechaRadTesoreria = false;
        this.fundamentoPeticionText = "Fundamento petición:";
        this.fundamentoPeticionLabel = "Fundamento de la petición";
        this.prediosTramite = new ArrayList<TramitePredioEnglobe>();

        // despues de mutaciones de 5ª
        this.numeroPredialParte1 = "";
        this.numeroPredialParte2 = "";
        this.numeroPredialParte3 = "";
        this.numeroPredialParte4 = "";
        this.numeroPredialParte5 = "";
        this.numeroPredialParte6 = "";
        this.numeroPredialParte7 = "";
        this.numeroPredialParte8 = "";
        this.numeroPredialParte9 = "";
        this.numeroPredialParte10 = "";
        this.numeroPredialParte11 = "";
        this.numeroPredialParte12 = "";

        if (this.solicitudSeleccionada != null && this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.AUTOAVALUO.getCodigo())) {
            this.banderaTipoSolicitudAvaluo = false;
        } else {
            this.banderaTipoSolicitudAvaluo = true;
        }

        if (this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.REVISION_AVALUO.getCodigo()) ||
            this.solicitudSeleccionada.getTipo().equals(
                ESolicitudTipo.AUTOAVALUO.getCodigo())) {
            this.fundamentoPeticionText = "Síntesis petición:";
            this.fundamentoPeticionLabel = "Síntesis de la petición";
            this.tramiteSeleccionado.setFuente(ETramiteFuente.DE_PARTE.getCodigo());

            // Mostrar el campo de Fecha de radicación en tesorería en la
            // pantalla de detalle del trámite.
            if (this.solicitudSeleccionada.getSolicitanteSolicituds() != null) {
                for (SolicitanteSolicitud ss : this.solicitudSeleccionada
                    .getSolicitanteSolicituds()) {
                    if (ss != null &&
                        ESolicitanteSolicitudRelac.TESORERIA.toString()
                            .equals(ss.getRelacion())) {
                        this.mostrarFechaRadTesoreria = true;
                    }
                }
            }
        } else {
            this.fundamentoPeticionText = "Fundamento petición:";
            this.fundamentoPeticionLabel = "Fundamento de la petición";
        }
        this.criterio = "";

        if (this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.AUTOAVALUO.getCodigo())) {
            this.tramiteSeleccionado
                .setTipoTramite(ETramiteTipoTramite.AUTOESTIMACION.getCodigo());
            this.tipoAvaluoSeleccionado = null;
        }

        if (this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.REVISION_AVALUO.getCodigo())) {
            this.tramiteSeleccionado
                .setTipoTramite(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo());
        }

        if (this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.VIA_GUBERNATIVA.getCodigo())) {
            this.tramiteSeleccionado
                .setTipoTramite(ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION
                    .getCodigo());
        }

        if (this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.REVOCATORIA_DIRECTA.getCodigo())) {
            this.tramiteSeleccionado
                .setTipoTramite(ETramiteTipoTramite.REVOCATORIA_DIRECTA
                    .getCodigo());
        }
    }

//--------------------------------------------------------------------------------------------------
    public void getDocumentacion() {
        List<TramiteDocumentacion> documentacion = new ArrayList<TramiteDocumentacion>();
        if (this.tramiteSeleccionado.getTipoTramite() != null &&
            this.tramiteSeleccionado.getClaseMutacion() != null) {
            List<TipoDocumento> tds = this.getGeneralesService()
                .getCacheTiposDocumentoPorTipoTramite(
                    this.tramiteSeleccionado.getTipoTramite(),
                    this.tramiteSeleccionado.getClaseMutacion(),
                    this.tramiteSeleccionado.getSubtipo());
            for (TipoDocumento td : tds) {
                TramiteDocumentacion d = new TramiteDocumentacion();
                d.setAportado(ESiNo.NO.getCodigo());
                d.setTipoDocumento(td);
                d.setFechaAporte(new Date(System.currentTimeMillis()));
                d.setAdicional(ESiNo.NO.getCodigo());
                d.setRequerido(ESiNo.SI.getCodigo());
                if (this.tramiteSeleccionado.getPredio() != null) {
                    d.setDepartamento(this.tramiteSeleccionado.getPredio()
                        .getDepartamento());
                    d.setMunicipio(this.tramiteSeleccionado.getPredio()
                        .getMunicipio());
                } else {
                    d.setDepartamento(new Departamento());
                    d.setMunicipio(new Municipio());
                }
                documentacion.add(d);
            }
        }
        this.tramiteSeleccionado.setTramiteDocumentacions(documentacion);
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método listener para el cambio de tipo de trámite
     */
    public void cambioTipoTramite() {

        this.banderaSubtipoEnglobe = false;
        if (this.tramiteSeleccionado.isEsComplementacion()) {
            this.textoBotonRectificacionC = Constantes.TEXTO_BOTON_COMPLEMENTACION;
            inicializarDatosRectComp();
        } else if (this.tramiteSeleccionado.isEsRectificacion()) {
            this.textoBotonRectificacionC = Constantes.TEXTO_BOTON_RECTIFICACION;
            inicializarDatosRectComp();
        }

        inicializacionTipoTramite();
        inicializacionBanderaSubtipoEnglobe();
        inicializacionVariablesTipoTramite();

    }

    public void inicializacionVariablesTipoTramite() {

        this.selectedPredioTramite = null;
        this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
        this.selectedDatosRectifComplDataTable = new ArrayList<DatoRectificar>();
        this.tramiteSeleccionado.setTramiteRectificacions(new ArrayList<TramiteRectificacion>());
    }

    // -------------------------------------------------------------------------
    /**
     * Método que inicializa los datos de complementación o rectificación
     */
    private void inicializarDatosRectComp() {

        this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        this.selectedDatosRectifComplDataTable = new ArrayList<DatoRectificar>();
    }

    // -------------------------------------------------------------------------
    /**
     * Inicialización de los datos de tipo de trámite
     */
    private void inicializacionTipoTramite() {

        if (this.tramiteSeleccionado.getTipoTramite() != null &&
            !this.tramiteSeleccionado.getTipoTramite().isEmpty()) {

            if (!this.tramiteSeleccionado.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo())) {

                this.tramiteSeleccionado.setClaseMutacion(null);
                this.tramiteSeleccionado.setSubtipo(null);
                this.subtipoTramiteEsRequerido = false;

            } else if (this.tramiteSeleccionado.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) &&
                this.tramiteSeleccionado.getClaseMutacion() != null &&
                !this.tramiteSeleccionado.getClaseMutacion().isEmpty() &&
                (!this.tramiteSeleccionado.getClaseMutacion().equals(
                    EMutacionClase.SEGUNDA.getCodigo()) || !this.tramiteSeleccionado
                .getClaseMutacion().equals(
                    EMutacionClase.QUINTA.getCodigo()))) {

                this.tramiteSeleccionado.setSubtipo(null);
            }

        }

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Listener del subtipo de mutación
     */
    public void actualizarDesdeSubtipo() {

        if (this.tramiteSeleccionado.getSubtipo().equals(
            EMutacion2Subtipo.ENGLOBE.getCodigo())) {
            this.banderaSubtipoEnglobe = true;
        } else {
            this.banderaSubtipoEnglobe = false;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Quita de la lista de predios del trámite el que se haya seleccionado dando click en el ícono
     *
     * @author pedro.garcia
     */
    public void removePredioFromListaPrediosTramite() {
        this.prediosTramite.remove(this.selectedPredioTramite);
        if (!this.prediosTramite.isEmpty()) {
            this.tramiteSeleccionado.setPredio(this.prediosTramite.get(0).getPredio());
        }
        if (this.selectedPredioTramite.getId() != null) {
            TramiteDetallePredio tdp =
                new TramiteDetallePredio(this.selectedPredioTramite);
            this.getTramiteService().removerTramiteDetallePredio(tdp);
        }

        if (this.listaGlobalPrediosColindantes != null &&
            !this.listaGlobalPrediosColindantes.isEmpty() &&
            this.listaGlobalPrediosColindantes
                .contains(this.selectedPredioTramite.getPredio())) {

            this.listaGlobalPrediosColindantes
                .remove(this.selectedPredioTramite.getPredio());
        }

        this.selectedPredioTramite = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * marca uno de los predios como principal
     *
     * @author pedro.garcia
     */
    public void markTramiteAsPrincipal() {
        LOGGER.info("markTramiteAsPrincipal action called");

        for (TramitePredioEnglobe tpe : this.prediosTramite) {
            tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
        }
        this.selectedPredioTramite.setEnglobePrincipal(ESiNo.SI.getCodigo());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Permite actualizar la lista de documentos adicionales
     */
    public void adicionarDocumentoAdicional() {

        if (this.documentacionAdicional == null ||
            this.documentacionAdicional.isEmpty()) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }
        this.documentoSeleccionado = new TramiteDocumentacion();
        this.documentoSeleccionado.setDocumentoSoporte(new Documento());

        TipoDocumento td = new TipoDocumento();
        this.documentoSeleccionado.setTipoDocumento(td);
        this.documentoSeleccionado.setAporta(true);
        if (this.departamentoTramite != null &&
            this.departamentoTramite.getCodigo() != null &&
            !this.departamentoTramite.getCodigo().isEmpty()) {
            this.departamentoDocumento = this.departamentoTramite;
        } else {
            this.departamentoDocumento = null;
        }
        if (this.municipioTramite != null &&
            this.municipioTramite.getCodigo() != null &&
            !this.municipioTramite.getCodigo().isEmpty()) {
            this.municipioDocumento = this.municipioTramite;
        } else {
            this.municipioDocumento = null;
        }
        this.rutaMostrar = new String();
        this.updateDepartamentosDocumento();
        this.updateMunicipiosDocumento();
        this.editandoDocumentacion = false;
        this.banderaDocumentoAdicional = true;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Inicializa los objetos para la edicion de un Documento requerido
     */
    public void nuevoDocumentoRequerido() {

        if (this.departamentoTramite != null &&
            this.departamentoTramite.getCodigo() != null &&
            !this.departamentoTramite.getCodigo().isEmpty()) {
            this.departamentoDocumento = this.departamentoTramite;
        } else {
            this.departamentoDocumento = null;
        }

        if (this.municipioTramite != null &&
            this.municipioTramite.getCodigo() != null &&
            !this.municipioTramite.getCodigo().isEmpty()) {
            this.municipioDocumento = this.municipioTramite;
        } else {
            this.municipioDocumento = null;
        }

        this.updateDepartamentosDocumento();
        this.updateMunicipiosDocumento();

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Contiene la lista de departamentos disponibles para Colombia
     *
     * @deprecated usar una lista de departamentos para cada uno de los requerimientos. Ver
     * departamentosAvisos.
     */
    @Deprecated
    public List<SelectItem> getDepartamentos() {
        List<SelectItem> departamentos = new ArrayList<SelectItem>();
        departamentos.add(new SelectItem(null, this.DEFAULT_COMBOS));

        this.dptosAvisos = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);

        if (this.getOrdenDepartamentosDocumento().equals(EOrden.CODIGO)) {
            Collections.sort(dptosAvisos);
        } else {
            Collections.sort(dptosAvisos, Departamento.getComparatorNombre());
        }
        for (Departamento d : dptosAvisos) {
            if (this.getOrdenDepartamentosDocumento().equals(EOrden.CODIGO)) {
                departamentos.add(new SelectItem(d.getCodigo(), d.getCodigo() +
                    "-" + d.getNombre()));
            } else {
                departamentos.add(new SelectItem(d.getCodigo(), d.getNombre()));
            }
        }

        return departamentos;
    }

    public void cambioTipoSolicitud() {
        this.solicitudSeleccionada.setSolicitudAvisoRegistro(null);
        if (this.solicitudSeleccionada.isSolicitudAvisos()) {
            updateDepartamentosAvisos();
            this.solicitudSeleccionada
                .setSolicitudAvisoRegistro(new SolicitudAvisoRegistro());
        }

        if (this.solicitudSeleccionada.getTipo().equals(
            ESolicitudTipo.REVISION_AVALUO.getCodigo())) {
            this.fundamentoPeticionText = "Síntesis petición:";
            this.fundamentoPeticionLabel = "Sintesis de la petición";
        } else {
            this.fundamentoPeticionText = "Fundamento petición:";
            this.fundamentoPeticionLabel = "Fundamento de la petición";
        }
        //lorena.salamanca:: Se cambian valores por defecto #2589
        Dominio d = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
            EDominio.SOLICITUD_TIPO, this.solicitudSeleccionada.getTipo());
        this.solicitudSeleccionada.setAsunto(d.getValor());

    }

//--------------------------------------------------------------------------------------------------
    public void cambioClaseMutacion() {

        // D: si es de segunda o quinta, se hace requerido escoger el subtipo de
        // trámite; y se pone el subtipo en el trámite seleccionado
        String claseMutacionSeleccionada;
        claseMutacionSeleccionada = this.tramiteSeleccionado.getClaseMutacion();

        if (claseMutacionSeleccionada
            .equals(EMutacionClase.SEGUNDA.getCodigo()) ||
            claseMutacionSeleccionada.equals(EMutacionClase.QUINTA.getCodigo())) {
            this.subtipoTramiteEsRequerido = true;
            this.banderaClaseDeMutacionSeleccionada = true;
            this.subtipoClaseMutacionV = this.generalMB
                .getSubtipoClaseMutacionV(claseMutacionSeleccionada);
            //N: aquí iba la inicializacion de los predios al cambiar la clase de mutacion
            //...
        } else {
            this.subtipoTramiteEsRequerido = false;
            this.tramiteSeleccionado.setSubtipo(null);
        }

        inicializacionBanderaSubtipoEnglobe();
    }

    // -------------------------------------------------------------------------
    /**
     * Inicialización de la bandera subtipo englobe
     */
    private void inicializacionBanderaSubtipoEnglobe() {

        if (this.tramiteSeleccionado.getClaseMutacion() != null &&
            !this.tramiteSeleccionado.getClaseMutacion().isEmpty() &&
            this.tramiteSeleccionado.getSubtipo() != null &&
            !this.tramiteSeleccionado.getSubtipo().isEmpty() &&
            this.tramiteSeleccionado.getSubtipo().equals(
                EMutacion2Subtipo.ENGLOBE.getCodigo())) {

            this.banderaSubtipoEnglobe = true;
        } else {
            this.banderaSubtipoEnglobe = false;
        }
    }

    /**
     * Getter de pla propiedad avisoRechazo
     *
     * @return
     */
    public AvisoRegistroRechazo getAvisoRechazo() {
        return avisoRechazo;
    }

    /**
     * Setter de pla propiedad avisoRechazo
     *
     * @param avisoRechazo
     */
    public void setAvisoRechazo(AvisoRegistroRechazo avisoRechazo) {
        this.avisoRechazo = avisoRechazo;
    }

    /**
     * Método que realiza los procesos asociados a descartar un aviso. Almacena la entidad
     * AvisoRegistroRechazo y sus respectivos AvisoRegistroRechazoPredios. Además de esto aumenta el
     * número de rechazos en la solicitudAvisoRegistro.
     *
     * @author fabio.navarrete
     */
    public void descartarAviso() {

        if (validarCamposAvisoRechazo()) {
            List<AvisoRegistroRechazoPredio> avisoRegistroRechazoPredios =
                new ArrayList<AvisoRegistroRechazoPredio>();
            for (Predio pred : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {
                AvisoRegistroRechazoPredio avisoRegistroRechazoPredio =
                    new AvisoRegistroRechazoPredio();
                avisoRegistroRechazoPredio
                    .setAvisoRegistroRechazo(this.avisoRechazo);
                avisoRegistroRechazoPredio.setNumeroPredial(pred
                    .getNumeroPredial());
                avisoRegistroRechazoPredio.setPredio(pred);
                avisoRegistroRechazoPredio.setFechaLog(new Date());
                avisoRegistroRechazoPredio.setUsuarioLog(this.usuario
                    .getLogin());
                avisoRegistroRechazoPredios.add(avisoRegistroRechazoPredio);
            }
            this.avisoRechazo
                .setAvisoRegistroRechazoPredios(avisoRegistroRechazoPredios);
            this.avisoRechazo
                .setSolicitudAvisoRegistro(this.solicitudSeleccionada
                    .getSolicitudAvisoRegistro());
            this.avisoRechazo.setUsuarioLog(this.usuario.getLogin());
            this.avisoRechazo.setFechaLog(new Date());

            Integer numRechazos = this.solicitudSeleccionada
                .getSolicitudAvisoRegistro().getAvisosRechazados();
            this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .setAvisosRechazados(++numRechazos);
            this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .getAvisoRegistroRechazos().add(this.avisoRechazo);
            this.solicitudSeleccionada = this.getTramiteService()
                .guardarActualizarSolicitud(this.usuario,
                    this.solicitudSeleccionada, this.tramitesExistentes);

            this.fundamentoPeticionText = "Fundamento petición:";
            this.fundamentoPeticionLabel = "Fundamento de la petición";
            this.existeRechazoPredio = true;
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método que realiza la validación de campos requerida para rechazar un aviso
     *
     * @author juan.agudelo
     */
    private boolean validarCamposAvisoRechazo() {

        if (this.avisoRechazo.getObservaciones().length() > 2000) {

            RequestContext context = RequestContext.getCurrentInstance();

            String mensaje = "Observaciones: Error de validación: el tamaño máximo para el campo" +
                " es de 2000 caracteres.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("descartarAvisoForm:observaciones", mensaje);

            return false;
        } else {
            return true;
        }
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 12-08-2013 refactor de nombre: decía que tomaba predios del mapa
     */
    private List<Predio> getPrediosSeleccionadosDeComponenteSeleccPredios() {
        return this.consultaPredioMB.getPrediosSeleccionados2();
    }
//--------------------------------------------------------------------------------------------------

    // ----------- rectificación/complementación
    /**
     * action del botón "Adicionar datos seleccionados". Adiciona los datos seleccionados del
     * picklist de datos a rectificar o complementar a la variable que los guarda
     *
     * @author pedro.garcia
     */
    public void adicionarDatosRectifCompl() {

        LOGGER.debug("adicionando datos seleccionados...");

        // D: la primera vez que entra aquí este atributo es nulo, porque nadie
        // lo ha inicializado
        if (this.selectedDatosRectifComplPL == null) {
            this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        } else {
            this.selectedDatosRectifComplPL.clear();
        }

        // OJO: no se usa la lista target, sino el atributo target del DualListModel
        if (this.datosRectifComplDLModel.getTarget().isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar el tipo de rectificación o complementación, y luego los datos a modificar");
            return;
        }

        for (DatoRectificar drTarget : this.datosRectifComplDLModel.getTarget()) {

            //D: si se va a rectificar por 'cancelación por doble inscripción' este dato, 
            //   debe ser el único seleccionado
            if ((drTarget.isDatoRectifCDI() &&
                !this.selectedDatosRectifComplDataTable.isEmpty() &&
                !contieneDatoRectificarCDI(this.selectedDatosRectifComplDataTable)) ||
                (!drTarget.isDatoRectifCDI() && contieneDatoRectificarCDI(
                this.selectedDatosRectifComplDataTable))) {

                this.addMensajeError(
                    "La rectificación de cancelación por doble inscripción debe ser única");
                this.selectedDatosRectifComplDataTable.clear();
                return;
            } else {
                if (!this.selectedDatosRectifComplDataTable.contains(drTarget)) {
                    this.selectedDatosRectifComplPL.add(drTarget);
                }
            }
        }

        this.selectedDatosRectifComplDataTable.addAll(this.selectedDatosRectifComplPL);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Retirar datos". Retira de la lista de datos para rectificación o
     * complementación los que se hayan seleccionado de la tabla resumen
     *
     * @author pedro.garcia
     */
    public void retirarDatoRectifCompl() {

        LOGGER.debug("retirando datos seleccionados...");

        for (DatoRectificar dtR : this.selectedDatosRectifComplDT) {
            if (this.selectedDatosRectifComplDataTable.contains(dtR)) {
                this.selectedDatosRectifComplDataTable.remove(dtR);
            }
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que inicializa las variables al cerrar la ventana de detalle de solicitud
     *
     * @author juan.agudelo
     */
    public void cerrarDetalleSolicitudGeneral(CloseEvent close) {
        this.numeroSolicitud = "";
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de persona
     */
    public void tipoPersonaSel() {
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        if (this.tipoPersona == 0) {
            this.tipoPersonaN = true;
            this.banderaSolicitanteSolicitudSeleccionado = false;
            this.filtroBusquedaSolicitud.setNumeroIdentificacion("");
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo());
            this.solicitudes = new ArrayList<VSolicitudPredio>();
            this.banderaBuscarSolicitantes = false;
            this.banderaBuscarSolicitudes = false;
            this.banderaSolicitudSeleccionada = false;
        } else if (this.tipoPersona == 1) {
            this.tipoPersonaN = false;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT
                    .getCodigo());
            this.banderaSolicitanteSolicitudSeleccionado = false;
            this.filtroBusquedaSolicitud.setNumeroIdentificacion("");
            this.solicitudes = new ArrayList<VSolicitudPredio>();
            this.banderaBuscarSolicitantes = false;
            this.banderaBuscarSolicitudes = false;
            this.banderaSolicitudSeleccionada = false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton buscar solicitante
     */
    public void buscarSolicitantes() {
        boolean validarFS = validarFiltroSolicitantes();
        if (validarFS) {
            try {
                this.solicitantesRes = this.getTramiteService()
                    .buscarSolicitantesPorFiltro(
                        this.filtroDatosConsultaSolicitante, false);

                this.banderaBuscarSolicitantes = true;

                this.solicitudes = new ArrayList<VSolicitudPredio>();
                this.banderaBuscarSolicitudes = false;
                this.banderaSolicitanteSolicitudSeleccionado = false;

            } catch (Exception e) {
                LOGGER.debug(e.getMessage(), e);
                String mensaje = "La busqueda no pudo ser procesada";
                this.addMensajeError(mensaje);
            }
        } else {
            String mensaje = "La búsqueda debe contener como mínimo un parámetro";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que valida los parametros de la busqueda de solicitantes
     *
     * @return
     */
    public boolean validarFiltroSolicitantes() {
        if (!"".equals(this.filtroDatosConsultaSolicitante.getNumeroIdentificacion()) &&
            this.filtroDatosConsultaSolicitante.getNumeroIdentificacion() != null) {
            return true;
        } else if (this.filtroDatosConsultaSolicitante.getTipoIdentificacion() != null &&
            !"".equals(this.filtroDatosConsultaSolicitante.getTipoIdentificacion())) {
            return true;
        } else if (this.filtroDatosConsultaSolicitante.getDigitoVerificacion() != null &&
            !"".equals(this.filtroDatosConsultaSolicitante.getDigitoVerificacion())) {
            return true;
        } else if (this.filtroDatosConsultaSolicitante.getPrimerNombre() != null &&
            !"".equals(this.filtroDatosConsultaSolicitante.getPrimerNombre())) {
            return true;
        } else if (this.filtroDatosConsultaSolicitante.getSegundoNombre() != null &&
            !"".equals(this.filtroDatosConsultaSolicitante.getSegundoNombre())) {
            return true;
        } else if (this.filtroDatosConsultaSolicitante.getPrimerApellido() != null &&
            !"".equals(this.filtroDatosConsultaSolicitante.getPrimerApellido())) {
            return true;
        } else if (this.filtroDatosConsultaSolicitante.getSegundoApellido() != null &&
            !"".equals(this.filtroDatosConsultaSolicitante.getSegundoApellido())) {
            return true;
        } else {
            return false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método de escucha para la visualizacion del panel de busqueda de solicitud
     *
     * @return
     */
    public void seleccionarSolicitanteSolicitud() {

        this.banderaSolicitanteSolicitudSeleccionado = true;
        this.filtroBusquedaSolicitud
            .setNumeroIdentificacion(this.solicitanteSeleccionadoTemp
                .getNumeroIdentificacion());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método de escucha para la visualizacion del panel de busqueda de solicitud
     *
     * @return
     */
    public void desSeleccionarSolicitanteSolicitud() {

        this.banderaSolicitanteSolicitudSeleccionado = false;
        this.filtroBusquedaSolicitud.setNumeroIdentificacion("");
        this.solicitudes = new ArrayList<VSolicitudPredio>();
        this.banderaBuscarSolicitudes = false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método de escucha para la visualizacion del boton de asignación de solicitud
     *
     * @return
     */
    public void accionSeleccionarSolicitud(SelectEvent seleccion) {

        this.sTemp = (VSolicitudPredio) seleccion.getObject();
        this.banderaSolicitudSeleccionada = true;
        this.solicitudSeleccionada.setId(sTemp.getSolicitudId());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método de escucha para la visualizacion del boton de asignación de solicitud
     *
     * @return
     */
    public void accionDesSeleccionarSolicitud(UnselectEvent desSeleccion) {
        this.solicitudSeleccionada = new Solicitud();
        this.banderaSolicitudSeleccionada = false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al seleccionar una solicitud
     *
     * @return
     */
    public String mostrarSolicitud() {
        this.editandoSolicitud = false;
        this.solicitudSeleccionada = this.getTramiteService()
            .obtenerSolicitudCompletaInternaCorrespondencia(
                this.sTemp.getNumeroRadicacionSolicitud(), this.usuario);
        if (this.solicitudSeleccionada != null &&
            this.solicitudSeleccionada.getId() != null &&
            this.solicitudSeleccionada.getId() != 0) {
            this.tramitesSolicitud = this.solicitudSeleccionada.getTramites();
        }
        this.editandoSolicitud = false;
        this.banderaSolicitudCreada = true;
        this.generarReporteConstanciaRadicacion();
        this.banderaMostrarBotonImprimirConstanciaRadicacion = true;
        return "detalleSolicitudGeneral";
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo utilizado para guardar documentos adicionales y ejecutado en la ventana modal de
     * nuevoDocumentoTramite como accion sobre el boton Aceptar
     */
    public void guardarDocumentacion() {

        if (this.banderaDocumentoAdicional) {
            int i = 0;
            if (this.rutaMostrar.length() > 1) {
                i++;
            }
            if (this.documentoSeleccionado.isDigitalizable()) {
                i++;
            }
            if (this.documentoSeleccionado.isAnalogo()) {
                i++;
            }

            if (i == 0) {
                this.addMensajeError(
                    "Debe cargar un documento o determinar si es análogo o por digitalizar");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        }

        if (!this.documentoDerechoPeticionOTutela) {

            if (this.documentacionTramite == null) {
                this.documentacionTramite = new ArrayList<TramiteDocumentacion>();
            }
            boolean valido = this.validarDocumentoTramite();

            if (valido == true) {

                guardarDocumento();

                this.banderaDocumentoAdicional = false;
                this.editandoDocumentacion = false;

                this.archivoResultado = new File("");
                this.rutaMostrar = "";

                this.verEstadoDelTramiteDocumentacionADigitalizar();
            } else if (this.documentoReplica != null &&
                this.documentacionRequerida != null &&
                !this.documentacionRequerida.isEmpty()) {
                boolean estaEnRadicacion = false;
                TramiteDocumentacion tdAuxRemove = new TramiteDocumentacion();
                for (TramiteDocumentacion td : this.documentacionRequerida) {
                    if (td.getId() == null) {
                        estaEnRadicacion = true;
                        break;
                    }
                    if (td.getId().longValue() == this.documentoReplica
                        .getId().longValue()) {
                        tdAuxRemove = td;
                    }
                }
                if (!estaEnRadicacion) {
                    this.documentacionRequerida.remove(tdAuxRemove);
                    this.documentacionRequerida.add(this.documentoReplica);
                    this.documentoReplica = null;
                }
            }
        } else {

            this.guardarDocumentoDerechoPeticionOTutela();

            this.banderaDocumentoAdicional = false;
            this.editandoDocumentacion = false;

            this.rutaMostrar = "";

        }
    }

    /**
     * Método para guardar el documento de derecho de petición o tutela y asociarlo a la solicitud
     *
     * @author javier.aponte
     */
    public void guardarDocumentoDerechoPeticionOTutela() {

        ConsultaTramiteMB mbConsultaTramite = (ConsultaTramiteMB) UtilidadesWeb.getManagedBean(
            "consultaTramite");
        //v1.1.7
        String fileSeparator = System.getProperty("file.separator");

        this.solicitudDocumentacion = new SolicitudDocumentacion();

        this.solicitudDocumentacion.setSolicitud(this.solicitudSeleccionada);
        this.solicitudDocumentacion.setFechaAporte(new Date());
        this.solicitudDocumentacion.setUsuarioLog(this.usuario.getLogin());
        this.solicitudDocumentacion.setFechaLog(new Date(System.currentTimeMillis()));
        this.solicitudDocumentacion.setAportado(ESiNo.SI.getCodigo());

        // Verificar el valor de los folios
        if (!this.documentoSeleccionado.isAnalogo() && !this.documentoSeleccionado.isDigitalizable() &&
            this.rutaMostrar.trim().isEmpty()) {
            this.documentoSeleccionado.setCantidadFolios(0);
        }

        // Buscar el tipo de documento seleccionado.
        if (this.documentoSeleccionado.getTipoDocumento() != null && this.documentoSeleccionado.
            getTipoDocumento().getId() != null) {
            TipoDocumento td = getGeneralesService().buscarTipoDocumentoPorId(
                this.documentoSeleccionado.getTipoDocumento().getId());
            if (td != null) {
                this.documentoSeleccionado.setTipoDocumento(td);
                this.solicitudDocumentacion.setTipoDocumento(td);
            }
        }

        Documento doc = this.generarDocumento(this.documentoSeleccionado);
        this.documentoSeleccionado.setDocumentoSoporte(doc);

        doc.setTramiteId(mbConsultaTramite.getSelectedTramiteRow().getId());

        if (doc.getArchivo() != null && !doc.getArchivo().trim().isEmpty()) {
            doc.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        } else {
            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
        }

        if (doc.getArchivo() != null && !doc.getArchivo().equals(
            Constantes.CONSTANTE_CADENA_VACIA_DB)) {

            DocumentoSolicitudDTO datosGestorDocumental =
                DocumentoSolicitudDTO.crearArgumentoCargarSolicitudAvaluoComercial(
                    this.solicitudSeleccionada.getNumero(), this.solicitudSeleccionada.getFecha(),
                    doc.getArchivo(),
                    UtilidadesWeb.obtenerRutaTemporalArchivos() + fileSeparator + doc.getArchivo());

            // Subo el archivo a Alfresco y actualizo el documento
            doc = this.getGeneralesService().guardarDocumentosSolicitudAvaluoAlfresco(
                this.usuario, datosGestorDocumental, doc);
        } else {

            doc = this.getTramiteService().guardarYactualizarDocumento(doc);

        }

        if (doc != null) {
            this.solicitudDocumentacion.setSoporteDocumentoId(doc);

            this.documentoSeleccionado.setDocumentoSoporte(doc);
            this.documentoSeleccionado.setFechaLog(new Date());
            this.documentoSeleccionado.setUsuarioLog(this.usuario.getLogin());
            this.documentoSeleccionado.setAportado(ESiNo.SI.getCodigo());
            this.documentoSeleccionado.setAdicional(ESiNo.SI.getCodigo());
            this.documentoSeleccionado.setRequerido(ESiNo.NO.getCodigo());
            this.documentoSeleccionado.setIdRepositorioDocumentos(doc.getIdRepositorioDocumentos());

            this.documentoSeleccionado = this.getTramiteService().guardarTramiteDocumentacion(
                this.documentoSeleccionado);
            this.solicitudDocumentacion = this.getTramiteService().
                guardarActualizarSolicitudDocumentacion(solicitudDocumentacion);

            this.documentosDelTramiteTemporales.add(doc);
            this.tramiteDocumentacionTemporales.add(this.documentoSeleccionado);
            this.solicitudDocumentacionTemporales.add(this.solicitudDocumentacion);

        }

        this.documentoSeleccionado = new TramiteDocumentacion();
        this.documentoSeleccionado.setTipoDocumento(new TipoDocumento());
        this.seleccionarTramiteDerechoPeticionOTutelaListener();

        this.mostrarAceptarDerechoPeticionOTutela = true;

    }

    /**
     * Método encargado de marcar un trámite como derecho de petición o tutela
     *
     * @author javier.aponte
     */
    public void marcarTramiteComoDerechoPeticionOTutela() {

        ConsultaTramiteMB mbConsultaTramite = (ConsultaTramiteMB) UtilidadesWeb.getManagedBean(
            "consultaTramite");

        Tramite tramiteSeleccionadoDerechoPeticionOTutela = mbConsultaTramite.
            getSelectedTramiteRow();

        if (this.formaDePeticionDerechoDePeticion) {
            tramiteSeleccionadoDerechoPeticionOTutela.setFormaPeticion(
                ESolicitudFormaPeticion.DERECHO_DE_PETICION.getCodigo());
        }
        if (this.formaDePeticionTutela) {
            tramiteSeleccionadoDerechoPeticionOTutela.setFormaPeticion(
                ESolicitudFormaPeticion.TUTELA.getCodigo());
        }

        this.getTramiteService().actualizarTramite(tramiteSeleccionadoDerechoPeticionOTutela,
            usuario);

        List<Tramite> tramites = new ArrayList<Tramite>();

        tramites.add(tramiteSeleccionadoDerechoPeticionOTutela);
        //v1.1.7
        this.solicitudSeleccionada.setAsociadaSolicitudNumero(
            tramiteSeleccionadoDerechoPeticionOTutela.getSolicitud().getNumero());

        this.solicitudAsociadaASolicitudSeleccionada = new Solicitud();

        this.solicitudAsociadaASolicitudSeleccionada.setTramites(tramites);

        this.banderaMostrarTramiteRadicadoDerechoPeticionOTutela = true;

        this.finalizarSolicitud();
    }

    /**
     * Método encargado de enviar correo al director territorial y al responsable de conservación
     * informando que el trámite se marcó como derecho de petición o tutela
     *
     * @author javier.aponte
     */
    public void enviarCorreoInformandoDerechoPeticionOTutela() {

        List<String> destinatariosList = new ArrayList<String>();

        ConsultaTramiteMB mbConsultaTramite = (ConsultaTramiteMB) UtilidadesWeb.getManagedBean(
            "consultaTramite");

        Tramite tramiteSeleccionadoDerechoPeticionOTutela = mbConsultaTramite.
            getSelectedTramiteRow();

        // Se ajustan y setean los parámetros referentes a la aprobación y el
        // envio de correo electrónico.
        try {

            UsuarioDTO responsableConservacionODirectorTerritorial =
                this.getGeneralesService().
                    obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(
                        tramiteSeleccionadoDerechoPeticionOTutela, this.usuario);

            // Agregar responsable de conservación asociado al trámite
            if (responsableConservacionODirectorTerritorial != null &&
                responsableConservacionODirectorTerritorial.getLogin() != null &&
                !responsableConservacionODirectorTerritorial.getLogin().trim().isEmpty()) {
                destinatariosList.add(responsableConservacionODirectorTerritorial.getEmail());
            }

            String[] destinatarios = destinatariosList
                .toArray(new String[destinatariosList.size()]);

            // Remitente
            String remitente = this.usuario.getEmail();

            // Asunto y el contenido del correo electrónico.
            String asunto = null;
            if (this.formaDePeticionDerechoDePeticion) {
                asunto = ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor();
            }
            if (this.formaDePeticionTutela) {
                asunto = ESolicitudFormaPeticion.TUTELA.getValor();
            }

            // Plantilla de contenido
            String codigoPlantillaContenido = EPlantilla.MENSAJE_DERECHO_PETICION_O_TUTELA.
                getCodigo();

            // Parámetros de la plantilla del asunto
            String[] parametrosPlantillaAsunto = null;

            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaContenido = new String[6];
            parametrosPlantillaContenido[0] = tramiteSeleccionadoDerechoPeticionOTutela.
                getNumeroRadicacion();//numero radicado
            parametrosPlantillaContenido[1] = formatoFecha.format(
                tramiteSeleccionadoDerechoPeticionOTutela.getFechaRadicacion());//fecha de radicación
            parametrosPlantillaContenido[2] = tramiteSeleccionadoDerechoPeticionOTutela.
                getTipoTramiteCadenaCompleto();//tipo tramite
            parametrosPlantillaContenido[3] = tramiteSeleccionadoDerechoPeticionOTutela.
                getClaseMutacion() != null ? " " + tramiteSeleccionadoDerechoPeticionOTutela.
                        getClaseMutacion() : "";//clase mutación
            parametrosPlantillaContenido[4] =
                tramiteSeleccionadoDerechoPeticionOTutela.getSubtipo() != null ? " " +
                tramiteSeleccionadoDerechoPeticionOTutela.getSubtipo() : "";//subtipo

            String mensajePredios = "";
            List<Predio> predios = tramiteSeleccionadoDerechoPeticionOTutela.getPredios();
            for (Iterator<Predio> iterator = predios.iterator(); iterator.hasNext();) {
                Predio presioIterado = iterator.next();
                mensajePredios = mensajePredios + presioIterado.getNumeroPredial();
                if (iterator.hasNext()) {
                    mensajePredios = mensajePredios + ", ";
                }
            }
            parametrosPlantillaContenido[5] = mensajePredios;//numero predial 1>, <número predial 2>

            /* if(this.formaDePeticionDerechoDePeticion){ parametrosPlantillaContenido[2] =
             * ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor(); }
             * if(this.formaDePeticionTutela){ parametrosPlantillaContenido[2] =
             * ESolicitudFormaPeticion.TUTELA.getValor(); }
             */
            // Listado de archivos adjuntos
            String[] adjuntos = null;
            String[] titulosAdjuntos = null;

            //Solo se adjunta el archivo si el derecho de petición es sobre un trámite que ya se radicó
            if (this.formaDeTramiteTramiteRadicado && this.archivoResultado != null &&
                !this.archivoResultado.getPath().isEmpty()) {
                adjuntos = new String[]{this.archivoResultado.getPath()};

                // Nombre de los archivos adjuntos
                titulosAdjuntos = new String[]{
                    ConstantesComunicacionesCorreoElectronico.ARCHIVO_DERECHO_PETICION_O_TUTELA};
            }
            // Cargar parámetros en el managed bean del componente.
            VisualizacionCorreoActualizacionMB visualCorreoMB =
                (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                    .getManagedBean("visualizacionCorreoActualizacion");
            visualCorreoMB.inicializarParametros(asunto, destinatarios,
                remitente, adjuntos, codigoPlantillaContenido,
                parametrosPlantillaAsunto,
                parametrosPlantillaContenido, titulosAdjuntos);

            // Envio de correo electrónico
            visualCorreoMB.enviarCorreo();
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo del derecho de petición o tutela ");
            LOGGER.debug(e.getMessage(), e);
            this.addMensajeError("Error al enviar el correo del derecho de petición o tutela ");
        }

    }

    /**
     * Método ejecutado en el botón aceptar de la pantalla de trámite con derecho de petición
     *
     * @author javier.aponte
     */
    public void aceptarDerechoDePeticionOTutela() {

        if (this.solicitudDocumentacion == null) {
            this.addMensajeError("Es necesario asociar un documento");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //Marcar el trámite como derecho de petición o tutela
        this.marcarTramiteComoDerechoPeticionOTutela();

        //Enviar correo para el director territorial y el responsable de conservación
        this.enviarCorreoInformandoDerechoPeticionOTutela();

    }

//--------------------------------------------------------------------------------------------------
    public void guardarDocumento() {
        TramiteDocumentacion tdTemp = this.documentoSeleccionado;
        Random rnd = new Random();
        this.documentoSeleccionado.setIdTemporal(rnd.nextLong());
        tdTemp.setIdTemporal(rnd.nextLong());
        if (this.departamentoDocumento != null) {
            tdTemp.setDepartamento(this.departamentoDocumento);
        }
        if (this.municipioDocumento != null) {
            tdTemp.setMunicipio(this.municipioDocumento);
        }

        tdTemp.setFechaAporte(new Date());
        tdTemp.setTramite(this.tramiteSeleccionado);
        tdTemp.setUsuarioLog(this.usuario.getLogin());
        tdTemp.setFechaLog(new Date(System.currentTimeMillis()));

        // Verificar el valor de los folios
        if (!tdTemp.isAnalogo() && !tdTemp.isDigitalizable() &&
            this.rutaMostrar.trim().isEmpty()) {
            tdTemp.setCantidadFolios(0);
        }

        // Buscar el tipo de documento seleccionado.
        if (tdTemp.getTipoDocumento() != null && tdTemp.getTipoDocumento().getId() != null) {
            TipoDocumento td = getGeneralesService().buscarTipoDocumentoPorId(tdTemp.
                getTipoDocumento().getId());
            if (td != null) {
                tdTemp.setTipoDocumento(td);
            }
        }

        Documento doc = generarDocumento(tdTemp);
        this.documentoSeleccionado.setDocumentoSoporte(doc);

        if (this.banderaDocumentoAdicional == true) {
            tdTemp.setAdicional(ESiNo.SI.getCodigo());
            tdTemp.setRequerido(ESiNo.NO.getCodigo());
            this.documentacionAdicional.add(tdTemp);
        } else {
            tdTemp.setAdicional(ESiNo.NO.getCodigo());
            tdTemp.setRequerido(ESiNo.SI.getCodigo());
        }
        if (this.editandoDocumentacion == false && this.banderaDocumentoAdicional == false) {
            this.documentacionTramite.add(tdTemp);
        }

        this.documentoSeleccionado = new TramiteDocumentacion();

        this.documentosDelTramiteTemporales.add(doc);
        this.tramiteDocumentacionTemporales.add(this.documentoSeleccionado);
        this.solicitudDocumentacionTemporales.add(this.solicitudDocumentacion);

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Metodo que genera un nuevo documento con los datos del documento anterior y la los nuevos
     * valores asignados
     *
     * @param tdTemp
     * @return Documento
     */
    /*
     * @modified juanfelipe.garcia :: 15-08-2013 :: se asigna número de radicación y fecha del
     * documento original
     */
 /*
     * @modified leidy.gonzalez :: 13-01-2015 :: #11067 :: se asigna el id del tramite del
     * TramiteDocumentacion
     */
    private Documento generarDocumento(TramiteDocumentacion tdTemp) {

        Documento doc = new Documento();
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setDescripcion(this.documentoSeleccionado.getDetalle());
        doc.setUsuarioLog(this.usuario.getLogin());
        doc.setFechaLog(new Date(System.currentTimeMillis()));
        doc.setTipoDocumento(tdTemp.getTipoDocumento());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setNombreDeArchivoCargado(this.nombreTemporalArchivo);

        if (tdTemp.getTramite() != null && tdTemp.getTramite().getId() != null) {
            doc.setTramiteId(tdTemp.getTramite().getId());
        }
        if (this.documentoSeleccionado.getDocumentoSoporte() != null) {
            doc.setNumeroRadicacion(this.documentoSeleccionado.getDocumentoSoporte().
                getNumeroRadicacion());
            doc.setFechaRadicacion(this.documentoSeleccionado.getDocumentoSoporte().
                getFechaRadicacion());
        }
        //v1.1.7        
        if (this.rutaMostrar != null && !this.rutaMostrar.trim().isEmpty()) {
            doc.setArchivo(this.rutaMostrar);
            doc.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            tdTemp.setAportado(ESiNo.SI.getCodigo());
        } else if (!this.banderaNuevoArchivo &&
            !this.documentoSoporteEliminado &&
            this.documentoSeleccionado.getDocumentoSoporte() != null &&
            this.documentoSeleccionado.getDocumentoSoporte().getArchivo() != null &&
            !this.documentoSeleccionado.getDocumentoSoporte().getArchivo().equals(
                Constantes.CONSTANTE_CADENA_VACIA_DB)) {

            doc.setArchivo(this.documentoSeleccionado.getDocumentoSoporte().getArchivo());
            doc.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            tdTemp.setAportado(ESiNo.SI.getCodigo());
        } else if ((tdTemp.isAnalogo() || tdTemp.isDigitalizable()) && tdTemp.getCantidadFolios().
            intValue() > 0) {
            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            tdTemp.setAportado(ESiNo.SI.getCodigo());
        } else {
            doc.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            tdTemp.setAportado(ESiNo.NO.getCodigo());
        }
        //v1.1.7
        if (tdTemp.getDigital() != null && tdTemp.getDigital().equals(ESiNo.SI.getCodigo()) &&
            tdTemp.getCantidadFolios().intValue() > 0) {
            tdTemp.setAportado(ESiNo.SI.getCodigo());
        }

        this.banderaNuevoArchivo = false;
        return doc;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo utilizado para validar los datos de un nuevoDocumentoTramite como accion sobre el
     * boton Aceptar
     */
    /*
     * @modified juanfelipe.garcia :: 15-08-2013 :: se ajustó la validación cuando no encuentra el
     * radicado en correspondencia
     */
    public boolean validarDocumentoTramite() {
        RequestContext context = RequestContext.getCurrentInstance();

        if (this.banderaMostrarNumeroRadicado) {
            this.buscarRadicadoEnCorrespondecia();
            if (!this.radicadoValidado || this.documentoSeleccionado.getDocumentoSoporte() == null ||
                this.documentoSeleccionado.getDocumentoSoporte()
                    .getNumeroRadicacion() == null) {
                return false;
            }
        }

        if (this.banderaDocumentosFaltantes) {
            return true;
        }
        if (this.documentoSeleccionado.isDigitalizable() &&
            this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
            context.addCallbackParam("error", "error");
            String mensaje = "El documento fue marcado como digitalizable" +
                " pero ya se cargó en el sistema." +
                " Por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
            return false;
        }

        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * método action del botón "usar predios seleccionados" de la ventana que contiene los predios
     * seleccionados.
     */
    /*
     * @modified pedro.garcia 28-11-2013 Documentación. Adición de condición para el predio de un
     * trámite de rectificación Adición de condiciones para el predio de un trámite de cancelación
     * de predio
     */
    public void adicionarPrediosAlTramite() {

        boolean prediosValidos = true;

        if (this.solicitudSeleccionada.isSolicitudAvisos()) {
            prediosValidos = validarPrediosAAsignar();
        }

        //validar si el predio esta activo
        if (!this.validarPredioActivo()) {
            String msgError = "Los predios: ";
            for (String p : this.prediosCancelados) {
                msgError += " " + p + ", ";
            }

            msgError += " estan cancelados. No se puede radicar un tràmite con ellos";
            this.addMensajeError(msgError);
            return;
        }

        //@modified by juan.cruz:: #18834:: validar que el predio se encuentre en el SNC
        if (!this.validarPredioEnElSNC()) {
            return;
        }

        //@modified by leidy.gonzalez:: #20420:: Se valida que para los Tramites Masivos
        //el predio PH/Condominio no se encuentre cancelado
        if (this.tramiteSeleccionado.isRectificacion() ||
            this.tramiteSeleccionado.isTercera() ||
            this.tramiteSeleccionado.isCancelacionPredio() ||
            this.tramiteSeleccionado.isSegunda() ||
            this.tramiteSeleccionado.isQuinta()) {

            boolean predioCancelado = validarPredioCancelado();
            if (!predioCancelado) {
                this.addMensajeError(
                    "Este predio se encuentra cancelado, y no puede realizarse un tramite sobre dicho predio");
                return;
            }
        }

        //D: si es rectificación y el dato a rectificar es cancelación por doble inscripción...
        if (this.tramiteSeleccionado.isEsRectificacion() &&
            contieneDatoRectificarCDI(this.selectedDatosRectifComplDataTable)) {
            boolean predioTieneMejoras = validarPredioTieneMejoras();
            if (predioTieneMejoras) {
                this.addMensajeError(
                    "El predio usado tiene mejoras asociadas y no puede radicarse este trámite");
                return;
            }
        } else if (this.tramiteSeleccionado.isCancelacionPredio()) {
            boolean predioTieneMejoras = validarPredioTieneMejoras();
            if (predioTieneMejoras) {
                this.addMensajeError(
                    "El predio usado tiene mejoras asociadas y no puede radicarse este trámite");
                return;
            }
            //@modified by leidy.gonzalez:: #20420:: Se elimina validacion que no permite cancelar predios
            // de PH o Condominio.
        }

        if (prediosValidos) {
            List<Predio> predios = this.getPrediosSeleccionadosDeComponenteSeleccPredios();
            this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
            if (predios == null || predios.isEmpty()) {
                this.addMensajeWarn(ECodigoErrorVista.PREDIOS_NO_SELECCIONADOS.getMensajeUsuario());
                LOGGER.warn(ECodigoErrorVista.PREDIOS_NO_SELECCIONADOS.getMensajeTecnico());
            } else {
                for (int i = 0; i < predios.size(); i++) {
                    Predio unPredio = predios.get(i);

                    TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                    tpe.setPredio(unPredio);
                    this.prediosTramite.add(tpe);

                    if (this.listaGlobalPrediosColindantes == null) {
                        this.listaGlobalPrediosColindantes = new ArrayList<Predio>();
                    }

                    if (i > 0) {

                        List<Predio> listaColindantesTemp;
                        for (int j = 0; j < i; j++) {
                            listaColindantesTemp = this.getConservacionService().
                                buscarPrediosColindantesPorNumeroPredial(predios.get(j).
                                    getNumeroPredial());

                            if (listaColindantesTemp != null && !listaColindantesTemp.isEmpty()) {

                                for (Predio pTmp : listaColindantesTemp) {

                                    if (pTmp.getId().equals(
                                        predios.get(i).getId())) {

                                        predios.get(i).setNumeroPredialAsociadoPadre(predios.get(j).
                                            getNumeroPredial());

                                        if (!this.listaGlobalPrediosColindantes.contains(unPredio)) {
                                            this.listaGlobalPrediosColindantes.add(unPredio);
                                        }
                                        if (!this.listaGlobalPrediosColindantes.contains(predios.
                                            get(j))) {
                                            this.listaGlobalPrediosColindantes.add(predios.get(j));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    List<Tramite> tramites = this.getTramiteService().
                        buscarTramitesPendientesDePredio(unPredio.getId());
                    if (tramites != null && !tramites.isEmpty()) {
                        this.addMensajeWarn(ECodigoErrorVista.PREDIO_CON_TRAMITES_EJECUCION.
                            getMensajeUsuario(unPredio.getNumeroPredial()));
                        LOGGER.warn(ECodigoErrorVista.PREDIO_CON_TRAMITES_EJECUCION.
                            getMensajeTecnico());
                    }

                }
                if (this.tramiteSeleccionado != null) {
                    if (predios.size() > 1) {
                        this.tramiteSeleccionado.setTramitePredioEnglobes(this.prediosTramite);
                        this.tramiteSeleccionado.setPredio(predios.get(0));
                    } else {
                        this.tramiteSeleccionado.setTramitePredioEnglobes(null);
                        this.tramiteSeleccionado.setPredio(predios.get(0));
                    }
                }

                this.setSelectedDepartamentoTramite(predios.get(0).getDepartamento().getCodigo());
                this.onChangeDepartamentosTramite();
                this.setSelectedMunicipioTramite(predios.get(0).getMunicipio().getCodigo());
                this.tipoAvaluoSeleccionado = predios.get(0).getNumeroPredial().substring(6, 8)
                    .equals(ETipoAvaluo.RURAL.getCodigo()) ? ETipoAvaluo.RURAL.getNombre() :
                    ETipoAvaluo.URBANO.getNombre();
                this.selectedPredioTramite = null;

            }
        } else {
            String mensaje =
                "El predio(s) que está tratando de adicionar no corresponde al círculo registral. Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida los predios seleccionados desde el componente de búsqueda de predios para
     * ser asignados al trámite
     */
    private boolean validarPrediosAAsignar() {
        int contador = 0;
        boolean prediosValidos = true;
        if (this.getPrediosSeleccionadosDeComponenteSeleccPredios() != null &&
            !this.getPrediosSeleccionadosDeComponenteSeleccPredios().isEmpty()) {

            List<Municipio> municipiosTemp = this.getGeneralesService()
                .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
                    this.departamentoAvisos.getCodigo());

            for (Predio pTmp : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {
                for (Municipio mTmp : municipiosTemp) {
                    if (pTmp.getMunicipio().getCodigo().equals(mTmp.getCodigo())) {
                        contador++;
                        break;
                    }
                }
            }
        }
        prediosValidos =
            (contador == this.getPrediosSeleccionadosDeComponenteSeleccPredios().size());

        return prediosValidos;
    }

    // -------------------------------------------------------------------------
    /**
     * Método utilizado sobre la selección de predio colindante para adicionarlo a la lista de
     * predios seleccionados del trámite
     */
    public void adicionarPrediosColindantes() {

        //felipe.cadena::09-01-2015-#10422::Se informa si los predios colindantes tienen otros tramites asociados
        List<Tramite> tramites = this.getTramiteService().
            buscarTramitesPendientesDePredio(this.selectedPredioColindante.getId());
        if (tramites != null && !tramites.isEmpty()) {
            this.addMensajeWarn("El predio " + selectedPredioColindante.getNumeroPredial() +
                " tiene trámites en ejecución");
        }
        TramitePredioEnglobe tpe = new TramitePredioEnglobe();
        tpe.setPredio(this.selectedPredioColindante);
        this.prediosTramite.add(tpe);
        this.tramiteSeleccionado.setTramitePredioEnglobes(prediosTramite);
        this.tramiteSeleccionado.setPredio(this.prediosTramite.get(0).getPredio());
        this.listaPrediosColindantes.remove(this.selectedPredioColindante);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandLink que se pinta cuando hay varios predios de un aviso rechazado
     *
     * @author pedro.garcia
     */
    public void mostrarMultiplesPrediosAvisoRechazado() {
        //TODO:: Implementar funcionalidad de este metodo 
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de crear una nueva solicitud.
     *
     * @author fabio.navarrete
     * @modify juan.agudelo
     *
     */
    public void guardarSolicitud() {

        boolean validacionCamposSolicitud = validarCamposSolicitud();

        if (validacionCamposSolicitud) {
            if (this.solicitudSeleccionada != null) {

                if (this.solicitudSeleccionada.getSolicitudAvisoRegistro() != null) {
                    this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                        .setCirculoRegistral(this.circuloRegistralAvisos);
                    this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                        .setFechaLog(new Date());
                    this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                        .setUsuarioLog(this.usuario.getLogin());
                    this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                        .setSolicitud(this.solicitudSeleccionada);
                }

                this.tramitesSolicitud = new ArrayList<Tramite>();

                if (this.solicitudSeleccionada.getSolicitanteSolicituds() != null &&
                    !this.solicitudSeleccionada
                        .getSolicitanteSolicituds().isEmpty()) {
                    Solicitud auxSol;
                    try {
                        auxSol = this.getTramiteService()
                            .guardarActualizarSolicitud(this.usuario,
                                this.solicitudSeleccionada, this.tramitesExistentes);

                        if (auxSol != null) {
                            this.solicitudSeleccionada = auxSol;
                            this.banderaSolicitudCreada = true;
                            this.editandoSolicitud = false;

                            if (this.solicitudSeleccionada.getTipo().equals(
                                ESolicitudTipo.AUTOAVALUO.getCodigo())) {
                                this.banderaTipoSolicitudAvaluo = false;
                            } else {
                                this.banderaTipoSolicitudAvaluo = true;
                            }
                            String mensaje = "Solicitud creada exitosamente";
                            this.addMensajeInfo(mensaje);

                            //Se agrega para el caso de uso de derecho de petición o tutela :: javier.aponte :: 15/08/2013
                            if (this.formaDeTramiteTramiteRadicado) {
                                this.activarRelacionarTramite = true;
                            }

                        } else {
                            String mensaje = "Ocurrió un error al procesar la solicitud";
                            this.addMensajeError(mensaje);
                        }
                    } catch (ExcepcionSNC e) {
                        this.addMensajeError(e);
                    }
                } else {
                    String mensaje = "La solicitud no tiene solicitantes asociados," +
                        " por favor corrija los datos e intente nuevamente";
                    this.addMensajeError(mensaje);
                }
            } else {
                String mensaje = "La solicitud se encuentra vacía";
                this.addMensajeError(mensaje);
            }
        }
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que realiza la validación lógica para los campos requeridos en la solicitud catastral
     *
     * @author juan.agudelo
     */
    private boolean validarCamposSolicitud() {

        RequestContext context = RequestContext.getCurrentInstance();
        int contador = 0;

        if (this.solicitudSeleccionada.getFolios() == null) {

            String mensaje = "Folios: Error de validación: se necesita un valor.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("detalleSolicitudGeneralForm:folios", mensaje);
            contador++;
        }

        if (this.solicitudSeleccionada.getFolios() != null &&
            this.solicitudSeleccionada.getFolios() < 1) {

            String mensaje = "Folios: Error de validación: el valor mínimo debe ser mayor a 0.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("detalleSolicitudGeneralForm:folios", mensaje);
            contador++;
        }

        if (this.solicitudSeleccionada.getAsunto() != null &&
            !this.solicitudSeleccionada.getAsunto().isEmpty() &&
            this.solicitudSeleccionada.getAsunto().length() > 600) {

            String mensaje = "Asunto: Error de validación: el tamaño máximo para el campo es de" +
                " 600 caracteres.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("detalleSolicitudGeneralForm:asunto", mensaje);
            contador++;
        }

        if (this.solicitudSeleccionada.getObservaciones() != null &&
            !this.solicitudSeleccionada.getObservaciones().isEmpty() &&
            this.solicitudSeleccionada.getObservaciones().length() > 2000) {

            String mensaje = "Observaciones: Error de validación: el tamaño máximo para el campo" +
                " es de 2000 caracteres.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("detalleSolicitudGeneralForm:observaciones",
                mensaje);
            contador++;
        }

        if (this.solicitudSeleccionada.getAnexos() == null) {

            String mensaje = "Anexos: Error de validación: se necesita un valor.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("detalleSolicitudGeneralForm:anexos", mensaje);
            contador++;
        }

        if (this.solicitudSeleccionada.getTipoDocumentoCorrespondencia() == null ||
            this.solicitudSeleccionada.getTipoDocumentoCorrespondencia()
                .isEmpty()) {

            String mensaje =
                "Documento de correspondencia: Error de validación: se necesita un valor.";

            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "detalleSolicitudGeneralForm:tipoDocumentoCorrespondencia",
                mensaje);
            contador++;
        }

        if (this.solicitudSeleccionada.isSolicitudAvisos()) {

            if (this.departamentoAvisos == null) {

                String mensaje = "Departamento: Error de validación: se necesita un valor.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:departamentoAviso", mensaje);
                contador++;
            } else if (this.departamentoAvisos != null) {

                if (this.circulosRegistralesAvisos == null ||
                    this.circulosRegistralesAvisos.isEmpty()) {

                    String mensaje = "Circulo registral: Error de validación: se necesita un valor.";

                    context.addCallbackParam("error", "error");
                    this.addMensajeError(
                        "detalleSolicitudGeneralForm:circuloRegistralAviso", mensaje);
                    contador++;

                }
            }

            if (this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .getNumeroDocumentoSoporte() == null ||
                this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                    .getNumeroDocumentoSoporte().isEmpty()) {

                String mensaje =
                    "Número de documento de soporte: Error de validación: se necesita un valor.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:numeroDocumentoSoporteAviso", mensaje);
                contador++;
            }

            if (this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .getFechaDocumentoSoporte() == null) {

                String mensaje =
                    "Fecha de documento de soporte: Error de validación: se necesita un valor.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:fechaDocumentoAvisos", mensaje);
                contador++;
            } else if (this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .getFechaDocumentoSoporte() != null &&
                this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                    .getFechaDocumentoSoporte().after(new Date())) {

                String mensaje = "Fecha de documento de soporte: Error de validación: " +
                    " la fecha del documento no puede ser futura.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:fechaDocumentoAvisos", mensaje);
                contador++;
            }

            if (this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .getAvisosTotal() == null) {

                String mensaje = "Número de avisos: Error de validación: se necesita un valor.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:numeroAvisosAviso", mensaje);
                contador++;
            }

            if (this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                .getAvisosTotal() <= 0) {

                String mensaje =
                    "Número de avisos: la solicitud debe contar por lo menos con un aviso.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:numeroAvisosAviso", mensaje);
                contador++;
            }

            if (this.solicitudSeleccionada.getSolicitanteSolicituds().size() > 1) {

                String mensaje =
                    "Solicitandes de avisos: la solicitud solo debe contar con un solicitante" +
                    " y debe tener los datos del usuario de registro.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:solicitanteSolicitudTable", mensaje);
                contador++;
            } else if (this.solicitudSeleccionada.getSolicitanteSolicituds()
                .size() == 1 &&
                !this.solicitudSeleccionada.getSolicitanteSolicituds()
                    .get(0).getTipoPersona()
                    .equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {

                String mensaje =
                    "Solicitandes de avisos: el usuario solicitante del aviso debe ser" +
                    " el usuario de registro.";

                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "detalleSolicitudGeneralForm:solicitanteSolicitudTable", mensaje);
                contador++;
            }

        }

        if ((this.formaDePeticionDerechoDePeticion || this.formaDePeticionTutela) &&
            !this.formaDeTramiteTramiteNuevo && !this.formaDeTramiteTramiteRadicado) {

            String mensaje =
                "Forma de trámite: Error de validación: Debe ingresar la forma de trámite.";

            context.addCallbackParam("error", "error");
            this.addMensajeError("detalleSolicitudGeneralForm:formaDeTramite", mensaje);
            contador++;

        }

        if (contador > 0) {
            return false;
        }

        return true;
    }

//--------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar trámite
     *
     * @modify juan.agudelo
     *
     */
    /*
     * @modified by juanfelipe.garcia :: 13-11-2013 :: para vía gubernativa se corrigió el flujo de
     * radicación (CC-NP-CO-030) @modified by felipe.cadena :: 10-04-2015 :: validaciones por tipo
     * de tramite para tramites de ficha matriz
     */
    public void guardarTramiteSolicitud() {
        RequestContext context = RequestContext.getCurrentInstance();
        this.documentacionTramite = new ArrayList<TramiteDocumentacion>();
        this.documentacionRequerida = new ArrayList<TramiteDocumentacion>();
        this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();

        //validacion necesaria para predios fiscales
        if (this.tramiteSeleccionado.getPredio() != null &&
            this.tramiteSeleccionado.getPredio().getTipoCatastro() != null &&
            this.tramiteSeleccionado.getPredio().getTipoCatastro()
                .equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
            this.banderaPredioFiscal = true;
        }

        this.establecerDatosPreliminaresTramiteNuevo();
        //validacion necesaria para predios fiscales
        if (this.banderaPredioFiscal && this.tramiteSeleccionado.isEsRectificacion()) {

            if (this.tramiteSeleccionado.isRectificacionFoto() ||
                 this.tramiteSeleccionado.isRectificacionDetalleCalificacion() ||
                 this.tramiteSeleccionado.isRectificacionZona()) {

                context.addCallbackParam("error", "error");
                String mensaje = "El tipo de trámite seleccionado no procede con predios fiscales.";
                this.addMensajeError(mensaje);
                return;
            }
        }

        //Los predios de quinta no se radican con predios asociados
        if (!this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.getPredio() != null) {
            //felipe.cadena::validaciones por tipo de tramite para tramites de ficha matriz
            if (!this.validarTipoTramitePredio()) {
                return;
            }
            //felipe.cadena::validaciones por predio para tramites de ficha matriz, solo informa si el predio tiene tramites 
            this.validarTramitesPredios();
        }

        //@modified by leidy.gonzalez::#20015::03/10/2016 Se validan
        //Los predios de quinta no se radican por la condicion de Propiedad
        if (this.tramiteSeleccionado.isQuintaNuevo() || this.tramiteSeleccionado.isQuintaOmitido()) {

            if (!this.validarTipoTramitePredioQuinta()) {
                return;
            }
        }

        //validar solicitantes para tramites de avisos
        //felipe.cadena::23-12-2014::#10743
        if (this.solicitudSeleccionada.getTipo().equals(ESolicitudTipo.AVISOS.getCodigo()) &&
            (this.tramiteSeleccionado.getSolicitanteTramites() == null ||
            this.tramiteSeleccionado.getSolicitanteTramites().isEmpty())) {
            context.addCallbackParam("error", "error");
            String mensaje = "El trámite debe tener solicitantes asociados";
            this.addMensajeError(mensaje);
            return;
        }

        boolean validarTramitesB = this.validacionTramiteMB.validarDatosTramite();

        if (validarTramitesB) {

            if (this.solicitudSeleccionada.getFecha() == null) {
                this.solicitudSeleccionada.setFecha(new Date(System.currentTimeMillis()));
            }

            if (this.solicitudSeleccionada.isViaGubernativa()) {
                // Sí la solicitud es de vía gubernativa se verifica que la
                // resolución del trámite no esté en firme, si dicha resolución
                // está en firme, se procederá con el trámite normalmente, pero
                // si la resolución está en proyección, el trámite asociado será
                // nuestro nuevo trámite a radicar.

                if (this.selectedTramiteGubernativaBuscado.getNumeroRadicacion() != null &&
                     !this.selectedTramiteGubernativaBuscado.getNumeroRadicacion().isEmpty()) {

                    if (this.selectedTramiteGubernativaBuscado.getEstado().equals(
                        ETramiteEstado.FINALIZADO_APROBADO.getCodigo())) {

                        String mensaje = "El trámite ya esta finalizado";
                        this.addMensajeWarn(mensaje);
                    }

                    //validar que el tramite original tenga una resolucion
                    if (this.selectedTramiteGubernativaBuscado.getResultadoDocumento() == null) {
                        context.addCallbackParam("error", "error");
                        String mensaje = "El trámite original seleccionado no tiene resolución";
                        this.addMensajeError(mensaje);
                        return;
                    }

                    //@modified by javier.aponte :: 3/08/15 :: Se valida que la actividad de comunicar notificación
                    //de resolución esté en estado finalizada para garantizar que ya paso por esa actividad
                    boolean realizoRegistroNotificacion = false;

                    //@modified by juanfelipe.garcia :: 31-07-2014 :: Los tramites de vìa gubernativa se 
                    //deben permitir radicar asi hayan pasado de la actividad de radicar recurso, 
                    //se encuentre en aplicar cambios o ya haya aplicado cambios(8887).
                    Map<EParametrosConsultaActividades, String> parametros =
                        new EnumMap<EParametrosConsultaActividades, String>(
                            EParametrosConsultaActividades.class);
                    parametros.put(EParametrosConsultaActividades.ID_INSTANCIA_PROCESO,
                        this.selectedTramiteGubernativaBuscado.getProcesoInstanciaId());
                    List<Actividad> listaActividades = this.getProcesosService().
                        consultarListaActividades(parametros);

                    for (Actividad actividad : listaActividades) {
                        if (ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION.equals(
                            actividad.getNombre()) &&
                            
                            (EEstadoActividad.FINALIZADA.toString().equals(actividad.getEstado()) ||
                             EEstadoActividad.TERMINADA.toString().equals(actividad.getEstado()))) {

                            realizoRegistroNotificacion = true;
                            break;
                        }
                    }
                    if (!realizoRegistroNotificacion &&
                         !this.selectedTramiteGubernativaBuscado.getEstado().equals(
                            ETramiteEstado.FINALIZADO_APROBADO.getCodigo())) {

                        context.addCallbackParam("error", "error");
                        String mensaje =
                            "El trámite debe realizar la comunicación y posteriormente el registro de la notificación";
                        this.addMensajeError(mensaje);
                        return;
                    }

                    this.solicitudSeleccionada.setTramiteNumeroRadicacionRef(
                        this.selectedTramiteGubernativaBuscado.getNumeroRadicacion());

                } else if (this.tramitesGubernativaAll != null &&
                     !this.tramitesGubernativaAll.isEmpty() &&
                     this.tramitesGubernativaAll.get(0).getNumeroRadicacion() != null &&
                     !this.tramitesGubernativaAll.get(0).getNumeroRadicacion().isEmpty()) {

                    if (this.tramitesGubernativaAll.get(0).getEstado().equals(
                        ETramiteEstado.FINALIZADO_APROBADO.getCodigo())) {

                        String mensaje = "El trámite ya esta finalizado";
                        this.addMensajeWarn(mensaje);
                    }

                    //validar que el tramite original tenga una resolución
                    if (this.tramitesGubernativaAll.get(0).getResultadoDocumento() == null) {
                        context.addCallbackParam("error", "error");
                        String mensaje = "El trámite original seleccionado no tiene resolución";
                        this.addMensajeError(mensaje);
                        return;
                    }

                    //@modified by javier.aponte :: 3/08/15 :: Se valida que la actividad de comunicar notificación
                    //de resolución esté en estado finalizada para garantizar que ya paso por esa actividad
                    boolean realizoRegistroNotificacion = false;

                    Map<EParametrosConsultaActividades, String> parametros =
                        new EnumMap<EParametrosConsultaActividades, String>(
                            EParametrosConsultaActividades.class);
                    parametros.put(EParametrosConsultaActividades.ID_INSTANCIA_PROCESO,
                        this.tramitesGubernativaAll.get(0).getProcesoInstanciaId());
                    List<Actividad> listaActividades = this.getProcesosService().
                        consultarListaActividades(parametros);

                    for (Actividad actividad : listaActividades) {
                        if (ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION.equals(
                            actividad.getNombre()) &&
                            
                            (EEstadoActividad.FINALIZADA.toString().equals(actividad.getEstado()) ||
                             EEstadoActividad.TERMINADA.toString().equals(actividad.getEstado()))) {
                            realizoRegistroNotificacion = true;
                            break;
                        }
                    }
                    if (!realizoRegistroNotificacion && !this.tramitesGubernativaAll.get(0).
                        getEstado()
                        .equals(ETramiteEstado.FINALIZADO_APROBADO.getCodigo())) {

                        context.addCallbackParam("error", "error");
                        String mensaje =
                            "El trámite debe realizar la comunicación y posteriormente el registro de la notificación";
                        this.addMensajeError(mensaje);
                        return;
                    }

                    this.solicitudSeleccionada.setTramiteNumeroRadicacionRef(
                        this.tramitesGubernativaAll.get(0).getNumeroRadicacion());
                }
            }//fin via gubernativa

            try {

                List<TramiteRequisito> tr = this.getTramiteService()
                    .conseguirDocumentacionTramiteRequerida(
                        this.tramiteSeleccionado);

                if (tr == null || tr.isEmpty()) {
                    String mensaje =
                        "No se pudo recuperar la información de los documentos requeridos. " +
                        "Por favor verifique que los predios tengan las siguiente información: Tipo de trámite " +
                        "(si es mutación, clase de mutación y/o subtipo), Clasificación y zona unidad orgánica.";
                    this.addMensajeWarn(mensaje);
                } else {
                    TramiteDocumentacion tdTemp;
                    if (tr.get(0).getTramiteRequisitoDocumentos() != null &&
                         !tr.get(0).getTramiteRequisitoDocumentos().isEmpty()) {
                        for (int i = 0; i < tr.get(0).getTramiteRequisitoDocumentos().size(); i++) {
                            tdTemp = new TramiteDocumentacion();
                            tdTemp.setTipoDocumento(
                                tr.get(0).getTramiteRequisitoDocumentos().get(i).getTipoDocumento());
                            tdTemp.setAportado(ESiNo.NO.getCodigo());
                            tdTemp.setRequerido(ESiNo.SI.getCodigo());
                            if (!this.documentacionRequerida.contains(tdTemp)) {
                                this.documentacionRequerida.add(tdTemp);
                                this.documentacionTramite.add(tdTemp);
                            }
                        }
                    }
                    if (this.hayRepresentanteLegal()) {
                        List<Dominio> ds = this.getGeneralesService().
                            buscarDominioPorNombreOrderByNombre(EDominio.DOCUMENTO_REPRESENTANTE);
                        for (Dominio d : ds) {
                            tdTemp = new TramiteDocumentacion();
                            tdTemp.setTipoDocumento(this.getGeneralesService().
                                buscarTipoDocumentoPorId(new Long(d.getCodigo())));
                            tdTemp.setAportado(ESiNo.NO.getCodigo());
                            tdTemp.setRequerido(ESiNo.SI.getCodigo());
                            if (!this.documentacionRequerida.contains(tdTemp)) {
                                this.documentacionRequerida.add(tdTemp);
                                this.documentacionTramite.add(tdTemp);
                            }
                        }
                    }
                    if (this.hayApoderado()) {
                        List<Dominio> ds = this.getGeneralesService().
                            buscarDominioPorNombreOrderByNombre(EDominio.DOCUMENTO_APODERADO);
                        for (Dominio d : ds) {
                            tdTemp = new TramiteDocumentacion();
                            tdTemp.setTipoDocumento(this.getGeneralesService().
                                buscarTipoDocumentoPorId(new Long(d.getCodigo())));
                            tdTemp.setAportado(ESiNo.NO.getCodigo());
                            tdTemp.setRequerido(ESiNo.SI.getCodigo());
                            if (!this.documentacionRequerida.contains(tdTemp)) {
                                this.documentacionRequerida.add(tdTemp);
                                this.documentacionTramite.add(tdTemp);
                            }
                        }
                    }
                }

                this.banderaTramitePAlmacenado = true;

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                String mensaje =
                    "Ocurrió un error al recuperar la información de los documentos requeridos";
                this.addMensajeError(mensaje);
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de establecer unos datos preliminares en el managed bean de validación y
     * adición de un nuevo trámite, para que se pueda hacer la validación del trámite
     *
     * @author javier.aponte
     * @modified felipe.cadena - se agregan las validadciones para la rectificaciones y
     * complementaciones.
     */
    private void establecerDatosPreliminaresTramiteNuevo() {

        this.validacionTramiteMB.setNuevoTramiteSeleccionado(this.tramiteSeleccionado);
        this.validacionTramiteMB.setSolicitudSeleccionada(this.solicitudSeleccionada);
        this.validacionTramiteMB.setTramitesGubernativaAll(this.tramitesGubernativaAll);
        this.validacionTramiteMB.
            setListaGlobalPrediosColindantes(this.listaGlobalPrediosColindantes);
        this.validacionTramiteMB.setTramitesPredioEnglobeNuevoTramiteAdicionado(this.prediosTramite);

        if (this.tramiteSeleccionado.isEsRectificacion() || this.tramiteSeleccionado.
            isEsComplementacion()) {

            this.validacionTramiteMB.setSelectedDatosRectificarOComplementarDataTable(
                this.selectedDatosRectifComplDataTable);

        }

        this.validacionTramiteMB.setUsuario(this.usuario);

        if (this.tramiteSeleccionado.isQuinta()) {

            this.validacionTramiteMB.setNumeroPredialParte1(this.numeroPredialParte1);
            this.validacionTramiteMB.setNumeroPredialParte2(this.numeroPredialParte2);
            this.validacionTramiteMB.setNumeroPredialParte3(this.numeroPredialParte3);
            this.validacionTramiteMB.setNumeroPredialParte4(this.numeroPredialParte4);
            this.validacionTramiteMB.setNumeroPredialParte5(this.numeroPredialParte5);
            this.validacionTramiteMB.setNumeroPredialParte6(this.numeroPredialParte6);
            this.validacionTramiteMB.setNumeroPredialParte7(this.numeroPredialParte7);
            this.validacionTramiteMB.setNumeroPredialParte8(this.numeroPredialParte8);
            this.validacionTramiteMB.setNumeroPredialParte9(this.numeroPredialParte9);
            this.validacionTramiteMB.setNumeroPredialParte10(this.numeroPredialParte10);
            this.validacionTramiteMB.setNumeroPredialParte11(this.numeroPredialParte11);
            this.validacionTramiteMB.setNumeroPredialParte12(this.numeroPredialParte12);
            this.validacionTramiteMB.setAreaTerreno(this.getAreaTerreno());

            if (this.departamentoTramite != null) {
                this.tramiteSeleccionado.setDepartamento(this.departamentoTramite);
            }
            if (this.municipioTramite != null) {
                this.tramiteSeleccionado.setMunicipio(this.municipioTramite);
            }

        }

        this.tramiteSeleccionado = this.validacionTramiteMB.establecerDatosTramiteNuevo(
            this.tramiteSeleccionado);

    }
    // --------------------------------------------------------------------------

    public void setSelectedDepartamentoAvisos(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosAvisos != null) {
            for (Departamento departamento : dptosAvisos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.solicitudSeleccionada != null) {
            this.departamentoAvisos = dptoSeleccionado;
        }
    }

    public String getSelectedDepartamentoAvisos() {
        if (this.solicitudSeleccionada != null &&
            this.departamentoAvisos != null) {
            return this.departamentoAvisos.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getDepartamentosAvisos() {
        return departamentosAvisos;
    }

    public void onChangeOrdenDepartamentosAvisos() {
        this.updateDepartamentosAvisos();
    }

    public void onChangeDepartamentosAvisos() {
        this.municipioAvisos = null;
        this.circuloRegistralAvisos = null;
        this.updateCirculosRegistralesAvisos();
    }

    public void cambiarOrdenDepartamentosAvisos() {
        if (this.ordenDepartamentosAvisos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosAvisos = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosAvisos = EOrden.CODIGO;
        }
        this.updateDepartamentosAvisos();
    }
//--------------------------------------------------------------------------------------------------

    public void updateDepartamentosAvisos() {
        this.departamentosAvisos = new LinkedList<SelectItem>();
        this.departamentosAvisos.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.dptosAvisos = this.getGeneralesService().getCacheDepartamentosPorPais(
            Constantes.COLOMBIA);
        if (this.ordenDepartamentosAvisos.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosAvisos);
        } else {
            Collections.sort(this.dptosAvisos, Departamento.getComparatorNombre());
        }
        for (Departamento departamento : dptosAvisos) {
            if (this.ordenDepartamentosAvisos.equals(EOrden.CODIGO)) {
                this.departamentosAvisos.add(new SelectItem(
                    departamento.getCodigo(), departamento.getCodigo() +
                    "-" + departamento.getNombre()));
            } else {
                this.departamentosAvisos.add(new SelectItem(
                    departamento.getCodigo(), departamento.getNombre()));
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    public void setSelectedMunicipioAvisos(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null && this.munisAvisos != null) {
            for (Municipio municipio : this.munisAvisos) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioAvisos = null;
        }
        if (this.solicitudSeleccionada != null) {
            this.municipioAvisos = municipioSelected;
        }
    }

    public boolean isMostrarFechaRadTesoreria() {
        return mostrarFechaRadTesoreria;
    }

    public void setMostrarFechaRadTesoreria(boolean mostrarFechaRadTesoreria) {
        this.mostrarFechaRadTesoreria = mostrarFechaRadTesoreria;
    }

    public String getSelectedMunicipioAvisos() {
        if (this.solicitudSeleccionada != null && this.municipioAvisos != null) {
            return this.municipioAvisos.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getMunicipiosAvisos() {
        return this.municipiosAvisos;
    }

    public void onChangeMunicipiosAvisos() {
        this.circuloRegistralAvisos = null;
        this.updateCirculosRegistralesAvisos();
    }

    public void onChangeOrdenMunicipiosAvisos() {
        this.updateMunicipiosAvisos();
    }
//--------------------------------------------------------------------------------------------------

    public void updateMunicipiosAvisos() {
        this.circuloRegistralAvisos = null;
        municipiosAvisos = new ArrayList<SelectItem>();
        munisAvisos = new ArrayList<Municipio>();
        municipiosAvisos.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.solicitudSeleccionada != null &&
            this.departamentoAvisos != null &&
            this.departamentoAvisos.getCodigo() != null) {
            munisAvisos = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoAvisos.getCodigo());
            if (this.ordenMunicipiosAvisos.equals(EOrden.CODIGO)) {
                Collections.sort(munisAvisos);
            } else {
                Collections.sort(munisAvisos, Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisAvisos) {
                if (this.ordenMunicipiosAvisos.equals(EOrden.CODIGO)) {
                    municipiosAvisos.add(new SelectItem(municipio.getCodigo(),
                        municipio.getCodigo3Digitos() + "-" +
                        municipio.getNombre()));
                } else {
                    municipiosAvisos.add(new SelectItem(municipio.getCodigo(),
                        municipio.getNombre()));
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    public void cambiarOrdenMunicipiosAvisos() {
        if (this.ordenMunicipiosAvisos.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosAvisos = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosAvisos = EOrden.CODIGO;
        }
        this.updateMunicipiosAvisos();
    }

    public void setSelectedCirculoRegistralAvisos(String codigo) {
        CirculoRegistral circuloRegistralSelected = null;
        if (codigo != null) {

            for (CirculoRegistral circuloReg : this.circsRegistralesAvisos) {
                if (codigo.equals(circuloReg.getCodigo())) {
                    circuloRegistralSelected = circuloReg;
                    break;
                }
            }

        }
        if (this.solicitudSeleccionada != null) {
            this.circuloRegistralAvisos = circuloRegistralSelected;
        }
    }
//--------------------------------------------------------------------------------------------------

    public String getSelectedCirculoRegistralAvisos() {
        if (this.solicitudSeleccionada != null &&
            this.circuloRegistralAvisos != null) {
            return this.circuloRegistralAvisos.getCodigo();
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al seleccionar documentos adicionales para eliminar
     *
     * @author juan.agudelo
     *
     */
    public void eliminarDocumentacionAdicional() {
        this.documentacionAdicional.remove(this.documentoSeleccionado);
        this.documentacionTramite.remove(this.documentoSeleccionado);
    }

    //--------------------------------------------------//
    /**
     * Método que elimina un TramiteDocumentacion seleccionado para la pantalla de
     * asociarDocumentosYAmpliarTiempos.
     *
     * @author david.cifuentes
     */
    public void eliminarDocumentacionAdicionalEnCompletarDocs() {
        this.documentacionAdicional.remove(this.documentoSeleccionado);
        this.documentacionTramite.remove(this.documentoSeleccionado);
        try {
            List<TramiteDocumentacion> tramiteDocumentacionEliminar =
                new ArrayList<TramiteDocumentacion>();
            tramiteDocumentacionEliminar.add(documentoSeleccionado);
            this.getTramiteService().eliminarTramitesDocumentacion(tramiteDocumentacionEliminar);
            this.addMensajeError("Se eliminó el documento satisfactoriamente.");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    //---------------------------------------------------//

    /**
     * Retorna el listado de items que van en el combo de círculos registrales
     *
     * @author fabio.navarrete
     * @return
     */
    public List<SelectItem> getCirculosRegistralesAvisos() {
        return this.circulosRegistralesAvisos;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Organiza la lista de los círculos registrales a mostrar en la GUI.
     *
     * @author fabio.navarrete
     */
    public void updateCirculosRegistralesAvisos() {
        circulosRegistralesAvisos = new ArrayList<SelectItem>();
        circsRegistralesAvisos = new ArrayList<CirculoRegistral>();
        circulosRegistralesAvisos.add(new SelectItem("", super.DEFAULT_COMBOS));
        if (this.departamentoAvisos != null) {
            circsRegistralesAvisos = this.getGeneralesService()
                .getCacheCirculosRegistralesPorDepartamento(
                    this.departamentoAvisos.getCodigo());
            if (this.ordenCirculosRegistralesAvisos.equals(EOrden.CODIGO)) {
                Collections.sort(circsRegistralesAvisos);
            } else {
                Collections.sort(circsRegistralesAvisos,
                    CirculoRegistral.getNombreComparator());
            }
            for (CirculoRegistral cr : circsRegistralesAvisos) {
                if (this.ordenCirculosRegistralesAvisos.equals(EOrden.CODIGO)) {
                    circulosRegistralesAvisos.add(new SelectItem(
                        cr.getCodigo(), cr.getCodigo() + "-" +
                        cr.getNombre()));
                } else {
                    circulosRegistralesAvisos.add(new SelectItem(
                        cr.getCodigo(), cr.getNombre()));
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    public void cambiarOrdenCirculosRegistralesAvisos() {
        if (this.ordenCirculosRegistralesAvisos.equals(EOrden.CODIGO)) {
            this.ordenCirculosRegistralesAvisos = EOrden.NOMBRE;
        } else {
            this.ordenCirculosRegistralesAvisos = EOrden.CODIGO;
        }
        this.updateCirculosRegistralesAvisos();
    }

    public String getOrdenDepartamentosAvisos() {
        return ordenDepartamentosAvisos.toString();
    }

    public String getOrdenMunicipiosAvisos() {
        return ordenMunicipiosAvisos.toString();
    }

    public String getOrdenCirculosRegistralesAvisos() {
        return ordenCirculosRegistralesAvisos.toString();
    }

    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenDepartamentosNombreAvisos() {
        return this.ordenDepartamentosAvisos.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenMunicipiosNombreAvisos() {
        return this.ordenMunicipiosAvisos.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los círculos registrales están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenCirculosRegistralesNombreAvisos() {
        return this.ordenCirculosRegistralesAvisos.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    public void setSelectedPaisSolicitante(String codigo) {
        Pais paisSeleccionado = null;
        if (codigo != null) {
            for (Pais pais : paissSolicitante) {
                if (codigo.equals(pais.getCodigo())) {
                    paisSeleccionado = pais;
                    break;
                }
            }
        }
        if (this.solicitanteSeleccionado != null) {
            this.paisSolicitante = paisSeleccionado;
        }
    }

    public String getSelectedPaisSolicitante() {
        if (this.solicitanteSeleccionado != null &&
            this.paisSolicitante != null) {
            return this.paisSolicitante.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getPaisesSolicitante() {
        return paisesSolicitante;
    }

    public void onChangeOrdenPaisesSolicitante() {
        this.updatePaisesSolicitante();
    }

    public void onChangePaisesSolicitante() {
        this.municipioSolicitante = null;
        this.departamentoSolicitante = null;
        this.updateDepartamentosSolicitante();
        this.updateMunicipiosSolicitante();
    }

    public void cambiarOrdenPaisesSolicitante() {
        if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
            this.ordenPaisesSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenPaisesSolicitante = EOrden.CODIGO;
        }
        this.updatePaisesSolicitante();
    }

    public void updatePaisesSolicitante() {
        paisesSolicitante = new ArrayList<SelectItem>();
        paisesSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));

        paissSolicitante = this.getGeneralesService().getCachePaises();
        if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
            Collections.sort(paissSolicitante);
        } else {
            Collections.sort(paissSolicitante, Pais.getComparatorNombre());
        }
        for (Pais pais : paissSolicitante) {
            if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
                paisesSolicitante.add(new SelectItem(pais.getCodigo(), pais
                    .getCodigo() + "-" + pais.getNombre()));
            } else {
                paisesSolicitante.add(new SelectItem(pais.getCodigo(), pais
                    .getNombre()));
            }
        }

    }

    public void setSelectedDepartamentoSolicitante(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosSolicitante != null) {
            for (Departamento departamento : dptosSolicitante) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.solicitanteSeleccionado != null) {
            this.departamentoSolicitante = dptoSeleccionado;
        }
    }

    public String getSelectedDepartamentoSolicitante() {
        if (this.solicitanteSeleccionado != null &&
            this.departamentoSolicitante != null) {
            return this.departamentoSolicitante.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getDepartamentosSolicitante() {
        return departamentosSolicitante;
    }

    public void onChangeOrdenDepartamentosSolicitante() {
        this.updateDepartamentosSolicitante();
    }

    public void onChangeDepartamentosSolicitante() {
        this.municipioSolicitante = null;
        this.updateMunicipiosSolicitante();
    }

    public void cambiarOrdenDepartamentosSolicitante() {
        if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosSolicitante = EOrden.CODIGO;
        }
        this.updateDepartamentosSolicitante();
    }
//--------------------------------------------------------------------------------------------------

    public void updateDepartamentosSolicitante() {
        departamentosSolicitante = new ArrayList<SelectItem>();
        departamentosSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.dptosSolicitante = this.getGeneralesService()
            .getCacheDepartamentosPorPais(this.paisSolicitante.getCodigo());
        if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosSolicitante);
        } else {
            Collections.sort(this.dptosSolicitante,
                Departamento.getComparatorNombre());
        }

        //Se coloca por default el departamento de la territorial.
        if (!this.dptosSolicitante.isEmpty()) {
            EstructuraOrganizacional eo = this.getGeneralesService().
                buscarEstructuraOrganizacionalPorCodigo(this.usuario.getCodigoTerritorial());

            this.departamentoSolicitante = eo.getMunicipio().getDepartamento();
        }

        for (Departamento departamento : this.dptosSolicitante) {
            if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
                departamentosSolicitante.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                    departamento.getNombre()));
            } else {
                departamentosSolicitante.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    public void setSelectedMunicipioSolicitante(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null && this.munisSolicitante != null) {
            for (Municipio municipio : this.munisSolicitante) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioSolicitante = null;
        }
        if (this.solicitanteSeleccionado != null) {
            this.municipioSolicitante = municipioSelected;
        }
    }

    public String getSelectedMunicipioSolicitante() {
        if (this.solicitanteSeleccionado != null &&
            this.municipioSolicitante != null) {
            return this.municipioSolicitante.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getMunicipiosSolicitante() {
        return this.municipiosSolicitante;
    }

    public void onChangeMunicipiosSolicitante() {
        //TODO:: Implementar funcionalidad de este metodo
    }

    public void onChangeOrdenMunicipiosSolicitante() {
        this.updateMunicipiosSolicitante();
    }

    public void updateMunicipiosSolicitante() {
        municipiosSolicitante = new ArrayList<SelectItem>();
        munisSolicitante = new ArrayList<Municipio>();
        municipiosSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.solicitanteSeleccionado != null &&
            this.departamentoSolicitante != null &&
            this.departamentoSolicitante.getCodigo() != null) {
            munisSolicitante = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoSolicitante.getCodigo());
            if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
                Collections.sort(munisSolicitante);
            } else {
                Collections.sort(munisSolicitante,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisSolicitante) {
                if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
                    municipiosSolicitante.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                        municipio.getNombre()));
                } else {
                    municipiosSolicitante.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    public void cambiarOrdenMunicipiosSolicitante() {
        if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosSolicitante = EOrden.CODIGO;
        }
        this.updateMunicipiosSolicitante();
    }

    public String getOrdenPaisesSolicitante() {
        return ordenPaisesSolicitante.toString();
    }

    public String getOrdenDepartamentosSolicitante() {
        return ordenDepartamentosSolicitante.toString();
    }

    public String getOrdenMunicipiosSolicitante() {
        return ordenMunicipiosSolicitante.toString();
    }

    /**
     * Determina si los países están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenPaisesNombreSolicitante() {
        return this.ordenPaisesSolicitante.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenDepartamentosNombreSolicitante() {
        return this.ordenDepartamentosSolicitante.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenMunicipiosNombreSolicitante() {
        return this.ordenMunicipiosSolicitante.equals(EOrden.NOMBRE);
    }

    public List<SelectItem> getRelacionesSolicitante() {
        return this.relacionesSolicitante;
    }
//--------------------------------------------------------------------------------------------------

    public void generarRelacionesSolicitante() {
        List<SelectItem> values = generalMB.getSolicitanteSolicitudRelacV();
        List<SelectItem> auxValues = new ArrayList<SelectItem>();

        for (SelectItem si : values) {
            if (!si.getValue().equals(ESolicitanteSolicitudRelac.AFECTADO.toString()) && !si.
                getValue().equals(ESolicitanteSolicitudRelac.ENTE.toString())) {
                if (!this.solicitanteTramite) {
                    if (this.solicitudSeleccionada.isSolicitudAvisos()) {
                        if (!si.getValue().equals(
                            ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL.toString()) &&
                            !si.getValue().equals(
                                ESolicitanteSolicitudRelac.APODERADO.toString())) {
                            auxValues.add(si);
                        }
                    } else if (si.getValue()
                        .equals(ESolicitanteSolicitudRelac.INTERMEDIARIO
                            .toString())) {
                        // No se debe listar 'Intermediario' dentro de las
                        // relaciones del solicitante cuando es una revisión de
                        // avaluo o una auto estimación.
                        if (!(this.solicitudSeleccionada.isRevisionAvaluo() ||
                            this.solicitudSeleccionada
                                .isSolicitudAutoavaluo())) {
                            auxValues.add(si);
                        }
                    } else {
                        auxValues.add(si);
                    }
                } else {
                    auxValues.add(si);
                }
            }
            values = auxValues;
        }

        this.relacionesSolicitante = values;
    }
// --------------------------------------------------------------------------------------------------

    public boolean isSolicitanteTramite() {
        return this.solicitanteTramite;
    }

    public void setSolicitanteTramite(boolean solicitanteTramite) {
        this.solicitanteTramite = solicitanteTramite;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de guardar/actualizar el solicitante actual en la tabla solicitante. Además
     * de esto genera el objetos de tipo SolicitanteSolicitud o SolicitanteTramite correspondiente.
     *
     * @author fabio.navarrete
     */
    /*
     * @modified by juanfelipe.garcia :: 13-05-2014 :: ajuste en el método al guardar un solicitante
     * nuevo
     */
    public void guardarSolicitante() {

        Solicitante auxSolicitante;

        boolean validarSol = validarSolicitante();
        if (validarSol) {

            this.solicitanteSeleccionado.setDireccionPais(this.paisSolicitante);
            this.solicitanteSeleccionado
                .setDireccionDepartamento(this.departamentoSolicitante);
            this.solicitanteSeleccionado
                .setDireccionMunicipio(this.municipioSolicitante);

            boolean yaAdicionado = false;
            boolean solicitanteCoincidenteBool = this.solicitanteSeleccionado.
                isSolicitanteCoincidente();

            List<SolicitanteSolicitud> sols = this.solicitudSeleccionada.getSolicitanteSolicituds();

            for (SolicitanteSolicitud s : sols) {
                Solicitante sol = s.getSolicitante();
                if (sol.getNumeroIdentificacion() != null &&
                    sol.getTipoIdentificacion() != null &&
                    sol.getTipoIdentificacion().equals(
                        this.solicitanteSeleccionado.getTipoIdentificacion()) &&
                    sol.getNumeroIdentificacion().equals(
                        this.solicitanteSeleccionado.getNumeroIdentificacion())) {
                    yaAdicionado = true;
                    break;
                }
            }

            auxSolicitante = this.getTramiteService()
                .guardarActualizarSolicitante(this.solicitanteSeleccionado);

            if (auxSolicitante != null) {

                this.addMensajeInfo("El solicitante fue " +
                    (yaAdicionado ? "actualizado" : "adicionado") +
                    " correctamente");

                auxSolicitante.setDireccionPais(this.solicitanteSeleccionado
                    .getDireccionPais());
                auxSolicitante.setDireccionDepartamento(this.solicitanteSeleccionado
                    .getDireccionDepartamento());
                auxSolicitante.setDireccionMunicipio(this.solicitanteSeleccionado
                    .getDireccionMunicipio());
                auxSolicitante.setRelacion(this.solicitanteSeleccionado
                    .getRelacion());
                auxSolicitante.setNotificacionEmail(this.solicitanteSeleccionado
                    .getNotificacionEmail());
                auxSolicitante.setSolicitanteCoincidente(solicitanteCoincidenteBool);

                this.solicitanteSeleccionado = auxSolicitante;
                if (this.solicitanteTramite) {
                    SolicitanteTramite solTram = new SolicitanteTramite();
                    solTram.incorporarDatosSolicitante(auxSolicitante);
                    solTram.setTramite(this.tramiteSeleccionado);
                    solTram.setSolicitante(this.solicitanteSeleccionado);
                    int i;
                    for (i = 0; i < this.tramiteSeleccionado
                        .getSolicitanteTramites().size(); i++) {
                        if (this.tramiteSeleccionado.getSolicitanteTramites()
                            .get(i).getSolicitante().getId()
                            .compareTo(this.solicitanteSeleccionado.getId()) == 0) {

                            this.tramiteSeleccionado
                                .getSolicitanteTramites()
                                .get(i)
                                .setSolicitante(this.solicitanteSeleccionado);
                            this.tramiteSeleccionado
                                .getSolicitanteTramites()
                                .get(i)
                                .incorporarDatosSolicitante(this.solicitanteSeleccionado);
                            break;
                        }
                    }
                    if (i == this.tramiteSeleccionado.getSolicitanteTramites().size()) {
                        this.tramiteSeleccionado.getSolicitanteTramites().add(solTram);
                    }
                } else {
                    SolicitanteSolicitud solSol = new SolicitanteSolicitud();
                    solSol.incorporarDatosSolicitante(auxSolicitante);
                    solSol.setSolicitud(this.solicitudSeleccionada);
                    solSol.setSolicitante(this.solicitanteSeleccionado);
                    int i;
                    for (i = 0; i < this.solicitudSeleccionada.getSolicitanteSolicituds().size();
                        i++) {
                        if (this.solicitudSeleccionada
                            .getSolicitanteSolicituds().get(i)
                            .getSolicitante().getId() != null &&
                            this.solicitudSeleccionada
                                .getSolicitanteSolicituds().get(i)
                                .getSolicitante().getId()
                                .compareTo(this.solicitanteSeleccionado.getId()) == 0) {

                            this.solicitudSeleccionada
                                .getSolicitanteSolicituds()
                                .get(i)
                                .setSolicitante(this.solicitanteSeleccionado);
                            this.solicitudSeleccionada
                                .getSolicitanteSolicituds()
                                .get(i)
                                .incorporarDatosSolicitante(this.solicitanteSeleccionado);
                            break;
                        }
                    }
                    if (i == this.solicitudSeleccionada.getSolicitanteSolicituds().size()) {
                        this.solicitudSeleccionada.getSolicitanteSolicituds().add(solSol);
                    }
                    if (this.solicitanteAEditar != null) {
                        List<SolicitanteSolicitud> solicitantesSolicitudTemp =
                            new ArrayList<SolicitanteSolicitud>();
                        solicitantesSolicitudTemp.addAll(this.solicitudSeleccionada.
                            getSolicitanteSolicituds());

                        if (solicitantesSolicitudTemp != null && !solicitantesSolicitudTemp.
                            isEmpty()) {
                            int elementoDeLaLista = 0;
                            for (SolicitanteSolicitud ssTemp : solicitantesSolicitudTemp) {

                                if (this.solicitanteAEditar.getTipoIdentificacion().equals(ssTemp.
                                    getTipoIdentificacion()) &&
                                    this.solicitanteAEditar.getNumeroIdentificacion().equals(ssTemp.
                                        getNumeroIdentificacion()) &&
                                    (!this.solicitanteAEditar.getTipoIdentificacion().equals(
                                        this.solicitanteSeleccionado.getTipoIdentificacion()) ||
                                    !this.solicitanteAEditar.getNumeroIdentificacion().equals(
                                        this.solicitanteSeleccionado.getNumeroIdentificacion()))) {
                                    this.solicitudSeleccionada.getSolicitanteSolicituds().remove(
                                        elementoDeLaLista);
                                }
                                elementoDeLaLista++;
                            }
                        }
                        this.solicitanteAEditar = null;
                    }

                }
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Error al almacenar solicitante");
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que realiza la validación lógica de campos para solicitante
     *
     * @author juan.agudelo
     */
    private boolean validarSolicitante() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.solicitanteSeleccionado.getNotificacionEmail() != null &&
            this.solicitanteSeleccionado.getNotificacionEmail().equals(
                ESiNo.SI.getCodigo()) &&
            (this.solicitanteSeleccionado.getCorreoElectronico() == null ||
            this.solicitanteSeleccionado
                .getCorreoElectronico().isEmpty())) {

            String mensaje = "La opción notificación por email fue seleccionada pero" +
                " no se diligenció una cuenta de correo electrónico." +
                " Por favor complete los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
            return false;
        }

        if (this.solicitanteSeleccionado.getTelefonoPrincipal() != null &&
            !this.solicitanteSeleccionado.getTelefonoPrincipal()
                .isEmpty() &&
            this.solicitanteSeleccionado.getDireccion() != null &&
            !this.solicitanteSeleccionado.getDireccion().isEmpty()) {

            if (this.paisSolicitante == null) {

                String mensaje = "Pais: error de validación: valor requerido.";
                this.addMensajeError(
                    "solicitanteSolicituddatosSolicitanteForm:paisSolicitante",
                    mensaje);
                context.addCallbackParam("error", "error");
                return false;

            }

            if (this.paisSolicitante.getCodigo().equals(Constantes.COLOMBIA)) {

                if (this.departamentoSolicitante == null) {
                    String mensaje = "Departamento: error de validación: valor requerido.";
                    this.addMensajeError(
                        "solicitanteSolicituddatosSolicitanteForm:departamentoSolicitante",
                        mensaje);
                    context.addCallbackParam("error", "error");
                    return false;
                }

                if (this.municipiosSolicitante == null) {
                    String mensaje = "Municipio: error de validación: valor requerido.";
                    this.addMensajeError(
                        "solicitanteSolicituddatosSolicitanteForm:municipioSolicitante",
                        mensaje);
                    context.addCallbackParam("error", "error");
                    return false;
                }

            }
        }
        if (this.solicitudSeleccionada.isSolicitudAvisos() &&
            this.solicitudSeleccionada.getNumero() == null) {
            if (!this.solicitanteSeleccionado.isJuridica() ||
                !this.solicitanteSeleccionado.getRelacion()
                    .equals(ESolicitanteSolicitudRelac.INTERMEDIARIO
                        .toString())) {
                this.addMensajeError(
                    "Para el tipo de solicitud Avisos de Registro, el solicitante debe ser Jurídico e Intermediario");
                context.addCallbackParam("error", "error");
                return false;
            }
        }

        //se valida el codigo postal
        if (this.solicitanteSeleccionado.getCodigoPostal() != null &&
            !this.solicitanteSeleccionado.getCodigoPostal().isEmpty() &&
            !this.dptosSolicitante.isEmpty()) {

            if (this.solicitanteSeleccionado.getDireccion() == null || this.solicitanteSeleccionado.
                getDireccion().isEmpty()) {
                this.addMensajeError("Dirección: Error de validación: se necesita un valor.");
                context.addCallbackParam("error", "error");
                return false;
            }

            if (this.departamentoSolicitante == null || this.solicitanteSeleccionado.getDireccion().
                isEmpty()) {
                this.addMensajeError("Departamento: Error de validación: se necesita un valor.");
                context.addCallbackParam("error", "error");
                return false;
            }

            if (!UtilidadesWeb.validarSoloDigitos(this.solicitanteSeleccionado.getCodigoPostal())) {
                this.addMensajeError("El Código postal debe contener solo números");
                context.addCallbackParam("error", "error");
                return false;
            }

            if (this.solicitanteSeleccionado.getCodigoPostal().length() < 6) {
                this.addMensajeError("El Código postal debe tener por lo menos 6 digitos");
                context.addCallbackParam("error", "error");
                return false;
            }
            String initCodigoPostal = this.solicitanteSeleccionado.getCodigoPostal().substring(0, 2);

            if (!this.departamentoSolicitante.getCodigo().equals(initCodigoPostal)) {
                this.addMensajeError(
                    "Código postal no corresponde con el código del departamento seleccionado");
                context.addCallbackParam("error", "error");
                return false;
            }

        }

        if (!this.dptosSolicitante.isEmpty() &&
            this.solicitanteSeleccionado.getDireccion() != null &&
            !this.solicitanteSeleccionado.getDireccion().isEmpty() &&
            this.municipioSolicitante == null) {
            this.addMensajeError("Municipio: Error de validación: se necesita un valor.");
            context.addCallbackParam("error", "error");
            return false;
        }

        if ((this.solicitudSeleccionada.isRevisionAvaluo() || this.solicitudSeleccionada.
            isSolicitudAutoavaluo()) &&
            this.solicitanteSeleccionado.getRelacion() != null &&
            this.solicitanteSeleccionado.getRelacion().equals(ESolicitanteSolicitudRelac.TESORERIA.
                toString()) &&
            (this.solicitanteSeleccionado.getTipoPersona() == null ||
            !this.solicitanteSeleccionado.getTipoPersona().equals(Constantes.TIPO_PERSONA_JURIDICA))) {
            this.addMensajeError(
                "La relación del solicitante sólo puede ser 'Tesorería' si el tipo de persona es 'Jurídica'.");
            context.addCallbackParam("error", "error");
            return false;
        }

        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado cuando se radica un trámite.
     *
     * @author juan.agudelo
     *
     * @modified by juanfelipe.garcia :: 31-07-2013 :: adición de logica para CC-NP-CO-012
     * @modified pedro.garcia 24-10-2013 cambio del método que guarda la solicitud. Definición de
     * mensajes a cliente
     * @modified lorena.salamanca :: 19-08-2015 :: Se agregan los predios si la mutacion es de
     * quinta y es PH o condominio
     */
    public void consolidarTramite() {

        RequestContext context = RequestContext.getCurrentInstance();

        List<TramiteRectificacion> tramiteRectificaciones;
        TramiteRectificacion tempTramiteRectificacion;

        this.areaTerreno = "";
        // Validar el contenido del trámite
        boolean validacion = validarDatosTramite();

        if (validacion) {
            if (this.documentacionTramite != null &&
                !this.documentacionTramite.isEmpty()) {

                if (this.documentacionRequerida != null) {
                    //v1.1.7
                    int contadorTramitesFaltantes = 0;
                    for (TramiteDocumentacion td : this.documentacionRequerida) {
                        if (td.getCantidadFolios() == null || td.getCantidadFolios() == 0) {
                            td.setCantidadFolios(0);
                            td.setAdicional(ESiNo.NO.getCodigo());
                            this.documentoSeleccionado = td;
                            if (td.getDigital() == null ||
                                td.getDigital().isEmpty()) {
                                td.setDigital(ESiNo.NO.getCodigo());
                            }
                            if (td.getRequerido().equals(ESiNo.SI.getCodigo()) &&
                                td.getAportado().equals(
                                    ESiNo.NO.getCodigo())) {
                                contadorTramitesFaltantes++;
                            }
                            Documento d = generarDocumento(td);
                            td.setDocumentoSoporte(d);
                        }
                    }

                    if (contadorTramitesFaltantes == 0) {
                        this.tramiteSeleccionado
                            .setDocumentacionCompleta(ESiNo.SI.getCodigo());
                    } else {
                        this.tramiteSeleccionado
                            .setDocumentacionCompleta(ESiNo.NO.getCodigo());
                    }
                }
                this.documentacionTramite = new ArrayList<TramiteDocumentacion>();
                this.documentacionTramite.addAll(this.documentacionRequerida);
                this.documentacionTramite.addAll(this.documentacionAdicional);

                this.tramiteSeleccionado
                    .setTramiteDocumentacions(this.documentacionTramite);
            }

            if (this.solicitudSeleccionada != null) {
                this.tramiteSeleccionado.setSolicitud(this.solicitudSeleccionada);
            }

            if (this.tramiteSeleccionado.isSegundaEnglobe()) {
                List<TramitePredioEnglobe> tpeList = new ArrayList<TramitePredioEnglobe>();
                TramitePredioEnglobe pteTemp;

                for (int p = 0; p < this.getPrediosTramite().size(); p++) {
                    pteTemp = new TramitePredioEnglobe();
                    pteTemp.setPredio(this.getPrediosTramite().get(p).getPredio());
                    pteTemp.setEnglobePrincipal(this.getPrediosTramite().get(p).
                        getEnglobePrincipal() == null ?
                            ESiNo.NO.getCodigo() : this.getPrediosTramite().get(p).
                            getEnglobePrincipal());
                    pteTemp.setTramite(this.tramiteSeleccionado);
                    pteTemp.setUsuarioLog(this.usuario.getLogin());
                    pteTemp.setFechaLog(new Date(System.currentTimeMillis()));
                    tpeList.add(pteTemp);
                }
                this.tramiteSeleccionado.setTramitePredioEnglobes(tpeList);
            }

            // D: si el trámite es de rectificación debe guardar los
            // TramiteRectificacion
            if (this.tramiteSeleccionado.isRectificacion()) {
                tramiteRectificaciones = new ArrayList<TramiteRectificacion>();
                for (DatoRectificar dr : this.selectedDatosRectifComplDataTable) {
                    dr.setUsuarioLog(this.usuario.getLogin());
                    dr.setFechaLog(new Date());
                    tempTramiteRectificacion = new TramiteRectificacion();
                    tempTramiteRectificacion.setTramite(this.tramiteSeleccionado);
                    tempTramiteRectificacion.setDatoRectificar(dr);
                    tempTramiteRectificacion.setUsuarioLog(this.usuario.getLogin());
                    tempTramiteRectificacion.setFechaLog(new Date());
                    tramiteRectificaciones.add(tempTramiteRectificacion);
                }
                this.tramiteSeleccionado.setTramiteRectificacions(tramiteRectificaciones);
            }

            // Si la solicitud es de avisos, ajustar las variables que llevan el
            // conteo de los avisos rechazados, aceptados y pendientes
            if (this.solicitudSeleccionada.isSolicitudAvisos()) {
                Integer numAceptados = this.solicitudSeleccionada
                    .getSolicitudAvisoRegistro().getAvisosAceptados();
                this.solicitudSeleccionada.getSolicitudAvisoRegistro()
                    .setAvisosAceptados(++numAceptados);
            }

            //Se determina si el predio es fiscal
            if (this.tramiteSeleccionado.isQuintaOmitido()) {
                Predio p = this.getConservacionService().getPredioPorNumeroPredial(
                    this.tramiteSeleccionado.getNumeroPredial());
                if (p != null &&
                    p.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                    this.banderaPredioFiscal = true;
                }
            }

            if (this.tramiteSeleccionado.getPredio() != null &&
                this.tramiteSeleccionado.getPredio().getTipoCatastro().equals(
                    EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo()) &&
                this.tramiteSeleccionado.getPredio().getTipoCatastro() != null) {
                this.banderaPredioFiscal = true;
            }

            //D: se define la clasificación del trámite
            if (this.tramiteSeleccionado.isMutacion() &&
                ((this.tramiteSeleccionado.isSegunda() && !this.banderaPredioFiscal) ||
                (this.tramiteSeleccionado.isTercera() && !this.banderaPredioFiscal) ||
                (this.tramiteSeleccionado.isQuinta() && !this.banderaPredioFiscal))) {

                this.tramiteSeleccionado
                    .setClasificacion(ETramiteClasificacion.TERRENO.toString());
            } else {
                this.tramiteSeleccionado
                    .setClasificacion(ETramiteClasificacion.OFICINA.toString());
            }

            //Los predios de quinta no se radican con predios asociados
            if (!this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.getPredio() != null) {
                //felipe.cadena::Se agregan los predios relacionados a la ficha matriz               
                this.agregarPrediosFM();
            }

            //Se agregan los predios si la mutacion es de quinta masivo y es PH o condominio
            if (this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.getPredio() != null) {
                if ((this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                    Constantes.FM_CONDOMINIO_END)) ||
                    (this.tramiteSeleccionado.isQuinta() && this.tramiteSeleccionado.getPredio().
                    getNumeroPredial().endsWith(Constantes.FM_PH_END))) {
                    this.agregarPrediosFM();
                }
            }

            // Se realiza el guardado de tramite estados finalizando el metodo guardarActualizarSolicitud2
            this.tramiteSeleccionado.setEstado(ETramiteEstado.RECIBIDO.getCodigo());
            this.tramiteSeleccionado.setSolicitud(this.solicitudSeleccionada);
            this.tramiteSeleccionado.setUsuarioLog(this.usuario.getLogin());
            this.tramiteSeleccionado.setFechaLog(new Date(System.currentTimeMillis()));
            this.tramitesSolicitud.add(this.tramiteSeleccionado);
            this.solicitudSeleccionada.setTramites(this.tramitesSolicitud);

            if (this.tramiteSeleccionado.getDepartamento() == null &&
                this.departamentoTramite != null) {
                this.tramiteSeleccionado
                    .setDepartamento(this.departamentoTramite);
            }

            if (this.tramiteSeleccionado.getMunicipio() == null &&
                this.municipioTramite != null) {
                this.tramiteSeleccionado.setMunicipio(this.municipioTramite);
            }

            try {
                if (this.tramiteSeleccionado.isEsTramiteDeRevisionDeAvaluo() ||
                    this.tramiteSeleccionado.isTramiteDeAutoavaluo()) {

                    if (this.solicitudSeleccionada.getSolicitante() != null) {
                        // Esta el propietario en la lista de solicitantes
                        this.solicitudSeleccionada.setEstado(ESolicitudEstado.TRAMITES_CONSOLIDADOS.
                            getCodigo());
                    }
                }

                Object[] actualizacionSolicitud =
                    this.getTramiteService().guardarActualizarSolicitud2(
                        this.usuario,
                        this.solicitudSeleccionada);

                this.solicitudSeleccionada = (Solicitud) ((actualizacionSolicitud[0] != null) ?
                    actualizacionSolicitud[0] : this.solicitudSeleccionada);

                if (actualizacionSolicitud[1] != null &&
                    !((HashMap<Integer, ArrayList<String>>) actualizacionSolicitud[1]).isEmpty()) {
                    UtilidadesWeb.definirMensajesResultadoParaCliente(this,
                        (HashMap<Integer, ArrayList<String>>) actualizacionSolicitud[1]);
                }

                //N: se extrajo en ese método lo que estaba aquí
                avanzarOCrearProcesosTramites();

                if (this.solicitudSeleccionada.isViaGubernativa() || this.solicitudSeleccionada.
                    isRevocatoriaDirecta()) {
                    finalizarSolicitud();
                }

                String mensaje = "El trámite se radicó exitosamente";
                this.addMensajeInfo(mensaje);

                this.banderaMostrarBotonImprimirConstanciaRadicacion = this.
                    activarBotonImprimirConstanciaRadicacion();

                //Esto se agrega cuando se radica un trámite de derecho de petición o tutela
                //javier.aponte 14/11/2013
                //Enviar correo para el director territorial y el responsable de conservación
                if (this.formaDePeticionDerechoDePeticion || this.formaDePeticionTutela) {

                    ConsultaTramiteMB mbConsultaTramite = (ConsultaTramiteMB) UtilidadesWeb.
                        getManagedBean("consultaTramite");

                    mbConsultaTramite.setSelectedTramiteRow(
                        this.solicitudSeleccionada.getTramites().get(0));

                    this.enviarCorreoInformandoDerechoPeticionOTutela();

                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                context.addCallbackParam("error", "error");
                String mensaje = "Ocurrió un error al procesar el trámite," +
                    " por favor intente nuevamente";
                initNuevoTramiteSolicitud();
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "El trámite no cuenta con la información necesaria," +
                " por favor ingrese los datos requeridos e intente nuevamente";
            this.addMensajeError(mensaje);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado para realizar las validaciones para guardar un trámite
     *
     * @author juan.agudelo
     *
     */
    private boolean validarDatosTramite() {
        RequestContext context = RequestContext.getCurrentInstance();

        if (this.solicitudSeleccionada == null) {
            context.addCallbackParam("error", "error");
            String mensaje = "El trámite no cuenta con una solicitud asociada";
            this.addMensajeError(mensaje);
            return false;
        } else if (this.tramiteSeleccionado.isSegundaEnglobe()) {
            if (this.tramiteSeleccionado.getTramitePredioEnglobes() == null ||
                this.tramiteSeleccionado.getTramitePredioEnglobes().isEmpty()) {
                context.addCallbackParam("error", "error");
                String mensaje = "El trámite es de tipo englobe pero no cuenta con una lista de" +
                    " predios asociados." +
                    " Por favor corrija los datos e intente nuevamente";
                this.addMensajeError(mensaje);
                return false;
            }
        } else if (this.tramiteSeleccionado.getTipoTramite().equalsIgnoreCase(
            ETramiteTipoTramite.RECTIFICACION.getCodigo())) {
            if (this.tramiteSeleccionado.getTramiteRectificacions() == null ||
                this.tramiteSeleccionado.getTramiteRectificacions()
                    .isEmpty()) {
                context.addCallbackParam("error", "error");
                String mensaje =
                    "El trámite seleccionado es de rectificación y no cuenta con datos" +
                    " a rectificar asociados." +
                    " Por favor corrija los datos e intente nuevamente";
                this.addMensajeError(mensaje);
                return false;
            }
        }

        boolean valido = true;
        for (SolicitanteSolicitud ss : this.solicitudSeleccionada.getSolicitanteSolicituds()) {
            if (ss.getRelacion().equalsIgnoreCase(ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL.
                getValor())) {
                valido = false;
                for (TramiteDocumentacion td : this.getDocumentacionRequerida()) {
                    if (td.getTipoDocumento().getId().equals(
                        Constantes.TIPO_DOCUMENTO_OBLIGATORIO_PARA_REPRESENTANTE_LEGAL) &&
                        td.isAporta()) {
                        valido = true;
                        break;
                    }
                }
                for (TramiteDocumentacion td : this.getDocumentacionAdicional()) {
                    if (td.getTipoDocumento().getId().equals(
                        Constantes.TIPO_DOCUMENTO_OBLIGATORIO_PARA_REPRESENTANTE_LEGAL) &&
                        td.isAporta()) {
                        valido = true;
                        break;
                    }
                }
                if (!valido) {
                    context.addCallbackParam("error", "error");
                    String mensaje =
                        "El representante legal debe aportar 'Fotocopia del Certificado de existencia y representación legal'";
                    this.addMensajeError(mensaje);
                }
            }
        }
        boolean valido2 = true;
        for (SolicitanteSolicitud ss : this.solicitudSeleccionada.getSolicitanteSolicituds()) {
            if (ss.getRelacion().equalsIgnoreCase(ESolicitanteSolicitudRelac.APODERADO.getValor())) {
                valido2 = false;
                for (TramiteDocumentacion td : this.getDocumentacionRequerida()) {
                    if (td.getTipoDocumento().getId().equals(
                        Constantes.TIPO_DOCUMENTO_OBLIGATORIO_PARA_APODERADO) &&
                        td.isAporta()) {
                        valido2 = true;
                        break;
                    }
                }
                for (TramiteDocumentacion td : this.getDocumentacionAdicional()) {
                    if (td.getTipoDocumento().getId().equals(
                        Constantes.TIPO_DOCUMENTO_OBLIGATORIO_PARA_APODERADO) &&
                        td.isAporta()) {
                        valido2 = true;
                        break;
                    }
                }
                if (!valido2) {
                    context.addCallbackParam("error", "error");
                    String mensaje = "El apoderado debe aportar 'Poder'";
                    this.addMensajeError(mensaje);
                }
            }
        }
        return valido && valido2;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Prepara el solicitante para para la inserción del nuevo objeto Solicitante
     *
     * @author fabio.navarrete
     */
    public void configureNewSolicitante() {
        this.solicitanteSeleccionado = new Solicitante();
        this.solicitanteSeleccionado
            .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                .getCodigo());
        this.solicitanteSeleccionado.setTipoPersona(EPersonaTipoPersona.NATURAL
            .getCodigo());
        Pais defaultPais = new Pais();
        defaultPais.setCodigo(Constantes.COLOMBIA);
        this.paisSolicitante = defaultPais;
        this.departamentoSolicitante = null;
        this.municipioSolicitante = null;
        this.solicitanteSeleccionado.setDireccionPais(defaultPais);
        this.solicitanteSeleccionado
            .setDireccionDepartamento(new Departamento());
        this.solicitanteSeleccionado.setDireccionMunicipio(new Municipio());
        this.solicitanteSeleccionado.setFechaLog(new Date());
        this.solicitanteSeleccionado.setUsuarioLog(this.usuario.getLogin());
        this.updatePaisesSolicitante();
        this.updateDepartamentosSolicitante();
        this.updateMunicipiosSolicitante();

        this.generarRelacionesSolicitante();
        this.banderaBusquedaSolicitanteRealizada = false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de buscar un solicitante a partir de los datos de documento
     *
     * @author fabio.navarrete
     */
    public void buscarSolicitante() {
        LOGGER.debug("Entrando a buscar solicitante");

        List<Solicitante> solicitantes = this.getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacionsList(
                this.solicitanteSeleccionado.getTipoIdentificacion(),
                this.solicitanteSeleccionado.getNumeroIdentificacion());

        Solicitante sol = null;

        if (solicitantes != null && solicitantes.size() == 1) {
            sol = solicitantes.get(0);
        } else if (solicitantes != null && solicitantes.size() > 1) {
            this.solicitantesCoincidentes = solicitantes;
            this.solicitanteCoincidenteSeleccionado = null;
            return;
        }

        if (sol != null) {
            this.paisSolicitante = sol.getDireccionPais();
            this.updateDepartamentosSolicitante();
            this.departamentoSolicitante = sol.getDireccionDepartamento();
            this.updateMunicipiosSolicitante();
            this.municipioSolicitante = sol.getDireccionMunicipio();
            this.solicitanteSeleccionado = sol;
        } else {
            this.addMensajeWarn(
                "No existe un solicitante con esa identificación. Por favor ingrese los datos del nuevo solicitante.");
            LOGGER.debug("buscarSolicitante Return null");
            String tipoIdentif = this.solicitanteSeleccionado.getTipoIdentificacion();
            String numDoc = this.solicitanteSeleccionado
                .getNumeroIdentificacion();
            String tipoPersona1 = this.solicitanteSeleccionado.getTipoPersona();
            setDefaultDataSolicitante();

            //Se coloca por default el departamento de la territorial.
            EstructuraOrganizacional eo = this.getGeneralesService().
                buscarEstructuraOrganizacionalPorCodigo(this.usuario.getCodigoTerritorial());
            this.departamentoSolicitante = eo.getMunicipio().getDepartamento();

            this.solicitanteSeleccionado.setTipoIdentificacion(tipoIdentif);
            this.solicitanteSeleccionado.setNumeroIdentificacion(numDoc);
            this.solicitanteSeleccionado.setTipoPersona(tipoPersona1);

            //se calcula al digito de verificación
            if (tipoIdentif.equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                this.solicitanteSeleccionado.
                    setDigitoVerificacion(String.valueOf(
                        this.getGeneralesService().calcularDigitoVerificacion(numDoc)));
            }
        }
        this.solicitanteSeleccionado
            .setRelacion(ESolicitanteSolicitudRelac.PROPIETARIO.toString());

        // Se registra el siguiente parámetro para que no se abra la tabla
        // de solicitantes coincidentes
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("error", "error");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que quema los datos de fechas departamento tipo de identificación y demás requeridos
     * para iniciar los solicitantes requeridos.
     *
     * @author fabio.navarrete
     */
    private void setDefaultDataSolicitante() {
        Solicitante sol = new Solicitante();
        sol.setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
        Pais defaultPais = new Pais();
        defaultPais.setCodigo(Constantes.COLOMBIA);
        this.paisSolicitante = defaultPais;
        this.departamentoSolicitante = new Departamento();
        this.municipioSolicitante = new Municipio();
        sol.setDireccionPais(defaultPais);
        sol.setDireccionDepartamento(new Departamento());
        sol.setDireccionMunicipio(new Municipio());
        sol.setFechaLog(new Date());
        sol.setUsuarioLog(this.usuario.getLogin());
        sol.setRelacion(Constantes.RELACION_DEFECTO);
        this.solicitanteSeleccionado = sol;
    }

    // --------------------------------------------------------------------------
    public void setSelectedDepartamentoTramite(String codigo) {
        Departamento dptoSeleccionado = null;
        this.numeroPredialParte1 = codigo;
        if (codigo != null && this.dptosTramite != null) {
            for (Departamento departamento : dptosTramite) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.tramiteSeleccionado != null) {
            this.departamentoTramite = dptoSeleccionado;
        }
    }

    // --------------------------------------------------------------------------
    public String getSelectedDepartamentoTramite() {
        if (this.tramiteSeleccionado != null &&
            this.departamentoTramite != null) {
            return this.departamentoTramite.getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public List<SelectItem> getDepartamentosTramite() {
        return departamentosTramite;
    }

    // --------------------------------------------------------------------------
    public void onChangeOrdenDepartamentosTramite() {
        this.updateDepartamentosTramite();
    }

    // --------------------------------------------------------------------------
    public void onChangeDepartamentosTramite() {
        this.municipioTramite = null;
        this.updateMunicipiosTramite();
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenDepartamentosTramite() {
        if (this.ordenDepartamentosTramite.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosTramite = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosTramite = EOrden.CODIGO;
        }
        this.updateDepartamentosTramite();
    }

    // --------------------------------------------------------------------------
    public void updateDepartamentosTramite() {
        departamentosTramite = new LinkedList<SelectItem>();
        departamentosTramite.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.usuarioDelegado) {
            this.dptosTramite = (ArrayList<Departamento>) this.getGeneralesService().
                getDepartamentoByCodigoEstructuraOrganizacional(this.usuario.getCodigoTerritorial());
        } else {
            this.dptosTramite = this.getGeneralesService()
                .buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
                    Constantes.COLOMBIA);

        }
        if (this.ordenDepartamentosTramite.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosTramite);
        } else {
            Collections.sort(this.dptosTramite,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : this.dptosTramite) {
            if (this.ordenDepartamentosTramite.equals(EOrden.CODIGO)) {
                departamentosTramite.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                    departamento.getNombre()));
            } else {
                departamentosTramite.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

    }

    // --------------------------------------------------------------------------
    public void setSelectedMunicipioTramite(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null) {
            this.numeroPredialParte2 = codigo.substring(2, 5);
        }
        if (codigo != null && this.munisTramite != null) {
            for (Municipio municipio : this.munisTramite) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioTramite = null;
        }
        if (this.solicitudSeleccionada != null) {
            this.municipioTramite = municipioSelected;
        }
    }

    public boolean isActividadEncontradaBool() {
        return actividadEncontradaBool;
    }

    public void setActividadEncontradaBool(boolean actividadEncontradaBool) {
        this.actividadEncontradaBool = actividadEncontradaBool;
    }

    // --------------------------------------------------------------------------
    public String getSelectedMunicipioTramite() {
        if (this.solicitudSeleccionada != null && this.municipioTramite != null) {
            return this.municipioTramite.getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public List<SelectItem> getMunicipiosTramite() {
        return this.municipiosTramite;
    }

    // --------------------------------------------------------------------------
    public void updateMunicipiosTramite() {
        this.municipiosTramite = new ArrayList<SelectItem>();
        this.munisTramite = new ArrayList<Municipio>();
        this.municipiosTramite.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.tramiteSeleccionado != null &&
            this.departamentoTramite != null &&
            this.departamentoTramite.getCodigo() != null) {
            if (this.usuarioDelegado) {
                List<Municipio> munisTramiteAux = this
                    .getGeneralesService()
                    .buscarMunicipioPorIdOficina(this.usuario.getCodigoEstructuraOrganizacional());

                for (Municipio municipio : munisTramiteAux) {
                    if (municipio.getDepartamento().getCodigo().equals(this.departamentoTramite.
                        getCodigo())) {
                        this.munisTramite.add(municipio);
                    }

                }

            } else {
                List<Municipio> munisTramiteAux = this
                    .getGeneralesService()
                    .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
                        this.departamentoTramite.getCodigo());

                for (Municipio municipio : munisTramiteAux) {
                    for (Municipio munDel : this.municipiosDelegados) {
                        if (!municipio.getCodigo().equals(munDel.getCodigo())) {
                            this.munisTramite.add(municipio);
                        }
                    }
                }
            }
            if (this.ordenMunicipiosTramite.equals(EOrden.CODIGO)) {
                Collections.sort(munisTramite);
            } else {
                Collections.sort(munisTramite, Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisTramite) {
                if (this.ordenMunicipiosTramite.equals(EOrden.CODIGO)) {
                    municipiosTramite.add(new SelectItem(municipio.getCodigo(),
                        municipio.getCodigo3Digitos() + "-" +
                        municipio.getNombre()));
                } else {
                    municipiosTramite.add(new SelectItem(municipio.getCodigo(),
                        municipio.getNombre()));
                }
            }
        }

        for (SelectItem si : this.municipiosTramite) {
            MunicipioComplemento mc = this.getGeneralesService().
                obtenerMunicipioComplementoPorCodigo(si.getValue().toString());

            if (mc == null) {
                continue;
            }
            if (mc.getConInformacionGdb().equals(ESiNo.NO.getCodigo())) {
                si.setDisabled(true);
            }
        }

    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenMunicipiosTramite() {
        if (this.ordenMunicipiosTramite.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosTramite = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosTramite = EOrden.CODIGO;
        }
        this.updateMunicipiosTramite();
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosNombreTramite() {
        return this.ordenDepartamentosTramite.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenMunicipiosNombreTramite() {
        return this.ordenMunicipiosTramite.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por CODIGO
     *
     * @return
     */
    public boolean isOrdenDepartamentosCodigoTramite() {
        return this.ordenDepartamentosTramite.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenMunicipiosCodigoTramite() {
        return this.ordenMunicipiosTramite.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    public void setSelectedDepartamentoDocumento(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosDocumento != null) {
            for (Departamento departamento : dptosDocumento) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.documentoSeleccionado != null) {
            this.departamentoDocumento = dptoSeleccionado;
        }
    }

    // --------------------------------------------------------------------------
    public String getSelectedDepartamentoDocumento() {
        if (this.documentoSeleccionado != null &&
            this.departamentoDocumento != null) {
            return this.departamentoDocumento.getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public List<SelectItem> getDepartamentosDocumento() {
        return departamentosDocumento;
    }

    // --------------------------------------------------------------------------
    public void onChangeOrdenDepartamentosDocumento() {
        this.updateDepartamentosDocumento();
    }

    // --------------------------------------------------------------------------
    public void onChangeDepartamentosDocumento() {
        this.municipioDocumento = null;
        this.updateMunicipiosDocumento();
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenDepartamentosDocumento() {
        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosDocumento = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosDocumento = EOrden.CODIGO;
        }
        this.updateDepartamentosDocumento();
    }

    // --------------------------------------------------------------------------
    public void updateDepartamentosDocumento() {
        departamentosDocumento = new LinkedList<SelectItem>();
        departamentosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.usuarioDelegado) {
            this.dptosDocumento = this.getGeneralesService().
                getCacheDepartamentosPorTerritorial(this.usuario.getCodigoTerritorial());

        } else {
            this.dptosDocumento = this.getGeneralesService()
                .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        }

        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosDocumento);
        } else {
            Collections.sort(this.dptosDocumento,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : this.dptosDocumento) {
            if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                    departamento.getNombre()));
            } else {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

    }

    // --------------------------------------------------------------------------
    public void setSelectedMunicipioDocumento(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null && this.munisDocumento != null) {
            for (Municipio municipio : this.munisDocumento) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioDocumento = null;
        }
        if (this.solicitudSeleccionada != null) {
            this.municipioDocumento = municipioSelected;
        }
    }

    // --------------------------------------------------------------------------
    public String getSelectedMunicipioDocumento() {
        if (this.solicitudSeleccionada != null &&
            this.municipioDocumento != null) {
            return this.municipioDocumento.getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public List<SelectItem> getMunicipiosDocumento() {
        return this.municipiosDocumento;
    }

    // --------------------------------------------------------------------------
    public void updateMunicipiosDocumento() {
        municipiosDocumento = new ArrayList<SelectItem>();
        munisDocumento = new ArrayList<Municipio>();
        municipiosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.documentoSeleccionado != null &&
            this.departamentoDocumento != null &&
            this.departamentoDocumento.getCodigo() != null) {
            if (this.usuarioDelegado) {
                this.munisDocumento = this.getGeneralesService().
                    getCacheMunicipiosTerritorialPorDepartametoYCodigoEstructuraOrganizacional(
                        this.departamentoDocumento.getCodigo(),
                        this.usuario.getCodigoTerritorial());
            } else {
                this.munisDocumento = this.getGeneralesService()
                    .getCacheMunicipiosPorDepartamento(
                        this.departamentoDocumento.getCodigo());
            }
            if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                Collections.sort(munisDocumento);
            } else {
                Collections.sort(munisDocumento,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisDocumento) {
                if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                        municipio.getNombre()));
                } else {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenMunicipiosDocumento() {
        if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosDocumento = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosDocumento = EOrden.CODIGO;
        }
        this.updateMunicipiosDocumento();
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosNombreDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenMunicipiosNombreDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosCodigoDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por CODIGO
     *
     * @return
     */
    public boolean isOrdenMunicipiosCodigoDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Revisa si un trámite de documentación incompleta, tiene documentos aportados, digitalizables
     * que no han sido cargados
     *
     * @author david.cifuentes
     */
    public void verEstadoDelTramiteDocumentacionADigitalizar() {

        int contador = 0;
        if (this.documentacionRequerida != null) {

            for (TramiteDocumentacion td : this.documentacionRequerida) {
                if (td != null) {

                    if (td.getDigital() != null &&
                        td.getDigital().equals(ESiNo.SI.getCodigo()) &&
                        (td.getDocumentoSoporte() == null ||
                        td.getDocumentoSoporte().getArchivo() == null ||
                        td.getDocumentoSoporte()
                            .getArchivo()
                            .equals(Constantes.CONSTANTE_CADENA_VACIA_DB) || td
                        .getDocumentoSoporte().getArchivo()
                        .isEmpty())) {
                        contador++;
                    }
                }
            }
        }
        if (contador > 0) {
            this.documentacionADigitalizarBool = true;
        } else {
            this.documentacionADigitalizarBool = false;
        }
    }

    // ----------------------------------------------
    public void reiniciarVariablesDeBusquedaViaGubernativa() {
        this.datosConsultaTramite = new FiltroDatosConsultaTramitesNotificacion();
        this.numResolucion = new String();
        this.listTramResolucion = new ArrayList<String>();
        this.listTramitesBuscadosPorCriterio = new ArrayList<Tramite>();
        this.numPredialBool = false;
        this.selectedTramiteGubernativaBuscado = new Tramite();
        this.selectedTramiteGubernativaBuscadoBool = false;
        this.selectedTramitesGubernativaBuscados = null;

        if (this.solicitudSeleccionada.isViaGubernativa()) {
            this.tramitesGubernativaAll = new ArrayList<Tramite>();
        }
    }

    // ----------------------------------------------
    public void buscarTramitesViaGubernativa() {

        List<Documento> listResolucionBusqueda = null;
        List<Tramite> listTramBusqueda = null;

        if ("numeroPredial".equals(this.criterio) &&
            this.datosConsultaTramite != null &&
            this.datosConsultaTramite.getNumeroPredialS1() != null &&
            this.datosConsultaTramite.getNumeroPredialS1().compareTo("") == 0) {
            this.addMensajeError("Digite un valor para el Número Predial");
            return;
        } else if ("resolucion".equals(this.criterio) &&
            this.numResolucion.trim().isEmpty()) {
            this.addMensajeError("Digite un valor para el número de resolución.");
            return;
        }

        if ("numeroPredial".equals(this.criterio) &&
            this.datosConsultaTramite != null &&
            this.datosConsultaTramite.getNumeroPredialS1() != null &&
            !this.datosConsultaTramite.getNumeroPredialS1().trim()
                .isEmpty()) {
            this.datosConsultaTramite.assembleNumeroPredialFromSegments();

            //felipe.cadena::05-01-2017::Validaciones delegacion
            String codigoMun = (this.datosConsultaTramite.getNumeroPredialS1() +
                this.datosConsultaTramite.getNumeroPredialS2());
            if (!this.validarMunicipioDelegado(codigoMun,
                ECodigoErrorVista.CONSULTA_DELEGACION_001,
                ECodigoErrorVista.CONSULTA_DELEGACION_011)) {
                return;
            }

            listResolucionBusqueda = this.getGeneralesService()
                .buscarResolucionesByNumeroPredialPredio(
                    this.datosConsultaTramite.getNumeroPredial());
            if (listResolucionBusqueda != null &&
                !listResolucionBusqueda.isEmpty()) {
                listTramResolucion = new ArrayList<String>();
                for (Documento tramTemp : listResolucionBusqueda) {
                    if (tramTemp != null) {
                        this.listTramResolucion.add(tramTemp
                            .getNumeroDocumento());
                    }
                }
            } else {
                this.addMensajeWarn(
                    "No se encontraron trámites para este número predial, por favor verifique la información.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        }
        if ("resolucion".equals(this.criterio) && this.numResolucion != null) {

            // Buscar la resolución asociada
            this.documentoResolucionVG = this.getGeneralesService()
                .buscarDocumentoPorNumeroDocumento(this.numResolucion);

            // Buscar el trámite con el número de resolución ingresado
            listTramBusqueda = this.getTramiteService().buscarTramitesPorNumeroDeResolucion(
                this.numResolucion);

            if (listTramBusqueda != null && !listTramBusqueda.isEmpty()) {
                // Si lo encuentra se deben realizar las validaciones sobre el
                // documento resolución.
                // - Se pueden asociar trámites que tengan una resolución,
                // exceptuando si la resolución se encuentra en proyección.

                this.listTramitesBuscadosPorCriterio = new ArrayList<Tramite>();
                for (Tramite tramIterator : listTramBusqueda) {

                    //felipe.cadena::05-01-2017::Validaciones delegacion
                    String codigoMun = tramIterator.getMunicipio().getCodigo();
                    if (!this.validarMunicipioDelegado(codigoMun,
                        ECodigoErrorVista.CONSULTA_DELEGACION_009,
                        ECodigoErrorVista.CONSULTA_DELEGACION_010)) {
                        this.addErrorCallback();
                        return;
                    }
                    if (tramIterator
                        .getResultadoDocumento()
                        .getEstado().equals(EDocumentoEstado.RESOLUCION_EN_PROYECCION
                            .getCodigo()) &&
                        this.solicitudSeleccionada.isRevocatoriaDirecta()) {
                        this.addMensajeWarn(
                            "El trámite encontrado para esta resolución corresponde a una resolución que se encuentra en proyección, por lo tanto debe esperar a que se notifique.");
                        this.addErrorCallback();
                    } else {
                        this.listTramitesBuscadosPorCriterio.add(tramIterator);
                    }
                }
            } else {
                this.addMensajeWarn(
                    "No se encontraron trámites para este número de resolución, por favor verifique la información.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        }

        this.numPredialBool = "numeroPredial".equals(this.criterio);
    }

    public void actualizarTablaTramitesViaGubernativa() {
        List<Tramite> listTramAux;
        int count;
        if (this.criterio != null &&
            "numeroPredial".equals(this.criterio.trim())) {
            if (this.resolucionViaGubernativa != null &&
                this.datosConsultaTramite.getNumeroPredial() != null) {
                listTramAux = this.getTramiteService()
                    .buscarTramitesConResolucionYNumeroPredialPredio(
                        this.resolucionViaGubernativa,
                        this.datosConsultaTramite.getNumeroPredial());

                this.numResolucion = this.resolucionViaGubernativa;
                if (listTramAux != null && !listTramAux.isEmpty()) {

                    this.tramitesGubernativaAll = new ArrayList<Tramite>();
                    for (Tramite tramTemp : listTramAux) {
                        if (tramTemp != null &&
                            !this.tramitesGubernativaAll
                                .contains(tramTemp)) {
                            this.tramitesGubernativaAll.add(tramTemp);
                        }
                    }
                }
            }
        } else if (this.criterio != null && this.criterio.equals("resolucion")) {
            if (this.solicitudSeleccionada.isRevocatoriaDirecta()) {
                if (this.selectedTramitesGubernativaBuscados == null ||
                    this.selectedTramitesGubernativaBuscados.length == 0) {
                    this.addMensajeError("Debe seleccionar al menos un trámite");
                    return;
                } else {
                    for (int i = 0; i < this.selectedTramitesGubernativaBuscados.length; i++) {
                        count = 0;
                        for (Tramite t : this.tramitesGubernativaAll) {
                            if (t.getId().equals(
                                this.selectedTramitesGubernativaBuscados[i]
                                    .getId())) {
                                count++;
                            }
                        }
                        if (count == 0) {
                            this.tramitesGubernativaAll
                                .add((Tramite) this.selectedTramitesGubernativaBuscados[i]);
                        }
                    }
                }
            } else if (this.solicitudSeleccionada.isViaGubernativa()) {
                if (this.selectedTramiteGubernativaBuscado == null) {
                    this.addMensajeError("Debe seleccionar un trámite");
                    return;
                } else {
                    // Empatar campos del trámite buscado con el trámite actual.
                    this.tramiteSeleccionado
                        .setResolucionContraProcede(this.numResolucion);
                    this.tramiteSeleccionado
                        .setNumeroRadicacionReferencia(this.selectedTramiteGubernativaBuscado
                            .getNumeroRadicacion());

                    this.tramitesGubernativaAll = new ArrayList<Tramite>();
                    this.tramitesGubernativaAll
                        .add((Tramite) this.selectedTramiteGubernativaBuscado);
                }
            }
        }
        // Se asocian los predios al trámite.
        asociarPrediosViaGubernativa();
        this.selectedTramitesGubernativaBuscados = null;
    }

    /**
     * Método que verifica si la resolución de un trámite está en firme o si está en proyeccción.
     */
    public void verificarResolucionDelTramite() {
        try {
            this.resolucionTramiteEnProyeccion = false;
            if (this.selectedTramiteGubernativaBuscado.getResultadoDocumento() != null) {
                Documento doc = this.getGeneralesService()
                    .buscarDocumentoPorId(
                        this.selectedTramiteGubernativaBuscado
                            .getResultadoDocumento().getId());

                if (doc != null && doc.getEstado() != null) {
                    if (doc.getEstado().equals(
                        EDocumentoEstado.RESOLUCION_EN_PROYECCION
                            .getCodigo())) {

                        List<Actividad> listaActividades = this
                            .getProcesosService()
                            .obtenerActividadesProceso(
                                this.selectedTramiteGubernativaBuscado
                                    .getProcesoInstanciaId());

                        if (listaActividades != null &&
                            listaActividades.size() == 1) {
                            this.actividadSeleccionada = listaActividades
                                .get(0);
                        }
                        this.resolucionTramiteEnProyeccion = true;
                    }
                }
            } else {
                this.resolucionTramiteEnProyeccion = false;
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            this.addMensajeError("Error al obtener el trámite.");
        }
    }

    // -----------------------------------------------------//
    /**
     * Método que reactiva un trámite que tiene su resolución en proyección.
     *
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia :: 31-10-2013 :: corrección en el flujo del método, estaba
     * colocando ProcesoInstanciaId en cadena vacía, para el tramiteBuscado
     */
    public void reactivarTramitePorViaGubernativa() {

        // Se verifica si el trámite seleccionado tiene una resolución
        // en firme o en proyección.
        verificarResolucionDelTramite();

        if (this.resolucionTramiteEnProyeccion) {
            List<Tramite> asociados = new ArrayList<Tramite>();
            if (this.selectedTramiteGubernativaBuscado != null) {
                asociados.add(this.selectedTramiteGubernativaBuscado);
            }
            this.tramiteSeleccionado.setTramites(asociados);
        }
    }

    public void retirarTramitesViaGubernativa() {

        if (this.tramiteDeleteSeleccionado != null) {
            this.tramitesGubernativaAll.remove(this.tramiteDeleteSeleccionado);
        }
        this.tramiteDeleteSeleccionado = null;
        asociarPrediosViaGubernativa();
    }

    public void asociarPrediosViaGubernativa() {
        if (this.tramitesGubernativaAll != null &&
            !this.tramitesGubernativaAll.isEmpty()) {
            // Se asocian los predios al tramite
            List<TramitePredioEnglobe> tramiteDetallePredioTemporal =
                new ArrayList<TramitePredioEnglobe>();
            List<Predio> prediosAux;
            for (Tramite t : this.tramitesGubernativaAll) {

                //solo para predios de quinta nuevo en asociar por via gubernativa
                if (t.isQuintaNuevo()) {
                    prediosAux = this.getTramiteService().buscarPrediosDeTramiteQuintaNuevo(t.
                        getId());
                } else {
                    prediosAux = this.getTramiteService().buscarPrediosDeTramite(t.getId());
                }

                if (prediosAux != null && !prediosAux.isEmpty()) {
                    for (Predio p : prediosAux) {
                        TramitePredioEnglobe tramiteDetallePredioTemp = new TramitePredioEnglobe();
                        tramiteDetallePredioTemp.setPredio(p);
                        tramiteDetallePredioTemp.setTramite(t);
                        tramiteDetallePredioTemp.setUsuarioLog(this.usuario
                            .getLogin());
                        tramiteDetallePredioTemporal
                            .add(tramiteDetallePredioTemp);
                    }
                }
            }
            this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
            this.prediosTramite.addAll(tramiteDetallePredioTemporal);
            if (this.prediosTramite != null && !this.prediosTramite.isEmpty()) {
                this.tramiteSeleccionado.setTramitePredioEnglobes(this.prediosTramite);
            }

            if (!this.prediosTramite.isEmpty()) {
                this.tramiteSeleccionado.setPredio(this.prediosTramite.get(0)
                    .getPredio());
            }
        }
    }
//-------------------------------------------------------------------------------------------------

    public void onSelectTramiteGubernativaRow(UnselectEvent ev) {
        LOGGER.debug("gestionarSolicitudesMB#onSelectTramiteGubernativaRow", ev);
    }
//-------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" en la página de selección de datos a rectificar
     */
    public void aceptarDatosARectificar() {

        TramiteRectificacion newTramiteRectificacion;
        List<TramiteRectificacion> tramiteRectificaciones;

        if (!this.selectedDatosRectifComplDataTable.isEmpty()) {
            tramiteRectificaciones = new ArrayList<TramiteRectificacion>();
            for (DatoRectificar dr : this.selectedDatosRectifComplDataTable) {
                newTramiteRectificacion = new TramiteRectificacion();
                newTramiteRectificacion.setDatoRectificar(dr);
                newTramiteRectificacion.setTramite(this.tramiteSeleccionado);
                newTramiteRectificacion.setFechaLog(new Date());
                newTramiteRectificacion.setUsuarioLog(this.usuario.getLogin());
                tramiteRectificaciones.add(newTramiteRectificacion);
            }

            this.tramiteSeleccionado
                .setTramiteRectificacions(tramiteRectificaciones);

        } else {
            this.addMensajeInfo(
                "Un trámite de rectificación debe tener datos a rectificar asociados");
        }

    }
//--------------------------------------------------------------------------------------------------

    public void eliminarSolicitanteDeLista() {

        int i;

        if (this.solicitanteTramite) {

            List<SolicitanteTramite> solicitanteTramitesTemp = new ArrayList<SolicitanteTramite>(
                this.tramiteSeleccionado.getSolicitanteTramites());

            for (i = 0; i < solicitanteTramitesTemp.size(); i++) {
                if (solicitanteTramitesTemp
                    .get(i)
                    .getSolicitante()
                    .getId()
                    .compareTo(
                        solicitanteTramiteSeleccionado.getSolicitante()
                            .getId()) == 0) {
                    this.tramiteSeleccionado.getSolicitanteTramites().remove(i);
                    break;
                }
            }
        } else {

            List<SolicitanteSolicitud> solicitanteSolicitudTemp =
                new ArrayList<SolicitanteSolicitud>(this.solicitudSeleccionada
                    .getSolicitanteSolicituds());

            for (i = 0; i < solicitanteSolicitudTemp.size(); i++) {
                if (solicitanteSolicitudTemp
                    .get(i)
                    .getSolicitante()
                    .getId() != null &&
                    solicitanteSolicitudTemp
                        .get(i)
                        .getSolicitante()
                        .getId()
                        .compareTo(
                            solicitanteSolicitudSeleccionado
                                .getSolicitante().getId()) == 0) {
                    this.solicitudSeleccionada.getSolicitanteSolicituds()
                        .remove(i);
                    break;
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified by leidy.gonzalez :: #13244:: 11/08/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    private void generarReporteConstanciaRadicacion() {

        FirmaUsuario firma;
        String documentoFirma;

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.CONSTANCIA_RADICACION;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_SOLICITUD",
            this.solicitudSeleccionada.getNumero());

        LOGGER.debug("Inicia ejecución servicio de reportes...");

        if (this.solicitudSeleccionada.getTramites() != null &&
            this.usuario != null &&
            this.usuario.getNombreCompleto() != null) {

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                this.usuario.getNombreCompleto());

            if (firma != null && this.usuario.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                parameters.put("FIRMA_USUARIO", documentoFirma);
            } else {
                this.addMensajeInfo("El tramite no tiene firma de usuario asociada.");
            }
        }

        this.reporteConstanciaRadicacion = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        if (this.reporteConstanciaRadicacion == null) {
            if (!this.solicitudSeleccionada.isSolicitudProductosCatastrales()) {
                this.addMensajeWarn(
                    "El reporte de constancia de radicación no se pudo generar en el momento, por favor intente más tarde.");
            } else {
                this.addMensajeError(Constantes.MENSAJE_ERROR_REPORTE);
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    @Implement
    public void doDatabaseStatesUpdate() {
        //Ninguna modificacion necesaria en BD
    }

    public void setupDescartarAviso() {
        this.avisoRechazo = new AvisoRegistroRechazo();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    /*
     * @modified leidy.gonzalez #10683 19-12-2014 :: cambio de caracteres especiales por espacion
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        this.banderaNuevoArchivo = true;
        this.rutaMostrar = eventoCarga.getFile().getFileName();

        try {
            String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

            this.archivoResultado = new File(directorioTemporal, this.rutaMostrar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer;
            buffer = new byte[(int) eventoCarga.getFile().getSize()];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.nombreTemporalArchivo = eventoCarga.getFile().getFileName();

            String mensaje = "El archivo seleccionado se precargó exitosamente al sistema.";
            this.addMensajeWarn(mensaje);

            // Se reinicia el valor de digitalizable y de aporta debido a que
            // estos tres campos son exluyentes entre si.
            if (documentoSeleccionado != null) {
                this.documentoSeleccionado.setAnalogo(false);
                this.documentoSeleccionado.setDigitalizable(false);
            }

        } catch (IOException e) {
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            LOGGER.debug(mensaje, e);
            this.addMensajeError(mensaje);
        }
    }

    /**
     * metodo validador de carga de archivos
     */
    public void archivoUploadAmpliarTiempos(FileUploadEvent eventoCarga) {
        this.ampliarTiempo = true;
        this.archivoUpload(eventoCarga);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prepara al solicitante de la solicitud para edición
     *
     * @author fabio.navarrete
     */
    public void setupSolicitanteSolicitudEdicion() {
        this.solicitanteSeleccionado
            .setRelacion(this.solicitanteSolicitudSeleccionado
                .getRelacion());
        this.solicitanteSeleccionado
            .setNotificacionEmail(this.solicitanteSolicitudSeleccionado
                .getNotificacionEmail());
        this.paisSolicitante = this.solicitanteSeleccionado.getDireccionPais();
        this.updateDepartamentosSolicitante();
        this.departamentoSolicitante = this.solicitanteSeleccionado
            .getDireccionDepartamento();
        this.updateMunicipiosSolicitante();
        this.municipioSolicitante = this.solicitanteSeleccionado
            .getDireccionMunicipio();

        this.solicitanteAEditar = (Solicitante) this.solicitanteSeleccionado.clone();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Prepara al solicitante del trámite para edición
     *
     * @author fabio.navarrete
     */
    public void setupSolicitanteTramiteEdicion() {
        this.solicitanteSeleccionado
            .setRelacion(this.solicitanteTramiteSeleccionado.getRelacion());
        this.solicitanteSeleccionado
            .setNotificacionEmail(this.solicitanteTramiteSeleccionado
                .getNotificacionEmail());
        this.paisSolicitante = this.solicitanteSeleccionado.getDireccionPais();
        this.updateDepartamentosSolicitante();
        this.departamentoSolicitante = this.solicitanteSeleccionado
            .getDireccionDepartamento();
        this.updateMunicipiosSolicitante();
        this.municipioSolicitante = this.solicitanteSeleccionado
            .getDireccionMunicipio();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Inicializa los documentos requeridos. Es la acción del ícono de edición del tab de
     * documentación requerida para el trámite.
     *
     * @author fabio.navarrete
     */
    /*
     * @documented pedro.garcia
     */
    /**
     * @modified by juanfelipe.garcia :: 27-12-2013 :: para tramites de vía gubernativa, se precarga
     * el departamento y el municipio
     */
    /*
     * @modified leidy.gonzalez #10683 19-12-2014 :: cambio de caracteres especiales por espacion
     */
    public void setupDocumentoRequeridoEdicion() {

        //javier.aponte:: 19/10/2015 :: refs#13433 Se inicia la variable de documento adicional en false
        this.banderaDocumentoAdicional = false;

        if (this.documentoSeleccionado != null &&
            this.documentoSeleccionado.getId() != null) {
            this.documentoReplica = (TramiteDocumentacion) this.documentoSeleccionado.clone();
            if (this.documentoReplica.getDocumentoSoporte() != null) {
                if (Constantes.CONSTANTE_CADENA_VACIA_DB.equals(this.documentoReplica.
                    getDocumentoSoporte().getArchivo())) {
                    this.rutaMostrar = Constantes.CONSTANTE_CADENA_VACIA_DB;
                } else {
                    String nombreArchivoSinCaracteresEspeciales = this.documentoReplica.
                        getDocumentoSoporte().getArchivo().replaceAll("[^a-zA-Z0-9./]", "_");
                    this.rutaMostrar = nombreArchivoSinCaracteresEspeciales;
                }
            } else {
                this.rutaMostrar = "";
            }
        }

        // Se reinicia el valor de digitalizable y de aporta debido a que estos tres campos son excluyentes entre si.
        if (this.documentoSeleccionado != null &&
            this.documentoSeleccionado.getDocumentoSoporte() != null &&
            this.documentoSeleccionado.getDocumentoSoporte().getArchivo() != null &&
            !this.documentoSeleccionado.getDocumentoSoporte().getArchivo().trim().isEmpty()) {
            this.rutaMostrar = this.documentoSeleccionado.getDocumentoSoporte().getArchivo();
            this.documentoSeleccionado.setDigitalizable(false);
            this.documentoSeleccionado.setAnalogo(false);
        }

        if (this.documentoSeleccionado.isDigitalizable()) {
            this.documentoSeleccionado.setAnalogo(false);
        } else if (this.documentoSeleccionado.isAnalogo()) {
            this.documentoSeleccionado.setDigitalizable(false);
        } else if (this.rutaMostrar != null &&
            this.rutaMostrar.trim().isEmpty()) {
            this.documentoSeleccionado.setCantidadFolios(0);
        }

        this.updateDepartamentosDocumento();
        if (this.departamentoTramite != null &&
            this.departamentoTramite.getCodigo() != null &&
            !this.departamentoTramite.getCodigo().isEmpty()) {
            this.departamentoDocumento = this.departamentoTramite;
        } else {
            this.departamentoDocumento = null;
        }

        this.updateMunicipiosDocumento();
        if (this.municipioTramite != null &&
            this.municipioTramite.getCodigo() != null &&
            !this.municipioTramite.getCodigo().isEmpty()) {
            this.municipioDocumento = this.municipioTramite;
        } else {
            this.municipioDocumento = null;
        }

        //En el campo Departamento y municipio dejar por defecto donde está ubicado 
        //el predio asociado al recurso de la vía gubernativa (CC-NP-CO-030)
        if (this.tramiteSeleccionado.isViaGubernativa()) {
            this.departamentoDocumento = this.tramiteSeleccionado.getPredio().getDepartamento();
            this.updateMunicipiosDocumento();
            this.municipioDocumento = this.tramiteSeleccionado.getPredio().getMunicipio();
        }

    }

    /**
     * Método encargado de asignar a la solicitud un estado para saber si está finalizada o no.
     */
    public void finalizarSolicitud() {
        this.solicitudSeleccionada
            .setEstado(ESolicitudEstado.TRAMITES_CONSOLIDADOS.getCodigo());
        this.guardarSolicitud();

        this.generarReporteConstanciaRadicacion();
        this.banderaMostrarBotonImprimirConstanciaRadicacion = true;
    }

    // -------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author juan.agudelo
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("gestionarSolicitudes");
        this.tareasPendientesMB.init();
        return "index";
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método para cerrar y salir del formulario
     */
    public String cerrarYSalir() {
        UtilidadesWeb.removerManagedBean("gestionarSolicitudes");
        return "index";
    }

    // -------------------------------------------------------------------------------------------------
    public void cleanRectificacionVariables() {
        this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        this.datosRectifComplSource = new ArrayList<DatoRectificar>();
        this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
        this.tipoDatoRectifComplSeleccionado = null;
    }

    public void deseleccionarTramites() {
        this.tramiteSeleccionado = new Tramite();
    }

    // --------------------------------------------------------------//
    // ------ Métodos usados en recibir documentos ------------------//
    // --------------------------------------------------------------//
    /**
     * Método inicial para asociar documentos.
     */
    public void recibirDocumentos() {
        this.limpiarFiltros();
        this.tramiteSeleccionadoBool = true;
    }

    /**
     * Método que inizializa los filtros de busqueda de trámites en la pantalla de recibir
     * documentos.
     */
    /*
     * @modified by juanfelipe.garcia :: 08-08-2013 :: adición de booleano para controlar el render
     * en pagina
     */
    public void limpiarFiltros() {
        this.tramitesConDocumentosFaltantes = new ArrayList<Tramite>();
        this.tramitesConDocumentosFaltantesCompleta = new ArrayList<Tramite>();
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.numSolicitudBusqueda = new String();
        this.numRadicacionBusqueda = new String();
        this.solicitanteBusqueda = new Solicitante();
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.banderaBuscarSolicitantes = false;
        this.mostrarPanelResultadoConsulta = false;
        this.deseleccionarTramites();
    }

    /**
     * Valida si el municipio dado pertenece a la delegada autenticada, o si es de una delegada y el
     * usuario autenticado no pertenece a una delegada.
     *
     * @author felipe.cadena
     * @return
     */
    private boolean validarMunicipioDelegado(String codigoMun, ECodigoErrorVista errorCodDlg,
        ECodigoErrorVista errorCodTerritorial) {

        boolean valido = true;

        if (this.usuarioDelegado) {
            if (codigoMun != null && !codigoMun.isEmpty()) {
                List<Municipio> municipiosList;

                boolean predioExiste = false;
                municipiosList = this.getGeneralesService().buscarMunicipioPorIdOficina(
                    this.usuario.getCodigoTerritorial());

                for (Municipio mun : municipiosList) {
                    if (mun.getCodigo().equals(codigoMun)) {
                        predioExiste = true;
                        break;
                    }
                }

                if (!predioExiste) {
                    this.addMensajeError(errorCodDlg.toString());
                    LOGGER.error(errorCodDlg.getMensajeTecnico());
                    return false;
                }
            }
        } else {
            for (Municipio mun : this.municipiosDelegados) {
                if (mun.getCodigo().equals(codigoMun)) {
                    this.addMensajeError(errorCodTerritorial.toString());
                    LOGGER.error(errorCodTerritorial.getMensajeTecnico());
                    return false;
                }
            }
        }

        return valido;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que realiza un filtro por diferentes criterios de los trámites con documentación
     * incompleta, que estén en el estado EN_ESPERA_DE_COMPLETAR_DOC
     */
    /*
     * @modified juanfelipe.garcia :: 17-09-2013 :: se quitan los tramites repetidos, resultados de
     * la consulta, cuando es un englobe
     */
    public void busquedaConFiltros() {
        this.deseleccionarTramites();
        this.mostrarPanelResultadoConsulta = true;
        // Busqueda con filtros para los trámites en el estado
        // EN_ESPERA_DE_COMPLETAR_DOC
        String estadoTramite = ETramiteEstado.EN_ESPERA_DE_COMPLETAR_DOC
            .getCodigo();

        //felipe.cadena::07-12-2016::Se validan las consultas para los usuarios delegados
        String codigoMun = (this.datosConsultaPredio.getNumeroPredialS1() + datosConsultaPredio.
            getNumeroPredialS2());
        if (!this.validarMunicipioDelegado(codigoMun,
            ECodigoErrorVista.CONSULTA_DELEGACION_001,
            ECodigoErrorVista.CONSULTA_DELEGACION_011)) {
            return;
        }
        if (this.usuarioDelegado) {
            //Se selecciona territorial o delegada por defecto
            this.datosConsultaPredio.setTerritorialId(this.usuario.getCodigoTerritorial());
        }

        List<Tramite> listaTramitesFiltradaPorBD;
        this.tramitesConDocumentosFaltantes = new ArrayList<Tramite>();
        boolean filtroBool = false;

        // Filtro por número de solicitud
        if (this.numSolicitudBusqueda != null &&
            !this.numSolicitudBusqueda.trim().isEmpty()) {
            filtroBool = true;
        } else {
            this.numSolicitudBusqueda = "";
        }

        // Filtro por número de radicación
        if (this.numRadicacionBusqueda != null &&
            !this.numRadicacionBusqueda.trim().isEmpty()) {
            filtroBool = true;
        } else {
            this.numRadicacionBusqueda = "";
        }

        // Filtro por solicitantes
        if (this.filtroDatosConsultaSolicitante != null) {
            filtroBool = true;
        } else {
            this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        }

        if (filtroBool) {
            listaTramitesFiltradaPorBD = this.getTramiteService()
                .buscarTramitesPorFiltros(estadoTramite,
                    this.numSolicitudBusqueda, numRadicacionBusqueda,
                    this.datosConsultaPredio, this.filtroDatosConsultaSolicitante);
            if (listaTramitesFiltradaPorBD == null ||
                listaTramitesFiltradaPorBD
                    .isEmpty()) {
                this.addMensajeInfo(
                    "La búsqueda no encontró ningún trámite, por favor intente nuevamente!");
                return;
            }
        } else {
            this.addMensajeInfo(
                "Debe haber al menos un filtro de busqueda, por favor intente nuevamente!");
            return;
        }
        //se quitan los tramites repetidos
        HashSet hs = new HashSet();
        hs.addAll(listaTramitesFiltradaPorBD);
        listaTramitesFiltradaPorBD.clear();
        listaTramitesFiltradaPorBD.addAll(hs);

        if (!this.usuarioDelegado) {
            List<Tramite> listaTramitesFiltradaPorDelegacion = new ArrayList<Tramite>();
            //se quitan los tramites de las delegadas
            boolean munDelegado;
            for (Tramite tramite : listaTramitesFiltradaPorBD) {
                munDelegado = false;
                for (Municipio mun : this.municipiosDelegados) {
                    if (mun.getCodigo().equals(tramite.getMunicipio().getCodigo())) {
                        munDelegado = true;
                        break;
                    }
                }
                if (!munDelegado) {
                    listaTramitesFiltradaPorDelegacion.add(tramite);
                }
            }
            this.tramitesConDocumentosFaltantes.addAll(listaTramitesFiltradaPorDelegacion);
        } else {
            this.tramitesConDocumentosFaltantes.addAll(listaTramitesFiltradaPorBD);
        }

    }

    // -----------------------------------------------//
    /**
     * Método que inicializa la documentación del trámite a asociar documentos y setea la solicitud
     * del mismo, y las fechas de entrega de documentos.
     */
    /*
     * @modified by juanfelipe.garcia :: 07-11-2013 :: adición de consulta de solicitud de
     * documentos
     */
    public void consultarTramite() {

        RequestContext context = RequestContext.getCurrentInstance();

        // Inicializar variables
        this.documentacionTramite = new ArrayList<TramiteDocumentacion>();
        this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        this.documentacionRequerida = new ArrayList<TramiteDocumentacion>();
        this.avanzarDeRecibirDocumentosBool = false;

        try {

            List<Actividad> listaActividades = this.getProcesosService()
                .getActividadesPorIdObjetoNegocio(
                    this.tramiteSeleccionado.getId());
            if (listaActividades != null && !listaActividades.isEmpty()) {
                this.actividadEncontradaBool = true;
                this.actividadSeleccionada = listaActividades.get(0);
            } else {
                this.addMensajeError(
                    "Error buscando el trámite seleccionado, el trámite presenta una inconsistencia en el sistema");
                this.addMensajeError(
                    "Por favor revise la información de busqueda e intente nuevamente!");
                this.actividadEncontradaBool = false;
                context.addCallbackParam("error", "error");
                return;
            }
        } catch (Exception e) {
            // NOTA: Este caso se dá cuando existe un
            // trámite en la BD que no tiene asociado ningún proceso. En teoría
            // ésto no debe pasar en producción, más sin embargo es necesaria
            // esta validación, pues en ambiente de pruebas genera errores.
            this.addMensajeError(
                "Error buscando el trámite seleccionado, el trámite presenta una inconsistencia en el sistema");
            this.
                addMensajeError("Por favor revise la información de busqueda e intente nuevamente!");
            this.actividadEncontradaBool = false;
            context.addCallbackParam("error", "error");
            LOGGER.debug(e.getMessage(), e);
            return;
        }

        this.preCargaDepartamentoMunicipio();

        // Documentación del trámite a asociar documentos
        this.documentacionTramite = this.getTramiteService()
            .obtenerTramiteDocumentacionPorIdTramite(
                this.tramiteSeleccionado.getId());

        // Documentación del trámite a asociar documentos
        List<TramiteDocumento> tds = this.getTramiteService().obtenerTramiteDocumentosPorTramiteId(
            this.tramiteSeleccionado.getId());
        this.tramiteSeleccionado.setTramiteDocumentos(tds);

        if (this.documentacionTramite != null &&
            !this.documentacionTramite.isEmpty()) {

            this.tramiteSeleccionado
                .setTramiteDocumentacions(this.documentacionTramite);
            if (this.tramiteSeleccionado.getDocumentacionRequerida() != null &&
                !this.tramiteSeleccionado.getDocumentacionRequerida()
                    .isEmpty()) {
                if (this.documentacionTramite != null &&
                    !this.documentacionTramite.isEmpty()) {
                    this.documentacionRequerida.addAll(this.tramiteSeleccionado
                        .getDocumentacionRequerida());
                }
            }

            for (TramiteDocumentacion td : this.documentacionRequerida) {
                if (td.getDocumentoSoporte() != null) {
                    if (td.getDocumentoSoporte().getArchivo() != null &&
                        !"".equals(td.getDocumentoSoporte().getArchivo()) &&
                        !" ".equals(td.getDocumentoSoporte().getArchivo())) {
                        td.setDigital(ESiNo.SI.getCodigo());
                    }
                }
            }

            // Solicitud del Trámite a asociar documentos
            Solicitud solicitud = this.getTramiteService().findSolicitudPorId(
                this.tramiteSeleccionado.getSolicitud().getId());
            if (solicitud != null) {
                this.solicitudSeleccionada = solicitud;
            }

            // CONSULTA DE LA SOLICITUD DE DOCUMENTOS
            // this.solicitudDocumentosBool =
            this.solicitudDocumentosBool = false;
            this.solicitudDeDocumentos = this.getTramiteService()
                .buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(
                    this.tramiteSeleccionado.getId());
            // CALCULO DE TIEMPOS
            if (this.solicitudDeDocumentos != null &&
                this.solicitudDeDocumentos.getFechaNotificacion() != null) {
                this.fechaComunicacionSolicitud = this.solicitudDeDocumentos
                    .getFechaNotificacion();
            }

            if (this.actividadSeleccionada != null &&
                this.actividadSeleccionada.getFechaExpiracion() != null) {
                this.fechaFinalizacionDeEspera = this.actividadSeleccionada
                    .getFechaExpiracion().getTime();
            } else {
                this.fechaFinalizacionDeEspera = new Date(
                    System.currentTimeMillis());
            }

            this.tiempoAmpliacion = 0;
            this.actualizarFechaDeFinalizacion();

            int contador = 0;
            for (TramiteDocumentacion td : this.documentacionRequerida) {
                if (td.getRequerido().equals(ESiNo.SI.getCodigo()) &&
                    td.getAportado().equals(ESiNo.NO.getCodigo())) {
                    contador++;
                }
            }
            this.avanzarDeRecibirDocumentosBool = (contador == 0) ? true :
                false;
        } else {
            // D: Este caso se dá cuando existe un trámite en la BD que no tiene
            // asociado ningún proceso.
            // En teoría ésto no debe pasar en producción, más sin embargo es
            // necesaria esta validación,pues en ambiente de pruebas genera
            // errores.
            this.addMensajeError(
                "Error buscando el trámite seleccionado, el trámite presenta una inconsistencia en el sistema");
            this.
                addMensajeError("Por favor revise la información de busqueda e intente nuevamente!");
            this.actividadEncontradaBool = false;
            context.addCallbackParam("error", "error");
        }
    }

    /**
     * Carga el departamento y municipio asociados al tramite para que aparezcan seleccionados en
     * lso combos
     *
     * @author christian.rodriguez
     */
    private void preCargaDepartamentoMunicipio() {

        if (this.tramiteSeleccionado != null) {
            if (this.tramiteSeleccionado.getDepartamento() != null) {
                this.departamentoTramite = this.tramiteSeleccionado.getDepartamento();
            }
            if (this.tramiteSeleccionado.getMunicipio() != null) {
                this.municipioTramite = this.tramiteSeleccionado.getMunicipio();
            }
        }

    }

    /**
     * Método que le suma días a una fecha determinada
     *
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia :: 17-10-2013 :: Cuando se ingrese Tiempo de ampliación de
     * espera de documentación, No debe habilitar el botón Avanzar proceso (5576)
     */
    public void actualizarFechaDeFinalizacion() {

        Date fecha;

        if (this.fechaFinalizacionDeEspera != null) {
            fecha = this.fechaFinalizacionDeEspera;
        } else {
            fecha = new Date();
        }

        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fecha.getTime());
        cal.add(Calendar.DATE, this.tiempoAmpliacion);
        this.nuevaFechaFinalizacion = new java.sql.Date(cal.getTimeInMillis());
        if (this.tiempoAmpliacion < 0 || this.tiempoAmpliacion > 30) {
            this.addMensajeError(
                "El tiempo de ampliación debe ser un número válido menor a 30 días hábiles.");
            return;
        }
        if (!this.tiempoAmpliacion.equals(0)) {
            this.ampliarTiempo = true;
        }
    }

    /**
     * Método que actualiza y valida los tramiteDocumentacions del trámite.
     *
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia :: 22-08-2013 :: se ajustó la lógica para actualizar los
     * documentos (#5249)
     */
 /*
     * @modified leidy.gonzalez :: 14-12-2015 :: #15513 :: se valida que se haya guardado documento
     * en el servidor de la documental, si no es asi se envia mensaje al usuario.
     */
    public void guardarTramiteConRecepcionDeDocumentos() {

        RequestContext context = RequestContext.getCurrentInstance();

        //david.murcia
        //Debdo a que la documentación cambia y se almacena antes de guardar en el ftp, se hace respaldo para comparar en caso de posibles fallos.
        this.documentacionTramiteTmp = this.getTramiteService()
            .obtenerTramiteDocumentacionPorIdTramite(
                this.tramiteSeleccionado.getId());

        if (this.documentacionTramite != null &&
            !this.documentacionTramite.isEmpty()) {

            //validacion adicional #5824, Al validar y guardar debe obligar a cargar el documento soporte de ampliación de tiempos
            if (this.ampliarTiempo && (this.rutaMostrar == null || this.rutaMostrar.isEmpty())) {
                this.addMensajeError("Selecciono ampliar tiempos, debe cargar un documento soporte");
                return;
            }

            if (this.documentacionRequerida != null) {

                int contadorTramitesFaltantes = 0;
                for (TramiteDocumentacion td : this.documentacionRequerida) {
                    if (td.getRequerido().equals(ESiNo.SI.getCodigo()) &&
                        td.getAportado().equals(ESiNo.NO.getCodigo())) {
                        contadorTramitesFaltantes++;
                    }
                }

                if (contadorTramitesFaltantes == 0) {
                    this.tramiteSeleccionado.setDocumentacionCompleta(ESiNo.SI
                        .getCodigo());
                } else {
                    this.tramiteSeleccionado.setDocumentacionCompleta(ESiNo.NO
                        .getCodigo());
                }
            }
            this.documentacionTramite = new ArrayList<TramiteDocumentacion>();
            this.documentacionTramite.addAll(this.tramiteSeleccionado.getTramiteDocumentacions());
            this.documentacionTramite.addAll(this.documentacionAdicional);

            this.getTramiteService().actualizarTramiteDocumentacion(this.documentacionTramite);

            this.documentacionTramite = this.getTramiteService()
                .obtenerTramiteDocumentacionPorIdTramite(this.tramiteSeleccionado.getId());

            this.tramiteSeleccionado.setTramiteDocumentacions(this.documentacionTramite);

            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }
        //Dado el caso que un archivo haya sido guardado en la pestaña de ampliar tiempo
        if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty() && this.ampliarTiempo) {

            Documento docAmpl = new Documento();
            docAmpl.setArchivo(this.rutaMostrar);
            TipoDocumento tipoDoc = this.getGeneralesService().buscarTipoDocumentoPorId(
                ETipoDocumento.DOCUMENTO_SOPORTE_DE_AMPLIACION_TIEMPOS.getId());
            docAmpl.setTipoDocumento(tipoDoc);
            docAmpl.setFechaLog(new Date());
            docAmpl.setUsuarioLog(this.usuario.getLogin());
            docAmpl.setUsuarioCreador(this.usuario.getLogin());
            docAmpl.setBloqueado(ESiNo.NO.getCodigo());
            docAmpl.setEstado(EDocumentoEstado.CARGADO.getCodigo());

            //DocumentoTramiteDTO para la actualizacion en alfresco
            DocumentoTramiteDTO dtoTram = new DocumentoTramiteDTO(
                this.rutaMostrar,
                ETipoDocumento.DOCUMENTO_SOPORTE_DE_AMPLIACION_TIEMPOS.getNombre(),
                new Date(),
                this.tramiteSeleccionado.getTipoTramite(),
                this.tramiteSeleccionado.getId());

            docAmpl = this.getGeneralesService().
                actualizarDocumentoYSubirArchivoAlfresco(this.usuario, dtoTram, docAmpl);

            TramiteDocumentacion tDocAmpl = new TramiteDocumentacion();
            tDocAmpl.setDocumentoSoporte(docAmpl);
            tDocAmpl.setFechaLog(new Date());
            tDocAmpl.setUsuarioLog(this.usuario.getLogin());
            tDocAmpl.setTipoDocumento(tipoDoc);
            tDocAmpl.setAportado(ESiNo.SI.getCodigo());
            tDocAmpl.setTramite(this.tramiteSeleccionado);
            tDocAmpl.setCantidadFolios(0);
            tDocAmpl.setAdicional(ESiNo.SI.getCodigo());
            tDocAmpl.setIdRepositorioDocumentos(docAmpl.getIdRepositorioDocumentos());
            tDocAmpl.setMunicipio(this.tramiteSeleccionado.getMunicipio());
            tDocAmpl.setDepartamento(this.tramiteSeleccionado.getDepartamento());

            this.documentoAmpliacionTemp = this.getTramiteService().guardarTramiteDocumentacion(
                tDocAmpl);
            this.documentacionTramite.add(documentoAmpliacionTemp);
            this.docAmpliarTiempoRecibido = true;
            this.ampliarTiempo = false;
        }

        this.tramiteSeleccionado.setUsuarioLog(this.usuario.getLogin());
        this.tramiteSeleccionado.setFechaLog(new Date(System
            .currentTimeMillis()));
        revisarDocumentosDigitalizados();

        try {
            // Actualizar los TramiteDocumentación del trámite modificados al
            // momento de completar la documentación del mismo.
            guardarTramiteDocumentacionAportada();
            this.tramiteSeleccionado.setTramiteDocumentacions(this.documentacionTramite);
            this.getTramiteService().actualizarTramiteDocumentos(this.tramiteSeleccionado,
                this.usuario);

            // Verificar estado de la documentación.
            if (this.tramiteSeleccionado.getDocumentacionCompleta().equals(ESiNo.NO.getCodigo())) {
                this.addMensajeWarn(
                    "Se guardó la documentación aportada, pero recuerde que aún no se ha aportado la totalidad de la documentación requerida.");
            } else if (this.documentacionTramite.get(0).getIdRepositorioDocumentos() != null) {
                this.addMensajeInfo("Se completó satisfactoriamente la documentación del trámite.");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            context.addCallbackParam("error", "error");
            limpiarFiltros();
            this.addMensajeError(
                "Ocurrió un error al actualizar los datos de la documentación y el trámite, por favor intente nuevamente");
        }
    }

    // ---------------------------------- //
    /**
     * Método que guarda los trámite documentación aportados, al momento de completar la
     * documentación solicitada.
     *
     * @author david.cifuentes
     * @throws Exception
     */
    /*
     * @modified leidy.gonzalez :: 20-01-2015 :: #7485 :: se iguala documentacionTramite despues de
     * guardadando el documento en Alfresco
     */
 /*
     * @modified leidy.gonzalez :: 14-12-2015 :: #15513 :: se modifica retorno de metodo y se
     * verifica que se haya almacenado el documento en el repositorio.
     */
    public void guardarTramiteDocumentacionAportada() throws Exception {
        try {
            ArrayList<TramiteDocumentacion> tramDocsActualizar =
                new ArrayList<TramiteDocumentacion>();
            List<TramiteDocumentacion> answerTramDocumentacion;
            if (this.documentacionTramite != null && !this.documentacionTramite.isEmpty()) {

                for (TramiteDocumentacion td : this.documentacionTramite) {
                    // Revisar si ya se guardó en alfresco.
                    if (td.getAportado().equals(ESiNo.SI.getCodigo()) && td.getDocumentoSoporte() !=
                        null &&
                        td.getDocumentoSoporte().getArchivo() != null &&
                        !td.getDocumentoSoporte().getArchivo().trim().isEmpty() &&
                        td.getIdRepositorioDocumentos() == null) {
                        tramDocsActualizar.add(td);
                    }
                }
            }

            // Actualizar la lista de tramitesDocumentacion guardadando el
            // documento en Alfresco
            if (!tramDocsActualizar.isEmpty()) {

                try {
                    answerTramDocumentacion = this.getTramiteService().
                        actualizarTramiteDocumentacionConAlfresco(
                            tramDocsActualizar, this.solicitudSeleccionada, this.usuario);

                    this.documentacionTramite = answerTramDocumentacion;

                    if ((this.documentacionTramite == null && this.documentacionTramite.isEmpty()) ||
                        this.documentacionTramite.get(0).getIdRepositorioDocumentos() == null) {

                        this.addMensajeWarn(ECodigoErrorVista.CARGA_DOCUMENTO_FALTANTE_001.
                            getMensajeUsuario());
                        LOGGER.warn(ECodigoErrorVista.CARGA_DOCUMENTO_FALTANTE_001.
                            getMensajeTecnico());
                        this.avanzarDeRecibirDocumentosBool = false;
                        return;
                    }
                } catch (ExcepcionSNC e) {
                    this.addMensajeWarn(ECodigoErrorVista.CARGA_DOCUMENTO_FALTANTE_001.
                        getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.CARGA_DOCUMENTO_FALTANTE_001.getMensajeTecnico());
                    // david.murcia
                    // Si está cargada se compara, de lo contrario se mantiene
                    // la documentación como se encuentra.
                    if (this.documentacionTramiteTmp != null && !this.documentacionTramiteTmp.
                        isEmpty()) {
                        List<TramiteDocumentacion> tAct = new ArrayList<TramiteDocumentacion>();
                        for (TramiteDocumentacion td : this.documentacionTramite) {
                            for (TramiteDocumentacion tdt : this.documentacionTramiteTmp) {
                                if (td.getId().equals(tdt.getId()) && td.
                                    getIdRepositorioDocumentos() == null) {
                                    // Si está vacío es porque no se actualizó
                                    // en el repositorio.
                                    tAct.add(tdt);
                                }
                            }
                        }
                        if (tAct != null && !tAct.isEmpty()) {
                            this.getTramiteService().actualizarTramiteDocumentacion(tAct);
//							this.documentacionTramite = this.getTramiteService()
//									.obtenerTramiteDocumentacionPorIdTramite(this.tramiteSeleccionado.getId());
                            this.consultarTramite();
                            this.documentacionTramiteTmp = null;
                        }
                        this.avanzarDeRecibirDocumentosBool = false;
                        return;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    // ---------------------------------- //
    /*
     * @modified by juanfelipe.garcia :: 21-10-2013 :: Ajuste en el método para que habilite
     * correctamente el botón avanzar proceso
     */
    public void revisarDocumentosDigitalizados() {

        if (this.tramiteSeleccionado.getDocumentacionCompleta().equals(
            ESiNo.SI.getCodigo())) {
            this.avanzarDeRecibirDocumentosBool = true;
            for (TramiteDocumentacion td : this.tramiteSeleccionado
                .getTramiteDocumentacions()) {
                if (td.getDigital() != null &&
                    td.getDigital().equals(ESiNo.NO.getCodigo())) {

                    if (td.getDocumentoSoporte() == null ||
                        td.getDocumentoSoporte().getArchivo() == null ||
                        td.getDocumentoSoporte().getArchivo().length() < 1) {
                        this.avanzarDeRecibirDocumentosBool = false;
                        break;
                    }
                }
            }

            if (!this.avanzarDeRecibirDocumentosBool) {
                this.addMensajeWarn(
                    "La documentación ha sido completada, pero los documentos marcados como digitales no han sido cargados. Por favor cárguelos e intente de nuevo.");
            }
        } else {
            this.actualizarFechaDeFinalizacion();
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que mueve el trámite en el process server.
     *
     * @author david.cifuentes
     */
    public void moverProcesoEnRecibirDocumentos() {

        try {
            int contador = 0;
            String docCompleta;
            if (this.tramiteSeleccionado.getDocumentacionCompleta() == null) {
                for (TramiteDocumentacion td : this.documentacionRequerida) {
                    if (td.getRequerido().equals(ESiNo.SI.getCodigo()) &&
                        td.getAportado().equals(ESiNo.NO.getCodigo())) {
                        contador++;
                    }
                }
                docCompleta = (contador == 0) ? ESiNo.SI.getCodigo() : ESiNo.NO
                    .getCodigo();
                this.tramiteSeleccionado.setDocumentacionCompleta(docCompleta);
            }

            if (this.tramiteSeleccionado.getDocumentacionCompleta().equals(
                ESiNo.NO.getCodigo())) {
                this.addMensajeInfo("La documentación del trámite no esta completa.");
                return;
            }

            if (this.tramiteSeleccionado.getDocumentacionCompleta().equals(
                ESiNo.SI.getCodigo())) {

                //javier.aponte :: refs #11140 :: Se modifica el estado del trámite al avanzar el proceso, queda como 'RECIBIDO', 
                //debido a que el estado 'EN PROCESO' aparentemente no se usaba :: 26/01/2016
                this.tramiteSeleccionado.setEstado(ETramiteEstado.RECIBIDO.getCodigo());

                this.tramiteSeleccionado.setTramiteDocumentacions(this.documentacionTramite);

                this.getTramiteService().actualizarTramite(
                    this.tramiteSeleccionado, this.usuario);

                this.addMensajeInfo(
                    "Se actualizó la documentación del trámite exitosamente. Este trámite ahora está en proceso.");
                this.tramitesConDocumentosFaltantes = new ArrayList<Tramite>();
            } else if (this.tiempoAmpliacion <= 0 || this.tiempoAmpliacion > 30) {
                this.addMensajeError(
                    "El tiempo de ampliación debe ser un número válido menor a 30 días hábiles.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

            if (validarMoverProceso()) {

                SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                UsuarioDTO usuarioEjecutor;

                // Verificar si el trámite tiene o no ejecutor asignado
                if (this.tramiteSeleccionado.getFuncionarioEjecutor() != null &&
                    !this.tramiteSeleccionado.getFuncionarioEjecutor()
                        .trim().isEmpty() &&
                    !this.siguienteActividad
                        .equals(ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR)) {

                    usuarioEjecutor = this.getGeneralesService()
                        .getCacheUsuario(
                            this.tramiteSeleccionado
                                .getFuncionarioEjecutor());
                    List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
                    usuarios.add(usuarioEjecutor);
                    solicitudCatastral.setUsuarios(usuarios);
                } else {

                    String estructuraOrg;

                    if (this.tramiteSeleccionado.getSolicitud().isViaGubernativa()) {

                        //OJO: algunas veces el municipio está en el predio del trámite y otras en el trámite
                        estructuraOrg = this.getGeneralesService().
                            getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                this.tramiteSeleccionado.getPredio().getMunicipio().getCodigo());

                    } else {
                        if (!this.tramiteSeleccionado.isQuinta()) {
                            estructuraOrg = this.getGeneralesService().
                                getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                    this.tramiteSeleccionado.getPredio().getMunicipio().getCodigo());
                        } else {
                            estructuraOrg = this.getGeneralesService()
                                .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                    this.tramiteSeleccionado.getMunicipio().getCodigo());
                        }
                    }

                    if (estructuraOrg == null) {
                        this.addMensajeError(
                            "No fué posible determinar la estructura organizacional destino para el municipio : ");
                        return;
                    } else {
                        LOGGER.debug("Estructura Organizacional: " + estructuraOrg);
                    }

                    solicitudCatastral
                        .setUsuarios(this
                            .getTramiteService()
                            .buscarFuncionariosPorRolYTerritorial(
                                estructuraOrg,
                                ERol.RESPONSABLE_CONSERVACION));
                }

                solicitudCatastral.setTransicion(this.siguienteActividad);

                if (this.tiempoAmpliacion > 0 && this.tiempoAmpliacion <= 30) {
                    solicitudCatastral.setDuracion(this.duracion);
                    this.getProcesosService().avanzarActividad(this.usuario,
                        this.actividadSeleccionada.getId(),
                        solicitudCatastral);
                } else {
                    solicitudCatastral.setDuracion(null);
                    this.getProcesosService().avanzarActividad(this.usuario,
                        this.actividadSeleccionada.getId(),
                        solicitudCatastral);
                }
                this.addMensajeInfo("Se avanzó el proceso correctamente");
                LOGGER.debug("++++ Se avanzó el proceso a la actividad: " + this.siguienteActividad);
                // Reiniciar las variables usadas para ampliar tiempos
                this.limpiarFiltros();
                this.tramiteSeleccionadoBool = true;
                tramitesConDocumentosFaltantes = new ArrayList<Tramite>();
                this.mostrarPanelResultadoConsulta = false;

                return;

            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            this.addMensajeError("Error al poner en proceso el trámite.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

    }

    /**
     * Método que valida la transición a la que se debe mover el trámite.
     *
     * @return
     */
    public boolean validarMoverProceso() {

        if (this.tramiteSeleccionado.getDocumentacionCompleta().equals(
            ESiNo.SI.getCodigo())) {

            // No hay ampliación de tiempos
            this.duracion = new Duration(0, 0, 0, 0);

            // Verificar si el trámite tiene o no ejecutor asignado
            if (this.tramiteSeleccionado.getFuncionarioEjecutor() == null ||
                this.tramiteSeleccionado.getFuncionarioEjecutor().trim().isEmpty()) {

                // NO tiene ejecutor asignado
                // El trámite debe ser reasignado si ya se determinó su
                // procedencia.
                if (revisarReasignacion()) {
                    // YA se determino la procedencia del trámite, por lo tanto
                    // se procede a reasignar un ejecutor.
                    this.siguienteActividad = ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES;
                    return true;

                } else {
                    // NO se ha determinado la procedencia del trámite la
                    // siguiente actividad es
                    // determinar procedencia del trámite.
                    this.siguienteActividad =
                        ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE;
                    return true;
                }
            } else {
                // SÍ tiene ejecutor asignado, verificar si es de TERRENO U
                // OFICINA
                if (this.tramiteSeleccionado.isTramiteTerreno()) {
                    // El trámite es de TERRENO, verificar si está comisionado
                    if (this.tramiteSeleccionado.isTramiteComisionado()) {
                        // El trámite está COMISIONADO, la siguiente actividad
                        // es revisar trámite
                        this.siguienteActividad =
                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                        return true;
                    } else {
                        // El trámite NO ESTÁ COMISIONADO, la siguiente
                        // actividad es comisionar
                        this.siguienteActividad = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                        return true;
                    }
                } else {
                    // El trámite es de OFICINA, la siguiente actividad es
                    // revisar trámite
                    this.siguienteActividad =
                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                    return true;
                }
            }
        } else if (this.tiempoAmpliacion > 0 && this.tiempoAmpliacion <= 30) {
            // El trámite se mantiene en la misma actividad pero se hace una
            // ampliacion de tiempos
            this.siguienteActividad = ProcesoDeConservacion.ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA;
            this.duracion = new Duration(this.tiempoAmpliacion, 0, 0, 0);
            this.addMensajeInfo("Se amplió el tiempo de espera de documentos satisfactoriamente");
            return true;
        }
        return false;
    }

    /**
     * Método que revisa si se debe reasignar un trámite a un nuevo ejecutor si ya se determinó la
     * procedencia del trámite. Retorna 'true' si el trámite se debe reasignar y ya se determino su
     * procedencia. Retorna 'false' si se debe asignar y no se ha determinado la procedencia del
     * tramite.
     *
     * @author david.cifuentes
     */
    public boolean revisarReasignacion() {
        // Sabemos que ya se determinó la procedencia de un trámite si en el
        // campo resultado del objeto de negocio se tiene la palabra REASIGNAR,
        // de lo contrario se establece que no se ha determinado su procedencia
        // y se debe proceder a reasignar ejecutor.

        SolicitudCatastral sc = this.getProcesosService()
            .obtenerObjetoNegocioConservacion(
                this.tramiteSeleccionado.getProcesoInstanciaId(),
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
        return sc != null;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que consulta la solicitud de Documentos guardada en Alfresco
     */
    public boolean consultarEnAlfrescoDocumentoRadicado() {

        String urlRutaArchivoTemporal;
        this.solicitudDeDocumentos = new TramiteDocumento();
        this.solicitudDeDocumentos = this.getTramiteService()
            .buscarTramiteDocumentoConSolicitudDeDocumentosEnAlfresco(
                this.tramiteSeleccionado.getId());

        if (this.solicitudDeDocumentos != null &&
            this.solicitudDeDocumentos.getDocumento() != null &&
            this.solicitudDeDocumentos.getIdRepositorioDocumentos() != null) {

            urlRutaArchivoTemporal = this.getGeneralesService()
                .descargarArchivoDeAlfrescoATemp(
                    this.solicitudDeDocumentos
                        .getIdRepositorioDocumentos());

            this.reporteSolicitudDocumentos = this.reportsService.
                consultarReporteDeGestorDocumental(urlRutaArchivoTemporal);

            return true;
        }
        return false;
    }

    // ---------------------------------------------//
    // Métodos que controlan eventos en la interfaz //
    // para hacer updates en algunos elementos al 	//
    // momento de recibir documentos. 				//
    // ---------------------------------------------//
    /**
     * @deprecated solo se usaba en un componente como listener, pero ese componente no admite
     * listeners. quitar el deprecated si se vuelve a usar.
     * @param event
     */
    @Deprecated
    public void reiniciarVariablesDeAsociarDocumentos(CloseEvent event) {
        // Reiniciar las variables usadas para ampliar tiempos
        this.limpiarFiltros();
        this.tramiteSeleccionadoBool = true;
    }

    public void onSelectTramiteRow(SelectEvent ev) {
        LOGGER.debug("AsociarDocumentosMB#onSelectDecretoRow");
        this.tramiteSeleccionadoBool = false;
        this.consultarTramite();
    }

    public void onUnselectTramiteRow(UnselectEvent ev) {
        LOGGER.debug("AsociarDocumentosMB#onUnSelectDecretoRow");
        this.tramiteSeleccionadoBool = true;
        this.tramiteSeleccionado = null;
    }

    // ------------------------------------------------ //
    /**
     * Método encargado de terminar
     */
    public String terminar() {
        try {
            UtilidadesWeb.removerManagedBean("gestionarSolicitudes");
            return "index";
        } catch (Exception e) {
            this.addMensajeError("Error al cerrar la sesión");
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización
     */
    /*
     * @modified leidy.gonzalez #10683 19-12-2014 :: cambio de caracteres especiales por espacion
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoSeleccionado.getDocumentoSoporte().getArchivo());

        return this.tipoMimeDocTemporal;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de finalizar los datos de documento temporal al cerrar la previsualización
     */
    public void cerrarDocumentoTemporalPrev() {
        // Si se queire hacer algo aca debe hacerse
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de la descarga de un archivo temporal de office
     */
    private void controladorDescargaArchivos() {

        if (this.documentoSeleccionado != null &&
            this.documentoSeleccionado.getDocumentoSoporte() != null &&
            this.documentoSeleccionado.getDocumentoSoporte()
                .getArchivo() != null &&
            !this.documentoSeleccionado.getDocumentoSoporte()
                .getArchivo().isEmpty()) {

            File fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.documentoSeleccionado.getDocumentoSoporte().getArchivo());
            InputStream stream;

            try {

                stream = new FileInputStream(fileTemp);
                cargarDocumentoTemporalPrev();
                this.fileTempDownload = new DefaultStreamedContent(stream,
                    this.tipoMimeDocTemporal, this.documentoSeleccionado.getDocumentoSoporte().
                        getArchivo());

            } catch (FileNotFoundException e) {
                String mensaje = "Ocurrió un error al cargar al archivo." +
                    " Por favor intente nuevamente.";
                this.addMensajeError(mensaje);
                LOGGER.debug(mensaje, e);
            }

        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la selección de predios en mutaciones de segunda englobe para buscar
     * la lista de predios colindantes
     */
    public void buscarPrediosColindantes() {

        this.banderaNoGeograficoEnglobe = false;
        this.listaPrediosColindantes = this.getConservacionService().
            buscarPrediosColindantesPorNumeroPredial(
                this.selectedPredioTramite.getPredio().getNumeroPredial());

        if (this.listaPrediosColindantes != null && !this.listaPrediosColindantes.isEmpty()) {
            for (Predio pTmp : this.listaPrediosColindantes) {
                pTmp.setNumeroPredialAsociadoPadre(this.selectedPredioTramite
                    .getPredio().getNumeroPredial());
            }

            if (this.listaGlobalPrediosColindantes == null) {
                this.listaGlobalPrediosColindantes = new ArrayList<Predio>();
            }

            if (!this.listaGlobalPrediosColindantes
                .contains(this.selectedPredioTramite.getPredio())) {
                this.listaGlobalPrediosColindantes
                    .add(this.selectedPredioTramite.getPredio());
            }

            for (Predio pTemp : this.listaPrediosColindantes) {
                if (!this.listaGlobalPrediosColindantes.contains(pTemp)) {
                    this.listaGlobalPrediosColindantes.add(pTemp);
                }
            }
            this.listaGlobalPrediosColindantes
                .addAll(this.listaPrediosColindantes);
        } else {
            this.listaPrediosColindantes = this.getConservacionService().
                buscarPrediosColindantesPorServicioWeb(
                    usuario, this.selectedPredioTramite.getPredio().getNumeroPredial());

            if (this.listaPrediosColindantes == null) {
                this.banderaNoGeograficoEnglobe = true;
                return;
            }

            if (this.listaPrediosColindantes != null && !this.listaPrediosColindantes.isEmpty()) {
                for (Predio pTmp : this.listaPrediosColindantes) {
                    pTmp.setNumeroPredialAsociadoPadre(this.selectedPredioTramite
                        .getPredio().getNumeroPredial());
                }

                if (this.listaGlobalPrediosColindantes == null) {
                    this.listaGlobalPrediosColindantes = new ArrayList<Predio>();
                }

                if (!this.listaGlobalPrediosColindantes
                    .contains(this.selectedPredioTramite.getPredio())) {
                    this.listaGlobalPrediosColindantes
                        .add(this.selectedPredioTramite.getPredio());
                }

                for (Predio pTemp : this.listaPrediosColindantes) {
                    if (!this.listaGlobalPrediosColindantes.contains(pTemp)) {
                        this.listaGlobalPrediosColindantes.add(pTemp);
                    }
                }
                this.listaGlobalPrediosColindantes.addAll(this.listaPrediosColindantes);
            } else {
                this.addMensajeWarn(
                    "No se encontraron predios colindantes para el predio seleccionado.");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la deselección de predios en mutaciones de segunda englobe para
     * inicializar la lista de predios colindantes y selección
     */
    public void reiniciarPrediosColindantes() {
        this.listaPrediosColindantes = null;
    }

    public List<DatoRectificar> getSelectedDatosRectifComplDataTable() {
        return selectedDatosRectifComplDataTable;
    }

    public void setSelectedDatosRectifComplDataTable(
        List<DatoRectificar> selectedDatosRectifComplDataTable) {
        this.selectedDatosRectifComplDataTable = selectedDatosRectifComplDataTable;
    }

    /**
     * Método invocado para buscar una solicitud pendiente en Correspondencia, trae datos y los
     * setea en la solicitud seleccionada
     */
    /*
     * @modified juanfelipe.garcia :: 15-08-2013 :: adición de booleano para validar el radicado
     * @modified felipe.cadena -- se agrega el parametro usuario a la consulta de radicacion
     */
 /*
     * @modified leidy.gonzalez :: 13-01-2015 :: #11067 :: se asigna el id del tramite del
     * TramiteDocumentacion
     */
    public void buscarRadicadoEnCorrespondecia() {

        Documento documento;

        if (this.usuarioDelegado) {
            if (this.documentoSeleccionado.getDocumentoSoporte().getNumeroRadicacion() == null ||
                this.documentoSeleccionado.getDocumentoSoporte().getNumeroRadicacion().isEmpty()) {
                this.addMensajeError(ECodigoErrorVista.CONSULTA_DELEGACION_002.toString());
                LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_002.getMensajeTecnico());
                return;
            } else {
                this.radicadoValidado = true;
                return;
            }
        }

        documento = this.getTramiteService().obtenerDocumentoPruebaCorrespondencia(
            this.documentoSeleccionado.getDocumentoSoporte().getNumeroRadicacion(), this.usuario);
        if (documento == null && !this.usuarioDelegado) {
            this.radicadoValidado = false;
            String mensaje = "El documento no se encontro en correspondencia," +
                " por favor corrija los datos e intente nuevamente";
            this.addMensajeError(mensaje);

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        } else {
            if (documento != null) {
                this.documentoSeleccionado.getDocumentoSoporte().setFechaRadicacion(documento.
                    getFechaRadicacion());
            }
            this.documentoSeleccionado.getDocumentoSoporte().setTramiteId(this.tramiteSeleccionado.
                getId());
            this.radicadoValidado = true;
        }

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método para el evento change en las tabs de recibir documentos
     *
     * @author franz.gamba
     */
    public void onTabChange() {
        this.rutaMostrar = "";
        this.documentoSeleccionado = new TramiteDocumentacion();
        this.documentoSeleccionado.setDocumentoSoporte(new Documento());
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que permite saber si el area de un terreno puede ser vacia o no
     *
     * @author franz.gamba
     * @return
     */
    public boolean isObligatoriaAreaTerreno() {
        if (this.prediosTramite != null && !this.prediosTramite.isEmpty()) {
            String numPredial = this.prediosTramite.get(0).getPredio()
                .getNumeroPredial();
            char[] numPredioVal = numPredial.toCharArray();

            if (this.solicitudSeleccionada.isSolicitudAutoavaluo() &&
                (numPredioVal[21] == '5' || numPredioVal[21] == '6')) {
                return true;
            }

            if (numPredioVal[21] == '5' || numPredioVal[21] == '6' ||
                numPredioVal[21] == '7' || numPredioVal[21] == '8' ||
                numPredioVal[21] == '9') {
                return false;
            }
        } else if (this.solicitudSeleccionada.isSolicitudAutoavaluo()) {
            return false;
        }
        return true;
    }

    private boolean hayRepresentanteLegal() {
        for (SolicitanteSolicitud ss : this.solicitudSeleccionada.getSolicitanteSolicituds()) {
            if (ss.getRelacion().equalsIgnoreCase(ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL.
                getValor())) {
                return true;
            }
        }
        return false;
    }

    private boolean hayApoderado() {
        for (SolicitanteSolicitud ss : this.solicitudSeleccionada.getSolicitanteSolicituds()) {
            if (ss.getRelacion().equalsIgnoreCase(ESolicitanteSolicitudRelac.APODERADO.getValor())) {
                return true;
            }
        }
        return false;
    }

    // ----------------------------------------------------- //
    /**
     * Método que basandose en un numero de resolución busca su documento asociado en alfresco.
     */
    public void cargarResolucionPorNumeroResolucion() {

        if (this.numResolucion != null && !this.numResolucion.trim().isEmpty()) {

            if (this.documentoResolucionVG != null &&
                this.documentoResolucionVG.getIdRepositorioDocumentos() != null) {
                // Descargar la resolución de alfresco y le agrega una marca de
                // agua
                this.resolucionWaterMark = this.reportsService
                    .consultarReporteDeGestorDocumental(this.documentoResolucionVG
                        .getIdRepositorioDocumentos());
            }
        }
    }

    // -----------------------------------------------//
    /**
     * Método que realiza el reinicio de la variable ruta a mostrar. Se creó un método para el set
     * de la variable debido a que es posible que se deban reiniciar otras más adelante.
     *
     * @param event
     */
    public void reiniciarValoresDocumentacion(
        org.primefaces.event.CloseEvent event) {
        this.rutaMostrar = "";
    }

    // -----------------------------------------------//
    /**
     * Método que se ejecuta cuando en la pantalla de radicación se selecciona o se desselecciona un
     * valor de la forma de petición derecho de petición
     *
     * @author javier.aponte
     */
    public void seleccionFormaDePeticionDerechoDePeticion() {
        this.activarFormaDeTramite = false;
        if (this.formaDePeticionDerechoDePeticion) {
            this.activarFormaDeTramite = true;
            this.solicitudSeleccionada.setAsunto(ESolicitudFormaPeticion.DERECHO_DE_PETICION.
                getValor() + "/" + this.solicitudSeleccionada.getAsunto());
            this.solicitudSeleccionada.setFormaPeticion(ESolicitudFormaPeticion.DERECHO_DE_PETICION.
                getCodigo());
        } else {
            this.solicitudSeleccionada.setAsunto(this.solicitudSeleccionada.getAsunto().replace(
                ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor() + "/", ""));
            this.solicitudSeleccionada.setFormaPeticion(null);
        }
        if (this.formaDePeticionTutela) {
            this.formaDePeticionTutela = false;
            this.solicitudSeleccionada.setAsunto(this.solicitudSeleccionada.getAsunto().replace(
                ESolicitudFormaPeticion.TUTELA.getValor() + "/", ""));
            this.solicitudSeleccionada.setFormaPeticion(ESolicitudFormaPeticion.TUTELA.getCodigo());
        }
    }

    /**
     * Método que se ejecuta cuando en la pantalla de radicación se selecciona o se desselecciona un
     * valor de la forma de petición tutela
     *
     * @author javier.aponte
     */
    public void seleccionFormaDePeticionTutela() {
        this.activarFormaDeTramite = false;
        if (this.formaDePeticionTutela) {
            this.activarFormaDeTramite = true;
            this.solicitudSeleccionada.setAsunto(ESolicitudFormaPeticion.TUTELA.getValor() + "/" +
                this.solicitudSeleccionada.getAsunto());
            this.solicitudSeleccionada.setFormaPeticion(ESolicitudFormaPeticion.TUTELA.getCodigo());
        } else {
            this.solicitudSeleccionada.setAsunto(this.solicitudSeleccionada.getAsunto().replace(
                ESolicitudFormaPeticion.TUTELA.getValor() + "/", ""));
            this.solicitudSeleccionada.setFormaPeticion(null);
        }
        if (this.formaDePeticionDerechoDePeticion) {
            this.formaDePeticionDerechoDePeticion = false;
            this.solicitudSeleccionada.setAsunto(this.solicitudSeleccionada.getAsunto().replace(
                ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor() + "/", ""));
            this.solicitudSeleccionada.setFormaPeticion(ESolicitudFormaPeticion.DERECHO_DE_PETICION.
                getCodigo());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para eliminar el documento digital asociado
     *
     * @author juanfelipe.garcia
     */
    public void eliminarDocumentoSoporte() {
        this.documentoSoporteEliminado = true;
        this.rutaMostrar = "";
        this.archivoResultado = null;
        this.banderaNuevoArchivo = false;
        this.nombreTemporalArchivo = new String();
        this.documentoSeleccionado.setCantidadFolios(0);
        this.documentoSeleccionado.setAporta(false);
    }
//--------------------------------------------------------------------------------------------------            

    /**
     * Método que se ejecuta cuando se selecciona el botón de relacionar trámite
     *
     * @author javier.aponte
     */
    public void relacionarTramite() {

        this.documentoSeleccionado = new TramiteDocumentacion();
        this.documentoSeleccionado.setTipoDocumento(new TipoDocumento());
        ConsultaTramiteMB mbConsultaTramite = (ConsultaTramiteMB) UtilidadesWeb.getManagedBean(
            "consultaTramite");
        mbConsultaTramite.resetForm();
    }

    /**
     * Método que se ejecuta cuando se selecciona un trámite en la tabla de derecho de petición
     * trámite
     *
     * @author javier.aponte
     */
    public void seleccionarTramiteDerechoPeticionOTutelaListener() {

        ConsultaTramiteMB mbConsultaTramite = (ConsultaTramiteMB) UtilidadesWeb.getManagedBean(
            "consultaTramite");

        Tramite tramiteSeleccionadoDerechoPeticionOTutela = mbConsultaTramite.
            getSelectedTramiteRow();

        this.documentoSeleccionado.setDepartamento(tramiteSeleccionadoDerechoPeticionOTutela.
            getDepartamento());
        this.documentoSeleccionado.setMunicipio(tramiteSeleccionadoDerechoPeticionOTutela.
            getMunicipio());
        this.documentoSeleccionado.setTramite(tramiteSeleccionadoDerechoPeticionOTutela);

        this.departamentoDocumento = tramiteSeleccionadoDerechoPeticionOTutela.getDepartamento();
        this.municipioDocumento = tramiteSeleccionadoDerechoPeticionOTutela.getMunicipio();

        this.updateDepartamentosDocumento();
        this.updateMunicipiosDocumento();

    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza una validación para activar el botón adicionar trámite. Retorna false
     * cuando la solicitud es una revisión de avalúo o una autoestimación, el solicitante es un
     * apoderado y ya se ha adicionado un trámite.
     *
     * @note: Solicitado por el control de calidad asociado a #5569
     *
     * @author david.cifuentes
     */
    public boolean isActivarAdicionarTramite() {
        if (this.banderaAvisosNoTerminados) {
            return true;
        }

        if (this.solicitudSeleccionada != null &&
            (this.solicitudSeleccionada.isRevisionAvaluo() || this.solicitudSeleccionada
            .isSolicitudAutoavaluo()) &&
            this.solicitudSeleccionada.getSolicitanteSolicituds() != null &&
            !this.solicitudSeleccionada.getSolicitanteSolicituds()
                .isEmpty() &&
            this.solicitudSeleccionada.getSolicitanteSolicituds().get(0)
                .getRelacion() != null &&
            this.solicitudSeleccionada
                .getSolicitanteSolicituds()
                .get(0)
                .getRelacion()
                .equals(ESolicitanteSolicitudRelac.APODERADO.toString()) &&
            this.tramitesSolicitud != null &&
            this.tramitesSolicitud.size() == 1) {
            return false;

        }
        return true;
    }
    // -----------------------------------------------//

    /**
     * Método que se ejecuta cuando en la pantalla de radicación se selecciona o se desselecciona un
     * valor de la forma de trámite nuevo
     *
     * @author javier.aponte
     */
    public void seleccionFormaTramiteNuevo() {
        if (this.formaDeTramiteTramiteRadicado) {
            this.formaDeTramiteTramiteRadicado = false;
        }
    }

    /**
     * Método que se ejecuta cuando en la pantalla de radicación se selecciona o se desselecciona un
     * valor de la forma de trámite radicado
     *
     * @author javier.aponte
     */
    public void seleccionFormaTramiteRadicado() {
        if (this.formaDeTramiteTramiteNuevo) {
            this.formaDeTramiteTramiteNuevo = false;
        }
    }

    /**
     * Método encargado de activar el botón de imprimir la constancia de radicación
     *
     * @author javier.aponte
     */
    private boolean activarBotonImprimirConstanciaRadicacion() {
        if (this.solicitudSeleccionada.isRevisionAvaluo() || this.solicitudSeleccionada.
            isSolicitudAutoavaluo() ||
            this.solicitudSeleccionada.isViaGubernativa()) {
            this.generarReporteConstanciaRadicacion();
            return true;
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * avanza o crea los procesos de los trámites de la solicitud. Se hizo una extracción a método
     * del segmento de código que estaba en el método consolidarTramite.
     *
     */
    /*
     * @modified pedro.garcia 03-12-2013 se diferencia si el trámite es de cancelación de predio o
     * de rectificación de cancelación por doble inscripción para llamar al método de validación de
     * geometría de predios
     */
    /**
     * @throws Exception
     * @modified by juanfelipe.garcia :: 27-12-2013 :: CC-NP-CO-030 suspender tramite original
     * cuando se radica una vía gubernativa
     * @modified by javier.aponte :: 10-08-2015 :: Se agrega try catch en el código de suspender la
     * actividad y se lanza la excepción
     */
    private void avanzarOCrearProcesosTramites() throws Exception {

        try {

            for (Tramite tramiteT : this.solicitudSeleccionada.getTramites()) {
                if (this.tramitesAlmacenados == null ||
                    !this.tramitesAlmacenados.contains(tramiteT.getId())) {

                    SolicitudCatastral sc = new SolicitudCatastral();
                    sc.setIdentificador(tramiteT.getId());
                    sc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(tramiteT.getFechaRadicacion());
                    sc.setFechaRadicacion(cal);
                    sc.setNumeroRadicacion(tramiteT.getNumeroRadicacion());

                    // Validar si los tramites tienen derecho de peticion o tutela para que sean visualizados en el arbol de tareas
                    Pattern patronConsultaDerechoPeticion = Pattern.compile(".*Derecho de petición.*");
                    Matcher matConsultaDerechoPeticion = patronConsultaDerechoPeticion.matcher(
                            this.solicitudSeleccionada.getAsunto());

                    Pattern patronConsultaTutela = Pattern.compile(".*Tutela.*");
                    Matcher matConsultaTutela = patronConsultaTutela.matcher(this.solicitudSeleccionada.
                            getAsunto());

                    if (matConsultaDerechoPeticion.matches()) {

                        sc.setObservaciones(ESolicitudFormaPeticion.DERECHO_DE_PETICION.getValor());

                    } else if (matConsultaTutela.matches()) {

                        sc.setObservaciones(ESolicitudFormaPeticion.TUTELA.getValor());
                    }

                    if (tramiteT.getPredio() != null) {
                        sc.setNumeroPredial(tramiteT.getPredio().getNumeroPredial());
                    }

                    boolean enviarAEscanear = false;
                    boolean enviarAControl = true;
                    boolean algunAdjunto = false;
                    int aportados = 0;

                    for (TramiteDocumentacion td : tramiteT.getTramiteDocumentacions()) {
                        if (td.getAportado() != null &&
                                td.getAportado().equals(ESiNo.SI.getCodigo()) &&
                                td.getDigital() != null &&
                                td.getDigital().equals(ESiNo.SI.getCodigo())) {
                            enviarAEscanear = true;
                        }
                        if (td.getAportado() != null &&
                                td.getAportado().equals(ESiNo.SI.getCodigo())) {
                            aportados++;
                            if (td.getDocumentoSoporte() == null ||
                                    td.getDocumentoSoporte().getArchivo() == null ||
                                    td.getDocumentoSoporte().getArchivo().equals(
                                            Constantes.CONSTANTE_CADENA_VACIA_DB) ||
                                    td.getDocumentoSoporte().getArchivo().isEmpty()) {
                                enviarAControl = false;
                            } else {
                                algunAdjunto = true;
                            }
                        }
                    }
                    if (tramiteT.isViaGubernativa()) {
                        if (this.tramitesGubernativaAll != null && !this.tramitesGubernativaAll.
                                isEmpty()) {
                            Tramite tramiteOriginal = this.tramitesGubernativaAll.get(0);
                            if (!tramiteOriginal.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.
                                    getCodigo())) {
                                List<Actividad> actividadesTramiteOriginal = this.getProcesosService().
                                        getActividadesPorIdObjetoNegocio(tramiteOriginal.getId());
                                if (actividadesTramiteOriginal != null && !actividadesTramiteOriginal.
                                        isEmpty()) {
                                    Actividad actividad = actividadesTramiteOriginal.get(0);
                                    String motivo = "Tramite suspendido por radicación de recurso";
                                    Calendar tiempoFinInterponerRecurso = actividad.getFechaExpiracion();
                                    try {
                                        this.getProcesosService().suspenderActividad(actividad.getId(),
                                                tiempoFinInterponerRecurso, motivo);
                                    } catch (Exception e) {
                                        this.addMensajeError(
                                                "Ocurrió un error al tratar de suspender el trámite con número de radicación: " +
                                                        this.tramitesGubernativaAll.get(0).getNumeroRadicacion());
                                        throw e;
                                    }
                                }
                            }
                        }
                    }
                    if (enviarAEscanear) {
                        sc.setRol(ERol.DIGITALIZADOR.getDistinguishedName());
                        sc.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
                        List<UsuarioDTO> usuarios;
                        usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                                this.usuario.getDescripcionEstructuraOrganizacional(),
                                ERol.DIGITALIZADOR);

                        /* CC-NP-CO-012 usar con la nueva version de process */
                        List<ActividadUsuarios> transicionesUsuarios =
                                new ArrayList<ActividadUsuarios>();
                        transicionesUsuarios.add(new ActividadUsuarios(
                                ProcesoDeConservacion.ACT_DIGITALIZACION_ESCANEAR, usuarios));
                        sc.setActividadesUsuarios(transicionesUsuarios);

                    } else if ((aportados > 0 && enviarAControl) || algunAdjunto) {
                        sc.setRol(ERol.CONTROL_DIGITALIZACION.getDistinguishedName());
                        sc.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
                        List<UsuarioDTO> usuarios;
                        usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                                this.usuario.getDescripcionEstructuraOrganizacional(),
                                ERol.CONTROL_DIGITALIZACION);

                        /* CC-NP-CO-012 usar con la nueva version de process */
                        List<ActividadUsuarios> transicionesUsuarios =
                                new ArrayList<ActividadUsuarios>();
                        transicionesUsuarios.add(new ActividadUsuarios(
                                ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO, usuarios));
                        sc.setActividadesUsuarios(transicionesUsuarios);

                    } else {
                        ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
                        usuarios.add(this.usuario);
                        /* CC-NP-CO-012 usar con la nueva version de process */
                        List<ActividadUsuarios> transicionesUsuarios =
                                new ArrayList<ActividadUsuarios>();
                        transicionesUsuarios.add(new ActividadUsuarios(
                                ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES, usuarios));
                        sc.setActividadesUsuarios(transicionesUsuarios);

                    }

                    if (tramiteT.getPredio() != null) {
                        sc.setNumeroPredial(tramiteT.getPredio().getNumeroPredial());
                    }
                    sc.setTipoTramite(tramiteT.getTipoTramiteCadenaCompleto());

                    if (this.resolucionTramiteEnProyeccion) {
                        // - Sí la resolución del trámite está en proyección significa que el
                        // trámite ya está creado, por lo que
                        // sólo se avanza a su siguiente actividad.
                        LOGGER.debug(
                                "avanzando actividad cuando la resolución del trámite está en proyección");
                        this.getProcesosService()
                                .avanzarActividad(this.usuario,
                                        this.actividadSeleccionada.getId(), sc);
                    } else {
                        // - Sí la resolución del trámite está en firme, se
                        // crea un nuevo proceso para este trámite.
                        if (tramiteT.getProcesoInstanciaId() == null) {
                            LOGGER.debug("creando proceso para el nuevo trámite");

                            //OJO :: si el servidor de procesos se demora mucho, las actualizaciones de bd siguientes se totean porque no hay una transacción
                            String procesoInstanciaId = this.getProcesosService().crearProceso(sc);
                            if (procesoInstanciaId == null) {
                                LOGGER.error("Ocurrió un error al generar el id de procesos");
                            }
                            tramiteT.setProcesoInstanciaId(procesoInstanciaId);

                            //D: Lanzar tarea asincronica para validar la geometria del predio-
                            //D: cuando el trámite es de cancelación de predio o de rectificación por cancelación por doble inscripción,
                            //   se hace una validación distinta
                            if (tramiteT.isCancelacionPredio() ||
                                    (tramiteT.isEsRectificacion() && contieneDatoRectificarCDI(
                                            this.selectedDatosRectifComplDataTable))) {
                                //TODO :: pedro.garcia refs #6050 :: probar esto
                                this.getGeneralesService().validarGeometriaPrediosTramCancelPredioA(
                                        tramiteT.getId(), this.usuario);
                            } else {
                                //Validar geometría de predios, no valida para mejoras se hace posteriormente
                                try {

                                    boolean tMejoras = false;

                                    if (tramiteT.getPredio() != null &&
                                            tramiteT.getPredio().isMejora()) {
                                        tMejoras = true;
                                    }
                                    for (Predio p : tramiteT.getPredios()) {
                                        if (p.isMejora()) {
                                            tMejoras = true;
                                        }
                                    }

                                    //Se realiza la validación si no es una mejora
                                    if (!tMejoras) {

                                        //felipe.cadena::24-07-2015::Se evalua el envio de la validacion asincronica
                                        Boolean validarAsincronico = false;
                                        if (tramiteT.isTramiteMasivo()) {
                                            if (tramiteT.getPredio().getNumeroPredial().endsWith(
                                                    Constantes.FM_CONDOMINIO_END) ||
                                                    tramiteT.isSegundaEnglobe()) {
                                                Parametro parametroMasivo = this.getGeneralesService().
                                                        getCacheParametroPorNombre(
                                                                EParametro.EDITOR_UMBRAL_ASYNC.toString());
                                                int numeroPredios = this.getConservacionService().
                                                        contarPrediosAsociadosATramite(tramiteT.getId());

                                                if (numeroPredios >= parametroMasivo.getValorNumero()) {
                                                    this.getGeneralesService().
                                                            validarGeometriaPrediosAsincronico(tramiteT,
                                                                    this.usuario);

                                                    validarAsincronico = true;
                                                }

                                            }
                                        }

                                        //felipe.cadena::24-07-2015::SI se envio la validacion asincronica no se envian mas validaciones
                                        if (!validarAsincronico) {
                                            //Se realiza la validación para trámites que sean geográficos pero que no sean de quinta

                                            tramiteT.setTramiteRectificacions(this.getTramiteService().
                                                    findTramiteRectificacionesByTramiteId(tramiteT.getId()));
                                            tramiteT.setComisionTramites(this.getTramiteService().
                                                    buscarComisionesTramitePorIdTramite(tramiteT.getId()));

                                            if (tramiteT.isTramiteGeografico() && !(tramiteT.isQuinta())) {

                                                //Se realiza la validación si el predio no es de condición de propiedad 8 ni 9
                                                if (!EPredioCondicionPropiedad.CP_8.getCodigo().equals(
                                                        tramiteT.getPredio().getCondicionPropiedad()) &&
                                                        !EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                                                                tramiteT.getPredio().getCondicionPropiedad())) {
                                                    this.getGeneralesService().validarGeometriaPredios(
                                                            tramiteT, this.usuario);
                                                } else {
                                                    //Si la condición de propiedad del predio es 8 o 9, se realiza la validación sólo si el trámite
                                                    //no es de tercera ni de rectificación de área
                                                    if (!tramiteT.isTercera() && !(tramiteT.
                                                            isRectificacion() && tramiteT.
                                                            isRectificacionArea())) {
                                                        this.getGeneralesService().
                                                                validarGeometriaPredios(tramiteT,
                                                                        this.usuario);
                                                    }
                                                }
                                            }

                                            //Se realiza la validación para trámites que sean de rectificación de zona
                                            if (tramiteT.isRectificacion() && tramiteT.
                                                    isRectificacionZona()) {
                                                this.getGeneralesService().validarGeometriaPredios(
                                                        tramiteT, this.usuario);
                                            }
                                        }
                                    }

                                } catch (Exception ex) {
                                    LOGGER.error("Error en GestionarSolicitudesMB " +
                                            "Ocurrió un error al validar geometría de predios del " +
                                            "trámite con id " + tramiteT.getId().toString(), ex);
                                    break;
                                }
                            }
                            /////// Fin tarea asincrónica

                        }
                    }

                    tramiteT.setTramiteDocumentos(
                            this.getTramiteService()
                                    .obtenerTramiteDocumentosPorTramiteId(tramiteT.getId()));
                    tramiteT.setTramiteDocumentacions(
                            this.getTramiteService()
                                    .obtenerTramiteDocumentacionPorIdTramite(tramiteT.getId()));
                    tramiteT = this.getTramiteService()
                            .actualizarTramiteDocumentos(tramiteT, this.usuario);
                    if (this.tramitesAlmacenados == null) {
                        this.tramitesAlmacenados = new ArrayList<Long>();
                    }
                    if (tramiteT != null){
                        this.tramitesAlmacenados.add(tramiteT.getId());

                        // Validar documentación aportada, en caso de que haya no se haya guardado en el FTP se acuerda no mostrar el archivo cargado en la temporal
                        if(tramiteT.getTramiteDocumentacions() != null){
                            for(TramiteDocumentacion td : tramiteT.getTramiteDocumentacions()){
                                if(td.getDocumentoSoporte() != null) {
                                    if (td.getDocumentoSoporte().getIdRepositorioDocumentos() == null || td.getDocumentoSoporte().getIdRepositorioDocumentos().trim().isEmpty()) {
                                        td.getDocumentoSoporte().setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                                        td.getDocumentoSoporte().setRutaArchivoLocal(Constantes.CONSTANTE_CADENA_VACIA_DB);
                                        td.getDocumentoSoporte().setRutaArchivoWeb(Constantes.CONSTANTE_CADENA_VACIA_DB);
                                        this.getTramiteService().guardarYactualizarDocumento(td.getDocumentoSoporte());
                                    }
                                }
                            }
                        }

                        try {
                            this.getGeneralesService().validarInconsistencias(tramiteT, this.usuario);
                        } catch (Exception e) {
                            LOGGER.debug("Ocurrió un error al validar la geometría de predios");
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            this.addMensajeError(
                    "Ocurrió un error al avanzar o crear los procesos de los trámites de la solicitud.");
            LOGGER.error("Ocurrió un error al avanzar o crear los procesos de los trámites de la solicitud. GestionarSolicitudesMB.avanzarOCrearProcesosTramites", e);
            throw e;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Este se ejecuta siempre, luego de haber hecho la primera transferencia de datos de fuente a
     * destino, al hacer submit de la form que contiene el picklist. </br>
     *
     * @author pedro.garcia
     */
    /*
     * Se trató de usar para la validación de que si escoge un dato específico este sea el único,
     * pero no se pudo hacer que no se ejecutara el método action del botón que hace submit, por lo
     * cual se deja esa validación allá
     */
    public void onRectifComplPLTransferListener(ValueChangeEvent e) {

        LOGGER.debug("transferencia en el picklist");
        //TODO:: Implementar logica para este metodo
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Revisa si alguno de los elementos de la lista es del tipo 'Cancelación por doble inscripción'
     *
     * @author pedro.garcia
     * @param lista
     * @return
     */
    private boolean contieneDatoRectificarCDI(List<DatoRectificar> lista) {

        boolean answer = false;

        for (DatoRectificar dr : lista) {
            if (dr.isDatoRectifCDI()) {
                answer = true;
                break;
            }
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que valida si alguno de los predios seleccionados para el trámite tiene mejoras. </br>
     * Se usa en el caso de una rectificación, cuando se seleccionó el dato 'cancelación doble
     * inscripción', en cuyo caso hay solo un predio en la lista
     *
     * @author pedro.garcia
     */
    private boolean validarPredioTieneMejoras() {

        boolean answer = false;

        if (this.getPrediosSeleccionadosDeComponenteSeleccPredios() != null &&
            !this.getPrediosSeleccionadosDeComponenteSeleccPredios().isEmpty()) {

            for (Predio pTmp : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {

                boolean tieneMejoras = this.getConservacionService().
                    tieneMejorasPredio(pTmp.getNumeroPredioDeNumeroPredial());
                if (tieneMejoras && !pTmp.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_5.getCodigo())) {
                    answer = true;
                    break;
                }
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que valida si el predio seleccionado para el trámite de cancelación de predio se puede
     * cancelar. </br>
     * La condición definida es que no tenga condición de propiedad 7, 8 o 9. </br>
     *
     * @author pedro.garcia
     */
    private boolean validarPredioEsCancelable() {

        boolean answer = true;

        if (this.getPrediosSeleccionadosDeComponenteSeleccPredios() != null &&
            !this.getPrediosSeleccionadosDeComponenteSeleccPredios().isEmpty()) {

            //D: solo debe haber un predio en esta lista
            for (Predio pTmp : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {
                String condicionProp = pTmp.getCondicionPropiedad();
                if (EPredioCondicionPropiedad.CP_7.getCodigo().equals(condicionProp) ||
                    EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionProp) ||
                    EPredioCondicionPropiedad.CP_9.getCodigo().equals(condicionProp)) {
                    answer = false;
                    break;
                }
            }

        }
        return answer;
    }

    /**
     * Método que valida si el predio seleccionado para el trámite de cancelación de predio se puede
     * cancelar. </br>
     * La condición definida es que no haya sido cancelado anteriormente. </br>
     *
     * @author leidy.gonzalez
     */
    private boolean validarPredioCancelado() {

        boolean answer = true;
        String estado = "";

        if (this.getPrediosSeleccionadosDeComponenteSeleccPredios() != null &&
            !this.getPrediosSeleccionadosDeComponenteSeleccPredios().isEmpty()) {

            //D: solo debe haber un predio en esta lista
            for (Predio pTmp : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {

                String condicionProp = pTmp.getCondicionPropiedad();
                if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionProp) ||
                    EPredioCondicionPropiedad.CP_9.getCodigo().equals(condicionProp)) {

                    FichaMatriz pfm = this.getConservacionService().buscarFichaMatrizPorPredioId(
                        pTmp.getId());

                    if (pfm != null && pfm.getEstado() != null) {
                        estado = pfm.getEstado();
                        if (EFichaMatrizEstado.CANCELADO.getCodigo()
                            .equals(estado)) {
                            answer = false;
                            break;
                        }
                    }
                }

            }

        }
        return answer;
    }

    /**
     * Método que se ejecuta cuando se oprime el botón de adicionar documento soporte en la ventana
     * de adicionar productos
     *
     * @author javier.aponte modificado por leidy.gonzalez
     * @modified by leidy.gonzalez 04/09/2014 Se agrega a el documento copia y la lista de
     * documentos copia los campos ingresados por el usuario
     */
    public void adicionarDocumentoSoporteProductos() {

        this.nuevoDocumentoSoporte.setTipoDocumento(this.tipoDocumentoSoporteSeleccionado);

        this.documentoCopia.setTipoDocumento(this.tipoDocumentoSoporteSeleccionado);

        if (this.documentosSoportesAdicionados == null) {
            this.documentosSoportesAdicionados = new ArrayList<Documento>();
        }

        //v1.1.9
        if (!this.documentosSoportesAdicionados.isEmpty()) {
            for (Documento doc : this.documentosSoportesAdicionados) {

                this.documentoCopia = (Documento) doc.clone();

                if ((this.nuevoDocumentoSoporte.getTipoDocumento() != null &&
                    this.nuevoDocumentoSoporte.getTipoDocumento().equals(doc.getTipoDocumento())) &&
                    (this.nuevoDocumentoSoporte.getNumeroDocumento() != null &&
                    this.nuevoDocumentoSoporte.getNumeroDocumento().
                        equals(doc.getNumeroDocumento()))) {
                    this.addMensajeError(
                        "Este número de documento soporte ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                    return;
                }
            }
        }
        this.documentosSoportesAdicionados.add(this.nuevoDocumentoSoporte);

        numeroDocumentoCopia = this.nuevoDocumentoSoporte.getNumeroDocumento();
        this.documentoCopia.setNumeroDocumento(numeroDocumentoCopia);
        this.documentosCopia.add(this.documentoCopia);

        this.nuevoDocumentoSoporte = new Documento();
        this.tipoDocumentoSoporteSeleccionado = new TipoDocumento();
    }

    /**
     * Método que se ejecuta cuando se oprime el botón de eliminar documento soporte en la ventana
     * de adicionar productos
     *
     * @author javier.aponte modificado por leidy.gonzalez Se agrega validación para eliminar cuando
     * es la ultima fila de la lista y cuando el numero de documento soporte es el mismo.
     */
    public void eliminarDocumentoSoporteProductos() {
        //v1.1.9
        List<Documento> documentosSoportesAuxiliares = new ArrayList<Documento>();

        if (this.documentosSoportesAdicionados != null) {
            for (Documento doc : this.documentosSoportesAdicionados) {

                if (doc.getNumeroDocumento().equals(
                    this.nuevoDocumentoSoporteSeleccionado
                        .getNumeroDocumento())) {

                    this.documentosSoportesAdicionados
                        .remove(this.nuevoDocumentoSoporteSeleccionado);
                    break;
                } else {
                    documentosSoportesAuxiliares.add(doc);
                    this.documentosSoportesAdicionados = documentosSoportesAuxiliares;
                    break;
                }

            }
        }

    }

    /**
     * Método encargado de guardar la edición del documento soporte
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez Se valida que no se ingrese el mismo documento soporte que ya ha
     * sido ingresado al ser editado.
     * @modified by leidy.gonzalez 04/09/2014 Se verifica que cuando un documento ya haya sido
     * ingresado, en la lista a visualizar no se muestre el número de documento editado sino el
     * insertado por el ususario.
     */
    public void aceptarEdicionDocumentoSoporteProducto() {

        int contador = 0;

        if (!this.documentosSoportesAdicionados.isEmpty()) {
            for (Documento doc : this.documentosSoportesAdicionados) {
                if ((this.nuevoDocumentoSoporteSeleccionado.getTipoDocumento() != null &&
                    this.nuevoDocumentoSoporteSeleccionado.getTipoDocumento().equals(doc.
                        getTipoDocumento())) &&
                    (this.nuevoDocumentoSoporteSeleccionado.getNumeroDocumento() != null &&
                    this.nuevoDocumentoSoporteSeleccionado.getNumeroDocumento().equals(doc.
                        getNumeroDocumento()))) {

                    contador++;
                }
                if (contador >= 2) {
                    for (Documento docCopia : documentosCopia) {
                        if (!this.nuevoDocumentoSoporteSeleccionado.getNumeroDocumento().equals(
                            docCopia.getNumeroDocumento())) {
                            doc.setNumeroDocumento(docCopia.getNumeroDocumento());
                        }
                    }
                    this.addMensajeError(
                        "Este número de documento soporte ya ha sido ingresado, por favor verifique los datos e intente nuevamente");
                    return;
                }
            }

            if (this.rutaMostrar != null && !this.rutaMostrar.trim().isEmpty()) {
                this.nuevoDocumentoSoporteSeleccionado.setArchivo(this.rutaMostrar);
                this.nuevoDocumentoSoporteSeleccionado.setEstado(EDocumentoEstado.CARGADO.
                    getCodigo());
            } else {

                this.nuevoDocumentoSoporteSeleccionado.setArchivo(
                    Constantes.CONSTANTE_CADENA_VACIA_DB);
                this.nuevoDocumentoSoporteSeleccionado.setEstado(EDocumentoEstado.RECIBIDO.
                    getCodigo());
            }
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("error", "error");

    }

    /**
     * Método encargado de inicializar las variables necesarias en el caso de productos, se ejecuta
     * cuando en la pantalla de radicación se oprime el botón de adicionar productos
     *
     * @author javier.aponte
     */
    public void inicializarVariablesProductos() {

        this.documentosCopia = new ArrayList<Documento>();

        this.documentoCopia = new Documento();

        this.tipoDocumentoSoporteSeleccionado = new TipoDocumento();

        this.nuevoDocumentoSoporte = new Documento();

        this.activarOpcionesProductosAsociados = true;

        this.activarOpcionesDocumentosSoportes = true;

        this.activarPanelProductosAsociados = false;

        this.documentosSoportesAdicionados = this.getTramiteService().
            buscarDocumentacionPorSolicitudId(this.solicitudSeleccionada.getId());

        this.productosCatastralesAsociados = this.getGeneralesService().
            buscarProductosCatastralesPorSolicitudId(this.solicitudSeleccionada.getId());

        if (this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.
            isEmpty()) {
            this.activarPanelProductosAsociados = true;

            for (ProductoCatastral pc : this.productosCatastralesAsociados) {
                //Si algún producto catastral ya fue almacenado no debe permitir la opción de adicionar, editar ni eliminar
                if (pc.getId() != null) {
                    this.activarOpcionesProductosAsociados = false;
                    break;
                }
            }
        }

        if (this.documentosSoportesAdicionados != null && !this.documentosSoportesAdicionados.
            isEmpty()) {

            for (Documento documentoTemp : this.documentosSoportesAdicionados) {

                //Si algún documento ya fue almacenado no debe permitir la opción de adicionar, editar ni eliminar
                if (documentoTemp.getId() != null) {
                    this.activarOpcionesDocumentosSoportes = false;
                    this.activarPanelProductosAsociados = true;
                    break;
                }

            }
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización
     *
     * @author javier.aponte
     */
    public String cargarDocumentoTemporalProductoPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.nuevoDocumentoSoporteSeleccionado.getArchivo());
        return this.tipoMimeDocTemporal;
    }

    /**
     * Método encargado de guardar los documentos soporte productos
     *
     * @author javier.aponte
     */
    public void guardarDocumentosSoporteProductos() {

        String mensaje = null;

        //Crear el proceso
        try {

            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(this.usuario);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_INGRESAR_AJUSTAR_SOLICITUD_PRODUCTO,
                usuarios));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("creando proceso para la nueva solicitud");

            if (this.solicitudSeleccionada.getProcesoInstanciaId() == null) {
                this.solicitudSeleccionada.setProcesoInstanciaId(this.getProcesosService().
                    crearProceso(spc));
            }

            this.getTramiteService().guardaYActualizaSolicitud(this.solicitudSeleccionada);

        } catch (Exception e) {

            mensaje = "Ocurrió un error al crear el proceso de productos";
            LOGGER.error("Ocurrió un error al crear el proceso de productos" + e.getMessage(), e);

        }

        if (mensaje != null) {
            this.addMensajeError(mensaje);
            return;
        }

        TipoDocumento td;
        Documento doc;
        String fileSeparator = System.getProperty("file.separator");

        for (Documento documentoTemp : this.documentosSoportesAdicionados) {

            if (documentoTemp.getId() == null) {
                documentoTemp.setUsuarioCreador(this.usuario.getLogin());
                documentoTemp.setBloqueado(ESiNo.NO.getCodigo());
                documentoTemp.setUsuarioLog(this.usuario.getLogin());
                documentoTemp.setFechaLog(new Date(System.currentTimeMillis()));
                documentoTemp.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                documentoTemp.setNombreDeArchivoCargado(documentoTemp.getArchivo());

                td = this.getGeneralesService().buscarTipoDocumentoPorId(documentoTemp.
                    getTipoDocumento().getId());

                if (td != null) {
                    documentoTemp.setTipoDocumento(td);
                }

                if (documentoTemp.getArchivo() != null && !documentoTemp.getArchivo().trim().
                    isEmpty()) {
                    documentoTemp.setEstado(EDocumentoEstado.CARGADO.getCodigo());
                } else {
                    documentoTemp.setArchivo(Constantes.CONSTANTE_CADENA_VACIA_DB);
                }

                if (documentoTemp.getArchivo() != null && !documentoTemp.getArchivo().equals(
                    Constantes.CONSTANTE_CADENA_VACIA_DB)) {

                    DocumentoSolicitudDTO datosGestorDocumental =
                        DocumentoSolicitudDTO.crearArgumentoCargarSolicitudAvaluoComercial(
                            this.solicitudSeleccionada.getNumero(), this.solicitudSeleccionada.
                            getFecha(), documentoTemp.getArchivo(),
                            UtilidadesWeb.obtenerRutaTemporalArchivos() + fileSeparator +
                            documentoTemp.getArchivo());

                    // Subo el archivo a Alfresco y actualizo el documento
                    doc = this.getGeneralesService().guardarDocumentosSolicitudAvaluoAlfresco(
                        this.usuario, datosGestorDocumental, documentoTemp);
                } else {

                    doc = this.getTramiteService().guardarYactualizarDocumento(documentoTemp);

                }

                if (doc != null) {

                    documentoTemp.setId(doc.getId());

                    this.solicitudDocumentacion = new SolicitudDocumentacion();

                    this.solicitudDocumentacion.setSolicitud(this.solicitudSeleccionada);
                    this.solicitudDocumentacion.setFechaAporte(new Date());
                    this.solicitudDocumentacion.setUsuarioLog(this.usuario.getLogin());
                    this.solicitudDocumentacion.setFechaLog(new Date(System.currentTimeMillis()));
                    this.solicitudDocumentacion.setAportado(ESiNo.SI.getCodigo());

                    this.solicitudDocumentacion.setSoporteDocumentoId(doc);
                    this.solicitudDocumentacion.setTipoDocumento(doc.getTipoDocumento());

                    this.solicitudDocumentacion = this.getTramiteService().
                        guardarActualizarSolicitudDocumentacion(solicitudDocumentacion);
                }
            }
        }
        this.activarOpcionesDocumentosSoportes = false;
        this.activarPanelProductosAsociados = true;
        this.addMensajeInfo("Información almacenada exitosamente");

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de eliminar productos catastrales asociados
     * en la ventana de adicionar productos
     *
     * @author javier.aponte
     */
    public void eliminarProductosCatastralesAsociados() {

        this.productosCatastralesAsociados.remove(this.productoCatastralSeleccionado);

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de editar productos catastrales asociados en
     * la ventana de adicionar productos
     *
     * @author javier.aponte
     */
    public void editarProductosCatastralesAsociados() {

        this.productoSeleccionado = this.productoCatastralSeleccionado.getProducto();

        this.grupoProductoSeleccionado = this.productoSeleccionado.getGrupoProducto();

        this.productoPorGrupoV = this.generalMB.getCodigoProductosV(this.grupoProductoSeleccionado.
            getId());

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de adicionar producto catastral y que se
     * encarga de inicializar las variables necesarias para adicionar un producto catastral
     *
     * @author javier.aponte
     */
    public void inicializarProductoCatastralAsociadoASolicitud() {

        this.productoCatastralSeleccionado = new ProductoCatastral();
        this.productoSeleccionado = new Producto();
        this.grupoProductoSeleccionado = new GrupoProducto();
        this.productoPorGrupoV = new ArrayList<SelectItem>();

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de aceptar en la pantalla de adicionar
     * producto
     *
     * @author javier.aponte
     */
    public void adicionarProductoCatastralAsociadoASolicitud() {

        if (!this.editarProductoCatastral) {
            boolean productoYaFueAsociado = false;

            if (this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.
                isEmpty()) {
                for (ProductoCatastral pcTemp : this.productosCatastralesAsociados) {
                    if (pcTemp.getProducto().getCodigo().equals(this.productoSeleccionado.
                        getCodigo())) {
                        pcTemp.setCantidad(pcTemp.getCantidad() +
                            this.productoCatastralSeleccionado.getCantidad());
                        productoYaFueAsociado = true;
                        break;
                    }
                }
            }

            if (!productoYaFueAsociado) {

                this.productoSeleccionado.setGrupoProducto(this.grupoProductoSeleccionado);

                this.productoCatastralSeleccionado.setProducto(this.productoSeleccionado);

                if (this.productosCatastralesAsociados == null) {
                    this.productosCatastralesAsociados = new ArrayList<ProductoCatastral>();
                }
                this.productosCatastralesAsociados.add(this.productoCatastralSeleccionado);
            }
        }
    }

    /**
     * Método encargado de buscar un producto por código
     *
     * @author javier.aponte
     */
    public void buscarProductoPorCodigo() {

        this.productoSeleccionado = this.getGeneralesService().obtenerProductoPorCodigo(
            this.productoSeleccionado.getCodigo());

    }

    /**
     * Método encargado de buscar una lista de productos por el id del grupo
     *
     * @author javier.aponte
     */
    public void buscarProductoPorGrupoId() {

        this.productoPorGrupoV = this.generalMB.getCodigoProductosV(this.grupoProductoSeleccionado.
            getId());

    }

    /**
     * Método encargado de guardar los productos catastrales asociados a la solicitud en la base de
     * datos
     *
     * @author javier.aponte
     */
    public void guardarProductosCatastralesAsociadosASolicitud() {

        ProductoCatastral pcTemp;
        GrupoProducto gp;

        try {

            for (ProductoCatastral pc : this.productosCatastralesAsociados) {

                gp = this.getGeneralesService().buscarGrupoProductoPorId(pc.getProducto().
                    getGrupoProducto().getId());

                if (gp != null) {
                    pc.getProducto().setGrupoProducto(gp);
                }

                if (pc.getId() == null) {
                    pc.setUsuarioLog(this.usuario.getLogin());
                    pc.setFechaLog(new Date());
                    pc.setSolicitud(this.solicitudSeleccionada);
                    pc.setDatosExtendidos(ESiNo.NO.getCodigo());
                    pc.setDatosPropietarios(ESiNo.NO.getCodigo());
                    pc.setEstado(EProductoCatastralEstado.SIN_EJECUTAR.getCodigo());

                    pcTemp = this.getGeneralesService().
                        guardarActualizarProductoCatastralAsociadoASolicitud(pc);

                    pc.setId(pcTemp.getId());
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error("Ocurrió un error al guardar el producto catastral de la solicitud: " +
                this.solicitudSeleccionada.getNumero(), e);
            this.addMensajeError(
                "Ocurrió un error al guardar el producto catastral de la solicitud: " +
                this.solicitudSeleccionada.getNumero());
        }
        this.activarOpcionesProductosAsociados = false;

    }

    /**
     * Método que valida si alguno de los predios seleccionados no esta en estado activo
     *
     * @author felipe.cadena
     */
    private boolean validarPredioActivo() {
        this.prediosCancelados = new LinkedList<String>();

        boolean answer = true;
        if (this.getPrediosSeleccionadosDeComponenteSeleccPredios() != null &&
            !this.getPrediosSeleccionadosDeComponenteSeleccPredios().isEmpty()) {

            for (Predio pTmp : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {
                pTmp = this.getConservacionService().obtenerPredioPorId(pTmp.getId());
                if (!pTmp.getEstado().equals(EPredioEstado.ACTIVO.getCodigo())) {
                    answer = false;
                    this.prediosCancelados.add(pTmp.getNumeroPredial());
                }
            }
        }

        return answer;
    }

    /**
     * Valida si el municipio del predio se encuentran activo en el sistema (Territoriales y UOC).
     * 13/06/2017
     *
     * @author juan.cruz
     * @return
     */
    private boolean validarPredioEnElSNC() {

        boolean answer = true;
        if (this.getPrediosSeleccionadosDeComponenteSeleccPredios() != null &&
            !this.getPrediosSeleccionadosDeComponenteSeleccPredios().isEmpty()) {

            for (Predio pTmp : this.getPrediosSeleccionadosDeComponenteSeleccPredios()) {
                pTmp = this.getConservacionService().obtenerPredioPorId(pTmp.getId());

                MunicipioComplemento mc = this.getGeneralesService().
                    obtenerMunicipioComplementoPorCodigo(pTmp.getMunicipio().getCodigo());

                if (mc == null || mc.getConInformacionGdb().equals(ESiNo.NO.getCodigo())) {
                    this.addMensajeError(ECodigoErrorVista.PREDIO_NO_ACTIVO_SISTEMA.
                        getMensajeUsuario(pTmp.getNumeroPredial()));
                    LOGGER.warn(ECodigoErrorVista.PREDIO_NO_ACTIVO_SISTEMA.getMensajeTecnico());
                    answer = false;
                }
            }
        }

        return answer;
    }

    /**
     * Método encargado de validar que se hayan ingresado documentos soportes para almacenarlos en
     * la base de datos
     *
     * @author javier.aponte
     */
    public void validarDocumentosSoportes() {

        if (this.documentosSoportesAdicionados == null || this.documentosSoportesAdicionados.
            isEmpty()) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Debe adicionar por lo menos un documento soporte");
        }

    }

    /**
     * Método encargado de validar que se hayan ingresado productos asociados para almacenarlos en
     * la base de datos
     *
     * @author javier.aponte
     */
    public void validarProductosAsociados() {

        if (this.productosCatastralesAsociados == null || this.productosCatastralesAsociados.
            isEmpty()) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Debe adicionar por lo menos un producto catastral");
        }
    }

    /**
     * Método encargado de avanzar la actividad de solicitud de productos a registrar o ajustar
     * datos de solicitud de productos
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez 22/09/2014 Se agregan nuevos filtros para realizar la consulta de
     * lista de actividades
     */
    public String registrarDatosDeProductos() {

        String mensaje = null;

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(this.usuario);

            List<ActividadUsuarios> transicionesUsuarios;
            transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_AJUSTAR_PRODUCTOS_TRAMITE_CATASTRAL,
                usuarios));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug(
                "Moviendo la actividad a Registrar productos e información trámite catastral");

            Map<EParametrosConsultaActividades, String> filtros =
                new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
            filtros.put(EParametrosConsultaActividades.ID_PRODUCTOS_CATASTRALES,
                this.solicitudSeleccionada.getId().toString());
            filtros.put(EParametrosConsultaActividades.NUMERO_SOLICITUD, this.solicitudSeleccionada.
                getNumero());
            filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
            filtros.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, this.usuario.getLogin());

            List<Actividad> actividades;

            actividades = this.getProcesosService().consultarListaActividades(filtros);

            if (actividades == null) {
                mensaje = "No se puede avanzar el proceso, por favor intente más tarde";
            }
            if (actividades != null && !actividades.isEmpty()) {
                this.getProcesosService().avanzarActividad(actividades.get(0).getId(), spc);
            }
        } catch (ExcepcionSNC ex) {
            mensaje =
                "Ocurrió un error al avanzar el proceso a registrar datos de producto solicitado";
            LOGGER.error("Error consultando la lista de actividades por el número de solicitud: " +
                this.solicitudSeleccionada.getNumero(), ex);
        }
        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return cerrar();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }

    }

    /**
     * Método para recuperar el tipo de solicitud de acuerdo al rol del usuario
     *
     * @return
     * @author javier.aponte
     */
    public List<SelectItem> getSolicitudTipoV() {
        List<SelectItem> lista = new ArrayList<SelectItem>();
        List<SelectItem> listaAux;

        listaAux = this.generalMB.getSolicitudTipoV();

        boolean isResponsableAtencionUsuario = false;
        boolean isFuncionarioRadicador = false;

        for (String rol : this.usuario.getRoles()) {
            if (ERol.RESPONSABLE_ATENCION_USUARIO.getRol().equalsIgnoreCase(rol)) {
                isResponsableAtencionUsuario = true;
            }
        }

        for (String rol : this.usuario.getRoles()) {
            if (ERol.FUNCIONARIO_RADICADOR.getRol().equalsIgnoreCase(rol)) {
                isFuncionarioRadicador = true;
            }
        }

        if (listaAux != null && !listaAux.isEmpty()) {
            if (isFuncionarioRadicador) {
                for (SelectItem l : listaAux) {
                    if (!ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo().equals(l.getValue())) {
                        lista.add(l);
                    }
                }
            }
            if (isResponsableAtencionUsuario) {
                for (SelectItem l : listaAux) {
                    if (ESolicitudTipo.PRODUCTOS_CATASTRALES.getCodigo().equals(l.getValue())) {
                        lista.add(l);
                    }
                }
            }
        }

        return lista;
    }

    /**
     * Determina si el tramite que se quiere radicacar si es consistente con el predio sobre el que
     * se va a realizar.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarTipoTramitePredio() {
        String tipoTramite = "";
        String claseMutacion = "";
        String subtipo = "";
        String condicionPropiedad;

        //felipe.cadena:: se consideran los tramites de via gubernativa
        if (this.tramiteSeleccionado.isViaGubernativa()) {
            if (this.tramitesGubernativaAll != null && !this.tramitesGubernativaAll.isEmpty()) {
                Tramite tramiteOriginal = this.tramitesGubernativaAll.get(0);
                tipoTramite = tramiteOriginal.getTipoTramite();
                claseMutacion = tramiteOriginal.getClaseMutacion();
                subtipo = tramiteOriginal.getSubtipo();
            }
        } else {
            tipoTramite = this.tramiteSeleccionado.getTipoTramite();
            claseMutacion = this.tramiteSeleccionado.getClaseMutacion();
            subtipo = this.tramiteSeleccionado.getSubtipo();
        }

        if (EMutacion2Subtipo.ENGLOBE.getCodigo().equals(subtipo)) {
            for (TramitePredioEnglobe t : this.prediosTramite) {
                if (t.getPredio().getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END) ||
                    t.getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END)) {
                    condicionPropiedad = Constantes.FM;
                } else {
                    condicionPropiedad = t.getPredio().getCondicionPropiedad();
                }

                List<DeterminaTramitePH> dts = this.getTramiteService().
                    obtenerDeterminaTramitePHPorParametrosTramite(tipoTramite, claseMutacion,
                        subtipo, condicionPropiedad);

                if (dts != null && !dts.isEmpty()) {
                    DeterminaTramitePH dt = dts.get(0);
                    if (!dt.getCondicion().equals(Constantes.HABILITADO)) {
                        this.addMensajeError(ECodigoErrorVista.PREDIO_INHABILITADO_TRAMITE.
                            toString());
                        return false;
                    }
                }

            }
        } else if (this.tramiteSeleccionado.isEsRectificacion()) {
            List<DatoRectificar> trs = this.selectedDatosRectifComplDataTable;

            if (this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                Constantes.FM_CONDOMINIO_END) ||
                this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                    Constantes.FM_PH_END)) {
                condicionPropiedad = Constantes.FM;
                boolean datosInhabilitados = false;
                String params = "";
                for (DatoRectificar dr : trs) {
                    List<DeterminaTramitePH> dts = this.getTramiteService().
                        obtenerDeterminaTramitePHPorParametrosTramite(tipoTramite, claseMutacion,
                            String.valueOf(dr.getId()), condicionPropiedad);
                    if (dts != null && !dts.isEmpty()) {
                        DeterminaTramitePH dt = dts.get(0);
                        if (!dt.getCondicion().equals(Constantes.HABILITADO)) {
                            datosInhabilitados = true;
                            params += dr.getNombre() + ", ";
                        }
                    }
                }

                if (datosInhabilitados) {
                    this.addMensajeError(ECodigoErrorVista.PREDIO_INHABILITADO_TRAMITE_RECT.
                        getMensajeUsuario(params));
                    return false;
                }
            }
            
            if (this.tramiteSeleccionado.getPredio().getCondicionPropiedad().
                    equals(EPredioCondicionPropiedad.CP_9.getCodigo()) && 
                    this.tramiteSeleccionado.isRectificacionArea()){
                
                condicionPropiedad = EPredioCondicionPropiedad.CP_9.getCodigo();
                
                boolean datosInhabilitados = false;
                
                String params = "";
                for (DatoRectificar dr : trs) {
                    List<DeterminaTramitePH> dts = this.getTramiteService().
                            obtenerDeterminaTramitePHPorParametrosTramite(tipoTramite, claseMutacion,
                                    String.valueOf(dr.getId()), condicionPropiedad);
                    if (dts != null && !dts.isEmpty()) {
                        DeterminaTramitePH dt = dts.get(0);
                        if (!dt.getCondicion().equals(Constantes.HABILITADO)) {
                            datosInhabilitados = true;
                            params += dr.getNombre() + ", ";
                        }
                    }
                }

                if (datosInhabilitados) {
                    this.addMensajeError(ECodigoErrorVista.PREDIO_INHABILITADO_TRAMITE.
                            getMensajeUsuario(params));
                    return false;
                }
            }

        } else {
            if (this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                Constantes.FM_CONDOMINIO_END) ||
                this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                    Constantes.FM_PH_END)) {
                condicionPropiedad = Constantes.FM;
            } else {
                condicionPropiedad = this.tramiteSeleccionado.getPredio().getCondicionPropiedad();
            }

            List<DeterminaTramitePH> dts = this.getTramiteService().
                obtenerDeterminaTramitePHPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                    condicionPropiedad);

            if (dts != null && !dts.isEmpty()) {
                DeterminaTramitePH dt = dts.get(0);
                if (!dt.getCondicion().equals(Constantes.HABILITADO)) {
                    this.addMensajeError(ECodigoErrorVista.PREDIO_INHABILITADO_TRAMITE.toString());
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Determina si el tramite de quinta que se quiere radicar si es consistente con el predio sobre
     * el que se va a realizar.
     *
     * @author leidy.gonzalez
     * @return
     */
    public boolean validarTipoTramitePredioQuinta() {
        String tipoTramite = "";
        String claseMutacion = "";
        String subtipo = "";
        String condicionPropiedad;

        tipoTramite = this.tramiteSeleccionado.getTipoTramite();
        claseMutacion = this.tramiteSeleccionado.getClaseMutacion();
        subtipo = this.tramiteSeleccionado.getSubtipo();

        assembleNumeroPredialFromSegments();

        if (this.numeroPredial != null && !this.numeroPredial.isEmpty() &&
            this.numeroPredialParte9 != null && !this.numeroPredialParte9.isEmpty()) {
            if (this.numeroPredial.endsWith(Constantes.FM_CONDOMINIO_END) ||
                this.numeroPredial.endsWith(Constantes.FM_PH_END)) {
                condicionPropiedad = Constantes.FM;
            } else {
                condicionPropiedad = this.numeroPredialParte9;
            }

            List<DeterminaTramitePH> dts = this.getTramiteService().
                obtenerDeterminaTramitePHPorParametrosTramite(tipoTramite, claseMutacion, subtipo,
                    condicionPropiedad);

            if (dts != null && !dts.isEmpty()) {
                DeterminaTramitePH dt = dts.get(0);
                if (!dt.getCondicion().equals(Constantes.HABILITADO)) {
                    this.addMensajeError(ECodigoErrorVista.PREDIO_INHABILITADO_TRAMITE.toString());
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Determina si los predios seleccionados tienen tramite en ejcucion
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarTramitesPredios() {

        if (EMutacion2Subtipo.ENGLOBE.getCodigo().equals(this.tramiteSeleccionado.getSubtipo())) {
            for (TramitePredioEnglobe t : this.prediosTramite) {
                if (t.getPredio().getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END) ||
                    t.getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END)) {
                    List<Tramite> tramites = this.getTramiteService().
                        buscarTramitesPendientesDePredio(t.getPredio().getId());
                    if (tramites != null && !tramites.isEmpty()) {
                        this.addMensajeWarn(ECodigoErrorVista.PREDIO_CON_TRAMITES_EJECUCION.
                            getMensajeUsuario(t.getPredio().getNumeroPredial()));
                        return false;
                    }

                    List<Predio> prediosFM = this.getConservacionService().
                        buscarPrediosPorRaizNumPredial(t.getPredio().getNumeroPredial().substring(0,
                            21));
                    for (Predio predio : prediosFM) {
                        tramites = this.getTramiteService().buscarTramitesPendientesDePredio(predio.
                            getId());
                        if (tramites != null && !tramites.isEmpty()) {
                            this.addMensajeWarn(ECodigoErrorVista.PREDIO_CON_TRAMITES_EJECUCION.
                                getMensajeUsuario(predio.getNumeroPredial()));
                            return false;
                        }
                    }
                }
            }
        } else {
            if (this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                Constantes.FM_CONDOMINIO_END) ||
                this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                    Constantes.FM_PH_END)) {
                List<Tramite> tramites = this.getTramiteService().buscarTramitesPendientesDePredio(
                    this.tramiteSeleccionado.getPredio().getId());
                if (tramites != null && !tramites.isEmpty()) {
                    this.addMensajeWarn(ECodigoErrorVista.PREDIO_CON_TRAMITES_EJECUCION.
                        getMensajeUsuario(this.tramiteSeleccionado.getPredio().getNumeroPredial()));
                    return false;
                }

                List<Predio> prediosFM = this.getConservacionService().
                    buscarPrediosPorRaizNumPredial(this.tramiteSeleccionado.getPredio().
                        getNumeroPredial().substring(0, 21));
                for (Predio predio : prediosFM) {
                    tramites = this.getTramiteService().buscarTramitesPendientesDePredio(predio.
                        getId());
                    if (tramites != null && !tramites.isEmpty()) {
                        this.addMensajeWarn(ECodigoErrorVista.PREDIO_CON_TRAMITES_EJECUCION.
                            getMensajeUsuario(predio.getNumeroPredial()));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Si el tramite tiene relacionado un predio ficha matriz, se agregan todos los predios
     * relacionados a esta ficha.
     *
     * @author felipe.cadena
     */
    public void agregarPrediosFM() {

        List<TramitePredioEnglobe> prediosAsociadosFM = new ArrayList<TramitePredioEnglobe>();
        if (EMutacion2Subtipo.ENGLOBE.getCodigo().equals(this.tramiteSeleccionado.getSubtipo())) {

            for (TramitePredioEnglobe t : this.prediosTramite) {
                if (t.getPredio().getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END) ||
                    t.getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END)) {

                    //se marca como englobe ph
                    this.tramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.ENGLOBE_PH.getCodigo());

                    List<Predio> prediosFM = this.getConservacionService().
                        buscarPrediosPorRaizNumPredial(t.getPredio().getNumeroPredial().substring(0,
                            21));
                    for (Predio predio : prediosFM) {

                        //@modified by leidy.gonzalez::#20927:: Se valida que no se duplique el predio ficha matriz,
                        //que se agrega a la lista de predios que se engloban.
                        if (t.getPredio().getId() != null && predio.getId() != null &&
                            !t.getPredio().getId().equals(predio.getId())) {

                            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                            tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                            tpe.setPredio(predio);
                            tpe.setTramite(this.tramiteSeleccionado);
                            tpe.setFechaLog(new Date());
                            tpe.setUsuarioLog(this.usuario.getLogin());
                            prediosAsociadosFM.add(tpe);

                        }
                    }
                }
            }
        } else {
            if (this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                Constantes.FM_CONDOMINIO_END) ||
                this.tramiteSeleccionado.getPredio().getNumeroPredial().endsWith(
                    Constantes.FM_PH_END)) {

                if (this.tramiteSeleccionado.isSegundaDesenglobe()) {
                    //se marca como desenglobe por etapas
                    this.tramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.POR_ETAPAS.getCodigo());
                } else if (this.tramiteSeleccionado.isRectificacion()) {
                    //se marca como rectificacion matriz
                    this.tramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.RECTIFICACION_MATRIZ.getCodigo());
                }

                if (this.tramiteSeleccionado.isTercera()) {
                    //se marca como tercera PH Condominio Másiva
                    this.tramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.TERCERA_MASIVA.getCodigo());
                }

                if (this.tramiteSeleccionado.isQuinta()) {
                    //se marca como tercera PH Condominio Másiva
                    this.tramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.QUINTA_MASIVO.getCodigo());
                }
                //@modified by leidy.gonzalez :: #20420:: Agrega los predios FM, para los tramites de Cancelacion Masiva
                if (this.tramiteSeleccionado.isCancelacionPredio()) {
                    //se marca como cancelacion PH Condominio Másiva
                    this.tramiteSeleccionado.setRadicacionEspecial(
                        ETramiteRadicacionEspecial.CANCELACION_MASIVA.getCodigo());
                }

                List<Predio> prediosFM = this.getConservacionService().
                    buscarPrediosPorRaizNumPredial(this.tramiteSeleccionado.getPredio().
                        getNumeroPredial().substring(0, 22));
                for (Predio predio : prediosFM) {
                    //No se agregan los predios de mejoras o los que estan cancelados.
                    if (predio.isMejora() || EPredioEstado.CANCELADO.getCodigo().equals(predio.
                        getEstado())) {
                        continue;
                    }
                    TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                    tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                    tpe.setPredio(predio);
                    tpe.setTramite(this.tramiteSeleccionado);
                    tpe.setFechaLog(new Date());
                    tpe.setUsuarioLog(this.usuario.getLogin());
                    prediosAsociadosFM.add(tpe);
                }
            }
        }

        if (this.tramiteSeleccionado.getTramitePredioEnglobes() == null) {
            this.tramiteSeleccionado.setTramitePredioEnglobes(prediosAsociadosFM);
        } else {
            this.tramiteSeleccionado.getTramitePredioEnglobes().addAll(prediosAsociadosFM);
        }
    }

    /**
     * Elimina los documentos que se relacionan temporalmente al tramite, cuando la acción no es
     * confirmada por el usuario.
     *
     * @author dumar.penuela
     */
    public void borrardocumentacionTemporalTramite() {
        try {

            if (!this.documentosDelTramiteTemporales.isEmpty()) {
                this.getTramiteService().eliminarDeTramiteDocumentoDocumentosAsociado(
                    this.documentosDelTramiteTemporales,
                    this.tramiteDocumentacionTemporales,
                    this.solicitudDocumentacionTemporales
                );

                this.documentosDelTramiteTemporales = new ArrayList<Documento>();
                this.tramiteDocumentacionTemporales = new ArrayList<TramiteDocumentacion>();
                this.solicitudDocumentacionTemporales = new ArrayList<SolicitudDocumentacion>();

            }
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al borrar los documentos temporales" +
                e.getMessage());
        }
    }

    /**
     * Método que realiza el set de solicitantes de una lista de solicitantes que coinciden con su
     * mismo número de identificación
     */
    public void aceptarSeleccionCoincidentes() {

        if (this.solicitanteCoincidenteSeleccionado != null) {
            this.paisSolicitante = this.solicitanteCoincidenteSeleccionado
                .getDireccionPais();
            this.updateDepartamentosSolicitante();
            this.departamentoSolicitante = this.solicitanteCoincidenteSeleccionado
                .getDireccionDepartamento();
            this.updateMunicipiosSolicitante();
            this.municipioSolicitante = this.solicitanteCoincidenteSeleccionado
                .getDireccionMunicipio();
            this.solicitanteSeleccionado = this.solicitanteCoincidenteSeleccionado;
            this.solicitanteSeleccionado
                .setRelacion(ESolicitanteSolicitudRelac.PROPIETARIO
                    .toString());

            // Ref:# 15956 :: david.cifuentes :: Se setea un atributo transient que determina al momento de pasar
            // los parámetros, el envio del ID como tipo y número de
            // identificación
            this.solicitanteSeleccionado.setSolicitanteCoincidente(true);
        }
    }

    public void assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();
        if (this.numeroPredialParte1 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte1);
        }
        if (this.numeroPredialParte2 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte2);
        }
        if (this.numeroPredialParte3 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte3);
        }
        if (this.numeroPredialParte4 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte4);
        }
        if (this.numeroPredialParte5 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte5);
        }
        if (this.numeroPredialParte6 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte6);
        }
        if (this.numeroPredialParte7 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte7);
        }
        if (this.numeroPredialParte8 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte8);
        }
        if (this.numeroPredialParte9 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte9);
        }
        if (this.numeroPredialParte10 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte10);
        }
        if (this.numeroPredialParte11 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte11);
        }
        if (this.numeroPredialParte12 == null) {
            numeroPredialAssembled.append("");
        } else {
            numeroPredialAssembled.append(this.numeroPredialParte12);
        }
        this.numeroPredial = numeroPredialAssembled.toString();
    }

    /*
     * @modified by leidy.gonzalez :: #13244:: 11/08/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarReporteAvisoRegistroRechazo() {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.AVISOS_REGISTRO_DESCARTADOS;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_SOLICITUD",
            this.solicitudSeleccionada.getNumero());

        LOGGER.debug("Inicia ejecución servicio de reportes...");

        this.reporteAvisoRegistroRechazo = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        if (this.reporteAvisoRegistroRechazo == null) {
            if (!this.solicitudSeleccionada.isSolicitudProductosCatastrales()) {
                this.addMensajeWarn("El reporte de aviso de registro de rechazo" +
                     " no se pudo generar en el momento, por favor intente más tarde.");
            } else {
                this.addMensajeError(Constantes.MENSAJE_ERROR_REPORTE);
            }
        }
    }

//end of class    
}
