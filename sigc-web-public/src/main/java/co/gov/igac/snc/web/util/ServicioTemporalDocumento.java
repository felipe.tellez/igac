package co.gov.igac.snc.web.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import co.gov.igac.snc.util.Constantes;

/**
 *
 * @author juan.agudelo
 *
 */
public class ServicioTemporalDocumento implements IServicioTemporalDocumento, Serializable {

    private static Log LOGGER = LogFactory.getLog(ServicioTemporalDocumento.class);

    /**
     * @see IServicioTemporalDocumento#nombreInicialDocumento(String, String, String, String)
     * @author juan.agudelo
     */
    @Override
    public String nombreInicialDocumento(String idTemporal, String tipoDocId,
        String codN, String extension) {
        LOGGER.debug("ServicioTemporalDocumento#nombreInicialDocumento");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        String anio = calendar.get(Calendar.YEAR) + "";
        String mesTemp = (calendar.get(Calendar.MONTH) + 1) + "";
        String mes = org.apache.commons.lang.StringUtils.leftPad(mesTemp, 2,
            "0");
        String diaTemp = calendar.get(Calendar.DAY_OF_MONTH) + "";
        String dia = org.apache.commons.lang.StringUtils.leftPad(diaTemp, 2,
            "0");

        StringBuilder nombreTemporalDocumento = new StringBuilder();

        // Fecha de carga del documento
        nombreTemporalDocumento.append(anio + mes + dia);
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // Identificador temporal de trámite, predio o solicitud
        if (idTemporal != null && !idTemporal.isEmpty()) {
            String idTemp = org.apache.commons.lang.StringUtils.leftPad(
                idTemporal, 10, "0");
            nombreTemporalDocumento.append(idTemp);
        } else {
            nombreTemporalDocumento.append("0000000000");
        }
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // Tipo de documento ID
        if (tipoDocId != null && !tipoDocId.isEmpty()) {
            String idTipoDocTemp = org.apache.commons.lang.StringUtils.leftPad(
                tipoDocId, 5, "0");
            nombreTemporalDocumento.append(idTipoDocTemp);
        } else {
            nombreTemporalDocumento.append("00000");
        }
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // Código N solo aplica para Fótografias y corresponde al identificador
        // de la unidad de construcción
        if (codN != null && !codN.isEmpty()) {
            String codUC = org.apache.commons.lang.StringUtils.leftPad(codN, 3,
                "0");
            nombreTemporalDocumento.append(codUC);
        } else {
            nombreTemporalDocumento.append("000");
        }
        // Separador
        nombreTemporalDocumento
            .append(Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE);
        // ID del documento en la base de datos, como no existe el documento el
        // la DB sera una cadena de ceros
        Random randomId = new Random();
        String randomIdTemp = (randomId.nextInt() + "").substring(0, 10);
        String docId = org.apache.commons.lang.StringUtils.leftPad(
            randomIdTemp, 10, "0");
        nombreTemporalDocumento.append(docId);
        // punto de separación del nombre y la extensión del archivo
        nombreTemporalDocumento.append(".");
        // extension del documento
        if (extension != null && !extension.isEmpty()) {
            nombreTemporalDocumento.append(extension);
        } else {
            nombreTemporalDocumento.append("xxx");
        }
        LOGGER.debug("Nombre: " + nombreTemporalDocumento.toString());
        return nombreTemporalDocumento.toString();
    }

}
