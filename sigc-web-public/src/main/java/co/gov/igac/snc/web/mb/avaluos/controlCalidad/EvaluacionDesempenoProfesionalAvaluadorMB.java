package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacionDetalle;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EAvaluoEvaluacionDetalles;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.EVariableEvaluacionAvaluoTipo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del CU-SA-AC-024 Evaluar Desempeño Profesional del Avaluador
 *
 * @author rodrigo.hernandez
 * @cu CU-SA-AC-024
 */
@Component("evaluarDesempenoProfesionalAvaluador")
@Scope("session")
public class EvaluacionDesempenoProfesionalAvaluadorMB extends
    EvaluacionDesempenoTerritorialYControlCalidadMB {

    /**
     *
     */
    private static final long serialVersionUID = 2152471045674945582L;

    /*
     * ------------ DATOS DE LA EVALUACION
     */
    /**
     * Lista de los detalles de EFICIENCIA de la evaluación del avalúo seleccionado
     */
    private List<AvaluoEvaluacionDetalle> listaEvaluacionDetallesEficiencia;

    /**
     * Orden de practica del avaluo seleccionado
     */
    private OrdenPractica ordenPractica;

    /**
     * Variable que almacena la suma parcial de puntajes de los detalles de EFICIENCIA
     */
    protected Double sumaParcialEficiencia;

    @Autowired
    private GeneralMB generalMB;

    // --------------------------------Reportes---------------------------------
    /**
     * Documento donde se almacena el comunicado generado
     */
    private Documento documentoAdjunto;

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /*
     * -------------- SETTERS Y GETTERS--------------------------------
     */
    public OrdenPractica getOrdenPractica() {
        return ordenPractica;
    }

    public void setOrdenPractica(OrdenPractica ordenPractica) {
        this.ordenPractica = ordenPractica;
    }

    public List<AvaluoEvaluacionDetalle> getListaEvaluacionDetallesEficiencia() {
        return listaEvaluacionDetallesEficiencia;
    }

    public void setListaEvaluacionDetallesEficiencia(
        List<AvaluoEvaluacionDetalle> listaEvaluacionDetallesEficiencia) {
        this.listaEvaluacionDetallesEficiencia = listaEvaluacionDetallesEficiencia;
    }

    public Double getSumaParcialEficiencia() {
        return sumaParcialEficiencia;
    }

    // -------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#init");

        this.actividadAsignacion = EAvaluoAsignacionProfActi.AVALUAR
            .getCodigo();

        this.iniciarDatos();

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#init");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#iniciarDatos");

        this.iniciarDatosEvaluacion();
        this.iniciarBanderasGeneral();
        this.iniciarListas();
        this.iniciarSumasPuntajes();
        this.consultarAsignacionAvaluoGeneral();
        this.consultarEvaluacionAvaluoGeneral();
        this.consultarTodosDetallesEvaluacionGeneral();

        this.cargarListasDetallesEvaluacionCalidadGeneral();
        this.cargarListasDetallesEvaluacionEficaciaGeneral();
        this.cargarListaDetallesEvaluacionEficiencia();

        calcularSumasParciales();

        this.calcularTotalGeneral();

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar los datos de la evaluacion
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatosEvaluacion() {
        super.iniciarDatosEvaluacionGeneral();
        this.ordenPractica = this.getAvaluosService()
            .consultarOrdenPracticaPorAvaluoId(
                this.avaluoSeleccionado.getId());
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar las listas
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListas() {
        super.iniciarListasGeneral();
        this.listaEvaluacionDetallesEficiencia = new ArrayList<AvaluoEvaluacionDetalle>();
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar las sumas Parcial de detalles de Calidad y Total de detalles de la
     * evaluacion
     *
     * @author rodrigo.hernandez
     */
    private void iniciarSumasPuntajes() {
        super.iniciarSumasPuntajesGeneral();
        this.sumaParcialEficiencia = 0D;
    }

    public void cargarListaDetallesEvaluacionEficiencia() {

        int numeroDevoluciones = 0;
        String nombreVariableEficiencia = "";

        String actividad = "";

        /*
         * Se define la actividad
         */
        if (this.banderaEsAvaluoDeTerritorial) {
            actividad = EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo();
        } else {
            actividad = EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo();
        }

        /*
         * Se consulta el numero de devoluciones
         */
        numeroDevoluciones = Integer.parseInt(String.valueOf(this
            .getAvaluosService().calcularDevolucionesAvaluo(
                this.avaluoSeleccionado.getId(),
                this.asignacionAvaluoSeleccionado.getId(), actividad)));

        /*
         * Se establece el nombre de la variable de EFICIENCIA a la que se le va a asignar el
         * puntaje
         */
        if (numeroDevoluciones == EAvaluoEvaluacionDetalles.SIN_DEVOLUCIONES
            .getValorDetalle()) {
            nombreVariableEficiencia = EAvaluoEvaluacionDetalles.SIN_DEVOLUCIONES
                .getNombreDetalle();

        } else if (numeroDevoluciones == EAvaluoEvaluacionDetalles.UNA_DEVOLUCION
            .getValorDetalle()) {
            nombreVariableEficiencia = EAvaluoEvaluacionDetalles.UNA_DEVOLUCION
                .getNombreDetalle();

        } else if (numeroDevoluciones == EAvaluoEvaluacionDetalles.DOS_DEVOLUCIONES
            .getValorDetalle()) {
            nombreVariableEficiencia = EAvaluoEvaluacionDetalles.DOS_DEVOLUCIONES
                .getNombreDetalle();

        } else if (numeroDevoluciones == EAvaluoEvaluacionDetalles.SIN_RESPUESTA_NO_APROBACION
            .getValorDetalle()) {
            nombreVariableEficiencia = EAvaluoEvaluacionDetalles.SIN_RESPUESTA_NO_APROBACION
                .getNombreDetalle();
        }
        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetalles) {
            if (aed.getVariableEvaluacionAvaluo()
                .getTipo()
                .equals(EVariableEvaluacionAvaluoTipo.EFICIENCIA.getValor())) {

                if (aed.getVariableEvaluacionAvaluo().getNombre()
                    .equals(nombreVariableEficiencia)) {
                    aed.setValor(aed.getVariableEvaluacionAvaluo()
                        .getValorMinimo());
                }

                this.listaEvaluacionDetallesEficiencia.add(aed);
            }
        }
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para calcular la suma parcial de los detalles de EFICIENCIA de la evaluación
     *
     * @author rodrigo.hernandez
     */
    private void calcularSumaParcialEficiencia() {
        LOGGER.debug(
            "Inicio EvaluacionDesempenoProfesionalAvaluadorMB#calcularSumaParcialEficiencia");

        this.sumaParcialEficiencia = 0D;

        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetallesEficiencia) {
            this.sumaParcialEficiencia = this.sumaParcialEficiencia +
                 aed.getValor();
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#calcularSumaParcialEficiencia");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Llama a los métodos para calcular las sumas parciales de los atributos de calidad, eficiencia
     * y eficacia
     *
     * @author christian.rodriguez
     */
    private void calcularSumasParciales() {
        this.calcularSumasParcialesGeneral();
        this.calcularSumaParcialEficiencia();
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método que se ejecuta cuando se hace click en el boton <b>Aceptar</b>
     *
     * @author rodrigo.hernandez
     */
    public void ejecutarEvaluacion() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#ejecutarEvaluacion");

        boolean banderaEsListaDetallesValidos = false;

        banderaEsListaDetallesValidos = this.validarDetallesEvaluacion();

        if (banderaEsListaDetallesValidos) {
            /*
             * Se calculan las sumas parciales de los detalles de la evaluación
             */
            this.calcularSumasParcialesGeneral();

            /*
             * Se calcula el total de los detalles de la evaluación
             */
            this.calcularTotalGeneral();

            /*
             * Se guarda/actualiza la evaluacion
             */
            this.guardarEvaluacion();
            this.generarDocumentoEvaluacion();
            this.crearDocumento();
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#ejecutarEvaluacion");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para crear documento(reporte) de la evaluacion </br>
     *
     * @author rodrigo.hernandez
     */
    private void generarDocumentoEvaluacion() {
        LOGGER.debug(
            "iniciando EvaluacionDesempenoProfesionalAvaluadorMB#generarDocumentoEvaluacion");

        // TODO::rodrigo.hernandez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        /*
         * Se crea mapa de parametros para enviarlo al servicio que genera el reporte
         */
        Map<String, String> parameters = new HashMap<String, String>();

        // TODO :: rodrigo.hernandez :: Parametros quemados, reemplazar cuando
        // se haya desarrollado el reporte :: 30/10/12
        this.reporteDTO = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        LOGGER.debug(
            "finalizando EvaluacionDesempenoProfesionalAvaluadorMB#generarDocumentoEvaluacion");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar documento generado para vista previa
     *
     * @author rodrigo.hernandez
     */
    public void verVistaPrevia() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#verVistaPrevia");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        previsualizacionDocMB.setReporteDTO(this.reporteDTO);

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#verVistaPrevia");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para enviar el oficio por correo a los avaluadores
     *
     * @author rodrigo.hernandez
     */
    private void enviarOficio() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#enviarOficio");

        List<SelectItem> listaItemsTipoTramite = this.generalMB
            .getAvaluosComercialesTipoTramite();
        List<String> destinatarios = new ArrayList<String>();
        boolean notificacionExitosa = false;

        /*
         * Se define el nombre del archivo de la carpeta donde consultar el archivo del comunicado
         * creado
         */
        String nombreArchivoCarpetaTemporal = this.reporteDTO
            .getArchivoReporte().getAbsolutePath();

        /*
         * Se inician los datos que son constantes en el cuerpo del correo
         */
        Object[] datosCorreo = null;

        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};

        if (this.documentoAdjunto != null) {
            adjuntos[0] = nombreArchivoCarpetaTemporal;
            adjuntosNombres[0] = "Reporte control calidad de territorial.pdf";
        }
        LOGGER.debug("Url archivo " + adjuntos[0]);

        /*
         * Se envía el correo tantas veces como numero de avaluadores tenga el avaluo
         */
        for (ProfesionalAvaluo pa : this.avaluoSeleccionado.getAvaluadores()) {

            /*
             * Se crea la lista de destinatarios
             */
            destinatarios = new ArrayList<String>();
            destinatarios.add(pa.getCorreoElectronico());

            /*
             * Se valida si el avaluador tiene interventor asociado para añadir a este en la lista
             * de destinatarios
             */
            if (pa.getInterventor() != null) {
                destinatarios.add(pa.getInterventor().getCorreoElectronico());
            }

            datosCorreo = new Object[6];

            /*
             * Se ajustan los datos del cuerpo del correo
             */
            datosCorreo[0] = pa.getNombreCompleto();

            /*
             * Se halla el nombre del tipo de tramite
             */
            for (SelectItem si : listaItemsTipoTramite) {
                if (si.getValue().equals(
                    this.avaluoSeleccionado.getTramite().getSolicitud()
                        .getTipo())) {
                    datosCorreo[1] = si.getLabel();
                    break;
                }
            }

            datosCorreo[2] = this.avaluoSeleccionado.getSecRadicado();
            datosCorreo[3] = this.avaluoSeleccionado.getSolicitante()
                .getNombreCompleto();
            datosCorreo[4] = this.profesionalControlCalidad != null ? this.profesionalControlCalidad
                .getNombreCompleto() : "";

            notificacionExitosa = this
                .getGeneralesService()
                .enviarCorreoElectronicoConCuerpo(
                    EPlantilla.MENSAJE_EVALUACION_DESEMPENO_PROFESIONAL_AVALUADOR,
                    destinatarios, null, null, datosCorreo, adjuntos,
                    adjuntosNombres);
        }

        if (notificacionExitosa) {
            this.addMensajeInfo("Se envio exitosamente el reporte por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError(
                "No se pudo enviar la evaluación de desempeño profesional del avaluador" +
                 " por correo electrónico para uno o varios avaluadores");
            context.addCallbackParam("error", "error");
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#enviarOficio");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Metodo para crear documento adjunto
     *
     * @author rodrigo.hernandez
     */
    private void crearDocumento() {
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.OTRO.getId());
        tipoDocumento.setNombre(ETipoDocumento.OTRO.getNombre());

        this.documentoAdjunto = new Documento();
        this.documentoAdjunto.setArchivo(this.reporteDTO.getArchivoReporte()
            .getPath());
        this.documentoAdjunto.setTipoDocumento(tipoDocumento);
        this.documentoAdjunto.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.documentoAdjunto.setBloqueado(ESiNo.NO.getCodigo());
        this.documentoAdjunto.setUsuarioCreador(this.usuario.getLogin());
        this.documentoAdjunto.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        this.documentoAdjunto.setFechaLog(new Date(System.currentTimeMillis()));
        this.documentoAdjunto.setUsuarioLog(this.usuario.getLogin());
        this.documentoAdjunto
            .setDescripcion("Evaluación desempeño profesional avaluador");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para almacenar el documento en BD y Alfresco
     *
     * @author rodrigo.hernandez
     */
    private void almacenarDocumento() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#generarDocumento");

        /*
         * Se almacena el documento en alfresco
         */
        this.guardarDocumentoAlfresco(this.documentoAdjunto.getTipoDocumento());

        /*
         * Se almacena el documento en la BD
         */
        this.documentoAdjunto = this.getGeneralesService().guardarDocumento(
            this.documentoAdjunto);

        /*
         * Se asocia el documento a la evaluación
         */
        this.evaluacionAvaluoSeleccionado.setDocumento(this.documentoAdjunto);

        /*
         * Se actualiza la evaluación
         */
        this.evaluacionAvaluoSeleccionado = this.getAvaluosService()
            .guardarActualizarAvaluoEvaluacion(
                this.evaluacionAvaluoSeleccionado, this.usuario);

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#generarDocumento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para guardar el documento generado en alfresco
     *
     * @author rodrigo.hernandez
     */
    private void guardarDocumentoAlfresco(TipoDocumento tipoDocumento) {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#guardarDocumentoAlfresco");

        Solicitud solTemp = this.avaluoSeleccionado.getTramite().getSolicitud();

        String urlArchivo = this.reporteDTO.getArchivoReporte()
            .getAbsolutePath();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(
                solTemp.getNumero(), solTemp.getFecha(),
                tipoDocumento.getNombre(), urlArchivo);

        this.documentoAdjunto = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, this.documentoAdjunto);

        if (this.documentoAdjunto == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#guardarDocumentoAlfresco");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para mover proceso. </br> Llamado desde la pagina
     * evaluarDesempenoProfesionalAvaluo.xhtml</br> Boton Mover Proceso
     *
     * @author rodrigo.hernandez
     */
    public void moverProceso() {
        /*
         * Se almacena el documento de la evaluacion
         */
        this.almacenarDocumento();

        /*
         * Se envía el correo a los avaluadores e interventores
         */
        this.enviarOficio();

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cerrar página principal asociada
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

}
