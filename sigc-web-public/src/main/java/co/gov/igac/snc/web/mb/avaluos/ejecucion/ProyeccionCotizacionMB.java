package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoCapitulo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoRequisitoDocumentacion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed Bean para CU-SA-AC-100 Proyectar cotizacion
 *
 * @author rodrigo.hernandez
 *
 */
@Component("proyectarCotizacionAvaluo")
@Scope("session")
public class ProyeccionCotizacionMB extends SNCManagedBean {

    private static final long serialVersionUID = 3289028499722995524L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProyeccionCotizacionMB.class);

    private Avaluo avaluo;

    private AvaluoCapitulo nuevoCapitulo;

    private List<AvaluoCapitulo> listaCapitulos;

    private AvaluoCapitulo[] listaCapitulosSeleccionados;

    private AvaluoRequisitoDocumentacion docRequisitoSeleccionado;

    private List<AvaluoRequisitoDocumentacion> listaDocumentosSeleccionados;

    private List<TipoDocumento> listaDocumentacionASolicitar;

    private TipoDocumento tipoDocumentoSeleccionado;

    private SolicitudDocumento solicitudDocumento;

    /**
     * usuario actual del sistema
     */
    private UsuarioDTO usuario;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    private ReporteDTO reporteDTO;

    /*
     * Banderas
     */
    private boolean banderaOficioGenerado;

    private boolean banderaEsAdicionCapitulo;

    private boolean banderaGeneraContrato;

    /*
     * Miscellaneous
     */
    private Date fechaDocumento;

    private EstructuraOrganizacional territorialAvaluo;

    private Double totalCotizacion;

    private String foliosAnexos;

    /**
     * Director de la territorial donde se realiza el tramite
     */
    private UsuarioDTO directorTerritorial;

    /**
     * Subdirector de catastro
     */
    private UsuarioDTO subdirectorCatastro;

    /*
     * ----- SETTERS Y GETTERS --------------------
     */
    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    public List<AvaluoCapitulo> getListaCapitulos() {
        return listaCapitulos;
    }

    public void setListaCapitulos(List<AvaluoCapitulo> listaCapitulos) {
        this.listaCapitulos = listaCapitulos;
    }

    public List<TipoDocumento> getListaDocumentacionASolicitar() {
        return listaDocumentacionASolicitar;
    }

    public void setListaDocumentacionASolicitar(
        List<TipoDocumento> listaDocumentacionASolicitar) {
        this.listaDocumentacionASolicitar = listaDocumentacionASolicitar;
    }

    public List<AvaluoRequisitoDocumentacion> getListaDocumentosSeleccionados() {
        return listaDocumentosSeleccionados;
    }

    public void setListaDocumentosSeleccionados(
        List<AvaluoRequisitoDocumentacion> listaDocumentosSeleccionados) {
        this.listaDocumentosSeleccionados = listaDocumentosSeleccionados;
    }

    public TipoDocumento getTipoDocumentoSeleccionado() {
        return tipoDocumentoSeleccionado;
    }

    public void setTipoDocumentoSeleccionado(
        TipoDocumento tipoDocumentoSeleccionado) {
        this.tipoDocumentoSeleccionado = tipoDocumentoSeleccionado;
    }

    public boolean isBanderaOficioGenerado() {
        return banderaOficioGenerado;
    }

    public void setBanderaOficioGenerado(boolean banderaOficioGenerado) {
        this.banderaOficioGenerado = banderaOficioGenerado;
    }

    public boolean isBanderaEsAdicionCapitulo() {
        return banderaEsAdicionCapitulo;
    }

    public void setBanderaEsAdicionCapitulo(boolean banderaEsAdicionCapitulo) {
        this.banderaEsAdicionCapitulo = banderaEsAdicionCapitulo;
    }

    public SolicitudDocumento getSolicitudDocumento() {
        return solicitudDocumento;
    }

    public void setSolicitudDocumento(SolicitudDocumento solicitudDocumento) {
        this.solicitudDocumento = solicitudDocumento;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public AvaluoCapitulo getNuevoCapitulo() {
        return nuevoCapitulo;
    }

    public void setNuevoCapitulo(AvaluoCapitulo nuevoCapitulo) {
        this.nuevoCapitulo = nuevoCapitulo;
    }

    public AvaluoRequisitoDocumentacion getDocRequisitoSeleccionado() {
        return docRequisitoSeleccionado;
    }

    public void setDocRequisitoSeleccionado(
        AvaluoRequisitoDocumentacion docRequisitoSeleccionado) {
        this.docRequisitoSeleccionado = docRequisitoSeleccionado;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public AvaluoCapitulo[] getListaCapitulosSeleccionados() {
        return listaCapitulosSeleccionados;
    }

    public void setListaCapitulosSeleccionados(
        AvaluoCapitulo[] listaCapitulosSeleccionados) {
        this.listaCapitulosSeleccionados = listaCapitulosSeleccionados;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public EstructuraOrganizacional getTerritorialAvaluo() {
        return territorialAvaluo;
    }

    public void setTerritorialAvaluo(EstructuraOrganizacional territorialAvaluo) {
        this.territorialAvaluo = territorialAvaluo;
    }

    public Double getTotalCotizacion() {
        return totalCotizacion;
    }

    public void setTotalCotizacion(Double totalCotizacion) {
        this.totalCotizacion = totalCotizacion;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return directorTerritorial;
    }

    public void setDirectorTerritorial(UsuarioDTO directorTerritorial) {
        this.directorTerritorial = directorTerritorial;
    }

    public UsuarioDTO getSubdirectorCatastro() {
        return subdirectorCatastro;
    }

    public void setSubdirectorCatastro(UsuarioDTO subdirectorCatastro) {
        this.subdirectorCatastro = subdirectorCatastro;
    }

    public boolean isBanderaGeneraContrato() {
        boolean result = false;

        if (this.avaluo.isRealizarContrato() != null) {

            if (this.avaluo.isRealizarContrato().equals(ESiNo.SI.getCodigo())) {
                result = true;
            }
        }

        return result;
    }

    public void setBanderaGeneraContrato(boolean banderaGeneraContrato) {
        this.banderaGeneraContrato = banderaGeneraContrato;
    }

    public String getFoliosAnexos() {
        return foliosAnexos;
    }

    public void setFoliosAnexos(String foliosAnexos) {
        this.foliosAnexos = foliosAnexos;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin ProyeccionCotizacionMB#init");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(
            172L);

        this.listaDocumentosSeleccionados = new ArrayList<AvaluoRequisitoDocumentacion>();
        this.tipoDocumentoSeleccionado = new TipoDocumento();
        this.listaCapitulos = new ArrayList<AvaluoCapitulo>();

        this.fechaDocumento = Calendar.getInstance().getTime();

        this.territorialAvaluo = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                String.valueOf(this.avaluo
                    .getEstructuraOrganizacionalCod()));

        this.cargarDirector();
        this.cargarSubdirectorCatastro();
        this.calcularTotalCotizacion();
        this.cargarCapitulos();
        this.cargarDocsRequisito();
        this.crearListaDocumentosASolicitar();

        LOGGER.debug("Fin ProyeccionCotizacionMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar datos del Director de Territorial
     *
     * @author rodrigo.hernandez
     */
    private void cargarDirector() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#cargarDirectorTerritorial");

        List<UsuarioDTO> directoresTerritorial = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.territorialAvaluo.getNombre(),
                ERol.DIRECTOR_TERRITORIAL);

        if (!directoresTerritorial.isEmpty()) {
            this.directorTerritorial = directoresTerritorial.get(0);
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#cargarDirectorTerritorial");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar datos del Subdirector de catastro
     *
     * @author rodrigo.hernandez
     */
    private void cargarSubdirectorCatastro() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#cargarSubdirectorCatastro");

        List<UsuarioDTO> subdirectores = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.territorialAvaluo.getNombre(),
                ERol.SUBDIRECTOR_CATASTRO);

        if (!subdirectores.isEmpty()) {
            this.subdirectorCatastro = subdirectores.get(0);
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#cargarSubdirectorCatastro");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para calcular el valor total de la cotizacion
     *
     * @author rodrigo.hernandez
     */
    private void calcularTotalCotizacion() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#calcularTotalCotizacion");

        this.totalCotizacion = 0D;

        for (AvaluoPredio predio : this.avaluo.getAvaluoPredios()) {
            this.totalCotizacion = this.totalCotizacion +
                 predio.getValorCotizacion();
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#calcularTotalCotizacion");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para armar la tabla con la informacion de capitulos en la pagina
     * proyectarCotizacion.xhtml
     *
     * @author rodrigo.hernandez
     */
    private void cargarCapitulos() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#cargarCapitulos");

        int contador = 0;

        List<AvaluoCapitulo> listaCapitulosDefault = this.getGeneralesService()
            .obtenerCapitulosAvaluo();

        List<AvaluoCapitulo> listaCapitulosAvaluo = this.getAvaluosService()
            .obtenerCapitulosPorAvaluoId(this.avaluo.getId());

        List<String> listaCodigosCapDefault = new ArrayList<String>();

        /*
         * Se completan los datos faltantes de los capitulos por default
         */
        for (AvaluoCapitulo capitulo : listaCapitulosDefault) {
            capitulo.setAvaluoId(this.avaluo.getId());
            capitulo.setUsuarioLog(this.usuario.getLogin());
            capitulo.setFechaLog(Calendar.getInstance().getTime());
            capitulo.setOtraDescripcion("");
            capitulo.setIdProvisional(contador);
            contador++;
        }

        /*
         * Si las lista de capitulos de avaluo esta vacia, entonces la lista de la tabla se llena
         * con los capitulos por default
         */
        if (listaCapitulosAvaluo.isEmpty()) {
            for (AvaluoCapitulo capitulo : listaCapitulosDefault) {
                this.listaCapitulos.add(capitulo);
            }
        } /*
         * Si no, se valida cuales son los capitulos por default que no se encuentran en la lista
         * del avaluo para pintar la tabla
         */ else {
            contador = 0;

            for (AvaluoCapitulo capituloAva : listaCapitulosAvaluo) {
                listaCodigosCapDefault.add(capituloAva.getCodigo());
            }

            for (AvaluoCapitulo capituloDef : listaCapitulosDefault) {
                if (!listaCodigosCapDefault.contains(capituloDef.getCodigo())) {
                    listaCapitulosAvaluo.add(capituloDef);
                    continue;
                }
            }

            for (AvaluoCapitulo capitulo : listaCapitulosAvaluo) {
                capitulo.setIdProvisional(contador);
                this.listaCapitulos.add(capitulo);
                contador++;
            }
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#cargarCapitulos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para
     *
     * @author rodrigo.hernandez
     */
    private void cargarDocsRequisito() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#cargarDocsRequisito");

        this.listaDocumentosSeleccionados = this.getAvaluosService()
            .obtenerDocsRequisitoProyeccionCotizacionPorAvaluoId(
                this.avaluo.getId());

        LOGGER.debug("Fin ProyeccionCotizacionMB#cargarDocsRequisito");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar la lista de tipos de documentos a solicitar
     *
     * @author rodrigo.hernandez
     */
    private void crearListaDocumentosASolicitar() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#crearListaDocumentosASolicitar");

        this.listaDocumentacionASolicitar = this.getGeneralesService()
            .getAllTipoDocumento();

        /*
         * Se selecciona el primer item de la lista
         */
        this.tipoDocumentoSeleccionado = this.listaDocumentacionASolicitar
            .get(0);

        LOGGER.debug("Fin ProyeccionCotizacionMB#crearListaDocumentosASolicitar");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para procesar evento al seleccionar si se realiza contrato o no
     *
     * @author rodrigo.hernandez
     */
    public void onChangeRealizaContrato() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#onChangeRealizaContrato");

        if (this.banderaGeneraContrato) {
            this.avaluo.setRealizarContrato(ESiNo.SI.getCodigo());
        } else {
            this.avaluo.setRealizarContrato(ESiNo.NO.getCodigo());
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#onChangeRealizaContrato");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear el oficio de la cotización
     *
     * @author rodrigo.hernandez
     */
    public void generarOficioCotizacion() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#generarOficioCotizacion");

        // TODO::rodrigo.hernandez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        /*
         * Se crea mapa de parametros para enviarlo al servicio que genera el reporte
         */
        Map<String, String> parameters = new HashMap<String, String>();

        this.reporteDTO = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        this.banderaOficioGenerado = true;

        /*
         * Se crea el objeto Documento, se guarda en BD y en Alfresco
         */
        this.crearDocumento();
        /*
         * Se asocian los documentos a la proyeccion de cotizacion
         */
        this.guardarDocumentosRequisito();
        /*
         * Se asocian los capitulos a la proyeccion de cotizacion
         */
        this.guardarCapitulos();
        /*
         * Se actualiza el avaluo
         */
        this.guardarAvaluo();

        LOGGER.debug("Fin ProyeccionCotizacionMB#generarOficioCotizacion");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para actualizar el avaluo
     *
     * @author rodrigo.hernandez
     */
    private void guardarAvaluo() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#guardarAvaluo");

        if (this.foliosAnexos == null || this.foliosAnexos.isEmpty()) {
            this.avaluo.setFoliosAnexos(0L);
        } else {
            this.avaluo.setFoliosAnexos(Long.valueOf(this.foliosAnexos));
        }

        this.getAvaluosService().actualizarAvaluo(this.avaluo);

        LOGGER.debug("Fin ProyeccionCotizacionMB#guardarAvaluo");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear el Documento asociado a la cotizacion
     *
     * @author rodrigo.hernandez
     */
    private void crearDocumento() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#crearDocumento");

        Documento documento = new Documento();
        TipoDocumento tipoDocumento = new TipoDocumento();
        TramiteDocumento td = new TramiteDocumento();

        /*
         * Se prepara el TipoDocumento
         */
        tipoDocumento.setId(ETipoDocumento.OTRO.getId());
        tipoDocumento.setNombre(ETipoDocumento.OTRO.getNombre());

        /*
         * Se prepara el Documento
         */
        documento.setTipoDocumento(tipoDocumento);
        documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        documento.setBloqueado(ESiNo.NO.getCodigo());
        documento.setArchivo(this.reporteDTO.getUrlWebReporte());
        documento.setUsuarioCreador(this.usuario.getLogin());
        documento.setUsuarioLog(this.usuario.getLogin());
        documento.setFechaLog(new Date(System.currentTimeMillis()));
        documento.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        documento.setDescripcion("Proyección de cotización");

        documento = this.guardarDocumentoEnGestorDocumental(documento,
            tipoDocumento);

        if (documento != null) {
            documento = this.getGeneralesService().guardarDocumento(documento);
        }

        /*
         * Se prepara el TramiteDocumento
         */
        td.setDocumento(documento);
        td.setTramite(this.avaluo.getTramite());
        td.setFechaLog(new Date());
        td.setfecha(new Date());
        td.setUsuarioLog(this.usuario.getLogin());

        td = this.getTramiteService().actualizarTramiteDocumento(td);

        if (td == null) {
            this.addMensajeError("Error al relacionar el documento al tramite");
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#crearDocumento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para almacenar el Documento creado en Alfresco
     *
     * @author rodrigo.hernandez
     */
    private Documento guardarDocumentoEnGestorDocumental(Documento documento,
        TipoDocumento tipoDocumento) {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#guardarDocumentoEnGestorDocumental");

        Solicitud solTemp = this.avaluo.getTramite().getSolicitud();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(solTemp
                .getNumero(), solTemp.getFecha(), tipoDocumento
                .getNombre(), this.reporteDTO.getArchivoReporte()
                    .getAbsolutePath());

        documento = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, documento);

        if (documento != null && documento.getIdRepositorioDocumentos() == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#guardarDocumentoEnGestorDocumental");

        return documento;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para ver la vista previa del oficio generado
     *
     * @author rodrigo.hernandez
     */
    public void verVistaPrevia() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#verVistaPrevia");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        previsualizacionDocMB.setReporteDTO(this.reporteDTO);

        LOGGER.debug("Fin ProyeccionCotizacionMB#verVistaPrevia");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear un nuevo capítulo
     *
     * @author rodrigo.hernandez
     */
    public void crearCapitulo() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#crearCapitulo");
        int id = this.listaCapitulos.size() + 1;

        this.nuevoCapitulo = new AvaluoCapitulo();

        this.nuevoCapitulo.setAvaluoId(this.avaluo.getId());
        this.nuevoCapitulo.setUsuarioLog(this.usuario.getLogin());
        this.nuevoCapitulo.setFechaLog(Calendar.getInstance().getTime());
        this.nuevoCapitulo.setIdProvisional(id);

        LOGGER.debug("Fin ProyeccionCotizacionMB#crearCapitulo");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para agregar el capitulo creado a la lista
     *
     * @author rodrigo.hernandez
     */
    public void agregarNuevoCapitulo() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#agregarNuevoCapitulo");

        /*
         * Se añade a la lista unicamente si se esta creando un capitulo nuevo
         */
        if (this.banderaEsAdicionCapitulo) {
            this.listaCapitulos.add(this.nuevoCapitulo);
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#agregarNuevoCapitulo");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para eliminar un capitulo creado
     *
     * @author rodrigo.hernandez
     */
    public void eliminarCapitulo() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#eliminarCapitulo");

        for (AvaluoCapitulo ac : this.listaCapitulos) {
            if (ac.getTitulo().equals(this.nuevoCapitulo.getTitulo())) {
                this.listaCapitulos.remove(ac);

                /*
                 * Si el capitulo tiene un id, se asume que viene de BD y por ende se elimina el
                 * registro
                 */
                if (ac.getId() != null) {
                    this.getAvaluosService()
                        .eliminarCapituloProyeccionCotizacion(ac);
                }

                break;
            }
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#eliminarCapitulo");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para adicionar un documento a la lista de documentacion seleccionada
     *
     * @author rodrigo.hernandez
     */
    public void agregarDocumentoRequisito() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#agregarDocumentoRequisito");

        /*
         * Lista con los ids de los documentos seleccionados. Se usan para verificar si un documento
         * a adicionar ya se encuentra seleccionado
         */
        List<Long> listaIdsDocsSeleccionados = null;
        String mensaje = "";
        AvaluoRequisitoDocumentacion docRequisito = null;
        boolean banderaDocRequisitoExistente = true;

        /*
         * Se valida si la lista de documentos seleccionados para solicitar no está vacía
         */
        if (!this.listaDocumentosSeleccionados.isEmpty()) {

            /*
             * Se reinicia la lista de ids de docs seleccionados
             */
            listaIdsDocsSeleccionados = new ArrayList<Long>();

            /*
             * Se cargar la lista de ids de docs seleccionados
             */
            for (AvaluoRequisitoDocumentacion ard : this.listaDocumentosSeleccionados) {
                listaIdsDocsSeleccionados.add(ard.getTipoDocumentoId());
            }

            /*
             * Se valida si la lista de ids de docs seleccionados contiene el documento a adicionar
             */
            if (listaIdsDocsSeleccionados
                .contains(this.tipoDocumentoSeleccionado.getId())) {
                banderaDocRequisitoExistente = true;
            } else {
                banderaDocRequisitoExistente = false;
            }

        } else {
            banderaDocRequisitoExistente = false;
        }

        /*
         * Si el documento ya existe en la lista docs seleccionados se notifica con un mensaje de
         * error
         */
        if (banderaDocRequisitoExistente) {
            mensaje = "El documento requisito ya esta en la lista de documentos a solicitar";
            this.addMensajeError(mensaje);
        } /*
         * Si no, se añade a la lista
         */ else {
            /*
             * Se adiciona el documento a la lista de docs seleccionados
             */
            docRequisito = new AvaluoRequisitoDocumentacion();
            docRequisito.setAvaluoId(this.avaluo.getId());
            docRequisito.setUsuarioLog(this.usuario.getLogin());
            docRequisito.setFechaLog(Calendar.getInstance().getTime());
            docRequisito.setTipoDocumentoId(this.tipoDocumentoSeleccionado
                .getId());
            docRequisito.setDescripcion(this.tipoDocumentoSeleccionado
                .getNombre());

            this.listaDocumentosSeleccionados.add(docRequisito);

            /*
             * Se notifica la adicion con un mensaje de informacion
             */
            mensaje = "El documento requisito ha sido agregado a la lista de documentos a solicitar";
            this.addMensajeInfo(mensaje);
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#agregarDocumentoRequisito");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para
     *
     * @author rodrigo.hernandez
     */
    public void borrarDocumentoRequisito() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#borrarDocumentoRequisito");

        this.listaDocumentosSeleccionados.remove(this.docRequisitoSeleccionado);

        /*
         * Si el documento tiene un id, se asume que viene de BD y por ende se elimina el registro
         */
        if (this.docRequisitoSeleccionado.getId() != null) {
            this.getAvaluosService().eliminarDocRequisitoProyeccionCotizacion(
                this.docRequisitoSeleccionado);
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#borrarDocumentoRequisito");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para almacenar los capitulos seleccionados
     *
     * @author rodrigo.hernandez
     */
    private void guardarCapitulos() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#guardarCapitulos");

        boolean result = false;
        List<AvaluoCapitulo> listaCapitulosAux = new ArrayList<AvaluoCapitulo>();
        String mensaje = "";

        if (this.listaCapitulosSeleccionados.length > 0) {
            for (AvaluoCapitulo cap : this.listaCapitulosSeleccionados) {
                listaCapitulosAux.add(cap);
            }

            result = this.getAvaluosService()
                .guardarActualizarCapitulosProyeccionCotizacion(
                    listaCapitulosAux);

            if (result) {
                mensaje = "Los capitulos fueron guardados exitosamente";
                this.addMensajeInfo(mensaje);
            } else {
                mensaje = "Hubo un problema al guardar los capitulos. Por favor revise";
                this.addMensajeError(mensaje);
            }
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#guardarCapitulos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para guardar los documentos que son requeridos
     *
     * @author rodrigo.hernandez
     */
    private void guardarDocumentosRequisito() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#guardarDocumentosRequisito");

        boolean result = false;
        String mensaje = "";

        if (this.listaDocumentosSeleccionados.size() > 0) {
            result = this.getAvaluosService()
                .guardarActualizarDocsRequisitoProyeccionCotizacion(
                    this.listaDocumentosSeleccionados);

            if (result) {
                mensaje = "Los documentos fueron guardados exitosamente";
                this.addMensajeInfo(mensaje);
            } else {
                mensaje = "Hubo un problema al guardar los documentos. Por favor revise";
                this.addMensajeError(mensaje);
            }
        }

        LOGGER.debug("Fin ProyeccionCotizacionMB#guardarDocumentosRequisito");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para enviar cotizacion a control de calidad. Mueve proceso
     *
     * @author rodrigo.hernandez
     */
    public void enviarAControlCalidad() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#enviarAControlCalidad");

        this.moverProceso();

        LOGGER.debug("Fin ProyeccionCotizacionMB#enviarAControlCalidad");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para avanzar proceso
     *
     * @author rodrigo.hernandez
     */
    private void moverProceso() {
        LOGGER.debug("Inicio SolicitudDocumentacionMB#avanzarProceso");

        // TODO::rodrigo.hernandez::Implementar funcionalidad para avanzar
        // proceso
        LOGGER.debug("Fin SolicitudDocumentacionMB#avanzarProceso");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cerrar la pagina
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {

        LOGGER.debug("iniciando ProyeccionCotizacionMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("finalizando ProyeccionCotizacionMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

}
