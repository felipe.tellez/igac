package co.gov.igac.snc.web.locator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactoryBean;

import co.gov.igac.snc.fachadas.IActualizacion;
import co.gov.igac.snc.fachadas.IAvaluos;
import co.gov.igac.snc.fachadas.IConservacion;
import co.gov.igac.snc.fachadas.IFormacion;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.IInterrelacion;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.fachadas.ITransaccional;
import co.gov.igac.snc.web.components.IAvanzarProcesoConservacion;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;

/**
 * Standard EJB Service Locator pattern with a dynamic remote proxy
 *
 * @author juan.mendez
 * @version 2.0
 */
public final class SNCServiceLocator {

    private static final Logger LOGGER = LoggerFactory.getLogger(SNCServiceLocator.class);

    private static SNCServiceLocator instance;

    private String conservacionJndi;
    private String procesosJndi;
    private String generalesJndi;
    private String actualizacionJndi;
    private String tramiteJndi;
    private String avaluosJndi;
    private String formacionJndi;
    private String transaccionalJndi;
    private String interrelacionJndi;

    private IContextListener context;

    private IConservacion proxyConservacion;
    private IProcesos proxyProcesos;
    private IGenerales proxyGenerales;
    private ITramite proxyTramite;
    private IActualizacion proxyActualizacion;
    private IAvaluos proxyAvaluos;
    private IFormacion proxyFormacion;
    private ITransaccional proxyTransaccional;
    private IAvanzarProcesoConservacion proxyAvanzarProcesoConservacion;
    private IInterrelacion proxyInterrelacion;

    private String avanzarProcesoConservacionClass;

    /**
     *
     */
    private SNCServiceLocator(IContextListener contexto) {
        SNCPropertiesUtil props = SNCPropertiesUtil.getInstance();
        conservacionJndi = props.getProperty("conservacionJndi");
        procesosJndi = props.getProperty("procesosJndi");
        generalesJndi = props.getProperty("generalesJndi");
        actualizacionJndi = props.getProperty("actualizacionJndi");
        tramiteJndi = props.getProperty("tramiteJndi");
        avaluosJndi = props.getProperty("avaluosJndi");
        formacionJndi = props.getProperty("formacionJndi");
        transaccionalJndi = props.getProperty("transaccionalJndi");
        interrelacionJndi = props.getProperty("interrelacionJndi");

        this.avanzarProcesoConservacionClass =
            "co.gov.igac.snc.web.components.impl.AvanzarProcesoConservacionBean";

        context = contexto;
    }

    /**
     *
     * @return
     */
    public static SNCServiceLocator getInstance(IContextListener contexto) {
        if (instance == null) {
            instance = new SNCServiceLocator(contexto);
        }
        return instance;
    }

    /**
     * Locate a Remote service
     *
     * @throws NamingException
	 * */
    public IConservacion getConservacionService() throws NamingException {
        if (proxyConservacion == null) {
            Object remoteObject = (conservacionJndi.endsWith("Local") ? lookupEJBLocal(
                conservacionJndi) :
                 lookupEJBRemote(conservacionJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IConservacion.class.getClassLoader(),
                new Class[]{IConservacion.class}, handler);
            proxyConservacion = (IConservacion) proxy;
        }
        return proxyConservacion;

    }

    /**
     * Locate a Remote service
     *
     * @author fredy.wilches
     * @throws NamingException
	 * */
    public IInterrelacion getInterrelacionService() throws NamingException {
        if (proxyInterrelacion == null) {
            Object remoteObject = (interrelacionJndi.endsWith("Local") ? lookupEJBLocal(
                interrelacionJndi) :
                 lookupEJBRemote(interrelacionJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IInterrelacion.class.getClassLoader(),
                new Class[]{IInterrelacion.class}, handler);
            proxyInterrelacion = (IInterrelacion) proxy;
        }
        return proxyInterrelacion;

    }

    /**
     *
     * @return @throws NamingException
     */
    public IProcesos getProcesosService() throws NamingException {
        if (proxyProcesos == null) {
            Object remoteObject =
                (conservacionJndi.endsWith("Local") ? lookupEJBLocal(procesosJndi) :
                 lookupEJBRemote(procesosJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IProcesos.class.getClassLoader(), new Class[]{
                IProcesos.class},
                handler);
            proxyProcesos = (IProcesos) proxy;
        }
        return proxyProcesos;
    }

    /**
     * Obtiene una referencia al servicio remoto (EJB) que implementa la interfaz IGenerales A esta
     * referencia se le aplican los Aspectos (AOP) para caché de datos en la capa web.
     *
     * @return
     * @throws NamingException
     */
    public IGenerales getGeneralesService() throws NamingException {
        if (proxyGenerales == null) {
            Object remoteObject = (conservacionJndi.endsWith("Local") ?
                lookupEJBLocal(generalesJndi) :
                 lookupEJBRemote(generalesJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IGenerales.class.getClassLoader(), new Class[]{
                IGenerales.class}, handler);
            //Proxy de la fachada ejb
            proxyGenerales = (IGenerales) proxy;
            ///////////////////////////////////////////////////////////////
            ProxyFactoryBean proxyFactory = new ProxyFactoryBean();
            proxyFactory.addInterface(IGenerales.class);
            proxyFactory.setBeanFactory(context.getApplicationContext());
            proxyFactory.setInterceptorNames(new String[]{"methodCacheInterceptor"});
            proxyFactory.setTarget(proxyGenerales);
            //proxy con aop de caché
            proxyGenerales = (IGenerales) proxyFactory.getObject();
            ///////////////////////////////////////////////////////////////
        }
        return proxyGenerales;
    }

    /**
     * Obtiene una referencia al servicio remoto (EJB) que implementa la interfaz ITramite A esta
     * referencia se le aplican los Aspectos (AOP) para caché de datos en la capa web.
     *
     * @return
     * @throws NamingException
     */
    public ITramite getTramiteService() throws NamingException {
        if (proxyTramite == null) {
            Object remoteObject =
                (conservacionJndi.endsWith("Local") ? lookupEJBLocal(tramiteJndi) :
                 lookupEJBRemote(tramiteJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(ITramite.class.getClassLoader(), new Class[]{
                ITramite.class}, handler);
            //Proxy de la fachada ejb
            proxyTramite = (ITramite) proxy;
            ///////////////////////////////////////////////////////////////
            ProxyFactoryBean proxyFactory = new ProxyFactoryBean();
            proxyFactory.addInterface(ITramite.class);
            proxyFactory.setBeanFactory(context.getApplicationContext());
            proxyFactory.setInterceptorNames(new String[]{"methodCacheInterceptor"});
            proxyFactory.setTarget(proxyTramite);
            //proxy con aop de caché
            proxyTramite = (ITramite) proxyFactory.getObject();
            ///////////////////////////////////////////////////////////////
        }
        return proxyTramite;
    }

    /**
     *
     * @return @throws NamingException
     */
    public IActualizacion getActualizacionService() throws NamingException {
        if (proxyActualizacion == null) {
            Object remoteObject = (conservacionJndi.endsWith("Local") ? lookupEJBLocal(
                actualizacionJndi) :
                 lookupEJBRemote(actualizacionJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IActualizacion.class.getClassLoader(),
                new Class[]{IActualizacion.class}, handler);
            proxyActualizacion = (IActualizacion) proxy;
        }
        return proxyActualizacion;
    }

    /**
     *
     * @return @throws NamingException
     */
    public IAvaluos getAvaluosService() throws NamingException {
        if (proxyAvaluos == null) {
            Object remoteObject =
                (conservacionJndi.endsWith("Local") ? lookupEJBLocal(avaluosJndi) :
                 lookupEJBRemote(avaluosJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IAvaluos.class.getClassLoader(), new Class[]{
                IAvaluos.class}, handler);
            proxyAvaluos = (IAvaluos) proxy;
        }
        return proxyAvaluos;
    }

    /**
     *
     * @return @throws NamingException
     */
    public IFormacion getFormacionService() throws NamingException {
        if (proxyFormacion == null) {
            Object remoteObject = (conservacionJndi.endsWith("Local") ?
                lookupEJBLocal(formacionJndi) :
                 lookupEJBRemote(formacionJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(IFormacion.class.getClassLoader(), new Class[]{
                IFormacion.class}, handler);
            proxyFormacion = (IFormacion) proxy;
        }
        return proxyFormacion;
    }

    /**
     * Locate a Remote service
     *
     * @throws NamingException
	 * */
    public ITransaccional getTransaccionalService() throws NamingException {
        if (proxyTransaccional == null) {
            Object remoteObject = (transaccionalJndi.endsWith("Local") ? lookupEJBLocal(
                transaccionalJndi) :
                 lookupEJBRemote(transaccionalJndi));
            InvocationHandler handler = new RemoteProxyHandler(remoteObject);
            Object proxy = Proxy.newProxyInstance(ITransaccional.class.getClassLoader(),
                new Class[]{ITransaccional.class}, handler);
            proxyTransaccional = (ITransaccional) proxy;
        }
        return proxyTransaccional;

    }

    /**
     * Instancia el objeto IAvanzarProcesoConservacion según la clase definida Ensambla el objeto
     * con los servicios adicionales de los que depende
     *
     * @return
     */
    public IAvanzarProcesoConservacion getAvanzarProcesoConservacion() {
        if (proxyAvanzarProcesoConservacion == null) {
            try {
                //Object proxy:
                Class<?> clazz = Class.forName(this.avanzarProcesoConservacionClass);
                Constructor<?> ctor = clazz.getConstructor();
                Object object = ctor.newInstance();
                InvocationHandler handler = new RemoteProxyHandler(object);
                Object proxy = Proxy.newProxyInstance(IAvanzarProcesoConservacion.class.
                    getClassLoader(),
                    new Class[]{IAvanzarProcesoConservacion.class}, handler);
                proxyAvanzarProcesoConservacion = (IAvanzarProcesoConservacion) proxy;

                //Object assembler:
                proxyAvanzarProcesoConservacion.setGeneralesService(getGeneralesService());
                proxyAvanzarProcesoConservacion.setProcesoService(getProcesosService());
                proxyAvanzarProcesoConservacion.setTramiteService(getTramiteService());
            } catch (Exception e) {
                throw SNCWebServiceExceptions.EXCEPCION_INFRAESTRUCTURA_0001.getExcepcion(LOGGER, e,
                    e.getMessage());
            }
        }
        return proxyAvanzarProcesoConservacion;
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Realiza el lookup de un ejb Remoto (Para jboss 7.1)
     *
     * @param jndi
     * @return
     * @throws NamingException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private Object lookupEJBRemote(String jndi) throws NamingException {
        final Hashtable jndiProperties = new Hashtable();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);
        Object remoteService = context.lookup(jndi);
        return remoteService;
    }

    /**
     * Realiza el lookup de un ejb Local (Para jboss 7.1)
     *
     * @return
     * @throws NamingException
     */
    private Object lookupEJBLocal(String jndi) throws NamingException {
        InitialContext ctx = new InitialContext();
        Object localService = ctx.lookup(jndi);
        return localService;
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
