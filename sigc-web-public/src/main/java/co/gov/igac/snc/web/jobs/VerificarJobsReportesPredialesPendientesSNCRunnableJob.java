package co.gov.igac.snc.web.jobs;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Este Job realiza las siguientes operaciones: Consulta los jobs de registros prediales que
 * terminaron de ejecutarse en la base de datos
 *
 * Envía los jobs encontrados para que sean ejecutadas las siguientes acciones según corresponda.
 * Para tal fin se utiliza una tarea VerificarJobTask. Dicha tarea se ejecuta en su propio hilo
 * buscando mayor paralelismo. ( Los threads son controlados por Spring )
 *
 *
 * @author javier.aponte
 *
 */
public class VerificarJobsReportesPredialesPendientesSNCRunnableJob extends AbstractLocator
    implements Runnable, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(
        VerificarJobsReportesPredialesPendientesSNCRunnableJob.class);

    private TaskExecutor taskExecutor;
    private Long cantidadJobs;

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void setCantidadJobs(Long cantidadJobs) {
        this.cantidadJobs = cantidadJobs;
    }

//--------------------------------------------------------------------------------------------------	
    @Override
    public void run() {
        try {
            List<ProductoCatastral> jobsPendientes = this.getGeneralesService().
                obtenerJobsReportesPredialesPendientesEnSNC(
                    this.cantidadJobs);
            if (jobsPendientes != null && jobsPendientes.size() > 0) {
                for (ProductoCatastral job : jobsPendientes) {
                    //envía los jobs a la cola (Dicha cola tiene un pool de instancias
                    //que se ejecutan en paralelo )
                    this.taskExecutor.execute(new VerificarJobReportesPredialesTask(job));
                }
            } else {
                LOGGER.warn("No se obtuvieron jobs pendientes para ejecución");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
