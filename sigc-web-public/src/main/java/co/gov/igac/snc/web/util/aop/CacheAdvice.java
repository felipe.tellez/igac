package co.gov.igac.snc.web.util.aop;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ejemplo de Aspecto de AOP para manejo de Cache utilizando ehcache
 *
 * http://www.packtpub.com/article/design-with-spring-aop
 * http://springtips.blogspot.com/2007/06/caching-methods-result-using-spring-and_23.html
 *
 * @author jcmendez
 *
 */
public class CacheAdvice implements MethodInterceptor {

    private static final Logger logger = LoggerFactory
        .getLogger(CacheAdvice.class);

    private Cache cache;

    /**
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object result = null;
        if (isCacheable(invocation.getMethod().getName())) {
            logger.debug("******************");
            logger.debug("** Cache Begin...");

            String cacheKey = getCacheKey(invocation);
            logger.info(new StringBuilder("Cache Key:").append(cacheKey).toString());
            Element element = (Element) cache.get(cacheKey);
            if (element == null) {
                logger.debug("******Consultando información para adicionar al Caché");
                result = invocation.proceed();
                element = new Element(cacheKey, result);
                cache.put(element);
                logger.debug(new StringBuilder(" Put:").append(cacheKey).toString());
                // .append(" value:").append(result).toString());
            } else {
                logger.debug("****** Se obtuvieron datos desde el Cache");
            }
            result = element.getValue();
            logger.debug("** Cache End...");
            logger.debug("******************");
        } else {
            result = invocation.proceed();
        }
        return result;
    }

    /**
     *
     * @param methodName
     * @return
     */
    private boolean isCacheable(String methodName) {
        boolean cacheable = methodName.startsWith("getCache");
        return cacheable;
    }

    /**
     *
     */
    public void flush() {
        logger.debug("flush");
        cache.flush();
    }

    /**
     * Genera un identificador único para el caché basado en la clase, nombre del método y
     * argumentos pasados a la función.
     *
     * @param pjp
     * @return
     */
    private String getCacheKey(MethodInvocation invocation) {
        String targetName = invocation.getMethod().getDeclaringClass().getCanonicalName();
        String methodName = invocation.getMethod().getName();
        Object[] arguments = invocation.getArguments();

        StringBuilder sb = new StringBuilder();
        sb.append(targetName).append(".").append(methodName);
        if ((arguments != null) && (arguments.length != 0)) {
            for (int i = 0; i < arguments.length; i++) {
                sb.append(".").append(arguments[i]);
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param cache
     */
    public void setCache(Cache cache) {
        logger.debug("setCache:" + cache.getName());
        this.cache = cache;
    }
}
