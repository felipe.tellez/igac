package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioPropietarioHis;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.util.EPersonaPredioPropiedadTipoTitulo;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.JustificacionPropiedadDTO;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ESeccionEjecucion;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean para el manejo de la justificación de propiedad de el predio(s) de un trámite en
 * modificación de información alfanumérica.
 *
 * @author fabio.navarrete
 *
 */
@Component("justificacionProyeccion")
@Scope("session")
public class JustificacionProyeccionMB extends SNCManagedBean {

    private static final long serialVersionUID = -4772747637799917307L;

    private static final Logger LOGGER = LoggerFactory.getLogger(JustificacionProyeccionMB.class);

    // -------------------- SERVICIOS -------------------- //
    @Autowired
    private ProyectarConservacionMB proyectarConservacionMB;

    @Autowired
    private CambioPropietarioMB propietariosMB;

    @Autowired
    private EntradaProyeccionMB entradaProyeccionMB;

    // -------------------- VARIABLES -------------------- //
    /**
     * Justificacion seleccionada para edición o eliminación
     */
    private JustificacionPropiedadDTO justificacionSeleccionada;
    /**
     * Justificacion seleccionada para edición o eliminación
     */
    private JustificacionPropiedadDTO justificacionSeleccionadaSinModificar;

    /**
     * Propietarios seleccionados al editar la justificación
     */
    private PPersonaPredio[] propietariosNuevosSeleccionados;

    private TramiteDocumentacion tramiteDocumentacionSeleccionado;
    private List<PPersonaPredioPropiedad> pPersonaPredioPropiedads;

    private boolean registroMatriculaAntigua;

    // -------------- GEOGRAPHIC DATA --------------
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenCirculosRegistrales = EOrden.CODIGO;

    /**
     * Usado al editar la justificación
     */
    private Departamento departamento;
    private List<SelectItem> departamentoSelectItems;

    /**
     * Usado al editar la justificación
     */
    private Municipio municipio;
    private List<Departamento> departamentos;
    private List<Municipio> municipios;
    private List<SelectItem> municipioSelectItems;
    private CirculoRegistral circuloRegistral;
    private List<SelectItem> circuloRegistralSelectItems;
    private List<CirculoRegistral> circulosRegistrales;

    /**
     * para indicar si la justificación de propiedad se le debe copiar a los demás predios y a todos
     * los propietarios
     */
    private boolean copiarJustificacionAtodos;

    private UsuarioDTO usuario;

    private List<PredioPropietarioHis> predioPropietariosHis;
    private Boolean adicionando;

    private Tramite tramite;

    /**
     * Tipos de derecho de propiedad, pueden variar si se trata de un predio de mejora
     */
    private List<SelectItem> tiposDerechoPropiedad;

    /**
     * Determina si se muestra o no el dato entidad emisora
     */
    private boolean banderaCartaVenta;

    /**
     * Lista de documentos disponibles para asociar en las justificaciones de propiedad
     */
    private List<TramiteDocumentacion> documentosParaAsociar;

    /**
     * Determina si se muestra o no el dato copiar Justificacion
     */
    private boolean habilitaCopiaJustificacion;

    /**
     * Fecha Creacion Predio
     */
    private Date fechaCreacionPredio;

    /**
     * Determina si se muestra o no el dato adicionar justificacion
     */
    private boolean banderaAdicionaJustificacion;

    @PostConstruct
    public void init() {

        usuario = MenuMB.getMenu().getUsuarioDto();
        this.habilitaCopiaJustificacion = false;
        this.banderaAdicionaJustificacion = true;

        //@modified by juanfelipe.garcia :: 27-11-2013 
        //esta lógica estaba en el getTramite y generaba ruido en los documentos del trámite en otros MB,
        //ahora solo asigna los documentos del trámite una vez
        this.tramite = proyectarConservacionMB.getTramite();
        if (tramite != null) {
            Predio predio = tramite.getPredio();
            List<TramitePredioEnglobe> pe = tramite.getTramitePredioEnglobes();
            this.tramite.setTramiteDocumentos(this.getTramiteService()
                .obtenerTramiteDocumentosPorTramiteId(
                    this.tramite.getId()));
            tramite.setTramitePredioEnglobes(pe);
            tramite.setPredio(predio);
        }

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

        PPredio ppredio = this.proyectarConservacionMB.getPredioSeleccionado();
        this.tiposDerechoPropiedad = generalMB.getDerechoPropiedadTipoTituloVCV();

        if (ppredio != null && !ppredio.isMejora()) {
            for (int i = 0; i < this.tiposDerechoPropiedad.size(); i++) {
                SelectItem si = this.tiposDerechoPropiedad.get(i);

                if (si.getValue().equals(EPersonaPredioPropiedadTipoTitulo.CARTA_VENTA.getValor())) {
                    this.tiposDerechoPropiedad.remove(si);
                }
            }
        }

        this.actualizarEntidad();
    }

    /*
     * GETTERS Y SETTERS
     */
    public boolean isBanderaAdicionaJustificacion() {
        return banderaAdicionaJustificacion;
    }

    public void setBanderaAdicionaJustificacion(boolean banderaAdicionaJustificacion) {
        this.banderaAdicionaJustificacion = banderaAdicionaJustificacion;
    }

    public Date getFechaCreacionPredio() {
        return fechaCreacionPredio;
    }

    public void setFechaCreacionPredio(Date fechaCreacionPredio) {
        this.fechaCreacionPredio = fechaCreacionPredio;
    }

    public boolean isHabilitaCopiaJustificacion() {
        return habilitaCopiaJustificacion;
    }

    public void setHabilitaCopiaJustificacion(boolean habilitaCopiaJustificacion) {
        this.habilitaCopiaJustificacion = habilitaCopiaJustificacion;
    }

    public List<TramiteDocumentacion> getDocumentosParaAsociar() {
        return documentosParaAsociar;
    }

    public void setDocumentosParaAsociar(List<TramiteDocumentacion> documentosParaAsociar) {
        this.documentosParaAsociar = documentosParaAsociar;
    }

    public List<SelectItem> getTiposDerechoPropiedad() {
        return tiposDerechoPropiedad;
    }

    public void setTiposDerechoPropiedad(List<SelectItem> tiposDerechoPropiedad) {
        this.tiposDerechoPropiedad = tiposDerechoPropiedad;
    }

    public JustificacionPropiedadDTO getJustificacionSeleccionadaSinModificar() {
        return justificacionSeleccionadaSinModificar;
    }

    public void setJustificacionSeleccionadaSinModificar(
        JustificacionPropiedadDTO justificacionSeleccionadaSinModificar) {
        this.justificacionSeleccionadaSinModificar = justificacionSeleccionadaSinModificar;
    }

    public boolean isBanderaCartaVenta() {
        return banderaCartaVenta;
    }

    public void setBanderaCartaVenta(boolean banderaCartaVenta) {
        this.banderaCartaVenta = banderaCartaVenta;
    }

    public Boolean getAdicionando() {
        return this.adicionando;
    }

    public boolean isCopiarJustificacionAtodos() {
        return this.copiarJustificacionAtodos;
    }

    public void setCopiarJustificacionAtodos(boolean copiarJustificacionAtodos) {
        this.copiarJustificacionAtodos = copiarJustificacionAtodos;
    }

    public JustificacionPropiedadDTO getJustificacionSeleccionada() {
        return this.justificacionSeleccionada;
    }

    public void setJustificacionSeleccionada(JustificacionPropiedadDTO justificacionSeleccionada) {

        if (justificacionSeleccionada == null) {
            return;
        }
        this.justificacionSeleccionada = justificacionSeleccionada;

        this.propietariosNuevosSeleccionados = new PPersonaPredio[this.justificacionSeleccionada.
            getJustificaciones().size()];
        for (int i = 0; i < this.propietariosNuevosSeleccionados.length; i++) {
            this.propietariosNuevosSeleccionados[i] = this.justificacionSeleccionada.
                getJustificaciones().get(i).getPPersonaPredio();
        }

        this.departamento = this.justificacionSeleccionada.getDepartamento();
        this.updateDepartamentos();
        this.municipio = this.justificacionSeleccionada.getMunicipio();
        this.updateMunicipios();

        this.adicionando = false;
    }

    public void setAdicionando(Boolean adicionando) {
        this.adicionando = adicionando;
    }

    public PPredio getPredioSeleccionado() {
        return proyectarConservacionMB.getPredioSeleccionado();
    }

    public Tramite getTramite() {
        return tramite;
    }

    public List<PPersonaPredioPropiedad> getpPersonaPredioPropiedads() {
        return this.pPersonaPredioPropiedads;
    }

    public void setpPersonaPredioPropiedads(
        List<PPersonaPredioPropiedad> pPersonaPredioPropiedads) {
        this.pPersonaPredioPropiedads = pPersonaPredioPropiedads;
    }

    public TramiteDocumentacion getTramiteDocumentacionSeleccionado() {
        return this.tramiteDocumentacionSeleccionado;
    }

    public void setTramiteDocumentacionSeleccionado(
        TramiteDocumentacion tramiteDocumentacionSeleccionado) {
        this.tramiteDocumentacionSeleccionado = tramiteDocumentacionSeleccionado;
    }

    public boolean isRegistroMatriculaAntigua() {
        return this.registroMatriculaAntigua;
    }

    public void setRegistroMatriculaAntigua(boolean registroMatriculaAntigua) {
        this.registroMatriculaAntigua = registroMatriculaAntigua;
    }

    /**
     * Retorna el listado de las justificaciones nuevas = null + I + M. Se incluyó null por los
     * quinta omitidos
     *
     * @return
     */
    private List<PPersonaPredioPropiedad> getPersonaPredioPropiedadInscritos() {
        ArrayList<PPersonaPredioPropiedad> personaPredioPropiedadInscritos =
            new ArrayList<PPersonaPredioPropiedad>();
        if (this.getPredioSeleccionado() != null && this.getPredioSeleccionado().
            getPPersonaPredios() != null) {
            for (PPersonaPredio persPred : getPredioSeleccionado().getPPersonaPredios()) {
                for (PPersonaPredioPropiedad ppp : persPred.getPPersonaPredioPropiedads()) {
                    if (ppp.getCancelaInscribe() == null ||
                        (ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.
                            getCodigo()) || ppp.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {
                        if (ppp.getCancelaInscribe() == null) {
                            if ((this.getTramite().isQuintaOmitido() && !this.getTramite().
                                isQuintaOmitidoNuevo()) ||
                                this.getTramite().isEsComplementacion() || this.getTramite().
                                isEsRectificacion()) {
                                personaPredioPropiedadInscritos.add(ppp);
                            }
                        } else {
                            personaPredioPropiedadInscritos.add(ppp);
                        }
                    }
                }
            }
        }
        return personaPredioPropiedadInscritos;
    }

    /**
     * Metodo que retorna las justificaciones agrupadas
     *
     * @return
     */
    public List<JustificacionPropiedadDTO> getJustificacionesNuevas() {
        List<JustificacionPropiedadDTO> j = new ArrayList<JustificacionPropiedadDTO>();
        List<PPersonaPredioPropiedad> ppps = getPersonaPredioPropiedadInscritos();

        if (ppps != null && !ppps.isEmpty()) {

            // @modified by leidy.gonzalez:: #20429:: Se valida que solo
            // se pueda adicionar una justificacion de Propiedad
            if (this.getTramite().isCancelacionMasiva()) {

                this.banderaAdicionaJustificacion = false;
            }

            for (PPersonaPredioPropiedad ppp : ppps) {

                // Se deben excluir las justificaciones de propiedad que sean de
                // migración.
                if (ppp != null && ppp.getModoAdquisicion() != null &&
                     (!ppp.getModoAdquisicion().equals("MIGRACION") || this.tramite.
                    isEsComplementacion())) {
                    JustificacionPropiedadDTO jDto = new JustificacionPropiedadDTO(
                        ppp);
                    int posicion = j.indexOf(jDto);
                    if (posicion == -1) {
                        //@modified by leidy.gonzalez:: #18225:: 06/07/2016 Se verifica que la fecha de titulo 
                        // a visualizar sea la fecha de inscripcion del predio seleccionado
//						if(this.getPredioSeleccionado()!= null && this.getPredioSeleccionado().getFechaInscripcionCatastral() != null){
//							jDto.setFechaTitulo(this.getPredioSeleccionado().getFechaInscripcionCatastral());
//			        	}

                        jDto.adicionarJustificacion(ppp);
                        j.add(jDto);
                    } else {
                        j.get(posicion).adicionarJustificacion(ppp);
                    }
                }
            }
        } else {
            this.banderaAdicionaJustificacion = true;
        }

        return j;
    }

    /**
     * Retorna el listado de las justificaciones vigentes = C + M
     *
     * @return
     */
    private List<PPersonaPredioPropiedad> getPersonaPredioPropiedadActuales() {
        ArrayList<PPersonaPredioPropiedad> personaPredioPropiedadActuales =
            new ArrayList<PPersonaPredioPropiedad>();
        if (this.getPredioSeleccionado() != null && this.getPredioSeleccionado().
            getPPersonaPredios() != null) {
            for (PPersonaPredio persPred : getPredioSeleccionado().getPPersonaPredios()) {
                for (PPersonaPredioPropiedad ppp : persPred.getPPersonaPredioPropiedads()) {
                    if (ppp.getCancelaInscribe() == null || ppp.getCancelaInscribe().equals(
                        EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                        ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.
                            getCodigo())) {
                        personaPredioPropiedadActuales.add(ppp);
                    }
                }
            }
        }
        return personaPredioPropiedadActuales;
    }

    /**
     * Metodo que retorna las justificaciones agrupadas
     *
     * @return
     */
    public List<JustificacionPropiedadDTO> getJustificacionesActuales() {
        List<JustificacionPropiedadDTO> j = new ArrayList<JustificacionPropiedadDTO>();
        for (PPersonaPredioPropiedad ppp : getPersonaPredioPropiedadActuales()) {
            JustificacionPropiedadDTO jDto = new JustificacionPropiedadDTO(ppp);
            int posicion = j.indexOf(jDto);
            if (posicion == -1) {
                jDto.adicionarJustificacion(ppp);
                j.add(jDto);
            } else {
                j.get(posicion).adicionarJustificacion(ppp);
            }
        }
        return j;
    }

    public PPersonaPredio[] getPropietariosNuevosSeleccionados() {
        return propietariosNuevosSeleccionados;
    }

    public void setPropietariosNuevosSeleccionados(
        PPersonaPredio[] propietariosNuevosSeleccionados) {
        this.propietariosNuevosSeleccionados = propietariosNuevosSeleccionados;
    }

    public List<SelectItem> getDepartamentoSelectItems() {
        return departamentoSelectItems;
    }

    public List<PredioPropietarioHis> getPredioPropietariosHis() {
        return predioPropietariosHis;
    }

    public void setPredioPropietariosHis(
        List<PredioPropietarioHis> predioPropietariosHis) {
        this.predioPropietariosHis = predioPropietariosHis;
    }

    /*
     * METODOS GUI @param codigo
     */
    public void setSelectedDepartamento(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento departamentoTemp : departamentos) {
                if (codigo.equals(departamentoTemp.getCodigo())) {
                    dptoSeleccionado = departamentoTemp;
                    break;
                }
            }
        }
        if (this.justificacionSeleccionada != null) {
            this.departamento = dptoSeleccionado;
        }
    }

    public String getSelectedDepartamento() {
        if (this.justificacionSeleccionada != null &&
             this.departamento != null) {
            return this.departamento.getCodigo();
        }
        return null;
    }

    public void onDepartamentoChange() {
        this.municipio = null;
        this.updateMunicipios();
        this.circuloRegistral = null;
        this.updateCirculosRegistrales();
    }

    public void cambiarOrdenDepartamentos() {
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
        }
        this.updateDepartamentos();
    }

    public void updateDepartamentos() {
        departamentoSelectItems = new LinkedList<SelectItem>();
        departamentoSelectItems.add(new SelectItem("", this.DEFAULT_COMBOS));

        departamentos = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(departamentos);
        } else {
            Collections.sort(departamentos, Departamento.getComparatorNombre());
        }
        for (Departamento departamentoTemp : departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                departamentoSelectItems.add(new SelectItem(departamentoTemp
                    .getCodigo(), departamentoTemp.getCodigo() + "-" +
                     departamentoTemp.getNombre()));
            } else {
                departamentoSelectItems.add(new SelectItem(departamentoTemp
                    .getCodigo(), departamentoTemp.getNombre()));
            }
        }
    }

    public void setSelectedMunicipio(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipioTemp : this.municipios) {
                if (codigo.equals(municipioTemp.getCodigo())) {
                    municipioSelected = municipioTemp;
                    break;
                }
            }
        } else {
            this.municipio = null;
        }
        if (this.justificacionSeleccionada != null) {
            this.municipio = municipioSelected;
        }
    }

    public String getSelectedMunicipio() {
        if (this.justificacionSeleccionada != null &&
             this.municipio != null) {
            return this.municipio.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getMunicipioSelectItems() {
        return this.municipioSelectItems;
    }

    public void onMunicipioChange() {
        this.circuloRegistral = null;
        this.updateCirculosRegistrales();
    }

    public void updateMunicipios() {
        municipioSelectItems = new ArrayList<SelectItem>();
        municipios = new ArrayList<Municipio>();
        municipioSelectItems.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.justificacionSeleccionada != null &&
             this.departamento != null &&
             this.departamento.getCodigo() != null) {
            municipios = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(this.departamento
                    .getCodigo());
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(municipios);
            } else {
                Collections.sort(municipios, Municipio.getComparatorNombre());
            }
            for (Municipio municipioTemp : municipios) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    municipioSelectItems.add(new SelectItem(municipioTemp
                        .getCodigo(), municipioTemp.getCodigo3Digitos() + "-" +
                         municipioTemp.getNombre()));
                } else {
                    municipioSelectItems.add(new SelectItem(municipioTemp
                        .getCodigo(), municipioTemp.getNombre()));
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    public void cambiarOrdenMunicipios() {
        try {
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                this.ordenMunicipios = EOrden.NOMBRE;
            } else {
                this.ordenMunicipios = EOrden.CODIGO;
            }
            this.updateMunicipios();
        } catch (Exception e) {
            LOGGER.error("error en JustificacionProyeccionMB#cambiarOrdenMunicipios: " +
                e.getMessage(), e);
            this.addMensajeError("Ocurrió un error al cambiar el ordenamiento de los municipios");
        }
    }

    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenDepartamentosNombre() {
        return this.ordenDepartamentos.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los círculos registrales están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenCirculosRegistralesNombre() {
        return this.ordenCirculosRegistrales.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenMunicipiosNombre() {
        return this.ordenMunicipios.equals(EOrden.NOMBRE);
    }

    /* public List<PPersonaPredio> getPropietariosNuevosNoCancelados() { return
     * this.propietariosNuevosNoCancelados;
	} */

 /* public void updatePropietariosNuevosNoCancelados()
     * { this.propietariosNuevosNoCancelados = new ArrayList<PPersonaPredio>(); if
     * (this.predioSeleccionado != null && this.predioSeleccionado.getPPersonaPredios() != null) {
     * for (PPersonaPredio ppp : this.predioSeleccionado .getPPersonaPredios()) { if
     * (ppp.getCancelaInscribe() == null || !ppp.getCancelaInscribe().equals(
     * EProyeccionCancelaInscribe.CANCELA.getCodigo())) { propietariosNuevosNoCancelados.add(ppp); }
     * } }
	} */
    public String getLabelRegistroMatricula() {
        if (this.registroMatriculaAntigua) {
            return "Información de registro de matrícula antigua";
        }
        return "Información de registro de matrícula nueva";
    }

    /**
     * Retorna el listado de items que van en el combo de círculos registrales
     *
     * @author fabio.navarrete
     * @return
     */
    public List<SelectItem> getCirculoRegistralSelectItems() {
        return this.circuloRegistralSelectItems;
    }

    public void cambiarOrdenCirculosRegistrales() {
        if (this.ordenCirculosRegistrales.equals(EOrden.CODIGO)) {
            this.ordenCirculosRegistrales = EOrden.NOMBRE;
        } else {
            this.ordenCirculosRegistrales = EOrden.CODIGO;
        }
        this.updateCirculosRegistrales();
    }

    public void onCirculoRegistralChange() {
    }

    // -------------- MÉTODOS DE NEGOCIO -------------------- //
    /**
     * Metodo incovado para adicionar una nueva justificacion de propiedad
     */
    public void adicionarTitulo() {
        this.justificacionSeleccionada = new JustificacionPropiedadDTO();
        this.justificacionSeleccionada.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
            getCodigo());
        this.justificacionSeleccionada.setFechaLog(new Date());
        this.justificacionSeleccionada.setUsuarioLog(this.usuario.getLogin());
        this.justificacionSeleccionada.setTipo(Constantes.TIPO_JUSTIFICACION_DEFECTO);
        this.justificacionSeleccionada.setModoAdquisicion(Constantes.MODO_ADQUISICION_TRADICION);
        this.propietariosNuevosSeleccionados = new PPersonaPredio[0];

        this.departamento = null;
        this.updateDepartamentos();

        if (this.getTramite().isQuinta()) {
            this.departamento = this.getTramite().getDepartamento();
            this.municipio = this.getTramite().getMunicipio();
        } else {
            this.departamento = this.getTramite().getPredios().get(0).getDepartamento();
            this.municipio = this.getTramite().getPredios().get(0).getMunicipio();
        }

        //@modified by leidy.gonzalez:: #20429:: Habilita o deshabilita boton de copia de
        // justificacion de propiedad a los predios resultantes de la cancelacion
        if (this.getTramite().isCancelacionMasiva()) {
            if (this.entradaProyeccionMB.getPrediosResultantesCancelacionMasiva() != null &&
                 !this.entradaProyeccionMB.getPrediosResultantesCancelacionMasiva().isEmpty() &&
                 this.entradaProyeccionMB.getPrediosResultantesCancelacionMasiva().size() > 1) {
                this.habilitaCopiaJustificacion = true;
            }

        }

        this.updateMunicipios();
        this.updateCirculosRegistrales();
        this.adicionando = true;
    }

    /**
     * Organiza la lista de los círculos registrales a mostrar en la GUI.
     *
     * @author fabio.navarrete
     */
    public void updateCirculosRegistrales() {
        circuloRegistralSelectItems = new ArrayList<SelectItem>();
        circulosRegistrales = new ArrayList<CirculoRegistral>();
        circuloRegistralSelectItems
            .add(new SelectItem("", super.DEFAULT_COMBOS));
        if (this.departamento != null && this.municipio != null) {
            circulosRegistrales = this.getGeneralesService()
                .getCacheCirculosRegistralesPorMunicipio(this.municipio
                    .getCodigo());
            if (this.ordenCirculosRegistrales.equals(EOrden.CODIGO)) {
                Collections.sort(circulosRegistrales);
            } else {
                Collections.sort(circulosRegistrales,
                    CirculoRegistral.getNombreComparator());
            }
            for (CirculoRegistral cr : circulosRegistrales) {
                if (this.ordenCirculosRegistrales.equals(EOrden.CODIGO)) {
                    circuloRegistralSelectItems
                        .add(new SelectItem(cr.getCodigo(), cr.getCodigo() +
                             "-" + cr.getNombre()));
                } else {
                    circuloRegistralSelectItems.add(new SelectItem(cr
                        .getCodigo(), cr.getNombre()));
                }
            }
        }
    }

    public String getSelectedCirculoRegistral() {
        if (this.justificacionSeleccionada != null &&
             this.circuloRegistral != null) {
            return this.circuloRegistral.getCodigo();
        }
        return null;
    }

    public void setSelectedCirculoRegistral(String codigo) {
        CirculoRegistral circuloRegistralSelected = null;
        if (codigo != null) {
            if (this.municipios != null) {
                for (CirculoRegistral circuloReg : this.circulosRegistrales) {
                    if (codigo.equals(circuloReg.getCodigo())) {
                        circuloRegistralSelected = circuloReg;
                        break;
                    }
                }
            }
        }
        if (this.justificacionSeleccionada != null) {
            this.circuloRegistral = circuloRegistralSelected;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "asociar" de la página de documentos asociados al trámite
     */
    public void asociarDocumento() {
        if (this.tramiteDocumentacionSeleccionado == null ||
            this.tramiteDocumentacionSeleccionado.getDocumentoSoporte() == null ||
            this.tramiteDocumentacionSeleccionado.getDocumentoSoporte().getArchivo() == null ||
            this.tramiteDocumentacionSeleccionado.getDocumentoSoporte().getArchivo().trim().equals(
                "")) {

            this.addMensajeError("Debe seleccionarse un documento digital");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
        this.justificacionSeleccionada.setDocumentoSoporte(
            this.tramiteDocumentacionSeleccionado.getDocumentoSoporte());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" de la página de documentos asociados al trámite modifified by
     * leidy.gonzalez Se agrega campo fecha titulo a los predios de condominio y ph generados por
     * ficha matriz Se agrega fecha de inscripcion catastral en la tabla PPredio cuando se agrega la
     * justificacion de propiedad al predio seleccionado tomado de esta la fecha de titulo ingresada
     * por el usuario para tramites de Desenglobe, de Quinta Nuevo y Quinta Omitido Nuevo
     *
     * @modified by leidy.gonzalez 08/08/2014 Se valida guardar datos de la justificación para
     * desenglobes.
     * @modified by leidy.gonzalez 02/10/2014 Se valida guardar la fecha de inscripcion para los
     * predios de desenglobe no mantiene
     * @modified david.cifuentes :: Ajustes por implementación de CC-119 en mutaciones de quinta
     * masivo :: 25/08/2015
     */
    /*
     * @modified by leidy.gonzalez :: 27/01/2015 :: #11217 :: Se aagrega que al guardar la fecha de
     * inscripcion catastral sea almacenada de igual manera para el predio seleccionado de la
     * proyeccion.
     */
    public void guardarDatosJustificacion() {

        List<PPersonaPredioPropiedad> justificacionesPredio;
        List<PPersonaPredioPropiedad> justificacionesActualizar =
            new ArrayList<PPersonaPredioPropiedad>();
        List<PPredio> prediosTramiteJustificacion = new ArrayList<PPredio>();
        List<PPredio> prediosSinCancelar = new ArrayList<PPredio>();

        //se actualiza el estado de los predio el tramite
        this.proyectarConservacionMB.cargarPrediosDesenglobe();

        boolean error = false;
        if (this.justificacionSeleccionada.getFechaRegistro() != null &&
            this.justificacionSeleccionada.getFechaRegistro().compareTo(
                this.justificacionSeleccionada.getFechaTitulo()) < 0) {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("La fecha del título no puede ser mayor a la fecha del registro");
            context.addCallbackParam("error", "error");
            error = true;
        }

        if (this.departamento != null && this.departamento.getCodigo() != null) {
            this.justificacionSeleccionada.setDepartamento(this.departamento);
        }
        if (this.municipio != null && this.municipio.getCodigo() != null) {
            this.justificacionSeleccionada.setMunicipio(this.municipio);
        }

        if (this.propietariosNuevosSeleccionados == null ||
            propietariosNuevosSeleccionados.length == 0) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            String mensaje = "Debe seleccionar algún propietario/poseedor";
            this.addMensajeError(mensaje);
            error = true;
        }

        if (error) {
            return;
        }

        // lorena.salamanca :: 30-09-2015 :: Se agrega para que entre al if si
        // es quinta nuevo u omitido porque se estaba toteando al intentar
        // guardar la justificacion de propiedad
        if (this.getTramite().isQuintaMasivo() || this.getTramite().isQuintaNuevo() || this.
            getTramite().isQuintaOmitido()) {
            prediosTramiteJustificacion = this.entradaProyeccionMB.getPrediosResultantes();
            if (this.tramite.isQuintaMasivo()) {
                prediosTramiteJustificacion = this.getConservacionService()
                    .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                        this.tramite.getId());
            }
        } else {
            // Cargue de predios asociados al trámite para mutaciones de segunda, o radicaciones masivas
            if (this.getTramite().isSegundaDesenglobe()) {
                prediosTramiteJustificacion = this.proyectarConservacionMB.getpPrediosDesenglobe();
            } else if (this.getTramite().isSegundaEnglobe()) {
                prediosTramiteJustificacion = this.proyectarConservacionMB.getpPrediosEnglobe();

                //@modified by leidy.gonzalez:: #20429:: Se valida que la fecha de titulo sea mayor que la fecha
                // en la que se radico el predio	   
            } else if (this.getTramite().isCancelacionMasiva()) {
                prediosTramiteJustificacion = this.entradaProyeccionMB.
                    getPrediosResultantesCancelacionMasiva();
                prediosSinCancelar = this.entradaProyeccionMB.getPrediosResultantes();
            } else {
                prediosTramiteJustificacion = this.entradaProyeccionMB.getPrediosResultantes();
            }
        }

        //@modified by leidy.gonzalez:: #20429:: Se valida que la fecha de titulo sea mayor que la fecha
        // en la que se radico el predio
        if (this.getTramite().isCancelacionMasiva()) {

            if (this.fechaCreacionPredio != null) {

                if (this.justificacionSeleccionada.getFechaTitulo() != null &&
                     this.justificacionSeleccionada.getFechaTitulo()
                        .compareTo(this.fechaCreacionPredio) < 0) {

                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    this.addMensajeError(
                        "La fecha de cancelación es inferior a la fecha de creación del PH o Condominio. Verifique");
                    context.addCallbackParam("error", "error");
                    error = true;
                    return;
                }

            } else {

                if (this.getTramite().getPredio() != null &&
                     this.getTramite().getPredio().getId() != null) {

                    this.fechaCreacionPredio = this.getConservacionService()
                        .consultarFechaCreacionPredio(this.getTramite().getPredio().getId());

                    if (this.fechaCreacionPredio != null &&
                         this.justificacionSeleccionada.getFechaTitulo() != null &&
                         this.justificacionSeleccionada.getFechaTitulo()
                            .compareTo(this.fechaCreacionPredio) < 0) {

                        RequestContext context = RequestContext
                            .getCurrentInstance();
                        this.addMensajeError(
                            "La fecha de cancelación es inferior a la fecha de creación del PH o Condominio. Verifique");
                        context.addCallbackParam("error", "error");
                        error = true;
                    }
                }

            }
        }

        //felipe.cadena:: Actualizar justificaciones de otros predios si han sido copiadas
        try {
            if ((this.getTramite().isSegundaDesenglobe() || this.getTramite().isQuintaMasivo()) &&
                 prediosTramiteJustificacion != null) {
                if (this.justificacionSeleccionadaSinModificar != null) {
                    for (PPredio predio : prediosTramiteJustificacion) {

                        justificacionesPredio = new LinkedList<PPersonaPredioPropiedad>();
                        for (PPersonaPredio persPred : predio.getPPersonaPredios()) {
                            List<PPersonaPredioPropiedad> justificaciones = this.
                                getConservacionService().
                                buscarPPersonaPredioPropiedadPorPPersonaPredio(persPred);
                            for (PPersonaPredioPropiedad ppp : justificaciones) {
                                if (ppp.getCancelaInscribe() == null ||
                                     EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(ppp.
                                        getCancelaInscribe()) ||
                                     EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(ppp.
                                        getCancelaInscribe())) {
                                    if (this.getTramite().isSegundaDesenglobe()) {
                                        justificacionesPredio.add(ppp);
                                    } else if (this.getTramite().isQuintaMasivo() && !predio.
                                        isPredioOriginalTramite()) {
                                        justificacionesPredio.add(ppp);
                                    }
                                }
                            }
                        }

                        for (PPersonaPredioPropiedad pppp : justificacionesPredio) {
                            JustificacionPropiedadDTO jp = new JustificacionPropiedadDTO(pppp);
                            if (this.justificacionSeleccionadaSinModificar.equals(jp)) {
                                PPersonaPredioPropiedad ppppAux = this.justificacionSeleccionada.
                                    crearPPersonaPredioPropiedad(pppp.getPPersonaPredio(), usuario);
                                ppppAux.setId(pppp.getId());
                                //pppp = this.getConservacionService().guardarActualizarPPersonaPredioPropiedad(ppppAux);
                                justificacionesActualizar.add(ppppAux);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Error actualizando los datos de la justificación");
        }

        try {
            //felipe.cadena::01-04-2016::#16250::Se asigna cancela inscribe en M cuando se trata de complementaciones de justificacion
            if (this.tramite.isEsComplementacion()) {
                this.justificacionSeleccionada.setCancelaInscribe(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                this.justificacionSeleccionada.setUsuarioLog(this.usuario.getLogin());
            }

            if (this.justificacionSeleccionada.getUsuarioLog() == null) {
                this.justificacionSeleccionada.setUsuarioLog(this.usuario.getLogin());
            }

            // @modified by leidy.gonzalez:: #20429:: Se valida que se hayan
            // seleccionado todos los propietarios para generar la justificacion 
            if (this.getTramite().isCancelacionMasiva()) {

                if (this.propietariosNuevosSeleccionados.length != getPropietariosNuevos().size()) {
                    this.addMensajeError(
                        "Debe seleccionar todos los propietarios nuevos en la Justificación de Propiedad" +
                        
                        "de la lista de: Propietarios y/o poseedores nuevos para asi poder generar tal Justificación");
                    return;
                }
            } else {

                if (this.tramite.isEnglobeVirtual() &&
                    this.justificacionSeleccionadaSinModificar == null) {
                    //leidy.gonzalez::#42933::08/11/2017
                    //Se guarda la justificacion seleccionada unicamente cuando se crea por 1ra vez,
                    //no cuando se edita
                    //Se valida que solo se pueda agregar una sola justificacion de propiedad por predio

                    List<PPersonaPredioPropiedad> ppp = new ArrayList<PPersonaPredioPropiedad>();
                    //Se consulta si ya se le habia agregado alguna justificacion al predio
                    for (PPersonaPredio pPersonaPredio : this.propietariosNuevosSeleccionados) {

                        ppp = this.getConservacionService().
                            consultarJustificacionesPorPropetario(pPersonaPredio.getId());

                        if (ppp != null && !ppp.isEmpty()) {

                            for (PPersonaPredioPropiedad pPersonaPredioPropiedad : ppp) {

                                if (pPersonaPredioPropiedad.getCancelaInscribe() != null &&
                                     EProyeccionCancelaInscribe.INSCRIBE.getCodigo().
                                        equals(pPersonaPredioPropiedad.getCancelaInscribe())) {

                                    pPersonaPredioPropiedad.setFechaRegistro(
                                        this.justificacionSeleccionada.getFechaRegistro());
                                    pPersonaPredioPropiedad.setTipoTitulo(
                                        this.justificacionSeleccionada.getTipoTitulo());
                                    pPersonaPredioPropiedad.setEntidadEmisora(
                                        this.justificacionSeleccionada.getEntidadEmisora());
                                    pPersonaPredioPropiedad.setModoAdquisicion(
                                        this.justificacionSeleccionada.getModoAdquisicion());
                                    pPersonaPredioPropiedad.setFechaTitulo(
                                        this.justificacionSeleccionada.getFechaTitulo());
                                    pPersonaPredioPropiedad.setNumeroTitulo(
                                        this.justificacionSeleccionada.getNumeroTitulo());
                                    pPersonaPredioPropiedad.setJustificacion(
                                        this.justificacionSeleccionada.getJustificacion());
                                    pPersonaPredioPropiedad.setTipo(this.justificacionSeleccionada.
                                        getTipo());
                                    pPersonaPredioPropiedad.setDocumentoSoporte(
                                        this.justificacionSeleccionada.getDocumentoSoporte());
                                    pPersonaPredioPropiedad.setLibro(this.justificacionSeleccionada.
                                        getLibro());
                                    pPersonaPredioPropiedad.setNumeroRegistro(
                                        this.justificacionSeleccionada.getNumeroRegistro());
                                    pPersonaPredioPropiedad.setPagina(
                                        this.justificacionSeleccionada.getPagina());
                                    pPersonaPredioPropiedad.setTomo(this.justificacionSeleccionada.
                                        getTomo());
                                    pPersonaPredioPropiedad.setValor(this.justificacionSeleccionada.
                                        getValor());

                                    this.getConservacionService().
                                        guardarActualizarPPersonaPredioPropiedad(
                                            pPersonaPredioPropiedad);

                                    this.addMensajeWarn("El predio " +
                                         pPersonaPredio.getPPredio().getNumeroPredial() +
                                         " ya tiene asociado una justificacion de propiedad," +
                                         " la cual ha sido reemplazada por la adicionada");

                                }
                                if (pPersonaPredioPropiedad.getCancelaInscribe() == null) {

                                    this.justificacionSeleccionada = this.getConservacionService().
                                        guardarJustificacionesPropiedad(
                                            this.justificacionSeleccionada,
                                            this.propietariosNuevosSeleccionados, this.usuario);
                                }
                            }

                        }
                    }

                    if (ppp == null || ppp.isEmpty()) {
                        this.justificacionSeleccionada = this.getConservacionService().
                            guardarJustificacionesPropiedad(this.justificacionSeleccionada,
                                this.propietariosNuevosSeleccionados, this.usuario);
                    }

                } else {

                    if (this.tramite.isEnglobeVirtual() &&
                        this.justificacionSeleccionadaSinModificar != null) {

                        this.justificacionSeleccionada = this.getConservacionService().
                            guardarJustificacionesPropiedad(this.justificacionSeleccionada,
                                this.propietariosNuevosSeleccionados, this.usuario);
                    }

                    if (!this.tramite.isEnglobeVirtual()) {
                        this.justificacionSeleccionada = this.getConservacionService().
                            guardarJustificacionesPropiedad(this.justificacionSeleccionada,
                                this.propietariosNuevosSeleccionados, this.usuario);
                    }
                }
            }

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                pp.setPPersonaPredioPropiedads(this.getConservacionService().
                    buscarPPersonaPredioPropiedadPorPPersonaPredio(pp));
                /* List<PPersonaPredioPropiedad> j=new ArrayList<PPersonaPredioPropiedad>(); for
                 * (PPersonaPredioPropiedad ppp:pp.getPPersonaPredioPropiedads()){ if
                 * (!ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())){
                 * j.add(ppp); } } for (PPersonaPredioPropiedad
                 * ppp:this.justificacionSeleccionada.getJustificaciones()){ if
                 * (ppp.getPPersonaPredio().getId().equals(pp.getId())){ j.add(ppp); } }
				pp.setPPersonaPredioPropiedads(j); */
            }

            //@modified by leidy.gonzalez:: #25173:: Se valida que para los englobes virtuales
            //se copien las justificaciones de los predios
            if ((this.getTramite().isSegundaDesenglobe() || this.getTramite().isQuintaMasivo() ||
                 this.getTramite().isEnglobeVirtual()) && this.copiarJustificacionAtodos) {
                boolean prediosSinPropietarios = false;
                for (PPredio p : prediosTramiteJustificacion) {
                    if (!this.getPredioSeleccionado().equals(p) && !p.isEsPredioFichaMatriz()) {
                        if (p.getPPersonaPredios().isEmpty()) {
                            prediosSinPropietarios = true;
                            this.addMensajeWarn(
                                "No es posible copiar la justificación porque el predio " +
                                 p.getNumeroPredial() + " no tiene propietarios");
                        }
                    }
                }

                if ((!prediosSinPropietarios && !this.getTramite().isEnglobeVirtual()) ||
                     (this.getTramite().isEnglobeVirtual())) {

                    for (PPredio p : prediosTramiteJustificacion) {
                        //felipe.cadena:: Si la justificacion ya esta relacionada al predio no se realiza ninguna accion.
                        if (this.justificacionSeleccionadaSinModificar != null) {
                            boolean existe = false;
                            justificacionesPredio = new LinkedList<PPersonaPredioPropiedad>();
                            for (PPersonaPredio persPred : p.getPPersonaPredios()) {
                                List<PPersonaPredioPropiedad> justificaciones = this.
                                    getConservacionService().
                                    buscarPPersonaPredioPropiedadPorPPersonaPredio(persPred);
                                for (PPersonaPredioPropiedad ppp : justificaciones) {
                                    if (ppp.getCancelaInscribe() == null ||
                                         EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(ppp.
                                            getCancelaInscribe()) ||
                                         EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(ppp.
                                            getCancelaInscribe())) {

                                        if (this.getTramite().isSegundaDesenglobe()) {
                                            justificacionesPredio.add(ppp);
                                        } else if (this.getTramite().isQuintaMasivo() && !p.
                                            isPredioOriginalTramite()) {
                                            justificacionesPredio.add(ppp);
                                        }
                                    }
                                }
                            }

                            for (PPersonaPredioPropiedad pppp : justificacionesPredio) {
                                JustificacionPropiedadDTO jp = new JustificacionPropiedadDTO(pppp);
                                if (this.justificacionSeleccionadaSinModificar.equals(jp)) {
                                    existe = true;
                                }
                            }
                            if (existe) {
                                continue;
                            }
                        }
                        List<PPersonaPredioPropiedad> pppTemp =
                            new ArrayList<PPersonaPredioPropiedad>();
                        /// No se incluye el predio seleccionado porque lineas antes ha sido almacenado
                        List<PPersonaPredio> ppps = new LinkedList<PPersonaPredio>();
                        if (this.getTramite().isEnglobeVirtual() &&
                             !this.getPredioSeleccionado().equals(p) && !p.isEsPredioFichaMatriz() &&
                             !EPredioEstado.CANCELADO.getCodigo().equals(p.getEstado())) {

                            for (PPersonaPredio ppp : p.getPPersonaPredios()) {
                                if (ppp.getCancelaInscribe() == null ||
                                     (ppp.getCancelaInscribe() != null &&
                                    !ppp.getCancelaInscribe().
                                        equals(EProyeccionCancelaInscribe.CANCELA.getCodigo()))) {

                                    //Se consulta si ya se le habia agregado alguna justificacion al predio
                                    pppTemp = this.getConservacionService().
                                        consultarJustificacionesPorPropetario(ppp.getId());
                                    //leidy.gonzalez::#42933::08/11/2017
                                    //Si existe la justificacion no permite agregar mas de una
                                    if (pppTemp != null && !pppTemp.isEmpty()) {

                                        for (PPersonaPredioPropiedad pPersonaPredioPropiedad
                                            : pppTemp) {

                                            if (pPersonaPredioPropiedad.getCancelaInscribe() != null &&
                                                 EProyeccionCancelaInscribe.INSCRIBE.getCodigo().
                                                    equals(pPersonaPredioPropiedad.
                                                        getCancelaInscribe())) {

                                                pPersonaPredioPropiedad.setFechaRegistro(
                                                    this.justificacionSeleccionada.
                                                        getFechaRegistro());
                                                pPersonaPredioPropiedad.setTipoTitulo(
                                                    this.justificacionSeleccionada.getTipoTitulo());
                                                pPersonaPredioPropiedad.setEntidadEmisora(
                                                    this.justificacionSeleccionada.
                                                        getEntidadEmisora());
                                                pPersonaPredioPropiedad.setModoAdquisicion(
                                                    this.justificacionSeleccionada.
                                                        getModoAdquisicion());
                                                pPersonaPredioPropiedad.setFechaTitulo(
                                                    this.justificacionSeleccionada.getFechaTitulo());
                                                pPersonaPredioPropiedad.setNumeroTitulo(
                                                    this.justificacionSeleccionada.getNumeroTitulo());
                                                pPersonaPredioPropiedad.setJustificacion(
                                                    this.justificacionSeleccionada.
                                                        getJustificacion());
                                                pPersonaPredioPropiedad.setTipo(
                                                    this.justificacionSeleccionada.getTipo());
                                                pPersonaPredioPropiedad.setDocumentoSoporte(
                                                    this.justificacionSeleccionada.
                                                        getDocumentoSoporte());
                                                pPersonaPredioPropiedad.setLibro(
                                                    this.justificacionSeleccionada.getLibro());
                                                pPersonaPredioPropiedad.setNumeroRegistro(
                                                    this.justificacionSeleccionada.
                                                        getNumeroRegistro());
                                                pPersonaPredioPropiedad.setPagina(
                                                    this.justificacionSeleccionada.getPagina());
                                                pPersonaPredioPropiedad.setTomo(
                                                    this.justificacionSeleccionada.getTomo());
                                                pPersonaPredioPropiedad.setValor(
                                                    this.justificacionSeleccionada.getValor());

                                                this.getConservacionService().
                                                    guardarActualizarPPersonaPredioPropiedad(
                                                        pPersonaPredioPropiedad);

                                                this.addMensajeWarn("El predio " +
                                                     p.getNumeroPredial() +
                                                    " ya tiene asociado una justificacion de propiedad," +
                                                    
                                                    " la cual ha sido reemplazada por la adicionada");

                                            } else if (pPersonaPredioPropiedad.getCancelaInscribe() ==
                                                null) {

                                                ppps.add(ppp);

                                                PPersonaPredio lista[] = new PPersonaPredio[ppps.
                                                    size()];
                                                lista = ppps.toArray(lista);

                                                this.getConservacionService().
                                                    guardarJustificacionesPropiedadEV(
                                                        this.justificacionSeleccionada, lista,
                                                        this.usuario);
                                            }
                                        }
                                        //Si no existe se almacena la justificacion    
                                    } else {
                                        ppps.add(ppp);
                                    }
                                }
                            }

                            if ((pppTemp == null || pppTemp.isEmpty())) {

                                PPersonaPredio lista[] = new PPersonaPredio[ppps.size()];
                                lista = ppps.toArray(lista);

                                this.getConservacionService().guardarJustificacionesPropiedadEV(
                                    this.justificacionSeleccionada, lista, this.usuario);
                            }

                        }
                        if (!this.getPredioSeleccionado().equals(p) && !p.isEsPredioFichaMatriz() &&
                             !this.getTramite().isEnglobeVirtual()) {

                            for (PPersonaPredio ppp : p.getPPersonaPredios()) {
                                if (ppp.getCancelaInscribe() == null ||
                                     !ppp.getCancelaInscribe().equals(
                                        EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                    ppps.add(ppp);
                                }
                            }
                            PPersonaPredio lista[] = new PPersonaPredio[ppps.size()];
                            lista = ppps.toArray(lista);

                            this.getConservacionService().guardarJustificacionesPropiedad(
                                this.justificacionSeleccionada, lista, this.usuario);
                        }
                    }
                }
            }

            //jonathan.chacon :redmine 16515 ajuste para primera predio fiscal
            if (this.getTramite().isPrimera() && this.tramite.getPredio().getTipoCatastro().equals(
                EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                this.getPredioSeleccionado().setFechaInscripcionCatastral(justificacionSeleccionada.
                    getFechaTitulo());
                this.getConservacionService().guardarActualizarPPredio(this.getPredioSeleccionado());
            }

            // Guardar justificaciones a actualizar
            for (PPersonaPredioPropiedad pppp : justificacionesActualizar) {
                this.getConservacionService().guardarActualizarPPersonaPredioPropiedad(pppp);
            }

            if ((this.getTramite().isSegunda() ||
                 this.getTramite().isQuintaMasivo() ||
                 this.getTramite().isQuintaNuevo() ||
                 this.getTramite().isQuintaOmitidoNuevo() ||
                 this.getTramite().isCancelacionPredio()) &&
                 !this.getTramite().isDesenglobeEtapas()) {

                for (PPredio p : prediosTramiteJustificacion) {

                    if (justificacionSeleccionada.getFechaTitulo() != null) {
                        if (this.copiarJustificacionAtodos) {

                            //@modified by leidy.gonzalez:: #20429 Se valida que al copiar las justificaciones, exista propietario
                            //asociado a todos los predios resultantes a cancelar. 
                            if (this.getTramite().isCancelacionMasiva()) {
                                if (p.getId() != null) {

                                    List<PPersonaPredio> personasPredio = this.
                                        getConservacionService().
                                        buscarPPersonaPrediosPorPredioIdInscritos(p.getId());

                                    if (personasPredio == null ||
                                        personasPredio.isEmpty()) {

                                        this.addMensajeError("El predio" +
                                             p.getNumeroPredial() +
                                            " no tiene asociado propietario, Verifique");
                                        return;
                                    }
                                }
                            } else {

                                /// No se incluye el predio seleccionado porque lineas antes ha sido almacenado
                                if (this.getTramite().isEnglobeVirtual() &&
                                     !this.getPredioSeleccionado().equals(p) && !p.
                                    isEsPredioFichaMatriz() &&
                                     !EPredioEstado.CANCELADO.getCodigo().equals(p.getEstado())) {

                                    p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                        getFechaTitulo());
                                    this.proyectarConservacionMB.getPredioSeleccionado().
                                        setFechaInscripcionCatastral(justificacionSeleccionada
                                            .getFechaTitulo());
                                    this.getConservacionService().guardarActualizarPPredio(p);

                                } else {
                                    p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                        getFechaTitulo());
                                    this.proyectarConservacionMB.getPredioSeleccionado().
                                        setFechaInscripcionCatastral(justificacionSeleccionada
                                            .getFechaTitulo());
                                    this.getConservacionService().guardarActualizarPPredio(p);
                                }
                            }

                            if (p.getCirculoRegistral() == null &&
                                 p.getNumeroRegistro() == null &&
                                 !EPredioCondicionPropiedad.CP_0.getCodigo().equals(
                                    proyectarConservacionMB.getPredioSeleccionado().
                                        getCondicionPropiedad())) {
                                p.setCirculoRegistral(proyectarConservacionMB.
                                    getPredioSeleccionado().getCirculoRegistral());
                                p.setNumeroRegistro(proyectarConservacionMB.getPredioSeleccionado().
                                    getNumeroRegistro());
                            }

                        } else {
                            if (this.getTramite().isCancelacionMasiva()) {

                                //@modified by leidy.gonzalez:: #20429 Se valida que al copiar las justificaciones, exista propietario
                                //asociado a todos los predios resultantes a cancelar. 
                                if (p.getId() != null) {

                                    List<PPersonaPredio> personasPredio = this.
                                        getConservacionService().
                                        buscarPPersonaPrediosPorPredioIdInscritos(p.getId());

                                    if (personasPredio == null ||
                                        personasPredio.isEmpty()) {

                                        this.addMensajeError("El predio" +
                                             p.getNumeroPredial() +
                                            " no tiene asociado propietario, Verifique");
                                        return;
                                    }
                                }

                            } else {
                                if (p.isEsPredioFichaMatriz() && !this.getPredioSeleccionado().
                                    equals(p)) {
                                    p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                        getFechaTitulo());
                                    this.getPredioSeleccionado().setFechaInscripcionCatastral(
                                        justificacionSeleccionada.getFechaTitulo());
                                }

                                if (this.getPredioSeleccionado().equals(p)) {
                                    p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                        getFechaTitulo());
                                    this.getPredioSeleccionado().setFechaInscripcionCatastral(
                                        justificacionSeleccionada.getFechaTitulo());
                                }

                                this.getConservacionService().guardarActualizarPPredio(p);
                            }

                        }

                    }
                }
                // @modified by leidy.gonzalez:: #20429:: Se valida que la
                // justificacion de Propiedad se asocie a los predios que no fueron cancelados
                List<PPersonaPredio> listPersonasPredioSinCanc = new ArrayList<PPersonaPredio>();
                if (this.getTramite().isCancelacionMasiva()) {
                    if (prediosTramiteJustificacion != null) {
                        for (PPredio p : prediosTramiteJustificacion) {
                            if (this.copiarJustificacionAtodos) {

                                this.justificacionSeleccionada = this.getConservacionService().
                                    guardarJustificacionesPropiedadCancelacionMasiva(
                                        this.justificacionSeleccionada,
                                        this.justificacionSeleccionadaSinModificar,
                                        this.propietariosNuevosSeleccionados, this.usuario);

                                p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                    getFechaTitulo());
                                this.proyectarConservacionMB.getPredioSeleccionado().
                                    setFechaInscripcionCatastral(justificacionSeleccionada
                                        .getFechaTitulo());
                            } else {

                                for (PPredio pSinCancelar : prediosSinCancelar) {
                                    p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                        getFechaTitulo());
                                    List<PPredioDireccion> direcciones = pSinCancelar.
                                        getPPredioDireccions();

                                    pSinCancelar.setPPredioDireccions(null);
                                    this.getConservacionService().guardarActualizarPPredio(
                                        pSinCancelar);
                                    pSinCancelar.setPPredioDireccions(direcciones);

                                    if (pSinCancelar.getPPersonaPredios() != null && !pSinCancelar.
                                        getPPersonaPredios().isEmpty()) {
                                        listPersonasPredioSinCanc.addAll(pSinCancelar.
                                            getPPersonaPredios());
                                    }
                                }
                                for (PPredio pSinCancelar : prediosTramiteJustificacion) {
                                    p.setFechaInscripcionCatastral(justificacionSeleccionada.
                                        getFechaTitulo());
                                    this.getConservacionService().guardarActualizarPPredio(
                                        pSinCancelar);

                                    List<PPersonaPredio> personasPredio =
                                        this.getConservacionService().
                                            buscarPPersonaPrediosPorPredioIdInscritos(pSinCancelar.
                                                getId());

                                    if (personasPredio != null &&
                                         !personasPredio.isEmpty()) {

                                        pSinCancelar.setPPersonaPredios(personasPredio);

                                        listPersonasPredioSinCanc.addAll(personasPredio);
                                    }
                                }

                                // @modified by leidy.gonzalez:: #20429:: Se valida que solo
                                // se agregue la justificacion a los predios no cancelados
                                if (!listPersonasPredioSinCanc.isEmpty()) {

                                    this.justificacionSeleccionada = this.getConservacionService().
                                        guardarJustificacionesPropiedadPrediosNoCancelados(
                                            this.justificacionSeleccionada,
                                            this.justificacionSeleccionadaSinModificar,
                                            listPersonasPredioSinCanc, this.usuario);

                                    for (PPersonaPredio pPersonaPredio : this.
                                        getPredioSeleccionado().getPPersonaPredios()) {
                                        List<PPersonaPredioPropiedad> ppp = this.
                                            getConservacionService().
                                            consultarJustificacionesPorPropetario(pPersonaPredio.
                                                getId());
                                        pPersonaPredio.setPPersonaPredioPropiedads(ppp);
                                    }

                                }
                            }
                        }
                    }

                    this.getPredioSeleccionado().setFechaInscripcionCatastral(
                        justificacionSeleccionada.getFechaTitulo());
                    this.getConservacionService().guardarActualizarPPredio(this.
                        getPredioSeleccionado());
                }

                List<PPredio> prediosProyeccion = this.getConservacionService()
                    .buscarPPrediosPorTramiteId((this.tramite.getId()));

                for (PPredio pp : prediosProyeccion) {
                    if (justificacionSeleccionada.getFechaTitulo() != null) {
                        if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pp.
                            getCancelaInscribe())) {

                            pp.setFechaInscripcionCatastral(justificacionSeleccionada.
                                getFechaTitulo());
                            this.getPredioSeleccionado().setFechaInscripcionCatastral(
                                justificacionSeleccionada.getFechaTitulo());
                            this.getConservacionService().guardarPPredio(pp);
                        }
                    }
                }
            }
            //@modified by leidy.gonzalez:: #20429:: Valida que la  fecha de titulo este asociada a los predios resultantes de la cancelacion
            if (this.getTramite().isCancelacionMasiva()) {
                if (prediosTramiteJustificacion != null &&
                     !prediosTramiteJustificacion.isEmpty()) {

                    for (PPredio pPredio : prediosTramiteJustificacion) {
                        if (pPredio.getFechaInscripcionCatastral() != null &&
                            this.justificacionSeleccionada.getFechaTitulo() != null) {

                            if (this.justificacionSeleccionada.getFechaTitulo().compareTo(
                                pPredio.getFechaInscripcionCatastral()) < 0 ||
                                this.justificacionSeleccionada.getFechaTitulo().compareTo(
                                    pPredio.getFechaInscripcionCatastral()) > 0) {

                                pPredio.setFechaInscripcionCatastral(justificacionSeleccionada.
                                    getFechaTitulo());
                            }

                        }

                        if (pPredio.getPPersonaPredios() != null &&
                            !pPredio.getPPersonaPredios().isEmpty()) {

                            for (PPersonaPredio pPersonaPredioTemp : pPredio.getPPersonaPredios()) {

                                if (pPersonaPredioTemp.getFechaInscripcionCatastral() != null) {

                                    if (this.justificacionSeleccionada.getFechaTitulo()
                                        .compareTo(pPersonaPredioTemp.getFechaInscripcionCatastral()) <
                                        0 ||
                                         this.justificacionSeleccionada.getFechaTitulo()
                                            .compareTo(pPersonaPredioTemp.
                                                getFechaInscripcionCatastral()) > 0) {

                                        pPersonaPredioTemp.setFechaInscripcionCatastral(
                                            justificacionSeleccionada
                                                .getFechaTitulo());

                                        pPersonaPredioTemp = this.getConservacionService().
                                            guardarActualizarPPersonaPredio(pPersonaPredioTemp);
                                    }
                                } else {
                                    pPersonaPredioTemp.setFechaInscripcionCatastral(
                                        justificacionSeleccionada
                                            .getFechaTitulo());
                                    pPersonaPredioTemp = this.getConservacionService().
                                        guardarActualizarPPersonaPredio(pPersonaPredioTemp);

                                }
                            }
                        }
                    }
                }
                // @modified by leidy.gonzalez:: #20429:: Se valida que solo
                // se pueda adicionar una justificacion de Propiedad
                for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {

                    if (pp.getPPersonaPredioPropiedads() != null && !pp.
                        getPPersonaPredioPropiedads().isEmpty()) {
                        this.banderaAdicionaJustificacion = false;

                    } else {
                        this.banderaAdicionaJustificacion = true;
                    }
                }

            }

            this.addMensajeInfo("Datos almacenados exitosamente");
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Error guardando la nueva justificación");
        }
        this.justificacionSeleccionadaSinModificar = null;
    }

    public boolean validarSeccion() {

        if (this.getTramite().isModificacionInscripcionCatastral()) {
            return true;
        }

        boolean valido = true;
        for (PPersonaPredio pp : this.propietariosMB.getPropietariosNuevos()) {
            if (pp.getCancelaInscribe() != null &&
                 pp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) &&
                 (pp.getPPersonaPredioPropiedads() == null || pp.getPPersonaPredioPropiedads().
                isEmpty())) {
                valido = false;
                this.addMensajeError(
                    "Persiste al menos un propietario/poseedor sin justificación de derecho de propiedad");
                break;
            }
        }
        for (PPersonaPredioPropiedad ppp : this.getPersonaPredioPropiedadInscritos()) {
            if (ppp.getCancelaInscribe() != null &&
                 ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) &&
                 (ppp.getModoAdquisicion() == null || !ppp.getModoAdquisicion().equals("MIGRACION")) &&
                 (ppp.getDocumentoSoporte() == null ||
                 ppp.getDocumentoSoporte().getArchivo() == null ||
                 ppp.getDocumentoSoporte().getArchivo().trim().equals(""))) {
                valido = false;
                this.addMensajeError("Persiste al menos una justificación sin documento adjunto");
                break;
            }
        }
        if (!this.getTramite().isQuintaMasivo()) {
            for (PPersonaPredioPropiedad ppp : this.getPersonaPredioPropiedadInscritos()) {
                if (ppp.getModoAdquisicion() != null && ppp.getModoAdquisicion().equals(
                    Constantes.MODO_ADQUISICION_TRADICION) && (ppp.getValor() == null)) {
                    valido = false;
                    this.addMensajeError(
                        "El valor de compra es obligatorio cuando el modo de adquisición es por tradición");
                    break;
                }
            }
        }
        return valido;
    }

    /**
     * Llamado al método que realiza los cambios en las tablas la proyección.
     */
    public void validarGuardar() {

        boolean resultadoValidacion = this.validarSeccion();
        proyectarConservacionMB.getValidezSeccion().put(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD,
            resultadoValidacion);

        /**
         * El método debe actualizar los personaPredio con las referencias insertadas a persona
         * predio propiedad y además de esto debe crear los registros para los
         * personaPredioPropiedad inscritos
         */
        try {
            this.getConservacionService().proyectarJustificacionDerechoPropiedad(
                this.usuario, this.getPersonaPredioPropiedadInscritos());
            if (!this.tramite.isQuintaMasivo()) {
                this.addMensajeInfo(
                    "Justificación de los derechos de propiedad almacenados de forma exitosa");
            }
        } catch (Exception e) {
            proyectarConservacionMB.getValidezSeccion().put(
                ESeccionEjecucion.JUSTIFICACION_PROPIEDAD, false);
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(
                "Hubo problemas guardando la justificación de los derechos de propiedad.");
        }
    }

    public void asociarNuevoDocumento() {
        tramite = proyectarConservacionMB.getTramite();
        Predio predio = tramite.getPredio();
        List<TramitePredioEnglobe> pe = tramite.getTramitePredioEnglobes();
        this.tramite.setTramiteDocumentacions(this.getTramiteService()
            .obtenerTramiteDocumentacionPorIdTramite(this.tramite.getId()));
        tramite.setTramitePredioEnglobes(pe);
        tramite.setPredio(predio);

        this.tramiteDocumentacionSeleccionado = null;
    }

    public void consultarHistoricoJustificacion() {
        try {
            this.predioPropietariosHis = this.getConservacionService()
                .getPredioPropietarioHisByPredio(this.getPredioSeleccionado().getId());
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Error al realizar la consulta de históricos");
        }
    }

    /**
     * Organiza la lista de los círculos registrales a mostrar en la GUI.
     *
     * @modified by leidy.gonzalez Se agrega validación para los tramites de quinta nuevo y quinta
     * nuevo omitido para que cuando se elimine la justificación del predio seleccionado y la fecha
     * de titulo de esta justificación es igual a la fecha de inscripción del predio original se
     * elimine esta fecha de inscripción catastral
     * @modified by leidy.gonzalez 08/08/2014 Se valida la eliminación de la justificación para
     * desenglobes.
     */
    public void eliminarJustificacion() {

        //se actualiza el estado de los predio el tramite
        this.proyectarConservacionMB.cargarPrediosDesenglobe();

        try {
            for (PPersonaPredioPropiedad ppp : this.justificacionSeleccionada.getJustificaciones()) {
                this.getConservacionService().eliminarPPersonaPredioPropiedad(ppp);
            }

            for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                pp.setPPersonaPredioPropiedads(this.getConservacionService().
                    buscarPPersonaPredioPropiedadPorPPersonaPredio(pp));
            }

            if (this.getTramite().isQuintaNuevo() || this.getTramite().isQuintaOmitidoNuevo() ||
                this.getTramite().isSegunda() || this.getTramite().isCancelacionPredio()) {
                for (PPredio p : entradaProyeccionMB.getPrediosResultantes()) {
                    if (this.getPredioSeleccionado().equals(p)) {
                        if (p.getFechaInscripcionCatastral() != null && justificacionSeleccionada.
                            getFechaTitulo().equals(p.getFechaInscripcionCatastral())) {
                            p.setFechaInscripcionCatastral(null);
                            this.getConservacionService().guardarActualizarPPredio(p);

                            break;
                        }
                    }
                }
            }

            // Elimina el registro de la base de datos
            /* this.getConservacionService()
             * .eliminarPPersonaPredioPropiedad(this.personaPredioPropiedadSeleccionado);
             *
             * // Elimina el registro del predio del trámite seleccionado for (PPersonaPredio ppp :
             * this.predioSeleccionado .getPPersonaPredios()) { for (PPersonaPredioPropiedad
             * pPersPredProp : ppp .getPPersonaPredioPropiedads()) { if
             * (pPersPredProp.getId().equals( this.personaPredioPropiedadSeleccionado.getId())) {
             * ppp.getPPersonaPredioPropiedads().remove(pPersPredProp); break; } } }
             *
             * // Elimina de la lista que se grafica en la tabla de la pantalla de //
             * justificaciones this.getPersonaPredioPropiedadInscritos()
					.remove(this.personaPredioPropiedadSeleccionado); */
            //Ahora es necesario que se recalcule los valores de la tabla p_predio_avaluo_catastral
            //después de cancelar o eliminar la justificación de propiedad
            this.getFormacionService()
                .revertirProyeccionAvaluosParaUnPredio(
                    this.getTramite().getId(),
                    this.getPredioSeleccionado().getId());

            // @modified by leidy.gonzalez:: #20429:: Se valida que solo
            // se pueda adicionar una justificacion de Propiedad
            if (this.getTramite().isCancelacionMasiva()) {
                for (PPersonaPredio pp : this.getPredioSeleccionado().getPPersonaPredios()) {
                    if (pp.getPPersonaPredioPropiedads() != null && !pp.
                        getPPersonaPredioPropiedads().isEmpty()) {
                        this.banderaAdicionaJustificacion = false;
                    } else {
                        this.banderaAdicionaJustificacion = true;
                    }
                }

                if (this.entradaProyeccionMB.getPrediosResultantes() != null &&
                     !this.entradaProyeccionMB.getPrediosResultantes().isEmpty()) {
                    for (PPredio pPredio : this.entradaProyeccionMB.getPrediosResultantes()) {

                        if (pPredio.getPPersonaPredios() != null && !pPredio.getPPersonaPredios().
                            isEmpty()) {

                            for (PPersonaPredio pPersonaPredioSinCanc : pPredio.getPPersonaPredios()) {

                                if (pPersonaPredioSinCanc.getPPersonaPredioPropiedads() != null &&
                                     !pPersonaPredioSinCanc.getPPersonaPredioPropiedads().isEmpty()) {

                                    for (PPersonaPredioPropiedad pPersonaPredioPropietarioSinCanc
                                        : pPersonaPredioSinCanc.getPPersonaPredioPropiedads()) {
                                        this.getConservacionService().
                                            eliminarPPersonaPredioPropiedad(
                                                pPersonaPredioPropietarioSinCanc);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            // @modified by leidy.gonzalez:: #25173:: Se valida que solo
            // se eliminen las justificaciones de Propiedad adicionadas
            if (this.getTramite().isEnglobeVirtual()) {

                if (this.proyectarConservacionMB.getpPrediosDesenglobe() != null &&
                     !this.proyectarConservacionMB.getpPrediosDesenglobe().isEmpty()) {

                    for (PPredio pPredio : this.proyectarConservacionMB.getpPrediosDesenglobe()) {

                        if (!pPredio.isEsPredioFichaMatriz() &&
                             !EPredioEstado.CANCELADO.getCodigo().equals(pPredio.getEstado()) &&
                             pPredio.getPPersonaPredios() != null && !pPredio.getPPersonaPredios().
                            isEmpty()) {

                            for (PPersonaPredio pPersonaPredio : pPredio.getPPersonaPredios()) {

                                if (pPersonaPredio.getPPersonaPredioPropiedads() != null &&
                                     !pPersonaPredio.getPPersonaPredioPropiedads().isEmpty()) {

                                    for (PPersonaPredioPropiedad justificaciones : pPersonaPredio.
                                        getPPersonaPredioPropiedads()) {

                                        if (justificaciones.getCancelaInscribe() != null &&
                                             !justificaciones.getCancelaInscribe().isEmpty() &&
                                             EProyeccionCancelaInscribe.INSCRIBE.getCodigo()
                                                .equals(justificaciones.getCancelaInscribe())) {

                                            this.getConservacionService().
                                                eliminarPPersonaPredioPropiedad(justificaciones);

                                        }
                                    }
                                }

                            }

                        }

                    }
                }
            }

            this.addMensajeInfo("Justificación de derecho de propiedad eliminada exitosamente");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(
                "Fallo al eliminar la justificación de derecho de propiedad seleccionada");
        }
    }

    /**
     * Listado de propietarios nuevos tomado de la pestaña de propietarios. Usado al editar la
     * justificación.
     *
     * @return
     */
    public List<PPersonaPredio> getPropietariosNuevos() {
        return this.propietariosMB.getPropietariosNuevos();
    }

    /**
     * Actualiza el estado de la entidad, basado en el tipo titulo.
     *
     * @author felipe.cadena
     */
    public void actualizarEntidad() {
        if (this.justificacionSeleccionada != null &&
            this.justificacionSeleccionada.getTipoTitulo() != null) {
            this.banderaCartaVenta = this.justificacionSeleccionada.getTipoTitulo().
                equals(EPersonaPredioPropiedadTipoTitulo.CARTA_VENTA.getValor());
        } else {
            this.banderaCartaVenta = false;
        }

    }

    /* public void setupJustificacionEdition() { this.propietariosNuevosSeleccionados = new
     * PPersonaPredio[1]; for (PPersonaPredio ppp : this.getPropietariosNuevos()) { if
     * (ppp.getId().equals( this.personaPredioPropiedadSeleccionado.getPPersonaPredio() .getId())) {
     * this.propietariosNuevosSeleccionados[0] = ppp; } } this.departamento =
     * this.personaPredioPropiedadSeleccionado.getDepartamento(); this.updateDepartamentos();
     * this.municipioTemp = this.personaPredioPropiedadSeleccionado.getMunicipio();
     * this.updateMunicipios(); this.adicionando=false;
	} */
    /**
     *
     */
    public void setupJustificacionEdition() {
        try {
            //@modified by leidy.gonzalez:: #18225:: 06/07/2016 Se verifica que la fecha de titulo 
            // a visualizar sea la fecha de inscripcion del predio seleccionado
//        	if(this.getPredioSeleccionado()!= null && this.getPredioSeleccionado().getFechaInscripcionCatastral() != null){
//        		this.justificacionSeleccionada.setFechaTitulo(this.getPredioSeleccionado().getFechaInscripcionCatastral());
//        	}
            this.justificacionSeleccionadaSinModificar =
                (JustificacionPropiedadDTO) this.justificacionSeleccionada.clone();
        } catch (CloneNotSupportedException ex) {
            LOGGER.debug("Error al respaldar la justificacion base");
        }
    }

}
