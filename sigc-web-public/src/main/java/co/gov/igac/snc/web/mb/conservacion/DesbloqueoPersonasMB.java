package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEntidadEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace de MB para manejar la consulta y el desbloqueo de personas
 *
 * @author juan.agudelo
 *
 * @modified by leidy.gonzalez:: #12497:: 02/06/2015 Se agregan banderas para activar o desactivar
 * paneles
 */
@Component("desbloqueoPersona")
@Scope("session")
public class DesbloqueoPersonasMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DesbloqueoPersonasMB.class);

    /*
     * Interfaces de servicio
     */
    @Autowired
    private IContextListener contexto;

    @Autowired
    private GeneralMB generalService;

    /**
     * lista de PersonaBloqueo
     */
    private List<PersonaBloqueo> personaBloqueada;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Predio a desbloquear
     */
    private String numeroIdentificacion;

    /**
     * Bandera de visualización del boton desbloquear
     */
    private boolean banderaBotonDesbloquear;
    private boolean banderaBotonGuardar;

    /**
     * Bandera de visualización de la tabla de resultado de la busqueda de bloqueos existentes sobre
     * un predio
     */
    private boolean banderaBuscar;

    /**
     * Lista que contiene la seleccion de predios bloqueados para desbloquear
     */
    private PersonaBloqueo personaBloqueoSeleccionada;

    /**
     * Motivo del desbloqueo de una persona
     */
    private String motivoDesbloqueo;

    /**
     * Ruta del archivo cargado
     */
    private String rutaMostrar;

    /**
     * Archivo resultado cargado
     */
    private File archivoResultado;

    /**
     * Mensaje de salida del bloqueo de persona
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteDesbloqueo;

    private boolean botonDesbloquear;

    /**
     * Variable de control de archivo de desbloqueo subido
     */
    private int conteoArchivoDesbloqueo;

    /**
     * Tipo de identificación para la busqueda de personas bloqueadas
     */
    private String tipoIdentificacion;

    /**
     * Tipo de identificación de una persona
     */
    private List<SelectItem> personaTiposIdentificacion;

    private boolean banderaGestionDesbloqueoPersona;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosBuscadorItemList;

    /**
     * Listas de items para el combo de tipo de bloqueo
     */
    private List<SelectItem> tiposBloqueoItemList;

    /**
     * valor seleccionado en el combo tipo de bloqueo
     */
    private String selectedTipoBloqueo;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamento;
    private String selectedMunicipio;

    /**
     * Determina si la opción de restitución de tierras fue seleccionada
     */
    private boolean selectedRestitucionTierras;

    /**
     * Lista de tipo {@link Entidad} con las entidades que se encuentran activas
     */
    private List<SelectItem> entidades;

    /**
     * Variable para asociarel id de la {
     *
     * @Entidad} que ordena el bloqueo del {@link Predio}
     */
    private Long idEntidadBloqueo;

    /**
     * Bandera de visualización del boton de busqueda
     */
    private boolean banderaEjecutaBusqueda;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Orden para la listas de departamento y municipio
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;

    // ------------- methods ------------------------------------
    public Map<String, List<SelectItem>> getMunicipiosDeptos() {
        return municipiosDeptos;
    }

    public void setMunicipiosDeptos(Map<String, List<SelectItem>> municipiosDeptos) {
        this.municipiosDeptos = municipiosDeptos;
    }

    public List<SelectItem> getMunicipiosBuscadorItemList() {
        return municipiosBuscadorItemList;
    }

    public void setMunicipiosBuscadorItemList(List<SelectItem> municipiosBuscadorItemList) {
        this.municipiosBuscadorItemList = municipiosBuscadorItemList;
    }

    public String getSelectedTipoBloqueo() {
        return selectedTipoBloqueo;
    }

    public void setSelectedTipoBloqueo(String selectedTipoBloqueo) {
        this.selectedTipoBloqueo = selectedTipoBloqueo;
    }

    public String getSelectedDepartamento() {
        return selectedDepartamento;
    }

    public void setSelectedDepartamento(String selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public String getSelectedMunicipio() {
        return selectedMunicipio;
    }

    public void setSelectedMunicipio(String selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public boolean isSelectedRestitucionTierras() {
        return selectedRestitucionTierras;
    }

    public void setSelectedRestitucionTierras(boolean selectedRestitucionTierras) {
        this.selectedRestitucionTierras = selectedRestitucionTierras;
    }

    public List<SelectItem> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<SelectItem> entidades) {
        this.entidades = entidades;
    }

    public List<SelectItem> getTiposBloqueoItemList() {
        return tiposBloqueoItemList;
    }

    public void setTiposBloqueoItemList(List<SelectItem> tiposBloqueoItemList) {
        this.tiposBloqueoItemList = tiposBloqueoItemList;
    }

    public boolean isBanderaEjecutaBusqueda() {
        return banderaEjecutaBusqueda;
    }

    public void setBanderaEjecutaBusqueda(boolean banderaEjecutaBusqueda) {
        this.banderaEjecutaBusqueda = banderaEjecutaBusqueda;
    }

    public Long getIdEntidadBloqueo() {
        return idEntidadBloqueo;
    }

    public void setIdEntidadBloqueo(Long idEntidadBloqueo) {
        this.idEntidadBloqueo = idEntidadBloqueo;
    }

    public List<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public boolean isBanderaGestionDesbloqueoPersona() {
        return banderaGestionDesbloqueoPersona;
    }

    public void setBanderaGestionDesbloqueoPersona(
        boolean banderaGestionDesbloqueoPersona) {
        this.banderaGestionDesbloqueoPersona = banderaGestionDesbloqueoPersona;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public List<SelectItem> getPersonaTiposIdentificacion() {
        return personaTiposIdentificacion;
    }

    public void setPersonaTiposIdentificacion(
        List<SelectItem> personaTiposIdentificacion) {
        this.personaTiposIdentificacion = personaTiposIdentificacion;
    }

    public int getConteoArchivoDesbloqueo() {
        return conteoArchivoDesbloqueo;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return mensajeSalidaConfirmationDialog;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public Documento getDocumentoSoporteDesbloqueo() {
        return documentoSoporteDesbloqueo;
    }

    public void setDocumentoSoporteDesbloqueo(Documento documentoSoporteDesbloqueo) {
        this.documentoSoporteDesbloqueo = documentoSoporteDesbloqueo;
    }

    public void setConteoArchivoDesbloqueo(int conteoArchivoDesbloqueo) {
        this.conteoArchivoDesbloqueo = conteoArchivoDesbloqueo;
    }

    public boolean isBotonDesbloquear() {
        return botonDesbloquear;
    }

    public void setBotonDesbloquear(boolean botonDesbloquear) {
        this.botonDesbloquear = botonDesbloquear;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public String getMotivoDesbloqueo() {
        return motivoDesbloqueo;
    }

    public void setMotivoDesbloqueo(String motivoDesbloqueo) {
        this.motivoDesbloqueo = motivoDesbloqueo;
    }

    public List<PersonaBloqueo> getPersonaBloqueada() {
        return personaBloqueada;
    }

    public void setPersonaBloqueada(List<PersonaBloqueo> personaBloqueada) {
        this.personaBloqueada = personaBloqueada;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public PersonaBloqueo getPersonaBloqueoSeleccionada() {
        return personaBloqueoSeleccionada;
    }

    public void setPersonaBloqueoSeleccionada(
        PersonaBloqueo personaBloqueoSeleccionada) {
        this.personaBloqueoSeleccionada = personaBloqueoSeleccionada;
    }

    public boolean isBanderaBuscar() {
        return banderaBuscar;
    }

    public void setBanderaBuscar(boolean banderaBuscar) {
        this.banderaBuscar = banderaBuscar;
    }

    public boolean isBanderaBotonDesbloquear() {
        return banderaBotonDesbloquear;
    }

    public void setBanderaBotonDesbloquear(boolean banderaBotonDesbloquear) {
        this.banderaBotonDesbloquear = banderaBotonDesbloquear;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("BloqueoPredioMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.banderaBotonDesbloquear = true;
        this.banderaBotonGuardar = false;
        this.banderaBuscar = false;
        this.botonDesbloquear = false;
        this.conteoArchivoDesbloqueo = 0;
        this.personaTiposIdentificacion = new ArrayList<SelectItem>();
        this.personaTiposIdentificacion = this.generalService
            .getPersonaTipoIdentificacionV();

        this.personaBloqueada = new ArrayList<PersonaBloqueo>();
        this.mensajeSalidaConfirmationDialog = "El desmarque" +
             " se realizó exitosamente." + " Desea continuar" +
             " en el módulo de desmarcar personas?";

        this.banderaGestionDesbloqueoPersona = false;
        this.banderaEjecutaBusqueda = false;

        this.tiposBloqueoItemList = new ArrayList<SelectItem>();

        List<Dominio> dominio = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.PREDIO_TIPO_DESBLOQUEO);

        for (Dominio dom : dominio) {
            this.tiposBloqueoItemList.add(new SelectItem(dom.getCodigo(), dom.getValor()));
        }

        this.selectedTipoBloqueo = "FALLADO";

        this.cargarDepartamentos();

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        this.ordenDepartamentos = EOrden.NOMBRE;

    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton Buscar
     */
    public void buscar() {

        Entidad entidad = null;

        this.personaBloqueada = this.getConservacionService()
            .buscarPersonaBloqueoByNumeroIdentificacion(
                this.numeroIdentificacion, this.tipoIdentificacion);

        for (PersonaBloqueo pb : this.personaBloqueada) {
            entidad = this.getConservacionService().findEntidadById(pb.getEntidadId());
            pb.setNombreEntidad(entidad.getNombre());
        }

        this.banderaBuscar = true;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para guardar los datos de persona bloqueo ejecutado sobre el boton desbloquear
     */
    public void desbloquearPB() {

        Map filtros;
        List<Actividad> actividades;
        List<Tramite> tramites;
        Entidad entidad;
        RequestContext context = RequestContext.getCurrentInstance();
        boolean datosValidos = true;

        Date fechaSistema = new Date();

        if (this.conteoArchivoDesbloqueo == 0) {
            String mensaje = "No se adjuntó un documento de soporte para desmarcar" +
                 " Por favor corrija los datos e intente nuevamente. ";
            this.addMensajeError(mensaje);
            datosValidos = false;
        }

        if (this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo() != null &&
             fechaSistema.before(this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo())) {
            String mensaje = "Fecha envío no puede ser mayor a la fecha actual, verifique.";
            this.addMensajeError(mensaje);
            datosValidos = false;
        }

        if (this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo() != null &&
             fechaSistema.before(this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo())) {
            String mensaje = "La fecha de recibido no puede ser mayor a la fecha actual, verifique";
            this.addMensajeError(mensaje);
            datosValidos = false;
        }

        if (this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo() != null &&
            this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo() != null &&
             this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo().before(
                this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo())) {
            String mensaje =
                "La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique.";
            this.addMensajeError(mensaje);
            datosValidos = false;
        }

        if (!esNumeroRadicacionValido(this.personaBloqueoSeleccionada.
            getNumeroRadicacionDesbloqueo(), usuario)) {
            String mensaje = "El número de radicado no se encontró en el sistema. Por favor revise.";
            this.addMensajeError(mensaje);
            datosValidos = false;
        }

        if (datosValidos) {

            this.personaBloqueoSeleccionada.setDocumentoSoporteDesbloqueo(
                this.documentoSoporteDesbloqueo);

            try {

                if (this.selectedRestitucionTierras == true) {
                    this.personaBloqueoSeleccionada.setRestitucionTierrasDesbloqueo("SI");
                } else {
                    this.personaBloqueoSeleccionada.setRestitucionTierrasDesbloqueo("NO");
                }

                this.personaBloqueoSeleccionada.setTipoDesbloqueo(this.selectedTipoBloqueo);

                Municipio m = new Municipio();
                m.setCodigo(this.selectedMunicipio);
                Departamento d = new Departamento();
                d.setCodigo(this.selectedDepartamento);
                this.personaBloqueoSeleccionada.setMunicipioDesbloqueo(m);
                this.personaBloqueoSeleccionada.setDepartamentoDesbloqueo(d);

                if (this.idEntidadBloqueo != null) {
                    entidad = this.getConservacionService().findEntidadById(this.idEntidadBloqueo);
                    this.personaBloqueoSeleccionada.setEntidadDesbloqueo(entidad);
                }

                List<PersonaBloqueo> personaParaDesbloquear = new ArrayList<PersonaBloqueo>();
                personaParaDesbloquear.add(this.personaBloqueoSeleccionada);
                this.getConservacionService().actualizarPersonaDesbloqueo(usuario,
                    personaParaDesbloquear);
                this.personaBloqueada.remove(this.personaBloqueoSeleccionada);

                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);

                tramites = this.getTramiteService().
                    buscarTramitesTiposTramitesByNumeroIdentificacion(
                        this.personaBloqueoSeleccionada.getPersona().getNumeroIdentificacion(),
                        this.personaBloqueoSeleccionada.getPersona().getTipoIdentificacion());

                if (tramites != null && !tramites.isEmpty()) {

                    for (Tramite tram : tramites) {

                        filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION, tram.
                            getNumeroRadicacion());
                        filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, String.valueOf(
                            tram.getId()));
                        actividades = this.getProcesosService().consultarListaActividades(filtros);

                        if (actividades != null && !actividades.isEmpty() && actividades.get(0).
                            getEstado().equals(EEstadoActividad.SUSPENDIDA.toString())) {
                            this.getProcesosService().reanudarActividad(actividades.get(0).getId());
                        }
                    }
                }

                this.entidades = new ArrayList<SelectItem>();
                this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));
                this.selectedDepartamento = this.DEFAULT_COMBOS_TODOS;
                this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
                this.selectedRestitucionTierras = false;
                this.selectedTipoBloqueo = this.DEFAULT_COMBOS;
                this.idEntidadBloqueo = null;
                this.selectedMunicipio = "Seleccionar";

                String mensaje = "La persona se desmarcó exitosamente!";
                this.addMensajeInfo(mensaje);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                String mensaje = "La persona no pudo ser desmarcada";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else {
            String mensaje = "La persona no pudo ser desmarcada";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método encargado de realizar la validación de los paramtros minimos del desbloqueo de persona
     */
    private boolean validarDatosDesbloqueo() {

        Date fechaSistema = new Date();

        if (this.conteoArchivoDesbloqueo == 0) {
            String mensaje = "No se adjuntó un documento de soporte para la desmarcación" +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo() != null &&
             fechaSistema.before(this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo())) {
            String mensaje = "Fecha envío no puede ser mayor a la fecha actual, verifique.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo() != null &&
             fechaSistema.before(this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo())) {
            String mensaje = "La fecha de recibido no puede ser mayor a la fecha actual, verifique";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo() != null &&
            this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo() != null &&
             this.personaBloqueoSeleccionada.getFechaRecibidoDesbloqueo().before(
                this.personaBloqueoSeleccionada.getFechaEnvioDesbloqueo())) {
            String mensaje =
                "La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (!esNumeroRadicacionValido(this.personaBloqueoSeleccionada.
            getNumeroRadicacionDesbloqueo(), usuario)) {
            String mensaje = "El número de radicado no se encontró en el sistema. Por favor revise.";
            this.addMensajeError(mensaje);
            return false;
        }

        return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de soporte a cargar
     */
    private void nuevoDocumento() {

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento.setId(ETipoDocumentoId.DOC_SOPORTE_DESBLOQUEO_PERSONA
            .getId());

        this.documentoSoporteDesbloqueo.setFechaDocumento(new Date());
        this.documentoSoporteDesbloqueo
            .setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
        this.documentoSoporteDesbloqueo.setBloqueado(ESiNo.NO.getCodigo());

        this.documentoSoporteDesbloqueo.setTipoDocumento(tipoDocumento);
        this.documentoSoporteDesbloqueo.setUsuarioCreador(this.usuario
            .getLogin());
        this.documentoSoporteDesbloqueo.setUsuarioLog(this.usuario.getLogin());
        this.documentoSoporteDesbloqueo.setFechaLog(new Date());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                eventoCarga.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            nuevoDocumento();
            this.documentoSoporteDesbloqueo.setArchivo(eventoCarga.getFile()
                .getFileName());

            this.banderaBotonGuardar = true;
            this.conteoArchivoDesbloqueo++;

            String mensaje = "El documento se cargó adecuadamente.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            e.printStackTrace();
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo para recueprar la dirección de la imagen
     */
    public String getUrlBase() {
        return FacesContext.getCurrentInstance().getExternalContext()
            .getRealPath("/");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado al cerrar las ventanas modales
     */
    public void cerrarEsc(org.primefaces.event.CloseEvent event) {

        String mensaje = new String();
        this.addMensajeError(mensaje);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosDesbloqueo() {
        this.documentoSoporteDesbloqueo = new Documento();
        this.conteoArchivoDesbloqueo = 0;
        this.banderaGestionDesbloqueoPersona = true;
        this.banderaBuscar = false;

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));
        this.selectedDepartamento = this.DEFAULT_COMBOS_TODOS;
        this.idEntidadBloqueo = null;
        this.selectedMunicipio = "Seleccionar";
        this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
        this.selectedRestitucionTierras = false;
        this.selectedTipoBloqueo = this.DEFAULT_COMBOS;
        this.selectedTipoBloqueo = "FALLADO";

        this.banderaEjecutaBusqueda = true;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void salirDesbloqueo(CloseEvent e) {
        inicializarDatosDesbloqueo();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar y bloquear otra persona
     */
    public void terminarYDesbloquearOtraP() {
        init();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar
     */
    public String terminar() {
        try {
            UtilidadesWeb.removerManagedBean("desbloqueoPersona");
            return "index";
        } catch (Exception e) {
            this.addMensajeError("Error al cerrar la sesión");
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Método encargado de cerrar el panel de gestion de documentos de desbloqueo
     *
     * @author leidy.gonzalez
     */
    public void cerrarCargaDocumentosDesbloqueo() {
        this.banderaBuscar = true;
        this.banderaGestionDesbloqueoPersona = false;

        this.banderaEjecutaBusqueda = false;

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));
        this.selectedDepartamento = this.DEFAULT_COMBOS_TODOS;
        this.selectedMunicipio = "Seleccionar";
        this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
        this.selectedRestitucionTierras = false;
        this.selectedTipoBloqueo = this.DEFAULT_COMBOS;
        this.idEntidadBloqueo = null;

    }

    /**
     * Metodo para actualizar los municipios
     *
     * @author dumar.penuela
     */
    public void actualizarMunicipiosBuscardorListener() {

        if (this.municipiosDeptos != null) {

            this.municipiosBuscadorItemList = this.municipiosDeptos.get(this.selectedDepartamento);
        }

    }

    /**
     * Metodo para cargar los departamentos
     *
     * @author dumar.penuela
     */
    public void cargarDepartamentos() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = generalService.getDepartamentos(Constantes.COLOMBIA);

        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService().getCacheMunicipiosByDepartamento(
                dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getCodigo() + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    /**
     * Metodo para constuir la lista de departamentos
     *
     * @author dumar.penuela
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(), departamento.
                getCodigo() + "-" +
                 departamento.getNombre()));
        }
    }

    public void cargarEntidades() {
        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        List<Entidad> entidadesActivas = this.getConservacionService().buscarEntidades(null,
            EEntidadEstado.ACTIVO.getEstado(), selectedDepartamento, selectedMunicipio);

        if (entidadesActivas != null && !entidadesActivas.isEmpty()) {
            for (Entidad e : entidadesActivas) {
                this.entidades.add(new SelectItem(e.getId(), e.getNombre()));
            }
        } else {
            Municipio d = this.getGeneralesService().getCacheMunicipioByCodigo(selectedMunicipio);
            this.addMensajeError("El municipio " + d.getNombre() +
                ", no tiene asociadas entidades que puedan realizar la marcación, verifique");
        }
    }

    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
            .add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    private boolean esNumeroRadicacionValido(final String numeroRadicacion, final UsuarioDTO usuario) {
        boolean retval = true;
        if (usuario == null || numeroRadicacion == null || numeroRadicacion.trim().isEmpty()) {
            retval = false;
        } else {
            retval = this.getConservacionService().validarNumeroRadicacion(numeroRadicacion.trim(),
                usuario);
        }
        if (usuario != null && usuario.getDescripcionEstructuraOrganizacional() != null &&
             usuario.getDescripcionEstructuraOrganizacional().contains(Constantes.PREFIJO_DELEGADOS)) {
            retval = true;
        }
        return retval;
    }

}
