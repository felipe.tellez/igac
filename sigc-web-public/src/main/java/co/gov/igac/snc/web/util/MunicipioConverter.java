package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.util.Constantes;

/**
 * Converter para reemplazar un municipio.
 *
 * @author david.cifuentes
 *
 */
public class MunicipioConverter implements Converter {

    @Implement
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String objectAsString) {

        Municipio answer;
        String[] objectAttributes;

        answer = new Municipio();
        objectAttributes = objectAsString.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

        answer.setCodigo(objectAttributes[0]);
        answer.setNombre(objectAttributes[1]);

        return answer;
    }

    @Implement
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        Municipio objDT = (Municipio) o;
        String answer = objDT.toString();
        return answer;
    }
}
