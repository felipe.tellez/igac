package co.gov.igac.snc.web.mb.avaluos.radicacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.AdministracionDocumentacionAvaluoComercialMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso CU-SA-AC-006 Reasignar Territorial a Ejecutar los avalúos de una
 * Radicación
 *
 * @cu CU-SA-AC-006
 *
 * @author felipe.cadena
 */
@Component("reasignacionTerritorialAvaluos")
@Scope("session")
public class ReasignacionTerritorialAvaluosMB extends SNCManagedBean implements Serializable {

    /**
     * serial generado
     */
    private static final long serialVersionUID = 6949867469271185775L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionDocumentacionAvaluoComercialMB.class);
    /**
     * Territorial actual relacionada
     */
    private String territorialActual;

    /**
     * Territorial seleccionada por el usuario para asiganar a las solicitudes o los avalúos
     */
    private String territorialAsignada;

    /**
     * Bandera que determina si se estan reasignando solicitudes o Sec. radicados si es true se
     * reasignan Solicitudes,y si es false se reasignan Sec. radicados
     */
    private Boolean banderaAsignarSolicitud;

    /**
     * Lista para seleccionar la territorial a asignar, varia dependiendo de la territorial actual.
     */
    private List<SelectItem> listaTerritoriales;
    /**
     * Lista de solicitudes para reasignar
     */
    private List<Solicitud> solicitudesLista;
    /**
     * Lista de solicitudes seleccionadas por el usuario para asignar una sola territorial
     */
    private Solicitud[] solicitudesSeleccionadas;
    /**
     * Lista de avaluos para reasignar
     */
    private List<Avaluo> avaluosLista;
    /**
     * Lista de avaluos seleccionadas por el usuario para asignar una sola territorial
     */
    private Avaluo[] avaluosSeleccionados;

    @Autowired
    private GeneralMB general;
    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    // ------------------ procesos ---------------------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private List<Actividad> bpmActivities;

    // --------getters-setters----------------
    public Solicitud[] getSolicitudesSeleccionadas() {
        return solicitudesSeleccionadas;
    }

    public void setSolicitudesSeleccionadas(Solicitud[] solicitudesSeleccionadas) {
        this.solicitudesSeleccionadas = solicitudesSeleccionadas;
    }

    public Boolean getBanderaAsignarSolicitud() {
        return this.banderaAsignarSolicitud;
    }

    public void setBanderaAsignarSolicitud(Boolean banderaAsignarSolicitud) {
        this.banderaAsignarSolicitud = banderaAsignarSolicitud;
    }

    public List<Avaluo> getAvaluosLista() {
        return this.avaluosLista;
    }

    public void setAvaluosLista(List<Avaluo> avaluosLista) {
        this.avaluosLista = avaluosLista;
    }

    public Avaluo[] getAvaluosSeleccionados() {
        return this.avaluosSeleccionados;
    }

    public void setAvaluosSeleccionados(Avaluo[] avaluosSeleccionados) {
        this.avaluosSeleccionados = avaluosSeleccionados;
    }

    public String getTerritorialAsignada() {
        return territorialAsignada;
    }

    public void setTerritorialAsignada(String territorialAsignada) {
        this.territorialAsignada = territorialAsignada;
    }

    public List<Solicitud> getSolicitudesLista() {
        return solicitudesLista;
    }

    public void setSolicitudesLista(List<Solicitud> solicitudesLista) {
        this.solicitudesLista = solicitudesLista;
    }

    public List<SelectItem> getListaTerritoriales() {
        return this.listaTerritoriales;
    }

    public void setListaTerritoriales(List<SelectItem> listaTerritoriales) {
        this.listaTerritoriales = listaTerritoriales;
    }

    public String getTerritorialActual() {
        return this.territorialActual;
    }

    public void setTerritorialActual(String territorialActual) {
        this.territorialActual = territorialActual;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ReasignarTerritorialEjecutarAvaluosRadicacionMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

//FIXME::felipe.cadena::datos prueba, se deben retirar cuando se defina su origen
        List<Avaluo> avaluosPrueba = new ArrayList<Avaluo>();
        Avaluo ava = this.getAvaluosService().buscarAvaluoPorSecRadicado("60402012ER00041214-1");
        avaluosPrueba.add(ava);
        ava = this.getAvaluosService().buscarAvaluoPorSecRadicado("60402012ER00041214-2");
        avaluosPrueba.add(ava);
        ava = this.getAvaluosService().buscarAvaluoPorSecRadicado("60402012ER00041214-3");
        avaluosPrueba.add(ava);
        ava = this.getAvaluosService().buscarAvaluoPorSecRadicado("60402012ER00041214-4");
        avaluosPrueba.add(ava);

        this.cargarDatosAvaluos(avaluosPrueba);
        // fin datos prueba

        this.usuario = MenuMB.getMenu().getUsuarioDto();

//TODO :: felipe.cadena::integraión con el motor de procesos.
        //this.extraerDatosArbolProcesosSolicitud();
        if (usuario.getCodigoTerritorial() == null) {
            usuario.setCodigoTerritorial(Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
        LOGGER.debug("User >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + usuario.getCodigoTerritorial());

        this.territorialActual = usuario.getCodigoTerritorial();
        this.iniciarTerritoriales();

    }

    /**
     * Método para cargar las solicitudes que necesitan reasignación
     *
     * @author felipe.cadena
     */
    public void cargarDatosSolicitudes(List<Solicitud> solicitudes) {
        LOGGER.debug("cargando datos solicitudes...");
        this.banderaAsignarSolicitud = true;
        this.solicitudesLista = solicitudes;
        for (Solicitud sol : this.solicitudesLista) {
            sol = this.getAvaluosService().buscarSolicitudPorNumero(sol.getNumero());
        }
    }

    /**
     * Método para cargar los avaluos que necesitan reasignación
     *
     * @author felipe.cadena
     */
    public void cargarDatosAvaluos(List<Avaluo> avaluos) {
        LOGGER.debug("cargando datos avaluos...");
        this.banderaAsignarSolicitud = false;
        this.avaluosLista = avaluos;

//TODO :: felipe.cadena :: 23-04-2013 :: hacer un solo viaje a la bd (buscar todos en un método) :: pedro.garcia
        for (Avaluo ava : this.avaluosLista) {
            ava = this.getAvaluosService().buscarAvaluoPorSecRadicado(ava.getSecRadicado());
        }
    }

    /**
     * Método para iniciar la lista de territoriales que se pueden asignar
     *
     * @author felipe.cadena
     *
     */
    public void iniciarTerritoriales() {
        LOGGER.debug("Iniciar territoriales... ");
        if (this.usuario.getCodigoTerritorial().
            equals(Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL)) {
            this.listaTerritoriales = this.general.getTerritorialesSinDireccionGeneral();
        } else {
            SelectItem si = new SelectItem(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL + " - " +
                 Constantes.NOMBRE_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
            this.listaTerritoriales = new ArrayList<SelectItem>();
            this.listaTerritoriales.add(si);
        }
    }

    /**
     * Método para asignar la nueva territorial seleccionada y actualizar la información en la base
     * de datos.
     *
     * Se llama desde reasignarTerritorialAvaluos.xhtml (p:commandButton id="reasignarBtn")
     *
     * @author felipe.cadena
     *
     */
    public void reasignarTerritorial() {
        LOGGER.debug("cambiando territorial... ");

        if (this.banderaAsignarSolicitud) {
            this.reasignarSolicitudes();
        } else {
            this.reasignarSecRadicados();
        }
        LOGGER.debug("Territorial cambiada... ");
    }

    /**
     * Método para reasignar territorial de una solicitud
     *
     * @author felipe.cadena
     */
    public void reasignarSolicitudes() {
        for (Solicitud sol : solicitudesSeleccionadas) {
            sol.setDestinoTerritorialId(this.territorialAsignada);
            this.getAvaluosService().guardarActualizarSolicitudAvaluoComercial(sol);
        }
    }

    /**
     * Método para reasignar territorial de un Sec. radicado
     *
     * @author felipe.cadena
     */
    public void reasignarSecRadicados() {

        for (Avaluo ava : avaluosSeleccionados) {
            if (this.territorialAsignada.equals(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL)) {
                LOGGER.debug("Sede central");
                ava.setEstructuraOrganizacionalCod(null);
                this.getAvaluosService().actualizarAvaluo(ava);
                ava.setEstructuraOrganizacionalCod(this.territorialAsignada);
            } else {
                ava.setEstructuraOrganizacionalCod(this.territorialAsignada);
                this.getAvaluosService().actualizarAvaluo(ava);
            }

        }

    }
}
