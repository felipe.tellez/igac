package co.gov.igac.snc.web.mb.tests;

import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.primefaces.model.DefaultStreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro.garcia
 */
@Component("jfreechart")
@Scope("session")
public class JFreeChartTestsMB implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(JFreeChartTestsMB.class);

    private DefaultStreamedContent chartAsStreamedContent;

//--------------------------------------------------------------------------------------------------    
    public DefaultStreamedContent getChartAsStreamedContent() {
        return this.chartAsStreamedContent;
    }

    public void setChartAsStreamedContent(DefaultStreamedContent chartAsStreamedContent) {
        this.chartAsStreamedContent = chartAsStreamedContent;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        JFreeChart chart;
        DefaultCategoryDataset defaultcategorydataset;
        TextTitle subtitle;
        CategoryPlot plot;
        LineAndShapeRenderer lineandshaperenderer;
        File chartFile;

        defaultcategorydataset = new DefaultCategoryDataset();
        defaultcategorydataset.addValue(121000000D, "año", new Integer(2009));
        defaultcategorydataset.addValue(120000000D, "año", new Integer(2010));
        defaultcategorydataset.addValue(124000000D, "año", new Integer(2011));
        defaultcategorydataset.addValue(124500000D, "año", new Integer(2012));
        defaultcategorydataset.addValue(130000000D, "año", new Integer(2013));

        chart = ChartFactory.createLineChart("Comportamiento del avalúo del predio", "Año",
            "precio $", defaultcategorydataset, PlotOrientation.VERTICAL, true, true, false);

        subtitle = new TextTitle("Valor del predio según el año");
        subtitle.setPosition(RectangleEdge.BOTTOM);
        subtitle.setHorizontalAlignment(HorizontalAlignment.RIGHT);
        chart.addSubtitle(subtitle);

        plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.white);

        NumberAxis numberaxis = (NumberAxis) plot.getRangeAxis();
        //numberaxis.setLowerBound(120000000D); //OJO
        //numberaxis.setUpperBound(140000000D);
        Double upperBound, lowerBound;
        upperBound = 130000000D + (130000000D - 120000000D);
        lowerBound = 120000000D - (130000000D - 120000000D);
        numberaxis.setRange(lowerBound, upperBound);

        lineandshaperenderer = (LineAndShapeRenderer) plot.getRenderer();
        lineandshaperenderer.setBaseShapesVisible(true);
        lineandshaperenderer.setDrawOutlines(true);

        chartFile = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(), "avaluoTestLineChart");
        LOGGER.debug("Archivo a crear " + chartFile.getAbsolutePath());

        try {
            ChartUtilities.saveChartAsPNG(chartFile, chart, 400, 300);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }
        try {
            this.chartAsStreamedContent = new DefaultStreamedContent(new FileInputStream(chartFile),
                "image/png");
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex.getMessage());
        }

    }

}
