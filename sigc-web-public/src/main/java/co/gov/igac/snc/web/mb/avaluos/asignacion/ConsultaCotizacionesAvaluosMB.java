/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB del cu
 *
 * @cu CU-SA-AC-040
 * @author christian.rodriguez
 */
@Component("consultaCotizacionesAvaluos")
@Scope("session")
public class ConsultaCotizacionesAvaluosMB extends SNCManagedBean {

    private static final long serialVersionUID = 256439640577167L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaCotizacionesAvaluosMB.class);

    /**
     * {@link FiltroDatosConsultaAvaluo} filtro usado para la consulta
     */
    private FiltroDatosConsultaAvaluo filtroAvaluos;

    /**
     * Lista con los {@link Avaluo} que tienen acta de control de calidad encontrados en la busqueda
     */
    private List<Avaluo> solicitudesDeCotizacion;

    /**
     * Lista de avaluadores de la territorial en sesion
     */
    private List<SelectItem> listaAvaluadores;

    /**
     * Lista con los departamentos para los combos
     */
    private ArrayList<SelectItem> listaDepartamentos;

    /**
     * Lista con los municipios para los combos
     */
    private ArrayList<SelectItem> listaMunicipios;

    /**
     * Avaluo seleccionado para visualización de detalles de cotizacion
     */
    private Avaluo avaluoSeleccionado;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si se está buscando por solicitante tipo natural o juridico
     */
    private boolean banderaEsPersonaNatural;

    /**
     * Bandera que indica si se debe solicitar la territorial o se debe tomar del usuario activo
     */
    private boolean banderaHabilitarTerritorial;

    /**
     * Bandera que indica si se muestran o no todos los datos de la tabla de predios del
     * {@link #avaluoSeleccionado}
     */
    private boolean banderaTablaExpandidaPredio;

    /**
     * Valor total de la sumatoria de los valores de cotización de cada predio asociado al
     * {@link #avaluoSeleccionado}
     */
    private Double valorTotalCotizacion;

    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaAvaluo getFiltroAvaluos() {
        return filtroAvaluos;
    }

    public List<Avaluo> getSolicitudesDeCotizacion() {
        return this.solicitudesDeCotizacion;
    }

    public List<SelectItem> getListaAvaluadores() {
        return this.listaAvaluadores;
    }

    public ArrayList<SelectItem> getListaDepartamentos() {
        return this.listaDepartamentos;
    }

    public ArrayList<SelectItem> getListaMunicipios() {
        return this.listaMunicipios;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public Double getValorTotalCotizacion() {
        this.valorTotalCotizacion = 0.0;
        if (this.avaluoSeleccionado != null) {
            for (AvaluoPredio predio : this.avaluoSeleccionado.getAvaluoPredios()) {
                this.valorTotalCotizacion += predio.getValorCotizacion();
            }
        }
        return this.valorTotalCotizacion;

    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    public void setBanderaEsPersonaNatural(boolean banderaEsPersonaNatural) {
        this.banderaEsPersonaNatural = banderaEsPersonaNatural;
    }

    public boolean isBanderaHabilitarTerritorial() {
        return this.banderaHabilitarTerritorial;
    }

    public boolean isBanderaTablaExpandidaPredio() {
        return this.banderaTablaExpandidaPredio;
    }

    public void setBanderaTablaExpandidaPredio(
        boolean banderaTablaExpandidaPredio) {
        this.banderaTablaExpandidaPredio = banderaTablaExpandidaPredio;
    }

    // -------------------------------------------------------------------------
    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de ConsultaAvaluosAprobadosMB");

        this.inicializarFiltroConsulta();
        this.determinarSiSeHabilitaTerritorial();
        this.cargarListaDepartamentos();

    }

    // ----------------------------------------------------------------------
    /**
     * Inicializa el filtro de consulta y lso valores por defecto
     *
     * @author christian.rodriguez
     */
    private void inicializarFiltroConsulta() {

        this.filtroAvaluos = new FiltroDatosConsultaAvaluo();

        this.filtroAvaluos
            .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                .getCodigo());
        this.filtroAvaluos.setTipoEmpresa(EPersonaTipoPersona.NATURAL
            .getCodigo());
        // this.filtroAvaluos
        // .setTipoTramite(ETramiteTipoTramite.SOLICITUD_DE_COTIZACION
        // .getCodigo());

        this.banderaEsPersonaNatural = true;
    }

    // ----------------------------------------------------------------------
    /**
     * Método que determina si se activa la {@link #banderaHabilitarTerritorial} dependiendo de si
     * el usuario pertenece al GIt o a una territorial
     *
     * @author christian.rodriguez
     */
    private void determinarSiSeHabilitaTerritorial() {

        UsuarioDTO usuarioActivo = MenuMB.getMenu().getUsuarioDto();

        String territorialCod = usuarioActivo
            .getCodigoEstructuraOrganizacional();

        if (territorialCod == null) {
            this.banderaHabilitarTerritorial = true;
        } else {
            this.banderaHabilitarTerritorial = false;
            this.filtroAvaluos.setTerritorial(territorialCod);
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lista de departamentos según la territorial del usuario activo.
     * Así mismo carga los municipios y avaluadores asociados
     *
     * @author christian.rodriguez
     */
    public void cargarListaDepartamentos() {

        this.listaDepartamentos = new ArrayList<SelectItem>();
        this.listaDepartamentos.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.filtroAvaluos.getTerritorial() != null &&
             !this.filtroAvaluos.getTerritorial().isEmpty()) {

            List<Departamento> departamentos = (ArrayList<Departamento>) this
                .getGeneralesService().getCacheDepartamentosPorTerritorial(
                    this.filtroAvaluos.getTerritorial());

            for (Departamento departamento : departamentos) {
                this.listaDepartamentos.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }

        }

        this.filtroAvaluos.setDepartamento(null);

        this.cargarListaMunicipios();
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lista de municipios según el departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void cargarListaMunicipios() {

        this.listaMunicipios = new ArrayList<SelectItem>();
        this.listaMunicipios.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.filtroAvaluos.getDepartamento() != null &&
             !this.filtroAvaluos.getDepartamento().isEmpty()) {

            List<Municipio> municipios = (ArrayList<Municipio>) this
                .getGeneralesService().getCacheMunicipiosByDepartamento(
                    this.filtroAvaluos.getDepartamento());

            for (Municipio municipio : municipios) {
                this.listaMunicipios.add(new SelectItem(municipio.getCodigo(),
                    municipio.getNombre()));
            }
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que realiza la busqueda de los avaluos que tienen acta de control de calidad
     * dependiendo del {@link FiltroDatosConsultaAvaluo} ingreasdo
     *
     * @author christian.rodriguez
     */
    public void buscar() {
        if (this.filtroAvaluos != null) {

            this.solicitudesDeCotizacion = this.getAvaluosService()
                .buscarAvaluosPorFiltro(this.filtroAvaluos);
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia el tipo de persona. Activa la bandera de si es
     * persona natural y cambia el tipo de documento por defecto
     *
     * @author lorena.salamanca
     * @modified christian.rodriguez
     */
    public void onChangeTipoPersona() {

        if (this.filtroAvaluos.getTipoEmpresa().equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {

            this.banderaEsPersonaNatural = true;
            this.filtroAvaluos
                .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo());
            this.filtroAvaluos.setNombreRazonSocial(null);
            this.filtroAvaluos.setSigla(null);

        } else {
            this.banderaEsPersonaNatural = false;
            this.filtroAvaluos
                .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.NIT
                    .getCodigo());
            this.filtroAvaluos.setPrimerNombre(null);
            this.filtroAvaluos.setSegundoNombre(null);
            this.filtroAvaluos.setPrimerApellido(null);
            this.filtroAvaluos.setSegundoApellido(null);
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que debe ser llamado cuando se carga el caso de uso desde el 88
     *
     * @author christian.rodriguez
     * @param solicitante {@link Solicitante} del cual se quieren consultar las solicitudes de
     * cotización
     */
    public void cargarDesdeCU88(Solicitante solicitante) {

        if (solicitante != null) {

            if (solicitante.getTipoPersona().equalsIgnoreCase(
                EPersonaTipoPersona.NATURAL.getCodigo())) {
                this.filtroAvaluos
                    .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                        .getCodigo());
                this.filtroAvaluos.setNombreRazonSocial(solicitante
                    .getRazonSocial());
                this.filtroAvaluos.setSigla(solicitante.getSigla());
            } else {
                this.filtroAvaluos
                    .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.NIT
                        .getCodigo());
                this.filtroAvaluos.setPrimerNombre(solicitante
                    .getPrimerNombre());
                this.filtroAvaluos.setSegundoNombre(solicitante
                    .getSegundoNombre());
                this.filtroAvaluos.setPrimerApellido(solicitante
                    .getPrimerApellido());
                this.filtroAvaluos.setSegundoApellido(solicitante
                    .getSegundoApellido());
            }

            this.buscar();
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

    // end of class
}
