/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileInputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.chart.CartesianChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaContingencia;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.colecciones.Pair;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteReasignacion;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el caso de uso de administración asignaciones, busca un trámite y se puede
 * administrar la asignación del ejecutor
 *
 * @author javier.aponte
 */
@Component("administracionAsignaciones")
@Scope("session")
public class AdministracionAsignacionesMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -9175673473118981480L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionAsignacionesMB.class);

    // ------ Servicios ------//
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextoWeb;

    // ------ Variables ------//
    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Trámites seleccionados de la tabla de resultado de trámites válidos para reasignar ejecutor
     */
    private Tramite[] selectedTramites;

    /**
     * Generación de modelo cartesiano para la grafica de barras
     */
    private CartesianChartModel cartesianModel;

    private List<SEjecutoresTramite> ejecutoresTramite;

    private SEjecutoresTramite ejecutorSeleccionado;

    /**
     * Tramites de la tabla de Tramites asignado a un ejecutor
     */
    private List<Tramite> assignedTramitesAEjecutor;

    /**
     * lista de trámites que cumplen los requisitos para que se les pueda reasignar ejecutor
     */
    private List<Tramite> tramitesValidos;

    /**
     * Almacena el id del trámite con la actividad en la que se encuentra actualmente.
     */
    private Map<Long, String> tablaActividades;

    /**
     * Variables para las graficas
     */
    private StreamedContent chartTerreno;
    private StreamedContent chartOficina;

    /**
     * entero para el numero de secuencia de la grafica
     */
    public int fileG = 0;

    private String prediosParaZoom;

    /**
     * nombre de la aplicación web (esta aplicacíon). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Estas variables se usan para que el mapa sepa qué capa y qué herramientas mostrar
     */
    private String moduleLayer;
    private String moduleTools;

    private TramiteEstado estadoTramite;

    // ------------------ GETTERS Y SETTERS -------------------- //
    public TramiteEstado getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(TramiteEstado estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public Tramite[] getSelectedTramites() {
        return selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public CartesianChartModel getCartesianModel() {
        return cartesianModel;
    }

    public void setCartesianModel(CartesianChartModel cartesianModel) {
        this.cartesianModel = cartesianModel;
    }

    public List<SEjecutoresTramite> getEjecutoresTramite() {
        return ejecutoresTramite;
    }

    public void setEjecutoresTramite(List<SEjecutoresTramite> ejecutoresTramite) {
        this.ejecutoresTramite = ejecutoresTramite;
    }

    public SEjecutoresTramite getEjecutorSeleccionado() {
        return ejecutorSeleccionado;
    }

    public void setEjecutorSeleccionado(SEjecutoresTramite ejecutorSeleccionado) {
        this.ejecutorSeleccionado = ejecutorSeleccionado;
    }

    public StreamedContent getChartTerreno() {
        return chartTerreno;
    }

    public void setChartTerreno(StreamedContent chartTerreno) {
        this.chartTerreno = chartTerreno;
    }

    public StreamedContent getChartOficina() {
        return chartOficina;
    }

    public void setChartOficina(StreamedContent chartOficina) {
        this.chartOficina = chartOficina;
    }

    public List<Tramite> getAssignedTramitesAEjecutor() {
        return assignedTramitesAEjecutor;
    }

    public void setAssignedTramitesAEjecutor(List<Tramite> assignedTramitesAEjecutor) {
        this.assignedTramitesAEjecutor = assignedTramitesAEjecutor;
    }

    public List<Tramite> getTramitesValidos() {
        return tramitesValidos;
    }

    public void setTramitesValidos(List<Tramite> tramitesValidos) {
        this.tramitesValidos = tramitesValidos;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    // ------------------ M É T O D O S -------------------- //
    @PostConstruct
    public void init() {

        // Usuario.
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.estadoTramite = new TramiteEstado();
        this.tramitesValidos = new ArrayList<Tramite>();

        this.inicializarVariablesVisorGIS();
    }

    // -------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.aponte
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("gestionarSolicitudes");
        this.tareasPendientesMB.init();
        this.init();
        return "index";
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Action ejecutada al dar click en el botón "Reasignar ejecutor" Valida que todos los trámites
     * seleccionados de la tabla tengan la misma clasificación (oficina, terreno)
     *
     * @modified pedro.garcia
     * @modified franz.gamba -> se realiza la consulta de los ejecutores en este punto para no
     * sobrecargar el inicio de la pagina
     */
    public void initAsignacionEjecutorTramites() {

        String clasificacionTramite, clasificacionTramiteTemp;
        boolean okToAsignarEjecutor;

        clasificacionTramite = this.getSelectedTramites()[0].getClasificacion();
        okToAsignarEjecutor = true;
        for (Tramite selectedTramite : this.getSelectedTramites()) {
            clasificacionTramiteTemp = selectedTramite.getClasificacion();
            if (clasificacionTramiteTemp != null &&
                 clasificacionTramite != null) {
                if (!clasificacionTramiteTemp
                    .equalsIgnoreCase(clasificacionTramite)) {
                    this.addMensajeError(
                        "Los trámites a los que se le va a asignar ejecutor deben tener la misma clasificación!");
                    okToAsignarEjecutor = false;
                    break;
                }
            }
        }

        if (!okToAsignarEjecutor) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
            return;
        }

        this.cartesianModel = new CartesianChartModel();
        createCartesianModel();
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Crea el modelo cartesiano de la grafica y alimenta la tabla ejecutores
     *
     * @author javier.aponte
     */
    //N: alguien modificó esta vaina y no documentó!!!
    private void createCartesianModel() {

        LOGGER.debug("En AdministracionAsignacionesMB entra a createCartesianModel");
        if (this.usuario != null) {
            this.ejecutoresTramite =
                this.getTramiteService()
                    .buscarFuncionariosTerritorialConClasificacionTramite(this.usuario
                        .getDescripcionEstructuraOrganizacional());
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Reasignar ejecutor"
     *
     * @author javier.aponte
     */
    /*
     * @modified felipe.cadena::13-01-2016::Se crea un objeto TramiteReasignacion para registrar mas
     * informacion acerca de la transferencia
     */
    public void reasignarEjecutor() {

        LOGGER.debug("En AdministracionAsignacionesMB entra en reasignarEjecutor");

        Boolean usuarioEjecutor = false;

        for (int k = 0; k < this.selectedTramites.length; k++) {

            if (this.selectedTramites[k].getFuncionarioEjecutor()
                .equals(this.ejecutorSeleccionado.getFuncionarioId())) {
                this.addMensajeError("El trámite con número de radicación: " +
                    this.selectedTramites[k].getNumeroRadicacion() +
                     ", se quiere reasignar al mismo ejecutor: " + this.selectedTramites[k].
                        getNombreFuncionarioEjecutor());
                return;
            }
        }

        UsuarioDTO usuarioDestinoActividad;

        List<Actividad> actividadesPorIdObjetoNegocio;

        try {
            if (this.selectedTramites.length > 0) {
                List<TramiteEstado> estados = new ArrayList<TramiteEstado>();
                for (int k = 0; k < this.selectedTramites.length; k++) {

                    TramiteEstado te = new TramiteEstado();
                    te.setFechaInicio(new Date());
                    te.setFechaLog(new Date());
                    te.setMotivo(this.estadoTramite.getMotivo());
                    te.setEstado(ETramiteEstado.REASIGNADO.getCodigo());
                    te.setResponsable(this.usuario.getLogin());
                    te.setUsuarioLog(this.usuario.getLogin());
                    te.setTramite(this.selectedTramites[k]);
                    estados.add(te);

                    usuarioDestinoActividad = this.getGeneralesService().
                        getCacheUsuario(this.ejecutorSeleccionado.getFuncionarioId()); //Usuario al que se transfiere la actividad

                    actividadesPorIdObjetoNegocio = this.getProcesosService().
                        getActividadesPorIdObjetoNegocio(this.selectedTramites[k].getId());

                    // felipe.cadena :: #10322 ::13/11/2014
                    if (actividadesPorIdObjetoNegocio != null && !actividadesPorIdObjetoNegocio.
                        isEmpty()) {
                        usuarioEjecutor = this.determinarUsuarioEjecutor(this.selectedTramites[k],
                            actividadesPorIdObjetoNegocio.get(0));
                        if (usuarioEjecutor == null) {
                            usuarioEjecutor = true;                            
                        }
                    }

                    this.selectedTramites[k]
                        .setFuncionarioEjecutor(this.ejecutorSeleccionado
                            .getFuncionarioId());
                    this.selectedTramites[k]
                        .setNombreFuncionarioEjecutor(this.ejecutorSeleccionado
                            .getFuncionario());

                    if (actividadesPorIdObjetoNegocio != null && !actividadesPorIdObjetoNegocio.
                        isEmpty()) {

                        for (Actividad currentActivity : actividadesPorIdObjetoNegocio) {
                            //transferir la actividad en el process
                            // felipe.cadena :: #10322 ::13/11/2014
                            String codTerritorial = this.getProcesosService().obtenerCodTerritorial(currentActivity.getId());
                            
                            if (usuarioDestinoActividad.getCodigoTerritorial().equals(codTerritorial)) {
                                
                                LOGGER.info("ACTIVIDAD CORRESPONDE A TRAMITE");

                                if (usuarioEjecutor) {
                                    LOGGER.debug("Se transfiere la actividad en process al usuario: "
                                            + usuarioDestinoActividad.getLogin());
                                    this.getProcesosService().transferirActividadConservacion(
                                            currentActivity.getId(), usuarioDestinoActividad);

                                    //felipe.cadena::13-01-2016::#14289::se crea objeto reasignacion para auditoria
                                    TramiteReasignacion tr = new TramiteReasignacion();
                                    tr.setTramite(this.selectedTramites[k]);
                                    tr.setFuncionarioOrigen(currentActivity.getFuncionario());
                                    tr.setFuncionarioDestino(usuarioDestinoActividad.getLogin());
                                    tr.setMotivo(this.estadoTramite.getMotivo());
                                    tr.setFechaLog(new Date());
                                    tr.setUsuarioLog(this.usuario.getLogin());

                                    tr = this.getTramiteService().guardarActualizarTramiteReasignacion(
                                            tr);

                                }
                            } else {
                                this.addMensajeError("No se pudo reasignar el ejecutor al trámite - Usuario Ejecutor No pertenece a la territorial");
                                throw new ExcepcionSNC("ERROR ACTIVIDAD PROCESS", "Error Modelo BPM", "Usuario Ejecutor No pertenece a la territorial");
                            }
                        }
                    }

                    this.enviarCorreo(usuarioDestinoActividad, this.selectedTramites[k].
                        getNumeroRadicacion());

                    this.tramitesValidos.remove(this.selectedTramites[k]);
                }
                this.getTramiteService().actualizarTramites(this.selectedTramites);
                this.getTramiteService().insertarTramiteEstados(estados);

                this.addMensajeInfo("Se reasignaron el(los) trámite(s) satisfactoriamente");

                this.selectedTramites = null;
            }
            LOGGER.debug("Se termino de reasignar ejecutores a los trámites");
        } catch (Exception e) {
            LOGGER.debug("No se pudo reasignar el ejecutor al trámite");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("No se pudo reasignar el ejecutor al trámite");
        } finally {
            this.estadoTramite = new TramiteEstado();
        }

    }

    /**
     * Determina si el usuario que tiene la actividad actual del tramite es o no el ejecutor del
     * mismo
     *
     * @author felipe.cadena
     * @param tramite
     * @param actividad
     * @return
     */
    public Boolean determinarUsuarioEjecutor(Tramite tramite, Actividad actividad) {

        String ejecutorActual = tramite.getFuncionarioEjecutor();
        UsuarioDTO usuarioActividad;
        //TODO :: felipe.cadena :: 13/11/2014 :: Se debe utilizar el metodo para obtener los usuarios de process
        List<UsuarioDTO> usuariosActividad = this.getProcesosService().obtenerUsuariosActividad(
            actividad.getId());

        if (usuariosActividad != null && !usuariosActividad.isEmpty()) {
            usuarioActividad = usuariosActividad.get(0);
        } else {
            return null;
        }

        if (usuarioActividad != null) {
            return ejecutorActual.equals(usuarioActividad.getLogin());
        } else {
            return null;
        }

    }

    /**
     * Método que envia un correo al ejecutor informando que se le ha cancelado la asignación del
     * trámite con el número de radicado correspondiente
     *
     * @author javier.aponte
     */
    public void enviarCorreo(UsuarioDTO usuarioDestinoActividad, String numeroRadicacion) {

        Object parametrosContenidoEmail[] = new Object[3];

        Locale colombia = new Locale("ES", "es_CO");

        String contenido = null;
        try {
            //enviar el correo electrónico al ejecutor
            contenido =
                ConstantesComunicacionesCorreoElectronico.CONTENIDO_CANCELACION_ASIGNACION_TRAMITE;

            parametrosContenidoEmail[0] = numeroRadicacion;
            parametrosContenidoEmail[1] = this.usuario.getNombreCompleto();
            parametrosContenidoEmail[2] = this.usuario.getDescripcionEstructuraOrganizacional().
                replaceAll("UOC_", "");

            contenido = new MessageFormat(contenido, colombia).format(parametrosContenidoEmail);

            this.getGeneralesService().enviarCorreo(
                usuarioDestinoActividad.getEmail(),
                ConstantesComunicacionesCorreoElectronico.ASUNTO_CANCELACION_ASIGNACION_TRAMITE,
                contenido, null, null);

        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico ");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeWarn("No se pudo enviar el correo electrónico al ejecutor: " +
                usuarioDestinoActividad.getNombreCompleto());
        }
    }

    // -----------------------------------------------------------------------------------------------
    /**
     * Metodo que retorna los tramites asignados al ejecutor seleccionado
     *
     * @author franz.gamba
     */
    public void consultarTramitesAsignadosAEjecutor() {

        LOGGER.debug("En AdministracionAsignacionesMB entra en consultarTramitesAsignadosAEjecutor");
        String nombreFileT;
        String nombreFileO;

        if (this.ejecutorSeleccionado == null) {
            this.addMensajeError("Debe seleccionar un ejecutor!");
            return;
        }

        Pair<List<Tramite>, Map<Long, String>> datosActividades = this.
            consultarTramitesEjecutorActividades();
        this.assignedTramitesAEjecutor = datosActividades.getFirst();
        this.tablaActividades = datosActividades.getSecond();
        //Construir tabla de contingencia 
        TablaContingencia t = new TablaContingencia();
        Map<Long, String> tablaTramites = new HashMap<Long, String>();

        //Obtener un mapa con id y clasificación de trámite (terreno u oficina)
        for (Tramite tr : assignedTramitesAEjecutor) {
            tablaTramites.put(tr.getId(), tr.getClasificacion());
        }
        String clasificacion;
        for (Long id : tablaActividades.keySet()) {
            clasificacion = tablaTramites.get(id);
            t.addValor(tablaActividades.get(id), clasificacion != null ? clasificacion :
                "No clasificado");
        }
        try {

            JFreeChart jfreechartO = ChartFactory.createPieChart(
                "Tramites de Oficina", createDatasetOficina(this.getGeneralesService()
                    .getCacheUsuario(this.ejecutorSeleccionado
                        .getFuncionarioId()), t), true, true, false);
            nombreFileO = "dynamichartO" + fileG;
            File chartFileO = new File(FileUtils.getTempDirectory().getAbsolutePath(), nombreFileO);
            LOGGER.debug("Archivo a crear " + chartFileO.getAbsolutePath());
            ChartUtilities.saveChartAsPNG(chartFileO, jfreechartO, 375, 300);
            chartOficina = new DefaultStreamedContent(new FileInputStream(
                chartFileO), "image/png");
            // /

            JFreeChart jfreechartT = ChartFactory.createPieChart(
                "Tramites de Terreno", createDatasetTerreno(this.getGeneralesService()
                    .getCacheUsuario(this.ejecutorSeleccionado
                        .getFuncionarioId()), t), true, true, false);
            nombreFileT = "dynamichartT" + fileG;
            File chartFileT = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                nombreFileT);
            ChartUtilities.saveChartAsPNG(chartFileT, jfreechartT, 375, 300);

            chartTerreno = new DefaultStreamedContent(new FileInputStream(
                chartFileT), "image/png");
            fileG++;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        LOGGER.debug("*****NUMERO DE CUENTA= " +
             this.assignedTramitesAEjecutor.size() + " *****");

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo que toma los tramites asignados a un ejecutor segun las actividades que le
     * corresponden
     *
     * @author franz.gamba
     * @param ejecutorSeleccionado
     */
    private Pair<List<Tramite>, Map<Long, String>> consultarTramitesEjecutorActividades() {
        UsuarioDTO usuario = // this.usuarioCorrespondienteSeleccionado(ejecutorSeleccionado);
            this.getGeneralesService().getCacheUsuario(this.ejecutorSeleccionado
                .getFuncionario());

        List<Actividad> actividadesUsuario = this.getProcesosService()
            .consultarListaActividades(usuario);

        Map<Long, String> tablaActividades = new HashMap<Long, String>();
        List<Tramite> tramitesDeEjecutor = new ArrayList<Tramite>();
        List<Long> tramiteIds = new ArrayList<Long>();
        for (Actividad act : actividadesUsuario) {
            tablaActividades.put(act.getIdObjetoNegocio(), act.getRutaActividad().getActividad());
        }
        tramiteIds.addAll(tablaActividades.keySet());
        tramitesDeEjecutor = this.getTramiteService()
            .obtenerTramitesPorIds(tramiteIds);
        return new Pair<List<Tramite>, Map<Long, String>>(tramitesDeEjecutor, tablaActividades);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo que genera la grafica de Torta de los estados de los tramites de Terreno
     *
     * @author franz.gamba
     * @return
     */
    private PieDataset createDatasetTerreno(UsuarioDTO usrSeleccionado, TablaContingencia t) {

        DefaultPieDataset dataset = new DefaultPieDataset();
        boolean existenTramites = false;
        try {
            if (t != null) {
                existenTramites = true;
            }
            if (existenTramites) {
                Set<String> tuplas = t.getValores().keySet();
                for (String tupla : tuplas) {
                    String[] datos = tupla.split(TablaContingencia.SEPARADOR);
                    if (datos[1].equals(ETramiteClasificacion.TERRENO.toString())) {
                        if (dataset.getKeys().contains(datos[0])) {
                            Double a = dataset.getValue(datos[0])
                                .doubleValue();
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue() + a));
                            LOGGER.debug("***** VALOR : " + a);
                        } else {
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue()));
                        }
                    }
                }
            }

            if (!existenTramites) {
                dataset.setValue("No existen trámites de este tipo",
                    new Double(0));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dataset;
    }

    /**
     * Metodo qeu genera la grafica de Torta de los estados de los tramites de Oficina
     *
     * @author franz.gamba
     * @return
     */
    private PieDataset createDatasetOficina(UsuarioDTO usrSeleccionado, TablaContingencia t) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        boolean existenTramites = false;
        try {
            if (t != null) {
                existenTramites = true;
            }
            if (existenTramites) {
                Set<String> tuplas = t.getValores().keySet();
                for (String tupla : tuplas) {
                    String[] datos = tupla.split(TablaContingencia.SEPARADOR);
                    if (datos[1].equals(ETramiteClasificacion.OFICINA.toString())) {
                        if (dataset.getKeys().contains(datos[0])) {
                            Double a = dataset.getValue(datos[0])
                                .doubleValue();
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue() + a));
                            LOGGER.debug("***** VALOR : " + a);
                        } else {
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue()));
                        }
                    }
                }
            }

            if (!existenTramites) {
                dataset.setValue("No existen trámites de este tipo",
                    new Double(0));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dataset;

    }

    //--------------------------------------------------------------------------------
    /**
     * Método que se encarga de asignar los trámites seleccionados al la lista de trámites válidos
     *
     * @author javier.aponte
     */
    public void asignarTramitesSeleccionados() {

        ConsultaTramiteMB consultaTramiteMB = (ConsultaTramiteMB) UtilidadesWeb
            .getManagedBean("consultaTramite");

        this.tramitesValidos = consultaTramiteMB.getTramitesSeleccionados();

    }

    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";
    }

    /**
     * Obtiene los predios de la seleccion de la tabla para hacerles zoom en el visor GIS
     *
     * @author lorena.salamanca
     * @param event
     */
    public void obtenerPrediosParaZoom(SelectEvent event) {
        this.prediosParaZoom = "";
        for (int k = 0; k < this.selectedTramites.length; k++) {
            String codigo = "";
            if (this.selectedTramites[k].isQuinta()) {
                codigo = this.selectedTramites[k].getNumeroPredial();
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }
            if (this.selectedTramites[k].isSegundaEnglobe()) {
                List<Long> idTramites = new LinkedList<Long>();
                idTramites.add(this.selectedTramites[k].getId());
                List<String> codigosSegunda = new ArrayList<String>();
                codigosSegunda = this.getTramiteService().getNumerosPredialesByTramiteId(idTramites);
                codigo = codigosSegunda.toString().replace("[", "");
                codigo = codigo.replace("]", "");
                codigo = codigo.replace(" ", "");
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }

            if (this.selectedTramites[k].isSegundaEnglobe() == false && this.selectedTramites[k].
                isQuinta() == false) {
                codigo = this.selectedTramites[k].getPredio().getNumeroPredial();
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }
        }
    }

    private boolean contienePredio(String codigo) {
        return this.prediosParaZoom.contains(codigo);
    }

    /**
     *
     * Retira los predios de la seleccion de la tabla para retirarlos del zoom del visor GIS
     *
     * @author lorena.salamanca
     * @param event
     */
    public void eliminarPrediosParaZoom(UnselectEvent event) {
        for (int k = 0; k < this.selectedTramites.length; k++) {
            String codigo;
            codigo = this.selectedTramites[k].getNumeroPredial();
            this.prediosParaZoom = this.prediosParaZoom.replace(codigo, "");
            this.prediosParaZoom = this.prediosParaZoom.replace(",,", ",");

            if (this.prediosParaZoom.charAt(this.prediosParaZoom.length() - 1) == ',') {
                this.prediosParaZoom = this.prediosParaZoom.substring(0,
                    this.prediosParaZoom.length() - 1);
            }
            if (this.prediosParaZoom.charAt(0) == ',') {
                this.prediosParaZoom = this.prediosParaZoom.substring(1,
                    this.prediosParaZoom.length());
            }
        }
    }
    // Fin de la clase

}
