/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.asignacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISParametrosAsignacion;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FuncionarioRecolector;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-002 Asignar recolector a área establecida
 *
 * @author pedro.garcia
 * @cu CU-TV-0F-002
 * @version 2.0
 *
 */
@Component("asignacionRecolector")
@Scope("session")
public class AsignacionRecolectorMB extends SNCManagedBean {

    /**
     * serial auto-generado
     */
    private static final long serialVersionUID = -517976981166965075L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AsignacionRecolectorMB.class);

    @Autowired
    private IContextListener contextoWeb;

    /**
     * Area escogida sin recolector asignado
     */
    private AreaCapturaOferta areaCapturaOferta;
    /**
     * Lista de Areas sin recolector asignado
     */
    private List<AreaCapturaOferta> areasSinRecolector;

    /**
     * Lista de recolectores
     */
    private List<FuncionarioRecolector> listaRecolectores;

    /**
     * Recolector seleccionado para revisar áreas o para asignarlo a un área
     */
    private FuncionarioRecolector recolectorSeleccionado;

    /**
     * Lista de áreas asignadas a un recolector
     */
    private String listaAreasRecolector;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * String de codigos de manzanas/veredas que son cargados para mostrar las manzanas/veredas de
     * una area oferta seleccionada para asignar recolector
     */
    private String manzanaVeredaCodigo;

    private UsuarioDTO usuario;

    /**
     * Bandera para identificar si el municipio seleccionado es rural o no
     */
    private boolean banderaRural;

    /**
     * Variable para indicar si se esta realizando proceso de asignación de recolector o
     * desasignación de área
     */
    private String valorSeleccionManzanasOfertas;

    /**
     * Bandera para indicar que ya se ha escogido un recolector de la tabla
     */
    private boolean banderaRecolectorSeleccionado;

    /**
     * Lista de regiones asociadas a un recolector que fueron escogidas para realizar designación
     */
    private RegionCapturaOferta[] listaRegionesSeleccionadasDesasignacion;

    /**
     * Lista de regiones que tiene asignadas un recolector
     */
    private List<RegionCapturaOferta> regionesRecolectorSeleccionado;

    /**
     * Variable que almacena el valor del id de la region a crear.
     */
    private String regionId;

    /**
     * Variable que almacena la observacion ingresada en el momento de crear una region en un area
     * descentralizada
     */
    private String observacionRegion;

    /**
     * Variable que almacena la posición del dialog areasParaAsignarRecolectorDialog
     */
    private String posicionVentanaInformacionRegion;

    /**
     * Lista de DetalleCapturaOferta del area seleccionada
     */
    private List<DetalleCapturaOferta> listaDetallesCapturaOferta;

    private RegionCapturaOferta regionSeleccionada;

    /**
     * indica si el usuario que ingresa el el coordinador del GIT avalúos
     */
    private boolean banderaEsCoordinador;

    // ------------------ procesos
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private Actividad bpmActivityInstance;

    /**
     * Contiene el id del área captura oferta seleccionada por el arbol de procesos
     */
    private Long areaCapturaOfertaId;

    private String idsAreasOferta;

    /**
     * Contiene las regiones de captura asociadas al área de captura
     */
    private List<RegionCapturaOferta> regionesCapturaArea;

    // --------------------------------------------------------------------------------------------------
    public List<AreaCapturaOferta> getAreasSinRecolector() {
        return areasSinRecolector;
    }

    public void setAreasSinRecolector(List<AreaCapturaOferta> areasSinRecolector) {
        this.areasSinRecolector = areasSinRecolector;
    }

    public List<FuncionarioRecolector> getListaRecolectores() {
        return listaRecolectores;
    }

    public void setListaRecolectores(List<FuncionarioRecolector> listaRecolectores) {
        this.listaRecolectores = listaRecolectores;
    }

    public FuncionarioRecolector getRecolectorSeleccionado() {
        return recolectorSeleccionado;
    }

    public void setRecolectorSeleccionado(FuncionarioRecolector recolectorSeleccionado) {
        this.recolectorSeleccionado = recolectorSeleccionado;
    }

    public String getListaAreasRecolector() {
        return listaAreasRecolector;
    }

    public void setListaAreasRecolector(String listaAreasRecolector) {
        this.listaAreasRecolector = listaAreasRecolector;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getManzanaVeredaCodigo() {
        return manzanaVeredaCodigo;
    }

    public void setManzanaVeredaCodigo(String manzanaVeredaCodigo) {
        this.manzanaVeredaCodigo = manzanaVeredaCodigo;
    }

    public AreaCapturaOferta getAreaSinRecolectorSeleccionada() {
        return areaCapturaOferta;
    }

    public void setAreaSinRecolectorSeleccionada(AreaCapturaOferta areaSinRecolectorSeleccionada) {
        this.areaCapturaOferta = areaSinRecolectorSeleccionada;
    }

    public boolean isBanderaRural() {
        return banderaRural;
    }

    public void setBanderaRural(boolean banderaRural) {
        this.banderaRural = banderaRural;
    }

    public String getIdsAreasOferta() {
        return idsAreasOferta;
    }

    public void setIdsAreasOferta(String idsAreasOferta) {
        this.idsAreasOferta = idsAreasOferta;
    }

    public String getValorSeleccionManzanasOfertas() {
        return valorSeleccionManzanasOfertas;
    }

    public void setValorSeleccionManzanasOfertas(String valorSeleccionManzanasOfertas) {
        this.valorSeleccionManzanasOfertas = valorSeleccionManzanasOfertas;
    }

    public boolean isBanderaRecolectorSeleccionado() {
        return banderaRecolectorSeleccionado;
    }

    public void setBanderaRecolectorSeleccionado(boolean banderaRecolectorSeleccionado) {
        this.banderaRecolectorSeleccionado = banderaRecolectorSeleccionado;
    }

    public RegionCapturaOferta[] getListaRegionesSeleccionadasDesasignacion() {
        return listaRegionesSeleccionadasDesasignacion;
    }

    public void setListaRegionesSeleccionadasDesasignacion(
        RegionCapturaOferta[] listaRegionesSeleccionadasDesasignacion) {
        this.listaRegionesSeleccionadasDesasignacion = listaRegionesSeleccionadasDesasignacion;
    }

    public List<RegionCapturaOferta> getRegionesRecolectorSeleccionado() {
        return regionesRecolectorSeleccionado;
    }

    public void setListaRegionesRecolector(List<RegionCapturaOferta> listaRegionesRecolector) {
        this.regionesRecolectorSeleccionado = listaRegionesRecolector;
    }

    public String getIdRegion() {
        return regionId;
    }

    public void setIdRegion(String idRegion) {
        this.regionId = idRegion;
    }

    public String getObservacionRegion() {
        return observacionRegion;
    }

    public void setObservacionRegion(String observacionRegion) {
        this.observacionRegion = observacionRegion;
    }

    public String getPosicionVentanaInformacionRegion() {
        return posicionVentanaInformacionRegion;
    }

    public void setPosicionVentanaInformacionRegion(String posicionVentanaInformacionRegion) {
        this.posicionVentanaInformacionRegion = posicionVentanaInformacionRegion;
    }

    public List<DetalleCapturaOferta> getListaDetallesCapturaOferta() {
        return listaDetallesCapturaOferta;
    }

    public void setListaDetallesCapturaOferta(List<DetalleCapturaOferta> listaDetallesCapturaOferta) {
        this.listaDetallesCapturaOferta = listaDetallesCapturaOferta;
    }

    public Long getAreaCapturaOfertaId() {
        return areaCapturaOfertaId;
    }

    public void setAreaCapturaOfertaId(Long areaCapturaOfertaId) {
        this.areaCapturaOfertaId = areaCapturaOfertaId;
    }

    public List<RegionCapturaOferta> getRegionesCapturaArea() {
        return regionesCapturaArea;
    }

    public void setRegionesCapturaArea(List<RegionCapturaOferta> regionesCapturaArea) {
        this.regionesCapturaArea = regionesCapturaArea;
    }

    public RegionCapturaOferta getRegionSeleccionada() {
        return regionSeleccionada;
    }

    public void setRegionSeleccionada(RegionCapturaOferta regionSeleccionada) {
        this.regionSeleccionada = regionSeleccionada;
    }

    public boolean isBanderaEsCoordinador() {
        return this.banderaEsCoordinador;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("iniciando AsignacionRecolectorMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.banderaEsCoordinador = false;

        this.extraerDatosArbolProcesos();
        this.iniciarVariables();

        this.inicializarVariablesVisorGIS();
        this.extraerInformacionCatastro();
        this.cargarRegionesAreaSeleccionada();
        this.cargarListaRecolectores();

        LOGGER.debug("finalizando AsignacionRecolectorMB#init");

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Determina si el usuario activo es coordinador GIT, dependiendo de estro se habilitaran las
     * opciones de nueva y visor gis
     *
     * @author christian.rodriguez
     * @return verdadero si el usuario activo es COORDINADOR_GIT, falso si no
     */
    private boolean usuarioEsCoordinadorGIT() {
        String[] rolesUsuario = usuario.getRoles();
        for (String rol : rolesUsuario) {
            if (rol.equalsIgnoreCase(ERol.COORDINADOR_GIT_AVALUOS.toString())) {
                this.banderaEsCoordinador = true;
                return true;
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------------
    /**
     * Extrae la información del área seleccionada del arbol de tareas
     *
     * @author christian.rodriguez
     */
    private void extraerDatosArbolProcesos() {

        this.bpmActivityInstance = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.areaCapturaOfertaId = this.bpmActivityInstance.getIdObjetoNegocio();

        this.idsAreasOferta = String.valueOf(this.areaCapturaOfertaId);

        this.cargarAreaCapturaOfertaSeleccionada();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Carga el area de captura seleccionada desde el arbol de tareas
     *
     * @author christian.rodriguez
     */
    private void cargarAreaCapturaOfertaSeleccionada() {
        this.areaCapturaOferta = this.getAvaluosService().buscarAreaCapturaOfertaPorId(
            this.areaCapturaOfertaId);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar listas, banderas y cadenas de texto
     *
     * @author rodrigo.hernandez
     */
    private void iniciarVariables() {

        this.recolectorSeleccionado = null;
        this.regionSeleccionada = null;
        this.manzanaVeredaCodigo = new String();
        this.listaRegionesSeleccionadasDesasignacion = new RegionCapturaOferta[]{};

    }

    // -------------------------------------------------------------------------------
    /**
     * Determina si el área de captura está asociada a un catastro centralizado o no
     *
     * @author rodrigo.hernandez
     */
    private void extraerInformacionCatastro() {

        if (this.banderaEsCoordinador) {
            this.posicionVentanaInformacionRegion = "";
        } else {
            this.posicionVentanaInformacionRegion = "left,center";
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {
        LOGGER.debug("iniciando AsignacionRecolectorMB#inicializarVariablesVisorGIS");

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.OFERTAS_INMOBILIARIAS.getCodigo();
        this.valorSeleccionManzanasOfertas =
            EVisorGISParametrosAsignacion.ASIGNAR_MANZANAS_A_RECOLECTOR_OFERTAS
                .getCodigo();

        LOGGER.debug("finalizando AsignacionRecolectorMB#inicializarVariablesVisorGIS");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que indica si el tipo de Area de Oferta es rural o urbana
     *
     * @author rodrigo.hernandez
     *
     * @return True si es zona rural False si es zona urbana
     */
    private boolean validarTipoDeAreaOferta() {

        boolean bandera = false;

        LOGGER.debug("iniciando AsignacionRecolectorMB#validarTipoDeAreaOferta");

        if (this.areaCapturaOferta.getZona() != null) {
            if (this.areaCapturaOferta.getZona().getCodigo().endsWith("00")) {
                bandera = true;
            } else {
                bandera = false;
            }
        } else {
            bandera = false;
        }

        LOGGER.debug("finalizando AsignacionRecolectorMB#validarTipoDeAreaOferta");

        return bandera;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para cargar lista de recolectores
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaRecolectores() {

        LOGGER.debug("iniciando AsignacionRecolectorMB#cargarListaRecolectores");

        List<UsuarioDTO> listaUsuariosRolRecolector = null;
        List<String> listaIdsUsuariosRolRecolector = new ArrayList<String>();

        String estructuraOrganizacional = this.usuario.getDescripcionEstructuraOrganizacional();

        if (estructuraOrganizacional == null) {
            estructuraOrganizacional = "0000";
        }

        listaUsuariosRolRecolector = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
            estructuraOrganizacional, ERol.RECOLECTOR_OFERTA_INMOBILIARIA);

        for (UsuarioDTO usuarioDto : listaUsuariosRolRecolector) {
            listaIdsUsuariosRolRecolector.add(usuarioDto.getLogin() + "-" +
                 usuarioDto.getNombreCompleto());
        }

        this.listaRecolectores = this.getAvaluosService()
            .getInformacionRecolectores(listaIdsUsuariosRolRecolector,
                this.areaCapturaOfertaId);

        LOGGER.debug("finalizando AsignacionRecolectorMB#cargarListaRecolectores");

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Carga los puntos que componen un area para visualizacion en el mapa
     *
     * @author rodrigo.hernandez
     */
    private void cargarDetalleAreaCreadaConVisorGIS() {

        LOGGER.debug("iniciando AsignacionRecolectorMB#cargarDetalleAreaCentralizada");

        this.banderaRural = validarTipoDeAreaOferta();

        this.manzanaVeredaCodigo = new String();

        for (DetalleCapturaOferta detalle : this.areaCapturaOferta.getDetalleCapturaOfertas()) {

            if (detalle.getRegionCapturaOferta() == null) {
                if (!this.manzanaVeredaCodigo.isEmpty()) {
                    this.manzanaVeredaCodigo = this.manzanaVeredaCodigo.concat("," +
                         detalle.getManzanaVeredaCodigo());
                } else {
                    this.manzanaVeredaCodigo = detalle.getManzanaVeredaCodigo();
                }
            }

        }

        if (this.manzanaVeredaCodigo.isEmpty()) {
            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje =
                "No hay manzanas disponibles para asignar a una región para el área de trabajo.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

        LOGGER.debug("finalizando AsignacionRecolectorMB#cargarDetalleAreaCentralizada");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que carga los datos necesarios para trabajar con un area descentralizada
     *
     * @author rodrigo.hernandez
     */
    private void cargarDetalleAreaCreadaManualmente() {

        LOGGER.debug("iniciando AsignacionRecolectorMB#cargarDetalleAreaDescentralizada");

        this.observacionRegion = "";

        LOGGER.debug("finalizando AsignacionRecolectorMB#cargarDetalleAreaDescentralizada");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para activar la ventana donde se crea una región de captura
     *
     * @author rodrigo.hernandez
     */
    public void cargarVentanaCreacion() {
        LOGGER.debug("iniciando AsignacionRecolectorMB#cargarVentanaCreacion");

        this.regionId = String
            .valueOf(this.getAvaluosService().consultarIdUltimaRegionRegistrada() + 1);

        if (this.usuarioEsCoordinadorGIT()) {
            this.cargarDetalleAreaCreadaManualmente();
        } else {
            this.cargarDetalleAreaCreadaConVisorGIS();
        }

        LOGGER.debug("finalizando AsignacionRecolectorMB#cargarVentanaCreacion");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para activar la ventana donde se consultan las regiones asignadas a un recolector,
     * tambien carga las manzanas asociadas a las regiones del recolector para ser visualizadas en
     * el VISOR GIS
     *
     * @author rodrigo.hernandez
     */
    public void cargarVentanaConsulta() {
        LOGGER.debug("iniciando AsignacionRecolectorMB#cargarVentanaConsulta");

        this.cargarRegionesAreaRecolector();

        this.manzanaVeredaCodigo = new String();

        if (this.regionesRecolectorSeleccionado != null) {

            for (RegionCapturaOferta region : this.regionesRecolectorSeleccionado) {
                for (DetalleCapturaOferta detalle : region.getDetalleCapturaOfertas()) {

                    if (detalle != null && !detalle.getManzanaVeredaCodigo().isEmpty()) {
                        if (!this.manzanaVeredaCodigo.isEmpty()) {
                            this.manzanaVeredaCodigo = this.manzanaVeredaCodigo.concat(",").concat(
                                detalle.getManzanaVeredaCodigo());
                        } else {
                            this.manzanaVeredaCodigo = detalle.getManzanaVeredaCodigo();
                        }
                    }
                }
            }
        }

        LOGGER.debug("finalizando AsignacionRecolectorMB#cargarVentanaConsulta");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Carga las regiones de captura del area seleccionada
     *
     * @author christian.rodriguez
     */
    private void cargarRegionesAreaSeleccionada() {

        this.regionesCapturaArea = this.getAvaluosService()
            .obtenerRegionesCapturaOfertaPorAreaCapturaOferta(this.areaCapturaOfertaId);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para cargar las regiones de un recolector
     *
     * @author rodrigo.hernandez
     */
    private void cargarRegionesAreaRecolector() {

        LOGGER.debug("iniciando AsignacionRecolectorMB#cargarRegionesAreaRecolector");

        this.regionesRecolectorSeleccionado = this.getAvaluosService()
            .obtenerRegionesCapturaOfertaDeAreaCapturaOfertaYRecolector(
                this.areaCapturaOferta.getId(), this.recolectorSeleccionado.getLogin());

        LOGGER.debug("finalizando AsignacionRecolectorMB#cargarRegionesAreaRecolector");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Listener usado para crear una región de captura
     *
     * @author christian.rodriguez
     */
    public void crearRegionCaptura() {

        RegionCapturaOferta rco = new RegionCapturaOferta();

        rco = new RegionCapturaOferta();
        rco.setAreaCapturaOferta(this.areaCapturaOferta);
        rco.setUsuarioLog(this.usuario.getLogin());
        rco.setFechaAsignacionRecolector(Calendar.getInstance().getTime());
        rco.setFechaLog(new Date());
        rco.setEstado(EEstadoRegionCapturaOferta.CREADA.getEstado());
        rco.setObservaciones(observacionRegion);

        rco = this.getAvaluosService().crearRegionCapturaOferta(rco);

        if (this.manzanaVeredaCodigo != null &&
             !this.manzanaVeredaCodigo.isEmpty()) {
            // Se asocian las manzanas/veredas seleccionadas a la región
            this.getAvaluosService().asociarManzanasVeredasARegionCreada(
                this.areaCapturaOferta, rco, this.manzanaVeredaCodigo);
        }

        this.forwardProcess(rco);

        this.cargarAreaCapturaOfertaSeleccionada();
        this.cargarRegionesAreaSeleccionada();
        this.iniciarVariables();

        this.addMensajeInfo("Creada la región con id " + rco.getId());

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Listener que asocia un recolector seleccionado con una región seleccionada
     *
     * @author christian.rodriguez
     */
    public void asignarRecolector() {
        LOGGER.debug("iniciando AsignacionRecolectorMB#asignarArea");

        if (this.regionSeleccionada != null && this.recolectorSeleccionado != null) {

            if (this.regionSeleccionada.getEstado().equals(
                EEstadoRegionCapturaOferta.CREADA.getEstado())) {

                this.regionSeleccionada.setAsignadoRecolectorId(this.recolectorSeleccionado
                    .getLogin());
                this.regionSeleccionada.setAsignadoRecolectorNombre(this.recolectorSeleccionado
                    .getNombreFuncionario());
                this.regionSeleccionada.setEstado(EEstadoRegionCapturaOferta.ASIGNADA.getEstado());

                this.regionSeleccionada = this.getAvaluosService()
                    .guardarActualizarRegionCapturaOferta(this.regionSeleccionada);

                this.cargarListaRecolectores();
                this.cargarRegionesAreaSeleccionada();
                this.iniciarVariables();

                this.addMensajeInfo("Asignación exitosa.");

            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje =
                    "Solo puede realizar asignaciones en regiones que se encuentren en estado " +
                     EEstadoRegionCapturaOferta.CREADA.getEstado() + ".";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "Debe seleccionar una región y un recolector.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

        LOGGER.debug("finalizando AsignacionRecolectorMB#asignarArea");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Desasigna el recolector de la region de captura seleccionada siempre y cuando el estado de la
     * region sea 'CREADA'
     *
     * @author christian.rodriguez
     */
    public void desasignarRecolector() {

        if (this.regionSeleccionada != null) {

            if (this.regionSeleccionada.getEstado().equals(
                EEstadoRegionCapturaOferta.ASIGNADA.getEstado())) {

                this.regionSeleccionada.setAsignadoRecolectorId(null);
                this.regionSeleccionada.setAsignadoRecolectorNombre(null);
                this.regionSeleccionada.setEstado(EEstadoRegionCapturaOferta.CREADA.getEstado());

                this.regionSeleccionada = this.getAvaluosService()
                    .guardarActualizarRegionCapturaOferta(this.regionSeleccionada);

                this.cargarListaRecolectores();
                this.cargarRegionesAreaSeleccionada();
                this.iniciarVariables();

                this.addMensajeInfo("Desasignación exitosa.");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje =
                    "Solo puede realizar desasignaciones en regiones que se encuentren en estado " +
                     EEstadoRegionCapturaOferta.ASIGNADA.getEstado() + ".";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "Debe seleccionar una región.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * crea la instancia del proceso
     *
     * @author pedro.garcia
     * @return false si no se pudo crear la instancia
     */
    private boolean forwardProcess(RegionCapturaOferta rco) {

        LOGGER.debug("Creando proceso Ofertas");

        boolean error = false;
        String idProceso;

        String transicion = ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_PROYECTAR_ORDEN_COMISION;
        String operacion = ProcesoDeOfertasInmobiliarias.OPERACION_CREAR_REGION;

        OfertaInmobiliaria ofertaInmobiliariaBPM = this.crearObjetoNegocioOfertas(rco, transicion,
            operacion);

        try {
            idProceso = this.getProcesosService().crearProceso(ofertaInmobiliariaBPM);
            LOGGER.debug("proceso creado con id " + idProceso);

            this.doDatabaseStatesUpdate(rco, idProceso);
        } catch (Exception ex) {
            LOGGER.error("Error creando la instancia del proceso: " + ex.getMessage());
            error = true;
        }

        return error;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Crea un objeto de negocio de tipo oferta inmobiliaria con lsod atos asociados al área y
     * region de ofertas
     *
     * @author christian.rodriguez
     * @param rco región de captura asociada al proceso
     * @param transicion transicion del proceso
     * @param operacion operacion del proceso
     * @return objeto de negocio con los datos del negocio
     */
    private OfertaInmobiliaria crearObjetoNegocioOfertas(RegionCapturaOferta rco,
        String transicion, String operacion) {
        List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
        OfertaInmobiliaria ofertaInmobiliariaBPM;
        DivisionAdministrativa municipioBPM, departamentoBPM, zonaBPM;

        listaUsuarios.add(this.usuario);

        // D: objeto OfertaInmobiliaria es el objeto de negocio del proceso
        ofertaInmobiliariaBPM = new OfertaInmobiliaria();
        municipioBPM = this.bpmActivityInstance.getMunicipio();
        departamentoBPM = this.bpmActivityInstance.getDepartamento();
        zonaBPM = this.bpmActivityInstance.getZona();

        ofertaInmobiliariaBPM.setMunicipio(municipioBPM);
        ofertaInmobiliariaBPM.setDepartamento(departamentoBPM);
        ofertaInmobiliariaBPM.setZona(zonaBPM);

        ofertaInmobiliariaBPM.setIdCorrelacion(this.areaCapturaOfertaId);
        ofertaInmobiliariaBPM.setIdentificador(rco.getId());
        ofertaInmobiliariaBPM.setObservaciones("Creación de región captura ofertas");
        ofertaInmobiliariaBPM.setTransicion(transicion);

        ofertaInmobiliariaBPM.setUsuarios(listaUsuarios);
        ofertaInmobiliariaBPM.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        ofertaInmobiliariaBPM.setFuncionario(this.usuario.getNombreCompleto());

        if (operacion != null) {
            ofertaInmobiliariaBPM.setOperacion(operacion);
        }

        return ofertaInmobiliariaBPM;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Actualiza la región captura oferta con el id de proceso.
     *
     * @author christian.rodriguez
     * @param rco RegionCapturaOferta que se quiere asociar al proceso
     * @param idProceso id del proceso que se quiere asociar a la región
     */
    private void doDatabaseStatesUpdate(RegionCapturaOferta rco, String idProceso) {
        try {
            rco.setProcesoInstanciaId(idProceso);
            rco = this.getAvaluosService().guardarActualizarRegionCapturaOferta(rco);
        } catch (Exception ex) {
            LOGGER.error("Error actualizando la RegionCapturaOferta con id " + rco.getId());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }
}
