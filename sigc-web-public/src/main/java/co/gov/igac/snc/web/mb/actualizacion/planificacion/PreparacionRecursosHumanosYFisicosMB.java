package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionEvento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionResponsable;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionSedeComision;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.actualizacion.Dotacion;
import co.gov.igac.snc.persistence.entity.actualizacion.EventoAsistente;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETipoRecursoHumano;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 *
 * @author franz.gamba
 *
 */
@Component("prepararRecursosHumanosyFisicos")
@Scope("session")
public class PreparacionRecursosHumanosYFisicosMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 3679776132192063234L;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    // ---------------------------------------------------------------------------------------------------------------------
    /*
     * Variables
     */
    private UsuarioDTO usuario;
    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionarEstudioDeCostosMB.class);

    /** Nombre del departamento al que pertenece el municipio en actualización. */
    private String departamentoNombre;
    /** Nombre del municipio que va a ser actualizado. */
    private String municipioNombre;
    /** Nombre del tipo de proyecto: Misional o Convenio. */
    private String tipoProyectoNombre;
    /** Nombre la zona: urbana, rural, urbana/rural */
    private String zonaNombre;
    /** Número de convenio. */
    private String convenioNumero;
    /** lista de convenios asociados con la resolución en curso */
    private List<Convenio> convenios;
    private Actualizacion actualizacion;

    /*
     * Datos para la asignacion de responsable de actualizacion
     */
    private boolean asignarResponsableActualizacion = false;

    private List<SelectItem> responsablesActualizacion;
    private List<SelectItem> funcionariosActualizacion;
    private List<UsuarioDTO> listaFuncionarios;
    private List<ActualizacionContrato> listaContratistas;
    private String selectedResponsable;
    private List<UsuarioDTO> funcionariosTerritorial;
    private UsuarioDTO selectedFuncionario;
    private UsuarioDTO funcionarioAEliminar;
    private List<RecursoHumano> recursosHumanosActualizacion;
    private List<UsuarioDTO> responsablesActualizacionDTO;

    /*
     * Datos para el registro de Induccion
     */
    private boolean registrarInduccion = false;

    private List<ActualizacionEvento> capacitaciones;
    private ActualizacionEvento nuevaCapacitacion;
    private ActualizacionContrato[] selectedAsistentes;
    private Date selectedDate;
    private boolean asistentesRegistrados;
    private boolean capacitacionesRegistradas;
    private List<EventoAsistente> asistenetesPorAsignar;
    private ActualizacionEvento actaSeleccionada;
    private String rutaDocumento;

    /*
     * Datos para el registro de sede y suministro de dotacion
     */
    private boolean registroSedeComision = false;
    private boolean registroDotacion = false;

    private List<Dotacion> dotaciones;
    private List<Dotacion> nuevaDotacion;
    private Dotacion dotacionSeleccionada;
    private boolean dotadosRegistrados;
    private String departamentoComision;
    private String municipioComision;
    private String direccionComision;
    private String soporteSede;
    private SelectItem selectedTipoSoporte;
    private List<SelectItem> tipoSoporteSede;
    // Listas generadas por las busquedas
    private List<SelectItem> departamentosColombia;
    private List<SelectItem> municipiosColombia;
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;

    private Departamento departamento;
    private Municipio municipio;
    private String departamentoCod;

    private ActualizacionSedeComision sedeComision;

    //recurso humano a recibir dotación
    private List<RecursoHumano> recursoHumanoADotar;
    private RecursoHumano[] selectedRecursoHumano;
    private List<Dotacion> dotacionPorAsignar;
    private String descripcionDotacion;
    private Date fechaDotacion;

    // ---------------------------------------------------------------------------------------------------------------------
    /*
     * Métodos
     */
    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getTipoProyectoNombre() {
        return tipoProyectoNombre;
    }

    public void setTipoProyectoNombre(String tipoProyectoNombre) {
        this.tipoProyectoNombre = tipoProyectoNombre;
    }

    public String getZonaNombre() {
        return zonaNombre;
    }

    public void setZonaNombre(String zonaNombre) {
        this.zonaNombre = zonaNombre;
    }

    public String getConvenioNumero() {
        return convenioNumero;
    }

    public void setConvenioNumero(String convenioNumero) {
        this.convenioNumero = convenioNumero;
    }

    public List<Convenio> getConvenios() {
        return convenios;
    }

    public void setConvenios(List<Convenio> convenios) {
        this.convenios = convenios;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<SelectItem> getResponsablesActualizacion() {
        return responsablesActualizacion;
    }

    public void setResponsablesActualizacion(
        List<SelectItem> responsablesActualizacion) {
        this.responsablesActualizacion = responsablesActualizacion;
    }

    public List<SelectItem> getFuncionariosActualizacion() {
        return funcionariosActualizacion;
    }

    public void setFuncionariosActualizacion(
        List<SelectItem> funcionariosActualizacion) {
        this.funcionariosActualizacion = funcionariosActualizacion;
    }

    public List<UsuarioDTO> getListaFuncionarios() {
        return listaFuncionarios;
    }

    public void setListaFuncionarios(List<UsuarioDTO> listaFuncionarios) {
        this.listaFuncionarios = listaFuncionarios;
    }

    public List<ActualizacionContrato> getListaContratistas() {
        return listaContratistas;
    }

    public void setListaContratistas(
        List<ActualizacionContrato> listaContratistas) {
        this.listaContratistas = listaContratistas;
    }

    public String getSelectedResponsable() {
        return selectedResponsable;
    }

    public void setSelectedResponsable(String selectedResponsable) {
        this.selectedResponsable = selectedResponsable;
    }

    public List<UsuarioDTO> getFuncionariosTerritorial() {
        return funcionariosTerritorial;
    }

    public void setFuncionariosTerritorial(
        List<UsuarioDTO> funcionariosTerritorial) {
        this.funcionariosTerritorial = funcionariosTerritorial;
    }

    public UsuarioDTO getSelectedFuncionario() {
        return selectedFuncionario;
    }

    public void setSelectedFuncionario(UsuarioDTO selectedFuncionario) {
        this.selectedFuncionario = selectedFuncionario;
    }

    public UsuarioDTO getFuncionarioAEliminar() {
        return funcionarioAEliminar;
    }

    public void setFuncionarioAEliminar(UsuarioDTO funcionarioAEliminar) {
        this.funcionarioAEliminar = funcionarioAEliminar;
    }

    public String getDepartamentoComision() {
        return departamentoComision;
    }

    public void setDepartamentoComision(String departamentoComision) {
        this.departamentoComision = departamentoComision;
    }

    public String getMunicipioComision() {
        return municipioComision;
    }

    public void setMunicipioComision(String municipioComision) {
        this.municipioComision = municipioComision;
    }

    public String getDireccionComision() {
        return direccionComision;
    }

    public void setDireccionComision(String direccionComision) {
        this.direccionComision = direccionComision;
    }

    public String getSoporteSede() {
        return soporteSede;
    }

    public void setSoporteSede(String soporteSede) {
        this.soporteSede = soporteSede;
    }

    public List<SelectItem> getTipoSoporteSede() {
        return tipoSoporteSede;
    }

    public void setTipoSoporteSede(List<SelectItem> tipoSoporteSede) {
        this.tipoSoporteSede = tipoSoporteSede;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    public List<ActualizacionEvento> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(List<ActualizacionEvento> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }

    public ActualizacionEvento getNuevaCapacitacion() {
        return nuevaCapacitacion;
    }

    public void setNuevaCapacitacion(ActualizacionEvento nuevaCapacitacion) {
        this.nuevaCapacitacion = nuevaCapacitacion;
    }

    public ActualizacionContrato[] getSelectedAsistentes() {
        return selectedAsistentes;
    }

    public void setSelectedAsistentes(ActualizacionContrato[] selectedAsistentes) {
        this.selectedAsistentes = selectedAsistentes;
    }

    public boolean isAsistentesRegistrados() {
        return asistentesRegistrados;
    }

    public void setAsistentesRegistrados(boolean asistentesRegistrados) {
        this.asistentesRegistrados = asistentesRegistrados;
    }

    public List<EventoAsistente> getAsistenetesPorAsignar() {
        return asistenetesPorAsignar;
    }

    public void setAsistenetesPorAsignar(
        List<EventoAsistente> asistenetesPorAsignar) {
        this.asistenetesPorAsignar = asistenetesPorAsignar;
    }

    public boolean isCapacitacionesRegistradas() {
        return capacitacionesRegistradas;
    }

    public void setCapacitacionesRegistradas(boolean capacitacionesRegistradas) {
        this.capacitacionesRegistradas = capacitacionesRegistradas;
    }

    public ActualizacionEvento getActaSeleccionada() {
        return actaSeleccionada;
    }

    public void setActaSeleccionada(ActualizacionEvento actaSeleccionada) {
        this.actaSeleccionada = actaSeleccionada;
    }

    public String getRutaDocumento() {
        return rutaDocumento;
    }

    public void setRutaDocumento(String rutaDocumento) {
        this.rutaDocumento = rutaDocumento;
    }

    public List<SelectItem> getDepartamentosColombia() {
        return departamentosColombia;
    }

    public void setDepartamentosColombia(List<SelectItem> departamentosColombia) {
        this.departamentosColombia = departamentosColombia;
    }

    public List<SelectItem> getMunicipiosColombia() {
        return municipiosColombia;
    }

    public void setMunicipiosColombia(List<SelectItem> municipiosColombia) {
        this.municipiosColombia = municipiosColombia;
    }

    public EOrden getOrdenDepartamentos() {
        return ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public String getDepartamentoCod() {
        return departamentoCod;
    }

    public void setDepartamentoCod(String departamentoCod) {
        this.departamentoCod = departamentoCod;
    }

    public ActualizacionSedeComision getSedeComision() {
        return sedeComision;
    }

    public void setSedeComision(ActualizacionSedeComision sedeComision) {
        this.sedeComision = sedeComision;
    }

    public List<UsuarioDTO> getResponsablesActualizacionDTO() {
        return responsablesActualizacionDTO;
    }

    public void setResponsablesActualizacionDTO(
        List<UsuarioDTO> responsablesActualizacionDTO) {
        this.responsablesActualizacionDTO = responsablesActualizacionDTO;
    }

    public boolean isAsignarResponsableActualizacion() {
        return asignarResponsableActualizacion;
    }

    public void setAsignarResponsableActualizacion(
        boolean asignarResponsableActualizacion) {
        this.asignarResponsableActualizacion = asignarResponsableActualizacion;
    }

    public boolean isRegistrarInduccion() {
        return registrarInduccion;
    }

    public void setRegistrarInduccion(boolean registrarInduccion) {
        this.registrarInduccion = registrarInduccion;
    }

    public boolean isRegistroSedeComision() {
        return registroSedeComision;
    }

    public void setRegistroSedeComision(boolean registroSedeComision) {
        this.registroSedeComision = registroSedeComision;
    }

    public boolean isRegistroDotacion() {
        return registroDotacion;
    }

    public void setRegistroDotacion(boolean registroDotacion) {
        this.registroDotacion = registroDotacion;
    }

    public List<RecursoHumano> getRecursosHumanosActualizacion() {
        return recursosHumanosActualizacion;
    }

    public void setRecursosHumanosActualizacion(
        List<RecursoHumano> recursosHumanosActualizacion) {
        this.recursosHumanosActualizacion = recursosHumanosActualizacion;
    }

    public List<Dotacion> getDotaciones() {
        return dotaciones;
    }

    public void setDotaciones(List<Dotacion> dotaciones) {
        this.dotaciones = dotaciones;
    }

    public List<Dotacion> getNuevaDotacion() {
        return nuevaDotacion;
    }

    public void setNuevaDotacion(List<Dotacion> nuevaDotacion) {
        this.nuevaDotacion = nuevaDotacion;
    }

    public boolean isDotadosRegistrados() {
        return dotadosRegistrados;
    }

    public void setDotadosRegistrados(boolean dotadosRegistrados) {
        this.dotadosRegistrados = dotadosRegistrados;
    }

    public List<RecursoHumano> getRecursoHumanoADotar() {
        return recursoHumanoADotar;
    }

    public void setRecursoHumanoADotar(
        List<RecursoHumano> recursoHumanoADotar) {
        this.recursoHumanoADotar = recursoHumanoADotar;
    }

    public RecursoHumano[] getSelectedRecursoHumano() {
        return selectedRecursoHumano;
    }

    public void setSelectedRecursoHumano(RecursoHumano[] selectedRecursoHumano) {
        this.selectedRecursoHumano = selectedRecursoHumano;
    }

    public List<Dotacion> getDotacionPorAsignar() {
        return dotacionPorAsignar;
    }

    public void setDotacionPorAsignar(List<Dotacion> dotacionPorAsignar) {
        this.dotacionPorAsignar = dotacionPorAsignar;
    }

    public String getDescripcionDotacion() {
        return descripcionDotacion;
    }

    public void setDescripcionDotacion(String descripcionDotacion) {
        this.descripcionDotacion = descripcionDotacion;
    }

    public Date getFechaDotacion() {
        return fechaDotacion;
    }

    public void setFechaDotacion(Date fechaDotacion) {
        this.fechaDotacion = fechaDotacion;
    }

    public Dotacion getDotacionSeleccionada() {
        return dotacionSeleccionada;
    }

    public void setDotacionSeleccionada(Dotacion dotacionSeleccionada) {
        this.dotacionSeleccionada = dotacionSeleccionada;
    }

    // ---------------------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.currentActivity = this.tareasPendientesMB
            .getInstanciaSeleccionada();

        Long idActualizacion = this.tareasPendientesMB
            .getInstanciaSeleccionada().getIdObjetoNegocio();

        //long idActualizacion = 250;
        this.actualizacion = this.getActualizacionService()
            .recuperarActualizacionPorId(idActualizacion);

        this.departamentoNombre = actualizacion.getDepartamento().getNombre();
        this.municipioNombre = actualizacion.getMunicipio().getNombre();
        this.convenios = this.getActualizacionService()
            .recuperarConveniosPorIdActualizacion(actualizacion.getId());
        if (convenios != null && convenios.size() > 0) {

            // TODO franz.gamba :: comprobar que el convenio es el primero en la
            // lista de convenios
            // de no serlo, cuadrar para que trabaje con el convenio que es.
            this.tipoProyectoNombre = this.convenios.get(0).getTipoProyecto();
            this.zonaNombre = this.convenios.get(0).getZona();
            this.convenioNumero = this.convenios.get(0).getNumero();
            this.capacitacionesRegistradas = false;
        }
        this.inicializarRegistroActasInduccion();

    }

    /**
     * Método para cargar los departamentos de Colombia.
     */
    public void cargarDepartamentosColombia() {
        this.departamentosColombia = new LinkedList<SelectItem>();
        this.departamentosColombia.add(new SelectItem(null, "Seleccionar..."));

        List<Departamento> departamentosDeColombia = this.getGeneralesService()
            .getCacheDepartamentosPorPais("140");
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(departamentosDeColombia);
        } else {
            Collections.sort(departamentosDeColombia, Departamento.getComparatorNombre());
        }
        for (Departamento departamento : departamentosDeColombia) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                departamentosColombia.add(new SelectItem(departamento.getCodigo() + "-" +
                     departamento.getNombre(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                departamentosColombia.add(new SelectItem(departamento.getCodigo() + "-" +
                     departamento.getNombre(), departamento.getNombre()));
                this.departamento = departamento;
            }
        }
    }

    /**
     * Método para cargar los municipios que corresponden al departamento seleccionado.
     */
    private void cargarMunicipiosDepartamento() {
        municipiosColombia = new LinkedList<SelectItem>();
        municipiosColombia.add(new SelectItem(null, "Seleccionar..."));

        if (this.departamentoComision != null) {
            List<Municipio> municipiosDeColombia = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(departamentoCod);
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(municipiosDeColombia);
            } else {
                Collections.sort(municipiosDeColombia, Municipio.getComparatorNombre());
            }
            for (Municipio municipioColombia : municipiosDeColombia) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    municipiosColombia.add(new SelectItem(municipioColombia.getCodigo() + "-" +
                         municipioColombia.getNombre(), municipioColombia.getCodigo3Digitos() +
                         "-" + municipioColombia.getNombre()));
                } else {
                    municipiosColombia.add(new SelectItem(municipioColombia.getCodigo() + "-" +
                         municipioColombia.getNombre(), municipioColombia.getNombre()));
                }
            }
        }
    }

    /**
     * Método que maneja el cambio de un departamento y la correspondiente actualización de los
     * municipios que pertenecen al departamento.
     */
    public void onCambioDepartamento() {
        LOGGER.debug("onCambioDepartamento...");

        String[] divided = this.departamentoComision.split("-");
        departamentoCod = divided[0];
        cargarMunicipiosDepartamento();
        this.municipioComision = null;

    }

    public SelectItem getSelectedTipoSoporte() {
        return selectedTipoSoporte;
    }

    public void setSelectedTipoSoporte(SelectItem selectedTipoSoporte) {
        this.selectedTipoSoporte = selectedTipoSoporte;
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar el reguistro de actas de inducción
     */
    public void inicializarRegistroActasInduccion() {
        if (!this.capacitacionesRegistradas) {
            this.capacitaciones = new ArrayList<ActualizacionEvento>();
        } else {
            this.capacitaciones = this.actualizacion
                .getActualizacionEventos();
        }
    }
    // -------------------------------------------------------------------------------------------------

    /**
     * Método para iniciar el reguistro de actas de dotación
     */
    public void inicializarRegistroDotaciones() {
        //carga las dotaciones de la actualización en caso de que ya tenga
        if (!this.dotadosRegistrados) {
            this.dotaciones = new ArrayList<Dotacion>();
        } else {
            this.dotaciones = this.actualizacion
                .getDotacions();
        }
        //trae el recurso humano
        //TODO franz.gamba:: validar si la dotacion cambia segun sea el responsable de actualizacion
        ActualizacionResponsable responsable = this.actualizacion.getActualizacionResponsables().
            get(0);
        if (responsable != null) {
            this.recursoHumanoADotar = responsable.getRecursoHumanos();
        } else {
            this.
                addMensajeError("No existen responsables asignados a este proceso de Actualización");
        }

    }

    // -------------------------------------------------------------------------------------------------
    public void inicializarAsignacionActualizacion() {
        this.responsablesActualizacionDTO = new ArrayList<UsuarioDTO>();
        this.responsablesActualizacion = new ArrayList<SelectItem>();
        this.funcionariosActualizacion = new ArrayList<SelectItem>();
        this.listaFuncionarios = new ArrayList<UsuarioDTO>();
        this.listaContratistas = new ArrayList<ActualizacionContrato>();
        this.funcionariosTerritorial = new ArrayList<UsuarioDTO>();

        this.responsablesActualizacionDTO = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionTerritorial(),
                ERol.RESPONSABLE_ACTUALIZACION);

        this.responsablesActualizacion.add(new SelectItem(null, "Seleccionar"));
        if (this.responsablesActualizacionDTO != null) {
            for (UsuarioDTO user : this.responsablesActualizacionDTO) {

                this.responsablesActualizacion.add(new SelectItem(user.getLogin(), user
                    .getNombreCompleto()));
            }
        }
        this.inicializarAsignarFuncionarios();
        this.listaContratistas = this.getActualizacionService()
            .getContratosPorActualizacion(this.actualizacion.getId());
    }

    public void inicializarAsignarFuncionarios() {
        /*
         * llena la lista de funcionarios por territorial
         */
        this.funcionariosTerritorial = this.getGeneralesService()
            .getFuncionariosTerritorial(
                this.usuario.getDescripcionTerritorial(),
                this.usuario.getDescripcionUOC());
    }

    public void agregarFuncionarioDeTerritorial() {
        this.listaFuncionarios.add(this.selectedFuncionario);
    }

    public void eliminarFuncionarioDeTerritorial() {
        this.listaFuncionarios.remove(this.funcionarioAEliminar);
    }

    public void inicializaRegistroDeSede() {
        this.cargarDepartamentosColombia();
        this.tipoSoporteSede = new ArrayList<SelectItem>();
        this.tipoSoporteSede.add(new SelectItem(null, "Seleccionar..."));
    }

    public void crearNuevaCapacitacion() {
        this.nuevaCapacitacion = new ActualizacionEvento();
        this.nuevaCapacitacion.setActaDocumentoId(null);
        this.asistentesRegistrados = false;
    }

    public void crearNuevasDotaciones() {
        this.nuevaDotacion = new ArrayList<Dotacion>();
        this.dotacionPorAsignar = new ArrayList<Dotacion>();
        this.dotadosRegistrados = false;
    }

    /*
     *
     */
    public void asignarAsistentesCapacitacionANuevaCapacitacion() {
        if (this.selectedAsistentes != null) {
            List<EventoAsistente> asistentesConfirmados = new ArrayList<EventoAsistente>();
            for (ActualizacionContrato ac : this.selectedAsistentes) {
                EventoAsistente asistente = new EventoAsistente();
                asistente.setActualizacionEvento(this.nuevaCapacitacion);
                asistente.setDocumentoIdentificacion(ac
                    .getContratistaIdentificacion());
                asistente.setFechaLog(new Date());
                asistente.setNombre(ac.getContratistaNombre());
                asistente.setUsuarioLog(this.usuario.getLogin());

                asistentesConfirmados.add(asistente);
            }
            this.asistenetesPorAsignar = asistentesConfirmados;
            this.asistentesRegistrados = true;

            this.nuevaCapacitacion
                .setEventoAsistentes(this.asistenetesPorAsignar);

            this.addMensajeInfo("Se asignaron satisfactoriamente los asistentes a la capacitación");

        } else {
            this.addMensajeError("No se seleccionaron asistentes");
        }
    }

    /*
     *
     */
    public void asignarRecursoHumanoALaDotacion() {
        if (this.selectedRecursoHumano != null) {
            List<RecursoHumano> recursoHumanoConfirmado = new ArrayList<RecursoHumano>();
            for (RecursoHumano rh : this.selectedRecursoHumano) {
                Dotacion nDotacion = new Dotacion();
                nDotacion.setActualizacion(this.actualizacion);
                nDotacion.setFechaLog(new Date());
                nDotacion.setUsuarioLog(this.usuario.getLogin());
                nDotacion.setRecursoHumano(rh);

                this.dotacionPorAsignar.add(nDotacion);

                recursoHumanoConfirmado.add(rh);
            }

            this.dotadosRegistrados = true;

            this.addMensajeInfo("Se asignó satisfactoriamente el recurso humano a dotar");

        } else {
            this.addMensajeError("No se seleccionó recurso humano");
        }
    }

    /*
     *
     */
    public ActualizacionDocumento crearActaConDocumento() {
        /* creacion del documento */
        Documento nuevoDocumento = new Documento();
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumentoId.OTRO.getId());

        nuevoDocumento.setTipoDocumento(tipoDocumento);
        nuevoDocumento.setArchivo(this.rutaDocumento);
        nuevoDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        nuevoDocumento.setBloqueado(ESiNo.NO.getCodigo());
        nuevoDocumento.setUsuarioCreador(this.usuario.getLogin());
        nuevoDocumento.setUsuarioLog(this.usuario.getLogin());
        nuevoDocumento.setFechaLog(new Date());

        nuevoDocumento = this.getGeneralesService().guardarDocumento(nuevoDocumento);

        /* Creacion del ActualizacionDocumento */
        ActualizacionDocumento actDocumento = new ActualizacionDocumento();
        actDocumento.setActualizacion(this.actualizacion);
        actDocumento.setDocumento(nuevoDocumento);
        actDocumento.setFechaLog(new Date());
        actDocumento.setFueGenerado(true);
        actDocumento.setUsuarioLog(this.usuario.getLogin());

        actDocumento = this.getActualizacionService().
            guardarYActualizarActualizacionDocumento(actDocumento);

        return actDocumento;
    }

    /*
     *
     */
    public void agregarActaDeInduccionYCapacitacion() {

        ActualizacionDocumento actualizacionDocumento = this.crearActaConDocumento();

        this.nuevaCapacitacion.setActualizacion(this.actualizacion);
        this.nuevaCapacitacion.setUsuarioLog(this.usuario.getLogin());
        if (actualizacionDocumento != null && actualizacionDocumento.getId() != null) {
            this.nuevaCapacitacion.setActaDocumentoId(actualizacionDocumento.getId());
        }

        this.capacitaciones.add(this.nuevaCapacitacion);
        this.nuevaCapacitacion = this.getActualizacionService()
            .guardarYactualizarActualizacionEvento(this.nuevaCapacitacion);
        for (EventoAsistente ca : this.asistenetesPorAsignar) {
            ca.setActualizacionEvento(this.nuevaCapacitacion);
        }
        this.asistenetesPorAsignar = this.getActualizacionService()
            .guardarYactualizarEventoAsistentes(asistenetesPorAsignar);
    }

    /*
     *
     */
    public void agregarActasDeDotaciones() {

        ActualizacionDocumento actualizacionDocumento = this.crearActaConDocumento();
        for (Dotacion dot : this.dotacionPorAsignar) {
            dot.setFecha(this.fechaDotacion);
            dot.setDescripcion(this.descripcionDotacion);
            if (actualizacionDocumento != null &&
                 actualizacionDocumento.getId() != null) {
                dot.setSoporteDocumentoId(actualizacionDocumento.getId());
            }
            this.dotaciones.add(dot);
        }

        for (Dotacion dotacion : this.dotaciones) {
            this.actualizacion.getDotacions().add(dotacion);
        }

        //TODO franz.gamba :: persistir las dotaciones
    }

    /*
     *
     */
    public void agregarDotacion() {

        for (Dotacion d : this.nuevaDotacion) {
            d.setActualizacion(this.actualizacion);
            d.setUsuarioLog(this.usuario.getLogin());
            this.dotaciones.add(d);
        }
        //this.nuevaDotacion = this.getActualizacionService()
        //	.guardarYactualizarActualizacionEvento(this.nuevaCapacitacion);
        for (EventoAsistente ca : this.asistenetesPorAsignar) {
            ca.setActualizacionEvento(this.nuevaCapacitacion);
        }
        this.asistenetesPorAsignar = this.getActualizacionService()
            .guardarYactualizarEventoAsistentes(asistenetesPorAsignar);
    }

    /*
     *
     */
    public void asignarCapacitacionesActualizacion() {
        this.actualizacion.setActualizacionEventos(this.capacitaciones);
        this.capacitacionesRegistradas = true;
    }

    /*
     *
     */
    public void asignarDotacionesActualizacion() {
        this.actualizacion.setDotacions(this.dotaciones);
        this.dotadosRegistrados = true;
    }

    /*
     *
     */
    public void eliminarActaSeleccionada() {
        this.capacitaciones.remove(this.actaSeleccionada);
        if (this.actualizacion.
            getActualizacionEventos().
            contains(this.actaSeleccionada)) {
            this.actualizacion.
                getActualizacionEventos().remove(this.actaSeleccionada);
        }
        this.getActualizacionService().
            eliminarActualizacionEvento(this.actaSeleccionada);
    }

    /*
     *
     */
    public void eliminarDotacionSeleccionada() {
        this.dotaciones.remove(this.dotacionSeleccionada);
        if (this.actualizacion.getDotacions().
            contains(this.dotacionSeleccionada)) {
            this.actualizacion.getDotacions().remove(this.dotacionSeleccionada);
        }

        //TODO:franz.gamba :: eliminar la dotacion de la base de datos
    }

    /*
     *
     */
    public String finalizarRegistroActas() {

        this.getActualizacionService().
            guardarYActualizarActualizacion(this.actualizacion);

        this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion,
            ERol.DIRECTOR_TERRITORIAL,
            ProcesoDeActualizacion.ACT_PLANIFICACION_PLAN_REGISTRAR_AREA_TRABAJO,
            this.usuario.getDescripcionTerritorial());

        UtilidadesWeb.removerManagedBean("prepararRecursosHumanosyFisicos");
        TareasPendientesMB tp = (TareasPendientesMB) UtilidadesWeb.
            getManagedBean("tareasPendientes");
        tp.init();
        return "index";
    }

    /**
     * Metodo que guarda la sede de la comisión
     */
    public void guardarSedeComisionActualizacion() {
        if (this.departamentoComision != null &&
            this.municipioComision != null &&
            this.direccionComision != null) {

            this.sedeComision = new ActualizacionSedeComision();
            this.sedeComision.setActualizacion(this.actualizacion);
            this.sedeComision.setDireccion(this.direccionComision);
            this.sedeComision.setFechaLog(new Date());
            this.sedeComision.setUsuarioLog(this.usuario.getLogin());
            if (this.soporteSede != null) {
                //this.sedeComision.setSoporteDocumentoId(this.soporteSede);
            }
        } else {
            this.addMensajeError("Hubo un fallo en el registro de la sede de la comisión." +
                " Por favor ingrese todos los cambios obligatorios.");
        }

        //TODO franz.gamba :: persistir la sede de la comision en la Base de Datos
    }

    /**
     * Método que asigna el recurso humano a la actualizacion y al responsable de ella
     */
    public String asignarRecursoHumanoYResponsableAActualizacion() {

        // Asignacion y persistencia del responsable
        UsuarioDTO responsable = new UsuarioDTO();
        if (this.selectedResponsable != null) {

            responsable = this.getGeneralesService().getCacheUsuario(this.selectedResponsable);

            ActualizacionResponsable responsableActualizacion = new ActualizacionResponsable();
            responsableActualizacion.setActualizacion(this.actualizacion);
            responsableActualizacion.setFechaLog(new Date());
            responsableActualizacion.setUsuarioLog(this.usuario.getLogin());
            responsableActualizacion.setIdentificacion(responsable
                .getIdentificacion());
            responsableActualizacion.setNombre(responsable.getNombreCompleto());

            responsableActualizacion = this.getActualizacionService().
                guardarYactualizarActualizacionResponsable(responsableActualizacion);

            // Creacion del recurso humano a partir de funcionarios y
            // contratistas seleccionados
            this.recursosHumanosActualizacion = new ArrayList<RecursoHumano>();

            if (this.listaFuncionarios != null) {
                for (UsuarioDTO usr : this.listaFuncionarios) {
                    RecursoHumano resource = new RecursoHumano();
                    resource.setActualizacionResponsable(responsableActualizacion);
                    resource.setDocumentoIdentificacion(usr.getIdentificacion());
                    resource.setFechaLog(new Date());
                    resource.setUsuarioLog(this.usuario.getLogin());
                    resource.setNombre(usr.getNombreCompleto());
                    resource.setTipo(ETipoRecursoHumano.FUNCIONARIO.getCodigo());
                    // TODO franz.gamba :: asignar las actividades asi estos
                    // funcionarios sean
                    // funcionarios de planta
                    resource.setActividad("Funcionario IGAC");

                    this.recursosHumanosActualizacion.add(resource);
                }
            }

            if (this.listaContratistas != null) {
                for (ActualizacionContrato contract : this.listaContratistas) {
                    RecursoHumano resource = new RecursoHumano();
                    resource.setActualizacionResponsable(responsableActualizacion);
                    resource.setDocumentoIdentificacion(contract
                        .getContratistaIdentificacion());
                    resource.setFechaLog(new Date());
                    resource.setUsuarioLog(this.usuario.getLogin());
                    resource.setNombre(contract.getContratistaNombre());
                    resource.setTipo(ETipoRecursoHumano.CONTRATISTA.getCodigo());
                    resource.setActividad(contract.getActividad());

                    this.recursosHumanosActualizacion.add(resource);
                }
            }

            this.recursosHumanosActualizacion = this.getActualizacionService().
                guardarYactualizarRecursoHumanoList(this.recursosHumanosActualizacion);

            this.getActualizacionService().guardarYActualizarActualizacion(this.actualizacion);

            this.getActualizacionService().ordenarInduccionAlPersonalYRegistroAreaDeTrabajo(
                this.currentActivity.getId(), this.actualizacion,
                this.usuario.getDescripcionTerritorial(), responsable);

            UtilidadesWeb.removerManagedBean("prepararRecursosHumanosyFisicos");
            TareasPendientesMB tp = (TareasPendientesMB) UtilidadesWeb.getManagedBean(
                "tareasPendientes");
            tp.init();
            return "index";
        } else {
            this.addMensajeError(
                "No se seleccionó ningún responsable para el proceso de Actualización.");
            return null;
        }
    }

    //---------------------------------------------------------------------------------------------
    /**
     *
     * @return
     */
    public String aceptarSedeYDotaciones() {

        //TODO: franz.gamba:: implementar la transicion de planificacion al primer CU de alistamiento
        UtilidadesWeb.removerManagedBean("prepararRecursosHumanosyFisicos");
        TareasPendientesMB tp = (TareasPendientesMB) UtilidadesWeb.
            getManagedBean("tareasPendientes");
        tp.init();
        return "index";
    }

    public String cerrar() {
        UtilidadesWeb.removerManagedBean("prepararRecursosHumanosyFisicos");
        TareasPendientesMB tp = (TareasPendientesMB) UtilidadesWeb.
            getManagedBean("tareasPendientes");
        tp.init();
        return "index";
    }
}
