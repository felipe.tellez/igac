package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.impl.procesador.ProcesadorDocumentosImpl;
import co.gov.igac.sigc.documental.interfaces.IProcesadorDocumentos;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.entity.vistas.VEjecutorComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.ERolesEjecutorComisionDepuracion;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.EventoScheduleComisionesDTO;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @descrition Managed bean para la aprobación de comisiones correspondiente al caso de uso
 * CU-NP-CO-175.
 *
 * @author david.cifuentes
 *
 */
@Component("aprobacionComisiones")
@Scope("session")
public class AprobacionComisionesMB extends ProcessFlowManager {

    /**
     * Serial
     */
    private static final long serialVersionUID = 7813374358229744680L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AprobacionComisionesMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS------------- */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextService;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * managed bean para pintar los trámites de una comisión y permitir el retiro de éstos
     */
    @Autowired
    private TramitesDeComisionMB tramitesDeComisionMB;

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable que almacena los datos del usuario.
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena la comisión seleccionada.
     */
    private VComision comisionSeleccionada;

    /**
     * Variable que almacena las comisiones seleccionadas.
     */
    private VComision[] comisionesSeleccionadas;

    /**
     * Variable donde se cargan las comisiones sobre las que se trabaja.
     */
    private LazyDataModel<VComision> lazyComisiones;

    /**
     * Variable booleana para la activación del botón de aprobar comisión.
     */
    private boolean aprobarActivoBool;

    /**
     * Variable booleana para la activación del botón de aprobar comisión.
     */
    private boolean rechazarActivoBool;

    /**
     * Variable booleana para la activación del botón de aprobar comisión.
     */
    private boolean modificarActivoBool;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String manzanaCodigo;

    /**
     * Lista de usuarios con el rol de director territorial.
     */
    private List<UsuarioDTO> directoresTerritoriales;

    /**
     * Lista de usuarios con el rol de coordinador territorial.
     */
    private List<UsuarioDTO> coordinadoresTerritorial;

    /**
     * Lista de tramites asociados a la comisión.
     */
    private List<Tramite> tramitesAsociadosComision;

    /**
     * Documento que es el memorando que se guarda en alfresco. Se necesita como variable de clase
     * para poder ser asignado como tal al objeto ComisionEstado que se guarda.
     */
    private Documento documentoMemorandoComision;

    /**
     * Lista de ejecutores de la comisión seleccionada.
     */
    private List<VEjecutorComision> ejecutoresComision;

    /**
     * Listado de trámites asociados a un ejecutor para alguna comisión seleccionada.
     */
    private List<Tramite> tramitesEjecutorComision;

    /**
     * Ejecutor de la comisión, seleccionado para visualizar sus trámites.
     */
    private VEjecutorComision selectedEjecutorComision;

    /**
     * Variable que almacena la razón del echazo de la comisión.
     */
    private String razonRechazoComision;

    /**
     * Modelo para el schedule de comisiones
     */
    private DefaultScheduleModel comisionesScheduleModel;

    /**
     * Variable usada para instanciar un mensaje de estado al usuario en caso de que ocurra un fallo
     * en alguno de los métodos que aprueba, rechaza o avanza la comisión.
     */
    private String mensaje;

    /**
     * Variables booleanas para distinguir en el avance del proces si se quiere aprobar o rechazar
     * una comisión.
     */
    private boolean aprobarComisionBool;
    private boolean rechazarComisionBool;

    //-----------   banderas   ------------------
    /**
     * Bandera que indica si el documento ha sido radicado
     */
    private boolean banderaDocumentoRadicado;

    /**
     * indica si este MB va a manejar las comisiones del subproceso de depuración
     */
    private boolean isSubprocesoDepuracion;

    /**
     * para decidir si se muestra o no el panel con los componentes para modificar la comisión. No
     * se debe mostrar cuando se han retirado todos los ejecutores de la comisión y esta ha sido
     * cancelada
     */
    private boolean mostrarPanelModificacionComision;

    //-------------- Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteMemorandoComision;

    /**
     * variable que almacena el número de radicado del memorando de comisión
     */
    private String numeroRadicado;

    //*************** Banderas para avanzar el proceso **************************
    /**
     * variable que indica si se debe avanzar el proceso después de hacer un retiro el ejecutor de
     * asociado a una comisión
     */
    private boolean avanzarProcesoDesdeRetiroEjecutorComision;

    /**
     * variable que indica si se debe avanzar el proceso después de aprobar o rechazar una comisión.
     * Se usa en complemento con {@link #avanzarProcesoDesdeRetiroTramiteComision}
     */
    private boolean avanzarProcesoDesdeAprobarORechazarComision;

    /**
     * variable que indica si se debe avanzar el proceso después de retirar un trámite de una
     * comisión
     */
    private boolean avanzarProcesoDesdeRetiroTramiteComision;

    /**
     * variable que almacena el mensajeTemp que se muestra al usuario con el número de comisión y de
     * radicación de las comisiones aprobadas
     */
    private String mensajeComisionRadicadaSuccess;

    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    /**
     * Variable que tiene los trámites asociados a la variable comsionSeleccionada
     */
    private List<Tramite> tramitesDeLaComision;

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public boolean isMostrarPanelModificacionComision() {
        return this.mostrarPanelModificacionComision;
    }

    public void setMostrarPanelModificacionComision(boolean mostrarPanelModificacionComision) {
        this.mostrarPanelModificacionComision = mostrarPanelModificacionComision;
    }

    public boolean isIsSubprocesoDepuracion() {
        return this.isSubprocesoDepuracion;
    }

    public void setIsSubprocesoDepuracion(boolean isSubprocesoDepuracion) {
        this.isSubprocesoDepuracion = isSubprocesoDepuracion;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isAprobarActivoBool() {
        return this.aprobarActivoBool;
    }

    public void setAprobarActivoBool(boolean aprobarActivoBool) {
        this.aprobarActivoBool = aprobarActivoBool;
    }

    public boolean isRechazarActivoBool() {
        return this.rechazarActivoBool;
    }

    public void setRechazarActivoBool(boolean rechazarActivoBool) {
        this.rechazarActivoBool = rechazarActivoBool;
    }

    public String getApplicationClientName() {
        return this.applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return this.moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public boolean isAprobarComisionBool() {
        return this.aprobarComisionBool;
    }

    public void setAprobarComisionBool(boolean aprobarComisionBool) {
        this.aprobarComisionBool = aprobarComisionBool;
    }

    public boolean isRechazarComisionBool() {
        return this.rechazarComisionBool;
    }

    public void setRechazarComisionBool(boolean rechazarComisionBool) {
        this.rechazarComisionBool = rechazarComisionBool;
    }

    public String getMensaje() {
        return this.mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getModuleTools() {
        return this.moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public DefaultScheduleModel getComisionesScheduleModel() {
        return this.comisionesScheduleModel;
    }

    public void setComisionesScheduleModel(
        DefaultScheduleModel comisionesScheduleModel) {
        this.comisionesScheduleModel = comisionesScheduleModel;
    }

    public VComision[] getComisionesSeleccionadas() {
        return this.comisionesSeleccionadas;
    }

    public void setComisionesSeleccionadas(VComision[] comisionesSeleccionadas) {
        this.comisionesSeleccionadas = comisionesSeleccionadas;
    }

    public String getRazonRechazoComision() {
        return this.razonRechazoComision;
    }

    public void setRazonRechazoComision(String razonRechazoComision) {
        this.razonRechazoComision = razonRechazoComision;
    }

    public ReporteDTO getReporteMemorandoComision() {
        return this.reporteMemorandoComision;
    }

    public void setReporteMemorandoComision(ReporteDTO reporteMemorandoComision) {
        this.reporteMemorandoComision = reporteMemorandoComision;
    }

    public List<VEjecutorComision> getEjecutoresComision() {
        return this.ejecutoresComision;
    }

    public void setEjecutoresComision(List<VEjecutorComision> ejecutoresComision) {
        this.ejecutoresComision = ejecutoresComision;
    }

    public List<Tramite> getTramitesEjecutorComision() {
        return this.tramitesEjecutorComision;
    }

    public void setTramitesEjecutorComision(
        List<Tramite> tramitesEjecutorComision) {
        this.tramitesEjecutorComision = tramitesEjecutorComision;
    }

    public VEjecutorComision getSelectedEjecutorComision() {
        return this.selectedEjecutorComision;
    }

    public void setSelectedEjecutorComision(
        VEjecutorComision selectedEjecutorComision) {
        this.selectedEjecutorComision = selectedEjecutorComision;
    }

    public Documento getDocumentoMemorandoComision() {
        return this.documentoMemorandoComision;
    }

    public void setDocumentoMemorandoComision(
        Documento documentoMemorandoComision) {
        this.documentoMemorandoComision = documentoMemorandoComision;
    }

    public List<UsuarioDTO> getCoordinadoresTerritorial() {
        return this.coordinadoresTerritorial;
    }

    public void setCoordinadoresTerritorial(
        List<UsuarioDTO> coordinadoresTerritorial) {
        this.coordinadoresTerritorial = coordinadoresTerritorial;
    }

    public List<Tramite> getTramitesAsociadosComision() {
        return this.tramitesAsociadosComision;
    }

    public void setTramitesAsociadosComision(
        List<Tramite> tramitesAsociadosComision) {
        this.tramitesAsociadosComision = tramitesAsociadosComision;
    }

    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public boolean isModificarActivoBool() {
        return this.modificarActivoBool;
    }

    public void setModificarActivoBool(boolean modificarActivoBool) {
        this.modificarActivoBool = modificarActivoBool;
    }

    public List<UsuarioDTO> getDirectoresTerritoriales() {
        return this.directoresTerritoriales;
    }

    public void setDirectoresTerritoriales(
        List<UsuarioDTO> directoresTerritoriales) {
        this.directoresTerritoriales = directoresTerritoriales;
    }

    public VComision getComisionSeleccionada() {
        return this.comisionSeleccionada;
    }

    public void setComisionSeleccionada(VComision comisionSeleccionada) {
        this.comisionSeleccionada = comisionSeleccionada;
    }

    public LazyDataModel<VComision> getLazyComisiones() {
        return this.lazyComisiones;
    }

    public void setLazyComisiones(LazyDataModel<VComision> lazyComisiones) {
        this.lazyComisiones = lazyComisiones;
    }

    public Date getFechaActual() {
        return new Date();
    }

    public boolean isBanderaDocumentoRadicado() {
        return this.banderaDocumentoRadicado;
    }

    public void setBanderaDocumentoRadicado(boolean banderaDocumentoRadicado) {
        this.banderaDocumentoRadicado = banderaDocumentoRadicado;
    }

    public String getMensajeComisionRadicadaSuccess() {
        return this.mensajeComisionRadicadaSuccess;
    }

    public void setMensajeComisionRadicadaSuccess(String mensajeComisionRadicadaSuccess) {
        this.mensajeComisionRadicadaSuccess = mensajeComisionRadicadaSuccess;
    }
//--------------------------------------------------------------------------------------------------

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        if (this.tareasPendientesMB.getOpcionActual().contains(
            ProcesoDeConservacion.ACT_DEPURACION_APROBAR_COMISION)) {
            this.isSubprocesoDepuracion = true;
        }

        //D: se obtienen los ids de los trámites objetivo a partir del árbol de tareas         
        this.idsTramitesActividades =
            this.tareasPendientesMB.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        // Cargar la lista de las comisiones a trabajar.
        this.cargarListaComisiones();

        // Inicialización de variables para la activación de botones.
        this.aprobarActivoBool = false;
        this.rechazarActivoBool = false;
        this.modificarActivoBool = false;

        // Inicialización del visor
        this.applicationClientName = this.contextService.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();

        // Cargue de los directores territoriales.
        this.directoresTerritoriales = (List<UsuarioDTO>) this
            .getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionTerritorial(),
                ERol.DIRECTOR_TERRITORIAL);

        // Cargue de los cordinadores de la territorial.
        this.coordinadoresTerritorial = (List<UsuarioDTO>) this
            .getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionTerritorial(),
                ERol.COORDINADOR);

        //D la bandera inicia en false porque aún el memorando de comision no se ha radicado
        this.banderaDocumentoRadicado = false;

        //------- init de las cosas para el schedule de comisiones
        this.setComisionesScheduleModel(new DefaultScheduleModel());

        //D: llenar los eventos del schedule. Se deben mostrar todos los tipos de comisión.
        cargarEventosComisiones();
    }

//--------------------------------------------------------------------------------------------------
    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método que realiza el cargue de las comisiones a trabajar.
     */
    public void cargarListaComisiones() {
        LOGGER.debug("cargando comisiones...");

        this.lazyComisiones = new LazyDataModel<VComision>() {

            private static final long serialVersionUID = 1L;

            @Override
            public List<VComision> load(int first, int pageSize,
                String sortField, SortOrder sortOrder,
                Map<String, String> filters) {

                List<VComision> answer = new ArrayList<VComision>();
                poblarComisionesLazy(answer, first, pageSize, sortField,
                    sortOrder.toString(), filters);
                return answer;
            }
        };
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza la búsuqeda de comisiones buscadas por territorial.
     *
     * @author david.cifuentes
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     */
    public void poblarComisionesLazy(List<VComision> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<VComision> rows;
        int count, size;

        List<Long> idTramites = new ArrayList<Long>();

        for (int i = 0; i < this.idsTramitesActividades.length; i++) {

            idTramites.add(i, this.idsTramitesActividades[i]);

        }

        if (this.isSubprocesoDepuracion) {
            rows = this.getTramiteService().buscarComisionesParaAprobar(idTramites,
                EComisionTipo.DEPURACION.getCodigo(), sortField, sortOrder, filters, first,
                pageSize);
            count = this.getTramiteService().contarComisionesParaAprobar(idTramites,
                EComisionTipo.DEPURACION.getCodigo());
        } else {
            rows = this.getTramiteService().buscarComisionesParaAprobar(idTramites,
                EComisionTipo.CONSERVACION.getCodigo(), sortField, sortOrder, filters, first,
                pageSize);
            count = this.getTramiteService().contarComisionesParaAprobar(idTramites,
                EComisionTipo.CONSERVACION.getCodigo());
        }

        this.lazyComisiones.setRowCount(count);

        if (rows != null) {
            size = rows.size();
            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el cargue de datos para realizar alguna acción sobre la comisión
     * seleccionada.
     *
     * @author david.cifuentes
     */
    public void cargarDatosComision() {
        if (this.comisionSeleccionada != null &&
             this.comisionSeleccionada.getNumero() != null) {

            this.ejecutoresComision = this.getTramiteService()
                .buscarEjecutoresPorNumeroComision(
                    this.comisionSeleccionada.getNumero());

            this.mostrarPanelModificacionComision = true;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza verifica que botones de acción se deben activar en base a las comisiones
     * seleccionadas.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public void revisarActivacionBotones() {
        // Si queda unicamente una comisión seleccionada, activar el botón
        // modificar comisión
        if (this.comisionesSeleccionadas != null &&
             this.comisionesSeleccionadas.length > 0) {

            // Si se selecciona una única comisión se habilita el botón
            // modificar y cancelar.
            if (this.comisionesSeleccionadas.length == 1) {
                this.comisionSeleccionada = this.comisionesSeleccionadas[0];
                //this.comisionSeleccionadaAux = this.comisionesSeleccionadas[0];
                this.modificarActivoBool = true;
                this.rechazarActivoBool = true;
            } else {
                this.rechazarActivoBool = false;
                this.modificarActivoBool = false;
                this.comisionSeleccionada = null;
            }
            this.aprobarActivoBool = true;
        } else {
            this.manzanaCodigo = "";
            this.aprobarActivoBool = false;
            this.rechazarActivoBool = false;
            this.modificarActivoBool = false;
            this.comisionSeleccionada = null;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Action sobre el botón 'visor GIS', realiza el set de los números prediales a dibujar en el
     * visor.
     *
     * @author david.cifuentes
     *
     */
    public void cargarPrediosVisor() {

        String numerosPrediales = "";
        if (this.comisionesSeleccionadas != null) {

            List<Long> idsComisiones = new ArrayList<Long>();
            for (VComision vc : this.comisionesSeleccionadas) {
                // Se recuperan los id's de las comisiones para buscar los
                // trámites.

                idsComisiones.add(vc.getId());
            }

            if (!idsComisiones.isEmpty()) {

                // Búsqueda de los trámites asociados a la comisión, los cuales
                // avanzaran en el proceso.
                List<Tramite> tramitesComisionesSeleccionadas = this
                    .getTramiteService().buscarTramitesDeComisiones(
                        idsComisiones);

                // Búsqueda de los predios de los trámites.
                if (tramitesComisionesSeleccionadas != null &&
                     !tramitesComisionesSeleccionadas.isEmpty()) {
                    List<Long> idsTramites = new ArrayList<Long>();
                    for (Tramite t : tramitesComisionesSeleccionadas) {
                        // Se recuperan los id's de os trámites para encontrar
                        // sus predios asociadios.

                        idsTramites.add(t.getId());
                    }

                    if (!idsTramites.isEmpty()) {

                        List<Predio> prediosAsociadosALosTramites = this
                            .getConservacionService()
                            .buscarPrediosPorListaDeIdsTramite(idsTramites);

                        // Set de los predios a visualizar en el visor.
                        if (prediosAsociadosALosTramites != null &&
                             !prediosAsociadosALosTramites.isEmpty()) {

                            numerosPrediales = new String();
                            for (Predio p : prediosAsociadosALosTramites) {
                                numerosPrediales += p.getNumeroPredial() + ",";
                            }
                            this.manzanaCodigo = numerosPrediales.substring(0, numerosPrediales.
                                length() - 1);
                        }
                    }
                }
            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que aprueba las comisiones y avanza en el proceso éstas.
     *
     * @author david.cifuentes
     *
     */
    public void aprobarORechazarYAvanzarProceso() {

        this.mensaje = "";
        this.mensajeComisionRadicadaSuccess = "";
        List<String> rutasMemorandos = new ArrayList<String>();

        if (this.comisionesSeleccionadas != null &&
             this.comisionesSeleccionadas.length > 0) {

            for (VComision vc : this.comisionesSeleccionadas) {

                try {
                    // Set de la comisión a trabajar.
                    this.comisionSeleccionada = vc;

                    // 1. Radicar y guardar el memorando de comisión.
                    if (this.aprobarComisionBool) {
                        this.comisionSeleccionada = this.radicarMemorandoComision();
                        //Agrega la ruta del memorando de comisión generado para después unirlo
                        //en un sólo pdf, esto se hace cuando se aprueban comisiones masivamente
                        rutasMemorandos.add(this.reporteMemorandoComision.
                            getRutaCargarReporteAAlfresco());
                    }

                    // 2. Enviar correos electrónicos a los responsables.
                    if (this.comisionSeleccionada != null) {
                        if (this.aprobarComisionBool) {
                            this.ajustarParametrosAprobarYEnviarCorreo();
                        } else if (this.rechazarComisionBool) {
                            this.ajustarParametrosRechazarYEnviarCorreo();
                        }
                    }

                    // 3. Avanzar en el proceso las comisiones seleccionadas.
                    if (this.comisionSeleccionada != null) {
                        this.mensajeComisionRadicadaSuccess = this.mensajeComisionRadicadaSuccess +
                            "\n Número de comisión: " + this.comisionSeleccionada.getNumero() +
                             " :: Número de radicado: " + this.numeroRadicado + "\n";

                        //Avanzar los trámites después de aprobar o rechazar una comisión
                        this.avanzarProcesoDesdeAprobarORechazarComision = true;
                        this.avanzarProceso(true);
                    }

                } catch (Exception e) {
                    LOGGER.error("Error en aprobarORechazarYAvanzarProceso: " + e.getMessage());
                    e.printStackTrace();
                    this.addMensajeError(this.mensaje);
                }
            }

            //Generar reporte unido cuando se aprueba masivamente
            if (this.comisionesSeleccionadas.length > 1 && !rutasMemorandos.isEmpty()) {
                IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
                String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
                this.reporteMemorandoComision =
                    this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
            }

            // Reiniciar variables.
            this.comisionSeleccionada = null;
            this.comisionesSeleccionadas = null;
            this.mensaje = "";
            this.aprobarComisionBool = false;
            this.rechazarComisionBool = false;
            // Reinicia variables para la activación de botones.
            this.aprobarActivoBool = false;
            this.rechazarActivoBool = false;
            this.modificarActivoBool = false;

        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private void obtenerNumeroRadicado(UsuarioDTO ejecutorSeleccionado) {

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.MEMORANDO_DE_APROBACION_DE_COMISION.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Aprobacion comisiones"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(ejecutorSeleccionado.getCodigoTerritorial()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(ejecutorSeleccionado.getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(ejecutorSeleccionado.getDireccionEstructuraOrganizacional()); // Direccion destino
        parametros.add(ejecutorSeleccionado.getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(ejecutorSeleccionado.getIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        this.numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);
        //ultimo paso: llamar procedimiento recibirRadicado (Servicio de correspondencia para crear tramites)
        this.getTramiteService().recibirRadicado(this.numeroRadicado, this.usuario.getLogin());

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que descarga el documento guardado de la comisión y genera un nuevo reporte con el
     * número de radicado, actualizando en la comision la referencia del documento guardado en
     * Alfresco.
     *
     * @author david.cifuentes
     *
     * @return
     * @throws Exception
     */
    private VComision radicarMemorandoComision() throws Exception {

        List<String> rutasMemorandos = new ArrayList<String>();

        try {

            this.ejecutoresComision = this.getTramiteService()
                .buscarEjecutoresPorNumeroComision(
                    this.comisionSeleccionada.getNumero());

            UsuarioDTO ejecutorSeleccionado;

            for (VEjecutorComision ec : this.ejecutoresComision) {

                ejecutorSeleccionado = this.getGeneralesService().getCacheUsuario(ec.
                    getIdFuncionarioEjecutor());

                if (ejecutorSeleccionado != null) {

                    /** ------ OBTENER NÚMERO DE RADICADO ------ * */
                    this.obtenerNumeroRadicado(ejecutorSeleccionado);

                    if (this.numeroRadicado != null) {
                        LOGGER.debug("Número Radicado: " + this.numeroRadicado);
                        this.banderaDocumentoRadicado = true;

                        /** ------ GENERAR MEMORANDO ------ * */
                        rutasMemorandos.add(this.generarMemoComision(ejecutorSeleccionado));

                        /** ------ GUARDAR DOCUMENTO MEMORANDO COMISIÓN------ * */
                        this.guardarMemoComision();
                    }
                } else {
                    throw SNCWebServiceExceptions.EXCEPCION_0001.getExcepcion(LOGGER);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error al radicar el memorando de comisión: " + e.getMessage());
            this.mensaje = "Error al radicar el memorando de comisión.";
            throw (e);
        }

        if (!rutasMemorandos.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
            this.reporteMemorandoComision =
                this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

        return this.comisionSeleccionada;

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * genera el reporte que contiene el memorando de comisión. Hay dos opciones para generarlo,
     * dependiendo de sí el documento ya fue radicado, en el caso en que ya se radicó primero radica
     * el memorando y genera el reporte ya con el número de radicación, en el caso contrario genera
     * el reporte sin el número de radicado Este método se llama desde el método de radicación.
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public String generarMemoComision(UsuarioDTO ejecutorSeleccionado) {

        FirmaUsuario firma = null;
        String documentoFirma;

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.MEMORANDO_COMISION;
        if (isSubprocesoDepuracion) {
            enumeracionReporte = EReporteServiceSNC.MEMORANDO_COMISION_DEPURACION;
        }
        Map<String, String> parameters = new HashMap<String, String>();

        UsuarioDTO directorTerritorial = null;

        parameters.put("COMISION_ID", String.valueOf(this.comisionSeleccionada.getId()));

        if (!isSubprocesoDepuracion) {
            parameters.put("FUNCIONARIO_EJECUTOR", ejecutorSeleccionado.getLogin());
        }

        if (this.directoresTerritoriales == null) {
            this.directoresTerritoriales =
                (List<UsuarioDTO>) this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionTerritorial(),
                    ERol.DIRECTOR_TERRITORIAL);
        }

        if (this.directoresTerritoriales != null && !this.directoresTerritoriales.isEmpty() &&
             this.directoresTerritoriales.get(0) != null) {
            directorTerritorial = this.directoresTerritoriales.get(0);
        }

        if (directorTerritorial != null && directorTerritorial.getNombreCompleto() != null) {

            parameters.put("NOMBRE_DIRECTOR_TERRITORIAL", directorTerritorial.getNombreCompleto());

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                this.usuario.getNombreCompleto());

            if (firma != null && directorTerritorial.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                parameters.put("FIRMA_USUARIO", documentoFirma);

            } else {
                this.addMensajeInfo("El tramite no tiene firma de usuario asociada.");
            }
        }

        if (this.numeroRadicado != null) {
            parameters.put("NUMERO_RADICADO", this.numeroRadicado);
        }

        this.reporteMemorandoComision = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        return this.reporteMemorandoComision.getRutaCargarReporteAAlfresco();

    }

    /**
     * Método que se ejecuta cuando se oprime el link de ver memorando
     *
     * @author javier.aponte
     */
    public void generarMemoComision() {

        List<String> rutasMemorandos = new ArrayList<String>();

        try {

            this.ejecutoresComision = this.getTramiteService()
                .buscarEjecutoresPorNumeroComision(
                    this.comisionSeleccionada.getNumero());

            UsuarioDTO ejecutorSeleccionado;

            for (VEjecutorComision ec : this.ejecutoresComision) {

                ejecutorSeleccionado = this.getGeneralesService().getCacheUsuario(ec.
                    getIdFuncionarioEjecutor());

                if (ejecutorSeleccionado != null) {

                    /** ------ GENERAR MEMORANDO ------ * */
                    rutasMemorandos.add(this.generarMemoComision(ejecutorSeleccionado));
                }

            }

        } catch (Exception e) {
            LOGGER.error("Error al generar el memorando de comisión: " + e.getMessage());
        }

        if (!rutasMemorandos.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
            this.reporteMemorandoComision =
                this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que crea un documento. Luego guarda el documento en Alfresco y en la base de datos
     * mediante el llamado al método guardar documento de la interfaz de IGenerales.
     *
     * @author javier.aponte
     */
    public void guardarMemoComision() {

        Documento documento = new Documento();

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.MEMORANDO_DE_APROBACION_DE_COMISION.getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.reporteMemorandoComision != null) {
                documento.setArchivo(this.reporteMemorandoComision.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documento.setNumeroRadicacion(this.numeroRadicado);
            //v1.1.7
            //modificación 11/04/2014 se establece fecha de radicación 
            documento.setFechaRadicacion(new Date());

            // Se sube el archivo a Alfresco y se actualiza el documento
            documento = this.getGeneralesService().guardarMemorandoConservacion(documento,
                this.numeroRadicado, this.usuario);

            if (documento != null) {
                this.documentoMemorandoComision = documento;
                if (this.comisionSeleccionada != null) {
                    this.comisionSeleccionada.setMemorandoDocumentoId(documento.getId());
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que instancia los parámetros para la aprobación de una comisión, y hace el el envio de
     * correo electrónico.
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 28-06-2013 se corrige la obtención de los ejecutores destinatarios de
     * correo 03-07-2013 se aplica la regla de envío de correos en comisiones de Depuración
     */
    private void ajustarParametrosAprobarYEnviarCorreo() {

        List<String> destinatariosList;

        // Se ajustan y setean los parámetros referentes a la aprobación y el
        // envio de correo electrónico.
        try {
            destinatariosList = obtenerCorreosEjecutoresDeComisionSeleccionada();

            //D: Para las comisiones de Depuración, si el encargado (o encargados) tiene rol 
            // "Topógrafo" solo se le envía correo a este(os). Como los roles de ejecutor y 
            // topógrafo son excluyentes en una comisión y los topógrafos solo existen en las comisiones
            // de Depuración, es suficiente validar esto:
            if (this.comisionSeleccionada.getRolFuncionarioEjecutor() != null &&
                !this.comisionSeleccionada.getRolFuncionarioEjecutor().contains(
                    ERolesEjecutorComisionDepuracion.TOPOGRAFO.getNombre())) {

                UsuarioDTO responsableConservacion = this.obtenerResponsableConservacion();
                // Responsable de conservación de la UOC.
                if (responsableConservacion != null &&
                    responsableConservacion.getEmail() != null &&
                    !responsableConservacion.getEmail().trim().isEmpty()) {
                    destinatariosList.add(responsableConservacion.getEmail());
                }

                // Coordinadores de conservación de la UOC.
                if (this.coordinadoresTerritorial != null &&
                     !this.coordinadoresTerritorial.isEmpty()) {
                    for (UsuarioDTO coordinador : this.coordinadoresTerritorial) {
                        if (coordinador != null && coordinador.getEmail() != null &&
                             !coordinador.getEmail().trim().isEmpty()) {
//                            destinatariosList.add(coordinador.getLogin().concat(ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC));
                            destinatariosList.add(coordinador.getEmail());
                        }
                    }
                }

            }

            String[] destinatarios = destinatariosList
                .toArray(new String[destinatariosList.size()]);

            // Remitente
            String remitente = this.usuario.getEmail();

            // Asunto y el contenido del correo electrónico.
            String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_APROBACION_DE_COMISON;

            // Plantilla de contenido
            String codigoPlantillaContenido = EPlantilla.MENSAJE_COMUNICACION_APROBACION_DE_COMISON
                .getCodigo();

            // Parámetros de la plantilla del asunto
            String[] parametrosPlantillaAsunto = new String[1];
            parametrosPlantillaAsunto[0] = this.comisionSeleccionada
                .getNumero();

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaContenido = new String[2];
            parametrosPlantillaContenido[0] = this.comisionSeleccionada
                .getNumero();
            parametrosPlantillaContenido[1] = this.usuario.getNombreCompleto();

            // Listado de archivos adjuntos
            String[] adjuntos = new String[]{this.reporteMemorandoComision
                .getRutaCargarReporteAAlfresco()};

            // Nombre de los archivos adjuntos
            String[] titulosAdjuntos = new String[]{
                ConstantesComunicacionesCorreoElectronico.ARCHIVO_ADJUNTO_APROBACION_COMISION};

            // Cargar parámetros en el managed bean del componente.
            VisualizacionCorreoActualizacionMB visualCorreoMB =
                (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                    .getManagedBean("visualizacionCorreoActualizacion");
            visualCorreoMB.inicializarParametros(asunto, destinatarios,
                remitente, adjuntos, codigoPlantillaContenido,
                parametrosPlantillaAsunto,
                parametrosPlantillaContenido, titulosAdjuntos);

            // Envio de correo electrónico
            visualCorreoMB.enviarCorreo();

        } catch (Exception e) {
            LOGGER.error("Error al enviar el correo de la aprobación de la comisión " +
                 this.comisionSeleccionada.getNumero() + ".");
            LOGGER.debug(e.getMessage());
            this.mensaje = "Error al enviar el correo de la aprobación de la comisión " +
                 this.comisionSeleccionada.getNumero() + ".";
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que instancia los parámetros para el rechazo de una comisión y el envio de correo
     * electrónico.
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 28-06-2013 se corrige la obtención de los ejecutores destinatarios de
     * correo 03-07-2013 se aplica la regla de envío de correos en comisiones de Depuración
     */
    private void ajustarParametrosRechazarYEnviarCorreo() {

        List<String> destinatariosList;

        // Se ajustan y setean los parámetros referentes a la rechazo y el
        // envio de correo electrónico.
        try {

            //D: Para las comisiones de Depuración, si el encargado (o encargados) tiene rol 
            // "Topógrafo" solo se le envía correo a este(os), y no a los que tienen rol "Ejecutor". 
            if (this.isSubprocesoDepuracion) {
                //D: Como los roles de ejecutor y  topógrafo son excluyentes en una comisión, 
                // es suficiente validar esto:                
                if (this.comisionSeleccionada.getRolFuncionarioEjecutor().contains(
                    ERolesEjecutorComisionDepuracion.TOPOGRAFO.getNombre())) {

                    destinatariosList = this.obtenerCorreosEjecutoresDeComisionSeleccionada();
                } else {
                    destinatariosList = new ArrayList<String>();
                }
            } else {
                destinatariosList = new ArrayList<String>();
            }

            //D: se envía correo al responsable de conservación si la comisión es de Conservación o
            // si es de Depuración y el encargado(s) tiene rol "Ejecutor" (recuerde que los roles no se
            // topógrafo y ejecutor son excluyentes) 
            if (!this.isSubprocesoDepuracion ||
                (this.isSubprocesoDepuracion && !this.comisionSeleccionada.
                    getRolFuncionarioEjecutor().contains(
                        ERolesEjecutorComisionDepuracion.TOPOGRAFO.getNombre()))) {

                UsuarioDTO responsableConservacion = this.obtenerResponsableConservacion();

                // Responsable de conservación de la UOC.
                if (responsableConservacion != null &&
                     responsableConservacion.getEmail() != null &&
                     !responsableConservacion.getEmail().trim()
                        .isEmpty()) {
                    destinatariosList
                        .add(responsableConservacion.getEmail());
//                                    .getLogin().concat(ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC));
                }
            }

            String[] destinatarios = destinatariosList
                .toArray(new String[destinatariosList.size()]);

            // Remitente
            String remitente = this.usuario.getEmail();

            // Asunto y el contenido del correo electrónico.
            String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_RECHAZO_DE_COMISON;

            // Plantilla de contenido
            String codigoPlantillaContenido = EPlantilla.MENSAJE_COMUNICACION_RECHAZO_DE_COMISON
                .getCodigo();

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaAsunto = new String[1];
            parametrosPlantillaAsunto[0] = this.comisionSeleccionada
                .getNumero();

            // Parámetros de la plantilla de contenido
            String[] parametrosPlantillaContenido = new String[4];
            parametrosPlantillaContenido[0] = this.comisionSeleccionada.getNumero();

            //Relacionar los trámites asociados a la comisión          
            StringBuilder tablaTramites = new StringBuilder();

            // Búsqueda de los trámites asociados a la comisión, para relacionarlos
            // en el correo del rechazo de la comisión
            if (this.tramitesDeLaComision == null || this.tramitesDeLaComision.isEmpty()) {
                this.tramitesDeLaComision = this.getTramiteService().buscarTramitesDeComision(
                    this.comisionSeleccionada.getId(), this.comisionSeleccionada.getTipoComision());
            }

            if (this.tramitesDeLaComision != null) {

                for (Tramite currentTramite : this.tramitesDeLaComision) {
                    tablaTramites.append("<tr>");
                    tablaTramites.append("<td>").append(currentTramite.getNumeroRadicacion()).
                        append("</td>");
                    tablaTramites.append("<td>").append(currentTramite.getFechaRadicacion()).append(
                        "</td>");
                    tablaTramites.append("<td>").append(currentTramite.
                        getTipoTramiteCadenaCompleto()).append("</td>");
                    tablaTramites.append("<td>").
                        append(currentTramite.getDepartamento().getNombre()).append("</td>");
                    tablaTramites.append("<td>").append(currentTramite.getMunicipio().getNombre()).
                        append("</td>");
                    if (currentTramite.getPredio() != null) {
                        tablaTramites.append("<td>").append(currentTramite.getPredio().
                            getNumeroPredial()).append("</td>");
                    } else {
                        tablaTramites.append("<td></td>");
                    }
                    tablaTramites.append("<td>").append(currentTramite.
                        getNombreFuncionarioEjecutor()).append("</td>");

                    tablaTramites.append("</tr>");
                }
            }
            parametrosPlantillaContenido[1] = tablaTramites.toString();
            parametrosPlantillaContenido[2] = this.razonRechazoComision;
            parametrosPlantillaContenido[3] = this.usuario.getNombreCompleto();

            // Cargar parámetros en el managed bean del componente.
            VisualizacionCorreoActualizacionMB visualCorreoMB =
                (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                    .getManagedBean("visualizacionCorreoActualizacion");
            visualCorreoMB.inicializarParametros(asunto, destinatarios,
                remitente, null, codigoPlantillaContenido,
                parametrosPlantillaAsunto, parametrosPlantillaContenido, null);

            // Envio de correo electrónico
            visualCorreoMB.enviarCorreo();
        } catch (Exception e) {
            LOGGER.error("Error al enviar el correo del rechazo de la comisión " +
                 this.comisionSeleccionada.getNumero() + ".");
            this.mensaje = "Error al enviar el correo del rechazo de la comisión " +
                 this.comisionSeleccionada.getNumero() + ".";
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de obtener el responsable de conservación o director territorial asociados
     * al trámite según corresponda
     *
     * @author javier.aponte
     * @return UsuarioDTO
     */
    private UsuarioDTO obtenerResponsableConservacion() {

        UsuarioDTO responsableConservacion = null;

        // Búsqueda de los trámites asociados a la comisión, en caso que no existan aún
//REVIEW :: es necesario hacer esto aquí????        
        this.tramitesDeLaComision = this.getTramiteService().buscarTramitesDeComision(
            this.comisionSeleccionada.getId(), this.comisionSeleccionada.getTipoComision());

        if (this.tramitesDeLaComision != null && !this.tramitesDeLaComision.isEmpty()) {

            responsableConservacion = this.getGeneralesService().
                obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(
                    this.tramitesDeLaComision.get(0), this.usuario);

        }
        return responsableConservacion;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que avanza el proceso de la comisión enviada como parámetro.
     *
     * @author david.cifuentes
     * @throws Exception
     *
     * @param mustDoDBUpdate indica si se debe llamar al método que hace las actualizaciones
     * necesarias en bd. No deben hacerse esas actualizaciones si ya se han hecho antes en el mismo
     * flujo donde se llama a este método
     */
    /*
     * @modified pedro.garcia 15-07-2013 se adiciona parámetro al método para que la actualización
     * de base de datos se haga opcional, ya que en algunos sitios donde se usa este método ya se ha
     * hecho como parte de ese método (aunque debería haberse centralizado)
     */
    public void avanzarProceso(boolean mustDoDBUpdate) throws Exception {
        try {
            if (this.validateProcess()) {

                this.setupProcessMessage();
                super.forwardProcess();
                if (mustDoDBUpdate) {
                    this.doDatabaseStatesUpdate();
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error avanzando el proceso: ", e, e.getMessage());
            this.mensaje = "Error al avanzar el proceso para la comisión No." +
                 this.comisionSeleccionada.getNumero() + ".";
            throw e;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para realizar validaciones previas al avance del proceso.
     *
     * @author david.cifuentes
     *
     * @return
     */
    @Implement
    @Override
    public boolean validateProcess() {
        // Hasta éste punto ya se han realizado las validaciones necesarias
        // previamente, por lo que no se hace necesaria ninguna adicional.
        return true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para instanciar el objeto de negocio y las transiciones a las cual se moverá el
     * proceso para el trámite de la comisión seleccionada.
     *
     * @author david.cifuentes
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        Actividad activity;
        ActivityMessageDTO messageDTO, messageDTODefault;
        String observaciones = "", processTransition = null;
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;
        List<ActivityMessageDTO> processMessages;
        UsuarioDTO usuarioDestinoActividad;
        String estructuraOrg;

        /**
         * @note Las observaciones que se dan en el template individualProcessCUd se setean en el
         * atributo 'message' del ProcessFlowManager. En el caso de actividades masivas este
         * mensajeTemp no se usa, pero hay que sacar el texto de observaciones de alli para
         * colocarlo en cada mensajeTemp de la lista.
         */
        if (this.getMessage() != null) {
            messageDTODefault = this.getMessage();
            observaciones = messageDTODefault.getComment();
        }

        processMessages = new ArrayList<ActivityMessageDTO>();
        //Si se avanza el proceso desde aprobar o rechazar comisión entra por acá
        if (this.avanzarProcesoDesdeAprobarORechazarComision) {

            // Búsqueda de los trámites asociados a la comisión, los cuales
            // avanzaran en el proceso.
            this.tramitesAsociadosComision = this.getTramiteService().buscarTramitesDeComision(
                this.comisionSeleccionada.getId(), this.comisionSeleccionada.getTipoComision());

            if (this.tramitesAsociadosComision != null) {

                for (Tramite currentTramite : this.tramitesAsociadosComision) {
                    messageDTO = new ActivityMessageDTO();
                    messageDTO.setUsuarioActual(this.usuario);
                    solicitudCatastral = new SolicitudCatastral();
                    usuariosActividad = new ArrayList<UsuarioDTO>();

                    activity = this.tareasPendientesMB
                        .buscarActividadPorIdTramite(currentTramite.getId());
                    messageDTO.setComment(observaciones);
                    messageDTO.setActivityId(activity.getId());

                    if (this.aprobarComisionBool) {
                        if (this.isSubprocesoDepuracion) {
                            //Se inicializan los datos de la actividad si se trata del proceso de depuración y dependiendo del rol
                            TramiteDepuracion td = this.getTramiteService().
                                buscarUltimoTramiteDepuracionPorIdTramite(currentTramite.getId());

                            if (td.getEjecutor() != null && !td.getEjecutor().equals("")) {
                                usuarioDestinoActividad = this.getGeneralesService().
                                    getCacheUsuario(td.getEjecutor());
                                processTransition =
                                    ProcesoDeConservacion.ACT_DEPURACION_REVISAR_PREPARAR_INFO;
                            } else {
                                usuarioDestinoActividad = this.getGeneralesService().
                                    getCacheUsuario(td.getTopografo());
                                processTransition =
                                    ProcesoDeConservacion.ACT_DEPURACION_EXP_AREA_IDENTIFICAR_PUNTOS_TOPOGRAFICOS;
                            }
                            usuariosActividad.add(usuarioDestinoActividad);
                            solicitudCatastral.setUsuarios(usuariosActividad);
                        } //D: este tipo de trámites siempre están en subproceso de ejecución
                        else {
                            if (currentTramite.getTipoTramite().equals(
                                ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo()) ||
                                 (currentTramite.getClaseMutacion() != null &&
                                currentTramite.getClaseMutacion().equals(EMutacionClase.CUARTA.
                                    getCodigo())) ||
                                 currentTramite.isViaGubernativa()) {

                                processTransition =
                                    ProcesoDeConservacion.ACT_EJECUCION_ALISTAR_INFORMACION;
                            } //D: este tipo de trámites siempre están en subproceso de asignación
                            else {
                                processTransition =
                                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;
                            }
                            usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                                currentTramite.getFuncionarioEjecutor());
                            usuariosActividad.add(usuarioDestinoActividad);
                            solicitudCatastral.setUsuarios(usuariosActividad);
                        }

                    }
                    if (this.rechazarComisionBool) {

                        if (this.isSubprocesoDepuracion) {
                            //Se inicializan los datos de la actividad si se trata del proceso de depuración y dependiendo del rol
                            TramiteDepuracion td = this.getTramiteService().
                                buscarUltimoTramiteDepuracionPorIdTramite(currentTramite.getId());

                            if (td.getEjecutor() != null && !td.getEjecutor().equals("")) {
                                processTransition =
                                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION_EJECUTOR;
                                usuariosActividad = this.getTramiteService().
                                    buscarFuncionariosPorRolYTerritorial(this.usuario.
                                        getDescripcionTerritorial(), ERol.RESPONSABLE_CONSERVACION);
                            } else {
                                processTransition =
                                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION;
                                usuarioDestinoActividad = this.getGeneralesService().
                                    getCacheUsuario(td.getTopografo());
                                usuariosActividad.add(usuarioDestinoActividad);
                            }

                            solicitudCatastral.setUsuarios(usuariosActividad);
                        } else {
                            if (currentTramite.getTipoTramite().equals(
                                ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo()) ||
                                 (currentTramite.getClaseMutacion() != null &&
                                 currentTramite.getClaseMutacion().equals(
                                    EMutacionClase.CUARTA.getCodigo()))) {
                                processTransition = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
                            } else {
                                processTransition = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                            }

                            if (currentTramite.isQuinta()) {
                                estructuraOrg = this.getGeneralesService()
                                    .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                        currentTramite.getMunicipio().getCodigo());
                            } else {
                                estructuraOrg = this.getGeneralesService()
                                    .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                        currentTramite.getPredio().getMunicipio().getCodigo());
                            }
                            usuariosActividad = this.getTramiteService().
                                buscarFuncionariosPorRolYTerritorial(
                                    estructuraOrg, ERol.RESPONSABLE_CONSERVACION);
                            solicitudCatastral.setUsuarios(usuariosActividad);
                        }
                    }

                    messageDTO.setTransition(processTransition);
                    solicitudCatastral.setTransicion(processTransition);

                    messageDTO.setSolicitudCatastral(solicitudCatastral);
                    processMessages.add(messageDTO);

                }

            }
            this.avanzarProcesoDesdeAprobarORechazarComision = false;
        } //Si se retiro el ejecutor de la comisión los trámites se envian 
        //a comisionar trámites de terreno
        else if (this.avanzarProcesoDesdeRetiroEjecutorComision) {

            if (this.tramitesEjecutorComision != null) {

                for (Tramite currentTramite : this.tramitesEjecutorComision) {
                    processMessages.add(this.crearProcessMessageParaComisionarTramitesTerreno(
                        currentTramite));
                }
            }
            this.avanzarProcesoDesdeRetiroEjecutorComision = false;
        } //Si se retiro un trámite de una comisión el trámite se envía a comisionar trámites de terreno
        else if (this.avanzarProcesoDesdeRetiroTramiteComision) {

            Tramite[] tramitesTemporales = this.tramitesDeComisionMB.getSelectedTramitesComision();

            if (tramitesTemporales != null) {

                for (Tramite currentTramite : tramitesTemporales) {
                    processMessages.add(this.crearProcessMessageParaComisionarTramitesTerreno(
                        currentTramite));
                }
            }

            this.avanzarProcesoDesdeRetiroTramiteComision = false;
        }

        if (!processMessages.isEmpty()) {
            this.setMasiveMessages(processMessages);
        } else {
            LOGGER.debug("Error al mover el proceso");
            this.addMensajeError("Error al avanzar el proceso, no se avanzará ninguna actividad.");
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método que crea un objeto ActivityMessageDTO a partir de la información de un trámite para
     * avanzarlo a la actividad comisionar trámites de terreno, si el tipo de trámite es una
     * mutación de cuarta o es una revisión de avalúo lo envía al proceso de ejecución, en caso
     * contrario lo envía al proceso de asignación
     *
     * @author javier.aponte
     */
    /*
     * @modified pedro.garcia 20-12-2013 se diferencia la transición que se usa cuando se trata del
     * subproceso de depuración
     */
    private ActivityMessageDTO crearProcessMessageParaComisionarTramitesTerreno(
        Tramite currentTramite) {

        Actividad activity;
        ActivityMessageDTO messageDTO, messageDTODefault;
        String observaciones = "", processTransition = null;
        SolicitudCatastral solicitudCatastral;
        String estructuraOrg;

        /*
         * @note Las observaciones que se dan en el template individualProcessCUd se setean en el
         * atributo 'message' del ProcessFlowManager. En el caso de actividades masivas este
         * mensajeTemp no se usa, pero hay que sacar el texto de observaciones de alli para
         * colocarlo en cada mensaje de la lista.
         */
        if (this.getMessage() != null) {
            messageDTODefault = this.getMessage();
            observaciones = messageDTODefault.getComment();
        }

        messageDTO = new ActivityMessageDTO();
        messageDTO.setUsuarioActual(this.usuario);
        solicitudCatastral = new SolicitudCatastral();

        activity = this.tareasPendientesMB.buscarActividadPorIdTramite(currentTramite.getId());
        messageDTO.setComment(observaciones);
        messageDTO.setActivityId(activity.getId());

        //D: cuando el trámite es de depuración, hay que verificar el rol del usuario que lo ejecuta 
        //  para determinar la transición
        if (this.isSubprocesoDepuracion) {
            TramiteDepuracion tramDep;

            tramDep = this.getTramiteService().
                buscarUltimoTramiteDepuracionPorIdTramite(currentTramite.getId());

            if (tramDep != null) {
                //D: si este campo es diferente de nulo, y como los roles (topógrafo, ejecutor y digitalizador)
                //  son excluyentes en un trámite depuración, se asume que este es el rol del usuario
                if (tramDep.getTopografo() != null) {
                    processTransition = ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION;
                } //D: por la misma razón de que son excluyentes, y dado que el trámite es de terreno (no hay opción de que sea digitalizador),
                // se supone que si no es topógrafo es ejecutor
                else {
                    processTransition =
                        ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION_EJECUTOR;
                }
            } else {
                LOGGER.error(
                    "No se pudo obtener el último registro TramiteDepuracion del trámite " +
                     currentTramite.getId().toString());
            }

        } else {

            if (currentTramite.getTipoTramite().equals(
                ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo()) ||
                 (currentTramite.getClaseMutacion() != null &&
                currentTramite.getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo()))) {
                processTransition = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
            } else {
                processTransition = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
            }
        }

        if (currentTramite.isQuinta()) {
            estructuraOrg = this.getGeneralesService()
                .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    currentTramite.getMunicipio().getCodigo());
        } else {
            estructuraOrg = this.getGeneralesService()
                .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    currentTramite.getPredio().getMunicipio().getCodigo());
        }
        solicitudCatastral.setUsuarios(this.getTramiteService().
            buscarFuncionariosPorRolYTerritorial(estructuraOrg, ERol.RESPONSABLE_CONSERVACION));

        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);

        messageDTO.setSolicitudCatastral(solicitudCatastral);

        return messageDTO;

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que arma un objeto ComisionEstado con los datos necesarios.
     *
     * @note Copia del método de ManejoComisionesTramiteMB de pedro.garcia.
     * @return
     */
    private ComisionEstado armarNuevaComisionEstado(VComision comision, String motivo) {

        ComisionEstado answer = null;
        Date currentDate;
        Comision comisionTemp;

        currentDate = new Date(System.currentTimeMillis());
        answer = new ComisionEstado();
        answer.setFecha(currentDate);
        answer.setFechaInicio(comision.getFechaInicio());
        answer.setFechaFin(comision.getFechaFin());
        answer.setEstado(comision.getEstado());
        answer.setFechaLog(currentDate);
        answer.setMotivo(motivo);
        answer.setMemorandoDocumento(this.documentoMemorandoComision);
        answer.setUsuarioLog(this.usuario.getLogin());

        comisionTemp = new Comision();
        comisionTemp.setId(comision.getId());
        answer.setComision(comisionTemp);

        return answer;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza las actualizaciones en la base de datos al avanzar el proceso,
     * actualizando el estado de la comisión, sus trámites asociados y creando un registro del
     * cambio de estado de la misma.
     *
     * @author david.cifuentes
     *
     * @return
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        ComisionEstado nuevaComisionEstado = null;

        if (this.comisionSeleccionada != null) {
            try {
                if (this.aprobarComisionBool) {
                    // Se actualiza el estado de la comisión.
                    this.comisionSeleccionada
                        .setEstado(EComisionEstado.POR_EJECUTAR.getCodigo());
                }
                if (this.rechazarComisionBool) {
                    // Se actualiza el estado de la comisión.
                    this.comisionSeleccionada
                        .setEstado(EComisionEstado.CANCELADA.getCodigo());
                }

                this.getTramiteService().actualizarComision(this.usuario,
                    this.comisionSeleccionada);

                if (this.aprobarComisionBool) {
                    // Se crea guarda un nuevo estado de comisión                
                    nuevaComisionEstado = this
                        .armarNuevaComisionEstado(this.comisionSeleccionada,
                            EComisionEstadoMotivo.COMISION_APROBADA.getCodigo());
                }
                if (this.rechazarComisionBool) {
                    // Se crea guarda un nuevo estado de comisión                                     
                    nuevaComisionEstado = this
                        .armarNuevaComisionEstado(this.comisionSeleccionada,
                            EComisionEstadoMotivo.COMISION_RECHAZADA.getCodigo());
                }

                if (nuevaComisionEstado != null) {
                    this.getTramiteService().insertarComisionEstado(
                        nuevaComisionEstado, this.usuario);
                }

            } catch (Exception ex) {
                LOGGER.error("Error al actualizar los estados de la comisión." +
                     ex.getMessage());
                return;
            }
        }

        //D: en caso de rechazo de la comisión, se actualizan los trámites de la comisión.
        if (this.rechazarComisionBool && this.tramitesAsociadosComision != null) {

            //D: los trámites pasan a no estar comisionados
            for (Tramite currentTramite : this.tramitesAsociadosComision) {
                currentTramite.setComisionado(ESiNo.NO.toString());
            }
            try {
                this.getTramiteService().actualizarTramites(this.tramitesAsociadosComision);
            } catch (Exception ex) {
                LOGGER.error("Error actualizando los trámites de la comisión rechazada");
                return;
            }

            //D: se debe borrar la asociación de los trámites a la comisión, es decir, se deben 
            //  borrar los datos de la tabla comision_tramite                   
            List<Long> idTramites = new ArrayList<Long>();
            for (Tramite t : this.tramitesAsociadosComision) {
                if (t.getId() != null) {
                    idTramites.add(t.getId());
                }
            }

            try {
                this.getTramiteService().eliminarTramiteComisionPorTramitesYComision(
                    idTramites, this.comisionSeleccionada.getId());
            } catch (Exception ex) {
                LOGGER.error("Error borrando las asociaciones entre comisión y trámites");
            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el ajuste de tiempos de una comisión seleccionada. Copia del método de
     * manejoComisionesTramite con el mismo nombre.
     *
     * @author david.cifuentes
     */
    public void ajustarTiemposComision() {

        Double tiempoAjusteComision, duracionTotal;
        boolean ajusteOK = true;

        tiempoAjusteComision = this.comisionSeleccionada.getDuracionAjuste();

        if ((tiempoAjusteComision.floatValue() % 0.5) != 0) {
            this.addMensajeError("El valor del ajuste de la duración de la comisión debe ser un " +
                 "número positivo múltiplo de 0.5!");
            ajusteOK = false;
        } else {

            duracionTotal = this.comisionSeleccionada.getDuracion() +
                 this.comisionSeleccionada.getDuracionAjuste();
            duracionTotal = Utilidades.roundToDecimals(duracionTotal, 2);
            this.comisionSeleccionada.setDuracionTotal(duracionTotal);
            this.comisionSeleccionada.setDuracionAjuste(tiempoAjusteComision);
            this.addMensajeInfo("Ajuste de tiempo exitoso!. Cierre esta ventana para continuar");
        }

        if (!ajusteOK) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error",
                "pailander en los tiempos de ajuste");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el guardado de la comisión.
     *
     * @author david.cifuentes
     *
     */
    public void modificarComision() {
        try {
            this.getTramiteService().actualizarComision(this.usuario,
                this.comisionSeleccionada);
            this.addMensajeInfo("Comisión modificada satisfactoriamente.");
            this.comisionSeleccionada = null;
            this.comisionesSeleccionadas[0] = null;
        } catch (Exception ex) {
            LOGGER.error("Excepción actualizando la comisión: " +
                 ex.getMessage());
            this.addMensajeError("Error al actualizar la comisión.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que retira al ejecutor de la comisión seleccionada.
     *
     * @note Método copiado de manejoComisionesTramiteMB y ajustado para el caso de uso aprobar
     * comisiones.
     *
     * @author david.cifuentes
     */
    /*
     * @modified pedro.garcia 28-06-2013 arreglo de validación para cancelar comisión
     */
    public void retirarEjecutor() {

        Double nuevaDuracion;
        boolean validateTiempos, cancelarComision;
        String mensajeTemp;

        cancelarComision = false;
        //D: si estoy retirando un ejecutor y la comisión solo tenía uno, entonces se cancela
        if (this.ejecutoresComision.size() <= 1) {
            this.addMensajeInfo(
                "* Se retiró el último ejecutor de la comisión!. Esta queda en estado " +
                EComisionEstado.CANCELADA.toString());
            cancelarComision = true;
            this.mostrarPanelModificacionComision = false;
        }

        this.tramitesEjecutorComision = this.getTramiteService().buscarTramitesDeEjecutorEnComision(
            this.selectedEjecutorComision.getIdFuncionarioEjecutor(), this.comisionSeleccionada.
            getId());

        if (this.tramitesEjecutorComision != null) {
            List<Long> idTramites = new ArrayList<Long>();
            for (Tramite t : this.tramitesEjecutorComision) {
                if (t.getId() != null) {
                    idTramites.add(t.getId());
                }
            }
            // Se eliminan las relaciones de la comisión con el trámite.
            boolean elimTC = this.getTramiteService().eliminarTramiteComisionPorTramitesYComision(
                idTramites, this.comisionSeleccionada.getId());
            if (!elimTC) {
                mensajeTemp = "Hubo un error eliminando las relaciones de la comisión con los " +
                    "trámites del ejecutor";
                LOGGER.error(mensajeTemp);
                this.addMensajeWarn(mensajeTemp);
            }
        } else {
            this.addMensajeError(
                "Ocurrió un error al buscar los trámites del ejecutor seleccionado.");
            return;
        }

        // Se marcan esos trámites como no comisionados.
        for (Tramite t : this.tramitesEjecutorComision) {
            t.setComisionTramites(null);
            t.setComisionado(ESiNo.NO.getCodigo());
        }

        // Actualización de los trámites.
        boolean tramitesActualizados =
            this.getTramiteService().actualizarTramites(this.tramitesEjecutorComision);

        if (!tramitesActualizados) {
            LOGGER.error("Error actualizando los trámites retirados");
        }

        //D: recalcular duración comisión. Solo es necesario hacerlo si la comisión no se va a cancelar.
        // Tampoco debe hacerse para las comisiones de Depuración porque la duración no depende de
        // los trámites
        if (!cancelarComision && !this.isSubprocesoDepuracion) {
            nuevaDuracion = this.getTramiteService().obtenerDuracionComision2(
                this.comisionSeleccionada.getId());

            if (nuevaDuracion != null) {
                // Set de la nueva duración.
                this.comisionSeleccionada.setDuracion(nuevaDuracion);

                // Validación de los tiempos de la comisión.
                validateTiempos = UtilidadesWeb.validarReglaDuracionComision(
                    this.comisionSeleccionada.getDuracion(),
                    this.comisionSeleccionada.getDuracionTotal());
                if (!validateTiempos) {
                    this.addMensajeInfo(
                        "Existe diferencia entre el tiempo de la comisión y la duración de la misma.");
                }

            } else {
                this.addMensajeError("Ocurrió un error recalculando la duración de la comisión.");
                return;
            }
        }

        this.addMensajeInfo("Ejecutor retirado de la comisión. El cambio ya fue guardado.");

        //D: si es necesario, se cambia el estado de la comisión
        if (cancelarComision) {
            ComisionEstado nuevaComisionEstado = null;
            // Se actualiza el estado de la comisión.
            this.comisionSeleccionada.setEstado(EComisionEstado.CANCELADA.getCodigo());
            // Se crea guarda un nuevo estado de comisión
            nuevaComisionEstado = this
                .armarNuevaComisionEstado(this.comisionSeleccionada,
                    EComisionEstadoMotivo.RETIRO_TODOS_LOS_EJECUTORES.getCodigo());
            this.getTramiteService().insertarComisionEstado(
                nuevaComisionEstado, this.usuario);

        }

        try {
            this.getTramiteService().actualizarComision(this.usuario, this.comisionSeleccionada);
        } catch (Exception ex) {
            LOGGER.error("Error actualizando la comisión: " + ex.getMessage());
        }

        this.ejecutoresComision.remove(this.selectedEjecutorComision);
        this.selectedEjecutorComision = null;

        try {

            //Avanzar los trámites que tenía asignado el ejecutor que se retiró 
            //de la comisión a comisionar tramites de terreno
            this.avanzarProcesoDesdeRetiroEjecutorComision = true;

            this.avanzarProceso(false);

            this.addMensajeInfo("Ejecutor retirado de la comisión satisfactoriamente.");

        } catch (Exception ex) {
            LOGGER.error("Error moviendo las actividades de los trámites retirados de una comisión");
            LOGGER.error(ex.getMessage());
            this.addMensajeError("Ocurrió un error al eliminar el ejecutor de la comisión.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que consulta los tramites del ejecutor seleccionado.
     *
     * @author david.cifuentes
     *
     */
    public void consultarTramitesDelEjecutor() {
        try {
            this.tramitesEjecutorComision = this
                .getTramiteService()
                .buscarTramitesDeEjecutorEnComision(
                    this.selectedEjecutorComision
                        .getIdFuncionarioEjecutor(),
                    this.comisionSeleccionada.getId());
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error: consultarTramitesDelEjecutor");
            LOGGER.error(ex.getMessage());
            this.addMensajeError(
                "Ocurrio un error al buscar los trámites del ejecutor seleccionado.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Action sobre el botón 'cerrar', redirige el arbol de tareas removiendo el managed bean
     * actual.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String cerrarPaginaPrincipal() {

        UtilidadesWeb.removerManagedBean("aprobacionComisiones");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    /** -------------------------------- */
    /** ----------- EVENTOS ------------ */
    /** -------------------------------- */
    /**
     * Método que gestiona la activación de los botones de acción sobre las comisiones
     * seleccionadas.
     *
     * @author david.cifuentes
     * @param ev
     */
    public void gestionActivacionBotonesSelectListener(SelectEvent ev) {
        this.revisarActivacionBotones();
        this.cargarPrediosVisor();
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que gestiona la activación de botones al deseleccionar las comisiones.
     *
     * @author david.cifuentes
     * @param ev
     */
    public void gestionActivacionBotonesUnselectListener(UnselectEvent ev) {
        this.revisarActivacionBotones();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método invocado para retirar los trámites de una comisión.
     */
    public void retirarTramitesComision() {
        this.tramitesDeComisionMB.retirarTramitesComision();
        //Avanzar los trámites que se retiraron de la comisión
        //a la actividad comisionar tramites de terreno
        this.avanzarProcesoDesdeRetiroTramiteComision = true;
        try {
            this.avanzarProceso(false);
        } catch (Exception e) {
            this.addMensajeError("No se pudo avanzar el proceso de los trámites que se retiraron" +
                " de la comisión");
        }

        this.tramitesDeComisionMB.setSelectedTramitesComision(null);
        this.comisionSeleccionada = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * adiciona eventos al schedule de comisiones según la consulta de comisiones por el estado dado
     *
     * @param estadoComision
     */
    /*
     * @modified pedro.garcia se hacen todas las consultas a la capa de negocio allá
     */
    private void cargarEventosScheduleComisionesPorEstado(String tipoComision,
        String estadoComision, String estiloEvento) {

        DefaultScheduleEvent eventoComision;
        List<EventoScheduleComisionesDTO> eventos;

        eventos = this.getTramiteService().armarEventosScheduleComisiones(
            this.usuario.getCodigoEstructuraOrganizacional(), tipoComision, estadoComision);

        for (EventoScheduleComisionesDTO evento : eventos) {
            eventoComision = new DefaultScheduleEvent();
            eventoComision.setTitle(evento.getTitulo());
            eventoComision.setStartDate(evento.getFechaInicio());
            eventoComision.setEndDate(evento.getFechaFin());
            eventoComision.setAllDay(true);
            eventoComision.setStyleClass(estiloEvento);

            this.comisionesScheduleModel.addEvent(eventoComision);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * calcula la duración de las comisiones de Depuración. La regla que aplica es fecha final -
     * fecha inicial + 0.5 (en días hábiles)
     *
     * @author pedro.garcia
     */
    private double calcularDuracionComisionDepuracion() {

        int tempDias;
        double answer;
        Integer tempDiasI;

        tempDiasI = this.getGeneralesService().calcularDiasHabilesEntreDosFechas(
            this.comisionSeleccionada.getFechaInicio(), this.comisionSeleccionada.getFechaFin());
        tempDias = tempDiasI.intValue();

        answer = (double) tempDias + 0.5;

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma una lista con las direcciones de correo de los ejecutores relacionados con la comisión
     * actualmente seleccionada
     *
     * @autor pedro.garcia
     * @return
     */
    private ArrayList<String> obtenerCorreosEjecutoresDeComisionSeleccionada() {

        ArrayList<String> answer = new ArrayList<String>();
        String[] ejecutores;
        String emailIGAC;
        UsuarioDTO funcionarioEjecutor;

        if (this.comisionSeleccionada.getFuncionarioEjecutor() != null) {

            ejecutores = this.comisionSeleccionada.getFuncionarioEjecutor().
                split(Constantes.SEPARADOR_CAMPOS_CONCATENADOS_BD);
            for (String currentEjecutor : ejecutores) {
                funcionarioEjecutor = this.getGeneralesService().getCacheUsuario(currentEjecutor);
                emailIGAC = funcionarioEjecutor.getEmail();
                answer.add(emailIGAC);
            }
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Listener del cierre de la ventana de modificación de la comisión. Se usa para garantizar que
     * se debe seleccionar de nuevo una comisión para que no queden valores pegados de la comisión
     * seleccionada anteriormente, que han sido modificados en el flujo de esa ventana, pero que no
     * se guardaron en bd
     *
     * @author pedro.garcia
     */
    public void cerrarVentanaModificacionComisionListener() {
        this.comisionSeleccionada = null;
        this.comisionesSeleccionadas = null;
        this.aprobarActivoBool = false;
        this.rechazarActivoBool = false;
        this.modificarActivoBool = false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * llama al método que carga los eventos de la programción de comisiones con los diferentes
     * valores según los estados que se requiere mostrar.
     *
     * Actualmente debe mostrar las comisiones en los estados creada : cuando se crea pero no se ha
     * aprobado por_ejecutar : cuando se aprueba en_ejecucion y las comisiones de todos los tipos
     * (actualmente depuración y conservación)
     *
     * @author pedro.garcia
     */
    /* Es una copia de ManejoComisionesTramitesMB#cargarEventosComisiones. No se hace un método
     * común porque habría que inyectar un MB en una clase de utilidades
     */
    private void cargarEventosComisiones() {

        String estadoComision, estiloEventoSchedule, tipoComisionConservacion, tipoComisionDepuracion;

        tipoComisionConservacion = EComisionTipo.CONSERVACION.getCodigo();
        tipoComisionDepuracion = EComisionTipo.DEPURACION.getCodigo();

        //D: los estilos para los diferentes eventos están definidos en el css 
        estadoComision = EComisionEstado.CREADA.getCodigo();
        estiloEventoSchedule = "comisionesCreadas";
        cargarEventosScheduleComisionesPorEstado(tipoComisionConservacion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.POR_EJECUTAR.getCodigo();
        estiloEventoSchedule = "comisionesPorEjecutar";
        cargarEventosScheduleComisionesPorEstado(tipoComisionConservacion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.EN_EJECUCION.getCodigo();
        estiloEventoSchedule = "comisionesEnEjecucion";
        cargarEventosScheduleComisionesPorEstado(tipoComisionConservacion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.CREADA.getCodigo();
        estiloEventoSchedule = "comisionesCreadas";
        cargarEventosScheduleComisionesPorEstado(tipoComisionDepuracion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.POR_EJECUTAR.getCodigo();
        estiloEventoSchedule = "comisionesPorEjecutar";
        cargarEventosScheduleComisionesPorEstado(tipoComisionDepuracion, estadoComision,
            estiloEventoSchedule);

        estadoComision = EComisionEstado.EN_EJECUCION.getCodigo();
        estiloEventoSchedule = "comisionesEnEjecucion";
        cargarEventosScheduleComisionesPorEstado(tipoComisionDepuracion, estadoComision,
            estiloEventoSchedule);

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * listener del cambio de valor de fecha final de la comisión. Se debe recalcular la duración
     * total de la comisión que depende de la fecha final y la inicial. Para las comisiones de
     * Depuración la duración es igual a la duración total.
     *
     * @author pedro.garcia
     */
    public void cambioFechaFinalComisionListener(DateSelectEvent dsEvent) {

        //N: en este punto la fecha no ha sido definida en el value del componente (aquí significa
        //  que this.nuevaComision.getFechaFin() es null), por lo que  hay que tomarla del evento.
        // Esto se debe a que el listener se ejecuta primero que el setter de la propiedad que contiene
        // el valor del componente que se obliga a procesar (definido con el atributo 'process')
        LOGGER.debug("seleccionó fecha final: " + dsEvent.getDate());

        this.comisionSeleccionada.setFechaFin(dsEvent.getDate());

        if (this.isSubprocesoDepuracion) {
            this.calcularDuracionesComision(2);
        } else {
            this.calcularDuracionesComision(1);
        }

    }
//------------------

    /**
     * listener del cambio de valor de fecha inicial de la comisión. Se usa para las comisiones de
     * de Depuración.
     *
     * @author pedro.garcia
     */
    /*
     * En el momento en que se cambia la fecha final, cuando se ejecuta el listener de esa acción,
     * la fecha inicial puede estar en null, porque no se ha seleccionado, o porque no se ha
     * procesado (ya que el listener se ejecuta primero que el setter de la propiedad). Debido a
     * esto, se fuerza a que al cambiar seleccionar la fecha inicial se venga al MB para que al
     * momento de calcular la duración de la comisión de depuración, esta esté disponible.
     */
    public void cambioFechaInicialComisionListener(DateSelectEvent dsEvent) {

        //N: en este punto la fecha no se ha sido definida en el value del componente (aquí significa
        //  que this.nuevaComision.getFechaFin() es null), por lo que  hay que tomarla del evento
        LOGGER.debug("seleccionó fecha inicial: " + dsEvent.getDate());

        this.comisionSeleccionada.setFechaInicio(dsEvent.getDate());

        if (this.isSubprocesoDepuracion) {
            this.calcularDuracionesComision(2);
        } else {
            this.calcularDuracionesComision(1);
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * calcula y asigna las duraciones (duración y duración total) de las comisiones según su tipo
     *
     * @author pedro.garcia 16-09-2013
     * @param tipoComision
     */
    private void calcularDuracionesComision(int tipoComision) {

        double duracion, duracionTotal;
        boolean cumpleRegla;

        duracion = 0.0;

        switch (tipoComision) {
            // de Conservación
            //D: no se hace nada porque en las de depuración la duración depende de los trámites pertenecientes
            case 1: {
                break;
            }
            // de Depuración
            case 2: {
                duracion = (this.comisionSeleccionada.getFechaInicio() != null &&
                    this.comisionSeleccionada.getFechaFin() != null) ?
                    UtilidadesWeb.calcularNumeroDiasEntreFechas(
                        this.comisionSeleccionada.getFechaInicio(), this.comisionSeleccionada.
                        getFechaFin()) :
                    0.0;
                this.comisionSeleccionada.setDuracion(duracion);

                break;
            }

            default: {
                break;
            }

        }

        if (this.comisionSeleccionada.getFechaInicio() != null &&
            this.comisionSeleccionada.getFechaFin() != null) {
            duracionTotal = UtilidadesWeb.calcularNumeroDiasEntreFechas(
                this.comisionSeleccionada.getFechaInicio(), this.comisionSeleccionada.getFechaFin());
            //N: hay una regla que dice que se le sume 0.5 días
            duracionTotal += 0.5;
            this.comisionSeleccionada.setDuracionTotal(duracionTotal);

            cumpleRegla = UtilidadesWeb.validarReglaDuracionComision(
                this.comisionSeleccionada.getDuracion(), this.comisionSeleccionada.
                getDuracionTotal());
            if (!cumpleRegla) {
                this.addMensajeWarn(
                    "Existe diferencia entre la duración total de la comisión y la duración de los trámites.");
            }
        }

    }

// end of class
}
