package co.gov.igac.snc.web.util;

import java.io.Serializable;

/**
 * Objeto para mostrar los items del estudio de costos
 *
 * @author franz.gamba
 *
 */
public class ItemDeEstudioDeCostosDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4171549093699301651L;

    private String codigo;
    private String item;
    private String total;
    private String enero;
    private String febrero;
    private String marzo;
    private String abril;
    private String mayo;
    private String junio;
    private String julio;
    private String agosto;
    private String septiembre;
    private String octubre;
    private String noviembre;
    private String diciembre;
    private String pruebaDistribucion;
    private String controlTotal;

    //------------------------------------------------------------------------------------------------------
    /**
     * Full Constructor
     *
     * @param codigo
     * @param item
     * @param total
     * @param enero
     * @param febrero
     * @param marzo
     * @param abril
     * @param mayo
     * @param junio
     * @param julio
     * @param agosto
     * @param septiembre
     * @param octubre
     * @param noviembre
     * @param diciembre
     * @param pruebaDistribucion
     * @param controlTotal
     */
    public ItemDeEstudioDeCostosDTO(String codigo, String item, String total,
        String enero, String febrero, String marzo, String abril,
        String mayo, String junio, String julio, String agosto,
        String septiembre, String octubre, String noviembre,
        String diciembre, String pruebaDistribucion, String controlTotal) {
        this.codigo = codigo;
        this.item = item;
        this.total = total;
        this.enero = enero;
        this.febrero = febrero;
        this.marzo = marzo;
        this.abril = abril;
        this.mayo = mayo;
        this.junio = junio;
        this.julio = julio;
        this.agosto = agosto;
        this.septiembre = septiembre;
        this.octubre = octubre;
        this.noviembre = noviembre;
        this.diciembre = diciembre;
        this.pruebaDistribucion = pruebaDistribucion;
        this.controlTotal = controlTotal;
    }
    //------------------------------------------------------------------------------------------------------

    /*
     * Getters y Setters
     */
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getEnero() {
        return enero;
    }

    public void setEnero(String enero) {
        this.enero = enero;
    }

    public String getFebrero() {
        return febrero;
    }

    public void setFebrero(String febrero) {
        this.febrero = febrero;
    }

    public String getMarzo() {
        return marzo;
    }

    public void setMarzo(String marzo) {
        this.marzo = marzo;
    }

    public String getAbril() {
        return abril;
    }

    public void setAbril(String abril) {
        this.abril = abril;
    }

    public String getMayo() {
        return mayo;
    }

    public void setMayo(String mayo) {
        this.mayo = mayo;
    }

    public String getJunio() {
        return junio;
    }

    public void setJunio(String junio) {
        this.junio = junio;
    }

    public String getJulio() {
        return julio;
    }

    public void setJulio(String julio) {
        this.julio = julio;
    }

    public String getAgosto() {
        return agosto;
    }

    public void setAgosto(String agosto) {
        this.agosto = agosto;
    }

    public String getSeptiembre() {
        return septiembre;
    }

    public void setSeptiembre(String septiembre) {
        this.septiembre = septiembre;
    }

    public String getOctubre() {
        return octubre;
    }

    public void setOctubre(String octubre) {
        this.octubre = octubre;
    }

    public String getNoviembre() {
        return noviembre;
    }

    public void setNoviembre(String noviembre) {
        this.noviembre = noviembre;
    }

    public String getDiciembre() {
        return diciembre;
    }

    public void setDiciembre(String diciembre) {
        this.diciembre = diciembre;
    }

    public String getPruebaDistribucion() {
        return pruebaDistribucion;
    }

    public void setPruebaDistribucion(String pruebaDistribucion) {
        this.pruebaDistribucion = pruebaDistribucion;
    }

    public String getControlTotal() {
        return controlTotal;
    }

    public void setControlTotal(String controlTotal) {
        this.controlTotal = controlTotal;
    }

}
