package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosCarga;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso CU-SA-AC-010 Consultar Carga de Trabajo
 *
 * @author felipe.cadena
 */
@Component("consultaCargaTrabajo")
@Scope("session")
public class ConsultaCargaTrabajoMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = 6866978748748306629L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaCargaTrabajoMB.class);
    /**
     * Usuario en sesión
     */
    private UsuarioDTO usuario;
    /**
     * Teritorial del usuario en sesión.
     */
    private String territorial;
    /**
     * Nombre del avaluador para realizar la busqueda.
     */
    private String nombreProfesional;
    /**
     * Avaluador seleccionado para mostrar información detallada.
     */
    private VProfesionalAvaluosCarga profesionalSeleccionado;
    /**
     * Avaluos seleccionado para detalles.
     */
    private Avaluo avaluoSeleccionado;
    /**
     * Titulo para el tipo de consulta que se esta mostrando
     */
    private String tituloConsultaAvaluos;

    /**
     * Lista con los avaluadores resultantes de la consulta
     */
    private List<VProfesionalAvaluosCarga> listaAvaluadores;
    /**
     * Lista de avalúos relacionados al avaluador
     */
    private List<Avaluo> listaAvaluos;
    /**
     * Bandera para determinar si se estan mostrando los avalúos realizados(Finalizados) o en
     * proceso
     */
    private Boolean banderaAvaluosRealizados;
    /**
     * Bandera para determinar que si estan mostrando los avalúos en control de calidad
     */
    private Boolean banderaControlCalidad;

    /**
     * Estructura para almacenar la información del histórico de avalúos
     */
    private List<Object[]> informacionHistorico;

    // --------getters-setters----------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<Object[]> getInformacionHistorico() {
        return this.informacionHistorico;
    }

    public void setInformacionHistorico(List<Object[]> informacionHistorico) {
        this.informacionHistorico = informacionHistorico;
    }

    public String getTituloConsultaAvaluos() {
        return this.tituloConsultaAvaluos;
    }

    public void setTituloConsultaAvaluos(String tituloConsultaAvaluos) {
        this.tituloConsultaAvaluos = tituloConsultaAvaluos;
    }

    public Boolean getBanderaAvaluosRealizados() {
        return this.banderaAvaluosRealizados;
    }

    public void setBanderaAvaluosRealizados(Boolean banderaAvaluosRealizados) {
        this.banderaAvaluosRealizados = banderaAvaluosRealizados;
    }

    public Boolean getBanderaControlCalidad() {
        return this.banderaControlCalidad;
    }

    public void setBanderaControlCalidad(Boolean banderaControlCalidad) {
        this.banderaControlCalidad = banderaControlCalidad;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public String getTerritorial() {
        return this.territorial;
    }

    public void setTerritorial(String territorial) {
        this.territorial = territorial;
    }

    public String getNombreProfesional() {
        return this.nombreProfesional;
    }

    public void setNombreProfesional(String nombreProfesional) {
        this.nombreProfesional = nombreProfesional;
    }

    public VProfesionalAvaluosCarga getProfesionalSeleccionado() {
        return this.profesionalSeleccionado;
    }

    public void setProfesionalSeleccionado(VProfesionalAvaluosCarga profesionalSeleccionado) {
        this.profesionalSeleccionado = profesionalSeleccionado;
    }

    public List<VProfesionalAvaluosCarga> getListaAvaluadores() {
        return this.listaAvaluadores;
    }

    public void setListaAvaluadores(List<VProfesionalAvaluosCarga> listaAvaluadores) {
        this.listaAvaluadores = listaAvaluadores;
    }

    public List<Avaluo> getListaAvaluos() {
        return this.listaAvaluos;
    }

    public void setListaAvaluos(List<Avaluo> listaAvaluos) {
        this.listaAvaluos = listaAvaluos;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaCargaTrabajoMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {
        //datos de prueba
        this.usuario = MenuMB.getMenu().getUsuarioDto();
    }

    /**
     * Método para buscar los avaluadores en base al criterio de busqueda nombre del profesional
     *
     * @author felipe.cadena
     */
    public void buscarAvaluadores() {
        LOGGER.debug("Consultando avaluadores...");

        this.listaAvaluadores = this.getAvaluosService().
            buscarProfesionalesAvaluosPorNombre(this.nombreProfesional,
                this.usuario.getCodigoTerritorial());

        LOGGER.debug("Avaluadores consultados " + this.listaAvaluadores.size());
    }

    /**
     * Método para buscar los avaluos realizados por el avaluador seleccionado
     *
     * @author felipe.cadena
     */
    public void buscarAvaluosRealizados() {
        LOGGER.debug("Consultando avaluos realizados...");

//OJO 
//TODO :: ?? :: 06-09-2013 :: hay que revisar el estado 'finalizado' porque si es finalizado ok se debe preguntar por 'finalizado_aprobado' :: pedro.garcia        
        if (this.profesionalSeleccionado.getTipoVinculacion().
            equals(EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS.getCodigo())) {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosRealizadosContratoPA(this.profesionalSeleccionado.getContratoId(),
                    EAvaluoAsignacionProfActi.AVALUAR.getCodigo(),
                    ETramiteEstado.FINALIZADO.getCodigo());
        } else {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosProfesionalPorAnio(this.profesionalSeleccionado.
                    getProfesionalAvaluosId(),
                    EAvaluoAsignacionProfActi.AVALUAR.getCodigo(),
                    ETramiteEstado.FINALIZADO.getCodigo(),
                    Calendar.getInstance().get(Calendar.YEAR));
        }

        this.banderaAvaluosRealizados = true;
        this.banderaControlCalidad = false;

        this.tituloConsultaAvaluos = "Avalúos realizados";

        LOGGER.debug("Avaluos consultados");
    }

    /**
     * Método para buscar los avaluos en proceso relacionados con el avaluador seleccionado
     *
     * @author felipe.cadena
     */
    public void buscarAvaluosEnProceso() {
        LOGGER.debug("Consultando avaluos en proceso...");

        if (this.profesionalSeleccionado.getTipoVinculacion().
            equals(EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS.getCodigo())) {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosRealizadosContratoPA(this.profesionalSeleccionado.getContratoId(),
                    EAvaluoAsignacionProfActi.AVALUAR.getCodigo(),
                    ETramiteEstado.EN_PROCESO.getCodigo());

        } else {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosProfesionalPorAnio(this.profesionalSeleccionado.
                    getProfesionalAvaluosId(),
                    EAvaluoAsignacionProfActi.AVALUAR.getCodigo(),
                    ETramiteEstado.EN_PROCESO.getCodigo(),
                    Calendar.getInstance().get(Calendar.YEAR));
        }

        this.banderaAvaluosRealizados = false;
        this.banderaControlCalidad = false;
        this.tituloConsultaAvaluos = "Avalúos en proceso";

        LOGGER.debug("Avaluos consultados");
    }

    /**
     * Método para buscar los avaluos con control de calidad en proceso relacionados con el
     * avaluador seleccionado
     *
     * @author felipe.cadena
     */
    public void buscarControlCalidadAvaluosProceso() {
        LOGGER.debug("Consultando avaluos control calidad proceso...");

        if (this.profesionalSeleccionado.getTipoVinculacion().
            equals(EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS.getCodigo())) {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosRealizadosContratoPA(this.profesionalSeleccionado.getContratoId(),
                    EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo(),
                    ETramiteEstado.EN_PROCESO.getCodigo());

            this.listaAvaluos.addAll(this.getAvaluosService().
                consultarAvaluosRealizadosContratoPA(this.profesionalSeleccionado.getContratoId(),
                    EAvaluoAsignacionProfActi.CC_GIT.getCodigo(),
                    ETramiteEstado.EN_PROCESO.getCodigo()));

        } else {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosProfesionalPorAnio(this.profesionalSeleccionado.
                    getProfesionalAvaluosId(),
                    EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo(),
                    ETramiteEstado.EN_PROCESO.getCodigo(),
                    Calendar.getInstance().get(Calendar.YEAR));
            this.listaAvaluos.addAll(this.getAvaluosService().
                consultarAvaluosProfesionalPorAnio(this.profesionalSeleccionado.
                    getProfesionalAvaluosId(),
                    EAvaluoAsignacionProfActi.CC_GIT.getCodigo(),
                    ETramiteEstado.EN_PROCESO.getCodigo(),
                    Calendar.getInstance().get(Calendar.YEAR)));
        }

        this.banderaAvaluosRealizados = false;
        this.banderaControlCalidad = true;
        this.tituloConsultaAvaluos = "Control de calidad a avalúos";

        LOGGER.debug("Avaluos consultados");
    }

    /**
     * Método para buscar los avaluos con control de calidad realizados relacionados con el
     * avaluador seleccionado
     *
     * @author felipe.cadena
     */
    public void buscarControlCalidadRealizados() {
        LOGGER.debug("Consultando avaluos control calidad realizados...");

        if (this.profesionalSeleccionado.getTipoVinculacion().
            equals(EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS.getCodigo())) {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosRealizadosContratoPA(this.profesionalSeleccionado.getContratoId(),
                    EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo(),
                    ETramiteEstado.FINALIZADO.getCodigo());

            this.listaAvaluos.addAll(this.getAvaluosService().
                consultarAvaluosRealizadosContratoPA(this.profesionalSeleccionado.getContratoId(),
                    EAvaluoAsignacionProfActi.CC_GIT.getCodigo(),
                    ETramiteEstado.FINALIZADO.getCodigo()));
        } else {
            this.listaAvaluos = this.getAvaluosService().
                consultarAvaluosProfesionalPorAnio(this.profesionalSeleccionado.
                    getProfesionalAvaluosId(),
                    EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo(),
                    ETramiteEstado.FINALIZADO.getCodigo(),
                    Calendar.getInstance().get(Calendar.YEAR));
            this.listaAvaluos.addAll(this.getAvaluosService().
                consultarAvaluosProfesionalPorAnio(this.profesionalSeleccionado.
                    getProfesionalAvaluosId(),
                    EAvaluoAsignacionProfActi.CC_GIT.getCodigo(),
                    ETramiteEstado.FINALIZADO.getCodigo(),
                    Calendar.getInstance().get(Calendar.YEAR)));
        }

        this.banderaAvaluosRealizados = true;
        this.banderaControlCalidad = true;
        this.tituloConsultaAvaluos = "Control de calidad a avalúos realizados";

        LOGGER.debug("Avaluos consultados");
    }

    /**
     * Método para consultar el historico de avaluos realizados por el profesional
     *
     * @author felipe.cadena
     */
    public void procesarHistoricoAvaluos() {
        LOGGER.debug("Historico avaluos...");

        this.informacionHistorico = this.getAvaluosService().
            consultarHistoricoAvaluosProfesional(this.profesionalSeleccionado.
                getProfesionalAvaluosId());

        LOGGER.debug("Avaluos consultados");
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaInformacionSolicitudMB#cerrar");

        UtilidadesWeb.removerManagedBean("consultaCargaTrabajo");
        UtilidadesWeb.removerManagedBean("consultaInformacionPredial");
        UtilidadesWeb.removerManagedBean("consultarDptoMunpioAvaluo");

        LOGGER.debug("finalizando ConsultaInformacionSolicitudMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método para cargar la información acerca de la evaluación del avalúo, llama el caso de uso
     * CU-SA-AC-024 enviando el avalúo seleccionado
     *
     * @author felipe.cadena
     */
    public void mostrarEvaluacion() {
        LOGGER.debug("mostrar Evaluacion = " + this.avaluoSeleccionado);
//TODO :: felipe.cadena::llamar caso de uso CU-SA-AC-024 enviando como parametro this.avaluoSeleccionado::?

    }
}
