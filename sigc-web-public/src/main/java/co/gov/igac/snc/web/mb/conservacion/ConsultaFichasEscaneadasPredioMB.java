/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author pedro.garcia
 */
@Component("consultaFichasEscaneadasPredio")
@Scope("session")
public class ConsultaFichasEscaneadasPredioMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -4006709055469157388L;

    private static final Logger LOGGER =
        LoggerFactory.getLogger(ConsultaFichasEscaneadasPredioMB.class);

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * predio con el que se trabaja
     */
    private Predio currentPredio;

    private String urlDocumento;

    /**
     * Documento de la ficha predial digital
     */
    private Documento fichaPredialDigital;

    /**
     * ReporteDTO del documento
     */
    private ReporteDTO reporteDocumento;

    /**
     * Documento de la carta catastral urbana
     */
    private Documento cartaCatastralUrbana;

    /**
     * Documento de la ficha predial escaneada
     */
    private Documento fichaPredialEscaneada;

    /**
     * Documento del plano anexo
     */
    private Documento planoAnexo;

    //------------------   methods    --------------------
    public String getUrlDocumento() {
        return this.urlDocumento;
    }

    public void setUrlDocumento(String urlDocumento) {
        this.urlDocumento = urlDocumento;
    }

    public Predio getCurrentPredio() {
        return this.currentPredio;
    }

    public void setCurrentPredio(Predio currentPredio) {
        this.currentPredio = currentPredio;
    }

    public Documento getFichaPredialDigital() {
        return fichaPredialDigital;
    }

    public void setFichaPredialDigital(Documento fichaPredialDigital) {
        this.fichaPredialDigital = fichaPredialDigital;
    }

    public Documento getCartaCatastralUrbana() {
        return this.cartaCatastralUrbana;
    }

    public void setCartaCatastralUrbana(Documento cartaCatastralUrbana) {
        this.cartaCatastralUrbana = cartaCatastralUrbana;
    }

    public Documento getFichaPredialEscaneada() {
        return fichaPredialEscaneada;
    }

    public void setFichaPredialEscaneada(Documento fichaPredialEscaneada) {
        this.fichaPredialEscaneada = fichaPredialEscaneada;
    }

    public Documento getPlanoAnexo() {
        return planoAnexo;
    }

    public void setPlanoAnexo(Documento planoAnexo) {
        this.planoAnexo = planoAnexo;
    }

    //--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init ConsultaFichasEscaneadasPredioMB");

        //D: se obtiene la referencia al mb que maneja los detalles del predio
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");

        Long predioSeleccionadoId = mb001.getSelectedPredioId();
        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioSeleccionadoId != null) {
            this.currentPredio = new Predio();
            if (!esPredioOrigen) {
                this.getPredioById(predioSeleccionadoId);
            } else {
                this.currentPredio = mb001.getSelectedPredio1();
                getDocumentosCurrentPredio();
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * se define este método solo para mantener el estándar del tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * Además de obtener los datos del predio se consultan las fichas escaneadas.
     *
     * @author pedro.garcia
     * @modified by javier.aponte Se consulta en la base de datos la ficha predial dígital asociada
     * al predio juan.cruz::07/12/2017::19012 se separa la carga de documentos a un metodo diferente
     * para poder buscarlos desde un método distinto
     * @param predioId
     */
    /*
     * @modified by leidy.gonzalez:: Se verifica si ya fue generada anteriormente la Carta Catastral
     * Urbana y se visualiza el reporte del documento.
     */
    private void getPredioById(Long predioId) {

        //D: get predio
        try {
            this.currentPredio = this.getConservacionService().obtenerPredioPorId(predioId);
            getDocumentosCurrentPredio();
        } catch (Exception ex) {
            LOGGER.
                error("Excepción consultando predio con id " + predioId + " : " + ex.getMessage());
            return;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de traer del gestor documental la ficha predial digital
     *
     * @author javier.aponte
     */
    public void traerFichaPredialDigitalDeGestorDocumental() {
        if (this.fichaPredialDigital != null) {

            this.reporteDocumento = this.reportsService.consultarReporteDeGestorDocumental(
                this.fichaPredialDigital.getIdRepositorioDocumentos());

            this.urlDocumento = this.reporteDocumento.getUrlWebReporte();
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método encargado de traer del gestor documental la carta catastral urbana
     *
     * @author javier.aponte
     */
    public void traerCartaCatastralUrbanaDeGestorDocumental() {
        if (this.cartaCatastralUrbana != null) {

            if (this.cartaCatastralUrbana.getIdRepositorioDocumentos().endsWith("pdf")) {
                this.reporteDocumento = this.reportsService.
                    consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(
                        this.cartaCatastralUrbana.getIdRepositorioDocumentos());
            } else {
                this.reporteDocumento = this.reportsService.consultarReporteDeGestorDocumental(
                    this.cartaCatastralUrbana.getIdRepositorioDocumentos());
            }

            this.urlDocumento = this.reporteDocumento.getUrlWebReporte();
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método encargado de traer del gestor documental la ficha predial escaneada
     *
     * @author javier.aponte
     */
    public void traerFichaPredialEscaneadaDeGestorDocumental() {
        if (this.fichaPredialEscaneada != null) {
            //v1.1.7
            this.reporteDocumento = this.reportsService.
                consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(
                    this.fichaPredialEscaneada.getIdRepositorioDocumentos());

            this.urlDocumento = this.reporteDocumento.getUrlWebReporte();
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método encargado de traer del gestor documental el plano anexo
     *
     * @author javier.aponte
     */
    public void traerPlanoAnexoDeGestorDocumental() {
        if (this.planoAnexo != null) {
            //v1.1.7
            this.reporteDocumento = this.reportsService.
                consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(
                    this.planoAnexo.getIdRepositorioDocumentos());

            this.urlDocumento = this.reporteDocumento.getUrlWebReporte();
        }
    }

    /**
     * Método que trae los documentos del predio actual
     *
     * @author juan.cruz
     */
    public void getDocumentosCurrentPredio() {
        try {

            if (this.fichaPredialDigital == null) {
                this.fichaPredialDigital = this.getConservacionService().
                    buscarDocumentoPorPredioIdAndTipoDocumento(this.currentPredio.getId(),
                        ETipoDocumento.FICHA_PREDIAL_DIGITAL.getId());
            }

        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage());
        }

        try {

            if (this.fichaPredialEscaneada == null) {
                this.fichaPredialEscaneada = this.getConservacionService().
                    buscarDocumentoPorPredioIdAndTipoDocumento(this.currentPredio.getId(),
                        ETipoDocumento.FICHA_PREDIAL_ESCANEADA.getId());
            }

        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial escaneada: " + ex.getMessage());
        }

        try {

            if (this.cartaCatastralUrbana == null) {
                this.cartaCatastralUrbana = this.getConservacionService().
                    buscarDocumentoPorNumeroPredialAndTipoDocumento(this.currentPredio.
                        getNumeroPredial().substring(0, 17),
                        ETipoDocumento.CARTA_CATASTRAL_URBANA.getId());
            } else {
                // Se valida que si existe el documento de carta catastral se tome del repositorio de documentos
                // la ruta del reporte a visualizar.
                this.reporteDocumento = this.reportsService.consultarReporteDeGestorDocumental(
                    this.cartaCatastralUrbana.getIdRepositorioDocumentos());
            }

        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo la ficha predial: " + ex.getMessage());
        }

        try {

            if (this.planoAnexo == null) {
                this.planoAnexo = this.getConservacionService().
                    buscarDocumentoPorPredioIdAndTipoDocumento(this.currentPredio.getId(),
                        ETipoDocumento.FICHA_PREDIAL_ESCANEADA_ANEXO_PLANO.getId());
            }

        } catch (Exception ex) {
            LOGGER.error("Excepción obteniendo el plano anexo de la ficha predial escaneada: " + ex.
                getMessage());
        }
    }

//end of class
}
