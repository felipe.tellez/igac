package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionLevantamiento;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionOrdenAjuste;
import co.gov.igac.snc.persistence.entity.actualizacion.AjusteManzanaArea;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.util.EActualizacionContratoActividad;
import co.gov.igac.snc.persistence.util.EAsuntoCorreoElectronicoActualizacion;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISParametrosAsignacion;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.actualizacion.VisualizacionCorreoActualizacionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.FiltroPredio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el CU 208 de alistamiento : Clasificar manzanas y areas para levantamiento
 * topografico
 *
 * @author lorena.salamanca
 * @modified by franz.gamba
 * @modified andres.eslava 07-11-2012
 *
 */
@Component("clasificarManzanasYAreasParaLevantamientoTopografico")
@Scope("session")
public class ClasificacionManzanasYAreasParaLevantamientoTopograficoMB extends
    SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ClasificacionManzanasYAreasParaLevantamientoTopograficoMB.class);

    /**
     * Topografo al que se le asigna la manzana para ajuste.
     */
    private Long codigoResponsableSIG;

    /**
     * Lista de topografos disponibles para la seleccion.
     */
    private List<SelectItem> responsablesSig;
    List<RecursoHumano> responsablesSigExistentes;

    /**
     * Lista de manzanas del municipio en proceso de actualización
     */
    private List<ManzanaVereda> manzanasMunicipio;

    /**
     * Lista de manzanas seleccionadas en la vista para la ordenAjuste
     */
    private ManzanaVereda[] manzanasSeleccionadas;

    /**
     * Establece si se debe incorporar nuevos manzanas en la orden de ajuste para el responsable SIG
     *
     */
    private boolean incorporarNuevasManzanas;

    /**
     * Establece si se debe digitalizar Nuevas Areas en la orden de ajuste para el responsable SIG
     *
     */
    private boolean digitalizarNuevasAreas;

    /**
     * Observaciones sobre el ajuste de manzanas asignado.
     */
    private String observaciones;

    /**
     * Manzanas seleccinadas desde el visor
     */
    private List<String> manzanasSeleccionadasVisor;

    /**
     * Observacion cuando no se aprueban los ajustes realizados
     */
    private String observacionesNoAprobado;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Determina la capa del visor usada por el Geoservicio.
     */
    private String moduleLayer;
    /**
     * Determina las herramientas a desplegar en el geoservicio.
     */
    private String moduleTools;
    /**
     * Manzanas de prueba
     */
    private String manzanaCodigo;
    private String valorSeleccionManzanas;

    /**
     * Bandera para notificar si las manzanas corresponden a una zona rural o urbana
     *
     */
    private Boolean flagEsRural;

    /**
     * texto correo electronico de notificacion de ajustes al responsable SIG
     */
    private String textoMailNotificacionAjustes;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites
     */
    private LazyDataModel<ManzanaVereda> lazyAssignedManzana;

    /**
     * Codigo del muicipio asociado a la actualizacion
     */
    private String codigoMunicipioAsociadoActualizacion;

    /**
     * Actualizacion asociada a la clasificacion de manzanas
     */
    private Actualizacion actualizacionAsociada;

    /**
     * Bandera para habilitar el panel de busqueda
     */
    private Boolean banderaPanelBusqueda;

    /**
     * Datos busqueda de numero predial inicial
     */
    private FiltroPredio filtroInicial;

    /**
     * Datos buscqueda de numero predial final
     */
    private FiltroPredio filtroFinal;

    /**
     * Bandera para habilitar la opcion finalizar ajuste
     */
    private Boolean banderaFinalizarAjustes;

    /**
     * Bander para habilitar la opcion finalizar levantamiento
     */
    private Boolean banderaFinalizarLevantamiento;

    /**
     * Usuario autenticado en el sistema.
     */
    private UsuarioDTO usuario;

    /**
     * Recurso humano ResponsableSIG seleccionado informacion completa
     */
    private RecursoHumano responsableSig;

    /**
     * Ordenes de ajuste asigandas
     */
    private List<AjusteManzanaArea> ajusteManzanaAreaAsignadasResponsableActivo;

    /**
     * Bandera para activar o desactivar el responsable sig.
     */
    private Boolean banderaResponsableSig;

    /**
     * Lista de manzanas en el rango consultado
     */
    private List<ManzanaVereda> listaManzanasEnRango;

    /**
     * Ordenes de ajuste seleccionadas para aceptació
     */
    private AjusteManzanaArea[] AjusteManzanaAreasSeleccionadas;

    /**
     * Lista de manzanas para levantamiento
     */
    private List<ManzanaVereda> listaAjusteManzanasLevantamiento;

    /*
     * arreglo de manzanas seleccionadas para levantamiento
     */
    private ManzanaVereda[] manzanasSeleccionadasLevantamiento;

    /**
     * Orden ajuste activa
     *
     * @return
     */
    private ActualizacionOrdenAjuste ordenAjusteParaAprobacionActiva;
    // **************************************************************************************************************************
    // Getters y Setters
    // **************************************************************************************************************************

    public ActualizacionOrdenAjuste getOrdenAjusteParaAprobacionActiva() {
        return ordenAjusteParaAprobacionActiva;
    }

    public void setOrdenAjusteParaAprobacionActiva(
        ActualizacionOrdenAjuste ordenAjusteParaAprobacionActiva) {
        this.ordenAjusteParaAprobacionActiva = ordenAjusteParaAprobacionActiva;
    }

    public ManzanaVereda[] getManzanasSeleccionadasLevantamiento() {
        return manzanasSeleccionadasLevantamiento;
    }

    public void setManzanasSeleccionadasLevantamiento(
        ManzanaVereda[] manzanasSeleccionadasLevantamiento) {
        this.manzanasSeleccionadasLevantamiento = manzanasSeleccionadasLevantamiento;
    }

    public List<ManzanaVereda> getListaAjusteManzanasLevantamiento() {
        return listaAjusteManzanasLevantamiento;
    }

    public void setListaAjusteManzanasLevantamiento(
        List<ManzanaVereda> listaAjusteManzanasLevantamiento) {
        this.listaAjusteManzanasLevantamiento = listaAjusteManzanasLevantamiento;
    }

    public List<AjusteManzanaArea> getAjusteManzanaAreaAsignadasResponsableActivo() {
        return ajusteManzanaAreaAsignadasResponsableActivo;
    }

    public void setAjusteManzanaAreaAsignadasResponsableActivo(
        List<AjusteManzanaArea> ajusteManzanaAreaAsignadasResponsableActivo) {
        this.ajusteManzanaAreaAsignadasResponsableActivo =
            ajusteManzanaAreaAsignadasResponsableActivo;
    }

    public AjusteManzanaArea[] getAjusteManzanaAreasSeleccionadas() {
        return AjusteManzanaAreasSeleccionadas;
    }

    public void setAjusteManzanaAreasSeleccionadas(
        AjusteManzanaArea[] ajusteManzanaAreasSeleccionadas) {
        AjusteManzanaAreasSeleccionadas = ajusteManzanaAreasSeleccionadas;
    }

    public Actualizacion getActualizacionAsociada() {
        return actualizacionAsociada;
    }

    public void setActualizacionAsociada(Actualizacion actualizacionAsociada) {
        this.actualizacionAsociada = actualizacionAsociada;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Boolean getBanderaResponsableSig() {
        return banderaResponsableSig;
    }

    public void setBanderaResponsableSig(Boolean banderaResponsableSig) {
        this.banderaResponsableSig = banderaResponsableSig;
    }

    public RecursoHumano getResponsableSig() {
        return responsableSig;
    }

    public void setResponsableSig(RecursoHumano responsableSig) {
        this.responsableSig = responsableSig;
    }

    public Boolean getBanderaFinalizarAjustes() {
        return banderaFinalizarAjustes;
    }

    public void setBanderaFinalizarAjustes(Boolean banderaFinalizarAjustes) {
        this.banderaFinalizarAjustes = banderaFinalizarAjustes;
    }

    public Boolean getBanderaFinalizarLevantamiento() {
        return banderaFinalizarLevantamiento;
    }

    public void setBanderaFinalizarLevantamiento(
        Boolean banderaFinalizarLevantamiento) {
        this.banderaFinalizarLevantamiento = banderaFinalizarLevantamiento;
    }

    public FiltroPredio getFiltroFinal() {
        return filtroFinal;
    }

    public void setFiltroFinal(FiltroPredio filtroFinal) {
        this.filtroFinal = filtroFinal;
    }

    public String getCodigoMunicipioAsociadoActualizacion() {
        return codigoMunicipioAsociadoActualizacion;
    }

    public void setCodigoMunicipioAsociadoActualizacion(
        String codigoMunicipioAsociadoActualizacion) {
        this.codigoMunicipioAsociadoActualizacion = codigoMunicipioAsociadoActualizacion;
    }

    public Boolean getBanderaPanelBusqueda() {
        return banderaPanelBusqueda;
    }

    public void setBanderaPanelBusqueda(Boolean banderaPanelBusqueda) {
        this.banderaPanelBusqueda = banderaPanelBusqueda;
    }

    public FiltroPredio getFiltroInicial() {
        return filtroInicial;
    }

    public void setFiltroInicial(FiltroPredio filtroInicial) {
        this.filtroInicial = filtroInicial;
    }

    public LazyDataModel<ManzanaVereda> getLazyAssignedManzana() {
        return lazyAssignedManzana;
    }

    public void setLazyAssignedManzana(LazyDataModel<ManzanaVereda> lazyAssignedManzana) {
        this.lazyAssignedManzana = lazyAssignedManzana;
    }

    public List<SelectItem> getResponsablesSig() {
        return responsablesSig;
    }

    public void setResponsablesSig(List<SelectItem> responsablesSig) {
        this.responsablesSig = responsablesSig;
    }

    public Long getCodigoResponsableSIG() {
        return this.codigoResponsableSIG;
    }

    public void setCodigoResponsableSIG(Long codigoResponsableSIG) {
        this.codigoResponsableSIG = codigoResponsableSIG;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getObservacionesNoAprobado() {
        return observacionesNoAprobado;
    }

    public void setObservacionesNoAprobado(String observacionesNoAprobado) {
        this.observacionesNoAprobado = observacionesNoAprobado;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public List<ManzanaVereda> getManzanasMunicipio() {
        return manzanasMunicipio;
    }

    public void setManzanasMunicipio(List<ManzanaVereda> manzanasMunicipio) {
        this.manzanasMunicipio = manzanasMunicipio;
    }

    public ManzanaVereda[] getManzanasSeleccionadas() {
        return manzanasSeleccionadas;
    }

    public void setManzanasSeleccionadas(ManzanaVereda[] manzanasSeleccionadas) {
        this.manzanasSeleccionadas = manzanasSeleccionadas;
    }

    public String getValorSeleccionManzanas() {
        return valorSeleccionManzanas;
    }

    public void setValorSeleccionManzanas(String valorSeleccionManzanas) {
        this.valorSeleccionManzanas = valorSeleccionManzanas;
    }

    public boolean isIncorporarNuevasManzanas() {
        return incorporarNuevasManzanas;
    }

    public void setIncorporarNuevasManzanas(boolean incorporarNuevasManzanas) {
        this.incorporarNuevasManzanas = incorporarNuevasManzanas;
    }

    public Boolean getFlagEsRural() {
        return flagEsRural;
    }

    public void setFlagEsRural(Boolean flagEsRural) {
        this.flagEsRural = flagEsRural;
    }

    public boolean isDigitalizarNuevasAreas() {
        return digitalizarNuevasAreas;
    }

    public void setDigitalizarNuevasAreas(boolean digitalizarNuevasAreas) {
        this.digitalizarNuevasAreas = digitalizarNuevasAreas;
    }

    public List<String> getManzanasSeleccionadasVisor() {
        return manzanasSeleccionadasVisor;
    }

    public void setManzanasSeleccionadasVisor(
        List<String> manzanasSeleccionadasVisor) {
        this.manzanasSeleccionadasVisor = manzanasSeleccionadasVisor;
    }

    public String getTextoMailNotificacionAjustes() {
        return textoMailNotificacionAjustes;
    }

    public void setTextoMailNotificacionAjustes(
        String textoMailNotificacionAjustes) {
        this.textoMailNotificacionAjustes = textoMailNotificacionAjustes;
    }

    public List<RecursoHumano> getResponsablesSigExistentes() {
        return responsablesSigExistentes;
    }

    public void setResponsablesSigExistentes(
        List<RecursoHumano> responsablesSigExistentes) {
        this.responsablesSigExistentes = responsablesSigExistentes;
    }

    public List<ManzanaVereda> getListaManzanasEnRango() {
        return listaManzanasEnRango;
    }

    public void setListaManzanasEnRango(List<ManzanaVereda> listaManzanasEnRango) {
        this.listaManzanasEnRango = listaManzanasEnRango;
    }
    // **************************************************************************************************************************
    // Bussines and event methods
    // **************************************************************************************************************************

    @PostConstruct
    public void init() {

        // Establece el usuario autenticado en el sistema. 
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // Establece la actualización asociada.
        this.actualizacionAsociada = this.getActualizacionService().
            obtenerActualizacionConMunicipio(579L);

        // Establece la lista de responsables SIG para su selección.
        responsablesSig = new LinkedList<SelectItem>();
        cargarListaResponsablesSig();

        // Carga las manzanas de un municipio.
        //this.setManzanasMunicipio(this.getActualizacionService().obtenerManzanasMunicipio(actualizacionAsociada.getMunicipio().getCodigo()));
        this.setManzanasMunicipio(this.getActualizacionService().obtenerManzanasMunicipio("95015"));

        // Establece las caracteristicas de la busqueda por rango de manzanas. 
        this.filtroInicial = new FiltroPredio();
        this.filtroFinal = new FiltroPredio();
        this.getFiltroInicial().setNumeroPredialS1(actualizacionAsociada.getMunicipio().
            getDepartamento().getCodigo());
        this.getFiltroInicial().setNumeroPredialS2(actualizacionAsociada.getMunicipio().
            getCodigo3Digitos());
        this.getFiltroFinal().setNumeroPredialS1(actualizacionAsociada.getMunicipio().
            getDepartamento().getCodigo());
        this.getFiltroFinal().setNumeroPredialS2(actualizacionAsociada.getMunicipio().
            getCodigo3Digitos());

        // Parametros necesarios para la carga del visor con los predios seleccionados.
        this.manzanaCodigo = "08001010300000439,08001010300000520,08001010300000779";
        this.moduleLayer = EVisorGISLayer.ACTUALIZACION.getCodigo();
        this.moduleTools = EVisorGISTools.ACTUALIZACION.getCodigo();
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.flagEsRural = true;

        // Parametros para la busqueda por rango
        this.listaManzanasEnRango = new LinkedList<ManzanaVereda>();

        observaciones = new String();
        observacionesNoAprobado = new String();
        this.valorSeleccionManzanas =
            EVisorGISParametrosAsignacion.SELECCIONAR_PREDIOS_ACTUALIZACION_CU208.getCodigo();
        banderaResponsableSig = Boolean.FALSE;
        this.setBanderaPanelBusqueda(true);

    }

    /**
     * Carga la lista de responsables de la asignacion de manzanas que requieren ajuste
     *
     * @author andres.eslava
     */
    private void cargarListaResponsablesSig() {

        try {
            responsablesSigExistentes = this.getActualizacionService()
                .obtenerRecursoHumanoPorActividad(EActualizacionContratoActividad.RESPONSABLE_SIG.
                    getCodigo());

            for (RecursoHumano responsableSig : responsablesSigExistentes) {
                responsablesSig.add(new SelectItem(responsableSig.getId(), responsableSig.
                    getNombre()));
            }
        } catch (Exception e) {
            this.addMensajeError("Error recuperando los responsables SIG.");
            LOGGER.error("Error recuperando los responsables SIG.", e);
        }
    }

    /**
     * Busca las manzanas en un rango
     *
     * @author andres.eslava
     */
    public void buscarManzanasPorRango() {
        try {

            String inicioRango = new String();
            String finalRango = new String();
            Boolean banderaRangoInicial = false;
            Boolean banderaRangoFinal = false;

            if (this.getFiltroInicial().getNumeroPredialS3() != null &&
                 this.getFiltroInicial().getNumeroPredialS4() != null &&
                 this.getFiltroInicial().getNumeroPredialS5() != null &&
                 this.getFiltroInicial().getNumeroPredialS6() != null &&
                 this.getFiltroInicial().getNumeroPredialS7() != null &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS3()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS4()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS5()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS6()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS7())) {
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS1());
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS2());
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS3());
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS4());
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS5());
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS6());
                inicioRango = inicioRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS7());
                banderaRangoInicial = true;
            }

            if (!"".equals(this.getFiltroInicial().getNumeroPredialS3()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS4()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS5()) &&
                 !"".equals(this.getFiltroInicial().getNumeroPredialS6()) &&
                 !"".equals(this.getFiltroFinal().getNumeroPredialS7()) &&
                 this.getFiltroInicial().getNumeroPredialS3() != null &&
                 this.getFiltroInicial().getNumeroPredialS4() != null &&
                 this.getFiltroInicial().getNumeroPredialS5() != null &&
                 this.getFiltroInicial().getNumeroPredialS6() != null &&
                 this.getFiltroFinal().getNumeroPredialS7() != null) {
                finalRango = finalRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS1());
                finalRango = finalRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS2());
                finalRango = finalRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS3());
                finalRango = finalRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS4());
                finalRango = finalRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS5());
                finalRango = finalRango.concat(this.getFiltroInicial()
                    .getNumeroPredialS6());
                finalRango = finalRango.concat(this.getFiltroFinal()
                    .getNumeroPredialS7());
                banderaRangoFinal = true;
            }
            if (banderaRangoInicial && banderaRangoFinal) {
                this.listaManzanasEnRango = this.getActualizacionService().obtenerManzanasPorRango(
                    inicioRango, finalRango);
                this.addMensajeInfo("Se buscaron manzanas desde: " + inicioRango + " hasta " +
                    finalRango + ".");
                LOGGER.info("Se buscaron manzanas desde: " + inicioRango + " hasta " + finalRango +
                    ".");
            } else {
                this.addMensajeError(
                    "Debe digitar un rango valido de manzanas, todos los espacios deben ser diligenciados.");
                LOGGER.warn(
                    "Debe digitar un rango valido de manzanas, todos los espacios deben ser diligenciados.");
            }

        } catch (Exception e) {
            this.addMensajeError("Error buscando las manzanas en el rango.");
            LOGGER.error("Error buscando las manzanas en el rango.", e);
        }
    }

    /**
     * Carga los datos del mensaje
     *
     * @author andres.eslava
     */
    public void enviarCorreoOrdenAjuste() {

        String asunto;
        String[] adjuntos = null;
        String codigoPlantillaContenido;
        String[] titulosAdjuntos = null;
        String[] destinatarios = new String[]{"andres.eslava@igac.gov.co"};
        String remitente = "andres.eslava@igac.gov.co";

        codigoPlantillaContenido = EPlantilla.MENSAJE_COMUNICACION_ORDEN_AJUSTE.getCodigo();
        asunto = EAsuntoCorreoElectronicoActualizacion.ASUNTO_SOLICITUD_ORDEN_AJUSTE.getAsunto();

        // Parámetros de la plantilla de contenido
        try {
            String[] parametrosPlantillaContenido = new String[10];

            parametrosPlantillaContenido[0] = MenuMB.getMenu().getUsuarioDto().getNombreCompleto();
            parametrosPlantillaContenido[1] = new String("Destino");
            parametrosPlantillaContenido[2] = Calendar.getInstance().getTime().toString();
            parametrosPlantillaContenido[3] = new String("Municipio");
            parametrosPlantillaContenido[4] = new String("Municipio");

            if (this.incorporarNuevasManzanas) {
                parametrosPlantillaContenido[5] = new String(
                    "<p>Realizar los ajustes necesarios al plano de conjunto urbano " +
                     "de este municipio, incorporando e identificando las nuevas " +
                     "manzanas, con base en la información contenida en " +
                     "los documentos cartográficos disponibles.</p>");
            }
            if (this.digitalizarNuevasAreas) {
                parametrosPlantillaContenido[6] = new String(
                    "<p>Digitalizar las nuevas áreas incorporadas al perímetro " +
                     "urbano indicadas en los documentos adjuntos, " +
                     "orientadas a la asignación de trabajos a un " +
                     "topógrafo por área</p>");
            }

            //if (this.manzanasSeleccionadas.length > 0) {
            parametrosPlantillaContenido[7] = new String(
                "<p>Realizar los ajustes necesarios al plano de " +
                 "conjunto urbano de este municipio, incorporando e " +
                 "identificando las nuevas manzanas, con base en la " +
                 "información contenida en los documentos cartográficos " +
                 "disponibles.</p>");
            //}

            parametrosPlantillaContenido[8] = new String("<p> " + this.observaciones + " </p>");
            parametrosPlantillaContenido[9] = MenuMB.getMenu().getUsuarioDto().getNombreCompleto();

            // Cargar parámetros en el managed bean del componente.
            VisualizacionCorreoActualizacionMB visualCorreoMB =
                (VisualizacionCorreoActualizacionMB) UtilidadesWeb
                    .getManagedBean("visualizacionCorreoActualizacion");
            visualCorreoMB.inicializarParametros(asunto, destinatarios, remitente,
                adjuntos, codigoPlantillaContenido, null,
                parametrosPlantillaContenido, titulosAdjuntos);

            // Se crea y persiste la orden de ajuste con las manzanas para ajuste correspondientes.		
            ActualizacionOrdenAjuste actualizacionOrdenAjuste = new ActualizacionOrdenAjuste();
            List<AjusteManzanaArea> manzanasParaAjuste = new LinkedList<AjusteManzanaArea>();

            for (int i = 0; i < manzanasSeleccionadas.length; i++) {
                AjusteManzanaArea nuevoAjusteManzana = new AjusteManzanaArea();
                nuevoAjusteManzana.setActualizacionOrdenAjuste(actualizacionOrdenAjuste);
                nuevoAjusteManzana.setFechaLog(new Date(System.currentTimeMillis()));
                nuevoAjusteManzana.setIdentificador(manzanasSeleccionadas[i].getCodigo());
                nuevoAjusteManzana.setUsuarioLog(this.usuario.getLogin());
                manzanasParaAjuste.add(nuevoAjusteManzana);
            }

            actualizacionOrdenAjuste.setActualizacionId(this.actualizacionAsociada.getId());
            actualizacionOrdenAjuste.setAjusteManzanaAreas(manzanasParaAjuste);
            actualizacionOrdenAjuste.setFechaLog(new Date(System.currentTimeMillis()));
            actualizacionOrdenAjuste.setObservaciones(this.observaciones);
            actualizacionOrdenAjuste.setRecursoHumano(this.responsableSig);
            actualizacionOrdenAjuste.setUsuarioLog(this.usuario.getLogin());

            if (this.digitalizarNuevasAreas) {
                actualizacionOrdenAjuste.setDigitalizarNuevasAreas("SI");
            } else {
                actualizacionOrdenAjuste.setDigitalizarNuevasAreas("NO");
            }

            if (this.incorporarNuevasManzanas) {
                actualizacionOrdenAjuste.setIncorporarNuevasManzanas("SI");
            } else {
                actualizacionOrdenAjuste.setIncorporarNuevasManzanas("NO");
            }
            ActualizacionOrdenAjuste actualizacionOrdenAjusteDB = this.getActualizacionService().
                guardarActualizacionOrdenAjuste(actualizacionOrdenAjuste);

        } catch (Exception e) {
            LOGGER.error(
                "Error notificando el ajuste  ClasificacionManzanasYAreasParaLevantamientoTopograficoMB#enviarCorreoOrdenAjuste...ERROR" +
                e);
            this.addMensajeError("Error enviando la notificacion del ajuste");
        }
    }

    /**
     * Busca el responsable sig en la
     *
     * @param idRecursoHumanoSeleccionado
     * @return
     * @author andres.eslava
     */
    private void buscaResponsablesSigSeleccionado() {
        if (null != this.codigoResponsableSIG) {
            for (RecursoHumano responsableSigExistente : responsablesSigExistentes) {
                if (this.codigoResponsableSIG == responsableSigExistente.getId()) {
                    this.setResponsableSig(responsableSigExistente);
                }
            }
        } else {
            this.addMensajeError("Debe Seleccionar un responsable sig, para asignar los ajustes.");
            LOGGER.warn("Debe Seleccionar un responsable sig, para asignar los ajustes.");
        }
    }

    /**
     * Carga las ordenes de ajuste de un responsable determinado
     *
     * @author andres.eslava
     */
    public void cargaOrdenesAjustePorResponsable() {
        buscaResponsablesSigSeleccionado();
        List<ActualizacionOrdenAjuste> ordenesAjusteAsignadasResponsableActivo = null;
        try {
            ordenesAjusteAsignadasResponsableActivo = this.getActualizacionService().
                obtenerOrdenesAjustePorResponsableSig(this.getResponsableSig());
            if (null != ordenesAjusteAsignadasResponsableActivo) {
                for (ActualizacionOrdenAjuste unaActualizacionOrdenAjuste
                    : ordenesAjusteAsignadasResponsableActivo) {
                    this.ordenAjusteParaAprobacionActiva = unaActualizacionOrdenAjuste;
                    for (AjusteManzanaArea ajuste : unaActualizacionOrdenAjuste.
                        getAjusteManzanaAreas()) {

                        this.ajusteManzanaAreaAsignadasResponsableActivo.add(ajuste);
                    }
                }
            } else {
                this.addMensajeError("Este responsable no tiene ajustes previos asignados");
            }
        } catch (Exception e) {
            this.addMensajeError("Error al cargar las ordenes de ajuste asignadas");
            LOGGER.error("Error al cargar las ordenes de ajuste asignadas", e);
        }
    }

    /**
     * Evento de cambio de tab en el visor, identificar si el responsable esta verificando el
     * trabajod el responsable sig
     *
     * @author andres.eslava
     */
    public void cambioTab(TabChangeEvent event) {
        if (event.getTab().getTitle().equals("Verificar manzanas")) {
            this.setBanderaResponsableSig(Boolean.FALSE);
            this.cargaOrdenesAjustePorResponsable();
        } else if (event.getTab().getTitle().equals("Manzanas para levantamiento topográfico")) {
            this.cargaTablaLevantamientoTopografico();
        } else {
            this.setBanderaResponsableSig(Boolean.TRUE);
        }
    }

    /**
     * Carga la lista de manzanas disponibles para levantamiento topografico
     *
     * @author andres.eslava
     */
    public void cargaTablaLevantamientoTopografico() {
        Boolean banderaIncluirLevantamiento = null;
        if ((null != this.manzanasMunicipio) && (manzanasMunicipio.size() > 0) &&
             (manzanasSeleccionadas.length > 0)) {
            for (ManzanaVereda manzana : this.manzanasMunicipio) {
                banderaIncluirLevantamiento = Boolean.TRUE;
                for (int i = 0; i < manzanasSeleccionadas.length; i++) {
                    if (manzana.getCodigo().equals(manzanasSeleccionadas[i].getCodigo())) {
                        banderaIncluirLevantamiento = Boolean.FALSE;
                    }
                }
                if (banderaIncluirLevantamiento) {
                    this.listaAjusteManzanasLevantamiento.add(manzana);
                }
            }
        } else if ((null != this.manzanasMunicipio) &&
             (manzanasMunicipio.size() > 0)) {
            this.listaAjusteManzanasLevantamiento = null;
            this.listaAjusteManzanasLevantamiento = new LinkedList<ManzanaVereda>();
            for (ManzanaVereda manzanaSinAjuste : this.manzanasMunicipio) {
                this.listaAjusteManzanasLevantamiento.add(manzanaSinAjuste);
            }
        } else {
            this.addMensajeError("No hay manzanas para levantamiento");
            LOGGER.warn("No hay manzanas para levantamiento");
        }
    }

    /**
     * Persiste el estado del ajuste aprobado
     *
     * @author andres.eslava
     */
    public void guardaAjustesAprobados() {
        try {
            this.ordenAjusteParaAprobacionActiva.setEstado("Aprobado");
            this.getActualizacionService().guardarActualizacionOrdenAjuste(
                ordenAjusteParaAprobacionActiva);
        } catch (Exception e) {
            this.addMensajeError("Error aprobando los ajustes");
            LOGGER.error("Error aprobando los ajustes", e);
        }
    }

    /**
     * Persiste el estado de los ajustes no aprobados
     *
     * @author andres.eslava
     */
    public void guardaAjustesNoAprobados() {
        try {
            this.ordenAjusteParaAprobacionActiva.setEstado("No aprobado");
            this.getActualizacionService().guardarActualizacionOrdenAjuste(
                ordenAjusteParaAprobacionActiva);
        } catch (Exception e) {
            this.addMensajeError("Error persisitiendo la desaprobacion de los ajustes");
            LOGGER.error("Error persisitiendo la desaprobacion de los ajustes", e);
        }
    }

    /**
     * Persiste las manzanas asignadas para levantamiento
     *
     * @author andres.eslava
     */
    public void asignarParaLevantamiento() {
        try {
            if (manzanasSeleccionadasLevantamiento.length > 0) {
                for (int i = 0; i < this.manzanasSeleccionadasLevantamiento.length; i++) {
                    ActualizacionLevantamiento nuevaActualizacionLevantamiento =
                        new ActualizacionLevantamiento();
                    nuevaActualizacionLevantamiento.setUsuarioLog(this.usuario.getLogin());
                    nuevaActualizacionLevantamiento.
                        setFechaLog(new Date(System.currentTimeMillis()));
                    nuevaActualizacionLevantamiento.setIdentificador(
                        this.manzanasSeleccionadasLevantamiento[i].getCodigo());
                    nuevaActualizacionLevantamiento.setActualizacion(this.actualizacionAsociada);

                    this.getActualizacionService().guardaActualizacionLevantamiento(
                        nuevaActualizacionLevantamiento);
                }
            } else {
                this.addMensajeError("No se ha seleccionada ninguna manzana para levantamiento.");
                LOGGER.warn("No se ha seleccionada ninguna manzana para levantamiento.");
            }
        } catch (Exception e) {
            this.addMensajeError("Error en la inclusión de las manzanas para levantamiento");
            LOGGER.error("Error en la inclusión de las manzanas para levantamiento", e);
        }
    }

    /**
     * Asigana manzanas en proceso de aprobacion para levantamiento topografico.
     *
     * @author andres.eslava
     */
    public void asignarAjusteParaLevantamiento() {
        try {
            if (AjusteManzanaAreasSeleccionadas.length > 0) {
                for (int i = 0; i < AjusteManzanaAreasSeleccionadas.length; i++) {
                    ActualizacionLevantamiento unaActualizacionLevantamiento =
                        new ActualizacionLevantamiento();
                    unaActualizacionLevantamiento.setUsuarioLog(this.usuario.getLogin());
                    unaActualizacionLevantamiento.setFechaLog(new Date(System.currentTimeMillis()));
                    unaActualizacionLevantamiento.setIdentificador(
                        AjusteManzanaAreasSeleccionadas[i].getIdentificador());
                    unaActualizacionLevantamiento.setActualizacion(this.actualizacionAsociada);

                    this.getActualizacionService().guardaActualizacionLevantamiento(
                        unaActualizacionLevantamiento);
                }
            } else {
                this.addMensajeError(
                    "Debe seleccionar almenos una manzana para ordenar el levantamiento");
                LOGGER.warn("Debe seleccionar almenos una manzana para ordenar el levantamiento");
            }
        } catch (Exception e) {
            this.addMensajeError("Error en la asignacion de manzana para levantamiento");
            LOGGER.error("Error en la asignacion de manzana para levantamiento", e);
        }

    }
}
