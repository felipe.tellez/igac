/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoParticipante;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluoPonente;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del cu Registro Acta comite
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-091 Registro Acta comite
 */
@Component("convocarComiteRegistrarActa")
@Scope("session")
public class ConvocatoriaComiteRegistroActaMB extends SNCManagedBean {

    private static final long serialVersionUID = -8798721179674715931L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConvocatoriaComiteRegistroActaMB.class);

    /**
     * Avaluos al que se le quiere registrar el acta
     */
    private Avaluo avaluo;

    /**
     * Representa el usuario activo en sesión
     */
    private UsuarioDTO usuario;

    @Autowired
    private IContextListener contextoWebApp;

    // --------------------------------Reportes---------------------------------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte de la orden de practica
     */
    private ReporteDTO actaReporte;

    /**
     * Archivo del acta de comité
     */
    private File actaDocumentoFile;
    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que determina el comité está iniciado (creado) o no
     */
    private boolean banderaComiteIniciado;

    /**
     * Lista de avaluadores de la territorial en sesion
     */
    private List<SelectItem> listaProfesionales;

    /**
     * Id del profesional seleccionado para adicion o eliminación
     */
    private Long profesionalSeleccionadoId;

    /**
     * Cadena de texto con la actividad a realizar por el {@link #profesionalSeleccionadoId} en el
     * comite, debe correspodner aun registro del dominio AVALUO_COMITE_PROF_ACTI
     */
    private String actividadProfesional;

    /**
     * Variable para almacenar la fecha del comité
     */
    private Date fechaComite;

    /**
     * Variable con los datos del comité
     */
    private ComiteAvaluo comite;

    /**
     * ponentes del {@link #comite}
     */
    private List<ComiteAvaluoPonente> ponentes;

    /**
     * Participantes del {@link #comite}
     */
    private List<ComiteAvaluoParticipante> participantes;

    /**
     * Lista con los profesionales asociados a la territorial
     */
    private List<ProfesionalAvaluo> profesionalesTerritorial;

    /**
     * Participante seleccionado
     */
    private ComiteAvaluoParticipante participanteSeleccionado;

    // ----------------------------Métodos SET y GET----------------------------
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public List<SelectItem> getListaProfesionales() {
        return this.listaProfesionales;
    }

    public Long getProfesionalSeleccionadoId() {
        return this.profesionalSeleccionadoId;
    }

    public void setProfesionalSeleccionadoId(Long profesionalSeleccionadoId) {
        this.profesionalSeleccionadoId = profesionalSeleccionadoId;
    }

    public String getActividadProfesional() {
        return this.actividadProfesional;
    }

    public void setActividadProfesional(String actividadProfesional) {
        this.actividadProfesional = actividadProfesional;
    }

    public Date getFechaComite() {
        return this.fechaComite;
    }

    public void setFechaComite(Date fechaComite) {
        this.fechaComite = fechaComite;
    }

    public ComiteAvaluo getComite() {
        return this.comite;
    }

    public void setComite(ComiteAvaluo comite) {
        this.comite = comite;
    }

    public List<ComiteAvaluoPonente> getPonentes() {
        return this.ponentes;
    }

    public List<ComiteAvaluoParticipante> getParticipantes() {
        return this.participantes;
    }

    public ComiteAvaluoParticipante getParticipanteSeleccionado() {
        return this.participanteSeleccionado;
    }

    public void setParticipanteSeleccionado(
        ComiteAvaluoParticipante participanteSeleccionado) {
        this.participanteSeleccionado = participanteSeleccionado;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getActaReporte() {
        return this.actaReporte;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaComiteIniciado() {
        return this.banderaComiteIniciado;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.extraerDatosProcess();

        this.determinarSiEsCreacionOFinalizacionComite();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que en un futuro deberá leer los datos del arbol del proceso y extrae el sec radicado
     * con el que se va a realizar el comite
     *
     * @author christian.rodriguez
     */
//TODO :: christian.rodriguez :: ¿para qué un método que lo único que hace es llamar a otro? :: pedro.garcia
    private void extraerDatosProcess() {

        this.inicializarDatosQuemados();
    }

//TODO :: christian.rodriguez :: ¿para qué un método con una únicá sentencia y que solo se llama en un sitio una vez? :: pedro.garcia
    private void inicializarDatosQuemados() {
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(
            172L);
    }

    // -------------------------------------------------------------------------
    /**
     * Método que determina si se está creando el comité o si a se está finalizando dependiento de
     * si existe un {@link ComiteAvaluo} asociado al {@link #avaluo} en base o no. Si no se ha
     * creado el comite, se inicializa el {@link #comite}
     *
     * @author christian.rodriguez
     */
    private void determinarSiEsCreacionOFinalizacionComite() {

        if (this.avaluo != null) {

            this.comite = this.getAvaluosService()
                .consultarComiteAvaluoPorIdAvaluo(this.avaluo.getId());

            if (this.comite == null) {

                this.ponentes = new ArrayList<ComiteAvaluoPonente>();
                this.participantes = new ArrayList<ComiteAvaluoParticipante>();

                this.inicializarObjetoComite();
                this.banderaComiteIniciado = false;

            } else {

                this.ponentes = this.comite.getComiteAvaluoPonentes();
                this.participantes = this.comite.getComiteAvaluoParticipantes();

                this.banderaComiteIniciado = true;
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que inicia el objeto {@link #comite} con lso valores por defecto. Tambien carga en la
     * lista {@link #ponentes} los avaluadores asociados al {@link #avaluo}
     *
     * @author christian.rodriguez
     */
    private void inicializarObjetoComite() {

        this.comite = new ComiteAvaluo();

        for (ProfesionalAvaluo profesional : this.avaluo.getAvaluadores()) {

            ComiteAvaluoPonente ponente = new ComiteAvaluoPonente();
            ponente.setProfesionalAvaluos(profesional);

            this.ponentes.add(ponente);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Carga los combos requeridos por la ventana de adicionar participante al comité
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaAdicionarParticipante() {
        this.cargarAvaluadoresTerritorial();
    }

    // -------------------------------------------------------------------------
    /**
     * Carga los combos requeridos por la ventana para cargar el acta firmada
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaCargarActaFirmada() {
        this.actaReporte = null;
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botoón 'Adicionar participantes' del dialogo de adicionad participante dahh!!!
     *
     * @author christian.rodriguez
     */
//TODO :: christian.rodriguez :: los comentarios que no sean de documentación ("dahh!!!") van entre /* ... */.
    // los métodos listener deben -por estándar- tener esa palabra en el nombre :: pedro.garcia
    public void adicionarParticipante() {

        if (this.actividadProfesional != null &&
             this.profesionalSeleccionadoId != null) {

            if (!this
                .buscarParticipanteEnListaParticipantes(profesionalSeleccionadoId)) {

                ComiteAvaluoParticipante participante = new ComiteAvaluoParticipante();
                participante.setActividad(this.actividadProfesional);

                ProfesionalAvaluo profesionalAAgregar = this
                    .buscarProfesionalEnListaProfesionalesTerritorial(this.profesionalSeleccionadoId);
                participante.setProfesionalAvaluos(profesionalAAgregar);
                participante.setNombre(profesionalAAgregar.getNombreCompleto());

                this.participantes.add(participante);

                this.addMensajeInfo(
                    "Profesional agregado a la lista de participantes del comité satisfactoriamente.");
            } else {
                this.addMensajeError(
                    "El profesional ya se encuentra en la lista de participantes del comité.");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón de eliminar participante, se encarga de eliminar el
     * {@link #participanteSeleccionado} de la lista de {@link #participantes}
     *
     * @author christian.rodriguez
     */
    public void eliminarParticipante() {

        if (this.participanteSeleccionado != null) {

            if (this.participantes.remove(this.participanteSeleccionado)) {
                this.addMensajeInfo(
                    "Profesional eliminado de la lista de participantes del comité satisfactoriamente.");
            } else {
                this.addMensajeError(
                    "El profesional no se encuentra en la lista de participantes del comité.");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que verifica si un participante se encuentra en la lsita de participantes o no
     *
     * @author christian.rodriguez
     * @param profesionalSeleccionadoId identificador del profesional a buscar
     * @return true si el participante ya está en la lista, false si no
     */
    private boolean buscarParticipanteEnListaParticipantes(
        Long profesionalSeleccionadoId) {
        for (ComiteAvaluoParticipante participante : this.participantes) {
            if (participante.getProfesionalAvaluos().getId() == profesionalSeleccionadoId) {
                return true;
            }
        }
        return false;
    }

    // -------------------------------------------------------------------------
    /**
     * Método utilitario que busca en la lista {@link #profesionalesTerritorial} el que corresponda
     * con el {@link #profesionalSeleccionadoId}
     *
     * @author christian.rodriguez
     * @param profesionalSeleccionadoId id del profesional seleccionado
     * @return {@link ProfesionalAvaluo} que corresponde al id enviado como parametro
     */
    private ProfesionalAvaluo buscarProfesionalEnListaProfesionalesTerritorial(
        Long profesionalSeleccionadoId) {

        for (ProfesionalAvaluo profesional : this.profesionalesTerritorial) {
            if (profesional.getId() == profesionalSeleccionadoId) {
                return profesional;
            }
        }
        return null;
    }

    // ----------------------------------------------------------------------
    /**
     * Método que se encarga de enviar la invitación a los participantes del comite por correo
     * electrónico
     *
     * @author christian.rodriguez
     */
    public void comunicarParticipantesYGuardarComite() {

        if (this.participantes != null && !this.participantes.isEmpty()) {

            if (this.guardarComiteAvaluo()) {
                this.comunicarParticipantes();
            }
        } else {
            this.addMensajeError(
                "No se pudo encontrario participantes para el envio del comunicado por correo electrónico.");
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Se encarga de enviar un correo electrónico a los participantes del comité
     *
     * @author christian.rodriguez
     */
    private void comunicarParticipantes() {
        List<String> destinatarios = new ArrayList<String>();

        for (ComiteAvaluoParticipante participante : this.participantes) {
            destinatarios.add(participante.getProfesionalAvaluos()
                .getCorreoElectronico());
        }

        Object[] datosCorreo = new Object[6];
        datosCorreo[0] = this.avaluo.getSecRadicado();
        datosCorreo[1] = this.comite.getFecha();
        datosCorreo[2] = this.comite.getLugar();
        datosCorreo[3] = this.comite.getCadenaNombresPonentes();
        datosCorreo[4] = this.usuario.getNombreCompleto();
        datosCorreo[5] = this.usuario.getRolesCadena();

        boolean notificacionExitosa = this.getGeneralesService()
            .enviarCorreoElectronicoConCuerpo(
                EPlantilla.MENSAJE_CONVOCATORIA_PONENCIA_AVALUO,
                destinatarios, null, null, datosCorreo, null, null);

        if (notificacionExitosa) {
            this.addMensajeInfo("Se envió exitosamente el comunicado por correo electrónico.");
        } else {
            this.addMensajeError("No se pudo enviar el comunicado por correo electrónico.");
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Método encargado de guardar en la bd el {@link #comite} con sus {@link #participantes} y
     * {@link #ponentes}
     *
     * @author christian.rodriguez
     */
    private boolean guardarComiteAvaluo() {

        if (this.comite != null && this.participantes != null &&
             !this.participantes.isEmpty() && this.ponentes != null &&
             !this.ponentes.isEmpty()) {

            this.comite.setComiteAvaluoParticipantes(this.participantes);
            this.comite.setComiteAvaluoPonentes(this.ponentes);

            if (this.comite.getNumeroActa() == null) {
                this.comite.setNumeroActa(this.getAvaluosService()
                    .obtenerNumeroActaComiteAvaluos());
            }

            this.comite = this.getAvaluosService()
                .guardarActualizarComiteAvaluo(this.comite,
                    this.avaluo.getId(), this.usuario);

            if (this.comite != null) {
                this.addMensajeInfo("Comité guardado satisfactoriamente.");
                this.determinarSiEsCreacionOFinalizacionComite();
                return true;
            }
        }

        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("error", "error");
        this.addMensajeError(
            "No fue posible guardar el comité, asegurese de que haya seleccionado los ponentes y los participantes.");

        return false;

    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón 'Generar acta'. Se encarga de generar el documento del acta
     *
     * @author christian.rodriguez
     */
    public void generarActa() {
        this.guardarComiteAvaluo();
        this.generarDocumentoActa();
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón 'Finalizar y avanzar' se encarga de marcar el acta como final y finalizar
     * el proceso
     *
     * @author christian.rodriguez
     */
//TODO :: christian.rodriguez :: ¿este va a ser listener? :: pedro.garcia
    public void finalizarYAvanzar() {

    }

    // -------------------------------------------------------------------------
    /**
     * Listener del fileupload. Se encarga de crear un objeto {@link File} para la carga del acta
     * firmada
     *
     * @author christian.rodriguez
     * @param eventoCarga
     */
    public void cargarActaFirmada(FileUploadEvent eventoCarga) {

        this.actaReporte = null;

        Object[] cargaArchivo = UtilidadesWeb.cargarArchivo(eventoCarga,
            this.contextoWebApp);

        if (cargaArchivo[0] != null) {

            this.actaDocumentoFile = (File) cargaArchivo[0];

            this.addMensajeInfo("Archivo cargado satisfactoriamente");

        } else {
            LOGGER.error("ERROR: " + cargaArchivo[2]);
            this.addMensajeError((String) cargaArchivo[1]);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método encargado de guardar el acta firmada y guardarla en db y alfresco si ya existia un
     * acta firmada, esta será reemplazada
     *
     * @author christian.rodriguez
     */
    public void cargarActaFirmadaYGuardar() {

        boolean errorValidacion = false;

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.actaDocumentoFile == null) {
            context.addCallbackParam("error", "error");
            this.addMensajeError("Debe cargar un documento pdf con el acta del comité firmada.");
            errorValidacion = true;
        }

        boolean huboAlgunAsistente = false;
        for (ComiteAvaluoParticipante participante : this.participantes) {
            if (participante.isAsisteBoolean()) {
                huboAlgunAsistente = true;
                break;
            }
        }

        if (!huboAlgunAsistente) {
            context.addCallbackParam("error", "error");
            this.addMensajeError(
                "Debe haber asistido algún participante al comité para poder cargar el acta.");
            errorValidacion = true;
        }

        if (!errorValidacion) {

            Documento actaDocumento = this.guardarActualizarDocumento(
                this.comite.getActaFirmadaDocumentoId(),
                this.actaDocumentoFile.getName(),
                this.actaDocumentoFile.getPath());

            if (actaDocumento == null) {
                this.addMensajeError("No se pudo guardar el acta del comité.");
            } else {
                this.comite.setActaFirmadaDocumentoId(actaDocumento.getId());
                this.guardarComiteAvaluo();
                this.cargarDocumentoDeActaDeComite(actaDocumento.getId());
            }
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Genera los valores para un combo box (selectOneMenu) con los profesionales de avalúos
     * asociados a una territorial
     *
     * @author christian.rodriguez
     */
    public void cargarAvaluadoresTerritorial() {

        this.listaProfesionales = new ArrayList<SelectItem>();
        this.listaProfesionales.add(new SelectItem(null, this.DEFAULT_COMBOS));

        this.profesionalesTerritorial = this.getAvaluosService()
            .buscarAvaluadoresPorTerritorial(
                this.usuario.getCodigoTerritorial());
        for (ProfesionalAvaluo profesional : profesionalesTerritorial) {
            this.listaProfesionales.add(new SelectItem(profesional.getId(),
                profesional.getNombreCompleto()));
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que genera el documento del acta de comité
     *
     * @author christian.rodriguez
     */
    public void generarDocumentoActa() {

        this.actaReporte = null;

        RequestContext context = RequestContext.getCurrentInstance();

        if (!this.generarReporte()) {
            context.addCallbackParam("error", "error");
            this.addMensajeError("No se pudo generar el acta del comité.");
            return;
        }

        Documento actaDocumento = this.guardarActualizarDocumento(
            this.comite.getActaDocumentoId(),
            this.actaReporte.getNombreReporte(),
            this.actaReporte.getRutaCargarReporteAAlfresco());

        if (actaDocumento == null) {
            context.addCallbackParam("error", "error");
            this.addMensajeError("No se pudo guardar el acta del comité.");
            return;
        } else {
            this.comite.setActaDocumentoId(actaDocumento.getId());
            this.guardarComiteAvaluo();
        }

        this.cargarDocumentoDeActaDeComite(actaDocumento.getId());
    }

    // -------------------------------------------------------------------------
    /**
     * Método que genera el reporte del acta de comité
     *
     * @author christian.rodriguez
     */
    private boolean generarReporte() {

        boolean reporteGenerado = false;

        // TODO::christian.rodriguez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        // TODO::christian.rodriguez :: Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_RADICADO", "");

        this.actaReporte = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        reporteGenerado = true;

        return reporteGenerado;

    }

    // -------------------------------------------------------------------------
    /**
     * Genera el {@link Documento} del acta de comité y lo guarda en la BD y en Alfresco, si el
     * documento ya existe se reemplaza en alfresco y se actualiza en db
     *
     * @author christian.rodriguez
     * @param idDocumentoActa identificador del {@link Documento} a actualizar
     */
    private Documento guardarActualizarDocumento(Long idDocumentoActa,
        String nombreArchivo, String pathArchivo) {

        Documento documentoActa = null;

        if (idDocumentoActa != null) {

            // Actualización de acta existente
            documentoActa = this.getGeneralesService()
                .buscarDocumentoPorIdConTipoDocumento(idDocumentoActa);

            this.getGeneralesService().borrarDocumentoEnAlfresco(
                documentoActa.getIdRepositorioDocumentos());
        } else {

            // Creación de acta por primera vez
            documentoActa = this.inicializarObjetoActaDocumento(nombreArchivo);
            documentoActa = this.getGeneralesService().guardarDocumento(
                documentoActa);
            documentoActa = this
                .getGeneralesService()
                .buscarDocumentoPorIdConTipoDocumento(documentoActa.getId());
        }

        documentoActa = this.guardarDocumentoEnGestorDocumental(documentoActa,
            documentoActa.getTipoDocumento(), pathArchivo);

        documentoActa = this.getGeneralesService().guardarDocumento(
            documentoActa);

        return documentoActa;

    }

    // -------------------------------------------------------------------------
    /**
     * Método encargado de crear un nuebo objeto de tipo {@link Documento} con los valores para
     * guardar un acta de comite
     *
     * @author christian.rodriguez
     * @return {@link Documento} creado
     */
    private Documento inicializarObjetoActaDocumento(String nombreArchivo) {

        TipoDocumento tipoDocumentoOP = new TipoDocumento();
        tipoDocumentoOP.setId(ETipoDocumento.AVALUOS_ACTA_COMITE.getId());
        tipoDocumentoOP.setNombre(ETipoDocumento.AVALUOS_ACTA_COMITE.getNombre());

        Documento actaDocumento;
        actaDocumento = new Documento();
        actaDocumento.setArchivo(nombreArchivo);
        actaDocumento.setTipoDocumento(tipoDocumentoOP);
        actaDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        actaDocumento.setBloqueado(ESiNo.NO.getCodigo());
        actaDocumento.setUsuarioCreador(this.usuario.getLogin());
        actaDocumento.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        actaDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        actaDocumento.setUsuarioLog(this.usuario.getLogin());
        actaDocumento.setDescripcion("Documento del actá de comité del avalúo " +
             this.avaluo.getSecRadicado());

        return actaDocumento;
    }

    // -------------------------------------------------------------------------
    /**
     * Método para guardar un documento de avalúos en el gestor de documentos
     *
     * @author christian.rodriguez
     * @param documento {@link Documento} a guardar
     * @param tipoDocumento {@link TipoDocumento} del documento a guardar
     * @return {@link Documento} guardado con el id del gestor de documentos
     */
    private Documento guardarDocumentoEnGestorDocumental(Documento documento,
        TipoDocumento tipoDocumento, String pathArchivo) {

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(this.avaluo
                .getTramite().getSolicitud().getNumero(), this.avaluo
                    .getTramite().getSolicitud().getFecha(),
                tipoDocumento.getNombre(), pathArchivo);

        documento = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, documento);

        if (documento != null && documento.getIdRepositorioDocumentos() == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }
        return documento;

    }

    // ----------------------------------------------------------------------
    /**
     * Método que se encarga de recuperar el documento de liquidación de costos de los
     * {@link #avaluosALiquidar} seleccionado para ser visualizado
     *
     * @author christian.rodriguez
     */
    private void cargarDocumentoDeActaDeComite(Long idDocumento) {

        Documento actaDocumento = this.getGeneralesService()
            .buscarDocumentoPorId(idDocumento);

        this.actaReporte = this.reportsService.consultarReporteDeGestorDocumental(
            actaDocumento.getIdRepositorioDocumentos());

        if (this.actaReporte == null) {

            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "La orden prática asociada no pudo ser cargada.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Listener usado para cargar el documento del acta firmada
     *
     * @author christian.rodriguez
     */
    public void cargarActaFirmada() {
        this.cargarDocumentoDeActaDeComite(this.comite
            .getActaFirmadaDocumentoId());
    }

    // ----------------------------------------------------------------------
    /**
     * Listener usado para cargar el documento del acta
     *
     * @author christian.rodriguez
     */
    public void cargarActa() {
        this.cargarDocumentoDeActaDeComite(this.comite.getActaDocumentoId());
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }
}
