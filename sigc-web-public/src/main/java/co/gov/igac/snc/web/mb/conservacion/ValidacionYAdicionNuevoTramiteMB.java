package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EDatoRectificarTipo;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EErroresDatosRectificar;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacion5Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETablaDatoRectificar;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.TramitePredioEnglobeDTO;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB generico para hacer validaciones cuando se va a crear un trámite nuevo y agrupa los métodos
 * comunes que se usan para adicionar el trámite nuevo
 *
 * @author javier.aponte
 *
 */
@Component("validacionYAdicionNuevoTramite")
@Scope("session")
public class ValidacionYAdicionNuevoTramiteMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 1657904661100413465L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ValidacionYAdicionNuevoTramiteMB.class);

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    /**
     * Trámite a validar y a adicionar
     */
    private Tramite nuevoTramiteSeleccionado;

    /**
     * Trámite inicial, este trámite se usa cuando se desea cancelar un trámite y se necesita
     * validar el trámite que se cancela con el nuevo trámite que se desea adicionar
     */
    private Tramite tramiteInicial;

    /**
     * Documentación asociada al trámite inicial
     */
    private List<TramiteDocumentacion> tramiteDocumentacionDeTramiteInicial;

    /**
     * Lista de trámites asociados al trámite inicial
     */
    private List<Tramite> tramitesAsociadosATramiteInicial;

    /**
     * Arreglo de trámites asociados al trámite inicial seleccionados
     */
    private Tramite[] tramitesAsociadosATramiteInicialSeleccionados;

    /**
     * Lista auxiliar con los DatoRectificar de la tabla datos por rectificar o complementar
     */
    private List<DatoRectificar> selectedDatosRectificarOComplementarDataTable;

    /**
     * lista con los DatoRectificar que se han seleccionado en el pick list
     */
    private List<DatoRectificar> selectedDatosRectifComplPL;

    /**
     * Lista que contiene los trámite predio englobe asociados a un nuevo trámite adicionado como se
     * está utilizando, esta variable contiene todos los predios asociados al trámite
     * independientemente de que sea o no un trámite de englobe
     */
    private List<TramitePredioEnglobe> tramitesPredioEnglobeNuevoTramiteAdicionado;

    /**
     * Lista que contiene los trámite predio englobe DTO asociados a un nuevo trámite adicionado
     * como se está utilizando, esta variable contiene todos los predios asociados al trámite
     * independientemente de que sea o no un trámite de englobe
     */
    private List<TramitePredioEnglobeDTO> tramitesPredioEnglobeNuevoTramiteAdicionadoDTO;

    /**
     * Objeto temporal de seleccion en la tabla predioTramite
     */
    private TramitePredioEnglobeDTO selectedPredioTramite;

    /**
     * Objeto temporal de seleccion en la tabla predioColindantes
     */
    private Predio predioRectificacion;

    /**
     * Solicitud que se encuentra seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Lista de Tramites de via gubernativa
     */
    private List<Tramite> tramitesGubernativaAll;

    /**
     * Lista que contiene los predios colindantes para englobe asociados al predio seleccionado
     */
    private List<Predio> listaGlobalPrediosColindantes;

    /**
     * Variable que contiene el usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Partes del número predial para mutaciones de segunda o quinta
     */
    private String numeroPredialParte1;
    private String numeroPredialParte2;
    private String numeroPredialParte3;
    private String numeroPredialParte4;
    private String numeroPredialParte5;
    private String numeroPredialParte6;
    private String numeroPredialParte7;
    private String numeroPredialParte8;
    private String numeroPredialParte9;
    private String numeroPredialParte10;
    private String numeroPredialParte11;
    private String numeroPredialParte12;
    private String areaTerreno;

    /**
     * Bandera para mostrar el tramite asociado sin permitir editarlo
     */
    private boolean banderaVerTramitePorAsociar;

    /**
     * Bandera de visualización de los documentos para asociar al trámite
     */
    private boolean banderaAsociarDocumentos;

    /**
     * bandera para indicar si el subtipo de trámite es dato requerido
     */
    private boolean subtipoTramiteEsRequerido;

    /**
     * Bandera que determina si el subtipo de la mutación de segunda es englobe
     */
    private boolean banderaSubtipoEnglobe;

    /**
     * Bandera que determina si se selecciono una clase de mutación
     */
    private boolean banderaClaseDeMutacionSeleccionada;

    /**
     * Bandera que determina si el subtipo de la mutación de quinta es nuevo
     */
    private boolean banderaSubtipoNuevo;

    /**
     * Bandera que indica si es tramiteTramite seleccionado es nuevo o va a ser editado
     */
    private boolean banderaEdicionTramiteTramite;

    /**
     * bandera que indica si un documento (requerido/adicional) se encuentra en edición o es de
     * lectura
     */
    private boolean editandoDocumentacion;

    /**
     * Bandera de solicitud de documentos adicionales
     */
    private boolean banderaDocumentoAdicional;

    /**
     * Bandera para saber si se está editando un documento adicional
     */
    private boolean banderaEditandoDocumentoAdicional;

    /**
     * lista con los valores del combo de tipos de trámite que se permiten para el tipo de solicitud
     * creada
     */
    private ArrayList<SelectItem> tiposTramitePorSolicitud;

    /**
     * Lista que almacena los tipos de Mutaciones
     */
    private List<SelectItem> mutacionClases;

    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;

    /**
     * Variable contadora de la asignaciónd de orden para los trámites seleccionados
     */
    private List<SelectItem> ordenEjecucionTramites;

    /**
     * Lista que contiene los predios colindantes para englobe asociados al predio seleccionado
     */
    private List<Predio> listaPrediosColindantes;

    /**
     * Objeto temporal de seleccion en la tabla predioColindantes
     */
    private Predio selectedPredioColindante;

    // ------------rectificación/complementación---------------------------------------------------
    /**
     * tipo de dato para rectificación o complementación seleccionado
     */
    private String tipoDatoRectifComplSeleccionado;

    /**
     * modelo del pick list de datos a modificar o complementar Se requieren dos listas: una fuente
     * y una destino
     */
    private DualListModel<DatoRectificar> datosRectifComplDLModel;

    private List<DatoRectificar> datosRectifComplSource;
    private List<DatoRectificar> datosRectifComplTarget;

    /**
     * lista con los DatoRectificar que se han seleccionado en el data table de resumen
     */
    private DatoRectificar[] selectedDatosRectifComplDT;

    /**
     * Lista de documentación adicional presente en el trámite seleccionado
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * Arreglo que contiene la documentación adicional para asociarla a la lista de trámites
     * asociados
     */
    private TramiteDocumentacion[] documentacionAdicionalSeleccionada;

    /**
     * Arreglo que contiene la documentación aportada para asociarla a la lista de trámites
     * asociados
     */
    private TramiteDocumentacion[] documentacionAportada;

    /**
     * Documento temporal de trámite
     */
    private TramiteDocumentacion documentoSeleccionado;

    /**
     * Datos geográficos del documento
     */
    private List<Departamento> dptosDocumento;
    private List<Municipio> munisDocumento;
    private Departamento departamentoDocumento;
    private Municipio municipioDocumento;
    private List<SelectItem> municipiosDocumento;
    private List<SelectItem> departamentosDocumento;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * Ruta del documento cargado
     */
    private String rutaMostrar;

    /**
     * nombre temporal del archivo cargado
     */
    private String nombreTemporalArchivo;

    /**
     * bandera para saltar validaciones si son predios fiscales
     */
    private boolean banderaPredioFiscal;
//-------------------------------------Getters and Setters------------------------------------

    public Tramite getNuevoTramiteSeleccionado() {
        return nuevoTramiteSeleccionado;
    }

    public void setNuevoTramiteSeleccionado(Tramite nuevoTramiteSeleccionado) {
        this.nuevoTramiteSeleccionado = nuevoTramiteSeleccionado;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public Tramite getTramiteInicial() {
        return tramiteInicial;
    }

    public void setTramiteInicial(Tramite tramiteInicial) {
        this.tramiteInicial = tramiteInicial;
    }

    public List<DatoRectificar> getSelectedDatosRectificarOComplementarDataTable() {
        return selectedDatosRectificarOComplementarDataTable;
    }

    public void setSelectedDatosRectificarOComplementarDataTable(
        List<DatoRectificar> selectedDatosRectificarOComplementarDataTable) {
        this.selectedDatosRectificarOComplementarDataTable =
            selectedDatosRectificarOComplementarDataTable;
    }

    public List<TramitePredioEnglobe> getTramitesPredioEnglobeNuevoTramiteAdicionado() {
        return tramitesPredioEnglobeNuevoTramiteAdicionado;
    }

    public void setTramitesPredioEnglobeNuevoTramiteAdicionado(
        List<TramitePredioEnglobe> tramitesPredioEnglobeNuevoTramiteAdicionado) {
        this.tramitesPredioEnglobeNuevoTramiteAdicionado =
            tramitesPredioEnglobeNuevoTramiteAdicionado;
    }

    public List<TramitePredioEnglobeDTO> getTramitesPredioEnglobeNuevoTramiteAdicionadoDTO() {
        return tramitesPredioEnglobeNuevoTramiteAdicionadoDTO;
    }

    public void setTramitesPredioEnglobeNuevoTramiteAdicionadoDTO(
        List<TramitePredioEnglobeDTO> tramitesPredioEnglobeNuevoTramiteAdicionadoDTO) {
        this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
            tramitesPredioEnglobeNuevoTramiteAdicionadoDTO;
    }

    public String getAreaTerreno() {
        return areaTerreno;
    }

    public void setAreaTerreno(String areaTerreno) {
        this.areaTerreno = areaTerreno;
    }

    public Predio getPredioRectificacion() {
        return predioRectificacion;
    }

    public void setPredioRectificacion(Predio predioRectificacion) {
        this.predioRectificacion = predioRectificacion;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Tramite> getTramitesGubernativaAll() {
        return tramitesGubernativaAll;
    }

    public void setTramitesGubernativaAll(List<Tramite> tramitesGubernativaAll) {
        this.tramitesGubernativaAll = tramitesGubernativaAll;
    }

    public List<Predio> getListaGlobalPrediosColindantes() {
        return listaGlobalPrediosColindantes;
    }

    public void setListaGlobalPrediosColindantes(
        List<Predio> listaGlobalPrediosColindantes) {
        this.listaGlobalPrediosColindantes = listaGlobalPrediosColindantes;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getNumeroPredialParte1() {
        return numeroPredialParte1;
    }

    public void setNumeroPredialParte1(String numeroPredialParte1) {
        this.numeroPredialParte1 = numeroPredialParte1;
    }

    public String getNumeroPredialParte2() {
        return numeroPredialParte2;
    }

    public void setNumeroPredialParte2(String numeroPredialParte2) {
        this.numeroPredialParte2 = numeroPredialParte2;
    }

    public String getNumeroPredialParte3() {
        return numeroPredialParte3;
    }

    public void setNumeroPredialParte3(String numeroPredialParte3) {
        this.numeroPredialParte3 = numeroPredialParte3;
    }

    public String getNumeroPredialParte4() {
        return numeroPredialParte4;
    }

    public void setNumeroPredialParte4(String numeroPredialParte4) {
        this.numeroPredialParte4 = numeroPredialParte4;
    }

    public String getNumeroPredialParte5() {
        return numeroPredialParte5;
    }

    public void setNumeroPredialParte5(String numeroPredialParte5) {
        this.numeroPredialParte5 = numeroPredialParte5;
    }

    public String getNumeroPredialParte6() {
        return numeroPredialParte6;
    }

    public void setNumeroPredialParte6(String numeroPredialParte6) {
        this.numeroPredialParte6 = numeroPredialParte6;
    }

    public String getNumeroPredialParte7() {
        return numeroPredialParte7;
    }

    public void setNumeroPredialParte7(String numeroPredialParte7) {
        this.numeroPredialParte7 = numeroPredialParte7;
    }

    public String getNumeroPredialParte8() {
        return numeroPredialParte8;
    }

    public void setNumeroPredialParte8(String numeroPredialParte8) {
        this.numeroPredialParte8 = numeroPredialParte8;
    }

    public String getNumeroPredialParte9() {
        return numeroPredialParte9;
    }

    public void setNumeroPredialParte9(String numeroPredialParte9) {
        this.numeroPredialParte9 = numeroPredialParte9;
    }

    public String getNumeroPredialParte10() {
        return numeroPredialParte10;
    }

    public void setNumeroPredialParte10(String numeroPredialParte10) {
        this.numeroPredialParte10 = numeroPredialParte10;
    }

    public String getNumeroPredialParte11() {
        return numeroPredialParte11;
    }

    public void setNumeroPredialParte11(String numeroPredialParte11) {
        this.numeroPredialParte11 = numeroPredialParte11;
    }

    public String getNumeroPredialParte12() {
        return numeroPredialParte12;
    }

    public void setNumeroPredialParte12(String numeroPredialParte12) {
        this.numeroPredialParte12 = numeroPredialParte12;
    }

    public boolean isBanderaVerTramitePorAsociar() {
        return banderaVerTramitePorAsociar;
    }

    public void setBanderaVerTramitePorAsociar(boolean banderaVerTramitePorAsociar) {
        this.banderaVerTramitePorAsociar = banderaVerTramitePorAsociar;
    }

    public boolean isBanderaAsociarDocumentos() {
        return banderaAsociarDocumentos;
    }

    public void setBanderaAsociarDocumentos(boolean banderaAsociarDocumentos) {
        this.banderaAsociarDocumentos = banderaAsociarDocumentos;
    }

    public boolean isSubtipoTramiteEsRequerido() {
        return subtipoTramiteEsRequerido;
    }

    public void setSubtipoTramiteEsRequerido(boolean subtipoTramiteEsRequerido) {
        this.subtipoTramiteEsRequerido = subtipoTramiteEsRequerido;
    }

    public boolean isBanderaSubtipoEnglobe() {
        return banderaSubtipoEnglobe;
    }

    public void setBanderaSubtipoEnglobe(boolean banderaSubtipoEnglobe) {
        this.banderaSubtipoEnglobe = banderaSubtipoEnglobe;
    }

    public TramitePredioEnglobeDTO getSelectedPredioTramite() {
        return selectedPredioTramite;
    }

    public void setSelectedPredioTramite(
        TramitePredioEnglobeDTO selectedPredioTramite) {
        this.selectedPredioTramite = selectedPredioTramite;
    }

    public boolean isBanderaClaseDeMutacionSeleccionada() {
        return banderaClaseDeMutacionSeleccionada;
    }

    public void setBanderaClaseDeMutacionSeleccionada(
        boolean banderaClaseDeMutacionSeleccionada) {
        this.banderaClaseDeMutacionSeleccionada = banderaClaseDeMutacionSeleccionada;
    }

    public boolean isBanderaSubtipoNuevo() {
        return banderaSubtipoNuevo;
    }

    public void setBanderaSubtipoNuevo(boolean banderaSubtipoNuevo) {
        this.banderaSubtipoNuevo = banderaSubtipoNuevo;
    }

    public boolean isBanderaEdicionTramiteTramite() {
        return banderaEdicionTramiteTramite;
    }

    public void setBanderaEdicionTramiteTramite(boolean banderaEdicionTramiteTramite) {
        this.banderaEdicionTramiteTramite = banderaEdicionTramiteTramite;
    }

    public boolean isEditandoDocumentacion() {
        return editandoDocumentacion;
    }

    public void setEditandoDocumentacion(boolean editandoDocumentacion) {
        this.editandoDocumentacion = editandoDocumentacion;
    }

    public boolean isBanderaDocumentoAdicional() {
        return banderaDocumentoAdicional;
    }

    public void setBanderaDocumentoAdicional(boolean banderaDocumentoAdicional) {
        this.banderaDocumentoAdicional = banderaDocumentoAdicional;
    }

    public boolean isBanderaEditandoDocumentoAdicional() {
        return banderaEditandoDocumentoAdicional;
    }

    public void setBanderaEditandoDocumentoAdicional(
        boolean banderaEditandoDocumentoAdicional) {
        this.banderaEditandoDocumentoAdicional = banderaEditandoDocumentoAdicional;
    }

    public List<Predio> getListaPrediosColindantes() {
        return listaPrediosColindantes;
    }

    public void setListaPrediosColindantes(List<Predio> listaPrediosColindantes) {
        this.listaPrediosColindantes = listaPrediosColindantes;
    }

    public DatoRectificar[] getSelectedDatosRectifComplDT() {
        return selectedDatosRectifComplDT;
    }

    public void setSelectedDatosRectifComplDT(
        DatoRectificar[] selectedDatosRectifComplDT) {
        this.selectedDatosRectifComplDT = selectedDatosRectifComplDT;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public TramiteDocumentacion[] getDocumentacionAdicionalSeleccionada() {
        return documentacionAdicionalSeleccionada;
    }

    public void setDocumentacionAdicionalSeleccionada(
        TramiteDocumentacion[] documentacionAdicionalSeleccionada) {
        this.documentacionAdicionalSeleccionada = documentacionAdicionalSeleccionada;
    }

    public TramiteDocumentacion[] getDocumentacionAportada() {
        return documentacionAportada;
    }

    public void setDocumentacionAportada(
        TramiteDocumentacion[] documentacionAportada) {
        this.documentacionAportada = documentacionAportada;
    }

    public TramiteDocumentacion getDocumentoSeleccionado() {
        return documentoSeleccionado;
    }

    public void setDocumentoSeleccionado(TramiteDocumentacion documentoSeleccionado) {
        this.documentoSeleccionado = documentoSeleccionado;
    }

    public List<SelectItem> getDepartamentosDocumento() {
        return departamentosDocumento;
    }

    public void setDepartamentosDocumento(List<SelectItem> departamentosDocumento) {
        this.departamentosDocumento = departamentosDocumento;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public List<SelectItem> getMunicipiosDocumento() {
        return municipiosDocumento;
    }

    public void setMunicipiosDocumento(List<SelectItem> municipiosDocumento) {
        this.municipiosDocumento = municipiosDocumento;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public boolean isBanderaPredioFiscal() {
        return banderaPredioFiscal;
    }

    public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
        this.banderaPredioFiscal = banderaPredioFiscal;
    }

    @PostConstruct
    public void init() {
        this.datosRectifComplDLModel = new DualListModel<DatoRectificar>();
        this.banderaPredioFiscal = false;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Copia de método que está en EjecucionMB de pedro.garcia
     *
     * @author javier.aponte
     * @return
     */
    public ArrayList<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> answer = null;
        List<TipoSolicitudTramite> tiposTramite = null;
        SelectItem itemToAdd;

        if (this.tramiteInicial != null &&
             this.tramiteInicial.getSolicitud() != null) {
            tiposTramite = this.getTramiteService()
                .obtenerTiposDeTramitePorSolicitud(
                    ESolicitudTipo.AVISOS.getCodigo());
        }
        if (tiposTramite != null && !tiposTramite.isEmpty()) {
            answer = new ArrayList<SelectItem>();
            for (TipoSolicitudTramite tst : tiposTramite) {
                itemToAdd = new SelectItem(tst.getTipoTramite(),
                    tst.getTipoTramiteValor());
                answer.add(itemToAdd);
            }
        }
        this.tiposTramitePorSolicitud = answer;
        return this.tiposTramitePorSolicitud;
    }

    public void setTiposTramitePorSolicitud(ArrayList<SelectItem> tiposTramite) {
        this.tiposTramitePorSolicitud = tiposTramite;
    }

    public List<SelectItem> getMutacionClases() {

        List<SelectItem> mutClass = this.generalMB.getMutacionClase();
        this.mutacionClases = new LinkedList<SelectItem>();

        if (mutClass != null && !mutClass.isEmpty()) {
            for (SelectItem s : mutClass) {
                if (!s.getLabel().equals(EMutacionClase.CUARTA.getNombre()) &&
                    !s.getLabel().equals(EMutacionClase.QUINTA.getNombre())) {
                    this.mutacionClases.add(s);
                }
            }
        }

        return mutacionClases;
    }

    public void setMutacionClases(List<SelectItem> mutacionClases) {
        this.mutacionClases = mutacionClases;
    }

    public List<SelectItem> getSubtipoClaseMutacionV() {
        return subtipoClaseMutacionV;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public Predio getSelectedPredioColindante() {
        return selectedPredioColindante;
    }

    public void setSelectedPredioColindante(Predio selectedPredioColindante) {
        this.selectedPredioColindante = selectedPredioColindante;
    }

    /**
     * Se usa este método porque en este MB se cambio el nombre del data table, pero en el xhtml
     * generico lo busca con el nombre viejo
     *
     * @return
     * @author javier.aponte
     */
    public List<DatoRectificar> getSelectedDatosRectifComplDataTable() {
        return selectedDatosRectificarOComplementarDataTable;
    }

// --------------------------------------------------------------------------------------------
    public DualListModel<DatoRectificar> getDatosRectifComplDLModel() {

        if (this.nuevoTramiteSeleccionado != null &&
             (this.nuevoTramiteSeleccionado.isRectificacion() || this.nuevoTramiteSeleccionado.
            isEsComplementacion())) {
            this.datosRectifComplSource = new ArrayList<DatoRectificar>();
            this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
            String naturaleza;

            if (this.nuevoTramiteSeleccionado != null &&
                 this.nuevoTramiteSeleccionado.isRectificacion()) {
                naturaleza = EDatoRectificarNaturaleza.RECTIFICACION
                    .geCodigo();
            } else {
                naturaleza = EDatoRectificarNaturaleza.COMPLEMENTACION
                    .geCodigo();
            }

            if (this.tipoDatoRectifComplSeleccionado != null &&
                 this.tipoDatoRectifComplSeleccionado.compareToIgnoreCase("") != 0 &&
                 naturaleza != null && !naturaleza.isEmpty()) {
                this.datosRectifComplSource = this.getTramiteService()
                    .obtenerDatosRectificarPorTipoYNaturaleza(
                        this.tipoDatoRectifComplSeleccionado, naturaleza);
            }

            this.datosRectifComplDLModel = new DualListModel<DatoRectificar>(
                this.datosRectifComplSource, this.datosRectifComplTarget);
        }
        return this.datosRectifComplDLModel;
    }

    public void setDatosRectifComplDLModel(
        DualListModel<DatoRectificar> datosRectifComplDLModel) {
        this.datosRectifComplDLModel = datosRectifComplDLModel;
    }

    public List<DatoRectificar> getDatosRectifComplSource() {
        return datosRectifComplSource;
    }

    public void setDatosRectifComplSource(
        List<DatoRectificar> datosRectifComplSource) {
        this.datosRectifComplSource = datosRectifComplSource;
    }

    public List<DatoRectificar> getDatosRectifComplTarget() {
        return datosRectifComplTarget;
    }

    public void setDatosRectifComplTarget(
        List<DatoRectificar> datosRectifComplTarget) {
        this.datosRectifComplTarget = datosRectifComplTarget;
    }

    public String getTipoDatoRectifComplSeleccionado() {
        return tipoDatoRectifComplSeleccionado;
    }

    public void setTipoDatoRectifComplSeleccionado(
        String tipoDatoRectifComplSeleccionado) {
        this.tipoDatoRectifComplSeleccionado = tipoDatoRectifComplSeleccionado;
    }

    public List<SelectItem> getOrdenEjecucionTramites() {
        return ordenEjecucionTramites;
    }

    public void setOrdenEjecucionTramites(List<SelectItem> ordenEjecucionTramites) {
        this.ordenEjecucionTramites = ordenEjecucionTramites;
    }

    public List<Tramite> getTramitesAsociadosATramiteInicial() {
        return tramitesAsociadosATramiteInicial;
    }

    public void setTramitesAsociadosATramiteInicial(
        List<Tramite> tramitesAsociadosATramiteInicial) {
        this.tramitesAsociadosATramiteInicial = tramitesAsociadosATramiteInicial;
    }

    public Tramite[] getTramitesAsociadosATramiteInicialSeleccionados() {
        return tramitesAsociadosATramiteInicialSeleccionados;
    }

    public void setTramitesAsociadosATramiteInicialSeleccionados(
        Tramite[] tramitesAsociadosATramiteInicialSeleccionados) {
        this.tramitesAsociadosATramiteInicialSeleccionados =
            tramitesAsociadosATramiteInicialSeleccionados;
    }
    //------------------------------METODOS----------------------------------------------------

    /**
     * Metodo que valida los datos que se van a rectificar, si encuentra un error en los datos,
     * muestra un mensaje y retorna null
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarDatosRectificacion() {

        if (this.selectedDatosRectificarOComplementarDataTable == null ||
             this.selectedDatosRectificarOComplementarDataTable.isEmpty()) {
            this.addMensajeError(EErroresDatosRectificar.NO_DATOS_RECTIFICAR.getMensaje());
            return false;
        }
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado == null ||
            this.tramitesPredioEnglobeNuevoTramiteAdicionado.isEmpty()) {
            this.addMensajeError(EErroresDatosRectificar.NO_PREDIO.getMensaje());
            return false;
        }
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado.size() > 1) {
            this.addMensajeError(EErroresDatosRectificar.VARIOS_PREDIOS.getMensaje());
            return false;
        }

        this.predioRectificacion = this.getConservacionService().
            obtenerPredioValidacionesRectificacionById(this.nuevoTramiteSeleccionado.getPredio().
                getId());

        for (DatoRectificar dato : this.selectedDatosRectificarOComplementarDataTable) {
            if (dato.getNombre().equals(EDatoRectificarNombre.AREA_TERRENO.getCodigo())) {
                if (this.predioRectificacion.isMejora()) {
                    this.addMensajeError(EErroresDatosRectificar.NO_AREA_MEJORA.getMensaje());
                    return false;
                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.FOTO.getCodigo())) {
                Boolean estadoFoto = this.validarFotos();
                if (estadoFoto != null && !estadoFoto) {
                    this.addMensajeError(EErroresDatosRectificar.NO_FOTO.getMensaje());
                    return false;
                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.COEFICIENTE.getCodigo())) {
                if (!this.predioRectificacion.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_7.getCodigo()) &&
                    !this.predioRectificacion.getCondicionPropiedad().equals(
                        EPredioCondicionPropiedad.CP_8.getCodigo()) &&
                    !this.predioRectificacion.getCondicionPropiedad().equals(
                        EPredioCondicionPropiedad.CP_9.getCodigo())) {
                    this.addMensajeError(EErroresDatosRectificar.CONDICION_7_8_9.getMensaje());
                    return false;
                }
            }
        }
        if (!this.validarDatosPropietario(EDatoRectificarNaturaleza.RECTIFICACION)) {
            return false;
        }
        if (!this.validarDatosPredio(EDatoRectificarNaturaleza.RECTIFICACION)) {
            return false;
        }

        return true;
    }

    /**
     * Método para validar si el predio tiene fotos asociadas para rectificar retorna falso si
     * ninguno de los componentes de las construcciones tiene fotos, verdadero si todos los
     * componentes tienen fotos y null si el dato foto no se va rectificar o complementar.
     *
     * @author felipe.cadena
     *
     * @return
     */
    public Boolean validarFotos() {

        int camposVacios = 0;
        int totalComps = 0;

        for (DatoRectificar dato : this.selectedDatosRectificarOComplementarDataTable) {
            if (dato.getNombre().equals(EDatoRectificarNombre.FOTO.getCodigo())) {

                for (UnidadConstruccion uc : this.predioRectificacion.getUnidadConstruccions()) {
                    for (UnidadConstruccionComp ucc : uc.getUnidadConstruccionComps()) {
                        List<Foto> fotosUcc = this.getConservacionService().
                            buscarFotoPorIdUnidadConstruccionYTipo(uc.getId(), ucc.getComponente());

                        totalComps++;
                        if (fotosUcc == null || fotosUcc.isEmpty()) {
                            camposVacios++;
                        }
                    }

                }
            }
        }

        if (camposVacios == totalComps) {
            return false;
        } else if (camposVacios == 0) {
            return true;
        } else {
            return null;
        }

    }

    /**
     * Metodo para validar los datos relacionados al propietario para tramites de rectificacion
     *
     * @author felipe.cadena
     * @param naturaleza
     * @return
     */
    public boolean validarDatosPropietario(EDatoRectificarNaturaleza naturaleza) {

        if (!this.validarTodosNombresPersona()) {
            this.addMensajeError(EErroresDatosRectificar.MODIFICAR_TODOS_NOMBRES.getMensaje());
            return false;
        }

        if (!this.validarCambiosNaturalAJuridica(naturaleza)) {
            return false;
        }

        List<DatoRectificar> estadoPropietario = this.determinarEstadoPropietario(naturaleza);

        for (DatoRectificar datoRectificar : this.selectedDatosRectificarOComplementarDataTable) {
            for (DatoRectificar datoEstado : estadoPropietario) {
                if (datoEstado.getId() == datoRectificar.getId()) {

                    if (naturaleza.equals(EDatoRectificarNaturaleza.RECTIFICACION)) {
                        if (!datoEstado.isRectificar()) {
                            this.mostrarMensajeError(datoEstado, naturaleza);
                            return false;
                        }
                    } else if (naturaleza.equals(EDatoRectificarNaturaleza.COMPLEMENTACION)) {
                        if (!datoEstado.isComplementar()) {
                            this.mostrarMensajeError(datoEstado, naturaleza);
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Metodo para validar los datos relacionados al predio para tramites de rectificacion.
     *
     * @author felipe.cadena
     *
     * @param naturaleza
     * @return
     */
    public boolean validarDatosPredio(EDatoRectificarNaturaleza naturaleza) {

        List<DatoRectificar> estadoPredio = this.determinarEstadoPredio(naturaleza);

        for (DatoRectificar datoRectificar : this.selectedDatosRectificarOComplementarDataTable) {
            for (DatoRectificar datoEstado : estadoPredio) {
                if (datoEstado.getId() == datoRectificar.getId()) {

                    if (naturaleza.equals(EDatoRectificarNaturaleza.RECTIFICACION)) {
                        if (!datoEstado.isRectificar()) {
                            this.mostrarMensajeError(datoEstado, naturaleza);
                            return false;
                        }
                    } else if (naturaleza.equals(EDatoRectificarNaturaleza.COMPLEMENTACION)) {
                        if (!datoEstado.isComplementar()) {
                            this.mostrarMensajeError(datoEstado, naturaleza);
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Muestra un mensaje al usuario con el error de validación.
     *
     * @author felipe.cadena
     * @param dato
     * @param naturaleza
     */
    public void mostrarMensajeError(DatoRectificar dato, EDatoRectificarNaturaleza naturaleza) {

        if (dato.isNoConstruccion()) {
            if (naturaleza.equals(EDatoRectificarNaturaleza.COMPLEMENTACION)) {
                this.
                    addMensajeError(EErroresDatosRectificar.NO_CONSTRUCCIONES_CONV_COM.getMensaje());
            } else {
                this.addMensajeError(EErroresDatosRectificar.NO_CONSTRUCCIONES_CONV.getMensaje());
            }
            return;
        }

        EErroresDatosRectificar[] errores = EErroresDatosRectificar.values();
        for (EErroresDatosRectificar err : errores) {
            if (err.getNombreCampo().equals(dato.getColumna()) &&
                err.getNaturaleza().equals(naturaleza.geCodigo())) {
                this.addMensajeError(err.getMensaje());
            }
        }

    }

    /**
     * Método para determinar que campos de los propietarios se pueden rectificar o complementar
     *
     * @author felipe.cadena
     * @param naturaleza
     * @return
     */
    public List<DatoRectificar> determinarEstadoPropietario(EDatoRectificarNaturaleza naturaleza) {

        List<DatoRectificar> estadoPersona = this.getTramiteService().
            obtenerDatosRectificarPorTipoYNaturaleza(EDatoRectificarTipo.PROPIETARIO.toString(),
                naturaleza.geCodigo());

        String nombreDato;
        Boolean estadoDato = null;

        for (DatoRectificar dato : estadoPersona) {

            String nombreCampo = "";
            nombreDato = dato.getColumna();
            String[] ds = nombreDato.split("_");
            for (int i = 0; i < ds.length; i++) {
                String string = ds[i];
                string = string.substring(0, 1) + string.substring(1).toLowerCase();
                nombreCampo += string;
            }

            if (dato.getTabla().equals(ETablaDatoRectificar.PERSONA_PREDIO_PROPIEDAD.getNombre()) &&
                 !this.validarExistenciaJustificacionPropiedad()) {
                dato.setComplementar(false);
                dato.setRectificar(false);
                continue;
            }

            estadoDato = this.validarCampoTabla(nombreCampo, dato.getTabla());

            if (estadoDato == null) {
                dato.setComplementar(true);
                dato.setRectificar(true);
            } else if (estadoDato) {
                dato.setComplementar(false);
                dato.setRectificar(true);
            } else {
                dato.setComplementar(true);
                dato.setRectificar(false);
            }
        }

        this.testMostrarEstado(estadoPersona);

        return estadoPersona;
    }

    /**
     * Método para determinar que campos del predio se pueden rectificar
     *
     * @author felipe.cadena
     * @return
     */
    public List<DatoRectificar> determinarEstadoPredio(EDatoRectificarNaturaleza naturaleza) {

        List<DatoRectificar> estadoPredio = this.getTramiteService().
            obtenerDatosRectificarPorTipoYNaturaleza(EDatoRectificarTipo.PREDIO.toString(),
                naturaleza.geCodigo());

        String nombreDato;
        Boolean estadoDato = null;

        for (DatoRectificar dato : estadoPredio) {

            String nombreCampo = "";
            nombreDato = dato.getColumna();
            String[] ds = nombreDato.split("_");
            for (int i = 0; i < ds.length; i++) {
                String string = ds[i];
                string = string.substring(0, 1) + string.substring(1).toLowerCase();
                nombreCampo += string;
            }

            if (dato.getNombre().equals(EDatoRectificarNombre.DIMENSION.getCodigo()) &&
                 (this.predioRectificacion.getUnidadesConstruccionNoConvencional().isEmpty() ||
                 this.predioRectificacion.getUnidadesConstruccionNoConvencional() == null)) {
                dato.setComplementar(false);
                dato.setRectificar(false);
                continue;
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.DIRECCION_PRINCIPAL.getCodigo())) {

                if (this.predioRectificacion.getPredioDireccions().size() > 1) {
                    dato.setComplementar(true);
                    dato.setRectificar(true);
                } else {
                    dato.setComplementar(false);
                    dato.setRectificar(false);
                }

                continue;
            }
            if (((dato.getTabla().equals(ETablaDatoRectificar.UNIDAD_CONSTRUCCION.getNombre()) ||
                 (dato.getTabla().equals(ETablaDatoRectificar.UNIDAD_CONSTRUCCION_COMP.getNombre()))) &&
                 (this.predioRectificacion.getUnidadesConstruccionConvencional().isEmpty() ||
                 this.predioRectificacion.getUnidadesConstruccionConvencional() == null)) &&
                 !dato.getNombre().equals(EDatoRectificarNombre.DIMENSION.getCodigo())) {

                dato.setComplementar(false);
                dato.setRectificar(false);
                dato.setNoConstruccion(true);
                continue;
                //felipe.cadena:: 23/07/2014 :: RM8759 :: Se deshabilita la validacion para rectificaciones de detalle de calificacion    
            } else if (dato.getNombre().equals(EDatoRectificarNombre.DETALLE_CALIFICACION.
                getCodigo())) {
                dato.setComplementar(true);
                dato.setRectificar(true);
                continue;
            }

            if (dato.getTabla().equals(ETablaDatoRectificar.FICHA_MATRIZ_PREDIO.getNombre())) {

                if (this.predioRectificacion.getFichaMatrizs() == null ||
                    this.predioRectificacion.getFichaMatrizs().isEmpty()) {
                    dato.setRectificar(false);
                    dato.setComplementar(true);
                } else {
                    dato.setComplementar(false);
                    dato.setRectificar(true);
                }
                continue;
            }

            estadoDato = this.validarCampoTablaPredios(nombreCampo, dato.getTabla());

            if (estadoDato == null) {
                dato.setComplementar(true);
                dato.setRectificar(true);
            } else if (estadoDato) {
                dato.setComplementar(false);
                dato.setRectificar(true);
            } else {
                dato.setComplementar(true);
                dato.setRectificar(false);
            }
        }

        this.testMostrarEstado(estadoPredio);
        return estadoPredio;
    }

    /**
     * Método para determinar si un campo de la información del predio puede rectificar o
     * complementar
     *
     * @author felipe.cadena
     * @param campo
     * @return true - rectificar <BR/>
     * false - complementar <BR/>
     * null - rectificar y complementar
     */
    private Boolean validarCampoTablaPredios(String campo, String tabla) {
        Method metodo;

        int nCamposVacios = 0;
        int pppVaciosTotal = 0;

        try {

            if (tabla.equals(ETablaDatoRectificar.PREDIO.getNombre())) {
                metodo = Predio.class.getDeclaredMethod("get" + campo);
                String valor = obtenerValorCampo(metodo, this.predioRectificacion);
                if (valor == null || valor.isEmpty()) {
                    return false;
                } else {
                    return true;
                }
            }
            if (tabla.equals(ETablaDatoRectificar.UNIDAD_CONSTRUCCION.getNombre())) {
                nCamposVacios = 0;
                for (UnidadConstruccion uc : this.predioRectificacion.getUnidadConstruccions()) {
                    metodo = UnidadConstruccion.class.getDeclaredMethod("get" + campo);
                    String valor = obtenerValorCampo(metodo, uc);
                    if (valor == null || valor.isEmpty()) {
                        nCamposVacios++;
                    }
                }
                if (nCamposVacios == 0) {

                } else if (nCamposVacios == this.predioRectificacion.getUnidadConstruccions().size()) {
                    return false;
                } else {
                    return null;
                }
            }
            if (tabla.equals(ETablaDatoRectificar.UNIDAD_CONSTRUCCION_COMP.getNombre())) {
                nCamposVacios = 0;
                pppVaciosTotal = 0;
                for (UnidadConstruccion uc : this.predioRectificacion.getUnidadConstruccions()) {
                    if (!uc.getUnidadConstruccionComps().isEmpty()) {
                        for (UnidadConstruccionComp ucc : uc.getUnidadConstruccionComps()) {
                            metodo = UnidadConstruccionComp.class.getDeclaredMethod("get" + campo);
                            String valor = obtenerValorCampo(metodo, ucc);
                            pppVaciosTotal++;
                            if (valor == null || valor.isEmpty()) {
                                nCamposVacios++;
                            }
                        }
                    } else {
                        pppVaciosTotal++;
                        nCamposVacios++;
                    }

                }
                if (nCamposVacios == 0) {
                    return true;
                } else if (nCamposVacios == pppVaciosTotal) {
                    return false;
                } else {
                    return null;
                }
            }
            if (tabla.equals(ETablaDatoRectificar.PREDIO_DIRECCION.getNombre())) {
                nCamposVacios = 0;
                if (this.predioRectificacion.getPredioDireccions() != null &&
                    !this.predioRectificacion.getPredioDireccions().isEmpty()) {
                    return null;
                } else {
                    return false;
                }
            }
            if (tabla.equals(ETablaDatoRectificar.PREDIO_ZONA.getNombre())) {
                nCamposVacios = 0;
                for (PredioZona pz : this.predioRectificacion.getPredioZonas()) {
                    metodo = PredioZona.class.getDeclaredMethod("get" + campo);
                    String valor = obtenerValorCampo(metodo, pz);
                    if (valor == null || valor.isEmpty()) {
                        nCamposVacios++;
                    }
                }
                if (nCamposVacios == 0) {
                    return true;
                } else if (nCamposVacios == this.predioRectificacion.getPredioDireccions().size()) {
                    return false;
                } else {
                    return null;
                }
            }

        } catch (Exception ex) {
            LOGGER.debug("Campo inexistente");
        }
        return true;
    }

    /**
     * Método para determinar si un campo de la lista de propietarios se puede rectificar o
     * complementar
     *
     * @author felipe.cadena
     * @param campo
     * @return true - rectificar false - complementar null - rectificar y complementar
     */
    private Boolean validarCampoTabla(String campo, String tabla) {
        Method metodo;

        int nPropietarios = this.predioRectificacion.getPersonaPredios().size();
        int nCamposVacios = 0;
        int pppVaciosTotal = 0;

        try {

            for (PersonaPredio pp : this.predioRectificacion.getPersonaPredios()) {

                if (tabla.equals(ETablaDatoRectificar.PERSONA.getNombre())) {
                    metodo = Persona.class.getDeclaredMethod("get" + campo);
                    String valor = this.obtenerValorCampo(metodo, pp.getPersona());
                    if (valor == null || valor.isEmpty()) {
                        nCamposVacios++;
                    }
                }
                if (tabla.equals(ETablaDatoRectificar.PERSONA_PREDIO.getNombre())) {
                    metodo = PersonaPredio.class.getDeclaredMethod("get" + campo);
                    String valor = this.obtenerValorCampo(metodo, pp);
                    if (valor == null || valor.isEmpty()) {
                        nCamposVacios++;
                    }
                }
                if (tabla.equals(ETablaDatoRectificar.PERSONA_PREDIO_PROPIEDAD.getNombre())) {
                    metodo = PersonaPredioPropiedad.class.getDeclaredMethod("get" + campo);

                    if (pp.getPersonaPredioPropiedads() == null) {
                        continue;
                    } else {
                        int pppVacios = 0;
                        for (PersonaPredioPropiedad ppp : pp.getPersonaPredioPropiedads()) {
                            if (campo.contains("Fecha") || campo.contains("Departamento") || campo.
                                contains("Municipio")) {
                                Object valor = (Object) metodo.invoke(ppp);
                                if (valor == null) {
                                    pppVacios++;
                                    pppVaciosTotal++;
                                }
                            } else {
                                String valor;
                                valor = this.obtenerValorCampo(metodo, ppp);
                                if (valor == null || valor.isEmpty()) {
                                    pppVacios++;
                                    pppVaciosTotal++;
                                } else if (valor.equals("MIGRACION") || valor.equals("NO DEFINIDO")) {
                                    return null;
                                }
                            }
                        }

                        if (pppVacios == pp.getPersonaPredioPropiedads().size()) {
                            nCamposVacios++;
                        }

                    }
                }

            }
        } catch (Exception ex) {
            LOGGER.debug("-- " + campo + "-- Campo inexistente");
        }

        if (nCamposVacios == 0 && pppVaciosTotal == 0) {
            return true;
        } else if (nCamposVacios == nPropietarios) {
            return false;
        } else {
            return null;
        }

    }

    /**
     * Método para determinar si existe alguna justificacion de propiedad asociada al predio retorna
     * verdadero si al menos existe una justificación, falso en caso contrario
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarExistenciaJustificacionPropiedad() {

        for (PersonaPredio pp : this.predioRectificacion.getPersonaPredios()) {
            if (!pp.getPersonaPredioPropiedads().isEmpty() ||
                pp.getPersonaPredioPropiedads() == null) {
                return true;
            }
        }

        return true;
    }

    /**
     * Método para determinar los tipos de propietarios del predio, retorna verdadero si todos son
     * Personas Naturales, falso si todos son Personas Juridicas y null si existen de ambos tipos.
     *
     * @author felipe.cadena
     * @return
     */
    public Boolean determinarTipoPropietarios() {

        int naturales = 0;
        for (PersonaPredio pp : this.predioRectificacion.getPersonaPredios()) {
            if (!pp.getPersona().getTipoIdentificacion().equals(EPersonaTipoIdentificacion.NIT.
                getCodigo())) {
                naturales++;
            }
        }

        if (naturales == 0) {
            return false;
        } else if (naturales == this.predioRectificacion.getPersonaPredios().size()) {
            return true;
        } else {
            return null;
        }

    }

    /**
     * Método para determinar si se pueden rectificar los nombres de la persona retorna falso si se
     * quieren cambiar todos los nombres del propietario ya que no esta permitido.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarTodosNombresPersona() {

        int nombres = 0;
        for (DatoRectificar dato : this.selectedDatosRectificarOComplementarDataTable) {
            if (dato.getNombre().equals(EDatoRectificarNombre.PRIMER_NOMBRE.getCodigo())) {
                nombres++;
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.SEGUNDO_NOMBRE.getCodigo())) {
                nombres++;
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.PRIMER_APELLIDO.getCodigo())) {
                nombres++;
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.SEGUNDO_APELLIDO.getCodigo())) {
                nombres++;
            }
        }

        if (nombres == 4) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Método para validar que no se realizen cambios a nombres de personas Naturales cuando todo
     * los propietarios son personas Juridicas.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarCambiosNaturalAJuridica(EDatoRectificarNaturaleza naturaleza) {

        Boolean tipoPropietarios = this.determinarTipoPropietarios();
        for (DatoRectificar dato : this.selectedDatosRectificarOComplementarDataTable) {
            if (dato.getNombre().equals(EDatoRectificarNombre.PRIMER_NOMBRE.getCodigo())) {
                if (tipoPropietarios != null &&
                     !tipoPropietarios) {

                    if (naturaleza.geCodigo().equals(EDatoRectificarNaturaleza.COMPLEMENTACION.
                        geCodigo())) {
                        this.addMensajeError(EErroresDatosRectificar.PRIMER_NOMBRE_TO_NIT_COM.
                            getMensaje());
                    } else {
                        this.addMensajeError(EErroresDatosRectificar.PRIMER_NOMBRE_TO_NIT.
                            getMensaje());
                    }

                    return false;

                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.SEGUNDO_NOMBRE.getCodigo())) {
                if (tipoPropietarios != null &&
                     !tipoPropietarios) {

                    if (naturaleza.geCodigo().equals(EDatoRectificarNaturaleza.COMPLEMENTACION.
                        geCodigo())) {
                        this.addMensajeError(EErroresDatosRectificar.SEGUNDO_NOMBRE_TO_NIT_COM.
                            getMensaje());
                    } else {
                        this.addMensajeError(EErroresDatosRectificar.SEGUNDO_NOMBRE_TO_NIT.
                            getMensaje());
                    }
                    return false;

                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.PRIMER_APELLIDO.getCodigo())) {
                if (tipoPropietarios != null &&
                     !tipoPropietarios) {

                    if (naturaleza.geCodigo().equals(EDatoRectificarNaturaleza.COMPLEMENTACION.
                        geCodigo())) {
                        this.addMensajeError(EErroresDatosRectificar.PRIMER_APELLIDO_TO_NIT_COM.
                            getMensaje());
                    } else {
                        this.addMensajeError(EErroresDatosRectificar.PRIMER_APELLIDO_TO_NIT.
                            getMensaje());
                    }
                    return false;

                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.SEGUNDO_APELLIDO.getCodigo())) {
                if (tipoPropietarios != null &&
                     !tipoPropietarios) {
                    if (naturaleza.geCodigo().equals(EDatoRectificarNaturaleza.COMPLEMENTACION.
                        geCodigo())) {
                        this.addMensajeError(EErroresDatosRectificar.SEGUNDO_APELLIDO_TO_NIT_COM.
                            getMensaje());
                    } else {
                        this.addMensajeError(EErroresDatosRectificar.SEGUNDO_APELLIDO_TO_NIT.
                            getMensaje());
                    }
                    return false;

                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.RAZON_SOCIAL.getCodigo())) {
                if (tipoPropietarios != null &&
                     tipoPropietarios) {
                    if (naturaleza.geCodigo().equals(EDatoRectificarNaturaleza.COMPLEMENTACION.
                        geCodigo())) {
                        this.addMensajeError(EErroresDatosRectificar.RAZON_SOCIAL_TO_NATURAL_COM.
                            getMensaje());
                    } else {
                        this.addMensajeError(EErroresDatosRectificar.RAZON_SOCIAL_TO_NATURAL.
                            getMensaje());
                    }
                    return false;

                }
            }
        }
        return true;
    }

    /**
     * Método para obtener el valor de un campo determinado de un objeto.
     *
     * @author felipe.cadena
     * @param metodo
     * @param objeto
     * @return
     */
    public String obtenerValorCampo(Method metodo, Object objeto) {
        String valor = null;
        try {
            valor = (String) metodo.invoke(objeto);
        } catch (ClassCastException e) {
            return "exist";
        } catch (Exception e) {
            LOGGER.debug("Error invocacion del metodo");
        }

        return valor;
    }

    /**
     * Método para imprimir un listado de resultados de la validación para pruebas
     *
     * @author felipe.cadena
     * @param estadoPersona
     */
    public void testMostrarEstado(List<DatoRectificar> estadoPersona) {

        for (DatoRectificar dato : estadoPersona) {
            LOGGER.debug("--" + dato.getNombre() + " -- " + dato.isRectificar() + " -- " + dato.
                isComplementar() + " -- C: " + dato.isNoConstruccion());
        }
    }

    /**
     * Método para completar los campos de número predial cuando vienen incompletos
     *
     * @author felipe.cadena
     * @param size
     * @param campo
     * @return
     */
    private String completarCampoNumeroPredial(int size, String campo) {
        int actualSize = campo.length();

        if (size > actualSize) {
            campo = "0" + campo;
            actualSize++;
        }

        return campo;
    }

    /**
     * Mètodo para validar el numero predial de los tràmites de quinta omitido
     *
     * @author felipe.cadena
     *
     * @modify by lorena.salamanca :: 19-08-2015 :: Se agrega validacion para PH y Condominio,
     * predio diligenciado hasta condicion de propiedad. Se agrega validacion para que si es un
     * tramite de quinta no diligencien una unidad. Se agrega validacion para verificar que exista
     * ficha matriz para el predio diligenciado. Se agrega mensaje de informacion con los predios
     * cancelados.
     *
     * @return
     */
    public boolean validarNumeroPredialQuintaOmitido() {
        boolean resultado = true;

        String numeroPredial = this.nuevoTramiteSeleccionado.getNumeroPredial();

        String condicionPropiedad = numeroPredial.substring(21, 22);

        if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo()) ||
            condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
            //Se valida que el nùmero predial este diligenciado hasta condicion de propiedad
            if (numeroPredial == null || numeroPredial.isEmpty() || numeroPredial.length() < 22) {
                this.addMensajeError(
                    "El número predial debe ser diligenciado hasta Condicion de propiedad.");
                return false;
            }
            if (numeroPredial.length() > 22) {
                if (Pattern.compile("[1-9]").matcher(numeroPredial.substring(22, numeroPredial.
                    length())).find()) {
                    this.addMensajeError(
                        "El trámite a radicar se debe realizar con el número predial de la ficha matriz.");
                    return false;
                }
            }
            if (numeroPredial == null || numeroPredial.isEmpty() || numeroPredial.length() < 30) {
                char[] array = new char[30 - numeroPredial.length()];
                Arrays.fill(array, '0');
                String a = new String(array);
                numeroPredial = numeroPredial + a;
                this.nuevoTramiteSeleccionado.setNumeroPredial(numeroPredial);
            }

            FichaMatriz fichaMatriz = this.getConservacionService().
                getFichaMatrizByNumeroPredialPredio(numeroPredial);
            if (fichaMatriz == null) {
                this.addMensajeError(
                    "No existe información de la ficha matriz para los predios de condición 9 u 8.");
                return false;
            }
            List<Predio> prediosCancelados = this.getConservacionService().
                obtenerPrediosCanceladosQuintaCondominioPH(numeroPredial);
            if (prediosCancelados != null) {
                if (prediosCancelados.size() > 0) {
                    StringBuilder predios = new StringBuilder();
                    for (Predio p : prediosCancelados) {
                        predios.append(p.getNumeroPredial());
                        predios.append(", ");
                    }
                    this.addMensajeInfo("Existen predios cancelados: " + predios.substring(0,
                        predios.lastIndexOf(", ")));
                }
            }
            this.nuevoTramiteSeleccionado.setPredio(this.getConservacionService().
                getPredioByNumeroPredial(numeroPredial));
        } else {
            //Se valida que el nùmero predial este completo
            if (numeroPredial == null || numeroPredial.isEmpty() || numeroPredial.length() < 30) {
                this.addMensajeError("El número predial debe ser diligenciado completamente.");
                return false;
            }

            //Se valida que no exista un predio con ese nùmero predial.
            Predio p = this.getConservacionService().
                buscarPredioPorNumeroPredialParaAvaluoComercial(numeroPredial);
            if (p != null && EPredioEstado.ACTIVO.getCodigo().equals(p.getEstado())) {
                this.addMensajeError("Ya existe un predio asociado a ese número predial.");
                return false;
            }
        }

        //Se valida que no se ingrese el nùmero de terreno en 0000
        String numeroTerreno = numeroPredial.substring(17, 21);
        if (numeroTerreno.equals("0000")) {
            this.addMensajeError("Se debe ingresar un número de terreno diferente a 0000.");
            return false;
        }

        return resultado;
    }

    /**
     * Mètodo para validar el numero predial de los tràmites de quinta nuevo
     *
     * @author felipe.cadena
     *
     * @modify by lorena.salamanca :: 19-08-2015 :: Se agrega validacion para PH y Condominio,
     * predio diligenciado hasta condicion de propiedad. Se agrega validacion para que si es un
     * tramite de quinta no diligencien una unidad. Se agrega validacion para verificar que exista
     * ficha matriz para el predio diligenciado. Se agrega mensaje de informacion con los predios
     * cancelados.
     * @return
     */
    public boolean validarNumeroPredialQuintaNuevo() {
        boolean resultado = true;

        String numeroPredial = this.nuevoTramiteSeleccionado.getNumeroPredial();

        String condicionPropiedad = numeroPredial.substring(21, 22);

        if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo()) ||
            condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
            //Se valida que el nùmero predial este diligenciado hasta condicion de propiedad
            if (numeroPredial.isEmpty() || numeroPredial.length() < 22) {
                this.addMensajeError(
                    "El número predial debe ser diligenciado hasta Condicion de propiedad.");
                return false;
            }
            if (numeroPredial.length() > 22) {
                if (Pattern.compile("[1-9]").matcher(numeroPredial.substring(22, numeroPredial.
                    length())).find()) {
                    this.addMensajeError(
                        "El trámite a radicar se debe realizar con el número predial de la ficha matriz.");
                    return false;
                }
            }
            FichaMatriz fichaMatriz = this.getConservacionService().
                getFichaMatrizByNumeroPredialPredio(numeroPredial);
            if (fichaMatriz == null) {
                this.addMensajeError(
                    "No existe información de la ficha matriz para los predios de condición 9 u 8.");
                return false;
            }
            List<Predio> prediosCancelados = this.getConservacionService().
                obtenerPrediosCanceladosQuintaCondominioPH(numeroPredial);
            if (prediosCancelados != null && prediosCancelados.size() > 0) {
                StringBuilder predios = new StringBuilder();
                for (Predio p : prediosCancelados) {
                    predios.append(p.getNumeroPredial());
                    predios.append(", ");
                }
                this.addMensajeInfo("Existen predios cancelados: " + predios.substring(0, predios.
                    lastIndexOf(", ")));
            }
            this.nuevoTramiteSeleccionado.setPredio(this.getConservacionService().
                getPredioByNumeroPredial(numeroPredial));
        } else {
            //Se valida que el numero predial este completo
            if (numeroPredial == null || numeroPredial.isEmpty() || numeroPredial.length() < 17) {
                this.addMensajeError("El número predial debe ser diligenciado hasta Manzana.");
                return false;
            }
        }

        //version 1.1.4 - felipe.cadena_7286
        // felipe.cadena:: #7286 :: Se retira esta validacion para permitir manzanas con valor '0000'
//        String codigoManzanaCorto = numeroPredial.substring(13, 17);
//        if (codigoManzanaCorto.equals("0000")) {
//            this.addMensajeError("El nùmero predial debe ser diligenciado por lo menos hasta manzana.");
//            return false;
//        }
        //Se valida que no exista un predio con ese nùmero predial.
        String codigomanzana = numeroPredial.substring(0, 17);
        ManzanaVereda m = this.getGeneralesService().getCacheManzanaVeredaByCodigo(codigomanzana);
        if (m == null) {
            this.addMensajeError("No existe una manzana con el número ingresado");
            return false;
        }

        return resultado;
    }

    /**
     * Metodo que valida los datos que se van a complementar, si encuentra un error en los datos,
     * muestra un mensaje y retorna null
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarDatosComplementacion() {

        if (this.selectedDatosRectificarOComplementarDataTable == null ||
             this.selectedDatosRectificarOComplementarDataTable.isEmpty()) {
            this.addMensajeError(EErroresDatosRectificar.NO_DATOS_COMPLEMENTAR.getMensaje());
            return false;
        }
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado == null ||
            this.tramitesPredioEnglobeNuevoTramiteAdicionado.isEmpty()) {
            this.addMensajeError(EErroresDatosRectificar.NO_PREDIO.getMensaje());
            return false;
        }
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado.size() > 1) {
            this.addMensajeError(EErroresDatosRectificar.VARIOS_PREDIOS.getMensaje());
            return false;
        }

        this.predioRectificacion = this.getConservacionService().
            obtenerPredioValidacionesRectificacionById(this.nuevoTramiteSeleccionado.getPredio().
                getId());

        for (DatoRectificar dato : this.selectedDatosRectificarOComplementarDataTable) {
            if (dato.getNombre().equals(EDatoRectificarNombre.FOTO.getCodigo())) {
                Boolean estadoFoto = this.validarFotos();
                if (estadoFoto != null && estadoFoto) {
                    this.addMensajeError(EErroresDatosRectificar.FOTO_EXISTE.getMensaje());
                    return false;
                }
            }
            if (dato.getNombre().equals(EDatoRectificarNombre.COEFICIENTE.getCodigo())) {
                if (!this.predioRectificacion.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_7.getCodigo()) ||
                    !this.predioRectificacion.getCondicionPropiedad().equals(
                        EPredioCondicionPropiedad.CP_8.getCodigo()) ||
                    !this.predioRectificacion.getCondicionPropiedad().equals(
                        EPredioCondicionPropiedad.CP_9.getCodigo())) {
                    this.addMensajeError(EErroresDatosRectificar.CONDICION_7_8_9_COMP.getMensaje());
                    return false;
                }
            }
        }

        if (!this.validarDatosPropietario(EDatoRectificarNaturaleza.COMPLEMENTACION)) {
            return false;
        }
        if (!this.validarDatosPredio(EDatoRectificarNaturaleza.COMPLEMENTACION)) {
            return false;
        }
        return true;
    }

    /**
     * Método encargado de realizar todas las validaciones correspondientes cuando se desea agregar
     * un nuevo trámite dependiendo del tipo de trámite
     *
     * @author javier.aponte
     * @return
     */
    /*
     * @modify by juanfelipe.garcia :: 20-08-2013 :: condicion del predio es 9 o 5 debe permitir
     * ingresar cero en el campo Auto estimación terreno (#5388) @modify by juanfelipe.garcia ::
     * 08-10-2013 :: Adición de validaciones para tramites nuevos (#5667)
     *
     * @modify by felipe.cadena :: 2-10-2013 :: Adición de validaciones para tramites de quinta
     *
     * @modify by juanfelipe.garcia :: 28-03-2014 :: Adición de validaciones para solicitantes
     * Persona Juridica (#7500)
     *
     * @modify by lorena.salamanca :: 19-08-2015 :: Se quita la validacion para el area nula para
     * mutacion quinta
     */
    public boolean validarDatosTramite() {

        if (!this.tramitesPredioEnglobeNuevoTramiteAdicionado.isEmpty()) {
            if (this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getTramite() != null &&
                 this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getTramite().isQuinta()) {
                Predio predioTemp = this.getConservacionService().getPredioByNumeroPredial(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getTramite().
                        getNumeroPredial());
                if (predioTemp != null && EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo().equals(
                    predioTemp.getTipoCatastro())) {
                    this.banderaPredioFiscal = true;
                }
            } else if (this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getPredio() != null &&
                 EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo().equals(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getPredio().
                        getTipoCatastro())) {
                this.banderaPredioFiscal = true;
            }
        }

        // se realizan validaciones sobre predios de condicion 5
        if (!this.validarPrediosCondicionCinco()) {
            return false;
        }

        //Si el trámite es de rectificación realiza las validaciones asociadas a este tipo de trámite
        if (this.nuevoTramiteSeleccionado.isEsRectificacion()) {

            return this.validarDatosRectificacion();

        } //Si el trámite es de complementación realiza las validaciones asociadas a este tipo de trámite
        else if (this.nuevoTramiteSeleccionado.isEsComplementacion()) {
            return this.validarDatosComplementacion();
        } // se valida el numero predial para tramites de quinta
        else if (this.nuevoTramiteSeleccionado.isQuinta()) {

            if (this.nuevoTramiteSeleccionado.isQuintaOmitido()) {
                if (!this.validarNumeroPredialQuintaOmitido()) {
                    return false;
                }
            } else {
                if (!this.validarNumeroPredialQuintaNuevo()) {
                    return false;
                }
            }
        }

        //Verifica si no hay predios asociados a un trámite
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado == null ||
            this.tramitesPredioEnglobeNuevoTramiteAdicionado.isEmpty() ||
             this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getPredio() == null) {

            //Si el trámite es de quinta es posible que no haya predios asociados
            if (this.nuevoTramiteSeleccionado.isQuinta()) {
                if (this.areaTerreno != null && !this.areaTerreno.equals("")) {
                    if (!this.areaTerreno.matches("^[0-9,;]+$") ||
                        Double.valueOf(this.areaTerreno) <= 0) {
                        String mensaje = "El área del predio debe ser un número mayor a cero," +
                             " por favor revise los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        return false;
                    } else {
                        this.nuevoTramiteSeleccionado.setAreaTerreno(Double.
                            valueOf(this.areaTerreno));
                    }
                }
            } //Si el trámite no es de quinta se debe producir un error cuando no haya predios asociados
            else {
                String mensaje = "El trámite seleccionado no cuenta con predios asociados," +
                     " por favor ingrese los datos requeridos e intente nuevamente";
                this.addMensajeError(mensaje);
                return false;
            }
        }
        //version 1.1.5 juanfelipe.garcia_7321 
        //refs #7500 validación adicional persona juridica
        boolean personaJuridica = false;
        if (this.solicitudSeleccionada.getSolicitanteSolicituds() != null &&
             !this.solicitudSeleccionada.getSolicitanteSolicituds().isEmpty()) {

            personaJuridica = true;
            for (SolicitanteSolicitud ss : this.solicitudSeleccionada.getSolicitanteSolicituds()) {
                if (!ss.getTipoPersona().equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {
                    personaJuridica = false;
                }
            }
        }
        ////version 1.1.5 juanfelipe.garcia_7321
        //refs #7500 para solicitudes de personas jurídicas el nuevoTramiteSeleccionado no tiene SolicitanteTramite,
        //se adiciona validación
        if ((this.solicitudSeleccionada.isSolicitudConIntermediario()) &&
             !personaJuridica &&
             (this.nuevoTramiteSeleccionado.getSolicitanteTramites() == null ||
            this.nuevoTramiteSeleccionado
                .getSolicitanteTramites().isEmpty())) {
            String mensaje = "El trámite seleccionado no cuenta con solicitantes asociados," +
                 " por favor ingrese los datos requeridos e intente nuevamente";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.solicitudSeleccionada.isRevisionAvaluo() ||
             this.nuevoTramiteSeleccionado.isTramiteDeAutoavaluo()) {

            //validaciones si el predio es fiscal
            if (this.banderaPredioFiscal && this.nuevoTramiteSeleccionado.isTramiteDeAutoavaluo()) {

                if (this.nuevoTramiteSeleccionado.getAreaConstruccion() == 0.0 &&
                    this.nuevoTramiteSeleccionado.getAreaTerreno() == null) {
                    String mensaje =
                        "Resumen: Error de validación: El predio es Fiscal y debe haber minimo una de las dos areas ingresadas.";
                    this.addMensajeError("nuevoTramiteSolForm:resumenAutoavaluo",
                        mensaje);
                    return false;
                }

                if (this.nuevoTramiteSeleccionado.getAreaTerreno() != null &&
                    this.nuevoTramiteSeleccionado.getAutoavaluoTerreno() == null) {
                    String mensaje =
                        "Resumen: Error de validación: El predio es Fiscal debe ingresar el autoavaluo del terreno.";
                    this.addMensajeError("nuevoTramiteSolForm:resumenAutoavaluo",
                        mensaje);
                    return false;
                }

                if (this.nuevoTramiteSeleccionado.getAreaConstruccion() != 0.0 &&
                    this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion() == 0.0) {
                    String mensaje =
                        "Resumen: Error de validación: El predio es Fiscal debe ingresar el autoavaluo de la construccion.";
                    this.addMensajeError("nuevoTramiteSolForm:resumenAutoavaluo",
                        mensaje);
                    return false;
                }
            }

            if (this.nuevoTramiteSeleccionado.getResumen() != null &&
                 !this.nuevoTramiteSeleccionado.getResumen().isEmpty() &&
                 this.nuevoTramiteSeleccionado.getResumen().length() > 600) {

                String mensaje =
                    "Resumen: Error de validación: el tamaño máximo para el campo es de" +
                     " 600 caracteres.";

                this.addMensajeError("nuevoTramiteSolForm:resumenAutoavaluo",
                    mensaje);
                return false;
            }

            if (this.nuevoTramiteSeleccionado.getFundamentoPeticion() != null &&
                 !this.nuevoTramiteSeleccionado.getFundamentoPeticion()
                    .isEmpty() &&
                 this.nuevoTramiteSeleccionado.getFundamentoPeticion()
                    .length() > 250) {

                String mensaje = "";

                if (this.solicitudSeleccionada.isRevisionAvaluo()) {
                    mensaje =
                        "Síntesis petición: Error de validación: el tamaño máximo para el campo es de" +
                         " 250 caracteres.";
                } else {
                    mensaje =
                        "Fundamento petición: Error de validación: el tamaño máximo para el campo es de" +
                         " 250 caracteres.";
                }

                this.addMensajeError("nuevoTramiteSolForm:fundamentoPeticion", mensaje);
                return false;
            }

            if (this.nuevoTramiteSeleccionado.getAreaTerreno() != null &&
                 this.nuevoTramiteSeleccionado.getPredio() != null) {

                String condicionPropiedad = this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0)
                    .getPredio().getNumeroPredial().substring(21, 22);

                boolean cp = predioEnPH(condicionPropiedad);

                if (cp == true && this.nuevoTramiteSeleccionado.getAreaTerreno() < 0) {

                    String mensaje = "Área terreno: Error de validación: el valor mínimo es 0.";
                    this.addMensajeError("nuevoTramiteSolForm:areaTerrenoAutoavaluo", mensaje);
                    return false;

                } else if (cp == false && this.nuevoTramiteSeleccionado.getAreaTerreno() < 1) {

                    String mensaje =
                        "Área terreno: Error de validación: el valor debe ser mayor a 0.";
                    this.addMensajeError("nuevoTramiteSolForm:areaTerrenoAutoavaluo", mensaje);
                    return false;

                }

            }

            if (this.nuevoTramiteSeleccionado.getAreaConstruccion() != null &&
                 this.nuevoTramiteSeleccionado.getAreaConstruccion() < 0) {

                String mensaje =
                    "Área construcción: Error de validación: el valor debe ser mínimo 0.";
                this.addMensajeError("nuevoTramiteSolForm:areaConstruccionAutoavaluo", mensaje);
                return false;

            }

            if (this.nuevoTramiteSeleccionado.getAutoavaluoTerreno() != null &&
                 this.nuevoTramiteSeleccionado.getAutoavaluoTerreno() < 1) {

                String condicionPropiedad = this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0)
                    .getPredio().getNumeroPredial().substring(21, 22);

                if (this.nuevoTramiteSeleccionado.getAutoavaluoTerreno() < 0 &&
                    (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_5.getCodigo()) ||
                    condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo()))) {

                    String mensaje =
                        "Autoavalúo terreno: Error de validación: el valor debe ser minimo 0.";
                    this.addMensajeError("nuevoTramiteSolForm:autoavaluoTerrenoAutoavaluo", mensaje);
                    return false;

                } else if (!condicionPropiedad.equals(EPredioCondicionPropiedad.CP_5.getCodigo()) &&
                    !condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
                    String mensaje =
                        "Autoavalúo terreno: Error de validación: el valor debe ser mayor a 0.";
                    this.addMensajeError("nuevoTramiteSolForm:autoavaluoTerrenoAutoavaluo", mensaje);
                    return false;
                }

            }

            if (this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion() != null &&
                 this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion() < 0) {

                String mensaje =
                    "Autoavalúo edificaciones: Error de validación: el valor debe ser mínimo 0.";
                this.
                    addMensajeError("nuevoTramiteSolForm:autoavaluoConstruccionAutoavaluo", mensaje);
                return false;

            }

        }

        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado.size() == 1) {

            //Si el trámite es de englobe y sólo cuenta con un predio asociado, se genera error
            if (this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {

                String mensaje = "El trámite seleccionado es de tipo englobe pero no cuenta con" +
                     " suficientes predios asociados," +
                     " por favor ingrese los predios faltantes e intente nuevamente!";
                this.addMensajeError(mensaje);
                return false;

            } else if (this.solicitudSeleccionada.isViaGubernativa() ||
                 this.solicitudSeleccionada.isRevocatoriaDirecta()) {

                if (this.tramitesGubernativaAll == null ||
                     this.tramitesGubernativaAll.size() < 1) {
                    this.addMensajeError("No ha seleccionado ningún trámite para asociar," +
                         " por favor seleccione un trámite e intente nuevamente!");
                    return false;
                }

            }
        }

        //Para los trámites de segunda englobe se hace esta validación, con el fin que no se puedan englobar 
        //predios que se encuentren en estado cancelado
        if (this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {

            if (this.tramitesPredioEnglobeNuevoTramiteAdicionado != null &&
                !this.tramitesPredioEnglobeNuevoTramiteAdicionado.isEmpty()) {

                String numeroManzana = this.nuevoTramiteSeleccionado.getPredio().getNumeroPredial().
                    substring(0, 17);

                for (TramitePredioEnglobe tpe : tramitesPredioEnglobeNuevoTramiteAdicionado) {
                    if (EPredioEstado.CANCELADO.getCodigo().equals(tpe.getPredio().getEstado())) {
                        String mensaje = "El predio con número predial " + tpe.getPredio().
                            getNumeroPredial() +
                            
                            " se encuentra en estado cancelado, por lo tanto no se puede englobar, por favor" +
                             " corrija los datos e intente nuevamente";
                        this.addMensajeError(mensaje);
                        return false;
                    }

                    //javier.aponte :: Se agrega validación para que no se permita radicar predios que estén 
                    //en diferentes manzanas :: 06/08/2015
                    if (!tpe.getPredio().getNumeroPredial().substring(0, 17).equals(numeroManzana.
                        substring(0, 17))) {
                        String mensaje =
                            "En un trámite de mutación segunda englobe, todos los predios deben pertenecer a la misma" +
                            " manzana, por favor corrija los datos e intente nuevamente";
                        this.addMensajeError(mensaje);
                        return false;
                    }
                }

                //valida si todos los predios a englobar son predios fiscales
                for (TramitePredioEnglobe tpe : tramitesPredioEnglobeNuevoTramiteAdicionado) {
                    if (tpe.getPredio().getTipoCatastro().equals(
                        EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                        for (TramitePredioEnglobe tpef : tramitesPredioEnglobeNuevoTramiteAdicionado) {
                            if (!tpef.getPredio().getTipoCatastro().equals(
                                EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                                String mensaje = "El englobe tiene un predio que no se encuentra" +
                                     " formado y debe realizarse este proceso";
                                this.addMensajeError(mensaje);
                                return false;
                            }
                            this.banderaPredioFiscal = true;
                        }
                    }
                }

            }

        }

        //En el flujo de radicación, el trámite inicial es null, se adiciona booleano de control para este caso
        boolean vieneDeRadicacion = false;
        if (this.tramiteInicial == null) {
            vieneDeRadicacion = true;
        }
        if (!vieneDeRadicacion && this.nuevoTramiteSeleccionado.isTercera() && this.tramiteInicial.
            isTercera()) {
            if (this.tramiteInicial.getPredio().getId().equals(this.nuevoTramiteSeleccionado.
                getPredio().getId())) {
                this.addMensajeError("El predio del trámite para asociar," +
                    
                    " no puede ser el mismo del trámite inicial que va a cancelar, por favor seleccione un predio diferente e intente nuevamente!");
                return false;
            }
        }

        // Esta validación se debe realizar cuando ya se hayan realizado todas
        // las otras validaciones necesarias
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionado.size() > 1) {
            if (this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {//aqui el tramite inicial puede estar en null en el flujo de radicacion
                Tramite tempTramiteInicial = null;
                int contPrediosInicial = 0;
                boolean contienePredioInicial = false;

                if (!vieneDeRadicacion) {
                    tempTramiteInicial = this.getTramiteService().
                        findTramitePruebasByTramiteId(this.tramiteInicial.getId());
                }

                List<Predio> listaPrediosTramiteAdicionado = new ArrayList<Predio>();

                for (TramitePredioEnglobe tpeTemp : this.tramitesPredioEnglobeNuevoTramiteAdicionado) {

                    //Validar que uno de los predios del trámite adicional sea del trámite inicial.
                    if (!vieneDeRadicacion) {
                        if (this.tramiteInicial.isSegundaEnglobe() &&
                            tempTramiteInicial.getPredios().contains(tpeTemp.getPredio())) {

                            contPrediosInicial++;
                        } else if (tpeTemp.getPredio().getId().equals(this.tramiteInicial.
                            getPredio().getId())) {
                            contienePredioInicial = true;
                        }
                    }

                    listaPrediosTramiteAdicionado.add(tpeTemp.getPredio());
                }

                //modificación para determinar colindantes
                if (!this.banderaPredioFiscal) {
                    if (!this.getConservacionService().
                        determinarColindancia(listaPrediosTramiteAdicionado,
                            listaPrediosTramiteAdicionado.get(0),
                            this.getUsuario(), false
                        ).isEmpty()) {
                        String mensaje =
                            "Uno o mas de los predios asociados al trámite para englobe" +
                             " no son colindantes!";
                        this.addMensajeWarn(mensaje);
                    }
                }

                //Para el caso de trámite inicial y trámite adicional englobes, uno de los predios del adicional debe ser del inicial. 
                if (!vieneDeRadicacion) {
                    if (this.tramiteInicial.isSegundaEnglobe()) {
                        if (contPrediosInicial > 1) {
                            String mensaje = "Solo uno de los predios del tramite inicial " +
                                 " se puede usar para realizar el englobe," +
                                 " por favor corrija los predios e intente nuevamente!";
                            this.addMensajeError(mensaje);
                            return false;
                        } else if (contPrediosInicial == 0) {
                            String mensaje =
                                "Uno de los predios asociados al trámite para englobe " +
                                 " debe ser el predio del trámite inicial," +
                                 " por favor corrija los predios e intente nuevamente!";
                            this.addMensajeError(mensaje);
                            return false;
                        }

                    } else if (!contienePredioInicial) {
                        String mensaje = "Uno de los predios asociados al trámite para englobe " +
                             " debe ser el predio del trámite inicial," +
                             " por favor corrija los predios e intente nuevamente!";
                        this.addMensajeError(mensaje);
                        return false;
                    }
                }

                Municipio municipioTemp = null;
                Municipio municipioTemp2 = null;

                municipioTemp = this.tramitesPredioEnglobeNuevoTramiteAdicionado.get(0).getPredio()
                    .getMunicipio();

                int municipios = 0;

                for (TramitePredioEnglobe tpe : this.tramitesPredioEnglobeNuevoTramiteAdicionado) {
                    municipioTemp2 = tpe.getPredio().getMunicipio();
                    if (!municipioTemp.getCodigo().equals(
                        municipioTemp2.getCodigo())) {
                        municipios++;
                    }
                }

                if (municipios == 0) {
                    return true;
                } else {
                    String mensaje = "Los predios asociados al trámite corresponden a diferentes" +
                         " municipios y no se puede realizar el englobe," +
                         " por favor corrija los predios e intente nuevamente!";
                    this.addMensajeError(mensaje);
                    return false;
                }

            } else if (this.solicitudSeleccionada.isViaGubernativa() ||
                 this.solicitudSeleccionada.isRevocatoriaDirecta()) {
            } else {
                String mensaje = "El trámite seleccionado cuenta con múltiples predios asociados" +
                     " y sólo debe tener uno," +
                     " por favor elimine los predios sobrantes e intente nuevamente";
                this.addMensajeError(mensaje);
                return false;
            }
        }
        return true;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida si dada la condición de propiedad del número predial el predio se encuentra
     * en PH
     */
    private boolean predioEnPH(String condicionPropiedad) {

        if (condicionPropiedad != null &&
             !condicionPropiedad.isEmpty() &&
             (condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_5
                    .getCodigo()) ||
             condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_6
                    .getCodigo()) ||
             condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_7
                    .getCodigo()) ||
             condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_8
                    .getCodigo()) || condicionPropiedad
                .equals(EPredioCondicionPropiedad.CP_9
                    .getCodigo()))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo para validar las mejoras para los tramites de autoavaluo
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarAutoavaluoMejora() {

        if (this.nuevoTramiteSeleccionado.getAreaConstruccion() == null) {
            this.nuevoTramiteSeleccionado.setAreaConstruccion(0d);
        }
        if (this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion() == null) {
            this.nuevoTramiteSeleccionado.setAutoavaluoConstruccion(0d);
        }
        if (this.nuevoTramiteSeleccionado.getAreaTerreno() == null) {
            this.nuevoTramiteSeleccionado.setAreaTerreno(0d);
        }
        if (this.nuevoTramiteSeleccionado.getAutoavaluoTerreno() == null) {
            this.nuevoTramiteSeleccionado.setAutoavaluoTerreno(0d);
        }

        if (!this.nuevoTramiteSeleccionado.getAutoavaluoTerreno().equals(0d) ||
             !this.nuevoTramiteSeleccionado.getAreaTerreno().equals(0d) ||
             this.nuevoTramiteSeleccionado.getAreaConstruccion().equals(0d) ||
             this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion().equals(0d)) {

            this.addMensajeError("Verifique los datos ingresados en la auto estimación");
            return false;
        }
        return true;
    }

    /**
     * Metodo para validar las mejoras para los tramites de autoavaluo
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarAutoavaluo() {

        if (this.nuevoTramiteSeleccionado.getAreaConstruccion() == null) {
            this.nuevoTramiteSeleccionado.setAreaConstruccion(0d);
        }
        if (this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion() == null) {
            this.nuevoTramiteSeleccionado.setAutoavaluoConstruccion(0d);
        }

        if ((this.nuevoTramiteSeleccionado.getAreaConstruccion() == null ||
            !this.nuevoTramiteSeleccionado.getAreaConstruccion().equals(0d)) &&
             this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion().equals(0d)) {

            this.addMensajeError(
                "Si el Auto estimación edificaciones es cero, el area de construcciòn debe ser cero");
            return false;
        }
        if (this.nuevoTramiteSeleccionado.getAreaConstruccion().equals(0d) &&
             !this.nuevoTramiteSeleccionado.getAutoavaluoConstruccion().equals(0d)) {

            this.addMensajeError(
                "Si el area de construcciòn es cero, la Auto estimación edificaciones debe ser cero");
            return false;
        }
        return true;
    }

    /**
     * Selecciona los predios del tramite que necesian validación de condición cinco e invoca las
     * validaciones pertinentes.
     *
     * @author felipe.cadena
     * @return
     */
    private Boolean validarPrediosCondicionCinco() {

        List<Predio> prediosTramite = new ArrayList<Predio>();
        List<Predio> prediosMejora = new ArrayList<Predio>();
        List<Predio> prediosTerreno = new ArrayList<Predio>();

        if (this.nuevoTramiteSeleccionado.isTramiteDeAutoavaluo()) {
            if (this.nuevoTramiteSeleccionado.getPredio().isMejora()) {
                return this.validarAutoavaluoMejora();
            } else {
                return this.validarAutoavaluo();
            }
        }

        if (!this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {
            return true;
        }

        if (this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {
            for (Predio predio : this.nuevoTramiteSeleccionado.getPredios()) {
                prediosTramite.add(predio);
            }
        } else {
            prediosTramite.add(this.nuevoTramiteSeleccionado.getPredio());
        }

        for (Predio predio : prediosTramite) {
            if (predio.isMejora()) {
                prediosMejora.add(predio);
            } else {
                prediosTerreno.add(predio);
            }
        }

        //Se realizan las validaciones cuando son solo mejoras
        if (!prediosMejora.isEmpty() && prediosTerreno.isEmpty()) {
            return this.validarSoloMejorasEnglobe(prediosMejora);
        }
        //Se realizan las validaciones para cuando son predios de terreno y mejoras
        if (!prediosMejora.isEmpty() && !prediosTerreno.isEmpty()) {
            return this.validarMejorasYTerrenoEnglobe(prediosMejora, prediosTerreno);
        }

        return true;
    }

    /**
     * Método que realiza las validaciones sobre los predios que son solo mejoras para un tramite de
     * englobe
     *
     * @author felipe.cadena
     * @param predios
     * @return
     */
    public Boolean validarSoloMejorasEnglobe(List<Predio> predios) {
        String numeroTerreno = "";
        List<Predio> prediosNoColindantes;

        if (!predios.isEmpty()) {
            numeroTerreno = predios.get(0).getNumeroPredial().substring(0, 21);
        }
        //se verifica que los predios pertenezcan al mismo terreno
        for (Predio predio : predios) {
            if (!predio.getNumeroPredial().startsWith(numeroTerreno)) {
                this.
                    addMensajeError("Las mejoras para el englobe deben pertenecer al mismo terreno");
                return false;
            }
        }

        //se verifica la colindancia del conjunto de mejoras
        if (!this.banderaPredioFiscal) {
            prediosNoColindantes = this.getConservacionService().
                determinarColindanciaMejoras(predios, this.usuario);

            if (!prediosNoColindantes.isEmpty()) {
                this.addMensajeError("Las mejoras para el englobe deben ser colindantes entre si");
                return false;
            }
        }

        return true;
    }

    /**
     * Método que realiza las validaciones cuando se va a realizar un englobe
     *
     * @author felipe.cadena
     * @param prediosMejora
     * @param prediosTerreno
     * @return
     */
    public Boolean validarMejorasYTerrenoEnglobe(List<Predio> prediosMejora,
        List<Predio> prediosTerreno) {

        List<Predio> prediosTerrenoNoColindantes = new ArrayList<Predio>();

        //se determina la colindancia de los predios de terreno
        prediosTerrenoNoColindantes = this.getConservacionService().
            determinarColindancia(prediosTerreno, prediosTerreno.get(0), this.usuario, false);

        if (!prediosTerrenoNoColindantes.isEmpty()) {
            this.addMensajeError(
                "Los predios de terreno para el englobe deben ser colindantes entre si");
            return false;
        }

        //se valida que todas las mejoras pertenezcan a los predios de terreno que
        //se van a englobar
        for (Predio mejora : prediosMejora) {
            String numeroTerrenoMejora = mejora.getNumeroPredial().substring(0, 21);

            boolean tieneTerreno = false;

            for (Predio predio : prediosTerreno) {
                if (predio.getNumeroPredial().startsWith(numeroTerrenoMejora)) {
                    tieneTerreno = true;
                    break;
                }
            }

            //Se elimina esta validación por el CC-NP-CO-064 Ajustar radicación de englobe con mejora de terreno
            /*
             * if(!tieneTerreno){ this.addMensajeError("La mejora "+ mejora.getNumeroPredial() +" no
             * pertenece a ninguno de los " + " terrenos selecionados. Todas las mejoras deben
             * pertenecer a los predios del englobe"); return false;
            } */
        }

        return true;
    }

    /**
     * Método que realiza las validaciones sobre los predios de condición de propiedad 5 en el
     * proceso de radicación
     *
     * @author felipe.cadena
     * @param predio
     * @return
     */
    public Boolean validarPredioCondicionCinco(Predio predio) {

        return true;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que valida los datos de cancelacion del trámite inicial con el trámite que se va a
     * crear
     *
     * @author javier.aponte
     */
    /*
     * @modified by juanfelipe.garcia :: 31-08-2013 :: adición de validación cuando el tramite tiene
     * mas de un predio y no es un englobe
     */
    public boolean validarAsociarTramitesNuevos() {

        // Verifica que los predios que se van a asociar al nuevo trámite pertenezcan al
        // mismo municipio del trámite que se va a cancelar
        if (this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.size() == 1) {
            if (!this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.get(0).getPredio().
                getMunicipio()
                .getCodigo().equals(this.tramiteInicial.getMunicipio().getCodigo())) {
                String mensaje =
                    "Los predios asociados al trámite corresponden a diferentes municipios" +
                     " por favor corrija los predios e intente nuevamente!";
                this.addMensajeError(mensaje);
                return false;
            }

        } //Si hay varios predios asociados al nuevo trámite
        else if (this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.size() > 1) {

            //Verifica si este predio es una mutación de segunda englobe
            if ((this.nuevoTramiteSeleccionado.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) &&
                 this.nuevoTramiteSeleccionado.getClaseMutacion() != null &&
                 !this.nuevoTramiteSeleccionado.getClaseMutacion()
                    .isEmpty() &&
                 this.nuevoTramiteSeleccionado.getClaseMutacion()
                    .equals(EMutacionClase.SEGUNDA.getCodigo()) &&
                 this.nuevoTramiteSeleccionado.getSubtipo() != null &&
                 !this.nuevoTramiteSeleccionado.getSubtipo().isEmpty() &&
                this.nuevoTramiteSeleccionado
                    .getSubtipo().equals(EMutacion2Subtipo.ENGLOBE.getCodigo()))) {

                Municipio municipioTemp = null;
                Municipio municipioTemp2 = null;

                municipioTemp = this.tramiteInicial.getMunicipio();

                int municipios = 0;

                //Se verifica que todos los predios que se van a asociar en un desenglobe estén 
                //en el mismo municipio del trámite que se va a cancelar y que además todos estén
                //en el mismo municipio del trámite que se va a crear
                for (TramitePredioEnglobeDTO tpe
                    : this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO) {
                    municipioTemp2 = tpe.getPredio().getMunicipio();
                    if (!municipioTemp.getCodigo().equals(
                        municipioTemp2.getCodigo())) {
                        municipios++;
                    }
                    if (!municipioTemp.getCodigo().equals(
                        this.nuevoTramiteSeleccionado.getMunicipio()
                            .getCodigo())) {
                        municipios++;
                    }
                }

                if (municipios == 0) {
                    return true;
                } else {
                    String mensaje =
                        "Los predios asociados al trámite corresponden a diferentes municipios" +
                         " y no se puede realizar el englobe," +
                         " por favor corrija los predios e intente nuevamente!";
                    this.addMensajeError(mensaje);
                    return false;
                }

            } else {
                String mensaje = "El trámite seleccionado cuenta con múltiples predios asociados" +
                     " y sólo debe tener uno," +
                     " por favor elimine los predios sobrantes e intente nuevamente";
                this.addMensajeError(mensaje);
                return false;
            }
        }
        return true;
    }
//--------------------------------------------------------------------------------------------------	

    public Tramite establecerDatosTramiteNuevo(Tramite tramite) {

        this.nuevoTramiteSeleccionado = tramite;

        // D: si el trámite es de rectificación debe guardar los
        // TramiteRectificacion
        if (this.nuevoTramiteSeleccionado.isEsRectificacion() ||
             this.nuevoTramiteSeleccionado.isEsComplementacion()) {

            List<TramiteRectificacion> tramiteRectificaciones = null;
            TramiteRectificacion tempTramiteRectificacion;
            tramiteRectificaciones = new ArrayList<TramiteRectificacion>();
            for (DatoRectificar dr : this.selectedDatosRectificarOComplementarDataTable) {

                dr.setUsuarioLog(this.usuario.getLogin());
                dr.setFechaLog(new Date());
                tempTramiteRectificacion = new TramiteRectificacion();
                tempTramiteRectificacion.setTramite(this.nuevoTramiteSeleccionado);
                tempTramiteRectificacion.setDatoRectificar(dr);
                tempTramiteRectificacion.setUsuarioLog(this.usuario.getLogin());
                tempTramiteRectificacion.setFechaLog(new Date());
                tramiteRectificaciones.add(tempTramiteRectificacion);

            }
            this.nuevoTramiteSeleccionado.setTramiteRectificacions(tramiteRectificaciones);
        }

        //A los nuevos trámites se les asocia la solicitud seleccionada
        this.nuevoTramiteSeleccionado.setSolicitud(this.getSolicitudSeleccionada());

        //Se establece la fecha de radicación del trámite
        this.nuevoTramiteSeleccionado.setFechaRadicacion(new Date(System.currentTimeMillis()));

        if (this.nuevoTramiteSeleccionado.isQuinta()) {

            if (this.nuevoTramiteSeleccionado.isQuintaNuevo()) {

                if (this.numeroPredialParte1 == null ||
                     this.numeroPredialParte1.isEmpty()) {
                    this.numeroPredialParte1 = "00";
                }
                if (this.numeroPredialParte2 == null ||
                     this.numeroPredialParte2.isEmpty()) {
                    this.numeroPredialParte2 = "000";
                }
                if (this.numeroPredialParte3 == null ||
                     this.numeroPredialParte3.isEmpty()) {
                    this.numeroPredialParte3 = "00";
                }
                if (this.numeroPredialParte4 == null ||
                     this.numeroPredialParte4.isEmpty()) {
                    this.numeroPredialParte4 = "00";
                }
                if (this.numeroPredialParte5 == null ||
                     this.numeroPredialParte5.isEmpty()) {
                    this.numeroPredialParte5 = "00";
                }
                if (this.numeroPredialParte6 == null ||
                     this.numeroPredialParte6.isEmpty()) {
                    this.numeroPredialParte6 = "00";
                }
                if (this.numeroPredialParte7 == null ||
                     this.numeroPredialParte7.isEmpty()) {
                    this.numeroPredialParte7 = "0000";
                }
                if (this.numeroPredialParte8 == null ||
                     this.numeroPredialParte8.isEmpty()) {
                    this.numeroPredialParte8 = "0000";
                }
                if (this.numeroPredialParte9 == null ||
                     this.numeroPredialParte9.isEmpty()) {
                    this.numeroPredialParte9 = "0";
                }
                if (this.numeroPredialParte10 == null ||
                     this.numeroPredialParte10.isEmpty()) {
                    this.numeroPredialParte10 = "00";
                }
                if (this.numeroPredialParte11 == null ||
                     this.numeroPredialParte11.isEmpty()) {
                    this.numeroPredialParte11 = "00";
                }
                if (this.numeroPredialParte12 == null ||
                     this.numeroPredialParte12.isEmpty()) {
                    this.numeroPredialParte12 = "0000";
                }
            }

            this.nuevoTramiteSeleccionado
                .setNumeroPredial(this.completarCampoNumeroPredial(2, this.numeroPredialParte1) +
                     this.completarCampoNumeroPredial(3, this.numeroPredialParte2) +
                     this.completarCampoNumeroPredial(2, this.numeroPredialParte3) +
                     this.completarCampoNumeroPredial(2, this.numeroPredialParte4) +
                     this.completarCampoNumeroPredial(2, this.numeroPredialParte5) +
                     this.completarCampoNumeroPredial(2, this.numeroPredialParte6) +
                     this.completarCampoNumeroPredial(4, this.numeroPredialParte7) +
                     this.completarCampoNumeroPredial(4, this.numeroPredialParte8) +
                     this.completarCampoNumeroPredial(1, this.numeroPredialParte9) +
                     this.completarCampoNumeroPredial(2, this.numeroPredialParte10) +
                     this.completarCampoNumeroPredial(2, this.numeroPredialParte11) +
                     this.completarCampoNumeroPredial(4, this.numeroPredialParte12));

            if (this.tramiteInicial != null) {
                this.nuevoTramiteSeleccionado.setDepartamento(this.tramiteInicial.getDepartamento());
                this.nuevoTramiteSeleccionado.setMunicipio(this.tramiteInicial.getMunicipio());
            }
            if (this.nuevoTramiteSeleccionado.getPredio() != null) {
                this.nuevoTramiteSeleccionado.setPredio(null);
            }

        } else {
// TODO::juan.agudelo::09-03-12::Implementar la funcionalidad de
            // guardar para via gubernativa::juan.agudelo
            if (this.tramitesPredioEnglobeNuevoTramiteAdicionado != null &&
                !this.tramitesPredioEnglobeNuevoTramiteAdicionado.isEmpty()) {
                this.nuevoTramiteSeleccionado.setDepartamento(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado
                        .get(0).getPredio().getDepartamento());
                this.nuevoTramiteSeleccionado.setMunicipio(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado
                        .get(0).getPredio().getMunicipio());
            }

        }

        if (this.tramiteInicial != null) {
            this.nuevoTramiteSeleccionado.setClasificacion(ETramiteClasificacion.OFICINA.toString());
            this.nuevoTramiteSeleccionado.setTramite(this.tramiteInicial);
        }

        if (!this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {
            this.nuevoTramiteSeleccionado.setTramitePredioEnglobes(null);
        }

        this.nuevoTramiteSeleccionado.setFuncionarioRadicador(this.usuario.getLogin());
        this.nuevoTramiteSeleccionado.
            setNombreFuncionarioRadicador(this.usuario.getNombreCompleto());
        this.nuevoTramiteSeleccionado.setUsuarioLog(this.usuario.getLogin());
        this.nuevoTramiteSeleccionado.setFechaLog(new Date(System.currentTimeMillis()));

        return this.nuevoTramiteSeleccionado;

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton de adicionar tramite nuevo, como la opción de adicionar
     * nuevos trámites que se hace por medio de este método siempre se debe a que ya existe un
     * trámite y se adicionan nuevos trámites con base en éste, los nuevos trámites toman algunos
     * datos del trámite que los origino
     *
     * @author javier.aponte
     */
    public void adicionarTramiteNuevo() {

        if (this.tramiteInicial != null && this.tramiteDocumentacionDeTramiteInicial == null) {
            this.tramiteDocumentacionDeTramiteInicial =
                this.getTramiteService().obtenerTramiteDocumentacionPorIdTramite(
                    this.tramiteInicial.getId());
            this.tramiteInicial.setTramiteDocumentacions(tramiteDocumentacionDeTramiteInicial);
        }

        this.subtipoTramiteEsRequerido = false;
        this.banderaSubtipoEnglobe = false;
        this.banderaClaseDeMutacionSeleccionada = false;
        this.banderaEdicionTramiteTramite = false;

        this.nuevoTramiteSeleccionado = new Tramite();

        this.nuevoTramiteSeleccionado.setDepartamento(this.tramiteInicial.getDepartamento());
        this.nuevoTramiteSeleccionado.setMunicipio(this.tramiteInicial.getMunicipio());
        this.nuevoTramiteSeleccionado.setFuente(this.tramiteInicial.getFuente());

        // Asignar el mismo departamento del trámite inicial a los nuevos trámites adicionados
        if (this.nuevoTramiteSeleccionado.getDepartamento() != null &&
             this.nuevoTramiteSeleccionado.getDepartamento().getCodigo() != null) {
            this.numeroPredialParte1 = this.tramiteInicial.getDepartamento().getCodigo();
        } else {
            this.numeroPredialParte1 = "";
        }

        // Cargar los mismos valores del departamento viene.
        if (this.nuevoTramiteSeleccionado.getMunicipio() != null &&
             this.nuevoTramiteSeleccionado.getMunicipio().getCodigo() != null) {
            this.numeroPredialParte2 = this.tramiteInicial.getMunicipio().getCodigo3Digitos();
        } else {
            this.numeroPredialParte2 = "";
        }

        this.numeroPredialParte3 = "";
        this.numeroPredialParte4 = "";
        this.numeroPredialParte5 = "";
        this.numeroPredialParte6 = "";
        this.numeroPredialParte7 = "";
        this.numeroPredialParte8 = "";
        this.numeroPredialParte9 = "";
        this.numeroPredialParte10 = "";
        this.numeroPredialParte11 = "";
        this.numeroPredialParte12 = "";

        this.nuevoTramiteSeleccionado.setEstado(ETramiteEstado.POR_APROBAR.getCodigo());

        if (this.tramiteInicial.getTramitePredioEnglobes() != null &&
             !this.tramiteInicial.getTramitePredioEnglobes().isEmpty()) {
            this.tramitesPredioEnglobeNuevoTramiteAdicionado = this.tramiteInicial.
                getTramitePredioEnglobes();

            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
                UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado);

            //Ajustar esta linea si no asocia los predios, tener en cuenta la persistencia
            this.nuevoTramiteSeleccionado.setTramitePredioEnglobes(
                this.tramitesPredioEnglobeNuevoTramiteAdicionado);
        } else {
            this.tramitesPredioEnglobeNuevoTramiteAdicionado = new ArrayList<TramitePredioEnglobe>();
            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            tpe.setFechaLog(new Date());
            tpe.setPredio(this.tramiteInicial.getPredio());
            tpe.setUsuarioLog(this.usuario.getLogin());
            tpe.setTramite(this.nuevoTramiteSeleccionado);
            //tpe = this.getTramiteService().guardarYactualizarTramitePredioEnglobe(tpe);

            this.tramitesPredioEnglobeNuevoTramiteAdicionado.add(tpe);
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
                UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado);

            this.nuevoTramiteSeleccionado.setPredio(this.tramiteInicial.getPredio());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de trámite
     *
     * @author juan.agudelo
     *
     */
    public void cambioTipoTramite() {
        this.subtipoTramiteEsRequerido = false;
        this.banderaClaseDeMutacionSeleccionada = false;
        this.nuevoTramiteSeleccionado.setSubtipo(null);
        this.nuevoTramiteSeleccionado.setTramiteRectificacions(null);
        this.selectedDatosRectificarOComplementarDataTable = new ArrayList<DatoRectificar>();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Metodo que se ejecuta al cambiar el tipo de mutación
     */
    public void cambioClaseMutacion() {

        // D: si es de segunda o quinta, se hace requerido escoger el subtipo de
        // trámite; y se pone
        // el subtipo en el trámite seleccionado
        String claseMutacionSeleccionada;
        claseMutacionSeleccionada = this.nuevoTramiteSeleccionado
            .getClaseMutacion();
        if (claseMutacionSeleccionada
            .equals(EMutacionClase.SEGUNDA.getCodigo()) ||
             claseMutacionSeleccionada.equals(EMutacionClase.QUINTA
                .getCodigo())) {
            this.subtipoTramiteEsRequerido = true;
            this.banderaClaseDeMutacionSeleccionada = true;
            this.subtipoClaseMutacionV = this.generalMB
                .getSubtipoClaseMutacionV(claseMutacionSeleccionada);

            if (this.nuevoTramiteSeleccionado.getPredio() != null) {
                this.nuevoTramiteSeleccionado.setPredio(null);
            }
            if (this.nuevoTramiteSeleccionado.getPredios() != null ||
                 !this.nuevoTramiteSeleccionado.getPredios().isEmpty()) {
                this.nuevoTramiteSeleccionado.setTramitePredioEnglobes(null);
            }
        } else {
            this.subtipoTramiteEsRequerido = false;
            this.nuevoTramiteSeleccionado.setSubtipo(null);
        }
    }

    /**
     * Listener del subtipo de mutación
     */
    public void actualizarDesdeSubtipo() {

        if (this.nuevoTramiteSeleccionado.getSubtipo().equals(
            EMutacion2Subtipo.ENGLOBE.getCodigo())) {
            this.banderaSubtipoEnglobe = true;
        } else {
            this.banderaSubtipoEnglobe = false;
        }
        if (this.nuevoTramiteSeleccionado.getSubtipo().equals(
            EMutacion5Subtipo.NUEVO.getCodigo())) {
            this.banderaSubtipoNuevo = true;
        } else {
            this.banderaSubtipoNuevo = false;
        }
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * action ejecutada sobre el botón "adicionar predios al trámite" en la página de selección
     * predios
     */
    public void adicionarPrediosAlTramite() {
        List<Predio> predios = this.getPrediosSeleccionadosMapa();

        if (this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO == null ||
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.isEmpty()) {
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
                new ArrayList<TramitePredioEnglobeDTO>();
        }
        //se valida que no tenga predios repetidos
        HashSet hs = new HashSet();
        hs.addAll(predios);
        predios.clear();
        predios.addAll(hs);

        for (Predio p : predios) {
            TramitePredioEnglobeDTO tpe = new TramitePredioEnglobeDTO();
            tpe.setPredio(p);
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.add(tpe);
        }
        if (this.nuevoTramiteSeleccionado != null) {
            if (predios != null && !predios.isEmpty()) {
                if (predios.size() > 1) {
                    List<TramitePredioEnglobe> prediosTramiteList =
                        new ArrayList<TramitePredioEnglobe>();
                    for (TramitePredioEnglobeDTO dto
                        : this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO) {
                        TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                        tpe.setEnglobePrincipal(dto.getEnglobePrincipal());
                        tpe.setFechaLog(dto.getFechaLog());
                        tpe.setId(dto.getId());
                        tpe.setPredio(dto.getPredio());
                        tpe.setTramite(dto.getTramite());
                        tpe.setUsuarioLog(dto.getUsuarioLog());
                        prediosTramiteList.add(tpe);
                    }
                    this.nuevoTramiteSeleccionado
                        .setTramitePredioEnglobes(prediosTramiteList);
                    this.nuevoTramiteSeleccionado.setPredio(predios.get(0));
                } else {
                    this.nuevoTramiteSeleccionado.setTramitePredioEnglobes(null);
                    this.nuevoTramiteSeleccionado.setPredio(predios.get(0));
                }
            }
        }
    }

    /**
     * Método para devolver una lista de los predios seleccionados del mapa
     *
     * @return
     * @author javier.aponte
     */
    public List<Predio> getPrediosSeleccionadosMapa() {
        return this.consultaPredioMB.getPrediosSeleccionados2();
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la selección de predios en mutaciones de segunda englobe para buscar
     * la lista de predios colindantes
     */
    public void buscarPrediosColindantes() {

        if (this.banderaSubtipoEnglobe) {
            this.listaPrediosColindantes = this.getConservacionService()
                .buscarPrediosColindantesPorNumeroPredial(
                    this.selectedPredioTramite.getPredio()
                        .getNumeroPredial());
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la deselección de predios en mutaciones de segunda englobe para
     * inicializar la lista de predios colindantes y selección
     */
    public void reiniciarPrediosColindantes() {
        this.listaPrediosColindantes = null;
    }

    // -------------------------------------------------------------------------
    /**
     * Método utilizado sobre la selección de predio colindante para adicionarlo a la lista de
     * predios seleccionados del trámite
     */
    public void adicionarPrediosColindantes() {
        List<Predio> predios = this.getPrediosSeleccionadosMapa();

        if (this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO == null ||
             this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.isEmpty()) {
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
                new ArrayList<TramitePredioEnglobeDTO>();
        }
        predios.add(this.selectedPredioColindante);
        //se valida que no tenga predios repetidos
        HashSet hs = new HashSet();
        hs.addAll(predios);
        predios.clear();
        predios.addAll(hs);

        for (Predio p : predios) {
            TramitePredioEnglobeDTO tpe = new TramitePredioEnglobeDTO();
            tpe.setPredio(p);
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.add(tpe);
        }

        if (this.nuevoTramiteSeleccionado != null) {
            if (predios != null && !predios.isEmpty()) {
                if (predios.size() > 1) {
                    List<TramitePredioEnglobe> prediosTramiteList =
                        new ArrayList<TramitePredioEnglobe>();
                    for (TramitePredioEnglobeDTO dto
                        : this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO) {
                        TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                        tpe.setEnglobePrincipal(dto.getEnglobePrincipal());
                        tpe.setFechaLog(dto.getFechaLog());
                        tpe.setId(dto.getId());
                        tpe.setPredio(dto.getPredio());
                        tpe.setTramite(dto.getTramite());
                        tpe.setUsuarioLog(dto.getUsuarioLog());
                        prediosTramiteList.add(tpe);
                    }
                    this.nuevoTramiteSeleccionado
                        .setTramitePredioEnglobes(prediosTramiteList);
                    this.nuevoTramiteSeleccionado.setPredio(predios.get(0));
                }

            }
        }
        this.listaPrediosColindantes.remove(this.selectedPredioColindante);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Quita de la lista de predios del trámite el que se haya seleccionado dando click en el ícono
     *
     * @author pedro.garcia
     */
    public void removePredioFromListaPrediosTramite() {
        LOGGER.info("removePredioFromListaPrediosTramite action called");
        this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO.remove(this.selectedPredioTramite);
    }

//--------------------------------------------------------------------------------------------------
    public void cleanRectificacionVariables() {
        this.selectedDatosRectificarOComplementarDataTable = new ArrayList<DatoRectificar>();
        this.datosRectifComplSource = new ArrayList<DatoRectificar>();
        this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
        this.tipoDatoRectifComplSeleccionado = null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * marca uno de los predios como principal
     *
     * @author pedro.garcia
     */
    public void markTramiteAsPrincipal() {
        LOGGER.info("markTramiteAsPrincipal action called");

        for (TramitePredioEnglobe tpe : this.tramitesPredioEnglobeNuevoTramiteAdicionado) {
            tpe.setEnglobePrincipal("");
        }
        this.selectedPredioTramite.setEnglobePrincipal(ESiNo.SI.getCodigo());
    }

// --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton guardar trámite nuevo
     */
    public void guardarTramiteNuevo() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.tramitesAsociadosATramiteInicial == null) {
            this.tramitesAsociadosATramiteInicial = new ArrayList<Tramite>();
        }

        if (this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {

            this.nuevoTramiteSeleccionado
                .setTramitePredioEnglobes(UtilidadesWeb.obtenerListaTramitePredioEnglobe(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO));

        } else if (this.nuevoTramiteSeleccionado.isMutacion()) {

            this.tramitesPredioEnglobeNuevoTramiteAdicionado = new ArrayList<TramitePredioEnglobe>();
            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            tpe.setFechaLog(new Date());
            tpe.setPredio(this.tramiteInicial.getPredio());
            tpe.setUsuarioLog(this.usuario.getLogin());
            tpe.setTramite(this.nuevoTramiteSeleccionado);

            this.tramitesPredioEnglobeNuevoTramiteAdicionado.add(tpe);

            this.nuevoTramiteSeleccionado.setPredio(this.tramiteInicial.getPredio());

        }

        // Validar el contenido del trámite
        boolean tramiteEsValido = this.validarTramiteNuevo();

        if (tramiteEsValido) {

            this.nuevoTramiteSeleccionado = this.establecerDatosTramiteNuevo(
                this.nuevoTramiteSeleccionado);

            if (this.ordenEjecucionTramites == null ||
                 this.ordenEjecucionTramites.isEmpty()) {
                this.ordenEjecucionTramites = new ArrayList<SelectItem>();
                this.ordenEjecucionTramites.add(new SelectItem(1));
            } else if (!this.banderaEdicionTramiteTramite) {
                this.ordenEjecucionTramites.add(new SelectItem(
                    this.tramitesAsociadosATramiteInicial.size() + 1));
            }

            if (!this.banderaEdicionTramiteTramite) {
                this.tramitesAsociadosATramiteInicial.add(this.nuevoTramiteSeleccionado);
            }

            this.tramiteInicial.setTramites(this.tramitesAsociadosATramiteInicial);

            this.banderaEdicionTramiteTramite = false;

        } else {
            context.addCallbackParam("error", "error");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de validar los datos asociados a un trámite nuevo que se desea guardar
     *
     * @author javier.aponte
     * @return
     */
    private boolean validarTramiteNuevo() {

        //En este punto no se pueden agregar trámites nuevos de via gubernativa
        this.setTramitesGubernativaAll(null);

        //A los nuevos trámites se les asocia la solicitud del trámite inicial
        this.setSolicitudSeleccionada(this.tramiteInicial.getSolicitud());

        return this.validarDatosTramite();

    }

//--------------------------------------------------------------------------------------------
    /**
     * Método que elimina el trámite seleccionado de la lista de trámites asociados al trámite
     * seleccionado
     */
    public void removerTramiteDeTramiteTramites() {

        if (this.nuevoTramiteSeleccionado != null) {
            this.tramitesAsociadosATramiteInicial.remove(this.nuevoTramiteSeleccionado);
            this.ordenEjecucionTramites.remove(this.tramitesAsociadosATramiteInicial.size());
        }
    }
//--------------------------------------------------------------------------------------------------		

    public void editableMode() {
        this.banderaEdicionTramiteTramite = true;
        if (this.nuevoTramiteSeleccionado != null &&
            this.nuevoTramiteSeleccionado.getTramitePredioEnglobes() != null &&
             !this.nuevoTramiteSeleccionado.getTramitePredioEnglobes().isEmpty()) {

            if (this.nuevoTramiteSeleccionado.isSegundaEnglobe()) {
                this.tramitesPredioEnglobeNuevoTramiteAdicionado =
                    this.nuevoTramiteSeleccionado.getTramitePredioEnglobes();//predios asociados sin importar el tipo
                this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
                    UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
                        this.tramitesPredioEnglobeNuevoTramiteAdicionado);
            } else {
                this.tramitesPredioEnglobeNuevoTramiteAdicionado =
                    new ArrayList<TramitePredioEnglobe>();

                TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                tpe.setFechaLog(new Date());
                tpe.setPredio(this.tramiteInicial.getPredio());
                tpe.setUsuarioLog(this.usuario.getLogin());
                tpe.setTramite(this.tramiteInicial);

                this.tramitesPredioEnglobeNuevoTramiteAdicionado.add(tpe);
            }
            this.tramitesPredioEnglobeNuevoTramiteAdicionadoDTO =
                UtilidadesWeb.obtenerListaTramitePredioEnglobeDTO(
                    this.tramitesPredioEnglobeNuevoTramiteAdicionado);

        }

        if (this.nuevoTramiteSeleccionado.isRectificacion() ||
             this.nuevoTramiteSeleccionado.isEsComplementacion()) {

            List<DatoRectificar> drs = new ArrayList<DatoRectificar>();
            for (TramiteRectificacion tr : this.nuevoTramiteSeleccionado.getTramiteRectificacions()) {
                drs.add(tr.getDatoRectificar());
            }
            this.selectedDatosRectificarOComplementarDataTable = drs;
        }
    }

// ------------   rectificación/complementación  ---------------------------------------------------
    /**
     * action del botón "Adicionar datos seleccionados". Adiciona los datos seleccionados del
     * picklist de datos a rectificar o complementar a la variable que los guarda
     *
     * @author pedro.garcia
     */
    public void adicionarDatosRectifCompl() {

        LOGGER.debug("adicionando datos seleccionados...");

        // D: la primera vez que entra aquí este atributo es nulo, porque nadie
        // lo ha inicializado
        if (this.selectedDatosRectifComplPL == null) {
            this.selectedDatosRectifComplPL = new ArrayList<DatoRectificar>();
        } else {
            this.selectedDatosRectifComplPL.clear();
        }

        // OJO: no se usa la lista target, sino el atributo target del DualListModel
        if (this.datosRectifComplDLModel.getTarget().isEmpty()) {
            this.addMensajeError(
                "Debe seleccionar el tipo de rectificación o complementación, y luego los datos a modificar");
            return;
        }

        for (DatoRectificar drTarget : this.datosRectifComplDLModel.getTarget()) {

            //D: si se va a rectificar por 'cancelación por doble inscripción' este dato, 
            //   debe ser el único seleccionado
            if ((drTarget.isDatoRectifCDI() &&
                this.selectedDatosRectificarOComplementarDataTable.size() >= 1 &&
                !contieneDatoRectificarCDI(this.selectedDatosRectificarOComplementarDataTable)) ||
                 (!drTarget.isDatoRectifCDI() && contieneDatoRectificarCDI(
                this.selectedDatosRectificarOComplementarDataTable))) {

                this.addMensajeError(
                    "La rectificación de cancelación por doble inscripción debe ser única");
                this.selectedDatosRectificarOComplementarDataTable.clear();
                return;
            } else {
                if (!this.selectedDatosRectificarOComplementarDataTable.contains(drTarget)) {
                    this.selectedDatosRectifComplPL.add(drTarget);
                }
            }
        }

        this.selectedDatosRectificarOComplementarDataTable.addAll(this.selectedDatosRectifComplPL);

    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Retirar datos". Retira de la lista de datos para rectificación o
     * complementación los que se hayan seleccionado de la tabla resumen
     *
     * @author pedro.garcia
     */
    public void retirarDatoRectifCompl() {

        LOGGER.debug("retirando datos seleccionados...");

        for (DatoRectificar dtR : this.selectedDatosRectifComplDT) {
            if (this.selectedDatosRectificarOComplementarDataTable.contains(dtR)) {
                this.selectedDatosRectificarOComplementarDataTable.remove(dtR);
            }
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "aceptar" en la página de selección de datos a rectificar
     */
    public void aceptarDatosARectificar() {

        TramiteRectificacion newTramiteRectificacion;
        List<TramiteRectificacion> tramiteRectificaciones;

        if (!this.selectedDatosRectificarOComplementarDataTable.isEmpty()) {
            tramiteRectificaciones = new ArrayList<TramiteRectificacion>();
            for (DatoRectificar dr : this.selectedDatosRectificarOComplementarDataTable) {
                newTramiteRectificacion = new TramiteRectificacion();
                newTramiteRectificacion.setDatoRectificar(dr);
                newTramiteRectificacion
                    .setTramite(this.nuevoTramiteSeleccionado);
                newTramiteRectificacion.setFechaLog(new Date());
                newTramiteRectificacion.setUsuarioLog(this.usuario.getLogin());
                tramiteRectificaciones.add(newTramiteRectificacion);
            }
            this.nuevoTramiteSeleccionado.setTramiteRectificacions(tramiteRectificaciones);
        } else {
            this.addMensajeInfo(
                "Un trámite de rectificación debe tener datos a rectificar asociados");
            return;
        }
    }
//--------------------------------------------------------------------------------------------------		

    /**
     * Consultar datos a rectificar por tipo de trámite
     *
     * @author javier.aponte
     */
    public void consultarDatosARectificarPorTramiteSeleccionado() {

        if (this.selectedDatosRectificarOComplementarDataTable != null &&
            !this.selectedDatosRectificarOComplementarDataTable.isEmpty()) {
            this.selectedDatosRectificarOComplementarDataTable.clear();
        }
        if (this.nuevoTramiteSeleccionado.getTramiteRectificacions() != null &&
            !this.nuevoTramiteSeleccionado.getTramiteRectificacions().isEmpty()) {
            for (TramiteRectificacion tr : this.nuevoTramiteSeleccionado.getTramiteRectificacions()) {
                if (tr.getDatoRectificar() != null) {
                    this.selectedDatosRectificarOComplementarDataTable.add(tr.getDatoRectificar());
                }
            }
        }

    }

    // --------------------------------------------------------------------------
    public String getSelectedDepartamentoDocumento() {
        if (this.tramiteInicial != null &&
             this.tramiteInicial.getDepartamento() != null) {
            return this.tramiteInicial.getDepartamento().getCodigo();
        }
        return null;
    }

    // --------------------------------------------------------------------------
    public void setSelectedDepartamentoDocumento(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosDocumento != null) {
            for (Departamento departamento : dptosDocumento) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.documentoSeleccionado != null) {
            this.departamentoDocumento = dptoSeleccionado;
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosNombreDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenMunicipiosNombreDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     */
    public boolean isOrdenDepartamentosCodigoDocumento() {
        return this.ordenDepartamentosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    /**
     * Determina si los municipios están siendo ordenados por CODIGO
     *
     * @return
     */
    public boolean isOrdenMunicipiosCodigoDocumento() {
        return this.ordenMunicipiosDocumento.equals(EOrden.CODIGO);
    }

    // --------------------------------------------------------------------------
    public void updateDepartamentosDocumento() {
        departamentosDocumento = new LinkedList<SelectItem>();
        departamentosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.dptosDocumento = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosDocumento);
        } else {
            Collections.sort(this.dptosDocumento,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : this.dptosDocumento) {
            if (this.ordenDepartamentosDocumento.equals(EOrden.CODIGO)) {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                departamentosDocumento.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

    }
    // --------------------------------------------------------------------------

    public String getSelectedMunicipioDocumento() {
        if (this.tramiteInicial != null &&
             this.tramiteInicial.getMunicipio() != null) {
            return this.tramiteInicial.getMunicipio().getCodigo();
        }
        return null;
    }
    // --------------------------------------------------------------------------

    public void setSelectedMunicipioDocumento(String codigo) {
        @SuppressWarnings("unused")
        Municipio municipioSelected = null;
        if (codigo != null && this.munisDocumento != null) {
            for (Municipio municipio : this.munisDocumento) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioDocumento = null;
        }
    }

    // --------------------------------------------------------------------------
    public void updateMunicipiosDocumento() {
        municipiosDocumento = new ArrayList<SelectItem>();
        munisDocumento = new ArrayList<Municipio>();
        municipiosDocumento.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.documentoSeleccionado != null &&
             this.departamentoDocumento != null &&
             this.departamentoDocumento.getCodigo() != null) {
            munisDocumento = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoDocumento.getCodigo());
            if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                Collections.sort(munisDocumento);
            } else {
                Collections.sort(munisDocumento,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisDocumento) {
                if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    municipiosDocumento.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------
    public void cambiarOrdenMunicipiosDocumento() {
        if (this.ordenMunicipiosDocumento.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosDocumento = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosDocumento = EOrden.CODIGO;
        }
        this.updateMunicipiosDocumento();
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton asociar documentos desde el caso de uso de revisar trámite
     * asignado
     *
     * @author javier.aponte
     *
     */
    public void asociarDocumentacion() {

        boolean banderaDocumentoYaEstaAsociado = false;
        boolean mostrarMensaje = false;
        if (this.tramitesAsociadosATramiteInicialSeleccionados != null &&
            this.tramitesAsociadosATramiteInicialSeleccionados.length > 0) {
            if ((this.documentacionAportada != null && this.documentacionAportada.length > 0) ||
                 (this.documentacionAdicionalSeleccionada != null &&
                this.documentacionAdicionalSeleccionada.length > 0)) {

                for (Tramite tTemp : this.tramitesAsociadosATramiteInicialSeleccionados) {

                    if (this.documentacionAportada != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAportada) {
                            for (TramiteDocumentacion tTempDocReq : tTemp.
                                getDocumentacionRequerida()) {
                                if (tTempDocReq.getTipoDocumento().getNombre().equals(tdTemp.
                                    getTipoDocumento().getNombre())) {
                                    banderaDocumentoYaEstaAsociado = true;
                                }
                            }
                            if (banderaDocumentoYaEstaAsociado == false) {
                                if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                    tTemp.setTramiteDocumentacionsTemp(
                                        new ArrayList<TramiteDocumentacion>());
                                }
                                if (!tTemp.getTramiteDocumentacionsTemp()
                                    .contains(tdTemp)) {
                                    tTemp.getTramiteDocumentacionsTemp().add(
                                        tdTemp);
                                    mostrarMensaje = true;
                                }

                            } else {
                                String mensaje = "Ya habia asociado ese documento," +
                                     " por favor seleccione otro documento e intente nuevamente.";
                                this.addMensajeError(mensaje);
                            }
                        }
                    }

                    if (this.documentacionAdicionalSeleccionada != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAdicionalSeleccionada) {
                            if (tTemp.getTramiteDocumentacions() == null) {
                                tTemp.
                                    setTramiteDocumentacions(new ArrayList<TramiteDocumentacion>());
                            }
                            if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                tTemp.setTramiteDocumentacionsTemp(
                                    new ArrayList<TramiteDocumentacion>());
                            }
                            if (!tTemp.getTramiteDocumentacionsTemp()
                                .contains(tdTemp)) {
                                tTemp.getTramiteDocumentacionsTemp().add(
                                    tdTemp);
                                mostrarMensaje = true;
                            }
                        }
                    }
                }
                if (mostrarMensaje) {
                    String mensaje = "Se asoció correctamente la documentación";
                    this.addMensajeInfo(mensaje);
                }
            } else {
                String mensaje = "Debe seleccionar por lo menos un documento para" +
                     " ser asociado," +
                     " por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Debe seleccionar por lo menos un trámite para" +
                 " asociar la documentación," +
                 " por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método ejecutado sobre el boton desasociar documentos desde el caso de uso de revisar trámite
     * asignado
     *
     * @author javier.aponte
     *
     */
    public void desasociarDocumentacion() {

        if (this.tramitesAsociadosATramiteInicialSeleccionados != null &&
            this.tramitesAsociadosATramiteInicialSeleccionados.length > 0) {
            if ((this.documentacionAportada != null && this.documentacionAportada.length > 0) ||
                 (this.documentacionAdicionalSeleccionada != null &&
                this.documentacionAdicionalSeleccionada.length > 0)) {

                for (Tramite tTemp : this.tramitesAsociadosATramiteInicialSeleccionados) {

                    if (this.documentacionAportada != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAportada) {
                            if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                tTemp.setTramiteDocumentacionsTemp(
                                    new ArrayList<TramiteDocumentacion>());
                            }
                            if (tTemp.getTramiteDocumentacionsTemp()
                                .contains(tdTemp)) {
                                tTemp.getTramiteDocumentacionsTemp().remove(
                                    tdTemp);
                            }
                        }
                    }

                    if (this.documentacionAdicionalSeleccionada != null) {
                        for (TramiteDocumentacion tdTemp : this.documentacionAdicionalSeleccionada) {
                            if (tTemp.getTramiteDocumentacionsTemp() == null) {
                                tTemp.setTramiteDocumentacionsTemp(
                                    new ArrayList<TramiteDocumentacion>());
                            }
                            if (tTemp.getTramiteDocumentacionsTemp()
                                .contains(tdTemp)) {
                                tTemp.getTramiteDocumentacionsTemp().remove(
                                    tdTemp);
                            }
                        }
                    }
                }

            } else {
                String mensaje = "Debe seleccionar por lo menos un documento para" +
                     " ser desasociado," +
                     " por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Debe seleccionar por lo menos un trámite para" +
                 " desasociar la documentación," +
                 " por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Permite actualizar la lista de documentos adicionales
     */
    public void adicionarDocumentoAdicional() {
        Random rnd = new Random();
        if (this.documentacionAdicional == null ||
             this.documentacionAdicional.isEmpty()) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }
        this.documentoSeleccionado = new TramiteDocumentacion();
        TipoDocumento td = new TipoDocumento();
        this.documentoSeleccionado.setTipoDocumento(td);
        this.documentoSeleccionado.setAporta(true);
        this.documentoSeleccionado.setIdTemporal(rnd.nextLong());
        if (this.tramiteInicial != null &&
             this.tramiteInicial.getDepartamento() != null &&
             this.tramiteInicial.getDepartamento().getCodigo() != null &&
             !this.tramiteInicial.getDepartamento().getCodigo()
                .isEmpty()) {
            this.departamentoDocumento = this.tramiteInicial
                .getDepartamento();
        } else {
            this.departamentoDocumento = null;
        }
        if (this.tramiteInicial != null &&
             this.tramiteInicial.getMunicipio() != null &&
             this.tramiteInicial.getMunicipio().getCodigo() != null &&
             !this.tramiteInicial.getMunicipio().getCodigo()
                .isEmpty()) {
            this.municipioDocumento = this.tramiteInicial.getMunicipio();
        } else {
            this.municipioDocumento = null;
        }
        this.updateDepartamentosDocumento();
        this.updateMunicipiosDocumento();
        this.banderaDocumentoAdicional = true;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        File archivoResultado = null;
        this.rutaMostrar = eventoCarga.getFile().getFileName();

        try {
            archivoResultado = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.rutaMostrar);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.nombreTemporalArchivo = eventoCarga.getFile().getFileName();

            String mensaje = "El archivo seleccionado se cargo exitosamente al sistema.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            LOGGER.error("Error en ValidacionYAdicionNuevoTramiteMB#archivoUpload: " +
                e.getMessage());
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Metodo utilizado para guardar documentos adicionales y ejecutado en la ventana modal de
     * nuevoDocumentoTramite como accion sobre el boton Aceptar
     */
    public void guardarDocumentacion() {

        guardarDocumento();

        this.banderaDocumentoAdicional = false;
        this.editandoDocumentacion = false;

        this.rutaMostrar = "";

    }
//--------------------------------------------------------------------------------------------------

    public void guardarDocumento() {

        TramiteDocumentacion tdTemp = this.documentoSeleccionado;

        if (this.documentacionAdicional == null) {
            this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        }

        if (this.departamentoDocumento != null) {
            tdTemp.setDepartamento(this.departamentoDocumento);
        }
        if (this.municipioDocumento != null) {
            tdTemp.setMunicipio(this.municipioDocumento);
        }
        if (this.documentoSeleccionado != null &&
             this.documentoSeleccionado.getTipoDocumento() != null &&
             this.documentoSeleccionado.getTipoDocumento()
                .getId() != null) {
            tdTemp.setTipoDocumento(this.documentoSeleccionado
                .getTipoDocumento());
        } else if (this.documentoSeleccionado != null &&
             this.documentoSeleccionado.getTipoDocumento() != null &&
             this.documentoSeleccionado.getTipoDocumento()
                .getClase() != null) {

            if (this.documentoSeleccionado.getTipoDocumento()
                .getClase().equals(Constantes.OTRO)) {
                tdTemp.getTipoDocumento().setId(
                    ETipoDocumento.OTRO.getId());
            } else if (this.documentoSeleccionado.getTipoDocumento()
                .getClase().equals(Constantes.OFICIO)) {
                tdTemp.getTipoDocumento().setId(
                    ETipoDocumento.OFICIOS_REMISORIOS.getId());
            } else if (this.documentoSeleccionado.getTipoDocumento()
                .getClase().equals(Constantes.RESOLUCION)) {
                tdTemp.getTipoDocumento().setId(
                    ETipoDocumento.RESOLUCION_DE_CONSERVACION
                        .getId());
            }
        }
        tdTemp.setUsuarioLog(this.usuario.getLogin());
        tdTemp.setFechaLog(new Date(System.currentTimeMillis()));
        tdTemp.setCantidadFolios(this.documentoSeleccionado.getCantidadFolios());

        tdTemp.setIdTemporal(Long.valueOf(this.documentacionAdicional.size() + 1));

        Documento doc = generarDocumento(tdTemp);
        this.documentoSeleccionado.setDocumentoSoporte(doc);

        if (this.banderaDocumentoAdicional == true) {
            tdTemp.setAdicional(ESiNo.SI.getCodigo());
            tdTemp.setRequerido(ESiNo.NO.getCodigo());
            if (!this.banderaEditandoDocumentoAdicional) {
                this.documentacionAdicional.add(tdTemp);
            }
        } else {
            tdTemp.setAdicional(ESiNo.NO.getCodigo());
            tdTemp.setRequerido(ESiNo.SI.getCodigo());
        }

        this.documentoSeleccionado = new TramiteDocumentacion();

    }
//--------------------------------------------------------------------------------------------------		

    private Documento generarDocumento(TramiteDocumentacion tdTemp) {

        Documento doc = new Documento();
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioLog(this.usuario.getLogin());
        doc.setFechaLog(new Date(System.currentTimeMillis()));
        doc.setTipoDocumento(tdTemp.getTipoDocumento());
        doc.setDescripcion(this.documentoSeleccionado.getDetalle());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setNombreDeArchivoCargado(this.nombreTemporalArchivo);

        if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
            doc.setArchivo(this.rutaMostrar);
            doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
            tdTemp.setAportado(ESiNo.SI.getCodigo());
        } else if (this.documentoSeleccionado.getDocumentoSoporte() != null) {
            doc.setArchivo(this.documentoSeleccionado.getDocumentoSoporte().getArchivo());
        } else {
            doc.setArchivo("");
        }

        return doc;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al seleccionar documentos adicionales para eliminar
     *
     * @author juan.agudelo
     *
     */
    public void eliminarDocumentacionAdicional() {
        this.documentacionAdicional.remove(this.documentoSeleccionado);
    }

    public void eliminarDocumentacionAdicionalAsociarTramitesNuevos() {
        for (TramiteDocumentacion docAdicionalIterador : this.documentacionAdicional) {
            if ((docAdicionalIterador.getCantidadFolios() == this.documentoSeleccionado.
                getCantidadFolios()) && (docAdicionalIterador.getDepartamento() ==
                this.documentoSeleccionado.getDepartamento()) && (docAdicionalIterador.
                getMunicipio() == this.documentoSeleccionado.getMunicipio()) &&
                (docAdicionalIterador.getDetalle() == this.documentoSeleccionado.getDetalle()) &&
                (docAdicionalIterador.getDocumentoSoporte() == this.documentoSeleccionado.
                getDocumentoSoporte()) && (docAdicionalIterador.getTipoDocumento() ==
                this.documentoSeleccionado.getTipoDocumento())) {
                int index = this.documentacionAdicional.indexOf(docAdicionalIterador);
                this.documentacionAdicional.remove(index);
            }
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método encargado de finalizar los datos de documento temporal al cerrar la previsualización
     */
    public void cerrarDocumentoTemporalPrev() {
        // Si se queire hacer algo aca debe hacerse
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Revisa si alguno de los elementos de la lista es del tipo 'Cancelación por doble inscripción'
     *
     * @author pedro.garcia
     * @param lista
     * @return
     */
    private boolean contieneDatoRectificarCDI(List<DatoRectificar> lista) {

        boolean answer = false;

        for (DatoRectificar dr : lista) {
            if (dr.isDatoRectifCDI()) {
                answer = true;
                break;
            }
        }

        return answer;

    }

}
