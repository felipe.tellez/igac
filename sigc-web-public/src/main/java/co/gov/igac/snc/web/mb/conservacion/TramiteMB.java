package co.gov.igac.snc.web.mb.conservacion;

import org.springframework.beans.factory.annotation.Autowired;

import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB generico para presentar informacion de un tramite y/o su solicitud.
 *
 * @author fredy.wilches
 *
 */
public class TramiteMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 1657904661100413465L;
    private Tramite tramite;

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public void setTramite(Long tramiteId) {
        tramite = this.getTramiteService().consultarTramite(tramiteId);
    }

}
