package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAreaRegistrada;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EAvaluoAreaRegTipo;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * Managed bean CU-SA-AC-160 Administrar Áreas Adoptadas, el caso de uso llega desde el CU-SA-AC-015
 *
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-160
 */
@Component("administrarAreasAdoptadas")
@Scope("session")
public class AdministrarAreasAdoptadasMB extends SNCManagedBean {

    private static final long serialVersionUID = 7354425168956789572L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministrarAreasAdoptadasMB.class);
    /**
     * Avalúo asociado a las areas
     */
    private Avaluo avaluo;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;
    /**
     * Tipo de area que se selecciona para edicion
     */
    private EAvaluoAreaRegTipo tipoAreaSeleccionada;
    /**
     * Area seleccionada para edición o eliminación
     */
    private AvaluoAreaRegistrada areaSeleccionada;
    /**
     * Area seleccionada para edición o eliminación de construccion
     */
    private AvaluoAreaRegistrada areaTemCon;
    /**
     * Area seleccionada para edición o eliminación de construccion
     */
    private AvaluoAreaRegistrada areaAdoptadaSeleccionada;
    /**
     * Variable para almacenar la descripción de la fuente en caso de que no este en el dominio
     */
    private String otraFuente;
    /**
     * listas de areas asociadas al avalúo
     */
    private List<AvaluoAreaRegistrada> areasRegistradasTerreno;
    private List<AvaluoAreaRegistrada> areasRegistradasConstruccion;
    private List<AvaluoAreaRegistrada> areasAdoptadasTerreno;
    private List<AvaluoAreaRegistrada> areasAdoptadasConstruccion;
    private List<AvaluoAreaRegistrada> areasRegistradasConstruccionTotal;
    /**
     * Bandera para deternminar si la fuente del area es de tipo otro y se debe ingresar una
     * descripción especifica
     */
    private boolean banderaOtraFuente;
    /**
     * Bandera para definir si se esta modificando un área
     */
    private boolean banderaModificarArea;
    /**
     * Banderas para determinar los flujos de consulta
     */
    private boolean banderaRegistroTerreno;
    private boolean banderaRegistroConstruccion;
    /**
     * unidad de constrccion relacionadad a las áreas a registrar.
     */
    private AvaluoPredioConstruccion construccion;

    // ----------------------------Métodos SET y GET----------------------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public AvaluoAreaRegistrada getAreaAdoptadaSeleccionada() {
        return areaAdoptadaSeleccionada;
    }

    public void setAreaAdoptadaSeleccionada(AvaluoAreaRegistrada areaAdoptadaSeleccionada) {
        this.areaAdoptadaSeleccionada = areaAdoptadaSeleccionada;
    }

    public List<AvaluoAreaRegistrada> getAreasRegistradasConstruccionTotal() {
        return areasRegistradasConstruccionTotal;
    }

    public void setAreasRegistradasConstruccionTotal(
        List<AvaluoAreaRegistrada> areasAdoptadasConstruccionTotal) {
        this.areasRegistradasConstruccionTotal = areasAdoptadasConstruccionTotal;
    }

    public AvaluoPredioConstruccion getConstruccion() {
        return construccion;
    }

    public void setConstruccion(AvaluoPredioConstruccion construccion) {
        this.construccion = construccion;
    }

    public boolean isBanderaOtraFuente() {
        return banderaOtraFuente;
    }

    public void setBanderaOtraFuente(boolean banderaOtraFuente) {
        this.banderaOtraFuente = banderaOtraFuente;
    }

    public boolean isBanderaRegistroTerreno() {
        return banderaRegistroTerreno;
    }

    public void setBanderaRegistroTerreno(boolean banderaRegistroTerreno) {
        this.banderaRegistroTerreno = banderaRegistroTerreno;
    }

    public boolean isBanderaRegistroConstruccion() {
        return banderaRegistroConstruccion;
    }

    public void setBanderaRegistroConstruccion(boolean banderaRegistroConstrucion) {
        this.banderaRegistroConstruccion = banderaRegistroConstrucion;
    }

    public boolean isBanderaModificarArea() {
        return banderaModificarArea;
    }

    public void setBanderaModificarArea(boolean banderaModificarArea) {
        this.banderaModificarArea = banderaModificarArea;
    }

    public String getOtraFuente() {
        return otraFuente;
    }

    public void setOtraFuente(String otraFuente) {
        this.otraFuente = otraFuente;
    }

    public EAvaluoAreaRegTipo getTipoAreaSeleccionada() {
        return tipoAreaSeleccionada;
    }

    public void setTipoAreaSeleccionada(EAvaluoAreaRegTipo tipoAreaSeleccionada) {
        this.tipoAreaSeleccionada = tipoAreaSeleccionada;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public AvaluoAreaRegistrada getAreaSeleccionada() {
        return areaSeleccionada;
    }

    public void setAreaSeleccionada(AvaluoAreaRegistrada areaSeleccionada) {
        this.areaSeleccionada = areaSeleccionada;
    }

    public List<AvaluoAreaRegistrada> getAreasRegistradasTerreno() {
        return areasRegistradasTerreno;
    }

    public void setAreasRegistradasTerreno(List<AvaluoAreaRegistrada> areasRegistradasTerreno) {
        this.areasRegistradasTerreno = areasRegistradasTerreno;
    }

    public List<AvaluoAreaRegistrada> getAreasRegistradasConstruccion() {
        return areasRegistradasConstruccion;
    }

    public void setAreasRegistradasConstruccion(
        List<AvaluoAreaRegistrada> areasRegistradasConstruccion) {
        this.areasRegistradasConstruccion = areasRegistradasConstruccion;
    }

    public List<AvaluoAreaRegistrada> getAreasAdoptadasTerreno() {
        return areasAdoptadasTerreno;
    }

    public void setAreasAdoptadasTerreno(List<AvaluoAreaRegistrada> areasAdoptadasTerreno) {
        this.areasAdoptadasTerreno = areasAdoptadasTerreno;
    }

    public List<AvaluoAreaRegistrada> getAreasAdoptadasConstruccion() {
        return areasAdoptadasConstruccion;
    }

    public void setAreasAdoptadasConstruccion(List<AvaluoAreaRegistrada> areasAdoptadasConstruccion) {
        this.areasAdoptadasConstruccion = areasAdoptadasConstruccion;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdministrarAreasAdoptadasMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
        this.areaTemCon = new AvaluoAreaRegistrada();
        this.banderaModificarArea = false;
    }

    /**
     * Método que se usa cuando se llega desde el caso de uso 15, solo para registrar areas de
     * terreno
     *
     * @author felipe.cadena
     * @param idAvaluo
     */
    public void registrarAreasTerreno(Long idAvaluo) {
        LOGGER.debug("Iniciando...");
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.banderaOtraFuente = false;
        this.banderaRegistroConstruccion = false;
        this.banderaRegistroTerreno = true;
        this.tipoAreaSeleccionada = EAvaluoAreaRegTipo.TERRENO;
        this.areaSeleccionada = new AvaluoAreaRegistrada();
        this.consultarAreas();
    }

    /**
     * Método que se usa cuando se llega desde el caso de uso 74, solo para registrar areas de
     * construccion
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @param idUnidadConstruccion
     */
    public void registrarAreasConstruccion(Long idAvaluo, Long idUnidadConstruccion) {
        LOGGER.debug("Iniciando...");
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.construccion = this.getAvaluosService().
            consultarAvaluoPredioConstruccionPorId(idUnidadConstruccion);
        this.banderaOtraFuente = false;
        this.banderaRegistroConstruccion = true;
        this.banderaRegistroTerreno = false;
        this.tipoAreaSeleccionada = EAvaluoAreaRegTipo.TERRENO;
        this.areaSeleccionada = new AvaluoAreaRegistrada();
        this.consultarAreas();

    }

    /**
     * Método que se usa cuando se llega desde el caso de uso 19 en el cual todo es consulta
     *
     * @author felipe.cadena
     * @param idAvaluo
     */
    public void consultarAreasAdoptadas(Long idAvaluo) {
        LOGGER.debug("Iniciando...");
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.banderaOtraFuente = false;
        this.banderaRegistroConstruccion = false;
        this.banderaRegistroTerreno = false;
        this.tipoAreaSeleccionada = EAvaluoAreaRegTipo.TERRENO;
        this.areaSeleccionada = new AvaluoAreaRegistrada();
        this.consultarAreas();
    }

    /**
     * Método para consultar las areas relacionadas al avalúo
     *
     * @author felipe.cadena
     */
    public void consultarAreas() {
        LOGGER.debug("Consultando areas...");
        List<AvaluoAreaRegistrada> totalAreas = this.getAvaluosService().
            consultarAreasRegistradasPorAvaluo(this.avaluo.getId());

        this.areasAdoptadasConstruccion = new ArrayList<AvaluoAreaRegistrada>();
        this.areasAdoptadasTerreno = new ArrayList<AvaluoAreaRegistrada>();
        this.areasRegistradasConstruccion = new ArrayList<AvaluoAreaRegistrada>();
        this.areasRegistradasTerreno = new ArrayList<AvaluoAreaRegistrada>();
        this.areasRegistradasConstruccionTotal = new ArrayList<AvaluoAreaRegistrada>();

        for (AvaluoAreaRegistrada aar : totalAreas) {
            if (aar.getAdoptada().equals(ESiNo.SI.getCodigo())) {
                if (aar.getTipo().equals(EAvaluoAreaRegTipo.TERRENO.getCodigo())) {
                    this.areasAdoptadasTerreno.add(aar);
                    this.areasRegistradasTerreno.add(aar);
                } else {
                    this.areasAdoptadasConstruccion.add(aar);
                    if (this.construccion != null) {
                        if (aar.getAvaluoPredioConstruccion().getId() == this.construccion.getId()) {
                            this.areasRegistradasConstruccion.add(aar);
                        }
                    }

                    this.areasRegistradasConstruccionTotal.add(aar);
                }
            } else {
                if (aar.getTipo().equals(EAvaluoAreaRegTipo.TERRENO.getCodigo())) {
                    this.areasRegistradasTerreno.add(aar);
                } else {
                    if (this.construccion != null) {
                        if (aar.getAvaluoPredioConstruccion().getId() == this.construccion.getId()) {
                            this.areasRegistradasConstruccion.add(aar);
                        }
                    }
                    this.areasRegistradasConstruccionTotal.add(aar);
                }
            }
        }

    }

    /**
     * Metodo para crear la área del avalúo
     *
     * @author felipe.cadena
     */
    public void crearArea() {
        LOGGER.debug("Creando area ...");
        if (!this.banderaOtraFuente) {
            List<Dominio> doms = this.getGeneralesService().
                buscarDominioPorNombreOrderByNombre(EDominio.AVALUO_AREA_REG_FUENTE);
            for (Dominio dominio : doms) {
                if (dominio.getCodigo().equals(this.areaSeleccionada.getFuente())) {
                    this.areaSeleccionada.setFuenteDescripcion(dominio.getValor());
                }
            }
        } else {
            this.areaSeleccionada.setFuenteDescripcion(this.otraFuente);
        }
        if (!this.banderaModificarArea) {
            this.areaSeleccionada.setTipo(this.tipoAreaSeleccionada.getCodigo());
            this.areaSeleccionada.setAdoptada(ESiNo.NO.getCodigo());
            this.areaSeleccionada.setAvaluo(this.avaluo);
        }

        if (this.banderaRegistroConstruccion) {
            this.areaSeleccionada.setAvaluoPredioConstruccion(this.construccion);
        }

        this.areaSeleccionada = this.getAvaluosService().
            guardarActualizarAvaluoAreaRegistradaAvaluo(this.areaSeleccionada, this.usuario);
        this.areaSeleccionada = this.getAvaluosService().
            consultarAreasRegistradasPorIdConAtributo(this.areaSeleccionada.getId());

        if (!this.banderaModificarArea) {
            if (this.tipoAreaSeleccionada == EAvaluoAreaRegTipo.TERRENO) {
                this.areasRegistradasTerreno.add(this.areaSeleccionada);
            } else {
                this.areasRegistradasConstruccion.add(this.areaSeleccionada);
            }
        }
        this.areaSeleccionada = new AvaluoAreaRegistrada();
        this.otraFuente = "";
        this.banderaModificarArea = false;
    }

    /**
     * Método para modificar un area registrada
     *
     * @author felipe.cadena
     */
    public void modificarArea() {
        LOGGER.debug("modifica area...");
        this.banderaModificarArea = true;
        if (this.areaSeleccionada.getFuente().equals("0")) {
            this.otraFuente = this.areaSeleccionada.getFuenteDescripcion();
            this.banderaOtraFuente = true;
        } else {
            this.otraFuente = "";
            this.banderaOtraFuente = false;
        }

    }

    /**
     * Método para eliminar un area registrada
     *
     * @author felipe.cadena
     */
    public void eliminarArea() {
        LOGGER.debug("elimina area...");

        if (this.getAvaluosService().eliminarAvaluoAreaRegistradaAvaluo(areaSeleccionada) == null) {
            this.addMensajeError("Error al eliminar el area registrada seleccionadad " +
                 areaSeleccionada.getFuenteDescripcion());
        } else {
            this.addMensajeInfo("Area registrada eliminada con exito");
        }
        if (this.tipoAreaSeleccionada == EAvaluoAreaRegTipo.TERRENO) {
            this.areasRegistradasTerreno.remove(this.areaSeleccionada);
            this.areasAdoptadasTerreno.remove(this.areaSeleccionada);
        } else {
            this.areasRegistradasConstruccion.remove(this.areaSeleccionada);
            this.areasAdoptadasConstruccion.remove(this.areaSeleccionada);
        }
    }

    /**
     * Método adoptar el area seleccionada
     *
     * @author felipe.cadena
     */
    public void adoptarArea() {
        LOGGER.debug("adoptando area...");
        this.areaTemCon = null;

        if (this.tipoAreaSeleccionada == EAvaluoAreaRegTipo.TERRENO) {
            if (this.areasAdoptadasTerreno.size() > 0) {
                AvaluoAreaRegistrada aart = this.areasAdoptadasTerreno.get(0);
                int idx = this.areasRegistradasTerreno.indexOf(aart);
                this.areasRegistradasTerreno.get(idx).setAdoptada(ESiNo.NO.getCodigo());
                this.getAvaluosService().
                    guardarActualizarAvaluoAreaRegistradaAvaluo(this.areasRegistradasTerreno.
                        get(idx), this.usuario);
            }

            this.areasAdoptadasTerreno = new ArrayList<AvaluoAreaRegistrada>();
            this.areaSeleccionada.setAdoptada(ESiNo.SI.getCodigo());
            this.getAvaluosService().
                guardarActualizarAvaluoAreaRegistradaAvaluo(this.areaSeleccionada, this.usuario);
            this.areasAdoptadasTerreno.add(this.areaSeleccionada);

        } else {

            for (AvaluoAreaRegistrada aar : areasAdoptadasConstruccion) {
                if (aar.getAvaluoPredioConstruccion() != null) {
                    if (aar.getAvaluoPredioConstruccion().getId() == this.construccion.getId()) {
                        this.areaTemCon = aar;
                    }
                }
            }

            if (this.areaTemCon != null) {
                int idx = this.areasRegistradasConstruccion.indexOf(this.areaTemCon);
                this.areasRegistradasConstruccion.get(idx).setAdoptada(ESiNo.NO.getCodigo());
                this.getAvaluosService().
                    guardarActualizarAvaluoAreaRegistradaAvaluo(this.areasRegistradasConstruccion.
                        get(idx), this.usuario);
                this.areasAdoptadasConstruccion.remove(this.areaTemCon);
            }

            this.areaSeleccionada.setAdoptada(ESiNo.SI.getCodigo());
            this.getAvaluosService().
                guardarActualizarAvaluoAreaRegistradaAvaluo(this.areaSeleccionada, this.usuario);
            this.areasAdoptadasConstruccion.add(this.areaSeleccionada);
        }
    }

    /**
     * Listener para determinar si se trabaja con áreas de terreno o de construcción
     *
     * @author felipe.cadena
     * @param event
     */
    public void listenerCambioArea(TabChangeEvent event) {
        LOGGER.debug("cambio pestaña ... " + event.getTab().getTitle());
        String tab = event.getTab().getTitle();

        if (tab.equals(EAvaluoAreaRegTipo.TERRENO.getValor())) {
            this.tipoAreaSeleccionada = EAvaluoAreaRegTipo.TERRENO;
        } else {
            this.tipoAreaSeleccionada = EAvaluoAreaRegTipo.CONSTRUCCION;
        }

    }

    /**
     * Listener para determinar si la fuente que se va a utilizar es de tipo 'Otro'
     *
     * @author felipe.cadena
     * @param event
     */
    public void listenerSeleccionFuente() {
        LOGGER.debug("cambio fuente ... ");

        if (this.areaSeleccionada.getFuente().equals("0")) {
            this.banderaOtraFuente = true;
        } else {
            this.banderaOtraFuente = false;
        }

    }

    /**
     * Método para actualizar las lista de areas registradas cuando el caso de uso se usa como
     * consulta.
     *
     * @author felipe.cadena
     */
    public void actualizarAreasConstruccion() {

        if (!this.banderaRegistroConstruccion) {
            this.areasRegistradasConstruccion.clear();
            for (AvaluoAreaRegistrada aar : this.areasRegistradasConstruccionTotal) {
                if (aar.getAvaluoPredioConstruccion().getId() ==
                     this.areaAdoptadaSeleccionada.getAvaluoPredioConstruccion().getId()) {
                    this.areasRegistradasConstruccion.add(aar);
                }
            }
        }

    }
}
