package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoObservacion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.radicacion.ConsultaInformacionSolicitudMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * Managed bean CU-SA-AC-141 Administrar Observaciones de Cotización, el caso de uso llega desde el
 * process
 *
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-141
 */
@Component("administrarObservacionesCotizacion")
@Scope("session")
public class AdministrarObservacionesCotizacionMB extends SNCManagedBean {

    private static final long serialVersionUID = 6354951156956852572L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministrarObservacionesCotizacionMB.class);
    /**
     * Avalúo asociado a la consulta de controles de calidad
     */
    private Avaluo avaluo;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;
    /**
     * Observación seleccionada
     */
    private AvaluoObservacion observacionActual;
    /**
     * predios relacionados al avalúo
     */
    private List<AvaluoObservacion> observaciones;
    private boolean banderaEdicion;
    private boolean banderaPermiteEdicion;
    private boolean banderaCreacion;
    private boolean banderaEnvio;
    private boolean banderaAceptar;

    // ----------------------------Métodos SET y GET----------------------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isBanderaPermiteEdicion() {
        return banderaPermiteEdicion;
    }

    public void setBanderaPermiteEdicion(boolean banderaPermiteEdicion) {
        this.banderaPermiteEdicion = banderaPermiteEdicion;
    }

    public boolean isBanderaAceptar() {
        return banderaAceptar;
    }

    public void setBanderaAceptar(boolean banderaAceptar) {
        this.banderaAceptar = banderaAceptar;
    }

    public boolean isBanderaEdicion() {
        return banderaEdicion;
    }

    public void setBanderaEdicion(boolean banderaEdicion) {
        this.banderaEdicion = banderaEdicion;
    }

    public boolean isBanderaCreacion() {
        return banderaCreacion;
    }

    public void setBanderaCreacion(boolean banderaCreacion) {
        this.banderaCreacion = banderaCreacion;
    }

    public boolean isBanderaEnvio() {
        return banderaEnvio;
    }

    public void setBanderaEnvio(boolean banderaEnvio) {
        this.banderaEnvio = banderaEnvio;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public AvaluoObservacion getObservacionActual() {
        return observacionActual;
    }

    public void setObservacionActual(AvaluoObservacion observacionActual) {
        if (observacionActual != null) {
            this.observacionActual = observacionActual;
        }
    }

    public List<AvaluoObservacion> getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(List<AvaluoObservacion> observaciones) {
        this.observaciones = observaciones;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdministrarObservacionesCotizacionMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
        //this.observacionActual = new AvaluoObservacion();
        this.observaciones = new ArrayList<AvaluoObservacion>();
        this.iniciarCU(172L);
    }

    public void iniciarCU(Long idAvaluo) {
        LOGGER.debug("Iniciando...");
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.consultarObservaciones();
        this.banderaAceptar = false;
        this.banderaEdicion = false;
        this.banderaPermiteEdicion = false;
        this.banderaEnvio = false;
        this.banderaCreacion = true;
    }

    /**
     * Método para consultar las observaciones relacionadas al avalúo
     *
     * @author felipe.cadena
     */
    public void consultarObservaciones() {
        LOGGER.debug("Consultando observaciones...");
        this.observaciones = this.getAvaluosService().
            consultarObservacionesPorAvaluo(this.avaluo.getId());
        if (this.observaciones == null) {
            this.observaciones = new ArrayList<AvaluoObservacion>();
        }
    }

    /**
     * Metodo para crear la observación
     *
     * @author felipe.cadena
     */
    public void crearObservacion() {
        LOGGER.debug("Creando observacion ...");

        this.observacionActual = this.getAvaluosService().guardarActualizarObservacionesAvaluo(
            observacionActual, usuario);

        if (banderaCreacion) {
            this.observaciones.add(this.observacionActual);
        }
        LOGGER.debug("Obs  " + observacionActual.getObservacion());
        this.observacionActual = new AvaluoObservacion();
        this.banderaEdicion = false;
        this.banderaCreacion = false;
        this.banderaPermiteEdicion = false;
        this.banderaEnvio = false;
    }

    /**
     * Prepara el flujo para agregar una nueva observacion.
     *
     * @author felipe.cadena
     */
    public void adicionarObservacion() {
        LOGGER.debug("adiciona obervacion");
        this.observacionActual = new AvaluoObservacion();
        this.observacionActual.setFechaObservacion(new Date());
        this.observacionActual.setAvaluo(this.avaluo);
        this.observacionActual.setObservacion("");
        this.banderaEdicion = true;
        this.banderaCreacion = true;
        this.banderaPermiteEdicion = false;
        this.banderaEnvio = false;
    }

    /**
     * Prepara el flujo para agregar una nueva observacion.
     *
     * @author felipe.cadena
     */
    public void modificarObservacion() {
        LOGGER.debug("modifica obervacion");
        this.banderaEdicion = true;
    }

    /**
     * listener para la selección de observaciones
     *
     * @author felipe.cadena
     */
    public void listenerSeleccion(SelectEvent event) {

        LOGGER.debug("Sel --> " + this.observacionActual.getObservacion());
        this.banderaEdicion = false;

        if (this.observacionActual.getFechaEnvio() == null) {
            this.banderaPermiteEdicion = true;
            this.banderaEnvio = true;
        } else {
            this.banderaPermiteEdicion = false;
            this.banderaEnvio = false;
        }

    }

    /**
     * Método para enviar la observación por correo al profesional asignado
     *
     * @author felipe.cadena
     */
    public void enviarObservacion() {

        List<String> destinatarios = new ArrayList<String>();

        String nombresDestinatarios = "";

        for (ProfesionalAvaluo pa : this.avaluo.getAvaluadores()) {
            destinatarios.add(pa.getCorreoElectronico());
        }

        for (String string : destinatarios) {
            nombresDestinatarios = nombresDestinatarios.concat(string + "; ");
        }

        Object[] datosCorreo = new Object[4];
        datosCorreo[0] = this.avaluo.getSecRadicado();//sec. radicado
        datosCorreo[1] = this.observacionActual.getObservacion();//observacion
        datosCorreo[2] = this.usuario.getNombreCompleto();//usuario
        datosCorreo[3] = this.usuario.getRolesCadena();//rol usuario

        boolean notificacionExitosa = this
            .getGeneralesService()
            .enviarCorreoElectronicoConCuerpo(
                EPlantilla.MENSAJE_OBSERVACIONES_ELABORACION_COTIZACION,
                destinatarios, null, null, datosCorreo, null,
                null);

        if (notificacionExitosa) {
            this.observacionActual.setFechaEnvio(new Date());
            this.observacionActual = this.getAvaluosService().guardarActualizarObservacionesAvaluo(
                observacionActual, usuario);
            this.observacionActual = new AvaluoObservacion();
            this.banderaEdicion = false;
            this.banderaCreacion = false;
            this.banderaPermiteEdicion = false;
            this.banderaEnvio = false;
            this.addMensajeInfo("Se envio exitosamente la observacion por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo enviar la observación.");
            context.addCallbackParam("error", "error");
        }

//TODO :: felipe.cadena::mover el tramite para asignación de control de calidad en sede central.
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando AdministrarObservacionesCotizacionMB#cerrar");

        UtilidadesWeb.removerManagedBean("administrarObservacionesCotizacion");

        ConsultaInformacionSolicitudMB cisMB = (ConsultaInformacionSolicitudMB) UtilidadesWeb
            .getManagedBean("consultaInformacionSolicitud");
        cisMB.cerrar();

        LOGGER.debug("finalizando AdministrarObservacionesCotizacionMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
