/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.ValidacionProyeccionDelTramiteDTO;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.slf4j.LoggerFactory;

/**
 * Se implementan validaciones asociadas a los tramites de englobe virtual
 *
 * @author felipe.cadena
 */
public class ValidadorEnglobeVirtual extends SNCManagedBean {

    private static final org.slf4j.Logger LOGGER = LoggerFactory
        .getLogger(ValidadorEnglobeVirtual.class);

    private Tramite tramite;
    private List<ValidacionProyeccionDelTramiteDTO> listaErrores;
    private List<PPredio> prediosNuevos;
    private List<PPredio> prediosCompletos;
    private List<Predio> prediosEnFirme;
    private PPredio predioFichaMatriz;
    private Map<Long, String> matriculas;
    private List<String> registros;
    private List<String> nPrediales;
    private double diferenciaAreas;
    private int vigenciaActual;
    private PFichaMatriz fichaMatriz;
    private boolean validacionPreGeograficaOK;

    public enum Secciones {
        JUSTIFICACION("Justificación de propiedad"),
        PROPIETARIO("Propietarios y/o  poseedores"),
        PROYECCION("Proyección del trámite"),
        MATRICULA("Matrícula"),
        FICHA_MATRIZ("Ficha matriz"),
        INSCRIPCIONES_DECRETOS("Inscripciones y decretos"),
        DETALLE_PREDIO("Detalle del predio"),
        DETALLE_AVALUO("Detalle del avalúo"),
        GEOGRAFICO("Geográfico");

        private final String value;

        Secciones(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public ValidadorEnglobeVirtual(Tramite tramite) {
        this.tramite = tramite;
    }

    public boolean isValidacionPreGeograficaOK() {
        return validacionPreGeograficaOK;
    }

    public void setValidacionPreGeograficaOK(boolean validacionPreGeograficaOK) {
        this.validacionPreGeograficaOK = validacionPreGeograficaOK;
    }

    /**
     * Valida la informacion del tramite, teneinedo en cuenta si ya se hizo la edicion geografica
     *
     * @param postGeografico
     * @return
     */
    public List<ValidacionProyeccionDelTramiteDTO> validar(boolean postGeografico) {

        this.listaErrores = new ArrayList<ValidacionProyeccionDelTramiteDTO>();

        this.prediosNuevos = new ArrayList<PPredio>();

        this.prediosCompletos = this.getConservacionService().buscarPPrediosCompletosPorTramiteId(
            this.tramite.getId());

        this.prediosEnFirme = this.getConservacionService().obtenerPrediosCompletosPorTramiteId(
            this.tramite.getId());

        this.matriculas = new HashMap<Long, String>();

        this.registros = new ArrayList<String>();

        this.nPrediales = new ArrayList<String>();

        this.vigenciaActual = Calendar.getInstance().get(Calendar.YEAR);

        this.diferenciaAreas = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.DIFERENCIA_AREA_TERRENO_PERMITIDA.name()).getValorNumero();

        for (PPredio pp : this.prediosCompletos) {
            if (!pp.isPredioOriginalTramite()) {
                this.prediosNuevos.add(pp);
            }

            if (pp.getNumeroPredial().equals(this.tramite.obtenerInfoCampoAdicional(
                EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV))) {
                this.predioFichaMatriz = pp;
                this.fichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
                    this.predioFichaMatriz.getId());
            }

            if (pp.getCirculoRegistral() != null) {
                this.matriculas.put(pp.getId(), pp.getCirculoRegistral().getCodigo() + pp.
                    getNumeroRegistro());
            }
            if (pp.getNumeroRegistro() != null) {
                this.registros.add(pp.getNumeroRegistro());
            }
            this.nPrediales.add(pp.getNumeroPredial());
        }

        //validaciones 
        this.validarMatricula();
        this.validarTipoFicha();
        this.validarUnidadesFicha();
        this.validarNumeroPredial();
        this.validarUbicacion();
        this.validarProyeccionTramite();
        this.validarCoeficientes();
        this.validarPropietarios();
        this.validacionPreGeograficaOK = this.listaErrores.isEmpty();
        if (postGeografico) { //Solo tiene sentido validarlas si ya se realizo la edicion geografica
            this.validarReplicasGeograficas();
            this.validarDetalleAvaluo();
        }

        return listaErrores;
    }

    /**
     * Validaciones asociadas a matricula inmobiliaria
     *
     * @author felipe.cadena
     */
    private void validarMatricula() {

        List<Predio> matriculasExistentes = this.getConservacionService().
            validarExistenciaMatriculasPorMunicipio(this.registros,
                this.tramite.getPredio().getMunicipio().getCodigo());

        List<PPredio> prediosAValidar = new ArrayList<PPredio>();
        prediosAValidar.addAll(this.prediosNuevos);
        prediosAValidar.add(this.predioFichaMatriz);
        for (PPredio pp : prediosAValidar) {

            if ((pp.getNumeroRegistro() == null || pp.getNumeroRegistro().isEmpty()) ||
                 (pp.getCirculoRegistral() == null)) {

                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_016.getMensajeUsuario(pp.
                    getNumeroPredial()),
                    pp.getId(), ValidadorEnglobeVirtual.Secciones.MATRICULA.getValue());

            } else if (pp.getNumeroRegistro().equals("0")) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_018.getMensajeUsuario(pp.
                    getNumeroPredial()),
                    pp.getId(), ValidadorEnglobeVirtual.Secciones.MATRICULA.getValue());
            } else {

                for (Map.Entry es : this.matriculas.entrySet()) {
                    if (es.getValue().toString().equals(pp.getCirculoRegistral().
                        getCodigo() + pp.getNumeroRegistro()) &&
                         !pp.getId().equals(es.getKey())) {

                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_017.getMensajeUsuario(
                            pp.getNumeroPredial()),
                            pp.getId(), ValidadorEnglobeVirtual.Secciones.MATRICULA.getValue());
                        break;
                    }
                }

                for (Predio pMat : matriculasExistentes) {
                    if (pMat.getNumeroRegistro().equals(pp.getNumeroRegistro()) &&
                         !pMat.getId().equals(pp.getId())) {
                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_019.getMensajeUsuario(
                            pp.getNumeroPredial()),
                            pp.getId(), ValidadorEnglobeVirtual.Secciones.MATRICULA.getValue());
                        break;
                    }
                }
            }
        }
    }

    /**
     * Valida si el tipo de ficha es consistente con las fichas asociadas
     *
     *
     * @author felipe.cadena
     * @return
     */
    private void validarTipoFicha() {

        String tipoSeleccionado = this.predioFichaMatriz.getCondicionPropiedad();

        boolean con = false;
        boolean ph = false;
        boolean mix = false;

        for (PPredio pp : this.prediosCompletos) {
            if (!EPredioCondicionPropiedad.CP_0.getCodigo().equals(pp.getCondicionPropiedad()) &&
                 !EPredioCondicionPropiedad.CP_5.getCodigo().equals(pp.getCondicionPropiedad())) {

                if (pp.getCondicionPropiedad().
                    equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
                    ph = true;
                }
                if (pp.getCondicionPropiedad().
                    equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
                    con = true;
                }

                if (pp.getCondicionPropiedad().
                    equals(EPredioCondicionPropiedad.CP_1.getCodigo()) ||
                     (ph && con)) {
                    mix = true;
                    break;
                }

            }
        }

        if (tipoSeleccionado.equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
            if (!ph) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_021.getMensajeUsuario(),
                    null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.getValue());
            }
        }

        if (tipoSeleccionado.equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
            if (!con) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_022.getMensajeUsuario(),
                    null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.getValue());
            }
        }

        if (this.tramite.getFichaMatrizPredioTerrenos() == null ||
            this.tramite.getFichaMatrizPredioTerrenos().size() < 2) {
            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_054.getMensajeUsuario(),
                null, ValidadorEnglobeVirtual.Secciones.PROYECCION.getValue());

        }
    }

    /**
     * Valida los datos asociados a la proyeccion general
     *
     *
     * @author felipe.cadena
     */
    private void validarProyeccionTramite() {

        Map<Long, String> torres = new HashMap<Long, String>();
        Map<Long, String> torresProyectadas = new HashMap<Long, String>();
        boolean condominios[] = new boolean[9999];

        for (Predio predio : this.prediosEnFirme) {
            if (!predio.getEdificio().equals("00")) {
                torres.put(predio.getId(), predio.getEdificio());
            }
        }

        for (PPredio predio : this.prediosCompletos) {
            if (!predio.getEdificio().equals("00")) {
                torresProyectadas.put(predio.getId(), predio.getEdificio());
            }
        }

        for (PPredio pp : this.prediosCompletos) {
            String torreBase = torres.get(pp.getId());
            String torreBasePro = torresProyectadas.get(pp.getId());

            if (!torres.isEmpty()) {
                for (Entry<Long, String> en : torresProyectadas.entrySet()) {
                    if (en.getValue().equals(torreBasePro)) {
                        long predio = en.getKey();
                        if (torres.get(predio) != null && !torres.get(predio).equals(torreBase)) {
                            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_036.
                                getMensajeUsuario(pp.getNumeroPredial()),
                                null, ValidadorEnglobeVirtual.Secciones.PROYECCION.getValue());
                        }
                    }
                }
            }

        }

        int max = 0;
        for (PPredio predio : this.prediosCompletos) {
            if (!EPredioEstado.CANCELADO.getCodigo().equals(predio.getEstado()) &&
                 predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {

                int nCon = Integer.valueOf(predio.getUnidad());
                if (max < nCon) {
                    max = nCon;
                }
                condominios[nCon] = true;
            }
        }

        for (int i = 1; i < max; i++) {
            if (!condominios[i]) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_037.getMensajeUsuario(),
                    null, Secciones.PROYECCION.getValue());
                break;
            }
        }
    }

    /**
     * Valida la consistencia de las unidades asociadas a la ficha
     *
     * @author felipe.cadena
     */
    private void validarUnidadesFicha() {

        int[] nPrediosTorres = new int[100];
        int maxTorre = 0;

        String condicionFicha;
        int minCounter = 0;
        String mensaje;
        if (this.predioFichaMatriz.isMixto() || this.predioFichaMatriz.isCondominio()) {
            condicionFicha = EPredioCondicionPropiedad.CP_8.getCodigo();
            mensaje = "Condominio";
        } else {
            condicionFicha = EPredioCondicionPropiedad.CP_9.getCodigo();
            mensaje = "PH";
        }
        for (PPredio predio : this.prediosCompletos) {
            if (!predio.isEsPredioFichaMatriz() && !predio.isMejora() &&
                !EPredioCondicionPropiedad.CP_0.getCodigo().equals(predio.getCondicionPropiedad())) {
                if (predio.getCondicionPropiedad().equals(condicionFicha)) {
                    minCounter++;
                }
                nPrediosTorres[Integer.valueOf(predio.getEdificio())]++;
            }
        }

        if (minCounter < 2) {

            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_042.getMensajeUsuario(mensaje),
                null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());
        }

        for (PFichaMatrizTorre torre : this.fichaMatriz.getPFichaMatrizTorres()) {

            if (torre.getTorre() == null) {
                if (!EPredioEstado.CANCELADO.getCodigo().equals(torre.getEstado())) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_071.getMensajeUsuario(),
                        null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());
                }
            } else {
                int nTorre = torre.getTorre().intValue();
                int nUnidades = nPrediosTorres[nTorre];
                if (maxTorre < nTorre) {
                    maxTorre = nTorre;
                }
                if ((torre.getUnidades() + torre.getUnidadesSotanos()) != nUnidades) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_043.getMensajeUsuario(torre.
                        getTorre().toString()),
                        null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());

                }
            }

        }

        for (int i = 1; i <= maxTorre; i++) {
            if (nPrediosTorres[i] <= 0) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_044.getMensajeUsuario(),
                    null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());
                break;
            }
        }
    }

    /**
     * Valida los propietarios de cada uno de los predios
     *
     * @author felipe.cadena
     */
    private void validarPropietarios() {

        Date fechaBase = this.predioFichaMatriz.getFechaInscripcionCatastral();
        int maxJus = 0;

        for (PPredio predio : this.prediosCompletos) {
            if (!EPredioEstado.CANCELADO.getCodigo().equals(predio.getEstado())) {

                List<String> props = new ArrayList<String>();
                if (!predio.isEsPredioFichaMatriz() &&
                    (predio.getPPersonaPredios() == null || predio.getPPersonaPredios().isEmpty())) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_045.getMensajeUsuario(
                        predio.getNumeroPredial()),
                        null, ValidadorEnglobeVirtual.Secciones.PROPIETARIO.getValue());
                }

                if (predio.getFechaInscripcionCatastral() == null) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_055.getMensajeUsuario(
                        predio.getNumeroPredial()),
                        null, ValidadorEnglobeVirtual.Secciones.INSCRIPCIONES_DECRETOS.getValue());
                } else if (!predio.getFechaInscripcionCatastral().equals(fechaBase)) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_050.getMensajeUsuario(
                        predio.getNumeroPredial()),
                        null, ValidadorEnglobeVirtual.Secciones.INSCRIPCIONES_DECRETOS.getValue());
                }

                if (!predio.getId().equals(this.predioFichaMatriz.getId())) {
                    for (PPersonaPredio prop : predio.getPPersonaPredios()) {
                        String tipoId = prop.getPPersona().getTipoIdentificacion();
                        String nId = prop.getPPersona().getNumeroIdentificacion();
                        String propKey = null;

                        //Para tipo de identificacion cero se debe validar el nombre
                        if ("0".equals(nId)) {
                            propKey = tipoId + nId + prop.getPPersona().getNombre();
                        } else {
                            propKey = tipoId + nId;
                        }

                        if (props.contains(propKey)) {
                            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_046.
                                getMensajeUsuario(predio.getNumeroPredial()),
                                null, ValidadorEnglobeVirtual.Secciones.PROPIETARIO.getValue());
                            break;
                        } else {
                            props.add(propKey);
                        }
                        if (prop.getPPersonaPredioPropiedads() == null || prop.
                            getPPersonaPredioPropiedads().isEmpty()) {
                            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_047.
                                getMensajeUsuario(predio.getNumeroPredial()),
                                null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.getValue());
                        } else {
                            if (prop.getPPersonaPredioPropiedads().size() > maxJus) {
                                maxJus = prop.getPPersonaPredioPropiedads().size();
                            }
                            for (PPersonaPredioPropiedad ppp : prop.getPPersonaPredioPropiedads()) {
                                if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(ppp.
                                    getCancelaInscribe())) {
                                    if (ppp.getFechaTitulo() == null) {
                                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_048.
                                            getMensajeUsuario(predio.getNumeroPredial()),
                                            null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.
                                                getValue());
                                    }
                                    if (ppp.getDocumentoSoporte() == null) {
                                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_049.
                                            getMensajeUsuario(predio.getNumeroPredial()),
                                            null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.
                                                getValue());
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        //validacion de justificaciones
        if (maxJus > 0) {
            for (PPredio predio : this.prediosCompletos) {
                boolean jusOk = true;
                List<PPersonaPredioPropiedad> jusBase = null;
                if (!EPredioEstado.CANCELADO.getCodigo().equals(predio.getEstado())) {
                    if (!predio.getId().equals(this.predioFichaMatriz.getId())) {
                        for (PPersonaPredio prop : predio.getPPersonaPredios()) {

                            if (prop.getPPersonaPredioPropiedads().size() < maxJus) {
                                jusOk = false;
                            } else if (jusBase == null) {
                                jusBase = prop.getPPersonaPredioPropiedads();
                            } else {
                                for (PPersonaPredioPropiedad pppBase : jusBase) {
                                    if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(
                                        pppBase.getCancelaInscribe())) {
                                        for (PPersonaPredioPropiedad ppp : prop.
                                            getPPersonaPredioPropiedads()) {
                                            if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().
                                                equals(ppp.getCancelaInscribe())) {
                                                jusOk = this.compararJustificacion(pppBase, ppp);
                                            }
                                        }
                                    }
                                }
                                if (!jusOk) {
                                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_051.
                                        getMensajeUsuario(predio.getNumeroPredial()),
                                        null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.
                                            getValue());
                                }
                            }

                        }

                    }

                }
            }
        }

    }

    /**
     * Valida la consistencia de los coeficientes de las unidades asociadas a la ficha
     *
     * @author felipe.cadena
     */
    private void validarCoeficientes() {

        List<PFichaMatrizPredio> prediosFicha = this.getConservacionService().
            buscarFichaMatrizPredioPorPFichaMatrizId(this.fichaMatriz.getId());

        double total = 0d;

        for (PFichaMatrizPredio pfmp : prediosFicha) {
            total += pfmp.getCoeficiente();
            if (pfmp.getCoeficiente() <= 0d) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_039.getMensajeUsuario(pfmp.
                    getNumeroPredial()),
                    null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());
            }

            if (pfmp.getCoeficiente() >= 1.0d) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_040.getMensajeUsuario(pfmp.
                    getNumeroPredial()),
                    null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());
            }
        }

        if (Math.abs(total - 1) > 0.00000001) {
            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_038.getMensajeUsuario(String.
                valueOf(total)),
                null, ValidadorEnglobeVirtual.Secciones.FICHA_MATRIZ.getValue());
        }
    }

    /**
     * Realiza validaciones relacionadas al numero predial
     *
     * @author felipe.cadena
     */
    private void validarNumeroPredial() {

        List<Predio> prediosExistentes = this.getConservacionService().
            consultaPrediosPorNumeroPredial(this.nPrediales);
        List<PPredio> prediosExistentesProyectados = this.getConservacionService().
            consultaPPrediosPorNumeroPredial(this.nPrediales);

        String terreno = this.predioFichaMatriz.getPredio();
        for (PPredio pp : this.prediosCompletos) {
            if (!EPredioEstado.CANCELADO.getCodigo().equals(pp.getEstado())) {
                if (!terreno.equals(pp.getTerreno())) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_023.getMensajeUsuario(pp.
                        getNumeroPredial()),
                        null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.getValue());
                }

                //valida sobre predios en firme
                for (Predio predio : prediosExistentes) {
                    if (predio.getNumeroPredial().equals(pp.getNumeroPredial()) &&
                         !pp.getId().equals(predio.getId())) {
                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_024.getMensajeUsuario(
                            pp.getNumeroPredial()),
                            null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.getValue());
                        break;
                    }
                }

                //valida sobre predios en proyectados
                for (PPredio pPredio : prediosExistentesProyectados) {
                    if (pPredio.getNumeroPredial().equals(pp.getNumeroPredial()) &&
                         !pp.getId().equals(pPredio.getId())) {
                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_024.getMensajeUsuario(
                            pp.getNumeroPredial()),
                            null, ValidadorEnglobeVirtual.Secciones.JUSTIFICACION.getValue());
                        break;
                    }
                }
            }

        }

    }

    /**
     * Realiza validaciones relacionadas a la ubicacion
     *
     * @author felipe.cadena
     */
    private void validarUbicacion() {

        for (PPredio pp : this.prediosCompletos) {
            if (!EPredioEstado.CANCELADO.getCodigo().equals(pp.getEstado())) {
                if (pp.getPPredioDireccions() == null || pp.getPPredioDireccions().isEmpty()) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_025.getMensajeUsuario(pp.
                        getNumeroPredial()),
                        pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_PREDIO.getValue());
                }

                if (pp.getTipo() == null || pp.getTipo().trim().isEmpty()) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_026.getMensajeUsuario(pp.
                        getNumeroPredial()),
                        pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_PREDIO.getValue());
                }

                if (pp.getDestino() == null || pp.getDestino().trim().isEmpty()) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_027.getMensajeUsuario(pp.
                        getNumeroPredial()),
                        pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_PREDIO.getValue());
                }
            }
        }
    }

    /**
     * Realiza validaciones relacionadas a la ubicacion
     *
     * @author felipe.cadena
     */
    private void validarDetalleAvaluo() {

        for (PPredio pp : this.prediosCompletos) {
            if (!EPredioEstado.CANCELADO.getCodigo().equals(pp.getEstado())) {
                if (!pp.isUnidadEnPH() && (pp.getAreaTerreno() == null || pp.getAreaTerreno().
                    equals(0d))) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_028.getMensajeUsuario(pp.
                        getNumeroPredial()),
                        pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
                }

                if (!pp.getId().equals(this.predioFichaMatriz.getId())) {
                    for (Predio pFirme : this.prediosEnFirme) {
                        if (pFirme.getId().equals(pp.getId())) {
                            if (!this.validarContruccionesOriginales(pFirme, pp)) {
                                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_029.
                                    getMensajeUsuario(pp.getNumeroPredial()),
                                    pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.
                                    getValue());
                            }
                        }
                    }
                }

                for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                    if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(puc.
                        getCancelaInscribe()) ||
                         EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(puc.
                            getCancelaInscribe())) {
                        if (puc.getAvaluo() <= 0d) {
                            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_032.
                                getMensajeUsuario(puc.getUnidad(), pp.getNumeroPredial()),
                                pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.
                                getValue());
                        }
                        if (puc.getAreaConstruida() <= 0d) {
                            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_075.
                                getMensajeUsuario(puc.getUnidad(), pp.getNumeroPredial()),
                                pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.
                                getValue());
                        }
                    }

                }

                if (pp.getAvaluoCatastral() == null || pp.getAvaluoCatastral() < 0d) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_033.getMensajeUsuario(pp.
                        getNumeroPredial()),
                        pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
                }

                //validacion de avaluos y zonas historicas
                this.validarZonasHistorico(pp);

                for (PPredioAvaluoCatastral ava : pp.getPPredioAvaluoCatastrals()) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(ava.getVigencia());
                    int vigenciaAvaluo = c.get(Calendar.YEAR);
                    if (pp.getCondicionPropiedad().
                        equals(EPredioCondicionPropiedad.CP_8.getCodigo()) &&
                         vigenciaAvaluo == this.vigenciaActual &&
                         ava.getValorTerrenoPrivado() < 0d) {
                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_034.getMensajeUsuario(
                            pp.getNumeroPredial()),
                            pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
                    }
                    if (pp.getCondicionPropiedad().
                        equals(EPredioCondicionPropiedad.CP_9.getCodigo()) &&
                         vigenciaAvaluo == this.vigenciaActual &&
                         ava.getValorTerreno() < 0d) {
                        this.agregarError(ECodigoErrorVista.MODIFICACION_PH_035.getMensajeUsuario(
                            pp.getNumeroPredial()),
                            pp.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
                    }
                }

            } else {
                if (EPredioEstado.CANCELADO.getCodigo().equals(pp.getEstado()) &&
                    (pp.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0.getCodigo()) ||
                    pp.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo())) ||
                    pp.isEsPredioFichaMatriz()) {
                    for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                        if (puc.getAreaConstruida() > 0d) {
                            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_030.
                                getMensajeUsuario(),
                                null, ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
                        }
                    }
                }
            }
        }
    }

    /**
     * Valida que las construcciones de la unidades originales no se hallan modificado
     *
     * @author felipe.cadena
     */
    private boolean validarContruccionesOriginales(Predio pFirme, PPredio pProyectado) {

        boolean existe;
        for (UnidadConstruccion uc : pFirme.getUnidadConstruccions()) {
            existe = false;
            for (PUnidadConstruccion puc : pProyectado.getPUnidadConstruccions()) {

                if (uc.getUnidad().equals(puc.getUnidad()) &&
                     uc.getUsoConstruccion().getId().equals(puc.getUsoConstruccion().getId()) &&
                     uc.getAreaConstruida().equals(puc.getAreaConstruida()) &&
                     uc.getTotalPuntaje().equals(puc.getTotalPuntaje())) {

                    existe = true;
                    break;
                }
            }
            if (!existe) {
                return false;
            }
        }

        return true;
    }

    private boolean validarZonasPredio(PPredio predio) {

        Double areaTotal = 0d;
        for (PPredioZona zona : predio.getPPredioZonas()) {
            Calendar c = Calendar.getInstance();
            c.setTime(zona.getVigencia());
            int vigenciaZona = c.get(Calendar.YEAR);
            if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(zona.getCancelaInscribe()) &&
                 this.vigenciaActual == vigenciaZona) {
                areaTotal += zona.getArea();
            }
        }

        if (Math.abs(areaTotal - predio.getAreaTerreno()) > this.diferenciaAreas) {
            return false;
        }
        return true;
    }

    /**
     * Determina la consistencia de las zonas en cada vigencia
     *
     * @author felipe.cadena
     * @param predio
     */
    private void validarZonasHistorico(PPredio predio) {

        List<PPredioZona> zonasTotales = this.getConservacionService().
            buscarZonasNoCanceladaPorPPredioId(predio.getId());
        Map<Integer, List<PPredioZona>> zonasPorVigencia = new HashMap<Integer, List<PPredioZona>>();
        Map<Integer, PPredioAvaluoCatastral> avaluosPorVigencia =
            new HashMap<Integer, PPredioAvaluoCatastral>();

        Calendar vigenciaTramite = Calendar.getInstance();
        if (predio.getFechaInscripcionCatastral() == null) {
            return;
        }
        vigenciaTramite.setTime(predio.getFechaInscripcionCatastral());
        int vigenciaInicial = vigenciaTramite.get(Calendar.YEAR);
        double areaPredio = predio.getAreaTerreno();
        double sumAreaVigencia = 0d;
        boolean tipoZonaVigenciaTemp = false;

        //se inicializan zonas por vigencia
        if (zonasTotales != null && !zonasTotales.isEmpty()) {
            for (PPredioZona ppz : zonasTotales) {
                Calendar vigenciaZona = Calendar.getInstance();
                vigenciaZona.setTime(ppz.getVigencia());
                int vigencia = vigenciaZona.get(Calendar.YEAR);

                if (!zonasPorVigencia.containsKey(vigencia)) {
                    List<PPredioZona> zonas = new ArrayList<PPredioZona>();
                    zonas.add(ppz);
                    zonasPorVigencia.put(vigencia, zonas);
                } else {
                    zonasPorVigencia.get(vigencia).add(ppz);
                }
            }
        }
        //se inicializan avaluos por vigencia
        if (predio.getPPredioAvaluoCatastrals() != null && !predio.getPPredioAvaluoCatastrals().
            isEmpty()) {
            for (PPredioAvaluoCatastral ppav : predio.getPPredioAvaluoCatastrals()) {
                Calendar vigenciaAva = Calendar.getInstance();
                vigenciaAva.setTime(ppav.getVigencia());
                int vigencia = vigenciaAva.get(Calendar.YEAR);
                avaluosPorVigencia.put(vigencia, ppav);
            }
        }

        //valida area zonas por vigencia
        for (int i = this.vigenciaActual; i > vigenciaInicial; i--) {
            List<PPredioZona> zonasVigencia = zonasPorVigencia.get(i);
            sumAreaVigencia = 0;
            tipoZonaVigenciaTemp = false;
            if (!predio.isUnidadEnPH() && zonasVigencia != null && !zonasVigencia.isEmpty()) {

                for (PPredioZona ppz : zonasVigencia) {
                    sumAreaVigencia += ppz.getArea();
                    if (ppz.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo())) {
                        tipoZonaVigenciaTemp = true;
                    }
                }

                if (Math.abs(sumAreaVigencia - areaPredio) > this.diferenciaAreas &&
                     tipoZonaVigenciaTemp) {
                    this.agregarError(ECodigoErrorVista.MODIFICACION_PH_031.getMensajeUsuario(
                        String.valueOf(i), predio.getNumeroPredial()),
                        null, ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
                }

            }

            //validacion de avaluos
            PPredioAvaluoCatastral avaluoVigencia = avaluosPorVigencia.get(i);

            if (avaluoVigencia == null || avaluoVigencia.getValorTotalAvaluoCatastral() <= 0d) {
                this.agregarError(ECodigoErrorVista.MODIFICACION_PH_074.getMensajeUsuario(predio.
                    getNumeroPredial(), String.valueOf(i)),
                    predio.getId(), ValidadorEnglobeVirtual.Secciones.DETALLE_AVALUO.getValue());
            }

        }

    }

    /**
     * Determina si existen replicas 97 y 98 asociadas al tramite
     *
     * @author felipe.cadena
     */
    public void validarReplicasGeograficas() {

        if (!this.getGeneralMB().validarExistenciaCopiaEdicion(this.tramite,
            ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId(), null)) {
            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_053.getMensajeUsuario(),
                null, ValidadorEnglobeVirtual.Secciones.GEOGRAFICO.getValue());
        }

        if (!this.getGeneralMB().validarExistenciaCopiaEdicion(this.tramite,
            ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId(), null)) {
            this.agregarError(ECodigoErrorVista.MODIFICACION_PH_052.getMensajeUsuario(),
                null, ValidadorEnglobeVirtual.Secciones.GEOGRAFICO.getValue());
        }

    }

    /**
     * Determina si dos justificaciones son iguales
     *
     * @author felipe.cadena
     */
    private boolean compararJustificacion(PPersonaPredioPropiedad p1, PPersonaPredioPropiedad p2) {

        if (!p1.getFechaTitulo().equals(p2.getFechaTitulo())) {
            return false;
        }
        if (!p1.getMunicipio().getCodigo().equals(p2.getMunicipio().getCodigo())) {
            return false;
        }
        if (!p1.getNumeroTitulo().equals(p2.getNumeroTitulo())) {
            return false;
        }
        if (!p1.getTipoTitulo().equals(p2.getTipoTitulo())) {
            return false;
        }

        return true;
    }

    /**
     * Agrega un error de validacion cuando este se presenta
     *
     * @param error
     * @param nPredio
     * @param seccion
     */
    private void agregarError(String error, Long predioId, String seccion) {

        ValidacionProyeccionDelTramiteDTO valPro = new ValidacionProyeccionDelTramiteDTO();
        valPro.setMensajeError(error);
        valPro.setPredioId(predioId);
        valPro.setSeccion(seccion);

        this.listaErrores.add(valPro);

    }

}
