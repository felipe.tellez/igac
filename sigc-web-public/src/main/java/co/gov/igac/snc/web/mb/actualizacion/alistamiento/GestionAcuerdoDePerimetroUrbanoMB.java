package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionPerimetroUrbano;
import co.gov.igac.snc.persistence.entity.actualizacion.PerimetroUrbanoNorma;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @cu205 gestionar Acuerdo para la actualizaciond de Perimetro Urbano en el procesod e
 * actualizacion.
 *
 * @author andres.eslava
 */
@Component("gestionarAcuerdoDePerimetroUrbano")
@Scope("session")
public class GestionAcuerdoDePerimetroUrbanoMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionAcuerdoDePerimetroUrbanoMB.class);

    private static final long serialVersionUID = -2964745655243791827L;

    private String normaSeleccionadaLista;

    /**
     * Normas a incluir en el proyeto de
     */
    private List<String> normasTabla;
    /**
     * Norma seleccionada
     */
    private String selectedNorma;

    /**
     * Nombre del alcalde al que va dirigido el oficio.
     */
    private String nombreAlcalde;

    /**
     * Servicio para la generacion del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Archivo generado para oficio con el servicio de reportes.
     */
    private ReporteDTO oficioDto;

    /**
     * Archivo generado para proyecto con el servicio de reportes.
     */
    private ReporteDTO proyectoDto;

    /**
     * Usuario autenticado en el sistema.
     */
    private UsuarioDTO usuario;

    /**
     * Contexto del servidor de aplicaciones
     */
    @Autowired
    private IContextListener context;

    /**
     * Marca si el reporte se genero correctamente
     */
    private boolean reporteGeneradoOK;

    /**
     * Identificador del oficio
     */
    private String urlArchivo;

    /**
     * marca si el perimetro esta generado
     *
     */
    private boolean perimetroCreado;

    /**
     * Establece si el formulario fue diligenciado
     */
    private Boolean formularioDiligenciado;
    // TODO
    // Inyeccion de MB que tiene el plano y los puntos.
    // @Autowired
    // private **********

    /**
     * Documento Asociado a oficio de gestion perimetro urbano
     */
    private Documento documentoOficioProyectoPerimetroUrbano;

    /**
     * Documento asociado a proyecto de gestion perimetro urbano
     */
    private Documento documentoProyectoPerimetroUrbano;

    /**
     * Actualizacion asociada
     */
    private Actualizacion actualizacionAsociada;

    /**
     * Número Radico Oficio
     *
     */
    private String numeroRadicadoOficio;

    /**
     * Número Radico Proyecto
     */
    private String numeroRadicadoProyecto;

    /**
     * Bandera valida que esta radicando y persistiendo los documentos
     */
    private Boolean banderaRadicandoPersistiendo;

    // **************************************************************************************************************************
    // Getters y Setters
    // **************************************************************************************************************************
    public Actualizacion getActualizacionAsociada() {
        return actualizacionAsociada;
    }

    public void setActualizacionAsociada(Actualizacion actualizacionAsociada) {
        this.actualizacionAsociada = actualizacionAsociada;
    }

    public Boolean getBanderaRadicandoPersistiendo() {
        return banderaRadicandoPersistiendo;
    }

    public void setBanderaRadicandoPersistiendo(Boolean banderaRadicandoPersistiendo) {
        this.banderaRadicandoPersistiendo = banderaRadicandoPersistiendo;
    }

    public String getNumeroRadicadoOficio() {
        return numeroRadicadoOficio;
    }

    public void setNumeroRadicadoOficio(String numeroRadicadoOficio) {
        this.numeroRadicadoOficio = numeroRadicadoOficio;
    }

    public String getNumeroRadicadoProyecto() {
        return numeroRadicadoProyecto;
    }

    public void setNumeroRadicadoProyecto(String numeroRadicadoProyecto) {
        this.numeroRadicadoProyecto = numeroRadicadoProyecto;
    }

    public Documento getDocumentoOficioProyectoPerimetroUrbano() {
        return documentoOficioProyectoPerimetroUrbano;
    }

    public void setDocumentoOficioProyectoPerimetroUrbano(
        Documento documentoOficioProyectoPerimetroUrbano) {
        this.documentoOficioProyectoPerimetroUrbano = documentoOficioProyectoPerimetroUrbano;
    }

    public Documento getDocumentoProyectoPerimetroUrbano() {
        return documentoProyectoPerimetroUrbano;
    }

    public void setDocumentoProyectoPerimetroUrbano(
        Documento documentoProyectoPerimetroUrbano) {
        this.documentoProyectoPerimetroUrbano = documentoProyectoPerimetroUrbano;
    }

    public String getNormaSeleccionadaLista() {
        return normaSeleccionadaLista;
    }

    public void setNormaSeleccionadaLista(String normaSeleccionadaLista) {
        this.normaSeleccionadaLista = normaSeleccionadaLista;
    }

    public List<String> getNormasTabla() {
        return normasTabla;
    }

    public void setNormasTabla(List<String> normasTabla) {
        this.normasTabla = normasTabla;
    }

    public String getSelectedNorma() {
        return selectedNorma;
    }

    public String getNombreAlcalde() {
        return nombreAlcalde;
    }

    public void setNombreAlcalde(String nombreAlcalde) {
        this.nombreAlcalde = nombreAlcalde;
    }

    public void setSelectedNorma(String selectedNorma) {
        this.selectedNorma = selectedNorma;
    }

    public String getUrlArchivo() {
        return urlArchivo;
    }

    public void setUrlArchivo(String urlArchivo) {
        this.urlArchivo = urlArchivo;
    }

    public ReportesUtil getReportsService() {
        return reportsService;
    }

    public void setReportsService(ReportesUtil reportsService) {
        this.reportsService = reportsService;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public IContextListener getContext() {
        return context;
    }

    public void setContext(IContextListener context) {
        this.context = context;
    }

    public boolean isReporteGeneradoOK() {
        return reporteGeneradoOK;
    }

    public void setReporteGeneradoOK(boolean reporteGeneradoOK) {
        this.reporteGeneradoOK = reporteGeneradoOK;
    }

    public boolean isPerimetroCreado() {
        return perimetroCreado;
    }

    public void setPerimetroCreado(boolean perimetroCreado) {
        this.perimetroCreado = perimetroCreado;
    }

    public Boolean getFormularioDiligenciado() {
        return formularioDiligenciado;
    }

    public void setFormularioDiligenciado(Boolean formularioDiligenciado) {
        this.formularioDiligenciado = formularioDiligenciado;
    }

    public ReporteDTO getOficioDto() {
        return oficioDto;
    }

    public void setOficioDto(ReporteDTO oficioDto) {
        this.oficioDto = oficioDto;
    }

    public ReporteDTO getProyectoDto() {
        return proyectoDto;
    }

    public void setProyectoDto(ReporteDTO proyectoDto) {
        this.proyectoDto = proyectoDto;
    }

    // **************************************************************************************************************************
    //  Bussines and Screen methods
    // **************************************************************************************************************************
    @PostConstruct
    public void init() {
        iniciarVaribles();
    }

    private void iniciarVaribles() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        normasTabla = new ArrayList<String>();
        normaSeleccionadaLista = new String();
        reporteGeneradoOK = false;
        nombreAlcalde = new String();
        validarExistenciaPerimetro();
        this.banderaRadicandoPersistiendo = Boolean.FALSE;

        // javier.aponte::28-11-2012::datos de prueba por falta de integracion::javier.aponte
        this.establecerActualizacionAsociadaActualizacionPerimetroUrbano(507L);
    }

    /**
     * Valida que un perimetro creado en el editor geografico exista.
     *
     * @author andres.eslava
     *
     */
    private void validarExistenciaPerimetro() {
        // TODO javier.aponte::26-11-12::debe verificar la existencia del plano y los puntos::andres.eslava
        // condicion sobre el manage bean generado CU actualizacion 217
        perimetroCreado = true;
    }

    /**
     * Agrega la norma seleccionada en vista.
     *
     * @author andres.eslava
     */
    public void agregarNorma() {
        normasTabla.add(this.normaSeleccionadaLista);
    }

    /**
     * Remueve la norma de la lista de normas adicionadas
     *
     * @return
     *
     * @author andres.eslava
     */
    public void desvincularNorma() {
        try {
            normasTabla.remove(this.selectedNorma);
        } catch (Exception e) {
            this.addMensajeError("Error eliminando la norma de la lista.");
            LOGGER.error("Error eliminando la norma de la lista.", e);
        }
    }

    /**
     * Genera el documento correspondiente al oficio de remisión del acuerdo de perimetro urbano.
     *
     * @param
     * @author andres.eslava
     */
    @SuppressWarnings("deprecation")
    public void proyectarOficioAcuerdoPerimetroUrbano() {
        this.documentoOficioProyectoPerimetroUrbano = new Documento();
        try {
            //Obtiene la url del reporte
            String urlReporte = EReporteServiceSNC.OFICIO_GESTION_ACUERDO_PERIMETRO_URBANO.
                getUrlReporte();
            //Se establece los parametros del reporte		
            Map<String, String> parameters = new HashMap<String, String>();

            if (banderaRadicandoPersistiendo) {
                parameters.put("NUMERO_RADICACION", this.obtenerNumeroRadicado());
            }

            this.validaFormularioCompleto();
            if (this.formularioDiligenciado) {
                parameters.put("COPIA_USO_RESTRINGIDO", "true");
            } else {
                parameters.put("COPIA_USO_RESTRINGIDO", "false");
            }

            this.formularioDiligenciado = Boolean.FALSE;

            parameters.put("NOMBRE_ALCALDE", this.getNombreAlcalde());
            parameters.put("GENERADO_POR", this.usuario.getNombreCompleto());
            parameters.put("NUMERO_RADICADO", this.documentoOficioProyectoPerimetroUrbano.
                getNumeroRadicacion());
            parameters.put("ACTUALIZACION_ID", this.actualizacionAsociada.getId().toString());
            parameters.put("ROL_USUARIO", this.usuario.getRolesCadena());

            // Genera el reporte
            this.oficioDto = reportsService.generarReporte(parameters,
                EReporteServiceSNC.OFICIO_GESTION_ACUERDO_PERIMETRO_URBANO, this.usuario);

            //Establece el tipo de documento
            TipoDocumento tipoDocumentoProyecto = new TipoDocumento();
            tipoDocumentoProyecto.setId(ETipoDocumento.OFICIOS_REMISORIOS.getId());

            // Determinan la entidad documento
            documentoOficioProyectoPerimetroUrbano.setArchivo(this.oficioDto.getArchivoReporte().
                getName());
            documentoOficioProyectoPerimetroUrbano.setTipoDocumento(tipoDocumentoProyecto);
            documentoOficioProyectoPerimetroUrbano.
                setEstado(EDocumentoEstado.SIN_CARGAR.getCodigo());
            documentoOficioProyectoPerimetroUrbano.setBloqueado(ESiNo.NO.getCodigo());
            documentoOficioProyectoPerimetroUrbano.setUsuarioCreador(this.usuario.getLogin());
            documentoOficioProyectoPerimetroUrbano.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documentoOficioProyectoPerimetroUrbano.setFechaLog(new Date(System.currentTimeMillis()));
            documentoOficioProyectoPerimetroUrbano.setUsuarioLog(this.usuario.getLogin());
            documentoOficioProyectoPerimetroUrbano.setDescripcion(
                "oficio remisión Proyecto de gestion de perimetro urbano para el municipio");

            PrevisualizacionDocMB previsualizacion = (PrevisualizacionDocMB) UtilidadesWeb.
                getManagedBean("previsualizacionDocumento");

            previsualizacion.setReporteDTO(this.oficioDto);
        } catch (Exception e) {
            this.addMensajeError(
                "Error en la generacion del oficio remisorio para la previsualizacion");
            LOGGER.error("Error en la generacion del oficio remisorio para la previsualizacion", e);
        }
    }

    /**
     * Genera el documento proyecto de perimetro urbano
     *
     * @author andres.eslava
     */
    public void proyectarProyectoPerimetroUrbano() {

        this.documentoProyectoPerimetroUrbano = new Documento();
        try {
            String urlReporte = EReporteServiceSNC.PROYECTO_GESTION_ACUERDO_PERIMETRO_URBANO
                .getUrlReporte();
            LOGGER.debug("Url Reporte:" + urlReporte);
            Map<String, String> parameters = new HashMap<String, String>();

            this.validaFormularioCompleto();

            if (this.formularioDiligenciado) {
                parameters.put("COPIA_USO_RESTRINGIDO", "false");
            } else {
                parameters.put("COPIA_USO_RESTRINGIDO", "true");
            }
            if (banderaRadicandoPersistiendo) {
                parameters.put("NUMERO_RADICADO", this.obtenerNumeroRadicado());
            }

            this.formularioDiligenciado = Boolean.FALSE;

            parameters.put("ACTUALIZACION_ID", this.actualizacionAsociada.getId().toString());
            parameters.put("NOMBRE_ALCALDE", this.nombreAlcalde);

            this.proyectoDto = reportsService.generarReporte(parameters,
                EReporteServiceSNC.PROYECTO_GESTION_ACUERDO_PERIMETRO_URBANO, this.usuario);

            TipoDocumento tipoDocumentoProyecto = new TipoDocumento();
            tipoDocumentoProyecto.setId(ETipoDocumento.PROYECTO_GESTION_PERIMETRO_URBANO.getId());

            documentoProyectoPerimetroUrbano.setArchivo(this.proyectoDto
                .getArchivoReporte().getName());
            documentoProyectoPerimetroUrbano
                .setTipoDocumento(tipoDocumentoProyecto);
            documentoProyectoPerimetroUrbano
                .setEstado(EDocumentoEstado.SIN_CARGAR.getCodigo());
            documentoProyectoPerimetroUrbano.setBloqueado(ESiNo.NO.getCodigo());
            documentoProyectoPerimetroUrbano.setUsuarioCreador(this.usuario
                .getLogin());
            documentoProyectoPerimetroUrbano
                .setEstructuraOrganizacionalCod(this.usuario
                    .getCodigoEstructuraOrganizacional());
            documentoProyectoPerimetroUrbano.setFechaLog(new Date(System
                .currentTimeMillis()));
            documentoProyectoPerimetroUrbano.setUsuarioLog(this.usuario
                .getLogin());
            documentoProyectoPerimetroUrbano
                .setDescripcion("Proyecto de gestion de perimetro urbano");

            PrevisualizacionDocMB previsualizacion = (PrevisualizacionDocMB) UtilidadesWeb
                .getManagedBean("previsualizacionDocumento");

            previsualizacion.setReporteDTO(this.proyectoDto);
        } catch (Exception e) {
            this.addMensajeError("Error en la generacion del proyecto para la previsualizacion");
            LOGGER.error("Error en la generacion del proyecto para la previsualizacion", e);
        }
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     */
    private String obtenerNumeroRadicado() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add("800"); // Territorial=compania
        parametros.add("SC_SNC"); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add("1"); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add(""); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add("800"); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add("5000"); // Destino lugar
        parametros.add(""); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(""); // Direccion destino
        parametros.add(""); // Tipo identificacion destino
        parametros.add(""); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    /**
     *
     * @author andres.eslava
     */
    public void proyectarPlanoAcuerdoPerimetroRegistrado() {
    }

    /**
     * Radica en correspondencia y registra en el servidor de archivos los documentos asociados a la
     * actualización de perimetro urbano
     *
     * @author andres.eslava
     */
    public void radicarRegistrarProyectoPerimetroUrbano() {

        // TODO javier.aponte::26-11-2012::Definicion de puntos plano perimetro::andres.eslava
        this.validaFormularioCompleto();
        if (this.getFormularioDiligenciado()) {
            try {
                ActualizacionPerimetroUrbano actualizacionPerimetroUrbano =
                    new ActualizacionPerimetroUrbano();

                // Genera el reporte a persistir
                banderaRadicandoPersistiendo = Boolean.TRUE;
                this.proyectarOficioAcuerdoPerimetroUrbano();
                this.proyectarPlanoAcuerdoPerimetroRegistrado();
                this.proyectarProyectoPerimetroUrbano();

                this.documentoOficioProyectoPerimetroUrbano = this.getActualizacionService()
                    .guardarDocumentoDeActualizacionEnAlfresco(
                        this.documentoOficioProyectoPerimetroUrbano,
                        this.actualizacionAsociada, this.usuario);

                this.documentoProyectoPerimetroUrbano = this.getActualizacionService()
                    .guardarDocumentoDeActualizacionEnAlfresco(
                        this.documentoProyectoPerimetroUrbano,
                        this.actualizacionAsociada, this.usuario);

                banderaRadicandoPersistiendo = Boolean.FALSE;

                //Persiste el proyecto y sus  normas asociadas
                actualizacionPerimetroUrbano.setAcuerdoDocumento(
                    this.documentoProyectoPerimetroUrbano);
                actualizacionPerimetroUrbano.setOficioDocumento(
                    this.documentoOficioProyectoPerimetroUrbano);
                actualizacionPerimetroUrbano.setNombreAlcalde(this.nombreAlcalde);
                actualizacionPerimetroUrbano.setActualizacionId(this.actualizacionAsociada.getId());
                actualizacionPerimetroUrbano.setUsuarioLog(this.usuario.getNombreCompleto());
                actualizacionPerimetroUrbano.setFechaLog(new Date(System.currentTimeMillis()));

                actualizacionPerimetroUrbano = this.getActualizacionService().
                    guardarActualizacionPerimetroUrbano(actualizacionPerimetroUrbano, this.usuario);

                for (String norma : this.normasTabla) {
                    PerimetroUrbanoNorma perimetroUrbanoNorma = new PerimetroUrbanoNorma();
                    perimetroUrbanoNorma.setActualizacionPerimetroUrbId(
                        actualizacionPerimetroUrbano.getId());
                    List<Dominio> normasVigentesExistentes = this.getGeneralesService().
                        getCacheDominioPorNombre(EDominio.NORMAS_VIGENTES);
                    for (Dominio dominio : normasVigentesExistentes) {
                        if (dominio.getCodigo().equals(norma)) {
                            perimetroUrbanoNorma.setNorma(dominio.getCodigo());
                            perimetroUrbanoNorma.setDescripcion(dominio.getDescripcion());
                        }
                    }
                    this.getActualizacionService().guardarPerimetroUrbanoNorma(perimetroUrbanoNorma,
                        usuario);
                }
                this.addMensajeInfo("La actualización fue radicada y registrada con exito.");
                LOGGER.info("La actualización fue radicada y registrada con exito.");
            } catch (Exception e) {
                this.addMensajeError(
                    "Error en la radicacion y registro de la actualizacion de perimetro urbano.");
                LOGGER.error(
                    "Error en la radicacion y registro de la actualizacion de perimetro urbano.", e);
            }

        } else {
            this.addMensajeError("Debe agregar las normas y Digitar el nombre del alcalde");
            LOGGER.warn(
                "Debe agregar las normas y Digitar el nombre del alcalde: this.getFormularioDiligenciado() false");
        }
    }

    /**
     * Valida que el formulario normas y nombre del alcalde esten debidamente diligenciado.
     *
     * @author andres.eslava
     */
    public void validaFormularioCompleto() {
        if (this.nombreAlcalde.isEmpty() || this.normasTabla.isEmpty()) {
            this.formularioDiligenciado = false;
        } else {
            this.formularioDiligenciado = true;
        }
    }

    /**
     * Establece la actualización asociada al proceso de actualizacion de perimetro urbano.
     *
     * @param idActualzacion
     * @author andres.eslava
     */
    public void establecerActualizacionAsociadaActualizacionPerimetroUrbano(Long idActualzacion) {
        this.actualizacionAsociada = this.getActualizacionService().
            obtenerActualizacionConMunicipio(idActualzacion);
    }

}
