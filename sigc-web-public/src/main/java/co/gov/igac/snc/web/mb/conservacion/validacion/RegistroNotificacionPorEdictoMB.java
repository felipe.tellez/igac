/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.IServicioTemporalDocumento;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.ServicioTemporalDocumento;
import co.gov.igac.snc.web.util.procesos.ProcesosUtilidadesWeb;

/**
 * Managed bean para el CU Registrar notificación
 *
 * @author pedro.garcia
 */
//TODO:: este MB es igual al de RegistroNotificacionMB falta, una vez terminado el otro, hacer copy
// y paste de lo que falte (mejor hacerlo de todo), teniendo en cuenta los archivos que son diferentes
//FIXME::
@Component("registroNotificacionPorEdicto")
@Scope("session")
public class RegistroNotificacionPorEdictoMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = 296199969745989266L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        RegistroNotificacionPorEdictoMB.class);

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contexto;

    private IServicioTemporalDocumento servicioTemporalDocumento;

    /**
     * trámite sober el que se está trabajando
     */
    private Tramite currentTramite;

    /**
     * id del trámite seleccionado de la tabla
     */
    private long currentTramiteId;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    //----------     banderas   ---------
    /**
     * indica si el notificado es solicitante del trámite
     */
    private boolean notificadoEsSolicitante;

    /**
     * indica si el botón "guardar" que mueve el proceso y guarda los doc cargados se debe habilitar
     */
    private boolean disabledGuardarButton;

    /**
     * indican si los respectivos documentos se cargaron ya
     */
    private boolean docNotifCargado;
    private boolean docAutorizacionCargado;

    /**
     * valor del combo de tipo de persona notificada
     */
    private String tipoPersonaNotif;
    public static String tipoPersonaNotifSolicitante = "solicitante";
    public static String tipoPersonaNotifAutorizado = "autorizado";

    /**
     * lista completa de solicitantes del trámite: los de la solicitud y los de trámite
     */
    private ArrayList<Solicitante> solicitantesRegistroNotificacion;

    //-----------    atributos con valores a guardar   ------
    private String tipoIdentificPersonaNotif;

    private String nombrePersonaNotif;

    private String numeroIdentificPersonaNotif;

    private String renunciaARecursos;

    private String formaNotificacion;

    private Date fechaNotificacion;

    /**
     * solicitante seleccionado de la tabla
     */
    private Solicitante selectedSolicitante;

    //---------      carga de archivos  ------
    /**
     * nombre del tipo de archivo que se carga 'notificacion', 'autorizacion'
     */
    private String archivoACargar;

    private String nombreArchivoNotificacionCargado;

    private String nombreArchivoAutorizacionCargado;

    /**
     * objetos Documento que se van a guardar en la bd
     */
    private Documento documentoNotificacion;
    private Documento documentoAutorizacionDeNotificacion;

    private static String tipoDocumentoNotificacion = "notificacion";
    private static String tipoDocumentoAutorizacionDeNotificacion = "autorizacion";

    //-----   BPM  --------
    private Actividad currentProcessActivity;

    //--------------  methods   ---------------
    public Date getFechaNotificacion() {
        return this.fechaNotificacion;
    }

    public void setFechaNotificacion(Date fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    public String getNombreArchivoNotificacionCargado() {
        return this.nombreArchivoNotificacionCargado;
    }

    public void setNombreArchivoNotificacionCargado(String nombreArchivoNotificacionCargado) {
        this.nombreArchivoNotificacionCargado = nombreArchivoNotificacionCargado;
    }

    public String getNombreArchivoAutorizacionCargado() {
        return this.nombreArchivoAutorizacionCargado;
    }

    public void setNombreArchivoAutorizacionCargado(String nombreArchivoAutorizacionCargado) {
        this.nombreArchivoAutorizacionCargado = nombreArchivoAutorizacionCargado;
    }

    public String getArchivoACargar() {
        return this.archivoACargar;
    }

    public void setArchivoACargar(String archivoACargar) {
        this.archivoACargar = archivoACargar;
    }

    public boolean isDisabledGuardarButton() {
        return this.disabledGuardarButton;
    }

    public void setDisabledGuardarButton(boolean disabledGuardarButton) {
        this.disabledGuardarButton = disabledGuardarButton;
    }

    public Solicitante getSelectedSolicitante() {
        return this.selectedSolicitante;
    }

    public void setSelectedSolicitante(Solicitante selectedSolicitante) {
        this.selectedSolicitante = selectedSolicitante;
    }

    public String getTipoPersonaNotifSolicitante() {
        return RegistroNotificacionPorEdictoMB.tipoPersonaNotifSolicitante;
    }

    public String getTipoPersonaNotifAutorizado() {
        return RegistroNotificacionPorEdictoMB.tipoPersonaNotifAutorizado;
    }

    public Tramite getCurrentTramite() {
        return this.currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
    }

    public ArrayList<Solicitante> getSolicitantesRegistroNotificacion() {
        return this.solicitantesRegistroNotificacion;
    }

    public void setSolicitantesRegistroNotificacion(
        ArrayList<Solicitante> solicitantesRegistroNotificacion) {
        this.solicitantesRegistroNotificacion = solicitantesRegistroNotificacion;
    }

    public String getTipoIdentificPersonaNotif() {

        return this.tipoIdentificPersonaNotif;
    }

    public void setTipoIdentificPersonaNotif(String tipoIdentificPersonaNotif) {
        this.tipoIdentificPersonaNotif = tipoIdentificPersonaNotif;
    }

    public String getNombrePersonaNotif() {
        return this.nombrePersonaNotif;
    }

    public void setNombrePersonaNotif(String nombrePersonaNotif) {
        this.nombrePersonaNotif = nombrePersonaNotif;
    }

    public String getNumeroIdentificPersonaNotif() {
        return this.numeroIdentificPersonaNotif;
    }

    public void setNumeroIdentificPersonaNotif(String numeroIdentificPersonaNotif) {
        this.numeroIdentificPersonaNotif = numeroIdentificPersonaNotif;
    }

    public String getRenunciaARecursos() {
        return this.renunciaARecursos;
    }

    public void setRenunciaARecursos(String renunciaARecursos) {
        this.renunciaARecursos = renunciaARecursos;
    }

    public String getFormaNotificacion() {
        return this.formaNotificacion;
    }

    public void setFormaNotificacion(String formaNotificacion) {
        this.formaNotificacion = formaNotificacion;
    }

    public boolean isNotificadoEsSolicitante() {
        return this.notificadoEsSolicitante;
    }

    public void setNotificadoEsSolicitante(boolean notificadoEsSolicitante) {
        this.notificadoEsSolicitante = notificadoEsSolicitante;
    }

    public String getTipoPersonaNotif() {
        return this.tipoPersonaNotif;
    }

    /**
     * dependiendo de lo que escoja en el combo se cambia el valor de la bandera
     */
    public void setTipoPersonaNotif(String tipoPersonaNotif) {
        this.tipoPersonaNotif = tipoPersonaNotif;

        if (this.tipoPersonaNotif.
            equals(RegistroNotificacionPorEdictoMB.tipoPersonaNotifSolicitante)) {
            this.notificadoEsSolicitante = true;
        } else {
            this.notificadoEsSolicitante = false;
            this.selectedSolicitante = null;
            this.numeroIdentificPersonaNotif = null;
            this.nombrePersonaNotif = null;
        }
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on RegistroNotificacionMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "registroNotificacionPorEdicto");

//FIXME::uncomment when integrated with activities tree
        //D: se obtienen el id del trámite objetivo a partir del árbol de tareas
//        this.currentTramiteId =
//	            this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();
//        this.currentTramite =
//            this.tareasPendientesMB.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();
//        this.currentProcessActivity = this.tareasPendientesMB.
//    		getActividadesSeleccionadas().buscarActividadPorIdTramite(this.currentTramite.getId());
        this.currentTramiteId = 96l;

        //D: buscar los trámites
//TODO::determinar si puedo usar un método menos pesado para traer los datos del trámite que necesito.
// depende de si este trámite es el que se usa en los detalles de solicitud
        this.currentTramite =
            this.getTramiteService().buscarTramiteSolicitudPorId(this.currentTramiteId);

        this.solicitantesRegistroNotificacion = (ArrayList<Solicitante>) this.getTramiteService().
            obtenerSolicitantesTodosDeTramite(
                this.currentTramiteId, this.currentTramite.getSolicitud().getId(),
                this.currentTramite.getSolicitud().getTipo());

        LOGGER.debug("RegistroNotificacionMB initalized. ");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener se selección en la tabla de solicitantes actualiza los valores de los componentes
     * que guardan los datos de la persona notificada
     *
     */
    public void updateValoresPersonaNotificada(SelectEvent event) {
        this.tipoIdentificPersonaNotif = this.selectedSolicitante.getTipoIdentificacion();
        this.numeroIdentificPersonaNotif = this.selectedSolicitante.getNumeroIdentificacion();
        this.nombrePersonaNotif = this.selectedSolicitante.getNombreCompleto();
    }
//--------------------------------------------------------------------------------------------------}

    /**
     * action del botón con el mismo nombre
     *
     * 1. verifica que se hayan cargado los archivos necesarios 2. guarda los Documento que se
     * requiera (notificación y/o autorización) 3. guarda los TramiteDocumentacion correspondientes
     * 4. avanza el proceso
     *
     */
    public String guardarRegistroNotificacion() {

        // 1.
        if (this.tipoPersonaNotif.
            equals(RegistroNotificacionPorEdictoMB.tipoPersonaNotifSolicitante)) {
            if (!this.docNotifCargado) {
                this.addMensajeError("Debe cargar el documento de notificación");
                return "";
            }
        } else if (this.tipoPersonaNotif.equals(
            RegistroNotificacionPorEdictoMB.tipoPersonaNotifAutorizado)) {
            if (!this.docNotifCargado || !this.docAutorizacionCargado) {
                this.
                    addMensajeError("Debe cargar el documento de notificación y el de autorización");
                return "";
            }
        }

        int exceptionIdentifier = 0;
        try {
            //D: 2.
            if (!this.guardarDocumento(
                RegistroNotificacionPorEdictoMB.tipoDocumentoNotificacion,
                this.nombreArchivoNotificacionCargado,
                "Archivo de notificación suministrado por el usuario")) {
                exceptionIdentifier = 1;
                throw new Exception("No se pudo guardar el documento de notificación");
            }

            if (this.tipoPersonaNotif.equals(
                RegistroNotificacionPorEdictoMB.tipoPersonaNotifAutorizado)) {
                if (!this.guardarDocumento(
                    RegistroNotificacionPorEdictoMB.tipoDocumentoAutorizacionDeNotificacion,
                    this.nombreArchivoAutorizacionCargado,
                    "Archivo de autorización notificación suministrado por el usuario")) {
                    exceptionIdentifier = 1;
                    throw new Exception(
                        "No se pudo guardar el documento de autorización de notificación");
                }
            }

            // 3.
        } catch (Exception ex) {
            switch (exceptionIdentifier) {
                case (1): {
                    throw SNCWebServiceExceptions.EXCEPCION_0006.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                default: {
                    break;
                }
            }
        }

        // 4.
        this.forwardThisProcess();

        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * listeners de los componentes de carga de archivos
     *
     * @param eventoCarga
     */
    public void cargarArchivo1(FileUploadEvent eventoCarga) {
        this.archivoACargar = RegistroNotificacionPorEdictoMB.tipoDocumentoNotificacion;
        this.cargarArchivo(eventoCarga);
        this.docNotifCargado = true;
    }

    public void cargarArchivo2(FileUploadEvent eventoCarga) {
        this.archivoACargar =
            RegistroNotificacionPorEdictoMB.tipoDocumentoAutorizacionDeNotificacion;
        this.cargarArchivo(eventoCarga);
        this.docAutorizacionCargado = true;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * listener de los componentes de carga de archivos escribe el contenido del archivo contenido
     * en el FileUploadEvent en la carpeta temporal de documentos, y lo deja así disponible para que
     * sea creado el Documento en la base de datos
     *
     * @param eventoCarga
     */
    private void cargarArchivo(FileUploadEvent eventoCarga) {

        File archivoResultado = null;
        //String extension = FilenameUtils.getExtension(eventoCarga.getFile().getFileName());
        this.servicioTemporalDocumento = new ServicioTemporalDocumento();
        String rutaMostrar = eventoCarga.getFile().getFileName();
        String directorioTemporal = "";

        try {
            // TODO esto debe ser generico no del reporte
            directorioTemporal = FileUtils.getTempDirectory().getAbsolutePath();

            archivoResultado = new File(directorioTemporal, rutaMostrar);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw SNCWebServiceExceptions.EXCEPCION_0005.getExcepcion(LOGGER, ex);
        }

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(archivoResultado);

            byte[] buffer = new byte[Math.round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            if (this.archivoACargar.
                equals(RegistroNotificacionPorEdictoMB.tipoDocumentoNotificacion)) {
                LOGGER.debug("cargado el de notificación");
                this.nombreArchivoNotificacionCargado = rutaMostrar;
            } else if (this.archivoACargar.equals(
                RegistroNotificacionPorEdictoMB.tipoDocumentoAutorizacionDeNotificacion)) {
                LOGGER.debug("cargado el de notificación");
                this.nombreArchivoAutorizacionCargado = rutaMostrar;
            }

        } catch (FileNotFoundException ex) {
            LOGGER.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            LOGGER.error(RegistroNotificacionPorEdictoMB.class.getName(), ex);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Guarda el documento con la comunicación de la notificación en el servidor documental, e
     * inserta el respectivo registro en la tabla Documento
     *
     * @param tipoDocumento 'notificacion' o 'autorizacion'
     * @param nombreArchivoEnRutaTemp nombre del archivo con el que quedó en la ruta temporal
     * @param descripcionArchivo descripción del Documento
     *
     * @return true si todo OK
     */
    private boolean guardarDocumento(String tipoDocumento, String nombreArchivoEnRutaTemp,
        String descripcionArchivo) {

        boolean answer = true;
        Documento tempDocumento;
        TipoDocumento tipoDoc;
        Long tipoDocumentoId = 0l;
        Date fechaLogYRadicacion;

        if (tipoDocumento.equals(RegistroNotificacionPorEdictoMB.tipoDocumentoNotificacion)) {
            tipoDocumentoId = ETipoDocumentoId.DOCUMENTO_DE_NOTIFICACION.getId();
        } else if (tipoDocumento.equals(
            RegistroNotificacionPorEdictoMB.tipoDocumentoAutorizacionDeNotificacion)) {
            tipoDocumentoId = ETipoDocumentoId.DOCUMENTO_DE_AUTORIZACION_DE_NOTIFICACION.getId();
        }

        try {
            tipoDoc =
                this.getGeneralesService().buscarTipoDocumentoPorId(tipoDocumentoId);
        } catch (Exception ex) {
            LOGGER.error("error buscando TipoDocumento con id " + tipoDocumentoId + " : " + ex.
                getMessage());
            return false;
        }

        tempDocumento = new Documento();
        fechaLogYRadicacion = new Date(System.currentTimeMillis());

        //D: se arma el objeto Documento con todos los atributos posibles...
        //D: el método que guarda el documento en el gestor documental recibe el nombre del archivo
        //   sin la ruta
        tempDocumento.setArchivo(nombreArchivoEnRutaTemp);
        tempDocumento.setTramiteId(this.currentTramiteId);
        tempDocumento.setTipoDocumento(tipoDoc);
        tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
        tempDocumento.setUsuarioCreador(this.currentUser.getLogin());
        tempDocumento.setEstructuraOrganizacionalCod(this.currentUser.
            getCodigoEstructuraOrganizacional());
        tempDocumento.setFechaRadicacion(fechaLogYRadicacion);

//FIXME:: uncomment this when integrated with process
        //tempDocumento.setProcesoId(this.currentProcessActivity.getId());
        tempDocumento.setFechaLog(fechaLogYRadicacion);
        tempDocumento.setUsuarioLog(this.currentUser.getLogin());
        tempDocumento.setDescripcion(descripcionArchivo);

        try {
            //D: se guarda el documento
            if (tipoDocumento.equals(RegistroNotificacionPorEdictoMB.tipoDocumentoNotificacion)) {
                this.documentoNotificacion =
                    this.getTramiteService().guardarDocumento(this.currentUser, tempDocumento);
                if (this.documentoNotificacion == null) {
                    this.addMensajeError(
                        "Ocurrió un error en el almacenamiento del documento, por favor intente más tarde. ");
                }
            } else if (tipoDocumento.equals(
                RegistroNotificacionPorEdictoMB.tipoDocumentoAutorizacionDeNotificacion)) {
                this.documentoAutorizacionDeNotificacion =
                    this.getTramiteService().guardarDocumento(this.currentUser, tempDocumento);
                if (this.documentoAutorizacionDeNotificacion == null) {
                    this.addMensajeError(
                        "Ocurrió un error en el almacenamiento del documento, por favor intente más tarde. ");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("error guardando Documento : " + ex.getMessage());
            return false;
        }

        return answer;
    }

//--------------------------------------------------------------------------------------------------
    //----------------   BPM  ---------------------
    @Implement
    public boolean validateProcess() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
//-------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";

//FIXME::ESTA NO ES LA TRANSICIÓN. Definir el usuario destino de la actividad!!
        processTransition = ProcesoDeConservacion.ACT_ASIGNACION_COMPLETAR_DOC_SOLICITADA;
        observaciones = "";

        messageDTO =
            ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                this.currentProcessActivity.getId(), processTransition, this.currentUser,
                observaciones);

        this.setMessage(messageDTO);

    }
//-------------------------------------------------------------------------------------

    @Implement
    public void doDatabaseStatesUpdate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
//-------------------------------------------------------------------------------------

    private void forwardThisProcess() {
        this.validateProcess();
        this.setupProcessMessage();
        this.doDatabaseStatesUpdate();
        this.forwardProcess();
    }

//end of class
}
