/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del cu Consutlar actas de comité
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-045
 */
@Component("consultaActasComite")
@Scope("session")
public class ConsultaActasComiteMB extends SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaActasComiteMB.class);

    /**
     * {@link FiltroDatosConsultaAvaluo} filtro usado para la consulta
     */
    private FiltroDatosConsultaAvaluo filtroAvaluos;

    /**
     * Lista con los {@link Avaluo} que tienen acta de control de calidad encontrados en la busqueda
     */
    private List<Avaluo> avaluosConActa;

    /**
     * Lista de avaluadores de la territorial en sesion
     */
    private List<SelectItem> listaAvaluadores;

    /**
     * Lista con los departamentos para los combos
     */
    private ArrayList<SelectItem> listaDepartamentos;

    /**
     * Lista con los municipios para los combos
     */
    private ArrayList<SelectItem> listaMunicipios;

    /**
     * Avalúo seleccionado para la visualización del acta
     */
    private Avaluo avaluoSeleccionado;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si se está buscando por solicitante tipo natural o juridico
     */
    private boolean banderaEsPersonaNatural;

    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaAvaluo getFiltroAvaluos() {
        return filtroAvaluos;
    }

    public List<Avaluo> getAvaluosConActa() {
        return this.avaluosConActa;
    }

    public List<SelectItem> getListaAvaluadores() {
        return this.listaAvaluadores;
    }

    public ArrayList<SelectItem> getListaDepartamentos() {
        return this.listaDepartamentos;
    }

    public ArrayList<SelectItem> getListaMunicipios() {
        return this.listaMunicipios;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    // --------------------------------Reportes---------------------------------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte del actá
     */
    private ReporteDTO actaReporte;

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getActaReporte() {
        return this.actaReporte;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de ConsultaAvaluosAprobadosMB");

        this.inicializarFiltroConsulta();

        this.cargarAvaluadoresTerritorial();
        this.cargarListaDepartamentos();
        this.cargarAvaluadoresTerritorial();
    }

    // ----------------------------------------------------------------------
    /**
     * Inicializa el filtro de consulta y lso valores por defecto
     *
     * @author christian.rodriguez
     */
    private void inicializarFiltroConsulta() {
        this.filtroAvaluos = new FiltroDatosConsultaAvaluo();
        this.filtroAvaluos
            .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                .getCodigo());
        this.filtroAvaluos.setTipoEmpresa(EPersonaTipoPersona.NATURAL
            .getCodigo());
        this.banderaEsPersonaNatural = true;
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lista de departamentos según la territorial del usuario activo.
     * Así mismo carga los municipios y avaluadores asociados
     *
     * @author christian.rodriguez
     */
    public void cargarListaDepartamentos() {

        this.listaDepartamentos = new ArrayList<SelectItem>();
        this.listaDepartamentos.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.filtroAvaluos.getTerritorial() != null &&
             !this.filtroAvaluos.getTerritorial().isEmpty()) {

            List<Departamento> departamentos = (ArrayList<Departamento>) this
                .getGeneralesService().getCacheDepartamentosPorTerritorial(
                    this.filtroAvaluos.getTerritorial());

            for (Departamento departamento : departamentos) {
                this.listaDepartamentos.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }

        }

        this.filtroAvaluos.setDepartamento(null);

        this.cargarListaMunicipios();
        this.cargarAvaluadoresTerritorial();
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lista de municipios según el departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void cargarListaMunicipios() {

        this.listaMunicipios = new ArrayList<SelectItem>();
        this.listaMunicipios.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.filtroAvaluos.getDepartamento() != null &&
             !this.filtroAvaluos.getDepartamento().isEmpty()) {

            List<Municipio> municipios = (ArrayList<Municipio>) this
                .getGeneralesService().getCacheMunicipiosByDepartamento(
                    this.filtroAvaluos.getDepartamento());

            for (Municipio municipio : municipios) {
                this.listaMunicipios.add(new SelectItem(municipio.getCodigo(),
                    municipio.getNombre()));
            }
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Retorna los valores para un combo box (selectOneMenu) con los profesionales de avalúos
     * asociados a una territorial
     *
     * @author felipe.cadena
     * @return
     */
    public void cargarAvaluadoresTerritorial() {
        LOGGER.debug("cargando avaluadores...");

        this.listaAvaluadores = new ArrayList<SelectItem>();
        this.listaAvaluadores.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.filtroAvaluos.getTerritorial() != null &&
             !this.filtroAvaluos.getTerritorial().isEmpty()) {

            List<ProfesionalAvaluo> lpa = this.getAvaluosService()
                .buscarAvaluadoresPorTerritorial(
                    this.filtroAvaluos.getTerritorial());
            for (ProfesionalAvaluo profesional : lpa) {
                listaAvaluadores.add(new SelectItem(profesional.getId(),
                    profesional.getNombreCompleto()));
            }
        }
        LOGGER.debug("carga terminada");

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que realiza la busqueda de los avaluos que tienen acta de control de calidad
     * dependiendo del {@link FiltroDatosConsultaAvaluo} ingreasdo
     *
     * @author christian.rodriguez
     */
    public void buscar() {
        if (this.filtroAvaluos != null) {

            this.filtroAvaluos.setConActaControlCalidad(true);

            this.avaluosConActa = this.getAvaluosService()
                .buscarAvaluosPorFiltro(this.filtroAvaluos);
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se encarga de recuperar el acta del avaluo seleccionado para ser visualizada
     *
     * @author christian.rodriguez
     */
    public void cargarActa() {

        if (this.avaluoSeleccionado != null) {

            ComiteAvaluo comite = this.getAvaluosService()
                .consultarComiteAvaluoPorIdAvaluo(
                    this.avaluoSeleccionado.getId());

            Documento actaDocumento = this.getGeneralesService()
                .buscarDocumentoPorId(comite.getActaFirmadaDocumentoId());

            this.actaReporte = this.reportsService.consultarReporteDeGestorDocumental(
                actaDocumento.getIdRepositorioDocumentos());

            if (this.actaReporte == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "El acta asociada no pudo ser cargada.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia el tipo de persona. Activa la bandera de si es
     * persona natural y cambia el tipo de documento por defecto
     *
     * @author lorena.salamanca
     * @modified christian.rodriguez
     */
    public void onChangeTipoPersona() {

        if (this.filtroAvaluos.getTipoEmpresa().equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {

            this.banderaEsPersonaNatural = true;
            this.filtroAvaluos
                .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo());
            this.filtroAvaluos.setNombreRazonSocial(null);
            this.filtroAvaluos.setSigla(null);

        } else {
            this.banderaEsPersonaNatural = false;
            this.filtroAvaluos
                .setSolicitanteTipoIdentificacion(EPersonaTipoIdentificacion.NIT
                    .getCodigo());
            this.filtroAvaluos.setPrimerNombre(null);
            this.filtroAvaluos.setSegundoNombre(null);
            this.filtroAvaluos.setPrimerApellido(null);
            this.filtroAvaluos.setSegundoApellido(null);
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

}
