package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del CU-SA-AC-030 Generar Oficio para territorial de costos honorarios * Este CU se
 * llama desde el CU-SA-AC-025 Calcular costos y/o honorarios del avalúo comercial
 *
 * @author rodrigo.hernandez
 * @cu CU-SA-AC-030
 */
@Component("generarOficioTerritorialCostosHonorarios")
@Scope("session")
public class GeneracionOficioTerritorialCostosHonorariosMB extends
    SNCManagedBean {

    private static final long serialVersionUID = -6874180433844957599L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneracionOficioTerritorialCostosHonorariosMB.class);

    /**
     * Anexos
     */
    private String anexos;

    /**
     * Fecha del oficio
     */
    private Date fechaOficio;

    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Usuario coordinador GIT de avaluos ldap
     */
    private UsuarioDTO coordinadorGITAvaluos;

    /**
     * Usuario director de territorial
     */
    private UsuarioDTO directorTerritorial;

    /**
     * Lista de avaluos asociados al Cons. Documento
     */
    private List<Avaluo> listaAvaluos;

    /**
     * Solicitud asociada a los avaluos
     */
    private Solicitud solicitud;

    /**
     * la territorial donde se está realizando el avalúo.
     */
    private EstructuraOrganizacional territorialAvaluo;

    /*
     * Variables relacionadas a el documento que se genera
     */
    /**
     * Documento que representa el oficio genrado.
     */
    private Documento oficioReporte;

    private ReporteDTO reporteDTO;

    /*
     * Totales
     */
    private Double totalServicio;

    private Double totalHonorarios;

    private Double ivaHonorarios;

    private Double totalHonorariosAPagar;

    /*
     * Miscelaneous
     */
    /**
     * Lista de avaluadores creada a partir de la lista de avaluos recibida del CU-025
     */
    private List<RegistroAvaluadorAvaluo> listaRegistrosAvaluadorAvaluo;

    private Parametro resolucionPagoHonorarios;

    private Parametro resolucionPagoCostoServicio;

    private Parametro iva;

    /**
     * Dato que viene desde el CU-025
     */
    private Double baseHonorarios;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    protected IContextListener contexto;

    /**
     * Variable que indica si el oficio ya fue generado o no
     */
    private boolean banderaOficioGenerado;

    /* ----- SETTERS Y GETTERS ------------------ */
    public List<Avaluo> getListaAvaluos() {
        return listaAvaluos;
    }

    public void setListaAvaluos(List<Avaluo> listaAvaluos) {
        this.listaAvaluos = listaAvaluos;
    }

    public Documento getOficioReporte() {
        return oficioReporte;
    }

    public void setOficioReporte(Documento oficioReporte) {
        this.oficioReporte = oficioReporte;
    }

    public List<RegistroAvaluadorAvaluo> getListaRegistrosAvaluadorAvaluo() {
        return listaRegistrosAvaluadorAvaluo;
    }

    public void setListaRegistrosAvaluadorAvaluo(
        List<RegistroAvaluadorAvaluo> listaRegistrosAvaluadorAvaluo) {
        this.listaRegistrosAvaluadorAvaluo = listaRegistrosAvaluadorAvaluo;
    }

    public boolean isBanderaOficioGenerado() {
        return banderaOficioGenerado;
    }

    public void setBanderaOficioGenerado(boolean banderaOficioGenerado) {
        this.banderaOficioGenerado = banderaOficioGenerado;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public EstructuraOrganizacional getTerritorialAvaluo() {
        return territorialAvaluo;
    }

    public void setTerritorialAvaluo(EstructuraOrganizacional territorialAvaluo) {
        this.territorialAvaluo = territorialAvaluo;
    }

    public Date getFechaOficio() {
        return fechaOficio;
    }

    public void setFechaOficio(Date fechaOficio) {
        this.fechaOficio = fechaOficio;
    }

    public UsuarioDTO getCoordinadorGITAvaluos() {
        return coordinadorGITAvaluos;
    }

    public void setCoordinadorGITAvaluos(UsuarioDTO coordinadorGITAvaluos) {
        this.coordinadorGITAvaluos = coordinadorGITAvaluos;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return directorTerritorial;
    }

    public void setDirectorTerritorial(UsuarioDTO directorTerritorial) {
        this.directorTerritorial = directorTerritorial;
    }

    public Parametro getResolucionPagoHonorarios() {
        return resolucionPagoHonorarios;
    }

    public Parametro getResolucionPagoCostoServicio() {
        return resolucionPagoCostoServicio;
    }

    public void setResolucionPagoCostoServicio(
        Parametro resolucionPagoCostoServicio) {
        this.resolucionPagoCostoServicio = resolucionPagoCostoServicio;
    }

    public void setResolucionPagoHonorarios(Parametro resolucionPagoHonorarios) {
        this.resolucionPagoHonorarios = resolucionPagoHonorarios;
    }

    public Parametro getIva() {
        return iva;
    }

    public void setIva(Parametro iva) {
        this.iva = iva;
    }

    public Double getBaseHonorarios() {
        return baseHonorarios;
    }

    public void setBaseHonorarios(Double baseHonorarios) {
        this.baseHonorarios = baseHonorarios;
    }

    public Double getTotalServicio() {
        return totalServicio;
    }

    public void setTotalServicio(Double totalServicio) {
        this.totalServicio = totalServicio;
    }

    public Double getTotalHonorarios() {
        return totalHonorarios;
    }

    public void setTotalHonorarios(Double totalHonorarios) {
        this.totalHonorarios = totalHonorarios;
    }

    public Double getIvaHonorarios() {
        return ivaHonorarios;
    }

    public void setIvaHonorarios(Double ivaHonorarios) {
        this.ivaHonorarios = ivaHonorarios;
    }

    public Double getTotalHonorariosAPagar() {
        return totalHonorariosAPagar;
    }

    public void setTotalHonorariosAPagar(Double totalHonorariosAPagar) {
        this.totalHonorariosAPagar = totalHonorariosAPagar;
    }

    public String getAnexos() {
        return anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    // -------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#init");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.iniciarListas();
        this.iniciarBanderas();
        this.cargarParametros();

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar listas
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListas() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#iniciarListas");

        this.listaAvaluos = new ArrayList<Avaluo>();
        this.listaRegistrosAvaluadorAvaluo =
            new ArrayList<GeneracionOficioTerritorialCostosHonorariosMB.RegistroAvaluadorAvaluo>();

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#iniciarListas");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar variables booleanas
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderas() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#iniciarBanderas");

        this.banderaOficioGenerado = false;

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#iniciarBanderas");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar desde otros MB
     *
     * @author rodrigo.hernandez
     */
    public void cargarDesdeOtrosMB(List<Avaluo> listaAvaluos,
        Double valorBaseHonorarios) {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#cargarDesdeOtrosMB");

        this.listaAvaluos = listaAvaluos;

        this.baseHonorarios = valorBaseHonorarios;

        /*
         * Se define la fecha del oficio
         */
        this.fechaOficio = Calendar.getInstance().getTime();

        /*
         * Se define la territorial
         */
        this.territorialAvaluo = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                String.valueOf(this.listaAvaluos.get(0)
                    .getEstructuraOrganizacionalCod()));

        /*
         * Se carga el coordinador GIT
         */
        this.obtenerCoordinadorGITAvaluos();

        /*
         * Se carga el director de la territorial
         */
        this.obtenerDirectorTerritorial();

        /*
         * Se carga la solicitud
         */
        this.solicitud = this.listaAvaluos.get(0).getTramite().getSolicitud();

        this.cargarListaAvaluadores();

        this.calcularTotales();

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#cargarDesdeOtrosMB");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar parametros del sistema
     *
     * @author rodrigo.hernandez
     */
    private void cargarParametros() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#cargarParametroResolucionPagoHonorarios");

        /*
         * Se carga el parametro RESOLUCION_PAGO_HONORARIOS
         */
        this.resolucionPagoHonorarios = this.getGeneralesService()
            .getCacheParametroPorNombre(
                EParametro.RESOLUCION_PAGO_HONORARIOS.toString());

        /*
         * Se carga el parametro RESOLUCION_PAGO_COSTO_SERVICIO
         */
        this.resolucionPagoCostoServicio = this.getGeneralesService()
            .getCacheParametroPorNombre(
                EParametro.RESOLUCION_PAGO_COSTO_SERVICIO.toString());

        /*
         * Se carga el parametro IVA
         */
        this.iva = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.IVA.toString());

        LOGGER.debug("Fin ProyeccionCotizacionMB#cargarParametroResolucionPagoHonorarios");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear la lista de registros de AvaluadorAvaluo a partir de los avaluos recibidos
     * del CU-025
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaAvaluadores() {
        LOGGER.debug("Inicio GeneracionOficioInterventorDelAvaluadorMB#cargarListaAvaluadores");

        RegistroAvaluadorAvaluo raa = null;

        this.listaRegistrosAvaluadorAvaluo =
            new ArrayList<GeneracionOficioTerritorialCostosHonorariosMB.RegistroAvaluadorAvaluo>();

        for (Avaluo avaluo : this.listaAvaluos) {

            for (ProfesionalAvaluo pa : avaluo.getAvaluadores()) {

                raa = new RegistroAvaluadorAvaluo(pa, avaluo);
                raa.setIva(this.iva.getValorNumero());

                LOGGER.debug(raa.toString());

                this.listaRegistrosAvaluadorAvaluo.add(raa);
            }

        }

        LOGGER.debug("Fin GeneracionOficioInterventorDelAvaluadorMB#cargarListaAvaluadores");
    }

    // -------------------------------------------------------------------------
    /**
     * Método que consulta el coordinador GIT de avaluos asociado a la territorial del
     * {@link #usuarioActivo}
     *
     * @author christian.rodriguez
     */
    private void obtenerCoordinadorGITAvaluos() {

        if (this.territorialAvaluo != null) {
            List<UsuarioDTO> coordinadoresGITAvaluos = this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                    ERol.COORDINADOR_GIT_AVALUOS);

            if (coordinadoresGITAvaluos != null &&
                 !coordinadoresGITAvaluos.isEmpty()) {
                this.coordinadorGITAvaluos = coordinadoresGITAvaluos.get(0);
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que consulta el coordinador GIT de avaluos asociado a la territorial del
     * {@link #usuarioActivo}
     *
     * @author rodrigo.hernandez
     */
    private void obtenerDirectorTerritorial() {

        if (this.territorialAvaluo != null) {

            List<UsuarioDTO> listaDirectoresTerritorial = this
                .getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.territorialAvaluo.getNombre(),
                    ERol.DIRECTOR_TERRITORIAL);

            if (listaDirectoresTerritorial != null &&
                 !listaDirectoresTerritorial.isEmpty()) {
                this.directorTerritorial = listaDirectoresTerritorial.get(0);
            }
        }
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para calcular los totales: </br></br> - Total Servicio</br> - Total Honorarios</br> -
     * IVA Honorarios</br> - Total Honorarios a pagar</br>
     *
     *
     *
     * @author rodrigo.hernandez
     */
    private void calcularTotales() {
        LOGGER.debug("Inicio ProyeccionCotizacionMB#calcularTotales");

        this.totalServicio = 0D;
        this.totalHonorarios = 0D;
        this.totalHonorariosAPagar = 0D;
        this.ivaHonorarios = 0D;

        /*
         * Se calcula el total del servicio
         */
        for (RegistroAvaluadorAvaluo raa : this.listaRegistrosAvaluadorAvaluo) {
            this.totalServicio = this.totalServicio + raa.getSubtotal();
        }

        /*
         * Se calcula el total de los honorarios
         */
        for (RegistroAvaluadorAvaluo raa : this.listaRegistrosAvaluadorAvaluo) {
            this.totalHonorarios = this.totalHonorarios +
                 raa.getAvaluo().getValorHonorariosIGAC();
        }

        /*
         * Se calcula el total del IVA de los honorarios
         */
        for (RegistroAvaluadorAvaluo raa : this.listaRegistrosAvaluadorAvaluo) {
            this.ivaHonorarios = this.ivaHonorarios +
                 (raa.getAvaluo().getValorHonorariosIGAC() * raa.getIva());
        }

        /*
         * Se calcula el total de los honorarios a pagar
         */
        this.totalHonorariosAPagar = this.totalHonorarios + this.ivaHonorarios;

        LOGGER.debug("Fin ProyeccionCotizacionMB#calcularTotales");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para enviar el oficio por correo al coordinador del GIT de avalúos
     *
     * @author rodrigo.hernandez
     */
    public void confirmarOficio() {
        LOGGER.debug("Confirmando oficio");

        this.generarOficio();
        this.generarDocumento();

        LOGGER.debug("Oficio confirmado");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear el reporte
     *
     * @author rodrigo.hernandez
     */
    private void generarOficio() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#generarOficio");

        // TODO::rodrigo.hernandez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        /*
         * Se crea mapa de parametros para enviarlo al servicio que genera el reporte
         */
        Map<String, String> parameters = new HashMap<String, String>();

        // TODO :: rodrigo.hernandez :: Parametros quemados, reemplazar cuando
        // se haya desarrollado el reporte :: 30/10/12
        this.reporteDTO = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        this.banderaOficioGenerado = true;

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#generarOficio");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar documento generado para vista previa
     *
     * @author rodrigo.hernandez
     */
    public void verVistaPrevia() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#cargarReporte");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        previsualizacionDocMB.setReporteDTO(this.reporteDTO);

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#cargarReporte");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para
     *
     * @author rodrigo.hernandez
     */
    private void generarDocumento() {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#generarDocumento");

        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.OFICIOS_REMISORIOS.getId());
        tipoDocumento.setNombre(ETipoDocumento.OFICIOS_REMISORIOS.getNombre());

        this.oficioReporte = new Documento();
        this.oficioReporte.setArchivo(this.reporteDTO.getUrlWebReporte());
        this.oficioReporte.setTipoDocumento(tipoDocumento);
        this.oficioReporte.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.oficioReporte.setBloqueado(ESiNo.NO.getCodigo());
        this.oficioReporte.setUsuarioCreador(this.usuario.getLogin());
        this.oficioReporte.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        this.oficioReporte.setFechaLog(new Date(System.currentTimeMillis()));
        this.oficioReporte.setUsuarioLog(this.usuario.getLogin());
        this.oficioReporte.setDescripcion("Oficio interventor del avaluador");

        this.guardarDocumentoAlfresco(tipoDocumento);

        this.oficioReporte = this.getGeneralesService().guardarDocumento(
            this.oficioReporte);

        /*
         * TODO::rodrigo.hernandez::Pendiente asociar documento creado a Cons Documento
         */
        this.banderaOficioGenerado = true;

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#generarDocumento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para guardar el documento generado en alfresco
     *
     * @author rodrigo.hernandez
     */
    private void guardarDocumentoAlfresco(TipoDocumento tipoDocumento) {
        LOGGER.debug("Inicio GeneracionOficioInterventorAvaluadorMB#guardarDocumentoAlfresco");

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(
                this.solicitud.getNumero(), this.solicitud.getFecha(),
                tipoDocumento.getNombre(),
                this.reporteDTO.getRutaCargarReporteAAlfresco());

        this.oficioReporte = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, this.oficioReporte);

        if (this.oficioReporte == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

        LOGGER.debug("Fin GeneracionOficioInterventorAvaluadorMB#guardarDocumentoAlfresco");
    }

    /**
     * Inner class usada para crear registros de la tabla
     * <b>avaluosConsDocumentoTable</b> de la pagina
     * <b>generarOficioInterventorDelAvaluador.xhtml</b>
     *
     * @author rodrigo.hernandez
     *
     */
    public class RegistroAvaluadorAvaluo {

        private ProfesionalAvaluo avaluador;
        private Avaluo avaluo;
        private Double subtotal;
        private Double iva;

        private RegistroAvaluadorAvaluo(ProfesionalAvaluo avaluador,
            Avaluo avaluo) {
            super();
            this.avaluador = avaluador;
            this.avaluo = avaluo;
        }

        public ProfesionalAvaluo getAvaluador() {
            return avaluador;
        }

        public void setAvaluador(ProfesionalAvaluo avaluador) {
            this.avaluador = avaluador;
        }

        public Avaluo getAvaluo() {
            return avaluo;
        }

        public void setAvaluo(Avaluo avaluo) {
            this.avaluo = avaluo;
        }

        public Double getSubtotal() {

            subtotal = this.avaluo.getValorHonorariosIGAC() +
                 (this.avaluo.getValorHonorariosIGAC() * this.iva);

            return subtotal;
        }

        public void setSubtotal(Double subtotal) {
            this.subtotal = subtotal;
        }

        public Double getIva() {
            return iva;
        }

        public void setIva(Double iva) {
            this.iva = iva;
        }

        @Override
        public String toString() {
            String result = this.avaluo.getSecRadicado() + " - " +
                 this.avaluador.getNombreCompleto();

            return result;
        }

    }

}
