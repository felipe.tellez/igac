package co.gov.igac.snc.web.util;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.GeneralMB;

/**
 * Converter para traer el nombre de un departamento a partir del codigo
 *
 * @author franz.gamba
 *
 */
public class DepartamentoNombreConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartamentoNombreConverter.class);

    private GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorString) {

        try {
            return valorString;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " + valorString);
            return valorString;
        }
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorObjeto) {

        List<Departamento> departamentos = this.generalMB.getDepartamentos(Constantes.COLOMBIA);
        String nombreDepartamento = null;
        for (Departamento depto : departamentos) {
            if (valorObjeto.equals(depto.getCodigo())) {
                nombreDepartamento = depto.getNombre();
            }
        }
        try {
            return nombreDepartamento;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " +
                 valorObjeto);
            return (String) valorObjeto;
        }
    }
}
