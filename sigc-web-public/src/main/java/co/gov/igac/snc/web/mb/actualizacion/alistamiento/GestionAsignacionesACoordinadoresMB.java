package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionReconocimiento;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.util.EActualizacionContratoActividad;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author javier.barajas Creacion de metodos para buscar las actualizacion reconocimiento TODO:
 * depende de process cargar las manzanas para asignar en la tabla actuializacion reconocimiento
 * @cu 220 Gestion Asignacion a coordinadores
 */
@Component("gestionarAsignacionesACoordinadores")
@Scope("session")
public class GestionAsignacionesACoordinadoresMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        GestionAsignacionesACoordinadoresMB.class);

    private String responsableSIG;

    @Autowired
    private IContextListener contextoWeb;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Lista de coordinadores por actualizacion
     */
    private List<RecursoHumano> listaCoordinadores;

    /**
     * Seleccion de topografo
     */
    private ArrayList<SelectItem> selectCoordinadores;

    /**
     * Sleccion ActualizacionReconocimiento
     */
    private ActualizacionReconocimiento[] selectActualizacionReconocimiento;

    /**
     * Seleccion del coordinador
     */
    private String selecionCoordinador;

    /**
     * Seleccion coordinador para la busqueda
     */
    private String selecionCoordinadorBusqueda;

    /**
     * Codigo del municipio
     */
    private String codigoMunicipio;

    /**
     * Lisata de manzanas de la actualziacion
     */
    private List<ManzanaVereda> listaManzanas;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;

    private String moduleTools;

    /**
     * Busqueda de manzanas
     */
    private String manzanaInicial;

    private String manzanaFinal;

    /**
     * busqueda Coordinadores
     */
    private String findCoordinadores;

    /**
     * Mostrar busqueda
     */
    private boolean renderedBusqueda;

    /**
     * Lista resultado de busqueda
     */
    private List<ActualizacionReconocimiento> listaBusqueda;

    /**
     * Lista manzanas veredas busqueda
     */
    private List<ManzanaVereda> listaManzanaVeredaFind;

    /**
     * Codigos de manzana a mostrar por el visor
     */
    private String manzanaCodigo;

    /**
     * mostrar boton para asignar
     */
    private boolean rendererAsignar;

    /**
     * HAbilita coordinador
     */
    private boolean habilitaCoordinador;

    /**
     * Tipo de reconocimiento
     */
    private String cadenaTipoDMCoFicha;

    /**
     * Objeto de recurso humano
     */
    private RecursoHumano coordinadorAsignar;

    /**
     * Actualizacion(Objeto de Negocio)
     */
    private Actualizacion seleccionActualizacion;

    /**
     * se habilita tipo de reconocimiento
     */
    private boolean renderedTipo;

    /**
     * lista de ActualizacionReconocimiento
     */
    private List<ActualizacionReconocimiento> listaActualizacionReconocimiento;

    /**
     * lista de ActualizacionReconocimiento despues de la busqueda
     */
    private List<ActualizacionReconocimiento> listaARFind;

    /**
     * Muestra si hay recurso humano
     */
    private boolean renderedColumnaCoordinador;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;

    @PostConstruct
    public void init() {

        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 24/10/12
        // Obtener la actualizacióin del arbol de tareas.
        this.setActualizacionSeleccionada(this.getActualizacionService()
            .recuperarActualizacionPorId(450L));

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.moduleLayer = "ac";
        this.moduleTools = "ac";
        this.listaCoordinadores = new ArrayList<RecursoHumano>();
        this.selectCoordinadores = new ArrayList<SelectItem>();
        this.listaCoordinadores = this.getActualizacionService().
            buscarrecursoHumanoporActividadYActualizacionId(450L,
                EActualizacionContratoActividad.COORDINADORES.getCodigo());

        this.seleccionActualizacion = this.getActualizacionService().recuperarActualizacionPorId(
            450L);
        this.codigoMunicipio = this.seleccionActualizacion.getMunicipio().getCodigo();
        this.adicionarDatosVitem(this.selectCoordinadores, this.listaCoordinadores);
        this.listaManzanas = new ArrayList<ManzanaVereda>();
        this.listaActualizacionReconocimiento = new ArrayList<ActualizacionReconocimiento>();
        this.buscarManzanasActualizacion();
        this.manzanaInicial = "Manzana Inicial";
        this.manzanaFinal = "Manzana Final";
        this.findCoordinadores = "Buscar por Coodinador";
        this.listaBusqueda = new ArrayList<ActualizacionReconocimiento>();
        this.renderedBusqueda = false;
        this.rendererAsignar = false;
        this.habilitaCoordinador = false;
        this.renderedTipo = false;
        this.coordinadorAsignar = new RecursoHumano();
        this.renderedColumnaCoordinador = false;
    }
    // **************************************************************************************************************************
    // Getters y Setters
    // **************************************************************************************************************************

    public String getSelecionCoordinadorBusqueda() {
        return selecionCoordinadorBusqueda;
    }

    public void setSelecionCoordinadorBusqueda(String selecionCoordinadorBusqueda) {
        this.selecionCoordinadorBusqueda = selecionCoordinadorBusqueda;
    }

    public void setActualizacionSeleccionada(Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getResponsableSIG() {
        return responsableSIG;
    }

    public void setResponsableSIG(String responsableSIG) {
        this.responsableSIG = responsableSIG;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public void setListaCoordinadores(List<RecursoHumano> listaCoordinadores) {
        this.listaCoordinadores = listaCoordinadores;
    }

    public List<RecursoHumano> getListaCoordinadores() {
        return listaCoordinadores;
    }

    public ArrayList<SelectItem> getSelectCoordinadores() {
        return selectCoordinadores;
    }

    public void setSelectCoordinadores(ArrayList<SelectItem> selectCoordinadores) {
        this.selectCoordinadores = selectCoordinadores;
    }

    public String getSelecionCoordinador() {
        return selecionCoordinador;
    }

    public void setSelecionCoordinador(String selecionCoordinador) {
        this.selecionCoordinador = selecionCoordinador;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public List<ManzanaVereda> getListaManzanas() {
        return listaManzanas;
    }

    public void setListaManzanas(List<ManzanaVereda> listaManzanas) {
        this.listaManzanas = listaManzanas;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getManzanaInicial() {
        return manzanaInicial;
    }

    public void setManzanaInicial(String manzanaInicial) {
        this.manzanaInicial = manzanaInicial;
    }

    public String getManzanaFinal() {
        return manzanaFinal;
    }

    public void setManzanaFinal(String manzanaFinal) {
        this.manzanaFinal = manzanaFinal;
    }

    public String getFindCoordinadores() {
        return findCoordinadores;
    }

    public void setFindCoordinadores(String findCoordinadores) {
        this.findCoordinadores = findCoordinadores;
    }

    public void setSelectActualizacionReconocimiento(
        ActualizacionReconocimiento selectActualizacionReconocimiento[]) {
        this.selectActualizacionReconocimiento = selectActualizacionReconocimiento;
    }

    public ActualizacionReconocimiento[] getSelectActualizacionReconocimiento() {
        return selectActualizacionReconocimiento;
    }

    public boolean isRenderedBusqueda() {
        return renderedBusqueda;
    }

    public void setRenderedBusqueda(boolean renderedBusqueda) {
        this.renderedBusqueda = renderedBusqueda;
    }

    public List<ActualizacionReconocimiento> getListaBusqueda() {
        return listaBusqueda;
    }

    public void setListaBusqueda(List<ActualizacionReconocimiento> listaBusqueda) {
        this.listaBusqueda = listaBusqueda;
    }

    public List<ManzanaVereda> getListaManzanaVeredaFind() {
        return listaManzanaVeredaFind;
    }

    public void setListaManzanaVeredaFind(List<ManzanaVereda> listaManzanaVeredaFind) {
        this.listaManzanaVeredaFind = listaManzanaVeredaFind;
    }

    public boolean isRendererAsignar() {
        return rendererAsignar;
    }

    public void setRendererAsignar(boolean rendererAsignar) {
        this.rendererAsignar = rendererAsignar;
    }

    public boolean isHabilitaCoordinador() {
        return habilitaCoordinador;
    }

    public void setHabilitaCoordinador(boolean habilitaCoordinador) {
        this.habilitaCoordinador = habilitaCoordinador;
    }

    public String getCadenaTipoDMCoFicha() {
        return cadenaTipoDMCoFicha;
    }

    public void setCadenaTipoDMCoFicha(String cadenaTipoDMCoFicha) {
        this.cadenaTipoDMCoFicha = cadenaTipoDMCoFicha;
    }

    public RecursoHumano getCoordinadorAsignar() {
        return coordinadorAsignar;
    }

    public void setCoordinadorAsignar(RecursoHumano coordinadorAsignar) {
        this.coordinadorAsignar = coordinadorAsignar;
    }

    public Actualizacion getSeleccionActualizacion() {
        return seleccionActualizacion;
    }

    public void setSeleccionActualizacion(Actualizacion seleccionActualizacion) {
        this.seleccionActualizacion = seleccionActualizacion;
    }

    public boolean isRenderedTipo() {
        return renderedTipo;
    }

    public void setRenderedTipo(boolean renderedTipo) {
        this.renderedTipo = renderedTipo;
    }

    public List<ActualizacionReconocimiento> getListaActualizacionReconocimiento() {
        return listaActualizacionReconocimiento;
    }

    public void setListaActualizacionReconocimiento(
        List<ActualizacionReconocimiento> listaActualizacionReconocimiento) {
        this.listaActualizacionReconocimiento = listaActualizacionReconocimiento;
    }

    public void setListaARFind(List<ActualizacionReconocimiento> listaARFind) {
        this.listaARFind = listaARFind;
    }

    public List<ActualizacionReconocimiento> getListaARFind() {
        return listaARFind;
    }

    public boolean isRenderedColumnaCoordinador() {
        return renderedColumnaCoordinador;
    }

    public void setRenderedColumnaCoordinador(boolean renderedColumnaCoordinador) {
        this.renderedColumnaCoordinador = renderedColumnaCoordinador;
    }

    // **************************************************************************************************************************
    // Funciones
    // **************************************************************************************************************************
    /**
     * Llena el select item de los coordinadores
     */
    private void adicionarDatosVitem(List<SelectItem> lista,
        List<RecursoHumano> listRecursoHumano) {

        for (RecursoHumano t : listRecursoHumano) {
            if (t.getNombre() != null) {
                lista.add(new SelectItem(String.valueOf(t.getId()), t.getNombre()));
            }
        }
    }

    /**
     * Busca las manzanas de la actualizacion para mostrar
     */
    public void buscarManzanasActualizacion() {
        try {
            int i = 0;
            this.listaActualizacionReconocimiento = this.getActualizacionService().
                getManazanasporMunicipioAsignarCoordinadorPaginacionActualizacionReconocimiento(450L,
                    null);
            //TODO: Se agrega consulta del recurso humano por id por que no traia el recurso humano con la consulta anterior
            for (ActualizacionReconocimiento ar : this.listaActualizacionReconocimiento) {
                try {
                    RecursoHumano rh = this.getActualizacionService().buscarRecursoHumanoporId(ar.
                        getRecursoHumano().getId());
                    ar.setRecursoHumano(rh);
                    //LOGGER.debug("id:"+ar.getRecursoHumano().getNombre());
                } catch (Exception e) {
                    LOGGER.error(
                        "No se encontraron las manzanas de la actualizacion a asiganar coordinador: ",
                        e);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_WARN,
                        "No se encontraron las manzanas de la actualizacion a asiganar coordinador",
                        "No se encontraron las manzanas de la actualizacion a asiganar coordinador"));
                }
            }

        } catch (Exception e) {
            LOGGER.error(
                "No se encontraron las manzanas de la actualizacion a asiganar coordinador ", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN,
                "No se encontraron las manzanas de la actualizacion a asiganar coordinador",
                "No se encontraron las manzanas de la actualizacion a asiganar coordinador"));
        }
    }

    /**
     * Busqueda por manzanas
     */
    public void buscarManzanas() {

        if (!this.manzanaFinal.equals(null) && !this.manzanaInicial.equals(null) &&
             !this.manzanaFinal.isEmpty() && !this.manzanaInicial.isEmpty()) {
            if (this.manzanaFinal.length() == 17 && this.manzanaInicial.length() == 17) {
                if (Long.parseLong(this.manzanaInicial.substring(0, 5)) == Long.parseLong(
                    this.manzanaFinal.substring(0, 5))) {
                    if (Long.parseLong(this.manzanaFinal.substring(5, 7)) > Long.parseLong(
                        this.manzanaInicial.substring(5, 7))) {
                        LOGGER.debug("ZONA MAYOR---------------");
                        this.generarNuevaListaManzanas();
                    } else {
                        if (Long.parseLong(this.manzanaFinal.substring(5, 7)) == Long.parseLong(
                            this.manzanaInicial.substring(5, 7))) {
                            if (Long.parseLong(this.manzanaFinal.substring(7, 9)) > Long.parseLong(
                                this.manzanaInicial.substring(7, 9))) {
                                LOGGER.debug("SECTOR MAYOR---------------");
                                this.generarNuevaListaManzanas();

                            } else {
                                if (Long.parseLong(this.manzanaFinal.substring(7, 9)) == Long.
                                    parseLong(this.manzanaInicial.substring(7, 9))) {
                                    if (Long.parseLong(this.manzanaFinal.substring(9, 11)) > Long.
                                        parseLong(this.manzanaInicial.substring(9, 11))) {
                                        LOGGER.debug("COMUNA MAYOR---------------");
                                        this.generarNuevaListaManzanas();
                                    } else {
                                        if (Long.parseLong(this.manzanaFinal.substring(9, 11)) ==
                                            Long.parseLong(this.manzanaInicial.substring(9, 11))) {
                                            if (Long.parseLong(this.manzanaFinal.substring(11, 13)) >
                                                Long.
                                                    parseLong(this.manzanaInicial.substring(11, 13))) {
                                                LOGGER.debug("BARRIO MAYOR---------------");
                                                this.generarNuevaListaManzanas();
                                            } else {
                                                if (Long.parseLong(this.manzanaFinal.substring(11,
                                                    13)) == Long.parseLong(this.manzanaInicial.
                                                        substring(11, 13))) {
                                                    if (Long.parseLong(this.manzanaFinal.substring(
                                                        13, 17)) > Long.parseLong(
                                                            this.manzanaInicial.substring(13, 17))) {
                                                        LOGGER.debug("MANZANA MAYOR---------------");
                                                        this.generarNuevaListaManzanas();
                                                    } else {
                                                        FacesContext.getCurrentInstance().
                                                            addMessage(null, new FacesMessage(
                                                                FacesMessage.SEVERITY_WARN,
                                                                "La manzana inicial no es mayor a la final en el codigo manzana",
                                                                "La manzana inicial no es mayor a la finalen el codigo manzana"));
                                                    }
                                                } else {
                                                    FacesContext.getCurrentInstance().addMessage(
                                                        null, new FacesMessage(
                                                            FacesMessage.SEVERITY_WARN,
                                                            "La manzana inicial no es mayor a la final en el codigo barrio",
                                                            "La manzana inicial no es mayor a la finalen el codigo barrio"));
                                                }
                                            }
                                        } else {
                                            FacesContext.getCurrentInstance().addMessage(null,
                                                new FacesMessage(FacesMessage.SEVERITY_WARN,
                                                    "La manzana inicial no es mayor a la final en el codigo comuna",
                                                    "La manzana inicial no es mayor a la finalen el codigo comuna"));
                                        }
                                    }

                                } else {
                                    FacesContext.getCurrentInstance().addMessage(null,
                                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                            "La manzana inicial no es mayor a la final en el codigo sector",
                                            "La manzana inicial no es mayor a la finalen el codigo sector"));
                                }
                            }
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                                FacesMessage.SEVERITY_WARN,
                                "La manzana inicial no es mayor a la final en el codigo zona ",
                                "La manzana inicial no es mayor a la finalen el codigo zona"));
                        }
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_WARN,
                        "Los codigos ingresados no correspondes al mismo municipio",
                        "Los codigos ingresados no correspondes al mismo municipio"));
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "Los codigos ingresados no correspondes a ningun numero de manzana",
                    "Los codigos ingresados no correspondes a ningun numero de manzana"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No ha ingresado ningun rango de manzanas",
                "No ha ingresado ningun rango de manzanas"));
        }
    }

    /**
     * Genera el resultado de la buqueda de manzanas
     */
    public void generarNuevaListaManzanas() {
        this.renderedBusqueda = true;
        int begin = 0, fin = 0;
        this.listaARFind = this.getActualizacionService()
            .getManazanasporMunicipioAsignarCoordinadorPaginacionActualizacionReconocimiento(450L,
                null);		//this.listaActualizacionReconocimiento.

        begin = this.buscaManzana(this.manzanaInicial);
        fin = this.buscaManzana(this.manzanaFinal);
        if (begin != -1 && fin != -1) {
            this.listaARFind = this.listaARFind.subList(begin, fin);
            //TODO: Se agrega consulta del recurso humano por id por que no traia el recurso humano con la consulta anterior
            for (ActualizacionReconocimiento ar : this.listaARFind) {

                try {
                    RecursoHumano rh = this.getActualizacionService().buscarRecursoHumanoporId(ar.
                        getRecursoHumano().getId());
                    ar.setRecursoHumano(rh);
                    //LOGGER.debug("id:"+ar.getRecursoHumano().getNombre());
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }

            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se encontro el codigo de la manzana dentro la lista",
                "No se encontro el codigo de la manzana dentro la lista"));
        }

    }

    /**
     * Busca por coordinador
     *
     * @author javier.barajas
     */
    public void buscarCoordinador() {
        RecursoHumano buscarCoordinador = new RecursoHumano();
        try {
            try {

                buscarCoordinador = this.getActualizacionService().buscarRecursoHumanoporId(Long.
                    valueOf(this.selecionCoordinadorBusqueda));
            } catch (Exception e) {
                LOGGER.error("No se pudo consultar por el coordinador escogido", e);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_WARN, "No se pudo consultar por el coordinador escogido",
                    "No se pudo consultar por el coordinador escogido"));
            }
            this.listaARFind = this.getActualizacionService().
                buscaActualizacionReconocimientoPorRecursoHumanoId(buscarCoordinador.getId());
            //TODO: Se agrega consulta del recurso humano por id por que no traia el recurso humano con la consulta anterior
            for (ActualizacionReconocimiento ar : this.listaARFind) {
                try {
                    RecursoHumano rh = this.getActualizacionService().buscarRecursoHumanoporId(ar.
                        getRecursoHumano().getId());
                    ar.setRecursoHumano(rh);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            this.renderedBusqueda = true;

        } catch (Exception e) {
            LOGGER.error("No se pudo consultar por el coordinador escogido", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo consultar por el coordinador escogido",
                "No se pudo consultar por el coordinador escogido"));
        }
    }

    public int buscaManzana(String manzanaCod) {
        int index = -1;
        for (ActualizacionReconocimiento ar : this.listaARFind) {
            if (ar.getManzanaCodigo().equals(manzanaCod)) {
                index = this.listaARFind.indexOf(ar);
            }
        }
        return index;
    }

    /**
     * Evento que lanza cuando se selecciona una tabla
     *
     * @param evento
     */
    public void rowSelected(org.primefaces.event.SelectEvent evento) {

        this.rendererAsignar = true;
        this.obtenerManzanas();
    }

    /**
     * Obtiene las manazanas segun la seleccion
     */
    public void obtenerManzanas() {
        int contador = 0;
        if (this.selectActualizacionReconocimiento.length > 0) {
            if (this.selectActualizacionReconocimiento.length == 1) {
                this.manzanaCodigo = this.selectActualizacionReconocimiento[0].getManzanaCodigo().
                    toString();
            } else {
                for (ActualizacionReconocimiento al : this.selectActualizacionReconocimiento) {
                    if (contador == 0) {
                        this.manzanaCodigo = al.getManzanaCodigo().toString();
                    } else {
                        this.manzanaCodigo = this.manzanaCodigo + al.getManzanaCodigo().toString();
                    }
                    if (this.selectActualizacionReconocimiento.length - 1 != contador) {
                        this.manzanaCodigo = this.manzanaCodigo + ",";
                        contador++;
                    }
                }
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No ha seleccionado ninguna manzana",
                "No ha seleccionado ninguna manzana"));

        }

    }

    /**
     * Asignar en ActualizacionReconocimiento
     *
     * @return
     */
    public void asignarActualizacionReconocimiento() {
        if (this.rendererAsignar && this.habilitaCoordinador) {
            for (ActualizacionReconocimiento ac : this.listaActualizacionReconocimiento) {
                for (ActualizacionReconocimiento ar : this.selectActualizacionReconocimiento) {
                    if (ac.equals(ar)) {
                        ac.setRecursoHumano(this.coordinadorAsignar);
                        ac.setTipo(this.cadenaTipoDMCoFicha);
                    }
                }
            }
            this.renderedColumnaCoordinador = true;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN,
                "No ha seleccionado ninguna manzana ni asociado coordinadores, ni tipo",
                "No ha seleccionado ninguna manzana ni asociado coordinadores, ni tipo"));
        }
    }

    /**
     * Guardar en ActualizacionReconocimiento
     *
     * @return
     */
    public void guardarActualizacionReconocimiento() {
        if (this.renderedTipo) {
            for (ActualizacionReconocimiento ac : this.listaActualizacionReconocimiento) {
                ActualizacionReconocimiento obtenerIdar = new ActualizacionReconocimiento();
                ac.setTipo(this.cadenaTipoDMCoFicha);
                obtenerIdar = this.getActualizacionService().agregarActualizacionReconocimiento(ac);
                LOGGER.debug("ID AR:" + obtenerIdar);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No ha seleccionado el tipo de actualizacion",
                "No ha seleccionado el tipo de actualizacion"));
        }
    }

    /**
     * Habilita la asignadion de coordinadores
     *
     * @return
     */

    public void habilitarCoordinador() {
        this.habilitaCoordinador = true;
        try {
            this.coordinadorAsignar = this.getActualizacionService().buscarRecursoHumanoporId(Long.
                valueOf(this.selecionCoordinador));
        } catch (Exception e) {
            LOGGER.error("No se pudo consultar por el coordinador escogido", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_WARN, "No se pudo consultar por el coordinador escogido",
                "No se pudo consultar por el coordinador escogido"));
        }
    }

    /**
     * ver cadenaTipoDMCoFichas
     *
     * @return
     */
    public void verCadenaTiopoDMCoFichas() {
        this.renderedTipo = true;
    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("gestionarAsignacionesACoordinadores");
        this.tareasPendientesMB.init();
        //this.init();
        return "index";
    }

}
