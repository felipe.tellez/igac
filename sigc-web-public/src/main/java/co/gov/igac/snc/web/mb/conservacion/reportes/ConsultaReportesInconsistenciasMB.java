package co.gov.igac.snc.web.mb.conservacion.reportes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReportesInconsistenciasDinamicosSNC;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author javier.barajas
 *
 * MB Reportes Inconsistencias
 */
@Component("consultaReportesInconsistencias")
@Scope("session")
public class ConsultaReportesInconsistenciasMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        ConsultaReportesInconsistenciasMB.class);

    @Autowired
    private IContextListener contextoWeb;

    /**
     * Archivo del reporte dinamico
     */
    private File archivoReporteDinamico;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Url del reporte dinamico
     */
    private String urlArchivoReporte;

    /**
     * Lista de items de los reportes
     */
    private ArrayList<SelectItem> listaReportesInconsistenciasItemSelect;

    /**
     * Id del reporte seleccionado
     */
    private String selectReporte;

    /**
     * Si hay reporte permite mostrar
     */
    private boolean renderedReporte;

    /**
     * Usuario de la aplicacion
     */
    private UsuarioDTO usuario;

    /**
     * Atributos para el reporte de memorando de comisión
     */
    private StreamedContent reporteDinamico;

    /**
     * Código del departamento seleccionado
     */
    private String selectedDepartamentoCod;

    /**
     * Código del municipio seleccionado
     */
    private String selectedMunicipioCod;

    public void setSelectedDepartamento(String selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    /**
     * Código del municipio seleccionado
     */
    private String selectedMunicipio;
    /**
     * Código del municipio seleccionado
     */
    private String selectedDepartamento;

    /**
     * Mostrar lista de departamentos y municipios
     */
    private boolean renderedDepartamentosYmunicipios;

    /**
     * Parametro del reporte
     */
    private Map<String, String> parametros;

    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.renderedReporte = true;
        this.renderedDepartamentosYmunicipios = false;

    }

    // **************************************************************************************************************************
    // Getters y Setters
    // **************************************************************************************************************************
    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public File getArchivoReporteDinamico() {
        return archivoReporteDinamico;
    }

    public void setArchivoReporteDinamico(File archivoReporteDinamico) {
        this.archivoReporteDinamico = archivoReporteDinamico;
    }

    public String getUrlArchivoReporte() {
        return urlArchivoReporte;
    }

    public void setUrlArchivoReporte(String urlArchivoReporte) {
        this.urlArchivoReporte = urlArchivoReporte;
    }

    public ArrayList<SelectItem> getListaReportesInconsistenciasItemSelect() {
        return listaReportesInconsistenciasItemSelect;
    }

    public void setListaReportesInconsistenciasItemSelect(
        ArrayList<SelectItem> listaReportesInconsistenciasItemSelect) {
        this.listaReportesInconsistenciasItemSelect = listaReportesInconsistenciasItemSelect;
    }

    public String getSelectReporte() {
        return selectReporte;
    }

    public void setSelectReporte(String selectReporte) {
        this.selectReporte = selectReporte;
    }

    public boolean isRenderedReporte() {
        return renderedReporte;
    }

    public void setRenderedReporte(boolean renderedReporte) {
        this.renderedReporte = renderedReporte;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public StreamedContent getReporteDinamico() {
        InputStream stream;

        try {
            stream = new FileInputStream(this.archivoReporteDinamico);
            this.reporteDinamico = new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_PDF, "Reporte.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte " + e.getMessage());
        }

        return reporteDinamico;
    }

    public void setReporteDinamico(StreamedContent reporteDinamico) {
        this.reporteDinamico = reporteDinamico;
    }

    public String getSelectedDepartamentoCod() {
        return selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String selectedDepartamentoCod) {

        this.selectedDepartamentoCod = selectedDepartamentoCod;
    }

    public String getSelectedMunicipioCod() {
        return selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String selectedMunicipioCod) {

        this.selectedMunicipioCod = selectedMunicipioCod;
    }

    public boolean isRenderedDepartamentosYmunicipios() {
        return renderedDepartamentosYmunicipios;
    }

    public void setRenderedDepartamentosYmunicipios(
        boolean renderedDepartamentosYmunicipios) {
        this.renderedDepartamentosYmunicipios = renderedDepartamentosYmunicipios;
    }

    public Map<String, String> getParametros() {
        return parametros;
    }

    public void setParametros(Map<String, String> parametros) {
        this.parametros = parametros;
    }

    public String getSelectedMunicipio() {
        return selectedMunicipio;
    }

    public void setSelectedMunicipio(String selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public String getSelectedDepartamento() {
        return selectedDepartamento;
    }

    // **************************************************************************************************************************
    // Funciones
    // **************************************************************************************************************************
    /**
     * Metodo para generar el reporte
     *
     */
    public void generarReporte() {

        LOGGER.debug("eNTRO GENERAR REPORTE++++++++++++++++++++++");
        this.parametros = new HashMap<String, String>();
        if (EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_CEDULA_NOMBRE.
            getIdConfiguracionReporte().equals(Long.getLong(this.selectReporte)) ||
            EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_NOMBRE_CEDULA.
                getIdConfiguracionReporte().equals(Long.getLong(this.selectReporte)) ||
            EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_LISTA_DIRECCION.
                getIdConfiguracionReporte().equals(Long.getLong(this.selectReporte)) ||
            EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_LISTA_GEOGRAFICA_PREDIOS_SIN_REPRESENTACION.
                getIdConfiguracionReporte().equals(Long.getLong(this.selectReporte))) {

            this.seleccionDeptYmuni();

            this.parametros.put("DEPARTAMENTO_CODIGO", this.selectedDepartamentoCod);
            this.parametros.put("MUNICIPIO_CODIGO", this.selectedMunicipioCod);

        }

        try {

            this.archivoReporteDinamico = this.getGeneralesService().
                obtenerReporteDinamico(this.parametros, Long.parseLong(this.selectReporte),
                    this.usuario);

            if (this.archivoReporteDinamico != null || this.archivoReporteDinamico.length() < 2000) {
                LOGGER.debug("Se genero el reporte en:" + this.archivoReporteDinamico.getPath());
                this.urlArchivoReporte = this.getGeneralesService().publicarArchivoEnWeb(
                    archivoReporteDinamico.getPath());
                this.renderedReporte = true;
                LOGGER.debug("++++++++++++URl reporte:" + this.archivoReporteDinamico.getPath());
            } else {
                LOGGER.debug("No pudo generar el reporte:");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "No pudo generar el Reporte",
                    "No pudo generar el Reporte"));
            }
        } catch (Exception e) {
            LOGGER.debug("No pudo generar el reporte:");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "No pudo generar el Reporte",
                "No pudo generar el Reporte"));
        }
    }

    /**
     * Accion cuando selecciona un reporte
     *
     */
    public void seleccionaReporte() {

        LOGGER.debug("------------Entro a seleccionar el reporte" + Long.parseLong(
            this.selectReporte));

        if (EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_CEDULA_NOMBRE.
            getIdConfiguracionReporte().equals(Long.parseLong(this.selectReporte)) ||
            EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_NOMBRE_CEDULA.
                getIdConfiguracionReporte().equals(Long.parseLong(this.selectReporte)) ||
            EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_LISTA_DIRECCION.
                getIdConfiguracionReporte().equals(Long.parseLong(this.selectReporte)) ||
            EReportesInconsistenciasDinamicosSNC.INCONSISTENCIA_LISTA_GEOGRAFICA_PREDIOS_SIN_REPRESENTACION.
                getIdConfiguracionReporte().equals(Long.parseLong(this.selectReporte))) {

            LOGGER.debug("+++++Entro para mostrar los municipios");
            this.renderedDepartamentosYmunicipios = true;
        } else {
            this.renderedDepartamentosYmunicipios = false;
        }

        LOGGER.debug("............Salio de la validacion" + this.renderedDepartamentosYmunicipios);

    }

    /**
     * saber el departamento y municipio
     *
     * @return
     */
    public void seleccionDeptYmuni() {
        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb
            .getManagedBean("combosDeptosMunis");

        this.selectedDepartamentoCod = combosDeptosMunisMB.getSelectedDepartamentoCod();

        this.selectedMunicipioCod = combosDeptosMunisMB.getSelectedMunicipioCod();
    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("consultaReportesInconsistencias");
        //this.tareasPendientesMB.init();
        return "index";
    }

}
