package co.gov.igac.snc.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.documental.DocumentoServiceParams;
import co.gov.igac.sigc.documental.impl.procesador.ProcesadorDocumentosImpl;
import co.gov.igac.sigc.documental.interfaces.IProcesadorDocumentos;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.IReporteService;
import co.gov.igac.sigc.reportes.ReporteServiceFactory;
import co.gov.igac.sigc.reportes.model.Reporte;
import co.gov.igac.snc.comun.utilerias.PropertyLoader;
import co.gov.igac.snc.util.log.LogMensajesReportesUtil;
import co.gov.igac.snc.web.controller.AbstractLocator;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class ReportesUtil extends AbstractLocator {

    /**
     *
     */
    private static final long serialVersionUID = 808732839101851686L;
    private static ReportesUtil reportes;

    private ReportesUtil() {

    }

    public static ReportesUtil getInstance() {
        if (reportes == null) {
            reportes = new ReportesUtil();
        }
        return reportes;
    }

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReportesUtil.class);

    /**
     * Función que genera un reporte y publica el archivo en web retorna su contenido en un
     * ReporteDTO que contiene un File del archivo y la url de la publicación web
     *
     * @param parameters
     * @param enumeracionReporte
     * @param loggedInUser
     * @param formato formato de salida del reporte, las opciones son Reporte.FORMAT_EXCEL y
     * Reporte.FORMAT_CSV sino se envía por defecto se genera el reporte en pdf
     * @return ReporteDTO dto que contiene el File del reporte y la url del archivo para verlo desde
     * la aplicación web
     * @author javier.aponte
     */
    public ReporteDTO generarReporte(Map<String, String> parameters,
        Object enumeracionReporte, UsuarioDTO loggedInUser,
        String... formato) {

        LOGGER.debug("en ReportsMB#generarReporte");
        ReporteDTO resultado = null;

        try {
            String urlReporte = null;

            // Cast del objeto que tiene los datos de la url del reporte
            if (enumeracionReporte instanceof EReporteServiceSNC) {
                urlReporte = ((EReporteServiceSNC) enumeracionReporte).getUrlReporte();
            } else if (enumeracionReporte instanceof String) {
                urlReporte = (String) enumeracionReporte;
            }

            IReporteService service = ReporteServiceFactory.getService();
            Reporte reporte = new Reporte(loggedInUser);
            reporte.setUrl(urlReporte);

            EstructuraOrganizacional estructuraOrganizacional = this
                .getGeneralesService()
                .buscarEstructuraOrganizacionalPorCodigo(
                    loggedInUser.getCodigoEstructuraOrganizacional());

            reporte.setEstructuraOrganizacional(estructuraOrganizacional);
            if (formato != null && formato.length > 0) {
                reporte.setFormato(formato[0]);
            }

            Set<Entry<String, String>> entries = parameters.entrySet();
            Iterator<Entry<String, String>> it = entries.iterator();
            while (it.hasNext()) {
                Entry<String, String> entry = (Entry<String, String>) it.next();
                reporte.addParameter(entry.getKey(), entry.getValue());
            }

            File archivoReporte = service.getReportAsFile(reporte,
                new LogMensajesReportesUtil(this.getGeneralesService(),
                    loggedInUser));

            if (archivoReporte != null) {

                resultado = this.crearReporteDTO(archivoReporte);

            } else {
                LOGGER.error("Ocurrió un error al generar el reporte, el archivo es null");
            }
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar el reporte: " +
                 e.getMessage());
        }
        return resultado;
    }

    /**
     * Función que consulta un archivo en alfresco por medio del id de repositorio de documentos y
     * arma un objeto de tipo reporteDTO
     *
     * @param idRepositorioDocumentos
     * @return ReporteDTO dto que contiene el File del reporte y la url del archivo para verlo desde
     * la aplicación web
     * @author javier.aponte
     */
    public ReporteDTO consultarReporteDeGestorDocumental(String idRepositorioDocumentos) {

        LOGGER.debug("en ReportsMB#generarReporteDeAlfresco");
        ReporteDTO resultado = null;

        try {
            String urlTemporal;
            urlTemporal = this.getGeneralesService()
                .descargarArchivoAlfrescoYSubirAPreview(
                    idRepositorioDocumentos);
            FileNameMap fileNameMap = URLConnection.getFileNameMap();

            if (urlTemporal == null || urlTemporal.isEmpty()) {
                LOGGER.error("Ocurrió un errror al consultar el documento en alfresco: " +
                     "consultarURLDeArchivoEnAlfresco retorno null o vacio");
                return resultado;
            }

            String rutaTempLocal = this.getGeneralesService()
                .descargarArchivoDeAlfrescoATemp(idRepositorioDocumentos);

            File archivoLocalReporte = new File(rutaTempLocal);
            String tipoMimeReporte;

            tipoMimeReporte = fileNameMap.getContentTypeFor(archivoLocalReporte
                .getPath());

            InputStream stream;
            StreamedContent streamedContentReporte;

            stream = new FileInputStream(archivoLocalReporte);
            streamedContentReporte = new DefaultStreamedContent(stream,
                tipoMimeReporte, archivoLocalReporte.getName());

            resultado = new ReporteDTO();
            resultado.setArchivoReporte(archivoLocalReporte);
            resultado.setNombreReporte(archivoLocalReporte.getName());
            resultado.setUrlWebReporte(urlTemporal);
            resultado.setRutaCargarReporteAAlfresco(rutaTempLocal);
            resultado.setTipoMimeReporte(tipoMimeReporte);
            resultado.setStreamedContentReporte(streamedContentReporte);

        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede generar el archivo, ");
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar el reporte: " +
                 e.getMessage());
        }
        return resultado;
    }

    /**
     * Método que retorna un objeto de tipo ReporteDTO a partir de un File, lo caul consiste en
     * publicar el archivo en web, crear un objeto StreamedContent para poder usar el componente
     * FileDownload de Prime, este DTO contiene la información asociada al documento.
     * @link{ReporteDTO}
     *
     * @param archivoReporte
     * @return ReporteDTO dto que contiene el File del reporte y la url del archivo para verlo desde
     * la aplicación web
     * @author javier.aponte
     */
    public ReporteDTO generarReporteDesdeUnFile(File archivoReporte) {

        LOGGER.debug("en ReportsMB#generarReporteDesdeUnFile");
        ReporteDTO resultado = null;

        try {
            if (archivoReporte.exists()) {

                resultado = this.crearReporteDTO(archivoReporte);

            } else {
                LOGGER.error("Ocurrió un error al generar el reporte, el archivo es null");
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar el reporte: " +
                 e.getMessage());
        }
        return resultado;
    }

    /**
     * Método que retorna un objeto de tipo ReporteDTO a partir de una ruta de un archivo, lo caul
     * consiste en publicar el archivo en web, crear un objeto StreamedContent para poder usar el
     * componente FileDownload de Prime, este DTO contiene la información asociada al documento.
     * @link{ReporteDTO}
     *
     * @param rutaArchivoReporte
     * @return ReporteDTO dto que contiene el File del reporte y la url del archivo para verlo desde
     * la aplicación web @author javier.aponte
     */
    public ReporteDTO generarReporteDesdeUnaRutaDeArchivo(
        String rutaArchivoReporte) {

        LOGGER.debug("en ReportsMB#generarReporteDesdeUnaRutaDeArchivo");
        ReporteDTO resultado = null;

        try {
            File archivoReporte = new File(rutaArchivoReporte);
            if (archivoReporte.exists()) {

                resultado = this.crearReporteDTO(archivoReporte);

            } else {
                LOGGER.error("Ocurrió un error al generar el reporte, el archivo es null");
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar el reporte: " +
                 e.getMessage());
        }
        return resultado;
    }

    // ----------------------------------------------------- //
    /**
     * Método que adiciona una marca de agua sobre un archivo pdf
     *
     * @note Basado en el método adicionarMarcaDeAguaAPDFVectorial(String, String) de
     * ProcesadorDocumentosImpl.
     *
     * @author david.cifuentes
     *
     * @param rutaPDF Ruta local donde se encuentra el archivo.
     * @return
     *
     */
    public ReporteDTO adicionarMarcaDeAguaAPDF(String rutaPDF) {

        ReporteDTO answer = null;
        try {

            String nombreDirectorioTemporalRecursos, nombreArchivoParametros, fileSeparator, tempResourcesFolderPath, watermarkName, watermarkPath;
            File archivoSalida, systemTemporal, resource, tempResourcesFolder;
            ClassLoader cl;
            Properties props;
            URL fileUrl;
            int n;

            nombreDirectorioTemporalRecursos = "sigc_documental_recursos";
            fileSeparator = System.getProperty("file.separator");
            nombreArchivoParametros = DocumentoServiceParams.DOCUMENT_SERVER_PARAMS_FILENAME
                .getId();
            props = PropertyLoader.loadProperties(nombreArchivoParametros);
            watermarkName = (String) props
                .get(DocumentoServiceParams.WATERMARK_VECTOR.getId());

            cl = Thread.currentThread().getContextClassLoader();
            fileUrl = cl.getResource(watermarkName);
            resource = new File(java.net.URLDecoder.decode(fileUrl.getPath(),
                "UTF-8"));

            systemTemporal = FileUtils.getTempDirectory();
            tempResourcesFolderPath = systemTemporal.getAbsolutePath() +
                 fileSeparator + nombreDirectorioTemporalRecursos;
            LOGGER.debug("tempResourcesFolderPath:" + tempResourcesFolderPath);
            tempResourcesFolder = new File(tempResourcesFolderPath);

            // Esto se hace para obligar a que se ejecute el constructor de
            // ProcesadorDocumentosImpl
            // que obliga a copiar la imagen de marca de agua
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();

            watermarkPath = tempResourcesFolder.getAbsolutePath() +
                 fileSeparator + resource.getName();

            File archivoImagen = new File(watermarkPath);
            if (!archivoImagen.exists()) {
                LOGGER.error("Error al agregar la marca de agua al documento: " +
                     "No se encontró la imagen de marca de agua");
                return null;
            }

            PdfReader reader = new PdfReader(rutaPDF);
            n = reader.getNumberOfPages();

            String pathDirectory = this.generateFileName();
            File directorioSalida = new File(pathDirectory);
            directorioSalida.mkdirs();
            archivoSalida = File.createTempFile("snc_doc_", ".pdf",
                directorioSalida);
            FileOutputStream fileOutputStream = new FileOutputStream(
                archivoSalida);
            PdfStamper stamp = new PdfStamper(reader, fileOutputStream);
            PdfContentByte under;
            Image img = Image.getInstance(watermarkPath);
            img.setAbsolutePosition(0, 0);

            int i = 0;
            while (i < n) {
                i++;
                under = stamp.getOverContent(i);
                under.addImage(img);
            }

            reader.close();
            stamp.setFullCompression();
            stamp.close();

            if (archivoSalida != null) {
                answer = this.crearReporteDTO(archivoSalida);

            }

        } catch (Exception e) {
            LOGGER.error("Error al agregar la marca de agua al documento: " +
                 e.getMessage());
        }
        return answer;
    }

    /**
     * Método que consulta un documento en alfresco y le adiciona una marca de agua, y retorna un
     * objeto reporteDTO que contiene los datos del documento que tiene la marca de agua
     *
     * @param idRepositorioDocumentos
     * @return reporteDTO
     * @author javier.aponte
     */
    public ReporteDTO consultarDeGestorDocumentalYAdicionarMarcaDeAguaAPDF(
        String idRepositorioDocumentos) {

        LOGGER.debug("en ReportsMB#consultarDeAlfrescoYAdicionarMarcaDeAguaAPDF");
        ReporteDTO resultado = null;
        String urlTemporal;
        try {

            urlTemporal = this.getGeneralesService()
                .descargarArchivoDeAlfrescoATemp(idRepositorioDocumentos);

            if (urlTemporal == null || urlTemporal.isEmpty()) {
                LOGGER.error("Ocurrió un errror al consultar el documento en alfresco: " +
                     "consultarURLDeArchivoEnAlfresco retorno null o vacio");
                return resultado;
            }
            resultado = this.adicionarMarcaDeAguaAPDF(urlTemporal);
        } catch (Exception e) {
            LOGGER.error("Error al consulta de alfresco y agregar la marca de agua al documento: " +
                 e.getMessage());
        }
        return resultado;

    }

    /**
     * El método retorna una ruta en la carpeta temporal con un nombre de una carpeta generada con
     * un número aleatorio único
     *
     * @return path ruta en la carpeta temporal con el nombre de una carpeta única
     * @author javier.aponte
     */
    public String generateFileName() {
        String path = "";
        String fileSeparator = System.getProperty("file.separator");

        path = FileUtils.getTempDirectory().getAbsolutePath() + fileSeparator +
             UUID.randomUUID().toString();

        return path.toString();
    }

    /**
     * Método que crea un objeto de tipo ReporteDTO apartir de un file y de un nombre del reporte
     *
     * @param archivoReporte
     * @param nombreReporte
     * @return reporteDTO
     */
    protected ReporteDTO crearReporteDTO(File archivoReporte) {

        ReporteDTO resultado = null;
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String urlWebReporte;

        urlWebReporte = this.getGeneralesService()
            .publicarArchivoEnWeb(archivoReporte.getPath());
        LOGGER.debug("Url público del archivo:" + urlWebReporte);

        String tipoMimeReporte;

        tipoMimeReporte = fileNameMap.getContentTypeFor(archivoReporte
            .getPath());

        InputStream stream;
        StreamedContent streamedContentReporte;

        try {

            stream = new FileInputStream(archivoReporte);
            streamedContentReporte = new DefaultStreamedContent(stream,
                tipoMimeReporte, archivoReporte.getName());

            resultado = new ReporteDTO();
            resultado.setArchivoReporte(archivoReporte);
            resultado.setUrlWebReporte(urlWebReporte);
            resultado.setTipoMimeReporte(tipoMimeReporte);
            resultado.setRutaCargarReporteAAlfresco(archivoReporte.getPath());
            resultado.setStreamedContentReporte(streamedContentReporte);
            resultado.setNombreReporte(archivoReporte.getName());

        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede generar el archivo, ");
        }

        return resultado;
    }

    /**
     * Método que consulta un documento en alfresco y le adiciona una marca de agua a pdf Raster, y
     * retorna un objeto reporteDTO que contiene los datos del documento que tiene la marca de agua
     *
     * @param idRepositorioDocumentos
     * @return reporteDTO
     * @author javier.aponte
     */
    public ReporteDTO adicionarMarcaDeAguaAPDFRaster(
        String idRepositorioDocumentos) {

        LOGGER.debug("en ReportsMB#consultarDeAlfrescoYAdicionarMarcaDeAguaAPDF");
        ReporteDTO resultado = null;
        String urlTemporal;
        try {

            urlTemporal = this.getGeneralesService()
                .descargarArchivoDeAlfrescoATemp(idRepositorioDocumentos);

            if (urlTemporal == null || urlTemporal.isEmpty()) {
                LOGGER.error("Ocurrió un errror al consultar el documento en alfresco: " +
                     "consultarURLDeArchivoEnAlfresco retorno null o vacio");
                return resultado;
            }

            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();

            String documentoSalida = procesadorDocumentos.adicionarMarcaDeAguaAPDFRaster(FileUtils.
                getTempDirectoryPath(), urlTemporal);

            resultado = this.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);

        } catch (Exception e) {
            LOGGER.error("Error al consulta de alfresco y agregar la marca de agua al documento: " +
                 e.getMessage());
        }
        return resultado;

    }

    /**
     * Función que descarga un archivo del ftp por medio del id de repositorio de documentos y arma
     * un objeto de tipo reporteDTO
     *
     * @param idRepositorioDocumentos
     * @return ReporteDTO dto que contiene el File del reporte y la url del archivo para verlo desde
     * la aplicación web
     * @author javier.aponte
     */
    public ReporteDTO descargarArchivoDeFtp(String idRepositorioDocumentos) {

        LOGGER.debug("en ReportsMB#descargarArchivoDeFtp");
        ReporteDTO resultado = null;

        try {
            String rutaTempLocal = this.getGeneralesService()
                .descargarArchivoDeAlfrescoATemp(idRepositorioDocumentos);

            if (rutaTempLocal == null || rutaTempLocal.isEmpty()) {
                LOGGER.error("Ocurrió un errror al descargar el documento de alfresco: " +
                     "descargarArchivoDeAlfrescoATemp retorno null o vacio");
                return resultado;
            }

            File archivoLocalReporte = new File(rutaTempLocal);
            String tipoMimeReporte;

            FileNameMap fileNameMap = URLConnection.getFileNameMap();

            tipoMimeReporte = fileNameMap.getContentTypeFor(archivoLocalReporte
                .getPath());

            InputStream stream;
            StreamedContent streamedContentReporte;

            stream = new FileInputStream(archivoLocalReporte);
            streamedContentReporte = new DefaultStreamedContent(stream,
                tipoMimeReporte, archivoLocalReporte.getName());

            resultado = new ReporteDTO();
            resultado.setArchivoReporte(archivoLocalReporte);
            resultado.setNombreReporte(archivoLocalReporte.getName());
            resultado.setRutaCargarReporteAAlfresco(rutaTempLocal);
            resultado.setTipoMimeReporte(tipoMimeReporte);
            resultado.setStreamedContentReporte(streamedContentReporte);

        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede generar el archivo, ");
        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar el reporte: " +
                 e.getMessage());
        }
        return resultado;
    }

}
