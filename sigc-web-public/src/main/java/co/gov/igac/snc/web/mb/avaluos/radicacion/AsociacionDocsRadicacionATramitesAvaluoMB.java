/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.apache.commons.lang.NotImplementedException;

/**
 * Managed bean para el CU-SA-AC-115
 *
 * @author pedro.garcia
 * @cu CU-SA-AC-115
 * @version 2.0
 */
@Component("asociacionDocsRadicacionATramitesAvaluo")
@Scope("session")
public class AsociacionDocsRadicacionATramitesAvaluoMB extends SNCManagedBean {

    private static final long serialVersionUID = 182105781945673352L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AsociacionDocsRadicacionATramitesAvaluoMB.class);

    /**
     * solicitud con la que se está trabajando
     */
    private Solicitud solicitud;

    /**
     * solicitante de la solicitud actual
     */
    private SolicitanteSolicitud solicitanteSolicitud;

    /**
     * territorial del usuario con sesión
     */
    private EstructuraOrganizacional territorialUsuario;

    /**
     * Usuario loggeado en la sesión
     */
    private UsuarioDTO usuario;

    private List<Avaluo> listaTramitesSolicitud;

    /**
     * Avaluos seleccionados desde la tabla
     */
    private Avaluo[] tramitesSeleccionados;

    /**
     * Avaluo seleccionado
     */
    private Avaluo tramiteSeleccionado;

    /**
     * Lista de documentos asociados a la solicitud
     */
    private List<SolicitudDocumentacion> listaDocumentosSolicitud;

    /**
     * Documentos de la solicitud seleccionados
     */
    private SolicitudDocumentacion[] documentosSolicitudSeleccionados;

    /**
     * Lista de documentos asociados al tramite seleccionado
     */
    private List<TramiteDocumentacion> listaDocumentosTramite;

    /**
     * Lista de documentos asociados a los tramites seleccionados en la tabla
     */
    private List<TramiteDocumentacion> listaDocumentosTramitesSeleccionados;

    /**
     * Documentos asociados seleccionados a los tramites seleccionados
     */
    private TramiteDocumentacion[] documentosTramiteSeleccionados;

    /**
     * Soporte del documento seleccionado para visualizar
     */
    private Documento documentoSeleccionado;

    private File fileDocumento;

    private String rutaArchivoTemporal;

    /*
     * --------- Variables usadas en visualizacion de archivos -----------------
     */
    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    private StreamedContent archivo;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportes = ReportesUtil.getInstance();

    @Autowired
    protected IContextListener contextoWeb;

    // ---------- getters y setters
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public SolicitanteSolicitud getSolicitanteSolicitud() {
        return this.solicitanteSolicitud;
    }

    public void setSolicitanteSolicitud(
        SolicitanteSolicitud solicitanteSolicitud) {
        this.solicitanteSolicitud = solicitanteSolicitud;
    }

    public EstructuraOrganizacional getTerritorialUsuario() {
        return this.territorialUsuario;
    }

    public void setTerritorialUsuario(
        EstructuraOrganizacional territorialUsuario) {
        this.territorialUsuario = territorialUsuario;
    }

    // ---------- methods ---------------------------------------------
    public List<SolicitudDocumentacion> getListaDocumentosSolicitud() {
        return listaDocumentosSolicitud;
    }

    public void setListaDocumentosSolicitud(
        List<SolicitudDocumentacion> listaDocumentosSolicitud) {
        this.listaDocumentosSolicitud = listaDocumentosSolicitud;
    }

    public SolicitudDocumentacion[] getDocumentosSolicitudSeleccionados() {
        return documentosSolicitudSeleccionados;
    }

    public void setDocumentosSolicitudSeleccionados(
        SolicitudDocumentacion[] documentosSolicitudSeleccionados) {
        this.documentosSolicitudSeleccionados = documentosSolicitudSeleccionados;
    }

    public List<TramiteDocumentacion> getListaDocumentosTramite() {
        return listaDocumentosTramite;
    }

    public void setListaDocumentosTramite(
        List<TramiteDocumentacion> listaDocumentosTramite) {
        this.listaDocumentosTramite = listaDocumentosTramite;
    }

    public TramiteDocumentacion[] getDocumentosTramiteSeleccionados() {
        return documentosTramiteSeleccionados;
    }

    public void setDocumentosTramiteSeleccionados(
        TramiteDocumentacion[] documentosTramiteSeleccionados) {
        this.documentosTramiteSeleccionados = documentosTramiteSeleccionados;
    }

    public List<Avaluo> getListaTramitesSolicitud() {
        return listaTramitesSolicitud;
    }

    public void setListaTramitesSolicitud(List<Avaluo> listaTramitesSolicitud) {
        this.listaTramitesSolicitud = listaTramitesSolicitud;
    }

    public Avaluo[] getTramitesSeleccionados() {
        return tramitesSeleccionados;
    }

    public void setTramitesSeleccionados(Avaluo[] tramitesSeleccionados) {
        this.tramitesSeleccionados = tramitesSeleccionados;
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public Documento getDocumentoSeleccionado() {
        return documentoSeleccionado;
    }

    public void setDocumentoSeleccionado(Documento documentoSeleccionado) {
        this.documentoSeleccionado = documentoSeleccionado;
    }

    public Avaluo getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Avaluo tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public StreamedContent getArchivo() {
        InputStream stream;

        try {
            stream = new FileInputStream(this.fileDocumento);
            this.archivo = new DefaultStreamedContent(stream,
                this.tipoMimeDocTemporal, "Memorando_comision_oferta.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte");
        }

        return this.archivo;
    }

    public void setArchivo(StreamedContent archivo) {
        this.archivo = archivo;
    }

    public File getFileDocumento() {
        return fileDocumento;
    }

    public void setFileDocumento(File fileDocumento) {
        this.fileDocumento = fileDocumento;
    }

    public String getRutaArchivoTemporal() {
        return rutaArchivoTemporal;
    }

    public void setRutaArchivoTemporal(String rutaArchivoTemporal) {
        this.rutaArchivoTemporal = rutaArchivoTemporal;
    }

    public List<TramiteDocumentacion> getListaDocumentosTramitesSeleccionados() {
        return listaDocumentosTramitesSeleccionados;
    }

    public void setListaDocumentosTramitesSeleccionados(
        List<TramiteDocumentacion> listaDocumentosTramitesSeleccionados) {
        this.listaDocumentosTramitesSeleccionados = listaDocumentosTramitesSeleccionados;
    }

    // -------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de AsociacionDocsRadicacionATramitesAvaluoMB");

        this.iniciarDatosSolicitud();
        this.cargarDocumentosSolicitud();
        this.cargarTramitesSolicitud();

        LOGGER.debug("fin de AsociacionDocsRadicacionATramitesAvaluoMB");
    }

    // ------------------------------------------------------------------------------
    /**
     * Método para iniciar datos de la solicitud
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatosSolicitud() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

//TODO :: snc.avaluos :: obtener este dato del bpm
        Long solicitudId = 43500L;

        /* Se carga la solicitud */
        this.solicitud = this.getTramiteService()
            .buscarSolicitudFetchTramitesBySolicitudId(solicitudId);

        String codigoTerritorial = this.usuario
            .getCodigoEstructuraOrganizacional();

        /* Se define la territorial del usurio */
        this.territorialUsuario = this.getGeneralesService()
            .obtenerTerritorialPorCodigo(codigoTerritorial);

        /* Se carga el solicitante de la solicitud */
        this.solicitanteSolicitud = this.solicitud.getSolicitante();

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar los documentos de una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarDocumentosSolicitud() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#cargarDocumentosSolicitud");

        /* Se buscan los documentos aportados asociados a la solicitud */
        this.listaDocumentosSolicitud = this.getAvaluosService()
            .obtenerSolicitudDocumentacionPorIdSolicitud(
                this.solicitud.getId(), true);

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#cargarDocumentosSolicitud");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar los tramites de una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarTramitesSolicitud() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#cargarTramitesSolicitud");

        this.listaTramitesSolicitud = this.getAvaluosService()
            .consultarAvaluosPorSolicitudId(this.solicitud.getId());

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#cargarTramitesSolicitud");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar los documentos del tramite seleccionado
     *
     * @author rodrigo.hernandez
     */
    public void cargarDocumentosTramite() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#cargarDocumentosTramite");

        this.listaDocumentosTramite = this.getTramiteService()
            .obtenerTramiteDocumentacionPorIdTramite(
                this.tramiteSeleccionado.getTramite().getId());

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#cargarDocumentosTramite");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método preparacion del documento a visualizar
     *
     * @author rodrigo.hernandez
     */
    public void visualizarDocumento() {
        LOGGER.debug("Visualizando Documento");

        FileNameMap fileNameMap = URLConnection.getFileNameMap();

        this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoSeleccionado.getArchivo());

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        /* Se crea la ruta para cargar el documento en el visor */
        ReporteDTO rdto = this.reportes.consultarReporteDeGestorDocumental(
            this.documentoSeleccionado.getIdRepositorioDocumentos());
        this.rutaArchivoTemporal = rdto.getUrlWebReporte();

        /*
         * Se crea la ruta para buscar el documento a descargar (cuando se hace click en el boton
         * Imprimir)
         */
        fileDocumento = rdto.getArchivoReporte();

        previsualizacionDocMB.setRutaArchivoTemporal(this.rutaArchivoTemporal);

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar los TramiteDocumentacion de los tramites seleccionados en la tabla
     *
     * @author rodrigo.hernandez
     */
    private boolean cargarTramiteDocumentacionsTramitesSeleccionados() {

        boolean result = true;

        LOGGER.debug(
            "Inicio AsociacionDocsRadicacionATramitesAvaluoMB#cargarTramiteDocumentacionsTramitesSeleccionados");

        List<Long> listaIdsTramites = new ArrayList<Long>();

        for (Avaluo avaluo : this.tramitesSeleccionados) {
            listaIdsTramites.add(avaluo.getTramite().getId());
        }

        this.listaDocumentosTramitesSeleccionados = this.getAvaluosService()
            .consultarDocumentosTramitePorIdsTramites(listaIdsTramites);

        LOGGER.debug(
            "Fin AsociacionDocsRadicacionATramitesAvaluoMB#cargarTramiteDocumentacionsTramitesSeleccionados");

        return result;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para validar si un tramite ya tiene un documento de radicado asociado
     *
     * @author rodrigo.hernandez
     */
    private boolean validarAsociacionDocumentoTramite(Avaluo tramite,
        Documento documentoRadicado) {
        boolean result = true;

        LOGGER.debug(
            "Inicio AsociacionDocsRadicacionATramitesAvaluoMB#validarAsociacionDocumentoTramite");

        /*
         * Se busca dentro de la lista de TramiteDocumentacions cargada con los tramites
         * seleccionados.
         */
        for (TramiteDocumentacion traDoc : this.listaDocumentosTramitesSeleccionados) {

            /*
             * Se valida si existe un TramiteDocumentacion asociado al tramite que esta siendo
             * validado
             */
            if (traDoc.getTramite().getId()
                .equals(tramite.getTramite().getId())) {

                /*
                 * Si existe un TramiteDocumentacion asociado al tramite que esta siendo validado,
                 * se valida que el documento sea igual al documento del radicado que esta siendo
                 * validado
                 */
                if (traDoc.getDocumentoSoporte().getId()
                    .equals(documentoRadicado.getId())) {
                    result = false;
                    break;
                }
            }
        }

        LOGGER.debug(
            "Fin AsociacionDocsRadicacionATramitesAvaluoMB#validarAsociacionDocumentoTramite");

        return result;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para asociar los documentos seleccionados de la solicitud a los tramites seleccionados
     *
     * @author rodrigo.hernandez
     */
    public void asociarDocumentosATramites() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#asociarDocumentosATramites");

        String mensaje = "";

        List<TramiteDocumentacion> listaTramiteDocumentacions =
            new ArrayList<TramiteDocumentacion>();
        boolean banderaAsociacionPermitida = false;
        boolean banderaValidacionDocumentoAsociadoATramite = false;
        TramiteDocumentacion tramiteDocumentacion = null;

        /*
         * Se valida que se haya escogido tanto algun documento de la radicacion como un tramite
         */
        if (this.documentosSolicitudSeleccionados.length > 0 &&
             this.tramitesSeleccionados.length > 0) {
            banderaAsociacionPermitida = true;
        }

        if (banderaAsociacionPermitida) {

            this.cargarTramiteDocumentacionsTramitesSeleccionados();

            for (SolicitudDocumentacion solDoc : this.documentosSolicitudSeleccionados) {
                for (Avaluo avaluo : this.tramitesSeleccionados) {

                    /*
                     * Se valida que el documento no este asociado al tramite seleccionado
                     */
                    banderaValidacionDocumentoAsociadoATramite = this
                        .validarAsociacionDocumentoTramite(avaluo,
                            solDoc.getSoporteDocumentoId());

                    if (banderaValidacionDocumentoAsociadoATramite) {

                        /* Se crea un nuevo objeto TramiteDocumentacion */
                        tramiteDocumentacion = new TramiteDocumentacion();

                        /* Datos NO NULLABLE */
                        tramiteDocumentacion.setFechaLog(Calendar.getInstance()
                            .getTime());
                        tramiteDocumentacion.setUsuarioLog(this.usuario
                            .getLogin());
                        tramiteDocumentacion.setTipoDocumento(solDoc
                            .getTipoDocumento());
                        tramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
                        tramiteDocumentacion.setTramite(avaluo.getTramite());
                        tramiteDocumentacion.setCantidadFolios(1);
                        tramiteDocumentacion.setAdicional(ESiNo.SI.getCodigo());

                        /* Otros datos */
                        tramiteDocumentacion.setDocumentoSoporte(solDoc
                            .getSoporteDocumentoId());
                        tramiteDocumentacion.setDepartamento(avaluo
                            .getTramite().getDepartamento());
                        tramiteDocumentacion.setMunicipio(avaluo.getTramite()
                            .getMunicipio());
                        tramiteDocumentacion.setIdRepositorioDocumentos(solDoc
                            .getSoporteDocumentoId()
                            .getIdRepositorioDocumentos());

                        listaTramiteDocumentacions.add(tramiteDocumentacion);
                    }
                }

            }

            /*
             * Se guarda/actualiza la lista de TramiteDocumentacion creada a partir de la seleccion
             * de documentos del radicado y tramites del radicado
             */
            this.getTramiteService()
                .guardarActualizarListaTramiteDocumentacions(
                    listaTramiteDocumentacions);

            mensaje = "La asociación de documentos se realizó exitosamente";
            this.addMensajeInfo(mensaje);
        } else {
            if (this.documentosSolicitudSeleccionados.length == 0) {
                mensaje = " No ha escogido ningún documento asociado al radicado.";
                this.addMensajeError(mensaje);
            }

            if (this.tramitesSeleccionados.length == 0) {
                mensaje = " No ha escogido ningún trámite asociado al radicado.";
                this.addMensajeError(mensaje);
            }
        }

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#asociarDocumentosATramites");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para desasociar los documentos asociados a un tramite
     *
     * @author rodrigo.hernandez
     */
    public void desasociarDocumentosATramites() {

        LOGGER.debug(
            "Inicio AsociacionDocsRadicacionATramitesAvaluoMB#desasociarDocumentosATramites");

        String mensaje = "";

        if (this.documentosTramiteSeleccionados.length > 0) {

            List<TramiteDocumentacion> listaTramiteDocumentacions =
                new ArrayList<TramiteDocumentacion>();

            for (TramiteDocumentacion traDoc : this.documentosTramiteSeleccionados) {

                /*
                 * Se deja el documento de soporte como null porque al intentar borrar el registro
                 * de TramiteDocumentacion, se genera error ya que el documento esta asociado con el
                 * radicado
                 */
                traDoc.setDocumentoSoporte(null);

                listaTramiteDocumentacions.add(traDoc);

                /* Se elimina el elemento de la tabla */
                this.listaDocumentosTramite.remove(traDoc);
            }

            this.getTramiteService().borrarListaTramiteDocumentacions(
                listaTramiteDocumentacions);

            mensaje = " La desasociación fue realizada exitosamente.";
            this.addMensajeInfo(mensaje);

        } else {
            mensaje = " No ha escogido ningún documento asociado al tramite.";
            this.addMensajeError(mensaje);
        }
        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#desasociarDocumentosATramites");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para finalizar la asociacion de documentos a los tramites
     *
     * @author rodrigo.hernandez
     */
    public void finalizarAsociacionDocumentos() {
        LOGGER.debug(
            "Inicio AsociacionDocsRadicacionATramitesAvaluoMB#finalizarAsociacionDocumentos");

        boolean banderaAsociacionCompleta = this.validarAsociacionDocumentos();

        if (banderaAsociacionCompleta) {
            this.avanzarProceso();
        }

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#finalizarAsociacionDocumentos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para avanzar process una vez se hayan asociado todos los documentos de la solicitud
     *
     * @author rodrigo.hernandez
     */
//TODO :: snc.avaluos :: implementar este método :: pedro.garcia    
    private void avanzarProceso() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#avanzarProceso");

        throw new NotImplementedException("No se ha implementado la integración con el BPM");

        //LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#avanzarProceso");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para validar que todos los documentos de la solicitud fueron asociados a algun tramite
     *
     * @author rodrigo.hernandez
     */
    private boolean validarAsociacionDocumentos() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#validarAsociacionDocumentos");

        boolean result = true;

        List<Documento> docsTramites = this.getAvaluosService()
            .buscarDocsDeTramiteDocumentacionsPorSolicitudId(
                this.solicitud.getId());

        for (SolicitudDocumentacion solDoc : this.listaDocumentosSolicitud) {
            /*
             * Se valida que los documentos del radicado se encuentren relacionados con algun
             * tramite
             */
            if (!docsTramites.contains(solDoc.getSoporteDocumentoId())) {
                result = false;
                break;
            }
        }

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#validarAsociacionDocumentos");

        return result;
    }

    // -------------------------------------------------------------------------------------------
    public void cerrar() {
        LOGGER.debug("Inicio AsociacionDocsRadicacionATramitesAvaluoMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("Fin AsociacionDocsRadicacionATramitesAvaluoMB#cerrar");
    }

    // end of class
}
