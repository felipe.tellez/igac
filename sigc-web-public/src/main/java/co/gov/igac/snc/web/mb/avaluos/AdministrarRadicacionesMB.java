package co.gov.igac.snc.web.mb.avaluos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioAvaluo;
import co.gov.igac.snc.persistence.entity.tramite.VRadicacion;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosSolicitud;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;

@Component("administrarRadicaciones")
@Scope("session")
public class AdministrarRadicacionesMB extends SNCManagedBean implements
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3192560069126536848L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministrarRadicacionesMB.class);

    private List<Solicitud> solicitudes;
    // Dato usado como filtro en la búsqueda de solicitudes (Radicaciones)
    private FiltroDatosSolicitud filtro;

    /** Se usa para Mostrar las nuevas radicaciones de correspondencia. */
    private List<Solicitud> radicacionesNuevas;

    private Solicitud solicitud;

    //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
    /** Representante legal */
    private Solicitante representanteLegal;
    private SolicitanteSolicitud solicitanteRelacion;

    private TramitePredioAvaluo tramitePredioAvaluo;

    private List<SelectItem> tiposIdentificacion;
    private List<SelectItem> tiposPersona;
    private List<SelectItem> tiposSolicitante;
    private List<SelectItem> tiposTramite;
    private List<SelectItem> tiposSolicitud;
    private List<SelectItem> tiposAvaluosPredios;

    // Lista de países a mostrar en la pantalla
    private List<Pais> paises;
    private EOrden ordenPaises = EOrden.CODIGO;
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;

    private List<Departamento> deptos;
    private List<Municipio> munis;

    private List<VRadicacion> solicitudesRadicaciones;
    private LazyDataModel<Object[]> lazyModel;

    private Tramite tramite;

    private Solicitante solicitante;

    // Dato que almacena el valor del número de radicación de la solicitud de
    // correspondencia a buscar
    private String numRadicacionCorrespondencia;

    /**
     * Método que se ejecuta tras el llamado del Managed Bean en la interfaz.
     *
     * @author fabio.navarrete
     */
    @PostConstruct
    public void init() {
        LOGGER.debug("init AdministrarRadicacionesMB");

        filtro = new FiltroDatosSolicitud();
        solicitudes = new LinkedList<Solicitud>();
        solicitudesRadicaciones = new LinkedList<VRadicacion>();
        tramitePredioAvaluo = new TramitePredioAvaluo();

        setLazyModel(new LazyDataModel<Object[]>() {
            private static final long serialVersionUID = 415950195994545195L;

            @Override
            public List<Object[]> load(int first, int pageSize,
                String sortField, SortOrder sortOrder,
                Map<String, String> filters) {
                List<Object[]> respuestas = poblarRadicaciones(first, pageSize);
                return respuestas;
            }
        });

        radicacionesNuevas = new LinkedList<Solicitud>();
        quemarDatosSolicitante();

        // CARGA DATOS DOMINIO PERSONA_TIPO_IDENTIFICACION -> COMBOBOX
        List<Dominio> listDominio = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PERSONA_TIPO_IDENTIFICACION);
        this.tiposIdentificacion = new ArrayList<SelectItem>();
        this.tiposIdentificacion.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (Dominio d : listDominio) {
            if (!d.getCodigo().equals("NIT")) {
                tiposIdentificacion.add(new SelectItem(d.getCodigo(), d
                    .getCodigo() + "-" + d.getValor()));
            }
        }

        tiposPersona = obtenerDominio(EDominio.PERSONA_TIPO_PERSONA, true);
        tiposSolicitante = obtenerDominio(EDominio.SOLICITANTE_TIPO_SOLICITANT,
            true);
        tiposAvaluosPredios = obtenerDominio(EDominio.PREDIO_TIPO_AVALUO, false);
        tiposTramite = obtenerDominio(EDominio.TRAMITE_TIPO_TRAMITE_AVALUO,
            false);
        tiposSolicitud = obtenerDominio(EDominio.TRAMITE_TIPO_SOLICITUD, false);

        // CARGA PAÍSES
        paises = this.getGeneralesService().getCachePaises();
        deptos = this.getGeneralesService().getCacheDepartamentosPorPais("140");

        // buscar();
    }

    /**
     * Obtiene una lista de objetos SelectItem del dominio indicado.
     *
     * @param eDominio dominio a recuperar.
     * @param seleccion true, agrega un elemento que indica que se debe seleccionar, false obtiene
     * los dominios tal y como están registrados.
     * @return una lista de objetos SelectItem compuestos por el código y el valor del dominio.
     * @author jamir.avila
     */
    private List<SelectItem> obtenerDominio(EDominio eDominio, boolean seleccion) {
        List<SelectItem> resultado = new LinkedList<SelectItem>();
        if (seleccion) {
            resultado.add(new SelectItem("", this.DEFAULT_COMBOS));
        }

        List<Dominio> dominios = this.getGeneralesService()
            .getCacheDominioPorNombre(eDominio);
        for (Dominio dominio : dominios) {
            resultado.add(new SelectItem(dominio.getCodigo(), dominio
                .getValor()));
        }
        return resultado;
    }

    /**
     * Función encargada de crear un nuevo objeto solicitante y quemar los datos iniciales
     * requeridos para el solicitante.
     *
     * @author fabio.navarrete
     */
    public void quemarDatosSolicitante() {
        this.solicitante = obtenerDatosQuemados();
    }

    /**
     * Función encargada de crear un nuevo objeto solicitante para el representante legal y quemar
     * los datos iniciales requeridos para el representante legal
     *
     * @author fabio.navarrete
     * @author javier.aponte Añadi quemar el tipo de persona para el repersentante legal ya que
     * siempre se toma como persona natural
     */
    public void quemarDatosRepresentanteLegal() {
        if (this.representanteLegal != null) {
            Solicitante sol = obtenerDatosQuemados();
            sol.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
            this.solicitanteRelacion.incorporarDatosSolicitante(sol);
            this.representanteLegal = sol;
        }
    }

    /**
     * Método que quema los datos de fechas departamento tipo de identificación y demás requeridos
     * para iniciar los solicitantes requeridos.
     *
     * @author fabio.navarrete.
     * @return
     */
    public Solicitante obtenerDatosQuemados() {
        Solicitante sol = new Solicitante();
        sol.setTipoIdentificacion("CC");
        Pais defaultPais = new Pais();
        defaultPais.setCodigo(Constantes.COLOMBIA);
        sol.setDireccionPais(defaultPais);
        sol.setDireccionDepartamento(new Departamento());
        sol.setDireccionMunicipio(new Municipio());
        sol.setFechaLog(new Date());
        sol.setUsuarioLog("fabio.navarrete.quemado");
        return sol;
    }

    /**
     * Busca las radicaciones
     */
    public void buscar() {
        LOGGER.info("BuscarRadicacionesMB::buscar()");

        poblarRadicaciones(0, 10);
        long totalRegistros = this.getTramiteService()
            .contarSolicitudesCotizacionAvaluos(filtro);
        lazyModel.setRowCount((int) totalRegistros);
    }

    /**
     * Adiciona un predio en la solicitud. Para adicionar el predio se crea un trámite asociado con
     * la solicitud.
     *
     * @author jamir.avila
     */
    public void adicionarPredio() {
        LOGGER.info("inicio de adicionar predio");

        // TODO, Jamir: el siguiente fragmento de código está "quemado":
        Long solicitudId = 17L;
        Solicitud solicitud = null;
        try {
            solicitud = this.getTramiteService().findSolicitudPorId(solicitudId);
        } catch (Exception e) {
            LOGGER.error("solicitud null");
            // TODO: Jamir 20/05/2011 aquí va una excepción.
            e.printStackTrace();
            return;
        }
        LOGGER.info("solicitud recuperada");

        //
        // TODO Jamir: R E V I S A R
        //
        tramite = obtenerTramiteAsociadoConLaSolicitud(solicitud.getId());
        if (tramite == null) {
            //tramite = crearTramiteAsociadoConLaSolicitud(solicitud);
        }

        LOGGER.info("id del trámite de solicitud de cotización: " +
             tramite.getId());
        LOGGER.debug("tramitePredioAvaluo.departamentoCodigo: " +
             tramitePredioAvaluo.getDepartamentoCodigo());
        LOGGER.debug("tramitePredioAvaluo.areaConstruccion: " +
             tramitePredioAvaluo.getAreaConstruccion());

        tramitePredioAvaluo.setTramite(tramite);
        this.getTramiteService().adicionarPredioAAvaluo(tramitePredioAvaluo);
    }

    public String onFlowProcess(FlowEvent event) {
        // LOGGER.info("Current wizard step:" + event.getOldStep());
        // LOGGER.info("Next step:" + event.getNewStep());

        if (event.getOldStep().equalsIgnoreCase("solicitanteTab")) {
            guardarSolicitante();
        } else if (event.getOldStep().equalsIgnoreCase("representanteLegalTab")) {
            saveRepresentanteLegal();
        } else if (event.getOldStep().equalsIgnoreCase("tipoTramiteTab")) {
            //TODO Jamir: habilitar el siguiente método
            //saveTramite();
        } else if (event.getOldStep().equalsIgnoreCase("informacionPrediosTab")) {
            ;
        } else if (event.getOldStep().equalsIgnoreCase("confirmacionTab")) {
            ;
        }

        if (event.getNewStep().equalsIgnoreCase("solicitanteTab")) {
            ;
        } else if (event.getNewStep().equalsIgnoreCase("representanteLegalTab")) {
            ;
        } else if (event.getNewStep().equalsIgnoreCase("tipoTramiteTab")) {
            ;
        } else if (event.getNewStep().equalsIgnoreCase("informacionPrediosTab")) {
            cargarPredios();
        } else if (event.getNewStep().equalsIgnoreCase("confirmacionTab")) {
            ;
        }

        return event.getNewStep();
    }

    public FiltroDatosSolicitud getFiltro() {
        return filtro;
    }

    public void setFiltro(FiltroDatosSolicitud filtro) {
        this.filtro = filtro;
    }

    public List<Solicitud> getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(List<Solicitud> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public void buscarSolicitudes() {
        solicitudes = this.getTramiteService().buscarSolicitudes(filtro, 0, 0);
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public void guardarSolicitante() {
        Solicitante auxSolicitante = this.getTramiteService()
            .guardarActualizarSolicitante(this.solicitante);
        if (auxSolicitante != null) {
            this.addMensajeInfo("El solicitante fue almacenado correctamente");
            this.solicitud.getSolicitanteSolicituds().get(0).setSolicitante(auxSolicitante);
            this.solicitud.getSolicitanteSolicituds().get(0).incorporarDatosSolicitante(
                auxSolicitante);
        }
    }

    /**
     * Método encargado de buscar un solicitante a partir del tipo de documento y su número de
     * documento. Realiza un llamado a la fachada de trámites.
     *
     * @author fabio.navarrete
     */
    public void buscarSolicitante() {
        LOGGER.debug("buscarSolicitante");
        Solicitante sol = this.getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
                this.solicitante
                    .getNumeroIdentificacion(), this.solicitante
                    .getTipoIdentificacion());
        if (sol != null) {
            this.solicitante = sol;
            asignarRepresentanteLegal(this.solicitante);
        } else {
            LOGGER.debug("buscarSolicitante Return null");
            String tipoIdentif = this.solicitante
                .getTipoIdentificacion();
            String numDoc = this.solicitante
                .getNumeroIdentificacion();
            String tipoPersona = this.solicitante
                .getTipoPersona();
            quemarDatosSolicitante();
            this.solicitante.setTipoIdentificacion(tipoIdentif);
            this.solicitante.setNumeroIdentificacion(numDoc);
            this.solicitante.setTipoPersona(tipoPersona);
        }
    }

    /**
     * Método encargado de buscar un representante legal a partir del tipo de documento y su número
     * de documento. Realiza un llamado a la fachada de trámites.
     */
    public void buscarRepresentanteLegal() {
        //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
        LOGGER.debug("buscarRepresentanteLegal");
        Solicitante sol = this.getTramiteService()
            .buscarSolicitantePorTipoYNumeroDeDocumento(
                this.representanteLegal
                    .getNumeroIdentificacion(),
                this.representanteLegal
                    .getTipoIdentificacion());
        if (sol != null) {
            this.representanteLegal = sol;
            this.solicitanteRelacion.incorporarDatosSolicitante(sol);
        } else {
            LOGGER.debug("buscarRepresentanteLegal Return null");
            String tipoIdentif = this.representanteLegal
                .getTipoIdentificacion();
            String numDoc = this.representanteLegal
                .getNumeroIdentificacion();
            quemarDatosSolicitante();
            this.representanteLegal
                .setTipoIdentificacion(tipoIdentif);
            this.representanteLegal
                .setNumeroIdentificacion(numDoc);
        }
    }

    public void asignarRepresentanteLegal(Solicitante soli) {
        //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
    }

    public List<SelectItem> getTiposIdentificacion() {
        return tiposIdentificacion;
    }

    public void setTiposIdentificacion(List<SelectItem> tiposIdentificacion) {
        this.tiposIdentificacion = tiposIdentificacion;
    }

    public List<SelectItem> getTiposPersona() {
        return tiposPersona;
    }

    public void setTiposPersona(List<SelectItem> tiposPersona) {
        this.tiposPersona = tiposPersona;
    }

    public List<SelectItem> getTiposSolicitante() {
        return tiposSolicitante;
    }

    public void setTiposSolicitante(List<SelectItem> tiposSolicitante) {
        this.tiposSolicitante = tiposSolicitante;
    }

    public List<SelectItem> getPaises() {

        List<SelectItem> paises = new ArrayList<SelectItem>();
        if (this.ordenPaises.equals(EOrden.CODIGO)) {
            Collections.sort(this.paises);
        } else {
            Collections.sort(this.paises, Pais.getComparatorNombre());
        }

        for (Pais p : this.paises) {
            if (this.ordenPaises.equals(EOrden.CODIGO)) {
                paises.add(new SelectItem(p.getCodigo(), StringUtils.leftPad(
                    p.getCodigo(), 3, '0') +
                     "-" + p.getNombre()));
            } else {
                paises.add(new SelectItem(p.getCodigo(), p.getNombre()));
            }
        }
        return paises;
    }

    public List<SelectItem> getDepartamentos() {
        List<SelectItem> departamentos = new LinkedList<SelectItem>();
        departamentos.add(new SelectItem("", "Seleccionar..."));
        if (this.solicitud != null && this.solicitante != null && this.solicitante.
            getDireccionPais() != null) {
            deptos = this.getGeneralesService().getCacheDepartamentosPorPais(this.solicitante.
                getDireccionPais().getCodigo());
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                Collections.sort(this.deptos);
            } else {
                Collections.sort(this.deptos, Departamento.getComparatorNombre());
            }
            for (Departamento departamento : this.deptos) {
                if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                    departamentos.add(new SelectItem(departamento.getCodigo(), departamento.
                        getCodigo() + "-" + departamento.getNombre()));
                } else {
                    departamentos.add(new SelectItem(departamento.getCodigo(), departamento.
                        getNombre()));
                }
            }
        }
        return departamentos;
    }

    /**
     * @author jamir.avila
     * @return
     */
    public List<SelectItem> getDepartamentosColombia() {
        List<SelectItem> departamentos = new LinkedList<SelectItem>();
        departamentos.add(new SelectItem("", "Seleccionar..."));

        List<Departamento> departamentosColombia = this.getGeneralesService().
            getCacheDepartamentosPorPais("140");
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(departamentosColombia);
        } else {
            Collections.sort(departamentosColombia, Departamento.getComparatorNombre());
        }
        for (Departamento departamento : departamentosColombia) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                departamentos.add(new SelectItem(departamento.getCodigo(),
                    departamento.getCodigo() + "-" + departamento.getNombre()));
            } else {
                departamentos.
                    add(new SelectItem(departamento.getCodigo(), departamento.getNombre()));
            }
        }

        return departamentos;
    }

    public List<SelectItem> getMunicipios() {
        List<SelectItem> municipiosC = new ArrayList<SelectItem>();
        municipiosC.add(new SelectItem("", "Seleccionar..."));
        if (this.solicitud != null && this.solicitante != null &&
             this.solicitante.getDireccionPais() != null && this.solicitante.
            getDireccionDepartamento() != null) {
            munis = this.getGeneralesService().getCacheMunicipiosPorDepartamento(this.solicitante.
                getDireccionDepartamento().getCodigo());
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(munis);
            } else {
                Collections.sort(munis, Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munis) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    municipiosC.add(new SelectItem(municipio.getCodigo(), municipio.
                        getCodigo3Digitos() + "-" + municipio.getNombre()));
                } else {
                    municipiosC.add(new SelectItem(municipio.getCodigo(), municipio.getNombre()));
                }
            }
        }

        return municipiosC;
    }

    /**
     * @author jamir.avila
     * @return
     */
    public List<SelectItem> getMunicipiosColombia() {
        List<SelectItem> municipios = new LinkedList<SelectItem>();
        municipios.add(new SelectItem("", "Seleccionar..."));

        if (tramitePredioAvaluo.getDepartamentoCodigo() != null) {
            List<Municipio> municipiosColombia = this.getGeneralesService().
                getCacheMunicipiosPorDepartamento(tramitePredioAvaluo.getDepartamentoCodigo());
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(municipiosColombia);
            } else {
                Collections.sort(municipiosColombia,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipioColombia : municipiosColombia) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    municipios.add(new SelectItem(municipioColombia.getCodigo(), municipioColombia.
                        getCodigo3Digitos() + "-" + municipioColombia.getNombre()));
                } else {
                    municipios.add(new SelectItem(municipioColombia.getCodigo(), municipioColombia.
                        getNombre()));
                }
            }
        }

        return municipios;
    }

    public String getOrdenPaises() {
        return this.ordenPaises.toString();
    }

    public String getOrdenDepartamentos() {
        return this.ordenDepartamentos.toString();
    }

    public String getOrdenMunicipios() {
        return this.ordenMunicipios.toString();
    }

    public void cambiarOrdenPaises() {
        if (this.ordenPaises.equals(EOrden.CODIGO)) {
            this.ordenPaises = EOrden.NOMBRE;
        } else {
            this.ordenPaises = EOrden.CODIGO;
        }
    }

    public void cambiarOrdenDepartamentos() {
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
        }
    }

    public void cambiarOrdenMunicipios() {
        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
        }
    }

    public void setSelectedPaisCodigo(String codigo) {
        Pais paisSelected = new Pais();
        paisSelected.setCodigo(Constantes.COLOMBIA);
        for (Pais pais : paises) {
            if (codigo.equals(pais.getCodigo())) {
                paisSelected = pais;
                break;
            }
        }

        if (this.solicitud != null && this.solicitante != null) {
            this.solicitante.setDireccionPais(paisSelected);
            this.solicitante.setDireccionDepartamento(new Departamento());
        }
    }

    public String getSelectedPaisCodigo() {
        if (this.solicitud != null && this.solicitante != null &&
             this.solicitante.getDireccionPais() != null) {
            return this.solicitante.getDireccionPais().getCodigo();
        }
        return null;
    }

    public void setSelectedDepartamentoCodigo(String codigo) {
        LOGGER.
            info("AdministrarRadicacionesMB#setSelectedDepartamentoCodigo(" + codigo + ") INICIO");
        Departamento dptoSeleccionado = null;
        if (codigo != null && deptos != null) {
            for (Departamento departamento : deptos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.solicitud != null && this.solicitante != null) {
            this.solicitante.setDireccionDepartamento(dptoSeleccionado);
        }
        LOGGER.info("AdministrarRadicaciones#setSelectedDepartamentoCodigo(" + codigo + ") FIN");
    }

    public String getSelectedDepartamentoCodigo() {
        LOGGER.info("AdministrarRadicacionesMB#getSelectedDepartamentoCodigo() INICIO");
        if (this.solicitud != null && this.solicitante != null && this.solicitante.
            getDireccionDepartamento() != null) {
            return this.solicitante.getDireccionDepartamento().getCodigo();
        }
        LOGGER.info("AdministrarRadicacionesMB#getSelectedDepartamentoCodigo() FIN");
        return null;
    }

    public void setSelectedMunicipioCodigo(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null) {
            if (munis != null) {
                for (Municipio municipio : munis) {
                    if (codigo.equals(municipio.getCodigo())) {
                        municipioSelected = municipio;
                        break;
                    }
                }
            }
        }
        if (this.solicitud != null && this.solicitante != null) {
            this.solicitante.setDireccionMunicipio(municipioSelected);
        }
    }

    public String getSelectedMunicipioCodigo() {
        if (this.solicitud != null && this.solicitante != null && this.solicitante.
            getDireccionMunicipio() != null) {
            return this.solicitante.getDireccionMunicipio().getCodigo();
        }
        return null;
    }

    //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
    /*
     * public SolicitanteRelacion getSolicitanteRelacion() { return representanteLegal;
	} */
    //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
    /*
     * public void setSolicitanteRelacion(SolicitanteRelacion solicitanteRelacion) {
     * this.representanteLegal = solicitanteRelacion;
	}* */
    /**
     * Procedimiento que genera los datos necesarios para la relación del solicitante con el
     * representante legal.
     *
     * @author fabio.navarrete
     */
    public void cambioTipoPersona() {
        //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
        if (this.representanteLegal == null) {
            /* this.representanteLegal = new SolicitanteRelacion(); Solicitante representante = new
             * Solicitante();
             * representante.setTipoSolicitante(ESolicitanteTipoSolicitant.PRIVADA.getCodigo());
             * representante.setFechaLog(new Date());
             * representante.setUsuarioLog("fabio.navarrete.quemado");
             * this.representanteLegal.setSolicitanteBySolicitanteRelacionadoId(representante);
             * this.representanteLegal.setFechaDesde(new Date());
             * this.representanteLegal.setFechaLog(new Date()); */
            // TODO: cambiar el usuario por el de la sesión.
            this.representanteLegal.setUsuarioLog("fabio.navarrete.quemado");
        }
    }

    /**
     * Método que guarda el dato solicitanteRelación que almacena la relación entre el solicitante y
     * el representante legal.
     *
     * @author fabio.navarrete
     */
    public void saveRepresentanteLegal() {
        //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
        LOGGER.info("saveRepresentanteLegal");

        /*
         * this.representanteLegal.setSolicitanteBySolicitanteId(this.solicitante); if
         * (this.tramiteService .guardarActualizarSolicitanteRelacion(this.representanteLegal) !=
         * null) { this.addMensajeInfo("El representante legal fue almacenado correctamente");
		} */
    }

    private List<Object[]> poblarRadicaciones(int first, int pageSize) {
        List<Object[]> resultados = new LinkedList<Object[]>();

        solicitudesRadicaciones = this.getTramiteService()
            .buscarSolicitudesCotizacionAvaluos(filtro, first, pageSize);

        for (VRadicacion vRadicacion : solicitudesRadicaciones) {
            Object[] campos = new Object[9];
            int posicion = 0;
            campos[posicion++] = vRadicacion.getNumeroRadicacion();
            campos[posicion++] = vRadicacion.getFechaRadicacion();
            campos[posicion++] = vRadicacion.getNumeroRadicacionInicial();
            campos[posicion++] = vRadicacion.getNumeroRadicacionDefinitiva();
            campos[posicion++] = vRadicacion.getNombreCompleto();
            campos[posicion++] = vRadicacion.getTipoPersona();
            campos[posicion++] = vRadicacion.getDireccionPaisNombre();
            campos[posicion++] = vRadicacion.getDireccionDepartamentoNombre();
            campos[posicion++] = vRadicacion.getDireccionMunicipioNombre();
            resultados.add(campos);
        }

        return resultados;
    }

    public List<VRadicacion> getSolicitudesRadicaciones() {
        return solicitudesRadicaciones;
    }

    public void setSolicitudesRadicaciones(
        List<VRadicacion> solicitudesRadicaciones) {
        this.solicitudesRadicaciones = solicitudesRadicaciones;
    }

    public LazyDataModel<Object[]> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Object[]> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public void onTabChange(TabChangeEvent ev) {
        LOGGER.info("onTabChange(...)");
        ev.getTab();
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public List<SelectItem> getTiposTramite() {
        return tiposTramite;
    }

    public void setTiposTramite(List<SelectItem> tiposTramite) {
        this.tiposTramite = tiposTramite;
    }

    public List<SelectItem> getTiposSolicitud() {
        return tiposSolicitud;
    }

    public void setTiposSolicitud(List<SelectItem> tiposSolicitud) {
        this.tiposSolicitud = tiposSolicitud;
    }

    public TramitePredioAvaluo getTramitePredioAvaluo() {
        return tramitePredioAvaluo;
    }

    public void setTramitePredioAvaluo(TramitePredioAvaluo tramitePredioAvaluo) {
        this.tramitePredioAvaluo = tramitePredioAvaluo;
    }

    public void saveTramite() {
        throw new java.lang.UnsupportedOperationException(
            "AdministrarRadicacionesMB#saveTramite() no implementado.");
    }

    public void saveSolicitud() {
        throw new java.lang.UnsupportedOperationException(
            "AdministrarRadicacionesMB#saveSolicitud() no implementado.");
    }

    public List<SelectItem> getTiposAvaluosPredios() {
        return tiposAvaluosPredios;
    }

    public void setTiposAvaluosPredios(List<SelectItem> tiposAvaluosPredios) {
        this.tiposAvaluosPredios = tiposAvaluosPredios;
    }

    /**
     * Método que busca una radicación de correspondencia usando el enlace del procedimiento
     * almacenado.
     *
     * @author fabio.navarrete
     */
    public void buscarRadicacionCorrespondencia() {
        LOGGER.info("AdministrarRadicacionesMB#buscarRadicacionCorrespondencia()");
        Solicitud radicacionNueva = this.getTramiteService()
            .obtenerSolicitudAvaluosCorrespondencia(this.numRadicacionCorrespondencia, this.
                obtenerDatosUsuarioAutenticado());

        if (radicacionNueva != null) {
            LOGGER.info("id de la nueva radicación: " + radicacionNueva.getId());
            radicacionesNuevas = new LinkedList<Solicitud>();
            radicacionesNuevas.add(radicacionNueva);
        } else {
            this.addMensajeError("Radicación no encontrada");
        }
    }

    private Tramite obtenerTramiteAsociadoConLaSolicitud(Long solicitudId) {
        List<Tramite> tramitesSolicitud = this.getTramiteService()
            .consultarTramitesSolicitud(solicitudId);

        LOGGER.info("???: " + tramitesSolicitud);
        if (tramitesSolicitud == null || tramitesSolicitud.size() == 0) {
            LOGGER.error("No existe un trámite para la solicitud: " +
                 solicitudId);
            // TODO: Jamir 20/05/2011 aquí va una excepción.
            return null;
        }
        Tramite tramite = tramitesSolicitud.get(0);
        return tramite;
    }

    /**
     * Método encargado de cargar el campo solicitanteRelación local a partir de los datos cargados
     * de la solicitud obtenida de correspondencia.
     *
     * @author fabio.navarrete
     * @param solicitante
     */
    private void loadSolicitanteRelacion(Solicitante solicitante) {
        //TODO: fabio.navarrete y jamir.avila :: 02-08-2011 :: corregir el manejo de la relación con representante legal :: fabio.navarrete
        LOGGER.info("AdministrarRadicacionMB#loadSolicitanteRelacion() INICIO");

        /*
         * if (solicitante != null) { LOGGER.info("solicitante: " + solicitante); for
         * (SolicitanteRelacion solRel : solicitante .getSolicitanteRelacionsForSolicitanteId()) {
         * LOGGER.info("solicitante: " + solicitante.getNombreCompleto()); if
         * (solRel.getTipoRelacion().equals( Constantes.REPRESENTANTE_LEGAL) &&
         * solRel.getFechaHasta() == null) { this.representanteLegal = solRel; break; } }
         * cambioTipoPersona();
		} */
        LOGGER.info("AdministrarRadicacionMB#loadSolicitanteRelacion() FIN");
    }

    /**
     * Método que carga los predios asociados con el correspondiente avalúo.
     *
     * @author jamir.avila
     */
    private void cargarPredios() {
        LOGGER.info("AdministrarRadicacionesMB#cargarPredios() INICIO");
        if (this.tramite != null) {
            //TODO aquí se debe asegurar que se cuenta con los predios.
        }
        LOGGER.info("AdministrarRadicacionesMB#cargarPredios() INICIO");
    }

    public String getNumRadicacionCorrespondencia() {
        return numRadicacionCorrespondencia;
    }

    public void setNumRadicacionCorrespondencia(
        String numRadicacionCorrespondencia) {
        this.numRadicacionCorrespondencia = numRadicacionCorrespondencia;
    }

    /**
     * Inicializa el contenido del asistente, incluyendo la creación del trámite asociado con la
     * solicitud.
     *
     * @author jamir.avila
     */
    public void desplegarRadicacionInicial() {
        LOGGER.info("AdministrarRadicacionesMB#desplegarRadicacionInicial INICIO");

        UsuarioDTO usuario = new UsuarioDTO();
        // TODO: Jamir: el usuario está quemado.
        usuario.setLogin("[jamir.avila]");

        if (radicacionesNuevas == null || radicacionesNuevas.size() == 0) {
            // TODO Jamir: lanzar la excepción.
            throw new IllegalStateException("No existe una radicación nueva!");
        }
        Solicitud radicacionNueva = this.radicacionesNuevas.get(0);

        // crear una nueva solicitud y trámite
        solicitud = new Solicitud();

        // Iniciar el pais de la solicitud a Colombia
        Pais direccionPais = radicacionNueva.getSolicitanteSolicituds().get(0).getDireccionPais();
        if (direccionPais == null) {
            for (Pais pais : paises) {
                if (pais.getNombre().equalsIgnoreCase("colombia")) {
                    direccionPais = pais;
                    break;
                }
            }
            if (direccionPais == null) {
                throw new IllegalStateException(
                    "Imposible encontrar Colombia en la lista de paises.");
            }
        }

        // TODO Jamir: ESolicitudEstado.RECIBIDA.getCodigo() generar error!
        solicitud.setEstado("1");
        solicitud.setFecha(new java.util.Date());
        solicitud.setFechaLog(new java.util.Date());
        solicitud.setTipo(ETramiteTipoTramite.SOLICITUD_DE_COTIZACION.getCodigo());
        solicitud.setFinalizado(ESiNo.NO.getCodigo());
        solicitud.setFolios(radicacionNueva.getFolios());
        solicitud.setAnexos(radicacionNueva.getFolios());
        solicitud.setUsuarioLog(usuario.getLogin());
        solicitud.setNumero(numRadicacionCorrespondencia);

        Tramite tramite = new Tramite();
        tramite.setSolicitud(solicitud);
        tramite.setUsuarioLog(usuario.getLogin());
        tramite.setArchivado(ESiNo.SI.getCodigo());
        tramite.setEstado(ETramiteEstado.RECIBIDO.getCodigo());
        // TODO: Jamir: ¿cuál es el tipo de trámite correspondiente?
        tramite.setTipoTramite("[COTIZACION AVALUO]");
        // TODO: Jamir: ¿cuál es el tipo de solicitud correspondiente?

//TODO jamir.avila arrelar. modelo de datos cambió
        //tramite.setTipoSolicitud(ESolicitudTipo.SIAVAL.getCodigo());
        tramite.setFechaLog(new java.util.Date());

        solicitud.getTramites().add(tramite);

        LOGGER.info("Creando la solicitud y el trámite?");
        solicitud = this.getTramiteService().crearSolicitudTramite(usuario, solicitud);
        LOGGER.info("id de la solicitud: " + solicitud.getId());
        for (Tramite unTramite : solicitud.getTramites()) {
            LOGGER.info("id de trámite: " + unTramite.getId());
        }

        //tramite = tramiteService.crearSolicitudCotizacionAvaluo(tramite);
        LOGGER.info("AdministrarRadicacionesMB#desplegarRadicacionInicial FIN");
    }

    public List<Solicitud> getRadicacionesNuevas() {
        return radicacionesNuevas;
    }

    public void setRadicacionesNuevas(List<Solicitud> radicacionesNuevas) {
        this.radicacionesNuevas = radicacionesNuevas;
    }

    /**
     * Determina si los países están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenPaisesNombre() {
        return this.ordenPaises.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenDepartamentosNombre() {
        return this.ordenDepartamentos.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isOrdenMunicipiosNombre() {
        return this.ordenMunicipios.equals(EOrden.NOMBRE);
    }

    public Solicitante getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Solicitante solicitante) {
        this.solicitante = solicitante;
    }
}
