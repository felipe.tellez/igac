package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEntidadEstado;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoDesBloqueo;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.EnumMap;
import java.util.HashMap;

/**
 * Clase que hace de MB para manejar la consulta y el desbloqueo de predios
 *
 * @author juan.agudelo
 *
 * @modified by leidy.gonzalez:: #12497:: 27/05/2015:: Se elimina validacion que se estaba
 * realizando anteriormente para la fecha de desbloqueo e impedia que se desbloquearan los predios.
 */
@Component("desbloqueoPredio")
@Scope("session")
public class DesbloqueoPredioMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DesbloqueoPredioMB.class);

    /*
     * interfaces de servicio
     */
    @Autowired
    private IContextListener contexto;

    @Autowired
    private GeneralMB generalService;

    /**
     * variable donde se cargan los resultados paginados de la consulta de predios bloqueados
     */
    private LazyDataModel<PredioBloqueo> lazyAssignedBloqueos;

    /**
     * lista de PredioBloqueo paginados
     */
    private List<PredioBloqueo> rows;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Predio a desbloquear
     */
    private String numeroPredial;

    /**
     * Variable de control de archivo de desbloqueo subido
     */
    private int conteoArchivoDesbloqueo;

    /**
     * Archivo resultado cargado
     */
    private File archivoResultado;

    /**
     * filtro de datos para la consulta de predios bloqueados
     */
    private FiltroDatosConsultaPrediosBloqueo filtroCPredioB;

    /**
     * Bandera de visualización del boton desbloquear
     */
    private boolean banderaBotonDesbloquear;
    private boolean banderaBotonGuardar;

    /**
     * Bandera de visualización de la tabla de resultado de la busqueda de bloqueos existentes sobre
     * un predio
     */
    private boolean banderaBuscar;

    /**
     * Bandera de visualización del boton de busqueda
     */
    private boolean banderaEjecutaBusqueda;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteDesbloqueo;

    /**
     * Tipo de avaluo en predio
     */
    private String tipoAvaluo;

    /**
     * Codigo de sector en predio
     */
    private String sectorCodigo;

    /**
     * Lista que contiene la seleccion de predios bloqueados para desbloquear
     */
    private PredioBloqueo prediosDesbloqueoSeleccionados;

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    private StreamedContent fileTempDownload;

    /**
     * Archivo del reporte de diligencia de notificación personal
     */
    private File fileTemp;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Conteo de bloqueos actuales asociados a un predio
     */
    @SuppressWarnings("unused")
    private int count;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    private ArrayList<SelectItem> deptosEntidadItemList;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamentoCod;
    private String selectedMunicipioCod;

    /**
     * Mensaje de salida del bloqueo de persona
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * Orden para la listas de departamento y municipio
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;

    /**
     * datos de la busqueda
     */
    private Departamento departamento;
    private Municipio municipio;

    /**
     * indican si los combos están ordenados por nombre (si no, lo están por código)
     */
    private boolean municipiosOrderedByNombre;
    private boolean departamentosOrderedByNombre;
    private boolean banderaGestionDesbloqueoPredio;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosBuscadorItemList;

    /**
     * Listas de items para el combo de tipo de bloqueo
     */
    private List<SelectItem> tiposBloqueoItemList;

    /**
     * valor seleccionado en el combo tipo de bloqueo
     */
    private String selectedTipoBloqueo;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamento;
    private String selectedMunicipio;

    /**
     * Determina si la opción de restitución de tierras fue seleccionada
     */
    private boolean selectedRestitucionTierras;

    /**
     * Lista de tipo {@link Entidad} con las entidades que se encuentran activas
     */
    private List<SelectItem> entidades;

    /**
     * Variable para asociarel id de la {
     *
     * @Entidad} que ordena el bloqueo del {@link Predio}
     */
    private Long idEntidadBloqueo;

    // ------------- methods ------------------------------------
    public Map<String, List<SelectItem>> getMunicipiosDeptos() {
        return municipiosDeptos;
    }

    public void setMunicipiosDeptos(Map<String, List<SelectItem>> municipiosDeptos) {
        this.municipiosDeptos = municipiosDeptos;
    }

    public List<SelectItem> getMunicipiosBuscadorItemList() {
        return municipiosBuscadorItemList;
    }

    public void setMunicipiosBuscadorItemList(List<SelectItem> municipiosBuscadorItemList) {
        this.municipiosBuscadorItemList = municipiosBuscadorItemList;
    }

    public ArrayList<SelectItem> getDeptosEntidadItemList() {
        return deptosEntidadItemList;
    }

    public void setDeptosEntidadItemList(final ArrayList<SelectItem> deptosEntidadItemList) {
        this.deptosEntidadItemList = deptosEntidadItemList;
    }

    public String getSelectedTipoBloqueo() {
        return selectedTipoBloqueo;
    }

    public void setSelectedTipoBloqueo(String selectedTipoBloqueo) {
        this.selectedTipoBloqueo = selectedTipoBloqueo;
    }

    public String getSelectedDepartamento() {
        return selectedDepartamento;
    }

    public void setSelectedDepartamento(String selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public String getSelectedMunicipio() {
        return selectedMunicipio;
    }

    public void setSelectedMunicipio(String selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public boolean isSelectedRestitucionTierras() {
        return selectedRestitucionTierras;
    }

    public void setSelectedRestitucionTierras(boolean selectedRestitucionTierras) {
        this.selectedRestitucionTierras = selectedRestitucionTierras;
    }

    public List<SelectItem> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<SelectItem> entidades) {
        this.entidades = entidades;
    }

    public List<SelectItem> getTiposBloqueoItemList() {
        return tiposBloqueoItemList;
    }

    public void setTiposBloqueoItemList(List<SelectItem> tiposBloqueoItemList) {
        this.tiposBloqueoItemList = tiposBloqueoItemList;
    }

    public boolean isBanderaEjecutaBusqueda() {
        return banderaEjecutaBusqueda;
    }

    public void setBanderaEjecutaBusqueda(boolean banderaEjecutaBusqueda) {
        this.banderaEjecutaBusqueda = banderaEjecutaBusqueda;
    }

    public Long getIdEntidadBloqueo() {
        return idEntidadBloqueo;
    }

    public void setIdEntidadBloqueo(Long idEntidadBloqueo) {
        this.idEntidadBloqueo = idEntidadBloqueo;
    }

    public boolean isBanderaGestionDesbloqueoPredio() {
        return banderaGestionDesbloqueoPredio;
    }

    public void setBanderaGestionDesbloqueoPredio(
        boolean banderaGestionDesbloqueoPredio) {
        this.banderaGestionDesbloqueoPredio = banderaGestionDesbloqueoPredio;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public PredioBloqueo getPrediosDesbloqueoSeleccionados() {
        return prediosDesbloqueoSeleccionados;
    }

    public FiltroDatosConsultaPrediosBloqueo getFiltroCPredioB() {
        return filtroCPredioB;
    }

    public void setFiltroCPredioB(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB) {
        this.filtroCPredioB = filtroCPredioB;
    }

    public void setPrediosDesbloqueoSeleccionados(
        PredioBloqueo prediosDesbloqueoSeleccionados) {
        this.prediosDesbloqueoSeleccionados = prediosDesbloqueoSeleccionados;
    }

    public Documento getDocumentoSoporteDesbloqueo() {
        return documentoSoporteDesbloqueo;
    }

    public StreamedContent getFileTempDownload() {

        InputStream stream;

        try {
            stream = new FileInputStream(this.fileTemp);
            this.fileTempDownload = new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_PDF, "Documento_Soporte_Desbloqueo.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte");
        }

        return fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public void setDocumentoSoporteDesbloqueo(
        Documento documentoSoporteDesbloqueo) {
        this.documentoSoporteDesbloqueo = documentoSoporteDesbloqueo;
    }

    public int getConteoArchivoDesbloqueo() {
        return conteoArchivoDesbloqueo;
    }

    public void setConteoArchivoDesbloqueo(int conteoArchivoDesbloqueo) {
        this.conteoArchivoDesbloqueo = conteoArchivoDesbloqueo;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public String getSectorCodigo() {
        return sectorCodigo;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return mensajeSalidaConfirmationDialog;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return this.municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return this.departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(
        boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public boolean isBanderaBuscar() {
        return banderaBuscar;
    }

    public void setBanderaBuscar(boolean banderaBuscar) {
        this.banderaBuscar = banderaBuscar;
    }

    public boolean isBanderaBotonDesbloquear() {
        return banderaBotonDesbloquear;
    }

    public void setBanderaBotonDesbloquear(boolean banderaBotonDesbloquear) {
        this.banderaBotonDesbloquear = banderaBotonDesbloquear;
    }

    public PredioBloqueo getPrediosBloqueoSeleccionados() {
        return prediosDesbloqueoSeleccionados;
    }

    public void setPrediosBloqueoSeleccionados(
        PredioBloqueo prediosBloqueoSeleccionados) {
        this.prediosDesbloqueoSeleccionados = prediosBloqueoSeleccionados;
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public LazyDataModel<PredioBloqueo> getLazyAssignedBloqueos() {
        return lazyAssignedBloqueos;
    }

    public void setLazyAssignedBloqueos(
        LazyDataModel<PredioBloqueo> lazyAssignedBloqueos) {
        this.lazyAssignedBloqueos = lazyAssignedBloqueos;
    }

    public List<PredioBloqueo> getRows() {
        return rows;
    }

    public void setRows(List<PredioBloqueo> rows) {
        this.rows = rows;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    // --------------------------------------------------------------------------------
    public String getSelectedDepartamentoCod() {
        return this.selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String codigo) {

        this.selectedDepartamentoCod = codigo;
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento departamento : this.departamentos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        this.departamento = dptoSeleccionado;
    }

    // ---------------------------------------------------------------------
    public String getSelectedMunicipioCod() {
        return this.selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String codigo) {
        this.selectedMunicipioCod = codigo;

        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.municipio = municipioSelected;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("BloqueoPredioMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.banderaBotonDesbloquear = true;
        this.banderaBuscar = false;
        this.prediosDesbloqueoSeleccionados = null;

        this.ordenDepartamentos = EOrden.NOMBRE;
        this.departamentosOrderedByNombre = true;
        this.ordenMunicipios = EOrden.NOMBRE;
        this.municipiosOrderedByNombre = true;
        this.banderaEjecutaBusqueda = false;

        // Inicialización de departamento
        for (String sTemp : this.usuario.getRoles()) {
            if (sTemp.equals(ERol.SUBDIRECTOR_CATASTRO.toString())) {
                this.departamentos = (ArrayList<Departamento>) this
                    .getGeneralesService()
                    .buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
                        Constantes.COLOMBIA);
                break;
            } else {

                this.departamentos = (ArrayList<Departamento>) this
                    .getGeneralesService()
                    .getDepartamentoByCodigoEstructuraOrganizacional(
                        this.usuario
                            .getCodigoEstructuraOrganizacional());

                break;
            }
        }

        this.filtroCPredioB = new FiltroDatosConsultaPrediosBloqueo();
        this.mensajeSalidaConfirmationDialog = "El desmarque" +
             " se realizó exitosamente." + " Desea continuar" +
             " en el módulo de desmarcar predios?";

        this.banderaGestionDesbloqueoPredio = false;

        this.tiposBloqueoItemList = new ArrayList<SelectItem>();

        List<Dominio> dominio = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.PREDIO_TIPO_DESBLOQUEO);

        for (Dominio dom : dominio) {
            this.tiposBloqueoItemList.add(new SelectItem(dom.getCodigo(), dom.getValor()));
        }

        this.selectedTipoBloqueo = EPredioBloqueoTipoDesBloqueo.FALLADO.getCodigo();

        this.cargarDepartamentos();

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        this.cargarDepartamentosEntidad();

    }

    private void cargarDepartamentosEntidad() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = generalService.getDepartamentos(Constantes.COLOMBIA);

        this.cargarDeptosEntidadItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService().
                obteneMunicipiosDepartamentoSinCatastroDescentralizado(
                    dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getCodigo().substring(2, m.getCodigo().length()) + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    public void cargarDeptosEntidadItemList(List<Departamento> departamentosList) {

        this.deptosEntidadItemList = new ArrayList<SelectItem>();

        if (!this.deptosEntidadItemList.isEmpty()) {
            this.deptosEntidadItemList.clear();
        }
        this.deptosEntidadItemList.add(new SelectItem(null, DEFAULT_COMBOS));
        for (final Departamento dapto : departamentosList) {
            if (!dapto.getCodigo().equals(Constantes.BOGOTA_CODIGO)) {
                this.deptosEntidadItemList.add(new SelectItem(dapto.getCodigo(), dapto.getCodigo() +
                    "-" +
                     dapto.getNombre()));
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    public void poblar() {

        int count;
        count = this.getConservacionService().contarPrediosBloqueo(
            this.filtroCPredioB);

        if (count > 0) {
            this.lazyAssignedBloqueos.setRowCount(count);
        }

    }

    // --------------------------------------------------------------------------------------------------
    private void populatePredioBloqueoLazyly(List<PredioBloqueo> answer,
        int first, int pageSize) {
        Entidad entidad = null;

        rows = new ArrayList<PredioBloqueo>();

        int size;
        rows = this.getConservacionService().buscarPrediosBloqueo(
            filtroCPredioB, first, pageSize);

        if (rows != null) {
            size = rows.size();
            for (int i = 0; i < size; i++) {
                if (rows.get(i).getEntidadId() != null) {
                    entidad = this.getConservacionService().findEntidadById(rows.get(i).
                        getEntidadId());
                    rows.get(i).setNombreEntidad(entidad.getNombre());
                }
                answer.add(rows.get(i));
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton Buscar
     */
    @SuppressWarnings("serial")
    public void buscar() {

        if (this.filtroCPredioB != null &&
             (this.filtroCPredioB.getDepartamento() != null ||
             this.filtroCPredioB.getMunicipio() != null ||
             (this.filtroCPredioB.getDepartamentoCodigo() != null && !this.filtroCPredioB
            .getDepartamentoCodigo().isEmpty()) ||
             (this.filtroCPredioB.getMunicipioCodigo() != null && !this.filtroCPredioB
            .getMunicipioCodigo().isEmpty()) ||
             (this.filtroCPredioB.getTipoAvaluo() != null && !this.filtroCPredioB
            .getTipoAvaluo().isEmpty()) || (this.filtroCPredioB
                .getSectorCodigo()) != null &&
             !this.filtroCPredioB.getSectorCodigo().isEmpty()) ||
             (this.filtroCPredioB.getComunaCodigo() != null && !this.filtroCPredioB
            .getComunaCodigo().isEmpty()) ||
             (this.filtroCPredioB.getBarrioCodigo() != null && !this.filtroCPredioB
            .getBarrioCodigo().isEmpty()) ||
             (this.filtroCPredioB.getManzanaVeredaCodigo() != null && !this.filtroCPredioB
            .getManzanaVeredaCodigo().isEmpty()) ||
             (this.filtroCPredioB.getPredio() != null && !this.filtroCPredioB
            .getPredio().isEmpty()) ||
             (this.filtroCPredioB.getCondicionPropiedad() != null && !this.filtroCPredioB
            .getCondicionPropiedad().isEmpty()) ||
             (this.filtroCPredioB.getEdificio() != null && !this.filtroCPredioB
            .getEdificio().isEmpty()) ||
             (this.filtroCPredioB.getPiso() != null && !this.filtroCPredioB
            .getPiso().isEmpty()) ||
             (this.filtroCPredioB.getUnidad() != null && !this.filtroCPredioB
            .getUnidad().isEmpty())) {

            this.banderaBotonDesbloquear = true;
            this.banderaBuscar = true;

            if (this.departamento != null) {
                this.departamento.setCodigo(this.selectedDepartamentoCod);
            }

            if (this.municipio != null) {
                this.municipio.setCodigo(this.selectedMunicipioCod);
            }

            this.lazyAssignedBloqueos = new LazyDataModel<PredioBloqueo>() {

                @Override
                public List<PredioBloqueo> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<PredioBloqueo> answer = new ArrayList<PredioBloqueo>();
                    populatePredioBloqueoLazyly(answer, first, pageSize);

                    return answer;
                }
            };
            poblar();
            this.banderaBuscar = true;
        } else {
            String mensaje = "Para realizar la busqueda se necesita por lo menos un parametro." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para guardar los datos de predio bloqueo ejecutado sobre el boton guardar
     */
    public void desbloquearPB() {

        RequestContext context = RequestContext.getCurrentInstance();
        Boolean error = false;
        Date fechaSistema = new Date();
        Map filtros;
        List<Actividad> actividades;
        List<Tramite> tramites;
        Entidad entidad;

        if (this.prediosDesbloqueoSeleccionados.getFechaEnvioDesbloqueo() != null &&
             fechaSistema.before(this.prediosDesbloqueoSeleccionados.getFechaEnvioDesbloqueo())) {
            String mensaje = "Fecha envío no puede ser mayor a la fecha actual, verifique.";
            this.addMensajeError(mensaje);
            error = true;
        }

        if (this.prediosDesbloqueoSeleccionados.getFechaRecibidoDesbloqueo() != null &&
             fechaSistema.before(this.prediosDesbloqueoSeleccionados.getFechaRecibidoDesbloqueo())) {
            String mensaje = "La fecha de recibido no puede ser mayor a la fecha actual, verifique";
            this.addMensajeError(mensaje);
            error = true;
        }

        if (this.prediosDesbloqueoSeleccionados.getFechaRecibidoDesbloqueo() != null &&
            this.prediosDesbloqueoSeleccionados.getFechaEnvioDesbloqueo() != null &&
             this.prediosDesbloqueoSeleccionados.getFechaRecibidoDesbloqueo().before(
                this.prediosDesbloqueoSeleccionados.getFechaEnvioDesbloqueo())) {
            String mensaje =
                "La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique.";
            this.addMensajeError(mensaje);
            error = true;
        }

        if (this.prediosDesbloqueoSeleccionados.getFechaInicioBloqueo() != null &&
            this.prediosDesbloqueoSeleccionados.getFechaDesbloqueo() != null &&
            this.prediosDesbloqueoSeleccionados.getFechaDesbloqueo().before(
                this.prediosDesbloqueoSeleccionados.getFechaInicioBloqueo())) {
            String mensaje = "La fecha de desmarcación debe ser menor a la fecha de " +
                 "inicial de marcación (Fecha inicial de marcación: " +
                Constantes.FORMAT_FECHA_RESOLUCION.format(this.prediosDesbloqueoSeleccionados.
                    getFechaInicioBloqueo()) + ")";
            this.addMensajeError(mensaje);
            error = true;
        }

        if (!esNumeroRadicacionValido(this.prediosDesbloqueoSeleccionados.
            getNumeroRadicacionDesbloqueo(), usuario)) {
            String mensaje = "El número de radicado no se encontró en el sistema. Por favor revise.";
            this.addMensajeError(mensaje);
            error = true;
        }

        if (!error) {
            if (this.conteoArchivoDesbloqueo > 0) {

                if (this.prediosDesbloqueoSeleccionados != null) {
                    this.prediosDesbloqueoSeleccionados.setEntidadId(this.idEntidadBloqueo);
                }

                this.prediosDesbloqueoSeleccionados
                    .setDocumentoSoporteDesbloqueo(this.documentoSoporteDesbloqueo);

                try {

                    if (this.selectedRestitucionTierras == true) {
                        this.prediosDesbloqueoSeleccionados.setRestitucionTierrasDesbloqueo("SI");
                    } else {
                        this.prediosDesbloqueoSeleccionados.setRestitucionTierrasDesbloqueo("NO");
                    }

                    this.prediosDesbloqueoSeleccionados.setTipoDesbloqueo(this.selectedTipoBloqueo);

                    Municipio m = new Municipio();
                    m.setCodigo(this.selectedMunicipio);
                    Departamento d = new Departamento();
                    d.setCodigo(this.selectedDepartamento);
                    this.prediosDesbloqueoSeleccionados.setMunicipioDesbloqueo(m);
                    this.prediosDesbloqueoSeleccionados.setDepartamentoDesbloqueo(d);

                    if (this.idEntidadBloqueo != null) {
                        entidad = this.getConservacionService().findEntidadById(
                            this.idEntidadBloqueo);
                        this.prediosDesbloqueoSeleccionados.setEntidadDesbloqueo(entidad);
                    }

                    List<PredioBloqueo> predioParaDesbloquear = new ArrayList<PredioBloqueo>();
                    predioParaDesbloquear.add(this.prediosDesbloqueoSeleccionados);
                    this.getConservacionService().actualizarPrediosDesbloqueo(usuario,
                        predioParaDesbloquear);
                    this.rows.remove(this.prediosDesbloqueoSeleccionados);

                    filtros = new EnumMap<EParametrosConsultaActividades, String>(
                        EParametrosConsultaActividades.class);

                    tramites = this.getTramiteService().buscarTramitesTiposTramitesByNumeroPredial(
                        this.prediosDesbloqueoSeleccionados.getPredio().getNumeroPredial());

                    if (tramites != null && !tramites.isEmpty()) {

                        for (Tramite tram : tramites) {

                            filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION, tram.
                                getNumeroRadicacion());
                            filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, String.
                                valueOf(tram.getId()));
                            actividades = this.getProcesosService().consultarListaActividades(
                                filtros);

                            if (actividades != null && !actividades.isEmpty() && actividades.get(0).
                                getEstado().equals(EEstadoActividad.SUSPENDIDA.toString())) {
                                this.getProcesosService().reanudarActividad(actividades.get(0).
                                    getId());
                            }
                        }
                    }

                    this.entidades = new ArrayList<SelectItem>();
                    this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));
                    this.selectedDepartamento = this.DEFAULT_COMBOS_TODOS;
                    this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
                    this.selectedRestitucionTierras = false;
                    this.selectedTipoBloqueo = this.DEFAULT_COMBOS;
                    this.idEntidadBloqueo = null;
                    this.selectedMunicipio = "Seleccionar";

                    String mensaje = "El predio se desmarcó exitosamente!";
                    this.addMensajeInfo(mensaje);
                    this.inicializarDatosDesbloqueo();
                    this.banderaGestionDesbloqueoPredio = false;
                    this.banderaBuscar = true;
                    this.banderaEjecutaBusqueda = false;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    String mensaje = "El predio no pudo ser desmarcado";
                    this.addMensajeError(mensaje);
                }
            } else if (this.conteoArchivoDesbloqueo == 0) {

                String mensaje = "No se adjuntó un documento de soporte para desmarcar";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else {

            String mensaje = "El predio no se pudo desmarcar";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");

        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de soporte a cargar
     */
    private void nuevoDocumento() {

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento.setId(ETipoDocumentoId.DOC_SOPORTE_DESBLOQUEO_PREDIO
            .getId());

        this.documentoSoporteDesbloqueo
            .setPredioId(this.prediosDesbloqueoSeleccionados.getPredio()
                .getId());
        this.documentoSoporteDesbloqueo.setFechaDocumento(new Date());
        this.documentoSoporteDesbloqueo
            .setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
        this.documentoSoporteDesbloqueo.setBloqueado(ESiNo.NO.getCodigo());

        this.documentoSoporteDesbloqueo.setTipoDocumento(tipoDocumento);
        this.documentoSoporteDesbloqueo.setUsuarioCreador(this.usuario
            .getLogin());
        this.documentoSoporteDesbloqueo.setUsuarioLog(this.usuario.getLogin());
        this.documentoSoporteDesbloqueo.setFechaLog(new Date());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                eventoCarga.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            nuevoDocumento();
            this.documentoSoporteDesbloqueo.setArchivo(eventoCarga.getFile()
                .getFileName());

            this.banderaBotonGuardar = true;
            this.conteoArchivoDesbloqueo++;

            String mensaje = "El documento se cargó adecuadamente.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            e.printStackTrace();
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado al cerrar las ventanas modales
     */
    public void cerrarEsc(org.primefaces.event.CloseEvent event) {

        String mensaje = new String();
        this.addMensajeError(mensaje);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de departamentos
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.updateDepartamentosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
            .add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de municipios
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenMunicipios() {

        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.updateMunicipiosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * actualiza la lista de municipios según el departamento seleccionado
     *
     * @author pedro.garcia
     */
    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.selectedDepartamentoCod != null) {
            this.municipios = (ArrayList<Municipio>) this
                .getGeneralesService()
                .buscarMunicipiosConCatastroCentralizadoPorCodigoDepartamento(
                    this.selectedDepartamentoCod);

            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(this.municipios);
            } else {
                Collections.sort(this.municipios,
                    Municipio.getComparatorNombre());
            }

            for (Municipio municipio : this.municipios) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    this.municipiosItemList.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    this.municipiosItemList.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener del cambio en el combo de departamentos
     */
    public void onChangeDepartamentos() {
        this.municipio = null;
        this.updateMunicipiosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosDesbloqueo() {
        this.documentoSoporteDesbloqueo = new Documento();
        this.prediosDesbloqueoSeleccionados.setFechaDesbloqueo(null);
        this.prediosDesbloqueoSeleccionados.setMotivoDesbloqueo(null);
        this.conteoArchivoDesbloqueo = 0;
        this.banderaGestionDesbloqueoPredio = true;
        this.banderaBuscar = false;
        this.banderaEjecutaBusqueda = true;
        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));
        this.idEntidadBloqueo = null;
        this.selectedRestitucionTierras = false;
        this.selectedTipoBloqueo = this.DEFAULT_COMBOS;
        this.selectedTipoBloqueo = EPredioBloqueoTipoDesBloqueo.FALLADO.getCodigo();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void salirDesbloqueo(CloseEvent e) {
        inicializarDatosDesbloqueo();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar y bloquear otra persona
     */
    public void terminarYDesbloquearOtraP() {
        init();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar
     */
    public String terminar() {
        try {
            UtilidadesWeb.removerManagedBean("bloqueoPersona");
            return "index";
        } catch (Exception e) {
            this.addMensajeError("Error al cerrar la sesión");
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de la descarga de un archivo temporal de office
     */
    public void controladorDescargaArchivos() {

        if (this.documentoSoporteDesbloqueo != null &&
             this.documentoSoporteDesbloqueo.getArchivo() != null &&
             !documentoSoporteDesbloqueo.getArchivo().isEmpty()) {

            this.fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.documentoSoporteDesbloqueo.getArchivo());

        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoSoporteDesbloqueo.getArchivo());
    }

    /**
     * Método encargado de cerrar el panel de gestion de documentos de desbloqueo
     *
     * @author leidy.gonzalez
     */
    public void cerrarCargaDocumentosDesbloqueo() {
        this.banderaBuscar = true;
        this.banderaGestionDesbloqueoPredio = false;
        this.banderaEjecutaBusqueda = false;

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));
        this.selectedDepartamento = this.DEFAULT_COMBOS_TODOS;
        this.selectedMunicipio = "Seleccionar";
        this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
        this.selectedRestitucionTierras = false;
        this.selectedTipoBloqueo = this.DEFAULT_COMBOS;
        this.idEntidadBloqueo = null;
    }

    /**
     * Metodo para actualizar los municipios
     *
     * @author dumar.penuela
     */
    public void actualizarMunicipiosBuscardorListener() {

        if (this.municipiosDeptos != null) {

            this.municipiosBuscadorItemList = this.municipiosDeptos.get(this.selectedDepartamento);
        }

    }

    /**
     * Metodo para cargar los departamentos
     *
     * @author dumar.penuela
     */
    public void cargarDepartamentos() {
        List<Municipio> municipiosList;
        List<SelectItem> municipiosItemListTemp;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        /* List<Departamento> deptosList = (ArrayList<Departamento>) this .getGeneralesService()
         * .getDepartamentoByCodigoEstructuraOrganizacional( this.usuario
										.getCodigoEstructuraOrganizacional()); */
        List<Departamento> deptosList = generalService.getDepartamentos(Constantes.COLOMBIA);
        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService().
                obteneMunicipiosDepartamentoSinCatastroDescentralizado(
                    dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getCodigo() + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);
        }
    }

    /**
     * Metodo para constuir la lista de departamentos
     *
     * @author dumar.penuela
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        this.departamentosItemList.add(new SelectItem(null, DEFAULT_COMBOS));
        for (Departamento dapto : departamentosList) {
            if (!dapto.getCodigo().equals(Constantes.BOGOTA_CODIGO)) {
                this.departamentosItemList.add(new SelectItem(dapto.getCodigo(), dapto.getCodigo() +
                    "-" +
                     dapto.getNombre()));
            }
        }
    }

    public void cargarEntidades() {
        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        List<Entidad> entidadesActivas = this.getConservacionService().buscarEntidades(null,
            EEntidadEstado.ACTIVO.getEstado(), selectedDepartamento, selectedMunicipio);

        if (entidadesActivas != null && !entidadesActivas.isEmpty()) {
            for (Entidad e : entidadesActivas) {
                this.entidades.add(new SelectItem(e.getId(), e.getNombre()));
            }
        } else {
            Municipio d = this.getGeneralesService().getCacheMunicipioByCodigo(selectedMunicipio);
            this.addMensajeError("El municipio " + d.getNombre() +
                ", no tiene asociadas entidades que puedan realizar la marcación, verifique");
        }
    }

    private boolean esNumeroRadicacionValido(final String numeroRadicacion, final UsuarioDTO usuario) {
        boolean retval = true;
        if (usuario == null || numeroRadicacion == null || numeroRadicacion.trim().isEmpty()) {
            retval = false;
        } else {
            retval = this.getConservacionService().validarNumeroRadicacion(numeroRadicacion.trim(),
                usuario);
        }
        if (usuario != null && usuario.getDescripcionEstructuraOrganizacional() != null &&
            usuario.getDescripcionEstructuraOrganizacional().contains(Constantes.PREFIJO_DELEGADOS)) {
            retval = true;
        }
        return retval;
    }

}
