/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.web.mb.actualizacion;

import co.gov.igac.generales.dto.DocumentoTramiteDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.VCargueCica;
import co.gov.igac.snc.persistence.entity.actualizacion.ValoresActualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.*;
import co.gov.igac.snc.util.*;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.*;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.csvreader.CsvReader;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Clase para controlar la carga y validaciones de las tablas de avaluos para los procesos de
 * actualizacion express se usa principalmente el la vista registrarTablasActualizacion.xhtml
 *
 * @author felipe.cadena
 * @cu CU-NP-CO-257 Registrar Tablas de Actualización
 */
@Component("registroTablasActualizacion")
@Scope("session")
public class RegistroTablasActualizacionMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 7576908178244023347L;

    //Posibles errores de los archivos
    private static final String PUNTAJE_UNICIDAD = "El puntaje se encuentra duplicado. Fila: ";
    private static final String PUNTAJE_RANGO = "El puntaje debe ser un valor entre 0 y 100. Fila:";
    private static final String PUNTAJE_NO_CONVENCIONAL =
            "El puntaje asociado no es valido para la construcciòn segun el manual de reconocimiento, para el uso: ";
    private static final String VALOR = "El valor debe ser mayor que 0, Fila: ";
    private static final String ZONA =
            "Existen zonas geoeconómicas con un formato no válido, las zonas geoeconómicas deben ser de dos posiciones, la zona debe ser un valor numerico entre 01 y 99. Fila: ";
    private static final String ZONA_DUPLICADA =
            "Existen zonas repetidas en la tabla de terreno, verifique. Fila: ";
    private static final String VALOR_BLANCO = "Existe un valor no diligenciado en la fila: ";
    private static final String VALOR_NUMERICO_INCORRECTO =
            "Valor númerico mal diligenciado en la fila: ";
    private static final String NO_PUNTAJE =
            "No todos los puntos están relacionados en la tabla, verifique. ";
    private static final String MIXED_CONVENCIONALES =
            "La tabla a cargar tiene construcciones convencionales y no convencionales, verifique.";
    private static final String VALOR_PREVIO_EXISTE_TERRENO =
            "Ya se encuentra cargada una tabla de terreno para este destino verifique.";
    private static final String VALOR_PREVIO_EXISTE_CONSTRUCCION =
            "Ya se encuentra cargada una tabla de construcción  para esta zona, sector, zona homogénea física, destino verifique.";

    private static final Logger LOGGER = LoggerFactory
            .getLogger(RegistroTablasActualizacionMB.class);

    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Variable item list con los valores de los tipos de tablas
     */
    private List<SelectItem> tipoTablaItemList;

    /**
     * Variable con el el tipo de tabla seleccionado
     */
    private String tipoTablaSeleccionado;
    /**
     * Variable item list con los valores de los departamentos disponibles
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Lista de zonas opciones carga tabla
     */
    private List<SelectItem> zonasItemList;
    private List<SelectItem> sectoresItemList;
    private List<SelectItem> zhfItemList;
    private List<SelectItem> destinosItemList;
    private List<SelectItem> usosItemList;

    private String[] zonasSelected;
    private String[] sectoresSelected;
    private String[] zhfSelected;
    private String[] destinosSelected;
    private String[] usosSelected;

    /**
     * Listas datos previos
     */
    private List<String> zonasPrevList;
    private List<String> sectoresPrevList;
    private List<String> zhfPrevList;
    private List<String> destinosPrevList;
    private List<String> usosPrevList;

    private List<String> datosTerreno;
    private List<String> datosConstruccion;
    private List<SelectItem> detallesOpcion;

    private String opcionSeleccionadaDetalle;

    @Autowired
    private CombosDeptosMunisMB combos;

    /**
     * Variable con el codigo del departamento seleccionado
     */
    private String departamentoSeleccionado;
    /**
     * Variable item list con los valores de las vigencias
     */
    private List<SelectItem> vigenciasItemList;

    /**
     * Variable con el año de la vigencia
     */
    private String vigenciaSeleccionada;
    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosItemList;
    /**
     * Variable con el codigo del departamento seleccionado
     */
    private String municipioSeleccionado;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     * Archivos que se han cargado asociados al municipio y departamento seleccionados
     */
    private List<ValoresActualizacion> valoresActualizacion;

    /**
     * Poceso actual asociado al municipio y a la vigencias seleccionadas
     */
    private AxMunicipio axMunicipioActual;

    /**
     * Valor seleccionao para la realizar acciones de la tabla de registro de tablas.
     */
    private ValoresActualizacion valorActualizacionSeleccionado;

    /**
     * Determina si ya se finalizo el proceso de carga de tablas
     */
    private boolean banderaProcesoFinalizado;

    private List<String[]> archivoTabla;

    /**
     * Determina si se esta carganado una regla o una tabla
     */
    private boolean banderaCargarRegla;
    private boolean banderaTablaCargada;

    private boolean banderaValidacionFallida;

    /**
     * Determina si existe un proceso activo para la vigencia y el municipio seleccionado
     */
    private boolean banderaProcesoActivo;

    private List<String[]> archivosCarga;

    private String nombreArchivoTabla;

    private List<String[]> erroresValidacion;

    private List<UsoConstruccion> usosConstruccionSistema;

    private Map<String, List<CalificacionAnexo>> puntajesAnexos;

    private Documento documentoTabla;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Objeto que contiene los datos del reporte de terreno o construcción
     */
    private ReporteDTO reporteDeTerrenoOConstruccion;

    /**
     * archivo con la informacion de cargue del proceso de actualizacion
     */
    private File archivoDatosActualizacion;
    
    /**
     * archivo de soporte PDF
     */
    private File archivoPdf;

    /**
     * archivo con los predios devueltos a CICA
     */
    private StreamedContent archivoDevueltoCica;

    /**
     * Ruta local archivo de actualizacion
     */
    private String rutaArchivoDatosActualizacion;
    
    /**
     * Ruta local archivo pdf
     */
    private String rutaArchivoPdf;

    private boolean banderaArchivoCargado;

    private String PREDIOS_END_FLAG = "EONP";

    private List<String> prediosProyectados;

    private boolean archivoDevueltoDescargado;

    private boolean prediosProyectadosSnc;

    /**
     * Lista de procesos de cargue.
     */
    private List<MunicipioActualizacion> procesosCargue;

    private MunicipioActualizacion cargueSeleccionado;

    private boolean permitirCargue;

    private int cantidadPrediosCica;
    private int cantidadPrediosSnc;
    private int numPrediosCargue;
    private Date fechaCargueExitoso;


    /**
     * Variable de tipo streamed content para la descarga de reporte de inconsistencias
     */
    private StreamedContent reporteInconsistencias;
    
    private StreamedContent documentoSoporte;

    /**
     * Listados de predios proyectdos de la jurisdiccion
     */
    private VCargueCica[] prediosProyectadosSeleccionados;
    
    /**
     * Listados de archivos de tablas
     */
    private ValoresActualizacion[] archivosTablasSeleccionados;

    private FiltroCargueDTO filtroCargueDTO;

    /**
     * variable donde se cargan los resultados paginados de la consulta de los predios proyectados
     */
    private LazyDataModel<VCargueCica> prediosProyectadosCargue;

    private List<Object[]> listaResumenPredios;

    private List<Long> prediosParaEliminarProyeccion;
    
    /**
     * Managed bean del reportes de tablas de actualización
     */
    private ReportesTablasActualizacionMB reportesTablasActualizacionMB;

    //omisiones comisiones
    private String estadoOmisiones;
    private Integer prediosComision;
    private Integer prediosOmision;
    private Date fechaInformeOmisiones;


    //preliquidar
    private String estadoPreliquidar;
    private Date fechaInformePreliquidar;

    //preliquidar
    private String estadoRenumerar;
    private Date fechaInformeRenumerar;

    private boolean cargueFinalizado = false;
    private boolean carguePreliquidado = false;
    private boolean tablasCargadas = false;
    
    private boolean cargueSoporteDocumento = true;
    private boolean estadoSoporteDocumento = true;
    private String nombreSoporteDocumento = "";


    // --------------------GETTERS AND SETTERS--------------------//


    public Date getFechaCargueExitoso() {
        return fechaCargueExitoso;
    }

    public void setFechaCargueExitoso(Date fechaCargueExitoso) {
        this.fechaCargueExitoso = fechaCargueExitoso;
    }

    public boolean isTablasCargadas() {
        return tablasCargadas;
    }

    public void setTablasCargadas(boolean tablasCargadas) {
        this.tablasCargadas = tablasCargadas;
    }

    public boolean isCarguePreliquidado() {
        return carguePreliquidado;
    }

    public void setCarguePreliquidado(boolean carguePreliquidado) {
        this.carguePreliquidado = carguePreliquidado;
    }

    public boolean isCargueFinalizado() {
        return cargueFinalizado;
    }

    public void setCargueFinalizado(boolean cargueFinalizado) {
        this.cargueFinalizado = cargueFinalizado;
    }

    public String getEstadoOmisiones() {
        return estadoOmisiones;
    }

    public void setEstadoOmisiones(String estadoOmisiones) {
        this.estadoOmisiones = estadoOmisiones;
    }

    public Integer getPrediosComision() {
        return prediosComision;
    }

    public void setPrediosComision(Integer prediosComision) {
        this.prediosComision = prediosComision;
    }

    public Integer getPrediosOmision() {
        return prediosOmision;
    }

    public void setPrediosOmision(Integer prediosOmision) {
        this.prediosOmision = prediosOmision;
    }

    public Date getFechaInformeOmisiones() {
        return fechaInformeOmisiones;
    }

    public void setFechaInformeOmisiones(Date fechaInformeOmisiones) {
        this.fechaInformeOmisiones = fechaInformeOmisiones;
    }

    public String getEstadoPreliquidar() {
        return estadoPreliquidar;
    }

    public void setEstadoPreliquidar(String estadoPreliquidar) {
        this.estadoPreliquidar = estadoPreliquidar;
    }

    public Date getFechaInformePreliquidar() {
        return fechaInformePreliquidar;
    }

    public void setFechaInformePreliquidar(Date fechaInformePreliquidar) {
        this.fechaInformePreliquidar = fechaInformePreliquidar;
    }

    public String getEstadoRenumerar() {
        return estadoRenumerar;
    }

    public void setEstadoRenumerar(String estadoRenumerar) {
        this.estadoRenumerar = estadoRenumerar;
    }

    public Date getFechaInformeRenumerar() {
        return fechaInformeRenumerar;
    }

    public void setFechaInformeRenumerar(Date fechaInformeRenumerar) {
        this.fechaInformeRenumerar = fechaInformeRenumerar;
    }

    public boolean isPrediosProyectadosSnc() {
        return prediosProyectadosSnc;
    }

    public void setPrediosProyectadosSnc(boolean prediosProyectadosSnc) {
        this.prediosProyectadosSnc = prediosProyectadosSnc;
    }

    public List<Object[]> getListaResumenPredios() {
        return listaResumenPredios;
    }

    public void setListaResumenPredios(List<Object[]> listaResumenPredios) {
        this.listaResumenPredios = listaResumenPredios;
    }

    public FiltroCargueDTO getFiltroCargueDTO() {
        return filtroCargueDTO;
    }

    public void setFiltroCargueDTO(FiltroCargueDTO filtroCargueDTO) {
        this.filtroCargueDTO = filtroCargueDTO;
    }

    public VCargueCica[] getPrediosProyectadosSeleccionados() {
        return prediosProyectadosSeleccionados;
    }

    public void setPrediosProyectadosSeleccionados(VCargueCica[] prediosProyectadosSeleccionados) {
        this.prediosProyectadosSeleccionados = prediosProyectadosSeleccionados;
    }
    
    public ValoresActualizacion[] getArchivosTablasSeleccionados() {
        return archivosTablasSeleccionados;
    }

    public void setArchivosTablasSeleccionados(ValoresActualizacion[] archivosTablasSeleccionados) {
        this.archivosTablasSeleccionados = archivosTablasSeleccionados;
    }

    public MunicipioActualizacion getCargueSeleccionado() {
        return cargueSeleccionado;
    }

    public void setCargueSeleccionado(MunicipioActualizacion cargueSeleccionado) {
        this.cargueSeleccionado = cargueSeleccionado;
    }

    public LazyDataModel<VCargueCica> getPrediosProyectadosCargue() {
        return prediosProyectadosCargue;
    }

    public void setPrediosProyectadosCargue(LazyDataModel<VCargueCica> prediosProyectadosCargue) {
        this.prediosProyectadosCargue = prediosProyectadosCargue;
    }

    public StreamedContent getReporteInconsistencias() {
        return reporteInconsistencias;
    }

    public void setReporteInconsistencias(StreamedContent reporteInconsistencias) {
        this.reporteInconsistencias = reporteInconsistencias;
    }
    
     public StreamedContent getDocumentoSoporte() {
        return documentoSoporte;
    }

    public void setDocumentoSoporte(StreamedContent documentoSoporte) {
        this.documentoSoporte = documentoSoporte;
    }

    public int getCantidadPrediosCica() {
        return cantidadPrediosCica;
    }

    public void setCantidadPrediosCica(int cantidadPrediosCica) {
        this.cantidadPrediosCica = cantidadPrediosCica;
    }

    public int getCantidadPrediosSnc() {
        return cantidadPrediosSnc;
    }

    public void setCantidadPrediosSnc(int cantidadPrediosSnc) {
        this.cantidadPrediosSnc = cantidadPrediosSnc;
    }

    public boolean isPermitirCargue() {
        return permitirCargue;
    }

    public void setPermitirCargue(boolean permitirCargue) {
        this.permitirCargue = permitirCargue;
    }

    public List<MunicipioActualizacion> getProcesosCargue() {
        return procesosCargue;
    }

    public void setProcesosCargue(List<MunicipioActualizacion> procesosCargue) {
        this.procesosCargue = procesosCargue;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<SelectItem> getDetallesOpcion() {
        return detallesOpcion;
    }

    public void setDetallesOpcion(List<SelectItem> detallesOpcion) {
        this.detallesOpcion = detallesOpcion;
    }

    public boolean isBanderaProcesoActivo() {
        return banderaProcesoActivo;
    }

    public void setBanderaProcesoActivo(boolean banderaProcesoActivo) {
        this.banderaProcesoActivo = banderaProcesoActivo;
    }

    public CombosDeptosMunisMB getCombos() {
        return combos;
    }

    public void setCombos(CombosDeptosMunisMB combos) {
        this.combos = combos;
    }

    public List<SelectItem> getTipoTablaItemList() {
        return tipoTablaItemList;
    }

    public void setTipoTablaItemList(List<SelectItem> tipoTablaItemList) {
        this.tipoTablaItemList = tipoTablaItemList;
    }

    public String getTipoTablaSeleccionado() {
        return tipoTablaSeleccionado;
    }

    public void setTipoTablaSeleccionado(String tipoTablaSeleccionado) {
        this.tipoTablaSeleccionado = tipoTablaSeleccionado;
    }

    public String getVigenciaSeleccionada() {
        return vigenciaSeleccionada;
    }

    public void setVigenciaSeleccionada(String vigenciaSeleccionada) {
        this.vigenciaSeleccionada = vigenciaSeleccionada;
    }

    public List<SelectItem> getVigenciasItemList() {
        return vigenciasItemList;
    }

    public void setVigenciasItemList(List<SelectItem> vigenciasItemList) {
        this.vigenciasItemList = vigenciasItemList;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public String getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    public void setDepartamentoSeleccionado(String departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public String getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(String municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public Map<String, List<SelectItem>> getMunicipiosDeptos() {
        return municipiosDeptos;
    }

    public void setMunicipiosDeptos(Map<String, List<SelectItem>> municipiosDeptos) {
        this.municipiosDeptos = municipiosDeptos;
    }

    public List<ValoresActualizacion> getValoresActualizacion() {
        return valoresActualizacion;
    }

    public void setValoresActualizacion(List<ValoresActualizacion> valoresActualizacion) {
        this.valoresActualizacion = valoresActualizacion;
    }

    public ValoresActualizacion getValorActualizacionSeleccionado() {
        return valorActualizacionSeleccionado;
    }

    public void setValorActualizacionSeleccionado(
            ValoresActualizacion valorActualizacionSeleccionado) {
        this.valorActualizacionSeleccionado = valorActualizacionSeleccionado;
    }

    public boolean isBanderaProcesoFinalizado() {
        return banderaProcesoFinalizado;
    }

    public void setBanderaProcesoFinalizado(boolean banderaProcesoFinalizado) {
        this.banderaProcesoFinalizado = banderaProcesoFinalizado;
    }

    public boolean isBanderaCargarRegla() {
        return banderaCargarRegla;
    }

    public void setBanderaCargarRegla(boolean banderaCargarRegla) {
        this.banderaCargarRegla = banderaCargarRegla;
    }

    public boolean isBanderaTablaCargada() {
        return banderaTablaCargada;
    }

    public void setBanderaTablaCargada(boolean banderaTablaCargada) {
        this.banderaTablaCargada = banderaTablaCargada;
    }

    public List<String[]> getArchivosCarga() {
        return archivosCarga;
    }

    public void setArchivosCarga(List<String[]> archivosCarga) {
        this.archivosCarga = archivosCarga;
    }

    public List<String[]> getErroresValidacion() {
        return erroresValidacion;
    }

    public void setErroresValidacion(List<String[]> erroresValidacion) {
        this.erroresValidacion = erroresValidacion;
    }

    public boolean isBanderaValidacionFallida() {
        return banderaValidacionFallida;
    }

    public void setBanderaValidacionFallida(boolean banderaValidacionFallida) {
        this.banderaValidacionFallida = banderaValidacionFallida;
    }

    public List<SelectItem> getZonasItemList() {
        return zonasItemList;
    }

    public void setZonasItemList(List<SelectItem> zonasItemList) {
        this.zonasItemList = zonasItemList;
    }

    public String[] getZonasSelected() {
        return zonasSelected;
    }

    public void setZonasSelected(String[] zonasSelected) {
        this.zonasSelected = zonasSelected;
    }

    public List<SelectItem> getSectoresItemList() {
        return sectoresItemList;
    }

    public void setSectoresItemList(List<SelectItem> sectoresItemList) {
        this.sectoresItemList = sectoresItemList;
    }

    public List<SelectItem> getZhfItemList() {
        return zhfItemList;
    }

    public void setZhfItemList(List<SelectItem> zhfItemList) {
        this.zhfItemList = zhfItemList;
    }

    public List<SelectItem> getDestinosItemList() {
        return destinosItemList;
    }

    public void setDestinosItemList(List<SelectItem> destinosItemList) {
        this.destinosItemList = destinosItemList;
    }

    public List<SelectItem> getUsosItemList() {
        return usosItemList;
    }

    public void setUsosItemList(List<SelectItem> usosItemList) {
        this.usosItemList = usosItemList;
    }

    public String[] getSectoresSelected() {
        return sectoresSelected;
    }

    public void setSectoresSelected(String[] sectoresSelected) {
        this.sectoresSelected = sectoresSelected;
    }

    public String[] getZhfSelected() {
        return zhfSelected;
    }

    public void setZhfSelected(String[] zhfSelected) {
        this.zhfSelected = zhfSelected;
    }

    public String[] getDestinosSelected() {
        return destinosSelected;
    }

    public void setDestinosSelected(String[] destinosSelected) {
        this.destinosSelected = destinosSelected;
    }

    public String[] getUsosSelected() {
        return usosSelected;
    }

    public void setUsosSelected(String[] usosSelected) {
        this.usosSelected = usosSelected;
    }

    public String getOpcionSeleccionadaDetalle() {
        return opcionSeleccionadaDetalle;
    }

    public void setOpcionSeleccionadaDetalle(String opcionSeleccionadaDetalle) {
        this.opcionSeleccionadaDetalle = opcionSeleccionadaDetalle;
    }

    public ReporteDTO getReporteDeTerrenoOConstruccion() {
        return reporteDeTerrenoOConstruccion;
    }

    public void setReporteDeTerrenoOConstruccion(
            ReporteDTO reporteDeTerrenoOConstruccion) {
        this.reporteDeTerrenoOConstruccion = reporteDeTerrenoOConstruccion;
    }

    public boolean isBanderaArchivoCargado() {
        return banderaArchivoCargado;
    }

    public void setBanderaArchivoCargado(boolean banderaArchivoCargado) {
        this.banderaArchivoCargado = banderaArchivoCargado;
    }

    public List<String> getPrediosProyectados() {
        return prediosProyectados;
    }

    public void setPrediosProyectados(List<String> prediosProyectados) {
        this.prediosProyectados = prediosProyectados;
    }

    public StreamedContent getArchivoDevueltoCica() {
        return archivoDevueltoCica;
    }

    public void setArchivoDevueltoCica(StreamedContent archivoDevueltoCica) {
        this.archivoDevueltoCica = archivoDevueltoCica;
    }

    public boolean isArchivoDevueltoDescargado() {
        return archivoDevueltoDescargado;
    }

    public void setArchivoDevueltoDescargado(boolean archivoDevueltoDescargado) {
        this.archivoDevueltoDescargado = archivoDevueltoDescargado;
    }
    
    public boolean isCargueSoporteDocumento() {
        return cargueSoporteDocumento;
    }

    public void setCargueSoporteDocumento(boolean cargueSoporteDocumento) {
        this.cargueSoporteDocumento = cargueSoporteDocumento;
    }
    
    public boolean isEstadoSoporteDocumento() {
        return estadoSoporteDocumento;
    }

    public void setEstadoSoporteDocumento(boolean estadoSoporteDocumento) {
        this.estadoSoporteDocumento = estadoSoporteDocumento;
    }
    
    public String getNombreSoporteDocumento() {
        return nombreSoporteDocumento;
    }

    public void setNombreSoporteDocumento(String nombreSoporteDocumento) {
        this.nombreSoporteDocumento = nombreSoporteDocumento;
    }

    // --------------------------METODOS--------------------------//
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.cargarDepartamentos();

        //cargar vigencias
        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
        this.vigenciasItemList = generalMB.getRangoAniosAscDesdeActual();

        this.banderaProcesoFinalizado = false;
	    this.banderaProcesoActivo = true;
        this.banderaTablaCargada = false;

        this.archivosCarga = new ArrayList<String[]>();
        String[] val = {"Archivo terreno", "false", "false"};
        this.archivosCarga.add(val);
        String[] val2 = {"Archivo construcciones", "true", "false"};
        this.archivosCarga.add(val2);

        this.banderaValidacionFallida = false;

        this.usosConstruccionSistema = this.getConservacionService().obtenerUsosConstruccion();

        //Inicializar puntajes anexos
        this.puntajesAnexos = new HashMap<String, List<CalificacionAnexo>>();

        for (UsoConstruccion uso : this.usosConstruccionSistema) {

            List<CalificacionAnexo> puntajesAnexo = this.getConservacionService().
                    obtenerCalificacionesAnexoPorIdUsoConstruccion(uso.getId());

            if (puntajesAnexo != null && !puntajesAnexo.isEmpty()) {
                this.puntajesAnexos.put(String.valueOf(uso.getId()), puntajesAnexo);
            }

        }

    }

    /**
     * Metodo para cargar las opciones disponibles en la vista de cargar tablas
     *
     * @author felipe.cadena
     */
    public void cargarOpcionesCargaTablas() {
        this.banderaValidacionFallida = false;

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

        if (!validarDatosBusqueda()) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
            return;
        }

        this.tipoTablaItemList = new ArrayList<SelectItem>();

        this.tipoTablaItemList.add(new SelectItem(null, "Seleccionar..."));
        for (EValoresActualizacionTipo tipo : EValoresActualizacionTipo.values()) {
            SelectItem si = new SelectItem(tipo.getCodigo(), tipo.getCodigo());
            this.tipoTablaItemList.add(si);
        }

        //lista de zona
        this.combos.setSelectedDepartamentoCod(this.departamentoSeleccionado);
        this.combos.updateMunicipiosItemList();
        this.combos.setSelectedMunicipioCod(this.municipioSeleccionado);
        this.combos.updateZonasItemList();

        for (int i = 0; i < this.combos.getZonasItemList().size(); i++) {
            SelectItem si = this.combos.getZonasItemList().get(i);
            if (si.getValue() == null) {
                this.combos.getZonasItemList().remove(i);
            }
        }

        //lista de destinos
        this.destinosItemList = generalMB.getPredioDestinoV();

        //lista de zhf
        this.zhfItemList = generalMB.getZonaFisicasV();
        for (int i = 0; i < this.zhfItemList.size(); i++) {
            SelectItem si = this.zhfItemList.get(i);
            if (si.getValue() == null || si.getValue().toString().isEmpty()) {
                this.zhfItemList.remove(i);
            }
        }

        //lista de usos
        this.usosItemList = generalMB.getUnidadConstruccionCV();

        this.banderaTablaCargada = false;

        this.tipoTablaSeleccionado = null;
        this.zonasSelected = null;
        this.sectoresSelected = null;
        this.usosSelected = null;
        this.zhfSelected = null;
        this.destinosSelected = null;

        if (this.erroresValidacion != null) {
            this.erroresValidacion.clear();
        }
    }

    /**
     * Actualiza los sectores segun la zona seleccionada
     *
     * @author felipe.cadena
     */
    public void updateSectores() {

        ArrayList<SelectItem> sectoresAux = new ArrayList<SelectItem>();

        for (String zona : this.zonasSelected) {
            this.combos.setSelectedZonaCod(zona);
            this.combos.onChangeZonaListener();

            for (SelectItem siNew : this.combos.getSectoresItemList()) {
                boolean exists = false;

                if (siNew.getValue() == null) {
                    continue;
                }
                for (SelectItem siOld : sectoresAux) {
                    if (siOld.getLabel().equals(siNew.getLabel())) {
                        exists = true;
                    }
                }
                if (!exists) {
                    sectoresAux.add(siNew);
                }
            }
        }

        this.combos.setSectoresItemList(sectoresAux);
        this.sectoresItemList = this.combos.getSectoresItemList();
    }

    /**
     * Metodo para cargar los departamentos asociados a la territorial.
     *
     * @author felipe.cadena
     */
    public void cargarDepartamentos() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList = new LinkedList<Municipio>();
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.usuario.getCodigoTerritorial());

        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getActualizacionService().
                    obtenerMunicipiosEnActualizacionPorDepto(dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                        m.getCodigo().substring(2, 5) + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    /**
     * Metodo para constuir la lista de departamentos asociados a la territorial
     *
     * @param departamentosList
     * @author felipe.cadena
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                    departamento.getCodigo() + "-" + departamento.getNombre()));
        }
    }

    /**
     * Metodo para actualizar los usuarios asociados al rol seleccionado
     *
     * @author felipe.cadena
     */
    public void actualizarMunicipiosListener() {
        if (this.municipiosDeptos != null) {

            this.municipiosItemList = this.municipiosDeptos.get(this.departamentoSeleccionado);
        }
    }

    /**
     * Metodo para establecer los cargues
     *
     *
     * @author felipe.cadena
     */
    public void establecerCargues() {

        this.procesosCargue = new ArrayList<MunicipioActualizacion>();

        if(this.municipioSeleccionado==null || this.municipioSeleccionado.isEmpty() ||
                this.departamentoSeleccionado.isEmpty() || this.departamentoSeleccionado == null ||
                this.vigenciaSeleccionada.isEmpty() || this.vigenciaSeleccionada == null ){
            this.addMensajeError("Se debe establecer la jurisdicción y vigencia de la actualización, por favor verifique");
            return;
        }
        this.cargueSoporteDocumento = false;

        this.procesosCargue = this.getActualizacionService().
                obtenerMunicipioActualizacionPorMunicipioYEstado(this.municipioSeleccionado,
                        null, true, Integer.valueOf(this.vigenciaSeleccionada));
        this.permitirCargue = true;
        for (MunicipioActualizacion ma: this.procesosCargue) {
            if(ECargueCicaEstado.PROCESANDO.isCodigoIgual(ma.getEstado())){
                this.permitirCargue = false;
            }
            if(ECargueCicaEstado.CARGUE_EXITOSO.isCodigoIgual(ma.getEstado())){
                this.fechaCargueExitoso = ma.getFechaInicio();
            }
        }

        this.valoresActualizacion = this.getActualizacionService().
                obtenerValoresActualizacionPorMunicipioDepartamento(this.municipioSeleccionado,
                        this.departamentoSeleccionado,
                        this.vigenciaSeleccionada);

        FiltroCargueDTO filtro = new FiltroCargueDTO();
        filtro.setMunicipioCodigo(this.municipioSeleccionado);
        filtro.setVigencia(Integer.valueOf(this.vigenciaSeleccionada));

        //TODO::determinar si el cargue esta finalizado
        this.cargueFinalizado = false;
        this.carguePreliquidado = false;

        if(this.valoresActualizacion != null && !this.valoresActualizacion.isEmpty()){
            this.tablasCargadas = true;
        } else{
            tablasCargadas = false;
        }

        if(filtro.getFilters()==null){
            filtro.setFilters(new HashMap<String, String>());
        }

        this.cantidadPrediosSnc = this.getActualizacionService().
                countPPrediosCargueCica(filtro);
        
         List<Documento> docsReporte = this.getGeneralesService().
                buscarDocumentoPorNumeroDocumentoYTipo(this.vigenciaSeleccionada + "-" + this.municipioSeleccionado,
                        new Long(2011)
                );
        if (docsReporte != null && !docsReporte.isEmpty()) {
            this.estadoSoporteDocumento = false;
            Documento docReporte = docsReporte.get(0);
            this.nombreSoporteDocumento = docReporte.getArchivo();
        }else{
            this.estadoSoporteDocumento = true;
            this.nombreSoporteDocumento = "";
        }
        
        this.reportesTablasActualizacionMB = (ReportesTablasActualizacionMB) UtilidadesWeb.
            getManagedBean("reportesTablasActualizacion");
        
        this.reportesTablasActualizacionMB.cargarReportesActualizacionPorMunicipioActualizacionId();

        this.cargarRangos(filtro);
        this.cargarOpcionesCargaTablas();
    }

    /**
     * Prepara los rangos para los filtros avanzados
     */
    public void cargarRangos(FiltroCargueDTO filtro){

        if(this.filtroCargueDTO==null){
            this.filtroCargueDTO = new FiltroCargueDTO();
        }

        Object[] rangosObj = this.getActualizacionService().
                buscarRangosFiltros(filtro);

        if(rangosObj==null){
            return;
        }

        this.filtroCargueDTO.getAreaTerrenoInicial().setValorMinimo(((BigDecimal) rangosObj[2]).doubleValue());
        this.filtroCargueDTO.getAreaTerrenoInicial().setValorMaximo(((BigDecimal) rangosObj[3]).doubleValue());

        this.filtroCargueDTO.getAreaTerrenoFinal().setValorMinimo(((BigDecimal) rangosObj[4]).doubleValue());
        this.filtroCargueDTO.getAreaTerrenoFinal().setValorMaximo(((BigDecimal) rangosObj[5]).doubleValue());

        this.filtroCargueDTO.getAreaTerrenoVariacion().setValorMinimo(((BigDecimal) rangosObj[6]).doubleValue());
        this.filtroCargueDTO.getAreaTerrenoVariacion().setValorMaximo(((BigDecimal) rangosObj[7]).doubleValue());

        this.filtroCargueDTO.getAreaConstruccionInicial().setValorMinimo(((BigDecimal) rangosObj[8]).doubleValue());
        this.filtroCargueDTO.getAreaConstruccionInicial().setValorMaximo(((BigDecimal) rangosObj[9]).doubleValue());

        this.filtroCargueDTO.getAreaConstruccionFinal().setValorMinimo(((BigDecimal) rangosObj[10]).doubleValue());
        this.filtroCargueDTO.getAreaConstruccionFinal().setValorMaximo(((BigDecimal) rangosObj[11]).doubleValue());

        this.filtroCargueDTO.getAreaConstruccionVariacion().setValorMinimo(((BigDecimal) rangosObj[12]).doubleValue());
        this.filtroCargueDTO.getAreaConstruccionVariacion().setValorMaximo(((BigDecimal) rangosObj[13]).doubleValue());

        this.filtroCargueDTO.getAvaluoInicial().setValorMinimo(((BigDecimal) rangosObj[14]).doubleValue());
        this.filtroCargueDTO.getAvaluoInicial().setValorMaximo(((BigDecimal) rangosObj[15]).doubleValue());

        this.filtroCargueDTO.getAvaluoFinal().setValorMinimo(((BigDecimal) rangosObj[16]).doubleValue());
        this.filtroCargueDTO.getAvaluoFinal().setValorMaximo(((BigDecimal) rangosObj[17]).doubleValue());

        this.filtroCargueDTO.getAvaluoVariacion().setValorMinimo(((BigDecimal) rangosObj[18]).doubleValue());
        this.filtroCargueDTO.getAvaluoVariacion().setValorMaximo(((BigDecimal) rangosObj[19]).doubleValue());

    }

    /**
     * Metodo para buscar los documentos de tablas registradas previamente
     *
     * @author felipe.cadena
     */
    public void buscar() {

        if (!validarDatosBusqueda()) {
            return;
        }

        this.valoresActualizacion = this.getActualizacionService().
                obtenerValoresActualizacionPorMunicipioDepartamento(this.municipioSeleccionado,
                        this.departamentoSeleccionado,
                        this.vigenciaSeleccionada);

        List<AxMunicipio> axMuns = this.getConservacionService().
                obtenerAxMunicipioPorMunicipioVigencia(this.municipioSeleccionado,
                        this.vigenciaSeleccionada);

        if (axMuns != null && !axMuns.isEmpty()) {
            this.axMunicipioActual = axMuns.get(0);
            if (this.axMunicipioActual.getEstadoTablas().equals(
                    EValoresActualizacionEstado.FINALIZADO.getCodigo())) {
                this.banderaProcesoFinalizado = true;
            }
            this.banderaProcesoActivo = true;
        } else {
            //this.banderaProcesoActivo = false;
            this.addMensajeWarn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_015.toString());
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_015.getMensajeTecnico());
            return;

        }

        this.procesarValoresExistentes();
        this.cargarOpcionesCargaTablas();

    }

    /**
     * Procesa los datos de las tablas cargados previamente
     *
     * @author felipe.cadena
     */
    private void procesarValoresExistentes() {

        this.zonasPrevList = new ArrayList<String>();
        this.sectoresPrevList = new ArrayList<String>();
        this.destinosPrevList = new ArrayList<String>();
        this.zhfPrevList = new ArrayList<String>();
        this.usosPrevList = new ArrayList<String>();
        this.datosTerreno = new ArrayList<String>();
        this.datosConstruccion = new ArrayList<String>();

        for (ValoresActualizacion va : this.valoresActualizacion) {

            if (va.getZona() != null) {
                String[] zonas = va.getZona().split(",");
                this.zonasPrevList.addAll(Arrays.asList(zonas));
            }

            if (va.getSector() != null) {
                String[] sectores = va.getSector().split(",");
                this.sectoresPrevList.addAll(Arrays.asList(sectores));
            }

            if (va.getDestino() != null) {
                String[] destinos = va.getDestino().split(",");
                this.destinosPrevList.addAll(Arrays.asList(destinos));
            }

            if (va.getZhf() != null) {
                String[] zhf = va.getZhf().split(",");
                this.zhfPrevList.addAll(Arrays.asList(zhf));
            }

            if (va.getUso() != null) {
                String[] usos = va.getUso().split(",");
                this.usosPrevList.addAll(Arrays.asList(usos));
            }

            if (va.getTipo().equals(EValoresActualizacionTipo.TERRENO.getCodigo())) {
                for (String zona : this.zonasPrevList) {

                    for (String destino : this.destinosPrevList) {
                        this.datosTerreno.add(zona + "-" + destino);
                    }

                }
            } else {
                for (String zona : this.zonasPrevList) {
                    for (String destino : this.destinosPrevList) {
                        for (String uso : this.usosPrevList) {
                            if (this.sectoresPrevList.isEmpty()) {
                                for (String zhf : this.zhfPrevList) {
                                    this.datosConstruccion.add(zona + "-" + destino + "-" + uso +
                                            "-" + "-" + zhf);
                                }
                            } else {
                                for (String sector : this.sectoresPrevList) {
                                    if (this.zhfPrevList.isEmpty()) {
                                        this.datosConstruccion.add(
                                                zona + "-" + destino + "-" + uso + "-" + sector + "-");
                                    }
                                    for (String zhf : this.zhfPrevList) {
                                        this.datosConstruccion.add(
                                                zona + "-" + destino + "-" + uso + "-" + sector + "-" +
                                                        zhf);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Se reinician las listas para preparar el siguiente registro
            this.zonasPrevList.clear();
            this.sectoresPrevList.clear();
            this.destinosPrevList.clear();
            this.zhfPrevList.clear();
            this.usosPrevList.clear();
        }
    }

    /**
     * valida que existan los datos necesarios para realizar la busqueda
     *
     * @return
     * @author felipe.cadena
     */
    private boolean validarDatosBusqueda() {
        if (this.municipioSeleccionado == null) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_001.toString());
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_001.getMensajeTecnico());
            return false;
        }

        if (this.departamentoSeleccionado == null) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_002.toString());
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_002.getMensajeTecnico());
            return false;
        }

        if (this.vigenciaSeleccionada == null) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_014.toString());
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_014.getMensajeTecnico());
            return false;
        }

        return true;
    }

    /**
     * Elimina la tabla seleccionada y ademas la regla asociada a esta tabla
     */
    public void eliminarTabla() {

        Object[] resultado = this.getActualizacionService().
                eliminarValoresActualizacion(this.valorActualizacionSeleccionado.getId(),
                        this.municipioSeleccionado);

        String[] mensajes = UtilidadesWeb.extraerMensajeSP(resultado);
        if (!mensajes[0].isEmpty()) {
            this.imprimirErrorProcedimientoDB(mensajes);
            return;
        }

        this.buscar();

        /*
         * En este punto se borran los reportes de actualización que existan para el municipio
         * seleccionado @javier.aponte 28/09/2016
         */
        this.getActualizacionService().borrarYActualizarReportesActualizacion(
                this.municipioSeleccionado, usuario.getLogin(), false);
    }

    /**
     * Método para cargar la regla con informacion de la tabla
     *
     * @author felipe.cadena
     */
    public void cargarArchivo(FileUploadEvent event) {

        int nColumnas = 0;

        this.archivoTabla = new ArrayList<String[]>();
        this.nombreArchivoTabla = event.getFile().getFileName();
        this.documentoTabla = new Documento();

        nColumnas = 2;

        this.archivoTabla = new ArrayList<String[]>();

        try {
			if(this.valoresActualizacion != null){
				for (ValoresActualizacion va : this.valoresActualizacion) {
					//valida archivo duplicado
					if (va.getNombre().equals(event.getFile().getFileName())) {
						this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_018.toString());
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
								Constantes.ERROR_REQUEST_CONTEXT);
						LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_018.getMensajeTecnico());
						return;
					}
				}
                        }
            CsvReader reglasImport = new CsvReader(event.getFile().getInputstream(), Charset.
                    defaultCharset());
            reglasImport.setDelimiter(';');
            reglasImport.readHeaders();

            while (reglasImport.readRecord()) {
                if (reglasImport.getColumnCount() < nColumnas) {
                    this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_004.toString());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                            Constantes.ERROR_REQUEST_CONTEXT);
                    LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_004.getMensajeTecnico());
                    return;
                }

                String[] record = new String[nColumnas];
                for (int i = 0; i < nColumnas; i++) {

                    record[i] = reglasImport.get(i);

                }

                this.archivoTabla.add(record);

            }

            //valida archivo vacio
            if (this.archivoTabla.isEmpty()) {
                this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_019.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                        Constantes.ERROR_REQUEST_CONTEXT);
                LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_019.getMensajeTecnico());
                return;
            }

            reglasImport.close();

            this.banderaTablaCargada = true;

            TipoDocumento td = new TipoDocumento();
            td.setId(ETipoDocumento.OTRO.getId());
            this.documentoTabla.setArchivo(event.getFile().getFileName());

            this.documentoTabla.setTipoDocumento(td);

            this.documentoTabla.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            this.documentoTabla.setBloqueado(ESiNo.NO.getCodigo());
            this.documentoTabla.setUsuarioCreador(this.usuario.getLogin());
            this.documentoTabla.setUsuarioLog(this.usuario.getLogin());
            this.documentoTabla.setFechaLog(new Date());

            //Se carga el documento en temporales
            UtilidadesWeb.cargarArchivoATemp(event, false);

            this.addMensajeInfo("Archivo cargado existosamente.");

        } catch (Exception e) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_006.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
            LOGGER.error(ECodigoErrorVista.REGISTRO_TABLAS_ACT_006.getMensajeTecnico(), e);
        }

    }

    /**
     * Cancela el proceso de carga cuando este presenta inconsistencias
     *
     * @author felipe.cadena
     */
    public void cancelarCargaArchivo() {

        this.banderaTablaCargada = false;

        this.archivosCarga.get(1)[2] = "false";
        this.archivosCarga.get(0)[2] = "false";

        if (this.erroresValidacion != null) {
            this.erroresValidacion.clear();
        }

    }

    /**
     * Valida si los archivos de tabla y regla fueron cargados de manera correcta.
     *
     * @author felipe.cadena
     */
    public void validarArchivosCarga() {
        if (this.tipoTablaSeleccionado == null) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_021.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_021.getMensajeTecnico());
            return;
        }

        if (!this.banderaTablaCargada) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_007.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_007.getMensajeTecnico());
            return;
        }

        this.erroresValidacion = new ArrayList<String[]>();
        this.banderaValidacionFallida = false;

        if (this.tipoTablaSeleccionado.equals(EValoresActualizacionTipo.CONSTRUCCION.getCodigo())) {
            this.validarArchivoConstrucciones();
        } else {
            this.validarArchivoTerreno();
        }

        //Si la validaciones son correctas se crea registro en VALORES_ACTUALIZACION y se invoca el procedimiento de cargar tablas
        ValoresActualizacion vaTerreno = new ValoresActualizacion();

        //preparar destinos
        if (this.destinosSelected.length < 1) {
            this.destinosSelected = new String[this.destinosItemList.size()];
            int k = 0;
            for (SelectItem si : this.destinosItemList) {
                this.destinosSelected[k++] = si.getValue().toString();
            }

        }

        //zonas
        if (this.zonasSelected.length < 1) {
            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_016.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
            LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_016.getMensajeTecnico());
            return;
        }

        try {

            //creacion atributos asociados a opciones de la tabla
            StringBuilder zona = new StringBuilder();
            StringBuilder destino = new StringBuilder();
            StringBuilder sector = new StringBuilder();
            StringBuilder zhf = new StringBuilder();
            StringBuilder usoCons = new StringBuilder();

            if (this.tipoTablaSeleccionado.equals(EValoresActualizacionTipo.TERRENO.getCodigo())) {
                for (String zonaSel : this.zonasSelected) {
                    for (String destinoSel : this.destinosSelected) {
                        if (this.datosTerreno != null && this.datosTerreno.contains(zonaSel + "-" + destinoSel)) {
                            this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO, zonaSel +
                                    "-" + destinoSel, VALOR_PREVIO_EXISTE_TERRENO);
                        }
                    }
                }
            } else {
                //destinos
                if (this.usosSelected.length < 1) {
                    this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_020.toString());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                            Constantes.ERROR_REQUEST_CONTEXT);
                    LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_020.getMensajeTecnico());
                    return;
                }
                for (String zonaSel : this.zonasSelected) {
                    for (String destinoSel : this.destinosSelected) {
                        for (String usoSel : this.usosSelected) {
                            if (this.sectoresSelected.length < 1) {
                                for (String zhfSel : this.zhfSelected) {
                                    if (this.datosConstruccion.contains(zonaSel + "-" + destinoSel +
                                            "-" + usoSel + "-" + "-" + zhfSel)) {
                                        this.agregarErrorValidacion(
                                                EValoresActualizacionTipo.CONSTRUCCION, zonaSel + "-" +
                                                        destinoSel + "-" + usoSel + "-" + "-" + zhfSel,
                                                VALOR_PREVIO_EXISTE_CONSTRUCCION);
                                    }
                                }
                            } else {
                                for (String sectorSel : this.sectoresSelected) {
                                    if (this.zhfSelected.length < 1) {
                                        if (this.datosConstruccion.contains(zonaSel + "-" +
                                                destinoSel + "-" + usoSel + "-" + sectorSel.substring(7) +
                                                "-")) {
                                            this.agregarErrorValidacion(
                                                    EValoresActualizacionTipo.CONSTRUCCION, zonaSel +
                                                            "-" + destinoSel + "-" + usoSel + "-" + sectorSel.
                                                            substring(7), VALOR_PREVIO_EXISTE_CONSTRUCCION);
                                        }
                                    }

                                    for (String zhfSel : this.zhfSelected) {
                                        if (zhfSel != null && this.datosConstruccion !=  null){
                                            if (this.datosConstruccion.contains(zonaSel + "-" +
                                                destinoSel + "-" + usoSel + "-" + "-" + sectorSel.
                                                substring(7) + "-" + zhfSel)) {
                                                this.agregarErrorValidacion(
                                                        EValoresActualizacionTipo.CONSTRUCCION, zonaSel +
                                                                "-" + destinoSel + "-" + usoSel + "-" + sectorSel.
                                                                substring(7) + "-" + zhfSel,
                                                        VALOR_PREVIO_EXISTE_CONSTRUCCION);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (this.banderaValidacionFallida) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                        Constantes.ERROR_REQUEST_CONTEXT);
                return;
            }

            //zonas
            if (this.zonasSelected.length > 0) {
                for (String zonaSel : this.zonasSelected) {
                    zona.append(zonaSel).append(",");
                }
                zona.deleteCharAt(zona.lastIndexOf(","));
            }

            //destinos
            if (this.destinosSelected.length > 0) {
                for (String destinoSel : this.destinosSelected) {
                    destino.append(destinoSel).append(",");
                }
                destino.deleteCharAt(destino.lastIndexOf(","));
            }

            if (this.tipoTablaSeleccionado.
                    equals(EValoresActualizacionTipo.CONSTRUCCION.getCodigo())) {

                //sectores
                if (this.sectoresSelected.length > 0) { 
                    for (String sectorSel : this.sectoresSelected) {
                        sector.append(sectorSel.substring(7)).append(",");
                    }
                    sector.deleteCharAt(sector.lastIndexOf(","));
                } else {
                    for (SelectItem sectorSel : this.sectoresItemList) {
                        sector.append(sectorSel.getValue().toString().substring(7)).append(",");
                    }
                    sector.deleteCharAt(sector.lastIndexOf(","));
                }

                //zhf
                if (this.zhfSelected.length > 0) {
                    for (String destinoSel : this.zhfSelected) {
                        zhf.append(destinoSel).append(",");
                    }
                    zhf.deleteCharAt(zhf.lastIndexOf(","));
                }

                //usos
                if (this.usosSelected.length > 0) {
                    for (String destinoSel : this.usosSelected) {
                        usoCons.append(destinoSel).append(",");
                    }
                    usoCons.deleteCharAt(usoCons.lastIndexOf(","));
                }

            }

            Departamento d = new Departamento();
            d.setCodigo(this.departamentoSeleccionado);
            vaTerreno.setDepartamento(d);
            Municipio m = new Municipio();
            m.setCodigo(this.municipioSeleccionado);
            vaTerreno.setMunicipio(m);
            vaTerreno.setZona(zona.toString());
            vaTerreno.setSector(sector.toString());
            vaTerreno.setDestino(destino.toString());
            vaTerreno.setUso(usoCons.toString());
            vaTerreno.setZhf(zhf.toString());
            vaTerreno.setEstado(EValoresActualizacionEstado.CARGADO.toString());
            vaTerreno.setTipo(this.tipoTablaSeleccionado);
            vaTerreno.setNombre(this.nombreArchivoTabla);
            vaTerreno.setUsuarioLog(this.usuario.getLogin());
            vaTerreno.setFechaLog(new Date());

            //vigencia
            Calendar c = Calendar.getInstance();
            c.set(Integer.valueOf(this.vigenciaSeleccionada), 0, 1);
            vaTerreno.setVigencia(c.getTime());

            //Se guarda el documento asociado
            DocumentoTramiteDTO tdDTOTerreno = new DocumentoTramiteDTO(
                    UtilidadesWeb.obtenerRutaTemporalArchivos() + File.separator + this.documentoTabla.
                            getArchivo(),
                    ETipoDocumento.OTRO.getNombre(),
                    new Date(),
                    "0",
                    0);
            tdDTOTerreno.setNombreDepartamento(this.departamentoSeleccionado);
            tdDTOTerreno.setNombreMunicipio(this.municipioSeleccionado);
            tdDTOTerreno.setNumeroPredial("0");
            this.documentoTabla = this.getGeneralesService().guardarDocumentoAlfresco(this.usuario,
                    tdDTOTerreno, this.documentoTabla);

            vaTerreno.setDocumentoId(this.documentoTabla.getId());
            vaTerreno = this.getActualizacionService().guardarActualizarValoresActualizacion(
                    vaTerreno);
        
             //LOGGER.info("Id tabla AX" + vaTerreno.getId());
             
             //Cargar_Tablas_Liquidacion
             
            Object[] resultado = this.getActualizacionService().
                    cargarTablasLiquidacion(this.municipioSeleccionado, vaTerreno.getId(), this.usuario.
                            getLogin());

            String[] mensajes = UtilidadesWeb.extraerMensajeSP(resultado);
            if (!mensajes[0].isEmpty()) {
                this.imprimirErrorProcedimientoDB(mensajes);
                return;
            }

            if (resultado == null) {
                this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_008.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                        Constantes.ERROR_REQUEST_CONTEXT);
                LOGGER.warn(ECodigoErrorVista.REGISTRO_TABLAS_ACT_008.getMensajeTecnico());
                return;
            }

        } catch (Exception ex) {

            this.addMensajeError(ECodigoErrorVista.REGISTRO_TABLAS_ACT_008.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT,
                    Constantes.ERROR_REQUEST_CONTEXT);
            LOGGER.error(ECodigoErrorVista.REGISTRO_TABLAS_ACT_008.getMensajeTecnico(), ex);
            return;

        }

        this.buscar();

        /*
         * En este punto se borran los reportes de actualización que existan para el municipio
         * seleccionado @javier.aponte 28/09/2016
         */
        this.getActualizacionService().borrarYActualizarReportesActualizacion(
                this.municipioSeleccionado, usuario.getLogin(), false);
    }

    /**
     * Calcula los avaluos con las tablas temporales de actualizacion correcta.
     *
     * @author felipe.cadena
     */
    public void calcularAvaluosMuncipio() {

        Object[] resultado = this.getActualizacionService().
                calcularAvaluosTablas(this.municipioSeleccionado);

        String[] mensajes = UtilidadesWeb.extraerMensajeSP(resultado);
        if (!mensajes[0].isEmpty()) {
            this.imprimirErrorProcedimientoDB(mensajes);
            return;
        }
        this.addMensajeInfo(ECodigoErrorVista.REGISTRO_TABLAS_ACT_011.toString());
        this.buscar();

        //se envia correo para informar del recalculo de avaluos.
        this.enviarCorreoRecalculo();

        /*
         * Desde este punto se realiza el llamado para borrar y actualizar los reportes de
         * actualización, se envía parametro generarReportes en true para que se generen de nuevo
         * los reportes de actualización javier.aponte :: 28/12/2015 :: refs#15460
         */
        this.getActualizacionService().borrarYActualizarReportesActualizacion(
                this.municipioSeleccionado, usuario.getLogin(), true);

    }

    /**
     * Coloca en firme las tablas de actualizacion para que queden como las de la proxima vigncia.
     * correcta.
     *
     * @author felipe.cadena
     */
    public void finalizarTablasActualizacion() {

        Object[] resultado = this.getActualizacionService().
                aplicarTablasLiquidacion(this.municipioSeleccionado);

        String[] mensajes = UtilidadesWeb.extraerMensajeSP(resultado);
        if (!mensajes[0].isEmpty()) {
            this.imprimirErrorProcedimientoDB(mensajes);
            return;
        }
        this.addMensajeInfo(ECodigoErrorVista.REGISTRO_TABLAS_ACT_012.toString());

        this.buscar();
    }

    /**
     * Se realizan las validaciones correspondientes a las construcciones
     *
     * @author felipe.cadena
     */
    public void validarArchivoConstrucciones() {

        List<UsoConstruccion> usosSeleccionados = new ArrayList<UsoConstruccion>();

        boolean[] puntajes = new boolean[100];
        puntajes[0] = true;
        //Arrays.fill(puntajes,true);
        
        //LOGGER.info("tamaño archivo " + this.archivoTabla.size());
        
        int fila = 0;
        for (String[] registro : this.archivoTabla) {

            fila++;
            try {
                //valida valores en blanco
                if ("".equals(registro[0]) ||
                        "".equals(registro[1])) {
                    this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION,
                            registro[0] + "-" + registro[1], VALOR_BLANCO + fila);
                }

                //valida puntajes
                if (!"".equals(registro[0])) {
                    Integer puntajeNum = Integer.valueOf(registro[0]);
                    if (puntajes[puntajeNum]) {
                        this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION,
                                registro[0] + "-" + registro[1], PUNTAJE_UNICIDAD + fila);
                    } else {
                        puntajes[puntajeNum] = true;
                    }
                    // LOGGER.info("puntaje " + puntajeNum);
                }
               
                //valida puntaje anexos
                for (String uso : this.usosSelected) {

                    if (this.puntajesAnexos.containsKey(uso)) {
                        List<CalificacionAnexo> puntajesManual = this.puntajesAnexos.get(uso);
                        boolean existePuntaje = false;
                        for (CalificacionAnexo calificacionAnexo : puntajesManual) {
                            LOGGER.info("POR ACAC " + calificacionAnexo.getPuntos().toString());
                            LOGGER.info("POR ACAC " + registro[0]);
                            if (registro[0].equals(calificacionAnexo.getPuntos().toString())) {
                                existePuntaje = true;
                                break;
                            }
                        }

                        if (!existePuntaje) {
                            this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION,
                                    registro[0] + "-" + registro[1], PUNTAJE_NO_CONVENCIONAL + uso);
                        }
                    }
                }

                //valida puntaje rango
                if (!"".equals(registro[0])) {
                    Integer puntaje = Integer.valueOf(registro[0]);
                    if (puntaje < 1 || puntaje > 99) {
                        this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION,
                                registro[0] + "-" + registro[1], PUNTAJE_RANGO + fila);
                    }
                }

                //valida valor
                if (!"".equals(registro[1])) {
                    Double valor = Double.valueOf(registro[1]);
                    if (valor <= 0) {
                        this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION,
                                registro[0] + "-" + registro[1], VALOR);
                    }
                }

            } catch (NumberFormatException ex) {
                //Valores numericos con caracteres raros
                LOGGER.error(ex.getMessage(), ex);
                this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION, registro[0] +
                        "-" + registro[1], VALOR_NUMERICO_INCORRECTO + fila);
            }

        }

        //valida tipos de uso de costruccion
        for (String uso : this.usosSelected) {
            for (UsoConstruccion ucs : this.usosConstruccionSistema) {
                if (ucs.getId().equals(Long.valueOf(uso))) {
                    usosSeleccionados.add(ucs);
                }
            }
        }

        boolean convencional = true;
        for (int i = 0; i < usosSeleccionados.size(); i++) {
            UsoConstruccion uso = usosSeleccionados.get(i);
            if (uso.getTipoUso().equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                if (i > 0 && !convencional) {
                    this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION, uso.
                            getNombre(), MIXED_CONVENCIONALES);
                }
                convencional = true;
            } else {
                if (i > 0 && convencional) {
                    this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION, uso.
                            getNombre(), MIXED_CONVENCIONALES);
                }
                convencional = false;
            }

        }

            //LOGGER.info("puntaje tamaño " + puntajes.length);
        //Valida total puntajes convencionales
        if (convencional) {
            for (int i = 0; i < puntajes.length; i++) {
                //LOGGER.info("puntaje bool " + puntajes[i]);
                if (!puntajes[i]) {
                    this.agregarErrorValidacion(EValoresActualizacionTipo.CONSTRUCCION, "Punto: " +
                            i, NO_PUNTAJE);
                }
            }
        }

    }

    /**
     * Valida el archivo relacionado a los datos de terreno
     *
     * @author felipe.cadena
     */
    public void validarArchivoTerreno() {

        boolean[] zonasExistentes = new boolean[100];

        int fila = 0;
        //validaciones terreno
        for (String[] registro : this.archivoTabla) {
            fila++;

            try {
                //valida valores en blanco
                if ("".equals(registro[0]) ||
                        "".equals(registro[1])) {
                    this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO,
                            registro[0] + "-" + registro[1], VALOR_BLANCO + fila);
                }

                //validar zonas
                if (!"".equals(registro[0])) {
                    if (registro[0].length() != 2) {
                        this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO, registro[0] +
                                "-" + registro[1], ZONA + fila);
                    }
                    Integer zonaNum = Integer.valueOf(registro[0]);
                    if (zonaNum < 1 || zonaNum > 99) {
                        this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO, registro[0] +
                                "-" + registro[1], ZONA + fila);
                    } else {
                        if (zonasExistentes[zonaNum]) {
                            this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO,
                                    registro[0] + "-" + registro[1], ZONA_DUPLICADA + fila);
                        } else {
                            zonasExistentes[zonaNum] = true;
                        }
                    }

                }

                //valida valor
                if (!"".equals(registro[1])) {
                    Double valor = Double.valueOf(registro[1]);
                    if (valor <= 0) {
                        this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO, registro[0] +
                                "-" + registro[1], VALOR + fila);
                    }
                }

            } catch (NumberFormatException ex) {
                //Valores numericos con caracteres raros
                LOGGER.error(ex.getMessage(), ex);
                this.agregarErrorValidacion(EValoresActualizacionTipo.TERRENO, registro[0] + "-" +
                        registro[1], VALOR_NUMERICO_INCORRECTO + fila);
            }

        }
    }

    /**
     * Agrega una inconsistencia a la lista asociada a una carga de tabla de avaluo la cual se
     * muestra al usuario
     *
     * @param tabla
     * @param valor
     * @param inconsistencia
     * @author felipe.cadena
     */
    private void agregarErrorValidacion(EValoresActualizacionTipo tabla, String valor,
                                        String inconsistencia) {
        String[] error = {tabla.getCodigo(), valor, inconsistencia};
        this.banderaValidacionFallida = true;
        this.erroresValidacion.add(error);
    }

    /**
     * Prepara las opciones asociadas a una seleccion en particular
     *
     * @author felipe.cadena
     */
    public void cargarDetallesOpcion() {

        this.detallesOpcion = new ArrayList<SelectItem>();

        if (this.opcionSeleccionadaDetalle.equals("ZONA")) {
            //zonas
            String[] zonas = this.valorActualizacionSeleccionado.getZona().split(",");

            for (String zona : zonas) {
                for (SelectItem si : this.combos.getZonasItemList()) {
                    if (zona.equals(si.getValue())) {
                        this.detallesOpcion.add(si);
                    }
                }
            }
        }
        if (this.opcionSeleccionadaDetalle.equals("SECTOR")) {
            this.zonasItemList = new ArrayList<SelectItem>();
            this.zonasSelected = this.valorActualizacionSeleccionado.getZona().split(",");

            this.updateSectores();

            //sectores
            String[] sectores = this.valorActualizacionSeleccionado.getSector().split(",");

            for (String sector : sectores) {
//                for (SelectItem si : this.combos.getSectoresItemList()) {
//                    if (sector.equals(si.getValue())) {
//                        this.detallesOpcion.add(si);
//                    }
//                }
                this.detallesOpcion.add(new SelectItem(sector, sector));
            }
        }
        if (this.opcionSeleccionadaDetalle.equals("ZHF")) {
            //zhf
            String[] zhfs = this.valorActualizacionSeleccionado.getZhf().split(",");

            for (String zhf : zhfs) {
                for (SelectItem si : this.zhfItemList) {
                    if (zhf.equals(si.getValue())) {
                        this.detallesOpcion.add(si);
                    }
                }
            }
        }
        if (this.opcionSeleccionadaDetalle.equals("DESTINO")) {
            //destinos
            String[] destinos = this.valorActualizacionSeleccionado.getDestino().split(",");

            for (String destino : destinos) {
                for (SelectItem si : this.destinosItemList) {
                    if (destino.equals(si.getValue())) {
                        this.detallesOpcion.add(si);
                    }
                }
            }
        }
        if (this.opcionSeleccionadaDetalle.equals("USO")) {
            //usos
            String[] usos = this.valorActualizacionSeleccionado.getUso().split(",");

            for (String uso : usos) {
                for (SelectItem si : this.usosItemList) {
                    if (uso.equals(si.getValue())) {
                        this.detallesOpcion.add(si);
                    }
                }
            }
        }

    }

    private void imprimirErrorProcedimientoDB(String[] mensajes) {
        this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(mensajes[0]));
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam(Constantes.ERROR_REQUEST_CONTEXT, Constantes.ERROR_REQUEST_CONTEXT);
        LOGGER.warn(ECodigoErrorVista.ERROR_DB_PROC.getMensajeTecnico(mensajes[1]));
    }

    /**
     * Método encargado de generar el reporte de terreno o construcción
     *
     * @author javier.aponte
     */
    public void generarReporteDeTerrenoOConstruccion() {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_DE_TERRENO;

        if (EValoresActualizacionTipo.CONSTRUCCION.getCodigo().equals(
                this.valorActualizacionSeleccionado.getTipo())) {
            enumeracionReporte = EReporteServiceSNC.REPORTE_DE_CONSTRUCCION;
        }

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("VALOR_ACTUALIZACION_ID", String.valueOf(this.valorActualizacionSeleccionado.
                getId()));

        try {

            this.reporteDeTerrenoOConstruccion = reportsService.generarReporte(parameters,
                    enumeracionReporte, this.usuario);

        } catch (Exception e) {
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER,
                    e, enumeracionReporte.getUrlReporte());
        }
    }

    /**
     * Metodo para enviar correo al usuario cuando se recalculan los avluos basaos en las tablas
     * cargadas.
     *
     * @author felipe.cadena
     */
    private void enviarCorreoRecalculo() {

        String correoDestino = this.usuario.getEmail();
        String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_RECALCULO_AVALUOS_ACTUALIZACION;

        if (correoDestino == null || correoDestino.isEmpty()) {
            return;
        }
        // Consulta de la plantilla del contenido.
        Plantilla plantillaContenido = this.getGeneralesService().
                recuperarPlantillaPorCodigo(EPlantilla.MENSAJE_RECALCULO_AVALUOS_ACTUALIZACION.
                        getCodigo());

        // Consulta del contenido del correo.
        String contenidoCorreo = plantillaContenido.getHtml();

        // Envio de correo electronico
        this.getGeneralesService().enviarCorreo(correoDestino, asunto,
                contenidoCorreo, null, null);
    }

    /**
     * Metodo para cargar archivos relacionados a un nuevo proceso de actualizacion
     *
     * @param eventoCarga
     * @author felipe.cadena
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        String rutaZip = eventoCarga.getFile().getFileName();

        try {
            String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

            this.archivoDatosActualizacion = new File(directorioTemporal, rutaZip);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoDatosActualizacion);

            byte[] buffer;
            buffer = new byte[(int) eventoCarga.getFile().getSize()];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.rutaArchivoDatosActualizacion = this.archivoDatosActualizacion.getCanonicalPath();

            this.addMensajeInfo(ECodigoErrorVista.RADICACION_ACTUALIZACION_012.toString());

            this.banderaArchivoCargado = true;

        } catch (IOException e) {
            this.addMensajeInfo(ECodigoErrorVista.RADICACION_ACTUALIZACION_011.toString());
            this.addErrorCallback();
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_011.getMensajeTecnico(), e);

        }
    }
    
    /**
     * Metodo para cargar archivos pdf
     *
     * @param eventoCarga
     * @author carlos.bello
     */
    public void archivoUploadDocumentos(FileUploadEvent eventoCarga) {

        String ruta = eventoCarga.getFile().getFileName();

        try {
            String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

            this.archivoPdf = new File(directorioTemporal, ruta);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.archivoPdf);

            byte[] buffer;
            buffer = new byte[(int) eventoCarga.getFile().getSize()];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.rutaArchivoPdf = this.archivoPdf.getCanonicalPath();
            this.banderaArchivoCargado = true;

            this.addMensajeInfo(ECodigoErrorVista.RADICACION_ACTUALIZACION_012.toString());           

        } catch (IOException e) {
            this.addMensajeInfo(ECodigoErrorVista.RADICACION_ACTUALIZACION_011.toString());
            this.addErrorCallback();
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_011.getMensajeTecnico(), e);

        }
    }

    /**
     * Metodo para cargar archivos relacionados a un nuevo proceso de actualizacion
     *
     * @author felipe.cadena
     */
    public void procesarArchivoCarga() {

        List<String> prediosCarga = new ArrayList<String>();

        //procesa numeros prediales
        String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();
        File archivoResumen  = new File(directorioTemporal, "archivoResumen.conf");

        archivoResumen = UtilidadesWeb.unzipArchivoUnico(this.archivoDatosActualizacion, archivoResumen);


        String st;
        try {
            BufferedReader br = new BufferedReader(new FileReader(archivoResumen));
            boolean firstLine = true;

            while ((st = br.readLine()) != null) {
                if(firstLine){
                    this.cantidadPrediosCica = Integer.valueOf(st);
                    firstLine = false;
                    continue;
                }

                if (st.equals(PREDIOS_END_FLAG)) {
                    break;
                } else {
                    prediosCarga.add(st);
                }

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        this.numPrediosCargue = prediosCarga.size();

        List<PPredio> prediosSnc = this.getConservacionService().consultaPPrediosPorNumeroPredial(prediosCarga, "14", false);

        this.prediosProyectados = this.procesarProyeccionSnc(prediosSnc);

        if(this.prediosProyectados.isEmpty()){
            this.prediosProyectadosSnc = false;
            this.prediosProyectados = this.validarPrediosProyectados(prediosCarga);
        } else{
            this.prediosProyectadosSnc = true;
        }

        if (!this.prediosProyectados.isEmpty()) {
            List<String> prediosDevueltos = new ArrayList<String>();

            for (String predio:prediosCarga) {
                if(!this.prediosProyectados.contains(predio)){
                    prediosDevueltos.add(predio);
                }
            }
            this.generarArchivoDevueltos(prediosDevueltos);
            this.addErrorCallback();
        } else {
            this.procesarCargue(archivoResumen);
        }

        this.banderaArchivoCargado = false;

    }
    
    /**
     * Metodo para cargar archivos soporte en pdf
     *
     * @author 
     */
    public void procesarArchivoCargaPdf() {
        
        guardarArchivoCargue(archivoPdf.getPath(), null);

        this.establecerCargues();

        this.banderaArchivoCargado = false;

    }
    
    /**
     * Metodo para descargar archivos soporte en pdf
     *
     * @author 
     */
    public void descargaPdfSoporte(){
        List<Documento> docsReporte = this.getGeneralesService().
                buscarDocumentoPorNumeroDocumentoYTipo(this.vigenciaSeleccionada + "-" + this.municipioSeleccionado,
                        new Long(2011)
                );
        if (docsReporte != null && !docsReporte.isEmpty()) {
            //Collections.sort(docsReporte);
            Documento docReporte = docsReporte.get(0);

            ReporteDTO rdto;
            ReportesUtil rUtil = ReportesUtil.getInstance();
            rdto = rUtil.consultarReporteDeGestorDocumental(docReporte.getIdRepositorioDocumentos());
            this.documentoSoporte = rdto.getStreamedContentReporte();
        }

        if (this.documentoSoporte == null) {
            this.addMensajeError("NO HAY ARCHIVOS");
            LOGGER.warn("NO HAY ARCHIVOS");
        }
    }

    public List<String> procesarProyeccionSnc( List<PPredio> prediosSnc){

        List<String> prediosDevueltos = new ArrayList<String>();

        for (PPredio predio:prediosSnc) {
            if(!this.prediosProyectados.contains(predio)){
                prediosDevueltos.add(predio.getNumeroPredial());
            }
        }

        return prediosDevueltos;

    }

    public void procesarCargue(File archivoResumen){
        MunicipioActualizacion nuevoCargue = new MunicipioActualizacion();

        Departamento depto = new Departamento();
        depto.setCodigo(this.departamentoSeleccionado);
        nuevoCargue.setDepartamento(depto);

        Municipio mun = new Municipio();
        mun.setCodigo(this.municipioSeleccionado);
        nuevoCargue.setMunicipio(mun);
        nuevoCargue.setVigencia(Integer.valueOf(this.vigenciaSeleccionada));
        nuevoCargue.setPredios(Integer.valueOf(this.numPrediosCargue).longValue());

        nuevoCargue.setEstado(ECargueCicaEstado.PROCESANDO.getCodigo());
        nuevoCargue.setActivo(ESiNo.SI.getCodigo());
        nuevoCargue.setFechaLog(new Date());
        nuevoCargue.setFechaInicio(new Date());
        nuevoCargue.setUsuarioLog(this.usuario.getLogin());

        this.archivoDevueltoDescargado = true;
        nuevoCargue = this.getActualizacionService().guardarArchivosActualizacion(nuevoCargue,
                this.usuario, this.archivoDatosActualizacion.getAbsolutePath());
        guardarArchivoCargue(archivoResumen.getPath(), nuevoCargue);

        this.getActualizacionService().iniciarCargueCica(nuevoCargue.getId(), this.usuario.getLogin());
        this.establecerCargues();

    }

    public void procesarCargueDirecto(){

        String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();
        File archivoResumen  = new File(directorioTemporal, "archivoResumen.conf");

        archivoResumen = UtilidadesWeb.unzipArchivoUnico(this.archivoDatosActualizacion, archivoResumen);
        this.procesarCargue(archivoResumen);

    }

    /**
     * Determina si existen predios proyetados en el listados dado
     *
     * @param predios
     * @return
     */
    public List<String> validarPrediosProyectados(List<String> predios) {

        List<String> prediosProyectados = new ArrayList<String>();
        List<VCargueCica> vPrediosProyectados = this.getActualizacionService().buscarPrediosProyectadosCica(predios);

        for (VCargueCica vc:vPrediosProyectados) {
            prediosProyectados.add(vc.getNumeroPredialCica());
        }

        return prediosProyectados;
    }


    /**
     * Genera el archivo de los predios para devolver
     *
     * @param predios
     * @return
     */
    public void generarArchivoDevueltos(List<String> predios) {

        try {
            File fileDevueltoCica = new File("DevolucionCargue.SNC");
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileDevueltoCica));

            for(String predio:predios) {
                writer.write(predio+"\n");
            }
            writer.write(this.PREDIOS_END_FLAG);

            writer.close();
            InputStream stream;
            stream = new FileInputStream(fileDevueltoCica);
            this.archivoDevueltoCica = new DefaultStreamedContent(stream,
                    "", "DevolucionCargue.SNC");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Prepara los predios para la devolucion al CICA
     */
    public void setupDevolverPredios(){

        List<String> prediosSeleccionados = new ArrayList<String>();

        if(prediosProyectadosSeleccionados.length == 0){
            this.addMensajeError("Debe seleccionar por lo menos un predio.");
            this.addErrorCallback();
            return;
        }

        List<VCargueCica> prediosBase = Arrays.asList(this.prediosProyectadosSeleccionados);

        List<VCargueCica> prediosAsociados = this.getActualizacionService().buscarPrediosAsociadosCargue(prediosBase);

        this.prediosParaEliminarProyeccion = new ArrayList<Long>();
        for(VCargueCica item:prediosAsociados){
            this.prediosParaEliminarProyeccion.add(item.getPredioId());
        }

        for(VCargueCica item:prediosAsociados){
            prediosSeleccionados.add(item.getNumeroPredialCica());
        }

        this.generarArchivoDevueltos(prediosSeleccionados);

    }

    /**
     * Confirma la devolucion de los predios al CICA
     */
    public void confirmarDevolverPredios(){


        this.prediosParaEliminarProyeccion.size();
        this.prediosProyectadosSeleccionados = null;

        this.getActualizacionService().eliminarProyeccionCica(this.prediosParaEliminarProyeccion);
        this.establecerCargues();

    }

    /**
     * Prepara los datos para confirma la liquidacion
     */
    public void setupConfirmarLiquidacion(){

        //TODO::validar si se puede liquidar



    }

    /**
     * Confirma la liquidacion
     */
    public void confirmarLiquidacion(){

        this.getActualizacionService().aplicarCambiosAct(this.municipioSeleccionado, Integer.valueOf(this.vigenciaSeleccionada));

        this.cargueFinalizado = true;

    }

    /**
     * Muestra el reporte de inconsistencias cuando la validacion falla
     *
     * @author felipe.cadena
     */
    public void generarReporteInconsistencias() {

        List<Documento> docsReporte = this.getGeneralesService().
                buscarDocumentoPorNumeroDocumentoYTipo(
                        this.cargueSeleccionado.getId() + "-" +
                                this.cargueSeleccionado.getMunicipio().getCodigo(),
                        ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION.getId()
                );
        if (docsReporte != null && !docsReporte.isEmpty()) {

            Documento docReporte = docsReporte.get(0);

            ReporteDTO rdto;
            ReportesUtil rUtil = ReportesUtil.getInstance();
            rdto = rUtil.consultarReporteDeGestorDocumental(docReporte.getIdRepositorioDocumentos());
            this.reporteInconsistencias = rdto.getStreamedContentReporte();
        }

        if (this.reporteInconsistencias == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_002.toString());
            LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_002.getMensajeTecnico());
        }
    }

    /**
     * Consulta el archivo con los predios involucrados en el cargue
     *
     * @author felipe.cadena
     */
    public void consultarArchivoCargue() {

        List<Documento> docsReporte = this.getGeneralesService().
                buscarDocumentoPorNumeroDocumentoYTipo(
                        this.cargueSeleccionado.getId() + "-" +
                                this.cargueSeleccionado.getMunicipio().getCodigo(),
                        ETipoDocumento.LISTADO_PREDIOS_CARGUE_CICA.getId()
                );
        if (docsReporte != null && !docsReporte.isEmpty()) {

            Documento docReporte = docsReporte.get(0);

            ReporteDTO rdto;
            ReportesUtil rUtil = ReportesUtil.getInstance();
            rdto = rUtil.consultarReporteDeGestorDocumental(docReporte.getIdRepositorioDocumentos());
            this.archivoDevueltoCica = rdto.getStreamedContentReporte();
            this.archivoDevueltoDescargado = true;
        }

    }

    public void descargarArchivoDevueltos(){
        this.archivoDevueltoDescargado = false;
    }

    /**
     * Guardar el archivo con los predios involucrados en el cargue
     *
     * @author felipe.cadena
     */
    public void guardarArchivoCargue(String localPath, MunicipioActualizacion nuevoCargue) {

        DocumentoTramiteDTO dtDto = new DocumentoTramiteDTO(
                localPath,
                ETipoDocumento.LISTADO_PREDIOS_CARGUE_CICA.getNombre(),
                new Date(), null, 0);

        dtDto.setNumeroPredial(this.municipioSeleccionado);
        dtDto.setNombreDepartamento(this.departamentoSeleccionado);
        dtDto.setNombreMunicipio(this.municipioSeleccionado);

        Documento d = new Documento();
        TipoDocumento td = new TipoDocumento();
        
        if(nuevoCargue != null){
            td.setId(ETipoDocumento.LISTADO_PREDIOS_CARGUE_CICA.getId());
            d.setTipoDocumento(td);
            d.setNumeroDocumento(nuevoCargue.getId() + "-" +
                this.municipioSeleccionado);
        }else{
            td.setId(new Long(2011));
            d.setTipoDocumento(td);
            d.setNumeroDocumento(this.vigenciaSeleccionada + "-" +
                this.municipioSeleccionado);
        }
        
        d.setArchivo(localPath);
        d.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        d.setBloqueado(ESiNo.NO.getCodigo());
        d.setUsuarioCreador(this.usuario.getLogin());
        d.setUsuarioLog(this.usuario.getLogin());
        d.setFechaLog(new Date());

        d = this.getGeneralesService().actualizarDocumentoYSubirArchivoAlfresco(usuario,
                dtDto, d);
    }


    /**
     * Elimina un registro de la tabla de cargues
     *
     * @return
     */
    public void eliminarCargue() {
        this.getActualizacionService().eliminarMunicipioActualizacion(this.cargueSeleccionado);
        this.establecerCargues();
    }


    /**
     * Carga resumen de predios
     *
     * @return
     */
    public void buscarResumenPredios() {

        if(this.filtroCargueDTO.getFilters()==null){
            this.filtroCargueDTO.setFilters(new HashMap<String, String>());
        }

        this.listaResumenPredios = this.getActualizacionService().buscarResumenPredios(this.filtroCargueDTO);
    }

    /**
     * Inicializa los filtros avanzados
     */
    public void setupFiltros(){

        if(this.filtroCargueDTO==null){
            this.filtroCargueDTO = new FiltroCargueDTO();
        }

        this.filtroCargueDTO.setDeptoCodigo(this.departamentoSeleccionado);
        this.filtroCargueDTO.setMunicipioCodigo(this.municipioSeleccionado);
        this.filtroCargueDTO.setVigencia(Integer.valueOf(this.vigenciaSeleccionada));

    }

    /**
     * Listener de cambio de tab en el tabview
     */
    public void onTabChange(TabChangeEvent event){

        String tabId = event.getTab().getId();

        if (tabId.equals("tabInfoAlfa")) {
            this.setupFiltros();
            this.prediosProyectadosCargue = new LazyDataModel<VCargueCica>() {

                @Override
                public List<VCargueCica> load(int first, int pageSize,
                                          String sortField, SortOrder sortOrder, Map<String, String> filters) {
                    List<VCargueCica> answer = new ArrayList<VCargueCica>();

                    populatePrediosLazyly(answer, first, pageSize, sortField, sortOrder.toString(),
                            filters);

                    return answer;
                }
            };
        }

    }

    /**
     * Hace la búsqueda de trámites por filtros
     *
     * @author javier.aponte
     */
    private void populatePrediosLazyly(List<VCargueCica> answer, int first, int pageSize,
                                        String sortField, String sortOrder, Map<String, String> filters) {

        List<VCargueCica> rows;
        int size, count;

        this.filtroCargueDTO.setDeptoCodigo(this.departamentoSeleccionado);
        this.filtroCargueDTO.setMunicipioCodigo(this.municipioSeleccionado);
        this.filtroCargueDTO.setVigencia(Integer.valueOf(this.vigenciaSeleccionada));

        this.filtroCargueDTO.setFirst(first);
        this.filtroCargueDTO.setPageSize(pageSize);
        this.filtroCargueDTO.setSortField(sortField);
        this.filtroCargueDTO.setSortOrder(sortOrder);
        this.filtroCargueDTO.setFilters(filters);
        rows = this.getActualizacionService().
                buscarPPrediosCargueCica(this.filtroCargueDTO);


        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }
        count = 0;

        //D: se cuentan los de la bd asignar el tamaño del datamodel
        count = this.getActualizacionService().
                countPPrediosCargueCica(this.filtroCargueDTO);

        this.prediosProyectadosCargue.setRowCount(count);

    }

    public void filtrarInfoAlfanumerica(){
        LOGGER.debug(this.filtroCargueDTO.getAreaConstruccionFinal().toString());
    }

    /**
     * Limpia los filtros avanzados
     */
    public void limpiarFiltros(){
        this.filtroCargueDTO.getAreaConstruccionFinal().limpiarValores();
        this.filtroCargueDTO.getAreaConstruccionInicial().limpiarValores();
        this.filtroCargueDTO.getAreaConstruccionVariacion().limpiarValores();

        this.filtroCargueDTO.getAreaTerrenoInicial().limpiarValores();
        this.filtroCargueDTO.getAreaTerrenoFinal().limpiarValores();
        this.filtroCargueDTO.getAreaTerrenoVariacion().limpiarValores();

        this.filtroCargueDTO.getAvaluoInicial().limpiarValores();
        this.filtroCargueDTO.getAvaluoFinal().limpiarValores();
        this.filtroCargueDTO.getAvaluoVariacion().limpiarValores();
    }

    /**
     * Consulta los datos de la tabla de informacion alfanumerica
     */
    public void buscarInfoAlfanumerica(){

        //TODO::generar omisiones

    }


    /**
     * Genera el informe de omisiones y comisiones
     */
    public void generarInformeOmisiones(){

       //TODO::generar omisiones

    }

    /**
     * Invoca preliquidación
     */
    public void preliquidar(){

        this.getActualizacionService().preliquidarCargueAct(this.municipioSeleccionado, Integer.valueOf(this.vigenciaSeleccionada));

        this.carguePreliquidado = true;

    }

    /**
     * Invoca renumeracion
     */
    public void renumerar(){

        //TODO::renumerar

    }


    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("reportesTablasActualizacion");

        UtilidadesWeb.removerManagedBean("registroTablasActualizacion");

        return ConstantesNavegacionWeb.INDEX;
    }

}
