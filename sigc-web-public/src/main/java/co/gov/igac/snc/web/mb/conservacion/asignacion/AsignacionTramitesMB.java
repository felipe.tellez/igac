/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.Actividades;
import co.gov.igac.snc.apiprocesos.indicadores.contenedores.TablaContingencia;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.comun.colecciones.Pair;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.SEjecutoresTramite;
import co.gov.igac.snc.util.TramiteConEjecutorAsignadoDTO;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Managed bean para la asignación de trámites
 *
 * N: no se hace actualización de la tabla TRAMITE_ESTADO sino hasta el momento en que se mueve el
 * proceso
 *
 * @author pedro.garcia
 * @version 2.0
 * @cu CU-CO-A-05
 */
@Component("asignacionTramites")
@Scope("session")
public class AsignacionTramitesMB extends ProcessFlowManager {

    /**
     * número serial autogenerado
     */
    private static final long serialVersionUID = 8472942067201587387L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AsignacionTramitesMB.class);

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextoWeb;

    private UsuarioDTO usuario;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites a los que no se
     * les ha asignado ejecutor
     */
    private LazyDataModel<Tramite> lazyWorkingTramites;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites a los ya se les
     * ha asignado ejecutor, pero aún no han sido cambiados al siguiente estado del BPM
     */
    private LazyDataModel<Tramite> lazyAssignedTramites;

    /**
     * Mapa que contiene los ids de trámite y su clasificación correspondiente
     */
    private List<TramiteConEjecutorAsignadoDTO> tramitesConEjecutorAsignado;

    /**
     * variable que guarda el valor del id de la territorial del usuario
     */
    private String territorialId;

    private List<String> funcionarios;
    private List<SEjecutoresTramite> ejecutoresTramite;
    private SEjecutoresTramite ejecutorSeleccionado;
    private boolean showReclasificacionModal;

    /**
     * Trámites seleccionados de la tabla de trámites sin ejecutor asignado
     */
    private Tramite[] selectedTramites;

    /**
     * Trámites seleccionados de la tabla de trámites con ejecutor asignado
     */
    private Tramite[] selectedAssignedTramites;

    /**
     * trámite seleccionado para ver detalles. Se debe tener aquí porque es posible que el usuario
     * decida soicitar docs adicionales para el trámite del que está viendo los detalles
     */
    private Tramite selectedTramiteForDetailsChecking;

    /**
     * variable usada para poder preguntar en la página si hubo algún error de validación, ya que no
     * se pudo hacer funcionar el mecanismo del onerror o onsuccess de los componentes primefaces
     */
    private String errorSeverity;

    /**
     * is del trámite que se escohe para ver los detalles
     */
    private Long selectedTramiteId;

    /**
     * Lista de tramite(s) asociado(s) a un tramite
     */
    private List<Tramite> tramitesAsociados;

    /**
     * Banderas de visualizacion de botones para solicitar otro documento
     */
    private boolean otroDoc;
    private boolean predioNoSeleccionado;
    private boolean adicionarDoc;
    private boolean generarOficioB;

    /**
     * Cadena modo de adquisición en solicitar otro documento
     */
    private String modoDeadquisicion;
    private String modoDeadquisicion2;

    /**
     * Lista de documentos asociados a un tramite
     */
    private List<TramiteDocumentacion> documentos;

    /**
     * Lista de documentos faltantes asociados a un tramite
     */
    private List<TramiteDocumentacion> documentosFaltantes;

    /**
     * Lista de documentos aportados asociados a un tramite
     */
    private List<TramiteDocumentacion> documentosAportados;

    /**
     * Lista de documentos adicionales aportados asociados a un tramite
     */
    private List<TramiteDocumentacion> documentosAdicionalesAportados;

    /** *
     * Documentacion asociada a un tramite
     */
    private TramiteDocumentacion tramiteDocumentacion;

    /**
     * Lista de items seleccionados para adquisicion
     */
    private List<SelectItem> adq;

    /**
     * Lista de docuemntos solicitados
     */
    private List<TramiteDocumentacion> documentosSoli;

    /**
     * Detalle de documento para solicitar un nuevo documento
     */
    private String detalleDocumento;

    /**
     * Estado del tramite
     */
    private TramiteEstado estadoTramite;

    /**
     * Tramites de la tabla de Tramites asignado a un ejecutor
     */
    private List<Tramite> assignedTramitesAEjecutor;

    /**
     * Almacena el id del trámite con la actividad en la que se encuentra actualmente.
     */
    private Map<Long, String> tablaActividades;

    /**
     * Variables para las graficas
     */
    private StreamedContent chartTerreno;
    private StreamedContent chartOficina;
    // entero para el numero de secuencia de la grafica
    public int fileG = 0;

    // Lista de funcionarios ejecutores pero de tipo UsuarioDTO
    private List<UsuarioDTO> ejecutoresTramiteDTO;

    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    /**
     * Lista de tramites para ser asignados a ejecutores antes de mover el proceso para el calculo
     * de graficas
     */
    private List<Tramite[]> tramitesPorAsignar;

    private String prediosParaZoom;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * Variable booleana usada para mostrar o no los trámites asignados a un ejecutor seleccionado.
     */
    private boolean visualizarPanelTramitesBool;

    /**
     * Tramites que fallan al avanzar posque la activiad ya esta reclamada.
     */
    private List<Long> tramitesErrorAvanzar = new ArrayList<Long>();

    // ------------- methods ------------------------------------
    public Tramite getSelectedTramiteForDetailsChecking() {
        return this.selectedTramiteForDetailsChecking;
    }

    public void setSelectedTramiteForDetailsChecking(
        Tramite selectedTramiteForDetailsChecking) {
        this.selectedTramiteForDetailsChecking = selectedTramiteForDetailsChecking;
    }

    public List<Tramite> getAssignedTramitesAEjecutor() {
        return this.assignedTramitesAEjecutor;
    }

    public void setAssignedTramitesAEjecutor(
        List<Tramite> assignedTramitesAEjecutor) {
        this.assignedTramitesAEjecutor = assignedTramitesAEjecutor;
    }

    public String getTerritorialId() {
        return territorialId;
    }

    public StreamedContent getChartTerreno() {
        return this.chartTerreno;
    }

    public void setChartTerreno(StreamedContent chartTerreno) {
        this.chartTerreno = chartTerreno;
    }

    public StreamedContent getChartOficina() {
        return this.chartOficina;
    }

    public void setChartOficina(StreamedContent chartOficina) {
        this.chartOficina = chartOficina;
    }

    public Long getSelectedTramiteId() {
        return selectedTramiteId;
    }

    public void setSelectedTramiteId(Long selectedTramiteId) {
        this.selectedTramiteId = selectedTramiteId;
    }

    public Tramite[] getSelectedTramites() {
        return this.selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public void setTerritorialId(String territorialId) {
        this.territorialId = territorialId;
    }

    public boolean isShowReclasificacionModal() {
        return showReclasificacionModal;
    }

    public void setShowReclasificacionModal(boolean showReclasificacionModal) {
        this.showReclasificacionModal = showReclasificacionModal;
    }

    public boolean isVisualizarPanelTramitesBool() {
        return visualizarPanelTramitesBool;
    }

    public void setVisualizarPanelTramitesBool(boolean visualizarPanelTramitesBool) {
        this.visualizarPanelTramitesBool = visualizarPanelTramitesBool;
    }

    public LazyDataModel<Tramite> getLazyWorkingTramites() {
        return this.lazyWorkingTramites;
    }

    public void setLazyWorkingTramites(LazyDataModel<Tramite> lazyModelP) {
        this.lazyWorkingTramites = lazyModelP;
    }

    public LazyDataModel<Tramite> getLazyAssignedTramites() {
        return this.lazyAssignedTramites;
    }

    public void setLazyAssignedTramites(
        LazyDataModel<Tramite> lazyAssignedTramites) {
        this.lazyAssignedTramites = lazyAssignedTramites;
    }

    public Tramite[] getSelectedAssignedTramites() {
        return selectedAssignedTramites;
    }

    public void setSelectedAssignedTramites(Tramite[] selectedAssignedTramites) {
        this.selectedAssignedTramites = selectedAssignedTramites;
    }

    public List<String> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<String> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public List<SEjecutoresTramite> getEjecutoresTramite() {
        return ejecutoresTramite;
    }

    public void setEjecutoresTramite(List<SEjecutoresTramite> ejecutoresTramite) {
        this.ejecutoresTramite = ejecutoresTramite;
    }

    public SEjecutoresTramite getEjecutorSeleccionado() {
        return this.ejecutorSeleccionado;
    }

    public void setEjecutorSeleccionado(SEjecutoresTramite ejecutorSeleccionado) {
        this.ejecutorSeleccionado = ejecutorSeleccionado;
    }

    public String getErrorSeverity() {
        return errorSeverity;
    }

    public void setErrorSeverity(String errorSeverity) {
        this.errorSeverity = errorSeverity;
    }

    public boolean isOtroDoc() {
        return otroDoc;
    }

    public void setOtroDoc(boolean otroDoc) {
        this.otroDoc = otroDoc;
    }

    public boolean isPredioNoSeleccionado() {
        return predioNoSeleccionado;
    }

    public void setPredioNoSeleccionado(boolean predioNoSeleccionado) {
        this.predioNoSeleccionado = predioNoSeleccionado;
    }

    public String getModoDeadquisicion2() {
        return modoDeadquisicion2;
    }

    public void setModoDeadquisicion2(String modoDeadquisicion2) {
        this.modoDeadquisicion2 = modoDeadquisicion2;
    }

    public boolean isGenerarOficioB() {
        return generarOficioB;
    }

    public void setGenerarOficioB(boolean generarOficioB) {
        this.generarOficioB = generarOficioB;
    }

    public List<TramiteDocumentacion> getDocumentosSoli() {
        return documentosSoli;
    }

    public void setDocumentosSoli(List<TramiteDocumentacion> documentosSoli) {
        this.documentosSoli = documentosSoli;
    }

    public boolean isAdicionarDoc() {
        return adicionarDoc;
    }

    public void setAdicionarDoc(boolean adicionarDoc) {
        this.adicionarDoc = adicionarDoc;
    }

    public String getModoDeadquisicion() {
        return modoDeadquisicion;
    }

    public List<SelectItem> getAdq() {
        return adq;
    }

    public void setAdq(List<SelectItem> adq) {
        this.adq = adq;
    }

    public void setModoDeadquisicion(String modoDeadquisicion) {
        this.modoDeadquisicion = modoDeadquisicion;
    }

    public List<Tramite> getTramitesAsociados() {
        return tramitesAsociados;
    }

    public void setTramitesAsociados(List<Tramite> tramitesAsociados) {
        this.tramitesAsociados = tramitesAsociados;
    }

    public List<TramiteDocumentacion> getDocumentosFaltantes() {
        return documentosFaltantes;
    }

    public void setDocumentosFaltantes(
        List<TramiteDocumentacion> documentosFaltantes) {
        this.documentosFaltantes = documentosFaltantes;
    }

    public List<TramiteDocumentacion> getDocumentosAportados() {
        return documentosAportados;
    }

    public void setDocumentosAportados(
        List<TramiteDocumentacion> documentosAportados) {
        this.documentosAportados = documentosAportados;
    }

    public List<TramiteDocumentacion> getDocumentosAdicionalesAportados() {
        return documentosAdicionalesAportados;
    }

    public void setDocumentosAdicionalesAportados(
        List<TramiteDocumentacion> documentosAdicionalesAportados) {
        this.documentosAdicionalesAportados = documentosAdicionalesAportados;
    }

    public List<TramiteDocumentacion> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<TramiteDocumentacion> documentos) {
        this.documentos = documentos;
    }

    public TramiteDocumentacion getTramiteDocumentacion() {
        return tramiteDocumentacion;
    }

    public void setTramiteDocumentacion(
        TramiteDocumentacion tramiteDocumentacion) {
        this.tramiteDocumentacion = tramiteDocumentacion;
    }

    public String getDetalleDocumento() {
        return detalleDocumento;
    }

    public void setDetalleDocumento(String detalleDocumento) {
        this.detalleDocumento = detalleDocumento;
    }

    public TramiteEstado getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(TramiteEstado estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public List<Tramite[]> getTramitesPorAsignar() {
        return tramitesPorAsignar;
    }

    public void setTramitesPorAsignar(List<Tramite[]> tramitesPorAsignar) {
        this.tramitesPorAsignar = tramitesPorAsignar;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("entra al init del AsignacionTramitesMB");

        this.tramitesErrorAvanzar = new ArrayList<Long>();

        // D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "asignacionTramites");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // D: se obtienen los ids de los trámites objetivo a partir del árbol de
        // tareas
        this.idsTramitesActividades = this.tareasPendientesMB
            .obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        // D: con este método se obtiene el código de la territorial. Si es uoc
        // da el código de la uoc
        this.territorialId = MenuMB.getMenu().getUsuarioDto()
            .getCodigoEstructuraOrganizacional();

        this.tramitesPorAsignar = new ArrayList<Tramite[]>();

        this.inicializarVariablesVisorGIS();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * constructor del mb
     *
     * @author pedro.garcia
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public AsignacionTramitesMB() {

        this.lazyWorkingTramites = new LazyDataModel<Tramite>() {

            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();

                populateTramitesLazyly(answer, first, pageSize, sortField, sortOrder.toString(),
                    filters);

                return answer;
            }
        };

        this.lazyAssignedTramites = new LazyDataModel<Tramite>() {

            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();

                populateTramitesConEjecutorLazyly(answer, first, pageSize);

                return answer;
            }
        };
    }

//--------------------------------------------------------------------------------------------------
    /**
     * hace la búsqueda de trámites para asignar ejecutor
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     */
    private void populateTramitesLazyly(List<Tramite> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<Tramite> rows;
        int sizeTotal, sizePagina;

        rows = this.getTramiteService().buscarTramitesParaAsignacionDeEjecutor(
            this.idsTramitesActividades, sortField, sortOrder, filters, first, pageSize);

        sizeTotal = this.getTramiteService().contarTramitesParaAsignacionDeEjecutor(
            this.idsTramitesActividades);
        this.lazyWorkingTramites.setRowCount(sizeTotal);

        sizePagina = rows.size();
        for (int i = 0; i < sizePagina; i++) {
            answer.add(rows.get(i));
        }

    }

//--------------------------------------------------------------------------------------------------
    private void populateTramitesConEjecutorLazyly(List<Tramite> answer,
        int first, int pageSize) {

        List<Tramite> rows;
        int sizeTotal, sizePagina;

        rows = this.getTramiteService().buscarTramitesConEjecutorAsignado(
            this.idsTramitesActividades,
            first, pageSize);

        sizeTotal = this.getTramiteService().contarTramitesConEjecutorAsignado(
            this.idsTramitesActividades);
        this.lazyAssignedTramites.setRowCount(sizeTotal);

        sizePagina = rows.size();
        for (int i = 0; i < sizePagina; i++) {
            answer.add(rows.get(i));
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Action ejecutada al dar click en el botón "Reclasificar"
     *
     * @author pedro.garcia
     * @modified pedro.garcia ya no se valida que los trámites que se van a reclasificar tengan la
     * misma clasificación
     */
    public void reclasificarTramites() {

        String oficina = ETramiteClasificacion.OFICINA.toString();
        String terreno = ETramiteClasificacion.TERRENO.toString();

        for (int k = 0; k < this.selectedTramites.length; k++) {
            //se muestra un mensaje para los tramites de complementacion
            if (this.selectedTramites[k].getTipoTramite().equals(
                ETramiteTipoTramite.COMPLEMENTACION.getCodigo())) {
                this.addMensajeError(
                    "El tràmite " + this.selectedTramites[k].getNumeroRadicacion() +
                    " es de complementación no se permite reclasificar");
                continue;
            }

            if (this.selectedTramites[k].isQuintaOmitido()) {
                Predio p = this.getConservacionService().getPredioPorNumeroPredial(
                    this.selectedTramites[k].getNumeroPredial());
                if (p != null) {
                    if (p.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                        this.addMensajeError("El tràmite " + this.selectedTramites[k].
                            getNumeroRadicacion() +
                            " tiene predios fiscales no se permite reclasificar");
                        continue;
                    }
                }
            }

            if (this.selectedTramites[k].getPredio() != null) {
                if (this.selectedTramites[k].getPredio().getTipoCatastro().equals(
                    EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                    this.addMensajeError("El tràmite " + this.selectedTramites[k].
                        getNumeroRadicacion() + " tiene predios fiscales no se permite reclasificar");
                    continue;
                }
            }

            if (!this.selectedTramites[k].isEsComplementacion() && !this.selectedTramites[k].
                isRevisionAvaluo() && !this.selectedTramites[k].isCuarta()) {

                if (this.selectedTramites[k].getClasificacion().equals("OFICINA")) {
                    selectedTramites[k].setClasificacion(terreno);

                } else if (selectedTramites[k].getClasificacion().equals("TERRENO")) {
                    selectedTramites[k].setClasificacion(oficina);
                }
            }
        }
        this.getTramiteService().actualizarTramites(selectedTramites);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Asignar ejecutor"
     *
     * @modified pedro.garcia no se actualiza estado del trámite
     * @modified 23-02-2012::franz.gamba se agrega la lista de tramites por asignar para manejar los
     * cambios que podrian darse en las cargas de los ejecutores de los tramites
     */
    //@modified javier.aponte Si el trámite es de terreno se hace el llamado para
    // 			que se genere la ficha predial digital y la carta catastral urbana :: 18/01/2016 :: refs#15532

    public void asignarEjecutor() {

        LOGGER.debug("En AsignacionTramitesMB entra en asignarEjecutor");

        if (this.selectedTramites.length > 0) {
            for (int k = 0; k < this.selectedTramites.length; k++) {
                this.selectedTramites[k]
                    .setFuncionarioEjecutor(this.ejecutorSeleccionado
                        .getFuncionarioId());
                this.selectedTramites[k]
                    .setNombreFuncionarioEjecutor(this.ejecutorSeleccionado
                        .getFuncionario());

                //javier.aponte :: refs#15532 :: se hace llamado para generar la ficha predial y la carta catastral
                //cuando el trámite es un trámite de terreno :: 18/01/2016
                if (this.selectedTramites[k].isTramiteTerreno()) {

                    //Se genera ficha predial digital
                    try {
                        boolean fpd = this.getConservacionService().
                            actualizarFichaPredialDigital(this.selectedTramites[k].getId(), usuario,
                                false);
                        if (!fpd) {
                            LOGGER.error("Ocurrió un error al guardar o actualizar la ficha " +
                                "predial digital del trámite con id " + String.valueOf(
                                    this.selectedTramites[k].getId()));
                        }
                    } catch (ExcepcionSNC e) {
                        LOGGER.error("Ocurrió un error al guardar o actualizar la ficha " +
                            "predial digital del trámite con id " + String.valueOf(
                                this.selectedTramites[k].getId()));
                    }

                    //Se genera la carta catastral urbana
                    boolean ccu = this.getConservacionService().
                        actualizarCartaCatastralUrbana(this.selectedTramites[k].getId(),
                            this.usuario, false);
                    if (!ccu) {
                        LOGGER.error("Error en EnvioAAsignacion al guardar o actualizar la carta " +
                            "catastral urbana trámite con id " + this.selectedTramites[k].getId().
                                toString());
                        break;
                    }
                }
            }
            this.getTramiteService().actualizarTramites(this.selectedTramites);

            this.addMensajeInfo("Se asignó el ejecutor al trámite(s)");

            /*
             * busca si en la lista de tramites por asignar ya existia una lista de tramites
             * correspondientes a un ejecutor: de ser el caso le adiciona los tramites
             * seleccionados; si no existe una lista de tramites para este ejecutor, la crea.
             */
            int i = 0;
            boolean ejecutorFlag = false;
            for (Tramite[] listTrams : this.tramitesPorAsignar) {

                ejecutorFlag = listTrams[0].getFuncionarioEjecutor().equals(
                    this.ejecutorSeleccionado.getFuncionarioId());
                if (ejecutorFlag) {
                    this.tramitesPorAsignar.set(i, this.concatTramite(
                        listTrams, this.selectedTramites));
                }
                i++;
            }
            // Si no encontro tramites por asignar con ese ejecutor crea un
            // nuevo registro
            if (!ejecutorFlag) {
                this.tramitesPorAsignar.add(this.selectedTramites);
            }

            this.selectedTramites = null;
        }

        LOGGER.debug("Se termino de asignar ejecutor al tramite");
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @modified pedro.garcia
     * @deprecated porque ya no se va a una pantalla intermedia sino que la reclasificación se hace
     * directamente como action del botón en la pantalla principal
     */
    @Deprecated
    public void selReclasificar() {
        LOGGER.debug("En AsignacionTramitesMB entra en seleccionadosReclasificar");

        String oficina = ETramiteClasificacion.OFICINA.toString();
        String terreno = ETramiteClasificacion.TERRENO.toString();

        for (int k = 0; k < this.selectedTramites.length; k++) {
            if (this.selectedTramites[k].getClasificacion().equals("OFICINA")) {
                selectedTramites[k].setClasificacion(terreno);

            } else if (selectedTramites[k].getClasificacion().equals("TERRENO")) {
                selectedTramites[k].setClasificacion(oficina);
            }
        }
        this.getTramiteService().actualizarTramites(selectedTramites);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Action ejecutada al dar click en el botón "Asignar ejecutor" Valida que todos los trámites
     * seleccionados de la tabla tengan la misma clasificación (oficina, terreno)
     *
     * @modified pedro.garcia
     * @modified franz.gamba -> se realiza la consulta de los ejecutores en este punto para no
     * sobrecargar el inicio de la pagina
     */
    public void initAsignacionEjecutorTramites() {

        String clasificacionTramite, clasificacionTramiteTemp;
        boolean okToAsignarEjecutor;

        clasificacionTramite = this.getSelectedTramites()[0].getClasificacion();
        okToAsignarEjecutor = true;
        for (Tramite selectedTramite : this.getSelectedTramites()) {
            clasificacionTramiteTemp = selectedTramite.getClasificacion();
            if (clasificacionTramiteTemp != null &&
                 clasificacionTramite != null) {
                if (!clasificacionTramiteTemp
                    .equalsIgnoreCase(clasificacionTramite)) {
                    this.addMensajeError(
                        "Los trámites a los que se le va a asignar ejecutor deben tener la misma clasificación!");
                    okToAsignarEjecutor = false;
                    break;
                }
            }
        }

        if (!okToAsignarEjecutor) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
            return;
        }

        loadEjecutoresTramites();
    }

//--------------------------------------------------------------------------------------------------
    public void handleDialogClose(CloseEvent event) {
        LOGGER.debug("Cierra modal de asignación de ejecutor o de reclasificación");

        // OJO: D: hay que poner el seleccionado en null para que prime no se
        // muera.
        // Tal vez sucede porque el seleccionado fue modificado en el modal ya
        // no sería devuelto
        // como resultado de la consulta
        this.selectedTramites = null;
        this.ejecutorSeleccionado = null;

        /** Variable para cancelar tramites */
        // this.selectedTramitesCancelar = null;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Acción ejecutada al dar click en el botón "Desasignar ejecutor"
     *
     * @modified pedro.garcia no se actualiza estado del trámite
     * @modified 23-02-2012::franz.gamba se agrega la funcionalidad para eliminar los tramites a
     * desasignar de la lista de tramites por asignar
     */
    public void desasignarEjecutor() {

        for (int k = 0; k < this.selectedAssignedTramites.length; k++) {
            this.selectedAssignedTramites[k].setFuncionarioEjecutor("");
            this.selectedAssignedTramites[k].setNombreFuncionarioEjecutor("");
        }

        try {
            this.getTramiteService().actualizarTramites(this.selectedAssignedTramites);
            this.addMensajeInfo("Se desasignó el ejecutor del trámite(s)");
        } catch (Exception ex) {
            LOGGER.error(
                "Ocurrió una excepción actualizando los trámites, desasignando ejecutor: " +
                ex.getMessage());
        }

        /*
         * remueve de la lista de tramites por asignar, los tramites selecionados
         */
        for (Tramite tramiteAsignado : this.selectedAssignedTramites) {
            tramitesPorAsignar =
                eliminarTramitesPorAsignar(tramiteAsignado, this.tramitesPorAsignar);

            for (SEjecutoresTramite set : this.ejecutoresTramite) {
                if (tramiteAsignado.getFuncionarioEjecutor().equals(set.getFuncionarioId())) {
                    Long t;
                    if (tramiteAsignado.isTramiteTerreno()) {
                        t = set.getTerreno() - 1;
                        set.setTerreno(t);
                    } else if (tramiteAsignado.isTramiteOficina()) {
                        t = set.getOficina() - 1;
                        set.setOficina(t);
                    }
                }
            }
        }

    }

    /**
     * Método para eliminar los tramites del listado de tramites por asignar
     *
     * @param t
     * @param tramites
     * @return
     */
    private List<Tramite[]> eliminarTramitesPorAsignar(Tramite t, List<Tramite[]> tramites) {
        int posA = 0;
        for (Tramite[] fila : tramites) {

            int pos = -1;
            for (int i = 0; i < fila.length; i++) {
                if (fila[i].getId().equals(t.getId())) {
                    pos = i;
                    break;
                }
            }
            if (pos != -1) {
                Tramite nuevaFila[] = eliminarTramiteDeArreglo(fila[pos], fila);
                tramites.set(posA, nuevaFila);
            }
            posA++;
        }
        return tramites;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Obtiene los tramites asociados a un tramite
     */
    @SuppressWarnings("unused")
    private void obtenerTramitesAsociados(Tramite tramite) {

        if (tramite != null) {
            if (tramite.getTramite() != null) {
                tramitesAsociados.add(tramite);
                obtenerTramitesAsociados(tramite.getTramite());
            }
        }
    }

    // ----------------- métodos de lo de procesos
    // --------------------------------------------------
    /**
     * se valida que haya al menos un trámite al que se le deba mover el proceso
     *
     * @see ProcessFlowManager#validateProcess()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public boolean validateProcess() {

        boolean answer = true;

        try {

            this.tramitesConEjecutorAsignado = this.getTramiteService().
                findIdsClasificacionTramitesConEjecutor(this.idsTramitesActividades);

            if (this.tramitesConEjecutorAsignado == null ||
                 this.tramitesConEjecutorAsignado.isEmpty()) {
                answer = false;
            }
        } catch (Exception e) {
            LOGGER.error(
                "error consultando trámites con ejecutor asignado en aAsignacionTramitesMB#validateProcess : " +
                 e.getMessage());
            answer = false;
        }

        return answer;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * pre: la lista con los trámites con ejecutor asignado (los que se van a mover) se inicializa
     * en validateProcess()
     *
     * OJO: la actividad del process no se debe definir en el init, porque esta actividad es masiva
     * y se requiere hallar la actividad por cada uno de los trámites involucrados
     *
     * @see ProcessFlowManager#setupProcessMessage()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void setupProcessMessage() {
        Actividad activity;
        ActivityMessageDTO messageDTO, messageDTODefault;
        String observaciones = "", processTransitionTerreno, processTransitionOficina;
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuariosActividad;
        UsuarioDTO usuarioObjetivoActividad;
        List<ActivityMessageDTO> processMessages;
        UsuarioDTO responsableConservacion;

        processTransitionTerreno = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
        processTransitionOficina = ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO;

        // D: las observaciones que se dan en el template generalProcessCU se
        // setean en el atributo
        // 'message' del ProcessFlowManager. En el caso de actividades masivas
        // este mensaje no se usa,
        // pero hay que sacar el texto de observaciones de alli para colocarlo
        // en cada mensaje de la lista
        if (this.getMessage() != null) {
            messageDTODefault = this.getMessage();
            observaciones = messageDTODefault.getComment();
        }

        processMessages = new ArrayList<ActivityMessageDTO>();
        for (TramiteConEjecutorAsignadoDTO currentTramite : this.tramitesConEjecutorAsignado) {
            messageDTO = new ActivityMessageDTO();
            usuarioObjetivoActividad = new UsuarioDTO();
            solicitudCatastral = new SolicitudCatastral();
            usuariosActividad = new ArrayList<UsuarioDTO>();

            //felipe.cadena::15-09-2016::#19450::Reclamar actividad por parte del
            //usuario autenticado no se permite avanzar si la actividad ya esta eclamada por otro usuario
            if (!this.reclamarActividad(currentTramite.getId())) {
                continue;
            }

            activity = this.tareasPendientesMB.buscarActividadPorIdTramite(currentTramite.getId());
            messageDTO.setComment(observaciones);
            messageDTO.setActivityId(activity.getId());

            // D: según alejandro.sanchez la transición que importa es la que se define en la
            // solicitud catastral; sin embargo, por si acaso se hace también en el messageDTO
            if (currentTramite.getClasificacion().equalsIgnoreCase(
                ETramiteClasificacion.TERRENO.toString())) {
                messageDTO.setTransition(processTransitionTerreno);
                solicitudCatastral.setTransicion(processTransitionTerreno);

                if (this.usuario.getRoles() != null) {

                    for (String rol : this.usuario.getRoles()) {
                        if (ERol.COORDINADOR.getRol().equalsIgnoreCase(rol)) {

                            Tramite tramite = this.getTramiteService()
                                .buscarTramitePorId(currentTramite.getId());

                            if (tramite != null &&
                                 tramite.getPredio() != null &&
                                 tramite.getPredio().getMunicipio() != null &&
                                 tramite.getPredio().getMunicipio()
                                    .getCodigo() != null) {

                                String estructuraOrg = this
                                    .getGeneralesService()
                                    .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                        tramite.getPredio()
                                            .getMunicipio()
                                            .getCodigo());

                                List<UsuarioDTO> responsablesConservacion = this
                                    .getTramiteService()
                                    .buscarFuncionariosPorRolYTerritorial(
                                        estructuraOrg,
                                        ERol.RESPONSABLE_CONSERVACION);

                                for (UsuarioDTO usuarioDTO : responsablesConservacion) {

                                    if (ELDAPEstadoUsuario.ACTIVO
                                        .codeExist(usuarioDTO.getEstado())) {

                                        responsableConservacion = usuarioDTO;
                                        this.usuario = responsableConservacion;

                                    }
                                }

                            }

                        }
                    }
                }

                usuarioObjetivoActividad = this.usuario;
            } else if (currentTramite.getClasificacion().equalsIgnoreCase(
                ETramiteClasificacion.OFICINA.toString())) {
                messageDTO.setTransition(processTransitionOficina);
                solicitudCatastral.setTransicion(processTransitionOficina);
                usuarioObjetivoActividad = this.getGeneralesService().getCacheUsuario(
                    currentTramite.getFuncionarioEjecutor());
            }

            usuariosActividad.add(usuarioObjetivoActividad);
            solicitudCatastral.setUsuarios(usuariosActividad);
            messageDTO.setUsuarioActual(this.usuario);
            //solicitudCatastral.setNumeroRadicacion(currentTramite.getNumeroRadicacion());
            messageDTO.setSolicitudCatastral(solicitudCatastral);

            processMessages.add(messageDTO);
        }
        //felipe.cadena::01-10-2016::#19450::Se envia correo si existen tramites con error en raclamar la actividad
        this.enviarCorreoErrorAvance();

        if (!processMessages.isEmpty()) {
            this.setMasiveMessages(processMessages);
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see ProcessFlowManager#doDatabaseStatesUpdate()
     *
     * Lo único que hay que hacer aquí es crear entradas para la tabla TRAMITE_ESTADO para los
     * trámites que finalmente quedaron con ejecutor asignado, ya que el estado de los trámites se
     * ha ido modificando con las acciones del usuario
     *
     * @author pedro.garcia
     *
     * OJO: ahora no hay que hacer nada porque el estado del trámite no cambia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que genera la gráfica de torta de los estados de los trámites de Terreno
     *
     * @author franz.gamba
     * @return
     */
    /*
     * @modified pedro.garcia 25-10-2013 eliminación de parámetro no usado. Ortografía
     */
    private PieDataset createDatasetT(TablaContingencia t) {

        DefaultPieDataset dataset = new DefaultPieDataset();
        boolean existenTramites = false;
        try {
            if (t != null) {
                existenTramites = true;
            }
            if (existenTramites) {
                Set<String> tuplas = t.getValores().keySet();
                for (String tupla : tuplas) {
                    String[] datos = tupla.split(TablaContingencia.SEPARADOR);
                    if (datos[1].equals(ETramiteClasificacion.TERRENO.toString())) {
                        if (dataset.getKeys().contains(datos[0])) {
                            Double a = dataset.getValue(datos[0])
                                .doubleValue();
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue() + a));
                            LOGGER.debug("***** VALOR : " + a);
                        } else {
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue()));
                        }
                    }
                }
            }
            /*
             * consulta si en los tramites por asignar existen tramites de terreno y de ser el caso
             * los agrega en la grafica con nombre de actividad: RevisarTramiteAsignado
             */
            if (this.tramitesPorAsignar.size() > 0) {
                for (Tramite[] tramites : this.tramitesPorAsignar) {
                    // consulta primero si existe una lista de tramites por
                    // asignar
                    // para
                    // ese ejecutor
                    if (tramites.length > 0) {
                        if (tramites[0].getFuncionarioEjecutor().equals(
                            this.ejecutorSeleccionado.getFuncionarioId())) {

                            existenTramites = true;
                            for (Tramite tram : tramites) {
                                // pregunta si el tramite es de Terreno
                                if (tram.isTramiteTerreno()) {
                                    if (dataset
                                        .getKeys()
                                        .contains(
                                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
                                        Double a = dataset
                                            .getValue(
                                                ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO).
                                            doubleValue();
                                        dataset.setValue(
                                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
                                            new Double(a + 1));
                                        LOGGER.debug("***** VALOR : " + a);
                                    } else {
                                        dataset.setValue(
                                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
                                            new Double(1));
                                    }
                                }
                            }

                        }
                    }
                }
            }
            if (!existenTramites) {
                dataset.setValue("No existen trámites de este tipo",
                    new Double(0));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dataset;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que genera la gráfica de Torta de los estados de los trámites de Oficina
     *
     * @author franz.gamba
     * @return
     */
    /*
     * @modified pedro.garcia 25-10-2013 eliminación de parámetro no usado. Ortografía
     */
    private PieDataset createDatasetO(TablaContingencia t) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        boolean existenTramites = false;
        try {
            if (t != null) {
                existenTramites = true;
            }
            if (existenTramites) {
                Set<String> tuplas = t.getValores().keySet();
                for (String tupla : tuplas) {
                    String[] datos = tupla.split(TablaContingencia.SEPARADOR);
                    if (datos[1].equals(ETramiteClasificacion.OFICINA.toString())) {
                        if (dataset.getKeys().contains(datos[0])) {
                            Double a = dataset.getValue(datos[0])
                                .doubleValue();
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue() + a));
                            LOGGER.debug("***** VALOR : " + a);
                        } else {
                            dataset.setValue(datos[0], new Double(t
                                .getValor(tupla).doubleValue()));
                        }
                    }
                }
            }
            /*
             * consulta si en los tramites por asignar existen tramites de oficina y de ser el caso
             * los agrega en la grafica con nombre de actividad: RevisarTramiteAsignado
             */
            for (Tramite[] tramites : this.tramitesPorAsignar) {
                // consulta primero si existe una lista de tramites por asignar
                // para
                // ese ejecutor
                if (tramites[0] != null) {
                    if (tramites[0].getFuncionarioEjecutor().equals(
                        this.ejecutorSeleccionado.getFuncionarioId())) {

                        existenTramites = true;
                        for (Tramite tram : tramites) {
                            // pregunta si el tramite es de Oficina
                            if (tram.isTramiteOficina()) {
                                if (dataset
                                    .getKeys()
                                    .contains(
                                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO)) {
                                    Double a = dataset
                                        .getValue(
                                            ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO).
                                        doubleValue();
                                    dataset.setValue(
                                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
                                        new Double(a + 1));
                                    LOGGER.debug("***** VALOR : " + a);
                                } else {
                                    dataset.setValue(
                                        ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO,
                                        new Double(1));
                                }
                            }
                        }

                    }
                }
            }
            if (!existenTramites) {
                dataset.setValue("No existen trámites de este tipo",
                    new Double(0));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return dataset;

    }
//-----------------------------------------------------------------------------------------------

    /**
     * Método que retorna los trámites asignados al ejecutor seleccionado
     *
     * @author franz.gamba
     */
    public void consultarTramitesAsignadosAEjecutor() {

        LOGGER.debug("En AsignacionTramitesMB entra en consultarTramitesAsignadosAEjecutor");
        String nombreFileT;
        String nombreFileO;
        PiePlot plotO;
        PiePlot plotT;

        if (this.ejecutorSeleccionado == null) {
            this.addMensajeError("Debe seleccionar un ejecutor!");
            return;
        }

        Pair<List<Tramite>, Map<Long, String>> datosActividades = this.
            consultarTramitesEjecutorActividades();
        this.assignedTramitesAEjecutor = datosActividades.getFirst();
        this.tablaActividades = datosActividades.getSecond();
        //Construir tabla de contingencia 
        TablaContingencia t = new TablaContingencia();
        Map<Long, String> tablaTramites = new HashMap<Long, String>();

        //Obtener un mapa con id y clasificación de trámite (terreno u oficina)
        for (Tramite tr : this.assignedTramitesAEjecutor) {
            tablaTramites.put(tr.getId(), tr.getClasificacion());
        }
        String clasificacion;
        for (Long id : this.tablaActividades.keySet()) {
            clasificacion = tablaTramites.get(id);
            t.addValor(this.tablaActividades.get(id), clasificacion != null ? clasificacion :
                "No clasificado");
        }
        try {
            //Se formatea el label del grafico
            PieSectionLabelGenerator generator = new StandardPieSectionLabelGenerator(
                "{1} ({2})", new DecimalFormat("0"), new DecimalFormat("0.00%"));

            JFreeChart jfreechartO = ChartFactory.createPieChart(
                "Trámites de oficina", createDatasetO(t), true, true, false);
            plotO = (PiePlot) jfreechartO.getPlot();
            plotO.setLabelGenerator(generator);

            nombreFileO = "dynamichartO" + this.fileG;

            File chartFileO = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(), nombreFileO);
            LOGGER.debug("Archivo a crear " + chartFileO.getAbsolutePath());
            ChartUtilities.saveChartAsPNG(chartFileO, jfreechartO, 375, 300);
            this.chartOficina = new DefaultStreamedContent(new FileInputStream(
                chartFileO), "image/png");

            JFreeChart jfreechartT = ChartFactory.createPieChart(
                "Trámites de terreno", createDatasetT(t), true, true, false);

            plotT = (PiePlot) jfreechartT.getPlot();
            plotT.setLabelGenerator(generator);

            nombreFileT = "dynamichartT" + this.fileG;
            File chartFileT = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(), nombreFileT);
            ChartUtilities.saveChartAsPNG(chartFileT, jfreechartT, 375, 300);

            this.chartTerreno = new DefaultStreamedContent(new FileInputStream(
                chartFileT), "image/png");
            this.fileG++;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        LOGGER.debug("*****NUMERO DE CUENTA= " +
             this.assignedTramitesAEjecutor.size() + " *****");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * método action del botón "Sí" en el confirmDialog del botón "Solicitar documentos requeridos
     * adicionales" en la pantalla detallesTramiteEnAsignacion
     *
     * 1. desasigna ejecutor del trámite 2. mueve el proceso. OJO: caso especial. porque es mover un
     * trámite individual en una actividad masiva
     *
     * @author pedro.garcia
     * @modified felipe.cadena 25-06-2013 -- Se actualizan los tramites para el proceso y se cambia
     * el retorno para que se pueda ir al arbol de tareas cuando no quedan tràmites
     */
    public String solicitarDocsParaTramite() {

        // D: 1.
        this.selectedTramiteForDetailsChecking.setFuncionarioEjecutor("");
        this.selectedTramiteForDetailsChecking.setNombreFuncionarioEjecutor("");

        // D: 2. se simula el flujo que se hace para avanzar proceso: validar,
        // set mensaje, hacer lo
        // de base de datos y moverlo.
        // No hay nada que validar
        // Set mensaje
        Actividad activity;
        ActivityMessageDTO messageDTO;
        String observaciones, processTransition;
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        solicitudCatastral = new SolicitudCatastral();

        usuariosActividad = new ArrayList<UsuarioDTO>();
        usuarioDestinoActividad = new UsuarioDTO();
        messageDTO = new ActivityMessageDTO();

        activity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.selectedTramiteForDetailsChecking.getId());
        messageDTO.setActivityId(activity.getId());
        processTransition = ProcesoDeConservacion.ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE;
        observaciones = "El trámite se devuelve a solicitud de documentos.";
        messageDTO.setComment(observaciones);
        // D: según alejandro.sanchez la transición que importa es la que se
        // define en la
        // solicitud catastral; sin embargo, por si acaso se hace también en el
        // messageDTO
        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);

        // D: se manda como usuario destino el que esta loggeado
        usuarioDestinoActividad.copiarEstructuraOrganizacional(this.usuario);
        usuarioDestinoActividad.setLogin(this.usuario.getLogin());
        usuariosActividad.add(usuarioDestinoActividad);
        solicitudCatastral.setUsuarios(usuariosActividad);

        messageDTO.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(messageDTO);

        // do data base update
        // D: actualiza el trámite
        try {
            this.getTramiteService()
                .actualizarTramite(this.selectedTramiteForDetailsChecking, this.usuario);
        } catch (Exception e) {
            LOGGER.error("error en AsignacionTramitesMB#solicitarDocsParaTramite: " +
                 e.getMessage());
        }

        // forward process
        try {
            //felipe.cadena::15-09-2016::#19450::Reclamar actividad por parte del usuario autenticado        
            if (!this.reclamarActividad(selectedTramiteForDetailsChecking.getId())) {
                this.tareasPendientesMB.cargarInit();
                this.init();
                return null;
            }

            this.getProcesosService().avanzarActividad(
                this.getMessage().getActivityId(),
                this.getMessage().getSolicitudCatastral());
        } catch (Exception e) {
            LOGGER.error(
                "error avanzando actividad en AsignacionTramitesMB#solicitarDocsParaTramite: " +
                 e.getMessage());
        }

        int counter = 0;
        for (long idTramite : this.idsTramitesActividades) {
            if (idTramite == this.selectedTramiteForDetailsChecking.getId()) {
                this.idsTramitesActividades[counter] = -1l;
            }
        }

        // Se actualizan los tramites pendientes en el process y se recarga el
        // init() del MB para que se actualice la tabla. si no hay tramites
        // pendientes se retorna al arbol de tareas.
        Actividades actividades = (Actividades) this.tareasPendientesMB.
            getNodoSeleccionadoInfo().getObjetoProceso().getNodo();
        List<Actividad> listaActividades = this.getProcesosService().
            consultarListaActividades(usuario, actividades.getRutaActividad().toString());
        if (listaActividades == null || listaActividades.isEmpty()) {
            UtilidadesWeb.removerManagedBean("asignacionTramites");
            this.tareasPendientesMB.init();
            return ConstantesNavegacionWeb.INDEX;
        }
        this.tareasPendientesMB.setListaInstanciasActividadesSeleccionadas(listaActividades);

        this.init();
        return " ";

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que busca dentro de los ejecutores seleccionados su correspondiente en UsuarioDTO
     *
     * @author franz.gamba
     * @param ejecutorSeleccionado
     */
    public UsuarioDTO usuarioCorrespondienteSeleccionado(
        SEjecutoresTramite ejecutor) {

        for (UsuarioDTO u : this.ejecutoresTramiteDTO) {
            if (u.getLogin().equals(ejecutor.getFuncionario())) {
                return u;
            }
        }

        return null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que toma los tramites asignados a un ejecutor segun las actividades que le
     * corresponden
     *
     * @author franz.gamba
     * @param ejecutorSeleccionado
     */
    private Pair<List<Tramite>, Map<Long, String>> consultarTramitesEjecutorActividades() {
        UsuarioDTO usuario_1 = this.getGeneralesService().getCacheUsuario(this.ejecutorSeleccionado
            .getFuncionarioId());
        /**
         * TODO Es posible que aquí toque especificar el rol ejecutor.
         */
        List<Actividad> actividadesUsuario = this.getProcesosService()
            .consultarListaActividades(usuario_1);

        Map<Long, String> tablaActividades_1 = new HashMap<Long, String>();
        List<Tramite> tramitesDeEjecutor = new ArrayList<Tramite>();
        List<Long> tramiteIds = new ArrayList<Long>();
        if (actividadesUsuario != null && !actividadesUsuario.isEmpty()) {
            for (Actividad act : actividadesUsuario) {
                tablaActividades_1.put(act.getIdObjetoNegocio(), act.getRutaActividad().
                    getActividad());
            }
            tramiteIds.addAll(tablaActividades_1.keySet());
            tramitesDeEjecutor = this.getTramiteService()
                .obtenerTramitesPorIds(tramiteIds);
        }
        return new Pair<List<Tramite>, Map<Long, String>>(tramitesDeEjecutor, tablaActividades_1);
    }

    /**
     * Metodo para concatenar arreglos de tramites
     *
     * @param a
     * @param b
     * @return
     */
    public Tramite[] concatTramite(Tramite[] a, Tramite[] b) {
        Tramite[] c = new Tramite[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }

    /**
     * Metodo que elimina un Tramite de un arreglo de tramites, generando un nuevo arreglo de
     * tamanio n-1
     *
     * @param tram
     * @param a
     * @return
     */
    public Tramite[] eliminarTramiteDeArreglo(Tramite tram, Tramite[] a) {
        Tramite[] r = new Tramite[a.length - 1];
        for (int i = 0, j = 0; i < a.length; i++) {
            if (!tram.getId().equals(a[i].getId())) {
                r[j++] = a[i];
            }
        }

        return r;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Carga los ejecutores tramite de la territorial con el numero de tramites que tiene según su
     * clasificación
     *
     * @author franz.gamba
     */
    private void loadEjecutoresTramites() {

        LOGGER.debug("En AsignacionTramitesMB entra a createCartesianModel");
        if (this.usuario != null) {
            if (this.usuario.getDescripcionUOC() != null &&
                 !this.usuario.getDescripcionUOC().equals("")) {
                this.ejecutoresTramite = this
                    .getTramiteService()
                    .buscarFuncionariosTerritorialConClasificacionTramite(
                        this.usuario
                            .getDescripcionUOC());
            } else {
                this.ejecutoresTramite = this
                    .getTramiteService()
                    .buscarFuncionariosTerritorialConClasificacionTramite(
                        this.usuario
                            .getDescripcionEstructuraOrganizacional());
            }

            //Si se han adicionado tramites a algun ejecutor en la pantalla de asignacionTramites
            if (this.ejecutoresTramite != null) {
                for (SEjecutoresTramite et : this.ejecutoresTramite) {

                    if (this.tramitesPorAsignar.size() > 0) {
                        for (Tramite[] tramites : this.tramitesPorAsignar) {
                            // consulta primero si existe una lista de tramites por
                            // asignar para ese ejecutor
                            if (tramites.length > 0) {
                                if (tramites[0].getFuncionarioEjecutor().equals(et.
                                    getFuncionarioId())) {

                                    for (Tramite tram : tramites) {
                                        // pregunta si el tramite es de Terreno
                                        Long t;
                                        if (tram.isTramiteTerreno()) {
                                            t = et.getTerreno() + 1;
                                            et.setTerreno(t);
                                        } else if (tram.isTramiteOficina()) {
                                            t = et.getOficina() + 1;
                                            et.setOficina(t);
                                        }
                                    }

                                }
                            }
                        }
                    }

                }
            }

//			 this.getTramiteService()
//			 .buscarFuncionariosEjecutoresPorTerritorial(this.usuario
//			 .getDescripcionEstructuraOrganizacional());
        }

        List<UsuarioDTO> ejecutores = this.getTramiteService()
            .buscarFuncionariosEjecutoresPorTerritorialDTO(this.usuario
                .getDescripcionEstructuraOrganizacional());

        //felipe.cadena::#15864::01-03-2016::Se valida que no esten disponibles
        //para asignacion los usuarios expirados o inactivos
        this.ejecutoresTramiteDTO = new ArrayList<UsuarioDTO>();

        for (UsuarioDTO usuarioDTO : ejecutores) {
            if (!usuarioDTO.getFechaExpiracion().before(new Date()) &&
                 ELDAPEstadoUsuario.ACTIVO.codeExist(usuarioDTO.getEstado())) {
                this.ejecutoresTramiteDTO.add(usuarioDTO);
            }
        }

    }
//----------------------	

    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Obtiene los predios de la seleccion de la tabla para hacerles zoom en el visor GIS
     *
     * @author lorena.salamanca
     * @param event
     */
    public void obtenerPrediosParaZoom(SelectEvent event) {
        String codigo;
        this.prediosParaZoom = "";
        for (int k = 0; k < this.selectedTramites.length; k++) {
            if (this.selectedTramites[k].isQuinta()) {
                codigo = this.selectedTramites[k].getNumeroPredial();
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }
            if (this.selectedTramites[k].isSegundaEnglobe()) {
                List<Long> idTramites = new LinkedList<Long>();
                idTramites.add(this.selectedTramites[k].getId());
                List<String> codigosSegunda;
                codigosSegunda = this.getTramiteService().getNumerosPredialesByTramiteId(idTramites);
                codigo = codigosSegunda.toString().replace("[", "");
                codigo = codigo.replace("]", "");
                codigo = codigo.replace(" ", "");
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }

            if (this.selectedTramites[k].isSegundaEnglobe() == false && this.selectedTramites[k].
                isQuinta() == false) {
                codigo = this.selectedTramites[k].getPredio().getNumeroPredial();
                if (!contienePredio(codigo)) {
                    if (!this.prediosParaZoom.isEmpty()) {
                        this.prediosParaZoom = this.prediosParaZoom.concat(",");
                    }
                    this.prediosParaZoom = this.prediosParaZoom.concat(codigo);
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    private boolean contienePredio(String codigo) {
        return this.prediosParaZoom.contains(codigo);
    }
//--------------------------------------------------------------------------------------------------	

    /**
     *
     * Retira los predios de la seleccion de la tabla para retirarlos del zoom del visor GIS
     *
     * @author lorena.salamanca
     * @param event
     */
    public void eliminarPrediosParaZoom(UnselectEvent event) {
        for (int k = 0; k < this.selectedTramites.length; k++) {
            String codigo;
            codigo = this.selectedTramites[k].getNumeroPredial();
            this.prediosParaZoom = this.prediosParaZoom.replace(codigo, "");
            this.prediosParaZoom = this.prediosParaZoom.replace(",,", ",");

            if (this.prediosParaZoom.charAt(this.prediosParaZoom.length() - 1) == ',') {
                this.prediosParaZoom = this.prediosParaZoom.substring(0,
                    this.prediosParaZoom.length() - 1);
            }
            if (this.prediosParaZoom.charAt(0) == ',') {
                this.prediosParaZoom = this.prediosParaZoom.substring(1,
                    this.prediosParaZoom.length());
            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Action ejecutado al dar click en el botón 'Consultar carga'
     *
     * Método que realiza la consulta de los ejecutores e inicializa las variables para visualizar
     * la carga de trámites que tiene cada uno.
     *
     * @author david.cifuentes
     */
    public void consultarCarga() {
        loadEjecutoresTramites();
        this.ejecutorSeleccionado = null;
        this.visualizarPanelTramitesBool = false;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para el manejo de la carga y visualización del panel los trámites asignados a un
     * ejecutor seleccionado.
     *
     * @author david.cifuentes
     *
     * @param ev
     */
    public void ejecutorSelectListener(SelectEvent ev) {
        // Consultar los trámites asignados al ejecutor
        this.consultarTramitesAsignadosAEjecutor();

        // Consultar las actividades de los trámites resultado y realizar el set
        // de las mismas en el campo actividadActualTramite
        // this.consultarActividadesDeTramitesDelEjecutorAsignado();
        this.visualizarPanelTramitesBool = true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método ejecutado al deseleccionar un ejecutor, se realiza el llamado a método que reinicia
     * las variables asociadas a la carga de los trámites de un ejecutor.
     *
     * @author david.cifuentes
     *
     * @param ev
     */
    public void ejecutorsUnselectListener(UnselectEvent ev) {
        this.limpiarEjecutor();
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que reinicia las variables asociadas a la carga de los trámites de un ejecutor.
     *
     * @author david.cifuentes
     *
     * @param ev
     */
    public void limpiarEjecutor() {
        this.ejecutorSeleccionado = null;
        this.visualizarPanelTramitesBool = false;
    }

    /**
     * Método que realiza el set de las actividades de los trámites de un ejecutor.
     *
     * @author david.cifuentes
     */
    public void consultarActividadesDeTramitesDelEjecutorAsignado() {
        // Consultar las actividades de los trámites.
        try {
            if (this.assignedTramitesAEjecutor != null &&
                 !this.assignedTramitesAEjecutor.isEmpty()) {

                List<Tramite> tramitesConActividad = this.getTramiteService()
                    .consultarActividadesDeTramites(
                        this.assignedTramitesAEjecutor);
                if (tramitesConActividad != null &&
                     !tramitesConActividad.isEmpty()) {
                    this.assignedTramitesAEjecutor = tramitesConActividad;
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error("ERROR: asignacionTramites#consultarActividadesDeTramitesDelEjecutor");
            this.addMensajeError(
                "Error en la consulta de las actividades de los trámites del ejecutor.");
        }
    }

    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author felipe.cadena
     */
    private boolean reclamarActividad(Long tramiteReclamar) {
        Actividad actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        List<Actividad> actividades;

        if (actividad == null) {
            actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (actividades != null && !actividades.isEmpty()) {
                for (Actividad actTemp : actividades) {
                    if (actTemp.getIdObjetoNegocio() == tramiteReclamar) {
                        actividad = actTemp;
                        break;
                    }
                }
            }
        }

        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.usuario.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    this.tramitesErrorAvanzar.add(tramiteReclamar);
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.usuario);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                this.tramitesErrorAvanzar.add(tramiteReclamar);
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }

    }

    /**
     * Metodo para enviar correo a usuario cuando se presenta error en el avanzae por que la
     * actividad ya fue reclamada.
     *
     * @author felipe.cadena
     */
    private void enviarCorreoErrorAvance() {

        if (!this.tramitesErrorAvanzar.isEmpty()) {

            StringBuilder radicados = new StringBuilder();
            StringBuilder solicitudes = new StringBuilder();
            String correoDestino = this.usuario.getEmail();
            String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_TRAMITES_RECLAMADOS;

            if (correoDestino == null || correoDestino.isEmpty()) {
                return;
            }

            //Parametros del correo
            String[] parametrosPlantillaContenido = new String[5];
            parametrosPlantillaContenido[0] = this.tareasPendientesMB.
                getListaInstanciasActividadesSeleccionadas().get(0).getNombre();

            List<Tramite> tramitesError = this.getTramiteService().
                buscarTramitesConEjecutorAsignado(idsTramitesActividades);

            for (Tramite tramite : tramitesError) {
                if (tramite.getNumeroRadicacion() != null && !tramite.getNumeroRadicacion().
                    isEmpty()) {
                    radicados.append(tramite.getNumeroRadicacion()).append(" <br/>");
                } else {
                    solicitudes.append(tramite.getSolicitud().getNumero()).append(" <br/>");
                }
            }

            if (!radicados.toString().isEmpty()) {
                parametrosPlantillaContenido[1] = Constantes.TEXTO_OTROS_TRAMITES_RECLAMADOS;
                parametrosPlantillaContenido[2] = radicados.toString();
            } else {
                parametrosPlantillaContenido[1] = "";
                parametrosPlantillaContenido[2] = "";
            }

            if (!solicitudes.toString().isEmpty()) {
                parametrosPlantillaContenido[3] =
                    Constantes.TEXTO_AUTOESTIMACION_TRAMITES_RECLAMADOS;
                parametrosPlantillaContenido[4] = solicitudes.toString();
            } else {
                parametrosPlantillaContenido[3] = "";
                parametrosPlantillaContenido[4] = "";
            }

            // Consulta de la plantilla del contenido.
            Plantilla plantillaContenido = this.getGeneralesService().recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_TRAMITES_AVANZADOS.getCodigo());

            // Consulta del contenido del correo.
            String contenidoCorreo = plantillaContenido.getHtml();

            if (contenidoCorreo != null) {

                // Se instancia el messageFormat.
                Locale colombia = new Locale("ES", "es_CO");
                MessageFormat messageFormat = new MessageFormat(
                    contenidoCorreo, colombia);

                // Se reemplazan los parametros en el contenido de la
                // plantilla.
                contenidoCorreo = messageFormat
                    .format(parametrosPlantillaContenido);

            }

            //format asunto
            Locale colombia = new Locale("ES", "es_CO");
            MessageFormat messageFormat = new MessageFormat(
                asunto, colombia);

            asunto = messageFormat
                .format(parametrosPlantillaContenido);

            // Envio de correo electronico
            this.getGeneralesService().enviarCorreo(correoDestino, asunto,
                contenidoCorreo, null, null);

        }

    }

// end of class
}
