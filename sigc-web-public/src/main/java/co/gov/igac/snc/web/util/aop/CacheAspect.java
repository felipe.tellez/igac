package co.gov.igac.snc.web.util.aop;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Ejemplo de Aspecto de AOP para manejo de Cache utilizando ehcache
 *
 * http://www.packtpub.com/article/design-with-spring-aop
 *
 * @author jcmendez
 *
 */
public class CacheAspect {

    private static final Logger logger = LoggerFactory
        .getLogger(CacheAspect.class);

    private Cache cache;

    /**
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    public Object cacheObject(ProceedingJoinPoint pjp) throws Throwable {
        logger.debug("cacheObject");

        Object result;
        String cacheKey = getCacheKey(pjp);

        Element element = (Element) cache.get(cacheKey);
        logger.info(new StringBuilder("CacheAspect invoke:").append(" Get:")
            .append(cacheKey).toString());
        // .append(" value:").append(element).toString());

        if (element == null) {
            logger.debug("Consultando información sin cache");
            result = pjp.proceed();
            element = new Element(cacheKey, result);
            cache.put(element);
            logger.debug(new StringBuilder(" Put:").append(cacheKey).toString());
            // .append(" value:").append(result).toString());
        } else {
            logger.debug("Obteniendo Resultados desde el Cache");
        }
        return element.getValue();
    }

    /**
     *
     */
    public void flush() {
        logger.debug("flush");
        cache.flush();
    }

    /**
     * Genera un identificador único para el caché basado en la clase, nombre del método y
     * argumentos pasados a la función.
     *
     * @param pjp
     * @return
     */
    private String getCacheKey(ProceedingJoinPoint pjp) {
        String targetName = pjp.getSignature().getDeclaringTypeName();
        String methodName = pjp.getSignature().getName();
        Object[] arguments = pjp.getArgs();

        StringBuilder sb = new StringBuilder();
        sb.append(targetName).append(".").append(methodName);
        if ((arguments != null) && (arguments.length != 0)) {
            for (int i = 0; i < arguments.length; i++) {
                sb.append(".").append(arguments[i]);
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param cache
     */
    public void setCache(Cache cache) {
        logger.debug("setCache:" + cache.getName());
        this.cache = cache;
    }

}
