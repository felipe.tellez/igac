package co.gov.igac.snc.web.util;

/**
 * Enumeración con los nombres de las secciones de las opciones de consulta avanzada de los reportes
 * prediales
 *
 * @documented leidy.gonzalez
 */
public enum ESeccionReportesPrediales {
    MATRICULA("Matricula"),
    RANGOS_DE_AREA("Rangos de área"),
    VALOR_AVALUO("Valor avalúo"),
    CRITERIOS_ECONOMICOS("Criterios Económicos"),
    JUSTFICACION_DE_PROPIEDAD("Justificación de Propiedad"),
    PREDIOS_BLOQUEADOS("Predios bloqueados");

    private String nombre;

    ESeccionReportesPrediales(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
}
