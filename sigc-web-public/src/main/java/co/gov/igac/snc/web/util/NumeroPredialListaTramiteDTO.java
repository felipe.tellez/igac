package co.gov.igac.snc.web.util;

import java.io.Serializable;
import java.util.List;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;

/**
 * Objeto para mostrar los numeros prediales y los numeros de radicado de cada uno
 *
 * @author javier.barajas
 * @cu 204 Generacion de Formularios SBC
 *
 */
public class NumeroPredialListaTramiteDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 145485667288269288L;

    //private static final long serialVersionUID = -7454856672882692880L;
    private String numeroPredial;

    private List<Tramite> tramitesAsociados;

    public NumeroPredialListaTramiteDTO() {

    }

    /**
     * Full constructor
     *
     * @param numeroPredial
     * @param List<Tramite>
     *
     */
    public NumeroPredialListaTramiteDTO(String numeroPredial,
        List<Tramite> tramitesAsociados) {
        super();
        this.numeroPredial = numeroPredial;
        this.tramitesAsociados.addAll(tramitesAsociados);
    }

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public List<Tramite> getTramitesAsociados() {
        return tramitesAsociados;
    }

    public void setTramitesAsociados(List<Tramite> tramitesAsociados) {
        this.tramitesAsociados = tramitesAsociados;
    }

}
