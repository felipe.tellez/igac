package co.gov.igac.snc.web.mb.tests;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pagm
 */
@Component("radioSelection")
@Scope("session")
public class RadioSelectionMB implements Serializable {
//public class RadioSelectionMB extends ManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 3855247885309611065L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RadioSelectionMB.class);

    private List<String> names;

    private String selectedName;

    public String getSelectedName() {
        return this.selectedName;
    }

    public void setSelectedName(String selectedName) {
        this.selectedName = selectedName;
    }

    public List<String> getNames() {
        return this.names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public RadioSelectionMB() {

    }

    @PostConstruct
    public void init() {

        this.names = new ArrayList<String>();
        this.names.add("lola");
        this.names.add("ramona");
        this.names.add("florinda");

    }

    public void rowSelectedListener() {
        LOGGER.debug("en el listener de selección de fila");
    }

}
