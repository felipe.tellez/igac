package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ComiteAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadEspecificacion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EGeneracionReporteControlCalidadAvaluo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * MB para los caso de uso: CU-SA-AC-018 Realizar informe control calidad al avalùo CU-SA-AC-138
 * Modificar informe control calidad al avalùo CU-SA-AC-114 Consultar informe control calidad al
 * avalùo
 *
 * @cu CU-SA-AC-018
 * @cu CU-SA-AC-138
 * @cu CU-SA-AC-114
 *
 * @author felipe.cadena
 */
// TODO :: felipe.cadena::estan pendientes la validaciones dependientes de los 
// estados del tramite ya que muchos de esos flujo posiblemente se
// manejen por el process. ademas solo se esta controlando las revisiones hasta que el profesional
// es el director del GIT de avalúos se debe definir como manejar las revisiones posteriores a este
// punto.
@Component("realizaInformeControlCalidad")
@Scope("session")
public class RealizaInformeControlCalidadMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = 6277968758758395118L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(RealizaInformeControlCalidadMB.class);
    /**
     * Usuario en sesión
     */
    private UsuarioDTO usuario;
    /**
     * Avalúo sobre el que se realiza el control de calidad
     */
    private Avaluo avaluo;
    /**
     * Variable para determinar si un avaluo paso por comite de avaluos
     */
    private Boolean comiteAvaluos;

    /**
     * Documento asociado al informe de control de calidad o acta comite
     */
    private Documento documento;

    /**
     * Revisión de control de calidad que se esta haciendo al momento de entrar en el caso de uso
     */
    private ControlCalidadAvaluoRev revisionActual;
    /**
     * Revisión de control de calidad que se realiza cuando el coordinador GIT solicito
     * observaciones.
     */
    private ControlCalidadAvaluoRev revisionObservasiones;
    /**
     * Revisión de control de calidad que se esta haciendo al momento de entrar en el caso de uso
     */
    private ControlCalidadAvaluo controlCalidadActual;
    /**
     * Lista para las opciones de aprobación de la soicitud.
     */
    private List<SelectItem> dominioAprobar;
    /**
     * Variable que determina que tipo de control de calidad se esta realizando, ya sea de
     * territorial, sede central o GIT de avalúos.
     */
    private String actividadControlCalidad;
    /**
     * Nombre del dialogo que se utilizara para generar el oficio dependiendo del del tipo de
     * aprobación que se le dio a la revisión del control de calidad.
     */
    private String nombreDialogoOficio;
    private String nombreFormDialogoOficio;
    /**
     * Banderas para los diferentes usos de la vista
     */
    private Boolean banderaCCTerritorial;
    private Boolean banderaCCCentral;
    private Boolean banderaCCGIT;
    private Boolean banderaCrearControlCalidad;
    private Boolean banderaCrearRevision;
    private Boolean banderaModificarRevision;
    private Boolean banderaConsultarRevision;
    private Boolean banderaModoLectura;
    private Boolean banderaDiasPlazo;
    /**
     * Lista de especificación para el control de calidad
     */
    private List<ControlCalidadEspecificacion> listaEspecificaciones;
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    // --------getters-setters----------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getNombreFormDialogoOficio() {
        return this.nombreFormDialogoOficio;
    }

    public void setNombreFormDialogoOficio(String nombreFormDialogoOficio) {
        this.nombreFormDialogoOficio = nombreFormDialogoOficio;
    }

    public String getNombreDialogoOficio() {
        return this.nombreDialogoOficio;
    }

    public void setNombreDialogoOficio(String nombreDialogoOficio) {
        this.nombreDialogoOficio = nombreDialogoOficio;
    }

    public Documento getDocumento() {
        return this.documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public List<SelectItem> getDominioAprobar() {
        return this.dominioAprobar;
    }

    public void setDominioAprobar(List<SelectItem> dominioAprobar) {
        this.dominioAprobar = dominioAprobar;
    }

    public ControlCalidadAvaluoRev getRevisionObservasiones() {
        return this.revisionObservasiones;
    }

    public void setRevisionObservasiones(ControlCalidadAvaluoRev revisionObservasiones) {
        this.revisionObservasiones = revisionObservasiones;
    }

    public String getActividadControlCalidad() {
        return this.actividadControlCalidad;
    }

    public void setActividadControlCalidad(String actividadControlCalidad) {
        this.actividadControlCalidad = actividadControlCalidad;
    }

    public Boolean getBanderaCCTerritorial() {
        return this.banderaCCTerritorial;
    }

    public Boolean getBanderaCCCentral() {
        return this.banderaCCCentral;
    }

    public Boolean getBanderaCCGIT() {
        return this.banderaCCGIT;
    }

    public Boolean getBanderaCrearControlCalidad() {
        return this.banderaCrearControlCalidad;
    }

    public Boolean getBanderaModoLectura() {
        return this.banderaModoLectura;
    }

    public Boolean getBanderaCrearRevision() {
        return this.banderaCrearRevision;
    }

    public Boolean getBanderaModificarRevision() {
        return this.banderaModificarRevision;
    }

    public Boolean getBanderaConsultarRevision() {
        return this.banderaConsultarRevision;
    }

    public Boolean getBanderaDiasPlazo() {
        return this.banderaDiasPlazo;
    }

    public ControlCalidadAvaluoRev getRevisionActual() {
        return this.revisionActual;
    }

    public void setRevisionActual(ControlCalidadAvaluoRev revisionActual) {
        this.revisionActual = revisionActual;
    }

    public ControlCalidadAvaluo getControlCalidadActual() {
        return this.controlCalidadActual;
    }

    public void setControlCalidadActual(ControlCalidadAvaluo controlCalidadActual) {
        this.controlCalidadActual = controlCalidadActual;
    }

    public List<ControlCalidadEspecificacion> getListaEspecificaciones() {
        return this.listaEspecificaciones;
    }

    public void setListaEspecificaciones(List<ControlCalidadEspecificacion> listaEspecificaciones) {
        this.listaEspecificaciones = listaEspecificaciones;
    }

    public Boolean getComiteAvaluos() {
        return this.comiteAvaluos;
    }

    public void setComiteAvaluos(Boolean comiteAvaluos) {
        this.comiteAvaluos = comiteAvaluos;
    }

    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on RealizaInformeControlCalidadMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.banderaDiasPlazo = true;
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        this.nombreDialogoOficio =
            EGeneracionReporteControlCalidadAvaluo.REPORTE_CONTROL_CALIDAD_TERRITORIAL.
                getWidgetVar();
        this.nombreFormDialogoOficio =
            EGeneracionReporteControlCalidadAvaluo.REPORTE_CONTROL_CALIDAD_TERRITORIAL.getForm();

//FIXME::felipe.cadena::datos prueba, se deben retirar cuando se defina su origen
        this.usuario.setIdentificacion("" + 456789123);

    }

    //Metodo de prueba para la consulta de una revisión 
    public void testConsultaRevision() {
        this.llamarConsultarRevision(172L, 14L, 24L,
            EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo());
    }

    //Metodo de prueba para la crear control calidad
    public void testCrearControlCalidad() {
        this.llamarCrearControlCalidad(172l,
            EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo());
    }

    //Metodo de prueba para la crear revisión
    public void testCrearRevision() {
        this.llamarCrearRevision(172l, 22L,
            EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo());
    }

    //Metodo de prueba para la crear revisión
    public void testCrearRevisionTerritorial() {
        this.llamarCrearRevision(172l, 24L,
            EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo());
    }

    //Metodo de prueba para el numero de dias de atrso de un avalúo
    public void testDiasRetraso() {
        int dRetraso = this.getAvaluosService().calcularTiempoRetrasoDias(172L);
        LOGGER.debug("Dias atraso : " + dRetraso);
        this.addMensajeError("Dias atraso : " + dRetraso);
    }

    /**
     * Método para crear un nuevo control de calidad de territorial, recibe el avalúo al cual se
     * realiza el control de calidad
     *
     * @author felipe.cadena
     * @param secRadicado
     * @param actividad - Tipo de actividad {@link EAvaluoAsignacionProfActi}
     */
    public void llamarCrearControlCalidad(Long idAvaluo, String actividad) {
        LOGGER.debug("Iniciando crear control calidad ...");

        this.revisionActual = new ControlCalidadAvaluoRev();
        this.controlCalidadActual = new ControlCalidadAvaluo();
        this.listaEspecificaciones = new ArrayList<ControlCalidadEspecificacion>();

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.cargarEspecificaciones();
        this.actividadControlCalidad = actividad;
        this.iniciarBanderasActividadCC();
        this.banderaCrearControlCalidad = true;
        this.banderaCrearRevision = false;
        this.banderaConsultarRevision = false;
        this.banderaModificarRevision = false;
        this.banderaModoLectura = false;
        this.iniciarRevision();

        LOGGER.debug("Fin control calidad");

    }

    /**
     * Método para crear una nueva revisión relacionada a un control de calidad ya existente, recibe
     * el avalúo al cual se realiza el control de calidad y el Id control de calidad existente
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @param idControlCalidad
     * @param actividad - Tipo de actividad {@link EAvaluoAsignacionProfActi}
     *
     */
    public void llamarCrearRevision(Long idAvaluo, Long idControlCalidad, String actividad) {
        LOGGER.debug("Iniciando crear revision ...");

        this.revisionActual = new ControlCalidadAvaluoRev();
        this.controlCalidadActual = new ControlCalidadAvaluo();
        this.listaEspecificaciones = new ArrayList<ControlCalidadEspecificacion>();

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.cargarEspecificaciones();
        this.controlCalidadActual.setId(idControlCalidad);
        this.actividadControlCalidad = actividad;
        this.iniciarBanderasActividadCC();
        this.banderaCrearControlCalidad = false;
        this.banderaCrearRevision = true;
        this.banderaConsultarRevision = false;
        this.banderaModificarRevision = false;
        this.banderaModoLectura = false;
        this.iniciarRevision();

        LOGGER.debug("Fin revision");

    }

    /**
     * Método para consultar una revisión relacionda a un control de calidad ya existente, recibe el
     * avalúo al cual se realiza el control de calidad y el Id de la revision
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @param idRevision
     *
     */
    public void llamarConsultarRevision(Long idAvaluo, Long idRevision, Long idControlCalidad,
        String actividad) {
        LOGGER.debug("Iniciando consultar revision...");

        this.revisionActual = new ControlCalidadAvaluoRev();
        this.controlCalidadActual = new ControlCalidadAvaluo();
        this.listaEspecificaciones = new ArrayList<ControlCalidadEspecificacion>();

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.revisionActual.setId(idRevision);
        this.controlCalidadActual.setId(idControlCalidad);
        this.actividadControlCalidad = actividad;
        this.iniciarBanderasActividadCC();

        this.banderaCrearControlCalidad = false;
        this.banderaCrearRevision = false;
        this.banderaConsultarRevision = true;
        this.banderaModificarRevision = false;
        this.banderaModoLectura = false;
        this.iniciarRevision();

        if (this.controlCalidadActual.getComiteAvaluos().equals(ESiNo.SI.getCodigo())) {
            this.comiteAvaluos = true;
        } else {
            this.comiteAvaluos = false;
        }

        LOGGER.debug("Fin consulta revision");

    }

    /**
     * Método modificar una revisión relacionda a un control de calidad ya existente, recibe el
     * avalúo al cual se realiza el control de calidad y el Id de la revision
     *
     * @author felipe.cadena
     * @param idAvaluo
     * @param idRevision
     * @param actividad - Tipo de actividad {@link EAvaluoAsignacionProfActi}
     *
     */
    public void llamarModificarRevision(Long idAvaluo, Long idRevision, Long idControlCalidad,
        String actividad) {
        LOGGER.debug("Iniciando modificar revision...");

        this.revisionActual = new ControlCalidadAvaluoRev();
        this.controlCalidadActual = new ControlCalidadAvaluo();
        this.listaEspecificaciones = new ArrayList<ControlCalidadEspecificacion>();

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(idAvaluo);
        this.revisionActual.setId(idRevision);
        this.controlCalidadActual.setId(idControlCalidad);

        this.actividadControlCalidad = actividad;
        this.iniciarBanderasActividadCC();
        this.banderaCrearControlCalidad = false;
        this.banderaCrearRevision = false;
        this.banderaConsultarRevision = false;
        this.banderaModificarRevision = true;
        this.banderaModoLectura = false;
        this.iniciarRevision();
        this.listaEspecificaciones = this.revisionActual.getControlCalidadEspecificacions();

        LOGGER.debug("Fin consulta revision");

    }

    /**
     * Método para iniciar las benderas dependiendo la actividad de control de calidad que se esta
     * ejecutando
     *
     * @param actividad
     */
    public void iniciarBanderasActividadCC() {
        LOGGER.debug("Iniciando banderas calidad ...");

        if (this.actividadControlCalidad.equals(EAvaluoAsignacionProfActi.CC_GIT.getCodigo())) {
            banderaCCGIT = true;
            banderaCCCentral = false;
            banderaCCTerritorial = false;
        } else if (this.actividadControlCalidad.equals(EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.
            getCodigo())) {
            banderaCCGIT = false;
            banderaCCCentral = true;
            banderaCCTerritorial = false;
        } else {
            banderaCCGIT = false;
            banderaCCCentral = false;
            banderaCCTerritorial = true;
        }

    }

    /**
     * Método para inicializar la revisión del control de calidad dependiendo del llamado que se
     * haga del caso de uso
     */
    public void iniciarRevision() {
        LOGGER.debug("Iniciando revision actual ...");
        if (this.banderaCrearControlCalidad) {
            //Se crean el control de calidad y la revisión inicial
            ProfesionalAvaluo pa = this.getAvaluosService().
                obtenerProfesionaAvaluosPorIdentificacion(this.usuario.getIdentificacion());
            this.controlCalidadActual = new ControlCalidadAvaluo();
            this.controlCalidadActual.setProfesionalAvaluos(pa);
            this.controlCalidadActual.setAvaluoId(this.avaluo.getId());
            this.controlCalidadActual.setTerritorialCodigo(this.usuario.getCodigoTerritorial());

            this.revisionActual = new ControlCalidadAvaluoRev();
            this.revisionActual.setAprobo(ESiNo.NO.getCodigo());
            this.revisionActual.setFecha(new Date());
            this.revisionActual.setPlazo(0L);
            this.revisionActual.setNumero(1l);
            this.revisionActual.setControlCalidadAvaluo(this.controlCalidadActual);
            this.revisionActual.setControlCalidadEspecificacions(this.listaEspecificaciones);
            for (ControlCalidadEspecificacion cce : this.listaEspecificaciones) {
                cce.setAvaluoControlCalidadRev(this.revisionActual);
            }

        } else if (this.banderaCrearRevision) {
            //Se consulta un control de calidad ya existente y se crea una nueva revisión
            this.controlCalidadActual = this.getAvaluosService().
                obtenerControlCalidadAvaluoPorId(this.controlCalidadActual.getId());

            this.revisionActual = new ControlCalidadAvaluoRev();
            this.revisionActual.setAprobo(ESiNo.NO.getCodigo());
            this.revisionActual.setFecha(new Date());
            this.revisionActual.setPlazo(0L);
            this.revisionActual.setNumero(new Long(this.controlCalidadActual.
                getControlCalidadAvaluoRevs().size() + 1));
            this.revisionActual.setControlCalidadAvaluo(this.controlCalidadActual);
            this.revisionActual.setControlCalidadEspecificacions(this.listaEspecificaciones);
            for (ControlCalidadEspecificacion cce : this.listaEspecificaciones) {
                cce.setAvaluoControlCalidadRev(this.revisionActual);
            }

        } else if (this.banderaModificarRevision || this.banderaConsultarRevision) {
            //Se consulta la revisión con el control de calidad asociado.

            this.controlCalidadActual = this.getAvaluosService().
                obtenerControlCalidadAvaluoPorId(this.controlCalidadActual.getId());
            for (ControlCalidadAvaluoRev ccr : this.controlCalidadActual.
                getControlCalidadAvaluoRevs()) {
                if (ccr.getId() == this.revisionActual.getId()) {
                    this.revisionActual = ccr;
                }
            }
            this.revisionActual = this.getAvaluosService().
                obtenerControlCalidadAvaluoRevPorId(this.revisionActual.getId());
            this.listaEspecificaciones = this.revisionActual.getControlCalidadEspecificacions();

        }

        this.procesarOpcionesAprobacion();
        LOGGER.debug("Revision iniciada");
    }

    /**
     * Método para procesar la opciones de aprobación de la revisión los valores para seleccionar
     * varian de acuerdo a las caracteristicas de la revisión de control de calidad que se necesita
     * realizar.
     */
    public void procesarOpcionesAprobacion() {
        List<Dominio> listDominio = this.getGeneralesService().
            buscarDominioPorNombreOrderByNombre(EDominio.CONTROL_CALIDAD_APROBO);
        this.dominioAprobar = new ArrayList<SelectItem>();
        if (this.revisionActual.getNumero() == 3 &&
             !this.actividadControlCalidad.equals(EAvaluoAsignacionProfActi.CC_GIT.getCodigo())) {
            this.dominioAprobar.add(new SelectItem(ESiNo.NO.getCodigo(), ESiNo.NO.getCodigo()));
            this.dominioAprobar.add(new SelectItem(ESiNo.SI.getCodigo(), ESiNo.SI.getCodigo()));
        } else if (this.banderaConsultarRevision) {
            for (Dominio dominio : listDominio) {
                this.dominioAprobar.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));

            }
        } else {

            for (Dominio dominio : listDominio) {
                if (this.revisionActual.getNumero() < 3 &&
                     !this.actividadControlCalidad.equals(EAvaluoAsignacionProfActi.CC_GIT.
                        getCodigo()) &&
                     dominio.getCodigo().equals(ESiNo.NO.getCodigo())) {
                    continue;
                }
                this.dominioAprobar.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));

            }
        }
    }

    /**
     * Método para cargar las que se deben tener en cuenta para una revisión
     */
    public void cargarEspecificaciones() {
        LOGGER.debug("Cargando especificaciones ...");
        List<Dominio> doms = this.getGeneralesService().
            buscarDominioPorNombreOrderByNombre(EDominio.CONTROL_CALIDAD_ESPECIFICAC);

        for (Dominio dom : doms) {
            ControlCalidadEspecificacion cce = new ControlCalidadEspecificacion();
            cce.setCumpleBoolean(false);
            cce.setAvaluoControlCalidadRev(this.revisionActual);
            cce.setEspecificacion(dom.getCodigo());

            this.listaEspecificaciones.add(cce);

        }

        LOGGER.debug("Size " + this.listaEspecificaciones.size());
        LOGGER.debug("Especificaciones cargadas");
    }

    /**
     * Método para validar el estado de la opción 'aprobar revisión' dependiendo de los valores de
     * las especificaciones
     *
     * @return
     */
    public Boolean validarAprobacion() {
        LOGGER.debug("Validando aprobacion ...");
        Boolean valido = true;

        for (ControlCalidadEspecificacion cce : this.listaEspecificaciones) {
            if (!cce.getCumpleBoolean()) {
                valido = false;
                break;
            }
        }
        LOGGER.debug("Validacion terminada ...");

        return valido;
    }

    /**
     * Método para validar si las especificaciones que tienen estado 'no cumple' tienen
     * observaciones asociadas. retorna falso si alguna espeficaciòn 'no cumple' y no tiene
     * observaciòn.
     *
     * @return
     */
    public Boolean validarObservaciones() {

        LOGGER.debug("Validando observaciones ...");
        Boolean valido = true;

        for (ControlCalidadEspecificacion cce : this.listaEspecificaciones) {
            if (!cce.getCumpleBoolean() && ("".equals(cce.getDescripcion()) ||
                cce.getDescripcion() == null)) {
                valido = false;
                break;
            }
        }
        LOGGER.debug("Validacion terminada ...");

        return valido;
    }

    /**
     * Método para mostrar el acta del comite
     *
     * @author felipe.cadena
     */
    public void mostrarActaComite() {
        LOGGER.debug("Mostrar acta de comite CU-SA-AC-091 Importar acta de Comité ");
//TODO :: felipe.cadena :: debería haber un método aparte para mover el proceso (no hacerlo aquí) :: pedro.garcia
//TODO :: felipe.cadena::Mover el tramite a la actividad de captura de datos de comite
    }

    /**
     * Método para para generar reporte de observaciones control calidad
     *
     * @author felipe.cadena
     */
    public void generarReporte() {

        ControlCalidadAvaluoRev revisionEnBase = null;
        for (ControlCalidadAvaluoRev ccr : this.controlCalidadActual.getControlCalidadAvaluoRevs()) {
            if (ccr.getNumero().equals(this.revisionActual.getNumero())) {
                revisionEnBase = ccr;
            }
        }

        if (this.banderaCCTerritorial && !this.getRevisionActual().
            getAprobo().equals(ESiNo.NO.getCodigo()) &&
             !this.getRevisionActual().getAprobo().equals(ESiNo.SI.getCodigo())) {
            LOGGER.debug("CU-SA-AC-064 Generar Reporte de " +
                 "Control de Calidad de Territorial ");

            GeneracionReporteControlCalidadTerritorialMB generacionReporteControlCalidadTerritorialMB =
                (GeneracionReporteControlCalidadTerritorialMB) UtilidadesWeb
                    .getManagedBean("generarReporteControlCalidadTerritorial");
            generacionReporteControlCalidadTerritorialMB
                .cargarDesdeOtrosMB(revisionEnBase);

        } else if (this.banderaCCTerritorial && this.getRevisionActual().
            getAprobo().equals(ESiNo.SI.getCodigo())) {
            LOGGER.debug("CU-SA-AC-066 Generar Oficio Remisorio de" +
                 " Aprobación de Control de Calidad Territorial ");

            GeneracionOficioRemisorioTerritorialMB oficioRemisorioTerritorialMB =
                (GeneracionOficioRemisorioTerritorialMB) UtilidadesWeb.
                    getManagedBean("generacionOficioRemisorioTerritorial");
            oficioRemisorioTerritorialMB.cargarDesdeOtrosMB(revisionEnBase);

        } else if (this.banderaCCCentral && !this.getRevisionActual().getAprobo().
            equals(ESiNo.NO.getCodigo()) &&
             !this.getRevisionActual().getAprobo().equals(ESiNo.SI.getCodigo())) {
            LOGGER.debug("CU-SA-AC-065 Generar Reporte Control de" +
                 " Calidad de GIT de Avalúos ");

            GeneracionReporteControlCalidadGITMB generacionReporteControlCalidadGITMB =
                (GeneracionReporteControlCalidadGITMB) UtilidadesWeb.
                    getManagedBean("generacionReporteControlCalidadGIT");
            generacionReporteControlCalidadGITMB.cargarDesdeOtrosMB(revisionEnBase);

//TODO :: felipe.cadena::integrar caso de uso CU-SA-AC-065 Generar Reporte Control de Calidad de GIT de Avalúos
        } else if (this.banderaCCCentral && this.getRevisionActual().
            getAprobo().equals(ESiNo.NO.getCodigo())) {
            LOGGER.debug("Mover tramite a actividad no aprobacion sede central ");
//TODO :: felipe.cadena::mover tramite a actividad no aprobacion sede central

        } else if (this.banderaCCTerritorial && this.getRevisionActual().
            getAprobo().equals(ESiNo.NO.getCodigo())) {
            LOGGER.debug("Mover tramite a actividad no aprobacion territorial ");
//TODO :: felipe.cadena::mover tramite a actividad no aprobacion territorial

        } else {
            LOGGER.debug("Mover tramite a actividad Aprobación sede central");
//TODO :: felipe.cadena::mover tramite a actividad Aprobación sede central
        }

    }

    /**
     * Método para para consultar informe ejecutivo de avalúo
     *
     * @author felipe.cadena
     */
    public void consultarInforme() {
        LOGGER.debug("Generar reporte de observaciones CU-SA-AC-019 Consultar Informe " +
             "Ejecutivo de Avalúo Comercial ");
//TODO :: felipe.cadena::integrar caso de uso CU-SA-AC-019 Consultar Informe Ejecutivo de Avalúo Comercial

    }

    /**
     * Se usa para modificar el control de calidad cuando se acaba de crear
     */
    public void modificarControlCalidad() {
        int len = this.controlCalidadActual.getControlCalidadAvaluoRevs().size();
        LOGGER.debug("Len " + len);

        this.llamarModificarRevision(this.avaluo.getId(),
            this.controlCalidadActual.getControlCalidadAvaluoRevs().get(len - 1).getId(),
            this.controlCalidadActual.getId(),
            this.actividadControlCalidad);
        this.banderaModoLectura = false;
        LOGGER.debug("Id rev  " + this.controlCalidadActual.
            getControlCalidadAvaluoRevs().get(len - 1).getId());

    }

    public void guardarControlCalidad() {
        LOGGER.debug("Guardar control de calidad ");

        if (!this.validarAprobacion() && this.revisionActual.getAprobo().
            equals(ESiNo.SI.getCodigo())) {

            this.addMensajeError("Si el control de calidad es aprobado entonces deben" +
                 " estar seleccionados todos los criterios a evaluar como CUMPLE");
        } else if (!this.validarObservaciones()) {

            this.addMensajeError("Si se tienen observaciones de control de calidad en el dato" +
                 " “Descripción” debe colocarse la observación correspondiente" +
                 " al criterio a evaluar que debe ser corregido");
        } else if (this.validarAprobacion() && !this.revisionActual.getAprobo().equals(ESiNo.SI.
            getCodigo())) {

            this.addMensajeError(
                "Si todos los criterios a evaluar cumplen para control de calidad," +
                 " se debe seleccionar que SI se aprobó el control de calidad");
        } else {

            if (this.banderaCrearControlCalidad) {
                this.controlCalidadActual.setActividad(this.actividadControlCalidad);
                this.controlCalidadActual.setControlCalidadAvaluoRevs(
                    new ArrayList<ControlCalidadAvaluoRev>());
            }
            if (this.banderaModificarRevision) {
                for (int i = 0; i < this.controlCalidadActual.getControlCalidadAvaluoRevs().size();
                    i++) {
                    ControlCalidadAvaluoRev ccr = this.controlCalidadActual.
                        getControlCalidadAvaluoRevs().get(i);
                    if (ccr.getId() == this.revisionActual.getId()) {
                        this.controlCalidadActual.
                            getControlCalidadAvaluoRevs().remove(i);
                        break;
                    }
                }
            }
            this.controlCalidadActual.setAprobo(this.revisionActual.getAprobo());
            this.controlCalidadActual.getControlCalidadAvaluoRevs().add(this.revisionActual);
            this.revisionActual.setControlCalidadAvaluo(this.controlCalidadActual);

            this.controlCalidadActual = this.getAvaluosService().
                crearActualizarControlCalidadAvaluo(this.controlCalidadActual, this.usuario);

            this.banderaModoLectura = true;

        }

        this.validarReporteAGenerar();

    }

    /**
     * Método para determinar qué diálogo se debe mostrar para generar el informe de la revisión del
     * control de calidad.
     *
     * @author felipe.cadena
     *
     */
    public void validarReporteAGenerar() {
        if (this.banderaCCTerritorial && !this.getRevisionActual().
            getAprobo().equals(ESiNo.NO.getCodigo()) &&
             !this.getRevisionActual().getAprobo().equals(ESiNo.SI.getCodigo())) {

            this.nombreDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.REPORTE_CONTROL_CALIDAD_TERRITORIAL.
                    getWidgetVar();
            this.nombreFormDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.REPORTE_CONTROL_CALIDAD_TERRITORIAL.getForm();
        } else if (this.banderaCCTerritorial && this.getRevisionActual().
            getAprobo().equals(ESiNo.SI.getCodigo())) {

            this.nombreDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.OFICIO_REMISORIO_TERRITORIAL.getWidgetVar();
            this.nombreFormDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.OFICIO_REMISORIO_TERRITORIAL.getForm();

        } else if (this.banderaCCCentral && !this.getRevisionActual().
            getAprobo().equals(ESiNo.NO.getCodigo()) &&
             !this.getRevisionActual().getAprobo().equals(ESiNo.SI.getCodigo())) {

            this.nombreDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.REPORTE_CONTROL_CALIDAD_GIT_AVALUOS.
                    getWidgetVar();
            this.nombreFormDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.REPORTE_CONTROL_CALIDAD_GIT_AVALUOS.getForm();
        } else {

            this.nombreDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.REPORTE_NO_APROBACION_GIT_AVALUOS.
                    getWidgetVar();
            this.nombreFormDialogoOficio =
                EGeneracionReporteControlCalidadAvaluo.REPORTE_NO_APROBACION_GIT_AVALUOS.getForm();

        }
    }

    /**
     * Método para buscar el acta relacionada al control de calidad actual y visualizarla.
     *
     * @author felipe.cadena
     */
    public void verActaComite() {

        ComiteAvaluo comite = this.getAvaluosService()
            .consultarComiteAvaluoPorIdAvaluo(this.avaluo.getId());

        this.documento = this.getGeneralesService().buscarDocumentoPorId(
            comite.getActaFirmadaDocumentoId());

        this.visualizarDocumento();
    }

    /**
     * Método para buscar el documento asociado a la revisión actual y visualizarlo.
     *
     * @author felipe.cadena
     */
    public void verDocumentoRevision() {
        LOGGER.debug("Buscando documento de revision...");
        this.documento = this.revisionActual.getSoporteDocumento();
        this.visualizarDocumento();
    }

    /**
     * Método preparacion del documento a visualizar, el documento ya debe estar guardado en
     * alfresco
     *
     * @author felipe.cadena
     */
    public void visualizarDocumento() {
        LOGGER.debug("Visualizando Documento");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        ReporteDTO reporteDTO = this.reportsService.
            consultarReporteDeGestorDocumental(this.documento.getIdRepositorioDocumentos());

        previsualizacionDocMB.setReporteDTO(reporteDTO);
    }

    /**
     * Método para actualizar el estado del componente de los dias de plazo de acuerdo al tipo de
     * aprobacion Es listener del componente 'h:selectOneRadio' con id="apRevisionSlt" que determina
     * si se aprueba o no la revisiòn de control de calidad.
     *
     * @author felipe.cadena
     */
    public void listenerAprobacion() {
        LOGGER.debug("Listener aprobacion");
        if (this.revisionActual.getAprobo().equals(ESiNo.SI.getCodigo()) ||
            this.revisionActual.getAprobo().equals(ESiNo.NO.getCodigo())) {
            this.revisionActual.setPlazo(0L);
            this.banderaDiasPlazo = true;
        } else {
            this.banderaDiasPlazo = false;
        }

    }

}
