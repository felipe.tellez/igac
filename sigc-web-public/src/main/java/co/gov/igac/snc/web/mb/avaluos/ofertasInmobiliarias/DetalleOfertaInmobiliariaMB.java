/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias;

import co.gov.igac.snc.persistence.entity.avaluos.Fotografia;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.util.ECondicionJuridica;
import co.gov.igac.snc.persistence.util.EOfertaTipoInmueble;
import co.gov.igac.snc.persistence.util.enumsOfertasInmob.EOfertaTipoOferta;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro.garcia
 */
@Component("detalleOfertaInmob")
@Scope("session")
public class DetalleOfertaInmobiliariaMB extends SNCManagedBean {

    private static final long serialVersionUID = -5179735166965075L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DetalleOfertaInmobiliariaMB.class);

    // ------------------ services ----------
    @Autowired
    private IContextListener contextoWebApp;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportes = ReportesUtil.getInstance();

    private OfertaInmobiliaria currentOferta;

    // ----- banderas
    /**
     * solo se usa para el composite component de fotografías poco genérico de Gabriel
     */
    private boolean banderaFotografiasCargadas;

    /**
     * banderas para determinar el tipo específico de oferta
     */
    /**
     * Bandera que determina si la oferta es una casa, un apartamento o una suite hotelera
     */
    private boolean banderaCasaApartamentoSuiteHotelera;

    /**
     * Bandera que determina si la oferta es un lote o una casa lote
     */
    private boolean banderaLoteCasaLote;

    /**
     * Bandera que determina si la oferta es un local
     */
    private boolean banderaLocal;

    /**
     * Bandera que determina si la oferta es una oficina o un consultorio
     */
    private boolean banderaOficinaConsultorio;

    /**
     * Bandera que determina si la oferta es una bodega
     */
    private boolean banderaBodega;

    /**
     * Bandera que determina si la oferta es una finca
     */
    private boolean banderaFinca;

    /**
     * Bandera que determina si la oferta es un edificio
     */
    private boolean banderaEdificio;

    /**
     * Bandera que determina si la oferta es un parqueadero
     */
    private boolean banderaParqueadero;

    // -------------- methods -------------------------
    public boolean isBanderaCasaApartamentoSuiteHotelera() {
        return this.banderaCasaApartamentoSuiteHotelera;
    }

    public void setBanderaCasaApartamentoSuiteHotelera(
        boolean banderaCasaApartamentoSuiteHotelera) {
        this.banderaCasaApartamentoSuiteHotelera = banderaCasaApartamentoSuiteHotelera;
    }

    public boolean isBanderaLoteCasaLote() {
        return this.banderaLoteCasaLote;
    }

    public void setBanderaLoteCasaLote(boolean banderaLoteCasaLote) {
        this.banderaLoteCasaLote = banderaLoteCasaLote;
    }

    public boolean isBanderaLocal() {
        return this.banderaLocal;
    }

    public void setBanderaLocal(boolean banderaLocal) {
        this.banderaLocal = banderaLocal;
    }

    public boolean isBanderaOficinaConsultorio() {
        return this.banderaOficinaConsultorio;
    }

    public void setBanderaOficinaConsultorio(boolean banderaOficinaConsultorio) {
        this.banderaOficinaConsultorio = banderaOficinaConsultorio;
    }

    public boolean isBanderaBodega() {
        return this.banderaBodega;
    }

    public void setBanderaBodega(boolean banderaBodega) {
        this.banderaBodega = banderaBodega;
    }

    public boolean isBanderaFinca() {
        return this.banderaFinca;
    }

    public void setBanderaFinca(boolean banderaFinca) {
        this.banderaFinca = banderaFinca;
    }

    public boolean isBanderaEdificio() {
        return this.banderaEdificio;
    }

    public void setBanderaEdificio(boolean banderaEdificio) {
        this.banderaEdificio = banderaEdificio;
    }

    public boolean isBanderaParqueadero() {
        return this.banderaParqueadero;
    }

    public void setBanderaParqueadero(boolean banderaParqueadero) {
        this.banderaParqueadero = banderaParqueadero;
    }

    public boolean isBanderaFotografiasCargadas() {
        return this.banderaFotografiasCargadas;
    }

    public void setBanderaFotografiasCargadas(boolean banderaFotografiasCargadas) {
        this.banderaFotografiasCargadas = banderaFotografiasCargadas;
    }

    public OfertaInmobiliaria getCurrentOferta() {
        return this.currentOferta;
    }

    public void setCurrentOferta(OfertaInmobiliaria newOferta) {
        if (this.currentOferta == null ||
             newOferta.getId() != this.currentOferta.getId()) {
            this.currentOferta = this.getAvaluosService()
                .buscarOfertaInmobiliariaPorId(newOferta.getId());

            if (this.isInmuebleDeProyectoNuevo()) {
                this.currentOferta = this.getAvaluosService()
                    .buscarOfertaInmobiliariaOfertaPadreByOfertaId(
                        currentOferta.getId());
            }

            if (this.currentOferta.getTipoInmueble() != null) {
                String tipoInmueble = this.currentOferta.getTipoInmueble();
                resetBanderasTipoInmueble();
                if (tipoInmueble.equals(EOfertaTipoInmueble.APARTAMENTO
                    .getCodigo()) ||
                     tipoInmueble.equals(EOfertaTipoInmueble.CASA
                        .getCodigo()) ||
                     tipoInmueble
                        .equals(EOfertaTipoInmueble.SUITEHOTELERA
                            .getCodigo())) {
                    this.banderaCasaApartamentoSuiteHotelera = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.BODEGA
                    .getCodigo())) {
                    this.banderaBodega = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.EDIFICIO
                    .getCodigo())) {
                    this.banderaEdificio = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.FINCA
                    .getCodigo())) {
                    this.banderaFinca = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.LOCAL
                    .getCodigo())) {
                    this.banderaLocal = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.LOTE
                    .getCodigo()) ||
                     tipoInmueble.equals(EOfertaTipoInmueble.CASALOTE
                        .getCodigo())) {
                    this.banderaLoteCasaLote = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.OFICINA
                    .getCodigo()) ||
                     tipoInmueble.equals(EOfertaTipoInmueble.CONSULTORIO
                        .getCodigo())) {
                    this.banderaOficinaConsultorio = true;
                } else if (tipoInmueble.equals(EOfertaTipoInmueble.PARQUEADERO
                    .getCodigo())) {
                    this.banderaParqueadero = true;
                }
            }
        }
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on init of DetalleOfertaInmobiliariaMB");

    }

//--------------------------------------------------------------------------------------------------
    public void dummyMethod() {
        LOGGER.debug("doing nothing");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * devuelve a false todas las banderas de tipo de inmueble para que no haya nunca dos en true
     */
    private void resetBanderasTipoInmueble() {

        this.banderaCasaApartamentoSuiteHotelera = false;
        this.banderaLoteCasaLote = false;
        this.banderaLocal = false;
        this.banderaOficinaConsultorio = false;
        this.banderaBodega = false;
        this.banderaFinca = false;
        this.banderaEdificio = false;
        this.banderaParqueadero = false;

    }

    /**
     * Devuelve si la oferta es propiedad horizontal o no
     *
     * @author christian.rodriguez
     * @return boolean verdadero si es oferta inmobiliaria de propiedad horizontal y falso en otro
     * caso
     */
    public boolean isPropiedadHorizontal() {
        if (this.currentOferta != null &&
             this.currentOferta.getCondicionJuridica() != null) {
            String condicionJuridica = this.currentOferta
                .getCondicionJuridica();
            if (condicionJuridica.toLowerCase().equals(
                ECondicionJuridica.PROPIEDAD_HORIZONTAL.getCodigo()
                    .toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Devuelve si la oferta es proyecto nuevo o no
     *
     * @author christian.rodriguez
     * @return boolean verdadero si es oferta inmobiliaria es un proyecto, falso en otro caso
     */
    public boolean isProyectoNuevo() {
        if (this.currentOferta != null &&
             this.currentOferta.getTipoOferta() != null) {
            String tipoOferta = this.currentOferta.getTipoOferta();
            if (tipoOferta.toLowerCase().equals(
                EOfertaTipoOferta.PROYECTOS.getCodigo().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Devuelve si la oferta es un inmueble de un proyecto nuevo o no
     *
     * @author christian.rodriguez
     * @return boolean verdadero si es es un inmueble de un proyecto nuevo, falso si no
     */
    public boolean isInmuebleDeProyectoNuevo() {
        if (this.currentOferta != null &&
             this.currentOferta.getTipoInmueble() != null &&
             this.currentOferta.getTipoOferta() != null) {
            return this.isProyectoNuevo();
        }
        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método action del botón "Ver fotografías" de la ventana de detalles de oferta.
     *
     * Carga del gestor documental las fotos de la oferta
     *
     * PRE: se ha consultado la oferta con los datos de fotografías de la tabla FOTOGRAFIA
     *
     * @author pedro.garcia
     */
    public void cargarFotografiasOfertaDesdeGestorDocumental() {

        this.banderaFotografiasCargadas = false;

        if (this.currentOferta != null &&
             this.currentOferta.getFotografias() != null &&
             !this.currentOferta.getFotografias().isEmpty()) {

            for (Fotografia f : this.currentOferta.getFotografias()) {
                if (!f.getIdRepositorioDocumentos().isEmpty()) {

                    ReporteDTO foto = this.reportes
                        .consultarReporteDeGestorDocumental(f
                            .getIdRepositorioDocumentos());

                    f.setNombreDescargaFotoAlfresco(foto.getUrlWebReporte());
                }
            }
            this.banderaFotografiasCargadas = true;
        }

    }

// end of class
}
