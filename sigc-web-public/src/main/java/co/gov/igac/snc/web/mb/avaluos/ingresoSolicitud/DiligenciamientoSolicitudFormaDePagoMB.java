package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("diligenciarSolicitudFormaDePago")
@Scope("session")
public class DiligenciamientoSolicitudFormaDePagoMB extends SNCManagedBean implements
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3192560069126536848L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DiligenciamientoSolicitudFormaDePagoMB.class);

    private boolean banderaPSE;
    private boolean banderaConsignacion;
    private boolean banderaContrato;
    private int formaDePagoVal;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @PostConstruct
    public void init() {
        LOGGER.debug("DiligenciarSolicitudFormaDePagoMB#init");

        this.banderaContrato = false;
        this.banderaConsignacion = false;
        this.banderaPSE = false;

    }

    /**
     * Accion ejecutada sobre el radio button asociado al tipo de bloqueo
     */
    public void renderFormaDePago() {

        if (this.formaDePagoVal == 0) {
            this.banderaContrato = true;
            this.banderaConsignacion = false;
            this.banderaPSE = false;
        } else if (this.formaDePagoVal == 1) {
            this.banderaContrato = false;
            this.banderaConsignacion = true;
            this.banderaPSE = false;
        } else if (this.formaDePagoVal == 2) {
            this.banderaContrato = false;
            this.banderaConsignacion = false;
            this.banderaPSE = true;
        }
    }

    public boolean isBanderaPSE() {
        return banderaPSE;
    }

    public void setBanderaPSE(boolean banderaPSE) {
        this.banderaPSE = banderaPSE;
    }

    public boolean isBanderaConsignacion() {
        return banderaConsignacion;
    }

    public void setBanderaConsignacion(boolean banderaConsignacion) {
        this.banderaConsignacion = banderaConsignacion;
    }

    public boolean isBanderaContrato() {
        return banderaContrato;
    }

    public void setBanderaContrato(boolean banderaContrato) {
        this.banderaContrato = banderaContrato;
    }

    public int getFormaDePagoVal() {
        return formaDePagoVal;
    }

    public void setFormaDePagoVal(int formaDePagoVal) {
        this.formaDePagoVal = formaDePagoVal;
    }

    public String cerrar() {

        UtilidadesWeb.removerManagedBean("diligenciarSolicitudFormaDePago");
        this.tareasPendientesMB.init();
        this.init();
        return "index";
    }

}
