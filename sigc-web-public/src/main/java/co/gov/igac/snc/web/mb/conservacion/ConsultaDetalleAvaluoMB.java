package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.DecimalFormat;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author fabio.navarrete
 */
@Component("consultaDetalleAvaluo")
@Scope("session")
public class ConsultaDetalleAvaluoMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8749574647708426741L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaDetalleAvaluoMB.class);

    // ------------------------------ Variables --------------------------------
    private Predio predio;
    private PredioAvaluoCatastral avaluo;

    private boolean banderaVistaConstruccionesConvencionalesExtendida;

    /** ---------------------------------- */
    /** ----------- SERVICIOS------------- */
    /** ---------------------------------- */
    @Autowired
    private IContextListener contextService;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String manzanaCodigo;

    /**
     * Datos necesarios para cargar el visor a nivel de zonas.
     */
    private String codigoMunicipio;
    private String codigosZHF;
    private String codigosZHG;

    /**
     * bandera para indicar que el predio es fiscal
     *
     * @author jonathan.chacon
     */
    private boolean banderaPredioFiscal;

//TODO :: andres.eslava :: 25-06-2013 :: usar 'this' para referirse a atributos de clase (revisar todas) :: pedro.garcia    
    public IContextListener getContextService() {
        return contextService;
    }

    public void setContextService(IContextListener contextService) {
        this.contextService = contextService;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public boolean isBanderaVistaConstruccionesConvencionalesExtendida() {
        return this.banderaVistaConstruccionesConvencionalesExtendida;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(String codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getCodigosZHF() {
        return codigosZHF;
    }

    public void setCodigosZHF(String codigosZHF) {
        this.codigosZHF = codigosZHF;
    }

    public String getCodigosZHG() {
        return codigosZHG;
    }

    public void setCodigosZHG(String codigosZHg) {
        this.codigosZHG = codigosZHg;
    }

    public void setBanderaVistaConstruccionesConvencionalesExtendida(
        boolean banderaVistaConstruccionesConvencionalesExtendida) {
        this.banderaVistaConstruccionesConvencionalesExtendida =
            banderaVistaConstruccionesConvencionalesExtendida;
    }

    public Predio getPredio() {
        return this.predio;
    }

    public PredioAvaluoCatastral getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(PredioAvaluoCatastral avaluo) {
        this.avaluo = avaluo;
    }

    public boolean isBanderaPredioFiscal() {
        return banderaPredioFiscal;
    }

    public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
        this.banderaPredioFiscal = banderaPredioFiscal;
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * Obtiene el predio por el número predial con sus respectivos avalúos. Además de esto obtiene
     * el avalúo más reciente y lo ubica en la variable de la clase avaluo.
     *
     * @param numPredial
     */
    public void getPredioById(Long predioId) {

        this.predio = this.getConservacionService().obtenerPredioConDetalleAvaluoPorId(predioId);
        if (this.predio != null) {
            if (this.predio.getPredioAvaluoCatastrals().size() > 0) {
                this.avaluo = this.predio.getPredioAvaluoCatastrals().get(0);
            }
            Date hoy = new Date();
            for (PredioAvaluoCatastral pac : this.predio.getPredioAvaluoCatastrals()) {
                if (pac.getVigencia().getYear() <= hoy.getYear() && pac.getVigencia().after(
                    this.avaluo.getVigencia())) {
                    this.avaluo = pac;
                }
            }
        } else {
            this.addMensajeInfo("Predio no encontrado");
        }

    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        this.predio = new Predio();
        this.avaluo = new PredioAvaluoCatastral();
        // Se obtiene un predio específico, para pruebas.
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb
            .getManagedBean("consultaPredio");
        Long idPredio = mb001.getSelectedPredioId();

        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (idPredio != null) {
            if (!esPredioOrigen) {
                this.getPredioById(idPredio);
            } else {
                this.predio = mb001.getSelectedPredio1();
            }
            if (this.predio.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.
                getCodigo())) {
                this.banderaPredioFiscal = true;
            }
        }

        // Inicialización del visor
        this.applicationClientName = this.contextService.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        cargaInformacionVisor();
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 19-11-2013 ahora NO muestra las construcciones que tengan algún valor
     * en el campo año_cancelacion
     */
    public List<UnidadConstruccion> getUnidadesConstruccionConvencional() {
        ArrayList<UnidadConstruccion> unidadesConst = new ArrayList<UnidadConstruccion>();
        for (UnidadConstruccion uni : this.predio.getUnidadConstruccions()) {
            if (uni.getTipoConstruccion().trim()
                .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                 uni.getAnioCancelacion() == null) {
                unidadesConst.add(uni);
            }
        }
        return unidadesConst;
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 19-11-2013 ahora NO muestra las construcciones que tengan algún valor
     * en el campo año_cancelacion
     */
    public List<UnidadConstruccion> getUnidadesConstruccionNoConvencional() {
        ArrayList<UnidadConstruccion> unidadesConst = new ArrayList<UnidadConstruccion>();
        for (UnidadConstruccion uni : predio.getUnidadConstruccions()) {
            if (uni.getTipoConstruccion().trim()
                .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()) &&
                 uni.getAnioCancelacion() == null) {
                unidadesConst.add(uni);
            }
        }
        return unidadesConst;
    }
//--------------------------------------------------------------------------------------------------

    public String getAreaTotalConsConvencional() {
        Double area = 0.0;
        for (UnidadConstruccion unidad : this
            .getUnidadesConstruccionConvencional()) {
            area += unidad.getAreaConstruida();
        }
        //version 1.1.4 - felipe.cadena_7286
        //felipe.cadena::redmine #7491 :: formato numero
        return UtilidadesWeb.formatDecimalNumber(area);
    }

    public String getAreaTotalConsNoConvencional() {
        Double area = 0.0;
        for (UnidadConstruccion unidad : this
            .getUnidadesConstruccionNoConvencional()) {
            area += unidad.getAreaConstruida();
        }
        //felipe.cadena::redmine #7491 :: formato numero
        return UtilidadesWeb.formatDecimalNumber(area);
    }

    public String getAreaTotalTerreno() {
        //Se retorna el área del predio que se almacenó en la BD que tiene dos cifras decimales refs#12022
        return String.valueOf(predio.getAreaTerreno());
    }

    public String getAreaTotalConstruccion() {
        Double areaTotal = new Double(this.getAreaTotalConsConvencional());
        areaTotal += new Double(this.getAreaTotalConsNoConvencional());
        //felipe.cadena::redmine #7491 :: formato numero
        return UtilidadesWeb.formatDecimalNumber(areaTotal);
    }

    /*
     * Carga la informacion necesaria para el visor de Zonas el predio es la fuente de informacion
     * de zonas a las que pertenece el predio. @return @author andres.eslava
     */
    private void cargaInformacionVisor() {

        this.codigosZHF = new String();
        this.codigosZHG = new String();

        for (PredioZona zona : this.predio.getPredioZonas()) {
            this.codigosZHF += zona.getZonaFisica() + ",";
            this.codigosZHG += zona.getZonaGeoeconomica() + ",";
        }
        if (this.codigosZHF.length() != 0) {
            this.codigosZHF = this.codigosZHF.substring(0, this.codigosZHF.length() - 1);
            this.codigosZHG = this.codigosZHG.substring(0, this.codigosZHG.length() - 1);
            this.codigoMunicipio = this.predio.getMunicipio().getCodigo();
        }

    }

    /**
     * Método para calcular el total de unidades asociadas a un predio
     *
     * @author javier.aponte
     */
    public int getTotalUnidadesConstruccion() {
        int resultado = 0;

        if (this.getUnidadesConstruccionConvencional() != null) {
            resultado = this.getUnidadesConstruccionConvencional().size();
        }
        if (this.getUnidadesConstruccionNoConvencional() != null) {
            resultado = resultado + getUnidadesConstruccionNoConvencional().size();
        }

        return resultado;
    }

}
