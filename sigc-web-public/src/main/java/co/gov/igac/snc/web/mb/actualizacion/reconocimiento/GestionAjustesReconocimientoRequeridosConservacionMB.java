package co.gov.igac.snc.web.mb.actualizacion.reconocimiento;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * ManagedBean para el CU 311 de reconocimiento : Gestioner Ajustes de Reconocimiento Requeridos Por
 * Conservacion
 *
 * @author andres.eslava
 *
 */
@Component("GestionAjustesReconocimientoRequeridosConservacion")
@Scope("session")
public class GestionAjustesReconocimientoRequeridosConservacionMB extends
    SNCManagedBean {

    /**
     * Atributo de serialización
     */
    private static final long serialVersionUID = 1L;

    @PostConstruct
    public void init() {

    }

}
