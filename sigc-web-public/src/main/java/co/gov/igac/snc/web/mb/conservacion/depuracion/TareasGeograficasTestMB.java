/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.Actividades;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para revisar tareas que estan en actividades geograficas.
 *
 * @author felipe.cadena
 *
 */
@Component("testTareasGeograficas")
@Scope("session")
public class TareasGeograficasTestMB extends ProcessFlowManager {

    private static final long serialVersionUID = 5608198030936651458L;
    private static final Logger LOGGER =
        LoggerFactory.getLogger(EstudioAjusteEspacialMB.class);
    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    @Autowired
    private IContextListener contextService;
    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;
    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentProcessActivity;
    /**
     * Tramite relacionado a la actividad actual
     */
    private Tramite tramiteActual;
    /**
     * Predios relacionados al tramite actual
     */
    private List<Tramite> tramites;

    /**
     * Rol del usuario actul.
     */
    private String rol;

    private String transicion;

    private String applicationClientName;
    @Autowired
    private IContextListener contextoWeb;

    private List<Actividad> actividades;

    //---------------------Getters and Setters---------------------//
    public Tramite getTramiteActual() {
        return tramiteActual;
    }

    public void setTramiteActual(Tramite tramiteActual) {
        this.tramiteActual = tramiteActual;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public List<Tramite> getTramites() {
        return tramites;
    }

    public void setTramites(List<Tramite> tramites) {
        this.tramites = tramites;
    }

    @PostConstruct
    public void init() {

        LOGGER.debug("on TareasGeograficasTestMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "testTareasGeograficas");

        this.tramiteActual = new Tramite();

        this.actividades = new ArrayList<Actividad>();

        List<Actividad> acts = this.getProcesosService().
            consultarListaActividadesFiltroNombreActividad(
                this.currentUser,
                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION);

        this.actividades.addAll(acts);
        acts = this.getProcesosService().consultarListaActividadesFiltroNombreActividad(
            this.currentUser, ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA);

        this.actividades.addAll(acts);
        acts = this.getProcesosService().consultarListaActividadesFiltroNombreActividad(
            this.currentUser, ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR);

        this.actividades.addAll(acts);

        this.tramites = new ArrayList<Tramite>();

        for (Actividad actividad : actividades) {
            Tramite t = this.cargarDatosTramite(actividad.getIdObjetoNegocio());
            this.tramites.add(t);
        }

        this.determinarRol();

    }

    //--------------------------------------------------------------------------------------------------
    //----------- bpm
    /**
     *
     */
    @Implement
    public boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------

    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO message = new ActivityMessageDTO();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();
        String transitionName = null;

        usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
            this.currentUser.getDescripcionEstructuraOrganizacional(),
            ERol.RESPONSABLE_SIG);

        transitionName = this.transicion;

        this.currentProcessActivity = this.buscarActividadPorIdTramite(this.tramiteActual.getId());

        message.setActivityId(this.currentProcessActivity.getId());

        solicitudCatastral.setUsuarios(usuariosActividad);
        message.setTransition(transitionName);
        solicitudCatastral.setTransicion(transitionName);

        message.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(message);

        if (this.getMessage().getComment() != null) {
            message.setComment(this.getMessage().getComment());
        }

    }
//--------------------------------------------------

    /**
     *
     */
    @Implement
    public void doDatabaseStatesUpdate() {
        // do nothing
    }

    /**
     * Método para cargar los datos relacionados al tramite que se deben mostrar.
     *
     * @author felipe.cadena
     */
    public Tramite cargarDatosTramite(Long tramiteId) {
        LOGGER.debug("Cargando datos tramite...");

        Tramite tramite = this.getTramiteService().
            findTramitePrediosSimplesByTramiteId(tramiteId);

        LOGGER.debug("Fin carga datos...");

        return tramite;
    }

    /**
     * Permite la búsqueda de un trámite en la lista de actividades.
     *
     * @author alejandro.sanchez
     */
    public Actividad buscarActividadPorIdTramite(Long idTramite) {
        if (idTramite != null) {
            for (Actividad a : this.actividades) {
                if (idTramite.equals(a.getIdObjetoNegocio())) {
                    return a;
                }
            }

        }
        return null;
    }

    /**
     * Método para determinar el tipo de rol de la actividad y en base a eso determinar el tipo de
     * funcionario que se va a asignar.
     *
     * @author felipe.cadena
     */
    public void determinarRol() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#determinarRol ");

        String[] roles = this.currentUser.getRoles();
        for (int i = 0; i < roles.length; i++) {
            if (roles[i].equals(ERol.EJECUTOR_TRAMITE.toString())) {

                this.rol = ERol.EJECUTOR_TRAMITE.toString();
                this.transicion =
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION;

            }
            if (roles[i].equals(ERol.DIGITALIZADOR_DEPURACION.toString())) {

                this.rol = ERol.DIGITALIZADOR_DEPURACION.toString();
                this.transicion =
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION;

            }
            if (roles[i].equals(ERol.TOPOGRAFO.toString())) {

                this.rol = ERol.TOPOGRAFO.toString();
                this.transicion =
                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA;

            }
        }

    }

    /**
     * Método para avanzar el proceso.
     *
     * @author felipe.cadena
     */
    public String avanzarProceso() {
        LOGGER.debug("Avanzando proceso...");
        this.setupProcessMessage();
        this.forwardProcess();

        //D: hay que hacer esto aquí porque no se puede factorizar. (me da pereza explicar por qué)
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;

    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando EstudioAjusteEspacialMB#cerrar");

        UtilidadesWeb.removerManagedBean("estudiarAjusteEspacial");

        LOGGER.debug("finalizando EstudioAjusteEspacialMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
