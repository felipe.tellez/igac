package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del CU-SA-AC-064 Generar Reporte de Control de Calidad de Territorial
 *
 * @author rodrigo.hernandez
 * @cu CU-SA-AC-064
 */
@Component("generarReporteControlCalidadTerritorial")
@Scope("session")
public class GeneracionReporteControlCalidadTerritorialMB extends
    SNCManagedBean {

    private static final long serialVersionUID = -2968371204581544324L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneracionReporteControlCalidadTerritorialMB.class);

    /*
     * -------------- ATRIBUTOS --------------------------------
     */
    /**
     * Avalúo asociado a la consulta de controles de calidad
     */
    private Avaluo avaluo;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;
    /**
     * Revisión asociada al oficio remisorio
     */
    private ControlCalidadAvaluoRev revisionCC;
    /**
     * Fecha en que se realiza el reporte
     */
    private Date fechaReporte;
    /**
     * Documento que representa el oficio genrado.
     */
    private Documento oficioReporte;
    /**
     * predios relacionados al avalúo
     */
    private ArrayList<Predio> prediosAvaluo;
    /**
     * Archivo del reporte de orden de práctica
     */
    private File oficioFile;
    /**
     * Url del archuivo para mostrarse
     */
    private String urlDocumento;
    /**
     * Stream para poder descargar el documento desde PrimeFaces
     */
    private StreamedContent streamDocumento;

    private ReporteDTO reporteDTO;

    /**
     * Director de la territorial donde se realiza el tramite
     */
    private UsuarioDTO directorTerritorial;
    /**
     * Profesional que está realizando el control de calidad a
     */
    private ProfesionalAvaluo profesionalControlCalidad;
    /**
     * la territorial donde se está realizando el avalúo.
     */
    private EstructuraOrganizacional territorialAvaluo;
    /**
     * Bandera para determinar que el oficio ya fue generado.
     */
    private Boolean banderaOficioGenerado;
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();


    /*
     * -------------- SETTERS Y GETTERS--------------------------------
     */
    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ControlCalidadAvaluoRev getRevisionCC() {
        return revisionCC;
    }

    public void setRevisionCC(ControlCalidadAvaluoRev revisionCC) {
        this.revisionCC = revisionCC;
    }

    public Date getFechaReporte() {
        return fechaReporte;
    }

    public void setFechaReporte(Date fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    public Documento getOficioReporte() {
        return oficioReporte;
    }

    public void setOficioReporte(Documento oficioReporte) {
        this.oficioReporte = oficioReporte;
    }

    public ArrayList<Predio> getPrediosAvaluo() {
        return prediosAvaluo;
    }

    public void setPrediosAvaluo(ArrayList<Predio> prediosAvaluo) {
        this.prediosAvaluo = prediosAvaluo;
    }

    public File getOficioFile() {
        return oficioFile;
    }

    public void setOficioFile(File oficioFile) {
        this.oficioFile = oficioFile;
    }

    public String getUrlDocumento() {
        return urlDocumento;
    }

    public void setUrlDocumento(String urlDocumento) {
        this.urlDocumento = urlDocumento;
    }

    public StreamedContent getStreamDocumento() {

        InputStream stream;

        try {
            stream = new FileInputStream(this.oficioFile);
            this.streamDocumento = new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_PDF,
                "Reporte control de calidad de territorial.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte");
        }

        return streamDocumento;
    }

    public void setStreamDocumento(StreamedContent streamDocumento) {
        this.streamDocumento = streamDocumento;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return directorTerritorial;
    }

    public void setDirectorTerritorial(UsuarioDTO directorTerritorial) {
        this.directorTerritorial = directorTerritorial;
    }

    public ProfesionalAvaluo getProfesionalControlCalidad() {
        return profesionalControlCalidad;
    }

    public void setProfesionalControlCalidad(
        ProfesionalAvaluo profesionalControlCalidad) {
        this.profesionalControlCalidad = profesionalControlCalidad;
    }

    public EstructuraOrganizacional getTerritorialAvaluo() {
        return territorialAvaluo;
    }

    public void setTerritorialAvaluo(EstructuraOrganizacional territorialAvaluo) {
        this.territorialAvaluo = territorialAvaluo;
    }

    public Boolean getBanderaOficioGenerado() {
        return banderaOficioGenerado;
    }

    public void setBanderaOficioGenerado(Boolean banderaOficioGenerado) {
        this.banderaOficioGenerado = banderaOficioGenerado;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    // -------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#init");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#iniciarDatos");

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario
                .setCodigoTerritorial(Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar datos del Director de Territorial
     *
     * @author rodrigo.hernandez
     */
    private void cargarDirector() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#cargarCoordinador");

        List<UsuarioDTO> directoresTerritorial = this.getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.territorialAvaluo.getNombre(),
                ERol.DIRECTOR_TERRITORIAL);

        if (!directoresTerritorial.isEmpty()) {
            this.directorTerritorial = directoresTerritorial.get(0);
        }

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#cargarCoordinador");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para llamar a este MB desde otros MB
     *
     * @author rodrigo.hernandez
     */
    public void cargarDesdeOtrosMB(ControlCalidadAvaluoRev revision) {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#cargarDesdeOtrosMB");

        this.revisionCC = revision;
        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(
            this.revisionCC.getControlCalidadAvaluo().getAvaluoId());

        this.profesionalControlCalidad = this.getAvaluosService()
            .obtenerProfesionalAvaluoPorId(
                this.revisionCC.getControlCalidadAvaluo()
                    .getProfesionalAvaluos().getId());
        this.fechaReporte = new Date();
        this.prediosAvaluo = (ArrayList<Predio>) this.getAvaluosService()
            .obtenerPrediosDeAvaluo(this.avaluo.getId());

        this.territorialAvaluo = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                String.valueOf(this.avaluo
                    .getEstructuraOrganizacionalCod()));

        this.cargarDirector();
        this.banderaOficioGenerado = false;

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#cargarDesdeOtrosMB");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para enviar el oficio por correo al coordinador del GIT de avalúos
     *
     * @author felipe.cadena
     */
    public void confirmarOficio() {
        LOGGER.debug("Confirmando oficio");

        this.generarOficio();
        this.generarDocumento();

        this.banderaOficioGenerado = true;

        LOGGER.debug("Oficio confirmado");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para crear el reporte
     *
     * @author rodrigo.hernandez
     */
    private void generarOficio() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#generarOficio");

// TODO::rodrigo.hernandez :: Cambiar la url del reporte cuando esté definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        /*
         * Se crea mapa de parametros para enviarlo al servicio que genera el reporte
         */
        Map<String, String> parameters = new HashMap<String, String>();

// TODO :: rodrigo.hernandez :: Parametros quemados, reemplazar cuando
        // se haya desarrollado el reporte :: 30/10/12
        this.setReporteDTO(this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario));

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#generarOficio");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar documento generado para vista previa
     *
     * @author rodrigo.hernandez
     */
    public void verVistaPrevia() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#cargarReporte");

        PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
            .getManagedBean("previsualizacionDocumento");

        previsualizacionDocMB.setReporteDTO(this.reporteDTO);

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#cargarReporte");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para enviar el oficio por correo a los avaluadores
     *
     * @author rodrigo.hernandez
     */
    public void enviarOficio() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#enviarOficio");

        List<String> destinatarios = new ArrayList<String>();
        boolean notificacionExitosa = false;

        /*
         * Se persiste el documento
         */
        this.guardarReporteDefinitivamente();

        this.oficioReporte = this.getGeneralesService().guardarDocumento(
            this.oficioReporte);

        TramiteDocumento td = new TramiteDocumento();
        td.setDocumento(this.oficioReporte);
        td.setTramite(this.avaluo.getTramite());
        td.setFechaLog(new Date());
        td.setfecha(new Date());
        td.setUsuarioLog(this.usuario.getLogin());

        td = this.getTramiteService().actualizarTramiteDocumento(td);

        /*
         * Se inician los datos que son constantes en el cuerpo del correo
         */
        Object[] datosCorreo = null;

        UsuarioDTO usuarioDirectorTerritorial = this
            .getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(
                this.territorialAvaluo.getNombre(),
                ERol.DIRECTOR_TERRITORIAL).get(0);

        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};

        if (this.oficioReporte != null) {
            adjuntos[0] = this.reporteDTO.getArchivoReporte().getAbsolutePath();
            adjuntosNombres[0] = "Reporte control calidad de territorial.pdf";
        }
        LOGGER.debug("Url archivo " + adjuntos[0]);

        /*
         * Se envía el correo tantas veces como numero de avaluadores tenga el avaluo
         */
        for (ProfesionalAvaluo pa : this.avaluo.getAvaluadores()) {

            /*
             * Se crea la lista de destinatarios
             */
            destinatarios = new ArrayList<String>();
            destinatarios.add(pa.getCorreoElectronico());

            datosCorreo = new Object[6];

            /*
             * Se ajustan los datos del cuerpo del correo
             */
            datosCorreo[0] = pa.getNombreCompleto();
            datosCorreo[1] = this.avaluo.getTramite().getSolicitud().getTipo();
            datosCorreo[2] = this.avaluo.getSecRadicado();
            datosCorreo[3] = this.avaluo.getSolicitante().getNombreCompleto();
            datosCorreo[4] = usuarioDirectorTerritorial.getNombreCompleto();
            datosCorreo[5] = this.territorialAvaluo.getNombre();

            notificacionExitosa = this
                .getGeneralesService()
                .enviarCorreoElectronicoConCuerpo(
                    EPlantilla.MENSAJE_REPORTE_CONTROL_CALIDAD_TERRITORIAL,
                    destinatarios, null, null, datosCorreo, adjuntos,
                    adjuntosNombres);
        }

        if (notificacionExitosa) {
            this.addMensajeInfo("Se envio exitosamente el reporte por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError(
                "No se pudo enviar el reporte de control de calidad de territorial" +
                 " por correo electrónico para uno o varios avaluadores");
            context.addCallbackParam("error", "error");
        }

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#enviarOficio");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para
     *
     * @author rodrigo.hernandez
     */
    private void generarDocumento() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#generarDocumento");

        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(ETipoDocumento.OFICIOS_REMISORIOS.getId());
        tipoDocumento.setNombre(ETipoDocumento.OFICIOS_REMISORIOS.getNombre());

        this.oficioReporte = new Documento();
        this.oficioReporte.setArchivo(this.reporteDTO.getUrlWebReporte());
        this.oficioReporte.setTipoDocumento(tipoDocumento);
        this.oficioReporte.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        this.oficioReporte.setBloqueado(ESiNo.NO.getCodigo());
        this.oficioReporte.setUsuarioCreador(this.usuario.getLogin());
        this.oficioReporte.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        this.oficioReporte.setFechaLog(new Date(System.currentTimeMillis()));
        this.oficioReporte.setUsuarioLog(this.usuario.getLogin());
        this.oficioReporte
            .setDescripcion("Reporte control de calidad de territorial" +
                 this.avaluo.getSecRadicado());

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#generarDocumento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para guardar el documento generado en alfresco
     *
     * @author rodrigo.hernandez
     */
    private void guardarDocumentoAlfresco(TipoDocumento tipoDocumento) {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#guardarDocumentoAlfresco");

        Solicitud solTemp = this.avaluo.getTramite().getSolicitud();

        String urlArchivo = this.reporteDTO.getArchivoReporte()
            .getAbsolutePath();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(
                solTemp.getNumero(), solTemp.getFecha(),
                tipoDocumento.getNombre(), urlArchivo);

        this.oficioReporte = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, this.oficioReporte);

        if (this.oficioReporte == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#guardarDocumentoAlfresco");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para guardar la información del oficio en base de datos y en alfresco.
     *
     * @author rodrigo.hernandez
     */
    private void guardarReporteDefinitivamente() {
        LOGGER.debug(
            "Inicio GeneracionReporteControlCalidadTerritorialMB#guardarReporteDefinitivamente");

        this.guardarDocumentoAlfresco(oficioReporte.getTipoDocumento());

        this.oficioReporte = this.getGeneralesService().guardarDocumento(
            this.oficioReporte);

        TramiteDocumento td = new TramiteDocumento();
        td.setDocumento(this.oficioReporte);
        td.setTramite(this.avaluo.getTramite());
        td.setFechaLog(new Date());
        td.setfecha(new Date());
        td.setUsuarioLog(this.usuario.getLogin());

        td = this.getTramiteService().actualizarTramiteDocumento(td);

        LOGGER.debug(
            "Fin GeneracionReporteControlCalidadTerritorialMB#guardarReporteDefinitivamente");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para reiniciar la variables cuando se cancela la generación del reporte.
     *
     * @author rodrigo.hernandez
     */
    public void cancelarGenaracionOficio() {
        LOGGER.debug("Inicio GeneracionReporteControlCalidadTerritorialMB#cancelarGenaracionOficio");

        this.oficioFile = null;
        this.oficioReporte = null;
        this.banderaOficioGenerado = false;

        LOGGER.debug("Fin GeneracionReporteControlCalidadTerritorialMB#cancelarGenaracionOficio");
    }

}
