package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.DateSelectEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionComision;
import co.gov.igac.snc.persistence.entity.actualizacion.ComisionIntegrante;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.util.EActualizacionComisionObjeto;
import co.gov.igac.snc.persistence.util.EComisionIntegranteEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author lorena.salamanca
 * @modified by franz.gamba
 *
 */
@Component("proyectarMemorandoDeComision")
@Scope("session")
public class ProyeccionMemorandoDeComisionMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    private UsuarioDTO usuario;
    /** Actualizacion en proceso */
    private Actualizacion actualizacion;
    /*
     * Variable usada en el pickList
     */
    private DualListModel<String> funcionariosComisionar;
    private List<RecursoHumano> recursoHumanoActualizacion;

    private List<String> recursoHumanoSinCom;
    private List<String> recursoHumanoPorCom;
    /**
     * Fecha inicial de la comision
     */
    private Date fechaInicial;

    /**
     * Fecha final de la comision
     */
    private Date fechaFinal;
    /** Dias qeu dura la comision */
    private Long diasComision;
    /** Bandera para cuando el recurso fue comisionado */
    private boolean recursoComisionado;

    /**
     * Banderas para las transiciones
     */
    private boolean comisionPreliminar;
    private boolean autorizacionComision;

    /** Lista de objetos para los parametros del correo */
    private Object[] datosCorreo;
    private List<String> correoDestinatarios;
    private List<String> destinatarios;

    /** Format de los mensajes */
    private MessageFormat messageFormat;

    /** String que contiene el texto del correo */
    private String contenido;
    /** String para almacenar los comenatrios adicionales realizados por el Director Territorial */
    private String comentariosAdicionales = new String("");

    private String correoDestinatario;
    private UsuarioDTO directorTerritorial;

    //---------------------------------------------------------------------------------------------
    /*
     * Métodos de acceso
     */
    public DualListModel<String> getFuncionariosComisionar() {
        return funcionariosComisionar;
    }

    public void setFuncionariosComisionar(DualListModel<String> funcionariosComisionar) {
        this.funcionariosComisionar = funcionariosComisionar;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public TareasPendientesMB getTareasPendientesMB() {
        return tareasPendientesMB;
    }

    public void setTareasPendientesMB(TareasPendientesMB tareasPendientesMB) {
        this.tareasPendientesMB = tareasPendientesMB;
    }

    public Actividad getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Actividad currentActivity) {
        this.currentActivity = currentActivity;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<RecursoHumano> getRecursoHumanoActualizacion() {
        return recursoHumanoActualizacion;
    }

    public void setRecursoHumanoActualizacion(
        List<RecursoHumano> recursoHumanoActualizacion) {
        this.recursoHumanoActualizacion = recursoHumanoActualizacion;
    }

    public List<String> getRecursoHumanoSinCom() {
        return recursoHumanoSinCom;
    }

    public void setRecursoHumanoSinCom(List<String> recursoHumanoSinCom) {
        this.recursoHumanoSinCom = recursoHumanoSinCom;
    }

    public List<String> getRecursoHumanoPorCom() {
        return recursoHumanoPorCom;
    }

    public void setRecursoHumanoPorCom(List<String> recursoHumanoPorCom) {
        this.recursoHumanoPorCom = recursoHumanoPorCom;
    }

    public Long getDiasComision() {
        return diasComision;
    }

    public void setDiasComision(Long diasComision) {
        this.diasComision = diasComision;
    }

    public boolean isRecursoComisionado() {
        return recursoComisionado;
    }

    public void setRecursoComisionado(boolean recursoComisionado) {
        this.recursoComisionado = recursoComisionado;
    }

    public boolean isComisionPreliminar() {
        return comisionPreliminar;
    }

    public void setComisionPreliminar(boolean comisionPreliminar) {
        this.comisionPreliminar = comisionPreliminar;
    }

    public boolean isAutorizacionComision() {
        return autorizacionComision;
    }

    public void setAutorizacionComision(boolean autorizacionComision) {
        this.autorizacionComision = autorizacionComision;
    }

    public Object[] getDatosCorreo() {
        return datosCorreo;
    }

    public void setDatosCorreo(Object[] datosCorreo) {
        this.datosCorreo = datosCorreo;
    }

    public MessageFormat getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(MessageFormat messageFormat) {
        this.messageFormat = messageFormat;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getComentariosAdicionales() {
        return comentariosAdicionales;
    }

    public void setComentariosAdicionales(String comentariosAdicionales) {
        this.comentariosAdicionales = comentariosAdicionales;
    }

    public String getCorreoDestinatario() {
        return correoDestinatario;
    }

    public void setCorreoDestinatario(String correoDestinatario) {
        this.correoDestinatario = correoDestinatario;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return directorTerritorial;
    }

    public void setDirectorTerritorial(UsuarioDTO directorTerritorial) {
        this.directorTerritorial = directorTerritorial;
    }

    public List<String> getCorreoDestinatarios() {
        return correoDestinatarios;
    }

    public void setCorreoDestinatarios(List<String> correoDestinatarios) {
        this.correoDestinatarios = correoDestinatarios;
    }

    public List<String> getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(List<String> destinatarios) {
        this.destinatarios = destinatarios;
    }

    //---------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

//		this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();
//		
//		Long idActualizacion = this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();
        Long idActualizacion = 450L;
        this.actualizacion = this.getActualizacionService().
            getActualizacionConResponsables(idActualizacion);

        //TODO franz.gamba:: Cambiar esto para cuando se este realizando la integracion
//		if(this.currentActivity.getNombre().
//				equals(ProcesoDeActualizacion.ACT_ALISTAMIENTO_PROY_COMISION_RECORRER_MUN)){
//			this.comisionPreliminar = true;
//			this.autorizacionComision = false;
//		}else if(this.currentActivity.getNombre().
//				equals(ProcesoDeActualizacion.ACT_ALISTAMIENTO_APROBAR_COMISION_RECORRER_MUN)){
//			this.comisionPreliminar = false;
//			this.autorizacionComision = true;
//		}
        this.alimentarPickList();

        this.directorTerritorial = this.getTramiteService().
            buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionTerritorial(), ERol.DIRECTOR_TERRITORIAL).get(0);
//		if(this.autorizacionComision){
        this.comisionPreliminar = true;
        if (false) {
            List<ComisionIntegrante> integrantes = this.getActualizacionService().
                obtenerComisionIntegrantesByActualizacionIdYObjeto(idActualizacion,
                    EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.getCodigo() +
                     this.actualizacion.getMunicipio().getNombre());
            if (integrantes == null || integrantes.isEmpty()) {
                this.addMensajeWarn("La comisión no tiene integrantes asignados aún.");
            } else if (integrantes != null && !integrantes.isEmpty()) {
                for (ComisionIntegrante ci : integrantes) {
                    if (!this.funcionariosComisionar.getTarget().
                        contains(ci.getRecursoHumano().getNombre())) {
                        this.funcionariosComisionar.getTarget().add(ci.getRecursoHumano().
                            getNombre());
                    }
                }
                List<String> nuevoSource = new ArrayList<String>();
                for (String s : this.funcionariosComisionar.getSource()) {
                    if (!this.funcionariosComisionar.getTarget().contains(s)) {
                        nuevoSource.add(s);
                    }
                }
                this.funcionariosComisionar.setSource(nuevoSource);

            }
        }

    }

    //---------------------------------------------------------------------------------------------
    /*
     * Método que carga la lista de recursos humanos para enviar a comisión
     */
    public void alimentarPickList() {
        // TODO : franz.gamba :: validar si siempre es el responsable No.1 en la
        // lista de responsables
        this.recursoHumanoSinCom = new LinkedList<String>();
        this.recursoHumanoPorCom = new LinkedList<String>();

        for (int i = 0; i < this.actualizacion.getActualizacionResponsables()
            .size(); i++) {

            Long idResponsableAct = this.actualizacion
                .getActualizacionResponsables().get(i).getId();

            this.recursoHumanoActualizacion = this.getActualizacionService()
                .getRecursoHumanoByResponsableActId(idResponsableAct);

            for (RecursoHumano rh : this.recursoHumanoActualizacion) {
                this.recursoHumanoSinCom.add(rh.getNombre());
            }

        }
        this.funcionariosComisionar = new DualListModel<String>(
            this.recursoHumanoSinCom, this.recursoHumanoPorCom);
    }

    //---------------------------------------------------------------------------------------------
    public void validarComisionPreliminar() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.fechaFinal != null &&
             this.fechaFinal.before(this.fechaInicial)) {
            String mensaje = "La fecha final no puede ser menor a la fecha inicial." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        } else {
            // Duracion de la comision en dias
            Long duracion = (this.fechaFinal.getTime() - this.fechaInicial
                .getTime()) / (1000 * 60 * 60 * 24);
            if (this.funcionariosComisionar.getTarget() != null &&
                 !this.funcionariosComisionar.getTarget().isEmpty()) {
                // Se crea la comision
                ActualizacionComision comision = new ActualizacionComision();
                comision.setActualizacion(this.actualizacion);
                comision.setDuracion(duracion.doubleValue());
                comision.setFechaFin(this.fechaFinal);
                comision.setFechaInicio(this.fechaFinal);
                comision.setFechaLog(new Date());
                comision.setObjeto(EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.
                    getCodigo() +
                     this.actualizacion.getMunicipio().getNombre());
                comision.setUsuarioLog(this.usuario.getLogin());

                // persistencia de la comision
                comision = this.getActualizacionService()
                    .actualizarActualizacionComision(comision);

                List<ComisionIntegrante> comisionIntegrantes = new LinkedList<ComisionIntegrante>();
                for (String s : this.funcionariosComisionar.getTarget()) {
                    for (RecursoHumano rh : this.recursoHumanoActualizacion) {
                        if (rh.getNombre().equals(s)) {

                            ComisionIntegrante integrante = new ComisionIntegrante();
                            integrante.setActualizacionComision(comision);
                            integrante.setFechaLog(new Date());
                            integrante.setEstado(EComisionIntegranteEstado.ORIGINAL.toString());
                            integrante.setUsuarioLog(this.usuario.getLogin());
                            integrante.setRecursoHumano(rh);

                            comisionIntegrantes.add(integrante);
                        }
                    }
                }
                this.generarCorreo(comisionIntegrantes);
                // persistencia de la liste de comision integrantes
                comisionIntegrantes = this.getActualizacionService()
                    .actualizarComisionIntegrantes(comisionIntegrantes);

                comision.setComisionIntegrantes(comisionIntegrantes);
                comision = this.getActualizacionService()
                    .actualizarActualizacionComision(comision);

                this.recursoComisionado = true;
                //this.generarCorreo();
                this.addMensajeInfo(
                    "Los funcionarios y/o contratistas fueron asignados a la comision éxitosamente.");
            } else {
                this.addMensajeError("Debe seleccionar por lo menos un funcionario o contratista.");
            }

        }
    }
    //--------------------------------------------------------------------------------------------

    /**
     * Método que calcula los dias entre dos fechas para comisionar
     */

    public void onDataSelectCalendar(DateSelectEvent event) {
        if (this.fechaFinal != null) {
            if (this.fechaInicial != null) {
                this.diasComision = (this.fechaFinal.getTime() - this.fechaInicial
                    .getTime()) / Constantes.MILISEG_POR_DIA;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que finaliza la autorizacion de la comision y avanza el proceso
     */
    public void autorizarComisionRecurso() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.fechaFinal != null &&
             this.fechaFinal.before(this.fechaInicial)) {
            String mensaje = "La fecha final no puede ser menor a la fecha inicial." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        } else {
            // Duracion de la comision en dias
            Long duracion = (this.fechaFinal.getTime() - this.fechaInicial
                .getTime()) / (1000 * 60 * 60 * 24);
            if (this.funcionariosComisionar.getTarget() != null &&
                !this.funcionariosComisionar.getTarget().isEmpty()) {

                ActualizacionComision comision =
                    this.getActualizacionService().
                        obtenerActComisionPoractualizacionIdYObjeto(this.actualizacion.getId(),
                            EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.getCodigo() +
                             this.actualizacion.getMunicipio().getNombre());

                if (comision != null) {

                    comision.setDuracion(duracion.doubleValue());
                    comision.setFechaFin(this.fechaFinal);
                    comision.setFechaInicio(this.fechaFinal);

                    List<ComisionIntegrante> integrantes = this.getActualizacionService()
                        .obtenerComisionIntegrantesByActualizacionIdYObjeto(
                            this.actualizacion.getId(),
                            EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.getCodigo());

                    if (integrantes != null && !integrantes.isEmpty()) {
                        List<ComisionIntegrante> integrantesFinales =
                            new ArrayList<ComisionIntegrante>();
                        List<String> inteNombres = new ArrayList<String>();
                        //Selecciona los comisión integrantes que ya existian para la comision y 
                        //fueron nuevamente seleccionados
                        for (ComisionIntegrante ci : integrantes) {
                            boolean seleccionado = false;
                            for (String nombre : this.funcionariosComisionar.getTarget()) {
                                if (ci.getRecursoHumano().getNombre().equals(nombre)) {
                                    integrantesFinales.add(ci);
                                    inteNombres.add(nombre);
                                    seleccionado = true;
                                }
                            }
                            if (!seleccionado) {
                                ci.setEstado(EComisionIntegranteEstado.RETIRADO.toString());
                                ci = this.getActualizacionService().
                                    actualizarComisionIntegrante(ci);
                            }
                        }
                        // Se generan los Comision integrantes a partir de los nuevos funcionarios
                        // y/o contratistas seleccionados en esta transición
                        for (String s : this.funcionariosComisionar.getTarget()) {
                            if (!inteNombres.contains(s)) {
                                for (RecursoHumano rh : this.recursoHumanoActualizacion) {
                                    if (rh.getNombre().equals(s)) {

                                        ComisionIntegrante integrante = new ComisionIntegrante();
                                        integrante.setActualizacionComision(comision);
                                        integrante.setFechaLog(new Date());
                                        integrante.setEstado(EComisionIntegranteEstado.ADICIONADO.
                                            toString());
                                        integrante.setUsuarioLog(this.usuario.getLogin());
                                        integrante.setRecursoHumano(rh);

                                        integrantesFinales.add(integrante);
                                    }
                                }
                            }
                        }
                        this.generarCorreo(integrantesFinales);
                    } else {
                        this.addMensajeError(
                            "No se encontraron integrantes para la comision seleccionados" +
                            " el na revision previa del responsable.");
                    }

                } else {
                    this.addMensajeError("Ocurrió un error a cargar la comisión de " +
                        "recorrido preliminar para este municipio.");
                }

            } else if (this.funcionariosComisionar.getTarget() == null ||
                this.funcionariosComisionar.getTarget().isEmpty()) {
                this.addMensajeError("Debe seleccionar por lo menos un funcionario " +
                    "o contratista para autorizar la comision.");
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que inicializa los parametros del correo
     */
    public void consolidarDatosCorreo(List<String> destinatarios, List<String> correoDestinatarios) {
        Calendar hoy = Calendar.getInstance();
        Date fecha = new java.util.Date(hoy.getTimeInMillis());

        this.datosCorreo = new Object[7];
        this.datosCorreo[0] = this.directorTerritorial.getLogin() +
             ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC;
        this.datosCorreo[1] = destinatarios.toString();
        this.datosCorreo[2] = correoDestinatarios.toString();
        this.datosCorreo[3] = fecha;
        this.datosCorreo[4] = this.actualizacion.getMunicipio().getNombre();
        this.datosCorreo[5] = this.directorTerritorial.getNombreCompleto();

    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que genera el contendido del correo
     *
     */
    public void generarCorreo(List<ComisionIntegrante> integrantes) {

//		List<ComisionIntegrante> integrantes = this.getActualizacionService()
//				.obtenerComisionIntegrantesByActualizacionIdYObjeto(
//						this.actualizacion.getId(), 
//						EActualizacionComisionObjeto.COMISION_RECORRIDO_PRELIMINAR.getCodigo()
//						+ this.actualizacion.getMunicipio().getNombre());
        List<String> destinatarios = null;
        List<String> correoDestinatarios = null;

        if (integrantes != null && !integrantes.isEmpty()) {
            for (ComisionIntegrante ci : integrantes) {
                if ((destinatarios == null || destinatarios.isEmpty()) &&
                    (correoDestinatarios == null || correoDestinatarios.isEmpty())) {
                    destinatarios = new ArrayList<String>();
                    correoDestinatarios = new ArrayList<String>();

                    destinatarios.add(ci.getRecursoHumano().getNombre());
                    correoDestinatarios.add(ci.getRecursoHumano().getNombre() +
                        
                        ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC);
                } else if ((destinatarios != null && !destinatarios.isEmpty()) &&
                    (correoDestinatarios != null && !correoDestinatarios.isEmpty())) {
                    destinatarios.add(ci.getRecursoHumano().getNombre());
                    correoDestinatarios.add(ci.getRecursoHumano().getNombre() +
                        
                        ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC);
                }
            }
        }

        this.consolidarDatosCorreo(destinatarios, correoDestinatarios);

        Plantilla plantilla = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_RRHH_COMISION_RECORRIDO_PRELIMINAR.getCodigo());
        if (null != plantilla) {
            contenido = plantilla.getHtml();
            if (null == contenido) {
                super.addMensajeError("Imposible recuperar la plantilla: " +
                     EPlantilla.MENSAJE_RRHH_COMISION_RECORRIDO_PRELIMINAR.getCodigo());
            } else {

                Locale colombia = new Locale("ES", "es_CO");
                this.messageFormat = new MessageFormat(this.contenido, colombia);

                this.contenido = this.messageFormat.format(this.datosCorreo);
            }
        }

    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Método que envia el correo a los destinatarios
     */
    public void enviarCorreo() {
        StringBuilder destinatarios = null;

        for (String s : this.correoDestinatarios) {
            if (destinatarios == null) {
                destinatarios = new StringBuilder();
                destinatarios.append(s);
            } else {
                destinatarios.append(",");
                destinatarios.append(s);
            }
        }

        this.getGeneralesService().enviarCorreo(destinatarios.toString(),
            "Correo comision preliminar a " + this.actualizacion.getMunicipio().getNombre(),
            this.contenido, new String[0], new String[0]);
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Método que avanza el proceso y cierra la pantalla
     */
    public String avanzarProceso() {

        //TODO franz.gamba:: Prendiente de implementar aca las transiciones con el process
        return this.cerrar();
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Método que cierra la pantalla
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("proyectarMemorandoDeComision");
        this.tareasPendientesMB.init();
        return "index";
    }
}
