package co.gov.igac.snc.web.util;

/**
 * @author ariel.ortiz
 *
 * @description Enumeracion de las posibles escalas a las cuales se puede visualizar una plancha hay
 * que tener en cuenta que si el dato de zona es urbano entonces el dato a mostrar es 1:2000, si el
 * dato es de una zona rural entonces se deben mostrar las opciones de escala 1:5000 y 1:10000
 *
 * @cu CU-TV-0F-05
 *
 * @version 1.0
 */
public enum EValoresEscala {

    ESCALA_1_2000("2000"),
    ESCALA_1_5000("5000"),
    ESCALA_1_10000("10000");

    private String escala;

    private EValoresEscala(String escalaSeleccion) {
        this.escala = escalaSeleccion;
    }

    @Override
    public String toString() {
        return this.escala;
    }

}
