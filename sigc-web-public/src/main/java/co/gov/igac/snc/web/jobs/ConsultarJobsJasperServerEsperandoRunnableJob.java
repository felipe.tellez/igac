package co.gov.igac.snc.web.jobs;

import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.controller.AbstractLocator;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Este Job realiza las siguientes operaciones: Consulta los jobs que enviaron a ejecución en
 * JasperServer hace más de n horas y que todavía siguen esperando
 *
 * Envía los jobs encontrados para que intente volver a enviarlos. Para tal fin se utiliza una tarea
 * ReenviarJobTask. Dicha tarea se ejecuta en su propio hilo buscando mayor paralelismo. ( Los
 * threads son controlados por Spring )
 *
 *
 * @author javier.aponte
 *
 */
public class ConsultarJobsJasperServerEsperandoRunnableJob extends AbstractLocator implements
    Runnable, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(
        ConsultarJobsJasperServerEsperandoRunnableJob.class);

    private TaskExecutor taskExecutor;

    private Integer numeroJobs;

    public void setNumeroJobs(Integer numeroJobs) {
        this.numeroJobs = numeroJobs;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void run() {
        LOGGER.debug("Consulta de jobs de JasperServer esperando Runnable");
        try {
            List<ProductoCatastralJob> jobsPendientes = this.getGeneralesService().
                obtenerJobsJasperServerEsperando(numeroJobs);

            if (jobsPendientes != null && jobsPendientes.size() > 0) {
                for (ProductoCatastralJob job : jobsPendientes) {

                    LOGGER.debug("execute Consulta de jobs de JasperServer esperando Task" +
                         "job id: " + job.getId());
                    //envía los jobs a la cola (Dicha cola tiene un pool de instancias que se ejecutan en paralelo )
                    this.taskExecutor.execute(new ConsultarJobsJasperServerEsperandoTask(job));
                }
            } else {
                LOGGER.info("No hay jobs de JasperServer esperando");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
