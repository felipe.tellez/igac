/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * MB para el caso de uso CU-SA-AC-085 Administrar Valores Comprometidos de avalúos en Proceso
 *
 * @cu CU-SA-AC-85
 * @author felipe.cadena
 */
@Component("administrarValoresComprometidos")
@Scope("session")
public class AdministracionValoresComprometidosMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = 5901352537945679767L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaAdicionesContratoActivoMB.class);

    /**
     * Información acerca del avaluador
     */
    private VProfesionalAvaluosContrato avaluador;

    /**
     * Valor total comprometido, la suma del valor comprometido de todos los avaluos relacionados al
     * avaluador
     */
    private Double valorTotalComprometido;

    /**
     * Valor IVA del valor total comprometido
     */
    private Double valorTotalIvaComprometido;

    /**
     * Avalúo seleccionado para la modificación
     */
    private Avaluo avaluoSeleccionado;

    /**
     * Valor del iva, sugun paametro del sistema.
     */
    private Double iva;

    /**
     * Lista de avaluos relacionados al avaluador
     */
    private List<Avaluo> avaluosEnProceso;

    // --------getters-setters----------------
    public VProfesionalAvaluosContrato getAvaluador() {
        return avaluador;
    }

    public void setAvaluador(VProfesionalAvaluosContrato avaluador) {
        this.avaluador = avaluador;
    }

    public Avaluo getAvaluoSeleccionado() {
        return avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public Double getValorTotalComprometido() {
        return valorTotalComprometido;
    }

    public void setValorTotalComprometido(Double valorTotalComprometido) {
        this.valorTotalComprometido = valorTotalComprometido;
    }

    public Double getValorTotalIvaComprometido() {
        return valorTotalIvaComprometido;
    }

    public void setValorTotalIvaComprometido(Double volorTotalIvaComprometido) {
        this.valorTotalIvaComprometido = volorTotalIvaComprometido;
    }

    public List<Avaluo> getAvaluosEnProceso() {
        return avaluosEnProceso;
    }

    public void setAvaluosEnProceso(List<Avaluo> avaluosEnProceso) {
        this.avaluosEnProceso = avaluosEnProceso;
    }

    // -------Metodos-----------
    /**
     * Método para hacer el llamado de este caso de uso desde el CU-SA-AC-051
     */
    public void cargarDesdeCU051(VProfesionalAvaluosContrato avaluador) {

        LOGGER.debug("Cargando datos desde caso de uso externo...");

        this.iva = this.getGeneralesService()
            .getCacheParametroPorNombre(EParametro.IVA.toString())
            .getValorNumero();

        FiltroDatosConsultaProfesionalAvaluos fa = new FiltroDatosConsultaProfesionalAvaluos();
        fa.setNumeroIdentificacion(avaluador.getNumeroIdentificacion());
        List<VProfesionalAvaluosContrato> la = this.getAvaluosService()
            .buscarAvaluadoresPorFiltro(fa);

        if (!la.isEmpty()) {
            this.avaluador = la.get(0);
        }
        this.buscarAvaluosProceso();
        this.actualizarTotales();

    }

    public void buscarAvaluosProceso() {
        LOGGER.debug("Buscando avaluos...");

        this.avaluosEnProceso = this.getAvaluosService().
            consultarAvaluosEnProcesoAvaluador(this.avaluador.getProfesionalAvaluosId());

        LOGGER.debug("Busqueda terminada");
    }

    public void actualizarTotales() {
        LOGGER.debug("Actualizando totales...");
        this.valorTotalComprometido = 0d;
        for (Avaluo avaluo : this.avaluosEnProceso) {
            this.valorTotalComprometido += avaluo.getValorPreliminar();
        }

        this.valorTotalIvaComprometido = this.valorTotalComprometido * this.iva;

        LOGGER.debug("Totales actualizados...");
    }

    /**
     * Método para modificar el valor comprometido de un avalúo y actualizar datos relacionados con
     * dicho valor.
     *
     * @author felipe.cadena
     */
    public void modificarValorComprometido() {
        LOGGER.debug("Actualizando valores...");

        this.avaluoSeleccionado.
            setValorIva(this.iva * this.avaluoSeleccionado.getValorPreliminar());
        this.getAvaluosService().actualizarAvaluo(avaluoSeleccionado);

        this.actualizarTotales();

        LOGGER.debug("Valores actualizados");
    }

}
