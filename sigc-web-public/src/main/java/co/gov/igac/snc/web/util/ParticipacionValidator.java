package co.gov.igac.snc.web.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;

/**
 *
 * @author fredy.wilches Clase validador para los porcentajes de participación
 */
public class ParticipacionValidator implements Validator {

    public void validate(FacesContext context, UIComponent toValidate,
        Object value) {
        if (value != null) {
            String p = (String) value;
            if (!p.equals("")) {
                try {
                    Double d = new Double(p);
                    if (d.doubleValue() <= 0 || d.doubleValue() > 100) {
                        ((UIInput) toValidate).setValid(false);
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Porcentaje de participación debe ser mayor a 0 y menor que 100", null);
                        context.addMessage(toValidate.getClientId(context), message);
                    }
                } catch (NumberFormatException nfe) {
                    ((UIInput) toValidate).setValid(false);
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Porcentaje de participación inválido", null);
                    context.addMessage(toValidate.getClientId(context), message);
                }
            }
        }
    }
}
