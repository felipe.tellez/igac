/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.apache.commons.io.FileUtils;

/**
 *
 * Managed bean del cu Generar Documento de Liquidación Costo Avalúo
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-029 Generar Documento de Liquidación Costo Avalúo
 *
 * @modified felipe.cadena Se cambia la forma de manejar los al archivo para que funcione con el
 * ftp.
 */
@Component("generacionDocLiquidacionAvaluo")
@Scope("session")
public class GeneracionDocumentoLiquidacionCostoAvaluoMB extends SNCManagedBean {

    private static final long serialVersionUID = -8798721179674715931L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneracionDocumentoLiquidacionCostoAvaluoMB.class);

    /**
     * Lista con los {@link Avaluo} que se van a liquidar
     */
    private List<Avaluo> avaluosALiquidar;

    /**
     * Territorial asociada al usuario logeado en el sistema
     */
    private EstructuraOrganizacional territorial;

    /**
     * Corresponde a la fecha del sistema
     */
    private Date fechaOficio;

    /**
     * Usuario coordinador GIT de avaluos ldap
     */
    private UsuarioDTO coordinadorGITAvaluos;

    /**
     * Usuario jefe de division financiera ldap
     */
    private UsuarioDTO jefeDivisionFinanciera;

    /**
     * Usuario activo en el sistema
     */
    private UsuarioDTO usuarioActivo;

    /**
     * {@link Solicitud} asociada a los {@link #avaluosALiquidar}
     */
    private Solicitud solicitud;

    /**
     * Variable para guardar los anexos del documento
     */
    private String anexos;

    /**
     * {@link ContratoInteradministrativo} asociado al {@link Solicitante} de la {@link #solicitud}
     */
    private ContratoInteradministrativo contratoInteradministrativo;

    /**
     * SOolicitante asociado a la {@link #solicitud}
     */
    private SolicitanteSolicitud solicitanteSolicitud;

    /**
     * costo del servicio, se traduce en la suma de los honorarios IGAC de los avalúos a liquidar
     */
    private Double costoServicio;

    // --------------------------------Reportes---------------------------------
    @Autowired
    protected IContextListener contextoWeb;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Documento con el documento de liquidación
     */
    private Documento liquidacionDocumento;

    /**
     * url publica del documento de la liquidacion
     */
    private String liquidacionDocumentoURL;

    /**
     * Archivo del reporte de la liquidacion
     */
    private File liquidacionDocumentoFile;

    private ReporteDTO liquidacionDocumentoDTO;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public List<Avaluo> getAvaluosALiquidar() {
        return this.avaluosALiquidar;
    }

    public EstructuraOrganizacional getTerritorial() {
        return this.territorial;
    }

    public ReporteDTO getLiquidacionDocumentoDTO() {
        return liquidacionDocumentoDTO;
    }

    public void setLiquidacionDocumentoDTO(ReporteDTO liquidacionDocumentoDTO) {
        this.liquidacionDocumentoDTO = liquidacionDocumentoDTO;
    }

    public Date getFechaOficio() {
        this.fechaOficio = new Date();
        return this.fechaOficio;
    }

    public UsuarioDTO getCoordinadorGITAvaluos() {
        return this.coordinadorGITAvaluos;
    }

    public UsuarioDTO getJefeDivisionFinanciera() {
        return this.jefeDivisionFinanciera;
    }

    public UsuarioDTO getUsuarioActivo() {
        return this.usuarioActivo;
    }

    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public String getAnexos() {
        return this.anexos;
    }

    public void setAnexos(String anexos) {
        this.anexos = anexos;
    }

    public ContratoInteradministrativo getContratoInteradministrativo() {
        return this.contratoInteradministrativo;
    }

    public SolicitanteSolicitud getSolicitanteSolicitud() {
        return this.solicitanteSolicitud;
    }

    public Double getCostoServicio() {
        return this.costoServicio;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public StreamedContent getLiquidacionDocumento() {

        return this.liquidacionDocumentoDTO.getStreamedContentReporte();
    }

    // -----------------------Métodos SET y GET banderas------------------------
    // ---------------------------------Métodos---------------------------------
    // -------------------------------------------------------------------------
    /**
     * Método que debe ser usado cuando se quiera iniciar este caso de uso desde otro
     *
     * @author christian.rodriguez
     * @param avaluosALiquidar lista de los avalúos a liquidar
     */
    public void cargarDesdeOtrosCU(List<Avaluo> avaluosALiquidar) {

        this.usuarioActivo = MenuMB.getMenu().getUsuarioDto();

        this.territorial = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                this.usuarioActivo.getCodigoTerritorial());

        if (avaluosALiquidar != null && !avaluosALiquidar.isEmpty()) {

            this.avaluosALiquidar = avaluosALiquidar;

            this.solicitud = avaluosALiquidar.get(0).getTramite()
                .getSolicitud();

            this.solicitanteSolicitud = this.solicitud.getSolicitante();

            this.obtenerCoordinadorGITAvaluos();
            this.obtenerJefeDivisionFinanciera();
            this.calcularCostoServicio();
            this.obtenerContratoInteradministrativoSolicitante();
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Calcula el costo del servicio (suma de los honorarios IGAC de los {@link #avaluosALiquidar}
     *
     * @author christian.rodriguez
     */
    private void calcularCostoServicio() {
        this.costoServicio = new Double(0.0);
        for (Avaluo avaluo : this.avaluosALiquidar) {
            if (avaluo.getValorHonorariosIGAC() != null) {
                this.costoServicio += avaluo.getValorHonorariosIGAC();
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que consulta el coordinador GIT de avaluos asociado a la territorial del
     * {@link #usuarioActivo}
     *
     * @author christian.rodriguez
     */
    private void obtenerCoordinadorGITAvaluos() {

        if (this.territorial != null) {
            List<UsuarioDTO> coordinadoresGITAvaluos = this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                    ERol.COORDINADOR_GIT_AVALUOS);

            if (coordinadoresGITAvaluos != null &&
                 !coordinadoresGITAvaluos.isEmpty()) {
                this.coordinadorGITAvaluos = coordinadoresGITAvaluos.get(0);
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que consulta el coordinador GIT de avaluos asociado a la territorial del
     * {@link #usuarioActivo}
     *
     * @author christian.rodriguez
     */
    private void obtenerJefeDivisionFinanciera() {

        if (this.territorial != null) {
            // List<UsuarioDTO> jefesDivisionFinanciera =
            // this.getTramiteService()
            // .buscarFuncionariosPorRolYTerritorial(
            // this.usuarioActivo.getDescripcionTerritorial(),
            // ERol.SUBDIRECTOR_CATASTRO);
            //
            // if (jefesDivisionFinanciera != null
            // && !jefesDivisionFinanciera.isEmpty()) {
            // this.coordinadorGITAvaluos = jefesDivisionFinanciera.get(0);
            // }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que consulta el contrato interadministrativo activo asociado a la {@link #solicitud}
     *
     * @author christian.rodriguez
     */
    private void obtenerContratoInteradministrativoSolicitante() {

        if (this.solicitanteSolicitud != null) {

            FiltroDatosConsultaContratoInteradministrativo filtro =
                new FiltroDatosConsultaContratoInteradministrativo();

            filtro.setEntidadSolicitanteSolicitudId(this.solicitanteSolicitud
                .getId());
            filtro.setActivo(true);

            List<ContratoInteradministrativo> contratos = this
                .getAvaluosService()
                .buscarContratosInteradministrativosPorFiltros(null, filtro);

            if (contratos != null && !contratos.isEmpty()) {
                this.contratoInteradministrativo = contratos.get(0);
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que genera el documento de la liquidacion de los costos de los avalúos
     *
     * @author christian.rodriguez
     */
    public void generarDocumentoLiquidacionCostos() {

        if (!this.generarReporte()) {
            this.addMensajeError("No se pudo generar el documento de la liquidacion de los costos.");
            return;
        }

        this.liquidacionDocumento = this.guardarDocumento();
        if (this.liquidacionDocumento == null) {
            this.addMensajeError("No se pudo guardar el documento de la liquidacion de los costos.");
            return;
        }

        this.cargarDocumentoDeLiquidacionDeCostos(this.liquidacionDocumento
            .getId());
    }

    // -------------------------------------------------------------------------
    /**
     * Método que genera el reporte de la orden de liquidación
     *
     * @author christian.rodriguez
     */
    private boolean generarReporte() {

        boolean reporteGenerado = false;

        // TODO::christian.rodriguez :: Cambiar la url del reporte cuando esté
        // definido
        String urlReporte = EReporteServiceSNC.REPORTE_PRUEBA.getUrlReporte();

        // Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_RADICADO", "");

        try {
            this.liquidacionDocumentoDTO = this.reportsService.generarReporte(parameters,
                EReporteServiceSNC.REPORTE_PRUEBA, this.usuarioActivo);
            this.liquidacionDocumentoFile = this.liquidacionDocumentoDTO.getArchivoReporte();
            this.liquidacionDocumentoURL = this.liquidacionDocumentoDTO.getUrlWebReporte();
            reporteGenerado = true;

        } catch (Exception ex) {
            LOGGER.error("error generando reporte de liquidación de avalúos: " +
                 ex.getMessage());
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER,
                ex, urlReporte);
        }
        return reporteGenerado;
    }

    // ----------------------------------------------------------------------
    /**
     * Método que se encarga de recuperar el documento de liquidación de costos de los
     * {@link #avaluosALiquidar} seleccionado para ser visualizado
     *
     * @author christian.rodriguez
     */
    private void cargarDocumentoDeLiquidacionDeCostos(Long idDocumento) {

        this.liquidacionDocumento = this.getGeneralesService()
            .buscarDocumentoPorId(idDocumento);

        if (this.liquidacionDocumento != null) {
            this.obtenerRutaDocumentoDesdeAlfresco();
        }

        if (this.liquidacionDocumento == null ||
             this.liquidacionDocumentoURL == null ||
             this.liquidacionDocumentoURL.isEmpty()) {

            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "El documento no pudo ser cargado.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Genera la URL pública del oficio de no aprobación
     *
     * @author christian.rodriguez
     *
     */
    private void obtenerRutaDocumentoDesdeAlfresco() {

        if (this.liquidacionDocumento != null && this.liquidacionDocumentoDTO != null &&
             this.liquidacionDocumento.getIdRepositorioDocumentos() != null) {

            this.liquidacionDocumentoURL = this.liquidacionDocumentoDTO.getUrlWebReporte();

        }
    }

    // -------------------------------------------------------------------------
    /**
     * Genera el {@link Documento} y el {@link TramiteDocumento} asociados al oficio y los guarda en
     * al BD
     *
     * @author christian.rodriguez
     */
    private Documento guardarDocumento() {

        TipoDocumento tipoDocumentoOP = new TipoDocumento();
        tipoDocumentoOP
            .setId(ETipoDocumento.AVALUOS_DOCUMENTO_LIQUIDACION_COSTOS
                .getId());
        tipoDocumentoOP
            .setNombre(ETipoDocumento.AVALUOS_DOCUMENTO_LIQUIDACION_COSTOS
                .getNombre());

        Documento nuevoDocumento = new Documento();
        nuevoDocumento.setArchivo(this.liquidacionDocumentoFile.getName());
        nuevoDocumento.setTipoDocumento(tipoDocumentoOP);
        nuevoDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        nuevoDocumento.setBloqueado(ESiNo.NO.getCodigo());
        nuevoDocumento.setUsuarioCreador(this.usuarioActivo.getLogin());
        nuevoDocumento.setEstructuraOrganizacionalCod(this.usuarioActivo
            .getCodigoEstructuraOrganizacional());
        nuevoDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        nuevoDocumento.setUsuarioLog(this.usuarioActivo.getLogin());
        nuevoDocumento
            .setDescripcion(
                "Documento de liquidación de avalaúos asociados al Cons. Documento: FIXMEEEE!!!!");

        nuevoDocumento = this.getGeneralesService().guardarDocumento(
            nuevoDocumento);

        nuevoDocumento = this.guardarDocumentoEnGestorDocumental(
            nuevoDocumento, tipoDocumentoOP);

        if (nuevoDocumento != null) {
            nuevoDocumento = this.getGeneralesService().guardarDocumento(
                nuevoDocumento);
        }

        TramiteDocumento trDocumento = new TramiteDocumento();
        trDocumento.setDocumento(nuevoDocumento);
        trDocumento.setFechaLog(new Date());
        trDocumento.setfecha(new Date());
        trDocumento.setUsuarioLog(this.usuarioActivo.getLogin());

        trDocumento = this.getTramiteService().actualizarTramiteDocumento(
            trDocumento);

        return nuevoDocumento;

    }

    // -------------------------------------------------------------------------
    /**
     * Método para guardar un documento de avalúos en el gestor de documentos
     *
     * @author christian.rodriguez
     * @param documento {@link Documento} a guardar
     * @param tipoDocumento {@link TipoDocumento} del documento a guardar
     * @return {@link Documento} guardado con el id del gestor de documentos
     */
    private Documento guardarDocumentoEnGestorDocumental(Documento documento,
        TipoDocumento tipoDocumento) {

        String urlArchivo = UtilidadesWeb.obtenerRutaTemporalArchivos() +
             documento.getArchivo();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(
                this.solicitud.getNumero(), this.solicitud.getFecha(),
                tipoDocumento.getNombre(), urlArchivo);

        documento = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuarioActivo,
                datosGestorDocumental, documento);

        if (documento != null && documento.getIdRepositorioDocumentos() == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }
        return documento;

    }
}
