/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.util.FiltroDatosConsultaContratoInteradministrativo;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso CU-SA-AC-053 Consultar Contratos Interadministrativos
 *
 * @cu CU-SA-AC-053
 *
 * @author felipe.cadena
 *
 */
@Component("consultaContratosInteradministrativos")
@Scope("session")
public class ConsultaContratosInteradministrativosMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = -8677750343941280965L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaContratosInteradministrativosMB.class);

    /**
     * ManagedBean del caso de uso 008
     */
    private AdminContratosInteradministrativosMB administrarContratosMB;

    //-----criterios de busqueda------------
    /**
     * Tipo de empresa asociada al tramite
     */
    private String tipoEmpresa;
    private String personaNatural;
    private String razonSocial;
    private String sigla;
    private String tipoDocumento;
    private String numeroDocumento;
    private String digitoVerificacion;
    private String interventorIgac;
    private String numeroContrato;
    private Integer vigencia;

    /**
     * Lista para guardar los contratos obtenidos en la busqueda
     */
    private List<ContratoInteradministrativo> listaContratos;
    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean banderaEsPersonaNatural;

    /**
     * Bandera que indica si se deben mostrar todas las columans en la tabla de resultados
     */
    private boolean banderaMostrarTodasColumnas;

    /**
     * Contrato seleccionado para la administración de contratos
     */
    private ContratoInteradministrativo contratoSeleccionado;

    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    // --------getters-setters----------------
    public String getTipoEmpresa() {
        return this.tipoEmpresa;
    }

    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public ContratoInteradministrativo getContratoSeleccionado() {
        return this.contratoSeleccionado;
    }

    public void setContratoSeleccionado(ContratoInteradministrativo contratoSeleccionado) {
        this.contratoSeleccionado = contratoSeleccionado;
    }

    public List<ContratoInteradministrativo> getListaContratos() {
        return this.listaContratos;
    }

    public void setListaContratos(List<ContratoInteradministrativo> listaContratos) {
        this.listaContratos = listaContratos;
    }

    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    public void setBanderaEsPersonaNatural(boolean banderaEsPersonaNatural) {
        this.banderaEsPersonaNatural = banderaEsPersonaNatural;
    }

    public String getPersonaNatural() {
        return this.personaNatural;
    }

    public void setPersonaNatural(String personaNatural) {
        this.personaNatural = personaNatural;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return this.numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDigitoVerificacion() {
        return this.digitoVerificacion;
    }

    public void setDigitoVerificacion(String digitoVerificacion) {
        this.digitoVerificacion = digitoVerificacion;
    }

    public String getInterventorIgac() {
        return this.interventorIgac;
    }

    public void setInterventorIgac(String interventorIgac) {
        this.interventorIgac = interventorIgac;
    }

    public String getNumeroContrato() {
        return this.numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Integer getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(Integer vigencia) {
        this.vigencia = vigencia;
    }

    public boolean isBanderaMostrarTodasColumnas() {
        return this.banderaMostrarTodasColumnas;
    }

    public void setBanderaMostrarTodasColumnas(boolean banderaMostrarTodasColumnas) {
        this.banderaMostrarTodasColumnas = banderaMostrarTodasColumnas;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on consultaContratosInteradministrativosMB init");
        this.iniciarVariables();

    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

    }

    /**
     * Retorna el valor de la enumeración EPersonaTipoIdentificacion.NIT
     *
     * @return
     */
    public String getNit() {
        return EPersonaTipoIdentificacion.NIT.getCodigo();
    }

    /**
     * Actualiza la bandera tipo presona
     *
     * @author felipe.cadena
     */
    public void onChangeTipoPersona() {

        if (this.tipoEmpresa.equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {
            this.banderaEsPersonaNatural = true;

        } else {
            this.banderaEsPersonaNatural = false;

        }
        this.personaNatural = null;
        this.razonSocial = null;
        this.sigla = null;
        this.tipoDocumento = null;
        this.numeroDocumento = null;
        this.digitoVerificacion = null;
        this.interventorIgac = null;
        this.numeroContrato = null;
        this.vigencia = null;

    }

    /**
     * Método para realizar la busqueda de los contratos con base en los paremetros ingresados por
     * el usuaio
     *
     * @author felipe.cadena
     *
     */
    public void buscarContratos() {
        LOGGER.debug("Buscando contratos...");

        FiltroDatosConsultaSolicitante filtro = new FiltroDatosConsultaSolicitante();

        filtro.setTipoPersona(this.tipoEmpresa);

        if (this.banderaEsPersonaNatural) {
            if ("".equals(this.personaNatural)) {
                filtro.setPrimerNombre(null);
            } else {
                filtro.setPrimerNombre(this.personaNatural);
            }
            filtro.setTipoIdentificacion(this.tipoDocumento);
            filtro.setRazonSocial(null);
            filtro.setSigla(null);
            filtro.setDigitoVerificacion(null);
        } else {
            if ("".equals(this.razonSocial)) {
                filtro.setRazonSocial(null);
            } else {
                filtro.setRazonSocial(this.personaNatural);
            }
            if ("".equals(this.sigla)) {
                filtro.setSigla(null);
            } else {
                filtro.setSigla(this.sigla);
            }
            if ("".equals(this.digitoVerificacion)) {
                filtro.setDigitoVerificacion(null);
            } else {
                filtro.setDigitoVerificacion(this.digitoVerificacion);
            }
            filtro.setTipoIdentificacion(this.getNit());
            filtro.setNombreCompleto(null);
        }

        if ("".equals(this.numeroDocumento)) {
            filtro.setNumeroIdentificacion(null);
        } else {
            filtro.setNumeroIdentificacion(this.numeroDocumento);
        }

        if ("".equals(this.numeroContrato)) {
            this.numeroContrato = null;
        }
        if ("".equals(this.interventorIgac)) {
            this.interventorIgac = null;
        }

        FiltroDatosConsultaContratoInteradministrativo filtroContrato =
            new FiltroDatosConsultaContratoInteradministrativo();
        filtroContrato.setNombreCompletoInterventor(this.interventorIgac);
        filtroContrato.setNumeroContrato(this.numeroContrato);
        if (this.vigencia != null) {
            filtroContrato.setVigencia(this.vigencia.longValue());
        }

        this.listaContratos = this.getAvaluosService()
            .buscarContratosInteradministrativosPorFiltros(filtro, filtroContrato);

        LOGGER.debug("Busqueda terminada");

    }

    /**
     * Método para llamar al caso de uso CU-SA-AC-008, se puede enviar como parametro el objeto
     * contratoSeleccionado o cualquiera e sus atributos.
     *
     * @author felipe.cadena
     *
     */
    public void administrarContrato() {

        this.administrarContratosMB = (AdminContratosInteradministrativosMB) UtilidadesWeb
            .getManagedBean("adminContratosInteradministrativos");

        this.administrarContratosMB.cargarDesdeCU53(
            this.contratoSeleccionado.getEntidadSolicitante(), this.contratoSeleccionado);
    }

    /**
     * Método para llamar la consulta desde el caso de uso 054
     *
     * @author felipe.cadena
     *
     * @param entidadSolicitente - Entidad de la cual se quiere consultar los contratos
     */
    public void cargarDesdeCU54(Solicitante entidadSolicitente) {

        this.tipoEmpresa = entidadSolicitente.getTipoPersona();
        this.numeroDocumento = entidadSolicitente.getNumeroIdentificacion();
        this.tipoDocumento = entidadSolicitente.getTipoIdentificacion();

        if (this.tipoEmpresa.equals(EPersonaTipoPersona.JURIDICA.getCodigo())) {
            this.razonSocial = entidadSolicitente.getRazonSocial();
            this.sigla = entidadSolicitente.getSigla();
            this.digitoVerificacion = entidadSolicitente.getDigitoVerificacion();
            this.personaNatural = null;
            this.banderaEsPersonaNatural = false;
        } else {
            this.personaNatural = entidadSolicitente.getNombreCompleto();
            this.razonSocial = null;
            this.sigla = null;
            this.digitoVerificacion = null;
            this.banderaEsPersonaNatural = true;
        }

        this.buscarContratos();

    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaInformacionSolicitudMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        return ConstantesNavegacionWeb.INDEX;
    }
}
