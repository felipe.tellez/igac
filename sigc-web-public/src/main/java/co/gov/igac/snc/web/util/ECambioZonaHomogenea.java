/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.util;

/**
 * Define los tipos de cambios que existen para las rectificaciones de area CU-NP-CO-199
 * Rectificación de zona homogénea o física
 *
 * @author felipe.cadena
 */
public enum ECambioZonaHomogenea {
    CAMBIO_CODIGO("Cambio de código de zonas homogeneas", 0),
    AJUSTE_ESTUDIO("Requiere ajustar estudio actual de zonas homogeneas", 1);

    private String valor;
    private int codigo;

    private ECambioZonaHomogenea(String valor, int codigo) {
        this.valor = valor;
        this.codigo = codigo;
    }

    public String getValor() {
        return valor;
    }

    public int getCodigo() {
        return codigo;
    }
}
