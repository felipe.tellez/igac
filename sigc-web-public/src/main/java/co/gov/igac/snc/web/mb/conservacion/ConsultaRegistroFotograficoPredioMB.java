/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace las veces de managed bean para la consulta de fotos de un predio
 *
 * @author pedro.garcia
 */
@Component("consultaRegistroFotograficoPredio")
@Scope("session")
public class ConsultaRegistroFotograficoPredioMB extends SNCManagedBean
    implements Serializable {

    private static final long serialVersionUID = 4777534652576048262L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaRegistroFotograficoPredioMB.class);

    /**
     * Lista que contiene el registro fotografico del predio
     */
    private List<Foto> fotosPredio;

    /**
     * predio del que se ven los detalles de trámites
     */
    private Predio predioForPhotos;

    /**
     * Contiene eldocumento vinculo de la fotografia a previsualizar
     */
    private Documento documentoFoto;

    private String urlFoto;

    //private String imagenBase;
//--------------    methods    ---
    public Predio getPredioForPhotos() {
        return this.predioForPhotos;
    }

    public void setPredioForPhotos(Predio predioForPhotos) {
        this.predioForPhotos = predioForPhotos;
    }

    public List<Foto> getFotosPredio() {
        return this.fotosPredio;
    }

    public void setFotosPredio(List<Foto> fotosTramite) {
        this.fotosPredio = fotosTramite;
    }

    public String getUrlFoto() {
        return this.urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public Documento getDocumentoFoto() {
        return this.documentoFoto;
    }

    public void setDocumentoFoto(Documento documentoFoto) {
        this.documentoFoto = documentoFoto;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("ConsultaRegistroFotograficoPredioMB init");

        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        Long predioSeleccionadoId = mb001.getSelectedPredioId();

        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioSeleccionadoId != null) {
            this.predioForPhotos = new Predio();
            if (!esPredioOrigen) {
                this.getPredioById(predioSeleccionadoId);
                try {
                    this.fotosPredio = this.getConservacionService().buscarFotografiasPredio(
                        predioSeleccionadoId);
                } catch (Exception e) {
                    this.addMensajeError("Error al consultar las fotografias");
                    LOGGER.error(e.getMessage());
                }
            } else {
                this.predioForPhotos = mb001.getSelectedPredio1();
                this.fotosPredio = mb001.getSelectedPredio1().getFotos();
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 22-05-2013 se elimina el uso de un método que requería el envío de
     * todo el objeto Documento y que usaba los métodos viejos de ftp
     */
    public void cargarImagen() {

        //D: en el método que busca las fotos las fotos no se cargan los archivos, así que se hace aquí
        //  para el archivo que se quiere ver
        this.urlFoto = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
            this.documentoFoto.getIdRepositorioDocumentos());

        this.documentoFoto.setRutaArchivoLocal(this.urlFoto);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * se define este método solo para mantener el estándar del tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * También se hacen las consultas de los trámites pendientes y realizados
     *
     * @author pedro.garcia
     * @param predioId
     */
    private void getPredioById(Long predioId) {

        this.predioForPhotos = this.getConservacionService().obtenerPredioPorId(predioId);

    }

//end of class
}
