package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.vistas.VSolicitudPredio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

/**
 * Clase que hace las veces de managed bean para ConsultaTramite, al principio se penso en hacer un
 * composite generico para la consulta de trámites pero luego dado la especifidad del caso de uso de
 * administración de asignaciones esta clase perdió generalidad y es posible que sólo sirva para
 * este caso de uso
 *
 * @author javier.aponte
 *
 * @version 2.0
 * @cu se usa en la búsqueda de tramites, para el caso de uso de administración de asignaciones
 */
@Component("consultaTramite")
@Scope("session")
public class ConsultaTramiteMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7576908157594023335L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaTramiteMB.class);

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * listas con los valores de los combos de selección para la búsqueda
     */
    private ArrayList<SelectItem> territorialesItemList;
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * Lista con los valores del combo de tipos de trámite acorde al el tipo de solicitud
     * seleccionado
     */
    private ArrayList<SelectItem> tiposTramitePorSolicitud;

    private FiltroDatosConsultaTramite datosConsultaTramite;

    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;

    private UsuarioDTO currentUser;
    /**
     * Lista que contiene los funcionarios ejecutores que se encuentren asociados, a la territorial
     * del usuario autenticado
     */
    private List<SelectItem> ejecutoresSelectItem;

    /**
     * se usa para que no se consulte lista de departamentos usando la territorial ya que en algunos
     * casos no se usa
     */
    private boolean seUsaTerritorial;

    private boolean renderResultPanel;

    /**
     * Bandera de visualización de la tabla de resultado de búsqueda de trámites
     */
    private boolean banderaBusquedaTramites;

    /**
     * indican si existe un trámite seleccionado para activar los datos del regístro de la
     * notificación
     */
    private boolean banderaDatosRegistroNotificacion;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites
     */
    private LazyDataModel<Tramite> lazyAssignedTramites;

    /**
     * lista de trámites paginados
     */
    private List<Tramite> rows;

    /**
     * Arreglo con lo trámites que se seleccionen en la tabla del resultado de la busqueda
     */
    private List<Tramite> tramitesSeleccionados = new ArrayList<Tramite>();

    /**
     * Trámite seleccionado en la pantalla de trámites seleccionados
     */
    private Tramite tramiteSeleccionado2;

    private Tramite tramiteSeleccionadoDocumentos;

    /**
     * se usa para pasar un id que sirva para saber qué método se debe ejecutar cuando en el CC de
     * consulta de predios se selecciona uno de los trámites del resultado de la búsqueda
     */
    private int listenerMethodIdentifier;

    /**
     * Variable que indica si la busqueda de trámite muestra la opción de tipo de trámite de via
     * gubernativa modified by javier.aponte 20-09-2012 ya no se usa
     */
    @Deprecated
    private boolean banderaMostrarTipoTramiteViaGubernativa;

    /**
     * Variable item list con los valores del tipo de solicitud
     */
    private List<SelectItem> tipoSolicitudItemList;

    @Autowired
    private GeneralMB generalMB;

    /**
     * Variable usada para capturar el código de una territorial seleccionada de la lista de
     * territoriales.
     */
    private String territorialSeleccionadaCodigo;

    /**
     * Variable usada para capturar el código de una UOC seleccionada de la lista de UOCs de la
     * territorial.
     */
    private String unidadOperativaSeleccionadaCodigo;

    /**
     * Lista de select items de las UOC.
     */
    private List<SelectItem> unidadesOperativasCatastro;

    /**
     * Lista de select items de las tipo persona.
     */
    private List<SelectItem> tipoPersonaList;

    /**
     * Variable que almacena la territorial seleccionada.
     */
    private EstructuraOrganizacional territorialSeleccionada;

    /**
     * Lista de unidades operativas de catastro
     */
    private List<EstructuraOrganizacional> unidadesOperativasCatastroObj;

    /**
     * Lista de objetos estructuras organizacionales de subdireciones.
     */
    private List<EstructuraOrganizacional> subdireccionesOficinasObj;

    /**
     * Lista de select items de subdireciones.
     */
    private List<SelectItem> subdireccionesOficinas;

    /**
     * Variable que almacena la unidad operativa seleccionada.
     */
    private EstructuraOrganizacional unidadOperativaSeleccionada;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites
     */
    private List<Tramite> tramitesResult;

    /**
     * Objeto usado como contenedor de los datos de un solicitante para su consulta.
     */
    private FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante;

    /**
     * Valor asociado al radio de seleccion de tipo de persona
     */
    private int tipoPersona;

    /**
     * Lista resultado de busqueda de solicitantes
     */
    private List<Solicitante> solicitantesRes;

    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean tipoPersonaN;

    /**
     * Bandera que indica si el tipo de persona es juridica en la busqueda del solicitante
     */
    private boolean tipoPersonaJ;

    /**
     * Bandera de visualización de la tabla de resultado de busqueda de solicitantes
     */
    private boolean banderaBuscarSolicitantes;

    /**
     * Bandera de seleccion de solicitante para la busqueda de solicitudes
     */
    private boolean banderaSolicitanteSolicitudSeleccionado;

    /**
     * Objeto que contiene los campos filtro para buscar solicitudes
     */
    private VSolicitudPredio filtroBusquedaSolicitud;

    /**
     * Arreglo de las solicitudes resultado de una consulta por filtro
     */
    private List<VSolicitudPredio> solicitudes;

    /**
     * Bandera que habilita la trabla de resultados de busqueda de solicitudes
     */
    private boolean banderaBuscarSolicitudes;

    /**
     * Bandera de seleccion de solicitud
     */
    private boolean banderaSolicitudSeleccionada;

    /**
     * Variable que almacena el login del funcionario ejecutor seleccionado.
     */
    private String funcionarioEjecutor;

    /**
     * Variable que almacena una fecha minima de búsqueda.
     */
    private Date minDate;

    /**
     * Objeto que contiene los datos del reporte con marca de agua
     */
    private ReporteDTO reporteDocumentoWaterMark;

    /*
     * variable para el estado del tramite
     */
    private String estadoTramite;

    /**
     * Lista de select items de los estados del tramite.
     */
    private List<SelectItem> estadosTramiteItemList;

    /**
     * Lista que contiene la información de la documentacion requerida
     */
    private List<TramiteDocumentacion> documentacionRequerida;

    /**
     * Lista que contiene la información de la documentacion adicional
     */
    private List<TramiteDocumentacion> documentacionAdicional;

    /**
     * Lista que contiene la información de la documentacion generada por el sistema
     */
    private List<Documento> documentacionGenerada;

    /**
     * documentación del trámite que se ha seleccionado de la tabla
     */
    private TramiteDocumentacion selectedTramiteDocumentacion;

    /**
     * almacena la ultima observacion del tramiteEstado de un tramite, cuando el estado es CANCELADO
     */
    private HashMap<Long, String> tramiteObs;

    /**
     * bandera para mostrar observaciones de tramites cancelados
     */
    private boolean observaciones;
    /**
     * Bandera para mostrar los documentos del trámite
     */
    private boolean rolPermitidoDocumentosTramite;

    /**
     * Trámite seleccionado de la tabla de resultados de la busqueda de trámite en el caso de uso de
     * trámite de derecho de petición o tutela
     *
     * @author javier.aponte
     */
    private Tramite selectedTramiteRow;

    /**
     * Determina si el usuario autenticado se pertenece a una entidad delegada
     */
    private boolean usuarioDelegado;
    /**
     * Determina si el usuario autenticado se pertenece a una entidad habilitada
     */

    private boolean usuarioHabilitada;

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("deprecation")
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaTramiteMB");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.usuarioHabilitada = MenuMB.getMenu().isUsuarioHabilitado();

        this.datosConsultaTramite = new FiltroDatosConsultaTramite();

        this.territorialesItemList = new ArrayList<SelectItem>();
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList = new ArrayList<SelectItem>();
        this.estadosTramiteItemList = new ArrayList<SelectItem>();

        this.datosConsultaTramite
            .setNumeroPredialPartes(new NumeroPredialPartes());

        this.tramitesResult = new ArrayList<Tramite>();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.territorialSeleccionada = new EstructuraOrganizacional();
        this.unidadesOperativasCatastro = new ArrayList<SelectItem>();
        this.tipoPersonaList = new ArrayList<SelectItem>();
        this.tipoPersonaList.add(new SelectItem(0, "Natural"));
        this.tipoPersonaList.add(new SelectItem(1, "Jurídica"));
        this.tipoPersona = -1;
        this.filtroBusquedaSolicitud = new VSolicitudPredio();

        //se asigna por defecto la territorial del usuario y se cargan sus Unidades operativas
        this.banderaBuscarSolicitudes = false;
        this.banderaBuscarSolicitantes = false;
        this.banderaSolicitanteSolicitudSeleccionado = false;
        this.tipoPersonaN = false;
        this.tipoPersonaJ = false;
        this.funcionarioEjecutor = "";

        // Inicialización de una fecha mínima para el intervalo de consulta
        Date actualDate = new Date();
        int minYear = actualDate.getYear() - 200;
        actualDate.setYear(minYear);
        this.minDate = actualDate;

        //estodo consulta
        this.estadoTramite = "-1";

        this.seUsaTerritorial = false;
        this.renderResultPanel = false;
        this.observaciones = false;

        this.tipoSolicitudItemList = new ArrayList<SelectItem>();
        this.tipoPersona = 0;

        LOGGER.debug("hace el init de la lista de Territoriales");

        //Poblamiento de la lista de territoriales para selección
        if (this.territorialesItemList == null ||
             this.territorialesItemList.isEmpty()) {
            // this.territorialesItemList.add(new SelectItem("", "Seleccione"));
            List<EstructuraOrganizacional> eoList;
            if (this.usuarioDelegado || this.usuarioHabilitada) {
                eoList = new ArrayList<EstructuraOrganizacional>();
                eoList.add(this.getGeneralesService().
                    buscarEstructuraOrganizacionalPorCodigo(this.currentUser.getCodigoTerritorial()));
                this.cargarFuncionariosEjecutores();
            } else {
                eoList = this.getGeneralesService().getCacheTerritoriales();
            }

            this.setTerritorialesItemList(eoList);
        }

        //Se selecciona la territorial a la cual pertenece el usuario registrado 
        this.territorialSeleccionadaCodigo = this.currentUser.getCodigoTerritorial();

        this.onCambioTerritorial();
        this.cargarTiposSolicitud();
        setRolPermitidoDocumentosTramite(this.currentUser.getRoles());

        // ------------------- //
    }

    public void setRolPermitidoDocumentosTramite(String[] rolesUsuarioActual) {
        for (String rol : rolesUsuarioActual) {
            if (rol.equalsIgnoreCase(ERol.FUNCIONARIO_RADICADOR.getRol())) {
                this.rolPermitidoDocumentosTramite = false;
                break;
            }
            if (rol.equalsIgnoreCase(ERol.RESPONSABLE_CONSERVACION.getRol()) ||
                rol.equalsIgnoreCase(ERol.DIRECTOR_TERRITORIAL.getRol()) ||
                rol.equalsIgnoreCase(ERol.SECRETARIA_CONSERVACION.getRol()) ||
                rol.equalsIgnoreCase(ERol.ABOGADO.getRol()) ||
                rol.equalsIgnoreCase(ERol.CONSULTA_TRAMITE.getRol())) {
                this.rolPermitidoDocumentosTramite = true;
                break;
            }
        }
    }

    public FiltroDatosConsultaTramite getDatosConsultaTramite() {
        return datosConsultaTramite;
    }

    public void setDatosConsultaTramite(
        FiltroDatosConsultaTramite datosConsultaTramite) {
        this.datosConsultaTramite = datosConsultaTramite;
    }

    public EstructuraOrganizacional getTerritorialSeleccionada() {
        return territorialSeleccionada;
    }

    public void setTerritorialSeleccionada(
        EstructuraOrganizacional territorialSeleccionada) {
        this.territorialSeleccionada = territorialSeleccionada;
    }

    public List<EstructuraOrganizacional> getSubdireccionesOficinasObj() {
        return subdireccionesOficinasObj;
    }

    public void setTerritorialesItemList(
        ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public boolean isUsuarioHabilitada() {
        return usuarioHabilitada;
    }

    public void setUsuarioHabilitada(boolean usuarioHabilitada) {
        this.usuarioHabilitada = usuarioHabilitada;
    }
    
    public String getFuncionarioEjecutor() {
        return funcionarioEjecutor;
    }

    public void setFuncionarioEjecutor(String funcionarioEjecutor) {
        this.funcionarioEjecutor = funcionarioEjecutor;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public List<SelectItem> getSubdireccionesOficinas() {
        return subdireccionesOficinas;
    }

    public ReporteDTO getReporteDocumentoWaterMark() {
        return reporteDocumentoWaterMark;
    }

    public void setReporteDocumentoWaterMark(ReporteDTO reporteDocumentoWaterMark) {
        this.reporteDocumentoWaterMark = reporteDocumentoWaterMark;
    }

    public VSolicitudPredio getFiltroBusquedaSolicitud() {
        return filtroBusquedaSolicitud;
    }

    public void setFiltroBusquedaSolicitud(
        VSolicitudPredio filtroBusquedaSolicitud) {
        this.filtroBusquedaSolicitud = filtroBusquedaSolicitud;
    }

    public List<VSolicitudPredio> getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(List<VSolicitudPredio> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public boolean isBanderaBuscarSolicitudes() {
        return banderaBuscarSolicitudes;
    }

    public void setBanderaBuscarSolicitudes(boolean banderaBuscarSolicitudes) {
        this.banderaBuscarSolicitudes = banderaBuscarSolicitudes;
    }

    public boolean isBanderaSolicitudSeleccionada() {
        return banderaSolicitudSeleccionada;
    }

    public void setBanderaSolicitudSeleccionada(
        boolean banderaSolicitudSeleccionada) {
        this.banderaSolicitudSeleccionada = banderaSolicitudSeleccionada;
    }

    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public boolean isBanderaSolicitanteSolicitudSeleccionado() {
        return banderaSolicitanteSolicitudSeleccionado;
    }

    public void setBanderaSolicitanteSolicitudSeleccionado(
        boolean banderaSolicitanteSolicitudSeleccionado) {
        this.banderaSolicitanteSolicitudSeleccionado = banderaSolicitanteSolicitudSeleccionado;
    }

    public boolean isBanderaBuscarSolicitantes() {
        return banderaBuscarSolicitantes;
    }

    public void setBanderaBuscarSolicitantes(boolean banderaBuscarSolicitantes) {
        this.banderaBuscarSolicitantes = banderaBuscarSolicitantes;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public List<SelectItem> getUnidadesOperativasCatastro() {
        return unidadesOperativasCatastro;
    }

    public List<EstructuraOrganizacional> getUnidadesOperativasCatastroObj() {
        return unidadesOperativasCatastroObj;
    }

    public boolean isTipoPersonaN() {
        return tipoPersonaN;
    }

    public void setTipoPersonaN(boolean tipoPersonaN) {
        this.tipoPersonaN = tipoPersonaN;
    }

    public boolean isTipoPersonaJ() {
        return tipoPersonaJ;
    }

    public void setTipoPersonaJ(boolean tipoPersonaJ) {
        this.tipoPersonaJ = tipoPersonaJ;
    }

    public List<Solicitante> getSolicitantesRes() {
        return solicitantesRes;
    }

    public void setSolicitantesRes(List<Solicitante> solicitantesRes) {
        this.solicitantesRes = solicitantesRes;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(
        FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public List<Tramite> getTramitesResult() {
        return tramitesResult;
    }

    public void setTramitesResult(List<Tramite> tramitesResult) {
        this.tramitesResult = tramitesResult;
    }

    public FiltroDatosConsultaSolicitante getFiltroDatosConsultaSolicitante() {
        return filtroDatosConsultaSolicitante;
    }

    public void setFiltroDatosConsultaSolicitante(
        FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante) {
        this.filtroDatosConsultaSolicitante = filtroDatosConsultaSolicitante;
    }

    public String getUnidadOperativaSeleccionadaCodigo() {
        return unidadOperativaSeleccionadaCodigo;
    }

    public EstructuraOrganizacional getUnidadOperativaSeleccionada() {
        return unidadOperativaSeleccionada;
    }

    public void setUnidadOperativaSeleccionadaCodigo(
        String unidadOperativaSeleccionadaCodigo) {
        this.unidadOperativaSeleccionadaCodigo = unidadOperativaSeleccionadaCodigo;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return this.territorialesItemList;
    }

    public void setTerritorialesItemList(List<EstructuraOrganizacional> items) {
        LOGGER.debug("Entra al método que retorna la lista de territoriales");

        for (EstructuraOrganizacional territorial : items) {
            this.territorialesItemList.add(new SelectItem(territorial
                .getCodigo(), territorial.getNombre()));
        }
    }

    public List<SelectItem> getEjecutoresSelectItem() {
        return ejecutoresSelectItem;
    }

    public void setEjecutoresSelectItem(List<SelectItem> ejecutoresSelectItem) {
        this.ejecutoresSelectItem = ejecutoresSelectItem;
    }

    public Tramite getTramiteSeleccionado2() {
        return tramiteSeleccionado2;
    }

    public void setTramiteSeleccionado2(Tramite tramiteSeleccionado2) {
        this.tramiteSeleccionado2 = tramiteSeleccionado2;
    }

    // -------------------------------------------------------------------------------------------------
    /**
     * @return the departamentosItemList
     */
    public ArrayList<SelectItem> getDepartamentosItemList() {

        List<Departamento> deptosList;

        // TODO se podría llevar un flag que indique si la territorial cambió
        // para evitar hacer esta consulta
        // siempre. Lo mismo aplica para municipios
        if (this.datosConsultaTramite.getTerritorialId() != null &&
             this.datosConsultaTramite.getTerritorialId().compareTo("") != 0) {
            deptosList = this.getGeneralesService()
                .getCacheDepartamentosPorTerritorial(
                    this.datosConsultaTramite.getTerritorialId());
        } else {
            if (!this.seUsaTerritorial) {
                deptosList = this.getGeneralesService()
                    .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
            } else {
                deptosList = new ArrayList<Departamento>();
                this.datosConsultaTramite.setDepartamentoId("");
                this.setMunicipiosItemList(new ArrayList<Municipio>());
            }
        }
        this.setDepartamentosItemList(deptosList);

        return this.departamentosItemList;
    }

    // ----------------
    /**
     * @param departamentosItemList the departamentosItemList to set
     */
    public void setDepartamentosItemList(List<Departamento> departamentosList) {
        LOGGER.debug("entra al que retorna la lista de Departamentos");

        // D: para evitar que queden los elementos de búsquedas anteriores
        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento
                .getCodigo(), departamento.getNombre()));
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @return the municipiosItemList
     */
    public ArrayList<SelectItem> getMunicipiosItemList() {

        List<Municipio> municipiosList;

        if (this.datosConsultaTramite.getDepartamentoId() != null &&
             this.datosConsultaTramite.getDepartamentoId().compareTo("") != 0) {
            municipiosList = this.getGeneralesService()
                .getCacheMunicipiosByDepartamento(
                    this.datosConsultaTramite.getDepartamentoId());
        } else {
            municipiosList = new ArrayList<Municipio>();
        }
        this.setMunicipiosItemList(municipiosList);

        return this.municipiosItemList;

    }

    // -----------------
    /**
     * @param municipiosItemList the municipiosItemList to set
     */
    public void setMunicipiosItemList(List<Municipio> municipiosList) {

        // D: para evitar que queden los elementos de búsquedas anteriores
        if (!this.municipiosItemList.isEmpty()) {
            this.municipiosItemList.clear();
        }

        for (Municipio municipio : municipiosList) {
            this.municipiosItemList.add(new SelectItem(municipio.getCodigo(),
                municipio.getNombre()));
        }

    }

    public LazyDataModel<Tramite> getLazyAssignedTramites() {
        return lazyAssignedTramites;
    }

    public void setLazyAssignedTramites(
        LazyDataModel<Tramite> lazyAssignedTramites) {
        this.lazyAssignedTramites = lazyAssignedTramites;
    }

    // -------------- methods ---------------
    public ArrayList<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> answer = null;
        List<TipoSolicitudTramite> tiposTramite = null;
        SelectItem itemToAdd;

        if (this.datosConsultaTramite.getTipoSolicitud() != null) {
            tiposTramite = this.getTramiteService()
                .obtenerTiposDeTramitePorSolicitud(
                    this.datosConsultaTramite.getTipoSolicitud());
        }
        if (tiposTramite != null && !tiposTramite.isEmpty()) {
            answer = new ArrayList<SelectItem>();
            answer.add(new SelectItem("", this.DEFAULT_COMBOS));
            for (TipoSolicitudTramite tst : tiposTramite) {
                itemToAdd = new SelectItem(tst.getTipoTramite(),
                    tst.getTipoTramiteValor());
                answer.add(itemToAdd);
            }
        }
        this.tiposTramitePorSolicitud = answer;
        return this.tiposTramitePorSolicitud;
    }

    public void setTiposTramitePorSolicitud(ArrayList<SelectItem> tiposTramite) {
        this.tiposTramitePorSolicitud = tiposTramite;
    }

    public List<SelectItem> getSubtipoClaseMutacionV() {
        this.subtipoClaseMutacionV = new ArrayList<SelectItem>();
        this.subtipoClaseMutacionV.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.subtipoClaseMutacionV = this.generalMB
            .getSubtipoClaseMutacionV(this.datosConsultaTramite
                .getClaseMutacion());
        return subtipoClaseMutacionV;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public boolean isSeUsaTerritorial() {
        return seUsaTerritorial;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public void setSeUsaTerritorial(boolean seUsaTerritorial) {
        this.seUsaTerritorial = seUsaTerritorial;
    }

    public boolean isRenderResultPanel() {
        return renderResultPanel;
    }

    public void setRenderResultPanel(boolean renderResultPanel) {
        this.renderResultPanel = renderResultPanel;
    }

    public boolean isBanderaBusquedaTramites() {
        return banderaBusquedaTramites;
    }

    public void setBanderaBusquedaTramites(boolean banderaBusquedaTramites) {
        this.banderaBusquedaTramites = banderaBusquedaTramites;
    }

    public boolean isBanderaDatosRegistroNotificacion() {
        return banderaDatosRegistroNotificacion;
    }

    public void setBanderaDatosRegistroNotificacion(
        boolean banderaDatosRegistroNotificacion) {
        this.banderaDatosRegistroNotificacion = banderaDatosRegistroNotificacion;
    }

    public List<Tramite> getTramitesSeleccionados() {
        return tramitesSeleccionados;
    }

    public void setTramitesSeleccionados(List<Tramite> tramitesSeleccionados) {
        this.tramitesSeleccionados = tramitesSeleccionados;
    }

    public int getListenerMethodIdentifier() {
        return this.listenerMethodIdentifier;
    }

    public void setListenerMethodIdentifier(int listenerMethodIdentifier) {
        this.listenerMethodIdentifier = listenerMethodIdentifier;
    }

    public boolean isBanderaMostrarTipoTramiteViaGubernativa() {
        return banderaMostrarTipoTramiteViaGubernativa;
    }

    public void setBanderaMostrarTipoTramiteViaGubernativa(
        boolean banderaMostrarTipoTramiteViaGubernativa) {
        this.banderaMostrarTipoTramiteViaGubernativa = banderaMostrarTipoTramiteViaGubernativa;
    }

    public Tramite getSelectedTramiteRow() {
        return selectedTramiteRow;
    }

    public void setSelectedTramiteRow(Tramite selectedTramiteRow) {
        this.selectedTramiteRow = selectedTramiteRow;
    }

    public List<SelectItem> getTipoSolicitudItemList() {

        return tipoSolicitudItemList;
    }

    public void setTipoSolicitudItemList(List<SelectItem> tipoSolicitudItemList) {
        this.tipoSolicitudItemList = tipoSolicitudItemList;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que carga los tipos de solicitud
     */
    public void cargarTiposSolicitud() {

        if (this.tipoSolicitudItemList != null &&
             !this.tipoSolicitudItemList.isEmpty()) {
            this.tipoSolicitudItemList.clear();
        }
        for (Dominio dominio : this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITUD_TIPO)) {
            if (!ESolicitudTipo.REVOCATORIA_DIRECTA.getCodigo().equals(
                dominio.getCodigo())) {
                this.tipoSolicitudItemList.add(new SelectItem(dominio
                    .getCodigo(), dominio.getValor()));
            }
        }

    }

    /**
     * Método encargado de cargar los funcionarios ejecutores en una lista de de SelecItem asociados
     * a la territorial del usuario en sesion
     */
    public void cargarFuncionariosEjecutores() {

        this.ejecutoresSelectItem = new ArrayList<SelectItem>();
        List<UsuarioDTO> ejecutores;
        String codigoUOSeleccionada;

        if (this.usuarioDelegado || this.usuarioHabilitada) {
            codigoUOSeleccionada = this.currentUser.getDescripcionTerritorial();
        } else if (this.territorialSeleccionada != null) {
            if (this.unidadOperativaSeleccionada != null) {
                codigoUOSeleccionada = this.unidadOperativaSeleccionada.getNombre();
                codigoUOSeleccionada = codigoUOSeleccionada.toUpperCase().replace("UOC ", "UOC_");
            } else {
                codigoUOSeleccionada = this.territorialSeleccionada.getNombre().toUpperCase();
            }

            codigoUOSeleccionada = Utilidades.delTildes(codigoUOSeleccionada);

        } else {
            codigoUOSeleccionada = this.currentUser
                .getDescripcionEstructuraOrganizacional();
        }

        ejecutores = this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                codigoUOSeleccionada,
                ERol.EJECUTOR_TRAMITE);

        if (ejecutores != null && !ejecutores.isEmpty()) {
            for (UsuarioDTO ejecutor : ejecutores) {
                this.ejecutoresSelectItem.add(new SelectItem(ejecutor
                    .getLogin(), ejecutor.getNombreCompleto()));
            }
        }

        Collections.sort(this.ejecutoresSelectItem, new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem sItem1, SelectItem sItem2) {
                String sItem1Label = sItem1.getLabel();
                String sItem2Label = sItem2.getLabel();

                return (sItem1Label.compareToIgnoreCase(sItem2Label));
            }
        });

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de solicitud
     *
     * @author javier.aponte
     */
    public void cambioTipoSolicitud() {

        this.datosConsultaTramite.setTipoTramite(null);
        this.datosConsultaTramite.setClaseMutacion(null);
        this.datosConsultaTramite.setSubtipo(null);
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado al cambiar el tipo de trámite
     *
     * @author javier.aponte
     */
    public void cambioTipoTramite() {

        this.datosConsultaTramite.setClaseMutacion(null);
        this.datosConsultaTramite.setSubtipo(null);
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado para limpiar el valor del departamentoId y del municipioId
     *
     * @author javier.aponte
     */
    public void limpiarDptosMunicipios() {
        this.datosConsultaTramite.setDepartamentoId("");
        this.datosConsultaTramite.setMunicipioId("");
    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de buscar trámites por filtro, ejecutado en la acción del botón buscar
     *
     * @author javier.aponte
     */
    @SuppressWarnings("serial")
    public void buscarTramitesPorFiltro() {

        if (validarDatosConsultaTramite()) {

            this.lazyAssignedTramites = new LazyDataModel<Tramite>() {

                @Override
                public List<Tramite> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<Tramite> answer = new ArrayList<Tramite>();
                    populateBusquedaTramitesLazyly(answer, first, pageSize,
                        sortField, sortOrder.toString());

                    return answer;
                }
            };
            poblar();

        }

        this.renderResultPanel = true;

        if (this.lazyAssignedTramites.getRowCount() == 0) {
            this.addMensajeWarn(
                "No se encontraron trámites que cumplan los criterios de busqueda, " +
                
                "ó es posible que el(los) trámite(s) no tenga(n) ejecutor asignado, que ya se haya generado una resolución para " +
                
                "el(los) trámite(s), o que el(los) trámites tengan una comisión asociada que este en estado 'En Ejecución' o 'Por Ejecutar'");

        }

    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de validar los datos basicos para la consulta de trámites
     *
     * @author javier.aponte
     */
    private boolean validarDatosConsultaTramite() {

        NumeroPredialPartes npp = this.datosConsultaTramite
            .getNumeroPredialPartes();

        if (this.datosConsultaTramite.getFechaRadicacionFinal() != null &&
             this.datosConsultaTramite.getFechaRadicacion() == null) {
            String mensaje =
                "Se ingresó una fecha final de radicación pero no una fecha inicial de radicación, por favor" +
                 "corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (!(this.datosConsultaTramite.getDepartamentoId() != null && !this.datosConsultaTramite
            .getDepartamentoId().isEmpty()) &&
             !(this.datosConsultaTramite.getMunicipioId() != null && !this.datosConsultaTramite
            .getMunicipioId().isEmpty()) &&
             !(this.datosConsultaTramite.getNumeroSolicitud() != null && !this.datosConsultaTramite
            .getNumeroSolicitud().isEmpty()) &&
             !(this.datosConsultaTramite.getFechaSolicitud() != null) &&
             !(this.datosConsultaTramite.getTipoSolicitud() != null && !this.datosConsultaTramite
            .getTipoSolicitud().isEmpty()) &&
             !(this.datosConsultaTramite.getNumeroRadicacion() != null && !this.datosConsultaTramite
            .getNumeroRadicacion().isEmpty()) &&
             !(this.datosConsultaTramite.getFechaRadicacion() != null) &&
             !(this.datosConsultaTramite.getTipoTramite() != null && !this.datosConsultaTramite
            .getTipoTramite().isEmpty()) &&
             !(this.datosConsultaTramite.getClaseMutacion() != null && !this.datosConsultaTramite
            .getClaseMutacion().isEmpty()) &&
             !(this.datosConsultaTramite.getSubtipo() != null && !this.datosConsultaTramite
            .getSubtipo().isEmpty()) &&
             !(this.datosConsultaTramite.getClasificacion() != null && !this.datosConsultaTramite
            .getClasificacion().isEmpty()) &&
             !(this.datosConsultaTramite.getFuncionarioEjecutor() != null &&
            !this.datosConsultaTramite
                .getFuncionarioEjecutor().isEmpty()) &&
             !(this.datosConsultaTramite.getNumeroResolucion() != null && !this.datosConsultaTramite
            .getNumeroResolucion().isEmpty()) &&
             !(this.datosConsultaTramite.getFechaResolucion() != null) &&
             !(this.datosConsultaTramite.getTipoIdentificacion() != null &&
            !this.datosConsultaTramite
                .getTipoIdentificacion().isEmpty()) &&
             !(this.datosConsultaTramite.getNumeroIdentificacion() != null &&
            !this.datosConsultaTramite
                .getNumeroIdentificacion().isEmpty()) &&
             !(this.datosConsultaTramite.getDigitoVerificacion() != null &&
            !this.datosConsultaTramite
                .getDigitoVerificacion().isEmpty())) {

            if (!(npp.getDepartamentoCodigo() != null && !npp
                .getDepartamentoCodigo().isEmpty()) &&
                 !(npp.getMunicipioCodigo() != null && !npp
                .getMunicipioCodigo().isEmpty()) &&
                 !(npp.getTipoAvaluo() != null && !npp.getTipoAvaluo()
                .isEmpty()) &&
                 !(npp.getSectorCodigo() != null && !npp
                .getSectorCodigo().isEmpty()) &&
                 !(npp.getComunaCodigo() != null && !npp
                .getComunaCodigo().isEmpty()) &&
                 !(npp.getBarrioCodigo() != null && !npp
                .getBarrioCodigo().isEmpty()) &&
                 !(npp.getManzanaVeredaCodigo() != null && !npp
                .getManzanaVeredaCodigo().isEmpty()) &&
                 !(npp.getPredio() != null && !npp.getPredio().isEmpty()) &&
                 !(npp.getCondicionPropiedad() != null && !npp
                .getCondicionPropiedad().isEmpty()) &&
                 !(npp.getEdificio() != null && !npp.getEdificio()
                .isEmpty()) &&
                 !(npp.getPiso() != null && !npp.getPiso().isEmpty()) &&
                 !(npp.getUnidad() != null && !npp.getUnidad().isEmpty())) {

                String mensaje =
                    "Para realizar la busqueda de trámites se requiere por lo menos un parámetro." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                return false;
            }
        }

        return true;

    }

    // ------------------------------------------------------------------------
    public void poblar() {

        int count;
        count = this.getTramiteService()
            .contarTramitePorFiltroTramiteAdministracionAsignaciones(
                this.datosConsultaTramite);

        this.banderaBusquedaTramites = true;
        this.banderaDatosRegistroNotificacion = false;

        if (count > 0) {
            this.lazyAssignedTramites.setRowCount(count);
        } else {
            this.lazyAssignedTramites.setRowCount(0);
        }

    }

    // ------------------------------------------------------------------------
    private void populateBusquedaTramitesLazyly(List<Tramite> answer,
        int first, int pageSize, String sortField, String sortOrder) {

        this.rows = new ArrayList<Tramite>();

        int size;
        this.rows = this.getTramiteService()
            .buscarTramitePorFiltroTramiteAdministracionAsignaciones(
                datosConsultaTramite, sortField, sortOrder, first,
                pageSize);
        size = rows.size();

        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del botón "limpiar" pone los combos de departamentos y municipios como al comienzo
     */
    public void resetForm() {

        // D: esto hace que, cuando se refresque la página y se vuelvan a armar
        // los combos, queden
        // como al inicio
        this.datosConsultaTramite.setTerritorialId(null);

        // D: definir a mano valores null o vacíos para los otros campos
        this.datosConsultaTramite = new FiltroDatosConsultaTramite();

        // D: se establece el objeto de números prediales
        this.datosConsultaTramite
            .setNumeroPredialPartes(new NumeroPredialPartes());

        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();

        this.lazyAssignedTramites = null;

        this.renderResultPanel = false;

        this.selectedTramiteRow = null;

    }

    public void seleccionTramiteDeResultadosBusquedaListener(SelectEvent event) {
        switch (this.listenerMethodIdentifier) {

            // D: el trámite seleccionado debe reemplazar a lo que ya están en la
            // lista para que solo
            // haya uno
            case (1): {
                this.adicionarTramiteListener(event, true);
                break;
            }

            // D: se pueden seleccionar varios predios que se van sumando a la
            // lista
            case (2): {
                this.adicionarTramiteListener(event, false);
                break;
            }
            default: {
                break;
            }
        }
    }

    // -------------------------------------------------
    /**
     * @modified pedro.garcia cambio de firma del método: para admitir varargs. Se usa para saber si
     * el trámite se adiciona o reemplaza al que esté en el arreglo (para los casos donde solo se
     * quiere un trámite seleccionado)
     * @param replaceTramite optional argument false es el valor que se considera por defecto; true
     * si se quiere que en lugar de adicionarlo a la lista remplace al que esté allí
     */
    // TODO::pedro.garcia::cambiar a private cuando se haya puesto la búsqueda
    // con el componente en todos los sitios::pedro.garcia
    // TODO::pedro.garcia::revisar, porque fue necesario adicionar el metodo
    // previo, ya que este no es valido para JSF como un listener, y ya se
    // usaba::fredy.wilches
    private void adicionarTramiteListener(SelectEvent event,
        boolean... replaceTramite) {

        try {
            Tramite tramite = (Tramite) event.getObject();
            if (tramite != null) {
                if (!estaSeleccionado(tramite.getId())) {
                    // D: si el trámite debe reemplazar al que estaba (o, a los
                    // que estaban)
                    if (replaceTramite != null && replaceTramite.length > 0 &&
                         replaceTramite[0] == true) {
                        this.tramitesSeleccionados.clear();
                    }
                    this.tramitesSeleccionados.add(tramite);
                } else {
                    this.addMensajeError("El trámite con número de radicación " +
                         tramite.getNumeroRadicacion() +
                         " ya se encuentra seleccionado");
                }
            } else {
                this.addMensajeError("No al seleccionar el objeto ");
            }

        } catch (Exception e) {
            LOGGER.error("error adicionando/reemplazando trámite: " +
                 e.getMessage());
        }

    }

    // --------------------------------------------------------------------------------------------------
    public boolean estaSeleccionado(Long tramiteId) {
        for (Tramite tramiteTmp : this.tramitesSeleccionados) {
            if (tramiteId.equals(tramiteTmp.getId())) {
                return true;
            }
        }
        return false;
    }

    public void limpiarTramites() {
        this.tramitesSeleccionados = new ArrayList<Tramite>();
    }

    public void eliminarTramite() {
        this.tramitesSeleccionados.remove(this.tramiteSeleccionado2);
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza un filtro por diferentes criterios para la búsqueda de trámites en la
     * pantalla inicial de listaTareas.
     *
     * @author david.cifuentes
     */
    /*
     * @modified by juanfelipe.garcia se adiciona el campo estadoTramite en el filtro (CU - 181) y
     * se adiciona logica para menejo de estados
     */
    public void busquedaConFiltros() {

        List<Tramite> listaTramitesFiltrados = new ArrayList<Tramite>();
        this.tramitesResult = new ArrayList<Tramite>();
        this.tramiteObs = new HashMap<Long, String>();

        //		this.lazyModel = new TramiteLazyDataModel(tramitesResult);
        //para el caso en el que el usuario no tenga habilitado el selector de Estado de tramite,
        //la consulta debe mostrar solo los tramites activos
        //felipe.cadena::07-12-2016::Se validan las consultas para los usuarios delegados
        if (this.usuarioDelegado || this.usuarioHabilitada) {
            String codigoMun = (this.datosConsultaPredio.getNumeroPredialS1() + datosConsultaPredio.
                getNumeroPredialS2());

            if (codigoMun != null && !codigoMun.isEmpty()) {
                List<Municipio> municipiosList;

                boolean predioExiste = false;
                municipiosList = this.getGeneralesService().buscarMunicipioPorIdOficina(
                    this.currentUser.getCodigoTerritorial());

                for (Municipio mun : municipiosList) {
                    if (mun.getCodigo().equals(codigoMun)) {
                        predioExiste = true;
                        break;
                    }
                }

                if (!predioExiste) {
                    this.addMensajeWarn(ECodigoErrorVista.CONSULTA_DELEGACION_001.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_DELEGACION_001.getMensajeTecnico());
                    return;
                }
            }
        }

        try {

            if ((this.datosConsultaTramite.getNumeroSolicitud() != null &&
                 !this.datosConsultaTramite.getNumeroSolicitud().isEmpty()) &&
                 (this.datosConsultaTramite.getNumeroRadicacion() == null ||
                 this.datosConsultaTramite.getNumeroRadicacion().isEmpty())) {

                Solicitud s = this.getConservacionService().findSolicitudByNumSolicitud(
                    this.datosConsultaTramite.getNumeroSolicitud());

                if (s.getTipo().equals(ESolicitudTipo.TRAMITE_ACTUALIZACION.getCodigo())) {
                    this.addMensajeWarn(ECodigoErrorVista.CONSULTA_TRAMITE_001.toString());
                    LOGGER.error(ECodigoErrorVista.CONSULTA_TRAMITE_001.getMensajeTecnico());
                    return;
                }

            }

            // Esto debido a los cambios 
            this.datosConsultaTramite.setEstadoTramite(this.estadoTramite);

            // Filtro funcionario ejecutor
            if (this.unidadOperativaSeleccionada != null) {
                if (this.usuarioDelegado || this.usuarioHabilitada) {
                    this.datosConsultaTramite.setTerritorialId(this.currentUser.
                        getCodigoTerritorial());
                } else {
                    this.datosConsultaTramite.setTerritorialId(
                        this.unidadOperativaSeleccionadaCodigo);
                }
            } else {
                if (this.usuarioDelegado || this.usuarioHabilitada) {
                    this.datosConsultaTramite.setTerritorialId(this.currentUser.
                        getCodigoTerritorial());
                } else {
                    this.datosConsultaTramite.setTerritorialId(this.territorialSeleccionadaCodigo);
                }
            }

            // Filtro unidad operativa de catastro
            if (this.unidadOperativaSeleccionada != null) {
                this.datosConsultaTramite.setCodidoUnidadOperativa(this.unidadOperativaSeleccionada.
                    getCodigo());
            }

            listaTramitesFiltrados = this.getTramiteService()
                .consultaDeTramitesPorFiltros(true,
                    this.datosConsultaTramite,
                    this.datosConsultaPredio,
                    this.filtroDatosConsultaSolicitante,
                    true,
                    this.funcionarioEjecutor,
                    this.territorialSeleccionada,
                    this.unidadOperativaSeleccionada);

            if (listaTramitesFiltrados == null || listaTramitesFiltrados.isEmpty()) {
                this.addMensajeWarn(
                    "La búsqueda no encontró ningún trámite, por favor intente nuevamente.");
                return;
            }

            for (Tramite t : listaTramitesFiltrados) {
                if (t.getEstado() != null && this.estadoTramite.equals(ETramiteEstado.CANCELADO.
                    getCodigo())) {
                    try {
                        obtenerUltimoTramiteEstado(t.getId(), t.getTramiteEstados());
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }

                if (t.getTramites() != null && !t.getTramites().isEmpty()) {
                    setObservacionConAsociados(t);
                }

                this.tramitesResult.add(t);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error al realizar la búsqueda de los trámites.");
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método para obtener el ultimo motivo de el tramiteEstado de un tramite cancelado.
     *
     * @param idTramite valor que se usa como llave del HashMap para poder consultarlo en la pagina
     *
     * @param tramEstadoList lista con los TramiteEstados de un tramite cancelado
     *
     * @author juanfelipe.garcia
     *
     */
    private void setObservacionConAsociados(Tramite inTramite) {
        String msjObservacionAsociados = "";
        for (Tramite tAsoc : inTramite.getTramites()) {
            if (tAsoc.getEstado().equals(ETramiteEstado.RECIBIDO.getCodigo()) || tAsoc.getEstado().
                equals(ETramiteEstado.APROBADO.getCodigo())) {
                msjObservacionAsociados =
                    "Trámite a la espera de realizar trámites asociados, no puede ser ejecutado";
                break;
            }
        }
        if (tramiteObs.containsKey(inTramite.getId())) {
            this.tramiteObs.put(inTramite.getId(), tramiteObs.get(inTramite.getId()) + "-" +
                msjObservacionAsociados);
        } else {
            this.tramiteObs.put(inTramite.getId(), msjObservacionAsociados);
        }
    }

    private void obtenerUltimoTramiteEstado(Long idTramite, List<TramiteEstado> tramEstadoList) {
        Date fechaLog = tramEstadoList.get(0).getFechaLog();
        String obs = "";

        for (TramiteEstado te : tramEstadoList) {
            if (te.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo())) {
                if (obs.isEmpty()) {
                    obs = te.getMotivo();
                }
                if (te.getFechaLog().after(fechaLog)) {
                    fechaLog = te.getFechaLog();
                    obs = te.getMotivo();
                }
            }
        }
        this.tramiteObs.put(idTramite, obs);
    }

    // ----------------------------------------------------- //
    /**
     * Método que inizializa los filtros de busqueda de trámites en la pantalla de recibir
     * documentos.
     *
     * @author david.cifuentes
     *
     */
    public void limpiarFiltros() {
        this.init();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta cuando se selecciona una territorial del menú de las territoriales.
     *
     * @author david.cifuentes
     *
     */
    public void setTerritorialSeleccionada() {
        this.territorialSeleccionada = findTerritorial(territorialSeleccionadaCodigo);
    }

    public void setUnidadesOperativasCatastro() {
        if (!this.territorialSeleccionada.isDireccionGeneral()) {
            this.unidadesOperativasCatastroObj = this.getGeneralesService()
                .getCacheEstructuraOrganizacionalPorTipoYPadre(
                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                    this.territorialSeleccionadaCodigo);

            this.unidadesOperativasCatastro = new ArrayList<SelectItem>();

            for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
                this.unidadesOperativasCatastro.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        }

    }

    public void setUnidadOperativaSeleccionada() {
        if (this.currentUser.isUoc()) {
            LOGGER.info("Predefinir UOC igual a la del usuario actual");
            this.unidadOperativaSeleccionadaCodigo = this.currentUser.getCodigoUOC();

            this.unidadOperativaSeleccionada = null;

            for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
                if (eo.getCodigo().equals(this.unidadOperativaSeleccionadaCodigo)) {
                    this.unidadOperativaSeleccionada = eo;
                    break;
                }
            }
        }
    }

    public void setSubdireccionesOficinas() {

        /*
         * Sección para la asignación de subdireccionesoficinas al objeto subdireccionesOficinasObj
         */
        if (this.territorialSeleccionada.isDireccionGeneral()) {
            this.subdireccionesOficinasObj = this.getGeneralesService()
                .getCacheSubdireccionesYOficinas(Constantes.DIRECCION_GENERAL_CODIGO);
        } else {
            this.subdireccionesOficinasObj = this.getGeneralesService()
                .getCacheEstructuraOrganizacionalPorTipoYPadre(
                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                    this.territorialSeleccionadaCodigo);
        }

        /*
         * Sección para la asignación de subdireccionesoficinas al objeto subdireccionesOficinas
         */
        this.subdireccionesOficinas = new ArrayList<SelectItem>();
        this.subdireccionesOficinas.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (EstructuraOrganizacional eo : this.subdireccionesOficinasObj) {
            this.subdireccionesOficinas.add(new SelectItem(eo.getCodigo(), eo.getNombre()));

        }

    }

    public void onCambioTerritorial() {
        setTerritorialSeleccionada();
        if (territorialSeleccionada != null) {
            setUnidadesOperativasCatastro();
            setSubdireccionesOficinas();
            setUnidadOperativaSeleccionada();
            cargarFuncionariosEjecutores();
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta cuando se selecciona un estado
     *
     * @author juanfelipe.garcia
     *
     */
    public void seleccionaEstado() {
        if (estadoTramite.equals(ETramiteEstado.CANCELADO.getCodigo())) {
            this.observaciones = true;
        } else {
            this.observaciones = false;
        }

    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional findTerritorial(String codigoTerritorial) {
        EstructuraOrganizacional estructuraOrganizacional, answer = null;
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            if (eo.getCodigo().equals(codigoTerritorial)) {
                answer = eo;
                break;
            }
        }
        if (answer != null) {
            estructuraOrganizacional = this
                .getGeneralesService()
                .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
            if (estructuraOrganizacional != null) {
                return estructuraOrganizacional;
            } else {
                return answer;
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar una UOC.
     */
    // ----------------------------------------------------- //
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de persona
     */
    public void tipoPersonaSel() {
        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        if (this.tipoPersona == 0) {
            this.tipoPersonaN = true;
            this.tipoPersonaJ = false;
            this.banderaSolicitanteSolicitudSeleccionado = false;
            this.filtroBusquedaSolicitud.setNumeroIdentificacion("");
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo());
            this.solicitudes = new ArrayList<VSolicitudPredio>();
            this.banderaBuscarSolicitantes = false;
            this.banderaBuscarSolicitudes = false;
            this.banderaSolicitudSeleccionada = false;
        } else if (this.tipoPersona == 1) {
            this.tipoPersonaJ = true;
            this.tipoPersonaN = false;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT
                    .getCodigo());
            this.banderaSolicitanteSolicitudSeleccionado = false;
            this.filtroBusquedaSolicitud.setNumeroIdentificacion("");
            this.solicitudes = new ArrayList<VSolicitudPredio>();
            this.banderaBuscarSolicitantes = false;
            this.banderaBuscarSolicitudes = false;
            this.banderaSolicitudSeleccionada = false;
        } else {
            this.tipoPersonaJ = false;
            this.tipoPersonaN = false;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que busca en Alfresco la resolucion del trámite.
     *
     * @author david.cifuentes
     */
    public void cargarResolucion() {

        if (this.tramiteSeleccionado2 != null &&
             this.tramiteSeleccionado2.getResultadoDocumento() != null &&
             this.tramiteSeleccionado2.getResultadoDocumento()
                .getIdRepositorioDocumentos() != null &&
             this.tramiteSeleccionado2.getNumeroResolucion() != null &&
             !this.tramiteSeleccionado2.getNumeroResolucion()
                .isEmpty()) {

            // Descargar la resolución de alfresco y le agrega una marca de agua
            this.reporteDocumentoWaterMark = this.reportsService.consultarReporteDeGestorDocumental(
                this.tramiteSeleccionado2.getResultadoDocumento().getIdRepositorioDocumentos());

        }

    }

    // ----------------------------------------------------- //
    /**
     * Action sobre el botón 'Volver', redirige a la página principal removiendo el managed bean
     * actual.
     *
     * @author david.cifuentes
     */
    public String volverAlInicio() {
        UtilidadesWeb.removerManagedBean("consultaTramite");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    public String getEstadoTramite() {
        return estadoTramite;
    }

    public void setEstadoTramite(String estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @return the estadoTramiteItemList
     */
    public List<SelectItem> getEstadosTramiteItemList() {

        if (!this.estadosTramiteItemList.isEmpty()) {
            this.estadosTramiteItemList.clear();
        }
        //this.estadosTramiteItemList.add(new SelectItem("", this.DEFAULT_COMBOS));
        this.estadosTramiteItemList.
            add(new SelectItem(ETramiteEstado.ACTIVO.getCodigo(), "ACTIVOS"));
        this.estadosTramiteItemList.add(new SelectItem(ETramiteEstado.FINALIZADO_APROBADO.
            getCodigo(), ETramiteEstado.FINALIZADO_APROBADO.getCodigo()));
        this.estadosTramiteItemList.add(new SelectItem(ETramiteEstado.ARCHIVADO.getCodigo(),
            ETramiteEstado.ARCHIVADO.getCodigo()));
        this.estadosTramiteItemList.add(new SelectItem(ETramiteEstado.CANCELADO.getCodigo(),
            ETramiteEstado.CANCELADO.getCodigo()));
        this.estadosTramiteItemList.add(new SelectItem(ETramiteEstado.RECHAZADO.getCodigo(),
            ETramiteEstado.RECHAZADO.getCodigo()));
        return estadosTramiteItemList;
    }

    // ----------------------------------------------------- //
    /**
     * Obtener los documentos asociados al tramite seleccionado
     *
     * @author juanfelipe.garcia
     */
    public void cargarDocumentos() {
        Tramite tempTramite;
        this.documentacionRequerida = new ArrayList<TramiteDocumentacion>();
        this.documentacionAdicional = new ArrayList<TramiteDocumentacion>();
        this.documentacionGenerada = new ArrayList<Documento>();

        if (tramiteSeleccionadoDocumentos != null) {
            //se consultan los documentos del trámite ()
            tempTramite = this.getTramiteService().consultarDocumentacionCompletaDeTramite(
                tramiteSeleccionadoDocumentos.getId());
            if (tempTramite.getTramiteDocumentacions() != null &&
                 !tempTramite.getTramiteDocumentacions().isEmpty()) {

                for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                    if (td.getRequerido() != null &&
                         td.getRequerido().equals(ESiNo.SI.toString())) {
                        this.documentacionRequerida.add(td);
                    }
                }

                for (TramiteDocumentacion td : tempTramite.getTramiteDocumentacions()) {
                    if (td.getAdicional() != null &&
                         td.getAdicional().equals(ESiNo.SI.toString()) &&
                        
                        (td.getRequerido() == null || td.getRequerido().equals(ESiNo.NO.toString()))) {
                        this.documentacionAdicional.add(td);
                    }
                }

            }

            //Documento soporte de actualizacion
            if (tramiteSeleccionadoDocumentos.isActualizacion()) {
                Documento docActualizacion = this.getGeneralesService().
                    buscarDocumentoPorTramiteIdyTipoDocumento(this.tramiteSeleccionadoDocumentos.
                        getId(),
                        ETipoDocumento.DOCUMENTO_RESULTADO_DE_ACTUALIZACION.getId());
                this.documentacionGenerada.add(docActualizacion);
            }

            //ETipoDocumento.AUTO_DE_PRUEBAS_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId()
            //ETipoDocumento.AUTO_DE_PRUEBAS_REVISION_AVALUO_AUTOESTIMACION_Y_REPOSICION.getId()
            if (tempTramite.getTramitePruebas() != null && tempTramite.getTramitePruebas().
                getAutoPruebasDocumento() != null) {
                this.documentacionGenerada.add(tempTramite.getTramitePruebas().
                    getAutoPruebasDocumento());
            }
            List<ComisionTramite> comisionTramiteLista = this.getTramiteService().
                buscarComisionesTramitePorIdTramite(tramiteSeleccionadoDocumentos.getId());

            boolean tieneDocCom = false;
            for (ComisionTramite ct : comisionTramiteLista) {
                //ETipoDocumento.MEMORANDO_DE_APROBACION_DE_COMISION.getId()
                if (ct.getComision().getMemorandoDocumentoId() != null) {
                    Long id = ct.getComision().getMemorandoDocumentoId();
                    Documento doc = this.getTramiteService().buscarDocumentoPorId(id);
                    this.documentacionGenerada.add(doc);
                    tieneDocCom = true;
                }
                List<ComisionEstado> comisionEstadoList = this.getTramiteService().
                    consultarHistoricoCambiosEstadoComision(ct.getComision().getId());
                //ETipoDocumento.APROBAR_SOLICITUD_DE_COMISION.getId()
                //ETipoDocumento.RECHAZO_DE_COMISION.getId()
                if (comisionEstadoList != null) {
                    for (ComisionEstado ce : comisionEstadoList) {
                        if (ce.getMemorandoDocumento() != null) {
                            if (tieneDocCom && !ce.getMemorandoDocumento().getId().equals(ct.
                                getComision().getMemorandoDocumentoId())) {
                                Documento doc = this.getTramiteService().buscarDocumentoPorId(ce.
                                    getMemorandoDocumento().getId());
                                this.documentacionGenerada.add(doc);
                            } else if (!tieneDocCom) {
                                Documento doc = this.getTramiteService().buscarDocumentoPorId(ce.
                                    getMemorandoDocumento().getId());
                                this.documentacionGenerada.add(doc);
                            }

                        }
                    }
                }
            }

            //ETipoDocumento.CONSTANCIA_DE_RADICACION
            List<Documento> docsSolicitudTramites = this.getTramiteService()
                .buscarDocumentosPorSolicitudId(tramiteSeleccionadoDocumentos.getSolicitud().getId());
            this.documentacionGenerada.addAll(docsSolicitudTramites);

            Documento documentoNotificacion = this.getGeneralesService().
                buscarDocumentoPorTramiteIdyTipoDocumento(
                    this.tramiteSeleccionadoDocumentos.getId(),
                    ETipoDocumento.DOCUMENTO_DE_NOTIFICACION.getId());
            if (documentoNotificacion != null) {
                this.documentacionGenerada.add(documentoNotificacion);
            }

            List<TramiteDocumento> listaDocumentos = this.getTramiteService().
                obtenerTramiteDocumentosPorTramiteId(tramiteSeleccionadoDocumentos.getId());

            if (listaDocumentos != null && !listaDocumentos.isEmpty()) {
                for (TramiteDocumento td : listaDocumentos) {
                    if (td.getDocumento().getTipoDocumento().getId().equals(
                        ETipoDocumento.CONSTANCIA_DE_RADICACION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_NO_PROCEDENCIA_AUTOESTIMACION_Y_REVISION_DE_AVALUO.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_NO_PROCEDENCIA_MUTACIONES.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_NO_PROCEDENCIA_VIA_GUBERNATIVA.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_PARA_RETIRAR_TRAMITE_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_SOLICITUD_DOCUMENTOS_FALTANTES_DE_REVISION_AVALUO_Y_MUTACIONES.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.INFORME_VISITA_REVISION_O_AUTOAVALUO.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.RESOLUCIONES_DE_CONSERVACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.EDICTO_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.EDICTO_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_Y_REPOSICION.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.COMUNICADO_CANCELACION_TRAMITE_CATASTRAL.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_SUSPENSION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_APLAZAMIENTO_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_REACTIVACION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_AMPLIACION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.MEMORANDO_DE_CANCELACION_DE_COMISION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.DOCUMENTO_RESULTADO_DE_ACTUALIZACION.getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_REPOSICION.
                                getId()) ||
                        td.getDocumento().getTipoDocumento().getId().equals(
                            ETipoDocumento.RESOLUCIONES_DE_ACTUALIZACION_QUEJA_APELACION_REVOCATORIA_DIRECTA.
                                getId())) {
                        this.documentacionGenerada.add(td.getDocumento());
                    }
                }
            }
            //se valida que no tenga documentos repetidos
            Set<Documento> hs = new HashSet<Documento>();
            hs.addAll(this.documentacionGenerada);
            this.documentacionGenerada.clear();
            this.documentacionGenerada.addAll(hs);
        }
    }

    public void setEstadosTramiteItemList(List<SelectItem> estadosTramiteItemList) {
        this.estadosTramiteItemList = estadosTramiteItemList;
    }

    // ----------------------------------------------------- //
    /**
     * Valida si el usuario tiene permisos para hacer consulta por estados de tramite
     *
     * @author juanfelipe.garcia
     */
    public boolean isRolPermitidoDocumentosTramite() {
        return rolPermitidoDocumentosTramite;
    }

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    public String cargarDocumentoTemporalPrev() {

        /* FileNameMap fileNameMap = URLConnection.getFileNameMap(); return this.tipoMimeDocTemporal
         * = fileNameMap
                        .getContentTypeFor(this.getSelectedTramiteDocumentacion().getDocumentoSoporte().getArchivo()); */
        return "";
    }

    public List<TramiteDocumentacion> getDocumentacionRequerida() {
        return documentacionRequerida;
    }

    public void setDocumentacionRequerida(
        List<TramiteDocumentacion> documentacionRequerida) {
        this.documentacionRequerida = documentacionRequerida;
    }

    public List<TramiteDocumentacion> getDocumentacionAdicional() {
        return documentacionAdicional;
    }

    public void setDocumentacionAdicional(
        List<TramiteDocumentacion> documentacionAdicional) {
        this.documentacionAdicional = documentacionAdicional;
    }

    public Tramite getTramiteSeleccionadoDocumentos() {
        return tramiteSeleccionadoDocumentos;
    }

    public void setTramiteSeleccionadoDocumentos(Tramite tramiteSeleccionadoDocumentos) {
        this.tramiteSeleccionadoDocumentos = tramiteSeleccionadoDocumentos;
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    public List<Documento> getDocumentacionGenerada() {
        return documentacionGenerada;
    }

    public void setDocumentacionGenerada(List<Documento> documentacionGenerada) {
        this.documentacionGenerada = documentacionGenerada;
    }

    public HashMap<Long, String> getTramiteObs() {
        return tramiteObs;
    }

    public void setTramiteObs(HashMap<Long, String> tramiteObs) {
        this.tramiteObs = tramiteObs;
    }

    public boolean isObservaciones() {
        return observaciones;
    }

    public void setObservaciones(boolean observaciones) {
        this.observaciones = observaciones;
    }

    public TramiteDocumentacion getSelectedTramiteDocumentacion() {
        return selectedTramiteDocumentacion;
    }

    public void setSelectedTramiteDocumentacion(TramiteDocumentacion selectedTramiteDocumentacion) {
        this.selectedTramiteDocumentacion = selectedTramiteDocumentacion;
    }

    public List<SelectItem> getTipoPersonaList() {
        return tipoPersonaList;
    }

    public void setTipoPersonaList(List<SelectItem> tipoPersonaList) {
        this.tipoPersonaList = tipoPersonaList;
    }

    // ------------------------------------------------------------------------
    /**
     * Método encargado de buscar trámites por filtro, ejecutado en la acción del botón buscar del
     * caso de uso de asociar derecho de petición o tutela a trámite
     *
     * @author javier.aponte
     */
    @SuppressWarnings("serial")
    public void buscarTramitesPorFiltroDerechoPeticionOTutela() {

        //TODO::javier.aponte :: llamar a validación y cambiar
        if (true) {

            this.lazyAssignedTramites = new LazyDataModel<Tramite>() {

                @Override
                public List<Tramite> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<Tramite> answer = new ArrayList<Tramite>();
                    populateBusquedaTramitesLazylyDerechoPeticionOTutela(answer, first, pageSize,
                        sortField, sortOrder.toString());

                    return answer;
                }
            };
            poblarTramitesDerechoDePeticionOTutela();

            this.renderResultPanel = true;

            if (this.lazyAssignedTramites.getRowCount() == 0) {
                this.addMensajeWarn(
                    "No se encontraron trámites que cumplan los criterios de busqueda, " +
                    
                    "ó es posible que el(los) trámite(s) no tenga(n) número de radicación o que ya estén en firme");

            }

        }

    }

    // ------------------------------------------------------------------------
    private void populateBusquedaTramitesLazylyDerechoPeticionOTutela(List<Tramite> answer,
        int first, int pageSize, String sortField, String sortOrder) {

        this.rows = new ArrayList<Tramite>();

        int size;
        this.rows = this.getTramiteService()
            .buscarTramitePorFiltroTramiteDerechoPeticionOTutela(
                this.datosConsultaTramite, this.filtroDatosConsultaSolicitante, sortField, sortOrder,
                first,
                pageSize);
        size = rows.size();

        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }
    }

    // ------------------------------------------------------------------------
    public void poblarTramitesDerechoDePeticionOTutela() {

        int count;
        count = this.getTramiteService()
            .contarTramitePorFiltroTramiteDerechoPeticionOTutela(
                this.datosConsultaTramite, this.filtroDatosConsultaSolicitante);

        this.banderaBusquedaTramites = true;
        this.banderaDatosRegistroNotificacion = false;

        if (count > 0) {
            this.lazyAssignedTramites.setRowCount(count);
        } else {
            this.lazyAssignedTramites.setRowCount(0);
        }

    }
}
