package co.gov.igac.snc.web.mb.tareas;

import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.ProcesoDeProductosCatastrales;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeControlCalidad;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;

public enum ETipoProceso {
    ACTUALIZACION(ProcesoDeActualizacion.MAPA_PROCESO),
    CONSERVACION(ProcesoDeConservacion.MAPA_PROCESO),
    OSMI(ProcesoDeOfertasInmobiliarias.MAPA_PROCESO),
    OSMI_CALIDAD(ProcesoDeControlCalidad.MAPA_PROCESO),
    PRODUCTOS_CATASTRALES(ProcesoDeProductosCatastrales.MAPA_PROCESO);

    private String tipoProceso;

    private ETipoProceso(String tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public String toString() {
        return tipoProceso;
    }

}
