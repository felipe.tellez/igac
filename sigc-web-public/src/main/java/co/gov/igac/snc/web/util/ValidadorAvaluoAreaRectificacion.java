/*
 * Proyecto SNC
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Clase con las validaciones sobre el area y el avaluo de los tramites de
 * retificacion
 *
 * @cu CC-NP-CO-083
 * @author felipe.cadena
 */
public class ValidadorAvaluoAreaRectificacion extends SNCManagedBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidadorAvaluoAreaRectificacion.class);

	private enum ECampoRC implements Serializable {
		UNIDAD_CONSTRUCCION_USO_ID(24L),
		UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION(32L),
		PREDIO_ZONA_ZONA_FISICA(48L),
		PREDIO_ZONA_ZONA_GEOECONOMICA(49L);

		private final Long id;

		ECampoRC(Long id) {
			this.id = id;
		}

		public Long getId() {
			return id;
		}
	}

	/**
	 * Realiza las validaciones de avaluo y areas para tramites de rectificacion
	 * 
	 * @author felipe.cadena
	 * @param predio
	 * @param rectificaciones
	 * @return
	 */
	public boolean validar(PPredio predio, List<TramiteRectificacion> rectificaciones) {

		List<DatoRectificar> datosRectificar = new ArrayList<DatoRectificar>();

		if (predio == null) {
			this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_01.toString());
			return false;
		}

		for (TramiteRectificacion rec : rectificaciones) {
			datosRectificar.add(rec.getDatoRectificar());
		}

		if (datosRectificar.contains(new DatoRectificar(ECampoRC.PREDIO_ZONA_ZONA_FISICA.getId()))
				|| datosRectificar.contains(new DatoRectificar(ECampoRC.PREDIO_ZONA_ZONA_GEOECONOMICA.getId()))) {
			if (!this.validarRectificacionZonas(predio)) {
				return false;
			}
		}
		if (datosRectificar.contains(new DatoRectificar(ECampoRC.UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION.getId()))
				|| datosRectificar.contains(new DatoRectificar(ECampoRC.UNIDAD_CONSTRUCCION_USO_ID.getId()))) {

			if (!this.validarAvaluoContrucciones(predio, EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
				return false;
			}

			if (!this.validarAvaluoTerrenoCP8(predio)) {
				return false;
			}

			if (!this.validarAvaluoContruccionesPrivadasCP89(predio)) {
				return false;
			}

			if (!this.validarAvaluoContrucciones(predio, EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Realiza las validaciones sobre los tramites de rectificacion de zonas
	 *
	 * @author felipe.cadena
	 * @param predio
	 * @return
	 */
	public boolean validarRectificacionZonas(PPredio predio) {

		LOGGER.debug("Valida ValidadorAvaluoAreaRectificacion#validarRectificacionArea");

		String condicionPropiedad;
		List<PPredioZona> zonas;
		double areaTotal = 0d;

		condicionPropiedad = predio.getCondicionPropiedad();

		if (EPredioCondicionPropiedad.CP_0.getCodigo().equals(condicionPropiedad)
				|| EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionPropiedad)
				|| EPredioCondicionPropiedad.CP_9.getCodigo().equals(condicionPropiedad)) {

			zonas = this.getConservacionService().buscarZonasHomogeneasPorPPredioIdYEstadoP(predio.getId(),
					EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

			if (zonas != null) {
				for (PPredioZona ppz : zonas) {
					areaTotal += ppz.getArea();
				}

				if (areaTotal != predio.getAreaTerreno()) {
					this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_02.toString());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Realiza las validaciones sobre los tramites de para las construcciones
	 * convencionales y no convencionales
	 *
	 * @author felipe.cadena
	 * @param predio
	 * @param tipoConstruccion
	 * @return
	 */
	public boolean validarAvaluoContrucciones(PPredio predio, String tipoConstruccion) {

		LOGGER.debug("Valida ValidadorAvaluoAreaRectificacion#validarAvaluoContrucciones");

		String condicionPropiedad;
		List<PUnidadConstruccion> construcciones;

		condicionPropiedad = predio.getCondicionPropiedad();

		if (EPredioCondicionPropiedad.CP_0.getCodigo().equals(condicionPropiedad)
				|| EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionPropiedad)
				|| EPredioCondicionPropiedad.CP_9.getCodigo().equals(condicionPropiedad)) {

			construcciones = this.getConservacionService().buscarUnidadesDeConstruccionPorPPredioId(predio.getId(),
					false);

			if (construcciones != null) {
				for (PUnidadConstruccion puc : construcciones) {
					if (puc.getTipoConstruccion().equals(tipoConstruccion) && puc.getAvaluo() <= 0) {
						this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_03.toString());
						return false;
					}
				}

			}
		}

		return true;
	}

	/**
	 * Realiza las validaciones sobre los tramites de rectificacion para avaluo
	 * terreno de predios de condominio
	 *
	 * @author felipe.cadena
	 * @param predio
	 * @return
	 */
	public boolean validarAvaluoTerrenoCP8(PPredio predio) {

		LOGGER.debug("Valida ValidadorAvaluoAreaRectificacion#validarAvaluoTerrenoCP8");

		String condicionPropiedad;
		List<PPredioAvaluoCatastral> avaluos;

		condicionPropiedad = predio.getCondicionPropiedad();

		if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionPropiedad)
				|| EPredioCondicionPropiedad.CP_9.getCodigo().equals(condicionPropiedad)) {

			avaluos = this.getConservacionService().obtenerListaPPredioAvaluoCatastralPorIdPPredio(predio.getId());

			if (avaluos != null && !avaluos.isEmpty()) {
				PPredioAvaluoCatastral ava = avaluos.get(0);
				if (predio.getAreaConstruccion() > 0 && ava.getValorTerrenoPrivado() <= 0) {
					this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_04.toString());
					return false;

				}

			}

		}

		return true;
	}

	/**
	 * Realiza las validaciones sobre los tramites de rectificacion para avaluos de
	 * construcciones privadas CP 8 y 9
	 *
	 * @author felipe.cadena
	 * @param predio
	 * @return
	 */
	public boolean validarAvaluoContruccionesPrivadasCP89(PPredio predio) {

		LOGGER.debug("Valida ValidadorAvaluoAreaRectificacion#validarAvaluoContruccionesPrivadasCP89");

		String condicionPropiedad;
		List<PPredioAvaluoCatastral> avaluos;

		condicionPropiedad = predio.getCondicionPropiedad();

		if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionPropiedad)) {

			avaluos = this.getConservacionService().obtenerListaPPredioAvaluoCatastralPorIdPPredio(predio.getId());

			if (avaluos != null && !avaluos.isEmpty()) {
				PPredioAvaluoCatastral ava = avaluos.get(0);
				if (ava.getValorTerrenoPrivado() <= 0) {
					this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_05.toString());
					return false;

				}

			}

		}

		return true;
	}

	/**
	 * Realiza las validaciones sobre los tramites de rectificacion de detalle de
	 * calificacion para avaluos de construcciones CP 8 y 9
	 *
	 * @author felipe.cadena
	 * @param predio
	 * @return
	 */
	public boolean validarUsoUnidadAvaluoConsCP89(PPredio predio) {

		LOGGER.debug("Valida ValidadorAvaluoAreaRectificacion#validarDetalleCalificacionAvaluoConsCP89");

		String condicionPropiedad;
		List<PPredioAvaluoCatastral> avaluos;

		condicionPropiedad = predio.getCondicionPropiedad();

		if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(condicionPropiedad)) {

			avaluos = this.getConservacionService().obtenerListaPPredioAvaluoCatastralPorIdPPredio(predio.getId());

			if (avaluos != null && !avaluos.isEmpty()) {
				PPredioAvaluoCatastral ava = avaluos.get(0);
				if (ava.getValorTerrenoPrivado() <= 0) {
					this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_04.toString());
					return false;

				}

			}

		}
		return true;
	}

}