/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.web.mb.actualizacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.actualizacion.ReporteActualizacion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import org.primefaces.model.StreamedContent;

/**
 * Clase para controlar los reportes de las tablas de avaluos para los procesos de actualizacion
 * express se usa principalmente el la vista reportesTablasActualizacion.xhtml
 *
 *
 *
 * @author javier.aponte
 */
@Component("reportesTablasActualizacion")
@Scope("session")
public class ReportesTablasActualizacionMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 7576908178244023347L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ReportesTablasActualizacionMB.class);

    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO usuario;

    private List<Documento> reportesActualizacion;

    private Documento reporteActualizacionSeleccionado;

    private ReporteDTO reporteGeneradoSeleccionado;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportesUtil = ReportesUtil.getInstance();

    /**
     * Managed bean del registro de tablas de actualización
     */
    private RegistroTablasActualizacionMB registroTablasActualizacionMB;
    
    /**
     * Variable de tipo streamed content para la descarga de reporte de inconsistencias
     */
    private StreamedContent reporteDocumentos;

    // --------------------GETTERS AND SETTERS--------------------//
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<Documento> getReportesActualizacion() {
        return reportesActualizacion;
    }

    public void setReportesActualizacion(
        List<Documento> reportesActualizacion) {
        this.reportesActualizacion = reportesActualizacion;
    }

    public Documento getReporteActualizacionSeleccionado() {
        return reporteActualizacionSeleccionado;
    }

    public void setReporteActualizacionSeleccionado(
        Documento reporteActualizacionSeleccionado) {
        this.reporteActualizacionSeleccionado = reporteActualizacionSeleccionado;
    }

    public ReporteDTO getReporteGeneradoSeleccionado() {
        return reporteGeneradoSeleccionado;
    }

    public void setReporteGeneradoSeleccionado(
        ReporteDTO reporteGeneradoSeleccionado) {
        this.reporteGeneradoSeleccionado = reporteGeneradoSeleccionado;
    }
    
    public StreamedContent getReporteDocumentos() {
        ReportesUtil rUtil = ReportesUtil.getInstance();
        
        this.reporteDocumentos = rUtil.consultarReporteDeGestorDocumental(this.reporteActualizacionSeleccionado.getIdRepositorioDocumentos()).getStreamedContentReporte();
            //this.reportesUtil.consultarReporteDeGestorDocumental(this.reporteActualizacionSeleccionado.);
         if (this.reporteDocumentos == null) {
            this.addMensajeError("NO EXISTE REPORTE EN FTP");
            LOGGER.warn("NO EXISTE REPORTE EN FTP");
        }
        return reporteDocumentos;
    }

    public void setReporteDocumentos(StreamedContent reporteDocumentos) {
        this.reporteDocumentos = reporteDocumentos;
    }

    // --------------------------METODOS--------------------------//
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        LOGGER.info("EJECUTO ESTO SSSS");
        this.cargarReportesActualizacionPorMunicipioActualizacionId();
    }

    /**
     * Método encargado de cargar los reportes de actualización por el id del municipio de
     * actualización, en caso que no haya ningún registro entonces crea en los registros para cada
     * uno de los reportes que se van a generar producto del cargue de las tablas de actualización
     *
     * @author javier.aponte
     */
    public void cargarReportesActualizacionPorMunicipioActualizacionId() {

        //1.Primero consulta en la BD si ya existen registros de los reportes
        //para el id del municipio actualización
        this.registroTablasActualizacionMB = (RegistroTablasActualizacionMB) UtilidadesWeb.
            getManagedBean("registroTablasActualizacion");

        if (this.registroTablasActualizacionMB.getMunicipioSeleccionado() != null && this.registroTablasActualizacionMB.getVigenciaSeleccionada() != null) {
            this.reportesActualizacion = this.getActualizacionService().
                buscarReportesActualizacionPorCodigoMunicipio(this.registroTablasActualizacionMB.
                    getMunicipioSeleccionado(), this.registroTablasActualizacionMB.getVigenciaSeleccionada());
        }

    }

    /**
     * Método encargado de traer el reporte del gestor documental para que pueda ser descargado
     *
     * @author javier.aponte
     */
    public void descargarArchivo() {
      //  LOGGER.info("descargue archivo " + this.reporteActualizacionSeleccionado.getIdRepositorioDocumentos());
        
        ReportesUtil rUtil = ReportesUtil.getInstance();
        
        this.reporteDocumentos = rUtil.consultarReporteDeGestorDocumental(this.reporteActualizacionSeleccionado.getIdRepositorioDocumentos()).getStreamedContentReporte();
            //this.reportesUtil.consultarReporteDeGestorDocumental(this.reporteActualizacionSeleccionado.);
         if (this.reporteDocumentos == null) {
            this.addMensajeError("NO EXISTE REPORTE EN FTP");
            LOGGER.warn("NO EXISTE REPORTE EN FTP");
        }
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author javier.aponte
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("reportesTablasActualizacion");

        return ConstantesNavegacionWeb.INDEX;
    }

}
