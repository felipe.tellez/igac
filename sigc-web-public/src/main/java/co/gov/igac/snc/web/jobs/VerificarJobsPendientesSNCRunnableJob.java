package co.gov.igac.snc.web.jobs;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Este Job realiza las siguientes operaciones: Consulta los jobs que terminaron de ejecutarse en
 * Arcgis Server
 *
 * Envía los jobs encontrados para que sean ejecutadas las siguientes acciones según corresponda.
 * Para tal fin se utiliza una tarea VerificarJobTask. Dicha tarea se ejecuta en su propio hilo
 * buscando mayor paralelismo. ( Los threads son controlados por Spring )
 *
 *
 * @author juan.mendez
 *
 */
public class VerificarJobsPendientesSNCRunnableJob extends AbstractLocator implements Runnable,
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4000626185063477986L;
    private static final Logger LOGGER = Logger.getLogger(
        VerificarJobsPendientesSNCRunnableJob.class);

    private TaskExecutor taskExecutor;
    private Long cantidadJobs;

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void setCantidadJobs(Long cantidadJobs) {
        this.cantidadJobs = cantidadJobs;
    }

//--------------------------------------------------------------------------------------------------	
    @Override
    public void run() {
        try {
            // this.getGeneralesService().verificarEjecucionJobsPendientesEnSNC();
            List<ProductoCatastralJob> jobsPendientes = this.getGeneralesService().
                obtenerJobsPendientesEnSNC(
                    this.cantidadJobs);
            if (jobsPendientes != null && jobsPendientes.size() > 0) {
                for (ProductoCatastralJob job : jobsPendientes) {
                    //envía los jobs a la cola (Dicha cola tiene un pool de instancias
                    //que se ejecutan en paralelo )
                    this.taskExecutor.execute(new VerificarJobTask(job));
                }
            } else {
                LOGGER.warn("No se obtuvieron jobs pendientes para ejecución");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
