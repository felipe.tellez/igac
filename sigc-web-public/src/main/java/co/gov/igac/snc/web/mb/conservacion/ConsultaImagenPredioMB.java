/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Foto;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastralDetalle;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.Date;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;

/**
 * Clase que hace las veces de managed bean para la consulta de fotos de un predio
 *
 * @author pedro.garcia
 */
@Component("consultaImagenPredio")
@Scope("session")
public class ConsultaImagenPredioMB extends SNCManagedBean
    implements Serializable {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaImagenPredioMB.class);

    /**
     * predio del que se ven los detalles de trámites
     */
    private Predio predioImagen;

    private UsuarioDTO usuario;

    private Documento documentoFoto;
    private Documento documentoFotoDetalle;
    ReporteDTO reporteDTOFoto;
    ReporteDTO reporteDTOFotoDetalle;

    private Boolean banderaJobEjecutandose;

    private ReportesUtil reportesUtil = ReportesUtil.getInstance();

    //private String imagenBase;
//--------------    methods    ---
    public Predio getPredioImagen() {
        return predioImagen;
    }

    public void setPredioImagen(Predio predioImagen) {
        this.predioImagen = predioImagen;
    }

    public Documento getDocumentoFoto() {
        return documentoFoto;
    }

    public void setDocumentoFoto(Documento documentoFoto) {
        this.documentoFoto = documentoFoto;
    }

    public Documento getDocumentoFotoDetalle() {
        return documentoFotoDetalle;
    }

    public void setDocumentoFotoDetalle(Documento documentoFotoDetalle) {
        this.documentoFotoDetalle = documentoFotoDetalle;
    }

    public Boolean getBanderaJobEjecutandose() {
        return banderaJobEjecutandose;
    }

    public void setBanderaJobEjecutandose(Boolean banderaJobEjecutandose) {
        this.banderaJobEjecutandose = banderaJobEjecutandose;
    }

    public ReporteDTO getReporteDTOFoto() {
        return reporteDTOFoto;
    }

    public void setReporteDTOFoto(ReporteDTO reporteDTOFoto) {
        this.reporteDTOFoto = reporteDTOFoto;
    }

    public ReporteDTO getReporteDTOFotoDetalle() {
        return reporteDTOFotoDetalle;
    }

    public void setReporteDTOFotoDetalle(ReporteDTO reporteDTOFotoDetalle) {
        this.reporteDTOFotoDetalle = reporteDTOFotoDetalle;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        try {

            LOGGER.debug("ConsultaImagenPredioMB init");
            this.banderaJobEjecutandose = false;
            ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean(
                "consultaPredio");
            Long predioSeleccionadoId = mb001.getSelectedPredioId();

            this.usuario = MenuMB.getMenu().getUsuarioDto();

            setPredioImagen(this.getConservacionService().obtenerPredioPorId(predioSeleccionadoId));
            Documento doc = new Documento();
            List<String> listaEstados = new ArrayList<String>();
            listaEstados.add(ProductoCatastralJob.AGS_ESPERANDO);
            //listaEstados.add(ProductoCatastralJob.AGS_TERMINADO);

            try {
                doc = this.getConservacionService().buscarDocumentoPorNumeroPredialAndTipoDocumento(
                    getPredioImagen().getNumeroPredial(), Constantes.TIPO_IMG_PREDIAL);
            } catch (Exception e) {
                e.printStackTrace();
                this.banderaJobEjecutandose = false;
            }
            if (doc != null && doc.getEstado().equals(Constantes.DOCUMENTACION_ESTADO_INCOMPLETO)) {
                this.banderaJobEjecutandose = true;
            }

            this.documentoFoto = this.getConservacionService().
                buscarDocumentoPorPredioIdAndTipoDocumento(predioSeleccionadoId,
                    Constantes.TIPO_IMG_PREDIAL);

            if (documentoFoto != null) {
                reporteDTOFoto = this.reportesUtil.consultarReporteDeGestorDocumental(documentoFoto.
                    getArchivo());
            }

            this.documentoFotoDetalle = this.getConservacionService().
                buscarDocumentoPorPredioIdAndTipoDocumento(predioSeleccionadoId,
                    Constantes.TIPO_IMG_PREDIAL_DETALLE);

            if (documentoFoto != null) {
                reporteDTOFotoDetalle = this.reportesUtil.consultarReporteDeGestorDocumental(
                    documentoFotoDetalle.getArchivo());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void crearJobImagenPredial() {

        try {
            ProductoCatastralDetalle pcdTemp = new ProductoCatastralDetalle();
            pcdTemp.setNumeroPredial(getPredioImagen().getNumeroPredial());
            Documento doc = this.getConservacionService().
                buscarDocumentoPorNumeroPredialAndTipoDocumento(
                    getPredioImagen().getNumeroPredial(), Constantes.TIPO_IMG_PREDIAL);
            if (doc == null) {
                doc = new Documento();
                Predio predio = this.getConservacionService().getPredioByNumeroPredial(
                    getPredioImagen().getNumeroPredial());
                
                doc.setEstado(Constantes.DOCUMENTACION_ESTADO_INCOMPLETO);
                doc.setBloqueado(ESiNo.NO.getCodigo());
                doc.setUsuarioCreador(usuario.getLogin());
                doc.setPredioId(predio.getId());
                doc.setNumeroPredial(predio.getNumeroPredial());
                //doc.setTramiteId(this.tramite.getId());
                doc.setUsuarioCreador(usuario.getLogin());
                doc.setFechaRadicacion(new Date(System.currentTimeMillis()));
                doc.setUsuarioLog(usuario.getLogin());
                doc.setFechaLog(new Date(System.currentTimeMillis()));
                TipoDocumento td = new TipoDocumento();
                td.setId(Constantes.TIPO_IMG_PREDIAL);
                doc.setTipoDocumento(td);
                doc.setArchivo("EnProceso");
            } else {
                doc.setEstado(Constantes.DOCUMENTACION_ESTADO_INCOMPLETO);
            }
            this.getGeneralesService().guardarDocumento(doc);
            
            this.banderaJobEjecutandose = true;
            this.getGeneralesService().generarImagenPredial(pcdTemp, this.usuario);
            init();
        } catch (Exception e) {
            init();
        }
    }

}
