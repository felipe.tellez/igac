package co.gov.igac.snc.web.mb.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.avaluos.administracion.AdminContratosInteradministrativosMB;
import co.gov.igac.snc.web.mb.avaluos.administracion.ConsultaContratosInteradministrativosMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para pantallas resumenSolicitante.xhtml y capturaDatosSolicitante.xhtml, cuya finalidad es
 * administrar la informacion de un solicitante </br></br>
 * <b>OJO: </b>Se debe tener en cuenta que el método
 * <i><b>cargarListaSolicitantes</b></i> es el que carga la lista de solicitantes y varía según el
 * MB que lo llama. padre.</br></br> <b>Por ejemplo:</b> {@link #cargarListaSolicitantesDesdeCU121}
 * es llamado desde el método <b>cargarVariablesExternas</b> en clase
 * <b>DiligenciamientoInformacionSolicitanteMB</b>
 *
 * @author rodrigo.hernandez
 *
 */
@Component("adminInfoSolicitante")
@Scope("session")
public class AdministracionInformacionSolicitanteMB extends SNCManagedBean {

    private static final long serialVersionUID = 3048407539879814179L;

    /**
     * ManagedBean del caso de uso 53
     */
    private ConsultaContratosInteradministrativosMB consultaContratosMB;

    /**
     * Lista de solicitantes para la tabla de la pagina
     * <b>resumenSolicitante.xhtml</b>
     */
    private List<Solicitante> listaSolicitantes;

    /**
     * Solicitante seleccionado en la tabla de la pagina
     * <b>resumenSolicitante.xhtml</b>
     */
    private Solicitante solicitanteSeleccionado;

    /**
     * Dato con informacion asociada a la lista de solicitantes que se recibe desde el MB que llama
     * a este
     */
    private Object extraData;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionInformacionSolicitanteMB.class);

    /**
     * Variable con codigo del icono que se va a usar segun el modo de datos
     */
    private String icon;

    /**
     * Variable con valor para el title del icono que se va a usar segun el modo de datos
     */
    private String iconText;

    /**
     * Datos geográficos del aviso
     */
    private Pais paisSolicitante;
    private Departamento departamentoSolicitante;
    private Municipio municipioSolicitante;

    /**
     * Listas de select Items avisos
     */
    private List<SelectItem> paisesSolicitante;
    private List<SelectItem> municipiosSolicitante;
    private List<SelectItem> departamentosSolicitante;

    /**
     * Listas geográficas de Avisos
     */
    private List<Pais> paissSolicitante;
    private List<Departamento> dptosSolicitante;
    private List<Municipio> munisSolicitante;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;
    private EOrden ordenDepartamentosTramite = EOrden.CODIGO;
    private EOrden ordenMunicipiosTramite = EOrden.CODIGO;

    /**
     * Orden para la listas de solicitante
     */
    private EOrden ordenDepartamentosSolicitante = EOrden.CODIGO;
    private EOrden ordenMunicipiosSolicitante = EOrden.CODIGO;
    private EOrden ordenPaisesSolicitante = EOrden.CODIGO;

    /**
     * Valores para las relaciones del solicitante.
     */
    private List<SelectItem> relacionesSolicitante;

    /**
     * Bandera que determina la visualización de los datos del solicitante solo si este se ha
     * buscado primero en el sistema
     */
    private boolean banderaBusquedaSolicitanteRealizada;

    /**
     * Bandera que determina la visualizacion del campo <b>Relacion</b> segun el CU que llame a este
     * MB
     */
    private UsuarioDTO usuario;

    /**
     * Solicitante seleccionado de la solicitud
     */
    private SolicitanteSolicitud solicitanteSolicitudSeleccionado;

    /**
     * Lista de items para cargar Tipos de Documento y Tipos de Solicitante
     */
    private List<SelectItem> tiposDocumentoSolicitante;
    private List<SelectItem> tiposSolicitante;

    @Autowired
    private GeneralMB generalMB;

    /**
     * Variable para indicar si el boton de adicionar solicitante se debe habilitar o no
     */
    private boolean banderaHabilitarBotonAdicionSolicitante;

    /**
     * Variable para indicar si se permite guardar el solicitante o no
     */
    private boolean banderaHabilitarGuardarSolicitante;

    /**
     * Variable que indica si el almacenamiento de la información de la solicitud se ha hecho de
     * forma definitiva o temporal
     */
    private boolean banderaModoLecturaHabilitado;

    /**
     * Variable que indica si se muestran o no todas las columnas
     */
    private boolean banderaTablaExpandida;

    /**
     * Variable que indica si un dato está restringido para edición
     */
    private boolean banderaEsDatoRestringido;

    /*
     * Variables para manejar mensajes en ventanas de confirmacion usadas en paginas que llaman a
     * resumenSolicitante.xhtml
     */
    /**
     * Variable que almacena el mensaje del dialog para confirmar adicion de un solicitante o
     * entidad
     */
    private String mensajeConfirmacionAdicion;

    /**
     * Variable que almacena el mensaje del dialog para confirmar eliminación de un solicitante o
     * entidad
     */
    private String mensajeConfirmacionEliminacion;

    /**
     * Variable que almacena el texto del boton para adicionar Entidad/Solicitante
     */
    private String textoBotonAdicionar;

    /**
     * Variable que indica que acción ejecutar cuando se llama el atributo oncomplete del boton
     * <b>Guardar y Adjuntar</b> en la pantalla
     * <b>capturaDatosSolicitante.xhtml</b>
     */
    private String funcionPantallaCapturaDatosCaller;

    /*
     * Variables relacionadas con el caso de uso CU-SA-AC-121 - Diligenciar informacion del
     * solicitante
     */
    /**
     * Bandera para indicar si este MB es llamado desde DiligenciamientoInformacionSolicitanteMB
     */
    private boolean banderaMBEsLlamadoDesdeCU121;

    /*
     * Variables relacionadas con el caso de uso CU-SA-AC-54 - Consultar informacion de entidades
     */
    /**
     * Bandera para indicar si este MB es llamado desde ConsultaInformacionEntidadesMB
     */
    private boolean banderaMBEsLlamadoDesdeCU54;

    /**
     * Campo que almacena el cargo para el SolicitanteSolicitud
     */
    private String cargoSolicitante;

    /*
     * Variables relacionadas con el caso de uso CU-SA-AC-008 - Administrar Contratos
     * Interadministrativos
     */
    private AdminContratosInteradministrativosMB adminContratosInteradministrativosMB;

    // --------------- Setters y Getters ----------------------------------
    public List<Solicitante> getListaSolicitantes() {
        return this.listaSolicitantes;
    }

    public void setListaSolicitantes(List<Solicitante> listaSolicitantes) {
        this.listaSolicitantes = listaSolicitantes;
    }

    public Solicitante getSolicitanteSeleccionado() {
        return solicitanteSeleccionado;
    }

    public void setSolicitanteSeleccionado(Solicitante solicitanteSeleccionado) {
        this.solicitanteSeleccionado = solicitanteSeleccionado;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isBanderaModoLecturaHabilitado() {
        return banderaModoLecturaHabilitado;
    }

    public void setBanderaModoLecturaHabilitado(
        boolean banderaModoLecturaHabilitado) {
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
    }

    public Pais getPaisSolicitante() {
        return paisSolicitante;
    }

    public void setPaisSolicitante(Pais paisSolicitante) {
        this.paisSolicitante = paisSolicitante;
    }

    public Departamento getDepartamentoSolicitante() {
        return departamentoSolicitante;
    }

    public void setDepartamentoSolicitante(Departamento departamentoSolicitante) {
        this.departamentoSolicitante = departamentoSolicitante;
    }

    public Municipio getMunicipioSolicitante() {
        return municipioSolicitante;
    }

    public void setMunicipioSolicitante(Municipio municipioSolicitante) {
        this.municipioSolicitante = municipioSolicitante;
    }

    public List<SelectItem> getPaisesSolicitante() {
        return paisesSolicitante;
    }

    public void setPaisesSolicitante(List<SelectItem> paisesSolicitante) {
        this.paisesSolicitante = paisesSolicitante;
    }

    public void setMunicipiosSolicitante(List<SelectItem> municipiosSolicitante) {
        this.municipiosSolicitante = municipiosSolicitante;
    }

    public void setDepartamentosSolicitante(
        List<SelectItem> departamentosSolicitante) {
        this.departamentosSolicitante = departamentosSolicitante;
    }

    public List<Pais> getPaissSolicitante() {
        return paissSolicitante;
    }

    public void setPaissSolicitante(List<Pais> paissSolicitante) {
        this.paissSolicitante = paissSolicitante;
    }

    public List<Departamento> getDptosSolicitante() {
        return dptosSolicitante;
    }

    public void setDptosSolicitante(List<Departamento> dptosSolicitante) {
        this.dptosSolicitante = dptosSolicitante;
    }

    public List<Municipio> getMunisSolicitante() {
        return munisSolicitante;
    }

    public void setMunisSolicitante(List<Municipio> munisSolicitante) {
        this.munisSolicitante = munisSolicitante;
    }

    public EOrden getOrdenDepartamentos() {
        return ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public EOrden getOrdenDepartamentosTramite() {
        return ordenDepartamentosTramite;
    }

    public void setOrdenDepartamentosTramite(EOrden ordenDepartamentosTramite) {
        this.ordenDepartamentosTramite = ordenDepartamentosTramite;
    }

    public EOrden getOrdenMunicipiosTramite() {
        return ordenMunicipiosTramite;
    }

    public void setOrdenMunicipiosTramite(EOrden ordenMunicipiosTramite) {
        this.ordenMunicipiosTramite = ordenMunicipiosTramite;
    }

    public EOrden getOrdenDepartamentosSolicitante() {
        return ordenDepartamentosSolicitante;
    }

    public void setOrdenDepartamentosSolicitante(
        EOrden ordenDepartamentosSolicitante) {
        this.ordenDepartamentosSolicitante = ordenDepartamentosSolicitante;
    }

    public EOrden getOrdenMunicipiosSolicitante() {
        return ordenMunicipiosSolicitante;
    }

    public void setOrdenMunicipiosSolicitante(EOrden ordenMunicipiosSolicitante) {
        this.ordenMunicipiosSolicitante = ordenMunicipiosSolicitante;
    }

    public EOrden getOrdenPaisesSolicitante() {
        return ordenPaisesSolicitante;
    }

    public void setOrdenPaisesSolicitante(EOrden ordenPaisesSolicitante) {
        this.ordenPaisesSolicitante = ordenPaisesSolicitante;
    }

    public List<SelectItem> getRelacionesSolicitante() {
        return relacionesSolicitante;
    }

    public void setRelacionesSolicitante(List<SelectItem> relacionesSolicitante) {
        this.relacionesSolicitante = relacionesSolicitante;
    }

    public boolean isBanderaBusquedaSolicitanteRealizada() {
        return banderaBusquedaSolicitanteRealizada;
    }

    public void setBanderaBusquedaSolicitanteRealizada(
        boolean banderaBusquedaSolicitanteRealizada) {
        this.banderaBusquedaSolicitanteRealizada = banderaBusquedaSolicitanteRealizada;
    }

    public SolicitanteSolicitud getSolicitanteSolicitudSeleccionado() {
        return solicitanteSolicitudSeleccionado;
    }

    public void setSolicitanteSolicitudSeleccionado(
        SolicitanteSolicitud solicitanteSolicitudSeleccionado) {
        this.solicitanteSolicitudSeleccionado = solicitanteSolicitudSeleccionado;
    }

    public List<SelectItem> getTiposDocumentoSolicitante() {
        return tiposDocumentoSolicitante;
    }

    public void setTiposDocumentoSolicitante(
        List<SelectItem> tiposDocumentoSolicitante) {
        this.tiposDocumentoSolicitante = tiposDocumentoSolicitante;
    }

    public List<SelectItem> getTiposSolicitante() {
        return tiposSolicitante;
    }

    public void setTiposSolicitante(List<SelectItem> tiposSolicitante) {
        this.tiposSolicitante = tiposSolicitante;
    }

    public boolean isBanderaHabilitarBotonAdicionSolicitante() {
        return banderaHabilitarBotonAdicionSolicitante;
    }

    public void setBanderaHabilitarBotonAdicionSolicitante(
        boolean banderaHabilitarBotonAdicionSolicitante) {
        this.banderaHabilitarBotonAdicionSolicitante = banderaHabilitarBotonAdicionSolicitante;
    }

    public boolean isBanderaHabilitarGuardarSolicitante() {
        return banderaHabilitarGuardarSolicitante;
    }

    public void setBanderaHabilitarGuardarSolicitante(
        boolean banderaHabilitarGuardarSolicitante) {
        this.banderaHabilitarGuardarSolicitante = banderaHabilitarGuardarSolicitante;
    }

    public void setSelectedPaisSolicitante(String codigo) {
        Pais paisSeleccionado = null;
        if (codigo != null) {
            for (Pais pais : this.paissSolicitante) {
                if (codigo.equals(pais.getCodigo())) {
                    paisSeleccionado = pais;
                    break;
                }
            }
        }
        if (this.solicitanteSeleccionado != null) {
            this.paisSolicitante = paisSeleccionado;
        }
    }

    public String getSelectedPaisSolicitante() {
        if (this.solicitanteSeleccionado != null &&
             this.paisSolicitante != null) {
            return this.paisSolicitante.getCodigo();
        }
        return null;
    }

    public void setSelectedDepartamentoSolicitante(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosSolicitante != null) {
            for (Departamento departamento : dptosSolicitante) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.solicitanteSeleccionado != null) {
            this.departamentoSolicitante = dptoSeleccionado;
        }
    }

    public String getSelectedDepartamentoSolicitante() {
        if (this.solicitanteSeleccionado != null &&
             this.departamentoSolicitante != null) {
            return this.departamentoSolicitante.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getDepartamentosSolicitante() {
        return departamentosSolicitante;
    }

    public void setSelectedMunicipioSolicitante(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null && this.munisSolicitante != null) {
            for (Municipio municipio : this.munisSolicitante) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioSolicitante = null;
        }
        if (this.solicitanteSeleccionado != null) {
            this.municipioSolicitante = municipioSelected;
        }
    }

    public String getSelectedMunicipioSolicitante() {
        if (this.solicitanteSeleccionado != null &&
             this.municipioSolicitante != null) {
            return this.municipioSolicitante.getCodigo();
        }
        return null;
    }

    public List<SelectItem> getMunicipiosSolicitante() {
        return this.municipiosSolicitante;
    }

    public Object getExtraData() {
        return extraData;
    }

    public void setExtraData(Object extraData) {
        this.extraData = extraData;
    }

    public boolean isBanderaEsLlamadoDesdeCU121() {
        return banderaMBEsLlamadoDesdeCU121;
    }

    public void setBanderaEsLlamadoDesdeCU121(boolean banderaEsLlamadoDesdeCU121) {
        this.banderaMBEsLlamadoDesdeCU121 = banderaEsLlamadoDesdeCU121;
    }

    public boolean isBanderaMBEsLlamadoDesdeCU54() {
        return banderaMBEsLlamadoDesdeCU54;
    }

    public void setBanderaMBEsLlamadoDesdeCU54(
        boolean banderaMBEsLlamadoDesdeCU54) {
        this.banderaMBEsLlamadoDesdeCU54 = banderaMBEsLlamadoDesdeCU54;
    }

    public boolean isBanderaEsDatoRestringido() {
        return banderaEsDatoRestringido;
    }

    public void setBanderaEsDatoRestringido(boolean banderaEsDatoRestringido) {
        this.banderaEsDatoRestringido = banderaEsDatoRestringido;
    }

    public String getMensajeConfirmacionAdicion() {
        return mensajeConfirmacionAdicion;
    }

    public void setMensajeConfirmacionAdicion(String mensajeConfirmacionAdicion) {
        this.mensajeConfirmacionAdicion = mensajeConfirmacionAdicion;
    }

    public String getMensajeConfirmacionEliminacion() {
        return mensajeConfirmacionEliminacion;
    }

    public void setMensajeConfirmacionEliminacion(
        String mensajeConfirmacionEliminacion) {
        this.mensajeConfirmacionEliminacion = mensajeConfirmacionEliminacion;
    }

    public String getTextoBotonAdicionar() {
        return textoBotonAdicionar;
    }

    public void setTextoBotonAdicionar(String textoBotonAdicionar) {
        this.textoBotonAdicionar = textoBotonAdicionar;
    }

    public String getFuncionPantallaCapturaDatosCaller() {
        return funcionPantallaCapturaDatosCaller;
    }

    public void setFuncionPantallaCapturaDatosCaller(
        String funcionPantallaCapturaDatosCaller) {
        this.funcionPantallaCapturaDatosCaller = funcionPantallaCapturaDatosCaller;
    }

    public boolean isBanderaTablaExpandida() {
        return this.banderaTablaExpandida;
    }

    public void setBanderaTablaExpandida(boolean banderaTablaExpandida) {
        this.banderaTablaExpandida = banderaTablaExpandida;
    }

    public String getCargoSolicitante() {
        return cargoSolicitante;
    }

    public void setCargoSolicitante(String cargoSolicitante) {
        this.cargoSolicitante = cargoSolicitante;
    }

    public String getIconText() {
        return iconText;
    }

    public void setIconText(String iconText) {
        this.iconText = iconText;
    }

    // ---------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" + "#init");

        this.iniciarDatos();

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" + "#init");
    }

    // ---------------------------------------------------------------------
    /**
     * Método para iniciar los datos del MB
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.icon = "ui-icon-pencil";
        this.listaSolicitantes = new ArrayList<Solicitante>();

        this.iniciarBanderasLlamadosMB();

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#iniciarDatos");
    }

    // ---------------------------------------------------------------------
    /**
     * Método para iniciar las listas de tipos de documentos y tipos de solicitante
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListaItemsTiposDocumentoSolicitante() {
        this.tiposDocumentoSolicitante = new ArrayList<SelectItem>();
        this.tiposSolicitante = new ArrayList<SelectItem>();

        this.iniciarListaItemsTiposDocumento();
        this.iniciarListaItemsTiposSolicitante();
    }

    // ---------------------------------------------------------------------
    /**
     * Método para setear las banderas que indican si este MB es llamado desde otro MB
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderasLlamadosMB() {
        this.banderaMBEsLlamadoDesdeCU121 = false;
        this.banderaMBEsLlamadoDesdeCU54 = false;
    }

    // ----------------------------------------------------------------------
    /**
     * Método para asignar valor a icono que abre pantalla con datos del solicitante seleccionado
     *
     * @author rodrigo.hernandez
     */
    private void setIconVerEditarInfoSolicitante() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#setIconVerEditarInfoSolicitante");

        if (!banderaModoLecturaHabilitado) {
            this.icon = "ui-icon-pencil";
            this.iconText = "Editar";
        } else {
            this.icon = "ui-icon-search";
            this.iconText = "Consultar";
        }

        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#setIconVerEditarInfoSolicitante");
    }

    /**
     * Método que carga el SelectOneMenu de Tipo de Documento segun valor escogido en SelectOneMenu
     * Tipo Empresa
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListaItemsTiposDocumento() {
        LOGGER.debug(
            "Iniciando AdministracionInformacionSolicitanteMB#iniciarListaItemsTiposDocumento");

        List<SelectItem> lista = new ArrayList<SelectItem>();

        lista = this.generalMB.getPersonaTipoIdentificacionV();

        for (SelectItem si : lista) {
            // Si es persona juridica solo carga el tipo de documento NIT
            if (this.solicitanteSeleccionado.getTipoPersona().equals(
                EPersonaTipoPersona.JURIDICA.getCodigo())) {
                if (si.getValue().equals(
                    EPersonaTipoIdentificacion.NIT.getCodigo())) {
                    this.tiposDocumentoSolicitante.add(si);
                }
            } else { // Si es persona natural carga tipos de documento C.C y
                // C.E
                if (si.getValue().equals(
                    EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                        .getCodigo()) ||
                     si.getValue().equals(
                        EPersonaTipoIdentificacion.CEDULA_EXTRANJERIA
                            .getCodigo())) {
                    this.tiposDocumentoSolicitante.add(si);
                }

            }

        }
        LOGGER.debug(
            "Finalizando AdministracionInformacionSolicitanteMB#iniciarListaItemsTiposDocumento");
    }

    /**
     * Método que carga el SelectOneMenu de Tipo de Solicitante segun valor escogido en
     * SelectOneMenu Tipo Empresa
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListaItemsTiposSolicitante() {
        LOGGER.debug(
            "Iniciando AdministracionInformacionSolicitanteMB#iniciarListaItemsTiposSolicitante");

        List<SelectItem> lista = new ArrayList<SelectItem>();

        lista = this.generalMB.getSolicitanteTipoSolicitanteV();

        for (SelectItem si : lista) {
            // Valida si es persona Natural
            if (this.solicitanteSeleccionado.getTipoPersona().equals(
                EPersonaTipoPersona.NATURAL.getCodigo())) {
                if (si.getValue().equals(
                    ESolicitanteTipoSolicitant.PRIVADA.getCodigo())) {
                    this.tiposSolicitante.add(si);
                }
            } else { // Si es persona Juridica
                if (si.getValue().equals(
                    ESolicitanteTipoSolicitant.PRIVADA.getCodigo()) ||
                     si.getValue().equals(
                        ESolicitanteTipoSolicitant.PUBLICA.getCodigo()) ||
                     si.getValue().equals(
                        ESolicitanteTipoSolicitant.MIXTA.getCodigo()) ||
                     si.getValue()
                        .equals(ESolicitanteTipoSolicitant.FISCALIA
                            .getCodigo()) ||
                     si.getValue().equals(
                        ESolicitanteTipoSolicitant.PROCURADURIA
                            .getCodigo()) ||
                     si.getValue().equals(
                        ESolicitanteTipoSolicitant.JUZGADO.getCodigo())) {
                    this.tiposSolicitante.add(si);
                }
            }
        }

        LOGGER.debug(
            "Finalizando AdministracionInformacionSolicitanteMB#iniciarListaItemsTiposSolicitante");
    }

    // -------------------------------------------------------------------------------------
    /**
     * Método para crear un Solicitante por default.
     *
     * @author rodrigo.hernandez
     *
     * @param tipoPersona
     */
    private void iniciarDatosSolicitante(String tipoPersona) {

        this.solicitanteSeleccionado = new Solicitante();

        this.updatePaisesSolicitante();
        this.setSelectedPaisSolicitante(Constantes.COLOMBIA);
        this.solicitanteSeleccionado.setDireccionPais(this.paisSolicitante);

        this.updateDepartamentosSolicitante();
        this.updateMunicipiosSolicitante();

        this.solicitanteSeleccionado.setTipoPersona(tipoPersona);

        this.solicitanteSeleccionado.setTipoIdentificacion(tipoPersona);

        this.departamentoSolicitante = null;
        this.municipioSolicitante = null;

        this.solicitanteSeleccionado
            .setDireccionDepartamento(new Departamento());
        this.solicitanteSeleccionado.setDireccionMunicipio(new Municipio());
        this.solicitanteSeleccionado.setFechaLog(new Date());
        this.solicitanteSeleccionado.setUsuarioLog(this.usuario.getLogin());

        this.banderaBusquedaSolicitanteRealizada = false;
        this.solicitanteSeleccionado
            .setTipoSolicitante(ESolicitanteTipoSolicitant.PRIVADA
                .getCodigo());

        this.solicitanteSeleccionado
            .setRelacion(ESolicitanteSolicitudRelac.PROPIETARIO.toString());

        this.iniciarListaItemsTiposDocumentoSolicitante();
        this.generarRelacionesSolicitante();
    }

    /**
     * Método para reiniciar valores de la pantalla
     * <b>capturaDatosSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    public void limpiarFormulario() {
        String tipoPersona = this.solicitanteSeleccionado.getTipoPersona();

        this.banderaEsDatoRestringido = false;
        this.iniciarDatosSolicitante(tipoPersona);
    }

    /**
     * Prepara el solicitante para la inserción de un nuevo objeto Solicitante
     *
     * @author rodrigo.hernandez
     */
    public void configureNewSolicitante() {
        String tipoPersona = EPersonaTipoPersona.NATURAL.getCodigo();

        this.banderaEsDatoRestringido = false;
        this.iniciarDatosSolicitante(tipoPersona);

    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método para actualizar selectOneMenu del campo <b>país</b> en pagina
     * <b>capturaDatosSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    public void updatePaisesSolicitante() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB#updatePaisesSolicitante");

        this.paisesSolicitante = new ArrayList<SelectItem>();
        this.paisesSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.paissSolicitante = this.getGeneralesService().getCachePaises();
        if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
            Collections.sort(paissSolicitante);
        } else {
            Collections.sort(paissSolicitante, Pais.getComparatorNombre());
        }
        for (Pais pais : this.paissSolicitante) {
            if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
                this.paisesSolicitante.add(new SelectItem(pais.getCodigo(),
                    pais.getCodigo() + "-" + pais.getNombre()));
            } else {
                this.paisesSolicitante.add(new SelectItem(pais.getCodigo(),
                    pais.getNombre()));
            }
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB#updatePaisesSolicitante");

    }

    // -------------------------------------------------------------------------------------------------------------
    /**
     * Método para actualizar selectOneMenu del campo <b>departamento</b> en pagina
     * <b>capturaDatosSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    public void updateDepartamentosSolicitante() {
        departamentosSolicitante = new ArrayList<SelectItem>();
        departamentosSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));

        // Si este MB es llamado desde ConsultaInformacionEntidadesMB, se cargan
        // los departamentos pertenecientes a la territorial del usuario en
        // sesión
        if (this.banderaMBEsLlamadoDesdeCU54) {

            String codigoTerritorial = this.usuario.getCodigoTerritorial();

            this.dptosSolicitante = (ArrayList<Departamento>) this
                .getGeneralesService().getCacheDepartamentosPorTerritorial(
                    codigoTerritorial);

        } else {
            this.dptosSolicitante = this.getGeneralesService()
                .getCacheDepartamentosPorPais(
                    this.paisSolicitante.getCodigo());
        }

        if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosSolicitante);
        } else {
            Collections.sort(this.dptosSolicitante,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.dptosSolicitante) {
            if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
                departamentosSolicitante.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                departamentosSolicitante.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------
    /**
     * Método para actualizar selectOneMenu del campo <b>municipio</b> en pagina
     * <b>capturaDatosSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    public void updateMunicipiosSolicitante() {
        municipiosSolicitante = new ArrayList<SelectItem>();
        munisSolicitante = new ArrayList<Municipio>();
        municipiosSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.solicitanteSeleccionado != null &&
             this.departamentoSolicitante != null &&
             this.departamentoSolicitante.getCodigo() != null) {
            munisSolicitante = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoSolicitante.getCodigo());
            if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
                Collections.sort(munisSolicitante);
            } else {
                Collections.sort(munisSolicitante,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisSolicitante) {
                if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
                    municipiosSolicitante.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    municipiosSolicitante.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------------------
    /**
     * Método para cargar valores de selectOneMenu del campo <b>relación</b> en pagina
     * <b>capturaDatosSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    public void generarRelacionesSolicitante() {
        LOGGER.
            debug("Iniciando AdministracionInformacionSolicitanteMB#generarRelacionesSolicitante");

        List<SelectItem> values = generalMB.getSolicitanteSolicitudRelacV();
        this.relacionesSolicitante = new ArrayList<SelectItem>();

        for (SelectItem si : values) {
            if (si.getValue().equals(
                ESolicitanteSolicitudRelac.PROPIETARIO.toString()) ||
                 si.getValue().equals(
                    ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                        .getValor().toUpperCase())) {
                this.relacionesSolicitante.add(si);
            }

        }

        LOGGER.debug(
            "Finalizando AdministracionInformacionSolicitanteMB#generarRelacionesSolicitante");
    }

    // --------------------------------------------------------------------------------------------------------
    /**
     * Prepara al solicitante de la solicitud para edición
     *
     * @author rodrigo.hernandez
     */
    public void setupSolicitanteEdicion() {
        this.iniciarListaItemsTiposDocumentoSolicitante();
        this.generarRelacionesSolicitante();
        this.banderaEsDatoRestringido = true;

        this.updatePaisesSolicitante();
        this.paisSolicitante = this.solicitanteSeleccionado.getDireccionPais();

        this.updateDepartamentosSolicitante();
        this.departamentoSolicitante = this.solicitanteSeleccionado
            .getDireccionDepartamento();

        this.updateMunicipiosSolicitante();
        this.municipioSolicitante = this.solicitanteSeleccionado
            .getDireccionMunicipio();

        this.banderaBusquedaSolicitanteRealizada = true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de cargar la entidad seleccionada en el caso de uso 54 para consultar los
     * contratos asociados documento
     *
     * @author christian.rodriguez
     */
    public void setupConsultaContratos() {
        this.consultaContratosMB = (ConsultaContratosInteradministrativosMB) UtilidadesWeb
            .getManagedBean("consultaContratosInteradministrativos");
        this.consultaContratosMB.cargarDesdeCU54(this.solicitanteSeleccionado);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de buscar un solicitante a partir de los datos de documento
     *
     * @author rodrigo.hernandez
     */
    public void buscarSolicitante() {
        LOGGER.debug("Entrando a buscar solicitante");

        Solicitante sol = this.getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
                solicitanteSeleccionado.getNumeroIdentificacion(),
                solicitanteSeleccionado.getTipoIdentificacion());
        if (sol != null) {
            this.paisSolicitante = sol.getDireccionPais();
            this.updateDepartamentosSolicitante();
            this.departamentoSolicitante = sol.getDireccionDepartamento();
            this.updateMunicipiosSolicitante();
            this.municipioSolicitante = sol.getDireccionMunicipio();
            this.solicitanteSeleccionado = sol;
            sol.setRelacion(ESolicitanteSolicitudRelac.PROPIETARIO.toString());

        } else {
            LOGGER.debug("buscarSolicitante Return null");
            String tipoIdentif = this.solicitanteSeleccionado
                .getTipoIdentificacion();
            String numDoc = this.solicitanteSeleccionado
                .getNumeroIdentificacion();
            String tipoPersona = this.solicitanteSeleccionado.getTipoPersona();

            setDefaultDataSolicitante();

            this.solicitanteSeleccionado.setTipoIdentificacion(tipoIdentif);
            this.solicitanteSeleccionado.setNumeroIdentificacion(numDoc);
            this.solicitanteSeleccionado.setTipoPersona(tipoPersona);
            this.banderaBusquedaSolicitanteRealizada = true;
        }

    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que quema los datos de fechas departamento tipo de identificación y demás requeridos
     * para iniciar los solicitantes requeridos.
     *
     * @author rodrigo.hernandez
     */
    private void setDefaultDataSolicitante() {
        Solicitante sol = new Solicitante();

        sol.setTipoIdentificacion("CC");

        this.setSelectedPaisSolicitante(Constantes.COLOMBIA);
        this.departamentoSolicitante = new Departamento();
        this.municipioSolicitante = new Municipio();

        sol.setDireccionPais(this.paisSolicitante);
        sol.setDireccionDepartamento(new Departamento());
        sol.setDireccionMunicipio(new Municipio());
        sol.setFechaLog(new Date());
        sol.setUsuarioLog(this.usuario.getLogin());
        sol.setRelacion(ESolicitanteSolicitudRelac.PROPIETARIO.toString());

        this.solicitanteSeleccionado = sol;
    }

    // --------------------------------------------------------------------------
    /**
     * Método para reiniciar los valores del selectOneMenu del departamento segun el país
     * seleccionado en la pagina
     * <b>capturaDatosSolicitante.xhtml</b> </br> Llamado desde el listener para el selectOneMenu
     * <b>paisSolicitante</b>
     *
     * @author rodrigo.hernandez
     */
    public void onChangePaisesSolicitante() {
        this.municipioSolicitante = null;
        this.departamentoSolicitante = null;
        this.updateDepartamentosSolicitante();
        this.updateMunicipiosSolicitante();
    }

    // ---------------------------------------------------------------------------------------------------
    /**
     * Método para cambiar el orden de los paises en la página
     * <b>capturaDatosSolicitante.xhtml</b> </br> Llamado desde el action del commandLink con id
     * <b>btnOrdenPaises</b>
     *
     * @author rodrigo.hernandez
     */
    public void cambiarOrdenPaisesSolicitante() {
        if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
            this.ordenPaisesSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenPaisesSolicitante = EOrden.CODIGO;
        }
        this.updatePaisesSolicitante();
    }

    // ---------------------------------------------------------------------------------------------------
    /**
     * Método para reiniciar los valores del selectOneMenu del municipio segun el departamento
     * seleccionado en la pagina
     * <b>capturaDatosSolicitante.xhtml</b> </br> Llamado desde el listener para el selectOneMenu
     * <b>departamentoSolicitante</b>
     *
     * @author rodrigo.hernandez
     */
    public void onChangeDepartamentosSolicitante() {
        this.municipioSolicitante = null;
        this.updateMunicipiosSolicitante();
    }

    // ---------------------------------------------------------------------------------------------------
    /**
     * Método para cambiar el orden de los departamentos en la página
     * <b>capturaDatosSolicitante.xhtml</b> </br> Llamado desde el action del commandLink con id
     * <b>btnOrdenDepartamentosSolicitante</b>
     *
     * @author rodrigo.hernandez
     */
    public void cambiarOrdenDepartamentosSolicitante() {
        if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosSolicitante = EOrden.CODIGO;
        }
        this.updateDepartamentosSolicitante();
    }

    // ---------------------------------------------------------------------------------------
    /**
     * Método para cambiar el orden de los municipios en la página
     * <b>capturaDatosSolicitante.xhtml</b> </br> Llamado desde el action del commandLink con id
     * <b>btnOrdenMunicipiosSolicitante</b>
     *
     * @author rodrigo.hernandez
     */
    public void cambiarOrdenMunicipiosSolicitante() {
        if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosSolicitante = EOrden.CODIGO;
        }
        this.updateMunicipiosSolicitante();
    }

    /**
     * Determina si los países están siendo ordenados por NOMBRE
     *
     * @return
     * @author rodrigo.hernandez
     */
    public boolean isOrdenPaisesNombreSolicitante() {
        return this.ordenPaisesSolicitante.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     *
     * @author rodrigo.hernandez
     */
    public boolean isOrdenDepartamentosNombreSolicitante() {
        return this.ordenDepartamentosSolicitante.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     *
     * @author rodrigo.hernandez
     */
    public boolean isOrdenMunicipiosNombreSolicitante() {
        return this.ordenMunicipiosSolicitante.equals(EOrden.NOMBRE);
    }

    // --------------------------------------------------------------------------------------------------------
    /**
     * Método encargado de guardar/actualizar el solicitante actual en la tabla solicitante. Además
     * de esto genera el objetos de tipo SolicitanteSolicitud correspondiente.
     *
     * @author rodrigo.hernandez
     */
    public void guardarSolicitante() {
        String relacion;
        String notificacionEmail;
        Pais auxPais;
        Departamento auxDepartamento;
        Municipio auxMunicipio;

        if (this.solicitanteValido()) {

            boolean yaAdicionado = determinarSolicitanteYaAdicionado();

            // Si a este MB lo llama el MB del CU-SA-AC-54 - Consultar
            // Informacion Entidades, se deshabilita el llamado al metodo para
            // restringir el numero de solicitantes/entidades a adicionar
            if (!banderaMBEsLlamadoDesdeCU54) {
                this.restringirNumeroSolicitantes(this.solicitanteSeleccionado);
            } else {
                this.banderaHabilitarGuardarSolicitante = true;
                this.banderaHabilitarBotonAdicionSolicitante = true;
            }

            if (this.banderaHabilitarGuardarSolicitante) {

                /*
                 * Se extraen los datos de relacion y notificacionEmail porque son atributos
                 * transient del solicitante y se pierden al actualizarlo
                 */
                relacion = this.solicitanteSeleccionado.getRelacion();
                notificacionEmail = this.solicitanteSeleccionado
                    .getNotificacionEmail();

                /*
                 * Se extraen los datos de Pais, Departamento y Municipio
                 */
                this.solicitanteSeleccionado
                    .setDireccionPais(this.paisSolicitante);
                this.solicitanteSeleccionado
                    .setDireccionDepartamento(this.departamentoSolicitante);
                this.solicitanteSeleccionado
                    .setDireccionMunicipio(this.municipioSolicitante);

                auxPais = this.solicitanteSeleccionado.getDireccionPais();
                auxDepartamento = this.solicitanteSeleccionado
                    .getDireccionDepartamento();
                auxMunicipio = this.solicitanteSeleccionado
                    .getDireccionMunicipio();

                this.solicitanteSeleccionado = this.getTramiteService()
                    .guardarActualizarSolicitante(
                        this.solicitanteSeleccionado);

                this.solicitanteSeleccionado.setRelacion(relacion);
                this.solicitanteSeleccionado
                    .setNotificacionEmail(notificacionEmail);
                this.solicitanteSeleccionado.setDireccionPais(auxPais);
                this.solicitanteSeleccionado
                    .setDireccionDepartamento(auxDepartamento);
                this.solicitanteSeleccionado
                    .setDireccionMunicipio(auxMunicipio);

                if (this.solicitanteSeleccionado != null) {
                    if (yaAdicionado) {

                        for (int cont = 0; cont < this.listaSolicitantes.size(); cont++) {
                            Solicitante sol = this.listaSolicitantes.get(cont);

                            if (sol.getNumeroIdentificacion() != null &&
                                 sol.getTipoIdentificacion() != null &&
                                 sol.getTipoIdentificacion().equals(
                                    this.solicitanteSeleccionado
                                        .getTipoIdentificacion()) &&
                                 sol.getNumeroIdentificacion().equals(
                                    this.solicitanteSeleccionado
                                        .getNumeroIdentificacion())) {
                                this.listaSolicitantes.set(cont,
                                    this.solicitanteSeleccionado);
                                break;
                            }
                        }

                        this.addMensajeWarn("El solicitante fue " +
                             "actualizado correctamente");
                    } else {
                        this.listaSolicitantes
                            .add(this.solicitanteSeleccionado);

                        this.addMensajeInfo("El solicitante fue " +
                             "adicionado correctamente");
                    }

                    if (this.banderaMBEsLlamadoDesdeCU121) {
                        this.actualizarSolicitanteSolicitudsDeSolicitud();
                    }

                }

            }

        } else {
            this.addMensajeError("Error al almacenar solicitante");
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB#guardarSolicitante");

    }

    /**
     * determina si el solicitante ya fue adicionado
     *
     * @author rodrigo.hernandez
     *
     * @return </br> <b>true</b> si ya fue adicionado </br> <b>false</b> si no ha sido adicionado a
     * la tabla</br>
     */
    private boolean determinarSolicitanteYaAdicionado() {
        boolean yaAdicionado = false;
        for (Solicitante sol : this.listaSolicitantes) {

            if (sol.getNumeroIdentificacion() != null &&
                 sol.getTipoIdentificacion() != null &&
                 sol.getTipoIdentificacion().equals(
                    this.solicitanteSeleccionado
                        .getTipoIdentificacion()) &&
                 sol.getNumeroIdentificacion().equals(
                    this.solicitanteSeleccionado
                        .getNumeroIdentificacion())) {
                yaAdicionado = true;
                break;
            }
        }
        return yaAdicionado;
    }

    // ---------------------------------------------------------------------------------------------------------
    //
    /**
     * Método que realiza la validación lógica de campos para solicitante
     *
     * @author rodrigo.hernandez
     */
    private boolean solicitanteValido() {

        if (!this.validarCorreoSolicitante() ||
             !this.validarPaisNumeroTelefonico()) {
            return false;
        }

        return true;
    }

    /**
     * Valida si al seleccionar el dato COLOMBIA para país, se seleccionen el departamento y el
     * municipio
     *
     * @author rodrigo.hernandez
     *
     * @return </br> <b>true:</b> si se seleccionaron el departamento y municipio al seleccionar el
     * pais colombia</br> <b>false:</b> si no se seleccionaron el departamento o el municipio al
     * seleccionar el pais Colombia
     */
    private boolean validarDeptoMunicParaPaisColombia() {

        if (this.validarPais()) {

            if (this.paisSolicitante.getCodigo().equals(Constantes.COLOMBIA)) {

                if (this.departamentoSolicitante == null) {
                    String mensaje = "Debe ingresar el dato requerido en Departamento";
                    this.addMensajeError(mensaje);

                    return false;
                }

                if (this.municipioSolicitante == null) {
                    String mensaje = "Debe ingresar el dato requerido en Municipio";
                    this.addMensajeError(mensaje);

                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Se valida si al diligenciar el dato de telefono principal, se están ingresando el dato de
     * pais
     *
     * @author rodrigo.hernandez
     *
     * @return false si se ignreso telefono principal pero no pais
     */
    private boolean validarPaisNumeroTelefonico() {
        boolean result = true;

        if (this.solicitanteSeleccionado.getTelefonoPrincipal() != null &&
             !this.solicitanteSeleccionado.getTelefonoPrincipal()
                .isEmpty() &&
             this.solicitanteSeleccionado.getDireccion() != null &&
             !this.solicitanteSeleccionado.getDireccion().isEmpty()) {

            result = this.validarPais();

        }

        return result;
    }

    private boolean validarPais() {
        boolean result = true;

        if (this.paisSolicitante == null) {

            String mensaje = "Debe ingresar el dato requerido en País";
            this.addMensajeError(mensaje);
            result = false;

        }

        return result;
    }

    /**
     * Valida que si se selecciono notificación por email el campo correo electrónico no debe ser
     * nulo o vacio.
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true </b>si se seleccionó el campo de notificar por correo y se ingresó correo
     * electronico</br> <b>false </b>si se seleccionó el campo de notificar por correo pero no se
     * ingresó correo electronico</br>
     *
     *
     *
     */
    private boolean validarCorreoSolicitante() {

        if (this.solicitanteSeleccionado.getNotificacionEmail() != null &&
             this.solicitanteSeleccionado.getNotificacionEmail().equals(
                ESiNo.SI.getCodigo()) &&
             (this.solicitanteSeleccionado.getCorreoElectronico() == null ||
            this.solicitanteSeleccionado
                .getCorreoElectronico().isEmpty())) {

            String mensaje = "La opción notificación por email fue seleccionada pero" +
                 " no se diligenció una cuenta de correo electrónico." +
                 " Por favor complete los datos e intente nuevamente.";
            this.addMensajeError(mensaje);

            return false;
        }
        return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que elimina un solicitante de la lista de solicitantes
     *
     * @author rodrigo.hernandez
     */
    public void eliminarSolicitanteDeLista() {

        for (Solicitante sol : this.listaSolicitantes) {
            if (sol.getId().compareTo(this.solicitanteSeleccionado.getId()) == 0) {
                this.listaSolicitantes.remove(sol);
                break;
            }
        }

        if (this.banderaMBEsLlamadoDesdeCU121) {
            this.actualizarSolicitanteSolicitudsDeSolicitud();
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que restringe el numero de solicitantes a ingresar, teniendo en cuenta: </br></br>
     * <b>1. </b>Cuando el solicitante sea persona NATURAL, solo existirá un solicitante en la tabla
     * resumen; por ende, no se podrán añadir más </br> </br> <b>2. </b>Cuando el solicitante sea
     * persona JURIDICA, solo existirá un solicitante en la tabla resumen y un representante legal
     * asociado a ese solicitante solicitante; por ende, no se podrán añadir más
     *
     * @author rodrigo.hernandez
     */
    private void restringirNumeroSolicitantes(Solicitante solicitante) {

        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB#restringirNumeroSolicitantes");

        // Se valida que la lista de solicitantes no este vacía ni sea
        // nula
        if (!this.listaSolicitantes.isEmpty() && this.listaSolicitantes != null) {

            // Se carga el solicitante que ya está almacenado en la tabla.
            Solicitante solicitanteAlmacenado = this.listaSolicitantes.get(0);

            // Se valida que el tipo de persona del solicitante a guardar sea
            // NATURAL
            if (solicitante.getTipoPersona().equals(
                EPersonaTipoPersona.NATURAL.getCodigo())) {

                if (solicitanteAlmacenado != null) {
                    // Se valida si el solicitante almacenado es persona
                    // JURIDICA
                    if (solicitanteAlmacenado.getTipoPersona().equals(
                        EPersonaTipoPersona.JURIDICA.getCodigo())) {

                        /*
                         * Se valida que el solicitante a guardar sea también REPRESENTANTE LEGAL
                         */
                        if (solicitante.getRelacion() != null &&
                             solicitanteAlmacenado
                                .getRelacion()
                                .equals(ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                                    .getValor().toUpperCase())) {

                            String mensaje = "Se asoció correctamente el representante legal.";
                            this.addMensajeInfo(mensaje);

                            this.banderaHabilitarGuardarSolicitante = true;
                            this.banderaHabilitarBotonAdicionSolicitante = false;

                        } /*
                         * el solicitante a guardar es PROPIETARIO
                         */ else {
                            String mensaje = "Se debe asociar únicamente un representante legal.";
                            this.addMensajeWarn(mensaje);

                            this.banderaHabilitarGuardarSolicitante = false;
                            this.banderaHabilitarBotonAdicionSolicitante = true;
                        }

                    } else {
                        // Si es persona natural y tiene el mismo Id,
                        // se toma como si se fuera a actualizar la información
                        if (solicitanteAlmacenado.getId().equals(
                            solicitante.getId())) {
                            this.banderaHabilitarGuardarSolicitante = true;
                            this.banderaHabilitarBotonAdicionSolicitante = false;
                        } /*
                         * Se intenta guardar un solicitante diferente de tipo NATURAL
                         */ else {
                            String mensaje =
                                "Se debe asociar únicamente una persona JURIDICA a la solicitud.";
                            this.addMensajeWarn(mensaje);

                            this.banderaHabilitarGuardarSolicitante = false;
                            this.banderaHabilitarBotonAdicionSolicitante = true;
                        }
                    }
                } /*
                 * No hay solicitante de tipo persona NATURAL almacenado
                 */ else {
                    this.banderaHabilitarGuardarSolicitante = true;
                    this.banderaHabilitarBotonAdicionSolicitante = false;
                }
            } // el tipo de persona del solicitante a guardar es JURIDICA
            else {

                // Se valida si el solicitante a guardar es propietario
                if (solicitante.getRelacion().equals(
                    ESolicitanteSolicitudRelac.PROPIETARIO.toString())) {

                    // Se valida si el solicitante que fue previamente insertado
                    // o ya estaba almacenado es Representante legal
                    if (solicitanteAlmacenado.getRelacion() == null ||
                         solicitanteAlmacenado
                            .getRelacion()
                            .equals(ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                                .getValor().toUpperCase())) {

                        // Se indica que se puede guardar la informacion del
                        // solicitante
                        this.banderaHabilitarGuardarSolicitante = true;

                        // Se indica que se oculta el boton para adicionar mas
                        // solicitantes ya que no se permiten ingresar mas
                        // solicitantes
                        this.banderaHabilitarBotonAdicionSolicitante = false;

                    }// El representante almacenado es Propietario
                    else {

                        // Si tiene el mismo Id,
                        // se toma como si se fuera a actualizar la información
                        if (solicitanteAlmacenado.getId().equals(
                            solicitante.getId())) {
                            this.banderaHabilitarGuardarSolicitante = true;
                            this.banderaHabilitarBotonAdicionSolicitante = false;
                        } else {

                            String mensaje = "Se debe asociar un representante legal al solicitante";
                            this.addMensajeWarn(mensaje);

                            // Se deshabilita la posibilidad de guardar el
                            // solicitante porque ya existe un propietario
                            this.banderaHabilitarGuardarSolicitante = false;

                            // Se activa el boton para adicionar mas
                            // solicitantes ya
                            // que aun queda pendiente añadir el representante
                            // legal
                            this.banderaHabilitarBotonAdicionSolicitante = true;
                        }
                    }

                    // Si el solicitante a guardar es Representante Legal
                } else if (solicitante.getRelacion().equals(
                    ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                        .getValor().toUpperCase())) {

                    // Se valida si el solicitante que fue previamente insertado
                    // o ya estaba almacenado es Propietario
                    if (solicitanteAlmacenado.getRelacion() == null ||
                         solicitanteAlmacenado.getRelacion().equals(
                            ESolicitanteSolicitudRelac.PROPIETARIO
                                .toString())) {

                        // Se indica que se puede guardar la informacion del
                        // solicitante
                        this.banderaHabilitarGuardarSolicitante = true;

                        // Se indica que se oculta el boton para adicionar mas
                        // solicitantes ya que no se permiten ingresar mas
                        // solicitantes
                        this.banderaHabilitarBotonAdicionSolicitante = false;

                    }// El representante almacenado es Representante Legal
                    else {
                        // Si tiene el mismo Id,
                        // se toma como si se fuera a actualizar la información
                        if (solicitanteAlmacenado.getId().equals(
                            solicitante.getId())) {
                            this.banderaHabilitarGuardarSolicitante = true;
                            this.banderaHabilitarBotonAdicionSolicitante = false;
                        } else {

                            String mensaje = "Se debe asociar un propietario al representante legal";
                            this.addMensajeWarn(mensaje);

                            // Se deshabilita la posibilidad de guardar el
                            // solicitante porque ya existe un propietario
                            this.banderaHabilitarGuardarSolicitante = false;

                            // Se activa el boton para adicionar mas
                            // solicitantes ya
                            // que aun queda pendiente añadir el representante
                            // legal
                            this.banderaHabilitarBotonAdicionSolicitante = true;
                        }
                    }
                }
            }

        } else {

            this.banderaHabilitarGuardarSolicitante = true;

            if (solicitante.getTipoPersona().equals(
                EPersonaTipoPersona.NATURAL.getCodigo())) {
                this.banderaHabilitarBotonAdicionSolicitante = false;
            } else {
                this.banderaHabilitarBotonAdicionSolicitante = true;
            }
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB#restringirNumeroSolicitantes");
    }

    // ------------------------------------------------------------------------------
    /**
     * Método para validar la visibilidad del boton <b>Adicionar Solicitante</b>
     * en la página <b>resumenSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    private void validarVisibilidadBotonAdicionarSolicitante() {
        int numeroSolicitantes = this.listaSolicitantes.size();
        Solicitante aux = null;

        // Se valida que la lista tenga datos
        if (numeroSolicitantes > 0) {

            aux = this.listaSolicitantes.get(0);

            // Existe la posibilidad que la tabla tenga un solicitante de tipo
            // NATURAL o tenga una persona de tipo JURIDICA
            if (numeroSolicitantes == 1) {
                // Se valida si el solicitante ingresado es persona NATURAL
                if (this.esSolicitanteSeleccionadoPersonaNatural(aux)) {
                    this.banderaHabilitarBotonAdicionSolicitante = false;
                } // el solicitante ingresado es persona JURIDICA
                else {
                    this.banderaHabilitarBotonAdicionSolicitante = true;
                }
            } // La lista tiene dos personas de tipo JURIDICA
            else if (numeroSolicitantes == 2) {
                this.banderaHabilitarBotonAdicionSolicitante = false;
            }
        } else {
            this.banderaHabilitarBotonAdicionSolicitante = true;
        }
    }

    // ------------------------------------------------------------------------------
    /**
     * Método para validar si el solicitante es Persona Natural
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true: </b>Es persona natural </br> <b>false: </b>Es persona juridica </br>
     */
    private boolean esSolicitanteSeleccionadoPersonaNatural(
        Solicitante solicitante) {
        boolean result = true;

        if (solicitante.getTipoPersona().equals(
            EPersonaTipoPersona.JURIDICA.getCodigo())) {
            result = false;
        }

        return result;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método usado para modificar la lista de SolicitanteSolicitud de una solicitud
     *
     * <b><i>Usado para caso de uso CU-SA-AC-121 - Diligenciar información solicitante</i></b>
     */
    private void actualizarSolicitanteSolicitudsDeSolicitud() {

        Solicitud solicitud = (Solicitud) this.extraData;

        if (solicitud.getSolicitanteSolicituds() == null) {
            solicitud
                .setSolicitanteSolicituds(new ArrayList<SolicitanteSolicitud>());
        }

        // Se comparan los tamaños de las listas para saber si se esta
        // insertando, eliminando o actualizando un registro
        if (solicitud.getSolicitanteSolicituds().size() < this.listaSolicitantes.size()) {
            this.crearSolicitanteSolicitud(solicitud);
        } else if (solicitud.getSolicitanteSolicituds().size() > this.listaSolicitantes.size()) {
            this.eliminarSolicitanteSolicitud(solicitud);
        } else {
            this.guardarSolicitanteSolicitud(solicitud);
        }

        this.extraData = solicitud;

        LOGGER.debug(
            "Fin AdministracionInformacionSolicitanteMB#actualizarSolicitanteSolicitudsDeSolicitud");

    }

    /**
     * Método para eliminar el registro de SolicitanteSolicitud asociado al solicitante seleccionado
     *
     * @param solicitud - Solicitud a la que pertenece la lista de SolicitantesSolicitud
     *
     * @author rodrigo.hernandez
     */
    private void eliminarSolicitanteSolicitud(Solicitud solicitud) {
        SolicitanteSolicitud solSol = null;

        for (int contador = 0; contador < solicitud.getSolicitanteSolicituds()
            .size(); contador++) {

            solSol = solicitud.getSolicitanteSolicituds().get(contador);

            if (solSol.getSolicitante().getId()
                .equals(this.solicitanteSeleccionado.getId())) {

                solicitud.getSolicitanteSolicituds().remove(contador);
            }
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB#eliminarSolicitanteSolicitud");

    }

    /**
     * Método para actualizar el registro de SolicitanteSolicitud asociado al solicitante
     * seleccionado
     *
     * @param solicitud - Solicitud a la que pertenece la lista de SolicitantesSolicitud
     *
     * @author rodrigo.hernandez
     */
    private void guardarSolicitanteSolicitud(Solicitud solicitud) {
        SolicitanteSolicitud solSol = null;

        for (int contador = 0; contador < solicitud.getSolicitanteSolicituds()
            .size(); contador++) {

            solSol = solicitud.getSolicitanteSolicituds().get(contador);

            if (solSol.getSolicitante().getId()
                .equals(this.solicitanteSeleccionado.getId())) {

                solSol.setSolicitud(solicitud);
                solSol.setSolicitante(this.solicitanteSeleccionado);
                solSol.setCargo(this.cargoSolicitante);
                solSol.incorporarDatosSolicitante(this.solicitanteSeleccionado);
                solicitud.getSolicitanteSolicituds().set(contador, solSol);
            }
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB#guardarSolicitanteSolicitud");
    }

    /**
     * Método para inserta un registro de SolicitanteSolicitud asociado al solicitante seleccionado
     *
     * @param solicitud - Solicitud a la que pertenece la lista de SolicitantesSolicitud
     *
     * @author rodrigo.hernandez
     */
    private void crearSolicitanteSolicitud(Solicitud solicitud) {
        SolicitanteSolicitud solSol = new SolicitanteSolicitud();

        solSol.setFechaLog(Calendar.getInstance().getTime());
        solSol.setUsuarioLog(this.usuario.getLogin());
        solSol.setSolicitud(solicitud);
        solSol.setSolicitante(this.solicitanteSeleccionado);
        solSol.incorporarDatosSolicitante(this.solicitanteSeleccionado);
        solicitud.getSolicitanteSolicituds().add(solSol);

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB#crearSolicitanteSolicitud");
    }

    // ---------------------------------------------------------------------
    /**
     * Método que se llama desde DiligenciamientoInformacionSolicitanteMB para cargar lista de
     * solicitantes
     *
     * @param listaSolicitantes - Lista de solicitantes
     *
     * @param banderaModoLecturaHabilitado - bandera que indica el modo de lectura de los datos</br>
     * <b>true: </b>Modo lectura </br> <b>false: </b>Modo edición
     * </br> </br>
     *
     * @param extraData - Dato extra, asociado a la información de los solicitantes que se necesite
     * modifcar
     *
     * @author rodrigo.hernandez
     */
    public void cargarDesdeCU121(List<SolicitanteSolicitud> listaSolicitantes,
        boolean banderaModoLecturaHabilitado, Solicitud solicitud) {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#cargarListaSolicitantesDesdeCU121");

        this.listaSolicitantes = this.crearListaSolicitantes(listaSolicitantes);

        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
        this.extraData = solicitud;

        this.banderaMBEsLlamadoDesdeCU121 = true;

        // Se define el icono segun el modo de lectura
        this.setIconVerEditarInfoSolicitante();

        // Se ajusta el boton de adicionar solicitante modo de datos
        this.validarVisibilidadBotonAdicionarSolicitante();

        this.definirMensajesDialogs();
        this.definirTextoBotonAdicionar();

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#cargarListaSolicitantesDesdeCU121");
    }

    // -------------------------------------------------------------------------------------
    /**
     * Método que se llama desde ConsultaInformacionEntidadesMB para cargar lista de entidades
     *
     * @param listaSolicitantes - Lista de solicitantes
     *
     * @param banderaModoLecturaHabilitado - bandera que indica el modo de lectura de los datos</br>
     * <b>true: </b>Modo lectura </br> <b>false: </b>Modo edición
     * </br> </br>
     *
     * @param extraData - Dato extra, asociado a la información de los solicitantes que se necesite
     * modifcar
     *
     * @author rodrigo.hernandez
     */
    public void cargarDesdeCU54(List<Solicitante> listaSolicitantes) {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#cargarListaSolicitantesDesdeCU54");

        this.listaSolicitantes = listaSolicitantes;

        this.banderaModoLecturaHabilitado = false;
        this.banderaMBEsLlamadoDesdeCU54 = true;
        this.banderaHabilitarBotonAdicionSolicitante = true;

        this.definirMensajesDialogs();
        this.definirTextoBotonAdicionar();

        // Se define el icono segun el modo de lectura
        this.setIconVerEditarInfoSolicitante();

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#cargarListaSolicitantesDesdeCU54");
    }

    /**
     * Método para cargar este MB desde otro MB que envíe una lista de SolicitanteSolicituds y no
     * requiera ninguna condicion especial para visualizar la informacion de los solicitantes
     *
     * @author rodrigo.hernandez
     *
     * @param listaSolicitanteSolicituds - Lista cuyos datos son de tipo SolicitanteSolicitud
     */
    public void cargarDesdeOtrosMBConSolicitanteSolicituds(
        List<SolicitanteSolicitud> listaSolicitanteSolicituds) {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#cargarDesdeOtrosMBConSolicitanteSolicituds");

        this.listaSolicitantes = this
            .crearListaSolicitantes(listaSolicitanteSolicituds);

        this.banderaModoLecturaHabilitado = true;
        this.banderaHabilitarBotonAdicionSolicitante = false;

        this.definirMensajesDialogs();
        this.definirTextoBotonAdicionar();

        // Se define el icono segun el modo de lectura
        this.setIconVerEditarInfoSolicitante();

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#cargarDesdeOtrosMBConSolicitanteSolicituds");
    }

    /*
     * ----- Funciones para acciones relacionadas a contratos de la entidad/solicitante -----
     */
    /**
     * Método para crear contratos. </br> <i><b>Llama a MB de CU-SA-AC-008 Administrar Contratos
     * Interadministrativos</b></i>
     *
     * @author rodrigo.hernandez
     */
    public void crearContrato() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#crearContrato");

        this.adminContratosInteradministrativosMB =
            (AdminContratosInteradministrativosMB) UtilidadesWeb
                .getManagedBean("adminContratosInteradministrativos");
        this.adminContratosInteradministrativosMB
            .cargarDesdeCU52(this.solicitanteSeleccionado);

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#crearContrato");
    }

    /*
     *
     * ----- Funciones de utilería --------------------------
     */
    /**
     * Método para definir los mensajes de los dialogs para eliminacion y adicion de
     * Solicitante/Entidad
     *
     * @author rodrigo.hernandez
     */
    private void definirMensajesDialogs() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#definirMensajesDialogs");

        if (this.banderaMBEsLlamadoDesdeCU54) {
            this.mensajeConfirmacionAdicion = "¿Está seguro de almacenar los datos de la entidad?";
            this.mensajeConfirmacionEliminacion =
                "¿Está seguro que desea eliminar la entidad de la lista?";
        } else {
            this.mensajeConfirmacionAdicion = "¿Está seguro de almacenar los datos del solicitante?";
            this.mensajeConfirmacionEliminacion =
                "¿Está seguro que desea eliminar el solicitante de la lista?";
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#definirMensajesDialogs");
    }

    /**
     * Método para definir el texto del boton para <b>Adicionar solicitante/entidad</b> en la pagina
     * <b>resumenSolicitante.xhtml</b>
     * adicion de Solicitante/Entidad
     *
     * @author rodrigo.hernandez
     */
    private void definirTextoBotonAdicionar() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#definirTextoBotonAdicionar");

        if (this.banderaMBEsLlamadoDesdeCU54) {
            this.textoBotonAdicionar = "Adicionar entidad";
        } else {
            this.textoBotonAdicionar = "Adicionar solicitante";
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#definirTextoBotonAdicionar");
    }

    /**
     * Método que se ejecuta para validar los campos obligatorios del formulario de la pagina
     * <b>capturaDatosSolicitante.xhtml</b>
     *
     * @author rodrigo.hernandez
     */
    public void validarDatosObligatoriosFormularioSolicitanteEntidad() {
        LOGGER.debug("Inicio AdministracionInformacionSolicitanteMB" +
             "#validarDatosObligatoriosFormularioSolicitanteEntidad");

        boolean esSolicitantePersonaNatural = this
            .esSolicitanteSeleccionadoPersonaNatural(this.solicitanteSeleccionado);

        // Se valida si el solicitante seleccionado es persona natural
        if (esSolicitantePersonaNatural) {
            if ((this.solicitanteSeleccionado.getTipoPersona().isEmpty() ||
                this.solicitanteSeleccionado
                    .getTipoPersona() == null) ||
                 (this.solicitanteSeleccionado.getTipoIdentificacion()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getTipoIdentificacion() == null) ||
                 (this.solicitanteSeleccionado.getNumeroIdentificacion()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getNumeroIdentificacion() == null) ||
                 (this.solicitanteSeleccionado.getPrimerNombre()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getPrimerNombre() == null) ||
                 (this.solicitanteSeleccionado.getPrimerApellido()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getPrimerApellido() == null)) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        }// el solicitante seleccionado es persona juridica
        else {
            if ((this.solicitanteSeleccionado.getTipoPersona().isEmpty() ||
                this.solicitanteSeleccionado
                    .getTipoPersona() == null) ||
                 (this.solicitanteSeleccionado.getTipoSolicitante()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getTipoSolicitante() == null) ||
                 (this.solicitanteSeleccionado.getNumeroIdentificacion()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getNumeroIdentificacion() == null) ||
                 (this.solicitanteSeleccionado.getDigitoVerificacion()
                    .isEmpty() || this.solicitanteSeleccionado
                    .getDigitoVerificacion() == null) ||
                 (this.solicitanteSeleccionado.getRazonSocial().isEmpty() ||
                this.solicitanteSeleccionado
                    .getRazonSocial() == null)) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        }

        LOGGER.debug("Fin AdministracionInformacionSolicitanteMB" +
             "#validarDatosObligatoriosFormularioSolicitanteEntidad");
    }

    /**
     * Método para crear una lista de solicitantes a partir de la lista de SolicitanteSolicitud
     *
     * @param listaSolicitanteSolicitud - Lista de solicitanteSolicitud asociado a la solicitud
     *
     * @return Lista de solicitantes
     *
     * @author rodrigo.hernandez
     */
    private List<Solicitante> crearListaSolicitantes(
        List<SolicitanteSolicitud> listaSolicitanteSolicitud) {
        List<Solicitante> listaSolicitantes = new ArrayList<Solicitante>();

        for (SolicitanteSolicitud solSol : listaSolicitanteSolicitud) {
            Solicitante sol = solSol.getSolicitante();

            // Se asigna la relacion al objeto Solicitante
            sol.setRelacion(solSol.getRelacion());

            listaSolicitantes.add(sol);
        }

        return listaSolicitantes;
    }

}
