/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.ejecucion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.avaluos.AreaCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaInmobiliaria;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaAreaCapturaOfertaEstado;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.RegistroOfertaInmobiliariaMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-07 Cargar al SNC
 *
 * @author ariel.ortiz
 * @cu CU-TV-0F-07
 * @version 2.0
 *
 */
@Component("cargarOfertasSNC")
@Scope("session")
public class CargaOfertasAlSNCMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistroOfertaInmobiliariaMB.class);

    /**
     * Serial autogenerado
     */
    private static final long serialVersionUID = -6182205142379617591L;

    @Autowired
    private RegistroOfertaInmobiliariaMB registroOfertaInmobiliaria;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * Id de la región seleccionada desde el arbol de procesos
     */
    private Long regionId;

    //------------------    procesos
    @Autowired
    protected TareasPendientesMB tareasPendientesMB;

    private Actividad bpmActivityInstance;

    // --------Atributos-----------------
    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    private List<OfertaInmobiliaria> ofertasInmob;
    private OfertaInmobiliaria ofertaInmobSeleccionada;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Esta variable es para el parametro de las capas del mapa
     */
    private String moduleLayer;
    /**
     * Esta variable es para el parametro de las herramientas del mapa
     */
    private String moduleTools;
    /**
     * En esta variable se guardan los codigos de los predios asociados a la oferta para hacerles
     * zoom en el visor
     */
    private String prediosParaZoom;

    /**
     * Region para ingresar las ofertas
     */
    private RegionCapturaOferta regionCapturaOferta;

    private boolean banderaRegionCerrada;

    // --------Getters y Setters---------
    public List<OfertaInmobiliaria> getOfertasInmob() {
        return ofertasInmob;
    }

    public void setOfertasInmob(List<OfertaInmobiliaria> ofertasInmob) {
        this.ofertasInmob = ofertasInmob;
    }

    public OfertaInmobiliaria getOfertaInmobSeleccionada() {
        return ofertaInmobSeleccionada;
    }

    public void setOfertaInmobSeleccionada(OfertaInmobiliaria ofertaInmobSeleccionada) {
        this.ofertaInmobSeleccionada = ofertaInmobSeleccionada;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public boolean isBanderaRegionCerrada() {
        return banderaRegionCerrada;
    }

    public void setBanderaRegionCerrada(boolean banderaRegionCerrada) {
        this.banderaRegionCerrada = banderaRegionCerrada;
    }

    // --------Methods---------
    public RegionCapturaOferta getRegionCapturaOferta() {
        return regionCapturaOferta;
    }

    public void setRegionCapturaOferta(RegionCapturaOferta regionCapturaOferta) {
        this.regionCapturaOferta = regionCapturaOferta;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("on CargaOfertasAlSNC init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.extraerIdRegionCapturaDelArbol();

        this.cargueDeOfertasInmobiliarias();

        this.inicializarVariablesVisorGIS();
    }

    /**
     * Extrae el id de la región de captura del objeto seleccionado desde el arbol de procesos
     *
     * @author christian.rodriguez
     */
    private void extraerIdRegionCapturaDelArbol() {
        this.bpmActivityInstance = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.regionId = Long.valueOf(this.bpmActivityInstance.getIdObjetoNegocio());
    }

    /**
     * Define los criterios que tiene la consulta para traer las ofertas
     *
     * @author christian.rodriguez
     *
     * @modified rodrigo.hernandez 14-06-2012 Se cambia la función para cargar la lista de ofertas
     * de un recolector
     */
    private void cargueDeOfertasInmobiliarias() {

        this.regionCapturaOferta = this.getAvaluosService().buscarRegionCapturaOfertaPorId(
            this.regionId);

        //Carga la lista de ofertas del recolector
        this.cargarListaDeOfertasRecolector();
    }

    /**
     * Carga la lista de ofertas inmobiliarias CREADAs y FINALIZADAs del recolector
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaDeOfertasRecolector() {
        this.ofertasInmob = this.getAvaluosService()
            .buscarListaOfertasInmobiliariasPorRecolector(
                this.usuario.getLogin(), this.regionCapturaOferta.getId(), EOfertaEstado.CREADA.
                getCodigo());

        this.ofertasInmob.addAll(this.getAvaluosService()
            .buscarListaOfertasInmobiliariasPorRecolector(
                this.usuario.getLogin(), this.regionCapturaOferta.getId(), EOfertaEstado.FINALIZADA.
                getCodigo()));
    }

    /**
     * Abre la página de registro/modificación de ofertas inmobiliarias
     *
     * @author christian.rodriguez
     * @modified rodrigo.hernandez se realiza llamado a metodo de RegistroOfertaInmobiliaria para
     * asignar region y recolector a la oferta q se vaya a crear
     *
     * @return le URL del caso de uso de registro de ofertas inmobiliarias
     */
    public String registrarModificarOferta() {

        this.registroOfertaInmobiliaria.init();

        this.registroOfertaInmobiliaria.setBanderaCreacionOfertaDesdeCargaAlSNC(true);

        this.registroOfertaInmobiliaria.precargaDatosCapturaOferta(
            this.regionCapturaOferta, this.usuario.getLogin());

        UtilidadesWeb.removerManagedBean("cargarOfertasSNC");

        return "/avaluos/ofertasInmob/registroOfertaInmobiliaria.jsf";
    }

    /**
     * Inicializa las variables requeridas por el VISOR GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        // se pone en of para que muestre la capa del mapa de ofertas
        // inmobiliarias
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        // no se pone en of porque en este caso de uso no hay permiso para
        // edicion
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";
    }

    /**
     * Finaliza una oferta inmobiliaria cambiandole el estado a finalizada
     *
     * @author christian.rodriguez
     */
    public void finalizarOferta() {
        if (this.ofertaInmobSeleccionada != null) {

            this.ofertaInmobSeleccionada = this.getAvaluosService().buscarOfertaInmobiliariaPorId(
                this.ofertaInmobSeleccionada.getId());

            this.ofertaInmobSeleccionada.setEstado(EOfertaEstado.FINALIZADA.getCodigo());

            this.getAvaluosService().guardarActualizarOfertaInmobiliaria(
                this.ofertaInmobSeleccionada, null, usuario);

            //Carga la lista de ofertas del recolector
            this.cargarListaDeOfertasRecolector();
        }
    }

    /**
     * Método para cerrar la región. Este método valida que todas las ofertas de la región estén
     * finalizadas para poder cerrar la región
     *
     * @author rodrigo.hernandez
     */
    public void cerrarRegion() {
        int contadorOfertasFinalizadas = 0;
        //Se valida que TODAS las ofertas creadas esten finalizadas

        for (OfertaInmobiliaria oi : this.ofertasInmob) {
            if (oi.getEstado() != null) {
                if (oi.getEstado().equals(EOfertaEstado.FINALIZADA.getCodigo())) {
                    contadorOfertasFinalizadas++;
                }
            }
        }

        if (this.ofertasInmob.isEmpty()) {

            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje =
                "La región no tiene ofertas registradas, por lo tanto no puede ser cerrada.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");

        } else if (contadorOfertasFinalizadas == this.ofertasInmob.size()) {

            if (this.forwardProcess()) {

                this.doDatabaseStatesUpdate();

                String mensaje = "La región fue cerrada exitosamente";
                this.addMensajeInfo(mensaje);
                this.banderaRegionCerrada = true;
            }

        } else {

            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "Todas las ofertas deben estar finalizadas para cerrar la región";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");

        }

    }

    /**
     * Método que mueve el proceso
     *
     * @author christian.rodriguez
     * @return true si el proceso se pudo mover satisfactoriamente
     */
    private boolean forwardProcess() {

        co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria ofertaBPM =
            this.crearObjectoDeNegocio();

        if (ofertaBPM == null) {
            return false;
        }

        String idActividad = this.bpmActivityInstance.getId();
        this.getProcesosService().avanzarActividad(idActividad, ofertaBPM);

        Long areaCapturaId = this.bpmActivityInstance.getIdCorrelacion();
        this.revisarCerrarProcesoPadre(areaCapturaId);

        return true;

    }

    /**
     * Revisa si el proceso padre asociado al área de captura tiene o no otras regiones activas
     * (procesos hijo), de no tener, entonces finaliza el proceso padre. Tambien revisa que no hayan
     * manazanas sin asociar a alguna región, de ser así el proceso padre sigue abierto
     *
     * @author christian.rodriguez
     * @param areaCapturaId id del áre de captura asociado a la región finalizada
     */
    private void revisarCerrarProcesoPadre(Long areaCapturaId) {

        AreaCapturaOferta aco = this.getAvaluosService().buscarAreaCapturaOfertaPorId(
            areaCapturaId);

        if (aco != null) {
            String idProcesoPadre = aco.getProcesoInstanciaId();

            try {
                Thread.sleep(2500);
                LOGGER.debug("Pausa mientras se espera que se cierre el proceso hijo asociado.");
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                LOGGER.debug("Ocurrio un error al pausar la ejecución del hilo.");
            }

            co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria procesoPadre =
                this
                    .getProcesosService().obtenerObjetoNegocioOfertasInmobiliariasPorIdProceso(
                        idProcesoPadre);

            if (procesoPadre != null) {
                if (procesoPadre.getOfertasEnEjecucion() == 0 &&
                     aco.getDetalleCapturaOfertas() != null) {

                    boolean tieneManzanaSinAsignar = false;

                    for (DetalleCapturaOferta manzana : aco.getDetalleCapturaOfertas()) {
                        if (manzana.getRegionCapturaOferta() == null) {
                            tieneManzanaSinAsignar = true;
                        }
                    }

                    if (!tieneManzanaSinAsignar) {
                        String trasicion = "Terminar";
                        procesoPadre.setTransicion(trasicion);

                        List<Actividad> actividadesProcesoPadre = this.getProcesosService()
                            .obtenerActividadesProceso(idProcesoPadre);

                        String idActividad = new String();
                        for (Actividad actividad : actividadesProcesoPadre) {
                            if (actividad.getNombre().equalsIgnoreCase(
                                ProcesoDeOfertasInmobiliarias.ACT_GEST_AREAS_ASIGNAR_RECOLECTOR_AREA)) {
                                idActividad = actividad.getId();
                            }
                        }

                        procesoPadre.setTerritorial(this.usuario
                            .getDescripcionEstructuraOrganizacional());
                        procesoPadre
                            .setObservaciones(
                                "Finalizado por terminación de proceso de recolección exitoso");

                        if (!idActividad.isEmpty()) {

                            this.getProcesosService().avanzarActividad(idActividad, procesoPadre);

                            aco.setEstado(EOfertaAreaCapturaOfertaEstado.FINALIZADA.getCodigo());
                            this.getAvaluosService().guardarAreaCapturaOferta(aco, this.usuario);
                        }
                    }

                }
            }
        }
    }

    /**
     * Crea el objeto de negocio que se va a mvoer por el proceso de control de calidad
     *
     * @author christian.rodriguez
     * @return objeto de negocio con los valores necesarios para la creación del proceso
     */
    private co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria crearObjectoDeNegocio() {

        co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria ofertaBPM =
            new co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria();
        Departamento departamento;
        Municipio municipio;
        Zona zona;
        DivisionAdministrativa departamentoBPM, municipioBPM, zonaBPM;

        departamento = this.regionCapturaOferta.getAreaCapturaOferta().getDepartamento();
        municipio = this.regionCapturaOferta.getAreaCapturaOferta().getMunicipio();
        zona = this.regionCapturaOferta.getAreaCapturaOferta().getZona();

        departamentoBPM = new DivisionAdministrativa(departamento.getCodigo(),
            departamento.getNombre());
        municipioBPM = new DivisionAdministrativa(municipio.getCodigo(), municipio.getNombre());

        if (zona != null) {
            zonaBPM = new DivisionAdministrativa(zona.getCodigo(), zona.getNombre());
            ofertaBPM.setZona(zonaBPM);
        }

        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        usuarios.add(this.usuario);

        ofertaBPM.setUsuarios(usuarios);
        ofertaBPM.setIdentificador(this.regionCapturaOferta.getId());
        ofertaBPM.setIdCorrelacion(this.regionCapturaOferta.getAreaCapturaOferta().getId());
        ofertaBPM.setDepartamento(departamentoBPM);
        ofertaBPM.setMunicipio(municipioBPM);
        ofertaBPM
            .setObservaciones("Finalización del proceso de Ofertas inmobiliarias");
        ofertaBPM
            .setTransicion(ProcesoDeOfertasInmobiliarias.ACT_EJECUCION_FINALIZAR);
        ofertaBPM.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());

        ofertaBPM.setFuncionario(this.usuario.getNombreCompleto());
        ofertaBPM.setIdRegion(String.valueOf(this.regionId));

        return ofertaBPM;
    }

    /**
     * Actualiza los objetos involucrados en el proceso de negocio. ACtualiza la región y la coloca
     * en estado FINALIZADA
     *
     * @author christian.rodriguez
     */
    private void doDatabaseStatesUpdate() {

        this.regionCapturaOferta.setEstado(EEstadoRegionCapturaOferta.CONTROL_CALIDAD.getEstado());
        this.getAvaluosService().guardarActualizarRegionCapturaOferta(regionCapturaOferta);

    }

    /**
     * Método ejecutado al cerrar la página
     *
     * @author christian.rodriguez
     * @return destino de la navegación
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

}
