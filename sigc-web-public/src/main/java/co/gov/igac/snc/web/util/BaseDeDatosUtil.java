/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import java.util.Date;

/**
 *
 * Se creó esta clase para evitar el acoplamiento de clases entities (por ejemplo que un
 * TramiteEstado deba conocer a un Tramite.
 *
 * Clase donde van métodos para: - armar objetos de base de datos que se usen en varias partes - ...
 *
 *
 *
 * @author pedro.garcia
 */
public class BaseDeDatosUtil {

    /**
     * Arma un objeto TramiteEstado para ser insertyado en BD
     *
     * @author pedro.garcia
     * @param estado
     * @param tramite
     * @param usuario
     * @return
     * @deprecated porque TramiteEstado ya conoce a Tramite, por lo que se creó un constructor que
     * hace esto allá
     * @see TramiteEstado(java.lang.String, co.gov.igac.snc.persistence.entity.tramite.Tramite,
     * java.lang.String)
     */
    @Deprecated
    public static TramiteEstado armarTramiteEstadoObjeto(String estado, Tramite tramite,
        UsuarioDTO usuario) {

        TramiteEstado newTramiteEstado = new TramiteEstado();

        newTramiteEstado.setEstado(estado);
        newTramiteEstado.setTramite(tramite);
        newTramiteEstado.setResponsable(usuario.getLogin());
        newTramiteEstado.setMotivo("");
        newTramiteEstado.setFechaInicio(new Date());

        return newTramiteEstado;
    }
//--------------------------------------------------------------------------------------------------

//end of class
}
