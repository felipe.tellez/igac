/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.TipoSolicitudTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.EDatoRectificarNaturaleza;
import co.gov.igac.snc.persistence.util.EMutacion2Subtipo;
import co.gov.igac.snc.persistence.util.EMutacion5Subtipo;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteFuente;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.conservacion.ValidacionYAdicionNuevoTramiteMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.tramite.gestion.RecuperacionTramitesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso de cargue de información al sistema
 *
 * @author pedro.garcia
 */
@Component("cargueInfoAlSistema")
@Scope("session")
public class CargueInfoAlSistemaMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -7550906157281308499L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CargueInfoAlSistemaMB.class);

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private ValidacionYAdicionNuevoTramiteMB validacionTramiteMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Valor de decision sobre cancelar el trámite inicial.
     */
    private String cancelarTramiteDecision;

    /**
     * Motivo de cancelar el trámite inicial
     */
    private String motivoCancelacion;

    /**
     * Lista de trámites asociados al trámite
     */
    private List<Tramite> tramiteTramites;

    /**
     * Trámite asociado a un trámite seleccionado
     */
    private Tramite tramiteTramiteSeleccionado;

    /**
     * Lista que contiene los predios asociados a un trámite
     */
    private List<TramitePredioEnglobe> prediosTramite;

    /**
     * Variable contadora de la asignaciónd de orden para los trámites seleccionados
     */
    private List<SelectItem> ordenEjecucionTramites;

    /**
     * Clases de mutación permitidas para los nuevos trámites generados en el CU
     */
    private List<SelectItem> mutacionClases;

    /**
     * lista con los valores del combo de tipos de trámite que se permiten para el tipo de solicitud
     * creada
     */
    private ArrayList<SelectItem> tiposTramitePorSolicitud;

    /**
     * lista con los DatoRectificar que se han seleccionado en el pick list
     */
    private List<DatoRectificar> selectedDatosRectifComplDataTable;

    /**
     * Partes del número predial para mutaciones de segunda o quinta
     */
    private String numeroPredialParte1;
    private String numeroPredialParte2;
    private String numeroPredialParte3;
    private String numeroPredialParte4;
    private String numeroPredialParte5;
    private String numeroPredialParte6;
    private String numeroPredialParte7;
    private String numeroPredialParte8;
    private String numeroPredialParte9;
    private String numeroPredialParte10;
    private String numeroPredialParte11;
    private String numeroPredialParte12;
    /**
     * Objeto temporal de seleccion en la tabla predioTramite
     */
    private TramitePredioEnglobe selectedPredioTramite;
    /**
     * Lista que contiene los subtipos para las clases de mutación
     */
    private List<SelectItem> subtipoClaseMutacionV;
    /**
     * tipo de dato para rectificación o complementación seleccionado
     */
    private String tipoDatoRectifComplSeleccionado;
    /**
     * modelo del pick list de datos a modificar o complementar Se requieren dos listas: una fuente
     * y una destino
     */
    private DualListModel<DatoRectificar> datosRectifComplDLModel;
    private List<DatoRectificar> datosRectifComplSource;
    private List<DatoRectificar> datosRectifComplTarget;
    /**
     * usuario loggeado
     */
    private UsuarioDTO usuario;
    /**
     * lista de trámites que están en esta actividad
     */
    private List<Tramite> tramitesParaCargueInfo;
    /**
     * trámites seleccionados para cargue de información
     */
    private Tramite selectedTramiteCargueInfo;
    /**
     * el resultado de la visita solo se ingresa solo para un trámite
     */
    private Tramite selectedTramiteParaResultadoVisita;
    /**
     * trámites seleccionados de la tabla de nuevos trámites
     */
    private Tramite[] selectedNuevosTramitesCargueInfo;
    /**
     * trámite usado para asociar trámites nuevos
     */
    private Tramite tramiteParaTramitesAsociados;
    /**
     * ComisionTramite del trámite seleccionado para resultado de visita. Sirve para guardar los
     * valores de la parte correspondiente en esa página
     */
    private ComisionTramite currentComisionTramite;

    /**
     * lista con los DatoRectificar que se han seleccionado en el data table de resumen
     */
    private DatoRectificar[] selectedDatosRectifComplDT;

    /**
     * valor del combo
     */
    private String hayNuevosTramites;

    /**
     * Tramite para asociar tramites
     */
    private Tramite tramiteSeleccionado;

    private ComisionTramite selectedComisionTramite;

    //------------- banderas    ----------
    /**
     * bandera para indicar si el subtipo de trámite es dato requerido
     */
    private boolean subtipoTramiteEsRequerido;

    /**
     * Bandera que determina si el subtipo de la mutación de segunda es englobe
     */
    private boolean banderaSubtipoEnglobe;

    /**
     * Bandera que determina si se selecciono una clase de mutación
     */
    private boolean banderaClaseDeMutacionSeleccionada;

    /**
     * Bandera que indica si es tramiteTramite seleccionado es nuevo o va a ser editado
     */
    private boolean banderaEdicionTramiteTramite;

    /**
     * Bandera que indica si la sección para adicionar nuevos tramites esta habilidad
     */
    private boolean nuevosTramitesHabilitado;

    /**
     * Bandera para mostrar el tramite asociado sin permitir editarlo
     */
    private boolean banderaVerTramitePorAsociar;

    /**
     * Bandera que determina si el subtipo de la mutación de quinta es nuevo
     */
    private boolean banderaSubtipoNuevo;

    /**
     * Bandera para expander la tabla
     */
    private boolean banderaExpanderTabla;

    /**
     * Bandera para obligar al usuario a guardar el resultado de la visita antes de mover el proceso
     */
    private boolean guardadoFlag;

    /**
     * Bandera para determinar si el tramite es de depuracion
     */
    private boolean banderaDepuracion;

    /**
     * Bandera para determinar si un tramite de conservaciòn necesita depuraciòn
     */
    private boolean banderaNecesitaDepuracion;
    /**
     * Bandera para determinar si un tramite seleccionado es de rectificacion
     */
    private boolean banderaRectificaZonas;

    /**
     * indica, internamente, si la actualización de bd que se hace al avanzar el proceso fue
     * exitosa. Se debe usar porque el método que hace las actualizaciones es implementación de la
     * interfaz (void)
     */
    private boolean dbStateUpdateOK;

    /**
     * Bandera para determinar si el tramite seleccionado requiere ajuste espacial
     */
    private boolean banderaTramiteEspacial;

    /**
     * indica si el botón "Devolver por comisión modificada" se debe mostrar en la interfaz de
     * usuario
     */
    private boolean mostrarBotonDevolverXComisCancelada;

    /**
     * indica si se debe habilitar o mostrar el botón 'informar resultado de la visita'
     */
    private boolean showBotonInformarResultadoVisita;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteMemorandoComision;

    /**
     * Objeto temporal de seleccion en la tabla predioColindantes
     */
    private Predio selectedPredioColindante;

    /**
     * Objeto que contiene los datos relacionados a la depuración del trámite
     */
    private TramiteDepuracion tramiteDepuracionInfo;

    /**
     * Inconsistencias relacionadas al trámite
     */
    private List<TramiteInconsistencia> inconsistenciasTramite;

    /**
     * Actividad actual del proceso.
     */
    private Actividad currentProcessActivity;

    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;

    /**
     * Lista que contiene los predios colindantes para englobe asociados al predio seleccionado
     */
    private List<Predio> listaPrediosColindantes;

    /**
     * Lista de predios originales del tramite Inicial
     */
    private List<TramitePredioEnglobe> prediosOriginalesList;

    /**
     * Bandera para habilitar las opciones de cancelar tramite inicial
     */
    private boolean habilitarCancelarTramite;

    /**
     * Bandera para hacer render los componentes de cancelación y trámites nuevos de la página
     */
    private boolean realizoInspeccionOcular;

    /**
     * Bandera para hacer render el componente de trámites nuevos de la página, cuando es tramite de
     * quinta nuevo
     */
    private boolean habilitarTramitesAdicionales;

    /**
     * Bandera para determinar si el tramite de depuracion tiene tramites asociados
     */
    private boolean banderaTramitesAsociadosDepuracion;

    /**
     * Bandera para determinar si un tramite tiene replica de consulta asociada.
     */
    private boolean banderaReplicaConsulta;

    /**
     * Bandera para saber si los predios del tramite seleccionado son fiscales
     */
    private boolean banderaPredioFiscal;

//------------     methods  ----------------------------
    public boolean isShowBotonInformarResultadoVisita() {
        return this.showBotonInformarResultadoVisita;
    }

    public void setShowBotonInformarResultadoVisita(boolean showBotonInformarResultadoVisita) {
        this.showBotonInformarResultadoVisita = showBotonInformarResultadoVisita;
    }

    public boolean isBanderaReplicaConsulta() {
        return banderaReplicaConsulta;
    }

    public void setBanderaReplicaConsulta(boolean banderaReplicaConsulta) {
        this.banderaReplicaConsulta = banderaReplicaConsulta;
    }

    public boolean isMostrarBotonDevolverXComisCancelada() {
        return this.mostrarBotonDevolverXComisCancelada;
    }

    public void setMostrarBotonDevolverXComisCancelada(boolean mostrarBotonDevolverXComisCancelada) {
        this.mostrarBotonDevolverXComisCancelada = mostrarBotonDevolverXComisCancelada;
    }

    public boolean isBanderaRectificaZonas() {
        return banderaRectificaZonas;
    }

    public void setBanderaRectificaZonas(boolean banderaRectificaZonas) {
        this.banderaRectificaZonas = banderaRectificaZonas;
    }

    public ComisionTramite getCurrentComisionTramite() {
        return this.currentComisionTramite;
    }

    public void setCurrentComisionTramite(ComisionTramite currentComisionTramite) {
        this.currentComisionTramite = currentComisionTramite;
    }

    public boolean isBanderaTramitesAsociadosDepuracion() {
        return banderaTramitesAsociadosDepuracion;
    }

    public void setBanderaTramitesAsociadosDepuracion(boolean banderaTramitesAsociadosDepuracion) {
        this.banderaTramitesAsociadosDepuracion = banderaTramitesAsociadosDepuracion;
    }

    public boolean isBanderaTramiteEspacial() {
        return banderaTramiteEspacial;
    }

    public void setBanderaTramiteEspacial(boolean banderaTramiteEspacial) {
        this.banderaTramiteEspacial = banderaTramiteEspacial;
    }

    public boolean isBanderaDepuracion() {
        return banderaDepuracion;
    }

    public void setBanderaDepuracion(boolean banderaDepuracion) {
        this.banderaDepuracion = banderaDepuracion;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public Tramite getTramiteParaTramitesAsociados() {
        return this.tramiteParaTramitesAsociados;
    }

    public void setTramiteParaTramitesAsociados(Tramite tramiteParaTramitesAsociados) {
        this.tramiteParaTramitesAsociados = tramiteParaTramitesAsociados;
    }

    public TramiteDepuracion getTramiteDepuracionInfo() {
        return this.tramiteDepuracionInfo;
    }

    public void setTramiteDepuracionInfo(TramiteDepuracion tramiteDepuracionInfo) {
        this.tramiteDepuracionInfo = tramiteDepuracionInfo;
    }

    public boolean isBanderaNecesitaDepuracion() {
        return this.banderaNecesitaDepuracion;
    }

    public void setBanderaNecesitaDepuracion(boolean banderaNecesitaDepuracion) {
        this.banderaNecesitaDepuracion = banderaNecesitaDepuracion;
    }

    public String getHayNuevosTramites() {
        return this.hayNuevosTramites;
    }

    public void setHayNuevosTramites(String hayNuevosTramites) {
        this.hayNuevosTramites = hayNuevosTramites;
        if (this.hayNuevosTramites.equalsIgnoreCase(ESiNo.SI.getCodigo())) {
            this.nuevosTramitesHabilitado = true;
        } else {
            this.nuevosTramitesHabilitado = false;
        }
    }

    public Tramite[] getSelectedNuevosTramitesCargueInfo() {
        return this.selectedNuevosTramitesCargueInfo;
    }

    public void setSelectedNuevosTramitesCargueInfo(Tramite[] selectedNuevosTramitesCargueInfo) {
        this.selectedNuevosTramitesCargueInfo = selectedNuevosTramitesCargueInfo;
    }

    public boolean isNuevosTramitesHabilitado() {
        if (this.hayNuevosTramites.equals(ESiNo.SI.getCodigo())) {
            return true;
        } else {
            return false;
        }
    }

    public void setNuevosTramitesHabilitado(boolean thereAreNewTramites) {
        this.nuevosTramitesHabilitado = thereAreNewTramites;
    }

    public Tramite getSelectedTramiteParaResultadoVisita() {
        return this.selectedTramiteParaResultadoVisita;
    }

    public void setSelectedTramiteParaResultadoVisita(Tramite selectedTramiteParaResultadoVisita) {
        this.selectedTramiteParaResultadoVisita = selectedTramiteParaResultadoVisita;
    }

    public List<Tramite> getTramitesParaCargueInfo() {
        return this.tramitesParaCargueInfo;
    }

    public void setTramitesParaCargueInfo(List<Tramite> tramitesParaCargueInfo) {
        this.tramitesParaCargueInfo = tramitesParaCargueInfo;
    }

    public Tramite getSelectedTramiteCargueInfo() {
        return this.selectedTramiteCargueInfo;
    }
//-----------------------------------------------

    /**
     * Como para dos de los botones (los de carga desde DMC o archivo) se admite selección múltiple,
     * pero para el de informe de visita no, se definen la variables que se usan en esa pantalla: el
     * trámite seleccionado y la comision-tramite
     *
     * @param selectedTramiteCargueInfo
     */
    public void setSelectedTramiteCargueInfo(Tramite selectedTramiteCargueInfo) {
        this.selectedTramiteCargueInfo = selectedTramiteCargueInfo;

        if (selectedTramiteCargueInfo != null) {

            this.selectedTramiteParaResultadoVisita = selectedTramiteCargueInfo;
            if (this.banderaDepuracion) {
                this.currentComisionTramite = this.selectedTramiteParaResultadoVisita.
                    getComisionTramiteDepuracion();
            } else {
                this.currentComisionTramite = this.selectedTramiteParaResultadoVisita.
                    getComisionTramite();
            }

            this.currentComisionTramite.setRealizoInspeccionOcular(ESiNo.SI.getCodigo());
        }
    }

    public String getCancelarTramiteDecision() {
        return this.cancelarTramiteDecision;
    }

    public void setCancelarTramiteDecision(String cancelarTramiteDecision) {
        this.cancelarTramiteDecision = cancelarTramiteDecision;
    }

    public String getMotivoCancelacion() {
        return this.motivoCancelacion;
    }

    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public List<Tramite> getTramiteTramites() {
        return this.tramiteTramites;
    }

    public void setTramiteTramites(List<Tramite> tramiteTramites) {
        this.tramiteTramites = tramiteTramites;
    }

    public Tramite getTramiteTramiteSeleccionado() {
        return this.tramiteTramiteSeleccionado;
    }

    public void setTramiteTramiteSeleccionado(Tramite tramiteTramiteSeleccionado) {
        this.tramiteTramiteSeleccionado = tramiteTramiteSeleccionado;
    }

    public List<TramitePredioEnglobe> getPrediosTramite() {
        return this.prediosTramite;
    }

    public void setPrediosTramite(List<TramitePredioEnglobe> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public boolean isSubtipoTramiteEsRequerido() {
        return this.subtipoTramiteEsRequerido;
    }

    public void setSubtipoTramiteEsRequerido(boolean subtipoTramiteEsRequerido) {
        this.subtipoTramiteEsRequerido = subtipoTramiteEsRequerido;
    }

    public List<SelectItem> getOrdenEjecucionTramites() {
        return this.ordenEjecucionTramites;
    }

    public void setOrdenEjecucionTramites(
        List<SelectItem> ordenEjecucionTramites) {
        this.ordenEjecucionTramites = ordenEjecucionTramites;
    }

    public List<Predio> getPrediosSeleccionadosMapa() {
        return this.consultaPredioMB.getPrediosSeleccionados2();
    }

    public boolean isBanderaEdicionTramiteTramite() {
        return this.banderaEdicionTramiteTramite;
    }

    public void setBanderaEdicionTramiteTramite(
        boolean banderaEdicionTramiteTramite) {
        this.banderaEdicionTramiteTramite = banderaEdicionTramiteTramite;
    }

    public String getNumeroPredialParte1() {
        return this.numeroPredialParte1;
    }

    public void setNumeroPredialParte1(String numeroPredialParte1) {
        this.numeroPredialParte1 = numeroPredialParte1;
    }

    public String getNumeroPredialParte10() {
        return this.numeroPredialParte10;
    }

    public void setNumeroPredialParte10(String numeroPredialParte10) {
        this.numeroPredialParte10 = numeroPredialParte10;
    }

    public String getNumeroPredialParte11() {
        return this.numeroPredialParte11;
    }

    public void setNumeroPredialParte11(String numeroPredialParte11) {
        this.numeroPredialParte11 = numeroPredialParte11;
    }

    public String getNumeroPredialParte12() {
        return this.numeroPredialParte12;
    }

    public void setNumeroPredialParte12(String numeroPredialParte12) {
        this.numeroPredialParte12 = numeroPredialParte12;
    }

    public String getNumeroPredialParte2() {
        return this.numeroPredialParte2;
    }

    public void setNumeroPredialParte2(String numeroPredialParte2) {
        this.numeroPredialParte2 = numeroPredialParte2;
    }

    public String getNumeroPredialParte3() {
        return numeroPredialParte3;
    }

    public void setNumeroPredialParte3(String numeroPredialParte3) {
        this.numeroPredialParte3 = numeroPredialParte3;
    }

    public String getNumeroPredialParte4() {
        return this.numeroPredialParte4;
    }

    public void setNumeroPredialParte4(String numeroPredialParte4) {
        this.numeroPredialParte4 = numeroPredialParte4;
    }

    public String getNumeroPredialParte5() {
        return numeroPredialParte5;
    }

    public void setNumeroPredialParte5(String numeroPredialParte5) {
        this.numeroPredialParte5 = numeroPredialParte5;
    }

    public String getNumeroPredialParte6() {
        return this.numeroPredialParte6;
    }

    public void setNumeroPredialParte6(String numeroPredialParte6) {
        this.numeroPredialParte6 = numeroPredialParte6;
    }

    public String getNumeroPredialParte7() {
        return this.numeroPredialParte7;
    }

    public void setNumeroPredialParte7(String numeroPredialParte7) {
        this.numeroPredialParte7 = numeroPredialParte7;
    }

    public String getNumeroPredialParte8() {
        return this.numeroPredialParte8;
    }

    public void setNumeroPredialParte8(String numeroPredialParte8) {
        this.numeroPredialParte8 = numeroPredialParte8;
    }

    public String getNumeroPredialParte9() {
        return this.numeroPredialParte9;
    }

    public void setNumeroPredialParte9(String numeroPredialParte9) {
        this.numeroPredialParte9 = numeroPredialParte9;
    }

    /**
     * @author pedro.garcia
     */
    public ArrayList<SelectItem> getTiposTramitePorSolicitud() {

        ArrayList<SelectItem> tiposDeTramite = null;
        List<TipoSolicitudTramite> tiposTramite = null;
        SelectItem itemToAdd;

        if (this.selectedTramiteCargueInfo != null &&
            this.selectedTramiteCargueInfo.getSolicitud() != null) {
            tiposTramite = this.getTramiteService().
                obtenerTiposDeTramitePorSolicitud(ESolicitudTipo.AVISOS.getCodigo());
        }
        if (tiposTramite != null && !tiposTramite.isEmpty()) {
            tiposDeTramite = new ArrayList<SelectItem>();
            for (TipoSolicitudTramite tst : tiposTramite) {
                if (this.selectedTramiteCargueInfo.isQuintaNuevo() &&
                    (tst.getTipoTramite().equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo()) ||
                    tst.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.getCodigo()))) {
                    //
                } else {
                    itemToAdd = new SelectItem(tst.getTipoTramite(),
                        tst.getTipoTramiteValor());
                    tiposDeTramite.add(itemToAdd);
                }
            }
        }
        this.tiposTramitePorSolicitud = tiposDeTramite;
        return this.tiposTramitePorSolicitud;
    }

    public void setTiposTramitePorSolicitud(ArrayList<SelectItem> tiposTramite) {
        this.tiposTramitePorSolicitud = tiposTramite;
    }

    public boolean isBanderaSubtipoEnglobe() {
        return this.banderaSubtipoEnglobe;
    }

    public void setBanderaSubtipoEnglobe(boolean banderaSubtipoEnglobe) {
        this.banderaSubtipoEnglobe = banderaSubtipoEnglobe;
    }

    public boolean isGuardadoFlag() {
        return this.guardadoFlag;
    }

    public void setGuardadoFlag(boolean guardadoFlag) {
        this.guardadoFlag = guardadoFlag;
    }

    public boolean isBanderaClaseDeMutacionSeleccionada() {
        return this.banderaClaseDeMutacionSeleccionada;
    }

    public void setBanderaClaseDeMutacionSeleccionada(
        boolean banderaClaseDeMutacionSeleccionada) {
        this.banderaClaseDeMutacionSeleccionada = banderaClaseDeMutacionSeleccionada;
    }

    public List<DatoRectificar> getSelectedDatosRectifComplDataTable() {
        return selectedDatosRectifComplDataTable;
    }

    public void setSelectedDatosRectifComplDataTable(
        List<DatoRectificar> selectedDatosRectifComplDataTable) {
        this.selectedDatosRectifComplDataTable = selectedDatosRectifComplDataTable;
    }

    public TramitePredioEnglobe getSelectedPredioTramite() {
        return this.selectedPredioTramite;
    }

    public void setSelectedPredioTramite(
        TramitePredioEnglobe selectedPredioTramite) {
        this.selectedPredioTramite = selectedPredioTramite;
    }

    public List<SelectItem> getSubtipoClaseMutacionV() {
        return this.subtipoClaseMutacionV;
    }

    public void setSubtipoClaseMutacionV(List<SelectItem> subtipoClaseMutacionV) {
        this.subtipoClaseMutacionV = subtipoClaseMutacionV;
    }

    public String getTipoDatoRectifComplSeleccionado() {
        return this.tipoDatoRectifComplSeleccionado;
    }

    public void setTipoDatoRectifComplSeleccionado(
        String tipoDatoRectifComplSeleccionado) {
        this.tipoDatoRectifComplSeleccionado = tipoDatoRectifComplSeleccionado;
    }

    public Predio getSelectedPredioColindante() {
        return selectedPredioColindante;
    }

    public void setSelectedPredioColindante(Predio selectedPredioColindante) {
        this.selectedPredioColindante = selectedPredioColindante;
    }

    public DualListModel<DatoRectificar> getDatosRectifComplDLModel() {

        if (this.tramiteTramiteSeleccionado != null &&
            (this.tramiteTramiteSeleccionado.isRectificacion() || this.tramiteTramiteSeleccionado.
            isEsComplementacion())) {
            this.datosRectifComplSource = new ArrayList<DatoRectificar>();
            this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
            String naturaleza;

            if (this.tramiteTramiteSeleccionado != null &&
                this.tramiteTramiteSeleccionado.isRectificacion()) {
                naturaleza = EDatoRectificarNaturaleza.RECTIFICACION
                    .geCodigo();
            } else {
                naturaleza = EDatoRectificarNaturaleza.COMPLEMENTACION
                    .geCodigo();
            }

            if (this.tipoDatoRectifComplSeleccionado != null &&
                this.tipoDatoRectifComplSeleccionado.compareToIgnoreCase("") != 0 &&
                naturaleza != null && !naturaleza.isEmpty()) {
                this.datosRectifComplSource = this.getTramiteService()
                    .obtenerDatosRectificarPorTipoYNaturaleza(
                        this.tipoDatoRectifComplSeleccionado, naturaleza);
            }

            this.datosRectifComplDLModel = new DualListModel<DatoRectificar>(
                this.datosRectifComplSource, this.datosRectifComplTarget);
        }
        return this.datosRectifComplDLModel;
    }

    public void setDatosRectifComplDLModel(
        DualListModel<DatoRectificar> datosRectifComplDLModel) {
        this.datosRectifComplDLModel = datosRectifComplDLModel;
    }

    public DatoRectificar[] getSelectedDatosRectifComplDT() {
        return selectedDatosRectifComplDT;
    }

    public void setSelectedDatosRectifComplDT(
        DatoRectificar[] selectedDatosRectifComplDT) {
        this.selectedDatosRectifComplDT = selectedDatosRectifComplDT;
    }

    public Tramite getTramiteSeleccionado() {
        this.tramiteSeleccionado = this.tramiteTramiteSeleccionado;
        return this.tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
        this.tramiteTramiteSeleccionado = this.tramiteSeleccionado;
    }

    public boolean isBanderaVerTramitePorAsociar() {
        return banderaVerTramitePorAsociar;
    }

    public void setBanderaVerTramitePorAsociar(boolean banderaVerTramitePorAsociar) {
        this.banderaVerTramitePorAsociar = banderaVerTramitePorAsociar;
    }

    public boolean isBanderaSubtipoNuevo() {
        return banderaSubtipoNuevo;
    }

    public void setBanderaSubtipoNuevo(boolean banderaSubtipoNuevo) {
        this.banderaSubtipoNuevo = banderaSubtipoNuevo;
    }

    public ReporteDTO getReporteMemorandoComision() {
        return reporteMemorandoComision;
    }

    public void setReporteMemorandoComision(ReporteDTO reporteMemorandoComision) {
        this.reporteMemorandoComision = reporteMemorandoComision;
    }

    public ComisionTramite getSelectedComisionTramite() {
        return this.selectedComisionTramite;
    }

    public void setSelectedComisionTramite(ComisionTramite selectedComisionTramite) {
        this.selectedComisionTramite = selectedComisionTramite;
    }

    public boolean isBanderaExpanderTabla() {
        return banderaExpanderTabla;
    }

    public void setBanderaExpanderTabla(boolean banderaExpanderTabla) {
        this.banderaExpanderTabla = banderaExpanderTabla;
    }

    public List<Predio> getListaPrediosColindantes() {
        return listaPrediosColindantes;
    }

    public void setListaPrediosColindantes(List<Predio> listaPrediosColindantes) {
        this.listaPrediosColindantes = listaPrediosColindantes;
    }

    public List<TramitePredioEnglobe> getPrediosOriginalesList() {
        return prediosOriginalesList;
    }

    public void setPrediosOriginalesList(List<TramitePredioEnglobe> prediosOriginalesList) {
        this.prediosOriginalesList = prediosOriginalesList;
    }

    public boolean isHabilitarCancelarTramite() {
        return habilitarCancelarTramite;
    }

    public void setHabilitarCancelarTramite(boolean habilitarCancelarTramite) {
        this.habilitarCancelarTramite = habilitarCancelarTramite;
    }

    public boolean isRealizoInspeccionOcular() {
        return realizoInspeccionOcular;
    }

    public void setRealizoInspeccionOcular(boolean realizoInspeccionOcular) {
        this.realizoInspeccionOcular = realizoInspeccionOcular;
    }

    public boolean isHabilitarTramitesAdicionales() {
        return habilitarTramitesAdicionales;
    }

    public void setHabilitarTramitesAdicionales(boolean habilitarTramitesAdicionales) {
        this.habilitarTramitesAdicionales = habilitarTramitesAdicionales;
    }

    public List<SelectItem> getMutacionClases() {
        List<SelectItem> mutClass = this.generalMB.getMutacionClase();
        this.mutacionClases = new LinkedList<SelectItem>();

        if (mutClass != null && !mutClass.isEmpty()) {
            for (SelectItem s : mutClass) {
                if (!s.getLabel().equals(EMutacionClase.CUARTA.getNombre()) &&
                    !s.getLabel().equals(EMutacionClase.QUINTA.getNombre())) {

                    if (s.getLabel().equals(EMutacionClase.PRIMERA.getNombre())) {
                        if (!this.selectedTramiteCargueInfo.isQuintaNuevo()) {
                            this.mutacionClases.add(s);
                        }
                    } else if (s.getLabel().equals(EMutacionClase.TERCERA.getNombre())) {
                        if ((this.selectedTramiteCargueInfo.isTercera() ||
                            this.selectedTramiteCargueInfo.isQuintaNuevo()) &&
                            this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.NO.getCodigo())) {
                            //
                        } else {
                            this.mutacionClases.add(s);
                        }
                    } else if (s.getLabel().equals(EMutacionClase.SEGUNDA.getNombre())) {
                        if (this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.NO.getCodigo()) &&
                            this.selectedTramiteCargueInfo.isQuintaNuevo()) {
                            // 
                        } else {
                            this.mutacionClases.add(s);
                        }
                    } else {
                        this.mutacionClases.add(s);
                    }
                }

            }
        }

        return this.mutacionClases;
    }

    public void setMutacionClases(List<SelectItem> mutacionClases) {
        this.mutacionClases = mutacionClases;
    }

    public boolean isBanderaPredioFiscal() {
        return banderaPredioFiscal;
    }

    public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
        this.banderaPredioFiscal = banderaPredioFiscal;
    }
//--------------------------------------------------------------------------------------------------    

    public void generarMemoComision() {

        Documento documentoMemorandoComision;
        String idDocEnGestorDocumental;

        documentoMemorandoComision = this.buscarDocumentoMemorandoComision();
        if (documentoMemorandoComision == null) {
            this.addMensajeWarn(
                "No se encontró el memorando comisión, por favor verifique que ya se haya generado y radicado");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        idDocEnGestorDocumental = documentoMemorandoComision.getIdRepositorioDocumentos();

        this.reporteMemorandoComision = this.reportsService.
            consultarReporteDeGestorDocumental(idDocEnGestorDocumental);

        if (this.reporteMemorandoComision == null) {
            this.addMensajeError(Constantes.MENSAJE_ERROR_REPORTE);
        }

    }
// ----------------------------------------------------- //

    /**
     * Método que retorna un documento del memorando de comisión asociado a la comisión
     *
     * @return TramiteDocumento
     * @author javier.aponte
     */
    private Documento buscarDocumentoMemorandoComision() {

        Documento memorandoComision = null;

        if (this.currentComisionTramite.getComision().getMemorandoDocumentoId() != null) {
            memorandoComision = this.getGeneralesService().buscarDocumentoPorId(
                this.currentComisionTramite.getComision().getMemorandoDocumentoId());
        }

        return memorandoComision;
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {

        LOGGER.debug("on CargueInfoAlSistemaMB#init ");
        //this.currentTramite = this.getTramiteService().consultarDocumentacionDeTramite(this.selectedTramiteId);

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "cargueInfoAlSistema");

        long[] idsTramitesTareas =
            this.tareasPendientesMB.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        Actividad act = this.tareasPendientesMB.buscarActividadPorIdTramite(idsTramitesTareas[0]);
        String nombreActividad;

        if (act.getNombre().equals(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_AL_SNC)) {
            nombreActividad = EComisionTipo.CONSERVACION.toString();
        } else {
            nombreActividad = EComisionTipo.DEPURACION.toString();
        }

        //D: buscar los trámites
        this.tramitesParaCargueInfo =
            this.getTramiteService().
                buscarTramitesParaCargueInfo(idsTramitesTareas, nombreActividad);

        if (this.tramitesParaCargueInfo == null || this.tramitesParaCargueInfo.isEmpty()) {
            LOGGER.warn("La consulta de trámites resultó nula o vacía, lo cual es un error.");

        } else {

            for (Tramite t : this.tramitesParaCargueInfo) {
                List<TramiteDocumento> documentosPruebas = this.getTramiteService()
                    .obtenerTramiteDocumentosDePruebasPorTramiteId(
                        t.getId());
                if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                    t.setDocumentosPruebas(documentosPruebas);
                }

                //consultar la ultima razón de rechazo depuración
                TramiteDepuracion ultimoTramiteDepuracion = this.getTramiteService().
                    buscarUltimoTramiteDepuracionPorIdTramite(t.getId());
                if (ultimoTramiteDepuracion != null) {
                    String razonesRechazoDepuracion = ultimoTramiteDepuracion.getRazonesRechazo();
                    if (razonesRechazoDepuracion != null && !razonesRechazoDepuracion.isEmpty()) {
                        t.setUltimaRazonRechazoDepuracion(razonesRechazoDepuracion);
                    }

                }

            }

            //Se determina si es una actividad de depuracion
            if (this.tramitesParaCargueInfo.get(0).getComisionTramiteDepuracion() != null) {
                this.banderaDepuracion = true;
            } else {
                this.banderaDepuracion = false;
                this.banderaTramitesAsociadosDepuracion = false;
            }

        }

        this.hayNuevosTramites = ESiNo.NO.getCodigo();

        this.banderaVerTramitePorAsociar = false;
        this.banderaExpanderTabla = false;
        this.cancelarTramiteDecision = ESiNo.NO.getCodigo();

        this.tramiteTramites = new ArrayList<Tramite>(0);

        this.guardadoFlag = false;

        this.tramiteDepuracionInfo = null;
        this.banderaNecesitaDepuracion = false;
        this.habilitarCancelarTramite = true;
        this.realizoInspeccionOcular = true;
        this.habilitarTramitesAdicionales = true;
        this.banderaTramiteEspacial = false;
        this.showBotonInformarResultadoVisita = false;
        this.banderaPredioFiscal = true;

    }

//--------  methods implemented from interface
    /**
     * No hay que validar nada
     *
     * @return
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }
//-----------------------------------------------

    /**
     * Hay dos actividades en el proceso que son iguales. Las posibles transiciones dependen del
     * tipo de trámite o de la clasificación del mismo: si es de revisión de avalúo o de autoavalúo,
     * la única posible transición es a la actividad Asociar documento al nuevo trámite; si el
     * trámite es de terreno, puede irse a esa misma actividad o a Modificar información
     * alfanumérica.
     *
     *
     * @author pedro.garcia
     */
    /*
     * @modified felipe.cadena --Se agrega las condiciones de transicion cuando la actividad es del
     * subproceso de depuración.
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        Actividad activity;
        ActivityMessageDTO messageDTO;
        String observaciones, processTransition;
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();

        Tramite tramSelected;

        tramSelected = this.selectedTramiteCargueInfo;

        if (this.banderaDepuracion) {
            if (this.nuevosTramitesHabilitado) {
                processTransition = ProcesoDeConservacion.ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES;
                usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(), ERol.COORDINADOR);

                // En caso de que se hayan asociado nuevos trámites se debe guardar
                // la actividad donde se asociaron en depuración, para poder
                // distinguir estos trámites en la actividad de 'Aprobar nuevos
                // trámites', e identificar cuales trámites son provenientes de
                // depuración y cuales de conservación.
                if (this.tramiteTramites != null &&
                    this.tramiteTramites.isEmpty()) {
                    solicitudCatastral
                        .setObservaciones(this.currentProcessActivity
                            .getNombre());
                }
            } else {
//                this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(activity, this.usuario);
//                return;
                processTransition =
                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR;
                usuariosActividad.add(this.usuario);
            }
        } else {
            if (tramSelected.getTipoTramite().equalsIgnoreCase(
                ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo()) ||
                tramSelected.getTipoTramite().equalsIgnoreCase(
                    ETramiteTipoTramite.AUTOESTIMACION.getCodigo()) ||
                tramSelected.isCuarta()) {

                if (this.currentComisionTramite.getRealizoInspeccionOcular().equals(
                    ESiNo.NO.getCodigo())) {
                    processTransition = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
                    usuariosActividad = this.getTramiteService().
                        buscarFuncionariosPorRolYTerritorial(
                            this.usuario.getDescripcionEstructuraOrganizacional(),
                            ERol.RESPONSABLE_CONSERVACION);
                } else {
                    if (this.nuevosTramitesHabilitado) {
                        // Ejecutor
                        processTransition =
                            ProcesoDeConservacion.ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE;
                        usuariosActividad.add(this.usuario);
                    } else {
                        processTransition =
                            ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO;
                        usuariosActividad.add(this.usuario);
                    }
                }
            } else {
                if (this.currentComisionTramite.getRealizoInspeccionOcular().equals(
                    ESiNo.NO.getCodigo())) {

                    if (tramSelected.isRevisionAvaluo() || tramSelected.isCuarta()) {
                        processTransition = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;
                    } else {
                        processTransition = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
                    }

                    usuariosActividad = this.getTramiteService().
                        buscarFuncionariosPorRolYTerritorial(
                            this.usuario.getDescripcionEstructuraOrganizacional(),
                            ERol.RESPONSABLE_CONSERVACION);
                } else {
                    if (this.nuevosTramitesHabilitado) {
                        // Ejecutor
                        processTransition =
                            ProcesoDeConservacion.ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE;
                        usuariosActividad.add(this.usuario);
                    } else {
                        processTransition =
                            ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA;
                        usuariosActividad.add(this.usuario);
                    }
                }
            }
        }

        messageDTO = new ActivityMessageDTO();

        activity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.selectedTramiteParaResultadoVisita.getId());
        messageDTO.setActivityId(activity.getId());
        observaciones = "El trámite sale de la actividad Cargar al SNC.";
        messageDTO.setComment(observaciones);
        messageDTO.setUsuarioActual(this.usuario);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setUsuarios(usuariosActividad);

        messageDTO.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(messageDTO);

    }

//-----------------------------------------------
    /**
     * Hasta este punto se actualiza el estado de la comisión a 'finalizada'. Si se hiciera antes no
     * se podría modificar el informe de la visita. Se actualiza el trámite para que figure como 'no
     * comisionado'. Si se decide cancelar el trámite, se actualiza el registro de TramiteEstado
     * correspondiente. Según se definió, el trámite queda en el estado "por cancelar"
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        boolean comisionActualizada, tramiteActualizado, debeActualizarEstadoComision;

        this.dbStateUpdateOK = true;

        Comision comision = this.currentComisionTramite.getComision();

        this.selectedTramiteCargueInfo.setComisionado(ESiNo.NO.getCodigo());
        tramiteActualizado = this.getTramiteService().actualizarTramite2(
            this.selectedTramiteCargueInfo, this.usuario);

        if (tramiteActualizado) {

            //D: se revisa si se debe cambiar el estado de la comisión. Si sí, se actualiza la comisión
            //  y se crea el respectivo registro en Comision_Estado
            debeActualizarEstadoComision = this.validarDebeActualizarEC();
            if (debeActualizarEstadoComision) {
                boolean comisionEstadoActualizado;
                ComisionEstado comisionEstado = new ComisionEstado();

                comisionEstado.setUsuarioLog(this.usuario.getLogin());
                comisionEstado.setComision(comision);
                comisionEstado.setFecha(new Date());
                comisionEstado.setFechaLog(new Date());
                comisionEstado.setEstado(EComisionEstado.FINALIZADA.getCodigo());
                comisionEstado.setMotivo(EComisionEstadoMotivo.COMISION_FINALIZADA.getCodigo());

                comisionEstadoActualizado = this.getTramiteService().
                    guardarActualizarComisionEstado2(comisionEstado);

                if (comisionEstadoActualizado) {
                    comision.setEstado(EComisionEstado.FINALIZADA.getCodigo());

                    comisionActualizada = this.getTramiteService().actualizarComision2(this.usuario,
                        comision);

                    if (!comisionActualizada) {
                        LOGGER.warn("No se pudo actualizar la comisión con id " + comision.getId());
                        this.dbStateUpdateOK = false;
                    }
                } else {
                    LOGGER.warn("No se pudo guardar el registro de ComisionEstado");
                }
            }

        } else {
            LOGGER.warn("No se pudo actualizar el trámite con id " +
                this.selectedTramiteCargueInfo.getId());
            this.dbStateUpdateOK = false;
        }

    }
//-----------------------------------------------

    /**
     * en el caso en que el llamado al BPM se debe hacer desde un botón que no está definido en
     * alguno de los template de procesos (porque por ejemplo hay al mismo tiempo dos botones que
     * mueven la actividad a diferentes transiciones), se debe hacer el llamado a los métodos como
     * si se estuviera implementando alguno de los métodos que avanzan el proceso del ProcessMB
     *
     * @author pedro.garcia
     */
    private boolean forwardThisProcess() {

        boolean answer = false;

        if (this.tramitesParaCargueInfo != null) {
            if (this.validateProcess()) {
                this.setupProcessMessage();
                this.doDatabaseStatesUpdate();

                if (this.dbStateUpdateOK) {
                    super.forwardProcess();
                    answer = true;
                }
            }
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón con el mismo nombre en la pantalla de resultado de la visita. Se hace para
     * un solo trámite.
     *
     * 1. validaciones 2. actualizaciones del trámite (incluye la inserción de los nuevos trámites
     * asociados)
     */
    /*
     * @modified juanfelipe.garcia :: 12-07-2013 :: se adiciono condición para cancelar, se requiere
     * minimo un nuevo trámite asociado
     *
     * @modify by juanfelipe.garcia :: 08-10-2013 :: Adición de validaciones para tramites nuevos
     * (#5667)
     */
    public void guardarResultadoVisita() {

        boolean okOrdenTramites;
        //D: 0
        //validación adicional si se arrepiente de cancelar el trámite original y ya tiene tramites asociados creados
        if (this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.NO.getCodigo()) &&
            !this.tramiteTramites.isEmpty()) {
            List<Tramite> tramitesAuxList = new ArrayList<Tramite>();
            boolean tramitesValidos = true;
            for (Tramite tram : this.tramiteTramites) {
                if (this.selectedTramiteCargueInfo.isTercera() && tram.isTercera()) {
                    tramitesValidos = false;
                } else if (this.selectedTramiteCargueInfo.isQuintaNuevo() && (tram.isTercera() ||
                    tram.isSegunda())) {
                    tramitesValidos = false;
                } else {
                    tramitesAuxList.add(tram);
                }
            }
            this.tramiteTramites.clear();
            this.tramiteTramites.addAll(tramitesAuxList);

            if (!tramitesValidos) {
                this.addMensajeError(
                    "Algunos trámites adicionales fueron eliminados, no son válidos para el trámite original.");
                return;
            }

        }

        //D: 1
        // validar que si se cancela haya un motivo y que se asocie un nuevo tramite
        if (this.cancelarTramiteDecision.equalsIgnoreCase(ESiNo.SI.getCodigo())) {
            if (this.motivoCancelacion == null ||
                this.motivoCancelacion.equalsIgnoreCase("")) {
                this.addMensajeError(
                    "Si decide cancelar el trámite, se debe dar un motivo de la cancelación.");
                return;
            }
            if (this.tramiteTramites == null || this.tramiteTramites.isEmpty()) {
                this.addMensajeError(
                    "Si decide cancelar el trámite, se debe adicionar al menos un tramite nuevo.");
                return;
            }
        }
        //Validar que si Selecciono la opción de trámite nuevo debe adicionar al menos uno
        if (hayNuevosTramites.equalsIgnoreCase(ESiNo.SI.getCodigo()) &&
            (this.tramiteTramites == null ||
            this.tramiteTramites.isEmpty())) {
            this.addMensajeError(
                "Selecciono la opción de tramite nuevo, pero no adiciono un tramite.");
            return;
        }

        // validar que el órden de ejecución de los trámites sea diferente para todos ellos
        if (this.nuevosTramitesHabilitado) {
            okOrdenTramites = this.validarOrdenTramitesNuevos();
            if (!okOrdenTramites) {
                this.addMensajeError(
                    "El orden de ejecución de los trámites debe ser diferente para todos ellos");
                return;
            }
        }

        //D: 2
        // asignar la ComisionTramite modificada
        if (this.tramiteTramites.isEmpty()) {

            Comision comision = this.currentComisionTramite.getComision();

            //D: la visita del trámite se considera realizada aunque no haya podido realizar inspección ocular
            this.currentComisionTramite.setRealizada(ESiNo.SI.getCodigo());
            this.currentComisionTramite.setTramitesNuevosAdicionales(this.hayNuevosTramites);

            //N: al hacer esto se pierden los objetos a los que se les hizo fetch, por eso se usa el truquini
            // de volver asignar la comisión al objeto actualizado.
            this.currentComisionTramite = this.getTramiteService().
                actualizarComisionTramite(this.currentComisionTramite);
            this.currentComisionTramite.setComision(comision);
        }

        this.selectedTramiteCargueInfo.setComisionTramite(this.currentComisionTramite);

        if (!this.tramiteTramites.isEmpty()) {
            for (Tramite t : this.tramiteTramites) {
                t.setFechaLog(new Date());
                t.setUsuarioLog(this.usuario.getLogin());
            }

            this.tramiteTramites = this.getTramiteService().actualizarTramitesAdicionales(
                this.tramiteTramites);

            String observaciones =
                "Marcado como por aprobar en la actividad cargar al SNC, pero aún no en firme";
            for (Tramite tram : this.tramiteTramites) {
                this.guardarTramiteEstado(tram, ETramiteEstado.POR_APROBAR.getCodigo(),
                    observaciones);
            }

            List<Long> idsTramitesAsociados = this.getTramiteService().
                obtenerIdsDeTramitesAsociadosPorTramiteId(
                    this.selectedTramiteCargueInfo.getId());
            for (Tramite tram : this.tramiteTramites) {
                if (!idsTramitesAsociados.contains(tram.getId())) {
                    this.selectedTramiteCargueInfo.getTramites().add(tram);
                }
            }
        }

        this.selectedTramiteCargueInfo.setTramiteDocumentos(this.getTramiteService().
            obtenerTramiteDocumentosPorTramiteId(this.selectedTramiteCargueInfo.getId()));

        if (this.prediosOriginalesList != null) {
            this.selectedTramiteCargueInfo.setTramitePredioEnglobes(this.prediosOriginalesList);
        }

        this.getTramiteService().actualizarTramite(this.selectedTramiteCargueInfo);

        this.addMensajeInfo("El resultado de la visita fue guardado satisfactoriamente.");
        this.guardadoFlag = true;
        this.habilitarCancelarTramite = false;
    }

    public void valueChangeTramitesNuevos(ValueChangeEvent e) {
        if (hayNuevosTramites.equalsIgnoreCase(ESiNo.NO.getCodigo())) {
            this.guardadoFlag = false;
        } else if (hayNuevosTramites.equalsIgnoreCase(ESiNo.SI.getCodigo()) &&
            this.tramiteTramites != null &&
            !this.tramiteTramites.isEmpty()) {
            this.guardadoFlag = true;
        }

    }

    public void seleccionaInspeccionOcular() {
        if (this.currentComisionTramite.getRealizoInspeccionOcular().equals(ESiNo.SI.getCodigo())) {
            this.realizoInspeccionOcular = true;
        } else {
            this.realizoInspeccionOcular = false;
            this.cancelarTramiteDecision = ESiNo.NO.getCodigo();
            this.hayNuevosTramites = ESiNo.NO.getCodigo();
        }

    }

    public void seleccionaCancelarTramite() {
        if (this.selectedTramiteCargueInfo.isQuintaNuevo()) {
            if (this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo())) {
                this.habilitarTramitesAdicionales = true;
            } else {
                this.habilitarTramitesAdicionales = false;
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la selección de predios en mutaciones de segunda englobe para buscar
     * la lista de predios colindantes
     */
    public void buscarPrediosColindantes() {

        if (this.banderaSubtipoEnglobe) {
            this.listaPrediosColindantes = this.getConservacionService()
                .buscarPrediosColindantesPorNumeroPredial(
                    this.selectedPredioTramite.getPredio()
                        .getNumeroPredial());
        }

    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado sobre la deselección de predios en mutaciones de segunda englobe para
     * inicializar la lista de predios colindantes y selección
     */
    public void reiniciarPrediosColindantes() {
        this.listaPrediosColindantes = null;
    }

    // -------------------------------------------------------------------------
    /**
     * Método utilizado sobre la selección de predio colindante para adicionarlo a la lista de
     * predios seleccionados del trámite
     */
    public void adicionarPrediosColindantes() {
        List<Predio> predios = this.getPrediosSeleccionadosMapa();

        predios.add(this.selectedPredioColindante);

        for (Predio p : predios) {
            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            tpe.setPredio(p);
            this.prediosTramite.add(tpe);
        }
        //se valida que no tenga predios repetidos
        HashSet hs = new HashSet();
        hs.addAll(predios);
        predios.clear();
        predios.addAll(hs);

        if (this.tramiteTramiteSeleccionado != null) {
            if (predios != null && !predios.isEmpty()) {
                if (predios.size() >= 1) {

                    this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(this.prediosTramite);
                    this.tramiteTramiteSeleccionado.setPredio(predios.get(0));
                    //Para el caso de adicionar tramite nuevo es necesario que en la validacion tenga el dato actualizado del nuevo tramite
                    try {
                        this.validacionTramiteMB.setNuevoTramiteSeleccionado(
                            tramiteTramiteSeleccionado);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }

            }
        }
        this.listaPrediosColindantes.remove(this.selectedPredioColindante);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "sí" que confirma el avance del proceso del trámite seleccionado.
     *
     * @author pedro.garcia
     * @return la constante que indica la página de inicio
     */
    public String avanzarProcesoTramiteComision() {

        boolean okAvance;

        okAvance = this.forwardThisProcess();

        if (okAvance) {
            //OJO: hay necesidad de hacer esto aquí porque se no se usa el template que llama a ProcessMB.forwardProcess
            //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
            UtilidadesWeb.removerManagedBean("cargueInfoAlSistema");

            //D: forzar el refrezco del árbol de tareas
            this.tareasPendientesMB.init();

            return ConstantesNavegacionWeb.INDEX;
        } else {
            return null;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Informar resultado de la visita". Se consulta el trámite con todos los
     * datos que se necesitan para poder adicionar un trámite asociado a este
     */
    /*
     * @modified juanfelipe.garcia :: 08-10-2013 :: Ajuste para limpiar datos cuando se cambia de
     * trámite inicial y cargue de predios iniciales
     */
    public void consultarTramiteCompleto() {

        this.selectedTramiteCargueInfo = this.getTramiteService().
            findTramitePruebasByTramiteId(this.selectedTramiteParaResultadoVisita.getId());
        if (this.selectedTramiteCargueInfo.getTramitePredioEnglobes() != null) {
            this.prediosOriginalesList = new ArrayList<TramitePredioEnglobe>(
                this.selectedTramiteCargueInfo.getTramitePredioEnglobes());
        }

        //limpíar datos iniciales
        this.tramiteTramites = new ArrayList<Tramite>();
        this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
        this.hayNuevosTramites = ESiNo.NO.getCodigo();

        if (this.selectedTramiteCargueInfo.getEstado().equals(ETramiteEstado.POR_CANCELAR.
            getCodigo())) {
            this.cancelarTramiteDecision = ESiNo.SI.getCodigo();
            this.habilitarCancelarTramite = false;
        } else {
            this.cancelarTramiteDecision = ESiNo.NO.getCodigo();
        }

        if (this.selectedTramiteCargueInfo.isQuintaNuevo()) {
            this.habilitarTramitesAdicionales = false;
        }

        if (this.selectedTramiteCargueInfo.getTramites() != null &&
            !this.selectedTramiteCargueInfo.getTramites().isEmpty()) {
            this.hayNuevosTramites = ESiNo.SI.getCodigo();

            this.tramiteTramites = this.getTramiteService()
                .getTramiteTramitesbyTramiteId(
                    this.selectedTramiteCargueInfo.getId());

            this.ordenEjecucionTramites = new LinkedList<SelectItem>();
            for (Tramite tram : this.tramiteTramites) {
                this.ordenEjecucionTramites.add(
                    new SelectItem(
                        tram.getOrdenEjecucion(), tram.getOrdenEjecucion().toString()));
            }
        }

        if (this.banderaDepuracion) {
            if (this.selectedTramiteCargueInfo.getTramite() != null) {
                this.banderaTramitesAsociadosDepuracion = true;
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    public String cerrar() {
        this.tareasPendientesMB.init();
        return "index";
    }

    public boolean validarOrdenTramitesNuevos() {

        if (this.tramiteTramites.size() == 1) {
            return true;
        }

        int tramitesAdicionales = this.tramiteTramites.size();
        int contadorTemporal = 0;

        for (int orden = 1; orden <= tramitesAdicionales; orden++) {
            for (Tramite tramite : this.tramiteTramites) {
                if (tramite.getOrdenEjecucion() == orden) {
                    contadorTemporal++;
                    break;
                }
            }
        }

        if (contadorTemporal == tramitesAdicionales) {
            return true;
        } else {
            return false;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que elimina el trámite seleccionado de la lista de trámites asociados al trámite
     * seleccionado
     */
    public void removerTramiteDeTramiteTramites() {

        if (this.tramiteTramiteSeleccionado != null) {
            if (this.tramiteTramiteSeleccionado.getId() != null) {
                this.getTramiteService().removerTramite(this.tramiteTramiteSeleccionado);
            }
            this.ordenEjecucionTramites.remove(this.tramiteTramiteSeleccionado.getOrdenEjecucion());
            this.tramiteTramites.remove(this.tramiteTramiteSeleccionado);
            this.recalcularOrdenesDeEjecucion();
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método que recalcula los ordenes de ejecucion
     */
    public void recalcularOrdenesDeEjecucion() {
        if (this.ordenEjecucionTramites.size() > 1) {
            int ordenMax = this.ordenEjecucionTramites.size();
            this.ordenEjecucionTramites = new ArrayList<SelectItem>();
            for (int i = 1; i < ordenMax; i++) {
                this.ordenEjecucionTramites.add(new SelectItem(i, "" + i));
            }
        } else if (this.ordenEjecucionTramites.size() == 1) {
            this.ordenEjecucionTramites = new ArrayList<SelectItem>();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action ejecutada sobre el botón "adicionar predios al trámite" en la página de selección
     * predios
     */
    public void adicionarPrediosAlTramite() {
        List<Predio> predios = this.getPrediosSeleccionadosMapa();
        // this.prediosSeleccionadosMapa = predios;
        if (this.prediosTramite == null) {
            this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
        }
        boolean existePredioBloqueado = false;
        String stNumeroPredialBloqueado = "";
        for (Predio p : predios) {
            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            List<PredioBloqueo> predioBloqueos = this.getConservacionService().
                obtenerPredioBloqueosPorNumeroPredial(p.getNumeroPredial());
            p.setPredioBloqueos(predioBloqueos);
            if (p.isEstaBloqueado()) {
                existePredioBloqueado = true;
                stNumeroPredialBloqueado = p.getNumeroPredial();
                break;
            }
            tpe.setPredio(p);
            this.prediosTramite.add(tpe);
        }
        //se valida que no tenga predios repetidos
        HashSet hs = new HashSet();
        hs.addAll(predios);
        predios.clear();
        predios.addAll(hs);

        if (tramiteTramiteSeleccionado != null && !existePredioBloqueado) {
            if (predios != null && !predios.isEmpty()) {
                if (predios.size() > 1 || this.prediosTramite.size() > 1) {
                    this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(this.prediosTramite);
                    this.tramiteTramiteSeleccionado.setPredio(predios.get(0));
                } else {
                    this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(null);
                    this.tramiteTramiteSeleccionado.setPredio(predios.get(0));
                }
            }
        }

        if (existePredioBloqueado) {
            this.addMensajeError("El predio " + stNumeroPredialBloqueado +
                " está bloqueado y no puede ser seleccionado para la rectificación");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado sobre el boton adicionar trámite nuevo en tramites asociados
     */
    /*
     * @modified juanfelipe.garcia :: 12-07-2013 :: adición de predio al tramiteTramiteSeleccionado
     */
    public void adicionarTramiteNuevo() {

        this.subtipoTramiteEsRequerido = false;

        this.tramiteTramiteSeleccionado = new Tramite();
        this.banderaClaseDeMutacionSeleccionada = false;
        this.banderaSubtipoEnglobe = false;
        this.tramiteTramiteSeleccionado.setDepartamento(this.selectedTramiteCargueInfo.
            getDepartamento());
        this.tramiteTramiteSeleccionado.setMunicipio(this.selectedTramiteCargueInfo.getMunicipio());
        // Para los nuevos trámites asociados la fuente debe ser de oficio.
        this.tramiteTramiteSeleccionado.setFuente(ETramiteFuente.DE_OFICIO.getCodigo());

        // despues de mutaciones de 5ª
        this.numeroPredialParte1 = this.tramiteTramiteSeleccionado.getDepartamento().getCodigo();
        this.numeroPredialParte2 = this.tramiteTramiteSeleccionado.getMunicipio().getCodigo().
            substring(2);
        this.numeroPredialParte3 = "";
        this.numeroPredialParte4 = "";
        this.numeroPredialParte5 = "";
        this.numeroPredialParte6 = "";
        this.numeroPredialParte7 = "";
        this.numeroPredialParte8 = "";
        this.numeroPredialParte9 = "";
        this.numeroPredialParte10 = "";
        this.numeroPredialParte11 = "";
        this.numeroPredialParte12 = "";

        this.banderaEdicionTramiteTramite = false;
        this.tramiteTramiteSeleccionado.setEstado(ETramiteEstado.POR_APROBAR.getCodigo());

        this.selectedTramiteCargueInfo = this.getTramiteService().
            findTramitePruebasByTramiteId(this.selectedTramiteParaResultadoVisita.getId());

        this.tramiteTramiteSeleccionado.setPredio(this.selectedTramiteCargueInfo.getPredio());
        // @modified by leidy.gonzalez::#15855::06/04/2016::Se agrega valor NO a TramitePredioEnglobe 
        // temporal para que sea almacenado en la Base de Datos
        try {
            if (this.selectedTramiteCargueInfo.getTramitePredioEnglobes() != null &&
                !this.selectedTramiteCargueInfo.getTramitePredioEnglobes().isEmpty()) {
                this.prediosTramite = this.selectedTramiteCargueInfo.getTramitePredioEnglobes();
            } else {
                this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
                if (!this.selectedTramiteCargueInfo.isQuinta()) {
                    TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                    tpe.setFechaLog(new Date());
                    tpe.setPredio(this.selectedTramiteCargueInfo.getPredio());
                    tpe.setUsuarioLog(this.usuario.getLogin());
                    tpe.setTramite(this.selectedTramiteCargueInfo);
                    tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                    tpe = this.getTramiteService().guardarYactualizarTramitePredioEnglobe(tpe);

                    this.prediosTramite.add(tpe);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("No se pudieron obtener los predios del tramite seleccionado: " + ex.
                getMessage(), ex);
            this.prediosTramite = new ArrayList<TramitePredioEnglobe>();
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al cambiar el tipo de trámite
     *
     * @author juan.agudelo
     *
     */
    public void cambioTipoTramite() {
        this.subtipoTramiteEsRequerido = false;
        this.banderaClaseDeMutacionSeleccionada = false;
        this.tramiteTramiteSeleccionado.setSubtipo(null);
        this.tramiteTramiteSeleccionado.setTramiteRectificacions(null);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado sobre el boton guardar trámite nuevo
     */
    /*
     * @modify by juanfelipe.garcia :: 08-10-2013 :: Adición de validaciones para tramites nuevos
     * (#5667)
     */
    public void guardarTramiteNuevo() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.cancelarTramiteDecision != null &&
            !this.cancelarTramiteDecision.isEmpty() &&
            this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo())) {

            //D: si se decide cancelar el trámite se debe crear un registro de cambio de estado
            String observaciones =
                "Marcado como cancelado en la actividad cargar al SNC, pero aún no en firme";
            this.guardarTramiteEstado(selectedTramiteCargueInfo, ETramiteEstado.POR_CANCELAR.
                getCodigo(), observaciones);

        }

        boolean cancelarTramiteInicial;
        cancelarTramiteInicial = this.validarAsociarTramitesNuevos();

        if (cancelarTramiteInicial) {

            this.establecerDatosPreliminaresTramiteNuevo();

            // Validar el contenido del trámite
            boolean validacion = this.validacionTramiteMB.validarDatosTramite();

            boolean bTienePrediosBloqueados = tienePrediosBloqueados();

            validacion = !bTienePrediosBloqueados;
            if (bTienePrediosBloqueados) {
                this.addMensajeError("Hay predios que estáan bloqueado y no " +
                    "pueden ser seleccionados para este trámite");
            }

            if (validacion) {

                this.tramiteTramiteSeleccionado.setOrdenEjecucion(1);

                if (this.ordenEjecucionTramites == null ||
                    this.ordenEjecucionTramites.isEmpty()) {
                    this.ordenEjecucionTramites = new ArrayList<SelectItem>();
                    this.ordenEjecucionTramites.add(new SelectItem(1));
                } else {
                    boolean ordenExist = false;
                    for (SelectItem s : this.ordenEjecucionTramites) {
                        if (s.getValue().equals(this.tramiteTramites.size() + 1)) {
                            ordenExist = true;
                        }
                    }
                    if (!ordenExist) {
                        this.ordenEjecucionTramites.add(new SelectItem(
                            this.tramiteTramites.size() + 1,
                            "" + (this.tramiteTramites.size() + 1)));
                    }
                }

                if (this.tramiteTramiteSeleccionado.isSegundaEnglobe()) {
                    Tramite tempTramiteInicial = this.getTramiteService().
                        findTramitePruebasByTramiteId(this.selectedTramiteCargueInfo.getId());
                    List<TramitePredioEnglobe> tpelist = new ArrayList<TramitePredioEnglobe>();
                    for (TramitePredioEnglobe tpeTemp : this.prediosTramite) {
                        if (this.selectedTramiteCargueInfo.isSegundaEnglobe() &&
                            tempTramiteInicial.getPredios().contains(tpeTemp.getPredio())) {

                            tpeTemp.setEnglobePrincipal("SI");
                        } else if (tpeTemp.getPredio().getId().equals(
                            this.selectedTramiteCargueInfo.getPredio().getId())) {
                            tpeTemp.setEnglobePrincipal("SI");
                        }

                        tpelist.add(tpeTemp);
                    }
                    this.prediosTramite.clear();
                    this.prediosTramite.addAll(tpelist);
                    this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(this.prediosTramite);

                }
                if (this.tramiteTramiteSeleccionado.getPredio() == null &&
                    this.prediosTramite != null &&
                    !this.prediosTramite.isEmpty()) {
                    this.tramiteTramiteSeleccionado.setPredio(
                        this.prediosTramite.get(0).getPredio());
                }

                if (this.banderaEdicionTramiteTramite == false) {
                    this.tramiteTramites.add(this.tramiteTramiteSeleccionado);
                }

                this.banderaEdicionTramiteTramite = false;

                this.selectedDatosRectifComplDataTable = null;
            } else {
                context.addCallbackParam("error", "error");
            }

        } else {
            context.addCallbackParam("error", "error");
        }
    }

    /**
     * Método encargado de guardar el registro en la tabla trámite estado
     *
     * @author javier.aponte
     */
    private void guardarTramiteEstado(Tramite t, String codigoTramiteEstado, String observaciones) {
        TramiteEstado tramEstado = new TramiteEstado();

        tramEstado.setEstado(codigoTramiteEstado);
        tramEstado.setFechaInicio(new Date());
        tramEstado.setFechaLog(new Date());
        tramEstado.setTramite(t);
        tramEstado.setObservaciones(observaciones);
        tramEstado.setUsuarioLog(this.usuario.getLogin());
        tramEstado.setMotivo(this.motivoCancelacion);

        try {
            tramEstado = this.getTramiteService().actualizarTramiteEstado(tramEstado, this.usuario);
            t.setTramiteEstado(tramEstado);
        } catch (Exception e) {
            LOGGER.error(
                "error actualizando el trámite-estado en CargueInfoAlSistemaMB#doDatabaseStatesUpdate:" +
                e, e);
            this.dbStateUpdateOK = false;
        }
    }

    /**
     * Método encargado de realizar validaciones cuando se cancela un trámite y se asocia uno nuevo
     * que practicamente va a venir a reemplzar el trámite que se cancela
     *
     * @return isTramiteValido
     * @author javier.aponte
     */
    private boolean validarAsociarTramitesNuevos() {

        this.validacionTramiteMB.setNuevoTramiteSeleccionado(this.tramiteTramiteSeleccionado);
        this.validacionTramiteMB.setTramitesPredioEnglobeNuevoTramiteAdicionadoDTO(UtilidadesWeb.
            obtenerListaTramitePredioEnglobeDTO(this.prediosTramite));
        this.validacionTramiteMB.setTramiteInicial(this.selectedTramiteCargueInfo);

        return this.validacionTramiteMB.validarAsociarTramitesNuevos();
    }

    /**
     * Método encargado de establecer los datos del nuevo trámite
     *
     * @author javier.aponte
     */
    /*
     * @modified by juanfelipe.garcia :: 30-09-2012 :: se adiciona el predio inicial al nuevo
     * tramite
     */
    private void establecerDatosPreliminaresTramiteNuevo() {

        if (this.tramiteTramiteSeleccionado.isSegundaEnglobe()) {
            List<TramitePredioEnglobe> tpelist = new ArrayList<TramitePredioEnglobe>();
            for (TramitePredioEnglobe tpeTemp : this.prediosTramite) {
                TramitePredioEnglobe tpeAux = new TramitePredioEnglobe();
                tpeAux.setPredio(tpeTemp.getPredio());
                tpeAux.setUsuarioLog(this.usuario.getLogin());
                tpeAux.setFechaLog(new Date());
                tpeAux.setEnglobePrincipal("NO");

                tpelist.add(tpeAux);
            }
            this.prediosTramite.clear();
            this.prediosTramite.addAll(tpelist);
            this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(this.prediosTramite);

        } else {
            if (this.prediosTramite != null && !this.prediosTramite.isEmpty()) {
                this.tramiteTramiteSeleccionado.setPredio(this.prediosTramite.get(0).getPredio());
            }
        }

        this.validacionTramiteMB.setNuevoTramiteSeleccionado(this.tramiteTramiteSeleccionado);

        this.validacionTramiteMB.setUsuario(this.usuario);

        //En este punto no se pueden agregar trámites nuevos de via gubernativa
        this.validacionTramiteMB.setTramitesGubernativaAll(null);

        //A los nuevos trámites se les asocia la solicitud del trámite inicial
        this.validacionTramiteMB.setSolicitudSeleccionada(this.selectedTramiteCargueInfo.
            getSolicitud());

        this.validacionTramiteMB.setTramitesPredioEnglobeNuevoTramiteAdicionado(this.prediosTramite);

        if (this.tramiteTramiteSeleccionado.isEsRectificacion() || this.tramiteTramiteSeleccionado.
            isEsComplementacion()) {

            this.validacionTramiteMB.setSelectedDatosRectificarOComplementarDataTable(
                this.selectedDatosRectifComplDataTable);

        }

        this.tramiteTramiteSeleccionado = this.validacionTramiteMB.establecerDatosTramiteNuevo(
            this.tramiteTramiteSeleccionado);

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Quita de la lista de predios del trámite el que se haya seleccionado dando click en el ícono
     *
     * @author pedro.garcia
     */
    public void removePredioFromListaPrediosTramite() {
        LOGGER.info("removePredioFromListaPrediosTramite action called");
        this.prediosTramite.remove(this.selectedPredioTramite);
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que se ejecuta al cambiar el tipo de mutación
     */
    public void cambioClaseMutacion() {

        // D: si es de segunda o quinta, se hace requerido escoger el subtipo de
        // trámite; y se pone
        // el subtipo en el trámite seleccionado
        String claseMutacionSeleccionada;
        claseMutacionSeleccionada = this.tramiteTramiteSeleccionado.getClaseMutacion();
        if (claseMutacionSeleccionada.equals(EMutacionClase.SEGUNDA.getCodigo()) ||
            claseMutacionSeleccionada.equals(EMutacionClase.QUINTA.getCodigo())) {
            this.subtipoTramiteEsRequerido = true;
            this.banderaClaseDeMutacionSeleccionada = true;
            this.subtipoClaseMutacionV = this.generalMB.getSubtipoClaseMutacionV(
                claseMutacionSeleccionada);

            if (this.tramiteTramiteSeleccionado.getPredio() != null) {
                this.tramiteTramiteSeleccionado.setPredio(null);
            }
            if (this.tramiteTramiteSeleccionado.getPredios() != null ||
                !this.tramiteTramiteSeleccionado.getPredios().isEmpty()) {
                this.tramiteTramiteSeleccionado.setTramitePredioEnglobes(null);
            }
        } else {
            this.subtipoTramiteEsRequerido = false;
            this.tramiteTramiteSeleccionado.setSubtipo(null);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Listener del subtipo de mutación
     */
    public void actualizarDesdeSubtipo() {

        if (this.tramiteTramiteSeleccionado.getSubtipo().equals(
            EMutacion2Subtipo.ENGLOBE.getCodigo())) {
            this.banderaSubtipoEnglobe = true;
        } else {
            this.banderaSubtipoEnglobe = false;
        }
        if (this.tramiteTramiteSeleccionado.getSubtipo().equals(
            EMutacion5Subtipo.NUEVO.getCodigo())) {
            this.banderaSubtipoNuevo = true;
        } else {
            this.banderaSubtipoNuevo = false;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Adicionar datos seleccionados". Adiciona los datos seleccionados del
     * picklist de datos a rectificar o complementar a la variable que los guarda
     *
     * @author pedro.garcia
     */
    public void adicionarDatosRectifCompl() {

        LOGGER.debug("adicionando datos seleccionados...");

        // D: la primera vez que entra aquí este atributo es nulo, porque nadie
        // lo ha inicializado
        if (this.selectedDatosRectifComplDataTable == null) {
            this.selectedDatosRectifComplDataTable = new ArrayList<DatoRectificar>();
        }

        // OJO: no se usa la lista target, sino el atributo target del
        // DualListModel
        for (DatoRectificar drTarget : this.datosRectifComplDLModel.getTarget()) {
            if (!this.selectedDatosRectifComplDataTable.contains(drTarget)) {
                this.selectedDatosRectifComplDataTable.add(drTarget);
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Retirar datos". Retira de la lista de datos para rectificación o
     * complementación los que se hayan seleccionado de la tabla resumen
     *
     * @author pedro.garcia
     */
    public void retirarDatoRectifCompl() {

        LOGGER.debug("retirando datos seleccionados...");

        for (DatoRectificar dtR : this.selectedDatosRectifComplDT) {
            if (this.selectedDatosRectifComplDataTable.contains(dtR)) {
                this.selectedDatosRectifComplDataTable.remove(dtR);
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" en la página de selección de datos a rectificar
     */
    public void aceptarDatosARectificar() {

        TramiteRectificacion newTramiteRectificacion;
        List<TramiteRectificacion> tramiteRectificaciones;

        if (!this.selectedDatosRectifComplDataTable.isEmpty()) {
            tramiteRectificaciones = new ArrayList<TramiteRectificacion>();
            for (DatoRectificar dr : this.selectedDatosRectifComplDataTable) {
                newTramiteRectificacion = new TramiteRectificacion();
                newTramiteRectificacion.setDatoRectificar(dr);
                newTramiteRectificacion.setTramite(this.tramiteTramiteSeleccionado);
                newTramiteRectificacion.setFechaLog(new Date());
                newTramiteRectificacion.setUsuarioLog(this.usuario.getLogin());
                tramiteRectificaciones.add(newTramiteRectificacion);
            }
            this.tramiteTramiteSeleccionado.setTramiteRectificacions(tramiteRectificaciones);
        } else {
            this.addMensajeInfo(
                "Un trámite de rectificación debe tener datos a rectificar asociados");
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Determina si el cuado de motivo cancelación va a estar visible o si no dependiendo del valor
     * del campo cancelar tramite desicion
     *
     * @author christian.rodriguez
     * @return verdadero si se va a cancelar el tramite, falso si no
     */
    public boolean isMotivoCancelacionHabilitado() {
        if (this.cancelarTramiteDecision.equals(ESiNo.SI.getCodigo())) {
            return true;
        } else {
            this.motivoCancelacion = new String();
            return false;
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Determina si el cuadro de 'tipo de novedad' va a estar visible o si no dependiendo del valor
     * del campo 'Realizó inspección ocular':
     *
     * @author christian.rodriguez
     * @return verdadero si se va a cancelar el tramite, falso si no
     */
    public boolean isTipoNovedadHabilitado() {
        if (this.currentComisionTramite.getRealizoInspeccionOcular().equals(ESiNo.NO.getCodigo())) {
            return true;
        }
        if (this.currentComisionTramite.getRealizoInspeccionOcular().equals(ESiNo.SI.getCodigo())) {
            this.currentComisionTramite.setNovedad(null);
            return false;
        } //D: si no pudo realizar la inspección ocular se debe dar el motivo (tipo de novedad)
        else {
            return true;
        }
    }
//-------------------------------------------------------------------------------------------------

    public void cleanRectificacionVariables() {
        this.selectedDatosRectifComplDataTable = new ArrayList<DatoRectificar>();
        this.datosRectifComplSource = new ArrayList<DatoRectificar>();
        this.datosRectifComplTarget = new ArrayList<DatoRectificar>();
        this.tipoDatoRectifComplSeleccionado = null;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método para mover el tramite cuando se va a depuración
     *
     * @author felipe.cadena
     * @return
     */
    public String moverADepuracion() {

        UsuarioDTO usuarioDestino = this.generalMB.validarEnvioDepuracion(this.tramiteSeleccionado,
            this.usuario, this.currentProcessActivity);
        if (usuarioDestino == null) {
            return null;
        }

        this.generalMB.crearRegistroTramiteDepuracion(this.tramiteSeleccionado, this.usuario);

        try {

            //felipe.cadena::ACCU::Se cambia la forma de generar la replica de consulta
            RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
                getManagedBean("recuperarTramites");
            recuperarTamitesMB.generarReplicaConsulta(this.tramiteSeleccionado, this.usuario,
                usuarioDestino, 0);
//			IAvanzarProcesoConservacion avanzarProcesoConservacionService = this.getAvanzarProcesoConservacion();
//			avanzarProcesoConservacionService.avanzarProcesoDepurarInformacionGeografica(this.usuario,usuarioDestino,
            //			this.currentProcessActivity);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            this.addMensajeError("Error al avanzar el proceso.");
            return "";
        }

        this.tareasPendientesMB.init();
        return cerrar();

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método para actualizar los valores de los datos de depuración asociados al trámite
     * seleccionado y determinar si necesita depuración o no.
     *
     * @author felipe.cadena
     * @param event
     */
    public void actualizarDatosDepuracion(SelectEvent event) {

        if (event != null) {
            Tramite tramite = (Tramite) event.getObject();

            this.tramiteSeleccionado = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(
                tramite.getId());
            this.banderaTramiteEspacial = this.tramiteSeleccionado.isTramiteGeografico();
            this.banderaRectificaZonas = this.tramiteSeleccionado.isRectificacionZona();
        }

        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.tramiteSeleccionado.getId());
        try {
            this.parametroEditorGeografico = this.usuario.getLogin() + "," +
                this.tramiteSeleccionado.getId() +
                "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class,
                    this.currentProcessActivity.getNombre());

            this.inconsistenciasTramite = this.getTramiteService().
                buscarTramiteInconsistenciaPorTramiteId(this.tramiteSeleccionado.getId());

            if (!this.inconsistenciasTramite.isEmpty()) {
                this.banderaNecesitaDepuracion = !this.inconsistenciasTramite.
                    get(0).getInconsistencia().equals(EInconsistenciasGeograficas.NO_INCONSISTENCIA.
                    getNombre());
            } else {
                this.banderaNecesitaDepuracion = false;
            }

        } catch (Exception e) {
            this.banderaNecesitaDepuracion = false;
        }

        //D: para ver si se activan los botones
        definirVisualizacion2Botones();
        determinarExistenciaReplica();

        //Valida si el predio es fiscal
        if (this.tramiteSeleccionado.getPredio() != null &&
            this.tramiteSeleccionado.getPredio().getTipoCatastro().equals(
                EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
            this.banderaPredioFiscal = true;
        } else {
            this.banderaPredioFiscal = false;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para actualizar las inconsistencias del tramite
     *
     * @author felipe.cadena
     * @param event
     */
    public void actualizarInconsistencias() {
        this.actualizarDatosDepuracion(null);

        //this.inconsistenciasTramite = this.getTramiteService().buscarTramiteInconsistenciaPorTramiteId(this.tramiteSeleccionado.getId());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Valida si se debe actualizar el estado de la comisión. Retorna true si se ha guardado el
     * resultado de la visita de todos los trámites pertenecientes a la comisión a la que pertenece
     * el trámite actual.
     *
     * @author pedro.garcia
     */
    private boolean validarDebeActualizarEC() {

        boolean answer = false;

        //D: se revisa si para todos los Comision_Tramite relacionados con la comisión a la que
        //   pertenece el trámite actual tienen valor 'SI' en el campo 'realizada'
        //N: esto sirve porque siempre que se selecciona un trámite se setea el comision-tramite
        Comision comision = this.currentComisionTramite.getComision();
        answer = this.getTramiteService().decidirActualizacionEstadoComision1(comision.getId());

        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Maneja la activación de los botones "Devolver por comisión modificada" e "informar resultado
     * de la visita". <br />
     * El priomero se debe activar si todos los trámites seleccionados tienen la comisión en estado
     * "cancelada". El segundo si la tienen en estado "en ejecución" <br />
     *
     * OJO: actualmente la selección es single
     *
     * @author pedro.garcia
     * @modified felipe.cadena- 20-12-2013 - Se agrega manejo de comisiones de depuración
     */
    private void definirVisualizacion2Botones() {

        ComisionTramite comisionSeleccionada;

        if (this.selectedTramiteCargueInfo != null) {
            this.mostrarBotonDevolverXComisCancelada = false;
            this.showBotonInformarResultadoVisita = false;

            if (this.banderaDepuracion) {
                comisionSeleccionada = this.selectedTramiteCargueInfo.getComisionTramiteDepuracion();
            } else {
                comisionSeleccionada = this.selectedTramiteCargueInfo.getComisionTramite();
            }

            if (comisionSeleccionada.getComision().getEstado().equals(
                EComisionEstado.CANCELADA.getCodigo())) {
                this.mostrarBotonDevolverXComisCancelada = true;
                this.banderaTramiteEspacial = false;
            } else if (comisionSeleccionada.getComision().getEstado().equals(
                EComisionEstado.EN_EJECUCION.getCodigo())) {
                this.showBotonInformarResultadoVisita = true;
            }
        } else {
            this.mostrarBotonDevolverXComisCancelada = false;
            this.showBotonInformarResultadoVisita = false;
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * listener del evento se deselección en la tabla de trámites para la actividad.
     *
     * @author pedro.garcia
     */
    public void tableRowUnselectListener(UnselectEvent se) {

        definirVisualizacion2Botones();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón para "devolver por comisión modificada".
     *
     * @author pedro.garcia
     */
    public void devolverPorComisionCancelada() {

        boolean error = false;
        String idTramite;
        Actividad actividad;

        idTramite = this.selectedTramiteCargueInfo.getId().toString();
        Map<EParametrosConsultaActividades, String> filtros =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        filtros.put(EParametrosConsultaActividades.ID_TRAMITE, idTramite);
        filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);

        List<Actividad> actividades = null;

        try {
            actividades = this.getProcesosService().consultarListaActividades(filtros);
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error consultando la lista de actividades para el trámite " +
                idTramite, ex);
            error = true;
        }

        if (actividades != null && !actividades.isEmpty()) {
            SolicitudCatastral solicitudCatastral, scRespuesta;
            List<UsuarioDTO> usuarios;
            String transicion;

            if (actividades.size() > 1) {
                LOGGER.warn("La lista de actividades para el trámite " + idTramite +
                    " tiene más de un elemento. Se toma la primera actividad para hacer el movimiento del trámite por cancelación de la comisión.");
            }

            actividad = actividades.get(0);
            solicitudCatastral = new SolicitudCatastral();

            //D: mover el trámite
            transicion = ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR;
            usuarios = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.usuario.getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_CONSERVACION);
            if (usuarios == null || usuarios.isEmpty()) {
                LOGGER.
                    warn("No se encontraron usuarios con el rol " + ERol.RESPONSABLE_CONSERVACION);
                error = true;
            }
            solicitudCatastral.setUsuarios(usuarios);
            solicitudCatastral.setTransicion(transicion);
            scRespuesta = this.getProcesosService().avanzarActividad(
                actividad.getId(), solicitudCatastral);
            if (scRespuesta == null) {
                LOGGER.warn("Error en Alistar información al mover el trámite " + idTramite);
                error = true;
            }

            //javier.aponte CC-NO-CO-034 Si la comisión está cancelada entonces elimina las relaciones de comisión trámite
            // y establece el trámite en comisionado NO
            this.selectedTramiteCargueInfo = this.generalMB.
                eliminarRelacionComisionTramite(this.selectedTramiteCargueInfo, this.usuario);

            //D: se quita de la lista para que no siga apareciendo en la interfaz
            this.tramitesParaCargueInfo.remove(this.selectedTramiteCargueInfo);
            this.selectedTramiteCargueInfo = null;

            this.mostrarBotonDevolverXComisCancelada = false;

            if (error) {
                this.addMensajeError(
                    "Ocurrió un error moviendo los trámites. Consulte con el administrador del sistema");
            } else {
                this.addMensajeInfo("El trámite se movió a la actividad " +
                    ProcesoDeConservacion.ACT_ASIGNACION_COMISIONAR);
            }

        }

    }

    /**
     *
     * determina si el tramite tiene replica de consulta asociada y marca la bandera correpondiente
     *
     * @author felipe.cadena
     */
    private void determinarExistenciaReplica() {

        Documento d = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.tramiteSeleccionado.getId(),
                ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());

        this.banderaReplicaConsulta = (d != null);

    }

    /**
     * Genera la replica de consulta cuando se requiere por parte del usuario.
     *
     * @author felipe.cadena
     * @return
     */
    public String generarReplicaConsulta() {
        //se transfiere el tramite a tiempos muertos y se inicia la generacion de la replica
//          this.getConservacionService().generarReplicaDeConsultaTramiteYAvanzarProceso(
//                    this.currentProcessActivity,
//                    this.usuario,
//                    this.usuario, 
//                    false);

        //felipe.cadena::ACCU::Se cambia la forma de generar la replica de consulta
        RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
            getManagedBean("recuperarTramites");
        recuperarTamitesMB.generarReplicaConsulta(this.tramiteSeleccionado, this.usuario, null, 1);

        UtilidadesWeb.removerManagedBean("cargueInfoAlSistema");
        this.tareasPendientesMB.init();
        return "index";
    }

    private boolean tienePrediosBloqueados() {
        if (this.tramiteTramiteSeleccionado.getPredio() != null) {
            Predio predio = this.tramiteTramiteSeleccionado.getPredio();
            List<PredioBloqueo> predioBloqueos = this.getConservacionService().
                obtenerPredioBloqueosPorNumeroPredial(predio.getNumeroPredial());
            predio.setPredioBloqueos(predioBloqueos);
            return predio.isEstaBloqueado();
        }
        return false;
    }

//end of class
}
