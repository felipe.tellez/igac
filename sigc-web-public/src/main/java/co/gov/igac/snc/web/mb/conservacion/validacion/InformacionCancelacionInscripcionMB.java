/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.EDatosPanelAvaluo;
import co.gov.igac.snc.util.EDatosPanelUbicacionPredio;
import co.gov.igac.snc.util.EIdPanelsInfoCancelaInscribe;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ComboBoxConverter;
import co.gov.igac.snc.web.util.MonedaConverter;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Managed bean para Validación de trámites en información de cancelación o inscripción
 *
 * @author juan.agudelo
 * @modified leidy.gonzalez Se agregan variables para visualizar la lista de fotografias de la
 * unidad de construccion guardadas en el repositorio
 */
@Component("infoCancelacionInscripcion")
@Scope("session")
public class InformacionCancelacionInscripcionMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 703586875287978488L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(InformacionCancelacionInscripcionMB.class);

    // --------------------------------- Variables ----------------------------
    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * Lista que contiene los predios inciales del trámite
     */
    private List<Predio> prediosIniciales;

    /**
     * Lista que contiene los predios finales del trámite
     */
    private List<PPredio> prediosFinales;

    /**
     * Predio inical seleccionado utilizado en la comparación de predios en información de
     * cancelación o inscripción
     */
    private Predio predioInicialSeleccionado;

    /**
     * Ficha matriz seleccionada
     */
    private FichaMatriz fichaMatrizSelecionada;

    /**
     * PFicha matriz seleccionada
     */
    private PFichaMatriz pfichaMatrizSelecionada;

    /**
     * Predio final seleccionado utilizado en la comparación de predios en información de
     * cancelación o inscripción
     */
    private PPredio predioFinalSeleccionado;

    /**
     * Trámite seleccionado
     */
    private Tramite tramiteSeleccionado;

    /**
     * Configuración de los paneles que se muestran en la información de cancelación o inscripción
     */
    private TramiteSeccionDatos configuracionTramite;

    /**
     * Tabla que contiene los datos a mostrar para el detalle de los predios seleccionados.
     *
     * OJO: esta tabla se llena de forma diferente dependiendo del tipo de información que se vaya a
     * mostrar (ubicación del predio, avalúo, etc).
     */
    /*
     * @documented pedro.garcia cochinada de juan.agudelo que usa una misma 'tabla' para mostrar
     * datos totalmente diferentes según el panel seleccionado en el tab de información cancelación
     * / inscripción
     */
    private List<String[]> tablaDatosPredios;

    /**
     * Tabla que contiene los datos a mostrar para el detalle de ubicación de los predios
     * seleccionados
     */
    private List<String[]> tablaDatosUbicacionPredio;

    /**
     * Identificador del panel activo
     */
    private String idPanelDetallePredios;

    /**
     *
     */
    private int tabSeleccionado;

    // ------------- Colores de la comparación --------------------------------
    /**
     * Imagen asociada a los datos inciales
     */
    private String imagenDatoInicial = "/imagesColorCircles/datoInicial.png";

    /**
     * Imagen asociada a los datos inciales
     */
    private String imagenDatoFinal = "/imagesColorCircles/datoFinal.png";

    /**
     * Imagen asociada a los datos inciales
     */
    private String imagenDatoModificado = "/imagesColorCircles/datoModificado.png";

    // ------------- Datos de las tablas --------------------------------------
    // Ubicacion predial
    private String[] numeroPredial = new String[5];
    private String[] numeroPredialAnterior = new String[5];
    private String[] tipoPredio = new String[5];
    private String[] matriculaInmobiliaria = new String[5];
    private String[] estadoPredio = new String[5];
    private String[] predioLey14 = new String[5];
    private String[] destinoPredio = new String[5];
    private String[] departamentoPredio = new String[5];
    private String[] municipioPredio = new String[5];
    private String[] corregimientoPredio = new String[5];
    private String[] localidadComunaPredio = new String[5];
    private String[] barrioVeredaPredio = new String[5];

    /**
     * variables que guardan los datos que se muestran en la tabla "detalle predios" en el panel
     * "detalle del avalúo". [0] = nombre del dato [1] = valor para el predio inicial [2] = valor
     * para el predio proyectado [3] = imagen que indica si cambió, o quedó igual el dato [4] = ?
     */
    /*
     * @documented pedro.garcia
     */
    private String[] valorTerrenoAvaluo = new String[5];
    private String[] valorTotalConstruccion = new String[5];
    private String[] valorTotalConstruccionConvencional = new String[5];
    private String[] valorTotalConstruccionNoConvencional = new String[5];
    private String[] areaTotalTerreno = new String[5];
    private String[] areaTotalConstruccion = new String[5];
    private String[] areaTotalConstruccionConvencional = new String[5];
    private String[] areaTotalConstruccionNoConvencional = new String[5];

    /**
     * Tabla construcciones predio inicial
     */
    private String[] construccionesConvencionales = new String[7];
    private String[] construccionesNoConvencionales = new String[7];
    private List<String[]> tablaDatosConstrucciones;

    /**
     * Tabla construcciones predio proyectado
     */
    private String[] construccionesConvencionalesP = new String[7];
    private String[] construccionesNoConvencionalesP = new String[7];
    private List<String[]> tablaDatosConstruccionesP;

    // Detalle de avalúo
    /**
     * Predio inical seleccionado utilizado en el detalle del avalúo
     */
    private Predio predioAvaluoSeleccionado;

    /**
     * Predio inical seleccionado utilizado en el detalle del avalúo
     */
    private PPredio ppredioAvaluoSeleccionado;

    /**
     * Lista de las unidades de construcción convencionales para el detalle del avalúo del predio
     * inicial
     */
    private List<UnidadConstruccion> construccionesConvencionalesD;

    /**
     * Lista de las unidades de construcción no convencionales para el detalle del avalúo del predio
     * inicial
     */
    private List<UnidadConstruccion> construccionesNoConvencionalesD;

    /**
     * Lista de las unidades de construcción convencionales para el detalle del avalúo del predio
     * proyectado
     */
    private ArrayList<PUnidadConstruccion> construccionesConvencionalesDP;

    /**
     * Lista de las unidades de construcción no convencionales para el detalle del avalúo del predio
     * proyectado
     */
    private ArrayList<PUnidadConstruccion> construccionesNoConvencionalesDP;

    // Justificación de propiedad
    private List<PersonaPredioPropiedad> tablaJustificacionPropiedad;
    private List<PPersonaPredioPropiedad> tablaJustificacionPPropiedad;
    private List<PersonaPredioPropiedad> tablaDetallesAdicionalesJPropiedad;
    private List<PersonaPredioPropiedad> tablaDetallesAdicionalesJPPropiedad;

    // Propietarios y / o poseedores
    private List<PersonaPredio> tablaPropietarios;
    private List<PPersonaPredio> tablaPPropietarios;
    private List<PersonaPredioPropiedad> tablaDetallesAdicionalesPropietarios;
    private List<PPersonaPredioPropiedad> tablaDetallesAdicionalesPPropietarios;

    // Ficha matriz
    private PPredio ppredioFichaMatriz;

    // Detalle de ubicación
    /**
     * Predio seleccionado con los datos de ubicación requeridos
     */
    private Predio predioUbicacionSeleccionado;

    /**
     * PPredio seleccionado con los datos de ubicación requeridos
     */
    private PPredio ppredioUbicacionSeleccionado;

    // Detalle de ficha matriz - unidades de construcción
    /**
     * Predio seleccionado con los datos de unidad de construcción requeridos
     */
    private UnidadConstruccion predioUnidadSeleccionado;

    /**
     * PPredio seleccionado con los datos de unidad de construcción requeridos
     */
    private PUnidadConstruccion ppredioUnidadSeleccionado;

    // Detalle de inscripciones y decretos
    /**
     * Avalúos catastrales aplicados al predio.
     */
    private List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastrales;

    /**
     * Lista de valores del campo 'cancela_inscribe' que se van a usar par filtrar las búsquedas de
     * los PPredios
     */
    private ArrayList<String> cmiValuesToUse;

    /**
     * Imagenes de la unidad de construccion
     *
     * @return
     */
    private List<PFoto> fotosUnidad;

    /**
     * Variable que contiene las fotografias seleccionados
     */
    private PFoto fotoSeleccionada;

    /**
     * ReporteDTO del documento
     */
    private ReporteDTO reporteDocumento;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * lista con las zonas asociadas al predio seleccionado
     */
    private List<PPredioZona> listaPredioZonaSeleccionado;

    /**
     * lista con las zonas asociadas al predio seleccionado
     */
    private List<PredioZona> listaPredioZonaOriginales;

    /**
     *
     * Datos para tabla detalle ficha matriz
     */
    private String[] infoAreas = new String[6];

    /**
     *
     * Datos para tabla detalle Pficha matriz
     */
    private String[] infoAreasP = new String[6];

//---------------   métodos  -------------------------------------------------------
    public List<PredioZona> getListaPredioZonaOriginales() {
        return listaPredioZonaOriginales;
    }

    public void setListaPredioZonaOriginales(List<PredioZona> listaPredioZonaOriginales) {
        this.listaPredioZonaOriginales = listaPredioZonaOriginales;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public ReporteDTO getReporteDocumento() {
        return reporteDocumento;
    }

    public void setReporteDocumento(ReporteDTO reporteDocumento) {
        this.reporteDocumento = reporteDocumento;
    }

    public PFoto getFotoSeleccionada() {
        return fotoSeleccionada;
    }

    public void setFotoSeleccionada(PFoto fotoSeleccionada) {
        this.fotoSeleccionada = fotoSeleccionada;
    }

    public List<PFoto> getFotosUnidad() {
        return this.fotosUnidad;
    }

    public void setFotosUnidad(List<PFoto> fotosUnidad) {
        this.fotosUnidad = fotosUnidad;
    }

    public List<UnidadConstruccion> getConstruccionesConvencionalesD() {
        return this.construccionesConvencionalesD;
    }

    public void setConstruccionesConvencionalesD(
        List<UnidadConstruccion> construccionesConvencionalesD) {
        this.construccionesConvencionalesD = construccionesConvencionalesD;
    }

    public List<UnidadConstruccion> getConstruccionesNoConvencionalesD() {
        return this.construccionesNoConvencionalesD;
    }

    public FichaMatriz getFichaMatrizSelecionada() {
        return this.fichaMatrizSelecionada;
    }

    public void setFichaMatrizSelecionada(FichaMatriz fichaMatrizSelecionada) {
        this.fichaMatrizSelecionada = fichaMatrizSelecionada;
    }

    public UnidadConstruccion getPredioUnidadSeleccionado() {
        return this.predioUnidadSeleccionado;
    }

    public PFichaMatriz getPfichaMatrizSelecionada() {
        return this.pfichaMatrizSelecionada;
    }

    public void setPfichaMatrizSelecionada(PFichaMatriz pfichaMatrizSelecionada) {
        this.pfichaMatrizSelecionada = pfichaMatrizSelecionada;
    }

    public void setPredioUnidadSeleccionado(
        UnidadConstruccion predioUnidadSeleccionado) {
        this.predioUnidadSeleccionado = predioUnidadSeleccionado;
    }

    public List<VPPredioAvaluoDecreto> getpPrediosAvaluosCatastrales() {
        return this.pPrediosAvaluosCatastrales;
    }

    public void setpPrediosAvaluosCatastrales(
        List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastrales) {
        this.pPrediosAvaluosCatastrales = pPrediosAvaluosCatastrales;
    }

    public PUnidadConstruccion getPpredioUnidadSeleccionado() {
        return this.ppredioUnidadSeleccionado;
    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified pedro.garcia 22-05-2013 se elimina el seteo de la ruta web porque se hace en el
     * llamado al método buscarPFotosUnidadConstruccion
     */
    public void setPpredioUnidadSeleccionado(PUnidadConstruccion ppredioUnidadSeleccionado) {

        this.ppredioUnidadSeleccionado = ppredioUnidadSeleccionado;

        this.fotosUnidad = this.getConservacionService().buscarPFotosUnidadConstruccion(
            this.ppredioUnidadSeleccionado.getId(), true);

    }
//--------------------------------------------------------------------------------------------------

    public void setConstruccionesNoConvencionalesD(
        List<UnidadConstruccion> construccionesNoConvencionalesD) {
        this.construccionesNoConvencionalesD = construccionesNoConvencionalesD;
    }

    public ArrayList<PUnidadConstruccion> getConstruccionesConvencionalesDP() {
        return this.construccionesConvencionalesDP;
    }

    public void setConstruccionesConvencionalesDP(
        ArrayList<PUnidadConstruccion> construccionesConvencionalesDP) {
        this.construccionesConvencionalesDP = construccionesConvencionalesDP;
    }

    public ArrayList<PUnidadConstruccion> getConstruccionesNoConvencionalesDP() {
        return this.construccionesNoConvencionalesDP;
    }

    public Predio getPredioUbicacionSeleccionado() {
        return this.predioUbicacionSeleccionado;
    }

    public void setPredioUbicacionSeleccionado(Predio predioUbicacionSeleccionado) {
        this.predioUbicacionSeleccionado = predioUbicacionSeleccionado;
    }

    public PPredio getPpredioUbicacionSeleccionado() {
        return this.ppredioUbicacionSeleccionado;
    }

    public void setPpredioUbicacionSeleccionado(PPredio ppredioUbicacionSeleccionado) {
        this.ppredioUbicacionSeleccionado = ppredioUbicacionSeleccionado;
    }

    public void setConstruccionesNoConvencionalesDP(
        ArrayList<PUnidadConstruccion> construccionesNoConvencionalesDP) {
        this.construccionesNoConvencionalesDP = construccionesNoConvencionalesDP;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Tramite getTramiteSeleccionado() {
        return this.tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public String[] getConstruccionesConvencionales() {
        return this.construccionesConvencionales;
    }

    public void setConstruccionesConvencionales(String[] construccionesConvencionales) {
        this.construccionesConvencionales = construccionesConvencionales;
    }

    public String[] getConstruccionesConvencionalesP() {
        return this.construccionesConvencionalesP;
    }

    public void setConstruccionesConvencionalesP(String[] construccionesConvencionalesP) {
        this.construccionesConvencionalesP = construccionesConvencionalesP;
    }

    public String[] getConstruccionesNoConvencionalesP() {
        return this.construccionesNoConvencionalesP;
    }

    public void setConstruccionesNoConvencionalesP(String[] construccionesNoConvencionalesP) {
        this.construccionesNoConvencionalesP = construccionesNoConvencionalesP;
    }

    public String[] getConstruccionesNoConvencionales() {
        return this.construccionesNoConvencionales;
    }

    public void setConstruccionesNoConvencionales(String[] construccionesNoConvencionales) {
        this.construccionesNoConvencionales = construccionesNoConvencionales;
    }

    public String getImagenDatoInicial() {
        return imagenDatoInicial;
    }

    public void setImagenDatoInicial(String imagenDatoInicial) {
        this.imagenDatoInicial = imagenDatoInicial;
    }

    public List<String[]> getTablaDatosConstrucciones() {
        return this.tablaDatosConstrucciones;
    }

    public Predio getPredioAvaluoSeleccionado() {
        return predioAvaluoSeleccionado;
    }

    public void setPredioAvaluoSeleccionado(Predio predioAvaluoSeleccionado) {
        this.predioAvaluoSeleccionado = predioAvaluoSeleccionado;
    }

    public PPredio getPpredioAvaluoSeleccionado() {
        return this.ppredioAvaluoSeleccionado;
    }

    public void setPpredioAvaluoSeleccionado(PPredio ppredioAvaluoSeleccionado) {
        this.ppredioAvaluoSeleccionado = ppredioAvaluoSeleccionado;
    }

    public void setTablaDatosConstrucciones(List<String[]> tablaDatosConstrucciones) {
        this.tablaDatosConstrucciones = tablaDatosConstrucciones;
    }

    public PPredio getPpredioFichaMatriz() {
        return this.ppredioFichaMatriz;
    }

    public void setPpredioFichaMatriz(PPredio ppredioFichaMatriz) {
        this.ppredioFichaMatriz = ppredioFichaMatriz;
    }

    public List<String[]> getTablaDatosConstruccionesP() {
        return this.tablaDatosConstruccionesP;
    }

    public void setTablaDatosConstruccionesP(List<String[]> tablaDatosConstruccionesP) {
        this.tablaDatosConstruccionesP = tablaDatosConstruccionesP;
    }

    public List<PersonaPredioPropiedad> getTablaJustificacionPropiedad() {
        return this.tablaJustificacionPropiedad;
    }

    public List<PersonaPredioPropiedad> getTablaDetallesAdicionalesJPropiedad() {
        return this.tablaDetallesAdicionalesJPropiedad;
    }

    public void setTablaDetallesAdicionalesJPropiedad(
        List<PersonaPredioPropiedad> tablaDetallesAdicionalesJPropiedad) {
        this.tablaDetallesAdicionalesJPropiedad = tablaDetallesAdicionalesJPropiedad;
    }

    public List<PersonaPredioPropiedad> getTablaDetallesAdicionalesJPPropiedad() {
        return this.tablaDetallesAdicionalesJPPropiedad;
    }

    public void setTablaDetallesAdicionalesJPPropiedad(
        List<PersonaPredioPropiedad> tablaDetallesAdicionalesJPPropiedad) {
        this.tablaDetallesAdicionalesJPPropiedad = tablaDetallesAdicionalesJPPropiedad;
    }

    public void setTablaJustificacionPropiedad(
        List<PersonaPredioPropiedad> tablaJustificacionPropiedad) {
        this.tablaJustificacionPropiedad = tablaJustificacionPropiedad;
    }

    public List<PPersonaPredioPropiedad> getTablaJustificacionPPropiedad() {
        return this.tablaJustificacionPPropiedad;
    }

    public void setTablaJustificacionPPropiedad(
        List<PPersonaPredioPropiedad> tablaJustificacionPPropiedad) {
        this.tablaJustificacionPPropiedad = tablaJustificacionPPropiedad;
    }

    public List<PersonaPredioPropiedad> getTablaDetallesAdicionalesPropietarios() {
        return this.tablaDetallesAdicionalesPropietarios;
    }

    public void setTablaDetallesAdicionalesPropietarios(
        List<PersonaPredioPropiedad> tablaDetallesAdicionalesPropietarios) {
        this.tablaDetallesAdicionalesPropietarios = tablaDetallesAdicionalesPropietarios;
    }

    public List<PPersonaPredioPropiedad> getTablaDetallesAdicionalesPPropietarios() {
        return this.tablaDetallesAdicionalesPPropietarios;
    }

    public void setTablaDetallesAdicionalesPPropietarios(
        List<PPersonaPredioPropiedad> tablaDetallesAdicionalesPPropietarios) {
        this.tablaDetallesAdicionalesPPropietarios = tablaDetallesAdicionalesPPropietarios;
    }

    public List<PersonaPredio> getTablaPropietarios() {
        return this.tablaPropietarios;
    }

    public void setTablaPropietarios(List<PersonaPredio> tablaPropietarios) {
        this.tablaPropietarios = tablaPropietarios;
    }

    public String getImagenDatoFinal() {
        return this.imagenDatoFinal;
    }

    public String getIdPanelDetallePredios() {
        return this.idPanelDetallePredios;
    }

    public void setIdPanelDetallePredios(String idPanelDetallePredios) {
        this.idPanelDetallePredios = idPanelDetallePredios;
    }

    public List<String[]> getTablaDatosUbicacionPredio() {
        return this.tablaDatosUbicacionPredio;
    }

    public void setTablaDatosUbicacionPredio(
        List<String[]> tablaDatosUbicacionPredio) {
        this.tablaDatosUbicacionPredio = tablaDatosUbicacionPredio;
    }

    public void setImagenDatoFinal(String imagenDatoFinal) {
        this.imagenDatoFinal = imagenDatoFinal;
    }

    public int getTabSeleccionado() {
        return this.tabSeleccionado;
    }

    public void setTabSeleccionado(int tabSeleccionado) {
        this.tabSeleccionado = tabSeleccionado;
    }

    public TramiteSeccionDatos getConfiguracionTramite() {
        return this.configuracionTramite;
    }

    public void setConfiguracionTramite(TramiteSeccionDatos configuracionTramite) {
        this.configuracionTramite = configuracionTramite;
    }

    public List<PPersonaPredio> getTablaPPropietarios() {
        List<PPersonaPredio> p = new ArrayList<PPersonaPredio>();
        if (this.tablaPPropietarios != null) {
            for (PPersonaPredio pp : this.tablaPPropietarios) {
                if (!(pp.getCancelaInscribe() != null && pp.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.CANCELA.getCodigo()))) {
                    p.add(pp);
                }
            }
        }
        return p;
    }

    public void setTablaPPropietarios(List<PPersonaPredio> tablaPPropietarios) {
        this.tablaPPropietarios = tablaPPropietarios;
    }

    public Predio getPredioInicialSeleccionado() {
        return this.predioInicialSeleccionado;
    }

    public void setPredioInicialSeleccionado(Predio predioInicialSeleccionado) {
        this.predioInicialSeleccionado = predioInicialSeleccionado;
    }

    public List<Predio> getPrediosIniciales() {
        return this.prediosIniciales;
    }

    public void setPrediosIniciales(List<Predio> prediosIniciales) {
        this.prediosIniciales = prediosIniciales;
    }

    public List<PPredio> getPrediosFinales() {
        return this.prediosFinales;
    }

    public void setPrediosFinales(List<PPredio> prediosFinales) {
        this.prediosFinales = prediosFinales;
    }

    public PPredio getPredioFinalSeleccionado() {
        return this.predioFinalSeleccionado;
    }

    public List<String[]> getTablaDatosPredios() {
        return this.tablaDatosPredios;
    }

    public void setTablaDatosPredios(List<String[]> tablaDatosPredios) {
        this.tablaDatosPredios = tablaDatosPredios;
    }

    public void setPredioFinalSeleccionado(PPredio predioFinalSeleccionado) {
        this.predioFinalSeleccionado = predioFinalSeleccionado;
    }

    public List<PPredioZona> getListaPredioZonaSeleccionado() {
        return listaPredioZonaSeleccionado;
    }

    public void setListaPredioZonaSeleccionado(List<PPredioZona> listaPredioZonaSeleccionado) {
        this.listaPredioZonaSeleccionado = listaPredioZonaSeleccionado;
    }

    public String[] getInfoAreas() {
        return infoAreas;
    }

    public void setInfoAreas(String[] infoAreas) {
        this.infoAreas = infoAreas;
    }

    public String[] getInfoAreasP() {
        return infoAreasP;
    }

    public void setInfoAreasP(String[] infoAreasP) {
        this.infoAreasP = infoAreasP;
    }

//--------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init de InformacionCancelacionInscripcionMB");
        this.idPanelDetallePredios = "";

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        if (this.tramiteSeleccionado != null) {

            //D: para los trámites de englobe y desenglobe, solo se deben mostrar en los predios proyectados
            //   los que estén con 'M' o 'I'
            if (this.tramiteSeleccionado.isSegundaEnglobe() ||
                this.tramiteSeleccionado.isSegundaDesenglobe()) {

                this.cmiValuesToUse = new ArrayList<String>();
                this.cmiValuesToUse.add(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                this.cmiValuesToUse.add(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            } // ... en los demás casos ese parámetro se envía como null (para que no se use ese filtro)
            else {
                this.cmiValuesToUse = null;
            }

            this.tramiteSeleccionado = this.getTramiteService()
                .buscarTramitePorId(this.tramiteSeleccionado.getId());

            this.prediosIniciales = this.getConservacionService().buscarPrediosPorTramiteId(
                this.tramiteSeleccionado.getId());

            this.prediosFinales = this.getConservacionService()
                .buscarPPrediosPorTramiteId(
                    this.tramiteSeleccionado.getId(),
                    this.cmiValuesToUse);

            // Encontrar el predio ficha matriz de la lista de predios
            // del trámite con el fin de que no se tenga presente en el
            // la lista de predios proyectados.
            if (this.tramiteSeleccionado.isSegundaDesenglobe()) {
                long idPPredioFichaMatriz = verificarIdPredioFichaMatriz();
                PPredio pPredioFichaMatriz = null;
                for (PPredio ppredio : this.prediosFinales) {
                    if (ppredio.getId().longValue() == idPPredioFichaMatriz) {
                        pPredioFichaMatriz = ppredio;
                    }
                }
                if (pPredioFichaMatriz != null) {
                    this.prediosFinales.remove(pPredioFichaMatriz);
                }
            }

            this.configuracionTramite = this.getTramiteService()
                .consultarConfiguracionTramite(
                    this.tramiteSeleccionado.getTipoTramite(),
                    this.tramiteSeleccionado.getClaseMutacion(),
                    this.tramiteSeleccionado.getSubtipo(),
                    this.tramiteSeleccionado.getRadicacionEspecial());

            this.idPanelDetallePredios = EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId();

            if (this.tramiteSeleccionado.getTipoTramite().equals(
                ETramiteTipoTramite.RECTIFICACION.getCodigo()) ||
                (this.tramiteSeleccionado.getTipoTramite().equals(
                    ETramiteTipoTramite.MUTACION.getCodigo()) && (!this.tramiteSeleccionado
                .getClaseMutacion().equals(EMutacionClase.PRIMERA.getCodigo()) &&
                !this.tramiteSeleccionado
                    .getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo())))) {

                inicializarDatosUbicacion();

            } else if ((this.tramiteSeleccionado.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) && this.tramiteSeleccionado
                .getClaseMutacion().equals(EMutacionClase.CUARTA.getCodigo())) ||
                (this.tramiteSeleccionado.getTipoTramite()
                    .equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo())) ||
                (this.tramiteSeleccionado.getTipoTramite()
                    .equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo()))) {

                this.idPanelDetallePredios = EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO
                    .getId();

                inicializarDatosAvaluos();
                almacenarDatosTablaDatosPredios();

            } else if ((this.tramiteSeleccionado.getTipoTramite().equals(
                ETramiteTipoTramite.MUTACION.getCodigo()) && this.tramiteSeleccionado
                .getClaseMutacion().equals(EMutacionClase.PRIMERA.getCodigo()))) {

                this.idPanelDetallePredios =
                    EIdPanelsInfoCancelaInscribe.PROPIETARIOS_Y_O_POSEEDORES
                        .getId();
                // TODO que deberia pasar????
            }

        }
        predioFinalCargar();
    }
//-----------------------------------------------------------------------------------------------

    /**
     * Método que inicializa los datos correspondientes a las filas que se muestran en la tabla de
     * datos de ubicación del predio
     */
    /*
     * @documented pedro.garcia
     */
    private void inicializarDatosUbicacion() {
        // Datos especificos de la Ubicación
        this.departamentoPredio[0] = EDatosPanelUbicacionPredio.DEPARTAMENTO.getDato();
        this.municipioPredio[0] = EDatosPanelUbicacionPredio.MUNICIPIO.getDato();
        this.corregimientoPredio[0] = EDatosPanelUbicacionPredio.CORREGIMIENTO.getDato();
        this.localidadComunaPredio[0] = EDatosPanelUbicacionPredio.LOCALIDAD_COMUNA.getDato();
        this.barrioVeredaPredio[0] = EDatosPanelUbicacionPredio.BARRIO_VEREDA.getDato();
        this.numeroPredial[0] = EDatosPanelUbicacionPredio.NUMERO_PREDIAL.getDato();
        this.matriculaInmobiliaria[0] = EDatosPanelUbicacionPredio.MATRICULA_INMOBILIARIA.getDato();
        this.numeroPredialAnterior[0] = EDatosPanelUbicacionPredio.NUMERO_PREDIAL_ANTERIOR.
            getDato();
        this.tipoPredio[0] = EDatosPanelUbicacionPredio.TIPO_PREDIO.getDato();
        this.estadoPredio[0] = EDatosPanelUbicacionPredio.ESTADO_PREDIO.getDato();
        this.predioLey14[0] = EDatosPanelUbicacionPredio.PREDIO_LEY_14.getDato();
        this.destinoPredio[0] = EDatosPanelUbicacionPredio.DESTINO_PREDIO.getDato();

        almacenarDatosTablaDatosPredios();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que inicializa los datos para la tabla de datos de avalúo del predio
     */
    /*
     * @documented pedro.garcia
     */
    public void inicializarDatosAvaluos() {
        this.valorTerrenoAvaluo[0] = EDatosPanelAvaluo.VALOR_TERRENO.getDato();
        this.valorTotalConstruccion[0] = EDatosPanelAvaluo.VALOR_TOTAL_CONSTRUCCION.getDato();
        this.valorTotalConstruccionConvencional[0] =
            EDatosPanelAvaluo.VALOR_TOTAL_CONSTRUCCION_CONVENCIONAL.getDato();
        this.valorTotalConstruccionNoConvencional[0] =
            EDatosPanelAvaluo.VALOR_TOTAL_CONSTRUCCION_NO_CONVENCIONAL.getDato();
        this.areaTotalTerreno[0] =
            EDatosPanelAvaluo.AREA_TOTAL_TERRENO.getDato();
        this.areaTotalConstruccion[0] = EDatosPanelAvaluo.AREA_TOTAL_CONSTRUCCION.getDato();
        this.areaTotalConstruccionConvencional[0] =
            EDatosPanelAvaluo.AREA_TOTAL_CONSTRUCCION_CONVENCIONAL.getDato();
        this.areaTotalConstruccionNoConvencional[0] =
            EDatosPanelAvaluo.AREA_TOTAL_CONSTRUCCION_NO_CONVENCIONAL.getDato();

        this.construccionesConvencionales[0] =
            EDatosPanelAvaluo.CONSTRUCCIONES_CONVENCIONALES.getDato();
        this.construccionesNoConvencionales[0] =
            EDatosPanelAvaluo.CONSTRUCCIONES_NO_CONVENCIONALES.getDato();

        this.construccionesConvencionalesP[0] = EDatosPanelAvaluo.CONSTRUCCIONES_CONVENCIONALES
            .getDato();
        this.construccionesNoConvencionalesP[0] =
            EDatosPanelAvaluo.CONSTRUCCIONES_NO_CONVENCIONALES.getDato();

        almacenarDatosTablaDatosPredios();
    }
//---------------------------------------------------------------------------------------------

    /**
     * Según el panel seleccionado adiciona los datos a la lista que sirve como value para mostrar
     * los datos
     */
    /*
     * @documented pedro.garcia
     */
    private void almacenarDatosTablaDatosPredios() {

        this.tablaDatosPredios = new ArrayList<String[]>();
        this.tablaDatosConstrucciones = new ArrayList<String[]>();
        this.tablaDatosConstruccionesP = new ArrayList<String[]>();

        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            adicionarDatosUbicacion();
        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            adicionarDatosAvaluo();

        }
    }
//---------------------------------------------------------------------------------------------

    /**
     * Método ejecutado para adicionar a la lista (con la que se pinta la tabla) los datos de avalúo
     * del predio
     */
    /*
     * @documented pedro.garcia
     */
    private void adicionarDatosAvaluo() {
        this.tablaDatosPredios.add(this.valorTerrenoAvaluo);
        this.tablaDatosPredios.add(this.valorTotalConstruccion);
        this.tablaDatosPredios.add(this.valorTotalConstruccionConvencional);
        this.tablaDatosPredios.add(this.valorTotalConstruccionNoConvencional);
        this.tablaDatosPredios.add(this.areaTotalTerreno);
        this.tablaDatosPredios.add(this.areaTotalConstruccion);
        this.tablaDatosPredios.add(this.areaTotalConstruccionConvencional);
        this.tablaDatosPredios.add(this.areaTotalConstruccionNoConvencional);

        this.tablaDatosConstrucciones.add(this.construccionesConvencionales);
        this.tablaDatosConstrucciones.add(this.construccionesNoConvencionales);

        this.tablaDatosConstruccionesP.add(this.construccionesConvencionalesP);
        this.tablaDatosConstruccionesP.add(this.construccionesNoConvencionalesP);

    }
//---------------------------------------------------------------------------------------------

    /**
     * Método ejecutado para adicionar a la lista (con la que se pinta la tabla) los datos de
     * ubicación de predio
     */
    /*
     * @documented pedro.garcia
     */
    private void adicionarDatosUbicacion() {
        this.tablaDatosPredios.add(this.departamentoPredio);
        this.tablaDatosPredios.add(this.municipioPredio);
        this.tablaDatosPredios.add(this.corregimientoPredio);
        this.tablaDatosPredios.add(this.localidadComunaPredio);
        this.tablaDatosPredios.add(this.barrioVeredaPredio);
        this.tablaDatosPredios.add(this.numeroPredial);
        this.tablaDatosPredios.add(this.numeroPredialAnterior);
        this.tablaDatosPredios.add(this.tipoPredio);
        this.tablaDatosPredios.add(this.matriculaInmobiliaria);
        this.tablaDatosPredios.add(this.estadoPredio);
        this.tablaDatosPredios.add(this.predioLey14);
        this.tablaDatosPredios.add(this.destinoPredio);

        this.tablaDatosUbicacionPredio = this.tablaDatosPredios;
    }
//---------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al realizar un cambio de panel en la comparación predio a predio
     */
    /*
     * @modified david.cifuentes - Ajustado por migración a PrimeFaces 3.2
     */
    public void cambioPanel(TabChangeEvent te) {
        if (te != null) {
            if (te.getTab()
                .getId()
                .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId()) ||
                te.getTab().getId()
                    .equals(EIdPanelsInfoCancelaInscribe.CONDICION_DE_PROPIEDAD.getId()) ||
                te.getTab().getId()
                    .equals(EIdPanelsInfoCancelaInscribe.FICHA_MATRIZ.getId()) ||
                te.getTab().getId()
                    .equals(EIdPanelsInfoCancelaInscribe.PROPIETARIOS_Y_O_POSEEDORES.getId()) ||
                te.getTab().getId()
                    .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId()) ||
                te.getTab().getId()
                    .equals(EIdPanelsInfoCancelaInscribe.INSCRIPCIONES_O_DECRETOS.getId())) {

                this.idPanelDetallePredios = te.getTab().getId();

                //D: debe hacer esto aquí porque según el panel se cargan datos diferentes
                predioInicialCargar();
                predioFinalCargar();
            }
        }
    }

    /**
     * Método creado para ajustar migración a PrimeFaces 3.2 y poder capturar evento
     * AjaxBehaviorEvent
     *
     * @author david.cifuentes
     */
    public void cambioPanel(AjaxBehaviorEvent abe) {
        // Éste método se creo para corregir un bug que se presentó al anidar un
        // accordionPanel dentro de un TabView manejando para ambos el mismo evento tabChange.
        LOGGER.debug("entra a cambioPanel de InformacionCancelacionInscripcionMB");
    }
//---------------------------------------------------------------------------------------------

    /**
     * Método que carga los datos resumen de información asociada al predio inicial seleccionado
     * dependiendo del modulo en el que se encuentre
     */
    /*
     * @modified pedro.garcia 06-08-2012 se adiciona llamado a predioInicialActualizar() que antes
     * se hacía en otro método
     */
    private void predioInicialCargar() {
        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            if (this.predioInicialSeleccionado != null) {
                this.prediosIniciales = this.getConservacionService()
                    .buscarPrediosPanelUbicacionPredioPorTramiteId(
                        this.tramiteSeleccionado.getId());

                this.fichaMatrizSelecionada = null;
                reemplazarPredioInicial();
            }
            inicializarDatosUbicacion();

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.CONDICION_DE_PROPIEDAD.getId())) {

            if (this.predioInicialSeleccionado != null) {
                this.prediosIniciales = this.getConservacionService()
                    .buscarPrediosPanelJPropiedadPorTramiteId(
                        this.tramiteSeleccionado.getId());

                this.fichaMatrizSelecionada = null;
                reemplazarPredioInicial();

                if (this.predioInicialSeleccionado.getPersonaPredios() != null &&
                    !this.predioInicialSeleccionado.getPersonaPredios().isEmpty()) {
                    this.tablaJustificacionPropiedad = new ArrayList<PersonaPredioPropiedad>();

                    for (PersonaPredio p : this.predioInicialSeleccionado.getPersonaPredios()) {
                        for (PersonaPredioPropiedad pp : p.getPersonaPredioPropiedads()) {
                            this.tablaJustificacionPropiedad.add(pp);
                        }
                    }
                }
            }

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            if (this.predioInicialSeleccionado != null) {
                this.prediosIniciales = this.getConservacionService()
                    .buscarPrediosPanelAvaluoPorTramiteId(this.tramiteSeleccionado.getId());

                this.fichaMatrizSelecionada = null;
                reemplazarPredioInicial();
            }

            inicializarDatosAvaluos();
        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.PROPIETARIOS_Y_O_POSEEDORES.getId())) {

            if (this.predioInicialSeleccionado != null) {
                this.prediosIniciales = this.getConservacionService()
                    .buscarPrediosPanelPropietariosPorTramiteId(
                        this.tramiteSeleccionado.getId());

                this.fichaMatrizSelecionada = null;
                reemplazarPredioInicial();

                if (this.predioInicialSeleccionado.getPersonaPredios() != null &&
                    !this.predioInicialSeleccionado.getPersonaPredios().isEmpty()) {
                    this.tablaPropietarios = new ArrayList<PersonaPredio>();

                    for (PersonaPredio p : this.predioInicialSeleccionado.getPersonaPredios()) {
                        this.tablaPropietarios.add(p);
                    }
                }
            }

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.FICHA_MATRIZ.getId())) {

            if (this.predioInicialSeleccionado != null) {
                this.prediosIniciales = this.getConservacionService().
                    buscarPrediosPanelFichaMatrizPorTramiteId(this.tramiteSeleccionado.getId());

                reemplazarPredioInicial();

                if (this.predioInicialSeleccionado.getFichaMatrizs() != null &&
                    !this.predioInicialSeleccionado.getFichaMatrizs().isEmpty()) {

                    this.fichaMatrizSelecionada =
                        this.predioInicialSeleccionado.getFichaMatrizs().get(0);

                    if (this.predioInicialSeleccionado.isPredioRural()) {
                        int[] areaTerreno = Utilidades.convertirAreaAformatoHectareas(new Double(
                            this.predioInicialSeleccionado.getAreaTotalTerreno()));
                        int[] areaTerrenoP = Utilidades.convertirAreaAformatoHectareas(
                            this.fichaMatrizSelecionada.getAreaTotalTerrenoPrivada());
                        int[] areaTerrenoC = Utilidades.convertirAreaAformatoHectareas(
                            this.fichaMatrizSelecionada.getAreaTotalTerrenoComun());
                        int[] areaTotCons = Utilidades.convertirAreaAformatoHectareas(
                            this.predioInicialSeleccionado.getAreaConstruccion());
                        int[] areaTotalConstruccionP = Utilidades.convertirAreaAformatoHectareas(
                            this.fichaMatrizSelecionada.getAreaTotalConstruidaPrivada());
                        int[] areaTotalConstruccionC = Utilidades.convertirAreaAformatoHectareas(
                            this.fichaMatrizSelecionada.getAreaTotalConstruidaComun());

                        this.infoAreas[0] = areaTerreno[0] + "  Ha  " + areaTerreno[1] + " m2 ";
                        this.infoAreas[1] = areaTerrenoP[0] + "  Ha  " + areaTerrenoP[1] + " m2 ";
                        this.infoAreas[2] = areaTerrenoC[0] + "  Ha  " + areaTerrenoC[1] + " m2 ";
                        this.infoAreas[3] = areaTotCons[0] + "  Ha  " + areaTotCons[1] + " m2 ";
                        this.infoAreas[4] = areaTotalConstruccionP[0] + "  Ha  " +
                            areaTotalConstruccionP[1] + " m2 ";
                        this.infoAreas[5] = areaTotalConstruccionC[0] + "  Ha  " +
                            areaTotalConstruccionC[1] + " m2 ";

                    } else {
                        this.infoAreas[0] = this.predioInicialSeleccionado.getAreaTotalTerreno() +
                            " m2";
                        this.infoAreas[1] =
                            this.fichaMatrizSelecionada.getAreaTotalTerrenoPrivada() + " m2";
                        this.infoAreas[2] = this.fichaMatrizSelecionada.getAreaTotalTerrenoComun() +
                            " m2";
                        this.infoAreas[3] = this.predioInicialSeleccionado.getAreaConstruccion() +
                            " m2";
                        this.infoAreas[4] = this.fichaMatrizSelecionada.
                            getAreaTotalConstruidaPrivada() + " m2";
                        this.infoAreas[5] = this.fichaMatrizSelecionada.
                            getAreaTotalConstruidaComun() + " m2";
                    }
                }

            }

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.INSCRIPCIONES_O_DECRETOS.getId())) {
            this.pfichaMatrizSelecionada = null;
        }

        if (this.predioInicialSeleccionado != null) {
            predioInicialActualizar();
        }

    }
//---------------------------------------------------------------------------------------------

    /**
     * Método que carga los datos resumen de información asociados al predio proyectado dependiendo
     * del modulo en el que se encuentre
     */
    /*
     * @modified pedro.garcia 06-08-2012 se elimina llamado innecesario a método
     * predioInicialActualizar 22-08-2012 Ya no se consulta la info de todos los ppredios
     * (chambonada: consultaba todos y luego llamaba a un método que seleccionaba el que debía
     * mostrar) sino únicamente del que fue seleccionado 27-09-2012 NO se muestrsn los propietarios
     * de los predios proyectados que tengan 'C'
     *
     * @modified by juanfelipe.garcia :: 18-09-2014 :: refs#9205 se ajusto la forma de obtener la
     * pfichaMatrizSelecionada a partir de un pFichaMatrizPredio.
     */
    private void predioFinalCargar() {

        PPredio ppredioInfo;

        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            if (this.predioFinalSeleccionado != null) {
                ppredioInfo = this.getConservacionService().
                    obtenerPPredioDatosUbicacion(this.predioFinalSeleccionado.getId());

                this.predioFinalSeleccionado = ppredioInfo;

                this.pfichaMatrizSelecionada = null;
            }
            inicializarDatosUbicacion();

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.CONDICION_DE_PROPIEDAD.getId())) {

            if (this.predioFinalSeleccionado != null) {

//TODO :: pedro.garcia refs #6050 :: borrar lo comentado cuando pase pruebas
//				this.prediosFinales = this.getConservacionService()
//						.buscarPPrediosPanelJPropiedadPorTramiteId(
//								this.tramiteSeleccionado.getId());
//                reemplazarPredioFinal();
                ppredioInfo = this.getConservacionService().
                    obtenerPPredioDatosJustificPropiedad(this.predioFinalSeleccionado.getId());

                this.predioFinalSeleccionado = ppredioInfo;

                this.pfichaMatrizSelecionada = null;

                if (this.predioFinalSeleccionado.getPPersonaPredios() != null &&
                    !this.predioFinalSeleccionado.getPPersonaPredios().isEmpty()) {

                    this.tablaJustificacionPPropiedad = new ArrayList<PPersonaPredioPropiedad>();
                    for (PPersonaPredio p : this.predioFinalSeleccionado.getPPersonaPredios()) {
                        for (PPersonaPredioPropiedad pp : p.getPPersonaPredioPropiedads()) {

                            //D: solo se deben mostrar los que tengan cancela inscribe en 'M' o 'I'
                            // Modificado por fredy.wilches: tomo CancelaInscribe de PPersonaPredio y no de PPersonaPredioPropiedad
                            if (!p.getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                                this.tablaJustificacionPPropiedad.add(pp);
                            }
                        }
                    }
                }
            }

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            if (this.predioFinalSeleccionado != null) {

//TODO :: pedro.garcia refs #6050 :: borrar lo comentado cuando pase pruebas
//				this.prediosFinales = this.getConservacionService()
//						.buscarPPrediosPanelAvaluoPorTramiteId(this.tramiteSeleccionado.getId());
//                reemplazarPredioFinal();
                ppredioInfo = this.getConservacionService().
                    obtenerPPredioDatosAvaluo(this.predioFinalSeleccionado.getId());

                this.predioFinalSeleccionado = ppredioInfo;

                this.pfichaMatrizSelecionada = null;

            }
            inicializarDatosAvaluos();
        } //D: este es el primer panel en el accordion, por lo que al seleccionar un predio llega aquí
        else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.PROPIETARIOS_Y_O_POSEEDORES.getId())) {

            if (this.predioFinalSeleccionado != null) {

//TODO :: pedro.garcia refs #6050 :: borrar lo comentado cuando pase pruebas
//				this.prediosFinales = this.getConservacionService()
//						.buscarPPrediosPanelPropietariosPorTramiteId(
//                        this.tramiteSeleccionado.getId());
//                reemplazarPredioFinal();
                ppredioInfo = this.getConservacionService().
                    obtenerPPredioDatosPropietarios(this.predioFinalSeleccionado.getId());

                this.predioFinalSeleccionado = ppredioInfo;

                this.pfichaMatrizSelecionada = null;

                if (this.predioFinalSeleccionado.getPPersonaPredios() != null &&
                    !this.predioFinalSeleccionado.getPPersonaPredios().isEmpty()) {

                    this.tablaPPropietarios = new ArrayList<PPersonaPredio>();
                    for (PPersonaPredio p : this.predioFinalSeleccionado.getPPersonaPredios()) {

                        //D: solo se deben mostrar los que tengan cancela inscribe en 'M' o 'I'
                        // Para el caso en el que el propietario se mantenga de un predio a otro,
                        // el campo cancela inscribe de éste llega como null, y  se debe visualizar
                        // en la tabla de igual manera.
                        if (p.getCancelaInscribe() == null ||
                            p.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.
                                getCodigo()) ||
                            p.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.
                                getCodigo())) {
                            this.tablaPPropietarios.add(p);
                        }
                    }
                }
            }

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.FICHA_MATRIZ.getId())) {

            if (this.predioFinalSeleccionado != null) {

//TODO :: pedro.garcia :: borrar lo comentado cuando pase pruebas
//				this.prediosFinales = this.getConservacionService()
//						.buscarPPrediosPanelFichaMatrizPorTramiteId(
//								this.tramiteSeleccionado.getId());
//				reemplazarPredioFinal();
                ppredioInfo = this.getConservacionService().
                    obtenerPPredioDatosFichaMatriz(this.predioFinalSeleccionado.getId());

                this.predioFinalSeleccionado = ppredioInfo;

                //para el predio la ficha matriz esta asociada es por p_ficha_matriz_predio
                PFichaMatrizPredio pFichaMatrizPredio = null;
                pFichaMatrizPredio = this.getConservacionService()
                    .obtenerPFichaMatrizPredioPorNumeroPredial(this.predioFinalSeleccionado
                        .getNumeroPredial());

                if (pFichaMatrizPredio != null &&
                    pFichaMatrizPredio.getPFichaMatriz() != null) {
                    this.pfichaMatrizSelecionada = pFichaMatrizPredio.getPFichaMatriz();
                    this.pfichaMatrizSelecionada.getPFichaMatrizPredios().get(0).getCoeficiente();
                    if (this.predioFinalSeleccionado.isPredioRural()) {
                        int[] areaTerreno = Utilidades.convertirAreaAformatoHectareas(new Double(
                            this.predioFinalSeleccionado.getAreaTotalTerreno()));
                        int[] areaTerrenoP = Utilidades.convertirAreaAformatoHectareas(
                            this.pfichaMatrizSelecionada.getAreaTotalTerrenoPrivada());
                        int[] areaTerrenoC = Utilidades.convertirAreaAformatoHectareas(
                            this.pfichaMatrizSelecionada.getAreaTotalTerrenoComun());
                        int[] areaTotCons = Utilidades.convertirAreaAformatoHectareas(
                            this.predioFinalSeleccionado.getAreaConstruccion());
                        int[] areaTotalConstruccionP = Utilidades.convertirAreaAformatoHectareas(
                            this.pfichaMatrizSelecionada.getAreaTotalConstruidaPrivada());
                        int[] areaTotalConstruccionC = Utilidades.convertirAreaAformatoHectareas(
                            this.pfichaMatrizSelecionada.getAreaTotalConstruidaComun());

                        this.infoAreasP[0] = areaTerreno[0] + "  Ha  " + areaTerreno[1] + " m2 ";
                        this.infoAreasP[1] = areaTerrenoP[0] + "  Ha  " + areaTerrenoP[1] + " m2 ";
                        this.infoAreasP[2] = areaTerrenoC[0] + "  Ha  " + areaTerrenoC[1] + " m2 ";
                        this.infoAreasP[3] = areaTotCons[0] + "  Ha  " + areaTotCons[1] + " m2 ";
                        this.infoAreasP[4] = areaTotalConstruccionP[0] + "  Ha  " +
                            areaTotalConstruccionP[1] + " m2 ";
                        this.infoAreasP[5] = areaTotalConstruccionC[0] + "  Ha  " +
                            areaTotalConstruccionC[1] + " m2 ";

                    } else {
                        this.infoAreasP[0] = this.predioFinalSeleccionado.getAreaTotalTerreno() +
                            " m2";
                        this.infoAreasP[1] = this.pfichaMatrizSelecionada.
                            getAreaTotalTerrenoPrivada() + " m2";
                        this.infoAreasP[2] =
                            this.pfichaMatrizSelecionada.getAreaTotalTerrenoComun() + " m2";
                        this.infoAreasP[3] = this.predioFinalSeleccionado.getAreaConstruccion() +
                            " m2";
                        this.infoAreasP[4] = this.pfichaMatrizSelecionada.
                            getAreaTotalConstruidaPrivada() + " m2";
                        this.infoAreasP[5] = this.pfichaMatrizSelecionada.
                            getAreaTotalConstruidaComun() + " m2";
                    }
                }

            }

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.INSCRIPCIONES_O_DECRETOS.getId())) {

            if (this.predioFinalSeleccionado != null) {
                this.pPrediosAvaluosCatastrales = this.getFormacionService()
                    .buscarAvaluosCatastralesPorIdPPredio(
                        this.predioFinalSeleccionado.getId());
            }
        }

        if (this.predioFinalSeleccionado != null) {
            predioFinalActualizar();
        }

    }
//---------------------------------------------------------------------------------------------

    /**
     * Método para actualizar y reemplazar el antiguo predio inicial por el predio inicial actual
     */
    private void reemplazarPredioInicial() {
        for (Predio pTemp : this.prediosIniciales) {
            if (pTemp.getId().equals(this.predioInicialSeleccionado.getId())) {
                this.predioInicialSeleccionado = pTemp;
                break;
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para actualizar y reemplazar el antiguo predio final por el predio final actual
     */
//TODO :: pedro.garcia refs #6050 :: eliminar cuando pase las pruebas del reemplazo de métodos chambones que traían todos los ppredios
    private void reemplazarPredioFinal() {
        for (PPredio pTemp : this.prediosFinales) {
            if (pTemp.getId().equals(this.predioFinalSeleccionado.getId())) {
                this.predioFinalSeleccionado = pTemp;
                break;
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para la desselección de un predio inicial
     */
    public void predioInicialdeSeleccionado(UnselectEvent un) {

        this.predioInicialSeleccionado = null;

        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            pIdesUbicacionPredio();

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            pIdesAvaluo();

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.CONDICION_DE_PROPIEDAD.getId())) {

            this.tablaJustificacionPropiedad = null;

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.FICHA_MATRIZ.getId())) {

            this.predioUnidadSeleccionado = null;

        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al realizar una deselección del predio inicial en el panel correspondiente a
     * avalúo
     */
    private void pIdesAvaluo() {
        this.valorTerrenoAvaluo[1] = null;
        this.valorTotalConstruccion[1] = null;
        this.valorTotalConstruccionConvencional[1] = null;
        this.valorTotalConstruccionNoConvencional[1] = null;
        this.areaTotalTerreno[1] = null;
        this.areaTotalConstruccion[1] = null;
        this.areaTotalConstruccionConvencional[1] = null;
        this.areaTotalConstruccionNoConvencional[1] = null;

        this.valorTerrenoAvaluo[3] = null;
        this.valorTotalConstruccion[3] = null;
        this.valorTotalConstruccionConvencional[3] = null;
        this.valorTotalConstruccionNoConvencional[3] = null;
        this.areaTotalTerreno[3] = null;
        this.areaTotalConstruccion[3] = null;
        this.areaTotalConstruccionConvencional[3] = null;
        this.areaTotalConstruccionNoConvencional[3] = null;

        this.valorTerrenoAvaluo[4] = this.imagenDatoFinal;
        this.valorTotalConstruccion[4] = this.imagenDatoFinal;
        this.valorTotalConstruccionConvencional[4] = this.imagenDatoFinal;
        this.valorTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;
        this.areaTotalTerreno[4] = this.imagenDatoFinal;
        this.areaTotalConstruccion[4] = this.imagenDatoFinal;
        this.areaTotalConstruccionConvencional[4] = this.imagenDatoFinal;
        this.areaTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;

        this.construccionesConvencionales[1] = null;
        this.construccionesNoConvencionales[1] = null;
        this.construccionesConvencionales[2] = null;
        this.construccionesNoConvencionales[2] = null;
        this.construccionesConvencionales[3] = null;
        this.construccionesNoConvencionales[3] = null;
        this.construccionesConvencionalesP[4] = this.imagenDatoFinal;
        this.construccionesNoConvencionalesP[4] = this.imagenDatoFinal;
        this.construccionesConvencionalesP[5] = this.imagenDatoFinal;
        this.construccionesNoConvencionalesP[5] = this.imagenDatoFinal;
        this.construccionesConvencionalesP[6] = this.imagenDatoFinal;
        this.construccionesNoConvencionalesP[6] = this.imagenDatoFinal;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al realizar una deselección del predio inicial en el panel correspondiente a
     * ubicación del predio
     */
    private void pIdesUbicacionPredio() {
        this.numeroPredial[1] = null;
        this.numeroPredialAnterior[1] = null;
        this.tipoPredio[1] = null;
        this.matriculaInmobiliaria[1] = null;
        this.estadoPredio[1] = null;
        this.predioLey14[1] = null;
        this.destinoPredio[1] = null;
        this.departamentoPredio[1] = null;
        this.municipioPredio[1] = null;
        this.corregimientoPredio[1] = null;
        this.localidadComunaPredio[1] = null;
        this.barrioVeredaPredio[1] = null;

        this.predioInicialSeleccionado = null;
        this.numeroPredial[3] = null;
        this.numeroPredialAnterior[3] = null;
        this.tipoPredio[3] = null;
        this.matriculaInmobiliaria[3] = null;
        this.estadoPredio[3] = null;
        this.predioLey14[3] = null;
        this.destinoPredio[3] = null;
        this.departamentoPredio[3] = null;
        this.municipioPredio[3] = null;
        this.corregimientoPredio[3] = null;
        this.localidadComunaPredio[3] = null;
        this.barrioVeredaPredio[3] = null;

        this.numeroPredial[4] = this.imagenDatoFinal;
        this.numeroPredialAnterior[4] = this.imagenDatoFinal;
        this.tipoPredio[4] = this.imagenDatoFinal;
        this.matriculaInmobiliaria[4] = this.imagenDatoFinal;
        this.estadoPredio[4] = this.imagenDatoFinal;
        this.predioLey14[4] = this.imagenDatoFinal;
        this.destinoPredio[4] = this.imagenDatoFinal;
        this.departamentoPredio[4] = this.imagenDatoFinal;
        this.municipioPredio[4] = this.imagenDatoFinal;
        this.corregimientoPredio[4] = this.imagenDatoFinal;
        this.localidadComunaPredio[4] = this.imagenDatoFinal;
        this.barrioVeredaPredio[4] = this.imagenDatoFinal;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método del evento de selección de un predio inicial (de la tabla "predios iniciales")
     */
    /*
     * @modified pedro.garcia 06-08-2012. El autor chambón hacía el llamado a
     * predioInicialActualizar() desde predioInicialCargar(), y luego aquí otra vez. Se dejó el
     * primero
     */
    public void predioInicialSeleccionar(SelectEvent se) {
        predioInicialCargar();
//		predioFinalCargar();

//		if (this.predioInicialSeleccionado != null) {
//			predioInicialActualizar();
//		}
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para la selección de un predio inicial
     */
    private void predioInicialActualizar() {

        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            predioInicialUbicacion();

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            predioInicialDetalleAvaluo();

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Asigna los valores que se van a mostrar en la tabla de comparación de predio antes y después
     * en el panel ubicación predio, para un predio inicial.
     */
    /*
     * @documented pedro.garcia
     */
    private void predioInicialUbicacion() {

        String tempValue;

        this.numeroPredial[1] = this.predioInicialSeleccionado
            .getFormattedNumeroPredialCompleto();

        if (this.predioInicialSeleccionado.getNumeroPredialAnterior() != null) {
            this.numeroPredialAnterior[1] = this.predioInicialSeleccionado
                .getNumeroPredialAnterior();
        }
        tempValue = ComboBoxConverter.getDescripcion(
            "PredioTipoV", this.predioInicialSeleccionado.getTipo());
        tempValue = this.predioInicialSeleccionado.getTipo() + " - " + tempValue;
        this.tipoPredio[1] = tempValue;

        //D: el dato matrícula inmobiliaria es la concatenación de los datos
        //   círculo registral y número registro
        tempValue = (this.predioInicialSeleccionado.getCirculoRegistral() != null) ?
            this.predioInicialSeleccionado.getCirculoRegistral().getCodigo() : "";
        tempValue = (this.predioInicialSeleccionado.getNumeroRegistro() != null) ?
            (tempValue + this.predioInicialSeleccionado.getNumeroRegistro()) : (tempValue + "");
        this.matriculaInmobiliaria[1] = tempValue;

        this.estadoPredio[1] = this.predioInicialSeleccionado.getEstado();
        if (this.predioInicialSeleccionado.getLey14() == true) {
            this.predioLey14[1] = ESiNo.SI.getCodigo();
        } else if (this.predioInicialSeleccionado.getLey14() == false) {
            this.predioLey14[1] = ESiNo.NO.getCodigo();
        }

        tempValue = ComboBoxConverter.getDescripcion(
            "PredioDestinoV", this.predioInicialSeleccionado.getDestino());
        tempValue = this.predioInicialSeleccionado.getDestino() + " - " + tempValue;
        this.destinoPredio[1] = tempValue;

        this.departamentoPredio[1] = this.predioInicialSeleccionado
            .getDepartamento().getNombre();
        this.municipioPredio[1] = this.predioInicialSeleccionado.getMunicipio()
            .getNombre();
        if (this.predioInicialSeleccionado.getCorregimientoCodigo() != null) {
            this.corregimientoPredio[1] = this.predioInicialSeleccionado
                .getCorregimientoCodigo();
        }
        if (this.predioInicialSeleccionado.getLocalidadCodigo() != null) {
            this.localidadComunaPredio[1] = this.predioInicialSeleccionado
                .getLocalidadCodigo();
        }
        this.barrioVeredaPredio[1] = this.predioInicialSeleccionado
            .getBarrioCodigo();

        if (this.predioInicialSeleccionado != null &&
            this.predioFinalSeleccionado != null) {
            if (this.numeroPredial[1] != null && this.numeroPredial[2] != null &&
                !this.numeroPredial[1].equals(this.numeroPredial[2])) {
                this.numeroPredial[3] = this.imagenDatoModificado;
                this.numeroPredial[4] = this.imagenDatoModificado;
            } else {
                this.numeroPredial[3] = this.imagenDatoInicial;
                this.numeroPredial[4] = this.imagenDatoFinal;
            }

            if ((this.numeroPredialAnterior[1] != null &&
                this.numeroPredialAnterior[2] != null && !this.numeroPredialAnterior[1]
                    .equals(this.numeroPredialAnterior[2])) ||
                (this.numeroPredialAnterior[1] != null && this.numeroPredialAnterior[2] == null) ||
                (this.numeroPredialAnterior[1] == null && this.numeroPredialAnterior[2] != null)) {
                this.numeroPredialAnterior[3] = this.imagenDatoModificado;
                this.numeroPredialAnterior[4] = this.imagenDatoModificado;
            } else {
                this.numeroPredialAnterior[3] = this.imagenDatoInicial;
                this.numeroPredialAnterior[4] = this.imagenDatoFinal;
            }

            if (this.tipoPredio[1] != null && this.tipoPredio[2] != null &&
                !this.tipoPredio[1].equals(this.tipoPredio[2])) {
                this.tipoPredio[3] = this.imagenDatoModificado;
                this.tipoPredio[4] = this.imagenDatoModificado;
            } else {
                this.tipoPredio[3] = this.imagenDatoInicial;
                this.tipoPredio[4] = this.imagenDatoFinal;
            }

            if ((this.matriculaInmobiliaria[1] != null &&
                this.matriculaInmobiliaria[2] != null && !this.matriculaInmobiliaria[1]
                    .equals(this.matriculaInmobiliaria[2])) ||
                (this.matriculaInmobiliaria[1] != null && this.matriculaInmobiliaria[2] == null) ||
                (this.matriculaInmobiliaria[1] == null && this.matriculaInmobiliaria[2] != null)) {
                this.matriculaInmobiliaria[3] = this.imagenDatoModificado;
                this.matriculaInmobiliaria[4] = this.imagenDatoModificado;
            } else {
                this.matriculaInmobiliaria[3] = this.imagenDatoInicial;
                this.matriculaInmobiliaria[4] = this.imagenDatoFinal;
            }

            if (this.estadoPredio[1] != null && this.estadoPredio[2] != null &&
                !this.estadoPredio[1].equals(this.estadoPredio[2])) {
                this.estadoPredio[3] = this.imagenDatoModificado;
                this.estadoPredio[4] = this.imagenDatoModificado;
            } else {
                this.estadoPredio[3] = this.imagenDatoInicial;
                this.estadoPredio[4] = this.imagenDatoFinal;
            }

            if (this.predioLey14[1] != null && this.predioLey14[2] != null &&
                !this.predioLey14[1].equals(this.predioLey14[2])) {
                this.predioLey14[3] = this.imagenDatoModificado;
                this.predioLey14[4] = this.imagenDatoModificado;
            } else {
                this.predioLey14[3] = this.imagenDatoInicial;
                this.predioLey14[4] = this.imagenDatoFinal;
            }

            if (this.destinoPredio[1] != null && this.destinoPredio[2] != null &&
                !this.destinoPredio[1].equals(this.destinoPredio[2])) {
                this.destinoPredio[3] = this.imagenDatoModificado;
                this.destinoPredio[4] = this.imagenDatoModificado;
            } else {
                this.destinoPredio[3] = this.imagenDatoInicial;
                this.destinoPredio[4] = this.imagenDatoFinal;
            }

            if (this.departamentoPredio[1] != null &&
                this.departamentoPredio[2] != null &&
                !this.departamentoPredio[1]
                    .equals(this.departamentoPredio[2])) {
                this.departamentoPredio[3] = this.imagenDatoModificado;
                this.departamentoPredio[4] = this.imagenDatoModificado;
            } else {
                this.departamentoPredio[3] = this.imagenDatoInicial;
                this.departamentoPredio[4] = this.imagenDatoFinal;
            }

            if (this.municipioPredio[1] != null &&
                this.municipioPredio[2] != null &&
                !this.municipioPredio[1].equals(this.municipioPredio[2])) {
                this.municipioPredio[3] = this.imagenDatoModificado;
                this.municipioPredio[4] = this.imagenDatoModificado;
            } else {
                this.municipioPredio[3] = this.imagenDatoInicial;
                this.municipioPredio[4] = this.imagenDatoFinal;
            }

            if ((this.corregimientoPredio[1] != null &&
                this.corregimientoPredio[2] != null && !this.corregimientoPredio[1]
                    .equals(this.corregimientoPredio[2])) ||
                (this.corregimientoPredio[1] != null && this.corregimientoPredio[2] == null) ||
                (this.corregimientoPredio[1] == null && this.corregimientoPredio[2] != null)) {
                this.corregimientoPredio[3] = this.imagenDatoModificado;
                this.corregimientoPredio[4] = this.imagenDatoModificado;
            } else {
                this.corregimientoPredio[3] = this.imagenDatoInicial;
                this.corregimientoPredio[4] = this.imagenDatoFinal;
            }

            if ((this.localidadComunaPredio[1] != null &&
                this.localidadComunaPredio[2] != null && !this.localidadComunaPredio[1]
                    .equals(this.localidadComunaPredio[2])) ||
                (this.localidadComunaPredio[1] != null && this.localidadComunaPredio[2] == null) ||
                (this.localidadComunaPredio[1] == null && this.localidadComunaPredio[2] != null)) {
                this.localidadComunaPredio[3] = this.imagenDatoModificado;
                this.localidadComunaPredio[4] = this.imagenDatoModificado;
            } else {
                this.localidadComunaPredio[3] = this.imagenDatoInicial;
                this.localidadComunaPredio[4] = this.imagenDatoFinal;
            }

            if (this.barrioVeredaPredio[1] != null &&
                this.barrioVeredaPredio[2] != null &&
                !this.barrioVeredaPredio[1]
                    .equals(this.barrioVeredaPredio[2])) {
                this.barrioVeredaPredio[3] = this.imagenDatoModificado;
                this.barrioVeredaPredio[4] = this.imagenDatoModificado;
            } else {
                this.barrioVeredaPredio[3] = this.imagenDatoInicial;
                this.barrioVeredaPredio[4] = this.imagenDatoFinal;
            }
        } else {
            this.numeroPredial[3] = this.imagenDatoInicial;
            this.numeroPredialAnterior[3] = this.imagenDatoInicial;
            this.tipoPredio[3] = this.imagenDatoInicial;
            this.matriculaInmobiliaria[3] = this.imagenDatoInicial;
            this.estadoPredio[3] = this.imagenDatoInicial;
            this.predioLey14[3] = this.imagenDatoInicial;
            this.destinoPredio[3] = this.imagenDatoInicial;
            this.departamentoPredio[3] = this.imagenDatoInicial;
            this.municipioPredio[3] = this.imagenDatoInicial;
            this.corregimientoPredio[3] = this.imagenDatoInicial;
            this.localidadComunaPredio[3] = this.imagenDatoInicial;
            this.barrioVeredaPredio[3] = this.imagenDatoInicial;
        }

        almacenarDatosTablaDatosPredios();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para la selección de un predio inicial si el panel es detalle del avalúo
     */
    private void predioInicialDetalleAvaluo() {

        String tempValue;

//REVIEW :: pedro.garcia :: supuestamente ahora el predio tiene actualizado el valor del avalúo. Preguntar a Fredy
// Revisar también donde asigna this.valorTerrenoAvaluo[2]
        tempValue = MonedaConverter.getAsString(this.predioInicialSeleccionado
            .getPredioAvaluoCatastrals().get(0).getValorTerreno());
        this.valorTerrenoAvaluo[1] = tempValue;

        tempValue = MonedaConverter.getAsString(this.predioInicialSeleccionado
            .getPredioAvaluoCatastrals().get(0).getValorTotalConstruccion());
        this.valorTotalConstruccion[1] = tempValue;

        tempValue = MonedaConverter.getAsString(this.predioInicialSeleccionado
            .getPredioAvaluoCatastrals().get(0).getValorTotalConsConvencional());
        this.valorTotalConstruccionConvencional[1] = tempValue;

        if (this.predioInicialSeleccionado.getPredioAvaluoCatastrals().get(0)
            .getValorTotalConsNconvencional() != null) {

            tempValue = MonedaConverter.getAsString(this.predioInicialSeleccionado
                .getPredioAvaluoCatastrals().get(0).getValorTotalConsNconvencional());
            this.valorTotalConstruccionNoConvencional[1] = tempValue;
        }

        this.areaTotalTerreno[1] = funcionConvertirAHectareasMts(this.predioInicialSeleccionado.
            getAreaTerreno(), this.predioInicialSeleccionado.isPredioRural());
        this.areaTotalConstruccion[1] = this.predioInicialSeleccionado
            .getAreaTotalConstruccion() + " m2";
        if (this.predioInicialSeleccionado.getAreaTotalConsConvencional() != null) {
            this.areaTotalConstruccionConvencional[1] = this.predioInicialSeleccionado
                .getAreaTotalConsConvencional() + " m2";
        }
        if (this.predioInicialSeleccionado.getAreaTotalConsNoConvencional() != null) {
            this.areaTotalConstruccionNoConvencional[1] = this.predioInicialSeleccionado
                .getAreaTotalConsNoConvencional() + " m2";
        }

        this.construccionesConvencionales[1] = this.predioInicialSeleccionado
            .getUnidadesConstruccionConvencional().size() + "";
        this.construccionesConvencionales[2] = this.predioInicialSeleccionado
            .getAreaTotalConsConvencional() + "";
        this.construccionesConvencionales[3] = this.predioInicialSeleccionado
            .getAvaluoUnidadesConstruccionConvencional() + "";
        this.construccionesNoConvencionales[1] = this.predioInicialSeleccionado
            .getUnidadesConstruccionNoConvencional().size() + "";
        this.construccionesNoConvencionales[2] = this.predioInicialSeleccionado
            .getAreaTotalConsNoConvencional() + "";
        this.construccionesNoConvencionales[3] = this.predioInicialSeleccionado
            .getAvaluoUnidadesConstruccionNoConvencional() + "";

        if (this.predioInicialSeleccionado != null &&
            this.predioFinalSeleccionado != null) {

            if (this.valorTerrenoAvaluo[1] != null &&
                this.valorTerrenoAvaluo[2] != null &&
                !this.valorTerrenoAvaluo[1]
                    .equals(this.valorTerrenoAvaluo[2])) {
                this.valorTerrenoAvaluo[3] = this.imagenDatoModificado;
                this.valorTerrenoAvaluo[4] = this.imagenDatoModificado;
            } else {
                this.valorTerrenoAvaluo[3] = this.imagenDatoInicial;
                this.valorTerrenoAvaluo[4] = this.imagenDatoFinal;
            }

            if (this.valorTotalConstruccion[1] != null &&
                this.valorTotalConstruccion[2] != null &&
                !this.valorTotalConstruccion[1]
                    .equals(this.valorTotalConstruccion[2])) {
                this.valorTotalConstruccion[3] = this.imagenDatoModificado;
                this.valorTotalConstruccion[4] = this.imagenDatoModificado;
            } else {
                this.valorTotalConstruccion[3] = this.imagenDatoInicial;
                this.valorTotalConstruccion[4] = this.imagenDatoFinal;
            }

            if (this.valorTotalConstruccionConvencional[1] != null &&
                this.valorTotalConstruccionConvencional[2] != null &&
                !this.valorTotalConstruccionConvencional[1]
                    .equals(this.valorTotalConstruccionConvencional[2])) {
                this.valorTotalConstruccionConvencional[3] = this.imagenDatoModificado;
                this.valorTotalConstruccionConvencional[4] = this.imagenDatoModificado;
            } else {
                this.valorTotalConstruccionConvencional[3] = this.imagenDatoInicial;
                this.valorTotalConstruccionConvencional[4] = this.imagenDatoFinal;
            }

            if ((this.valorTotalConstruccionNoConvencional[1] != null &&
                this.valorTotalConstruccionNoConvencional[2] != null &&
                !this.valorTotalConstruccionNoConvencional[1]
                    .equals(this.valorTotalConstruccionNoConvencional[2])) ||
                (this.valorTotalConstruccionNoConvencional[1] != null &&
                this.valorTotalConstruccionNoConvencional[2] == null)) {
                this.valorTotalConstruccionNoConvencional[3] = this.imagenDatoModificado;
                this.valorTotalConstruccionNoConvencional[4] = this.imagenDatoModificado;
            } else {
                this.valorTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
                this.valorTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalTerreno[1] != null &&
                this.areaTotalTerreno[2] != null && !this.areaTotalTerreno[1]
                    .equals(this.areaTotalTerreno[2])) ||
                (this.areaTotalTerreno[1] != null && this.areaTotalTerreno[2] == null)) {
                this.areaTotalTerreno[3] = this.imagenDatoModificado;
                this.areaTotalTerreno[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalTerreno[3] = this.imagenDatoInicial;
                this.areaTotalTerreno[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalConstruccion[1] != null &&
                this.areaTotalConstruccion[2] != null && !this.areaTotalConstruccion[1]
                    .equals(this.areaTotalConstruccion[2])) ||
                (this.areaTotalConstruccion[1] != null && this.areaTotalConstruccion[2] == null) ||
                (this.areaTotalConstruccion[1] == null && this.areaTotalConstruccion[2] != null)) {
                this.areaTotalConstruccion[3] = this.imagenDatoModificado;
                this.areaTotalConstruccion[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalConstruccion[3] = this.imagenDatoInicial;
                this.areaTotalConstruccion[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalConstruccionConvencional[1] != null &&
                this.areaTotalConstruccionConvencional[2] != null &&
                !this.areaTotalConstruccionConvencional[1]
                    .equals(this.areaTotalConstruccionConvencional[2])) ||
                (this.areaTotalConstruccionConvencional[1] != null &&
                this.areaTotalConstruccionConvencional[2] == null) ||
                (this.areaTotalConstruccionConvencional[1] == null &&
                this.areaTotalConstruccionConvencional[2] != null)) {
                this.areaTotalConstruccionConvencional[3] = this.imagenDatoModificado;
                this.areaTotalConstruccionConvencional[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalConstruccionConvencional[3] = this.imagenDatoInicial;
                this.areaTotalConstruccionConvencional[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalConstruccionNoConvencional[1] != null &&
                this.areaTotalConstruccionNoConvencional[2] != null &&
                !this.areaTotalConstruccionNoConvencional[1]
                    .equals(this.areaTotalConstruccionNoConvencional[2])) ||
                (this.areaTotalConstruccionNoConvencional[1] != null &&
                this.areaTotalConstruccionNoConvencional[2] == null) ||
                (this.areaTotalConstruccionNoConvencional[1] == null &&
                this.areaTotalConstruccionNoConvencional[2] != null)) {
                this.areaTotalConstruccionNoConvencional[3] = this.imagenDatoModificado;
                this.areaTotalConstruccionNoConvencional[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
                this.areaTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;
            }

            if (((this.construccionesConvencionales[1] != null &&
                this.construccionesConvencionalesP[1] != null &&
                !this.construccionesConvencionales[1].equals(this.construccionesConvencionalesP[1])) ||
                (this.construccionesConvencionales[1] != null &&
                this.construccionesConvencionalesP[1] == null) ||
                (this.construccionesConvencionales[1] == null &&
                this.construccionesConvencionalesP[1] != null)) ||
                ((this.construccionesConvencionales[2] != null &&
                this.construccionesConvencionalesP[2] != null &&
                !this.construccionesConvencionales[2].equals(this.construccionesConvencionalesP[2])) ||
                (this.construccionesConvencionales[2] != null &&
                this.construccionesConvencionalesP[2] == null) ||
                (this.construccionesConvencionales[2] == null &&
                this.construccionesConvencionalesP[2] != null)) ||
                (((this.construccionesConvencionales[3] != null &&
                this.construccionesConvencionalesP[3] != null &&
                !this.construccionesConvencionales[3].equals(this.construccionesConvencionalesP[3])) ||
                (this.construccionesConvencionales[3] != null &&
                this.construccionesConvencionalesP[3] == null) ||
                (this.construccionesConvencionales[3] == null &&
                this.construccionesConvencionalesP[3] != null)))) {

                this.construccionesConvencionales[4] = this.imagenDatoModificado;
                this.construccionesConvencionalesP[4] = this.imagenDatoModificado;
            } else {
                this.construccionesConvencionales[4] = this.imagenDatoInicial;
                this.construccionesConvencionalesP[4] = this.imagenDatoInicial;
            }

            if (((this.construccionesNoConvencionales[1] != null &&
                this.construccionesNoConvencionalesP[1] != null &&
                !this.construccionesNoConvencionales[1]
                    .equals(this.construccionesNoConvencionalesP[1])) ||
                (this.construccionesNoConvencionales[1] != null &&
                this.construccionesNoConvencionalesP[1] == null) ||
                (this.construccionesNoConvencionales[1] == null &&
                this.construccionesNoConvencionalesP[1] != null)) ||
                (((this.construccionesNoConvencionales[2] != null &&
                this.construccionesNoConvencionalesP[2] != null &&
                !this.construccionesNoConvencionales[2]
                    .equals(this.construccionesNoConvencionalesP[2])) ||
                (this.construccionesNoConvencionales[2] != null &&
                this.construccionesNoConvencionalesP[2] == null) ||
                (this.construccionesNoConvencionales[2] == null &&
                this.construccionesNoConvencionalesP[2] != null))) ||
                (((this.construccionesNoConvencionales[3] != null &&
                this.construccionesNoConvencionalesP[3] != null &&
                !this.construccionesNoConvencionales[3]
                    .equals(this.construccionesNoConvencionalesP[3])) ||
                (this.construccionesNoConvencionales[3] != null &&
                this.construccionesNoConvencionalesP[3] == null) ||
                (this.construccionesNoConvencionales[3] == null &&
                this.construccionesNoConvencionalesP[3] != null)))) {
                this.construccionesNoConvencionales[4] = this.imagenDatoModificado;
                this.construccionesNoConvencionalesP[4] = this.imagenDatoModificado;
            } else {
                this.construccionesNoConvencionales[4] = this.imagenDatoInicial;
                this.construccionesNoConvencionalesP[4] = this.imagenDatoInicial;
            }

        } else {
            this.valorTerrenoAvaluo[3] = this.imagenDatoInicial;
            this.valorTotalConstruccion[3] = this.imagenDatoInicial;
            this.valorTotalConstruccionConvencional[3] = this.imagenDatoInicial;
            this.valorTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
            this.areaTotalTerreno[3] = this.imagenDatoInicial;
            this.areaTotalConstruccion[3] = this.imagenDatoInicial;
            this.areaTotalConstruccionConvencional[3] = this.imagenDatoInicial;
            this.areaTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
            this.construccionesConvencionales[4] = this.imagenDatoInicial;
            this.construccionesConvencionales[5] = this.imagenDatoInicial;
            this.construccionesConvencionales[6] = this.imagenDatoInicial;
            this.construccionesNoConvencionales[4] = this.imagenDatoInicial;
            this.construccionesNoConvencionales[5] = this.imagenDatoInicial;
            this.construccionesNoConvencionales[6] = this.imagenDatoInicial;

        }

        almacenarDatosTablaDatosPredios();

    }

    /**
     * Método para convertir a hectareas el area en metros cuadrados
     *
     * @param areaTerreno
     * @return
     */
    private String funcionConvertirAHectareas(Double areaTerreno) {
        double areaTotal = (areaTerreno * 0.0001);
        return Double.toString(areaTotal);
    }

    private String funcionConvertirAHectareasMts(Double area, boolean isRural) {
        if (area == null) {
            area = 0.0;
        }
        if (isRural) {
            Integer ha = (int) (area / 10000);
            double mts = area - ha * 10000;
            return (ha > 0 ? ha + " ha " : " ") + (mts > 0 ? mts + " m2 " : "");
        } else {
            return area.toString() + " m2";
        }
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Método para la desselección de un predio inicial
     */
    public void predioFinaldeSeleccionado(UnselectEvent un) {
        this.predioFinalSeleccionado = null;

        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            pFdesUbicacionPredio();

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            pFdesAvaluo();
        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.CONDICION_DE_PROPIEDAD.getId())) {

            this.tablaJustificacionPPropiedad = null;

        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.FICHA_MATRIZ.getId())) {

            this.ppredioUnidadSeleccionado = null;

        }

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al realizar una deselección del predio final en el panel correspondiente a
     * ubicación del predio
     */
    private void pFdesUbicacionPredio() {
        this.numeroPredial[2] = null;
        this.numeroPredialAnterior[2] = null;
        this.tipoPredio[2] = null;
        this.matriculaInmobiliaria[2] = null;
        this.estadoPredio[2] = null;
        this.predioLey14[2] = null;
        this.destinoPredio[2] = null;
        this.departamentoPredio[2] = null;
        this.municipioPredio[2] = null;
        this.corregimientoPredio[2] = null;
        this.localidadComunaPredio[2] = null;
        this.barrioVeredaPredio[2] = null;

        this.numeroPredial[4] = null;
        this.numeroPredialAnterior[4] = null;
        this.tipoPredio[4] = null;
        this.matriculaInmobiliaria[4] = null;
        this.estadoPredio[4] = null;
        this.predioLey14[4] = null;
        this.destinoPredio[4] = null;
        this.departamentoPredio[4] = null;
        this.municipioPredio[4] = null;
        this.corregimientoPredio[4] = null;
        this.localidadComunaPredio[4] = null;
        this.barrioVeredaPredio[4] = null;

        this.numeroPredial[3] = this.imagenDatoInicial;
        this.numeroPredialAnterior[3] = this.imagenDatoInicial;
        this.tipoPredio[3] = this.imagenDatoInicial;
        this.matriculaInmobiliaria[3] = this.imagenDatoInicial;
        this.estadoPredio[3] = this.imagenDatoInicial;
        this.predioLey14[3] = this.imagenDatoInicial;
        this.destinoPredio[3] = this.imagenDatoInicial;
        this.departamentoPredio[3] = this.imagenDatoInicial;
        this.municipioPredio[3] = this.imagenDatoInicial;
        this.corregimientoPredio[3] = this.imagenDatoInicial;
        this.localidadComunaPredio[3] = this.imagenDatoInicial;
        this.barrioVeredaPredio[3] = this.imagenDatoInicial;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al realizar una deselección del predio final en el panel correspondiente a
     * avalúo
     */
    private void pFdesAvaluo() {

        this.valorTerrenoAvaluo[2] = null;
        this.valorTotalConstruccion[2] = null;
        this.valorTotalConstruccionConvencional[2] = null;
        this.valorTotalConstruccionNoConvencional[2] = null;
        this.areaTotalTerreno[2] = null;
        this.areaTotalConstruccion[2] = null;
        this.areaTotalConstruccionConvencional[2] = null;
        this.areaTotalConstruccionNoConvencional[2] = null;

        this.valorTerrenoAvaluo[4] = null;
        this.valorTotalConstruccion[4] = null;
        this.valorTotalConstruccionConvencional[4] = null;
        this.valorTotalConstruccionNoConvencional[4] = null;
        this.areaTotalTerreno[4] = null;
        this.areaTotalConstruccion[4] = null;
        this.areaTotalConstruccionConvencional[4] = null;
        this.areaTotalConstruccionNoConvencional[4] = null;

        this.valorTerrenoAvaluo[3] = this.imagenDatoInicial;
        this.valorTotalConstruccion[3] = this.imagenDatoInicial;
        this.valorTotalConstruccionConvencional[3] = this.imagenDatoInicial;
        this.valorTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
        this.areaTotalTerreno[3] = this.imagenDatoInicial;
        this.areaTotalConstruccion[3] = this.imagenDatoInicial;
        this.areaTotalConstruccionConvencional[3] = this.imagenDatoInicial;
        this.areaTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;

        for (int i = 0; i < construccionesConvencionalesP.length; i++) {
            this.construccionesConvencionalesP[i] = null;
        }

        for (int i = 0; i < construccionesNoConvencionalesP.length; i++) {
            this.construccionesNoConvencionalesP[i] = null;
        }

        for (int i = 4; i < construccionesConvencionales.length; i++) {
            this.construccionesConvencionales[i] = this.imagenDatoInicial;
        }

        for (int i = 4; i < construccionesNoConvencionales.length; i++) {
            this.construccionesNoConvencionales[i] = this.imagenDatoInicial;
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método del evento selección de un predio final (de la tabla de "predios proyectados")
     */
    /*
     * @documented pedro.garcia @modified pedro.garcia 06-08-2012. El autor chambón hacía el llamado
     * a predioFinalActualizar() desde predioFinalCargar(), y luego aquí otra vez. Se dejó el
     * primero
     */
    public void predioFinalSeleccionar(SelectEvent se) {
//TODO :: pedro.garcia refs #6050 :: eliminar lo comentado (basura de Gabriel) cuando pase pruebas
//		predioInicialCargar();

        predioFinalCargar();

//		if (this.predioFinalSeleccionado != null) {
//			predioFinalActualizar();
//		}
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método para la selección de un predio final
     */
    private void predioFinalActualizar() {

        if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.UBICACION_DEL_PREDIO.getId())) {

            pFdatosUbicacionPredio();
        } else if (this.idPanelDetallePredios
            .equals(EIdPanelsInfoCancelaInscribe.DETALLE_DEL_AVALUO.getId())) {

            pFdatosAvaluo();
        }
    }
//---------------------------------------------------------------------------------------------

    /**
     * Método invocado al seleccionar un predio final en el panel correspondiente a ubicación del
     * predio para llenar los datos que se muestran en la tabla en la columna 'predio proyectado'
     */
    /*
     * @documented pedro.garcia
     */
    private void pFdatosUbicacionPredio() {

        String tempValue;

        this.numeroPredial[2] = this.predioFinalSeleccionado.getFormattedNumeroPredialCompleto();
        if (this.predioFinalSeleccionado.getNumeroPredialAnterior() != null) {
            this.numeroPredialAnterior[2] = this.predioFinalSeleccionado.getNumeroPredialAnterior();
        }

        tempValue = ComboBoxConverter.getDescripcion(
            "PredioTipoV", this.predioFinalSeleccionado.getTipo());
        tempValue = this.predioFinalSeleccionado.getTipo() + " - " + tempValue;
        this.tipoPredio[2] = tempValue;

        //D: el dato matrícula inmobiliaria es la concatenación de los datos
        //   círculo registral y número registro
        tempValue = (this.predioFinalSeleccionado.getCirculoRegistral() != null) ?
            this.predioFinalSeleccionado.getCirculoRegistral().getCodigo() : "";
        tempValue = (this.predioFinalSeleccionado.getNumeroRegistro() != null) ?
            (tempValue + this.predioFinalSeleccionado.getNumeroRegistro()) : (tempValue + "");
        this.matriculaInmobiliaria[2] = tempValue;

        this.estadoPredio[2] = this.predioFinalSeleccionado.getEstado();
        if (this.predioFinalSeleccionado.getLey14() == true) {
            this.predioLey14[2] = ESiNo.SI.getCodigo();
        } else if (this.predioFinalSeleccionado.getLey14() == false) {
            this.predioLey14[2] = ESiNo.NO.getCodigo();
        }

        tempValue = ComboBoxConverter.getDescripcion(
            "PredioDestinoV", this.predioFinalSeleccionado.getDestino());
        tempValue = this.predioFinalSeleccionado.getDestino() + " - " + tempValue;
        this.destinoPredio[2] = tempValue;

        this.departamentoPredio[2] = this.predioFinalSeleccionado
            .getDepartamento().getNombre();
        this.municipioPredio[2] = this.predioFinalSeleccionado.getMunicipio().getNombre();
        if (this.predioFinalSeleccionado.getCorregimientoCodigo() != null) {
            this.corregimientoPredio[2] = this.predioFinalSeleccionado.getCorregimientoCodigo();
        }
        if (this.predioFinalSeleccionado.getLocalidadCodigo() != null) {
            this.localidadComunaPredio[2] = this.predioFinalSeleccionado.getLocalidadCodigo();
        }
        this.barrioVeredaPredio[2] = this.predioFinalSeleccionado
            .getBarrioCodigo();

        if (this.predioInicialSeleccionado != null &&
            this.predioFinalSeleccionado != null) {

            if (this.numeroPredial[1] != null && this.numeroPredial[2] != null &&
                !this.numeroPredial[1].equals(this.numeroPredial[2])) {
                this.numeroPredial[3] = this.imagenDatoModificado;
                this.numeroPredial[4] = this.imagenDatoModificado;
            } else {
                this.numeroPredial[3] = this.imagenDatoInicial;
                this.numeroPredial[4] = this.imagenDatoFinal;
            }

            if ((this.numeroPredialAnterior[1] != null &&
                this.numeroPredialAnterior[2] != null && !this.numeroPredialAnterior[1]
                    .equals(this.numeroPredialAnterior[2])) ||
                (this.numeroPredialAnterior[1] != null && this.numeroPredialAnterior[2] == null) ||
                (this.numeroPredialAnterior[1] == null && this.numeroPredialAnterior[2] != null)) {
                this.numeroPredialAnterior[3] = this.imagenDatoModificado;
                this.numeroPredialAnterior[4] = this.imagenDatoModificado;
            } else {
                this.numeroPredialAnterior[3] = this.imagenDatoInicial;
                this.numeroPredialAnterior[4] = this.imagenDatoFinal;
            }

            if (this.tipoPredio[1] != null && this.tipoPredio[2] != null &&
                !this.tipoPredio[1].equals(this.tipoPredio[2])) {
                this.tipoPredio[3] = this.imagenDatoModificado;
                this.tipoPredio[4] = this.imagenDatoModificado;
            } else {
                this.tipoPredio[3] = this.imagenDatoInicial;
                this.tipoPredio[4] = this.imagenDatoFinal;
            }

            if ((this.matriculaInmobiliaria[1] != null &&
                this.matriculaInmobiliaria[2] != null && !this.matriculaInmobiliaria[1]
                    .equals(this.matriculaInmobiliaria[2])) ||
                (this.matriculaInmobiliaria[1] != null && this.matriculaInmobiliaria[2] == null) ||
                (this.matriculaInmobiliaria[1] == null && this.matriculaInmobiliaria[2] != null)) {
                this.matriculaInmobiliaria[3] = this.imagenDatoModificado;
                this.matriculaInmobiliaria[4] = this.imagenDatoModificado;
            } else {
                this.matriculaInmobiliaria[3] = this.imagenDatoInicial;
                this.matriculaInmobiliaria[4] = this.imagenDatoFinal;
            }

            if (this.estadoPredio[1] != null && this.estadoPredio[2] != null &&
                !this.estadoPredio[1].equals(this.estadoPredio[2])) {
                this.estadoPredio[3] = this.imagenDatoModificado;
                this.estadoPredio[4] = this.imagenDatoModificado;
            } else {
                this.estadoPredio[3] = this.imagenDatoInicial;
                this.estadoPredio[4] = this.imagenDatoFinal;
            }

            if (this.predioLey14[1] != null && this.predioLey14[2] != null &&
                !this.predioLey14[1].equals(this.predioLey14[2])) {
                this.predioLey14[3] = this.imagenDatoModificado;
                this.predioLey14[4] = this.imagenDatoModificado;
            } else {
                this.predioLey14[3] = this.imagenDatoInicial;
                this.predioLey14[4] = this.imagenDatoFinal;
            }

            if (this.destinoPredio[1] != null && this.destinoPredio[2] != null &&
                !this.destinoPredio[1].equals(this.destinoPredio[2])) {
                this.destinoPredio[3] = this.imagenDatoModificado;
                this.destinoPredio[4] = this.imagenDatoModificado;
            } else {
                this.destinoPredio[3] = this.imagenDatoInicial;
                this.destinoPredio[4] = this.imagenDatoFinal;
            }

            if (this.departamentoPredio[1] != null &&
                this.departamentoPredio[2] != null &&
                !this.departamentoPredio[1].equals(this.departamentoPredio[2])) {
                this.departamentoPredio[3] = this.imagenDatoModificado;
                this.departamentoPredio[4] = this.imagenDatoModificado;
            } else {
                this.departamentoPredio[3] = this.imagenDatoInicial;
                this.departamentoPredio[4] = this.imagenDatoFinal;
            }

            if (this.municipioPredio[1] != null &&
                this.municipioPredio[2] != null &&
                !this.municipioPredio[1].equals(this.municipioPredio[2])) {
                this.municipioPredio[3] = this.imagenDatoModificado;
                this.municipioPredio[4] = this.imagenDatoModificado;
            } else {
                this.municipioPredio[3] = this.imagenDatoInicial;
                this.municipioPredio[4] = this.imagenDatoFinal;
            }

            if ((this.corregimientoPredio[1] != null &&
                this.corregimientoPredio[2] != null && !this.corregimientoPredio[1]
                    .equals(this.corregimientoPredio[2])) ||
                (this.corregimientoPredio[1] != null && this.corregimientoPredio[2] == null) ||
                (this.corregimientoPredio[1] == null && this.corregimientoPredio[2] != null)) {
                this.corregimientoPredio[3] = this.imagenDatoModificado;
                this.corregimientoPredio[4] = this.imagenDatoModificado;
            } else {
                this.corregimientoPredio[3] = this.imagenDatoInicial;
                this.corregimientoPredio[4] = this.imagenDatoFinal;
            }

            if ((this.localidadComunaPredio[1] != null &&
                this.localidadComunaPredio[2] != null && !this.localidadComunaPredio[1]
                    .equals(this.localidadComunaPredio[2])) ||
                (this.localidadComunaPredio[1] != null && this.localidadComunaPredio[2] == null) ||
                (this.localidadComunaPredio[1] == null && this.localidadComunaPredio[2] != null)) {
                this.localidadComunaPredio[3] = this.imagenDatoModificado;
                this.localidadComunaPredio[4] = this.imagenDatoModificado;
            } else {
                this.localidadComunaPredio[3] = this.imagenDatoInicial;
                this.localidadComunaPredio[4] = this.imagenDatoFinal;
            }

            if (this.barrioVeredaPredio[1] != null &&
                this.barrioVeredaPredio[2] != null &&
                !this.barrioVeredaPredio[1]
                    .equals(this.barrioVeredaPredio[2])) {
                this.barrioVeredaPredio[3] = this.imagenDatoModificado;
                this.barrioVeredaPredio[4] = this.imagenDatoModificado;
            } else {
                this.barrioVeredaPredio[3] = this.imagenDatoInicial;
                this.barrioVeredaPredio[4] = this.imagenDatoFinal;
            }
        } else {
            this.numeroPredial[4] = this.imagenDatoFinal;
            this.numeroPredialAnterior[4] = this.imagenDatoFinal;
            this.tipoPredio[4] = this.imagenDatoFinal;
            this.matriculaInmobiliaria[4] = this.imagenDatoFinal;
            this.estadoPredio[4] = this.imagenDatoFinal;
            this.predioLey14[4] = this.imagenDatoFinal;
            this.destinoPredio[4] = this.imagenDatoFinal;
            this.departamentoPredio[4] = this.imagenDatoFinal;
            this.municipioPredio[4] = this.imagenDatoFinal;
            this.corregimientoPredio[4] = this.imagenDatoFinal;
            this.localidadComunaPredio[4] = this.imagenDatoFinal;
            this.barrioVeredaPredio[4] = this.imagenDatoFinal;
        }

        almacenarDatosTablaDatosPredios();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método invocado al seleccionar un predio final. Llena los datos que se van a ver en el panel
     * 'detalle del avalúo' para el predio proyectado
     */
    /*
     * @documented pedro.garcia
     *
     * @modified by juanfelipe.garcia 07-05-2013 En el detalle de los datos de las construciones
     * solo debe mostrar las construcciones que en el campo CANCELA_INSCRIBE tiene una M o I
     * @modified by leidy.gonzalez se agrega validaciones para cuando el preedio final seleccionado
     * no tenga Unidades de Construccion asociadas
     */
    private void pFdatosAvaluo() {

        String tempValue;
        DecimalFormat numFormat = new DecimalFormat("#,###.00");

        tempValue = MonedaConverter.getAsString(this.predioFinalSeleccionado
            .getPPredioAvaluoCatastrals().get(0).getValorTerreno());
        this.valorTerrenoAvaluo[2] = tempValue;

        tempValue = MonedaConverter.getAsString(this.predioFinalSeleccionado
            .getPPredioAvaluoCatastrals().get(0).getValorTotalConstruccion());
        this.valorTotalConstruccion[2] = tempValue;

        tempValue = MonedaConverter.getAsString(this.predioFinalSeleccionado
            .getPPredioAvaluoCatastrals().get(0).getValorTotalConsConvencional());
        this.valorTotalConstruccionConvencional[2] = tempValue;

        if (this.predioFinalSeleccionado.getPPredioAvaluoCatastrals().get(0)
            .getValorTotalConsNconvencional() != null) {

            tempValue = MonedaConverter.getAsString(this.predioFinalSeleccionado
                .getPPredioAvaluoCatastrals().get(0).getValorTotalConsNconvencional());
            this.valorTotalConstruccionNoConvencional[2] = tempValue;
        }

        this.areaTotalTerreno[2] = funcionConvertirAHectareasMts(this.predioFinalSeleccionado.
            getAreaTerreno(), this.predioFinalSeleccionado.isPredioRural());
        if (this.predioFinalSeleccionado.getAreaTotalConstruccion() != null) {
            this.areaTotalConstruccion[2] = this.predioFinalSeleccionado
                .getAreaTotalConstruccion() + " m2";
        }
        if (this.predioFinalSeleccionado.getAreaTotalConsConvencional() != null) {
            this.areaTotalConstruccionConvencional[2] = this.predioFinalSeleccionado
                .getAreaTotalConsConvencional() + " m2";
        }
        if (this.predioFinalSeleccionado.getAreaTotalConsNoConvencional() != null) {
            this.areaTotalConstruccionNoConvencional[2] = this.predioFinalSeleccionado
                .getAreaTotalConsNoConvencional() + " m2";
        }
        //solo debe mostrar las construcciones que en el campo CANCELA_INSCRIBE tiene una M o I
        int construccionesC = 0;
        //v1.1.11
        if (this.predioFinalSeleccionado.getPUnidadesConstruccionConvencional() != null) {
            for (PUnidadConstruccion pUnidadC : this.predioFinalSeleccionado.
                getPUnidadesConstruccionConvencional()) {
                if (pUnidadC.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    pUnidadC.getCancelaInscribe()
                        .equals(EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo())) {
                    construccionesC++;
                }
            }
        }
        /* this.construccionesConvencionalesP[1] = this.predioFinalSeleccionado
         * .getPUnidadesConstruccionConvencional().size() + ""; */
        this.construccionesConvencionalesP[1] = construccionesC + "";
        if (this.predioFinalSeleccionado.getPUnidadesConstruccionConvencional() != null &&
            !this.predioFinalSeleccionado.getPUnidadesConstruccionConvencional().isEmpty()) {
            this.predioFinalSeleccionado.getPUnidadesConstruccionConvencional().get(0).
                getCancelaInscribe();
        }
        this.construccionesConvencionalesP[2] = this.predioFinalSeleccionado.
            getAreaTotalConsConvencional() + "";
        this.construccionesConvencionalesP[3] = UtilidadesWeb.formatNumber(
            this.predioFinalSeleccionado
                .getAvaluoPUnidadesConstruccionConvencional()) + "";

        int construccionesNC = 0;
        if (this.predioFinalSeleccionado.getPUnidadesConstruccionNoConvencional() != null) {
            for (PUnidadConstruccion pUnidadNC : this.predioFinalSeleccionado.
                getPUnidadesConstruccionNoConvencional()) {
                if (pUnidadNC.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.MODIFICA.getCodigo()) ||
                    pUnidadNC.getCancelaInscribe()
                        .equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                    construccionesNC++;
                }
            }
        }
        /* this.construccionesNoConvencionalesP[1] = this.predioFinalSeleccionado
         * .getPUnidadesConstruccionNoConvencional().size() + ""; */
        this.construccionesNoConvencionalesP[1] = construccionesNC + "";
        this.construccionesNoConvencionalesP[2] = this.predioFinalSeleccionado
            .getAreaTotalConsNoConvencional() + "";
        this.construccionesNoConvencionalesP[3] = UtilidadesWeb.formatNumber(
            this.predioFinalSeleccionado
                .getAvaluoPUnidadesConstruccionNoConvencional()) + "";

        if (this.predioInicialSeleccionado != null &&
            this.predioFinalSeleccionado != null) {

            if (this.valorTerrenoAvaluo[1] != null &&
                this.valorTerrenoAvaluo[2] != null &&
                !this.valorTerrenoAvaluo[1]
                    .equals(this.valorTerrenoAvaluo[2])) {
                this.valorTerrenoAvaluo[3] = this.imagenDatoModificado;
                this.valorTerrenoAvaluo[4] = this.imagenDatoModificado;
            } else {
                this.valorTerrenoAvaluo[3] = this.imagenDatoInicial;
                this.valorTerrenoAvaluo[4] = this.imagenDatoFinal;
            }

            if (this.valorTotalConstruccion[1] != null &&
                this.valorTotalConstruccion[2] != null &&
                !this.valorTotalConstruccion[1]
                    .equals(this.valorTotalConstruccion[2])) {
                this.valorTotalConstruccion[3] = this.imagenDatoModificado;
                this.valorTotalConstruccion[4] = this.imagenDatoModificado;
            } else {
                this.valorTotalConstruccion[3] = this.imagenDatoInicial;
                this.valorTotalConstruccion[4] = this.imagenDatoFinal;
            }

            if (this.valorTotalConstruccionConvencional[1] != null &&
                this.valorTotalConstruccionConvencional[2] != null &&
                !this.valorTotalConstruccionConvencional[1]
                    .equals(this.valorTotalConstruccionConvencional[2])) {
                this.valorTotalConstruccionConvencional[3] = this.imagenDatoModificado;
                this.valorTotalConstruccionConvencional[4] = this.imagenDatoModificado;
            } else {
                this.valorTotalConstruccionConvencional[3] = this.imagenDatoInicial;
                this.valorTotalConstruccionConvencional[4] = this.imagenDatoFinal;
            }

            if ((this.valorTotalConstruccionNoConvencional[1] != null &&
                this.valorTotalConstruccionNoConvencional[2] != null &&
                !this.valorTotalConstruccionNoConvencional[1]
                    .equals(this.valorTotalConstruccionNoConvencional[2])) ||
                (this.valorTotalConstruccionNoConvencional[1] != null &&
                this.valorTotalConstruccionNoConvencional[2] == null)) {
                this.valorTotalConstruccionNoConvencional[3] = this.imagenDatoModificado;
                this.valorTotalConstruccionNoConvencional[4] = this.imagenDatoModificado;
            } else {
                this.valorTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
                this.valorTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalTerreno[1] != null &&
                this.areaTotalTerreno[2] != null && !this.areaTotalTerreno[1]
                    .equals(this.areaTotalTerreno[2])) ||
                (this.areaTotalTerreno[1] != null && this.areaTotalTerreno[2] == null)) {
                this.areaTotalTerreno[3] = this.imagenDatoModificado;
                this.areaTotalTerreno[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalTerreno[3] = this.imagenDatoInicial;
                this.areaTotalTerreno[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalConstruccion[1] != null &&
                this.areaTotalConstruccion[2] != null && !this.areaTotalConstruccion[1]
                    .equals(this.areaTotalConstruccion[2])) ||
                (this.areaTotalConstruccion[1] != null && this.areaTotalConstruccion[2] == null) ||
                (this.areaTotalConstruccion[1] == null && this.areaTotalConstruccion[2] != null)) {
                this.areaTotalConstruccion[3] = this.imagenDatoModificado;
                this.areaTotalConstruccion[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalConstruccion[3] = this.imagenDatoInicial;
                this.areaTotalConstruccion[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalConstruccionConvencional[1] != null &&
                this.areaTotalConstruccionConvencional[2] != null &&
                !this.areaTotalConstruccionConvencional[1]
                    .equals(this.areaTotalConstruccionConvencional[2])) ||
                (this.areaTotalConstruccionConvencional[1] != null &&
                this.areaTotalConstruccionConvencional[2] == null) ||
                (this.areaTotalConstruccionConvencional[1] == null &&
                this.areaTotalConstruccionConvencional[2] != null)) {
                this.areaTotalConstruccionConvencional[3] = this.imagenDatoModificado;
                this.areaTotalConstruccionConvencional[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalConstruccionConvencional[3] = this.imagenDatoInicial;
                this.areaTotalConstruccionConvencional[4] = this.imagenDatoFinal;
            }

            if ((this.areaTotalConstruccionNoConvencional[1] != null &&
                this.areaTotalConstruccionNoConvencional[2] != null &&
                !this.areaTotalConstruccionNoConvencional[1]
                    .equals(this.areaTotalConstruccionNoConvencional[2])) ||
                (this.areaTotalConstruccionNoConvencional[1] != null &&
                this.areaTotalConstruccionNoConvencional[2] == null) ||
                (this.areaTotalConstruccionNoConvencional[1] == null &&
                this.areaTotalConstruccionNoConvencional[2] != null)) {
                this.areaTotalConstruccionNoConvencional[3] = this.imagenDatoModificado;
                this.areaTotalConstruccionNoConvencional[4] = this.imagenDatoModificado;
            } else {
                this.areaTotalConstruccionNoConvencional[3] = this.imagenDatoInicial;
                this.areaTotalConstruccionNoConvencional[4] = this.imagenDatoInicial;
            }

            if (((this.construccionesConvencionales[1] != null &&
                this.construccionesConvencionalesP[1] != null &&
                !this.construccionesConvencionales[1]
                    .equals(this.construccionesConvencionalesP[1])) ||
                (this.construccionesConvencionales[1] != null &&
                this.construccionesConvencionalesP[1] == null) ||
                (this.construccionesConvencionales[1] == null &&
                this.construccionesConvencionalesP[1] != null)) ||
                (((this.construccionesConvencionales[2] != null &&
                this.construccionesConvencionalesP[2] != null &&
                !this.construccionesConvencionales[2]
                    .equals(this.construccionesConvencionalesP[2])) ||
                (this.construccionesConvencionales[2] != null &&
                this.construccionesConvencionalesP[2] == null) ||
                (this.construccionesConvencionales[2] == null &&
                this.construccionesConvencionalesP[2] != null))) ||
                (((this.construccionesConvencionales[3] != null &&
                this.construccionesConvencionalesP[3] != null &&
                !this.construccionesConvencionales[3]
                    .equals(this.construccionesConvencionalesP[3])) ||
                (this.construccionesConvencionales[3] != null &&
                this.construccionesConvencionalesP[3] == null) ||
                (this.construccionesConvencionales[3] == null &&
                this.construccionesConvencionalesP[3] != null)))) {
                this.construccionesConvencionales[4] = this.imagenDatoModificado;
                this.construccionesConvencionalesP[4] = this.imagenDatoModificado;
            } else {
                this.construccionesConvencionales[4] = this.imagenDatoInicial;
                this.construccionesConvencionalesP[4] = this.imagenDatoInicial;
            }

            if (((this.construccionesNoConvencionales[1] != null &&
                this.construccionesNoConvencionalesP[1] != null &&
                !this.construccionesNoConvencionales[1]
                    .equals(this.construccionesNoConvencionalesP[1])) ||
                (this.construccionesNoConvencionales[1] != null &&
                this.construccionesNoConvencionalesP[1] == null) ||
                (this.construccionesNoConvencionales[1] == null &&
                this.construccionesNoConvencionalesP[1] != null)) ||
                (((this.construccionesNoConvencionales[2] != null &&
                this.construccionesNoConvencionalesP[2] != null &&
                !this.construccionesNoConvencionales[2]
                    .equals(this.construccionesNoConvencionalesP[2])) ||
                (this.construccionesNoConvencionales[2] != null &&
                this.construccionesNoConvencionalesP[2] == null) ||
                (this.construccionesNoConvencionales[2] == null &&
                this.construccionesNoConvencionalesP[2] != null))) ||
                (((this.construccionesNoConvencionales[3] != null &&
                this.construccionesNoConvencionalesP[3] != null &&
                !this.construccionesNoConvencionales[3]
                    .equals(this.construccionesNoConvencionalesP[3])) ||
                (this.construccionesNoConvencionales[3] != null &&
                this.construccionesNoConvencionalesP[3] == null) ||
                (this.construccionesNoConvencionales[3] == null &&
                this.construccionesNoConvencionalesP[3] != null)))) {
                this.construccionesNoConvencionales[4] = this.imagenDatoModificado;
                this.construccionesNoConvencionalesP[4] = this.imagenDatoModificado;
            } else {
                this.construccionesNoConvencionales[4] = this.imagenDatoInicial;
                this.construccionesNoConvencionalesP[4] = this.imagenDatoInicial;
            }

        } else {
            this.valorTerrenoAvaluo[4] = this.imagenDatoFinal;
            this.valorTotalConstruccion[4] = this.imagenDatoFinal;
            this.valorTotalConstruccionConvencional[4] = this.imagenDatoFinal;
            this.valorTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;
            this.areaTotalTerreno[4] = this.imagenDatoFinal;
            this.areaTotalConstruccion[4] = this.imagenDatoFinal;
            this.areaTotalConstruccionConvencional[4] = this.imagenDatoFinal;
            this.areaTotalConstruccionNoConvencional[4] = this.imagenDatoFinal;

            this.construccionesConvencionalesP[4] = this.imagenDatoFinal;
            this.construccionesConvencionalesP[5] = this.imagenDatoFinal;
            this.construccionesConvencionalesP[6] = this.imagenDatoFinal;
            this.construccionesNoConvencionalesP[4] = this.imagenDatoFinal;
            this.construccionesNoConvencionalesP[5] = this.imagenDatoFinal;
            this.construccionesNoConvencionalesP[6] = this.imagenDatoFinal;
        }

        almacenarDatosTablaDatosPredios();

    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la ventana de detalle de propietarios y / o poseedores
     */
    public void cerrarVentanaDPropietarios(CloseEvent ce) {
        this.tablaDetallesAdicionalesPropietarios = null;
        this.tablaDetallesAdicionalesPPropietarios = null;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la ventana de detalle de justificación de propiedad
     */
    public void cerrarVentanaDJPropiedad(CloseEvent ce) {
        this.tablaDetallesAdicionalesJPropiedad = null;
        this.tablaDetallesAdicionalesJPPropiedad = null;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la ventana de detalle de avalúo
     */
    public void cerrarVentanaDAvaluo(CloseEvent ce) {
        this.predioAvaluoSeleccionado = null;
        this.ppredioAvaluoSeleccionado = null;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la ventana de detalle de ubicación de predio
     */
    public void cerrarVentanaDUbicacion(CloseEvent ce) {
        this.predioUbicacionSeleccionado = null;
        this.ppredioUbicacionSeleccionado = null;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al cerrar la ventana de detalle de información de construcción
     */
    public void cerrarVentanaDInformacionConstruccion(CloseEvent ce) {
        this.predioUnidadSeleccionado = null;
        this.ppredioUnidadSeleccionado = null;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método ejecutado al solicitar mas detalles del avaluo para el predio inicial
     */
    public void detalleAvaluo() {

        if (this.predioInicialSeleccionado != null) {

            Predio predioAux = this.getConservacionService()
                .obtenerPredioConAvaluosPorId(
                    this.predioInicialSeleccionado.getId());
            if (predioAux != null &&
                predioAux.getUnidadesConstruccionConvencional() != null) {
                this.predioInicialSeleccionado.setUnidadConstruccions(predioAux
                    .getUnidadConstruccions());
            }

            this.predioAvaluoSeleccionado = this.predioInicialSeleccionado;

            if (this.predioAvaluoSeleccionado.getUnidadConstruccions() != null &&
                !this.predioAvaluoSeleccionado.getUnidadConstruccions().isEmpty()) {
                this.construccionesConvencionalesD = new ArrayList<UnidadConstruccion>();
                this.construccionesNoConvencionalesD = new ArrayList<UnidadConstruccion>();

                for (UnidadConstruccion uc : this.predioAvaluoSeleccionado.getUnidadConstruccions()) {
                    if (uc.getTipoConstruccion()
                        .trim()
                        .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                        this.construccionesConvencionalesD.add(uc);
                    } else if (uc.getTipoConstruccion()
                        .trim()
                        .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                        this.construccionesNoConvencionalesD.add(uc);
                    }
                }
            }
            //felipe.cadena::RM #11590::se obtienen las zonas vigentes
            this.listaPredioZonaOriginales = this.obtenePPrediosZonaList();

        }
    }
//---------------------------------------------------------------------------------------------

    /**
     * Método ejecutado al solicitar más detalles del avalúo para el predio proyectado
     *
     */
    /*
     * @modified juanfelipe.garcia 07-05-2013 En el detalle de los datos de las construciones solo
     * debe mostrar las construcciones que en el campo CANCELA_INSCRIBE tiene una M o I @modified by
     * juanfelipe.garcia :: 10-07-2014 :: se adiciona la lista con las zonas filtradas del predio
     * seleccionado (#8192)
     */
    public void cargarDetalleAvaluoP() {

        if (this.predioFinalSeleccionado != null) {

            this.ppredioAvaluoSeleccionado = this.predioFinalSeleccionado;

            if (this.ppredioAvaluoSeleccionado.getPUnidadConstruccions() != null &&
                !this.ppredioAvaluoSeleccionado.getPUnidadConstruccions().isEmpty()) {
                this.construccionesConvencionalesDP = new ArrayList<PUnidadConstruccion>();
                this.construccionesNoConvencionalesDP = new ArrayList<PUnidadConstruccion>();

                for (PUnidadConstruccion uc : this.ppredioAvaluoSeleccionado.
                    getPUnidadConstruccions()) {

                    if (uc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.
                        getCodigo()) ||
                        uc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.
                            getCodigo())) {
                        if (uc.getTipoConstruccion()
                            .trim()
                            .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
                            this.construccionesConvencionalesDP.add(uc);
                        } else if (uc.getTipoConstruccion()
                            .trim()
                            .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                            this.construccionesNoConvencionalesDP.add(uc);
                        }
                    }
                }
            }

            this.listaPredioZonaSeleccionado = obtenerPPrediosZonaList();

        }
    }

    /**
     * Lista con las zonas asociadas al predio, solo muestra las inscritas y las modificadas de la
     * vigencia mas reciente
     *
     * @author juanfelipe.garcia
     * @modified felipe.cadena::09-03-2015::RM #11590::Se descartan las zonas con cancela-inscribe
     * en modifica, y solo se toman en cuenta las de la ultima vigencia del predio
     *
     */
    private List<PPredioZona> obtenerPPrediosZonaList() {
        List<PPredioZona> predioZonaList = new ArrayList<PPredioZona>();
        HashMap<String, List<PPredioZona>> zonasPredios = new HashMap<String, List<PPredioZona>>();

        Calendar cAux = Calendar.getInstance();
        cAux.set(Calendar.YEAR, 1950);
        Date maxVigencia = cAux.getTime();

        for (PPredioZona pz : this.ppredioAvaluoSeleccionado.getPPredioZonas()) {
            if (pz.getCancelaInscribe() != null &&
                pz.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {

                if (pz.getVigencia().after(maxVigencia)) {
                    maxVigencia = pz.getVigencia();
                }

                String key = pz.getVigencia().toString();
                if (zonasPredios.containsKey(key)) {
                    zonasPredios.get(key).add(pz);

                } else {
                    List<PPredioZona> zns = new ArrayList<PPredioZona>();
                    zns.add(pz);
                    zonasPredios.put(key, zns);
                }
            }
        }

        if (!zonasPredios.isEmpty()) {
            predioZonaList = zonasPredios.get(maxVigencia.toString());
        }

        return predioZonaList;
    }

    /**
     * Lista con las zonas asociadas al predio, solo muestra las de la vigencia mas reciente. RM
     * #11590
     *
     * @author felipe.cadena
     *
     * @return
     *
     */
    private List<PredioZona> obtenePPrediosZonaList() {
        List<PredioZona> predioZonaList = new ArrayList<PredioZona>();
        HashMap<String, List<PredioZona>> zonasPredios = new HashMap<String, List<PredioZona>>();

        if (this.predioAvaluoSeleccionado.getPredioZonas() != null &&
            !this.predioAvaluoSeleccionado.getPredioZonas().isEmpty()) {

            Date maxVigencia;

            maxVigencia = this.predioAvaluoSeleccionado.getPredioZonas().get(0).getVigencia();

            for (PredioZona pz : this.predioAvaluoSeleccionado.getPredioZonas()) {

                if (pz.getVigencia().after(maxVigencia)) {
                    maxVigencia = pz.getVigencia();
                }

                String key = pz.getVigencia().toString();
                if (zonasPredios.containsKey(key)) {
                    zonasPredios.get(key).add(pz);

                } else {
                    List<PredioZona> zns = new ArrayList<PredioZona>();
                    zns.add(pz);
                    zonasPredios.put(key, zns);
                }
            }

            if (!zonasPredios.isEmpty()) {
                predioZonaList = zonasPredios.get(maxVigencia.toString());
            }
        }

        return predioZonaList;
    }

    /**
     * Método que verifica si dentro de la lista de predios del trámite se encuentra el predio ficha
     * matriz.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public long verificarIdPredioFichaMatriz() {
        long idPPredioFichaMatriz = 0;

        // Revisar si el predio del trámite tiene condición de
        // propiedad ocho o nueve, de ser así dicho predio no 
        // se tiene presente como predio proyectado.
        List<PPredio> pPrediosDesenglobe = this.getConservacionService()
            .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                this.tramiteSeleccionado.getId());
        for (PPredio p : pPrediosDesenglobe) {
            if (p.isEsPredioFichaMatriz()) {
                idPPredioFichaMatriz = p.getId().longValue();
            }
        }
        return idPPredioFichaMatriz;
    }

    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización
     *
     * @author leidy.gonzalez
     */
    public void traerFotosDeGestorDocumental() {

        if (this.fotoSeleccionada.getDocumento() != null && this.fotoSeleccionada.getDocumento().
            getIdRepositorioDocumentos() != null) {

            this.reporteDocumento = this.reportsService.consultarReporteDeGestorDocumental(
                this.fotoSeleccionada.getDocumento().getIdRepositorioDocumentos());

        }

        if (this.reporteDocumento != null) {
            PrevisualizacionDocMB previsualizacion = (PrevisualizacionDocMB) UtilidadesWeb.
                getManagedBean("previsualizacionDocumento");

            previsualizacion.setRutaArchivoTemporal(this.reporteDocumento.getUrlWebReporte());
            previsualizacion.setReporteDTO(this.reporteDocumento);
        }

    }

}
