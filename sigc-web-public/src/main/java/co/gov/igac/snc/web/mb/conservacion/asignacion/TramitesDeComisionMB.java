/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Comision;
import co.gov.igac.snc.persistence.entity.tramite.ComisionEstado;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VComision;
import co.gov.igac.snc.persistence.util.EComisionEstado;
import co.gov.igac.snc.persistence.util.EComisionEstadoMotivo;
import co.gov.igac.snc.persistence.util.EComisionTipo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;

/**
 * MB para mostrar y retirar trámites de una comisión
 *
 * Los MB que injecten este deberían tener un método llamado retirarTramitesComision(), que llama al
 * aquí implementado, pero podría requerir hacer más cosas (por ejemplo mover procesos de los
 * trámites retirados). Por eso se pide llamar el método del mb que inyecta a este MB.
 *
 * También se usa para mostrar las comisiones asociadas a un trámite.
 *
 *
 * @author pedro.garcia
 * @version 2.0
 * @cu CU-CO-A-04 CU-CO-A-07
 */
@Component("tramitesDeComision")
@Scope("session")
public class TramitesDeComisionMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 5349076280541979199L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TramitesDeComisionMB.class);

    //------- servicios
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * VComision seleccionada en alguna página que se envía como parámetro a este mb para ser usada
     * en las consultas
     */
    private VComision selectedVComision;

    /**
     * trámites seleccionados de la tabla donde se listan los relacionados con una comisión
     */
    private Tramite[] selectedTramitesComision;

    /**
     * Listado de trámites asociados a alguna comisión que se seleccione
     */
    private List<Tramite> tramitesAsociadosComision;

    /**
     * variable que guarda los datos del usuario que usa la aplicación
     */
    private UsuarioDTO loggedInUser;

    /**
     * lista de comisiones efectivas en las que ha estado el trámite del cual se quieren ver las
     * comisiones
     */
    private ArrayList<ComisionTramite> comisionesTramiteHaEstado;

    /**
     * ComisionTramite seleccionada de la tabla que muestra las comisiones relacionadas a un trámite
     */
    private ComisionTramite selectedComisionTramiteCT;

    /**
     * Trámite de trabajo para la funcionalidad de las comisiones de un tramite
     */
    private Tramite selectedTramiteComisionesTramite;

    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteMemorandoComisionEfectiva;

    //---- banderas
    /**
     * flag para decidir si se muestra el botón "Retirar trámite de comision" en la ventana que
     * muestra los trámites de una comisión
     */
    private boolean retirarTramiteButtonRendered;

    /**
     * para indicar a los mb que usen a este si al invocar el método que retira trámites de una
     * comisión la parte correspondiente a bd se hizo correctamente. Si no, por ejemplo, no se
     * debería devolver el proceso de esos trámites (cuando sea requerido)
     */
    private boolean tramitesRetiradosBD;

    /**
     * indica si se debe realizar la búsqueda de trámites asociados a la comisión. Se hace para
     * evitar que vuelva a buscar los trámites de una comisión cuando la que escoge es la misma
     * anterior
     */
    private boolean mustSearchForTramites;

    /**
     * indica si la comisión se cancela, debido al retiro de todos sus trámites
     */
    private boolean cancelarComision;

//------------   methods ----------------------------
    //-----    getters y setters   ------
    public ReporteDTO getReporteMemorandoComisionEfectiva() {
        return this.reporteMemorandoComisionEfectiva;
    }

    public void setReporteMemorandoComisionEfectiva(ReporteDTO reporteMemorandoComisionEfectiva) {
        this.reporteMemorandoComisionEfectiva = reporteMemorandoComisionEfectiva;
    }

    public ComisionTramite getSelectedComisionTramiteCT() {
        return this.selectedComisionTramiteCT;
    }

    public void setSelectedComisionTramiteCT(ComisionTramite selectedComisionTramiteCT) {
        this.selectedComisionTramiteCT = selectedComisionTramiteCT;
    }

    public Tramite getSelectedTramiteComisionesTramite() {
        return this.selectedTramiteComisionesTramite;
    }

    public void setSelectedTramiteComisionesTramite(Tramite selectedTramiteComisionesTramite) {
        this.selectedTramiteComisionesTramite = selectedTramiteComisionesTramite;
    }

    public ArrayList<ComisionTramite> getComisionesTramiteHaEstado() {
        return this.comisionesTramiteHaEstado;
    }

    public void setComisionesTramiteHaEstado(ArrayList<ComisionTramite> comisionesTramiteHaEstado) {
        this.comisionesTramiteHaEstado = comisionesTramiteHaEstado;
    }

    public boolean isCancelarComision() {
        return this.cancelarComision;
    }

    public void setCancelarComision(boolean cancelarComision) {
        this.cancelarComision = cancelarComision;
    }

    public boolean isTramitesRetiradosBD() {
        return this.tramitesRetiradosBD;
    }

    public void setTramitesRetiradosBD(boolean tramitesRetiradosBD) {
        this.tramitesRetiradosBD = tramitesRetiradosBD;
    }

    public UsuarioDTO getLoggedInUser() {
        return this.loggedInUser;
    }

    public void setLoggedInUser(UsuarioDTO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public List<Tramite> getTramitesAsociadosComision() {
        return this.tramitesAsociadosComision;
    }

    public void setTramitesAsociadosComision(List<Tramite> tramitesAsociadosComision) {
        this.tramitesAsociadosComision = tramitesAsociadosComision;
    }

    public boolean isRetirarTramiteButtonRendered() {
        return this.retirarTramiteButtonRendered;
    }

    public void setRetirarTramiteButtonRendered(boolean retirarTramiteButtonRendered) {
        this.retirarTramiteButtonRendered = retirarTramiteButtonRendered;
    }

    public Tramite[] getSelectedTramitesComision() {
        return this.selectedTramitesComision;
    }

    public void setSelectedTramitesComision(Tramite[] selectedTramitesComision) {
        this.selectedTramitesComision = selectedTramitesComision;
    }

    public VComision getSelectedVComision() {
        return this.selectedVComision;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * al definir este atributo se debe volver a consultar la lista de trámites asociados a esa
     * comisión
     *
     */
    public void setSelectedVComision(VComision selectedVComision) {

        if (selectedVComision != null && selectedVComision.getId() != null &&
            this.selectedVComision != null) {
            if (this.selectedVComision.getId() != selectedVComision.getId()) {
                //D: se pone en true para que busque trámites
                this.mustSearchForTramites = true;
            } else {
                this.mustSearchForTramites = false;
            }
        }

        this.selectedVComision = selectedVComision;
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del TramitesDeComisionMB");
        this.mustSearchForTramites = true;
    }

//-------------------------------------------------------------------------------------------------
    /**
     * action del botón "Retirar trámite(s) de comision"
     *
     * 1. se debe cambia el valor del campo 'comisionado' de los trámites retirados 2. eliminar las
     * relaciones comision_tramite OJO: aquí NO se devuelven -a otra actividad- los procesos de los
     * trámites retirados, porque en este punto aún no se han llevado a la siguiente actividad del
     * proceso 2.5. Se consulta la lista de comisiones tramite en la base de datos y se establecen
     * en el trámite 3. Si se retiran todos los trámites de la comisión se cambia su estado a
     * cancelada, siempre y cuando la comisión no esté en estado cancelada. 3.5. Si cambia de
     * estado, hay que registrarlo en la tabla COMISION_ESTADO 4. si no se cancela la comisión, se
     * actualiza y se valida la regla de la duración de la comisión
     *
     */
    //@modified by javier.aponte se cambia el orden de los pasos porque al eliminar la relación de comisión trámite
    // y después actualizar el trámite vuelve a crear el registro en comision_tramite
    public void retirarTramitesComision() {

        LOGGER.debug("retirando trámites de comisión...");

        Double nuevaDuracion;
        boolean cumpleRegla, errorBD = false;

        this.cancelarComision = false;
        this.tramitesRetiradosBD = false;

        if (this.selectedTramitesComision.length == this.tramitesAsociadosComision.size()) {
            this.addMensajeInfo("Se retiraron todos los trámites de la comisión por lo que ésta " +
                "quedó en estado " + EComisionEstado.CANCELADA.toString());
            this.cancelarComision = true;
        }

        //D: en el entity es una lista; sin embargo, por regla de negocio, solo hay una entrada
        //   para cada trámite en la tabla COMISION_TRAMITE
        //List<ComisionTramite> comisionTramiteRels;
        Long selectedTramiteId;
        List<Tramite> tramitesTemp = new ArrayList<Tramite>();

        List<ComisionTramite> comisionesTramiteVigentes;

        for (Tramite tempTramiteItem : this.selectedTramitesComision) {
            //N: esto no funciona porque ese campo es lazy en el entity
            //comisionTramiteRels = tempTramite.getComisionTramites();

            //N: tampoco funciona crear entities de Comision y Tramite y asociarlos a un ComisionTramite
            //   porque necesitaría el id para poder borrarlo
            //this.getTramiteService().borrarComisionesTramites(this.loggedInUser, comisionTramiteRels);
            try {

                //1.
                tempTramiteItem.setComisionado(ESiNo.NO.getCodigo());
                this.getTramiteService().actualizarTramite(tempTramiteItem, this.loggedInUser);

                //2.
                //D: eliminar relaciones trámite-comisión
                this.getTramiteService().
                    borrarRelacionTramiteComision(this.loggedInUser, tempTramiteItem.getId(),
                        this.selectedVComision.getId());

                //2.5. Se consulta la lista de comisiones tramite en la base de datos y se establecen en el trámite
                comisionesTramiteVigentes = this.getTramiteService().
                    buscarComisionesTramitePorIdTramite(tempTramiteItem.getId());
                tempTramiteItem.setComisionTramites(comisionesTramiteVigentes);

            } catch (Exception ex) {
                LOGGER.error(
                    "Excepción borrando relación comisión-trámite o actualizando trámite: " +
                    ex.getMessage());
                errorBD = true;

            }

            //D: para hacer que la lista que tiene el managed bean corresponda a los datos de la BD
            selectedTramiteId = tempTramiteItem.getId();

            tramitesTemp.clear();
            tramitesTemp.addAll(this.tramitesAsociadosComision);

            for (Tramite tramiteAsociado : tramitesTemp) {
                if (tramiteAsociado.getId().equals(selectedTramiteId)) {
                    this.tramitesAsociadosComision.remove(tramiteAsociado);
                    break;
                }
            }
        }

        //D: esto se hace para que los mb que usan a este sepan si se pudo hacer lo de bd
        //   correspondiente al retiro de los trámites de la comisión
        this.tramitesRetiradosBD = (errorBD) ? false : true;

        //Se cancela sólo si la comisión no está ya en estado cancelado
        //3.
        if (this.cancelarComision && !EComisionEstado.CANCELADA.equals(this.selectedVComision.
            getEstado())) {
            this.selectedVComision.setEstado(EComisionEstado.CANCELADA.toString());
            this.getTramiteService().actualizarComision(this.loggedInUser, this.selectedVComision);

            // 3.5
            ComisionEstado nuevoComisionEstado = this.initNuevoEstadoComision();
            nuevoComisionEstado.setEstado(EComisionEstado.CANCELADA.getCodigo());
            this.getTramiteService().insertarComisionEstado(nuevoComisionEstado, this.loggedInUser);
        } else {
            //5.
            //D: recalcular duración comisión (se hace solo si la comisión no es de depuración, porque
            // para estas se calcula la duración de forma independiente de los trámites)
            if (!this.selectedVComision.getTipoComision().equals(
                EComisionTipo.DEPURACION.getCodigo())) {
                nuevaDuracion =
                    this.getTramiteService().
                        obtenerDuracionComision2(this.selectedVComision.getId());
                this.selectedVComision.setDuracion(nuevaDuracion);

                //D: actualizar comisión. El método que se llama actualiza la tabla Comision (no V_Comision)
                this.getTramiteService().actualizarComision(this.loggedInUser,
                    this.selectedVComision);

                //D: validar regla de tiempos de comisión:
                cumpleRegla = UtilidadesWeb.validarReglaDuracionComision(
                    this.selectedVComision.getDuracion(),
                    this.selectedVComision.getDuracionTotal());

                if (!cumpleRegla) {
                    this.addMensajeWarn("Existe diferencia entre el tiempo de la comisión y la " +
                        "duración de la misma.");
                }
            }
        }

        //OJO: no hacer esto aquí porque se hace referencia a este arreglo en
        // AdministracionComisionesMB#retirarTramitesComision, que es el método action del botón
        //this.selectedTramitesComision = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link que va en el número de trámites en las tablas de comisiones
     */
    public void consultarTramitesComision() {

        if (this.mustSearchForTramites) {
            this.tramitesAsociadosComision = this.getTramiteService().buscarTramitesDeComision(
                this.selectedVComision.getId(), this.selectedVComision.getTipoComision());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Crea un nuevo objeto ComisionEstado que se usa para guardar el cambio de estado que se haga
     *
     * Al comenzar cada operación estas fechas deben ser iguales a las de la comisión seleccionada
     */
    private ComisionEstado initNuevoEstadoComision() {

        ComisionEstado answer = null;

        /**
         * esta se usa para relacionar el ComisionEstado con una Comision. Se tiene el id de la
         * comisión en el VComision seleccionado, pero hay que pasarle un objeto Comision al
         * ComisioEstado para hacerle la actualización
         */
        Comision relatedComision;
        Date currentDate = new Date(System.currentTimeMillis());

        answer = new ComisionEstado();
        answer.setFecha(currentDate);
        answer.setMotivo(EComisionEstadoMotivo.RETIRO_TODOS_LOS_TRAMITES.getCodigo());
        answer.setFechaInicio(this.selectedVComision.getFechaInicio());
        answer.setFechaFin(this.selectedVComision.getFechaFin());

        //D: asignarle una Comision al VComision
        relatedComision = new Comision();
        relatedComision.setId(this.selectedVComision.getId());
        answer.setComision(relatedComision);

        //D: datos de log
        answer.setUsuarioLog(this.loggedInUser.getLogin());
        answer.setFechaLog(currentDate);

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink que está en el dato 'número de comisiones' en la tabla de trámites por
     * comisionar, o trámites asociados a la comisión. Hace la consulta de las comisiones efectivas
     * en las que ha estado el trámite.
     *
     * @author pedro.garcia
     */
    public void consultarComisionesTramiteHaEstado() {

        this.comisionesTramiteHaEstado = (ArrayList<ComisionTramite>) this.getTramiteService().
            consultarComisionesTramiteEfectivasTramite(this.selectedTramiteComisionesTramite.getId());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link que tiene el número de memorando de la comisión en la pantalla donde se ven
     * las comisiones del trámite. Baja del gestor documental el archivo correspondiente al
     * memorando del último estado de la comisión, y deja todo listo para la visualización de éste.
     *
     * @author pedro.garcia
     */
    public void alistarMemorandoComisionEfectiva() {

        Long idDocEnGD;
        this.reporteMemorandoComisionEfectiva = null;
        Documento documentoMemorando;

        idDocEnGD = this.selectedComisionTramiteCT.getComision().getMemorandoDocumentoId();

        //N: se habría podido hacer que el memorandoDocumentoId de Comision fuera un Documento, pero
        //   habría tenido que hacer muchos cambios.
        documentoMemorando = this.getGeneralesService().obtenerDocumentoPorId(idDocEnGD);

        if (documentoMemorando != null) {
            this.reporteMemorandoComisionEfectiva =
                this.reportsService.consultarReporteDeGestorDocumental(
                    documentoMemorando.getIdRepositorioDocumentos());
        }
    }

//end of class
}
