package co.gov.igac.snc.web.mb.conservacion.asignacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import org.apache.commons.io.FileUtils;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.DatoRectificar;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteEstadoBloqueo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.components.IAvanzarProcesoConservacion;
import co.gov.igac.snc.web.mb.conservacion.ejecucion.VistaDetallesSolicitudMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.tramite.gestion.RecuperacionTramitesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.MessageFormat;
import java.util.Locale;

/**
 * ManagedBean para determinar la procedencia de la solicitud en el subproceso de asignación del
 * proceso de conservación
 *
 * @author javier.aponte
 */
@Component("estableceProcedenciaSolicitud")
@Scope("session")
public class EstableceProcedenciaSolicitudMB extends ProcessFlowManager {

    private static final long serialVersionUID = 2875998452364802234L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        EstableceProcedenciaSolicitudMB.class);

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Estado del tramite
     */
    private TramiteEstado estadoTramite;

    /**
     * Trámite actual. El seleccionado en la tabla de trámites de la actividad
     */
    private Tramite tramite;

    /**
     * Trámites seleccionados en la tabla de la pantalla inicial
     */
    private Tramite[] selectedTramites;

    // **************** BANDERAS DE EDICION****************
    private boolean banderaValidandoAsincrono;

    /**
     * Bandera que indica si se selecciono admitir tramite
     */
    private boolean banderaAdmitirTramite;
    /**
     * Bandera que indica si se selecciono rechazar tramite
     */
    private boolean banderaRechazarTramite;
    /**
     * Bandera que indica si se selecciono enviar a depurar información geográfica
     */
    private boolean banderaEnviarTramiteADepurarInformacionGeografica;

    /**
     * Bandera que indica si el documento ha sido radicado
     */
    private boolean banderaDocumentoRadicado;

    /**
     * Bandera que indica si el avaluo catastral es mayor a los autoavaluos
     */
    //private boolean banderaAvaluoCatastral;
    /**
     * Bandera que indica si el botón de enviar a depurar información geográfica se debe activar
     * cuando no existe información espacial
     */
    //private boolean banderaActivarEnvioADepurarInformacionGeografica;
    /**
     * Bandera que indica si se selecciono el boton de guardar motivo de rechazo
     */
    private boolean banderaGuardarMotivo;

    /**
     * Bandera para saber si ya se válido la geometría de un predio
     */
    private boolean banderaGeometriaPrediosValidada;

    /**
     * Comision Trámite Dato asociada al trámite
     */
    private List<ComisionTramiteDato> comisionesTramiteDato;

    //--------------Variables para el reporte ----------------------------
    /**
     * Número de radicado del memorando de comisión
     */
    private String numeroRadicado;

    /**
     * Objeto que contiene los datos del reporte de el oficio de no procedencia
     */
    private ReporteDTO oficioNoProcedencia;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    /**
     * Lista que almacena los tramites con radicacion cerrada exitosa
     */
    private List<Tramite> radicacionesCerradasExitosas;

    private String mensajeConfirmationAvanzarProceso;
    private boolean habilitarBotonValidarInconsistencias;
    private boolean habilitarBotonAdmitir;
    private boolean habilitarBotonAdmitirMasivo;
    private boolean habilitarBotonEnviarADepurar;
    private boolean habilitarBotonRechazarTramite;
    private boolean mostrarInconsistenciasGeograficas;

    private List<TramiteInconsistencia> inconsistenciasGeograficas;

    /**
     * variable que almacena las razones de rechazo de depuración
     */
    private String razonesRechazoDepuracion;

    /**
     * Bandera para hablitar la tabla de razones de rechazo de depuración
     */
    private boolean habilitarRazonesRechazoDepuracion;

    /**
     * Bandera para habilitar las opciones de trámite visible
     */
    private boolean banderaHabilitarTramiteVisible;

    private boolean banderaTramiteTieneManzana;

    /**
     * Bandera para habilitar las opciones de trámite visible
     */
    private boolean banderaReclamaActividad;

    /**
     * Bandera que permite determinar si el tipo de trámite está soportado en el sistema
     */
    private boolean banderaTramiteSoportado;

    /**
     * Bandera para determinar si la manzana asociada al predio seeccionado tienen predios en
     * actualizacion.
     */
    private boolean banderaManzanaActualizacion;

    private Solicitud solicitud;

    private int randomSpacer;

    private String mensajeCDI;

    /**
     * Tramites que fallan al avanzar posque la activiad ya esta reclamada.
     */
    private List<Long> tramitesErrorAvanzar = new ArrayList<Long>();

    /**
     * Bandera que indica si el tramite esta suspendido
     */
    private boolean tramiteEstadoBloqueo;

    //-----------Esto se agrega para convertir en másivo la actividad de establecer procedencia de la solicitud
    /**
     * variable donde se cargan los resultados paginados de la consulta de trámites (de terreno) a
     * los que no se les ha asignado una comisión
     */
    private LazyDataModel<Tramite> lazyTramites;

    public boolean isBanderaValidandoAsincrono() {
        return banderaValidandoAsincrono;
    }

    public void setBanderaValidandoAsincrono(boolean banderaValidandoAsincrono) {
        this.banderaValidandoAsincrono = banderaValidandoAsincrono;
    }

    public boolean isBanderaManzanaActualizacion() {
        return banderaManzanaActualizacion;
    }

    public void setBanderaManzanaActualizacion(boolean banderaManzanaActualizacion) {
        this.banderaManzanaActualizacion = banderaManzanaActualizacion;
    }

// -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        this.tramiteEstadoBloqueo = false;

        LOGGER.debug("init EstableceProcedenciaSolicitudMB");

        this.tramitesErrorAvanzar = new ArrayList<Long>();
        this.banderaReclamaActividad = true;

        this.mensajeCDI = "";
        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "estableceProcedenciaSolicitud");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.mensajeConfirmationAvanzarProceso = "";

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        try {

            // D: se obtienen los ids de los trámites objetivo a partir del árbol de
            // tareas
            this.idsTramitesActividades = this.tareasPendientesMB
                .obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

            this.habilitarBotonAdmitirMasivo = false;

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            this.addMensajeError(e);
        }

    }

    public List<SelectItem> getStatusOptions() {
        List<SelectItem> options = new ArrayList<SelectItem>();

        options.add(new SelectItem("1", "Mut"));
        options.add(new SelectItem(2, "Rect"));
        options.add(new SelectItem(3, "Comp"));

        return options;
    }

    /**
     * Método que se ejecuta cuando se selecciona un trámite para determinar la procedencia de la
     * solicitud
     *
     * @author javier.aponte
     */
    public String cargarTramiteSeleccionado() {

        this.habilitarBotonAdmitir = true;
        this.habilitarBotonRechazarTramite = true;
        this.mostrarInconsistenciasGeograficas = false;
        this.habilitarBotonValidarInconsistencias=false;
        this.banderaAdmitirTramite = false;
        this.banderaRechazarTramite = false;
        this.banderaDocumentoRadicado = false;
        this.banderaGuardarMotivo = false;
        this.banderaHabilitarTramiteVisible = true;

        this.banderaTramiteSoportado = true;

        //felipe.cadena::15-09-2016::#19450::Reclamar actividad por parte del usuario autenticado
        if (this.banderaReclamaActividad) {
            if (!this.reclamarActividad(this.tramite)) {
                this.tareasPendientesMB.cargarInit();
                this.init();
                return null;
            }
        }

        //felipe.cadena::31-08-2015::ACTAULIZACION::temporal con el tramite de actualizacion asociados.
        Tramite tramiteActualizacion = this.tramite.getTramiteActualizacionAsociado();
        this.tramite = this.getTramiteService().buscarTramitePorTramiteIdAsignacion(this.tramite.
            getId());
        this.tramite.setTramiteActualizacionAsociado(tramiteActualizacion);

        //felipe.cadena::31-08-2015::validacion tramites con predios en manzanas que se encuentran en proceso de actualizacion
        this.banderaManzanaActualizacion = false;
        if (tramite.getTramiteActualizacionAsociado() == null) {
            if (!tramite.isQuinta()) {
                List<Tramite> tramitesActManzana = this.getTramiteService().
                    buscarTramitesActualizacionPorManzana(this.tramite.getPredio().
                        getNumeroPredial().substring(0, 21));
                if (tramitesActManzana != null && !tramitesActManzana.isEmpty()) {
                    this.banderaManzanaActualizacion = true;
                }
            }
        }

        //felipe.cadena::24-07-2015::Validacion geometrias asincronicas
        if (this.tramite.isTramiteMasivo()) {

            if (!EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.tramite.getPredio().
                getCondicionPropiedad())) {

                this.determinaJobInconsistencias();
                if (this.banderaValidandoAsincrono == true) {
                    //D: activación de botones
                    this.habilitarBotonAdmitir = false;
                    this.habilitarBotonRechazarTramite = false;
                    this.habilitarBotonEnviarADepurar = false;
                    return ConstantesNavegacionWeb.ESTABLECE_PROCEDENCIA_SOLICICTUD_INDIVIDUAL;
                }
            }
        }

        //Control de Calidad #10924
        //Cuando se radique un trámite de segunda desenglobe y englobe 
        if (this.tramite.isSegunda()) {
            //Si el predio es de condición de propiedad 8 o 9
            //felipe.cadena:: 20-05-2015::Se permiten avanzar tramites de desenglobe por etapas
            if ((EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.tramite.getPredio().
                getCondicionPropiedad()) ||
                 EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.tramite.getPredio().
                    getCondicionPropiedad())) &&
                 !this.tramite.isDesenglobeEtapas() &&
                 !this.tramite.isRectificacionMatriz()) {
                // Esta funcionalidad no esta implementada en el sistema.
                this.banderaTramiteSoportado = false;
                return ConstantesNavegacionWeb.ESTABLECE_PROCEDENCIA_SOLICICTUD_INDIVIDUAL;
            }

        }

        // Validacion para predios fiscales
        if (this.tramite.getPredio() != null) {

            this.solicitud = this.getTramiteService().buscarSolicitudFetchTramitesBySolicitudId(
                this.tramite.getSolicitud().getId());

            if (this.tramite.getPredio().getTipoCatastro().equals(
                EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {

                this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
                    this.tramite.getId());
                this.habilitarBotonAdmitir = true;
                this.habilitarBotonRechazarTramite = true;
                this.banderaGeometriaPrediosValidada = true;
                this.habilitarBotonEnviarADepurar = false;

                this.estadoTramite =
                    this.getTramiteService().buscarTramiteEstadoVigentePorTramiteId(this.tramite.
                        getId());

                if (this.estadoTramite != null && this.estadoTramite.getEstado() != null &&
                     !this.estadoTramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo())) {
                    this.estadoTramite = new TramiteEstado();
                }

                return ConstantesNavegacionWeb.ESTABLECE_PROCEDENCIA_SOLICICTUD_INDIVIDUAL;
            }
        }

        if (this.tareasPendientesMB == null) {
            this.tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb.getManagedBean(
                "tareasPendientes");
        }

        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.tramite.getId());

        this.banderaGeometriaPrediosValidada = true;

        if (this.tramite.getComisionTramites() != null && !this.tramite.getComisionTramites().
            isEmpty()) {
            this.comisionesTramiteDato = this.getTramiteService()
                .buscarComisionTramiteDatoPorTramiteId(this.tramite.getId());

            for (ComisionTramite ct : this.tramite.getComisionTramites()) {
                ct.setComisionTramiteDatos(this.comisionesTramiteDato);
            }
        }

        if (this.tramite.getSolicitud().isSolicitudAutoavaluo()) {
            //lorena.salamanca :: Esto se hace para que si la fecha de radicacion del tramite es mayor al valor que se encuentra en la tabla
            //Parametros entonces no deje avanzar el trámite
            //modified by leidy.gonzalez 01/12/2015 Se modifica el valor que guarda la fecha de radicacion para los tramites de cuarta
            //tomando la fecha d ela solicitud.
            //modified by hector.arias 02/05/2017 Se modifica la condicional para no admitir las autoestimaciones cuando la fecha de solicitud
            //o la fecha de radicado de tesoreria segun corresponda el caso sea mayor o igual a la fechaNoAdmite                        
            Date fechaRadicacion = null;

            if (this.tramite.getFechaRadicacionTesoreria() != null) {
                fechaRadicacion = this.tramite.getFechaRadicacionTesoreria();
            } else if (this.tramite.getSolicitud().getFecha() != null) {
                fechaRadicacion = this.tramite.getSolicitud().getFecha();
            }
            Parametro p = this.getGeneralesService().getCacheParametroPorNombre(
                EParametro.FECHA_NO_ADMITE_TRAMITE.name());
            Date fechaNoAdmite = p.getValorFecha();
            if (fechaRadicacion != null) {
                fechaNoAdmite.setYear(fechaRadicacion.getYear());
                if (fechaRadicacion.compareTo(fechaNoAdmite) >= 0) {
                    this.habilitarBotonEnviarADepurar = false;
                    this.habilitarBotonAdmitir = false;
                }
            }

            //lorena.salamanca :: Esto se hace para que si el avaluo catastral es mayor a la suma de los autoavaluos de construccion y terreno
            //no deje admitir la procedencia.
            Double avaluoCatastral = this.tramite.getPredio().getAvaluoCatastral();
            Double autoAvalConstruccion = this.tramite.getAutoavaluoConstruccion();
            Double autoAvalTerreno = this.tramite.getAutoavaluoTerreno();
            if (autoAvalTerreno == null) {
                autoAvalTerreno = 0D;
            }
            if (autoAvalConstruccion == null) {
                autoAvalConstruccion = 0D;
            }
            Double valorAutoAvaluo = autoAvalTerreno + autoAvalConstruccion;

            if (valorAutoAvaluo <= avaluoCatastral) {
                this.habilitarBotonAdmitir = false;
            }
        } else if (this.determinarSiTramiteRequiereValidacionDeGeometriaDePredios()) {

            boolean valido = revisarValidacionGeometriaPredios();
            if (valido) {
                this.habilitarBotonAdmitir = true;
                this.habilitarBotonEnviarADepurar = false;
            } else {

                List<TramiteInconsistencia> tramiteInconsistenciasTemp;
                this.habilitarBotonAdmitir = false;
                this.habilitarBotonEnviarADepurar = true;

                // Set de las inconsistencias geográficas del trámite.
                this.inconsistenciasGeograficas = new ArrayList<TramiteInconsistencia>();

                tramiteInconsistenciasTemp = this.getTramiteService().
                    buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramite.getId());
                if (tramiteInconsistenciasTemp != null && !tramiteInconsistenciasTemp.isEmpty()) {
                    for (TramiteInconsistencia ti : tramiteInconsistenciasTemp) {
                        this.inconsistenciasGeograficas.add(ti);
                    }
                    this.mostrarInconsistenciasGeograficas = true;
                }
                this.habilitarRazonesRechazoDepuracion = false;
                //validar si el tramite paso por depuración
                TramiteDepuracion ultimoTramiteDepuracion = this.getTramiteService().
                    buscarUltimoTramiteDepuracionPorIdTramite(this.tramite.getId());
                // Set de las razones de rechazo depuración.
                if (ultimoTramiteDepuracion != null) {
                    this.razonesRechazoDepuracion = ultimoTramiteDepuracion.getRazonesRechazo();
                    if (this.razonesRechazoDepuracion != null && !this.razonesRechazoDepuracion.
                        isEmpty()) {
                        this.habilitarRazonesRechazoDepuracion = true;
                    }
                }
            }
        } //D: CC-NP-CO-195 se valida diferente si el trámites es de cancelación de predio o de
        //  rectificación por cancelación por doble inscripción
        else if ((this.tramite.isCancelacionPredio() && !this.tramite.isCancelacionMasiva()) ||
            (this.tramite.isEsRectificacion() && this.tramite.isRectificacionCDI())) {

            //D: la validación de geometría de predios se hizo, y se hizo diferente para estos tipos de trámite
            boolean valido = revisarValidacionGeometriaPrediosCancelPredio();
            if (!valido) {
                ArrayList<TramiteInconsistencia> inconsistenciasTemp;
                inconsistenciasTemp = (ArrayList<TramiteInconsistencia>) this.getTramiteService().
                    buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramite.getId());
                this.inconsistenciasGeograficas = new ArrayList<TramiteInconsistencia>();

                for (TramiteInconsistencia ti : inconsistenciasTemp) {
                    if (!ti.getDepurada().equals(ESiNo.NO.getCodigo())) {
                        this.inconsistenciasGeograficas.add(ti);
                    }
                }
            }
        } else {
            this.habilitarBotonAdmitir = true;
            this.habilitarBotonEnviarADepurar = false;
        }

        this.estadoTramite =
            this.getTramiteService().buscarTramiteEstadoVigentePorTramiteId(this.tramite.getId());
        if (this.estadoTramite != null && this.estadoTramite.getEstado() != null &&
             !this.estadoTramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo())) {
            this.estadoTramite = new TramiteEstado();
        }

        //CC-NP-CO-040 - validación condición 8 y 9 predios migrados, no tiene ficha matriz//mirar con felipe C/no se validan los tramites por etapas
        if ((this.tramite.isSegunda() ||
            this.tramite.isTercera() ||
            this.tramite.isRectificacionArea() ||
            this.tramite.isRectificacionAreaConstruccion() ||
            this.tramite.isRectificacionZona() ||
            this.tramite.isRectificacionDetalleCalificacion() ||
            this.tramite.isRectificacionUso()) &&
             !this.tramite.isTramiteMasivo()) {

            for (Predio p : this.tramite.getPredios()) {
                if ((p.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_8.getCodigo()) &&
                     !p.getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END)) ||
                    (p.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.getCodigo()) &&
                     !p.getNumeroPredial().endsWith(Constantes.FM_PH_END))) {

                    FichaMatriz ficha = this.getConservacionService().
                        obtenerFichaMatrizOrigen(p.getNumeroPredial());

                    if (ficha == null) {
                        this.habilitarBotonAdmitir = false;
                        this.habilitarBotonEnviarADepurar = false;
                        this.banderaHabilitarTramiteVisible = false;
                    }
                }
            }
        }

        this.solicitud = this.getTramiteService().buscarSolicitudFetchTramitesBySolicitudId(
            this.tramite.getSolicitud().getId());

        this.randomSpacer = (int) (Math.random() * 3) + 1;

        //lorena.salamanca :: 30-11-2015 :: Se agrega esta bandera porque seguia mostrando el mensaje de estar validando aun cuando ya habia terminado
        if (this.tramite.isQuintaMasivo()) {
            this.banderaValidandoAsincrono = false;
        }

        if (tienePredioBloqueado()) {
            this.tramiteEstadoBloqueo = true;
            this.habilitarBotonRechazarTramite = true;
            this.addMensajeError(
                "El predio se encuentra bloqueado con tipo de bloqueo Suspender, por lo tanto no puede proceder el trámite");
        }

        return ConstantesNavegacionWeb.ESTABLECE_PROCEDENCIA_SOLICICTUD_INDIVIDUAL;
    }

    /**
     * Método encargado de determinar si el trámite requiere validación de la geometría de predios
     *
     * @author javier.aponte
     */
    public boolean determinarSiTramiteRequiereValidacionDeGeometriaDePredios() {

        boolean answer = false;

        //Se valida la geometría de predios para los trámites geográficos, pero que no sea un trámite de quinta
        //ya que en este momento posiblemente el predio no exista
        if (this.tramite.isTramiteGeografico() && !this.tramite.isQuinta()) {

            //La validación se realiza si el predio no es de condición 8 ni 9
            if (!EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.tramite.getPredio().
                getCondicionPropiedad()) &&
                 !EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.tramite.getPredio().
                    getCondicionPropiedad())) {
                answer = true;
            } //Si el predio es de condición 8 0 9 
            else {
                //Se realiza la validación para trámites de tercera, rectificaciones de área
                if ((this.tramite.isTercera() ||
                    this.tramite.isCancelacionMasiva() ||
                    (this.tramite.isRectificacion() && (this.tramite.isRectificacionArea())))) {
                    answer = true;
                } // Validación de geometría para trámites por etapas condición 9
                else if ((EPredioCondicionPropiedad.CP_9.getCodigo().equals(
                    this.tramite.getPredio().getCondicionPropiedad()) ||
                     EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.tramite.getPredio().
                        getCondicionPropiedad())) &&
                    this.tramite.isDesenglobeEtapas()) {
                    answer = true;
                } else {
                    answer = false;
                }
            }
        } // Quintas masivas
        else if (this.tramite.isTramiteGeografico() && this.tramite.isQuintaMasivo()) {
            if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.tramite.getNumeroPredial().
                substring(21, 22)) ||
                 EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.tramite.getNumeroPredial().
                    substring(21, 22))) {
                answer = true;
            }
        } //Se valida la geometría de predios para los trámites de rectificación de zona
        else if (this.tramite.isRectificacionZona()) {

            //La validación se realiza si el predio no es de condición 8 ni 9
            if (!EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.tramite.getPredio().
                getCondicionPropiedad()) &&
                 !EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.tramite.getPredio().
                    getCondicionPropiedad())) {
                answer = true;
            } //Se valida la geometría para unidades condición 8 y 9 que no son ficha matriz
            else if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(this.tramite.getPredio().
                getCondicionPropiedad()) ||
                EPredioCondicionPropiedad.CP_9.getCodigo().equals(this.tramite.getPredio().
                    getCondicionPropiedad()) &&
                !this.tramite.isRectificacionZonaMatriz()) {
                answer = true;
            }

        } else if (this.tramite.isRectificacionZonaMatriz()) {
            answer = true;
        }

        return answer;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * @version 2.0
     * @author javier.aponte
     */
    @SuppressWarnings("serial")
    public EstableceProcedenciaSolicitudMB() {

        this.lazyTramites = new LazyDataModel<Tramite>() {

            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();

                populateTramitesLazyly(answer, first, pageSize, sortField, sortOrder.toString(),
                    filters);

                return answer;
            }
        };

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace la búsqueda de trámites por id
     *
     * @author javier.aponte
     */
    private void populateTramitesLazyly(List<Tramite> answer, int first, int pageSize,
        String sortField, String sortOrder, Map<String, String> filters) {

        List<Tramite> rows;
        int size, count;

        rows = this.getTramiteService().buscarTramitesParaDeterminarProcedenciaSolicitud(
            this.idsTramitesActividades,
            sortField, sortOrder,
            filters, first, pageSize);

        size = rows.size();
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }

        //D: se cuentan los de la bd asignar el tamaño del datamodel
        count = this.getTramiteService().contarTramitesParaDeterminarProcedenciaSolicitud(
            this.idsTramitesActividades);

        this.lazyTramites.setRowCount(count);

        this.randomSpacer = (int) (Math.random() * 3) + 1;

    }
//--------------------------------------------------------------------------------------------------

    /*
     * @modified felipe.cadena: Se valida directamente con los los predios relacionados al tramite.
     */
    private boolean revisarValidacionGeometriaPredios() {

        boolean resultado = true;

        List<TramiteInconsistencia> inconsisteciasTramite = this.getTramiteService().
            buscarTramiteInconsistenciaPorTramiteId(this.tramite.getId());

        //D: se pregunta si es nula o vacía porque cuando ya se validó y no hubo inconsistencias se
        // crea un registro en Tramite_Inconsistencia que dice 'No se encuentran inconsistencias geográficas',
        // para saber que ya se hizo la validación  
        this.habilitarBotonValidarInconsistencias = true;
        if (inconsisteciasTramite == null || inconsisteciasTramite.isEmpty()) {
            this.banderaGeometriaPrediosValidada = false;
            return false;
        }

        this.banderaTramiteTieneManzana = true;
        if (!inconsisteciasTramite.isEmpty()) {

            for (TramiteInconsistencia ti : inconsisteciasTramite) {
                if (ti.getInconsistencia().
                    equals(EInconsistenciasGeograficas.NO_MANZANA.getNombre())) {
                    this.banderaTramiteTieneManzana = false;
                }

                if (ti.getInconsistencia().
                    equals(EInconsistenciasGeograficas.NO_MANZANA.getNombre()) ||
                     ti.getInconsistencia().
                        equals(EInconsistenciasGeograficas.NO_PREDIO.getNombre()) ||
                     ti.getInconsistencia().equals(EInconsistenciasGeograficas.NO_FEATURE.
                        getNombre()) ||
                     ti.getInconsistencia().equals(EInconsistenciasGeograficas.MEJORA_SIN_TERRENO.
                        getNombre()) ||
                     ti.getInconsistencia().equals(EInconsistenciasGeograficas.NO_ZONAS.getNombre()) ||
                     ti.getInconsistencia().equals(
                        EInconsistenciasGeograficas.NUMERO_PREDIAL_REPETIDO.getNombre())) {
                    resultado = false;
                    break;
                }
            }
        }

        return resultado;
    }

//--------------------------------------------------------------------------------------------------
    public LazyDataModel<Tramite> getLazyTramites() {
        return lazyTramites;
    }

    public void setLazyTramites(LazyDataModel<Tramite> lazyTramites) {
        this.lazyTramites = lazyTramites;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public int getRandomSpacer() {
        return randomSpacer;
    }

    public void setRandomSpacer(int randomSpacer) {
        this.randomSpacer = randomSpacer;
    }

    public String getMensajeCDI() {
        return mensajeCDI;
    }

    public void setMensajeCDI(String mensajeCDI) {
        this.mensajeCDI = mensajeCDI;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public Tramite[] getSelectedTramites() {
        return selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public boolean isHabilitarBotonAdmitir() {
        return this.habilitarBotonAdmitir;
    }

    public void setHabilitarBotonAdmitir(boolean habilitarBotonAdmitir) {
        this.habilitarBotonAdmitir = habilitarBotonAdmitir;
    }

    public boolean isHabilitarBotonAdmitirMasivo() {
        return habilitarBotonAdmitirMasivo;
    }

    public void setHabilitarBotonAdmitirMasivo(boolean habilitarBotonAdmitirMasivo) {
        this.habilitarBotonAdmitirMasivo = habilitarBotonAdmitirMasivo;
    }

    public boolean isHabilitarBotonEnviarADepurar() {
        return this.habilitarBotonEnviarADepurar;
    }

    public void setHabilitarBotonEnviarADepurar(boolean habilitarBotonEnviarADepurar) {
        this.habilitarBotonEnviarADepurar = habilitarBotonEnviarADepurar;
    }

    public boolean isHabilitarBotonRechazarTramite() {
        return this.habilitarBotonRechazarTramite;
    }

    public void setHabilitarBotonRechazarTramite(boolean habilitarBotonRechazarTramite) {
        this.habilitarBotonRechazarTramite = habilitarBotonRechazarTramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public TramiteEstado getEstadoTramite() {
        return this.estadoTramite;
    }

    public void setEstadoTramite(TramiteEstado estadoTramite) {
        this.estadoTramite = estadoTramite;
    }

    public boolean isBanderaRechazarTramite() {
        return this.banderaRechazarTramite;
    }

    public void setBanderaRechazarTramite(boolean banderaRechazarTramite) {
        this.banderaRechazarTramite = banderaRechazarTramite;
    }

    public boolean isBanderaEnviarTramiteADepurarInformacionGeografica() {
        return this.banderaEnviarTramiteADepurarInformacionGeografica;
    }

    public void setBanderaEnviarTramiteADepurarInformacionGeografica(
        boolean banderaEnviarTramiteADepurarInformacionGeografica) {
        this.banderaEnviarTramiteADepurarInformacionGeografica =
            banderaEnviarTramiteADepurarInformacionGeografica;
    }

    public boolean isBanderaDocumentoRadicado() {
        return this.banderaDocumentoRadicado;
    }

    public void setBanderaDocumentoRadicado(boolean banderaDocumentoRadicado) {
        this.banderaDocumentoRadicado = banderaDocumentoRadicado;
    }

    public boolean isBanderaGuardarMotivo() {
        return this.banderaGuardarMotivo;
    }

    public void setBanderaGuardarMotivo(boolean banderaGuardarMotivo) {
        this.banderaGuardarMotivo = banderaGuardarMotivo;
    }

    public boolean isBanderaGeometriaPrediosValidada() {
        return this.banderaGeometriaPrediosValidada;
    }

    public void setBanderaGeometriaPrediosValidada(
        boolean banderaGeometriaPrediosValidada) {
        this.banderaGeometriaPrediosValidada = banderaGeometriaPrediosValidada;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public ReporteDTO getOficioNoProcedencia() {
        return this.oficioNoProcedencia;
    }

    public void setOficioNoProcedencia(ReporteDTO oficioNoProcedencia) {
        this.oficioNoProcedencia = oficioNoProcedencia;
    }

    public boolean isMostrarInconsistenciasGeograficas() {
        return this.mostrarInconsistenciasGeograficas;
    }

    public void setMostrarInconsistenciasGeograficas(boolean mostrarInconsistenciasGeograficas) {
        this.mostrarInconsistenciasGeograficas = mostrarInconsistenciasGeograficas;
    }

    public List<TramiteInconsistencia> getInconsistenciasGeograficas() {
        return this.inconsistenciasGeograficas;
    }

    public void setInconsistenciasGeograficas(List<TramiteInconsistencia> inconsistenciasGeograficas) {
        this.inconsistenciasGeograficas = inconsistenciasGeograficas;
    }

    public String getRazonesRechazoDepuracion() {
        return razonesRechazoDepuracion;
    }

    public void setRazonesRechazoDepuracion(String razonesRechazoDepuracion) {
        this.razonesRechazoDepuracion = razonesRechazoDepuracion;
    }

    public boolean isHabilitarRazonesRechazoDepuracion() {
        return habilitarRazonesRechazoDepuracion;
    }

    public boolean isBanderaHabilitarTramiteVisible() {
        return banderaHabilitarTramiteVisible;
    }

    public void setBanderaHabilitarTramiteVisible(boolean banderaHabilitarTramiteVisible) {
        this.banderaHabilitarTramiteVisible = banderaHabilitarTramiteVisible;
    }

    public boolean isBanderaTramiteSoportado() {
        return banderaTramiteSoportado;
    }

    public void setBanderaTramiteSoportado(boolean banderaTramiteSoportado) {
        this.banderaTramiteSoportado = banderaTramiteSoportado;
    }

    public boolean isTramiteEstadoBloqueo() {
        return tramiteEstadoBloqueo;
    }

    public void setTramiteEstadoBloqueo(boolean tramiteEstadoBloqueo) {
        this.tramiteEstadoBloqueo = tramiteEstadoBloqueo;
    }

//--------------------------------------------------------------------------------------------------
    public String getMensajeConfirmationAvanzarProceso() {
        return mensajeConfirmationAvanzarProceso;
    }

    public void setMensajeConfirmationAvanzarProceso(String mensajeConfirmationAvanzarProceso) {
        this.mensajeConfirmationAvanzarProceso = mensajeConfirmationAvanzarProceso;
    }

    public List<Tramite> getRadicacionesCerradasExitosas() {
        return radicacionesCerradasExitosas;
    }

    public void guardarMotivoRechazoTramite() {

        LOGGER.debug("Establecer Procedencia de Solicitud MB - Guardar Motivo Rechazo Tramite");

        if (this.estadoTramite.getMotivo().length() > Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO) {
            this.addMensajeError("El tamaño del texto del motivo no debe ser superior a " +
                Constantes.TAMANO_MOTIVO_TRAMITE_ESTADO + " caracteres");
            return;
        }

        this.estadoTramite.setFechaInicio(new Date(System.currentTimeMillis()));
        this.estadoTramite.setTramite(this.tramite);
        this.estadoTramite.setUsuarioLog(this.usuario.getLogin());
        this.estadoTramite.setFechaLog(new Date());
        this.estadoTramite.setEstado(ETramiteEstado.RECHAZADO.getCodigo());
        try {
            TramiteEstado auxEstadoTramite = this.getTramiteService().
                actualizarTramiteEstado(this.estadoTramite, this.usuario);
            if (auxEstadoTramite != null) {
                this.estadoTramite = auxEstadoTramite;
                this.addMensajeInfo("Motivo guardado satisfactoriamente");
            }
        } catch (Exception e) {
            LOGGER.debug("Error En Guardar Motivo Rechazo Tramite ");
            LOGGER.error("ERROR: " + e);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Admitir Trámite"
     *
     * @modified pedro.garcia
     */
    public String admitirTramite() {

        try {
            if (this.tramite != null && this.estadoTramite != null &&
                this.tramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo())) {
                this.getTramiteService().eliminarTramiteEstado(this.estadoTramite);
            }
        } catch (Exception e) {
            LOGGER.info(
                "Error en EstablecerProcedenciaSolicitudMB-admitirTramite eliminando el tramite estado" +
                 e.getMessage());
        }

        this.banderaAdmitirTramite = true;

        //D: si el trámite que se admite es de autoavalúo, se convierte en una mutación de cuarta
        if (this.tramite.getSolicitud().getTipo() != null) {
            if (this.tramite.getTipoTramite().equals(ETramiteTipoTramite.AUTOESTIMACION.getCodigo())) {
                Object[] resultado;
                resultado = this.getGeneralesService().generarNumeracion(
                    ENumeraciones.NUMERACION_RADICACION_CATASTRAL,
                    "0", this.tramite.getDepartamento().getCodigo(),
                    this.tramite.getMunicipio().getCodigo(), 0);
                if (resultado != null && resultado.length > 0) {
                    this.tramite.setNumeroRadicacion(resultado[0].toString());
                    this.tramite.setFechaRadicacion(new Date());
                }
                this.tramite.setTipoTramite(ETramiteTipoTramite.MUTACION.getCodigo());
                this.tramite.setClaseMutacion(EMutacionClase.CUARTA.getCodigo());
            }
        }

        this.forwardThisProcess();

        return cerrarPaginaPrincipal();
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * action del botón "Admitir Trámite" UNICAMENTE para autoavaluo
     *
     * @modified lorena.salamanca
     */
    public void admitirTramiteAutoavaluo() {

        try {
            if (this.tramite != null && this.estadoTramite != null &&
                this.tramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo())) {
                this.getTramiteService().eliminarTramiteEstado(this.estadoTramite);
            }
        } catch (Exception e) {
            LOGGER.info(
                "Error en EstablecerProcedenciaSolicitudMB-admitirTramite eliminando el tramite estado" +
                 e.getMessage());
        }

        this.banderaAdmitirTramite = true;

        //D: si el trámite que se admite es de autoavalúo, se convierte en una mutación de cuarta
        if (this.tramite.getSolicitud().getTipo() != null) {
            if (this.tramite.getTipoTramite().equals(ETramiteTipoTramite.AUTOESTIMACION.getCodigo())) {
                Object[] resultado;
                resultado = this.getGeneralesService().generarNumeracion(
                    ENumeraciones.NUMERACION_RADICACION_CATASTRAL,
                    "0", this.tramite.getDepartamento().getCodigo(),
                    this.tramite.getMunicipio().getCodigo(), 0);
                if (resultado != null && resultado.length > 0) {
                    this.tramite.setNumeroRadicacion(resultado[0].toString());
                    this.tramite.setFechaRadicacion(new Date());
                }
                this.tramite.setTipoTramite(ETramiteTipoTramite.MUTACION.getCodigo());
                this.tramite.setClaseMutacion(EMutacionClase.CUARTA.getCodigo());
                this.mensajeConfirmationAvanzarProceso = "El número de trámite asignado es " +
                    this.tramite.getNumeroRadicacion().toString();

            }
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Se mueve el proceso cuando es un tramite de Cuarta.
     *
     * @return
     */
    public String moverProcesoIsCuarta() {
        this.forwardThisProcess();
        return cerrarPaginaPrincipal();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @author javier.aponte
     * @throws Exception
     * @modified by leidy.gonzalez: #13244 :: 08/07/2015 se adiciona firma de usuario, si esta
     * existe al reporte
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarOficioNoProcedencia() throws Exception {

        EReporteServiceSNC enumeracionReporte =
            EReporteServiceSNC.OFICIO_NO_PROCEDENCIA_AUTOAVALUO_REVISION_Y_VIA_GUBERNATIVA;

        //Para los tipos de trámites de mutaciones genera el reporte con un formato diferente 
        if (this.tramite.isMutacion()) {
            enumeracionReporte = EReporteServiceSNC.OFICIO_NO_PROCEDENCIA_DE_MUTACIONES;
        }

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("TRAMITE_ID", String.valueOf(this.tramite.getId()));

        try {
            GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

            Object[] usuarioFirma = generalMB.buscarUsuarioFirmaOficionNoProcedencia(this.tramite.
                getTipoTramite(), this.usuario);

            if ((String) usuarioFirma[2] == null) {
                this.addMensajeInfo("El usuario no tiene firma de usuario asociada en el sistema.");
            }

            parameters.put("NOMBRE_USUARIO", (String) usuarioFirma[0]);
            parameters.put("CARGO_USUARIO", (String) usuarioFirma[1]);
            parameters.put("FIRMA_USUARIO", (String) usuarioFirma[2]);

            if (this.numeroRadicado != null) {
                parameters.put("NUMERO_RADICADO", this.numeroRadicado);
            }

            this.oficioNoProcedencia = reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);
        } catch (Exception e) {
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER,
                e, enumeracionReporte.getUrlReporte());
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Radicar"
     *
     * @modified pedro.garcia
     * @modified juanfelipe.garcia Realizar el cierre de una radicación cuando un trámite no
     * procede.
     */
    public void radicar() {

        try {

            // Establece el trámite con el que se va a trabajar
            //this.radicaDocumentosMB.setSelectedTramite(this.tramite);
            // 1. Generación del número de Radicación en correspondencia
            // (sticker de radicación)
            if (this.numeroRadicado == null) {

                //Para los tipos de trámites de mutaciones genera el reporte con un formato diferente 
                if (this.tramite.isMutacion()) {
                    this.numeroRadicado = this.obtenerNumeroRadicadoMutaciones();
                } else {
                    this.numeroRadicado = this.
                        obtenerNumeroRadicadoAutoestimacionRevisionAvaluoYViaGubernativa();
                }

            }
            if (this.numeroRadicado == null) {
                throw new Exception(
                    "No se pudo generar el número de radicación");
            }
            LOGGER.debug("Número Radicado: " + this.numeroRadicado);

            // 2. Generación del reporte con el número de radicación
            this.generarOficioNoProcedencia();

            //Guarda el documento en Alfresco y en la base de datos y lo asocia con
            //un trámite documento
            this.guardarDocumentoOficioNoProcedencia();

            this.banderaDocumentoRadicado = true;
            this.banderaRechazarTramite = true;
            this.habilitarBotonEnviarADepurar = false;
            this.habilitarBotonRechazarTramite = false;

            TramiteEstado te = new TramiteEstado(ETramiteEstado.RECHAZADO.getCodigo(), this.tramite,
                usuario.getLogin());
            te = this.getTramiteService().guardarActualizarTramiteEstado(te);
            this.tramite.setTramiteEstado(te);
            //this.tramite.setEstado(ETramiteEstado.RECHAZADO.getCodigo());

            /*
             * juanfelipe.garcia :: 02-05-2013 :: Adicion de logica CU-NP-CO-176 Realizar el cierre
             * de una radicación cuando un trámite no procede.
             *
             * - Ubicar el número de radicación con que se dio origen al trámite que no procede. -
             * Ubicar las radicaciones asociadas a la radicación del trámite - Traer el número de
             * radicación con que se generó el documento que informa la no procedencia del trámite -
             * Enviar a correspondencia las parejas de (número de radicación del trámite, radicación
             * de cierre y el mensaje “Trámite cancelado por no procedencia?). Se deben enviar todas
             * las radicaciones asociadas en pares cada una con la misma respuesta. - Identificar
             * aquellas radicaciones que han sido cerradas en forma exitosa en el sistema de
             * correspondencia
             */
            String numeroRadicacionOrigen = "";

            if (this.tramite.getTramite() != null) {
                numeroRadicacionOrigen = this.tramite.getTramite().getNumeroRadicacion();
            } else {
                numeroRadicacionOrigen = this.tramite.getNumeroRadicacion();
            }

            // número de radicación, generado anteriormente con el documento 
            String numeroRadicacionGeneracionDoc = this.numeroRadicado;

            // mensaje al cerrar la radicación
            String mensaje = "Trámite cancelado por no procedencia";

            cerrarRadicacionTramite(tramite.getSolicitud().getNumero(),
                this.numeroRadicado, usuario.getLogin());

            this.habilitarBotonAdmitir = false;

            // avanzar el proceso
            this.forwardThisProcess();

        } catch (Exception e) {
            this.addMensajeError("Ocurrió un error al radicar el oficio " + e.getMessage());
        }

        //return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------        

    public boolean radicarPares(String origen, String numRadicacion, String numRadDocumento,
        String mensaje) {
        boolean resultado = false;
        try {
            Map<String, String> parametros = new HashMap<String, String>();
            parametros.put("ORIGEN_RADICACION", origen);
            parametros.put("NUMERO_RADICACION", numRadicacion);
            parametros.put("NUMERO_RADICACION_DOCUMENTO", numRadDocumento);
            parametros.put("MENSAJE", mensaje);
            resultado = true;
        } catch (Exception e) {
            this.addMensajeError("Ocurrió un error al radicar el oficio " + e.getMessage());
        }
        return resultado;
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     */
    private String obtenerNumeroRadicadoMutaciones() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.OFICIO_NO_PROCEDENCIA_MUTACIONES.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("No procedencia tramite"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     *
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     */
    private String obtenerNumeroRadicadoAutoestimacionRevisionAvaluoYViaGubernativa() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(
            ETipoDocumento.OFICIO_NO_PROCEDENCIA_AUTOESTIMACION_Y_REVISION_DE_AVALUO.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("No procedencia solicitud"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en Alfresco y
     * en la base de datos mediante el llamado al método guardar documento de la interfaz de
     * ITramite. Posteriormente le asocia el documento al trámite documento creado al principio.
     *
     * @author javier.aponte
     * @throws Exception
     */
    public void guardarDocumentoOficioNoProcedencia() throws Exception {

        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Documento documento = new Documento();

        tramiteDocumento.setTramite(this.tramite);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();

            //Para los tipos de trámites de mutaciones genera el reporte con un formato diferente 
            if (this.tramite.isMutacion()) {
                tipoDocumento.setId(ETipoDocumento.OFICIO_NO_PROCEDENCIA_MUTACIONES
                    .getId());
            } else {
                tipoDocumento.setId(
                    ETipoDocumento.OFICIO_NO_PROCEDENCIA_AUTOESTIMACION_Y_REVISION_DE_AVALUO
                        .getId());
            }

            documento.setTipoDocumento(tipoDocumento);
            if (this.oficioNoProcedencia != null) {
                documento.setArchivo(this.oficioNoProcedencia.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(this.tramite.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documento.setNumeroRadicacion(this.numeroRadicado);
            //v1.1.7
            //modificación 11/04/2014 se establece fecha de radicación
            documento.setFechaRadicacion(new Date());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.usuario, documento);

            if (documento != null &&
                 documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos()
                    .trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                this.getTramiteService().actualizarTramiteDocumento(tramiteDocumento);
            }

        } catch (Exception e) {
            throw new Exception("Error al guardar el oficio de no procedencia en alfresco: " + e);
        }
    }

//--------------------------------------------------------------------------------------------------
    public String cerrar() {
        return ConstantesNavegacionWeb.INDEX;
    }

//-------------  métodos relacionaddos con el BPM  ------
    /**
     * @see ProcessFlowManager#validateProcess()
     * @author pedro.garcia
     *
     * No hay que validar cosa alguna para mover el proceso para el trámite
     */
    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ProcessFlowManager#setupProcessMessage()
     * @author pedro.garcia
     *
     * Se hace dependiendo de si se aceptó o rechazó el trámite
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();

        usuariosActividad = new ArrayList<UsuarioDTO>();
        messageDTO = new ActivityMessageDTO();

        messageDTO.setUsuarioActual(this.usuario);

        messageDTO.setActivityId(this.currentProcessActivity.getId());
        if (this.banderaAdmitirTramite) {
            processTransition = ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES;
            observaciones = "El trámite se admite.";

            //felipe.cadena::12-09-2016::#19450::Se envian las actividades a los coordinadores y los responsables
            usuariosActividad.addAll(this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION));

            usuariosActividad.addAll(this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(), ERol.COORDINADOR));

            //se comentó por javier.aponte porque al hacer el llamado al método doDatabaseStatesUpdate
            //no actualiza el trámite
            //this.banderaAdmitirTramite = false;
        } else if (this.banderaRechazarTramite) {
            processTransition = ProcesoDeConservacion.ACT_ASIGNACION_TRAMITE_NO_PROCEDE;
            observaciones = "El trámite se rechaza";

            //usuarioDestinoActividad.setLogin(ProcesoDeConservacion.USUARIO_SISTEMA_PROCESOS);
            //se cambió al usuario de la sesión, para que quede el registro en process 
            //de quien fue el responsable de terminar la ejecución del tramite
            usuarioDestinoActividad.setLogin(this.usuario.getLogin());
            usuariosActividad.add(usuarioDestinoActividad);
        }

        messageDTO.setComment(observaciones);
        //D: según alejandro.sanchez la transición que importa es la que se define en la
        //   solicitud catastral; sin embargo, por si acaso se hace también en el messageDTO
        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        solicitudCatastral.setUsuarios(usuariosActividad);
        messageDTO.setSolicitudCatastral(solicitudCatastral);
        if (banderaAdmitirTramite) {
            if (this.tramite.getSolicitud().getTipo() != null) {
                if (this.tramite.getSolicitud().getTipo().equals(ESolicitudTipo.AUTOAVALUO.
                    getCodigo())) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(this.tramite.getFechaRadicacion());
                    messageDTO.getSolicitudCatastral().setNumeroRadicacion(this.tramite.
                        getNumeroRadicacion());
                    messageDTO.getSolicitudCatastral().setFechaRadicacion(cal);
                    messageDTO.getSolicitudCatastral().setTipoTramite(this.tramite.
                        getTipoTramiteCadenaCompleto());
                }
            }
        }
        this.setMessage(messageDTO);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * 1. si el trámite se admitió, se actualiza (con los cambios hechos en el método
     * admitirTramite()
     *
     * @see ProcessFlowManager#doDatabaseStatesUpdate()
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

        if (this.banderaAdmitirTramite == true || this.banderaRechazarTramite == true) {
            this.getTramiteService().actualizarTramite(this.tramite, this.usuario);
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * en el caso en que el llamado al BPM se debe hacer desde un botón que no está definido en
     * alguno de los template de procesos (porque por ejemplo hay al mismo tiempo dos botones que
     * mueven la actividad a diferentes transiciones), se debe hacer el llamado a los métodos como
     * si se estuviera implementando alguno de los métodos que avanzan el proceso del ProcessMB
     *
     * @author pedro.garcia
     */
    private void forwardThisProcess() {

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();

        }
        super.forwardProcess();

        //No se genera la replica cuando no se envia a depuración :: #8518
        //Solo se crea la replica para tramites de rectificacion de zonas :: #8750
        if (this.banderaAdmitirTramite && this.tramite.isRectificacionZona()) {
            //se genera la replica para validaciones de depuración  
            //felipe.cadena::ACCU::Se cambia la forma de generar la replica de consulta
            RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
                getManagedBean("recuperarTramites");
            recuperarTamitesMB.generarReplicaConsulta(this.tramite, this.usuario, null, 1);
//            this.getConservacionService().generarReplicaDeConsultaTramiteYAvanzarProceso(
//                    this.currentProcessActivity, this.usuario, null, false);
        }

        //OJO: no se debe hacer estio aquí porque al dar click en "radicar" NO se debe quitar el MB
        //     de la sesión para permitir que se refresque el reporte y que quede habilitado el botón "imprimir"
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        //Utilidades.removerManagedBean("estableceProcedenciaSolicitud");				
        //D: forzar el refrezco del árbol de tareas
        //this.tareasPendientesMB.init();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Retorna a la pantalla de establecer procedencia de la solicitud
     * masivamente
     *
     * @author javier.aponte
     */
    public String cerrarEstableceProcedenciaSolicitudIndividual() {

        return ConstantesNavegacionWeb.ESTABLECE_PROCEDENCIA_SOLICICTUD;
    }

    /**
     * action del botón "cerrar" Retorna a la pantalla de establecer procedencia de la solicitud
     * masivamente
     *
     * @author javier.aponte
     */
    public String cerrarPaginaPrincipal() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método para enviar el trámite a depurar información geográfica porque no hay información
     * espacial del predio
     *
     * @author javier.aponte
     */
    public String enviarADepurarInformacionGeografica() {

        this.banderaEnviarTramiteADepurarInformacionGeografica = true;

        GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

        UsuarioDTO usuarioDestino = generalMB.validarEnvioDepuracion(this.tramite, this.usuario,
            this.currentProcessActivity);
        if (usuarioDestino == null) {
            return "";
        }

        generalMB.crearRegistroTramiteDepuracion(this.tramite, this.usuario);

        //BM::enviar a depuracion
        try {
            //se envia a generar replica de consulta si existe la manzana, si no solo se avanza el proceso a depuración
            if (this.banderaTramiteTieneManzana) {
//                this.getConservacionService().generarReplicaDeConsultaTramiteYAvanzarProceso(
//                    this.currentProcessActivity,
//                    this.usuario,
//                    usuarioDestino, 
//                    true);
                //felipe.cadena::ACCU::Se cambia la forma de generar la replica de consulta
                RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
                    getManagedBean("recuperarTramites");
                recuperarTamitesMB.
                    generarReplicaConsulta(this.tramite, this.usuario, usuarioDestino, 0);

            } else {
                IAvanzarProcesoConservacion avanzarProcesoConservacionService = this.
                    getAvanzarProcesoConservacion();
                avanzarProcesoConservacionService.avanzarProcesoDepurarInformacionGeografica(
                    this.usuario, usuarioDestino,
                    this.currentProcessActivity);
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            this.addMensajeError("Error al avanzar el proceso.");
            return "";
        }

        this.tareasPendientesMB.init();
        return cerrar();

    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método para validar de nuevo la geometría de los predios
     *
     * @author javier.aponte
     */
    /*
     * @modified felipe.cadena :: 01-11-2013 :: se agregan validaciones para predios de mejoras
     * @modified pedro.garcia 03-12-2013 se revisa si es trámite es de cancelación de predio o de
     * rectificación por cancelación por doble inscripción para usar otro método para validar de
     * forma sincrónica
     */
    public void validarInconsistenciasDeNuevo() {
    	String resultado = null;
        this.banderaReclamaActividad = false;
        //Se eliminan las inconsistencias existentes
        this.getGeneralesService().eliminarInconsistenciasPorTramite(this.tramite.getId());
        if (this.tramite.isTramiteMasivo()) {

            if (this.tramite.getPredio().getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END) ||
                !this.tramite.getPredio().getNumeroPredial().endsWith(Constantes.FM_PH_END) ||
                 this.tramite.isSegundaEnglobe()) {
                Parametro parametroMasivo = this.getGeneralesService().getCacheParametroPorNombre(
                    EParametro.EDITOR_UMBRAL_ASYNC.toString());
                int numeroPredios = this.getConservacionService().contarPrediosAsociadosATramite(
                    this.tramite.getId());

                if (numeroPredios >= parametroMasivo.getValorNumero() || this.tramite.
                    isQuintaMasivo()) {
                    this.getGeneralesService().validarGeometriaPrediosAsincronico(this.tramite,
                        this.usuario);
                    this.cargarTramiteSeleccionado();
                    this.banderaValidandoAsincrono = true;
                    this.addMensajeWarn(
                        "Se esta realizando la validación de la geometría. Esto puede tardar unos minutos");
                    return;
                }
            }
        }

        try {
            if (!this.tramite.isSegundaEnglobe()) {
                if (!tramite.isQuinta() || tramite.isQuintaMasivo()) {
                    if (this.tramite.getPredio().isMejora()) {
                        resultado = this.validarGeometriaMejoras();
                    } else {

                        //D: CC-NP-CO-195 se valida diferente si el trámites es de cancelación de predio o de
                        //  rectificación por cancelación por doble inscripción
                        if (this.tramite.isCancelacionPredio() ||
                            (this.tramite.isEsRectificacion() && this.tramite.isRectificacionCDI())) {
                            boolean validarCancelPredioS;
                            validarCancelPredioS = this.getGeneralesService().
                                validarGeometriaPrediosTramCancelPredioS(this.tramite.getId(),
                                    this.usuario);
                            if (!validarCancelPredioS) {
                                LOGGER.error(
                                    "Ocurrió un error al validar sincrónicamente la geometría de predios de un trámite de cancelación de predio o de rectificación por cancelación por doble inscripción");
                                this.habilitarBotonAdmitir = false;
                                this.habilitarBotonEnviarADepurar = false;
                                this.habilitarBotonRechazarTramite = false;
                            }
                        } else {
                            resultado = this.getGeneralesService().
                                revalidarGeometriaPredios(this.tramite, this.usuario);
                        }
                    }
                }
            } //N: si es de englobe
            else {
                int k = 0;
                int lenPredios = this.tramite.getPredios().size();
                for (Predio p : this.tramite.getPredios()) {
                    if (p.isMejora()) {
                        k++;
                    }
                }
                if (k == lenPredios) {
                    resultado = this.validarGeometriaMejoras();
                } else if (k > 0) {
                    resultado = this.validarGeometriaMejorasTerreno();
                } else {

                    resultado = this.getGeneralesService().
                        revalidarGeometriaPredios(this.tramite, this.usuario);
                }
            }

            //D: si es este tipo de trámite los botones se activan usando el método que aplica las 
            //  reglas para este tipo de trámite
            if (this.tramite.isCancelacionPredio() ||
                (this.tramite.isEsRectificacion() && this.tramite.isRectificacionCDI())) {
                revisarValidacionGeometriaPrediosCancelPredio();
            } else {

                List<TramiteInconsistencia> tramiteInconsistenciasTemp;

                // Set de las inconsistencias geográficas del trámite.
                this.inconsistenciasGeograficas = new ArrayList<TramiteInconsistencia>();

                tramiteInconsistenciasTemp = this.getTramiteService().
                    buscarTramiteInconsistenciaPorTramiteId(this.tramite.getId());

                if ((tramite.isQuintaMasivo() && !this.tramite.getPredio().getNumeroPredial().
                    endsWith(Constantes.FM_PH_END)) &&
                     (tramiteInconsistenciasTemp == null || tramiteInconsistenciasTemp.isEmpty())) {
                    this.banderaGeometriaPrediosValidada = false;
                    this.habilitarBotonValidarInconsistencias=true;
                    this.addMensajeError("Ocurrió un error al validar la geometría de predios");
                    return;
                } else {
                	 this.habilitarBotonValidarInconsistencias=true;
                    this.inconsistenciasGeograficas.addAll(tramiteInconsistenciasTemp);
//                    for (TramiteInconsistencia ti : tramiteInconsistenciasTemp) {
//                        this.inconsistenciasGeograficas.add(ti);
//                    }
                }

                if (resultado == null) {
                    this.habilitarBotonAdmitir = true;
                    this.habilitarBotonEnviarADepurar = false;
                } else {
                    this.habilitarBotonAdmitir = false;
                    this.habilitarBotonEnviarADepurar = true;
                }
            }
        } catch (Exception e) {

            this.addMensajeError("Ocurrió un error al validar la geometría de predios");
            this.habilitarBotonAdmitir = false;
            this.habilitarBotonEnviarADepurar = true;
        }

        this.init();
        this.banderaReclamaActividad = false;
        this.cargarTramiteSeleccionado();
        this.banderaGeometriaPrediosValidada = true;
        this.banderaReclamaActividad = true;
    }

    /**
     * Valida geometria de manera asincronica, solo se debe usar para tramites que involucran muchos
     * predios
     *
     * @author felipe.cadena
     */
    public void validarGeomtriaASincronico() {
        this.getGeneralesService().validarGeometriaPrediosAsincronico(this.tramite, this.usuario);
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * Método para validar cuando los predios del tramite son de tipo mejora y de tipo terreno.
     *
     * @author felipe.cadena
     * @return
     */
    public String validarGeometriaMejorasTerreno() {

        List<Predio> prediosMejora = new ArrayList<Predio>();

        //obtener predios mejora
        for (Predio predio : this.tramite.getPredios()) {
            if (predio.isMejora()) {
                prediosMejora.add(predio);
            }
        }

        //las posibles inconsistencias detectadas se almacenan en la BD
        String resultadoTerreno = this.getGeneralesService().
            revalidarGeometriaPredios(this.tramite, this.usuario);

        List<TramiteInconsistencia> resultadoMejoras = this.getGeneralesService().
            validarGeometriaMejorasTramite(this.tramite, prediosMejora, this.usuario);

        if (resultadoMejoras.isEmpty() && resultadoTerreno == null) {
            return null;
        } else {
            return "";
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método para validar cuando los predios del tramite son de tipo mejora.
     *
     * @author felipe.cadena
     * @return
     */

    private String validarGeometriaMejoras() {

        List<Predio> prediosTerreno = new ArrayList<Predio>();
        List<String> numerosTerreno = new ArrayList<String>();

        //obtener numeros de terreno de las mejoras
        for (Predio predio : this.tramite.getPredios()) {
            String numeroTerreno = predio.getNumeroPredial().substring(0, 21);
            numerosTerreno.add(numeroTerreno + "000000000");
        }

        numerosTerreno = this.eliminarNumerosPredialesDuplicados(numerosTerreno);

        prediosTerreno = this.getConservacionService().obtenerPrediosPorNumerosPrediales(
            numerosTerreno);

        if (prediosTerreno.isEmpty()) {
            // this.addMensajeError("Las mejoras no tienen un predio de terreno asociado");
            this.crearInconsistenciaTerreno();
            return "";
        }

        List<TramiteInconsistencia> resultadoTerreno = this.getGeneralesService().
            validarGeometriaPrediosTramite(this.tramite, prediosTerreno, this.usuario);

        List<TramiteInconsistencia> resultadoMejoras = this.getGeneralesService().
            validarGeometriaMejorasTramite(this.tramite, this.tramite.getPredios(), this.usuario);

        //Se descarta la inconsistencia de control 
        if (resultadoMejoras.size() == 1 && resultadoMejoras.get(0).getInconsistencia().
            equals(EInconsistenciasGeograficas.NO_INCONSISTENCIA.getNombre())) {
            resultadoMejoras = new ArrayList<TramiteInconsistencia>();
        }

        if (resultadoMejoras.isEmpty() && resultadoTerreno.isEmpty()) {
            return null;
        } else {
            return "";
        }
    }

    /**
     * Metodo para crear una inconsistencia cuando una mejora no tiene predio de terreno
     * correspondiente
     *
     * @author felipe.cadena
     */
    private void crearInconsistenciaTerreno() {

        TramiteInconsistencia tic = new TramiteInconsistencia();
        tic.setFechaLog(new Date());
        tic.setUsuarioLog(this.usuario.getLogin());
        tic.setTramite(this.tramite);
        tic.setTipo(Constantes.INCONSISTENCIA_GEOGRAFICA_PRIORITARIA);
        tic.setInconsistencia(EInconsistenciasGeograficas.MEJORA_SIN_TERRENO.getNombre());
        tic.setDepurada(ESiNo.NO.getCodigo());
        tic.setNumeroPredial(this.tramite.getPredio().getNumeroPredial());

        tic = this.getTramiteService().guardarActualizarTramiteInconsistencia(tic);

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Mètodo para eliminar los números prediales repetidos que puedan existir en una lista
     *
     * @author felipe.cadena
     * @param numeros
     * @return
     */
    private List<String> eliminarNumerosPredialesDuplicados(List<String> numeros) {
        List<String> resultado = new ArrayList<String>();

        if (numeros.size() < 2) {
            return numeros;
        }
        String numero = numeros.get(0);
        resultado.add(numero);

        for (int i = 1; i < numeros.size(); i++) {
            String numAux = numeros.get(i);
            if (!resultado.contains(numAux)) {
                resultado.add(numAux);
            }
        }

        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace las validaciones para la activación de botones para el caso especial de un trámite de
     * tipo cancelación de predio o de rectificación por cancelación por doble inscripción. </br>
     * Se hace la activación de botones de 'admitir', 'rechazar' y 'enviar a depuración'.
     *
     * @author pedro.garcia
     */
    private boolean revisarValidacionGeometriaPrediosCancelPredio() {

        boolean validpCP = false;
        boolean isCDIGeografico = false;

        List<TramiteInconsistencia> inconsisteciasTramite = this.getTramiteService().
            buscarTramiteInconsistenciaPorTramiteId(this.tramite.getId());

        //D: se pregunta si es nula o vacía porque cuando ya se validó y no hubo inconsistencias se
        // crea un registro en Tramite_Inconsistencia que dice 'No se encuentran inconsistencias geográficas',
        // para saber que ya se hizo la validación
        this.habilitarBotonValidarInconsistencias = true;
        if (inconsisteciasTramite == null || inconsisteciasTramite.isEmpty()) {
            this.banderaGeometriaPrediosValidada = false;
            return false;
        }

        if (inconsisteciasTramite.size() == 1) {
            TramiteInconsistencia ti = inconsisteciasTramite.get(0);
            if (ti.getInconsistencia().equals(EInconsistenciasGeograficas.NO_INCONSISTENCIA.
                getNombre())) {
                validpCP = true;
            }
        }

        //D: activación de botones
        this.habilitarBotonAdmitir = false;
        this.habilitarBotonRechazarTramite = false;
        this.habilitarBotonEnviarADepurar = false;

        boolean predioExiste = true;
        for (TramiteInconsistencia inconsistencia : inconsisteciasTramite) {
            if (inconsistencia.getInconsistencia().equals(EInconsistenciasGeograficas.NO_PREDIO.
                getNombre())) {
                predioExiste = false;
                break;
            }
        }

        Predio predio;
        boolean tieneMejoras;
        if (this.tramite.getPredio() != null) {
            predio = this.tramite.getPredio();
            tieneMejoras = this.getConservacionService().tieneMejorasPredio(
                predio.getNumeroPredioDeNumeroPredial());
        } else {
            LOGGER.error("El trámite actual no trajo el predio asociado");
            return validpCP;
        }

        if (this.tramite.isRectificacionCDI()) {
            boolean manzanaExiste = true;
            boolean zonaExiste = true;
            if ((!predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo()) &&
                 tieneMejoras)) {
                this.addMensajeWarn(
                    "El predio tiene mejoras asociadas al terreno luego no puede realizarse la cancelación");
                this.habilitarBotonRechazarTramite = true;

            } else if (predio.isCondicionPropiedadRara()) {
                this.habilitarBotonRechazarTramite = true;
            } else if (predioExiste) {
                //validar si tiene manzanas
                for (TramiteInconsistencia inconsistencia : inconsisteciasTramite) {
                    if (inconsistencia.getInconsistencia().equals(
                        EInconsistenciasGeograficas.NO_MANZANA.getNombre())) {
                        manzanaExiste = false;
                        break;
                    }
                }

                //validar si tiene zonas
                for (TramiteInconsistencia inconsistencia : inconsisteciasTramite) {
                    if (inconsistencia.getInconsistencia().equals(
                        EInconsistenciasGeograficas.NO_ZONAS.getNombre())) {
                        zonaExiste = false;
                        break;
                    }
                }

                isCDIGeografico = true;

            }

            if (isCDIGeografico) {

                if (!manzanaExiste) {
                    this.addMensajeError(
                        "El predio no tiene manzana asociada se debe ingresar la manzana geográfica para continuar con el trámite");
                    this.habilitarBotonAdmitir = false;
                    this.mensajeCDI =
                        "El predio no tiene manzana asociada se debe ingresar la manzana geográfica para continuar con el trámite \n";
                }
                if (!zonaExiste) {
                    this.addMensajeError(
                        "El predio no tiene zonas asociadas se deben ingresar las zonas geográficas para continuar con el trámite");
                    this.habilitarBotonAdmitir = false;
                    this.mensajeCDI +=
                        "El predio no tiene zonas asociadas se deben ingresar las zonas geográficas para continuar con el trámite \n";
                }
                if (zonaExiste && manzanaExiste) {
                    this.habilitarBotonAdmitir = true;
                }

                //Se transforma el tramite actual en un tramite geografico
                if (this.tramite.getTramiteRectificacions() != null && !this.tramite.
                    getTramiteRectificacions().isEmpty()) {
                    TramiteRectificacion tr = this.tramite.getTramiteRectificacions().get(0);
                    DatoRectificar dr = new DatoRectificar();
                    dr.setId(Constantes.TIPO_DATO_RECTIFICACION_CDI_GEO);
                    tr.setDatoRectificar(dr);
                    this.getTramiteService().guardarActualizarTramite2(this.tramite);
                } else {
                    this.addMensajeError("Error en los datos a rectiifcar del predio");
                    this.habilitarBotonAdmitir = true;
                }
                this.habilitarBotonRechazarTramite = true;

            } else {
                this.habilitarBotonAdmitir = true;
                this.habilitarBotonRechazarTramite = true;
            }
        } else {

            if (((predioExiste && validpCP) || !predioExiste) ||
                
                (!predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo()) &&
                !tieneMejoras) ||
                 (!predio.isCondicionPropiedadRara())) {

                this.habilitarBotonAdmitir = true;
                this.habilitarBotonRechazarTramite = true;
            } else if ((predioExiste && !validpCP) ||
                
                (!predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo()) &&
                !tieneMejoras) ||
                 (!predio.isCondicionPropiedadRara())) {

                this.habilitarBotonEnviarADepurar = true;
                this.habilitarBotonRechazarTramite = true;
            } else if ((!predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.
                getCodigo()) &&
                 tieneMejoras)) {
                this.addMensajeWarn(
                    "El predio tiene mejoras asociadas al terreno luego no puede realizarse la cancelación");
                this.habilitarBotonRechazarTramite = true;

            } else if (predio.isCondicionPropiedadRara()) {
                this.habilitarBotonRechazarTramite = true;
            }
        }
        return validpCP;

    }

    /**
     * Método que se ejecuta cuando se selecciona el trámite en la tabla de establecer procedencia
     * de la solicitud masiva
     *
     * @author javier.aponte
     */
    public void establecerTramiteSeleccionado() {

        this.tramite = this.getTramiteService().buscarTramitePorTramiteIdAsignacion(this.tramite.
            getId());

        VistaDetallesSolicitudMB vistaDetallesSolicitudMB =
            (VistaDetallesSolicitudMB) UtilidadesWeb.getManagedBean("vistaDetallesSolicitud");
        vistaDetallesSolicitudMB.setSolicitudSeleccionada(this.tramite.getSolicitud());

    }

    /**
     * Método que se ejecuta cuando se selecciona la opción de admitir trámites masivamente
     *
     * @author javier.aponte
     */
    public String admitirTramitesMasivamente() {

        this.banderaAdmitirTramite = true;

        //avanzar los trámites
        for (Tramite tramiteSelected : this.selectedTramites) {

            //felipe.cadena::15-09-2016::#19450::Reclamar actividad por parte del usuario autenticado     
            if (!this.reclamarActividad(tramiteSelected)) {
                continue;
            }
            this.tramite = tramiteSelected;
            this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
                this.tramite.getId());

            this.forwardThisProcess();
            //felipe.cadena::01-10-2016::#19450::Se envia correo si existen tramites con error en raclamar la actividad
            this.enviarCorreoErrorAvance();

        }
        return this.cerrarPaginaPrincipal();
    }

    /**
     * Método que se ejecuta cuando se selecciona un trámite en la tabla de admitir trámites
     * masivamente
     *
     * @author javier.aponte
     */
    public void seleccionTramiteListener() {

        if (!this.validarTramitesActualizacion()) {
            return;
        }
        this.validarHabilitarAdmitirTramitesMasivamente();
    }

    /**
     * Método que se ejecuta cuando se desselecciona un trámite en la tabla de admitir trámites
     * masivamente
     *
     * @author javier.aponte
     */
    public void desseleccionTramiteListener() {

        if (!this.validarTramitesActualizacion()) {
            return;
        }
        this.validarHabilitarAdmitirTramitesMasivamente();
    }

    /**
     * Método encargado de determinar si se habilita el botón de admitir trámites masivamente
     *
     * @author javier.aponte
     */
    private void validarHabilitarAdmitirTramitesMasivamente() {

        this.habilitarBotonAdmitirMasivo = true;

        if (this.selectedTramites != null && !(this.selectedTramites.length == 0)) {
            for (Tramite t : this.selectedTramites) {
                if (!(t.isPrimera() || t.isEsComplementacion() || (t.isEsRectificacion() &&
                     !t.isRectificacionArea() && !t.isRectificacionCancelacionDobleInscripcion() &&
                    !t.isRectificacionZona()))) {
                    this.habilitarBotonAdmitirMasivo = false;
                    this.addMensajeWarn(
                        "Ha seleccionado trámite(s) al (los) cual(es) se debe determinar la procedencia en forma individual");
                    break;
                }
                if (t.getEstadoBloqueo() != null && t.getEstadoBloqueo().equals(
                    ETramiteEstadoBloqueo.SUSPENDIDO.getCodigo())) {
                    this.habilitarBotonAdmitirMasivo = false;
                    this.addMensajeWarn(
                        "Ha seleccionado trámite(s) que se encuentra(n) bloqueado(s). Debe determinar la procedencia en forma individual");
                    break;
                }
            }
        } else {
            this.habilitarBotonAdmitirMasivo = false;
        }
    }

    /**
     * Método encargado de determinar si se habilita el botón de admitir trámites masivamente paa el
     * caso de preios con tramites en actualizacion.
     *
     * @author felipe.cadena
     */
    private boolean validarTramitesActualizacion() {

        this.habilitarBotonAdmitirMasivo = true;

        if (this.selectedTramites != null && !(this.selectedTramites.length == 0)) {
            for (Tramite t : this.selectedTramites) {
                if (t.isTramitesActualizacionRelacionados()) {
                    this.habilitarBotonAdmitirMasivo = false;
                    this.addMensajeWarn(ECodigoErrorVista.DETERMINA_PROCEDENCIA_ACT_001.
                        getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.DETERMINA_PROCEDENCIA_ACT_001.getMensajeTecnico());
                    break;
                }
            }
        } else {
            this.habilitarBotonAdmitirMasivo = false;
        }

        return this.habilitarBotonAdmitirMasivo;
    }

    /**
     * Determina si se ha enviado un job previo de inconsistencias para este tramite
     *
     * @author felipe.cadena
     */
    private void determinaJobInconsistencias() {

        this.banderaValidandoAsincrono = false;
        List<String> estados = new ArrayList<String>();
        estados.add(ProductoCatastralJob.AGS_EN_EJECUCION);
        estados.add(ProductoCatastralJob.AGS_ESPERANDO);
        estados.add(ProductoCatastralJob.AGS_TERMINADO);
        estados.add(ProductoCatastralJob.AGS_ERROR);

        List<ProductoCatastralJob> jobs = this.getGeneralesService().
            obtenerJobsPorEstadoTramiteIdTipo(estados, this.tramite.getId(),
                ProductoCatastralJob.SIG_JOB_VALIDAR_INCONSISTENCIAS);

        if (jobs != null && !jobs.isEmpty()) {
            this.banderaValidandoAsincrono = true;
        }
    }

    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author felipe.cadena
     */
    private boolean reclamarActividad(Tramite tramiteReclamar) {
        Actividad actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        List<Actividad> actividades;

        if (actividad == null) {
            actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (actividades != null && !actividades.isEmpty()) {
                for (Actividad actTemp : actividades) {
                    if (actTemp.getIdObjetoNegocio() == tramiteReclamar.getId()) {
                        actividad = actTemp;
                        break;
                    }
                }
            }
        }

        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.usuario.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    if (tramiteReclamar.getNumeroRadicacion() != null && !tramiteReclamar.
                        getNumeroRadicacion().isEmpty()) {
                        this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                            tramiteReclamar.getNumeroRadicacion()));
                    } else {
                        this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA_NO_RAD.
                            getMensajeUsuario(tramiteReclamar.getSolicitud().getNumero()));
                    }
                    this.tramitesErrorAvanzar.add(tramiteReclamar.getId());
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.usuario);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                if (tramiteReclamar.getNumeroRadicacion() != null && !tramiteReclamar.
                    getNumeroRadicacion().isEmpty()) {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                        tramiteReclamar.getNumeroRadicacion()));
                } else {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA_NO_RAD.getMensajeUsuario(
                        tramiteReclamar.getSolicitud().getNumero()));
                }
                this.tramitesErrorAvanzar.add(tramiteReclamar.getId());
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }
    }

    /**
     * Metodo para enviar correo a usuario cuando se presenta error en el avanzae por que la
     * actividad ya fue reclamada.
     *
     * @author felipe.cadena
     */
    private void enviarCorreoErrorAvance() {

        if (!this.tramitesErrorAvanzar.isEmpty()) {

            StringBuilder radicados = new StringBuilder();
            StringBuilder solicitudes = new StringBuilder();
            String correoDestino = this.usuario.getEmail();
            String asunto =
                ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_TRAMITES_RECLAMADOS;

            if (correoDestino == null || correoDestino.isEmpty()) {
                return;
            }

            //Parametros del correo
            String[] parametrosPlantillaContenido = new String[5];
            parametrosPlantillaContenido[0] = this.tareasPendientesMB.
                getListaInstanciasActividadesSeleccionadas().get(0).getNombre();

            List<Tramite> tramitesError = this.getTramiteService().
                buscarTramitesConEjecutorAsignado(idsTramitesActividades);

            for (Tramite tramiteE : tramitesError) {
                if (tramiteE.getNumeroRadicacion() != null && !tramiteE.getNumeroRadicacion().
                    isEmpty()) {
                    radicados.append(tramiteE.getNumeroRadicacion()).append(" <br/>");
                } else {
                    solicitudes.append(tramiteE.getSolicitud().getNumero()).append(" <br/>");
                }
            }

            if (!radicados.toString().isEmpty()) {
                parametrosPlantillaContenido[1] = Constantes.TEXTO_OTROS_TRAMITES_RECLAMADOS;
                parametrosPlantillaContenido[2] = radicados.toString();
            } else {
                parametrosPlantillaContenido[1] = "";
                parametrosPlantillaContenido[2] = "";
            }

            if (!solicitudes.toString().isEmpty()) {
                parametrosPlantillaContenido[3] =
                    Constantes.TEXTO_AUTOESTIMACION_TRAMITES_RECLAMADOS;
                parametrosPlantillaContenido[4] = solicitudes.toString();
            } else {
                parametrosPlantillaContenido[3] = "";
                parametrosPlantillaContenido[4] = "";
            }

            // Consulta de la plantilla del contenido.
            Plantilla plantillaContenido = this.getGeneralesService().recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_TRAMITES_AVANZADOS.getCodigo());

            // Consulta del contenido del correo.
            String contenidoCorreo = plantillaContenido.getHtml();

            if (contenidoCorreo != null) {

                // Se instancia el messageFormat.
                Locale colombia = new Locale("ES", "es_CO");
                MessageFormat messageFormat = new MessageFormat(
                    contenidoCorreo, colombia);

                // Se reemplazan los parametros en el contenido de la
                // plantilla.
                contenidoCorreo = messageFormat
                    .format(parametrosPlantillaContenido);

            }

            //format asunto
            Locale colombia = new Locale("ES", "es_CO");
            MessageFormat messageFormat = new MessageFormat(
                asunto, colombia);

            asunto = messageFormat
                .format(parametrosPlantillaContenido);

            // Envio de correo electronico
            this.getGeneralesService().enviarCorreo(correoDestino, asunto,
                contenidoCorreo, null, null);

        }

    }

    private boolean tienePredioBloqueado() {
        boolean bTienePredioBloqueado = false;
        if (this.tramite.getPredio() != null) {
            bTienePredioBloqueado = esPredioBloqueado(tramite.getPredio());
        } else if (this.tramite.getPredios() != null) {
            for (final Predio predio : tramite.getPredios()) {
                if (esPredioBloqueado(predio)) {
                    bTienePredioBloqueado = true;
                    break;
                }
            }
        }
        return bTienePredioBloqueado;
    }

    private boolean esPredioBloqueado(final Predio predio) {
        List<PredioBloqueo> bloqueos = this.getConservacionService().
            obtenerPredioBloqueosPorNumeroPredial(predio.getNumeroPredial());
        predio.setPredioBloqueos(bloqueos);
        return predio.isEstaBloqueado();
    }

    //18779.  Cerrar trámite en cordis 15/09/2016 wilmanjose.vega
    private boolean cerrarRadicacionTramite(final String radicado,
        final String radicadoResponde, final String usuario) {
        boolean resultado = true;
        final Object[] resultadoCierre = this.getTramiteService()
            .finalizarRadicacion(radicado, radicadoResponde, usuario);
        if (resultadoCierre != null) {
            for (final Object objeto : resultadoCierre) {
                final List lista = (List) objeto;
                if (lista != null && !lista.isEmpty()) {
                    int numeroErrores = 0;
                    for (final Object obj : lista) {
                        final Object[] arrObjetos = (Object[]) obj;
                        //TODO: Eliminar la validación del código de error de oracle luego de que 
                        //el servicio de CORDIS solucione el error: http://redmine.igac.gov.co/issues/21662
                        if (!arrObjetos[0].equals("0") &&
                            !arrObjetos[1].toString().contains("0-ORA-0000")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + arrObjetos[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al cerrar radicación");
                        resultado = false;
                    }
                }
            }
        }
        return resultado;
    }

	public boolean isHabilitarBotonValidarInconsistencias() {
		return habilitarBotonValidarInconsistencias;
	}

	public void setHabilitarBotonValidarInconsistencias(boolean habilitarBotonValidarInconsistencias) {
		this.habilitarBotonValidarInconsistencias = habilitarBotonValidarInconsistencias;
	}

//end of class    
}