/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.tests;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro.garcia
 *
 * Otra forma de hacerlo: Se puede usar la anotación @ManagedBean, pero debe ser usada también en el
 * mb que usa a este, y allá se debe usar @ManagedProperty para inyectar éste
 */
@Component("managedbeanUsedByOtherMB")
//@ManagedBean(name="managedbeanUsedByOtherMB")
@Scope("session")
public class MBUsedByOtherMB implements Serializable {

    private String cadena;

    @PostConstruct
    public void init() {
        this.cadena = "just initialized";
    }

    public void changeCadenaValue() {
        this.cadena = new Date().toString();
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

}
