/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.transversal;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Managed bean del cu Consultar (Administrar) información predial
 *
 * @author pedro.garcia
 * @cu CU-AC-SA-068
 */
@Component("consultaInformacionPredial")
@Scope("session")
public class ConsultaInformacionPredialMB extends SNCManagedBean {

    /**
     * Serial ID generado
     */
    private static final long serialVersionUID = 297583064013776641L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaInformacionPredialMB.class);

    @Autowired
    private IContextListener contextoWebApp;

    /**
     * avalúo del que se van a ver los predios
     */
    private Avaluo avaluoSeleccionado;

    /**
     * predios del avalúo
     */
    private ArrayList<Predio> prediosAvaluos;

    /**
     * predio del que se van a ver los detalles
     */
    private Predio selectedPredioForDetailsChecking;

    /**
     * solicitante del trámite correspondiente al avalúo actual
     */
    private SolicitanteSolicitud solicitanteTramite;

    /**
     * usuario con sesión en el sistema
     */
    private UsuarioDTO currentUser;

    /**
     * territorial del usuario actual
     */
    private EstructuraOrganizacional territorialUsuario;

    // -------------------- VARIABLES USADAS PARA INTEGRACION CON VISOR
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

//----------------------   getters and setters   --------------------------------------
    public EstructuraOrganizacional getTerritorialUsuario() {
        return this.territorialUsuario;
    }

    public void setTerritorialUsuario(EstructuraOrganizacional territorialUsuario) {
        this.territorialUsuario = territorialUsuario;
    }

    public SolicitanteSolicitud getSolicitanteTramite() {
        return this.solicitanteTramite;
    }

    public void setSolicitanteTramite(SolicitanteSolicitud solicitanteTramite) {
        this.solicitanteTramite = solicitanteTramite;
    }

    public ArrayList<Predio> getPrediosAvaluos() {
        return this.prediosAvaluos;
    }

    public void setPrediosAvaluos(ArrayList<Predio> prediosAvaluos) {
        this.prediosAvaluos = prediosAvaluos;
    }

    public Predio getSelectedPredioForDetailsChecking() {
        return this.selectedPredioForDetailsChecking;
    }

    public void setSelectedPredioForDetailsChecking(Predio selectedPredioForDetailsChecking) {
        this.selectedPredioForDetailsChecking = selectedPredioForDetailsChecking;
    }

    public String getApplicationClientName() {
        return this.applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return this.moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return this.moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    //----------------------    methods   -----------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de ConsultaInformacionPredialMB");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();
        String codigoTerritorial = this.currentUser.getCodigoEstructuraOrganizacional();
        this.territorialUsuario = this.getGeneralesService().obtenerTerritorialPorCodigo(
            codigoTerritorial);

        inicializarVariablesVisorGIS();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {

        this.applicationClientName = this.contextoWebApp.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.AVALUOS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }
//--------------------------------------------------------------------------------------------------

    public void inicializarConsultaPredioMB() {

        //this.predioSeleccionado = this.getConservacionService().buscarPredioPorNumeroPredialParaAvaluoComercial(this.numeroPredio);
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        mb001.cleanAllMB();

        if (this.selectedPredioForDetailsChecking != null) {
            mb001.setSelectedPredio1(this.selectedPredioForDetailsChecking);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que se llama desde otras páginas para consultar la información predial de un avalúo
     *
     * @author pedro.garcia
     */
    public void cargarInformacionPredial() {

        if (this.avaluoSeleccionado != null) {
            this.avaluoSeleccionado = this.getAvaluosService().obtenerInfoEncabezadoAvaluoPorId(
                this.avaluoSeleccionado.getId());

            this.solicitanteTramite = this.avaluoSeleccionado.getSolicitante();

            this.prediosAvaluos = (ArrayList<Predio>) this.getAvaluosService()
                .obtenerPrediosDeAvaluo(this.avaluoSeleccionado.getId());

        }
    }

//end of class
}
