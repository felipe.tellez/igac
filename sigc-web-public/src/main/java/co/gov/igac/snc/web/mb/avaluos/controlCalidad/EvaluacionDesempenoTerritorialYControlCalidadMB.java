package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Asignacion;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoEvaluacionDetalle;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.VariableEvaluacionAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.persistence.util.EAvaluoEvaluacionDetalles;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.EVariableEvaluacionAvaluoTipo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.PrevisualizacionDocMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

public class EvaluacionDesempenoTerritorialYControlCalidadMB extends
    SNCManagedBean {

    private static final long serialVersionUID = 7665534743536070325L;

    protected static final Logger LOGGER = LoggerFactory
        .getLogger(EvaluacionDesempenoProfesionalAvaluadorMB.class);

    /**
     * Fecha de la evaluación
     */
    protected Date fecha;

    /**
     * Observaciones generales de la evaluación
     */
    protected String observacionesGenerales;

    /**
     * Variable que almacena la suma parcial de puntajes de los detalles de CALIDAD
     */
    protected Double sumaParcialCalidad;

    /**
     * Variable que almacena la suma total de los puntajes de los detalles de la evaluación
     */
    protected Double sumaTotal;

    /**
     * Avaluo de los avaluadore a evaluar
     */
    protected Avaluo avaluoSeleccionado;

    /**
     * Lista de todos los detalles de la evaluación del avalúo seleccionado
     */
    protected List<AvaluoEvaluacionDetalle> listaEvaluacionDetalles;

    /**
     * Lista de los detalles de CALIDAD de la evaluación del avalúo seleccionado
     */
    protected List<AvaluoEvaluacionDetalle> listaEvaluacionDetallesCalidad;

    /**
     * Lista de los detalles de EFICACIA de la evaluación del avalúo seleccionado
     */
    protected List<AvaluoEvaluacionDetalle> listaEvaluacionDetallesEficacia;

    /**
     * Lista que se carga cuando un avalúo no tiene una evaluación activa
     */
    protected List<VariableEvaluacionAvaluo> listaVariablesEvaluacion;

    /**
     * Variable que indica si el avaluo fue creado en una territorial o en sede central</br>
     * <b>True</b> El avaluo fue creado en una territorial</br>
     * <b>False</b> El avaluo no fue creado en una territorial</br>
     */
    protected boolean banderaEsAvaluoDeTerritorial;

    /**
     * Variable que indica si la evaluacion ya fue almacenada </br> <b>True</b>
     * La evaluacion ya fue almacenada</br> <b>False</b> la evaluacion NO ha sido almacenada</br>
     */
    protected boolean banderaEsEvaluacionAlmacenada;

    /**
     * Profesional encargado del control de calidad del avalúo
     */
    protected ProfesionalAvaluo profesionalControlCalidad;

    /**
     * Variable que almacena la suma parcial de puntajes de los detalles de EFICACIA
     */
    protected Double sumaParcialEficacia;

    /**
     * usuario actual del sistema
     */
    protected UsuarioDTO usuario;

    /**
     * Asignacion asociada al avalúo
     */
    protected Asignacion asignacionAvaluoSeleccionado;

    /**
     * Evaluación del avalúo seleccionado
     */
    protected AvaluoEvaluacion evaluacionAvaluoSeleccionado;

    /**
     * Codigo de la actividad asociada a la asignacion, debe ser un código de
     * {@link EAvaluoAsignacionProfActi}
     */
    protected String actividadAsignacion;

    // --------------------------------Reportes---------------------------------
    /**
     * Archivo generado para el comunicado
     */
    protected ReporteDTO reporteDTO;

    protected ReportesUtil reportsService = ReportesUtil.getInstance();

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public Date getFecha() {
        return fecha;
    }

    public Avaluo getAvaluoSeleccionado() {
        return avaluoSeleccionado;
    }

    public List<AvaluoEvaluacionDetalle> getListaEvaluacionDetalles() {
        return listaEvaluacionDetalles;
    }

    public String getObservacionesGenerales() {
        return observacionesGenerales;
    }

    public void setObservacionesGenerales(String observacionesGenerales) {
        this.observacionesGenerales = observacionesGenerales;
    }

    public List<AvaluoEvaluacionDetalle> getListaEvaluacionDetallesCalidad() {
        return listaEvaluacionDetallesCalidad;
    }

    public List<AvaluoEvaluacionDetalle> getListaEvaluacionDetallesEficacia() {
        return listaEvaluacionDetallesEficacia;
    }

    public List<VariableEvaluacionAvaluo> getListaVariablesEvaluacion() {
        return listaVariablesEvaluacion;
    }

    public Double getSumaParcialCalidad() {
        return sumaParcialCalidad;
    }

    public Double getSumaTotal() {
        return sumaTotal;
    }

    public ProfesionalAvaluo getProfesionalControlCalidad() {
        return profesionalControlCalidad;
    }

    public Double getSumaParcialEficacia() {
        return sumaParcialEficacia;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getReporteDTO() {
        return this.reporteDTO;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsAvaluoDeTerritorial() {
        return banderaEsAvaluoDeTerritorial;
    }

    public boolean isBanderaEsEvaluacionAlmacenada() {
        return banderaEsEvaluacionAlmacenada;
    }

    // ---------------------------------Métodos---------------------------------
    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    protected void iniciarDatosGeneral() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#iniciarDatos");

        this.iniciarDatosEvaluacionGeneral();
        this.iniciarBanderasGeneral();
        this.iniciarListasGeneral();
        this.iniciarSumasPuntajesGeneral();
        this.consultarAsignacionAvaluoGeneral();
        this.consultarEvaluacionAvaluoGeneral();
        this.consultarTodosDetallesEvaluacionGeneral();

        this.cargarListasDetallesEvaluacionCalidadGeneral();
        this.cargarListasDetallesEvaluacionEficaciaGeneral();

        this.calcularSumaParcialCalidadGeneral();
        this.calcularSumaParcialEficaciaGeneral();

        this.calcularTotalGeneral();

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#iniciarDatos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar los atributos de tipo boolean
     *
     * @author rodrigo.hernandez
     */
    protected void iniciarBanderasGeneral() {
        this.banderaEsAvaluoDeTerritorial = false;
        this.banderaEsEvaluacionAlmacenada = false;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar los datos de la evaluacion
     *
     * @author rodrigo.hernandez
     */
    protected void iniciarDatosEvaluacionGeneral() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        /*
         * TODO: rodrigo.hernandez :: 20-11-2012 :: Este avalúo está quemado.
         */
        this.avaluoSeleccionado = this.getAvaluosService()
            .consultarAvaluoPorSecRadicadoConAtributos(
                "60402012ER00041219-4");

        /*
         * Se inicia la fecha de la evaluación
         */
        this.fecha = Calendar.getInstance().getTime();

        /*
         * Se determina si el avaluo fue hecho en una territorial o no
         */
        if (this.avaluoSeleccionado.getEstructuraOrganizacionalCod() != null) {
            this.banderaEsAvaluoDeTerritorial = true;
        } else {
            this.banderaEsAvaluoDeTerritorial = false;
        }

        /*
         * Dependiendo de si el avaluo fue hecho en una territorial o no, se crea el profesional de
         * control de calidad
         */
        if (this.banderaEsAvaluoDeTerritorial) {
            for (AvaluoAsignacionProfesional aap : this.avaluoSeleccionado
                .getAvaluoAsignacionProfesionals()) {
                if (aap.getActividad().equalsIgnoreCase(
                    EAvaluoAsignacionProfActi.CC_TERRITORIAL.getCodigo())) {
                    this.profesionalControlCalidad = aap.getProfesionalAvaluo();
                    break;
                }
            }
        } else {
            for (AvaluoAsignacionProfesional aap : this.avaluoSeleccionado
                .getAvaluoAsignacionProfesionals()) {
                if (aap.getActividad().equalsIgnoreCase(
                    EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo())) {
                    this.profesionalControlCalidad = aap.getProfesionalAvaluo();
                    break;
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar las listas
     *
     * @author rodrigo.hernandez
     */
    protected void iniciarListasGeneral() {
        this.listaEvaluacionDetalles = new ArrayList<AvaluoEvaluacionDetalle>();
        this.listaEvaluacionDetallesCalidad = new ArrayList<AvaluoEvaluacionDetalle>();
        this.listaEvaluacionDetallesEficacia = new ArrayList<AvaluoEvaluacionDetalle>();
        this.listaVariablesEvaluacion = new ArrayList<VariableEvaluacionAvaluo>();
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar las sumas Parcial de detalles de Calidad y Total de detalles de la
     * evaluacion
     *
     * @author rodrigo.hernandez
     */
    protected void iniciarSumasPuntajesGeneral() {
        this.sumaParcialCalidad = 0D;
        this.sumaParcialEficacia = 0D;
        this.sumaTotal = 0D;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para consultar la asignación activa del avalúo
     *
     * @author rodrigo.hernandez
     */
    protected void consultarAsignacionAvaluoGeneral() {
        this.asignacionAvaluoSeleccionado = this.getAvaluosService()
            .consultarAsignacionActualAvaluo(
                this.avaluoSeleccionado.getId(),
                this.actividadAsignacion);
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para consultar TODAS las variables de la evaluacion
     *
     * @author rodrigo.hernandez
     */
    protected void consultarTodosDetallesEvaluacionGeneral() {

        boolean banderaExisteEvaluacion = false;

        /*
         * Se valida si existe una evaluación asociada al avaluo
         */
        banderaExisteEvaluacion = this.consultarEvaluacionAvaluoGeneral();

        /*
         * Si existe una evaluacion activa
         */
        if (banderaExisteEvaluacion) {
            /*
             * Se cargan las variables de la evaluación asociada al avalúo
             */
            this.listaEvaluacionDetalles = this.evaluacionAvaluoSeleccionado
                .getAvaluoEvaluacionDetalles();
        } /*
         * Si no existe una evaluación
         */ else {

            AvaluoEvaluacionDetalle avaluoEvaluacionDetalle = null;

            /*
             * Se crea la evaluacion para asociarla al avalúo
             */
            this.evaluacionAvaluoSeleccionado = new AvaluoEvaluacion();

            /*
             * Se consultan las variables para asociarlas a la evaluación creada
             */
            this.listaVariablesEvaluacion = this.getAvaluosService()
                .consultarTodasVariableEvaluacionAvaluo();

            /*
             * Se crean los detalles de la evaluacion
             */
            for (VariableEvaluacionAvaluo vea : this.listaVariablesEvaluacion) {
                avaluoEvaluacionDetalle = new AvaluoEvaluacionDetalle();
                avaluoEvaluacionDetalle.setValor(0D);
                avaluoEvaluacionDetalle.setVariableEvaluacionAvaluo(vea);
                avaluoEvaluacionDetalle
                    .setAvaluoEvaluacion(this.evaluacionAvaluoSeleccionado);

                /*
                 * Se agrega a la lista de detalles de la evaluacion
                 */
                this.listaEvaluacionDetalles.add(avaluoEvaluacionDetalle);
            }

        }

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para consultar la evaluación asociada al avalúo seleccionado
     *
     * @author rodrigo.hernandez
     */
    protected boolean consultarEvaluacionAvaluoGeneral() {

        boolean existeEvaluacion = false;

        this.evaluacionAvaluoSeleccionado = this.getAvaluosService()
            .consultarEvaluacionesPorAvaluoAsignacionConAtributos(
                this.avaluoSeleccionado.getId(),
                this.asignacionAvaluoSeleccionado.getId());

        /*
         * Se valida que el avaluo ya tenga asociada una evaluación
         */
        if (this.evaluacionAvaluoSeleccionado != null) {
            existeEvaluacion = true;
        }

        return existeEvaluacion;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Carga la lista de las variables de tipo CALIDAD
     *
     * @author christian.rodriguez
     */
    protected void cargarListasDetallesEvaluacionCalidadGeneral() {
        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetalles) {
            /*
             * Se valida si el detalle de evaluacion es de tipo CALIDAD
             */
            if (aed.getVariableEvaluacionAvaluo().getTipo()
                .equals(EVariableEvaluacionAvaluoTipo.CALIDAD.getValor())) {

                this.listaEvaluacionDetallesCalidad.add(aed);

            }

        }
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Carga la lista de las variables de tipo EFICACIA
     *
     * @author christian.rodriguez
     */
    protected void cargarListasDetallesEvaluacionEficaciaGeneral() {
        /*
         * Variables auxiliares para detalles de EFICACIA
         */
        int numeroDiasAtraso = 0;
        String nombreVariableEficacia = "";

        /*
         * Se consulta el numero de dias de atraso
         */
        numeroDiasAtraso = this.getAvaluosService().calcularTiempoRetrasoDias(
            this.avaluoSeleccionado.getId());

        /*
         * Se establece el nombre de la variable de EFICACIA a la que se le va a asignar el puntaje
         */
        if (numeroDiasAtraso == EAvaluoEvaluacionDetalles.FECHA_ACORDADA
            .getValorDetalle()) {
            nombreVariableEficacia = EAvaluoEvaluacionDetalles.FECHA_ACORDADA
                .getNombreDetalle();

        } else if (numeroDiasAtraso == EAvaluoEvaluacionDetalles.UN_DIA_ATRASO
            .getValorDetalle()) {
            nombreVariableEficacia = EAvaluoEvaluacionDetalles.UN_DIA_ATRASO
                .getNombreDetalle();

        } else if (numeroDiasAtraso == EAvaluoEvaluacionDetalles.DOS_DIAS_ATRASO
            .getValorDetalle()) {
            nombreVariableEficacia = EAvaluoEvaluacionDetalles.DOS_DIAS_ATRASO
                .getNombreDetalle();

        } else if (numeroDiasAtraso == EAvaluoEvaluacionDetalles.TRES_O_MAS_DIAS_ATRASO
            .getValorDetalle()) {
            nombreVariableEficacia = EAvaluoEvaluacionDetalles.TRES_O_MAS_DIAS_ATRASO
                .getNombreDetalle();
        }

        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetalles) {
            if (aed.getVariableEvaluacionAvaluo().getTipo()
                .equals(EVariableEvaluacionAvaluoTipo.EFICACIA.getValor())) {

                if (aed.getVariableEvaluacionAvaluo().getNombre()
                    .equals(nombreVariableEficacia)) {
                    aed.setValor(aed.getVariableEvaluacionAvaluo()
                        .getValorMinimo());

                    LOGGER.debug(" aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaagh " +
                         String.valueOf(aed.isEsEditable()));
                }

                this.listaEvaluacionDetallesEficacia.add(aed);
            }
        }

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para calcular la suma parcial de los detalles de CALIDAD de la evaluación
     *
     * @author rodrigo.hernandez
     */
    protected void calcularSumaParcialCalidadGeneral() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#calcularSumaParcialCalidad");

        this.sumaParcialCalidad = 0D;

        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetallesCalidad) {
            this.sumaParcialCalidad = this.sumaParcialCalidad + aed.getValor();
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#calcularSumaParcialCalidad");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para calcular la suma parcial de los detalles de EFICACIA de la evaluación
     *
     * @author rodrigo.hernandez
     */
    protected void calcularSumaParcialEficaciaGeneral() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#calcularSumaParcialEficacia");

        this.sumaParcialEficacia = 0D;

        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetallesEficacia) {
            this.sumaParcialEficacia = this.sumaParcialEficacia +
                 aed.getValor();
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#calcularSumaParcialEficacia");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para calcular la suma total de los detalles de la evaluación
     *
     * @author rodrigo.hernandez
     */
    protected void calcularTotalGeneral() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#calcularsTotal");

        this.sumaTotal = 0D;

        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetalles) {
            this.sumaTotal = this.sumaTotal + aed.getValor();
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#calcularTotal");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para validar que los detalles de la evaluación este entre el rango permitido
     *
     * @author rodrigo.hernandez
     */
    protected boolean validarDetallesEvaluacion() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#validarDetallesEvaluacion");

        boolean result = true;
        String mensaje = "";

        /*
         * Se valida que los puntajes de los detalles de CALIDAD de la evaluacion esten dentro del
         * rango establecido
         */
        for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetallesCalidad) {
            /*
             * Se valida que el puntaje del detalle este dentro del rango establecido
             */
            if (aed.getValor() < aed.getVariableEvaluacionAvaluo()
                .getValorMinimo() ||
                 aed.getValor() > aed.getVariableEvaluacionAvaluo()
                .getValorMaximo()) {
                mensaje = "El puntaje del dato " +
                     aed.getVariableEvaluacionAvaluo().getNombre() +
                     " no corresponde al rango aceptado";

                this.addMensajeError(mensaje);

                result = false;
            }
        }

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#validarDetallesEvaluacion");

        return result;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Llama a los métodos para calcular las sumas parciales de los atributos de calidad y eficacia
     *
     * @author christian.rodriguez
     */
    protected void calcularSumasParcialesGeneral() {
        this.calcularSumaParcialCalidadGeneral();
        this.calcularSumaParcialEficaciaGeneral();
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para almacenar la evaluacion generada
     *
     * @author rodrigo.hernandez
     */
    protected void guardarEvaluacion() {
        LOGGER.debug("Inicio EvaluacionDesempenoProfesionalAvaluadorMB#guardarEvaluacion");

        /*
         * Se valida si la evaluacion del avaluo ya fue almacenado previamente en la BD
         */
        if (this.evaluacionAvaluoSeleccionado.getId() == null) {

            /*
             * Se establecen los datos de la evaluacion faltantes
             */
            this.evaluacionAvaluoSeleccionado
                .setAsignacionId(this.asignacionAvaluoSeleccionado.getId());
            this.evaluacionAvaluoSeleccionado
                .setAvaluoId(this.avaluoSeleccionado.getId());
            this.evaluacionAvaluoSeleccionado.setFecha(this.fecha);

            this.evaluacionAvaluoSeleccionado
                .setObservaciones(this.observacionesGenerales);

            this.evaluacionAvaluoSeleccionado
                .setAvaluoEvaluacionDetalles(this.listaEvaluacionDetalles);

            /*
             * Se persiste la evaluacion creada
             */
            this.evaluacionAvaluoSeleccionado = this.getAvaluosService()
                .guardarActualizarAvaluoEvaluacion(
                    this.evaluacionAvaluoSeleccionado, this.usuario);

            for (AvaluoEvaluacionDetalle aed : this.listaEvaluacionDetalles) {
                aed.setAvaluoEvaluacion(this.evaluacionAvaluoSeleccionado);
            }

        }

        /*
         * Se actualiza la evaluacion creada con los detalles
         */
        this.evaluacionAvaluoSeleccionado = this.getAvaluosService()
            .guardarActualizarAvaluoEvaluacion(
                this.evaluacionAvaluoSeleccionado, this.usuario);

        this.banderaEsEvaluacionAlmacenada = true;

        this.addMensajeInfo("La evaluación fue guardada exitosamente");

        LOGGER.debug("Fin EvaluacionDesempenoProfesionalAvaluadorMB#guardarEvaluacion");
    }

    // -------------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

    // end of class
}
