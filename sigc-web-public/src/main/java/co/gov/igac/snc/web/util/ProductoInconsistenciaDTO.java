package co.gov.igac.snc.web.util;

import java.io.Serializable;

/**
 * DTO que contiene información asociada al producto inconsistencia, por lo cual se guarda el nùmero
 * predial, la descripción del trámite catastral, y la descripción de la depuración geográfica
 *
 * @author javier.aponte
 *
 */
public class ProductoInconsistenciaDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6522859901337210216L;

    private String numeroPredial;

    private String tramiteCatastralDescripcion;

    private String depuracionGeoDescripcion;

    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public String getTramiteCatastralDescripcion() {
        return tramiteCatastralDescripcion;
    }

    public void setTramiteCatastralDescripcion(String tramiteCatastralDescripcion) {
        this.tramiteCatastralDescripcion = tramiteCatastralDescripcion;
    }

    public String getDepuracionGeoDescripcion() {
        return depuracionGeoDescripcion;
    }

    public void setDepuracionGeoDescripcion(String depuracionGeoDescripcion) {
        this.depuracionGeoDescripcion = depuracionGeoDescripcion;
    }

}
