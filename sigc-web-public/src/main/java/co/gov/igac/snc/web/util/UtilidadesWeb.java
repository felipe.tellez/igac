package co.gov.igac.snc.web.util;

import co.gov.igac.snc.ldap.SNCUserDetails;
import co.gov.igac.snc.persistence.entity.generales.ConexionUsuario;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EEstadoSesion;
import co.gov.igac.snc.util.NumeroPredialPartes;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;

import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.log4j.Level;
import org.springframework.security.core.Authentication;
//import sun.misc.BASE64Decoder;
import org.apache.commons.codec.binary.Base64;

/**
 * clase con métodos de utilidad para la parte web del proyecto
 *
 * @author ??
 */
@SuppressWarnings("deprecation")
public class UtilidadesWeb {

    private static final Logger LOGGER = LoggerFactory.getLogger(UtilidadesWeb.class);

    public static Object getManagedBean(String beanName) {
        FacesContext fc = null;
        Object result = null;
        try {
            fc = FacesContext.getCurrentInstance();
            Application app = fc.getApplication();
            result = app.evaluateExpressionGet(fc, "#{" + beanName + "}", Object.class);
        } catch (Exception e) {
            LOGGER.error("Error en UtilidadesWeb.getManagedBean': " +
                 e.getMessage(), e);
        }
        return result;
    }

    public static Object getManagedBean(String beanName, String property) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Application app = fc.getApplication();
        ValueBinding vb = app.createValueBinding("#{" + beanName + "." + property + "}");
        return vb.getValue(fc);
    }
//--------------------------------------------------------------------------------------------------

    public static void removerManagedBean(String beanName) {
        FacesContext fc = FacesContext.getCurrentInstance();
        //ExternalContext ex=fc.getExternalContext();
        HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
        HttpSession sesion = req.getSession(false);
        if (sesion != null) {
            sesion.removeAttribute(beanName);
        }
    }

    //----------------------------------------------------------------------------------------
    /**
     * Método que elimina de la sesión todos los managedbeans que no son indispensables para la
     * ejecución del sistema. Se copió de {@link TareasPendientesMB#limpiarSesion}
     *
     * @author christian.rodriguez
     */
    public static void removerManagedBeans() {

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
        HttpSession sesion = req.getSession(false);

        if (sesion != null) {

            @SuppressWarnings("unchecked")
            Enumeration<String> objetos = sesion.getAttributeNames();

            while (objetos.hasMoreElements()) {

                String atributo = objetos.nextElement();

                if (!atributo.equals("menu") && !atributo.equals("tareasPendientes") &&
                     !atributo.equals("SPRING_SECURITY_CONTEXT") &&
                     !atributo.startsWith("org.springframework.web") &&
                     !atributo.startsWith("javax.faces.request") &&
                     !atributo.startsWith("com.sun.faces")) {
                    sesion.removeAttribute(atributo);
                }
            }

        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static ArrayList getSelectItems(ArrayList datosBD) {
        ArrayList list = new ArrayList();
        for (int i = 0; i < datosBD.size(); i++) {
            Object[] objetos = (Object[]) datosBD.get(i);
            if (objetos[0] != null) {
                list.add(new SelectItem(objetos[0].toString(),
                    objetos[1].toString()));
            } else {
                list.add(new SelectItem("", objetos[1].toString()));
            }
        }
        return list;
    }
//--------------------------------------------------------------------------------------------------

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static ArrayList getSelectItems(int numeroColumnaValor,
        int numeroColumnaLabel,
        ArrayList datosBD) {
        ArrayList list = new ArrayList();
        for (int i = 0; i < datosBD.size(); i++) {
            Object[] objetos = (Object[]) datosBD.get(i);
            if (objetos[numeroColumnaValor] != null) {
                list.add(new SelectItem(objetos[numeroColumnaValor].
                    toString(),
                    objetos[numeroColumnaLabel].
                        toString()));
            } else {
                if (objetos[numeroColumnaLabel] != null) {
                    list.add(new SelectItem("",
                        objetos[numeroColumnaLabel].
                            toString()));
                }
            }
        }
        return list;
    }
//--------------------------------------------------------------------------------------------------

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static ArrayList getSelectItemsLabelConcatenado(int numeroColumnaValor,
        int numeroColumna1ConcatenarLabel,
        int numeroColumna2ConcatenerLabel,
        ArrayList datosBD) {
        ArrayList list = new ArrayList();
        for (int i = 0; i < datosBD.size(); i++) {
            Object[] objetos = (Object[]) datosBD.get(i);
            if (objetos[numeroColumnaValor] != null) {
                list.add(new SelectItem(objetos[numeroColumnaValor].
                    toString(),
                    objetos[numeroColumna1ConcatenarLabel].
                        toString() + "-" +
                    objetos[numeroColumna2ConcatenerLabel].
                        toString()));
            } else {
                if (objetos[numeroColumna2ConcatenerLabel] != null) {
                    list.add(new SelectItem("",
                        objetos[numeroColumna2ConcatenerLabel].
                            toString()));
                }
            }
        }
        return list;
    }

    public static String obtenerLabelSelectItem(Object valor, ArrayList<SelectItem> items) {
        String label = null;
        for (SelectItem item : items) {
            if (item.getValue().equals(valor)) {
                label = item.getLabel();
                break;
            }
        }
        return label;
    }

    /**
     * Retorna el string en formato capitalizado: e.g. hola mundo cruel --> Hola Mundo Cruel tipo de
     * dato --> Tipo de Dato
     *
     * @param s
     * @return
     */
    public static String capitalizarString(String s) {
        String[] palabras = s.split("\\s");
        String resultado = "";
        System.out.println("Palabras: ");
        for (String string : palabras) {
            if (string.length() >= 3) {
                resultado += string.substring(0, 1).toUpperCase() + string.substring(1).
                    toLowerCase() + " ";
            } else if (string.length() > 0) {
                resultado += string + " ";
            }
        }
        return resultado;
    }

    /**
     * Retorna un arreglo con el nombre y la extension del nombre de archivo entrado por parametro
     *
     * @param nombreArchivo nombre del archivo
     * @return arreglo[0]-> Nombre sin extension, arreglo[1]-> extension en may�scula
     */
    public static String[] darNombreExtensionArchivo(String nombreArchivo) {
        String nombre = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
        String extension = nombreArchivo.substring(nombreArchivo.lastIndexOf(".") + 1).toLowerCase();
        return new String[]{nombre, extension};
    }

    /**
     * Retorna un arreglo con el nombre y la extension del archivo entrado por parametro
     *
     * @param nombreArchivo nombre del archivo
     * @return arreglo[0]-> Nombre sin extension, arreglo[1]-> extension
     */
    public static String[] darNombreExtensionArchivo(File archivo) {
        return darNombreExtensionArchivo(archivo.getName());
    }

    //--------------------------------------------------------------	
    /**
     * Nombre: DateToString <br>
     * Descripcion:	funcion que realiza la conversion de fechas, de formato Date a String.<br>
     *
     * @author Yenny Nustez Arevalo
     * @version 1.0
     * @date 15/06/2009
     * @param dateFecha - Date con la fecha que se desea convertir.
     * @return fecha - String con la fecha correspondiente, en formato dd/MM/yyyy.
     */

    public static String DateToString(Date dateFecha) {
        String fecha = null;

        try {
            if (dateFecha != null) {
                SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
                fecha = formatoFecha.format(dateFecha);
            }
        } catch (Exception e) {
            LOGGER.error("Error en conversion de fechas Date to String .", e);
        }

        return fecha;
    }

    //--------------------------------------------------------------	
    /**
     * Nombre: StringToDate <br>
     * Descripcion:	funcion que realiza la conversion de fechas, de formato String a Date.<br>
     * Copyright: Procalculo Prosis S.A.
     *
     * @author Yenny Nustez Arevalo
     * @version 1.0
     * @date 30/04/2008
     * @param strFecha - String con la fecha en formato dd/mm/yyyy.
     * @return fecha - Date con la fecha correspondiente.
     */
    public static Date StringToDate(String strFecha) {
        Date fecha = null;
        try {
            if (strFecha != null) {
                SimpleDateFormat formatoTexto = new SimpleDateFormat("dd/MM/yyyy");
                fecha = formatoTexto.parse(strFecha);
            }
        } catch (Exception e) {
            LOGGER.error("Error en formato de fechas String to Date .");
        }

        return fecha;
    }

    /**
     * Método que copia un archivo a una ruta dada
     *
     * @param rutaArchivoOrigen archivo origen
     * @param rutaArchivoDestino archivo destino, puede ser una carpeta, (en cuyo caso se el nombre
     * destino sera identico al origen)
     * @param sobreescribir
     * @throws IOException
     */
    public static void copiarArchivo(String rutaArchivoOrigen, String rutaArchivoDestino,
        boolean sobreescribir) throws IOException {

        File archivoOrigen = new File(rutaArchivoOrigen);
        File archivoDestino = new File(rutaArchivoDestino);

        if (!archivoOrigen.canRead()) {
            throw new IOException("No ha sido posible leer el archivo origen");
        }
        if (!archivoOrigen.isFile()) {
            throw new IOException(
                "El directorio origen no es un archivo o no se encuentra disponible");
        }
        if (!archivoOrigen.exists()) {
            throw new IOException("El directorio origen no se encuentra disponible");
        }

        if (archivoDestino.isDirectory()) {
            archivoDestino = new File(archivoDestino, archivoOrigen.getName());
        }

        if (archivoDestino.exists()) {
            if (!archivoDestino.canWrite()) {
                throw new IOException("No es posible escribir el directorio destino");
            }

            if (!sobreescribir) {
                throw new IOException("El archivo destino existe, sin embargo no se ha sobreescrito");
            }
        } else {
            String carpetaPadre = archivoDestino.getParent();
            if (carpetaPadre == null) {
                throw new IOException("El directorio destino no se encuentra disponible (" +
                    carpetaPadre + ")");
            }
            File dir = new File(carpetaPadre);
            if (!dir.exists()) {
                throw new IOException("El directorio destino no existe o no se encuentra disponible");
            }
            if (dir.isFile()) {
                throw new IOException(
                    "El directorio destino no es una carpeta o no se encuentra disponible");
            }
            /* if (!dir.canWrite()) throw new IOException("El directorio destino no se encuentra
             * disponible para escritura ("+carpetaPadre+")"); */
        }

        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new FileInputStream(archivoOrigen);
            out = new FileOutputStream(archivoDestino);
            byte[] buffer = new byte[2048];
            int bytesRead;

            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // No deber�a ocurrir
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // No deber�a ocurrir
                }
            }
        }
    }

    /**
     * Método que mueve un archivo a una ruta dada
     *
     * @param rutaArchivoOrigen archivo origen
     * @param rutaArchivoDestino archivo destino, puede ser una carpeta, (en cuyo caso se el nombre
     * destino sera identico al origen)
     * @param sobreescribir
     * @throws IOException
     */
    public static void moverArchivo(String rutaArchivoOrigen, String rutaArchivoDestino,
        boolean sobreescribir) throws IOException {
        copiarArchivo(rutaArchivoOrigen, rutaArchivoDestino, sobreescribir);
        // Esta parte podr�a generar conflicto con el CU-125 cargar Archivos para sincronizacion
//    	 File archivoOrigen = new File(rutaArchivoOrigen);
//    	 if(archivoOrigen.delete())
//    	 {
//    		 throw new IOException("No ha sido posible eliminar el archivo origen");
//    	 }
    }

    /**
     * Método que permite crear una carpeta dada su ruta, se debe tener en cuenta que es necesario
     * que la carpeta contenedora (padre) de la carpeta a crear debe estar creada previamente
     *
     * @param rutaCarpeta ruta de la carpeta a crear
     * @throws IOException Se genera si ocurre alguna excepcion de lectura o escritura
     */
    public static void crearCarpeta(String rutaCarpeta) throws IOException {

        File carpetaCrear = new File(rutaCarpeta);

        if (!carpetaCrear.exists()) {
            String carpetaPadre = carpetaCrear.getParent();
            if (carpetaPadre == null) {
                throw new IOException(
                    "La carpeta contenedora de la carpeta que se ha intentado crear no se encuentra disponible");
            }
            File dir = new File(carpetaPadre);
            if (!dir.exists()) {
                throw new IOException(
                    "La carpeta contenedora de la carpeta que se ha intentado crear no existe o no se encuentra disponible");
            }
            if (dir.isFile()) {
                throw new IOException(
                    "La carpeta contenedora de la carpeta que se ha intentado crear no es una carpeta o no se encuentra disponible");
            }
            if (!dir.canWrite()) {
                throw new IOException(
                    "La carpeta contenedora de la carpeta que se ha intentado crear no se encuentra disponible para escritura");
            }
            carpetaCrear.mkdir();
        } else {
            throw new IOException("El nombre ingresado para la carpeta ya existe");
        }
    }

    /**
     * Remueve una carpeta dada su ruta
     *
     * @param rutaCarpeta ruta de la carpeta
     * @throws IOException Se genera si ocurre alguna excepcion de lectura o escritura
     */
    public static void removerCarpeta(String rutaCarpeta) throws IOException {
        File carpetaRemover = new File(rutaCarpeta);
        if (carpetaRemover.exists()) {
            if (!carpetaRemover.isDirectory()) {
                throw new IOException("La carpeta que ha intentado remover no es un directorio");
            }
            if (carpetaRemover.list() != null && carpetaRemover.list().length != 0) {
                for (File archivo : carpetaRemover.listFiles()) {
                    if (archivo.isDirectory()) {
                        throw new IOException(
                            "No es posible eliminar la carpeta de proyecto, existen subcarpetas dentro de la carpeta de proyecto");
                    } else {
                        removerArchivo(archivo.getAbsolutePath());
                    }
                }
            }
            if (!carpetaRemover.delete()) {
                throw new IOException(
                    "La carpeta que ha intentado remover no se encuentra disponible");
            }
        } else {
            throw new IOException("La carpeta que ha intentado remover no existe");
        }
    }

    public static void removerArchivo(String rutaArchivo) throws IOException {
        File archivoRemover = new File(rutaArchivo);
        if (archivoRemover.exists()) {
            if (archivoRemover.isDirectory()) {
                throw new IOException(
                    "El elemento que ha intentado remover no es un archivo, sino un directorio");
            }
            if (!archivoRemover.delete()) {
                throw new IOException(
                    "El archivo que ha intentado remover no se encuentra disponible");
            }
        } else {
            throw new IOException("El archivo: " + rutaArchivo +
                " que ha intentado remover no existe");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Retorna la cadena que identifica a la unidad de construcción siguiente que se adiciona a un
     * predio.
     *
     * @author pedro.garcia
     * @param unidadActual cadena que identifica a la unidad de construcción de la que se quiere
     * calcular la siguiente
     */
    public static String calcularSiguienteUnidadUnidadConstruccion(String unidadActual) {

        String answer;
        char[] letras;
        byte charTmp;
        boolean addLetter = false;

        if (unidadActual.equals("")) {
            answer = "A";
        } else {
            unidadActual = unidadActual.toUpperCase();
            letras = new char[unidadActual.length()];

            unidadActual.getChars(0, unidadActual.length(), letras, 0);
            for (int i = letras.length - 1; i >= 0; i--) {
                if (letras[i] == 'Z') {
                    letras[i] = 'A';
                    if (i == 0) {
                        addLetter = true;
                    }
                } else {
                    charTmp = (byte) letras[i];
                    charTmp++;
                    letras[i] = (char) charTmp;
                    break;
                }
            }

            answer = new String(letras);
            if (addLetter) {
                answer += "A";
            }
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna el número de días que hay entre las dos fechas dadas. OJO: no tiene en cuenta si los
     * días son hábiles o no. Si se requiera, se debería usar la función de Julio para que el
     * cálculo sea de días hábiles.
     *
     * @author pedro.garcia
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    public static int calcularNumeroDiasEntreFechas(Date fechaInicio, Date fechaFin) {

        GregorianCalendar calendarStart, calendarEnd;
        LocalDate localDateStart, localDateEnd;
        Days dias;
        int diasInt;

        calendarStart = new GregorianCalendar();
        calendarEnd = new GregorianCalendar();
        calendarStart.setTime(fechaInicio);
        calendarEnd.setTime(fechaFin);

        //OJO: Java indica que enero es el mes 0.
        //    por eso hay que sumarle 1 al mes
        localDateStart = new LocalDate(calendarStart.get(Calendar.YEAR),
            calendarStart.get(Calendar.MONTH) + 1, calendarStart.get(Calendar.DAY_OF_MONTH));
        localDateEnd = new LocalDate(calendarEnd.get(Calendar.YEAR),
            calendarEnd.get(Calendar.MONTH) + 1, calendarEnd.get(Calendar.DAY_OF_MONTH));

        dias = Days.daysBetween(localDateStart, localDateEnd);
        diasInt = dias.getDays();

        return diasInt;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Busca el archivo con la url dada en la carpeta web temporal y lo devuelve como un
     * DefaultStreamedContent que es el que usa primafaces para mostrar un documento en una página
     *
     * @author pedro.garcia
     * @version 2.0
     * @param urlArchivo url del archivo que se obtuvo al obtener el documento del gestor documental
     * @param nombreDoc nombre que se pasa como parámetro en la creación del DefaultStreamedContent.
     * Debe ser X.pdf
     * @param urlDirectorioWeb Url web del directorio para almacenamiento de archivos temporales
     * @param pathDirectorioTemp path del directorio de archivos temporal
     *
     * @return un arreglo de objetos de dos posiciones tal que: [0] = url del archivo que se buscó.
     * Es la misma que se le pasa como parámetro pero arreglándola [1] = contenido del archivo como
     * DefaultStreamedContent para ser impreso desde el componente primefaces
     *
     * @throws FileNotFoundException si no encuentra el archivo con la url dada, en la carpeta
     * temporal
     */
    public static Object[] obtenerArchivoDeGestorDocVistaWeb(
        String urlArchivo, String urlDirectorioWeb, String pathDirectorioTemp, String nombreDoc)
        throws FileNotFoundException {

        Object[] answer = new Object[2];

        DefaultStreamedContent contenidoArchivo = null;
        String urlArchivoArreglada = "";
        String urlArchivoTemp = "";

        //D: truquini que hay que hacer para obtener la url real
        if (!urlArchivo.trim().isEmpty()) {
            urlArchivoArreglada =
                urlDirectorioWeb.substring(0, urlDirectorioWeb.length() - 1) + urlArchivo;
            answer[0] = urlArchivoArreglada;

            urlArchivoTemp = pathDirectorioTemp.substring(0,
                pathDirectorioTemp.length() - 1) + urlArchivo;

        }

        //D: se obtiene el contenido del archivo
        File archivo = new File(urlArchivoTemp);
        InputStream stream;
        try {
            stream = new FileInputStream(archivo);
            contenidoArchivo = new DefaultStreamedContent(stream, "application/pdf", nombreDoc);
            answer[1] = contenidoArchivo;

        } catch (FileNotFoundException e) {
            throw e;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * valida la regla definida para la duración de una comisión
     *
     * si |(duración total) - uración | > 1, entonces paila!!
     *
     * @author pedro.garcia
     * @param fechaInicio
     * @param fechaFin
     */
    public static boolean validarReglaDuracionComision(Double duracion, Double duracionTotal) {

        boolean isValid;

        if (Math.abs(duracionTotal - duracion) > 1) {
            isValid = false;
        } else {
            isValid = true;
        }
        return isValid;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace una operación simple para obtener la url completa que se debe consultar para ver un
     * documento que ha sido traído del gestor documental
     *
     * @author pedro.garcia
     *
     * @param urlDirectorioWebTemporal url del directorio web temporal de documentos (de donde se
     * leen los documentos para ser mostrados)
     * @param urlObtenida url retornada por el servicio documental
     */
    public static String obtenerUrlDocumentoObtenidoDeGestorDoc(
        String urlDirectorioWebTemporal, String urlObtenida) {

        String answer;
        answer = urlDirectorioWebTemporal.substring(0, urlDirectorioWebTemporal.length() - 1) +
            urlObtenida;

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Carga <b>un</b> archivo desde un evento {@link FileUploadEvent} de primefaces.
     *
     * @author christian.rodriguez
     * @param eventoCarga evento capturado desde el componente {@link FileUpload}
     * @param contextoWeb contexto web de la aplicación
     * @return un arreglo de objetos de tres posiciones tal que:
     * <ul>
     * <li>[0] = objeto de tipo {@link File} con el archivo cargado o null si no pudo ser
     * cargado</li>
     * <li>[1] = objeto de tipo {@link String} con los errores amigables ocurridos al cargar el
     * archivo</li>
     * <li>[2] = objeto de tipo {@link Exception} con la excepcion generada en caso de error</li>
     * </ul>
     * @deprecated Se debe usar {@link UtilidadesWeb#cargarArchivoATemp(org.primefaces.event.FileUploadEvent, boolean)
     */
    /*
     * modified pedro.garcia 07-05-2013 se puso como deprecated porque ya no se debe usar el
     * contexto web como parámetro
     */
    @Deprecated
    public static Object[] cargarArchivo(FileUploadEvent eventoCarga, IContextListener contextoWeb) {

        Object[] resultado = new Object[3];
        resultado = UtilidadesWeb.cargarArchivo(eventoCarga, false);
        return resultado;

    }
//------

    /**
     * Carga <b>un</b> archivo desde un evento {@link FileUploadEvent} de primefaces a la carpeta
     * temporal definida.
     *
     * @author pedro.garcia
     * @param eventoCarga evento capturado desde el componente {@link FileUpload}
     * @param addRandomToName indica si se debe adicionar un pedazo de nombre random al nombre del
     * archivo
     * @return un arreglo de objetos de tres posiciones tal que:
     * <ul>
     * <li>[0] = objeto de tipo {@link File} con el archivo cargado o null si no pudo ser
     * cargado</li>
     * <li>[1] = objeto de tipo {@link String} con los errores amigables ocurridos al cargar el
     * archivo</li>
     * <li>[2] = objeto de tipo {@link Exception} con la excepcion generada en caso de error</li>
     * </ul>
     */
    public static Object[] cargarArchivoATemp(FileUploadEvent eventoCarga, boolean addRandomToName) {
        Object[] resultado;
        resultado = UtilidadesWeb.cargarArchivo(eventoCarga, addRandomToName);
        return resultado;

    }
//------------

    /**
     * Carga <b>un</b> archivo desde un evento {@link FileUploadEvent} de primefaces.
     *
     * @author christian.rodriguez
     * @param eventoCarga evento capturado desde el componente {@link FileUpload}
     * @param addRandomName indica si se debe adicionar un pedazo de nombre random al nombre del
     * archivo
     *
     * @return un arreglo de objetos de tres posiciones tal que:
     * <ul>
     * <li>[0] = objeto de tipo {@link File} con el archivo cargado o null si no pudo ser
     * cargado</li>
     * <li>[1] = objeto de tipo {@link String} con los errores amigables ocurridos al cargar el
     * archivo</li>
     * <li>[2] = objeto de tipo {@link Exception} con la excepcion generada en caso de error</li>
     * </ul>
     *
     */
    /*
     * @modified pedro.garcia 07-05-2013 :: uso de un parámetro extra para decidir si se añade un
     * pedazo de nombre random al nombre del archivo. Se usa a veces en la aplicación.
     *
     * 08-05-2013 :: uso de parámetro extra que dice si el archivo es una imagen que se sube a la
     * carpeta web de imágenes temporales
     */
 /*
     * @modified leidy.gonzalez #10683 12-12-2014 :: cambio de caracteres especiales por espacion
     */
    private static Object[] cargarArchivo(FileUploadEvent eventoCarga, boolean addRandomName) {

        Object[] resultado = new Object[3];

        File archivoResultado = null;
        String errorLegible = null;
        Exception errorDebug = null;
        String rutaArchivo;
        FileOutputStream fileOutputStream;

        rutaArchivo = UtilidadesWeb.obtenerRutaTemporalArchivos();

        //String nombreArchivoSinCaracteresEspeciales = eventoCarga.getFile().getFileName().replaceAll("[^a-zA-Z0-9./]", "_");
        try {

            if (addRandomName) {
                archivoResultado = new File(rutaArchivo, "Temporal" + (int) (Math.random() *
                    10000000) +
                     Constantes.SEPARADOR_NOMBRE_TEMPORAL_ARCHIVO_TEMPORAL + eventoCarga.getFile().
                        getFileName());
            } else {
                archivoResultado = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(),
                    eventoCarga.getFile().getFileName());
            }
        } catch (NullPointerException ex) {
            errorLegible = "No se encontró el archivo especificado.";
            errorDebug = ex;
        }

        if (errorLegible == null) {
            try {
                fileOutputStream = new FileOutputStream(archivoResultado);

                byte[] buffer = new byte[Math.round(eventoCarga.getFile().getSize())];

                int bulk;
                InputStream inputStream = eventoCarga.getFile().getInputstream();
                while (true) {
                    bulk = inputStream.read(buffer);
                    if (bulk < 0) {
                        break;
                    }
                    fileOutputStream.write(buffer, 0, bulk);
                }
                fileOutputStream.flush();
                fileOutputStream.close();
                inputStream.close();

            } catch (IOException ex) {
                errorLegible = "Los archivos seleccionados no pudieron ser cargados o creados";
                errorDebug = ex;
            }
        }

        resultado[0] = archivoResultado;
        resultado[1] = errorLegible;
        resultado[2] = errorDebug;

        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * método que se debe usar para obtener la ruta donde se cargan temporalmente los archivos que
     * se suben al sistema. Actualmente se está usando la carpeta temporal del sistema para el
     * usuario que corre la aplicación, tal como la retorna el método
     * FileUtils.getTempDirectory().getAbsolutePath() del apache commons
     *
     * @author pedro.garcia
     * @return
     */
    public static String obtenerRutaTemporalArchivos() {
        String answer = "";
        answer = FileUtils.getTempDirectory().getAbsolutePath();
        return answer;
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Partiendo del supuesto que un la cadena que recibe es un nombre de archivo o una ruta de
     * archivo, separa la cadena y devuelve el último pedazo que debe corresponder al nombre
     * original del archivo
     *
     * PRE: la cadena solo tiene una ocurrencia de la cadena que se usa para adicionar un prefijo
     * temporal al nombre de un archivo
     *
     * @author pedro.garcia
     * @param cadenaCualquiera
     * @return
     */
    public static String obtenerNombreSinTemporalArchivo(String cadenaCualquiera) {
        String answer = "";
        String[] splited;

        splited = cadenaCualquiera.split(Constantes.SEPARADOR_NOMBRE_TEMPORAL_ARCHIVO_TEMPORAL);
        if (splited != null && splited.length > 0) {
            answer = splited[splited.length - 1];
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Calcula la desviación estándar sobre un conjunto de números Double
     *
     * @author pedro.garcia
     * @param numeros
     * @return
     */
    public static Double calcularDesviacionEstandar(Double[] numeros) {

        Double desviacionEstandar = 0.0D;
        Double media, varianza, suma;

        if (numeros == null || numeros.length == 0) {
            return desviacionEstandar;
        }

        suma = 0.0D;
        for (Double numero : numeros) {
            suma += numero;
        }

        media = suma / numeros.length;

        suma = 0.0D;
        for (Double numero : numeros) {
            suma += Math.pow((numero - media), 2);
        }

        varianza = suma / numeros.length;
        desviacionEstandar = Math.sqrt(varianza);

        return desviacionEstandar;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Mètodo para dar formato moneda a nùmero.
     *
     * @author felipe.cadena
     * @param numero
     * @return
     */
    public static String formatNumber(double numero) {
        DecimalFormat numFormat = new DecimalFormat(Constantes.FORMATO_NUMERO_MONEDA);
        return numFormat.format(numero);
    }

    /**
     * Método para obtener el año como un int de un objeto java.util.Date
     *
     * @author javier.aponte
     */
    public static int getAnioDeUnDate(Date fecha) {

        Calendar c = Calendar.getInstance();
        c.setTime(fecha);

        return c.get(Calendar.YEAR);

    }

    /**
     * Método para obtener una lista de trámites predio englobe DTO a partir de una lista de trámite
     * predio englobe, se usa en todas las partes donde se adiciona un trámite nuevo
     *
     * Se copio y pego este método desarrollado por juanfelipe.garcia
     *
     * @param prediosTramiteList
     * @return
     * @author javier.aponte
     */
    public static List<TramitePredioEnglobeDTO> obtenerListaTramitePredioEnglobeDTO(
        List<TramitePredioEnglobe> prediosTramiteList) {
        List<TramitePredioEnglobeDTO> lista = new ArrayList<TramitePredioEnglobeDTO>();
        for (TramitePredioEnglobe tpe : prediosTramiteList) {
            TramitePredioEnglobeDTO dto = new TramitePredioEnglobeDTO();
            dto.setEnglobePrincipal(tpe.getEnglobePrincipal());
            dto.setFechaLog(tpe.getFechaLog());
            dto.setId(tpe.getId());
            dto.setPredio(tpe.getPredio());
            dto.setTramite(tpe.getTramite());
            dto.setUsuarioLog(tpe.getUsuarioLog());
            lista.add(dto);
        }
        return lista;
    }

    /**
     * Método para obtener una lista de trámites predio englobe a partir de una lista de trámite
     * predio englobe DTO, se usa en todas las partes donde se adiciona un trámite nuevo
     *
     * Se copio y pego este método desarrollado por juanfelipe.garcia
     *
     * @param prediosTramiteList
     * @return
     * @author javier.aponte
     */
    public static List<TramitePredioEnglobe> obtenerListaTramitePredioEnglobe(
        List<TramitePredioEnglobeDTO> prediosTramiteDTOList) {
        List<TramitePredioEnglobe> lista = new ArrayList<TramitePredioEnglobe>();
        for (TramitePredioEnglobeDTO dto : prediosTramiteDTOList) {
            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            tpe.setEnglobePrincipal(dto.getEnglobePrincipal());
            tpe.setFechaLog(dto.getFechaLog());
            tpe.setId(dto.getId());
            tpe.setPredio(dto.getPredio());
            tpe.setTramite(dto.getTramite());
            tpe.setUsuarioLog(dto.getUsuarioLog());
            lista.add(tpe);
        }
        return lista;
    }

    /**
     * Método validar si una cadena tienen solo caracteres numericos.
     *
     * @author felipe.cadena
     *
     */
    public static boolean validarSoloDigitos(String cadena) {

        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }

    }

    /**
     * Método obtener el entero de una seccion del número predial, retirando los ceros iniciales.
     *
     * @author felipe.cadena
     *
     * @param cadena
     */
    public static int obtenerEntero(String cadena) {

        String res = "";
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) != '0') {
                res = cadena.substring(i);
                break;
            }
        }

        return Integer.valueOf(res);
    }

    /**
     * Método para retornar el nombre de una constante recibiendo como parametro su valor y su clase
     * base. Para enviar como parametro en una URL, no se admiten espacios ni caracteres especiales
     * asi que se envia el nombre de la constante.
     *
     * @author felipe.cadena
     *
     * @param clase
     * @param valor
     *
     */
    public static String obtenerNombreConstante(Class clase, String valor) {

        String resultado = "";

        Field[] fileds = clase.getFields();

        for (Field field : fileds) {
            try {
                String value = (String) field.get(null);
                if (value.equals(valor)) {
                    resultado = field.getName();
                    break;
                }
            } catch (Exception ex) {
                LOGGER.debug("Campo no es String");
            }
        }

        return resultado;

    }

    /**
     * Retorna la información del cliente de la conexion actual.
     *
     * @author felipe.cadena
     *
     * @param authentication
     * @param contexRequest
     * @param logAcceso
     *
     * @return
     */
    public static ConexionUsuario obtenerInfoConexion(HttpServletRequest contexRequest,
        Authentication authentication, LogAcceso logAcceso) {

        ConexionUsuario resultado = new ConexionUsuario();

        String ip;
        try {

            ip = InetAddress.getLocalHost().getHostAddress();

        } catch (UnknownHostException e) {

            ip = "--";

        }
        String maquinaCliente = logAcceso.getMaquinaCliente() + " " + logAcceso.
            getExploradorCliente();
        if (maquinaCliente.length() >= 199) {
            maquinaCliente = maquinaCliente.substring(0, 190);
        }

        resultado.setFechaInicioSesion(new Date());
        resultado.setMaquinaCliente(maquinaCliente);
        resultado.setMaquinaServidor(ip);
        resultado.setUsuario(logAcceso.getLogin());
        resultado.setSesion(contexRequest.getSession().getId());

        return resultado;
    }

    /**
     * Retorna la información del cliente de la sesion actual.
     *
     * @param authentication
     * @param contexRequest
     * @return
     */
    public static LogAcceso obtenerInfoSesion(HttpServletRequest contexRequest,
        Authentication authentication) {

        SNCUserDetails user;
        LogAcceso resultado;

        String remoteAddr = "";
        String dominio = "";
        String usuario = "";
        InetAddress inetAddressRe = null;
        String browser = "";
        try {

            browser = contexRequest.getHeader("User-Agent");
            int idx = browser.indexOf("(");
            browser = browser.substring(idx);
            if (browser.length() > 100) {
                browser = browser.substring(0, 99);
            }

            remoteAddr = getClientIpAddr(contexRequest);

            user = (SNCUserDetails) authentication.getPrincipal();
            usuario = user.getUsername();

            remoteAddr = remoteAddr.replace(".", ",");
            String[] addrs = remoteAddr.split(",");
            remoteAddr = remoteAddr.replace(",", ".");
            byte[] addrByte = new byte[4];

            for (int j = 0; j < 4; j++) {
                Integer i = Integer.valueOf(addrs[j]);
                addrByte[j] = i.byteValue();
            }
            inetAddressRe = InetAddress.getByAddress(addrByte);
            dominio = inetAddressRe.getCanonicalHostName();

        } catch (Exception ex) {
            LOGGER.debug("Arror al obtener ip ");
        }

        resultado = new LogAcceso(
            usuario,
            "",
            remoteAddr + " - " + dominio,
            browser);

        return resultado;
    }

    /**
     * Obtiene la ip de la maquina cliente
     *
     * @author felipe.cadena
     * @param request
     * @return
     */
    private static String getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * Retorna la información del cliente de la sesion actual.
     *
     * @param authentication
     * @param contexRequest
     * @return
     */
    public static LogAcceso obtenerInfoSesionDesktop(HttpServletRequest contexRequest) {

        //String user;
        LogAcceso resultado;
        contexRequest.getHeaderNames();

        String remoteAddr = "";
        String dominio = "";
        String usuario = "";
        InetAddress inetAddressRe = null;
        String browser = "";
        try {

            browser = contexRequest.getHeader("version");

            if (browser != null && browser.length() > 100) {
                browser = browser.substring(0, 99);
            }

            remoteAddr = contexRequest.getRemoteAddr();
            usuario = contexRequest.getHeader("Authorization");

            // Modificado y comentarios por FGWL: se usaba una clase del paquete sun, que no debe usarse por posibles fallas de compatibilidad en versiones posteriores o en otra plataforma.
            // Ver: http://www.oracle.com/technetwork/java/faq-sun-packages-142232.html
            // Ver: http://www.mkyong.com/java/access-restriction-the-type-base64encoder-is-not-accessible-due-to-restriction/
            //BASE64Decoder decoder = new BASE64Decoder();
            //Base64 decoder = new Base64();
            byte[] decodedBytes = Base64.decodeBase64(usuario.substring(6).getBytes());
            //byte[] decodedBytes = decoder.decodeBuffer(usuario.substring(6));

            usuario = new String(decodedBytes);
            int idx = usuario.indexOf(":");
            usuario = usuario.substring(0, idx);

            remoteAddr = remoteAddr.replace(".", ",");
            String[] addrs = remoteAddr.split(",");
            remoteAddr = remoteAddr.replace(",", ".");
            byte[] addrByte = new byte[4];

            for (int j = 0; j < 4; j++) {
                Integer i = Integer.valueOf(addrs[j]);
                addrByte[j] = i.byteValue();
            }
            inetAddressRe = InetAddress.getByAddress(addrByte);
            dominio = inetAddressRe.getCanonicalHostName();

        } catch (Exception ex) {
            LOGGER.debug("Error al obtener ip ");
        }

        resultado = new LogAcceso(
            usuario,
            EEstadoSesion.INICIADA.toString(),
            remoteAddr + " - " + dominio,
            EAtributosSesion.SNC_DESKTOP.getValor() + " V " + browser);

        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Define los mensajes que se van a mostrar al cliente a partir del hashmap que se retorna como
     * resultado de la ejecución del algún método en la capa de negocio.
     *
     * @author pedro.garcia
     *
     * @param mb managed bean al que se se van a adicionar los mensajes de diferente tipo
     * @param mensajes hashmap con los mensajes. Las llaves son los valores de los niveles de log, y
     * los valores son los mensajes para cada nivel.
     */
    public static void definirMensajesResultadoParaCliente(SNCManagedBean mb,
        HashMap<Integer, ArrayList<String>> mensajes) {

        for (Integer key : mensajes.keySet()) {
            ArrayList<String> mensajesTipoX = mensajes.get(key);
            switch (key.intValue()) {
                case (Level.INFO_INT): {
                    for (String mensajeTipoX : mensajesTipoX) {
                        mb.addMensajeInfo(mensajeTipoX);
                    }
                    break;
                }
                case (Level.WARN_INT): {
                    for (String mensajeTipoX : mensajesTipoX) {
                        mb.addMensajeWarn(mensajeTipoX);
                    }
                    break;
                }
                case (Level.ERROR_INT): {
                    for (String mensajeTipoX : mensajesTipoX) {
                        mb.addMensajeError(mensajeTipoX);
                    }
                    break;
                }

                default:
                    break;
            }
        }

    }

    /**
     * Método para formatear un numero a formato decimal y retorna un string
     *
     * redmine #7491
     *
     * @author felipe.cadena
     * @param number
     * @return
     */
    public static String formatDecimalNumber(Double number) {
        String pattern = Constantes.FORMATO_NUMERO_DECIMAL;
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(number);
        return output;
    }

    /**
     * retorna el numero predial construido de forma ordenada, si las partes no llegan diligenciadas
     * en orden retorna null
     *
     * @author felipe.cadena
     *
     * @param npp
     * @return
     */
    public static String obtenerNumeroPredial(NumeroPredialPartes npp) {

        String result = "";
        String resultOrdenado = "";
        List<String> partes = new ArrayList<String>();

        if (npp.getDepartamentoCodigo() != null) {
            partes.add(npp.getDepartamentoCodigo());
            result += npp.getDepartamentoCodigo();
            resultOrdenado += npp.getDepartamentoCodigo();
        }
        if (npp.getMunicipioCodigo() != null) {
            partes.add(npp.getMunicipioCodigo());
            result += npp.getDepartamentoCodigo();
            if (resultOrdenado.length() == 2) {
                resultOrdenado += npp.getMunicipioCodigo();
            }
        }
        if (npp.getTipoAvaluo() != null) {
            partes.add(npp.getTipoAvaluo());
            result += npp.getTipoAvaluo();
            if (resultOrdenado.length() == 5) {
                resultOrdenado += npp.getTipoAvaluo();
            }
        }
        if (npp.getSectorCodigo() != null) {
            partes.add(npp.getSectorCodigo());
            result += npp.getSectorCodigo();
            if (resultOrdenado.length() == 7) {
                resultOrdenado += npp.getSectorCodigo();
            }
        }
        if (npp.getComunaCodigo() != null) {
            partes.add(npp.getComunaCodigo());
            result += npp.getComunaCodigo();
            if (resultOrdenado.length() == 9) {
                resultOrdenado += npp.getComunaCodigo();
            }
        }
        if (npp.getBarrioCodigo() != null) {
            partes.add(npp.getBarrioCodigo());
            result += npp.getBarrioCodigo();
            if (resultOrdenado.length() == 11) {
                resultOrdenado += npp.getBarrioCodigo();
            }
        }
        if (npp.getManzanaVeredaCodigo() != null) {
            partes.add(npp.getManzanaVeredaCodigo());
            result += npp.getManzanaVeredaCodigo();
            if (resultOrdenado.length() == 13) {
                resultOrdenado += npp.getManzanaVeredaCodigo();
            }
        }
        if (npp.getPredio() != null) {
            partes.add(npp.getPredio());
            result += npp.getPredio();
            if (resultOrdenado.length() == 17) {
                resultOrdenado += npp.getPredio();
            }
        }
        if (npp.getCondicionPropiedad() != null) {
            partes.add(npp.getCondicionPropiedad());
            result += npp.getCondicionPropiedad();
            if (resultOrdenado.length() == 21) {
                resultOrdenado += npp.getCondicionPropiedad();
            }
        }
        if (npp.getEdificio() != null) {
            partes.add(npp.getEdificio());
            result += npp.getEdificio();
            if (resultOrdenado.length() == 22) {
                resultOrdenado += npp.getEdificio();
            }
        }
        if (npp.getPiso() != null) {
            partes.add(npp.getPiso());
            result += npp.getPiso();
            if (resultOrdenado.length() == 24) {
                resultOrdenado += npp.getPiso();
            }
        }
        if (npp.getUnidad() != null) {
            partes.add(npp.getUnidad());
            result += npp.getUnidad();
            if (resultOrdenado.length() == 26) {
                resultOrdenado += npp.getUnidad();
            }
        }

        if (!result.isEmpty()) {
            if (resultOrdenado.isEmpty() || resultOrdenado.length() < result.length()) {
                return null;
            } else {
                return resultOrdenado;
            }
        }

        return result;

    }

    /**
     * Retorna el error del procedimiento orientado el usuario en la posicion 0, y el mensaje
     * tecnico en la posicion 1 retorna cadenas vacia si no existen errores
     *
     * @param mensajes
     * @return
     */
    public static String[] extraerMensajeSP(Object mensajes[]) {

        String[] result = new String[2];
        result[0] = "";
        result[1] = "";

        if (mensajes != null && mensajes.length > 0) {
            for (Object o : mensajes) {
                List l = (List) o;
                if (l.size() > 0) {
                    for (Object obj : l) {
                        Object[] obje = (Object[]) obj;
                        if (!obje[0].equals("0")) {
                            result[0] = obje[3].toString();
                            result[1] = obje[2].toString();
                        }
                    }
                }
            }

        }
        return result;
    }
    
    /**
     * Completa el numero predial dado con el digito dado hasta el tamaña de 30 posiciones
     *
     * @param npp
     * @param digito
     * @return
     */
    public static String completarNumeroPredialConDigito(NumeroPredialPartes npp, int digito) {

        String numeropredial = UtilidadesWeb.obtenerNumeroPredial(npp);

        int finalLen = 30 - numeropredial.length();

        for (int i = 0; i < finalLen; i++) {
            numeropredial += String.valueOf(digito);
        }

        return numeropredial;
    }

    /**
     * Retorna el numero predial con formato
     *
     * @author felipe.cadena
     * @param numero
     * @return
     */
    public static String formatNumeroPredial(String numero) {
        String formatedNumPredial = "";

        if (numero != null && !numero.isEmpty()) {
            // Departamento
            formatedNumPredial = numero.substring(0, 2).concat("-");
            // Municipio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(2, 5)).concat("-");
            // Tipo Avalúo
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(5, 7)).concat("-");
            // Sector
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(7, 9)).concat("-");
            // Comuna
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(9, 11)).concat("-");
            // Barrio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(11, 13)).concat("-");
            // Manzana
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(13, 17)).concat("-");
            // Predio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(17, 21)).concat("-");
            // Condición Propiedad
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(21, 22)).concat("-");
            // Edificio
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(22, 24)).concat("-");
            // Piso
            formatedNumPredial = formatedNumPredial.concat(
                numero.substring(24, 26)).concat("-");
            // Unidad
            formatedNumPredial = formatedNumPredial.concat(numero
                .substring(26, 30));
        }
        return formatedNumPredial;

    }
    
    /**
     * Determina si la lista de items contiene uno en particular basado en el codigo(value) del item
     * dado si se trat de un objeto se debe tener implementado el metodo equals()
     *
     * @author felipe.cadena
     *
     * @param list
     * @param item
     * @return
     */
    public static boolean containsSelectItemByCode(List<SelectItem> list, SelectItem item) {

        for (SelectItem itm : list) {
            if (itm.getValue().equals(item.getValue())) {
                return true;
            }
        }

        return false;

    }

    public static File unzipArchivoUnico(File archivoZip, File archivo){
        try {
            OutputStream out = new FileOutputStream(archivo);
            FileInputStream fileInputStream = new FileInputStream(archivoZip);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            ZipInputStream zin = new ZipInputStream(bufferedInputStream);
            ZipEntry ze = null;
            while ((ze = zin.getNextEntry()) != null) {
                if (ze.getName().equals(archivo.getName())) {
                    byte[] buffer = new byte[9000];
                    int len;
                    while ((len = zin.read(buffer)) != -1) {
                        out.write(buffer, 0, len);
                    }
                    out.close();
                    break;
                }
            }
            zin.close();
            return archivo;
        }catch(Exception e){
            LOGGER.error("Error al descomprimir archivo");
            return null;
        }
    }

//end of class	    
}
