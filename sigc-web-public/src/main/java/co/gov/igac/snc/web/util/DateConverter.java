package co.gov.igac.snc.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.util.Constantes;

/**
 * Converter para las fechas en los comboBox.
 *
 * @author david.cifuentes
 */
public class DateConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DateConverter.class);

    @Override
    public Object getAsObject(FacesContext paramFacesContext,
        UIComponent paramUIComponent, String value) {

        SimpleDateFormat formatoString = new SimpleDateFormat(Constantes.FORMATO_FECHA);
        Date fecha = null;
        try {
            fecha = formatoString.parse(value);
        } catch (ParseException ex) {
            ex.printStackTrace();
            LOGGER.error("Error al convetir la fecha" + value);
        }
        return fecha;
    }

    @Override
    public String getAsString(FacesContext paramFacesContext,
        UIComponent paramUIComponent, Object paramObject) {
        if (paramObject == null) {
            return "";
        }
        SimpleDateFormat sdf =
            new SimpleDateFormat(Constantes.FORMATO_FECHA);
        String date = "";
        try {
            date = sdf.format(sdf.parse(paramObject.toString()));
        } catch (ParseException ex) {
            LOGGER.error("Error al convetir la fecha a string");
        }
        return date;
    }
}
