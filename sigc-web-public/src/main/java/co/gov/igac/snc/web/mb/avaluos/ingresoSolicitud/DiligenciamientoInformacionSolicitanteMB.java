package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.util.AdministracionInformacionSolicitanteMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Managed Bean para manejo de pantallas relacionadas a Informacion de Solicitante
 *
 * @cu CU-SA-AC-121
 *
 * @author rodrigo.hernandez
 *
 */
@Component("diligenciamientoInfoSolicitante")
@Scope("session")
public class DiligenciamientoInformacionSolicitanteMB extends SNCManagedBean {

    /**
     * serial auto-generado
     */
    private static final long serialVersionUID = 5417225239165088096L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DiligenciamientoInformacionSolicitanteMB.class);

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private AdministracionInformacionSolicitanteMB administracionInformacionSolicitanteMB;

    /**
     * Solicitante seleccionado en la busqueda de solicitudes
     */
    private Solicitante solicitanteSeleccionado;

    /**
     * Solicitud que se encuentra seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Variable que indica si el almacenamiento de la información de la solicitud se ha hecho de
     * forma definitiva o temporal
     */
    private boolean banderaModoLecturaHabilitado;

    /**
     * Fecha de radicación
     */
    private Date fechaRadicacion;

    // ------------------------------------------------------------------------------------------------------------------
    public Date getFechaRadicacion() {
        return fechaRadicacion;
    }

    public void setFechaRadicacion(Date fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public boolean isBanderaModoLecturaHabilitado() {
        return banderaModoLecturaHabilitado;
    }

    public void setBanderaModoLecturaHabilitado(
        boolean banderaModoLecturaHabilitado) {
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Iniciando método init");

        this.iniciarDatos();

        LOGGER.debug("Finalizando método init");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos del bean
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Iniciando método iniciarDatos");

        this.solicitanteSeleccionado = new Solicitante();

        LOGGER.debug("Finalizando método iniciarDatos");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo que se llama desde DiligenciarSolicitudMB para enviar datos de Solicitante y Tipo de
     * Almacenamiento de la informacion de la solicitud
     *
     * @author rodrigo.hernandez
     *
     * @param banderaModoLecturaHabilitado - bandera que indica el modo de lectura de los datos</br>
     * <b>true: </b>Modo lectura </br> <b>false: </b>Modo edición
     * </br> </br>
     * @param solicitud - Solicitud a la que se va a asociar solicitante </br>
     * @param listaSolicitanteSolicitud - Lista de solicitantes asociados a la solicitud
     *
     */
    public void cargarVariablesExternas(boolean banderaModoLecturaHabilitado,
        Solicitud solicitud,
        List<SolicitanteSolicitud> listaSolicitanteSolicitud) {

//		List<Solicitante> listaSolicitantes = new ArrayList<Solicitante>();
        this.solicitudSeleccionada = solicitud;

        this.setBanderaModoLecturaHabilitado(banderaModoLecturaHabilitado);

        this.solicitudSeleccionada
            .setSolicitanteSolicituds(listaSolicitanteSolicitud);

//		listaSolicitantes = this.crearListaSolicitantes(solicitud.getSolicitanteSolicituds());
        this.administracionInformacionSolicitanteMB
            .cargarDesdeCU121(this.solicitudSeleccionada.getSolicitanteSolicituds(),
                banderaModoLecturaHabilitado,
                this.solicitudSeleccionada);
    }

    /**
     * Método para validar si el solicitante es Persona Natural
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true: </b>Es persona natural </br> <b>false: </b>Es persona juridica </br>
     */
    private boolean esSolicitanteSeleccionadoPersonaNatural(
        Solicitante solicitante) {
        boolean result = true;

        if (solicitante.getTipoPersona().equals(
            EPersonaTipoPersona.JURIDICA.getCodigo())) {
            result = false;
        }

        return result;
    }

    /**
     * Método que evalua condiciones necesarias para guardar los datos del solicitante correctamente
     * en la solicitud
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    public boolean validarInfo() {
        boolean result = true;
        String mensaje = "";

        // Se valida que la lista solicitantes asociados a la solicitud no este
        // vacia
        if (this.solicitudSeleccionada.getSolicitanteSolicituds().size() > 0) {

            if (this.solicitudSeleccionada.getSolicitanteSolicituds().size() == 1) {

                // Se valida si el tipo de persona del solicitante es JURIDICA
                if (!this
                    .esSolicitanteSeleccionadoPersonaNatural(this.solicitudSeleccionada
                        .getSolicitanteSolicituds().get(0)
                        .getSolicitante())) {
                    // Para cuando hay un solicitante en la tabla se indica que
                    // si es
                    // propietario, se debe adicionar la info del representante
                    // legal
                    if (this.solicitudSeleccionada
                        .getSolicitanteSolicituds()
                        .get(0)
                        .getRelacion()
                        .equals(ESolicitanteSolicitudRelac.PROPIETARIO
                            .toString())) {
                        mensaje = "No ha ingresado los datos del representante legal";
                        this.addMensajeError(mensaje);
                        result = false;

                        // Para cuando hay un solicitante en la tabla se indica
                        // que
                        // si es representante legal, se debe adicionar la info
                        // del propietario
                    } else if (this.solicitudSeleccionada
                        .getSolicitanteSolicituds()
                        .get(0)
                        .getRelacion()
                        .equals(ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                            .getValor().toUpperCase())) {
                        mensaje = "No ha ingresado los datos del solicitante";
                        this.addMensajeError(mensaje);
                        result = false;
                    }
                }
            }
        } else {
            mensaje = "No se ha ingresado el solicitante";
            this.addMensajeError(mensaje);
            result = false;
        }

        return result;

    }

    /**
     * Método para crear una lista de solicitantes a partir de la lista de SolicitanteSolicitud
     *
     * @param listaSolicitanteSolicitud - Lista de solicitanteSolicitud asociado a la solicitud
     *
     * @return Lista de solicitantes
     *
     * @author rodrigo.hernandez
     */
    private List<Solicitante> crearListaSolicitantes(
        List<SolicitanteSolicitud> listaSolicitanteSolicitud) {
        List<Solicitante> listaSolicitantes = new ArrayList<Solicitante>();

        for (SolicitanteSolicitud solSol : listaSolicitanteSolicitud) {
            listaSolicitantes.add(solSol.getSolicitante());
        }

        return listaSolicitantes;
    }

}
