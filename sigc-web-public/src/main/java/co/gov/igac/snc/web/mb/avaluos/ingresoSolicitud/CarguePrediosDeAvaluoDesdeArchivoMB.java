package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.util.EArchivoCarguePrediosAvaluos;
import co.gov.igac.snc.persistence.util.EAvaluoPredioTipoAvaluo;
import co.gov.igac.snc.persistence.util.ECondicionJuridica;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.ETipoAvaluo;
import co.gov.igac.snc.util.EUnidadMedidaAreaTerreno;
import co.gov.igac.snc.util.EUnidadMedidaConstruccion;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ComboBoxConverter;
import co.gov.igac.snc.web.util.ConversorUnidadesArea;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("carguePrediosAvaluos")
@Scope("session")
public class CarguePrediosDeAvaluoDesdeArchivoMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 3192560069126536848L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(CarguePrediosDeAvaluoDesdeArchivoMB.class);

    @Autowired
    private IContextListener contexto;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Almacena el archivo cargado
     */
    private File archivoResultado;

    /**
     * Lista que contiene lso predios que se están cargando
     */
    private List<SolicitudPredio> predios;

    /**
     * Contiene una lista con los errores de validación encontrados al momento de realizar la carga.
     * La clase ErrorValidacion es una inner class de este SNCManagedBean
     */
    private List<ErrorValidacion> erroresValidacion;

    /**
     * Contiene la solicitud asociada a los predios
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Contiene la información del solicitante asociado a la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Contiene lso predios asociados a la solicitud
     */
    private List<SolicitudPredio> prediosSolicitud;

    /**
     * Bandera que indica si se debe mostrar o no todas las columnas de la tabla de registros
     * validos
     */
    private boolean banderaTablaExpandidaRegistrosValidos;

    /**
     * Bandera que indica si se debe mostrar o no todas las columnas de la tabla de registros
     * invalidos
     */
    private boolean banderaTablaExpandidaRegistrosInvalidos;

    /**
     * Usada para marcar un registro cuando se encuentran errores de validación
     */
    private boolean banderaDatosValidos;

    /**
     * Usada para marcar un registro con matricula antigua, se usa está bandera para validar otros
     * campos
     */
    private boolean banderaMatriculaAntigua;

    /**
     * Usada para marcar un registro con condicion juridica propiedad horizontal, se usa está
     * bandera para validar otros campos
     */
    private boolean banderaPropiedadHorizontal;

    /**
     * Bandera que indica si el archivo se cargo correctamente
     */
    private boolean banderaArchivoCargado;

    /**
     * Bandera que indica si el archivo se valido correctamente
     */
    private boolean banderaArchivoValidado;

    /**
     * Bandera que indica si el registro está asociado a un predio de tipo Urbano (true) o
     * rural(false)
     */
    private boolean banderaTipoAvaluoUrbano;

    // ---------------------Métodos GET y SET----------------------- //
    public List<SolicitudPredio> getPredios() {
        return predios;
    }

    public List<ErrorValidacion> getErroresValidacion() {
        return this.erroresValidacion;
    }

    public Solicitud getSolicitudSeleccionada() {
        return this.solicitudSeleccionada;
    }

    public SolicitanteSolicitud getSolicitante() {
        return this.solicitante;
    }

    public boolean isBanderaTablaExpandidaRegistrosValidos() {
        return this.banderaTablaExpandidaRegistrosValidos;
    }

    public void setBanderaTablaExpandidaRegistrosValidos(
        boolean banderaTablaExpandidaRegistrosValidos) {
        this.banderaTablaExpandidaRegistrosValidos = banderaTablaExpandidaRegistrosValidos;
    }

    public boolean isBanderaTablaExpandidaRegistrosInvalidos() {
        return this.banderaTablaExpandidaRegistrosInvalidos;
    }

    public void setBanderaTablaExpandidaRegistrosInvalidos(
        boolean banderaTablaExpandidaRegistrosInvalidos) {
        this.banderaTablaExpandidaRegistrosInvalidos = banderaTablaExpandidaRegistrosInvalidos;
    }

    public boolean isBanderaMatriculaAntigua() {
        return this.banderaMatriculaAntigua;
    }

    public boolean isBanderaArchivoCargado() {
        return this.banderaArchivoCargado;
    }

    public boolean isBanderaArchivoValidado() {
        return banderaArchivoValidado;
    }

    public boolean isBanderaTipoAvaluoUrbano() {
        return this.banderaTipoAvaluoUrbano;
    }

    public void setBanderaTipoAvaluoUrbano(boolean banderaTipoAvaluoUrbano) {
        this.banderaTipoAvaluoUrbano = banderaTipoAvaluoUrbano;
    }

    // --------------------------Métodos---------------------------- //
    @PostConstruct
    public void init() {
        LOGGER.debug("CarguePrediosDeAvaluoDesdeArchivoMB#init");

        this.inicializarVariables();
    }

    // ---------------------------------------------------------------------- //
    /**
     * Iniciailiza las variables requeridas por este caso de uso
     *
     * @author christian.rodriguez
     */
    private void inicializarVariables() {

        this.erroresValidacion =
            new ArrayList<CarguePrediosDeAvaluoDesdeArchivoMB.ErrorValidacion>();
        this.predios = new ArrayList<SolicitudPredio>();
        this.banderaArchivoCargado = false;
        this.banderaArchivoValidado = false;
        this.usuario = MenuMB.getMenu().getUsuarioDto();

    }

    // ---------------------------------------------------------------------- //
    /**
     * Listener que se encarga de realizar la carga del archivo desde el componente de primefaces
     * fileupload
     *
     * @author christian.rodriguez
     * @param eventoCarga evento enviado por el componente FileUpload
     */
    public void cargarArchivo(FileUploadEvent eventoCarga) {

        this.banderaArchivoValidado = false;

        Object[] cargaArchivo = UtilidadesWeb.cargarArchivo(eventoCarga, this.contexto);

        if (cargaArchivo[0] != null) {

            this.archivoResultado = (File) cargaArchivo[0];

            this.banderaArchivoCargado = true;

            String mensaje = "Documento cargado satisfactoriamente.";
            this.addMensajeInfo(mensaje);

        } else {

            this.banderaArchivoCargado = false;

            LOGGER.error("ERROR: " + cargaArchivo[2]);
            String mensaje = (String) cargaArchivo[1];
            this.addMensajeError(mensaje);
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Listener que ejecuta la validación de un archivo previamente cargado. Valida cada uno de los
     * componentes del registro y guarda los errores en la lista erroresValidacion
     *
     * @author christian.rodriguez
     */
    public void validarArchivo() {

        if (this.archivoResultado != null) {

            this.predios = new ArrayList<SolicitudPredio>();

            this.erroresValidacion =
                new ArrayList<CarguePrediosDeAvaluoDesdeArchivoMB.ErrorValidacion>();

            try {

                BufferedReader bf = new BufferedReader(new FileReader(this.archivoResultado));

                String separador = Constantes.CADENA_SEPARADOR_ARCHIVO_CARGUE_PREDIOS_AVALUOS;

                String registro;
                int numeroRenglon = 0;

                while ((registro = bf.readLine()) != null) {

                    String columnas[] = registro.split(separador);
                    numeroRenglon++;

                    if (this.estructuraArchivoValida(numeroRenglon, registro, columnas)) {

                        this.banderaDatosValidos = true;

                        String codigoDepartamento =
                            columnas[EArchivoCarguePrediosAvaluos.DEPARTAMENTO
                                .getColumna()];
                        String codigoMunicipio = columnas[EArchivoCarguePrediosAvaluos.MUNICIPIO
                            .getColumna()];
                        String tipoAvaluoPredio =
                            columnas[EArchivoCarguePrediosAvaluos.TIPO_AVALUO_PREDIO
                                .getColumna()].toUpperCase();
                        String numeroPredial = columnas[EArchivoCarguePrediosAvaluos.NUMERO_PREDIAL
                            .getColumna()];
                        String matriculaAntigua =
                            columnas[EArchivoCarguePrediosAvaluos.MATRICULA_ANTIGUA
                                .getColumna()];
                        String circuloRegistral =
                            columnas[EArchivoCarguePrediosAvaluos.CIRCULO_REGISTRAL
                                .getColumna()];
                        String noMatricula = columnas[EArchivoCarguePrediosAvaluos.NO_MATRICULA
                            .getColumna()];
                        String direccion = columnas[EArchivoCarguePrediosAvaluos.DIRECCION_PREDIO
                            .getColumna()];
                        String condicionJuridica =
                            columnas[EArchivoCarguePrediosAvaluos.CONDICION_JURIDICA
                                .getColumna()].toUpperCase();
                        String areaTerreno = columnas[EArchivoCarguePrediosAvaluos.AREA_TERRENO
                            .getColumna()];
                        String areaConstruccion =
                            columnas[EArchivoCarguePrediosAvaluos.AREA_CONSTRUCCION
                                .getColumna()];
                        String unidadMedidaAreaTerreno =
                            columnas[EArchivoCarguePrediosAvaluos.UNIDAD_MEDIDA_AREA_TERRENO
                                .getColumna()];
                        String unidadMedidaAreaConstruccion =
                            columnas[EArchivoCarguePrediosAvaluos.UNIDAD_MEDIDA_CONSTRUCCION
                                .getColumna()];

                        Predio predioTemp = this.validarNumeroPredial(numeroRenglon, registro,
                            numeroPredial);

                        if (numeroPredial != null && !numeroPredial.isEmpty()) {

                            Departamento depto = this.validarDepartamento(numeroRenglon, registro,
                                codigoDepartamento, numeroPredial);
                            Municipio muni = this.validarMunicipio(numeroRenglon, registro,
                                codigoMunicipio, numeroPredial);

                            this.validarTipoAvaluoPredio(numeroRenglon, registro, numeroPredial,
                                tipoAvaluoPredio);
                            this.validarMatriculaAntigua(numeroRenglon, registro, numeroPredial,
                                matriculaAntigua);
                            this.validarCirculoRegistral(numeroRenglon, registro, numeroPredial,
                                circuloRegistral);
                            this.validarNoMatricula(numeroRenglon, registro, numeroPredial,
                                noMatricula);

                            direccion = this.getGeneralesService().obtenerDireccionNormalizada(
                                direccion);
                            this.validarDireccion(numeroRenglon, registro, numeroPredial, direccion);

                            this.validarCondicionJuridica(numeroRenglon, registro, numeroPredial,
                                condicionJuridica);
                            this.validarAreaTerreno(numeroRenglon, registro, numeroPredial,
                                areaTerreno);
                            this.validarAreaConstruccion(numeroRenglon, registro, numeroPredial,
                                areaConstruccion);
                            this.validarUnidadMedidaAreaTerreno(numeroRenglon, registro,
                                numeroPredial, unidadMedidaAreaTerreno);
                            this.validarUnidadMedidaAreaConstruccion(numeroRenglon, registro,
                                numeroPredial, unidadMedidaAreaConstruccion);

                            if (this.banderaDatosValidos) {

                                SolicitudPredio predio = new SolicitudPredio();

                                predio.setDepartamento(depto);
                                predio.setMunicipio(muni);
                                predio.setPredioId(predioTemp.getId());
                                predio.setTipoAvaluoPredio(ComboBoxConverter.getCodigo(
                                    "AvaluoTipoPredioAvaluoV", tipoAvaluoPredio));
                                predio.setNumeroPredial(numeroPredial);
                                predio.setCirculoRegistral(circuloRegistral);
                                predio.setNumeroRegistro(noMatricula);
                                predio.setDireccion(direccion);
                                predio.setCondicionJuridica(condicionJuridica);

                                if (this.banderaTipoAvaluoUrbano) {
                                    predio.setAreaTerreno(ConversorUnidadesArea
                                        .convertirAMetroCuadrado(
                                            Double.parseDouble(areaTerreno),
                                            unidadMedidaAreaTerreno));
                                } else {
                                    predio.setAreaTerreno(ConversorUnidadesArea.convertirAHectarea(
                                        Double.parseDouble(areaTerreno),
                                        unidadMedidaAreaTerreno));
                                }

                                predio.setAreaConstruccion(Double.parseDouble(areaConstruccion));

                                this.predios.add(predio);
                            }
                        }
                    }
                }

                this.banderaArchivoCargado = false;

                if (this.predios != null && this.predios.size() > 0) {
                    this.banderaArchivoValidado = true;
                } else {
                    this.banderaArchivoValidado = false;
                }

                bf.close();

            } catch (FileNotFoundException e) {
                String mensaje = "El documento no pudo ser encontrado.";
                this.addMensajeError(mensaje);
            } catch (IOException e) {
                String mensaje = "El documento no pudo ser cargado.";
                this.addMensajeError(mensaje);
            }
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el departamento, (longitud, existencia y correspondencia con número predial
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de linea del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param codigoDepartamento código del depto extraido del registro
     * @return Departamento departamento asociado al codigo cargado, null si no se encontró
     */
    private Departamento validarDepartamento(int numeroRenglon, String registro,
        String codigoDepartamento, String numeroPredial) {

        int longitudCodigo = EArchivoCarguePrediosAvaluos.DEPARTAMENTO.getLongitud();

        if (codigoDepartamento.length() != longitudCodigo) {

            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.DEPARTAMENTO.getDescripcion() + " (" +
                 codigoDepartamento + ") debe ser de longitud " + longitudCodigo + ".";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);

        } else if (!numeroPredial.substring(0, 2).equals(codigoDepartamento)) {

            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.DEPARTAMENTO.getDescripcion() + " (" +
                 codigoDepartamento +
                 ") no corresponde con el departamento del número predial " +
                 numeroPredial.substring(0, 2) + ".";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);

        } else {

            Departamento detpo = this.getGeneralesService().getCacheDepartamentoByCodigo(
                codigoDepartamento);

            if (detpo == null) {
                String causal = "El valor del campo " +
                     EArchivoCarguePrediosAvaluos.DEPARTAMENTO.getDescripcion() + " (" +
                     codigoDepartamento +
                     ") no coincide con ninguno registrado en el sistema.";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            } else {

                return detpo;
            }
        }

        return null;

    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el municipio, (longitud, existencia y correspondencia con número predial
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param codigoMunicipio código del municipio extraido del registro
     * @param numeroPredial numero Predial asociado al registro
     */
    private Municipio validarMunicipio(int numeroRenglon, String registro, String codigoMunicipio,
        String numeroPredial) {

        int longitudCodigo = EArchivoCarguePrediosAvaluos.MUNICIPIO.getLongitud();

        if (codigoMunicipio.length() != longitudCodigo) {

            String causal = "El campo " + EArchivoCarguePrediosAvaluos.MUNICIPIO.getDescripcion() +
                 " (" + codigoMunicipio + ") debe ser de longitud " + longitudCodigo + ".";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);

        } else if (!numeroPredial.substring(2, 5).equals(codigoMunicipio)) {

            String causal = "El campo " + EArchivoCarguePrediosAvaluos.MUNICIPIO.getDescripcion() +
                 " (" + codigoMunicipio +
                 ") no corresponde con el departamento del número predial " +
                 numeroPredial.substring(2, 5) + ".";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);

        } else {

            Municipio muni = this.getGeneralesService().getCacheMunicipioByCodigo(
                numeroPredial.substring(0, 2).concat(codigoMunicipio));

            if (muni == null) {
                String causal = "El valor del campo " +
                     EArchivoCarguePrediosAvaluos.MUNICIPIO.getDescripcion() + " (" +
                     codigoMunicipio + ") no coincide con ninguno registrado en el sistema.";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            } else {
                return muni;
            }
        }

        return null;

    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el tipo de avaluo del predio, revisa que el dato cargado coincida con alguno de los
     * tipos de avaluos especificados en la enumeracion
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param tipoPredioAvaluo cadena de texto con el tipo de avaluo del predio
     */
    private void validarTipoAvaluoPredio(int numeroRenglon, String registro, String numeroPredial,
        String tipoPredioAvaluo) {

        boolean tipoAvaluoEncontrado = false;
        for (EAvaluoPredioTipoAvaluo tipoPredio : EAvaluoPredioTipoAvaluo.values()) {
            if (tipoPredio.toString().equalsIgnoreCase(tipoPredioAvaluo)) {
                tipoAvaluoEncontrado = true;
                break;
            }
        }

        if (!tipoAvaluoEncontrado) {
            String causal = "El valor del campo " +
                 EArchivoCarguePrediosAvaluos.TIPO_AVALUO_PREDIO.getDescripcion() + " (" +
                 tipoPredioAvaluo + ") no coincide con ninguno registrado en el sistema.";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        }

        if (tipoPredioAvaluo.equalsIgnoreCase(ETipoAvaluo.URBANO.toString())) {
            this.banderaTipoAvaluoUrbano = true;
        } else {
            this.banderaTipoAvaluoUrbano = false;
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida la longitud del numero predial
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     */
    private Predio validarNumeroPredial(int linea, String registro, String numeroPredial) {

        int longitudColumna = EArchivoCarguePrediosAvaluos.NUMERO_PREDIAL.getLongitud();

        if (longitudColumna != numeroPredial.length()) {

            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.NUMERO_PREDIAL.getDescripcion() + " (" +
                 numeroPredial + ") debe ser de longitud " + longitudColumna + ".";
            this.agregarErrorValidacion(linea, registro, numeroPredial, causal);
        } else {

            Predio predioTemp = this.getConservacionService().getPredioByNumeroPredial(
                numeroPredial);

            if (predioTemp == null) {
                String causal = "El valor del campo " +
                     EArchivoCarguePrediosAvaluos.NUMERO_PREDIAL.getDescripcion() + " (" +
                     numeroPredial +
                     ") no coincide con ninguno predio registrado en el sistema.";
                this.agregarErrorValidacion(linea, registro, numeroPredial, causal);
            } else {

                boolean predioYaAsociado = false;

                for (SolicitudPredio predio : this.predios) {
                    if (predio.getNumeroPredial().equals(numeroPredial)) {
                        String causal = "El valor del campo " +
                             EArchivoCarguePrediosAvaluos.NUMERO_PREDIAL.getDescripcion() +
                             " (" + numeroPredial + ") ya fue cargado en el renglón (" +
                             linea + ").";
                        this.agregarErrorValidacion(linea, registro, numeroPredial, causal);

                        predioYaAsociado = true;
                    }
                }
                if (this.prediosSolicitud != null) {
                    for (SolicitudPredio predio : this.prediosSolicitud) {
                        if (predio.getNumeroPredial().equals(numeroPredial)) {
                            String causal = "El predio con número predial (" + numeroPredial +
                                 ") ya se encuentra asociado a la solicitud.";
                            this.agregarErrorValidacion(linea, registro, numeroPredial,
                                causal);

                            predioYaAsociado = true;
                        }
                    }
                }
                if (!predioYaAsociado) {
                    return predioTemp;
                }
            }
        }

        return null;
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el tipo de avaluo del predio, revisa que el dato cargado coincida con alguno de los
     * tipos de avaluos especificados en la enumeracion
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param matriculaAntigua cadena de texto con el indicador de si el predio tiene o no tiene
     * matricula antigua
     */
    private void validarMatriculaAntigua(int numeroRenglon, String registro, String numeroPredial,
        String matriculaAntigua) {

        if (!matriculaAntigua.equalsIgnoreCase("s") && !matriculaAntigua.equalsIgnoreCase("n")) {

            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.MATRICULA_ANTIGUA.getDescripcion() + " (" +
                 matriculaAntigua +
                 ") puede contener únicamente los valores 'S', 's', 'N' ó 'n'.";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        } else {
            if (matriculaAntigua.equalsIgnoreCase("s")) {
                this.banderaMatriculaAntigua = true;
            } else {
                this.banderaMatriculaAntigua = false;
            }
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida la longitud del campo de circulo registral, y que exista en la base para cuando es
     * matricula antigua
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param circuloRegistral cadena de texto con el circuloRegistral
     */
    private void validarCirculoRegistral(int numeroRenglon, String registro, String numeroPredial,
        String circuloRegistral) {

        int longitudColumna = EArchivoCarguePrediosAvaluos.CIRCULO_REGISTRAL.getLongitud();

        if (!this.banderaMatriculaAntigua) {

            if (longitudColumna != circuloRegistral.length()) {
                String causal = "El campo " +
                     EArchivoCarguePrediosAvaluos.CIRCULO_REGISTRAL.getDescripcion() + " (" +
                     circuloRegistral + ") debe ser de longitud " + longitudColumna + ".";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);

            } else {

                CirculoRegistral circuloRegistralTemp = this.getGeneralesService()
                    .getCirculoRegistralByCodigo(circuloRegistral);

                if (circuloRegistralTemp == null) {
                    String causal = "El valor del campo " +
                         EArchivoCarguePrediosAvaluos.CIRCULO_REGISTRAL.getDescripcion() +
                         " (" + circuloRegistral +
                         ") no coincide con ninguno registrado en el sistema.";
                    this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
                }
            }
        } else {
            if (!circuloRegistral.isEmpty()) {
                String causal = "No debe ingresar datos (" +
                     EArchivoCarguePrediosAvaluos.CIRCULO_REGISTRAL.getDescripcion() +
                     ") de la matricula inmobiliaria antigua.";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            }
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida la longitud del numero de matricula y que se ingrese cuando es matricula antigua
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param noMatricula cadena de texto con el noMatricula
     */
    private void validarNoMatricula(int numeroRenglon, String registro, String numeroPredial,
        String noMatricula) {

        int longitudColumna = EArchivoCarguePrediosAvaluos.NO_MATRICULA.getLongitud();

        if (noMatricula.isEmpty() || noMatricula.length() > longitudColumna) {

            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.NO_MATRICULA.getDescripcion() + " (" +
                 noMatricula + ") debe ser de longitud menor o igual a " + longitudColumna +
                 ".";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida la longitud de la direccion
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param direccion cadena de texto con la direccion
     */
    private void validarDireccion(int numeroRenglon, String registro, String numeroPredial,
        String direccion) {

        int longitudColumna = EArchivoCarguePrediosAvaluos.DIRECCION_PREDIO.getLongitud();

        if (direccion.isEmpty() || direccion.length() > longitudColumna) {

            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.DIRECCION_PREDIO.getDescripcion() + " (" +
                 direccion + ") debe ser de longitud igual a " + longitudColumna + ".";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el tipo de avaluo del predio, revisa que el dato cargado coincida con alguno de los
     * tipos de avaluos especificados en la enumeracion
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param condicionJuridica cadena de texto con la condicion juridica
     */
    private void validarCondicionJuridica(int numeroRenglon, String registro, String numeroPredial,
        String condicionJuridica) {

        boolean condicionJuridicaEncontrada = false;
        for (ECondicionJuridica condicion : ECondicionJuridica.values()) {
            if (condicion.getCodigo().equalsIgnoreCase(condicionJuridica)) {
                condicionJuridicaEncontrada = true;
                break;
            }
        }

        if (!condicionJuridicaEncontrada) {
            String causal = "El valor del campo " +
                 EArchivoCarguePrediosAvaluos.CONDICION_JURIDICA.getDescripcion() + " (" +
                 condicionJuridica + ") no coincide con ninguno registrado en el sistema.";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        } else {
            if (condicionJuridica.equalsIgnoreCase(ECondicionJuridica.PROPIEDAD_HORIZONTAL
                .getCodigo())) {
                this.banderaPropiedadHorizontal = true;
            } else {
                this.banderaPropiedadHorizontal = false;
            }
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el campo de area terreno. Valida que sea de tipo númerico y que si es NPH el valor
     * debe ser mayor a 0
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param areaTerreno cadena de texto con el area de terreno
     */
    private void validarAreaTerreno(int numeroRenglon, String registro, String numeroPredial,
        String areaTerrenoString) {

        try {

            Double areaTerreno = Double.parseDouble(areaTerrenoString);

            if (!this.banderaPropiedadHorizontal && areaTerreno <= 0) {
                String causal = "El campo " +
                     EArchivoCarguePrediosAvaluos.AREA_TERRENO.getDescripcion() + " (" +
                     areaTerrenoString + ") debe ser mayor a 0.";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            }

        } catch (Exception e) {
            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.AREA_TERRENO.getDescripcion() + " (" +
                 areaTerrenoString + ") debe ser númerico.";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el campo de area construcción. Valida que sea de tipo númerico y que si es PH el valor
     * debe ser mayor a 0
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param areaTerreno cadena de texto con el area de construcción
     */
    private void validarAreaConstruccion(int numeroRenglon, String registro, String numeroPredial,
        String areaConstruccionString) {

        try {

            Double areaTerreno = Double.parseDouble(areaConstruccionString);

            if (this.banderaPropiedadHorizontal && areaTerreno <= 0) {
                String causal = "El campo " +
                     EArchivoCarguePrediosAvaluos.AREA_CONSTRUCCION.getDescripcion() + " (" +
                     areaConstruccionString + ") debe ser mayor a 0.";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            }

        } catch (Exception e) {
            String causal = "El campo " +
                 EArchivoCarguePrediosAvaluos.AREA_CONSTRUCCION.getDescripcion() + " (" +
                 areaConstruccionString + ") debe ser númerico.";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el campo unidad Medida Area Terreno. Valida que sea alguno de los tipos registrados en
     * la enumeracion
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param unidadMedidaAreaTerreno cadena de texto con la unidad de medida del area de terreno
     */
    private void validarUnidadMedidaAreaTerreno(int numeroRenglon, String registro,
        String numeroPredial, String unidadMedidaAreaTerreno) {

        if (this.banderaPropiedadHorizontal) {

            if (unidadMedidaAreaTerreno != null && !unidadMedidaAreaTerreno.isEmpty()) {

                String causal = "El campo " +
                     EArchivoCarguePrediosAvaluos.UNIDAD_MEDIDA_AREA_TERRENO.getDescripcion() +
                     " no se debe ingresar cuando el la condición de la propiedad es (" +
                     ECondicionJuridica.PROPIEDAD_HORIZONTAL.getCodigo() + ").";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            }

        } else {

            boolean unidadEncontrada = false;
            for (EUnidadMedidaAreaTerreno unidad : EUnidadMedidaAreaTerreno.values()) {
                if (unidad.getCodigo().equalsIgnoreCase(unidadMedidaAreaTerreno)) {
                    unidadEncontrada = true;
                    break;
                }
            }

            if (!unidadEncontrada) {
                String causal = "El valor del campo " +
                     EArchivoCarguePrediosAvaluos.UNIDAD_MEDIDA_AREA_TERRENO.getDescripcion() +
                     " (" + unidadMedidaAreaTerreno +
                     ") no coincide con ninguno registrado en el sistema.";
                this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
            }
        }
    }

    // ---------------------------------------------------------------------- //
    /**
     * Valida el campo unidad Medida Area Construccion. Valida que sea metro cuadrado
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param unidadMedidaAreaConstruccion cadena de texto con la unidad de medida del area de
     * construccion
     */
    private void validarUnidadMedidaAreaConstruccion(int numeroRenglon, String registro,
        String numeroPredial, String unidadMedidaAreaConstruccion) {

        if (!EUnidadMedidaConstruccion.METRO_CUADRADO.getCodigo().equalsIgnoreCase(
            unidadMedidaAreaConstruccion)) {

            String causal = "El valor del campo " +
                 EArchivoCarguePrediosAvaluos.UNIDAD_MEDIDA_CONSTRUCCION.getDescripcion() +
                 " (" + unidadMedidaAreaConstruccion + ") puede ser únicamente (" +
                 EUnidadMedidaConstruccion.METRO_CUADRADO.getCodigo() + ").";
            this.agregarErrorValidacion(numeroRenglon, registro, numeroPredial, causal);
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * Determina si la estructura del archivo es valida según el número de columnas.
     *
     * @author christian.rodriguez
     * @param cadena con el registro completo leido desde el archivo, se usa por si ocurre algún
     * error
     * @param columnas columnas extraidas del registro
     * @return true si el registro cuenta con las 19 columnas requeridas, false si no
     */
    private boolean estructuraArchivoValida(int numeroRenglon, String registro, String[] columnas) {

        int numeroColumnas = EArchivoCarguePrediosAvaluos.values().length;

        if (columnas[0] != null && columnas.length == numeroColumnas) {
            return true;
        } else {

            String causal = "No tiene los " + numeroColumnas + " campos requeridos para la carga.";

            this.agregarErrorValidacion(numeroRenglon, registro, "ARCHIVO", causal);
            return false;
        }

    }

    // ---------------------------------------------------------------------- //
    /**
     * Agrega un error de validación a la lista de errores, el error cuenta con un registro en el
     * que se encontró el error y un causal del error
     *
     * @author christian.rodriguez
     * @param numeroRenglon numero de renglón del archivo correspondiente al registro
     * @param registro cadena con el registro completo leido desde el archivo, se usa por si ocurre
     * algún error
     * @param numeroPredial numero Predial asociado al registro
     * @param causal causal que genero el error
     */
    private void agregarErrorValidacion(int numeroRenglon, String registro, String numeroPredial,
        String causal) {

        this.banderaDatosValidos = false;

        ErrorValidacion error = new ErrorValidacion(numeroRenglon, registro, causal, numeroPredial);
        this.erroresValidacion.add(error);

    }

    // ------------------------------------------------------------------------------------
    /**
     * Guarda los predios validos en la BD y los asocia a la solicitud
     *
     * @author christian.rodriguez
     */
    public void cargarPrediosValidos() {

        if (this.predios != null && this.predios.size() > 0) {

            for (SolicitudPredio predio : this.predios) {
                predio.setSolicitud(this.solicitudSeleccionada);
                predio.setUsuarioLog(this.usuario.getLogin());
                predio.setFechaLog(new Date());
            }

            this.getTramiteService().guardarActualizarSolicitudesPredios(predios);

            if (this.prediosSolicitud == null) {
                this.prediosSolicitud = new ArrayList<SolicitudPredio>();
            }

            this.prediosSolicitud.addAll(this.predios);

            this.banderaArchivoValidado = false;
            this.banderaArchivoCargado = false;

            this.addMensajeInfo("Los predios válidos fueron guardados satisfactoriamente");
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Listener que reinicializa las variables usadas en el caso de uso
     *
     * @author christian.rodriguez
     */
    public void iniciarListaPredios() {
        this.inicializarVariables();
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar variables desde DiligenciamientoSolicitudMB
     *
     * @author rodrigo.hernandez
     *
     * @param solicitud - Solicitud en curso
     * @param solicitante - Solicitante asociado a la solicitud
     * @param predios - Predios asociados a la solicitud
     * @param usuario - Usuario en sesión
     */
    public void cargarVariablesExternas(Solicitud solicitud, SolicitanteSolicitud solicitante,
        UsuarioDTO usuario, List<SolicitudPredio> predios) {

        LOGGER.debug("Iniciando CarguePrediosDeAvaluoDesdeArchivoMB#cargarVariablesExternas");

        this.solicitudSeleccionada = solicitud;
        this.solicitante = solicitante;
        this.usuario = usuario;
        this.prediosSolicitud = predios;

        this.inicializarVariables();

        LOGGER.debug("Finalizando CarguePrediosDeAvaluoDesdeArchivoMB#cargarVariablesExternas");

    }

    // ---------------------------------------------------------------------- //
    /**
     * Inner class que se usa para poder imprimir los errores de validación en la vista jsf
     *
     * @author christian.rodriguez
     *
     */
    public class ErrorValidacion {

        private int numeroRenglon;
        private String registro;
        private String causal;
        private String numeroPredial;

        private ErrorValidacion(int numeroRenglon, String registro, String causal,
            String numeroPredial) {
            this.numeroRenglon = numeroRenglon;
            this.registro = registro;
            this.causal = causal;
            this.numeroPredial = numeroPredial;
        }

        public int getNumeroRegistro() {
            return numeroRenglon;
        }

        public String getRegistro() {
            return registro;
        }

        public String getCausal() {
            return causal;
        }

        public String getNumeroPredial() {
            return numeroPredial;
        }

    }
}
