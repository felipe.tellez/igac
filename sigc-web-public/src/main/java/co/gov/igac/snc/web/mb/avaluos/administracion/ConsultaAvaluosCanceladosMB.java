/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del cu Consultar Avalúos Cancelados
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-106
 */
@Component("consultaAvaluosCancelados")
@Scope("session")
public class ConsultaAvaluosCanceladosMB extends SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaAvaluosCanceladosMB.class);

    /**
     * {@link FiltroDatosConsultaAvaluo} filtro usado para la consulta
     */
    private FiltroDatosConsultaAvaluo filtroAvaluos;

    /**
     * Lista con los {@link Avaluo} cancelados encontrados en la busqueda
     */
    private List<Avaluo> avaluosCancelados;

    /**
     * Avaluo seleccionado de la tabla de avalúos cancelados
     */
    private Avaluo avaluoSeleccionado;

    private ArrayList<SelectItem> listaDepartamentos;
    private ArrayList<SelectItem> listaMunicipios;

    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * Bandera que determina si persona natural o no
     */
    private boolean banderaEsPersonaNatural;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaAvaluo getFiltroAvaluos() {
        return filtroAvaluos;
    }

    public List<Avaluo> getAvaluosCancelados() {
        return avaluosCancelados;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public ArrayList<SelectItem> getListaDepartamentos() {
        return this.listaDepartamentos;
    }

    public ArrayList<SelectItem> getListaMunicipios() {
        return this.listaMunicipios;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de ConsultaAvaluosAprobadosMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.filtroAvaluos = new FiltroDatosConsultaAvaluo();
        this.filtroAvaluos.setTipoEmpresa(EPersonaTipoPersona.JURIDICA.getCodigo());
        this.cargarListaDepartamentos();
        this.cargarListaMunicipios();
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lsita de departamentos según la territorial del usuario activo
     *
     * @author christian.rodriguez
     */
    private void cargarListaDepartamentos() {

        this.listaDepartamentos = new ArrayList<SelectItem>();
        this.listaDepartamentos.add(new SelectItem(null, this.DEFAULT_COMBOS));

        List<Departamento> departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.usuario.getCodigoTerritorial());

        for (Departamento departamento : departamentos) {
            this.listaDepartamentos.add(new SelectItem(
                departamento.getCodigo(), departamento.getNombre()));
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lista de municipios según el departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void cargarListaMunicipios() {

        this.listaMunicipios = new ArrayList<SelectItem>();
        this.listaMunicipios.add(new SelectItem(null, this.DEFAULT_COMBOS));

        List<Municipio> municipios = (ArrayList<Municipio>) this
            .getGeneralesService().getCacheMunicipiosByDepartamento(
                this.filtroAvaluos.getDepartamento());

        for (Municipio municipio : municipios) {
            this.listaMunicipios.add(new SelectItem(municipio.getCodigo(),
                municipio.getNombre()));
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que realiza la busqueda de los avaluos candelados dependiendo del
     * {@link FiltroDatosConsultaAvaluo} ingreasdo
     *
     * @author christian.rodriguez
     */
    public void buscar() {
        if (this.filtroAvaluos != null) {

            this.filtroAvaluos.setEstadoTramiteAvaluo(ETramiteEstado.CANCELADO
                .getCodigo());

            this.avaluosCancelados = this.getAvaluosService()
                .buscarAvaluosPorFiltro(this.filtroAvaluos);
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia el tipo de empresa. Activa la bandera de tipo de
     * empresa dependiendo de si es natural o juridica
     *
     * @modified christian.rodriguez
     */
    public void onChangeTipoPersona() {

        if (this.filtroAvaluos.getTipoEmpresa().equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {
            this.banderaEsPersonaNatural = true;
        } else {
            this.banderaEsPersonaNatural = false;
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

}
