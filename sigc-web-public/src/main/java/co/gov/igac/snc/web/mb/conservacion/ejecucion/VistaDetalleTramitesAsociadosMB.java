/**
 *
 */
package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import javax.annotation.PostConstruct;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * @author cesar.moreno
 *
 */
@Component("vistaDetalleTramitesAsociados")
@Scope("session")
public class VistaDetalleTramitesAsociadosMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(VistaDetalleTramitesAsociadosMB.class);

    /**
     * Trámite con asociados seleccionado
     */
    private Tramite tramiteConAsociadosSeleccionado;

    @PostConstruct
    public void init() {
        LOGGER.debug("init VistaDetalleTramitesAsociados");
    }

    public Tramite getTramiteConAsociadosSeleccionado() {
        return tramiteConAsociadosSeleccionado;
    }

    public void setTramiteConAsociadosSeleccionado(Tramite tramiteConAsociadosSeleccionado) {
        Hibernate.initialize(tramiteConAsociadosSeleccionado);
        this.tramiteConAsociadosSeleccionado = tramiteConAsociadosSeleccionado;
    }
}
