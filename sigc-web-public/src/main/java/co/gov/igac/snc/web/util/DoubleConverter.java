package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author fredy.wilches
 *
 */
//TODO :: fredy.wilches :: 17-05-2012 :: no se entiende para qué es la clase :: pedro.garcia
public class DoubleConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DoubleConverter.class);

    @Override
    public Object getAsObject(FacesContext paramFacesContext,
        UIComponent paramUIComponent, String paramString) {
        if (paramString == null || paramString.equals("")) {
            return null;
        }
        return new Double(paramString);
    }

    @Override
    public String getAsString(FacesContext paramFacesContext,
        UIComponent paramUIComponent, Object paramObject) {
        return "" + paramObject;
    }

}
