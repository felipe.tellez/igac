package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.HFichaMatriz;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.HPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.ImageIcon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Managed bean usado en el detalle del predio para consultar el origen de un predio cuando esta ha
 * sido generado a partir del SNC.
 *
 * @see CU-NP-CO-243 Consultar origen del predio
 *
 * @author david.cifuentes
 * @version 1.0
 */
@Component("consultaOrigenPredio")
@Scope("session")
public class ConsultaOrigenPredioMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = -4080047933059767210L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaOrigenPredioMB.class);

    // --------------------------------------------------- //
    /**
     * Managed Bean usado para consultar los detalles del predio
     */
    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    @Autowired
    private transient IContextListener contexto;

    // --------------------------------------------------- //
    private Predio predio;
    private Tramite tramiteOrigenNumeroPredial;
    private Documento resolucionOriginaTramite;
    private List<HPredio> prediosOrigen;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;

    // --------------------------------------------------- //
    @PostConstruct
    public void init() {

        this.applicationClientName = this.contexto.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        Long predioId = mb001.getSelectedPredioId();

        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioId != null) {
            if (!esPredioOrigen) {
                this.getPredioByPredioId(predioId);
            } else {
                this.predio = mb001.getSelectedPredio1();
            }
        }

        // Cargue de información de origen del predio
        if (this.predio != null) {
            HPredio hPredio = this.getConservacionService().buscarPrimerProyeccionNumeroPredial(
                this.predio.getNumeroPredial());

            if (hPredio != null) {
                this.tramiteOrigenNumeroPredial = this.getTramiteService().
                    buscarTramiteCompletoConResolucion(hPredio.getTramiteId());
                if (this.tramiteOrigenNumeroPredial != null) {
                    // Resolución
                    if (this.tramiteOrigenNumeroPredial.getResultadoDocumento() != null) {
                        this.resolucionOriginaTramite = this.tramiteOrigenNumeroPredial.
                            getResultadoDocumento();
                    } else {
                        this.resolucionOriginaTramite = new Documento();
                    }

                    // Predios asociados al trámite
                    this.prediosOrigen = this.getConservacionService().buscarPrediosOrigenDeTramite(
                        this.tramiteOrigenNumeroPredial.getId());

                } else {
                    this.tramiteOrigenNumeroPredial = new Tramite();
                }
                //inicializar parametros editor geografico
                this.parametroEditorGeografico = this.usuario.getLogin() + "," +
                    this.tramiteOrigenNumeroPredial.getId() +
                     "," + Constantes.CONSULTA_HISTORICO;
            }
        }
    }

    // --------------------------------------------------- //
    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public Tramite getTramiteOrigenNumeroPredial() {
        return tramiteOrigenNumeroPredial;
    }

    public void setTramiteOrigenNumeroPredial(Tramite tramiteOrigenNumeroPredial) {
        this.tramiteOrigenNumeroPredial = tramiteOrigenNumeroPredial;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public List<HPredio> getPrediosOrigen() {
        return this.prediosOrigen;
    }

    public void setPrediosOrigen(List<HPredio> prediosOrigen) {
        this.prediosOrigen = prediosOrigen;
    }

    public Documento getResolucionOriginaTramite() {
        return resolucionOriginaTramite;
    }

    public void setResolucionOriginaTramite(Documento resolucionOriginaTramite) {
        this.resolucionOriginaTramite = resolucionOriginaTramite;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;

    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    // --------------------------------------------------- //
    /**
     * Método que hace la consulta de los datos que van en cada pestaña de detalles del predio
     *
     * @author david.cifuentes
     * @param predioId
     */
    public void getPredioByPredioId(Long predioId) {
        this.predio = this.getConservacionService()
            .obtenerPredioConPersonasPorPredioId(predioId);
    }

    // --------------------------------------------------- //
    /**
     * Método que realiza la incialización del managed bean de consulta de predio
     *
     * @author david.cifuentes
     */
    public void inicializarConsultaPredioMB() {
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb
            .getManagedBean("consultaPredio");
        mb001.cleanAllMB();
        if (this.predio != null) {
            this.consultaPredioMB.setSelectedPredioId(this.predio.getId());
        }
    }

}
