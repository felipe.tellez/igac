package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.util.ESiNo;

/**
 * Conversor usado en los checkbox, porque en html usan true o false, pero para poder atarlo
 * directamente a un atributo de un pojo, se usa este conversor, que convierte el true en SI, el
 * false en NO y visceversa
 *
 * @author fredy.wilches
 *
 */
public class BooleanConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(BooleanConverter.class);

    @Override
    public Object getAsObject(FacesContext paramFacesContext,
        UIComponent paramUIComponent, String paramString) {
        if (paramString.equals("false")) {
            return new Boolean(false);
        } else {
            return new Boolean(true);
        }
    }

    @Override
    public String getAsString(FacesContext paramFacesContext,
        UIComponent paramUIComponent, Object paramObject) {
        Boolean b = (Boolean) paramObject;
        if (b) {
            return ESiNo.SI.getCodigo();
        } else {
            return ESiNo.NO.getCodigo();
        }
    }

}
