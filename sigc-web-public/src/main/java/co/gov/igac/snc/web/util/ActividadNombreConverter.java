package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.util.EProcesoConservacionNombreActividad;

/**
 * Converter para reemplazar el nombre de la actividad que viene del proceso por el nombre para
 * mostrar al usuario.
 *
 * @author david.cifuentes
 *
 */
public class ActividadNombreConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActividadNombreConverter.class);

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorString) {

        try {
            return valorString;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " + valorString);
            return valorString;
        }
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorObjeto) {

        String nombreActividad = null;
        for (@SuppressWarnings("rawtypes") Enum e : EProcesoConservacionNombreActividad.values()) {
            if (valorObjeto.equals(((EProcesoConservacionNombreActividad) e).getCodigo())) {
                nombreActividad = ((EProcesoConservacionNombreActividad) e).getValor();
                break;
            }
        }
        try {
            return nombreActividad;
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " + valorObjeto);
            return (String) valorObjeto;
        }
    }
}
