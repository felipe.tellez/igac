/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.util.EInconsistenciasGeograficas;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso Estudiar ajuste espacial
 *
 * @author felipe.cadena
 * @CU CU-SA-AC-200
 */
@Component("estudiarAjusteEspacial")
@Scope("session")
public class EstudioAjusteEspacialMB extends ProcessFlowManager {

    private static final long serialVersionUID = 5608198030936651458L;
    private static final Logger LOGGER =
        LoggerFactory.getLogger(EstudioAjusteEspacialMB.class);
    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    @Autowired
    private IContextListener contextService;
    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;
    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentProcessActivity;
    /**
     * Tramite relacionado a la actividad actual
     */
    private Tramite tramiteActual;
    /**
     * Predios relacionados al tramite actual
     */
    private List<Predio> prediosTramite;
    /**
     * Inconsistencias asociadas al tramite
     */
    private List<TramiteInconsistencia> insconsistenciasTramite;
    /**
     * Entidad de depuración asociada al trmaite.
     */
    private TramiteDepuracion tramiteDepuracion;
    /**
     * Bandera que determina si el tramite es viable para depuración
     */
    private boolean banderaViable;
    /**
     * Bandera para habilitar el avance del proceso
     */
    private boolean banderaAvanzarProceso;
    /**
     * Bandera para habilitar las opciones de trámite visible
     */
    private boolean banderaHabilitarTramiteVisible;
    /**
     * Bandera para habilitar el ingreso de razones de rechazo
     */
    private boolean banderaMostrarRazonesRechazo;
    /**
     * Bandera para determinar si se selecciono un area en el caso de que se requiera.
     */
    private boolean banderaAreaSeleccionada;

    /**
     * Bandera para determinar si se generao una replica geogràfica.
     */
    private boolean banderaReplicaCreada;
    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;
    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String prediosParaZoom;
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;
    @Autowired
    private IContextListener contextoWeb;

    //---------------------Getters and Setters---------------------//
    public Tramite getTramiteActual() {
        return tramiteActual;
    }

    public void setTramiteActual(Tramite tramiteActual) {
        this.tramiteActual = tramiteActual;
    }

    public boolean isBanderaReplicaCreada() {
        return banderaReplicaCreada;
    }

    public void setBanderaReplicaCreada(boolean banderaReplicaCreada) {
        this.banderaReplicaCreada = banderaReplicaCreada;
    }

    public boolean isBanderaAreaSeleccionada() {
        return banderaAreaSeleccionada;
    }

    public void setBanderaAreaSeleccionada(boolean banderaAreaSeleccionada) {
        this.banderaAreaSeleccionada = banderaAreaSeleccionada;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public List<TramiteInconsistencia> getInsconsistenciasTramite() {
        return insconsistenciasTramite;
    }

    public void setInsconsistenciasTramite(List<TramiteInconsistencia> insconsistenciasTramite) {
        this.insconsistenciasTramite = insconsistenciasTramite;
    }

    public List<Predio> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<Predio> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public TramiteDepuracion getTramiteDepuracion() {
        return tramiteDepuracion;
    }

    public void setTramiteDepuracion(TramiteDepuracion tramiteDepuracion) {
        this.tramiteDepuracion = tramiteDepuracion;
    }

    public boolean isBanderaViable() {
        return banderaViable;
    }

    public void setBanderaViable(boolean banderaViable) {
        this.banderaViable = banderaViable;
    }

    public boolean isBanderaAvanzarProceso() {
        return banderaAvanzarProceso;
    }

    public void setBanderaAvanzarProceso(boolean banderaAvanzarProceso) {
        this.banderaAvanzarProceso = banderaAvanzarProceso;
    }

    public boolean isBanderaMostrarRazonesRechazo() {
        return banderaMostrarRazonesRechazo;
    }

    public void setBanderaMostrarRazonesRechazo(boolean banderaMostrarRazonesRechazo) {
        this.banderaMostrarRazonesRechazo = banderaMostrarRazonesRechazo;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public boolean isBanderaHabilitarTramiteVisible() {
        return banderaHabilitarTramiteVisible;
    }

    public void setBanderaHabilitarTramiteVisible(boolean banderaHabilitarTramiteVisible) {
        this.banderaHabilitarTramiteVisible = banderaHabilitarTramiteVisible;
    }

    private String mensaje;

    @PostConstruct
    public void init() {

        LOGGER.debug("on InformeVisitaRevisionOAutoavaluoMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "estudiarAjusteEspacial");

        this.tramiteActual = new Tramite();
        this.banderaAvanzarProceso = false;
        this.banderaMostrarRazonesRechazo = false;
        this.banderaHabilitarTramiteVisible = true;

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        Long idtramiteActual = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();
        this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.tramiteActual.setId(idtramiteActual);
        this.cargarDatosTramite();
        this.validarInconsistencias();
        this.inicializarVariablesVisorGIS();

        Documento replica = this.getGeneralesService().buscarDocumentoPorTramiteIdyTipoDocumento(
            idtramiteActual, ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());

        if (this.banderaAreaSeleccionada) {
            if (replica != null && replica.getArchivo() != null) {
                this.banderaReplicaCreada = true;
            } else {
                this.banderaReplicaCreada = false;
            }
        } else {
            this.banderaReplicaCreada = true;
        }

        if (this.tramiteDepuracion.getViable() == null) {
            this.tramiteDepuracion.setViable(ESiNo.SI.getCodigo());
            this.banderaViable = true;
        }

        if (this.tramiteActual.isSegunda() ||
            this.tramiteActual.isTercera() ||
            this.tramiteActual.isRectificacionArea() ||
            this.tramiteActual.isRectificacionAreaConstruccion()) {

            for (Predio p : this.tramiteActual.getPredios()) {
                if (p.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_8.getCodigo()) ||
                    p.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {

                    FichaMatriz ficha = this.getConservacionService().
                        obtenerFichaMatrizOrigen(p.getNumeroPredial().substring(0, 21));

                    if (ficha == null) {
                        this.tramiteDepuracion.setViable(ESiNo.NO.getCodigo());
                        this.banderaAreaSeleccionada = false;
                        this.banderaHabilitarTramiteVisible = false;
                        this.banderaViable = false;
                    }
                }
            }
        }

    }

    //--------------------------------------------------------------------------------------------------
    //----------- bpm
    /**
     *
     */
    @Implement
    public boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------

    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO message = new ActivityMessageDTO();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();
        String transitionName = null;

        if (!this.banderaViable) {
            String actividadPrevia = this.currentProcessActivity.getObservacion();

            if (actividadPrevia.equals(
                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE)) {
                usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.currentUser.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);
            } else {
                usuariosActividad = new ArrayList<UsuarioDTO>();
                usuariosActividad.add(this.getGeneralesService().
                    getCacheUsuario(this.tramiteActual.getFuncionarioEjecutor()));
            }

            transitionName = actividadPrevia;
        } else if (this.tramiteDepuracion.getCampo().equals(ESiNo.NO.getCodigo())) {
            usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.currentUser.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_SIG);
            transitionName = ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_DIGITALIZADOR;
        } else {
            usuariosActividad = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.currentUser.getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION);
            // transitionName = "Depuración.Revisar tareas a realizar en terreno";
            transitionName = ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_TERRENO;

            // se clasifica el tramite como terreno si no tiene clasificacion
            if (this.tramiteActual.getClasificacion() == null ||
                 "".equals(this.tramiteActual.getClasificacion())) {
                this.tramiteActual.setClasificacion(ETramiteClasificacion.TERRENO.toString());
                Boolean act = this.getTramiteService().
                    actualizarTramite2(tramiteActual, currentUser);
                if (!act) {
                    this.addMensajeError("Error al actualizar el Tràmite");
                    return;
                }
            }
        }

        message.setActivityId(this.currentProcessActivity.getId());

        solicitudCatastral.setUsuarios(usuariosActividad);
        message.setTransition(transitionName);
        solicitudCatastral.setTransicion(transitionName);

        message.setSolicitudCatastral(solicitudCatastral);
        this.setMessage(message);

        if (this.getMessage().getComment() != null) {
            message.setComment(this.getMessage().getComment());
        }

    }
//--------------------------------------------------

    /**
     *
     */
    @Implement
    public void doDatabaseStatesUpdate() {
        // do nothing
    }

    /**
     * Método para cargar los datos relacionados al tramite que se deben mostrar.
     *
     * @author felipe.cadena
     */
    public void cargarDatosTramite() {
        LOGGER.debug("Cargando datos tramite...");

        this.tramiteActual = this.getTramiteService().
            findTramitePruebasByTramiteId(this.tramiteActual.getId());

        this.prediosTramite = this.tramiteActual.getPredios();

        this.tramiteDepuracion = this.getTramiteService().
            buscarUltimoTramiteDepuracionPorIdTramite(this.tramiteActual.getId());

        this.actualizarOpcionesDepuracion();
        this.parametroEditorGeografico = this.currentUser.getLogin() + "," + this.tramiteActual.
            getId() +
            "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class,
                this.currentProcessActivity.getNombre());

        this.insconsistenciasTramite = this.getTramiteService().
            buscarTramiteInconsistenciaPorTramiteId(this.tramiteActual.getId());

        LOGGER.debug("Fin carga datos...");
    }

    /**
     * Mètodo para verificar si hay una inconsistencia de falta de manzana
     *
     * @author felipe.cadena
     */
    public void validarInconsistencias() {
        this.banderaAreaSeleccionada = true;
        for (TramiteInconsistencia ti : this.insconsistenciasTramite) {
            if (ti.getInconsistencia().equals(EInconsistenciasGeograficas.NO_MANZANA.getNombre())) {
                this.banderaAreaSeleccionada = false;
                break;
            }
        }
    }

    /**
     * Método para inicializar las variables del visor GIS
     *
     * @author felipe.cadena modificado por leidy.gonzalez
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.OFERTAS_INMOBILIARIAS.getCodigo();
        this.prediosParaZoom = "";

        for (Predio predio : this.prediosTramite) {
            this.prediosParaZoom += predio.getNumeroPredial() + ",";
        }

        if (tramiteActual.isQuinta()) {
            this.prediosParaZoom += tramiteActual.getNumeroPredial() + ",";
        }
        this.prediosParaZoom = this.prediosParaZoom.substring(0, this.prediosParaZoom.lastIndexOf(
            ','));
    }

    /**
     * Método para actualizar las opciones disponibles dependiendo si el tramite es viable o no.
     *
     * @author felipe.cadena
     */
    public void actualizarOpcionesDepuracion() {
        LOGGER.debug("Actualizando estado...");
        this.banderaAvanzarProceso = false;
        if (this.tramiteDepuracion.getViable() == null) {
            this.banderaViable = false;
        } else if (this.tramiteDepuracion.getViable().equals(ESiNo.SI.getCodigo())) {
            this.banderaViable = true;
            this.banderaMostrarRazonesRechazo = false;
        } else {
            this.banderaViable = false;
        }
    }

    /**
     * Método para limpiar las tareas si el tramite ya no es viable.
     *
     * @author felipe.cadena
     */
    public void limpiarTareas() {
        this.tramiteDepuracion.setCampo(null);
        this.tramiteDepuracion.setTareasRealizar(null);
    }

    /**
     * Método para limpiar las razones de rechazo si el tramite es viable.
     *
     * @author felipe.cadena
     */
    public void limpiarRazonesRechazo() {
        this.tramiteDepuracion.setRazonesRechazo(null);
    }

    /**
     * Método para habilitar el ingreso de razones de rechazo cuando el tramite no es viable.
     *
     * @author felipe.cadena
     */
    public void habilitarRazones() {
        this.limpiarTareas();
        this.banderaMostrarRazonesRechazo = true;
    }

    /**
     * Método para guardar los cambios realizados en la depuración del tramite.
     *
     * @author felipe.cadena
     */
    public void guardarDepuracion() {
        LOGGER.debug("Guardando depuración...");
        if (this.tramiteDepuracion.getViable().equals(ESiNo.NO.getCodigo()) &&
            (this.tramiteDepuracion.getRazonesRechazo() == null || this.tramiteDepuracion.
            getRazonesRechazo().isEmpty())) {
            this.addMensajeError("Debe registrar las razones del rechazo del ajuste espacial");
        }

        this.banderaAvanzarProceso = false;
        if (this.banderaViable &&
             this.banderaAreaSeleccionada) {
            this.banderaAvanzarProceso = true;
        }
        if (!this.banderaViable &&
             (this.tramiteDepuracion.getRazonesRechazo() != null && !this.tramiteDepuracion.
            getRazonesRechazo().isEmpty())) {
            this.banderaAvanzarProceso = true;
        }

        this.getTramiteService().actualizarTramiteDepuracion(this.tramiteDepuracion);

        if (this.tramiteDepuracion.getRazonesRechazo() != null && !this.tramiteDepuracion.
            getRazonesRechazo().isEmpty()) {
            this.addMensajeInfo("Se guardó la información satisfactoriamente");
        }

    }

    /**
     * Método para avanzar el proceso.
     *
     * @author felipe.cadena
     */
    public String avanzarProceso() {
        LOGGER.debug("Avanzando proceso...");
        this.setupProcessMessage();
        this.forwardProcess();

        //D: hay que hacer esto aquí porque no se puede factorizar. (me da pereza explicar por qué)
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;

    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando EstudioAjusteEspacialMB#cerrar");

        UtilidadesWeb.removerManagedBean("estudiarAjusteEspacial");
        this.tareasPendientesMB.init();

        LOGGER.debug("finalizando EstudioAjusteEspacialMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
