package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.validacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.contenedores.DivisionAdministrativa;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeControlCalidad;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidad;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOferta;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadOfertaOb;
import co.gov.igac.snc.persistence.entity.avaluos.OfertaPredio;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.DetalleOfertaInmobiliariaMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed Bean para realizar control de calidad sobre las ofertas de un recolector
 *
 * @author rodrigo.hernandez
 * @cu CU-TV-0F-009
 * @version 2.0
 *
 */
@Component("realizacionControlCalidadOferta")
@Scope("session")
public class RealizacionControlCalidadOfertaMB extends SNCManagedBean {

    /**
     * serial auto-generado
     */
    private static final long serialVersionUID = 1893033539656838816L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RealizacionControlCalidadOfertaMB.class);

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private DetalleOfertaInmobiliariaMB detalleOfertaInmobiliariaMB;

    @Autowired
    protected TareasPendientesMB tareasPendientesMB;

    @Autowired
    private IContextListener contextoWeb;

    private Actividad bpmActivityInstance;
    /**
     * Contador de ofertas por revisar
     */
    private int contadorOfertasPorRevisar;

    /**
     * Recolector a revisar
     */
    private UsuarioDTO recolector;

    /**
     * Lista de Ofertas del Recolector
     */
    private List<ControlCalidadOferta> listaOfertasRecolector;

    /**
     * Oferta seleccionada para realizar control de calidad
     */
    private ControlCalidadOferta ofertaSeleccionada;

    /**
     * Id del Recolector
     */
    private String idRecolector;

    /**
     * Id del Control de Calidad
     */
    private Long controlCalidadId;

    /**
     * Lista de Observaciones hechas sobre la oferta que se va a revisar
     */
    private List<ControlCalidadOfertaOb> listaObservacionesOferta;

    /**
     * Observación de la oferta que se va a revisar
     */
    private ControlCalidadOfertaOb observacionOferta;

    /**
     * Bandera que indica que se va a crear una observación sobre la oferta a revisar
     */
    private boolean banderaOfertaRechazada;

    /**
     * Motivo del Rechazo de la Oferta
     */
    private String motivoRechazoOferta;

    /**
     * Usuario de la aplicación
     */
    private UsuarioDTO usuario;

    /**
     * Bandera para indicar si se puede mover el proceso
     */
    private boolean banderaMoverProceso;

    /**
     * Variable que almacena los numeros prediales asociados a una oferta inmobiliaria
     */
    private String prediosParaZoom;

    /**
     * Variable que almacena el estado resultado de la muestra evaluada
     *
     * @author christian.rodriguez
     */
    private String estadoSubMuestra;

    /**
     * Constante para definir el porcentaje de aprobación
     */
    private static final double PORCENTAJE_APROBACION = 1d;

    /**
     * Constante para definir el porcentaje de aprobación condicional
     */
    private static final double PORCENTAJE_APROBACION_CONDICIONAL = 0.60d;

    /**
     * Variable para almacenar el porcentaje de aprobacion obtenido en el control de calidad
     */
    private double calculoPorcentaje;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Esta variable es para el parametro de las capas del mapa
     */
    private String moduleLayer;
    /**
     * Esta variable es para el parametro de las herramientas del mapa
     */
    private String moduleTools;

    private boolean banderaVisorVisible;

    private RegionCapturaOferta regionSeleccionada;

    private boolean banderaDeshabilitarBotones;

    /**
     * Control de calidad asociado a la regiÃ³n
     */
    private ControlCalidad controlCalidad;

    public boolean isBanderaDeshabilitarBotones() {
        return banderaDeshabilitarBotones;
    }

    public int getContadorOfertasPorRevisar() {
        return contadorOfertasPorRevisar;
    }

    public void setContadorOfertasPorRevisar(int contadorOfertasPorRevisar) {
        this.contadorOfertasPorRevisar = contadorOfertasPorRevisar;
    }

    public UsuarioDTO getRecolector() {
        return recolector;
    }

    public void setRecolector(UsuarioDTO recolector) {
        this.recolector = recolector;
    }

    public List<ControlCalidadOferta> getListaOfertasRecolector() {
        return listaOfertasRecolector;
    }

    public void setListaOfertasRecolector(List<ControlCalidadOferta> listaOfertasRecolector) {
        this.listaOfertasRecolector = listaOfertasRecolector;
    }

    public ControlCalidadOferta getOfertaSeleccionada() {
        return ofertaSeleccionada;
    }

    public void setOfertaSeleccionada(ControlCalidadOferta ofertaSeleccionada) {
        this.ofertaSeleccionada = ofertaSeleccionada;
    }

    public List<ControlCalidadOfertaOb> getListaObservacionesOferta() {
        return listaObservacionesOferta;
    }

    public void setListaObservacionesOferta(List<ControlCalidadOfertaOb> listaObservacionesOferta) {
        this.listaObservacionesOferta = listaObservacionesOferta;
    }

    public ControlCalidadOfertaOb getObservacionOferta() {
        return observacionOferta;
    }

    public void setObservacionOferta(ControlCalidadOfertaOb observacionOferta) {
        this.observacionOferta = observacionOferta;
    }

    public boolean isBanderaOfertaRechazada() {
        return banderaOfertaRechazada;
    }

    public void setBanderaOfertaRechazada(boolean banderaOfertaRechazada) {
        this.banderaOfertaRechazada = banderaOfertaRechazada;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getMotivoRechazoOferta() {
        return motivoRechazoOferta;
    }

    public void setMotivoRechazoOferta(String motivoRechazoOferta) {
        this.motivoRechazoOferta = motivoRechazoOferta;
    }

    public boolean isBanderaMoverProceso() {
        return banderaMoverProceso;
    }

    public void setBanderaMoverProceso(boolean banderaMoverProceso) {
        this.banderaMoverProceso = banderaMoverProceso;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public double getCalculoPorcentaje() {
        return calculoPorcentaje;
    }

    public void setCalculoPorcentaje(double calculoPorcentaje) {
        this.calculoPorcentaje = calculoPorcentaje;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public boolean isBanderaVisorVisible() {
        return banderaVisorVisible;
    }

    public void setBanderaVisorVisible(boolean banderaVisorVisible) {
        this.banderaVisorVisible = banderaVisorVisible;
    }

    @PostConstruct
    public void init() {

        LOGGER.debug("on realizacionControlCalidadOferta init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.extraerIdControlCalidadDelArbol();

        this.inicializarVariablesVisorGIS();

        this.recolector = this.cargarDatosRecolector(this.idRecolector);

        this.cargarListaOfertasRecolector();

        // Se inicia el contador de ofertas por revisar
        this.contarOfertasPorRevisar();
        this.motivoRechazoOferta = "";
    }

    /**
     * Inicializa las variables requeridas por el VISOR GIS
     *
     * @author rodrigo.hernandez
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        // se pone en of para que muestre la capa del mapa de ofertas
        // inmobiliarias
        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        // no se pone en of porque en este caso de uso no hay permiso para
        // edicion
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }

    /**
     * Método para extraer los datos requeridos por el caso de uso que se reciben del process
     *
     * @author christian.rodriguez
     */
    private void extraerIdControlCalidadDelArbol() {

        this.bpmActivityInstance = this.tareasPendientesMB.getInstanciaSeleccionada();

        this.controlCalidadId = Long.valueOf(this.bpmActivityInstance.getIdCorrelacion());

        this.controlCalidad = this.getAvaluosService().obtenerControlCalidadPorId(
            this.controlCalidadId);

        this.regionSeleccionada = this.getAvaluosService().buscarRegionCapturaOfertaPorId(
            Long.valueOf(this.bpmActivityInstance.getIdRegion()));

        this.idRecolector = this.regionSeleccionada.getAsignadoRecolectorId();
    }

    /**
     * Método para cargar datos del recolector
     *
     * @author rodrigo.hernandez
     */
    private UsuarioDTO cargarDatosRecolector(String idRecolector) {

        UsuarioDTO usuario = this.getGeneralesService().getCacheUsuario(idRecolector);
        return usuario;
    }

    /**
     * Método para cargar la lista de ofertas de un recolector para realizar el control de calidad
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaOfertasRecolector() {
        this.listaOfertasRecolector = this.getAvaluosService()
            .buscarOfertasSeleccionadasPorIdRecolectorIdControlCalidadIdRegion(
                this.idRecolector, this.controlCalidadId, this.regionSeleccionada.getId());
    }

    /**
     * Método cargar datos de la oferta seleccionada
     *
     * @author rodrigo.hernandez
     */
    public void cargarDatosOfertaSeleccionada() {
        this.cargarListaObservacionesDeOferta();
        this.cargarOfertaInmobiliaria();
        this.cargarDetallesOferta();
    }

    /**
     * Método para actualizar contador de ofertas por revisar
     *
     * @author rodrigo.hernandez
     */
    private void contarOfertasPorRevisar() {

        this.contadorOfertasPorRevisar = 0;

        for (ControlCalidadOferta oferta : this.listaOfertasRecolector) {
            if (oferta.getAprobado() == null) {
                this.contadorOfertasPorRevisar++;
            }
        }

        if (this.contadorOfertasPorRevisar == 0) {
            this.banderaMoverProceso = true;
        }
    }

    /**
     * Método para cargar la lista de observaciones hechas previamente para una oferta
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaObservacionesDeOferta() {
        if (this.ofertaSeleccionada != null) {

            // Se realiza la carga de mensajes de observacion asociadas a la
            // oferta
            this.listaObservacionesOferta = getAvaluosService()
                .buscarObservacionesPorControlCalidadOfertaId(this.ofertaSeleccionada.getId());
        } else {
            this.listaObservacionesOferta = new ArrayList<ControlCalidadOfertaOb>();
        }
    }

    private void cargarDetallesOferta() {
        this.detalleOfertaInmobiliariaMB.setCurrentOferta(this.ofertaSeleccionada
            .getOfertaInmobiliariaId());
    }

    /**
     * Método para guardar las observaciones realizadas en el control de calidad sobre una oferta
     * revisada
     *
     * @author rodrigo.hernandez
     */
    public void guardarObservacionesControlCalidadOferta() {

        this.observacionOferta = null;

        // Valida si la oferta fue rechazada
        if (this.banderaOfertaRechazada) {
            // Se crea el mensaje de observación
            this.observacionOferta = new ControlCalidadOfertaOb();
            this.observacionOferta.setFecha(Calendar.getInstance().getTime());
            this.observacionOferta.setFechaLog(Calendar.getInstance().getTime());
            this.observacionOferta.setUsuario(this.usuario.getLogin());
            this.observacionOferta.setUsuarioLog(this.usuario.getLogin());
            this.observacionOferta.setMotivo(this.motivoRechazoOferta);

            // Se actualiza el estado de la observacion de la oferta
            // seleccionada como NO APROBADA
            this.observacionOferta.setAprobado(ESiNo.NO.getCodigo());

            // Se actualiza el estado de la oferta seleccionada como NO
            // APROBADA
            this.ofertaSeleccionada.setAprobado(ESiNo.NO.getCodigo());

            this.observacionOferta.setControlCalidadOferta(this.ofertaSeleccionada);

        } else {
            // Se actualiza el estado de la oferta seleccionada como APROBADA
            this.ofertaSeleccionada.setAprobado(ESiNo.SI.getCodigo());

        }

        this.getAvaluosService().guardarActualizarControlCalidadOferta(this.observacionOferta,
            this.ofertaSeleccionada);
        this.reiniciarValoresPanelObservaciones();
        this.contarOfertasPorRevisar();

    }

    /**
     * Método para cargar los numeros prediales de una oferta inmobiliaria
     *
     * @author rodrigo.hernandez
     */
    private void cargarOfertaInmobiliaria() {

        // Valida que no se haya cargado previamente la oferta inmobiliaria
        if (this.ofertaSeleccionada.getOfertaInmobiliariaId() == null) {
            this.ofertaSeleccionada.setOfertaInmobiliariaId(this.getAvaluosService()
                .buscarOfertaInmobiliariaPorId(
                    this.ofertaSeleccionada.getOfertaInmobiliariaId().getId()));
        }

    }

    /**
     * Método para crear la cadena de numeros prediales para el visor
     */
    public void crearParametroVisor() {
        this.prediosParaZoom = "";

        List<OfertaPredio> predios = this.ofertaSeleccionada.getOfertaInmobiliariaId()
            .getOfertaPredios();

        if (!predios.isEmpty()) {

            for (int contador = 0; contador < predios.size(); contador++) {
                if (contador != 0) {
                    this.prediosParaZoom = this.prediosParaZoom + "," +
                         predios.get(contador).getNumeroPredial();
                } else {
                    this.prediosParaZoom = this.prediosParaZoom +
                         predios.get(contador).getNumeroPredial();
                }
            }

        }

        this.activarBanderaVisor();
    }

    /**
     * Método para activar o desactivar bandera que indica si el visor es visible o no
     */
    public void activarBanderaVisor() {

        String codigoMunicipio = this.ofertaSeleccionada.getOfertaInmobiliariaId().getMunicipio()
            .getCodigo();

        boolean banderaMunicipioDescentralizado = this.getGeneralesService()
            .municipioPerteneceACatastroDescentralizado(codigoMunicipio);

        if (banderaMunicipioDescentralizado) {
            if (this.prediosParaZoom.isEmpty()) {
                this.banderaVisorVisible = false;
            } else {
                this.banderaVisorVisible = true;
            }
        } else {
            this.banderaVisorVisible = false;
        }

    }

    /**
     * Método para cargar visor con datos de la oferta
     */
    public void cargarMensajeErrorVisor() {
        RequestContext context = RequestContext.getCurrentInstance();
        String mensaje = "La oferta no contiene información espacial para mostrar";
        this.addMensajeError(mensaje);
        context.addCallbackParam("error", "error");
    }

    /**
     * Método para resetear la pantalla de ingreso de observaciones
     *
     * @author rodrigo.hernandez
     */
    public void reiniciarValoresPanelObservaciones() {
        this.banderaOfertaRechazada = false;
        this.motivoRechazoOferta = "";
    }

    /**
     * Método que se invoca al cerrar el panel de observaciones
     *
     * @param event
     */
    public void cerrarDialogObservaciones(org.primefaces.event.CloseEvent event) {
        this.reiniciarValoresPanelObservaciones();
    }

    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar
     *
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBeans();
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;

    }

    // ------------------------------- Metodos para integración con process
    // -------------------------------
    /**
     * Método para Mover Proceso
     *
     * @author rodrigo.hernandez
     */
    public void moverProceso() {

        this.calcularPorcentajeAprobacion();
        this.calcularEstadoSubMuestra();

        this.doDatabaseStatesUpdate();

        this.forwardProcess();
    }

    /**
     * Método para calcular porcentaje de aprobacion
     *
     * @author rodrigo.hernandez
     */
    private void calcularPorcentajeAprobacion() {
        int contadorOfertasAprobadas = 0;

        // Se cuentan las ofertas aprobadas
        for (ControlCalidadOferta oferta : this.listaOfertasRecolector) {
            if (oferta.getAprobado().equals(ESiNo.SI.getCodigo())) {
                contadorOfertasAprobadas++;
            }
        }

        // Se calcula el porcentaje de aprobacion
        this.calculoPorcentaje = (double) contadorOfertasAprobadas /
             this.listaOfertasRecolector.size();
    }

    /**
     * Dependiendo del porcentaje de aprobación se determina el estado de aprobación de la muestra
     *
     * @author christian.rodriguez
     */
    private void calcularEstadoSubMuestra() {

        if (this.calculoPorcentaje < PORCENTAJE_APROBACION_CONDICIONAL) {
            this.estadoSubMuestra = EOfertaEstado.RECHAZADA.getCodigo();
        } else if (this.calculoPorcentaje > PORCENTAJE_APROBACION_CONDICIONAL &&
             this.calculoPorcentaje < PORCENTAJE_APROBACION) {
            this.estadoSubMuestra = EOfertaEstado.APROBACION_CONDICIONADA.getCodigo();
        } else {
            this.estadoSubMuestra = EOfertaEstado.APROBADA.getCodigo();
        }
    }

    /**
     * Actualiza el estado de las ofertas inmobiliarias según el resultado del control de cálidad
     *
     * @author christian.rodriguez
     */
    private void doDatabaseStatesUpdate() {

        List<ControlCalidadOferta> listaOfertasControlCalidadRecolector =
            new ArrayList<ControlCalidadOferta>();

        LOGGER.debug(String.valueOf(this.calculoPorcentaje));

        // Carga todas las ofertas del recolector
        listaOfertasControlCalidadRecolector = getAvaluosService()
            .buscarOfertasPorIdRecolectorEIdControlCalidad(this.idRecolector,
                this.controlCalidadId);

        // Actualización del estado de las ofertas
        this.getAvaluosService().moverProcesoControlCalidadOferta(
            listaOfertasControlCalidadRecolector, estadoSubMuestra);

        // Finalización de la region de captura
        if (!this.estadoSubMuestra.equals(EOfertaEstado.APROBACION_CONDICIONADA.getCodigo())) {

            this.regionSeleccionada.setEstado(EEstadoRegionCapturaOferta.FINALIZADA.getEstado());
            this.getAvaluosService().guardarActualizarRegionCapturaOferta(this.regionSeleccionada);
        }

    }

    /**
     * Mueve el proceso a la siguiente transición (Modificar ofertas devueltas)
     *
     * @author christian.rodriguez
     */
    private void forwardProcess() {

        List<UsuarioDTO> recolector = new ArrayList<UsuarioDTO>();
        recolector.add(this.recolector);

        String transicion;
        if (this.estadoSubMuestra.equals(EOfertaEstado.APROBADA.getCodigo())) {
            transicion = ProcesoDeControlCalidad.ACT_VALIDACION_APROBAR_OFERTAS;
        } else if (this.estadoSubMuestra.equals(EOfertaEstado.APROBACION_CONDICIONADA.getCodigo())) {
            transicion = ProcesoDeControlCalidad.ACT_CORRECCION_CORREGIR_OFERTAS;
        } else {
            transicion = ProcesoDeControlCalidad.ACT_VALIDACION_BORRAR_OFERTAS;
        }

        co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad controlCalidadBPM =
            this
                .crearObjetoDeNegocio(recolector, transicion);

        this.moverActividad(transicion, controlCalidadBPM);
    }

    /**
     * Método encargado de avanzar la activdad
     *
     * @author christian.rodriguez
     * @param transicion transición destino
     * @param controlCalidadBPM objeto de negocio con los datos de la actividad a avanzar
     */
    private void moverActividad(
        String transicion,
        co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad controlCalidadBPM) {

        String idActividad = this.bpmActivityInstance.getId();
        try {

            this.getProcesosService().avanzarActividad(idActividad, controlCalidadBPM);

            this.addMensajeInfo("Control de calidad finalizado");
            this.banderaDeshabilitarBotones = true;

        } catch (Exception ex) {

            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError("No se pudo finalizar el control de calidad: " + ex.getMessage());
            context.addCallbackParam("error", "error");

            LOGGER.error(
                "Error moviendo actividad con id " + idActividad + " a la " + "actividad " +
                 transicion + " : " + ex.getMessage());
        }
    }

    /**
     * crea uhn objeto de negocio de tipo ControlCalidad con los datos necesarios para avanzar el
     * proceso
     *
     * @author christian.rodriguez
     * @param recolector usuario destino, puede ser nulo
     * @param transicion transicion de destino
     * @return objeto de negocio de tipo ControlCalidad
     */
    private co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad crearObjetoDeNegocio(
        List<UsuarioDTO> recolector, String transicion) {

        Departamento departamento;
        Municipio municipio;
        DivisionAdministrativa departamentoBPM, municipioBPM;

        departamento = this.regionSeleccionada.getAreaCapturaOferta().getDepartamento();
        municipio = this.regionSeleccionada.getAreaCapturaOferta().getMunicipio();

        departamentoBPM = new DivisionAdministrativa(departamento.getCodigo(),
            departamento.getNombre());
        municipioBPM = new DivisionAdministrativa(municipio.getCodigo(), municipio.getNombre());

        co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad controlCalidadBPM =
            new co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.ControlCalidad();

        controlCalidadBPM.setIdentificador(this.regionSeleccionada.getId());
        controlCalidadBPM.setIdRegion(String.valueOf(this.regionSeleccionada.getId()));
        controlCalidadBPM.setIdCorrelacion(this.controlCalidadId);
        controlCalidadBPM.setDepartamento(departamentoBPM);
        controlCalidadBPM.setMunicipio(municipioBPM);
        controlCalidadBPM.setTransicion(transicion);
        controlCalidadBPM.setUsuarios(recolector);
        controlCalidadBPM.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        controlCalidadBPM.setFuncionario(this.usuario.getNombreCompleto());

        return controlCalidadBPM;
    }

}
