package co.gov.igac.snc.web.mb.avaluos.administracion;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Profesion;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Managed bean del cu Administrar Información de Avaluadores
 *
 * @author pedro.garcia
 * @cu CU-SA-AC-035
 */
@Component("adminInfoAvaluadoresExternos")
@Scope("session")
public class AdminInformacionAvaluadoresExternosMB extends SNCManagedBean {

    private static final long serialVersionUID = 2734396405771696317L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdminInformacionAvaluadoresExternosMB.class);

    /**
     * ManagedBean del caso de uso 55
     */
    private ConsultaContratosAnterioresAvaluadorExternoMB consultaContratosAnterioresMB;

    /**
     * ManagedBean del caso de uso 51
     */
    private AdminInfoContratoActivoAvaluadorMB adminInfoContratoActivoAvaluadorMB;

    /**
     * mb para el cc de selección de departamento y municipio
     */
    @Autowired
    private CombosDeptosMunisMB combosDeptosMunis;

    private FiltroDatosConsultaProfesionalAvaluos datosConsultaAvaluador;

    /**
     * lista de territoriales para el dato de búsqueda
     */
    private ArrayList<SelectItem> territorialesItemList;

    /**
     * lista de profesiones para el dato de búsqueda
     */
    private ArrayList<SelectItem> profesionesItemList;

    /**
     * lista con los resultados de la búsqueda de avaluadores externos
     */
    private ArrayList<VProfesionalAvaluosContrato> listaAvaluadoresExternos;

    /**
     * guarda el registro seleccionado de la tabla de resultados de la búsqueda
     */
    private VProfesionalAvaluosContrato selectedAvaluador;

    /**
     * guarda los datos del avaluador que se consulta y -posiblemente- se modifica
     */
    private ProfesionalAvaluo currentAvaluador;

    //------------------   banderas   -----
    /**
     * indica si se muestra el panel de resultados de la búsqueda
     */
    private boolean verPanelResultados;

//----------------------------Métodos SET y GET----------------------------
    public CombosDeptosMunisMB getCombosDeptosMunis() {
        return this.combosDeptosMunis;
    }

    public void setCombosDeptosMunis(CombosDeptosMunisMB combosDeptosMunis) {
        this.combosDeptosMunis = combosDeptosMunis;
    }

    public ProfesionalAvaluo getCurrentAvaluador() {
        return this.currentAvaluador;
    }

    public void setCurrentAvaluador(ProfesionalAvaluo currentAvaluador) {
        this.currentAvaluador = currentAvaluador;
    }

    public VProfesionalAvaluosContrato getSelectedAvaluador() {
        return this.selectedAvaluador;
    }

    public void setSelectedAvaluador(VProfesionalAvaluosContrato selectedAvaluador) {
        this.selectedAvaluador = selectedAvaluador;
    }

    public ArrayList<VProfesionalAvaluosContrato> getListaAvaluadoresExternos() {
        return this.listaAvaluadoresExternos;
    }

    public void setListaAvaluadoresExternos(
        ArrayList<VProfesionalAvaluosContrato> listaAvaluadoresExternos) {
        this.listaAvaluadoresExternos = listaAvaluadoresExternos;
    }

    public ArrayList<SelectItem> getProfesionesItemList() {
        return this.profesionesItemList;
    }

    public void setProfesionesItemList(ArrayList<SelectItem> profesionesItemList) {
        this.profesionesItemList = profesionesItemList;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return this.territorialesItemList;
    }

    public void setTerritorialesItemList(ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public FiltroDatosConsultaProfesionalAvaluos getDatosConsultaAvaluador() {
        return this.datosConsultaAvaluador;
    }

    public void setDatosConsultaAvaluador(
        FiltroDatosConsultaProfesionalAvaluos datosConsultaAvaluador) {
        this.datosConsultaAvaluador = datosConsultaAvaluador;
    }

    public boolean isVerPanelResultados() {
        return this.verPanelResultados;
    }

    public void setVerPanelResultados(boolean verPanelResultados) {
        this.verPanelResultados = verPanelResultados;
    }

//---------------------------------    Métodos      ---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdminInformacionAvaluadoresExternosMB init");

        setTerritorialesItemList();
        setProfesionesItemList();

        this.datosConsultaAvaluador = new FiltroDatosConsultaProfesionalAvaluos();

    }
//--------------------------------------------------------------------------------------------------

    public String cerrarVentanaCU() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón de opción "Consultar datos del avaluador" de la tabla de resultados de la
     * búsqueda
     */
    public void consultarAvaluador() {

        this.currentAvaluador = this.getAvaluosService().obtenerProfesionalAvaluoPorId(
            this.selectedAvaluador.getProfesionalAvaluosId());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón de opción "Modificar datos del avaluador" de la tabla de resultados de la
     * búsqueda. Inicializa las variables del mb que maneja los combos de territoriales, deptos y
     * munis
     */
    public void initModificarAvaluador() {
        consultarAvaluador();

        this.combosDeptosMunis.updateTerritorialesItemList();
        this.combosDeptosMunis.setSelectedDepartamentoCod(
            this.currentAvaluador.getDireccionDepartamento().getCodigo());

        this.combosDeptosMunis.updateMunicipiosItemList();
        this.combosDeptosMunis.setSelectedMunicipioCod(
            this.currentAvaluador.getDireccionMunicipio().getCodigo());
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Arma la lista de select items para el combo de territoriales
     *
     * @author pedro.garcia
     */
    private void setTerritorialesItemList() {

        List<EstructuraOrganizacional> eoList;
        this.territorialesItemList = new ArrayList<SelectItem>();

        eoList = this.getGeneralesService().getCacheTerritoriales();

        this.territorialesItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
        if (eoList != null && !eoList.isEmpty()) {
            for (EstructuraOrganizacional territorial : eoList) {
                this.territorialesItemList.add(
                    new SelectItem(territorial.getCodigo(),
                        territorial.getCodigo() + "-" + territorial.getNombre()));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de select items para el combo de profesiones
     *
     * @author pedro.garcia
     */
    private void setProfesionesItemList() {

        List<Profesion> profList;
        this.profesionesItemList = new ArrayList<SelectItem>();

        profList = this.getGeneralesService().getCacheProfesiones();

        this.profesionesItemList.add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
        if (profList != null && !profList.isEmpty()) {
            for (Profesion profesion : profList) {
                this.profesionesItemList.add(
                    new SelectItem(profesion.getId(),
                        profesion.getId() + "-" + profesion.getNombre()));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "buscar" en la página principal del cu
     *
     * @author pedro.garcia
     */
    public void buscarAvaluadoresExternos() {

        this.verPanelResultados = true;

        this.listaAvaluadoresExternos = (ArrayList<VProfesionalAvaluosContrato>) this.
            getAvaluosService().buscarAvaluadoresPorFiltro(this.datosConsultaAvaluador);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * método que se ejecuta al aceptar la acción de modificación de la información de un avaluador
     */
    public void modificarInfoAvaluador() {

        this.currentAvaluador.setDireccionDepartamento(
            this.combosDeptosMunis.getSelectedDepartamento());
        this.currentAvaluador.setDireccionMunicipio(this.combosDeptosMunis.getSelectedMunicipio());

        this.getAvaluosService().actualizarProfesionalAvaluos(this.currentAvaluador);

        //D: se vuelva a hacer la búsqueda para refrescar la tabla
        this.listaAvaluadoresExternos = (ArrayList<VProfesionalAvaluosContrato>) this.
            getAvaluosService().buscarAvaluadoresPorFiltro(this.datosConsultaAvaluador);

        this.addMensajeInfo("Datos modificados.");

    }
//--------------------------------------------------------------------------------------------------

    /**
     * método dummy para obligar a que en el action del botón "aceptar" página de modificación de
     * datos del avaluador externo se venga hasta el MB luego de validar los campos del formulario
     */
    public void dummy() {
        FacesContext fc = FacesContext.getCurrentInstance();

        if (fc.isValidationFailed()) {
            LOGGER.debug("validation failed");
        }

    }

    // --------------------------------------------------------------------------------------
    /**
     * Listener que que carga los contratos anteriores del avaluador. Ejecuta el caso de uso 55 de
     * avalúos
     *
     * @author christian.rodriguez
     */
    public void cargarConsultaContratosAnterioresAvaluador() {

        this.consultaContratosAnterioresMB =
            (ConsultaContratosAnterioresAvaluadorExternoMB) UtilidadesWeb
                .getManagedBean("consultaContratosAnterioresAvaluador");
        this.consultaContratosAnterioresMB
            .cargarDesdeCU035(this.selectedAvaluador);
    }

    // --------------------------------------------------------------------------------------
    /**
     * Listener que que carga la información del contrato activo para su administración. Ejecuta el
     * caso de uso 51 de avalúos
     *
     * @author christian.rodriguez
     */
    public void cargarAdministrarInfoContratoActivoAvaluador() {

        this.adminInfoContratoActivoAvaluadorMB = (AdminInfoContratoActivoAvaluadorMB) UtilidadesWeb
            .getManagedBean("adminInfoContratoActivoAv");
        this.adminInfoContratoActivoAvaluadorMB
            .cargarDesdeCU35(this.selectedAvaluador);
    }

//end of class
}
