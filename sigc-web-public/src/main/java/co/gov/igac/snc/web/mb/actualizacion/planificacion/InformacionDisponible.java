package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.Serializable;

import co.gov.igac.snc.persistence.entity.generales.Dominio;

/**
 * Esta clase representa la información de disponible por parte de la Subdirección de Geografía y
 * Cartografía y de la Subidrección de Agrología para el proceso de actualización. Nota:
 * probablemente deba ser un objeto envolvente (Wrapper) de los objetos InformacionBasicaCarto e
 * InformacionBasicaAgro.
 *
 * @author jamir.avila
 */
public class InformacionDisponible implements Serializable {

    /**
     * Identificador de versión por omisión.
     */
    private static final long serialVersionUID = 1L;

    /** Dominios con la información básica para la construcción de este objeto */
    private Dominio dominio;

    /**
     * Constructor por omisión.
     */
    public InformacionDisponible(Dominio dominio) {
        this.dominio = dominio;
    }

    public String getInformacionAsociada() {
        return dominio.getValor();
    }

    public String getDescripcion() {
        return dominio.getDescripcion();
    }

    public Dominio getDominio() {
        return dominio;
    }
}
