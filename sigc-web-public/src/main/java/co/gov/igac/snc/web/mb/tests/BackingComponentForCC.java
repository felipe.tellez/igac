/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tests;

import javax.el.MethodExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

/**
 * @see
 * http://stackoverflow.com/questions/6453842/jsf-2-how-can-i-add-an-ajax-listener-method-to-composite-component-interface/6454339#comment7579957_6454118
 *
 * @author
 *
 * esta clase es para probar la solución dada al problema de que no reconoce un método con
 * parámetros definido como atributo en un CC.
 *
 * el atributo value es el que se usa en la difinición del CC como valor del atributo componentType
 *
 * NO funcionó porque sigue saliendo el error de "wrong number of arguments"
 */
@FacesComponent(value = "testCC")
public class BackingComponentForCC extends UINamingContainer {
//public class BackingComponentForCC extends UIComponentBase implements NamingContainer {

    public void ajaxEventListener(AjaxBehaviorEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        MethodExpression ajaxEventListener = (MethodExpression) getAttributes().get(
            "ajaxEventListener");
        ajaxEventListener.invoke(context.getELContext(), new Object[]{event});
    }

}
