package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.swing.ImageIcon;

import org.apache.commons.vfs2.FileSystemException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.IModeloUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PFmConstruccionComponente;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.conservacion.PModeloConstruccionFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioServidumbre;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PReferenciaCartografica;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.TipificacionUnidadConst;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionAnexo;
import co.gov.igac.snc.persistence.entity.formacion.CalificacionConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.TablaTerreno;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramite;
import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteSeccionDatos;
import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.util.EComisionTramiteResultAv;
import co.gov.igac.snc.persistence.util.EComponenteConstruccionCom;
import co.gov.igac.snc.persistence.util.EDatoRectificarNombre;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstadoPFichaMatrizTorreEtapas;
import co.gov.igac.snc.persistence.util.EFichaMatrizEstado;
import co.gov.igac.snc.persistence.util.EFotoTipo;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPersonaPredioPropiedadTipoTitulo;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipificacion;
import co.gov.igac.snc.persistence.util.EUnidadConstruccionTipoCalificacion;
import co.gov.igac.snc.persistence.util.EUnidadPisoUbicacion;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.persistence.util.EUnidadTipoDominio;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.ETipoSubprocesoConservacion;
import co.gov.igac.snc.util.FiltroDatosConsultaTramite;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.ValidacionProyeccionDelTramiteDTO;
import co.gov.igac.snc.util.constantes.ConstantesResolucionesTramites;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.AsociarDocumentosATramiteExistenteMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.CambioPropietarioMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.EntradaProyeccionMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.FichaMatrizMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.GestionDetalleAvaluoMB;
import co.gov.igac.snc.web.mb.conservacion.proyeccion.JustificacionProyeccionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.tramite.gestion.RecuperacionTramitesMB;
import co.gov.igac.snc.web.mb.util.ValidacionProyeccionUtil;
import co.gov.igac.snc.web.util.CalificacionConstruccionTreeNode;
import co.gov.igac.snc.web.util.ECambioZonaHomogenea;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ESeccionEjecucion;
import co.gov.igac.snc.web.util.HistoricoProyeccionViaGubernativaDTO;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.web.util.ValidadorEnglobeVirtual;
import co.gov.igac.snc.web.util.ValidadorAvaluoAreaRectificacion;
import co.gov.igac.snc.web.util.ValidarCompYRect;
import java.text.DecimalFormat;

/**
 *
 * @author fredy.wilches
 *
 */
@Component("proyectarConservacion")
@Scope("session")
public class ProyectarConservacionMB extends SNCManagedBean {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 4928784586268609428L;

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProyectarConservacionMB.class);

	/** ---------------------------------- */
	/** ----------- SERVICIOS ------------ */
	/** ---------------------------------- */
	@Autowired
	private TareasPendientesMB tareasPendientesMB;

	@Autowired
	private RectificacionComplementacionMB rcMB;

	/**
	 * Utilidad para la verificación y validación de la proyección de un
	 * {@link Tramite}
	 */
	private ValidacionProyeccionUtil validacionProyeccionService = ValidacionProyeccionUtil.getInstance();

	/** ---------------------------------- */
	/** ----------- VARIABLES ------------ */
	/** ---------------------------------- */
	/**
	 * Actividad de procesos
	 */
	private Actividad actividad;

	private static final String separadorCarpeta = System.getProperty("file.separator");

	private static final SimpleDateFormat formatoFechaTituloResolucion = new SimpleDateFormat(
			Constantes.FORMATO_FECHA_TITULO_RESOLUCION);

	/**
	 * Contiene la ruta de la imagen por defecto cuando no existen fotografias
	 * asociadas
	 */
	private String rutaWebImagenBase = "/imagenesBase/uploadIcon.png";

	private Tramite tramite;
	private Predio predioOrigen;
	private PPredio predioSeleccionado;
	private PPredioAvaluoCatastral avaluo = new PPredioAvaluoCatastral();

	/**
	 * lista con todos los items del combo de tipos de predio
	 */
	private List<SelectItem> tiposPredio;
	private List<SelectItem> tiposPredioEtapas;

	/**
	 * lista con todos los items del combo de destino predio
	 */
	private List<SelectItem> tiposDestinoPredio;

	/**
	 * lista con todos los items del combo de tipos predio servidumbre
	 */
	private List<SelectItem> tiposPredioServidumbre;

	private EOrden ordenPaises = EOrden.CODIGO;
	private EOrden ordenDepartamentos = EOrden.CODIGO;
	private EOrden ordenMunicipios = EOrden.CODIGO;

	/**
	 * Determina si se a realizar un desenglobe de mejora
	 */
	private boolean banderaDesenglobeMejora;

	/**
	 * Determina si los predios a englobar son solo mejoras
	 */
	private boolean banderaSoloMejorasEnglobe;
	/**
	 * Determina si los predios a englobar son mejoras y predios de terreno
	 */
	private boolean banderaMejorasTerrenoEnglobe;

	/**
	 * Determina si se activa o no el checkbox de aceptar la direccion normalizada
	 */
	private boolean banderaAceptarDireccionNormalizada;
	/**
	 * array con todos los predio servidumbre seleccionados para eliminar
	 */
	private PPredioServidumbre selectedPredioServidumbre;

	/**
	 * variable que contiene al predio que sea seleccionado en la tabla
	 */
	private PPredioDireccion predioDireccionSeleccionado;

	/**
	 * variable que contiene la servidumbre que se va a adicionar
	 */
	private PPredioServidumbre servidumbreAdicional;

	/**
	 * variable que contiene la opcion que se desea eliminar si predio o servidumbre
	 */
	private String opcionEliminar;

	/**
	 * variable que se usa para definir la configuración de la barra de tabs.
	 * Determina cuáles se muestran según el tipo de trámite
	 */
	private TramiteSeccionDatos configuracion;

	private PUnidadConstruccion unidadConstruccionSeleccionada;

	private PPersonaPredio propietarioSeleccionado;

	private Integer tab;

	private UsuarioDTO usuario;

	private UsuarioDTO digitalizador;

	/**
	 * lista de usuarios con el rol de responsable de conservación
	 */
	private List<UsuarioDTO> responsablesConservacion;

	private boolean banderaConstruccionConvencional;

	/**
	 * árbol que se arma por defecto
	 */
	private TreeNode treeComponentesConstruccion;

	/**
	 * los tres árboles que se arman para los tipos de calificación de la unidad de
	 * construcción Hubo que manejar 3 aparte y que se creen al principio para que
	 * no se generen errores por updates
	 */
	private TreeNode treeComponentesConstruccion1;
	private TreeNode treeComponentesConstruccion2;
	private TreeNode treeComponentesConstruccion3;

	/**
	 * lista con todos los items del combo de usos de construcción
	 */
	private List<SelectItem> usosUnidadConstruccionItemList;

	/**
	 * uso de construcción seleccionado en el combo
	 */
	private Long selectedUsoUnidadConstruccionId;

	/**
	 * uso de construcción como objeto para obtener valores necesarios para otros
	 * campos
	 */
	private UsoConstruccion selectedUsoUnidadConstruccion;

	/**
	 * lista con todos los posibles usos de construcción (tabla USO_CONSTRUCCION)
	 */
	private List<VUsoConstruccionZona> usosConstruccionList;

	/**
	 * tipificacion de la unidad de construcción seleccionada
	 */
	// private String tipificacionUnidadConstruccion;
	/**
	 * identifica si la unidad de construcción que se va a guardar es nueva
	 */
	private boolean esNuevaUnidadConstruccion;

	/**
	 * lista con los items del combo "descripción construccion"
	 */
	private ArrayList<SelectItem> descripcionConstruccionAnexoItemList;

	/**
	 * calificacion anexo seleccionado
	 */
	private CalificacionAnexo selectedCalificacionAnexo;

	/**
	 * lo que en la interfaz se llama "tipo", pero en realidad corresponde a los
	 * puntos de la calificación anexo
	 */
	private int tipoDescripcionConstruccion;

	/**
	 * determina visualizar boton aceptar
	 */
	private boolean banderaBotonAceptarCargarFoto;

	/**
	 * determina visualizar boton aceptar
	 */
	private boolean errorConfirmarDatosCargarFotografia;

	/**
	 * Contiene el archivo de imagen
	 */
	private File archivoResultadoFoto;

	/**
	 * Contiene los datos de la PFoto
	 */
	private PFoto currentPFoto;

	/**
	 * Permite al generación de variables aleatorias
	 */
	private Random generadorNumeroAleatorioNombreImagen;

	/**
	 * determina si existe una imagen para visualizar
	 */
	private boolean existeImagen;

	private CalificacionConstruccionTreeNode currentCalificacionConstruccionNode;

	private String mensajeFotos;

	/**
	 * Lista en la que se guardan los PFotos que se deben insertar en la BD al
	 * guardar la unidad de construcción
	 */
	private List<PFoto> pFotosList;

	/**
	 * lista en la que se guardan las PFotos que se deben eliminar porque fueron
	 * reemplazadas al modificar una unidad de construcción
	 */
	private List<PFoto> listaPFotosABorrar;

	/**
	 * Lista en la que se guardan las PModeloConstruccionFoto que se deben eliminar
	 * porque fueron reemplazadas al modificar una unidad de un modelo de
	 * construcción.
	 */
	private List<PModeloConstruccionFoto> listaPModeloConstFotoABorrar;

	/**
	 * variable usada para saber el estado del trámite.
	 */
	private TramiteEstado tramiteEstado;

	/**
	 * Avalúos catastrales aplicados al predio.
	 */
	private List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastrales;

	/**
	 * Observaciones de la pantalla de inscripciones y decretos.
	 */
	private String observacionesInscripcionesYDecretos;

	/**
	 * lista con todos los titulos de las resoluciones plantilla texto motivo
	 */
	private List<SelectItem> titulosResolucion;

	/**
	 * Variable usada para almacenar el id de un modelo de resolución seleccionado
	 */
	private String idModeloResolucionSeleccionado;

	/**
	 * variable que guarda el modelo resoluciòn que se seleccione
	 */
	private ModeloResolucion modeloResolucionSeleccionado;

	/**
	 * Variable que guarda el texto motivo en tramiteTextoResolucion
	 */
	private TramiteTextoResolucion tramiteTextoResolucion;

	/**
	 * Lista de las plantillas de ModeloResolucion segun el tramite
	 */
	private List<ModeloResolucion> modeloResolucionList;

	/**
	 * Lista de la plantilla de ModeloResolucion sin plantilla
	 */
	private List<ModeloResolucion> modeloSinPlantilla;

	/**
	 * Utilidad para la creación del reporte
	 */
	private ReportesUtil reportsService = ReportesUtil.getInstance();

	/**
	 * Variable usada para guardar una direccion nueva
	 */
	private String direccion;

	/**
	 * variable que guarda el código postal de una dirección nueva
	 */
	private String codigoPostalDireccion;

	/**
	 * Variable usada para saber si una direccion nueva es principal
	 */
	private Boolean principalBool;
	/**
	 * Variable usada para guardar la version normalizada de una direccion nueva
	 */
	private String direccionNormalizada;
	/**
	 * Variable usada para saber si se acepta la version normalizada de una
	 * direccion nueva
	 */
	private Boolean normalizadaBool;
	/**
	 * Variable usada para distinguir si una direccion es nueva o esta siendo
	 * modificada
	 */
	private Boolean editModeBool = false;

	/**
	 * Variable usada para almacenar el TramiteDocumento del informe del texto
	 * resolucion generado.
	 */
	private TramiteDocumento informeResolucionTramDoc;

	/**
	 * Variable usada para almacenar el documento del informe del texto resolucion.
	 */
	private Documento informeResolucionDoc;

	private boolean tipoDominioComun;

	/**
	 * Variable usada para almacenar el tipo de modelo de una construcción
	 */
	private String tipoModelo;

	/**
	 * Variable usada para almacenar las direcciones del ppredio
	 */
	private List<PPredioDireccion> predioSeleccionadoDirecciones;

	/**
	 * Variable booleana usada al momento de visualizar los datos de una
	 * construcción como solo lectura o en su edición.
	 */
	private boolean datosConstruccionSoloLectura;

	/**
	 * Variable usada para almacenar una dirección de un ppredio
	 */
	private PPredioDireccion pPredioDireccionSeleccionado;

	/**
	 * Variable usada para almacenar las direcciones editadas
	 */
	private List<PPredioDireccion> direccionesEditadas;

	/**
	 * Variable usada como validadora de direcciones, cuando se quiere eliminar la
	 * ultima dirección o cuando se quiere eliminar la dirección principal.
	 */
	private boolean validacionDireccionBool;

	/**
	 * Listas de predios para mutaciones de segunda deseneglobe y englobe.
	 */
	private List<PPredio> pPrediosEnglobe;
	private List<PPredio> pPrediosDesenglobe;

	private List<PPredio> pPrediosTerceraRectificacionMasiva;

	private List<PPredio> pPrediosQuintaMasivo;

	private List<PPredio> pPrediosCancelacionMasivo;

	private List<PPersonaPredio> pPersonaPrediosCancelacionM;

	/**
	 * Variable que almacena el codigo del circulo registral seleccionado al momento
	 * de un registro de matrícula nueva.
	 */
	private String circuloRegistralSeleccionado;

	/**
	 * Variable que almacena en nuevo numero de registro de matricula.
	 */
	private String numeroRegistroInfoMatriculaNueva;

	/**
	 * Variable que almacena la fecha de registro de una matricula inmobiliaria.
	 */
	private Date fechaRegistro;

	/**
	 * Lista de circulos registrales del municipio.
	 */
	private List<SelectItem> itemListCirculosRegistrales = new ArrayList<SelectItem>();

	/**
	 * Variable que almacena una referencia cartografica seleccionada.
	 */
	private PReferenciaCartografica referenciaCartograficaSeleccionada;

	/**
	 * Lista de títulos y fechas de títulos para ciertos tipos de trámite
	 */
	private List<SelectItem> fechasTitulos = new ArrayList<SelectItem>();
	/**
	 * Lista de inscripcion para tramites de quinta con mejoras
	 */
	private List<SelectItem> fechasInscripcionMejoras = new ArrayList<SelectItem>();

	private boolean tramiteSinTitulos;

	/**
	 * Lista que registra las observaciones sobre un tramite
	 */
	private List<TramiteEstado> historicoObservaciones;

	/**
	 * Variable boolean para saber si se está trabajando con UnidadesConstruccion o
	 * con PFmModeloConstruccion.
	 */
	private boolean unidadModeloConstruccionBool;

	/**
	 * Lista de modelos de unidad de construcción de la ficha matriz.
	 */
	private List<PFichaMatrizModelo> listModelosUnidadDeConstruccion;

	/**
	 * Variable que almacena el modelo de unidad de contrucción seleccionado.
	 */
	private PFichaMatrizModelo modeloUnidadDeConstruccionSeleccionado;

	/**
	 * Variable para almacenar el tipo de modelo.
	 */
	private String tipoDeModelo;

	/**
	 * Lista de las PFmModeloConstruccion para un modelo de construcción
	 * seleccionado.
	 */
	private List<PFmModeloConstruccion> listUnidadesDelModeloDeConstruccion;

	/**
	 * Variable para almacenar la PFmModeloConstruccion seleccionada.
	 */
	private PFmModeloConstruccion unidadDelModeloDeConstruccionSeleccionada;

	/**
	 * Variable usada para saber si se está editando una unidad del modelo, o se
	 * está agregando una nueva.
	 */
	private boolean unidadDelModeloEditModeBool;

	/**
	 * Lista de PModeloConstruccionFoto asociadas a la unidad del modelo de
	 * construcción.
	 */
	private List<PModeloConstruccionFoto> listPFotosUnidadDelModelo;

	/**
	 * Foto que se está asociando al componente de la unidad del modelo.
	 */
	private PModeloConstruccionFoto currentPModeloConstruccionFoto;

	/**
	 * Arbol donde se muestran los componentes de un modelo de construcción.
	 */
	private TreeNode treeComponentesConstruccionParaModelo;

	private HashMap<ESeccionEjecucion, Boolean> validezSeccion;

	private CambioPropietarioMB propietariosMB;

	private JustificacionProyeccionMB justificacionesMB;

	private EntradaProyeccionMB entradaProyeccionMB;

	private FichaMatrizMB fichaMatrizMB;

	/**
	 * Lista de elementos calificación construcción.
	 */
	private List<CalificacionConstruccion> listaElementosCalificacionConstruccion;

	/**
	 * Variable usada para saber si se está modificando o adicionando un modelo de
	 * construcción.
	 */
	private boolean editModeloBool;

	/**
	 * Lista de selectItems con los paises.
	 */
	private List<SelectItem> paises;

	/**
	 * Unidades de construccion convencionales vigentes
	 */
	private List<UnidadConstruccion> unidadesConstruccionConvencional;

	/**
	 * Unidades de construccion no convencionales vigentes
	 */
	private List<UnidadConstruccion> unidadesConstruccionNoConvencional;
	/**
	 * Unidades de construccion convencionales vigentes asociadas a la ficha matriz
	 */
	private List<UnidadConstruccion> unidadesConstruccionConvencionalFM;

	/**
	 * Unidades de construccion no convencionales vigentes asociadas a la ficha
	 * matriz
	 */
	private List<UnidadConstruccion> unidadesConstruccionNoConvencionalFM;

	private List<PUnidadConstruccion> pUnidadConstruccionComunesNoConvencionalEnglobeVirtual;

	private List<PUnidadConstruccion> pUnidadConstruccionComunesConvencionalEnglobeVirtual;

	/**
	 * Unidades de construccion convencionales vigentes seleccionadas para asociar
	 */
	private UnidadConstruccion[] unidadesConstruccionConvencionalSeleccionadas;

	/**
	 * Unidades de construccion no convencionales vigentes seleccionadas para
	 * asociar
	 */
	private UnidadConstruccion[] unidadesConstruccionNoConvencionalSeleccionadas;
	/**
	 * Unidades de construccion convencionales vigentes de la ficha matriz
	 * seleccionadas para asociar
	 */
	private UnidadConstruccion[] unidadesConstruccionConvencionalSeleccionadasFM;

	/**
	 * Unidades de construccion no convencionales vigentes de la ficha matriz
	 * seleccionadas para asociar
	 */
	private UnidadConstruccion[] unidadesConstruccionNoConvencionalSeleccionadasFM;

	private PUnidadConstruccion[] pUnidadesConstruccionConvencionalSeleccionadasEV;

	private PUnidadConstruccion[] pUnidadesConstruccionNoConvencionalSeleccionadasEV;

	private boolean tablaExpandidaConstruccionesConvencionalesVigentes = false;

	private boolean tablaExpandidaConstruccionesConvencionalesNuevas = false;

	/**
	 * Variable que almacena la lista de circulos registrales.
	 */
	private List<CirculoRegistral> listCirculosRegistrales;

	/**
	 * bandera que permite determinar si se debe o no mostrar la fecha de título en
	 * la pantalla de gestionar inscripciones y decretos, lo muestra para mutación
	 * de primera, de segunda y de quinta nueva
	 */
	private boolean banderaMostrarFechaTitulo;

	/**
	 * bandera que permite determinar si se debe o no mostrar la fecha de
	 * inscripción catastral en la pantalla de gestionar inscripciones y decretos,
	 * lo muestra para mutación de tercera, cuarta, quinta omitida
	 */
	private boolean banderaMostrarFechaInscripcion;

	/**
	 * bandera que permite determinar si se debe o no ingresar la fecha de
	 * inscripción catastral en la pantalla de gestionar inscripciones y decretos,
	 * lo muestra para rectificaciones y modificaciones catastrales
	 */
	private boolean banderaIngresarFechaInscripcion;

	/**
	 * Variable usada para saber si se encontró o no una fecha de actualización para
	 * un predio de trámite de mutación quinta omitido
	 */
	private boolean actualizacionEncontradaBool;

	/**
	 * Dimensión para las construcciones no convencionales
	 */
	private Double areaNoConvencional;

	/**
	 * indica si en la proyección del trámite éste era uno de quinta omitido, no se
	 * encontró, y se creó unio nuevo.
	 */
	private boolean tramiteQuintaOmitidoPredioExistente;

	/**
	 * Variable para soportar el valor del checkbox del autoavaluo, porque el
	 * conversor no funcionó en uno de los sentidos
	 */
	@SuppressWarnings("unused")
	private boolean autoavaluo;

	/**
	 * alto y anchode las imágenes del predio
	 */
	private int anchoImagen;
	private int altoImagen;

	/**
	 * Variable usada para saber si se van a actualizar las áreas de la ficha
	 * matriz.
	 */
	private boolean actualizarAreasFichaMatrizBool;

	/**
	 * Ruta de la imagen al iniciar el tramite
	 *
	 * @return
	 */
	private String imagenAntes;

	/**
	 * Ruta de la imagen al finalizar el tramite
	 *
	 * @return
	 */
	private String imagenDespues;

	/**
	 * Variable usada en el método de actualizar ficha matriz como parametro si se
	 * quiere guardar las áreas de ficha matriz o únicamente cargarlas.
	 */
	private boolean guardarAreasBool;

	/**
	 * Variable booleana para activar o desactivar el botón de asociar las unidades
	 * de construcción de un modelo al predio seleccionado.
	 */
	private boolean activeAsociarUnidadesBool;

	/**
	 * Variable usada para almacenar la ficha matriz en mutaciones de englobe y
	 * desenglobe.
	 */
	private PFichaMatriz fichaMatrizSeleccionada;

	/**
	 * Variables usadas para setear el valor del nombre, matricula de la ficha
	 * matriz.
	 */
	private String matriculaPredioFichaMatriz;
	private String nombrePredioFichaMatriz;
	private CirculoRegistral circuloRegistralFichaMatriz;

	/**
	 * Lista de PModeloConstruccionFoto de la unidad del modelo de contrucción
	 * pendientes por guardar.
	 */
	private List<PModeloConstruccionFoto> listaPModeloConstruccionFotoAGuardar;

	/**
	 * Variable booleana usada para controlar la activación del botón validar
	 * guardar de proyección.
	 */
	private boolean activarValidarGuardarBool;

	/**
	 * Variable usada para almacenar el id de la calificacionAnexo tanto de unidades
	 * de construcción como de unidades del modelo de construcción.
	 */
	private Long calificacionAnexoIdSelected;

	/**
	 * Comision Trámite Dato asociada al trámite
	 */
	private List<ComisionTramiteDato> comisionesTramiteDato;

	/**
	 * Se presenta cuando la construcción a pesar de tener puntaje no tiene
	 * componentes
	 */
	private boolean informacionMigracionIncompleta;

	/**
	 * Variable usada para saber si en mutación segunda desenglobe, cuando existe
	 * una ficha matriz, ésta tiene asociados modelos o no.
	 */
	private boolean existenModelosBool;

	/**
	 * Lista de unidades de construcción convencional del predio base en mutación
	 * segunda desenglobe.
	 */
	private List<UnidadConstruccion> listUnidadesConstruccionConvencionalVigentes;

	/**
	 * Lista de unidades de construcción no convencionales del predio base en
	 * mutación segunda desenglobe.
	 */
	private List<UnidadConstruccion> listUnidadesConstruccionNoConvencionalVigentes;

	/**
	 * Lista de ValidacionProyeccionDelTramiteDTO que almacena las validaciones del
	 * trámite por sección y predio para mutación de segunda desenglobe.
	 */
	private List<ValidacionProyeccionDelTramiteDTO> listaValidacionesTramiteSegundaDesenglobe;

	/**
	 * Indica si se encontraron errores en el validar y guardar.
	 */
	private boolean validaGuardarEV;

	/**
	 * Indica si el predio se encuentra proyectado en otro trámite
	 */
	private boolean predioEnTramite;
	/**
	 * Puntaje de la construcción al iniciar la edición
	 */
	private int puntajeAnterior;

	/**
	 * Puntaje de la construcción al iniciar la edición
	 */
	private int puntajeTotal;

	/**
	 * Bandera que valida si el calculo de las unidades de terreno se realizó
	 * exitósamente
	 */
	private boolean valorM2zonaFlag;

	/**
	 * Bandera que valida sí el calculo de las zonas se realizó exitósamente
	 */
	private boolean calculozonaFlag;

	/**
	 * Variable booleana usada para desactivar el botón asociar construcciones
	 * cuando no existen unidades de construcción vigentes.
	 */
	private boolean existenConstruccionesVigentesBool;

	// --------------Variables para el reporte ----------------------------
	/**
	 * Objeto que contiene los datos del reporte de resoluciones
	 */
	private ReporteDTO reporteResoluciones;

	/**
	 * ruta local de la fotografía del anexo - o construcción no convencional- que
	 * se adiciona o se modifica
	 */
	private String rutaLocalFotoAnexo;

	/**
	 * ruta web temporal de la foto del anexo nuevo o modificado
	 */
	private String rutaWebTemporalFotoAnexo;

	/**
	 * rutas temporales (local y web) de la foto que se carga
	 */
	private String rutaTemporalFoto, rutaWebTemporalFoto;

	/**
	 * bandera que dice si la unidad de construcción que se está creando o
	 * modificando es un anexo
	 */
	private boolean isAnexoCurrentOrNewUnidadConstruccion;

	/**
	 * Variable booleana que indica si la unidad del modelo de construcción que se
	 * está creando o modificando es un anexo.
	 */
	private boolean isAnexoONuevaUnidadDelModelo;

	/**
	 * Variables gestion de digitalizadores
	 */
	private String digitalizadorAsignado;
	private boolean banderaDigitalizacionAsignada;

	/**
	 * Bandera para determinar si un predio de mejora tiene fecha de actualizaciòn
	 */
	private boolean banderaQuintaMejoraFechaAct;

	/**
	 * Variable {@link Boolean} para habilitar un mensaje de validación cuando se
	 * haya seleccionado una {@link }
	 */
	private boolean validarCalificacionComponenteBool;

	// ------------------------------------------------------------------- //
	// --------------Variables caso de uso liquidar avalúos de trámites
	/**
	 * Variable que almacena si el cálculo de liquidación de avalúos se hace en
	 * forma retroactiva
	 */
	private String calculoAvaluoEnFormaRetroactiva;

	/**
	 * Variable que almacena si el cálculo de liquidación de avalúos se hace por un
	 * rango de años
	 */
	private boolean calculoAvaluoPorRangoDeAnios;

	/**
	 * Variable que almacena si mostar o no el cálculo de liquidación de avalúos por
	 * un rango de años
	 */
	private boolean mostrarCalculoAvaluoPorRangoDeAnios;

	/**
	 * Variable que almacena si mostar o no el cálculo de liquidación de avalúos por
	 * año
	 */
	private boolean mostrarCalculoAvaluoPorAnio;

	/**
	 * Variable que almacena si el año inicial del cálculo de liquidación de avalúos
	 * cuando se hace en por un rango de años
	 */
	private String anioInicialCalculoAvaluoPorRango;

	/**
	 * Variable que almacena si el año final del cálculo de liquidación de avalúos
	 * cuando se hace en por un rango de años
	 */
	private String anioFinalCalculoAvaluoPorRango;

	/**
	 * Variable que almacena el año a calcular en la liquidación de avaluos
	 */
	private String anioCalculoAvaluo;

	/**
	 * Valor del avalúo al calcular manualmente la liquidación de avaluos
	 */
	private double valorAvaluoCalculoAvaluo;

	/**
	 * bandera que indica si se activa el botón del cálculo de avalúo de forma
	 * manual
	 */
	private boolean activarCalculoAvaluoManual;

	/**
	 * bandera que indica si se activa el botón de eliminar cálculo de avalúo de
	 * forma manual
	 */
	private boolean activarEliminarCalculoAvaluoManual;

	/**
	 * bandera que indica si se debe mostrar la tabla de inscripciones y decretos en
	 * la pantalla de calculo de avaluos de forma manual
	 */
	private boolean mostrarTablaInscripcionesYDecretos;

	/**
	 * bandera que indica si se activa el botón de eliminar cálculo de avalúo de
	 * forma manual por rango de anios
	 */
	private boolean activarCalcularAvaluoPorRangoDeAnios;

	/**
	 * Objeto seleccionado de la tabla de proyectar inscripciones y decretos
	 */
	private VPPredioAvaluoDecreto pPredioAvaluoCatastralSeleccionado;

	/**
	 * Lista que contiene los años de los avalúos que no se pudieron calcular
	 */
	private List<SelectItem> aniosCalculoAvaluoPorRango;

	/**
	 * Lista de VPPredioAvaluoDecreto a los que se les va a calcular el avalúo en
	 * forma manual de manera retroactiva
	 */
	private List<VPPredioAvaluoDecreto> prediosAvaluoDecretoCalculoManual;

	/**
	 * Bandera para determinar si el avaluo se confirma (true) o se modifica (false)
	 */
	private boolean banderaConfirmaAvaluo;

	/**
	 * Bandera para determinar si la autoestimacion se acepta (true) o se rechaza
	 * (false)
	 */
	private boolean banderaAceptaAutoestimacion;

	/**
	 * Bandera para validación de campo Autoestimación seleccionado para mutación de
	 * cuarta
	 */
	private boolean banderaCampoAutoestimacionSeleccionado;

	/**
	 * Bandera para determinar si se activa el botón de procesar liquidación de
	 * avaluos cuando la fecha de título es igual a la fecha vigente
	 */
	private boolean banderaAnioTituloEsAnioVigente;

	/**
	 * Lista de posibles digitalizadores para realizar la edicion geografica
	 */
	private List<Object[]> digitalizadores;

	/**
	 * Digitalizador seleccionado para realizar la edición
	 */
	private Object[] digitalizadorSeleccionado;

	/**
	 * Predio seleccionado para la cancelacion
	 */
	private List<Predio> prediosSeleccionadosCancelacion;

	private boolean banderaConvencionalRectifica;

	/**
	 * Tramite de con la información de depuracion
	 */
	private TramiteDepuracion tramiteDepuracion;

	/**
	 * Se usa para el desenglobe de mejoras
	 */
	private List<PUnidadConstruccion> unidadesConsNuevasDesenglobeMejora;

	/**
	 * Se usa para obtener la Unidades de Construccion Proyectadas de los tramites
	 * de Tercera y Quinta
	 */
	private List<PUnidadConstruccion> pUnidadesConstruccion;

	/**
	 * Se usa para obtener las unidades de Construccion de tramites de Tercera y
	 * Quinta
	 */
	private List<UnidadConstruccion> unidadesConstruccion;

	/**
	 * Se usa para el desenglobe de mejoras
	 */
	private List<UnidadConstruccion> unidadesConsVigentesDesenglobeMejora;
	private List<UnidadConstruccion> unidadesConsNoConvencionalesVigentesDesenglobeMejora;

	private boolean banderaQuintaOmitidoMejora;

	/**
	 * * Indica si el usuario ya aceptó la cancelación de una doble inscripción
	 */
	private boolean banderaAceptaCancelacionDobleInscripcion;

	/**
	 * * Indica si el trámite es una cancelación de una doble inscripción
	 */
	private boolean banderaCancelacionDobleInscripcion;

	/**
	 * Bandera que habilita las observaciones de depuración
	 */
	private boolean mostrarObservacionesDepuracion;

	/**
	 * Bandera que habilita las ventanas de proyección de vía gubernativa
	 */
	private boolean mostrarProyeccionTramiteViaGubernativa;

	/**
	 * Indica si el usuario modifica o confirma el resultado de la resolución
	 * inicial
	 */
	private boolean modResultResolucionInicial;

	/**
	 * Habilita las opciones si el usuario modifica o confirma el resultado de la
	 * resolución inicial
	 */
	private boolean habilitarBotonesViaGubernativa;

	/**
	 * Tipos de documento aceptados para la modificación de linderos.
	 */
	private List<SelectItem> opcionesRecursoViaGubernativa;

	/**
	 * Bandera para habilitar botones Proyectar y Auto de prueba; para vía
	 * gubernativa se valida que el predio asociado al recurso de vía gubernativa
	 * tenga datos de proyección
	 */
	private boolean validacionViaGubernativa;

	/**
	 * Lista que contiene los históricos de proyecciones, usado para tramites de vía
	 * gubernativa
	 */
	private List<HistoricoProyeccionViaGubernativaDTO> listaTramitesHistoricoProyeccion;

	/**
	 * El uso de cada indice se encuentra documentado en el metodo getTabs
	 */
	private boolean tabs[] = new boolean[13];

	private Date fechaInscripcionCatastral = null;

	/**
	 * Determina si se trata de una rectificacion de zona geoeconomica o fisica.
	 */
	private boolean banderaRectificacionZonas;

	/**
	 * Determina que tipo de cambio de zona se va a realizar
	 */
	private int tipoCambioZona;

	/**
	 * Posibles tipo de cambio de zona
	 */
	private List<SelectItem> tiposCambioZona;

	/**
	 * Determina si el tramite esta en el proceso de validacion
	 */
	private boolean tramiteValidacion;

	/**
	 * Variable para determinar si paso por actividad de validación
	 */
	private Boolean isPasoActividadValidacion;

	/**
	 * Variable para saber si es un trámite de primera sin que se hayan procesado
	 * las inscripciones y/o decretos
	 */
	private boolean banderaTramitePrimeraSinInscripcionYDecretos;

	/**
	 * Bandera que determina si el area de construccion ya fue modificada para los
	 * trámites de rectificacion de este tipo.
	 */
	private boolean banderaRectAreaConstruccion;

	/**
	 * Solo aplica para tramites CDI
	 */
	private boolean banderaPredioCanceladoCDI;

	/**
	 * Mensaje que se muestra cuando los predios asociados al tramites ya estan
	 * proyectados
	 */
	private String mensajePredioEnTramite;

	/**
	 * Variable para controlar la activación del botón de envio al cordinador cuando
	 * el trámite es una mutación de quinta
	 */
	private boolean validacionQuinta;

	/**
	 * Variables usadas para el manejo de construcciones originales del predio
	 */
	private List<UnidadConstruccion> unidadConstruccionConvencionalPredioOriginal;
	private List<UnidadConstruccion> unidadConstruccionNoConvencionalPredioOriginal;
	private UnidadConstruccion[] unidadConstruccionConvencionalPredioOriginalSeleccionadas;
	private UnidadConstruccion[] unidadConstruccionNoConvencionalPredioOriginalSeleccionadas;

	/**
	 * Variable usada para verificar que existan datos en las tablas de proyección
	 * previo a enviar la actividad al coordinador
	 */
	private boolean verificacionDatosProyeccion;

	/**
	 * Bandera para identificar si los predios del tramite son fiscales
	 */
	private boolean banderaPredioFiscal;
	private boolean banderaSinFechaInscripcionFiscal;
	private boolean predioFiscalQuintaNuevo;

	/**
	 * Valor de área para los trámites con predios fiscales
	 */
	private double areaTotalTerrenoUnidadF;
	private double valorMetroFiscal;

	/**
	 * Bandera para manejar el calculo manual de los avaluos en los rpedios fiscales
	 */
	private boolean banderaAvaluoManualFiscales;
	private boolean banderaAvaluoSoloArea;

	/**
	 * Variable usada para validar si la matricula inmobiliaria de la ficha tiene
	 * valor
	 */
	private boolean matriculaVaciaBool;

	/**
	 * Variable usada para validar si la condicion de propiedad del predio es 0
	 */
	private boolean banderaCondicionProdpiedadCero;

	/**
	 * Variable usada para validar si se muestra la pestaña de ficha matriz en las
	 * rectificaciones masivas
	 */
	private boolean mostrarFichaMatrizRectificacion;

	/**
	 * Variable usada para validar si se seleccionó la pestaña de ficha matriz
	 */
	private boolean pestanaFichaMatrizSeleccionada;

	/*
	 * Variable usada para verificar si las validaciones se están corriendo para uno
	 * o para todos los predios de la quinta masiva
	 */
	private boolean validacionQuintaMasivoTodos;

	/**
	 * banderas modificacion de PH
	 */
	public boolean banderaEnglobeVirtual;
	/**
	 * determina el estado de las validaciones del englobe virtual
	 */
	private boolean validarGuardarEVOK;

	/**
	 * Variable que valida si se generaron mensajes de error durante la asociaciÃ³n
	 * de unidades para NPH
	 */
	private boolean messageError;

	/**
	 * Variables usadas para manejo de construcciones nuevas y vigentes.
	 */
	private List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes;
	private List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes;
	private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas;
	private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas;
	private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas;
	private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas;

	private boolean quintaNuevoCond5;

	// ------------------------------------------------------------ //
	// --------------------- M É T O D O S ----------------------- //
	// ------------------------------------------------------------ //
	@SuppressWarnings("unused")
	@PostConstruct
	public void init() {
		LOGGER.debug("******* entra al init de proyectar conservacion *******");

		this.usuario = MenuMB.getMenu().getUsuarioDto();

		String rutaArchivoEnWeb;
		this.banderaAceptaAutoestimacion = true;
		this.banderaAceptarDireccionNormalizada = false;
		this.mostrarFichaMatrizRectificacion = true;
		this.calculozonaFlag = false;

		try {

			this.actividad = this.tareasPendientesMB.getInstanciaSeleccionada();
			this.actividad.getEstado();

			this.tramite = this.getTramiteService()
					.buscarTramitePorTramiteIdProyeccion(this.actividad.getIdObjetoNegocio());

			if (this.getGeneralesService().buscarDocumentoPorTramiteIdyTipoDocumento(this.tramite.getId(),
					ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId()) != null) {
				this.calculozonaFlag = true;
			}

			if (this.tramite.getComisionTramites() != null && !this.tramite.getComisionTramites().isEmpty()) {
				this.comisionesTramiteDato = this.getTramiteService()
						.buscarComisionTramiteDatoPorTramiteId(this.tramite.getId());
				for (ComisionTramite ct : this.tramite.getComisionTramites()) {
					ct.setComisionTramiteDatos(this.comisionesTramiteDato);
				}
			}

			// leidy.gonzalez::CANCELACION MASIVA:: #20461::Valida que el PH o alguno de los
			// predios del PH no haya sido cancelados
			if (!this.tramite.isQuinta()) {

				List<PPredio> prediosTramite = this.getConservacionService()
						.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());

				// Se valida que esta validacion solo se haga la primera vez que se ingresa.
				if (prediosTramite == null) {

					if (this.tramite.getPredio() != null && this.tramite.getPredio().getEstado() != null
							&& !this.tramite.getPredio().getEstado().isEmpty()
							&& EFichaMatrizEstado.CANCELADO.getCodigo().equals(this.tramite.getPredio().getEstado())) {

						this.predioEnTramite = true;
						this.mensajePredioEnTramite = "La Ficha Matriz del Predio: "
								+ this.tramite.getPredio().getNumeroPredial()
								+ " se encuentra cancelado. No se puede, realizar el trámite sobre ella. ";
						LOGGER.debug("La Ficha Matriz del Predio: " + this.tramite.getPredio().getNumeroPredial()
								+ " se encuentra cancelado. No se puede realizar el trámite sobre ella.");
						return;
					}

					for (Predio predioTramite : this.tramite.getPredios()) {
						if (predioTramite.getEstado() != null && !predioTramite.getEstado().isEmpty()
								&& EFichaMatrizEstado.CANCELADO.getCodigo().equals(predioTramite.getEstado())) {

							this.predioEnTramite = true;
							this.mensajePredioEnTramite = "El predio: " + predioTramite.getNumeroPredial()
									+ " se encuentra cancelado. No se puede, realizar el trámites sobre ellos.";
							LOGGER.debug("El predio: " + predioTramite.getNumeroPredial()
									+ " se encuentra cancelado. No se puede realizar el trámites sobre ellos.");
							return;
						}
					}
				}
			}

			// leidy.gonzalez::CANCELACION MASIVA:: #20461::Carga PPredios Y
			// PPredioPropietario
			if (this.tramite.isCancelacionMasiva()) {
				this.cargarPrediosCancelacionMasivo();
				this.cargarPropietariosCancelacionMasiva();
			}

			// felipe.cadena::#5799::se valida si los predios asociados al
			// tramite tienen otros tramites en curso
			this.validarTramitesAsociadosAPredios();
			if (this.predioEnTramite) {
				return;
			}

			if (!this.tramite.isSegunda() && !this.tramite.isQuinta()) {
				// Esta condicion existe, porque la proyeccion se hace luego
				// desde la pantalla

				if (this.predioEnTramite) {
					return;
				} else {
					this.getTramiteService().generarProyeccion(tramite.getId());
				}
			}

			// lorena.salamanca :: 24-08-2015 :: Quinta Masivo.
			if (this.tramite.isQuintaMasivo()) {
				if (this.predioEnTramite) {
					return;
				}
				this.cargarPrediosQuintaMasivo();
				// Si la proyeccion no existe, se hace
				if (this.pPrediosQuintaMasivo == null || this.pPrediosQuintaMasivo.isEmpty()) {
					this.getTramiteService().generarProyeccion(tramite.getId());
				}
				this.consultarFichaMatriz();
				this.predioSeleccionado = null;
			}

			// david.cifuentes :: #14861 :: Actualización de las áreas de la
			// ficha matriz si el trámite ha ido a lo geográfico
			// david.cifuentes Implementación del ajuste para la incidencia
			// #14861, pendiente de aval para integración :: 30/10/2015
			boolean actualizaAreasFMDesenglobe = false;

			// felipe.cadena::21-01-2015::#11148::Obligar a ir a geografico para
			// los tramites segunda englobe.
			if (!Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA.equals(this.tramite.getEstadoGdb())) {
				// Verificar si ya ha ido a geografico
				if (this.tramite.isSegundaEnglobe() || this.tramite.isSegundaDesenglobe()) {
					List<TramiteDocumento> docsTramite = this.tramite.getTramiteDocumentos();
					boolean tieneReplica = false;
					for (TramiteDocumento td : docsTramite) {
						if (td.getDocumento().getTipoDocumento().getId()
								.equals(ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId())) {
							tieneReplica = true;
						}
					}

					if (!tieneReplica && this.tramite.isSegundaEnglobe()) {
						this.tramite.setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
						this.getTramiteService().actualizarTramite(this.tramite);
					}

					if (tieneReplica && this.tramite.isSegundaDesenglobe()) {
						actualizaAreasFMDesenglobe = true;
					}
				}
			}

			this.banderaAvaluoManualFiscales = false;
			this.banderaAvaluoSoloArea = false;
			this.banderaSinFechaInscripcionFiscal = false;
			// Validacion si es predio fiscal.
			if (this.tramite.isQuintaOmitido()) {
				Predio p = this.getConservacionService().getPredioPorNumeroPredial(this.tramite.getNumeroPredial());
				if (p != null) {
					if (p.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
						this.banderaPredioFiscal = true;
						this.tramite.setEstadoGdb(null);
						this.getTramiteService().actualizarTramite(this.tramite);
						if (p.getFechaInscripcionCatastral() == null) {
							this.banderaSinFechaInscripcionFiscal = true;
						}
					}
				}
			}

			if (this.tramite.isQuintaNuevo()) {
				PPredio p = this.getConservacionService().obtenerPPredioCompletoByIdTramite(this.tramite.getId());
				if (p != null) {
					// Valida que para los quinta nuevo no se pueda modificar la matricula
					// Inmobiliaria
					if (p.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
						this.quintaNuevoCond5 = true;
						this.setQuintaNuevoCond5(true);
					} else {
						this.quintaNuevoCond5 = false;
					}

					if (p.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
						this.banderaPredioFiscal = true;
						this.predioFiscalQuintaNuevo = true;
						this.banderaAvaluoManualFiscales = true;
					} else {
						this.banderaPredioFiscal = false;
						this.predioFiscalQuintaNuevo = false;
						this.banderaAvaluoManualFiscales = false;
					}
				}
			}

			if (this.tramite.getPredio() != null) {
				if (this.tramite.getPredio().getTipoCatastro()
						.equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
					this.banderaPredioFiscal = true;
					this.tramite.setEstadoGdb(null);
					this.getTramiteService().actualizarTramite(this.tramite);
				}
			}

			// javier.aponte :: Para trámites de tercera masivo y de
			// rectificación matriz no se hace esto porque el predio
			// se selecciona desde la pestaña de proyección del trámite ::
			// 04/08/2015
			if (!(this.tramite.isTerceraMasiva() || this.tramite.isRectificacionMatriz()
					|| this.tramite.isQuintaMasivo() || this.tramite.isCancelacionMasiva())) {
				obtenerPredioCompletoYAvaluo();
			}

			if (this.predioSeleccionado != null) {
				this.guardarAreas();
			}

			if (this.tramite.isSegundaEnglobe()) {
				this.cargarPrediosEnglobe();
			}

			if (this.tramite.isSegundaDesenglobe()) {
				this.cargarPrediosDesenglobe();
			}

			if (this.tramite.isSegundaDesenglobe() || this.tramite.isRectificacionMatriz()) {
				this.consultarFichaMatriz();
			}

			// david.cifuentes Implementación del ajuste para la incidencia
			// #14861, pendiente de aval para integración :: 30/10/2015
			// if(actualizaAreasFMDesenglobe && this.fichaMatrizSeleccionada !=
			// null){
			// this.actualizarAreasFichaMatriz(true);
			// }
			if (this.tramite.isTerceraMasiva() || this.tramite.isRectificacionMatriz()) {
				this.cargarPrediosTerceraRectificacionMasiva();
				if (this.tramite.isTerceraMasiva() && this.pPrediosTerceraRectificacionMasiva != null
						&& !this.pPrediosTerceraRectificacionMasiva.isEmpty()) {
					for (PPredio pTemp : this.pPrediosTerceraRectificacionMasiva) {
						pTemp.setFechaInscripcionCatastral(new Date(System.currentTimeMillis()));
					}
					this.getConservacionService().guardarPPredios(this.pPrediosTerceraRectificacionMasiva);
				}
				this.numeroRegistroInfoMatriculaNueva = this.tramite.getPredio().getNumeroPredial().substring(0, 22);
				this.consultarFichaMatriz();
				this.predioSeleccionado = null;
			}

			List<TramiteDocumento> documentosPruebas = this.getTramiteService()
					.obtenerTramiteDocumentosDePruebasPorTramiteId(this.tramite.getId());
			if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
				this.tramite.setDocumentosPruebas(documentosPruebas);
			}

			this.historicoObservaciones = this.getGeneralesService()
					.buscarTramiteEstadosPorTramiteId(this.tramite.getId());

			// D: cargar la variable que define qué tabs se muestran según el
			// tipo de trámite
			this.configuracion = this.getTramiteService().consultarConfiguracionTramite(this.tramite.getTipoTramite(),
					this.tramite.getClaseMutacion(), this.tramite.getSubtipo(), this.tramite.getRadicacionEspecial());

			// Carga datos dominio PREDIO_TIPO -> ComboBox
			this.tiposPredio = new ArrayList<SelectItem>();
			this.tiposPredioEtapas = new ArrayList<SelectItem>();
			List<Dominio> listDominio = this.getGeneralesService().getCacheDominioPorNombre(EDominio.PREDIO_TIPO);
			this.tiposPredio.add(new SelectItem("", "Seleccionar..."));
			for (Dominio dominio : listDominio) {
				this.tiposPredio
						.add(new SelectItem(dominio.getCodigo(), dominio.getCodigo() + " - " + dominio.getValor()));
				this.tiposPredioEtapas
						.add(new SelectItem(dominio.getCodigo(), dominio.getCodigo() + " - " + dominio.getValor()));
			}
			// Carga datos dominio DESTINO_PREDIO -> ComboBox

			this.tiposDestinoPredio = new ArrayList<SelectItem>();

			listDominio = this.getGeneralesService().getCacheDominioPorNombre(EDominio.PREDIO_DESTINO);
			for (Dominio dominio : listDominio) {
				this.tiposDestinoPredio
						.add(new SelectItem(dominio.getCodigo(), dominio.getCodigo() + " - " + dominio.getValor()));
			}

			// Carga datos dominio PREDIO_SERVIDUMBRE_SERVIDUM -> ComboBox
			this.tiposPredioServidumbre = new ArrayList<SelectItem>();

			listDominio = this.getGeneralesService().getCacheDominioPorNombre(EDominio.PREDIO_SERVIDUMBRE_SERVIDUM);
			for (Dominio dominio : listDominio) {
				this.tiposPredioServidumbre.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
			}

			this.predioDireccionSeleccionado = new PPredioDireccion();
			this.servidumbreAdicional = new PPredioServidumbre();

			this.modeloResolucionSeleccionado = new ModeloResolucion();
			if (this.tramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo())) {
				this.tramiteEstado = this.getTramiteService()
						.buscarTramiteEstadoVigentePorTramiteId(this.tramite.getId());
			}

			this.tramiteSinTitulos = true;

			// Se buscan los circulos registrales filtrados por municipio
			this.listCirculosRegistrales = new ArrayList<CirculoRegistral>();
			if (this.tramite != null && this.tramite.getMunicipio() != null) {
				String codigoMunicipio = this.tramite.getMunicipio().getCodigo();
				this.listCirculosRegistrales = this.getGeneralesService()
						.getCacheCirculosRegistralesPorMunicipio(codigoMunicipio);
			}
			if (!this.listCirculosRegistrales.isEmpty()) {
				SelectItem nombre;
				for (CirculoRegistral circulo : this.listCirculosRegistrales) {
					nombre = new SelectItem();
					nombre.setValue(circulo.getCodigo());
					nombre.setLabel(circulo.getNombre());
					this.itemListCirculosRegistrales.add(nombre);
				}
			}

			// Carga inicial de las plantillas de texto motivado.
			this.consultarModeloResolucionSegunTipoDeTramite();

			// this.actividad = tareasPendientesMB.getInstanciaSeleccionada();
			this.imagenAntes = null;
			this.imagenDespues = null;
			if (this.tramite.isTramiteGeografico()) {
				List<Documento> documentosImagenes = this.getConservacionService().buscarImagenesTramite(this.tramite);

				IDocumentosService service = DocumentalServiceFactory.getService();
				if (documentosImagenes != null) {
					for (Documento d : documentosImagenes) {

						if (d.getIdRepositorioDocumentos() != null && d.getArchivo() != null) {
							rutaArchivoEnWeb = this.getGeneralesService()
									.descargarArchivoAlfrescoYSubirAPreview(d.getIdRepositorioDocumentos());

							if (d.getTipoDocumento().getId()
									.equals(ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE.getId())) {
								this.imagenAntes = rutaArchivoEnWeb;
							}
							if (d.getTipoDocumento().getId()
									.equals(ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId())) {
								this.imagenDespues = rutaArchivoEnWeb;
							}
						}
					}
				}
				if (this.imagenAntes != null) {
					ImageIcon ii;
					java.net.URL imgURL;
					try {
						imgURL = new URL(this.imagenAntes);
						if (imgURL != null) {
							ii = new ImageIcon(imgURL, "");
							this.anchoImagen = ii.getIconWidth();
							this.altoImagen = ii.getIconHeight();
							double radio = (double) this.anchoImagen / this.altoImagen;
							this.anchoImagen = 900;
							this.altoImagen = (int) (this.anchoImagen / radio);
						}
					} catch (MalformedURLException e) {
						LOGGER.error("error armando la url de la imagen 'antes': " + e.getMessage());
					}
				}
			}
			// obtener información depuración, en caso de tenerla

			// consultar la ultima razón de rechazo depuración
			this.tramiteDepuracion = this.getTramiteService()
					.buscarUltimoTramiteDepuracionPorIdTramite(this.tramite.getId());
			this.mostrarObservacionesDepuracion = false;

			if (this.tramiteDepuracion != null) {
				String razonesRechazoDepuracion = this.tramiteDepuracion.getRazonesRechazo();
				if ((razonesRechazoDepuracion != null && !razonesRechazoDepuracion.isEmpty())
						|| (this.tramiteDepuracion.getTramiteDepuracionObservacions() != null
								&& !this.tramiteDepuracion.getTramiteDepuracionObservacions().isEmpty())) {
					this.mostrarObservacionesDepuracion = true;

				}

			}

		} catch (Exception e) {
			if (e.getCause() instanceof FileSystemException) {
				LOGGER.error("Ocurrió un error con el servidor de documentos", e.getMessage());
				this.addMensajeError("Ocurrió un error con el servidor de ftp, por favor intente de nuevo "
						+ "o comuníquese con el administrador");
			} else {
				LOGGER.error(e.getMessage(), e);
				this.addMensajeError(e);
			}
		}
		// findPredioAvaluo();

		// --- init CU-NP-CO-121
		// D: hay que inicializar una unidad de construcción para poder usarla
		// en el armado de uno de los árboles que se usa por defecto para
		// "mostrar" en
		// gestionDetalleUnidadesConstruccion, debido a que no funcionó la
		// condición de rendered para la forma definida allí
		if (this.unidadConstruccionSeleccionada == null) {
			this.unidadConstruccionSeleccionada = new PUnidadConstruccion();
			this.unidadConstruccionSeleccionada
					.setTipoCalificacion(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo());
		}

		this.treeComponentesConstruccion1 = armarArbolCalificacionConstruccion(
				this.unidadConstruccionSeleccionada.getTipoCalificacion());

		// Cargar arbol de componentes para la unidad del modelo de
		// construcción.
		this.treeComponentesConstruccionParaModelo = armarArbolCalificacionConstruccion(
				this.unidadConstruccionSeleccionada.getTipoCalificacion());

		this.actualizarListaDeDirecciones();

		// Tab seleccionado por defecto
		this.selectDefaultTab();
		inicializarValidezSecciones();

		setupUnidadesConstruccion();

		// Se valida si en las zonas proyectadas para el predio
		this.valorM2zonaFlag = true;
		this.validarPredioZonasValorM2();

		activarValidarGuardarBool = true;

		// Banderas del caso de uso de liquidar avaluos de trámites
		this.activarCalculoAvaluoManual = false;
		this.activarEliminarCalculoAvaluoManual = false;
		this.mostrarTablaInscripcionesYDecretos = false;
		this.activarCalcularAvaluoPorRangoDeAnios = false;
		this.aniosCalculoAvaluoPorRango = new ArrayList<SelectItem>();

		this.banderaAnioTituloEsAnioVigente = false;

		this.determinarDigitalizador();

		this.prediosSeleccionadosCancelacion = new ArrayList<Predio>();
		this.prediosSeleccionadosCancelacion.add(this.tramite.getPredio());

		this.banderaConvencionalRectifica = true;

		// se inician las banderas para los tramites con mejoras
		if (this.tramite.isSegundaEnglobe()) {
			this.determinarTipoEngloble();
		}

		if (this.tramite.isSegundaDesenglobe()) {
			if (this.tramite.getPredio().isMejora()) {
				this.banderaDesenglobeMejora = true;
			}
		}

		// Inicia validaciones de vía gubernativa CC-NP-CO-030
		this.mostrarProyeccionTramiteViaGubernativa = false;
		this.habilitarBotonesViaGubernativa = false;
		this.validacionViaGubernativa = false;
		if (this.tramite.isViaGubernativa() || this.tramite.isTipoTramiteViaGubernativaModificado()
				|| this.tramite.isTipoTramiteViaGubModSubsidioApelacion()) {

			if (this.tramite.isViaGubernativa()) {
				this.mostrarProyeccionTramiteViaGubernativa = true;

				this.opcionesRecursoViaGubernativa = new ArrayList<SelectItem>();

				this.opcionesRecursoViaGubernativa
						.add(new SelectItem(false, "Se confirma el resultado de la resolución inicial"));
				this.opcionesRecursoViaGubernativa
						.add(new SelectItem(true, "Se modifica el resultado de la resolución inicial"));
			}
			PPredio pp = null;
			try {
				pp = this.getConservacionService().obtenerPPredioCompletoById(this.tramite.getPredio().getId());
			} catch (Exception ex) {
				LOGGER.error("Ocurrió un error obteniedo el PPredio", ex.getMessage());
			}

			if (pp != null) {
				Tramite tramiteRef = null;
				FiltroDatosConsultaTramite filtrosTramite = new FiltroDatosConsultaTramite();
				filtrosTramite.setNumeroRadicacion(this.tramite.getSolicitud().getTramiteNumeroRadicacionRef());
				filtrosTramite.setEstadoTramite("0");

				List<Tramite> resultado = this.getTramiteService().consultaDeTramitesPorFiltros(true, filtrosTramite,
						null, null, false, null, null, null);

				if (resultado != null && !resultado.isEmpty()) {
					tramiteRef = resultado.get(0);
					this.listaTramitesHistoricoProyeccion = new ArrayList<HistoricoProyeccionViaGubernativaDTO>();
					HistoricoProyeccionViaGubernativaDTO historicoP = new HistoricoProyeccionViaGubernativaDTO();
					historicoP.setTipoProyeccion("Proyección inicial");
					tramiteRef = this.getTramiteService().findTramitePruebasByTramiteId(tramiteRef.getId());
					historicoP.setTramiteInfo(tramiteRef);
					this.listaTramitesHistoricoProyeccion.add(historicoP);
				}
				// adicionar informacion de los historicos
				// List<HPredio> prediosHistoricos = new ArrayList<HPredio>();
				// List<HPredio> prediosAnteriores =
				// this.getConservacionService().findHPredioListByConsecutivoCatastral(this.tramite.getPredio().getConsecutivoCatastral());
				// prediosAnteriores.get(0).
				this.validacionViaGubernativa = true;
			}
		}

		if (this.isBanderaCancelacionDobleInscripcion()) {

			if (this.predioSeleccionado != null && this.predioSeleccionado.getCancelaInscribe() != null
					&& this.predioSeleccionado.getCancelaInscribe()
							.equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {
				this.banderaAceptaCancelacionDobleInscripcion = true;
			} else {
				this.banderaAceptaCancelacionDobleInscripcion = false;
			}
			this.inicializarValidezSecciones();
		}
		if (this.tramite.isQuintaOmitido()) {
			String condicionPropiedad = this.tramite.getNumeroPredial().substring(21, 22);
			if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
				this.banderaQuintaOmitidoMejora = true;
				this.cargarFechasQuintaMejora();
			}
		}
		this.validacionQuinta = true;
		if (this.tramite.isQuinta() && (this.imagenAntes == null || this.imagenDespues == null)) {
			this.validacionQuinta = false;
		}

		// determinar si el tramite es de rectificacion de zonas
		this.banderaRectificacionZonas = this.tramite.isRectificacionZona();
		this.tipoCambioZona = ECambioZonaHomogenea.CAMBIO_CODIGO.getCodigo();

		this.tiposCambioZona = new ArrayList<SelectItem>();

		SelectItem si = new SelectItem(ECambioZonaHomogenea.CAMBIO_CODIGO.getCodigo(),
				ECambioZonaHomogenea.CAMBIO_CODIGO.getValor(), "", false);
		this.tiposCambioZona.add(si);
		si = new SelectItem(ECambioZonaHomogenea.AJUSTE_ESTUDIO.getCodigo(),
				ECambioZonaHomogenea.AJUSTE_ESTUDIO.getValor(), "", true);
		this.tiposCambioZona.add(si);

		this.setupTramiteValidacion();

		// felipe.cadena::14-19-2016::#19737::Se valida si el tramite paso por
		// la actividad de validacion a cargo del responsable conservacion.
		this.isPasoActividadValidacion = false;

		for (TramiteEstado estado : this.tramite.getTramiteEstados()) {
			if (ERol.RESPONSABLE_CONSERVACION.toString().equals(estado.getRolResponsable())) {
				this.isPasoActividadValidacion = true;
				break;
			}
		}

		this.banderaTramitePrimeraSinInscripcionYDecretos = false;

		this.banderaRectAreaConstruccion = true;

		this.banderaCancelacionDobleInscripcion = this.tramite.isRectificacionCDI();

		if (this.banderaCancelacionDobleInscripcion) {
			this.banderaPredioCanceladoCDI = !this.tramite.getPredio().getEstado()
					.equals(ETramiteEstado.CANCELADO.toString());
		} else {
			this.banderaPredioCanceladoCDI = true;
		}

		this.inicializarEstadoGDB();

		// Modelos de construccion para tramites de tercera
		if ((this.tramite.isTercera() && !this.tramite.isTerceraMasiva())
				&& this.tramite.getPredio().isEsPredioEnPH()) {

			String numPredial = this.tramite.getPredio().getNumeroPredial();

			FichaMatriz fm = this.getConservacionService().obtenerFichaMatrizOrigen(numPredial);

			// Revisar si la ficha matriz tiene modelos asociados.
			if (fm != null && fm.getFichaMatrizModelos() != null && fm.getFichaMatrizModelos().size() > 0) {
				this.existenModelosBool = true;
			}
		}
		// Modelos de construccion para tramites de tercera masiva o quinta
		// masiva
		if (this.tramite.isTerceraMasiva() || this.tramite.isQuintaMasivo()) {

			PFichaMatriz pfm = this.getConservacionService()
					.buscarPFichaMatrizPorPredioId(this.tramite.getPredio().getId());

			// Revisar si la ficha matriz tiene modelos asociados.
			if (pfm != null && pfm.getPFichaMatrizModelos() != null && pfm.getPFichaMatrizModelos().size() > 0) {
				this.existenModelosBool = true;
			}
		}
		if (this.tramite.isRectificacion()) {
			this.actualizarBanderaModificacion();
		}

		if (this.tramite.isCuarta() && this.banderaPredioFiscal) {
			this.areaTotalTerrenoUnidadF = this.predioSeleccionado.getAreaTerreno();
			this.getAreaTotalTerrenoUnidad();
			this.valorMetroFiscal = this.avaluo.getValorTerreno() / this.predioSeleccionado.getAreaTerreno();
		}

		// #16886 :: Se actualiza el campo estado_gdb del trámite cuando ha
		// vuelto de la geográfica para trámites de quinta masivo :: 13/04/2014
		if ((this.getTramite().isQuintaMasivo() || this.getTramite().isSegundaDesenglobe())
				&& this.getTramite().isTramiteGeografico()) {
			List<TramiteDocumento> docs = this.getTramiteService()
					.obtenerTramiteDocumentosPorTramiteId(this.getTramite().getId());
			boolean replicaFinal = false;

			for (TramiteDocumento td : docs) {
				if (td.getDocumento() != null && ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId()
						.equals(td.getDocumento().getTipoDocumento().getId())) {
					replicaFinal = true;
					break;
				}
			}
			if (replicaFinal) {
				this.getTramite().setEstadoGdb(null);
				this.getTramiteService().actualizarTramite(this.getTramite());
			}
		}

		if (this.isTodasSeccionesValidas()) {
			this.verificacionDatosProyeccion = this.validacionProyeccionService.verificarDatosProyeccion(this.tramite);
		}

		if (this.tramite.isTercera()) {
			this.tramite.setFechaInscripcionCatastral(new Date());
			this.getTramiteService().actualizarTramite(this.tramite);
		}

		this.validarGuardarEVOK = true;
		if (this.tramite.isEnglobeVirtual()) {
			this.inicializaEnglobeVirtual();
		}

	}

	// ------------------- methods
	// -----------------------------------------------------
	public boolean isQuintaNuevoCond5() {
		return quintaNuevoCond5;
	}

	public void setQuintaNuevoCond5(boolean quintaNuevoCond5) {
		this.quintaNuevoCond5 = quintaNuevoCond5;
	}

	public List<UnidadConstruccion> getUnidadesConstruccion() {
		return unidadesConstruccion;
	}

	public void setUnidadesConstruccion(List<UnidadConstruccion> unidadesConstruccion) {
		this.unidadesConstruccion = unidadesConstruccion;
	}

	public List<PUnidadConstruccion> getpUnidadesConstruccion() {
		return pUnidadesConstruccion;
	}

	public void setpUnidadesConstruccion(List<PUnidadConstruccion> pUnidadesConstruccion) {
		this.pUnidadesConstruccion = pUnidadesConstruccion;
	}

	public boolean isValidarGuardarEVOK() {
		return validarGuardarEVOK;
	}

	public void setValidarGuardarEVOK(boolean validarGuardarEVOK) {
		this.validarGuardarEVOK = validarGuardarEVOK;
	}

	public boolean isBanderaEnglobeVirtual() {
		return banderaEnglobeVirtual;
	}

	public void setBanderaEnglobeVirtual(boolean banderaEnglobeVirtual) {
		this.banderaEnglobeVirtual = banderaEnglobeVirtual;
	}

	public boolean isMostrarFichaMatrizRectificacion() {
		return mostrarFichaMatrizRectificacion;
	}

	public void setMostrarFichaMatrizRectificacion(boolean mostrarFichaMatrizRectificacion) {
		this.mostrarFichaMatrizRectificacion = mostrarFichaMatrizRectificacion;
	}

	public boolean isBanderaPredioFiscal() {
		return banderaPredioFiscal;
	}

	public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
		this.banderaPredioFiscal = banderaPredioFiscal;
	}

	public boolean isBanderaSinFechaInscripcionFiscal() {
		return banderaSinFechaInscripcionFiscal;
	}

	public void setBanderaSinFechaInscripcionFiscal(boolean banderaSinFechaInscripcionFiscal) {
		this.banderaSinFechaInscripcionFiscal = banderaSinFechaInscripcionFiscal;
	}

	public String getMensajePredioEnTramite() {
		return mensajePredioEnTramite;
	}

	public void setMensajePredioEnTramite(String mensajePredioEnTramite) {
		this.mensajePredioEnTramite = mensajePredioEnTramite;
	}

	public boolean isBanderaPredioCanceladoCDI() {
		return banderaPredioCanceladoCDI;
	}

	public void setBanderaPredioCanceladoCDI(boolean banderaPredioCanceladoCDI) {
		this.banderaPredioCanceladoCDI = banderaPredioCanceladoCDI;
	}

	/**
	 * Metodo para determinar si los tramites de tercera y rectificaciones CDI deben
	 * ir al editor geografico
	 *
	 * @author felipe.cadena
	 */
	public void inicializarEstadoGDB() {
		if ((this.getTramite().isRectificacionCDI() && this.getTramite().isTramiteGeografico())
				|| this.getTramite().isTercera()) {
			List<TramiteDocumento> docs = this.getTramiteService()
					.obtenerTramiteDocumentosPorTramiteId(this.getTramite().getId());
			boolean replicaFinal = false;

			for (TramiteDocumento td : docs) {
				if (td.getDocumento().getTipoDocumento().getId()
						.equals(ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId())) {
					replicaFinal = true;
					return;
				}
			}
			if (!replicaFinal) {
				this.getTramite().setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
				this.getTramiteService().actualizarTramite(this.getTramite());
			}
		}
	}

	public boolean isValidaGuardarEV() {
		return validaGuardarEV;
	}

	public void setValidaGuardarEV(boolean validaGuardarEV) {
		this.validaGuardarEV = validaGuardarEV;
	}

	public boolean isBanderaRectAreaConstruccion() {
		return banderaRectAreaConstruccion;
	}

	public void setBanderaRectAreaConstruccion(boolean banderaRectAreaConstruccion) {
		this.banderaRectAreaConstruccion = banderaRectAreaConstruccion;
	}

	public boolean isTramiteValidacion() {
		return tramiteValidacion;
	}

	public void setTramiteValidacion(boolean tramiteValidacion) {
		this.tramiteValidacion = tramiteValidacion;
	}

	public boolean isBanderaTramitePrimeraSinInscripcionYDecretos() {
		return banderaTramitePrimeraSinInscripcionYDecretos;
	}

	public void setBanderaTramitePrimeraSinInscripcionYDecretos(boolean banderaTramitePrimeraSinInscripcionYDecretos) {
		this.banderaTramitePrimeraSinInscripcionYDecretos = banderaTramitePrimeraSinInscripcionYDecretos;
	}

	// @modified by leidy.gonzalez:: ::15/06/2016:: Se agrega variable boolean
	// para que active o desactive la ficha matriz en las rectificaciones
	// masivas
	public boolean[] getTabs() {

		if (this.tramite.isCancelacionMasiva()) {
			if (this.predioSeleccionado != null && this.predioSeleccionado.isEsPredioFichaMatriz()
					&& this.fichaMatrizSeleccionada != null) {
				for (int i = 0; i < this.tabs.length; i++) {
					this.tabs[i] = false;
				}
				tabs[1] = true;
				tabs[7] = true;
				return this.tabs;
			}
		}

		if (this.tramite.isEnglobeVirtual()) {
			if (this.predioSeleccionado != null && this.predioSeleccionado.isEsPredioFichaMatriz()) {
				for (int i = 0; i < this.tabs.length; i++) {
					this.tabs[i] = false;
				}
				tabs[1] = true;
				tabs[7] = true;
				return this.tabs;
			}
		}

		// Histórico
		tabs[0] = this.predioSeleccionado != null && this.isValidacion();
		// Entrada (Proyeccion)
		tabs[1] = ((this.configuracion.getProyeccionTramite() != null
				&& this.configuracion.getProyeccionTramite().equals("SI")) || this.banderaCancelacionDobleInscripcion)
				&& (!this.rcMB.isDhAreaTotal() || this.tramite.isDesenglobeEtapas()
						|| this.tramite.isCancelacionMasiva())
				&& !this.tramite.isRevisionAvaluo();
		// Rectificacion Area (Proyeccion)
		tabs[2] = (this.rcMB.isDhAreaTotal() || this.tramite.isRectificacionZona()) && !tabs[1]
				&& !this.tramite.isRevisionAvaluo() && !this.tramite.isDesenglobeEtapas() // felipe.cadena:: #13167
				// :: 23-06-2015
				// ::siempre se muestra
				// el tab para
				// rectificaciones sobre
				// la ficha matriz
				&& !this.tramite.isRectificacionMatriz();// javier.aponte::#13320::07-07-2015::La
		// rectificación
		// matriz la muestra
		// en entrada
		// proyección
		// Gubernativa (Proyeccion)
		tabs[3] = this.isMostrarProyeccionTramiteViaGubernativa();
		// Propietarios poseedores
		tabs[4] = this.configuracion.getPropietarios() != null && this.configuracion.getPropietarios().equals("SI")
				&& this.rcMB.isHabilitarTabRectificacion() && this.predioSeleccionado != null
				&& !this.isBanderaCancelacionDobleInscripcion()
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !this.tramite.isRectificacionZona();
		// Justificacion de propiedad
		tabs[5] = this.configuracion.getJustificacionPropiedad() != null
				&& this.configuracion.getJustificacionPropiedad().equals("SI")
				&& this.rcMB.isHabilitarTabRectificacion() && !this.isBanderaCancelacionDobleInscripcion()
				&& this.predioSeleccionado != null
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !this.tramite.isRectificacionZona();
		// Datos generales
		tabs[6] = this.configuracion.getUbicacion() != null && this.configuracion.getUbicacion().equals("SI")
				&& this.rcMB.isHabilitarTabRectificacion() && this.predioSeleccionado != null
				&& !this.isBanderaCancelacionDobleInscripcion() // && !(this.isCondicion8o9() &&
																// this.tramite.isQuintaOmitido())
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !this.tramite.isRectificacionZona();
		// Ficha Matriz PH / Condominio
		tabs[7] = this.configuracion.getFichaMatriz() != null && this.configuracion.getFichaMatriz().equals("SI")
				&& this.isCondicion6o8o9() && this.fichaMatrizSeleccionada != null
				&& this.mostrarFichaMatrizRectificacion;
		// Detalle avaluo
		tabs[8] = this.configuracion.getAvaluos() != null && this.configuracion.getAvaluos().equals("SI")
				&& this.rcMB.isHabilitarTabRectificacion() && this.predioSeleccionado != null
				&& !this.isBanderaCancelacionDobleInscripcion()
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz());
		// Inscripciones y decretos
		// v1.1.9
		tabs[9] = (this.configuracion.getInscripcionesDecretos() != null
				&& this.configuracion.getInscripcionesDecretos().equals("SI") && this.rcMB.isHabilitarTabRectificacion()
				&& this.predioSeleccionado != null
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz()))
				&& (((!this.isBanderaConfirmaAvaluo() && this.tramite.getSolicitud().isRevisionAvaluo()))
						|| ((this.predioSeleccionado.getCancelaInscribe() != null
								&& this.predioSeleccionado.getCancelaInscribe().equals("M")) || this.tramite.isPrimera()
								|| this.tramite.isSegunda() || this.tramite.isCuarta() || this.tramite.isQuinta()
								|| this.tramite.isRectificacion() || this.tramite.isCancelacionPredio()))
				&& (this.isBanderaAceptaCancelacionDobleInscripcion() || !this.isBanderaCancelacionDobleInscripcion())
				&& (!this.banderaPredioFiscal || this.tramite.isQuinta() || this.tramite.isRectificacion()
						|| this.tramite.isCancelacionPredio());
		// Texto motivado
		tabs[10] = ((this.configuracion.getTextoMotivado() != null && this.configuracion.getTextoMotivado().equals("SI")
				&& this.rcMB.isHabilitarTabRectificacion() && this.predioSeleccionado != null
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz()))
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				|| this.tramite.isViaGubernativa())
				&& (this.isBanderaAceptaCancelacionDobleInscripcion() || !this.isBanderaCancelacionDobleInscripcion());
		// Asociar documentos
		tabs[11] = ((this.predioSeleccionado != null && this.rcMB.isHabilitarTabRectificacion()
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado.isEsPredioFichaMatriz()))
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isQuintaMasivo() && this.predioSeleccionado.isEsPredioFichaMatriz())
				|| this.tramite.isViaGubernativa())
				&& (this.isBanderaAceptaCancelacionDobleInscripcion() || !this.isBanderaCancelacionDobleInscripcion());
		// Comparativo geografico
		tabs[12] = this.imagenAntes != null && this.imagenDespues != null
				&& !(this.tramite.isTerceraMasiva() && this.predioSeleccionado != null
						&& this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isRectificacionMatriz() && this.predioSeleccionado != null
						&& this.predioSeleccionado.isEsPredioFichaMatriz())
				&& !(this.tramite.isSegundaDesenglobe() && this.predioSeleccionado != null
						&& this.predioSeleccionado.isEsPredioFichaMatriz());

		return tabs;
	}

	public void setTabs(boolean[] tabs) {
		this.tabs = tabs;
	}

	public boolean isBanderaQuintaMejoraFechaAct() {
		return banderaQuintaMejoraFechaAct;
	}

	public void setBanderaQuintaMejoraFechaAct(boolean banderaQuintaMejoraFechaAct) {
		this.banderaQuintaMejoraFechaAct = banderaQuintaMejoraFechaAct;
	}

	public List<SelectItem> getFechasInscripcionMejoras() {
		return fechasInscripcionMejoras;
	}

	public void setFechasInscripcionMejoras(List<SelectItem> fechasInscripcionMejoras) {
		this.fechasInscripcionMejoras = fechasInscripcionMejoras;
	}

	public boolean isBanderaQuintaOmitidoMejora() {
		return banderaQuintaOmitidoMejora;
	}

	public void setBanderaQuintaOmitidoMejora(boolean banderaQuintaOmitidoMejora) {
		this.banderaQuintaOmitidoMejora = banderaQuintaOmitidoMejora;
	}

	public List<UnidadConstruccion> getUnidadesConsVigentesDesenglobeMejora() {
		return unidadesConsVigentesDesenglobeMejora;
	}

	public void setUnidadesConsVigentesDesenglobeMejora(List<UnidadConstruccion> unidadesConsVigentesDesenglobeMejora) {
		this.unidadesConsVigentesDesenglobeMejora = unidadesConsVigentesDesenglobeMejora;
	}

	public List<UnidadConstruccion> getUnidadesConsNoConvencionalesVigentesDesenglobeMejora() {
		return unidadesConsNoConvencionalesVigentesDesenglobeMejora;
	}

	public void setUnidadesConsNoConvencionalesVigentesDesenglobeMejora(
			List<UnidadConstruccion> unidadesConsNoConvencionalesVigentesDesenglobeMejora) {
		this.unidadesConsNoConvencionalesVigentesDesenglobeMejora = unidadesConsNoConvencionalesVigentesDesenglobeMejora;
	}

	public boolean isValidacionViaGubernativa() {
		return validacionViaGubernativa;
	}

	public List<HistoricoProyeccionViaGubernativaDTO> getListaTramitesHistoricoProyeccion() {
		return listaTramitesHistoricoProyeccion;
	}

	public boolean isBanderaAvaluoManualFiscales() {
		return banderaAvaluoManualFiscales;
	}

	public void setBanderaAvaluoManualFiscales(boolean banderaAvaluoManualFiscales) {
		this.banderaAvaluoManualFiscales = banderaAvaluoManualFiscales;
	}

	public double getValorMetroFiscal() {
		return valorMetroFiscal;
	}

	public void setValorMetroFiscal(double valorMetroFiscal) {
		this.valorMetroFiscal = valorMetroFiscal;
	}

	public boolean isBanderaAvaluoSoloArea() {
		return banderaAvaluoSoloArea;
	}

	public void setBanderaAvaluoSoloArea(boolean banderaAvaluoSoloArea) {
		this.banderaAvaluoSoloArea = banderaAvaluoSoloArea;
	}

	public boolean isPredioFiscalQuintaNuevo() {
		return predioFiscalQuintaNuevo;
	}

	public void setPredioFiscalQuintaNuevo(boolean predioFiscalQuintaNuevo) {
		this.predioFiscalQuintaNuevo = predioFiscalQuintaNuevo;
	}

	public boolean isValidacionQuintaMasivoTodos() {
		return validacionQuintaMasivoTodos;
	}

	public void setValidacionQuintaMasivoTodos(boolean validacionQuintaMasivoTodos) {
		this.validacionQuintaMasivoTodos = validacionQuintaMasivoTodos;
	}

	/**
	 * Determinar los tipos de predios relacionados con el englobe
	 *
	 * @author felipe.cadena
	 */
	public void determinarTipoEngloble() {

		int aux = 0;
		int numPredios = this.tramite.getPredios().size();
		for (Predio predio : this.tramite.getPredios()) {
			if (predio.isMejora()) {
				aux++;
			}
		}

		if (aux == numPredios) {
			this.banderaSoloMejorasEnglobe = true;
			this.banderaMejorasTerrenoEnglobe = false;
		} else if (aux > 0) {
			this.banderaSoloMejorasEnglobe = false;
			this.banderaMejorasTerrenoEnglobe = true;
		} else {
			this.banderaSoloMejorasEnglobe = false;
			this.banderaMejorasTerrenoEnglobe = false;
		}
	}

	/**
	 * Metodo para inicializar las fechas de inscripciòn catastral para los tràmites
	 * de quinta omitido
	 */
	public void cargarFechasQuintaMejora() {

		this.fechasInscripcionMejoras = new LinkedList<SelectItem>();
		Date fechaUltimaActualizacion;
		String zona;

		zona = this.tramite.getNumeroPredial().substring(0, 7);
		fechaUltimaActualizacion = this.getConservacionService().obtenerVigenciaUltimaActualizacionPorZona(zona);

		SelectItem fechaVigencia = new SelectItem();

		String fecha;
		SimpleDateFormat formatoFecha = new SimpleDateFormat(Constantes.FORMATO_FECHA_CALENDAR);
		fecha = formatoFecha.format(fechaUltimaActualizacion);
		fechaVigencia.setLabel(fecha);
		fechaVigencia.setValue(fecha);
		this.fechasInscripcionMejoras.add(fechaVigencia);

		this.banderaQuintaMejoraFechaAct = true;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Método que consulta la ficha matriz si ya ha sido creada.
	 *
	 * @author david.cifuentes
	 */
	public void consultarFichaMatriz() {
		try {

			Long idPredioFM = 0L;
			String nombrePredioFichaMatrizTemp = "";
			String matriculaPredioFichaMatrizTemp = "";
			CirculoRegistral circuloRegistralFichaMatrizTemp = null;
			this.existenModelosBool = false;

			// EntradaProyeccionMB ep = (EntradaProyeccionMB)
			// UtilidadesWeb.getManagedBean("entradaProyeccion");
			boolean crearManzanas;
			if (this.tramite.getManzanasNuevas() == null) {
				crearManzanas = false;
			} else {
				crearManzanas = this.tramite.getManzanasNuevas() > 0;
			}

			if (this.pPrediosDesenglobe != null && !this.pPrediosDesenglobe.isEmpty()) {
				if (crearManzanas) {

					idPredioFM = this.predioSeleccionado.getId();
				} else {
					for (PPredio pp : this.pPrediosDesenglobe) {
						if (pp.isEsPredioFichaMatriz()) {
							idPredioFM = pp.getId();
							nombrePredioFichaMatrizTemp = pp.getNombre();
							matriculaPredioFichaMatrizTemp = pp.getNumeroRegistro();
							circuloRegistralFichaMatrizTemp = pp.getCirculoRegistral();
							break;
						}
					}
				}

				if (idPredioFM.longValue() != 0L) {
					if (this.tramite.isEnglobeVirtual()) {
						Long fichaMatrizId = this.tramite.getFichaMatrizPredioTerrenos().get(0).getFichaMatriz()
								.getId();
						this.fichaMatrizSeleccionada = this.getConservacionService()
								.buscarFichaPorNumeroPredialProyectada(this.tramite
										.obtenerInfoCampoAdicional(EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV));
					} else {
						this.fichaMatrizSeleccionada = this.getConservacionService()
								.buscarPFichaMatrizPorPredioId(idPredioFM);
						this.verificarTorresEnFirme();
					}
					// Set de la matricula inmobiliaria y el nombre del predio
					// ficha matriz.
					this.matriculaPredioFichaMatriz = matriculaPredioFichaMatrizTemp;
					this.nombrePredioFichaMatriz = nombrePredioFichaMatrizTemp;
					this.circuloRegistralFichaMatriz = circuloRegistralFichaMatrizTemp;

					// Revisar si la ficha matriz tiene modelos asociados.
					if (this.fichaMatrizSeleccionada != null
							&& this.fichaMatrizSeleccionada.getPFichaMatrizModelos() != null
							&& this.fichaMatrizSeleccionada.getPFichaMatrizModelos().size() > 0) {
						this.existenModelosBool = true;
					}
				}
			}

			// felipe.cadena::#13167::23-06-2015::carga la ficha matriz para
			// tramites de rectificacion matriz
			if (this.tramite.isRectificacionMatriz()) {
				this.fichaMatrizSeleccionada = this.getConservacionService()
						.buscarPFichaMatrizPorPredioId(this.tramite.getPredio().getId());
				this.verificarTorresEnFirme();
			}

			// javier.aponte::#13168::26-06-2015::carga la ficha matriz para
			// tramites de tercera ph condominio másivos
			if (this.tramite.isTerceraMasiva()) {
				this.fichaMatrizSeleccionada = this.getConservacionService()
						.buscarPFichaMatrizPorPredioId(this.tramite.getPredio().getId());
				this.verificarTorresEnFirme();
				for (PPredio pp : this.pPrediosTerceraRectificacionMasiva) {
					if (pp.isEsPredioFichaMatriz()) {
						idPredioFM = pp.getId();
						nombrePredioFichaMatrizTemp = pp.getNombre();
						matriculaPredioFichaMatrizTemp = pp.getNumeroRegistro();
						circuloRegistralFichaMatrizTemp = pp.getCirculoRegistral();
						break;
					}
				}

				// Set de la matricula inmobiliaria y el nombre del predio
				// ficha matriz.
				this.matriculaPredioFichaMatriz = matriculaPredioFichaMatrizTemp;
				this.nombrePredioFichaMatriz = nombrePredioFichaMatrizTemp;
				this.circuloRegistralFichaMatriz = circuloRegistralFichaMatrizTemp;

				// Revisar si la ficha matriz tiene modelos asociados.
				if (this.fichaMatrizSeleccionada != null
						&& this.fichaMatrizSeleccionada.getPFichaMatrizModelos() != null
						&& this.fichaMatrizSeleccionada.getPFichaMatrizModelos().size() > 0) {
					this.existenModelosBool = true;
				}
			}

			// lorena.salamanca :: 24-08-2015 :: Quinta Masivo.
			if (this.tramite.isQuintaMasivo()) {
				this.fichaMatrizSeleccionada = this.getConservacionService()
						.buscarPFichaMatrizPorPredioId(this.tramite.getPredio().getId());

				this.verificarTorresEnFirme();

				if (this.pPrediosQuintaMasivo == null || this.pPrediosQuintaMasivo.isEmpty()) {
					this.cargarPrediosQuintaMasivo();
				}

				if (this.pPrediosQuintaMasivo != null) {

					for (PPredio pp : this.pPrediosQuintaMasivo) {
						if (pp.isEsPredioFichaMatriz()) {
							idPredioFM = pp.getId();
							nombrePredioFichaMatrizTemp = pp.getNombre();
							matriculaPredioFichaMatrizTemp = pp.getNumeroRegistro();
							circuloRegistralFichaMatrizTemp = pp.getCirculoRegistral();
							break;
						}
					}

					// Set de la matricula inmobiliaria y el nombre del predio
					// ficha matriz.
					this.matriculaPredioFichaMatriz = matriculaPredioFichaMatrizTemp;
					this.nombrePredioFichaMatriz = nombrePredioFichaMatrizTemp;
					this.circuloRegistralFichaMatriz = circuloRegistralFichaMatrizTemp;

					// Los predios originales del trámite deben quedar con
					// cancela/inscribe en M
					// modified by leidy.gonzalez:: #40580:: Se comenta validacion de que los
					// predios
					// de quinta masivo que sean originales del tramite se coloquen en
					// cancela/inscribe
					// en Modifica
//					for (PPredio pp : this.pPrediosQuintaMasivo) {
//						if (pp.isPredioOriginalTramite()
//								&& pp.getCancelaInscribe() == null) {
//							pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
//									.getCodigo());
//							this.getConservacionService().actualizarPPredio(pp);
//						}
//					}
				}

				// Revisar si la ficha matriz tiene modelos asociados.
				if (this.fichaMatrizSeleccionada != null
						&& this.fichaMatrizSeleccionada.getPFichaMatrizModelos() != null
						&& this.fichaMatrizSeleccionada.getPFichaMatrizModelos().size() > 0) {
					this.existenModelosBool = true;
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	/**
	 * Determina si un tramite ya ha sido transferido a un digitalizador.
	 *
	 * @author felipe.cadena
	 *
	 */
	public void determinarDigitalizador() {
		TramiteDigitalizacion td;

		try {
			td = this.getTramiteService().buscarTramiteDigitalizacionPorEjecutor(this.usuario.getLogin(),
					this.tramite.getId());
		} catch (Exception e) {
			td = null;
		}

		if (td == null) {
			this.banderaDigitalizacionAsignada = false;
		} else {
			this.banderaDigitalizacionAsignada = true;
			this.digitalizadorAsignado = td.getUsuarioDigitaliza();
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que valida si en la edición geográfica se logró actualizar el valor
	 * por M2 de las zonas de los predios proyectados. Si en alguna de estas zonas
	 * el valor por M2 es '-1' ocurrio un error y se deben arreglar los datos para
	 * poder continuar con la ejecucion del trámite.
	 *
	 * @return
	 */
	public void validarPredioZonasValorM2() {

		List<PPredio> prediosProy = this.getConservacionService()
				.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
		if (prediosProy != null && !prediosProy.isEmpty()) {
			for (PPredio pp : prediosProy) {
				if (pp.getPPredioZonas() != null && !pp.getPPredioZonas().isEmpty()) {
					for (PPredioZona ppz : pp.getPPredioZonas()) {
						// si el valor por m2 es -1 ocurrio un error de datos al
						// calcular
						if (ppz.getCancelaInscribe() != null
								&& !ppz.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())
								&& ppz.getValorM2Terreno().intValue() == -1) {

							this.addMensajeError("Durante la edición geográfica no fue posible el "
									+ "cálculo del valor por metro cuadrado para alguna de las zonas "
									+ "para los predios proyectados. Por favor revise los datos y "
									+ "recalcule estos valores para continuar la ejecución del trámite.");
							this.valorM2zonaFlag = false;
							break;
						}
					}
					if (!this.valorM2zonaFlag) {
						break;
					}
				}
			}
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que valida que no existan errores en las zonas (que no se encuentren
	 * -1 en ninguno de los valores)
	 *
	 * @author franz.gamba
	 * @return
	 */
	public boolean isErrorEnZonas() {
		boolean error = false;

		List<PPredio> prediosProy = this.getConservacionService()
				.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
		if (prediosProy != null && !prediosProy.isEmpty()) {
			for (PPredio pp : prediosProy) {
				if (pp.getPPredioZonas() != null && !pp.getPPredioZonas().isEmpty()) {
					for (PPredioZona ppz : pp.getPPredioZonas()) {

						if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(ppz.getCancelaInscribe())
								&& (ppz.getValorM2Terreno().intValue() == -1 || "-1".equals(ppz.getZonaFisica())
										|| ppz.getZonaGeoeconomica().equals("-1"))) {
							error = true;
							break;
						}
					}
					if (error) {
						break;
					}
				}
			}
		}
		return error;
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que inicializa la ficha matriz, para los trámites de segunda
	 * desenglobe en el que la condicion de propiedad del predio es 8 o 9.
	 *
	 * @author david.cifuentes
	 */
	public void inicializarFichaMatriz() {
		try {

			consultarFichaMatriz();

			// En caso que no se tenga ficha matriz en el momento
			// actual.
			if (this.fichaMatrizSeleccionada == null) {
				this.fichaMatrizSeleccionada = new PFichaMatriz();
				this.fichaMatrizSeleccionada.setAreaTotalConstruidaComun(0D);
				this.fichaMatrizSeleccionada.setAreaTotalConstruidaPrivada(0D);
				this.fichaMatrizSeleccionada.setAreaTotalTerrenoComun(0D);
				this.fichaMatrizSeleccionada.setAreaTotalTerrenoPrivada(0D);
				this.fichaMatrizSeleccionada.setPPredio(this.predioSeleccionado);
				this.fichaMatrizSeleccionada.setValorTotalAvaluoCatastral(0D);
				this.fichaMatrizSeleccionada.setTotalUnidadesSotanos(0L);
				this.fichaMatrizSeleccionada.setFechaLog(new Date());
				this.fichaMatrizSeleccionada.setUsuarioLog(MenuMB.getMenu().getUsuarioDto().getLogin());

				this.fichaMatrizSeleccionada = this.getConservacionService()
						.guardarActualizarPFichaMatriz(this.fichaMatrizSeleccionada);
				this.verificarTorresEnFirme();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	// ------------------------------------------------------------ //
	/**
	 * Método que inicializa la ficha matriz, para los trámites de segunda englobe y
	 * desenglobe.
	 *
	 * @author david.cifuentes
	 */
	public List<PPredio> cargarCoeficientesCopropiedadPrediosFichaMatriz(List<PPredio> predios) {

		try {
			if (this.fichaMatrizSeleccionada != null && this.fichaMatrizSeleccionada.getPFichaMatrizPredios() != null
					&& !this.fichaMatrizSeleccionada.getPFichaMatrizPredios().isEmpty()) {
				for (PFichaMatrizPredio pfmp : this.fichaMatrizSeleccionada.getPFichaMatrizPredios()) {
					for (PPredio pp : predios) {
						if (pfmp.getNumeroPredial().equals(pp.getNumeroPredial())) {
							pp.setCoeficienteCopropiedadFM(pfmp.getCoeficiente());
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return predios;
	}

	// --------------------------------------------------------------------- //
	/**
	 * Se obtiene en predioSeleccionado el predio asociado al trámite con todos sus
	 * atributos y objetos dependientes incluido el avaluo. NO USAR EN
	 * DESENGLOBE!!!!
	 *
	 * @throws Exception
	 */
	private void obtenerPredioCompletoYAvaluo() throws Exception {

		this.predioEnTramite = false;
		if ((this.tramite.getPredio() != null && !this.tramite.isSegunda()) || this.tramite.isQuintaNuevo()
				|| this.tramite.isQuintaOmitidoNuevo()) {
			if (this.tramite.isQuintaNuevo() || this.tramite.isQuintaOmitidoNuevo()) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoByIdTramite(this.tramite.getId());
			} else {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoByIdyTramite(this.tramite.getPredio().getId(), this.tramite.getId());

			}
			if (this.predioSeleccionado == null && !this.tramite.isQuinta() && !this.tramite.isSegundaDesenglobe()
					&& !this.tramite.isViaGubernativa()) {
				LOGGER.error("No se encontró el predio asociado al trámite seleccionado PredioId:"
						+ tramite.getPredio().getId() + " TramiteId:" + tramite.getId());
				String mensaje = "No se encontró el predio asociado al trámite seleccionado";
				this.addMensajeError(mensaje);
				this.mensajePredioEnTramite = mensaje;
				this.predioEnTramite = true;
			}

			if (this.tramite.isCuarta()) {
				Calendar c = Calendar.getInstance();
				c.set(Calendar.DATE, 31);
				c.set(Calendar.MONTH, 11);
				Date d = c.getTime();
				d.setYear(this.tramite.getFechaRadicacion().getYear());
				if (this.predioSeleccionado != null) {
					this.predioSeleccionado.setFechaInscripcionCatastral(d);
				}
			}

		} else if (this.tramite.isSegundaEnglobe()) {
			this.predioSeleccionado = this.getConservacionService()
					.obtenerPPredioCompletoByIdTramite(this.tramite.getId());
		} else if (this.tramite.isSegundaDesenglobe()) {
			List<PPredio> prediosResultantes = this.getpPrediosDesenglobe();
			// Se toma como seleccionado el primero no cancelado que aparezca
			if (prediosResultantes != null && !prediosResultantes.isEmpty()) {
				for (PPredio p : prediosResultantes) {
					if (p.getCancelaInscribe() == null
							|| !p.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
						predioSeleccionado = p;
						break;
					}
				}
			}
			if (this.predioSeleccionado != null) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
			} else if (this.tramite.getPredio() != null && !this.tramite.isSegundaDesenglobe()) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.tramite.getPredio().getId());
			}
		}

		/*
		 * Traer avaluo para el predio seleccionado independente del tramite
		 */
		setAvaluo();
		if (this.predioSeleccionado != null) {
			this.predioSeleccionado.setSelected(true);
			this.predioOrigen = this.getConservacionService()
					.obtenerPredioConPersonasPorPredioId(this.predioSeleccionado.getId());
			if (this.predioOrigen != null) {
				this.unidadesConstruccionConvencional = this.getUnidadesConstruccionConvencionalNoCanceladas();
				this.unidadesConstruccionNoConvencional = this.getUnidadesConstruccionNoConvencionalNoCanceladas();

				// ref#19669 :: Carlos Ferro :: Asocia construcciones automaticmente si es
				// segunda englobe
				if (this.tramite.isSegundaEnglobe()) {
					if (this.unidadesConstruccionConvencional != null) {
						unidadesConstruccionConvencionalSeleccionadas = unidadesConstruccionConvencional
								.toArray(new UnidadConstruccion[unidadesConstruccionConvencional.size()]);
					}
					if (this.unidadesConstruccionNoConvencional != null) {
						unidadesConstruccionNoConvencionalSeleccionadas = unidadesConstruccionNoConvencional
								.toArray(new UnidadConstruccion[unidadesConstruccionNoConvencional.size()]);
					}

					this.asociarConstrucciones();
				}

			} else {
				this.unidadesConstruccionConvencional = new ArrayList<UnidadConstruccion>();
				this.unidadesConstruccionNoConvencional = new ArrayList<UnidadConstruccion>();
			}
		}
	}

	/**
	 * Método que recupera las unidades de construcción convencionales que no hayan
	 * sido canceladas
	 *
	 * @author javier.aponte
	 * @modified by leidy.gonzalez 24/09/2014 Se agrega validación para verificar
	 *           que las unidades de construccion convencional no sean nulas
	 * @return
	 */
	public List<UnidadConstruccion> getUnidadesConstruccionConvencionalNoCanceladas() {
		ArrayList<UnidadConstruccion> unidadesConst = new ArrayList<UnidadConstruccion>();
		if (this.predioOrigen.getUnidadesConstruccionConvencional() != null) {
			for (UnidadConstruccion uni : this.predioOrigen.getUnidadesConstruccionConvencional()) {
				if (uni.getAnioCancelacion() == null) {
					unidadesConst.add(uni);
				}
			}
		}
		return unidadesConst;
	}

	/**
	 * Método que recupera las unidades de construcción convencionales que no hayan
	 * sido canceladas
	 *
	 * @author javier.aponte
	 * @return
	 */
	public List<UnidadConstruccion> getUnidadesConstruccionNoConvencionalNoCanceladas() {
		ArrayList<UnidadConstruccion> unidadesConst = new ArrayList<UnidadConstruccion>();
		for (UnidadConstruccion uni : this.predioOrigen.getUnidadesConstruccionNoConvencional()) {
			if (uni.getAnioCancelacion() == null) {
				unidadesConst.add(uni);
			}
		}
		return unidadesConst;
	}

	public List<UnidadConstruccion> getUnidadesConstruccionConvencionalFM() {
		return unidadesConstruccionConvencionalFM;
	}

	public void setUnidadesConstruccionConvencionalFM(List<UnidadConstruccion> unidadesConstruccionConvencionalFM) {
		this.unidadesConstruccionConvencionalFM = unidadesConstruccionConvencionalFM;
	}

	public List<UnidadConstruccion> getUnidadesConstruccionNoConvencionalFM() {
		return unidadesConstruccionNoConvencionalFM;
	}

	public void setUnidadesConstruccionNoConvencionalFM(List<UnidadConstruccion> unidadesConstruccionNoConvencionalFM) {
		this.unidadesConstruccionNoConvencionalFM = unidadesConstruccionNoConvencionalFM;
	}

	public UnidadConstruccion[] getUnidadesConstruccionConvencionalSeleccionadasFM() {
		return unidadesConstruccionConvencionalSeleccionadasFM;
	}

	public void setUnidadesConstruccionConvencionalSeleccionadasFM(
			UnidadConstruccion[] unidadesConstruccionConvencionalSeleccionadasFM) {
		this.unidadesConstruccionConvencionalSeleccionadasFM = unidadesConstruccionConvencionalSeleccionadasFM;
	}

	public UnidadConstruccion[] getUnidadesConstruccionNoConvencionalSeleccionadasFM() {
		return unidadesConstruccionNoConvencionalSeleccionadasFM;
	}

	public void setUnidadesConstruccionNoConvencionalSeleccionadasFM(
			UnidadConstruccion[] unidadesConstruccionNoConvencionalSeleccionadasFM) {
		this.unidadesConstruccionNoConvencionalSeleccionadasFM = unidadesConstruccionNoConvencionalSeleccionadasFM;
	}

	public List<PUnidadConstruccion> getUnidadesConsNuevasDesenglobeMejora() {
		return unidadesConsNuevasDesenglobeMejora;
	}

	public void setUnidadesConsNuevasDesenglobeMejora(List<PUnidadConstruccion> unidadesConsNuevasDesenglobeMejora) {
		this.unidadesConsNuevasDesenglobeMejora = unidadesConsNuevasDesenglobeMejora;
	}

	// --------------------------------------------------------------------- //
	/**
	 * Escoge de los diferentes registros de avaluo, el vigente
	 */
	public void setAvaluo() {
		Date vigencia = null;
		Date hoy = new Date();
		this.avaluo = new PPredioAvaluoCatastral();
		if (this.predioSeleccionado != null && predioSeleccionado.getPPredioAvaluoCatastrals() != null) {
			for (PPredioAvaluoCatastral pac : predioSeleccionado.getPPredioAvaluoCatastrals()) {
				if (vigencia == null) {
					this.avaluo = pac;
					vigencia = this.avaluo.getVigencia();
				} else {
					if (this.tramite.isCuarta()) {
						if ((pac.getVigencia().after(vigencia) && pac.getVigencia().before(hoy)) || (pac.getVigencia()
								.after(vigencia) && pac.getVigencia().after(hoy)
								&& EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pac.getCancelaInscribe()))) {
							this.avaluo = pac;
							vigencia = this.avaluo.getVigencia();
						}
					} else {
						if (pac.getVigencia().after(vigencia) && pac.getVigencia().before(hoy)) {
							this.avaluo = pac;
							vigencia = this.avaluo.getVigencia();
						}
					}
				}
			}
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que determina la selección por defecto en la pantalla de proyección.
	 *
	 * @author fabio.navarrete
	 */
	private void selectDefaultTab() {
		if (this.tramite.isSegunda() || this.tramite.isQuinta()) {
			if (this.predioSeleccionado != null && this.isValidacion()) {
				this.tab = 1;
			} else {
				this.tab = 0;
			}
		} else if (this.tramite.isPrimera()) {
			this.tab = 1;
		}
	}

	public String getCodigoPostalDireccion() {
		return this.codigoPostalDireccion;
	}

	public void setCodigoPostalDireccion(String codigoPostalDireccion) {
		this.codigoPostalDireccion = codigoPostalDireccion;
	}

	public Predio getPredioOrigen() {
		return predioOrigen;
	}

	public void setPredioOrigen(Predio predioOrigen) {
		this.predioOrigen = predioOrigen;
	}

	public boolean isBanderaConstruccionConvencional() {
		return banderaConstruccionConvencional;
	}

	public void setBanderaConstruccionConvencional(boolean banderaConstruccionConvencional) {
		this.banderaConstruccionConvencional = banderaConstruccionConvencional;
	}

	public boolean isBanderaConfirmaAvaluo() {
		return this.banderaConfirmaAvaluo;
	}

	public void setBanderaConfirmaAvaluo(boolean banderaConfirmaAvaluo) {
		this.banderaConfirmaAvaluo = banderaConfirmaAvaluo;
	}

	public Actividad getActividad() {
		return this.actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}

	public Object[] getDigitalizadorSeleccionado() {
		return digitalizadorSeleccionado;
	}

	public void setDigitalizadorSeleccionado(Object[] digitalizadorSeleccionado) {
		this.digitalizadorSeleccionado = digitalizadorSeleccionado;
	}

	public List<Object[]> getDigitalizadores() {
		return digitalizadores;
	}

	public void setDigitalizadores(List<Object[]> digitalizadores) {
		this.digitalizadores = digitalizadores;
	}

	public boolean isBanderaRectificacionZonas() {
		return banderaRectificacionZonas;
	}

	public void setBanderaRectificacionZonas(boolean banderaRectificacionZonas) {
		this.banderaRectificacionZonas = banderaRectificacionZonas;
	}

	public List<SelectItem> getTiposCambioZona() {
		return tiposCambioZona;
	}

	public void setTiposCambioZona(List<SelectItem> tiposCambioZona) {
		this.tiposCambioZona = tiposCambioZona;
	}

	public int getTipoCambioZona() {
		return tipoCambioZona;
	}

	public void setTipoCambioZona(int tipoCambioZona) {
		this.tipoCambioZona = tipoCambioZona;
	}

	public boolean isExistenConstruccionesVigentesBool() {
		return this.existenConstruccionesVigentesBool;
	}

	public void setExistenConstruccionesVigentesBool(boolean existenConstruccionesVigentesBool) {
		this.existenConstruccionesVigentesBool = existenConstruccionesVigentesBool;
	}

	public HashMap<ESeccionEjecucion, Boolean> getValidezSeccion() {
		return this.validezSeccion;
	}

	public boolean isPredioEnTramite() {
		return predioEnTramite;
	}

	public void setPredioEnTramite(boolean predioEnTramite) {
		this.predioEnTramite = predioEnTramite;
	}

	public boolean isAnexoONuevaUnidadDelModelo() {
		return isAnexoONuevaUnidadDelModelo;
	}

	public void setAnexoONuevaUnidadDelModelo(boolean isAnexoONuevaUnidadDelModelo) {
		this.isAnexoONuevaUnidadDelModelo = isAnexoONuevaUnidadDelModelo;
	}

	public String getImagenAntes() {
		return imagenAntes;
	}

	public void setImagenAntes(String imagenAntes) {
		this.imagenAntes = imagenAntes;
	}

	public String getImagenDespues() {
		return imagenDespues;
	}

	public void setImagenDespues(String imagenDespues) {
		this.imagenDespues = imagenDespues;
	}

	public boolean isMatriculaVaciaBool() {
		return matriculaVaciaBool;
	}

	public void setMatriculaVaciaBool(boolean matriculaVaciaBool) {
		this.matriculaVaciaBool = matriculaVaciaBool;
	}

	public boolean isAutoavaluo() {
		return this.avaluo != null && this.avaluo.getAutoavaluo() != null
				&& this.avaluo.getAutoavaluo().equals(ESiNo.SI.getCodigo());
	}

	public void setAutoavaluo(boolean autoavaluo) {
		// Logica invertida porque llega el valor antes del click
		if (autoavaluo) {
			this.avaluo.setAutoavaluo(ESiNo.SI.getCodigo());
		} else {
			this.avaluo.setAutoavaluo(ESiNo.NO.getCodigo());
		}
	}

	public UnidadConstruccion[] getUnidadConstruccionConvencionalPredioOriginalSeleccionadas() {
		return unidadConstruccionConvencionalPredioOriginalSeleccionadas;
	}

	public void setUnidadConstruccionConvencionalPredioOriginalSeleccionadas(
			UnidadConstruccion[] unidadConstruccionConvencionalPredioOriginalSeleccionadas) {
		this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = unidadConstruccionConvencionalPredioOriginalSeleccionadas;
	}

	public boolean isMessageError() {
		return messageError;
	}

	public void setMessageError(boolean messageError) {
		this.messageError = messageError;
	}

	public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevas() {
		return unidadConstruccionConvencionalNuevas;
	}

	public void setUnidadConstruccionConvencionalNuevas(
			List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas) {
		this.unidadConstruccionConvencionalNuevas = unidadConstruccionConvencionalNuevas;
	}

	public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevas() {
		return unidadConstruccionNoConvencionalNuevas;
	}

	public void setUnidadConstruccionNoConvencionalNuevas(
			List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas) {
		this.unidadConstruccionNoConvencionalNuevas = unidadConstruccionNoConvencionalNuevas;
	}

	public Double getAreaNoConvencional() {
		return areaNoConvencional;
	}

	public void setAreaNoConvencional(Double areaNoConvencional) {
		this.areaNoConvencional = areaNoConvencional;
	}

	public boolean isTramiteQuintaOmitidoPredioExistente() {
		return this.tramiteQuintaOmitidoPredioExistente;
	}

	public void setTramiteQuintaOmitidoPredioExistente(boolean tramiteQuintaOmitidoPredioExistente) {
		this.tramiteQuintaOmitidoPredioExistente = tramiteQuintaOmitidoPredioExistente;
	}

	public int getPuntajeAnterior() {
		return this.puntajeAnterior;
	}

	public void setPuntajeAnterior(int puntajeAnterior) {
		this.puntajeAnterior = puntajeAnterior;
	}

	public int getPuntajeTotal() {
		return puntajeTotal;
	}

	public void setPuntajeTotal(int puntajeTotal) {
		this.puntajeTotal = puntajeTotal;
	}

	public void setValidezSeccion(HashMap<ESeccionEjecucion, Boolean> validezSeccion) {
		this.validezSeccion = validezSeccion;
	}

	public void setMensajeFotos(String mensajeFotos) {
		this.mensajeFotos = mensajeFotos;
	}

	public String getMensajeFotos() {
		return this.mensajeFotos;
	}

	public void setCurrentCalificacionConstruccionNode(
			CalificacionConstruccionTreeNode currentCalificacionConstruccionNode) {
		this.currentCalificacionConstruccionNode = currentCalificacionConstruccionNode;
	}

	public boolean isExistenModelosBool() {
		return existenModelosBool;
	}

	public void setExistenModelosBool(boolean existenModelosBool) {
		this.existenModelosBool = existenModelosBool;
	}

	public boolean isActivarValidarGuardarBool() {
		return activarValidarGuardarBool;
	}

	public void setActivarValidarGuardarBool(boolean activarValidarGuardarBool) {
		this.activarValidarGuardarBool = activarValidarGuardarBool;
	}

	public CalificacionConstruccionTreeNode getCurrentCalificacionConstruccionNode() {
		return currentCalificacionConstruccionNode;
	}

	public boolean isBanderaBotonAceptarCargarFoto() {
		return banderaBotonAceptarCargarFoto;
	}

	public void setBanderaBotonAceptarCargarFoto(boolean banderaBotonAceptarCargarFoto) {
		this.banderaBotonAceptarCargarFoto = banderaBotonAceptarCargarFoto;
	}

	public List<PModeloConstruccionFoto> getListaPModeloConstruccionFotoAGuardar() {
		return listaPModeloConstruccionFotoAGuardar;
	}

	public void setListaPModeloConstruccionFotoAGuardar(
			List<PModeloConstruccionFoto> listaPModeloConstruccionFotoAGuardar) {
		this.listaPModeloConstruccionFotoAGuardar = listaPModeloConstruccionFotoAGuardar;
	}

	public CirculoRegistral getCirculoRegistralFichaMatriz() {
		return this.circuloRegistralFichaMatriz;
	}

	public void setCirculoRegistralFichaMatriz(CirculoRegistral circuloRegistralFichaMatriz) {
		this.circuloRegistralFichaMatriz = circuloRegistralFichaMatriz;
	}

	public Long getCalificacionAnexoIdSelected() {
		return calificacionAnexoIdSelected;
	}

	public void setCalificacionAnexoIdSelected(Long calificacionAnexoIdSelected) {
		this.calificacionAnexoIdSelected = calificacionAnexoIdSelected;
	}

	public String getMatriculaPredioFichaMatriz() {
		return matriculaPredioFichaMatriz;
	}

	public void setMatriculaPredioFichaMatriz(String matriculaPredioFichaMatriz) {
		this.matriculaPredioFichaMatriz = matriculaPredioFichaMatriz;
	}

	public List<ValidacionProyeccionDelTramiteDTO> getListaValidacionesTramiteSegundaDesenglobe() {
		return this.listaValidacionesTramiteSegundaDesenglobe;
	}

	public void setListaValidacionesTramiteSegundaDesenglobe(
			List<ValidacionProyeccionDelTramiteDTO> listaValidacionesTramiteSegundaDesenglobe) {
		this.listaValidacionesTramiteSegundaDesenglobe = listaValidacionesTramiteSegundaDesenglobe;
	}

	public String getDigitalizadorAsignado() {
		return digitalizadorAsignado;
	}

	public void setDigitalizadorAsignado(String digitalizadorAsignado) {
		this.digitalizadorAsignado = digitalizadorAsignado;
	}

	public List<UnidadConstruccion> getListUnidadesConstruccionConvencionalVigentes() {
		return listUnidadesConstruccionConvencionalVigentes;
	}

	public void setListUnidadesConstruccionConvencionalVigentes(
			List<UnidadConstruccion> listUnidadesConstruccionConvencionalVigentes) {
		this.listUnidadesConstruccionConvencionalVigentes = listUnidadesConstruccionConvencionalVigentes;
	}

	public List<UnidadConstruccion> getListUnidadesConstruccionNoConvencionalVigentes() {
		return listUnidadesConstruccionNoConvencionalVigentes;
	}

	public void setListUnidadesConstruccionNoConvencionalVigentes(
			List<UnidadConstruccion> listUnidadesConstruccionNoConvencionalVigentes) {
		this.listUnidadesConstruccionNoConvencionalVigentes = listUnidadesConstruccionNoConvencionalVigentes;
	}

	public String getNombrePredioFichaMatriz() {
		return nombrePredioFichaMatriz;
	}

	public void setNombrePredioFichaMatriz(String nombrePredioFichaMatriz) {
		this.nombrePredioFichaMatriz = nombrePredioFichaMatriz;
	}

	public UnidadConstruccion[] getUnidadConstruccionNoConvencionalPredioOriginalSeleccionadas() {
		return unidadConstruccionNoConvencionalPredioOriginalSeleccionadas;
	}

	public void setUnidadConstruccionNoConvencionalPredioOriginalSeleccionadas(
			UnidadConstruccion[] unidadConstruccionNoConvencionalPredioOriginalSeleccionadas) {
		this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = unidadConstruccionNoConvencionalPredioOriginalSeleccionadas;
	}

	public boolean isUnidadDelModeloEditModeBool() {
		return this.unidadDelModeloEditModeBool;
	}

	public void setUnidadDelModeloEditModeBool(boolean unidadDelModeloEditModeBool) {
		this.unidadDelModeloEditModeBool = unidadDelModeloEditModeBool;
	}

	public PFichaMatriz getFichaMatrizSeleccionada() {
		return fichaMatrizSeleccionada;
	}

	public void setFichaMatrizSeleccionada(PFichaMatriz fichaMatrizSeleccionada) {
		this.fichaMatrizSeleccionada = fichaMatrizSeleccionada;
	}

	public boolean isBanderaDigitalizacionAsignada() {
		return banderaDigitalizacionAsignada;
	}

	public void setBanderaDigitalizacionAsignada(boolean banderaDigitalizacionAsignada) {
		this.banderaDigitalizacionAsignada = banderaDigitalizacionAsignada;
	}

	public boolean isActualizarAreasFichaMatrizBool() {
		return actualizarAreasFichaMatrizBool;
	}

	public void setActualizarAreasFichaMatrizBool(boolean actualizarAreasFichaMatrizBool) {
		this.actualizarAreasFichaMatrizBool = actualizarAreasFichaMatrizBool;
	}

	public List<UnidadConstruccion> getUnidadConstruccionNoConvencionalPredioOriginal() {
		return unidadConstruccionNoConvencionalPredioOriginal;
	}

	public void setUnidadConstruccionNoConvencionalPredioOriginal(
			List<UnidadConstruccion> unidadConstruccionNoConvencionalPredioOriginal) {
		this.unidadConstruccionNoConvencionalPredioOriginal = unidadConstruccionNoConvencionalPredioOriginal;
	}

	public boolean isTablaExpandidaConstruccionesConvencionalesVigentes() {
		return tablaExpandidaConstruccionesConvencionalesVigentes;
	}

	public void setTablaExpandidaConstruccionesConvencionalesVigentes(
			boolean tablaExpandidaConstruccionesConvencionalesVigentes) {
		this.tablaExpandidaConstruccionesConvencionalesVigentes = tablaExpandidaConstruccionesConvencionalesVigentes;
	}

	public boolean isTablaExpandidaConstruccionesConvencionalesNuevas() {
		return tablaExpandidaConstruccionesConvencionalesNuevas;
	}

	public void setTablaExpandidaConstruccionesConvencionalesNuevas(
			boolean tablaExpandidaConstruccionesConvencionalesNuevas) {
		this.tablaExpandidaConstruccionesConvencionalesNuevas = tablaExpandidaConstruccionesConvencionalesNuevas;
	}

	public boolean isGuardarAreasBool() {
		return guardarAreasBool;
	}

	public void setGuardarAreasBool(boolean guardarAreasBool) {
		this.guardarAreasBool = guardarAreasBool;
	}

	public PModeloConstruccionFoto getCurrentPModeloConstruccionFoto() {
		return currentPModeloConstruccionFoto;
	}

	public void setCurrentPModeloConstruccionFoto(PModeloConstruccionFoto currentPModeloConstruccionFoto) {
		this.currentPModeloConstruccionFoto = currentPModeloConstruccionFoto;
	}

	public List<UnidadConstruccion> getUnidadConstruccionConvencionalPredioOriginal() {
		return unidadConstruccionConvencionalPredioOriginal;
	}

	public void setUnidadConstruccionConvencionalPredioOriginal(
			List<UnidadConstruccion> unidadConstruccionConvencionalPredioOriginal) {
		this.unidadConstruccionConvencionalPredioOriginal = unidadConstruccionConvencionalPredioOriginal;
	}

	public boolean isErrorConfirmarDatosCargarFotografia() {
		return this.errorConfirmarDatosCargarFotografia;
	}

	public void setErrorConfirmarDatosCargarFotografia(boolean errorConfirmarDatosCargarFotografia) {
		this.errorConfirmarDatosCargarFotografia = errorConfirmarDatosCargarFotografia;
	}

	public boolean isBanderaDesenglobeMejora() {
		return banderaDesenglobeMejora;
	}

	public void setBanderaDesenglobeMejora(boolean banderaDesenglobeMejora) {
		this.banderaDesenglobeMejora = banderaDesenglobeMejora;
	}

	public List<PModeloConstruccionFoto> getListPFotosUnidadDelModelo() {
		return listPFotosUnidadDelModelo;
	}

	public void setListPFotosUnidadDelModelo(List<PModeloConstruccionFoto> listPFotosUnidadDelModelo) {
		this.listPFotosUnidadDelModelo = listPFotosUnidadDelModelo;
	}

	public List<CirculoRegistral> getListCirculosRegistrales() {
		return listCirculosRegistrales;
	}

	public void setListCirculosRegistrales(List<CirculoRegistral> listCirculosRegistrales) {
		this.listCirculosRegistrales = listCirculosRegistrales;
	}

	public String getRutaWebImagenBase() {
		return this.rutaWebImagenBase;
	}

	public void setRutaWebImagenBase(String rutaWebImagenBase) {
		this.rutaWebImagenBase = rutaWebImagenBase;
	}

	public boolean isUnidadModeloConstruccionBool() {
		return unidadModeloConstruccionBool;
	}

	public void setUnidadModeloConstruccionBool(boolean unidadModeloConstruccionBool) {
		this.unidadModeloConstruccionBool = unidadModeloConstruccionBool;
	}

	public boolean isActiveAsociarUnidadesBool() {
		return activeAsociarUnidadesBool;
	}

	public void setActiveAsociarUnidadesBool(boolean activeAsociarUnidadesBool) {
		this.activeAsociarUnidadesBool = activeAsociarUnidadesBool;
	}

	public TreeNode getTreeComponentesConstruccionParaModelo() {
		return this.treeComponentesConstruccionParaModelo;
	}

	public void setTreeComponentesConstruccionParaModelo(TreeNode treeComponentesConstruccionParaModelo) {
		this.treeComponentesConstruccionParaModelo = treeComponentesConstruccionParaModelo;
	}

	public File getArchivoResultadoFoto() {
		return archivoResultadoFoto;
	}

	public void setArchivoResultadoFoto(File archivoResultadoFoto) {
		this.archivoResultadoFoto = archivoResultadoFoto;
	}

	public boolean isEditModeloBool() {
		return editModeloBool;
	}

	public void setEditModeloBool(boolean editModeloBool) {
		this.editModeloBool = editModeloBool;
	}

	public boolean isBanderaSoloMejorasEnglobe() {
		return banderaSoloMejorasEnglobe;
	}

	public void setBanderaSoloMejorasEnglobe(boolean banderaSoloMejorasEnglobe) {
		this.banderaSoloMejorasEnglobe = banderaSoloMejorasEnglobe;
	}

	public boolean isBanderaMejorasTerrenoEnglobe() {
		return banderaMejorasTerrenoEnglobe;
	}

	public void setBanderaMejorasTerrenoEnglobe(boolean banderaMejorasTerrenoEnglobe) {
		this.banderaMejorasTerrenoEnglobe = banderaMejorasTerrenoEnglobe;
	}

	public boolean isBanderaAceptarDireccionNormalizada() {
		return banderaAceptarDireccionNormalizada;
	}

	public void setBanderaAceptarDireccionNormalizada(boolean banderaAceptarDireccionNormalizada) {
		this.banderaAceptarDireccionNormalizada = banderaAceptarDireccionNormalizada;
	}

	public List<CalificacionConstruccion> getListaElementosCalificacionConstruccion() {
		return listaElementosCalificacionConstruccion;
	}

	public void setListaElementosCalificacionConstruccion(
			List<CalificacionConstruccion> listaElementosCalificacionConstruccion) {
		this.listaElementosCalificacionConstruccion = listaElementosCalificacionConstruccion;
	}

	public boolean isTramiteSinTitulos() {
		return tramiteSinTitulos;
	}

	public void setTramiteSinTitulos(boolean tramiteSinTitulos) {
		this.tramiteSinTitulos = tramiteSinTitulos;
	}

	public PFoto getCurrentPFoto() {
		return currentPFoto;
	}

	public void setCurrentPFoto(PFoto currentPFoto) {
		this.currentPFoto = currentPFoto;
	}

	public boolean isActualizacionEncontradaBool() {
		return actualizacionEncontradaBool;
	}

	public void setActualizacionEncontradaBool(boolean actualizacionEncontradaBool) {
		this.actualizacionEncontradaBool = actualizacionEncontradaBool;
	}

	public Random getGeneradorNumeroAleatorioNombreImagen() {
		return generadorNumeroAleatorioNombreImagen;
	}

	public void setGeneradorNumeroAleatorioNombreImagen(Random generadorNumeroAleatorioNombreImagen) {
		this.generadorNumeroAleatorioNombreImagen = generadorNumeroAleatorioNombreImagen;
	}

	public void setTipoDescripcionConstruccion(int tipoDescripcionConstruccion) {
		this.tipoDescripcionConstruccion = tipoDescripcionConstruccion;
	}

	public boolean isBanderaAnioTituloEsAnioVigente() {
		return banderaAnioTituloEsAnioVigente;
	}

	public void setBanderaAnioTituloEsAnioVigente(boolean banderaAnioTituloEsAnioVigente) {
		this.banderaAnioTituloEsAnioVigente = banderaAnioTituloEsAnioVigente;
	}

	public boolean isBanderaAceptaAutoestimacion() {
		return banderaAceptaAutoestimacion;
	}

	public void setBanderaAceptaAutoestimacion(boolean banderaAceptaAutoestimacion) {
		this.banderaAceptaAutoestimacion = banderaAceptaAutoestimacion;
	}

	public boolean isBanderaCampoAutoestimacionSeleccionado() {
		return banderaCampoAutoestimacionSeleccionado;
	}

	public void setBanderaCampoAutoestimacionSeleccionado(boolean banderaCampoAutoestimacionSeleccionado) {
		this.banderaCampoAutoestimacionSeleccionado = banderaCampoAutoestimacionSeleccionado;
	}

	public JustificacionProyeccionMB getJustificacionesMB() {
		return justificacionesMB;
	}

	public void setJustificacionesMB(JustificacionProyeccionMB justificacionesMB) {
		this.justificacionesMB = justificacionesMB;
	}

	public PUnidadConstruccion[] getpUnidadesConstruccionConvencionalSeleccionadasEV() {
		return pUnidadesConstruccionConvencionalSeleccionadasEV;
	}

	public void setpUnidadesConstruccionConvencionalSeleccionadasEV(
			PUnidadConstruccion[] pUnidadesConstruccionConvencionalSeleccionadasEV) {
		this.pUnidadesConstruccionConvencionalSeleccionadasEV = pUnidadesConstruccionConvencionalSeleccionadasEV;
	}

	public PUnidadConstruccion[] getpUnidadesConstruccionNoConvencionalSeleccionadasEV() {
		return pUnidadesConstruccionNoConvencionalSeleccionadasEV;
	}

	public void setpUnidadesConstruccionNoConvencionalSeleccionadasEV(
			PUnidadConstruccion[] pUnidadesConstruccionNoConvencionalSeleccionadasEV) {
		this.pUnidadesConstruccionNoConvencionalSeleccionadasEV = pUnidadesConstruccionNoConvencionalSeleccionadasEV;
	}

	public List<PFoto> getListaPFotosABorrar() {
		return listaPFotosABorrar;
	}

	public void setListaPFotosABorrar(List<PFoto> listaPFotosABorrar) {
		this.listaPFotosABorrar = listaPFotosABorrar;
	}

	public List<PUnidadConstruccion> getpUnidadConstruccionComunesNoConvencionalEnglobeVirtual() {
		return pUnidadConstruccionComunesNoConvencionalEnglobeVirtual;
	}

	public void setpUnidadConstruccionComunesNoConvencionalEnglobeVirtual(
			List<PUnidadConstruccion> pUnidadConstruccionComunesNoConvencionalEnglobeVirtual) {
		this.pUnidadConstruccionComunesNoConvencionalEnglobeVirtual = pUnidadConstruccionComunesNoConvencionalEnglobeVirtual;
	}

	public List<PUnidadConstruccion> getpUnidadConstruccionComunesConvencionalEnglobeVirtual() {
		return pUnidadConstruccionComunesConvencionalEnglobeVirtual;
	}

	public void setpUnidadConstruccionComunesConvencionalEnglobeVirtual(
			List<PUnidadConstruccion> pUnidadConstruccionComunesConvencionalEnglobeVirtual) {
		this.pUnidadConstruccionComunesConvencionalEnglobeVirtual = pUnidadConstruccionComunesConvencionalEnglobeVirtual;
	}

	// --------------------------------------------------------------------------------------
	public boolean isValidacion() {
		if (ETipoSubprocesoConservacion.VALIDACION.getCodigo()
				.equals(this.actividad.getRutaActividad().getSubproceso())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isValidacionResponsable() {

		return isPasoActividadValidacion;
	}

	public boolean isValidacionCoordinador() {
		if (ETipoSubprocesoConservacion.EJECUCION.getCodigo().equals(this.actividad.getRutaActividad().getSubproceso())
				|| (ETipoSubprocesoConservacion.VALIDACION.getCodigo().equals(
						this.actividad.getRutaActividad().getSubproceso()) && this.ultimoUsuario(ERol.COORDINADOR))) {
			return true;
		} else {
			return false;
		}
	}

	public boolean ultimoUsuario(ERol rol) {
		List<TramiteEstado> observaciones = this.getGeneralesService()
				.buscarTramiteEstadosPorTramiteId(this.tramite.getId());
		if (observaciones != null && observaciones.size() > 0) {
			TramiteEstado o = observaciones.get(observaciones.size() - 1);
			if (o != null) {
				String login = o.getResponsable();
				UsuarioDTO usuarioTemp = this.getGeneralesService().getCacheUsuario(login);
				if (usuarioTemp != null) {
					for (String r : usuarioTemp.getRoles()) {
						if (rol.getRol().toString().toUpperCase().equals(r)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean isObligaGeografico() {

		if (this.banderaPredioFiscal) {
			return false;
		}

		if (this.tramite.getEstadoGdb() != null
				&& Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA.equals(this.tramite.getEstadoGdb())) {
			return true;
		} else {
			return false;
		}
	}

	public void obligarGeografico() {
		this.tramite.setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
		this.getTramiteService().actualizarTramite(this.tramite);
	}

	/**
	 * Validaciónes para mutaciones de quinta
	 *
	 * @author david.cifuentes
	 *
	 * @return
	 */
	public boolean isValidacionQuinta() {
		this.validacionQuinta = true;
		// Se deshabilita el boton de enviar al coordinador para las mutaciones
		// de quinta (Nuevo y Omitido), se debe pasar por el editor
		if (this.tramite.isQuinta() && (this.imagenAntes == null || this.imagenDespues == null)) {
			this.validacionQuinta = false;
		}

		// Si el predio es fiscal no va al editor
		if (this.banderaPredioFiscal) {
			this.validacionQuinta = true;
		}

		if (this.tramite.isQuintaMasivo() && !this.validacionQuintaMasivoTodos) {
			this.validacionQuinta = false;
		}

		return this.validacionQuinta;
	}

	public void setValidacionQuinta(boolean validacionQuinta) {
		this.validacionQuinta = validacionQuinta;
	}

	/**
	 * Éste dato se obtiene del CalificacionAnexo seleccionado
	 *
	 * @return
	 */
	public int getTipoDescripcionConstruccion() {
		this.tipoDescripcionConstruccion = (int) getSelectedCalificacionAnexo().getPuntos();
		return this.tipoDescripcionConstruccion;
	}

	// --------------------------------------------------------------------------------------------------
	public void setSelectedCalificacionAnexo(CalificacionAnexo selectedCalificacionAnexo) {
		this.selectedCalificacionAnexo = selectedCalificacionAnexo;
		if (this.selectedCalificacionAnexo != null) {
			this.calificacionAnexoIdSelected = this.selectedCalificacionAnexo.getId();
		}
	}

	/**
	 * es el getter del this.selectedCalificacionAnexo, pero además, arma la lista
	 * de items para el combo. Se hizo aquí porque del modo en que está armada la
	 * página hacía primero este getter que el de la lista de items del combo
	 * descripcionConstruccionAnexoItemList. Antes se armaba en el getter de la
	 * lista, pero el selectedCalificacionAnexo quedaba con el valor anterior
	 */
	public CalificacionAnexo getSelectedCalificacionAnexo() {

		List<CalificacionAnexo> calificacionAnexoList;
		SelectItem comboItem;
		boolean isFirst, useFirst;
		Long unidadConstruccionCalificacionAnexoId = -1l;

		this.descripcionConstruccionAnexoItemList = new ArrayList<SelectItem>();
		isFirst = true;
		useFirst = false;

		calificacionAnexoList = this.getConservacionService()
				.obtenerCalificacionesAnexoPorIdUsoConstruccion(this.selectedUsoUnidadConstruccionId);

		// El dato calificacionAnexoIdSelected es igual a
		// this.unidadConstruccionSeleccionada.getCalificacionAnexoId()
		// o para el caso de modelos de construcción a
		// this.unidadDelModeloDeConstruccionSeleccionada.getCalificacionAnexoId()
		// D: se supone que si es 'anexo' debería tener este dato, pero por si
		// acaso...
		if (this.calificacionAnexoIdSelected != null) {
			unidadConstruccionCalificacionAnexoId = this.calificacionAnexoIdSelected;
		} else {
			useFirst = true;
		}

		for (CalificacionAnexo ca : calificacionAnexoList) {
			comboItem = new SelectItem(ca, ca.getDescripcion());
			this.descripcionConstruccionAnexoItemList.add(comboItem);

			if (useFirst && isFirst) {
				this.selectedCalificacionAnexo = ca;
				isFirst = false;
			} else if (!useFirst) {
				if (ca.getId().longValue() == unidadConstruccionCalificacionAnexoId.longValue()) {
					this.selectedCalificacionAnexo = ca;
				}
			}
		}
		return this.selectedCalificacionAnexo;
	}

	// --------------------------------------------------------------------------------------------------
	public void setDescripcionConstruccionAnexoItemList(ArrayList<SelectItem> descripcionConstruccionAnexoItemList) {
		this.descripcionConstruccionAnexoItemList = descripcionConstruccionAnexoItemList;
	}

	public ArrayList<SelectItem> getDescripcionConstruccionAnexoItemList() {
		return this.descripcionConstruccionAnexoItemList;
	}

	// --------------------------------------------------------------------------------------------------
	public List<Predio> getPrediosSeleccionadosCancelacion() {
		return prediosSeleccionadosCancelacion;
	}

	public void setPrediosSeleccionadosCancelacion(List<Predio> prediosSeleccionadosCancelacion) {
		this.prediosSeleccionadosCancelacion = prediosSeleccionadosCancelacion;
	}

	public void setEsNuevaUnidadConstruccion(boolean esNuevaUnidadConstruccion) {
		this.esNuevaUnidadConstruccion = esNuevaUnidadConstruccion;
	}

	public boolean isExisteImagen() {
		return existeImagen;
	}

	public void setExisteImagen(boolean existeImagen) {
		this.existeImagen = existeImagen;
	}

	public boolean isEsNuevaUnidadConstruccion() {
		return this.esNuevaUnidadConstruccion;
	}

	public EOrden getOrdenPaises() {
		return this.ordenPaises;
	}

	public void setOrdenPaises(EOrden ordenPaises) {
		this.ordenPaises = ordenPaises;
	}

	public EOrden getOrdenDepartamentos() {
		return ordenDepartamentos;
	}

	public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
		this.ordenDepartamentos = ordenDepartamentos;
	}

	public EOrden getOrdenMunicipios() {
		return ordenMunicipios;
	}

	public void setOrdenMunicipios(EOrden ordenMunicipios) {
		this.ordenMunicipios = ordenMunicipios;
	}

	public Tramite getTramite() {
		return tramite;
	}

	public void setTramite(Tramite tramite) {
		this.tramite = tramite;
	}

	public PPredio getPredioSeleccionado() {
		return this.predioSeleccionado;
	}

	public List<PPredio> getPrediosSeleccionados() {
		List<PPredio> predios = new ArrayList<PPredio>();
		if (this.predioSeleccionado != null) {
			predios.add(this.predioSeleccionado);
		}
		return predios;
	}

	public void setPredioSeleccionado(PPredio predioSeleccionado) {
		this.predioSeleccionado = predioSeleccionado;
		this.actualizarListaDeDirecciones();
	}

	public PPredioDireccion getPredioDireccionSeleccionado() {
		return predioDireccionSeleccionado;
	}

	public void setPredioDireccionSeleccionado(PPredioDireccion predioDireccionSeleccionado) {
		this.predioDireccionSeleccionado = predioDireccionSeleccionado;
	}

	public PPredioServidumbre getServidumbreAdicional() {
		return servidumbreAdicional;
	}

	public void setServidumbreAdicional(PPredioServidumbre servidumbreAdicional) {
		this.servidumbreAdicional = servidumbreAdicional;
	}

	public List<SelectItem> getTiposPredioEtapas() {
		return tiposPredioEtapas;
	}

	public void setTiposPredioEtapas(List<SelectItem> tiposPredioEtapas) {
		this.tiposPredioEtapas = tiposPredioEtapas;
	}

	public List<SelectItem> getTiposPredio() {
		return tiposPredio;
	}

	public void setTiposPredio(List<SelectItem> tiposPredio) {
		this.tiposPredio = tiposPredio;
	}

	public List<SelectItem> getTiposDestinoPredio() {
		return tiposDestinoPredio;
	}

	public void setTiposDestinoPredio(List<SelectItem> tiposDestinoPredio) {
		this.tiposDestinoPredio = tiposDestinoPredio;
	}

	public List<SelectItem> getTiposPredioServidumbre() {
		return tiposPredioServidumbre;
	}

	public void setTiposPredioServidumbre(List<SelectItem> tiposPredioServidumbre) {
		this.tiposPredioServidumbre = tiposPredioServidumbre;
	}

	public String getOpcionEliminar() {
		return opcionEliminar;
	}

	public void setOpcionEliminar(String opcionEliminar) {
		this.opcionEliminar = opcionEliminar;
	}

	public boolean isVerificacionDatosProyeccion() {
		return this.verificacionDatosProyeccion;
	}

	public void setVerificacionDatosProyeccion(boolean verificacionDatosProyeccion) {
		this.verificacionDatosProyeccion = verificacionDatosProyeccion;
	}

	public TramiteEstado getTramiteEstado() {
		return tramiteEstado;
	}

	public ReporteDTO getReporteResoluciones() {
		return reporteResoluciones;
	}

	public void setReporteResoluciones(ReporteDTO reporteResoluciones) {
		this.reporteResoluciones = reporteResoluciones;
	}

	public List<TramiteEstado> getHistoricoObservaciones() {
		return historicoObservaciones;
	}

	public void setHistoricoObservaciones(List<TramiteEstado> historicoObservaciones) {
		this.historicoObservaciones = historicoObservaciones;
	}

	public void setTramiteEstado(TramiteEstado tramiteEstado) {
		this.tramiteEstado = tramiteEstado;
	}

	public TramiteTextoResolucion getTramiteTextoResolucion() {
		return tramiteTextoResolucion;
	}

	public List<PPredio> getpPrediosQuintaMasivo() {
		return pPrediosQuintaMasivo;
	}

	public void setpPrediosQuintaMasivo(List<PPredio> pPrediosQuintaMasivo) {
		this.pPrediosQuintaMasivo = pPrediosQuintaMasivo;
	}

	public ModeloResolucion getModeloResolucionSeleccionado() {
		return modeloResolucionSeleccionado;
	}

	public void setModeloResolucionSeleccionado(ModeloResolucion modeloResolucionSeleccionado) {
		this.modeloResolucionSeleccionado = modeloResolucionSeleccionado;
	}

	public void setTramiteTextoResolucion(TramiteTextoResolucion tramiteTextoResolucion) {
		this.tramiteTextoResolucion = tramiteTextoResolucion;
	}

	public List<ModeloResolucion> getModeloResolucionList() {
		return modeloResolucionList;
	}

	public void setModeloResolucionList(List<ModeloResolucion> modeloResolucionList) {
		this.modeloResolucionList = modeloResolucionList;
	}

	public List<SelectItem> getTitulosResolucion() {
		return titulosResolucion;
	}

	public void setTitulosResolucion(List<SelectItem> titulosResolucion) {
		this.titulosResolucion = titulosResolucion;
	}

	public String getIdModeloResolucionSeleccionado() {
		return this.idModeloResolucionSeleccionado;
	}

	public void setIdModeloResolucionSeleccionado(String idModeloResolucionSeleccionado) {
		this.idModeloResolucionSeleccionado = idModeloResolucionSeleccionado;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Boolean getPrincipalBool() {
		return principalBool;
	}

	public void setPrincipalBool(Boolean principalBool) {
		this.principalBool = principalBool;
	}

	public String getDireccionNormalizada() {
		return direccionNormalizada;
	}

	public void setDireccionNormalizada(String direccionNormalizada) {
		this.direccionNormalizada = direccionNormalizada;
	}

	public Boolean getNormalizadaBool() {
		return normalizadaBool;
	}

	public void setNormalizadaBool(Boolean normalizadaBool) {
		this.normalizadaBool = normalizadaBool;
	}

	public String getCirculoRegistralSeleccionado() {
		return circuloRegistralSeleccionado;
	}

	public void setCirculoRegistralSeleccionado(String circuloRegistralSeleccionado) {
		this.circuloRegistralSeleccionado = circuloRegistralSeleccionado;
	}

	public String getNumeroRegistroInfoMatriculaNueva() {
		return numeroRegistroInfoMatriculaNueva;
	}

	public void setNumeroRegistroInfoMatriculaNueva(String numeroRegistroInfoMatriculaNueva) {
		this.numeroRegistroInfoMatriculaNueva = numeroRegistroInfoMatriculaNueva;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public void setEditModeBool(Boolean editModeBool) {
		this.editModeBool = editModeBool;
	}

	public Boolean getEditModeBool() {
		return editModeBool;
	}

	public void setInformeResolucionTramDoc(TramiteDocumento informeResolucionTramDoc) {
		this.informeResolucionTramDoc = informeResolucionTramDoc;
	}

	public TramiteDocumento getInformeResolucionTramDoc() {
		return informeResolucionTramDoc;
	}

	public void setInformeResolucionDoc(Documento informeResolucionDoc) {
		this.informeResolucionDoc = informeResolucionDoc;
	}

	public Documento getInformeResolucionDoc() {
		return this.informeResolucionDoc;
	}

	public UsuarioDTO getDigitalizador() {
		return digitalizador;
	}

	public void setDigitalizador(UsuarioDTO digitalizador) {
		this.digitalizador = digitalizador;
	}

	public UsuarioDTO getUsuario() {
		return this.usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public TramiteSeccionDatos getConfiguracion() {
		return this.configuracion;
	}

	public void setConfiguracion(TramiteSeccionDatos configuracion) {
		this.configuracion = configuracion;
	}

	public PPredioServidumbre getSelectedPredioServidumbre() {
		return this.selectedPredioServidumbre;
	}

	public void setSelectedPredioServidumbre(PPredioServidumbre selectedPredioServidumbre) {
		this.selectedPredioServidumbre = selectedPredioServidumbre;
	}

	public UsoConstruccion getSelectedUsoUnidadConstruccion() {
		return this.selectedUsoUnidadConstruccion;
	}

	public void setSelectedUsoUnidadConstruccion(UsoConstruccion selectedUsoUnidadConstruccion) {
		this.selectedUsoUnidadConstruccion = selectedUsoUnidadConstruccion;
	}

	public Long getSelectedUsoUnidadConstruccionId() {
		return this.selectedUsoUnidadConstruccionId;
	}

	public List<SelectItem> getUsosUnidadConstruccionItemList() {
		return this.usosUnidadConstruccionItemList;
	}

	public void setUsosUnidadConstruccionItemList(ArrayList<SelectItem> usosUnidadConstruccionItemList) {
		this.usosUnidadConstruccionItemList = usosUnidadConstruccionItemList;
	}

	public void setTipoModelo(String tipoModelo) {
		this.tipoModelo = tipoModelo;
	}

	public String getTipoModelo() {
		return this.tipoModelo;
	}

	public PPredioAvaluoCatastral getAvaluo() {
		return avaluo;
	}

	public void setAvaluo(PPredioAvaluoCatastral avaluo) {
		this.avaluo = avaluo;
	}

	public Integer getTab() {
		return tab;
	}

	public void setTab(Integer tab) {
		this.tab = tab;
	}

	public boolean isTipoDominioComun() {
		return tipoDominioComun;
	}

	public void setTipoDominioComun(boolean tipoDominioComun) {
		this.tipoDominioComun = tipoDominioComun;
	}

	public PUnidadConstruccion getUnidadConstruccionSeleccionada() {
		return this.unidadConstruccionSeleccionada;
	}

	public void setUnidadConstruccionSeleccionada(PUnidadConstruccion unidadConstruccionSeleccionada) {
		this.unidadConstruccionSeleccionada = unidadConstruccionSeleccionada;
		this.unidadConstruccionSeleccionada = this.getConservacionService()
				.buscarUnidadDeConstruccionPorSuId(unidadConstruccionSeleccionada.getId());

		this.areaNoConvencional = this.unidadConstruccionSeleccionada.getAreaConstruida();
		if (this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps() == null
				|| this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps().isEmpty()) {
			this.informacionMigracionIncompleta = true;
		} else {
			this.informacionMigracionIncompleta = false;
		}

		if (unidadConstruccionSeleccionada.getTipoConstruccion()
				.equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
			this.banderaConstruccionConvencional = true;
		} else {
			this.banderaConstruccionConvencional = false;
		}
	}

	// ---------------------GET AND SET--------------------------
	public List<PPredio> getpPrediosCancelacionMasivo() {
		return pPrediosCancelacionMasivo;
	}

	public List<PPersonaPredio> getpPersonaPrediosCancelacionM() {
		return pPersonaPrediosCancelacionM;
	}

	public void setpPersonaPrediosCancelacionM(List<PPersonaPredio> pPersonaPrediosCancelacionM) {
		this.pPersonaPrediosCancelacionM = pPersonaPrediosCancelacionM;
	}

	public void setpPrediosCancelacionMasivo(List<PPredio> pPrediosCancelacionMasivo) {
		this.pPrediosCancelacionMasivo = pPrediosCancelacionMasivo;
	}

	public List<PPredioDireccion> getPredioSeleccionadoDirecciones() {
		return this.predioSeleccionadoDirecciones;
	}

	public void setPredioSeleccionadoDirecciones(List<PPredioDireccion> predioSeleccionadoDirecciones) {
		this.predioSeleccionadoDirecciones = predioSeleccionadoDirecciones;
	}

	public PPredioDireccion getpPredioDireccionSeleccionado() {
		return this.pPredioDireccionSeleccionado;
	}

	public void setpPredioDireccionSeleccionado(PPredioDireccion pPredioDireccionSeleccionado) {
		this.pPredioDireccionSeleccionado = pPredioDireccionSeleccionado;
	}

	public List<PPredioDireccion> getDireccionesEditadas() {
		return direccionesEditadas;
	}

	public void setDireccionesEditadas(List<PPredioDireccion> direccionesEditadas) {
		this.direccionesEditadas = direccionesEditadas;
	}

	public boolean isBanderaConvencionalRectifica() {
		return banderaConvencionalRectifica;
	}

	public void setBanderaConvencionalRectifica(boolean banderaConvencionalRectifica) {
		this.banderaConvencionalRectifica = banderaConvencionalRectifica;
	}

	public void setValidacionDireccionBool(boolean validacionDireccionBool) {
		this.validacionDireccionBool = validacionDireccionBool;
	}

	public boolean isValidacionDireccionBool() {
		return validacionDireccionBool;
	}

	public String getObservacionesInscripcionesYDecretos() {
		return observacionesInscripcionesYDecretos;
	}

	public void setObservacionesInscripcionesYDecretos(String observacionesInscripcionesYDecretos) {
		this.observacionesInscripcionesYDecretos = observacionesInscripcionesYDecretos;
	}

	public List<PPredio> getpPrediosEnglobe() {
		return pPrediosEnglobe;
	}

	public void setpPrediosEnglobe(List<PPredio> pPrediosEnglobe) {
		this.pPrediosEnglobe = pPrediosEnglobe;
	}

	public List<PPredio> getpPrediosDesenglobe() {
		return pPrediosDesenglobe;
	}

	public void setpPrediosDesenglobe(List<PPredio> pPrediosDesenglobe) {
		this.pPrediosDesenglobe = pPrediosDesenglobe;
	}

	public List<PPredio> getpPrediosTerceraRectificacionMasiva() {
		return pPrediosTerceraRectificacionMasiva;
	}

	public void setpPrediosTerceraRectificacionMasiva(List<PPredio> pPrediosTerceraRectificacionMasiva) {
		this.pPrediosTerceraRectificacionMasiva = pPrediosTerceraRectificacionMasiva;
	}

	public List<SelectItem> getItemListCirculosRegistrales() {
		return itemListCirculosRegistrales;
	}

	public void setItemListCirculosRegistrales(List<SelectItem> itemListCirculosRegistrales) {
		this.itemListCirculosRegistrales = itemListCirculosRegistrales;
	}

	public List<VPPredioAvaluoDecreto> getpPrediosAvaluosCatastrales() {
		return pPrediosAvaluosCatastrales;
	}

	public void setpPrediosAvaluosCatastrales(List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastrales) {
		this.pPrediosAvaluosCatastrales = pPrediosAvaluosCatastrales;
	}

	public List<UnidadConstruccion> getUnidadesConstruccionConvencional() {
		return unidadesConstruccionConvencional;
	}

	public void setUnidadesConstruccionConvencional(List<UnidadConstruccion> unidadesConstruccionConvencional) {
		this.unidadesConstruccionConvencional = unidadesConstruccionConvencional;
	}

	public List<UnidadConstruccion> getUnidadesConstruccionNoConvencional() {
		return unidadesConstruccionNoConvencional;
	}

	public void setUnidadesConstruccionNoConvencional(List<UnidadConstruccion> unidadesConstruccionNoConvencional) {
		this.unidadesConstruccionNoConvencional = unidadesConstruccionNoConvencional;
	}

	public UnidadConstruccion[] getUnidadesConstruccionConvencionalSeleccionadas() {
		return unidadesConstruccionConvencionalSeleccionadas;
	}

	public void setUnidadesConstruccionConvencionalSeleccionadas(
			UnidadConstruccion[] unidadesConstruccionConvencionalSeleccionadas) {
		this.unidadesConstruccionConvencionalSeleccionadas = unidadesConstruccionConvencionalSeleccionadas;
	}

	public UnidadConstruccion[] getUnidadesConstruccionNoConvencionalSeleccionadas() {
		return unidadesConstruccionNoConvencionalSeleccionadas;
	}

	public void setUnidadesConstruccionNoConvencionalSeleccionadas(
			UnidadConstruccion[] unidadesConstruccionNoConvencionalSeleccionadas) {
		this.unidadesConstruccionNoConvencionalSeleccionadas = unidadesConstruccionNoConvencionalSeleccionadas;
	}

	public boolean isMostrarObservacionesDepuracion() {
		return mostrarObservacionesDepuracion;
	}

	public boolean isMostrarProyeccionTramiteViaGubernativa() {
		return mostrarProyeccionTramiteViaGubernativa;
	}

	public void setMostrarProyeccionTramiteViaGubernativa(boolean mostrarProyeccionTramiteViaGubernativa) {
		this.mostrarProyeccionTramiteViaGubernativa = mostrarProyeccionTramiteViaGubernativa;
	}

	public boolean isModResultResolucionInicial() {
		return modResultResolucionInicial;
	}

	public void setModResultResolucionInicial(boolean modResultResolucionInicial) {
		this.modResultResolucionInicial = modResultResolucionInicial;
	}

	public List<SelectItem> getOpcionesRecursoViaGubernativa() {
		return opcionesRecursoViaGubernativa;
	}

	public void setOpcionesRecursoViaGubernativa(List<SelectItem> opcionesRecursoViaGubernativa) {
		this.opcionesRecursoViaGubernativa = opcionesRecursoViaGubernativa;
	}

	public double getAreaTotalTerrenoUnidadF() {
		return areaTotalTerrenoUnidadF;
	}

	public void setAreaTotalTerrenoUnidadF(double areaTotalTerrenoUnidadF) {
		this.areaTotalTerrenoUnidadF = areaTotalTerrenoUnidadF;
	}

	// -----------------------------------
	public String getAreaTotalConsConvencional(PPredio predioAValidar) {
		LOGGER.info("getAreaTotalConsConvencional");
		Double area = 0.0;
		predioAValidar.setTramite(this.tramite);
		if (predioAValidar.getPUnidadesConstruccionConvencionalNuevas() != null) {
			for (IModeloUnidadConstruccion unidad : predioAValidar.getPUnidadesConstruccionConvencionalNuevas()) {
				area += unidad.getAreaConstruida();
			}
		}
		return area.toString();
	}

	// ---------------------------
	public String getAreaTotalConsNoConvencional(PPredio predioAValidar) {
		LOGGER.info("getAreaTotalConsNoConvencional");
		Double area = 0.0;
		predioAValidar.setTramite(this.tramite);
		if (predioAValidar.getPUnidadesConstruccionNoConvencionalNuevas() != null) {
			for (IModeloUnidadConstruccion unidad : predioAValidar.getPUnidadesConstruccionNoConvencionalNuevas()) {
				area += unidad.getAreaConstruida();
			}
		}
		return area.toString();
	}

	public String getAreaTotalConsConvencional() {
		return this.getAreaTotalConsConvencional(this.predioSeleccionado);
	}

	// ---------------------------
	public String getAreaTotalConsNoConvencional() {
		return this.getAreaTotalConsNoConvencional(this.predioSeleccionado);
	}

	// ------------------------
	public String getAreaTotalTerreno() {
		LOGGER.info("getAreaTotalTerreno");
		Double areaTotal = 0.0;

		if (this.tramite.tieneReplica()) {
			areaTotal = this.predioSeleccionado.getAreaTerreno();
		} else {
			areaTotal = this.getAreaTotalTerrenoZonas(this.predioSeleccionado);
		}
		return areaTotal.toString();
	}

	public String getAreaTotalTerrenoUnidad() {
		LOGGER.info("getAreaTotalTerreno");
		Double areaTotal = 0.0;
		if (this.tramite.tieneReplica()) {
			areaTotal = this.predioSeleccionado.getAreaTerreno();
		} else {
			if (this.predioSeleccionado.getPPredioZonas() != null) {
				List<PPredioZona> zonas = new LinkedList<PPredioZona>();

				for (PPredioZona predZona : this.predioSeleccionado.getPPredioZonas()) {
					if (predZona.getCancelaInscribe() == null
							|| !predZona.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
						zonas.add(predZona);

					}
				}

				// Adicionalmente, se deben mostrar únicamente las zonas
				// vigentes, es decir, las del año actual, en caso de que no
				// existieran, se deben mostrar las del año anterior
				if (!zonas.isEmpty()) {
					zonas = this.verificarZonaVigente(zonas);
				}

				for (PPredioZona ppz : zonas) {
					areaTotal += ppz.getArea();
				}
			}
		}

		// 14052 :: jonathan.chacon :: 15/09/2015 ::Se hace el calculo del area
		// de terreno
		// para predios fiscales (algúnos tipos de trámite)
		if (this.banderaPredioFiscal) {

			areaTotal = this.predioSeleccionado.getAreaTerreno();

			if (this.tramite.isSegundaEnglobe()) {

				if (this.banderaSoloMejorasEnglobe) {
					areaTotal = 0.0;
				} else {
					areaTotal = this.predioSeleccionado.getAreaTerreno();
				}
			}

			if (this.tramite.isTercera()) {

				if (this.tramite.getPredio().isMejora()) {
					areaTotal = 0.0;
				} else {
					areaTotal = this.predioSeleccionado.getAreaTerreno();
				}
			}

			if (this.tramite.isQuintaOmitido()) {

				if (this.tramite.getPredio().isMejora()) {
					areaTotal = 0.0;
				} else {
					areaTotal = this.predioSeleccionado.getAreaTerreno();
				}
			}

			if (this.tramite.isQuintaNuevo()) {
				if (this.predioSeleccionado.isMejora()) {
					areaTotal = 0.0;
				} else {
					areaTotal = this.predioSeleccionado.getAreaTerreno();
				}
			}
		}
		if (this.banderaPredioFiscal && !this.tramite.isSegundaDesenglobe()) {

			if (this.predioSeleccionado.isPredioRural()) {
				Integer ha = (int) (areaTotal / 10000);
				double mts = areaTotal - ha * 10000;
				mts = Utilidades.redondearNumeroADosCifrasDecimales(mts);
				return (ha > 0 ? ha + " ha " : " ") + (mts > 0 ? mts + " m2 " : "");
			} else {
				areaTotal = Utilidades.redondearNumeroADosCifrasDecimales(areaTotal);
				return areaTotal.toString() + " m2";
			}
		} else {
			return areaTotal.toString();
		}

	}

	public String getAreaTotalConstruccion(PPredio predioAValidar) {
		LOGGER.info("getAreaTotalConstruccion");
		Double areaTotal = new Double(this.getAreaTotalConsConvencional(predioAValidar));
		areaTotal += new Double(this.getAreaTotalConsNoConvencional(predioAValidar));
		return areaTotal.toString();
	}

	public String getAreaTotalConstruccion() {
		return this.getAreaTotalConstruccion(this.predioSeleccionado);
	}

	public PPersonaPredio getPropietarioSeleccionado() {
		return propietarioSeleccionado;
	}

	public void setPropietarioSeleccionado(PPersonaPredio propietarioSeleccionado) {
		this.propietarioSeleccionado = propietarioSeleccionado;
	}

	public boolean isBanderaMostrarFechaTitulo() {
		return banderaMostrarFechaTitulo;
	}

	public void setBanderaMostrarFechaTitulo(boolean banderaMostrarFechaTitulo) {
		this.banderaMostrarFechaTitulo = banderaMostrarFechaTitulo;
	}

	public boolean isBanderaMostrarFechaInscripcion() {
		return banderaMostrarFechaInscripcion;
	}

	public void setBanderaMostrarFechaInscripcion(boolean banderaMostrarFechaInscripcion) {
		this.banderaMostrarFechaInscripcion = banderaMostrarFechaInscripcion;
	}

	public boolean isBanderaIngresarFechaInscripcion() {
		return banderaIngresarFechaInscripcion;
	}

	public void setBanderaIngresarFechaInscripcion(boolean banderaIngresarFechaInscripcion) {
		this.banderaIngresarFechaInscripcion = banderaIngresarFechaInscripcion;
	}

	public String getCalculoAvaluoEnFormaRetroactiva() {
		return calculoAvaluoEnFormaRetroactiva;
	}

	public void setCalculoAvaluoEnFormaRetroactiva(String calculoAvaluoEnFormaRetroactiva) {
		this.calculoAvaluoEnFormaRetroactiva = calculoAvaluoEnFormaRetroactiva;
	}

	public boolean isCalculoAvaluoPorRangoDeAnios() {
		return calculoAvaluoPorRangoDeAnios;
	}

	public void setCalculoAvaluoPorRangoDeAnios(boolean calculoAvaluoPorRangoDeAnios) {
		this.calculoAvaluoPorRangoDeAnios = calculoAvaluoPorRangoDeAnios;
	}

	public boolean isMostrarCalculoAvaluoPorRangoDeAnios() {
		return mostrarCalculoAvaluoPorRangoDeAnios;
	}

	public void setMostrarCalculoAvaluoPorRangoDeAnios(boolean mostrarCalculoAvaluoPorRangoDeAnios) {
		this.mostrarCalculoAvaluoPorRangoDeAnios = mostrarCalculoAvaluoPorRangoDeAnios;
	}

	public boolean isMostrarCalculoAvaluoPorAnio() {
		return mostrarCalculoAvaluoPorAnio;
	}

	public void setMostrarCalculoAvaluoPorAnio(boolean mostrarCalculoAvaluoPorAnio) {
		this.mostrarCalculoAvaluoPorAnio = mostrarCalculoAvaluoPorAnio;
	}

	public String getAnioInicialCalculoAvaluoPorRango() {
		return anioInicialCalculoAvaluoPorRango;
	}

	public void setAnioInicialCalculoAvaluoPorRango(String anioInicialCalculoAvaluoPorRango) {
		this.anioInicialCalculoAvaluoPorRango = anioInicialCalculoAvaluoPorRango;
	}

	public String getAnioFinalCalculoAvaluoPorRango() {
		return anioFinalCalculoAvaluoPorRango;
	}

	public void setAnioFinalCalculoAvaluoPorRango(String anioFinalCalculoAvaluoPorRango) {
		this.anioFinalCalculoAvaluoPorRango = anioFinalCalculoAvaluoPorRango;
	}

	public String getAnioCalculoAvaluo() {
		return anioCalculoAvaluo;
	}

	public void setAnioCalculoAvaluo(String anioCalculoAvaluo) {
		this.anioCalculoAvaluo = anioCalculoAvaluo;
	}

	public double getValorAvaluoCalculoAvaluo() {
		return valorAvaluoCalculoAvaluo;
	}

	public void setValorAvaluoCalculoAvaluo(double valorAvaluoCalculoAvaluo) {
		this.valorAvaluoCalculoAvaluo = valorAvaluoCalculoAvaluo;
	}

	public boolean isActivarCalculoAvaluoManual() {
		return activarCalculoAvaluoManual;
	}

	public void setActivarCalculoAvaluoManual(boolean activarCalculoAvaluoManual) {
		this.activarCalculoAvaluoManual = activarCalculoAvaluoManual;
	}

	public boolean isActivarEliminarCalculoAvaluoManual() {
		return activarEliminarCalculoAvaluoManual;
	}

	public void setActivarEliminarCalculoAvaluoManual(boolean activarEliminarCalculoAvaluoManual) {
		this.activarEliminarCalculoAvaluoManual = activarEliminarCalculoAvaluoManual;
	}

	public boolean isValidarCalificacionComponenteBool() {
		return validarCalificacionComponenteBool;
	}

	public void setValidarCalificacionComponenteBool(boolean validarCalificacionComponenteBool) {
		this.validarCalificacionComponenteBool = validarCalificacionComponenteBool;
	}

	public boolean isMostrarTablaInscripcionesYDecretos() {
		return mostrarTablaInscripcionesYDecretos;
	}

	public void setMostrarTablaInscripcionesYDecretos(boolean mostrarTablaInscripcionesYDecretos) {
		this.mostrarTablaInscripcionesYDecretos = mostrarTablaInscripcionesYDecretos;
	}

	public VPPredioAvaluoDecreto getpPredioAvaluoCatastralSeleccionado() {
		return pPredioAvaluoCatastralSeleccionado;
	}

	public void setpPredioAvaluoCatastralSeleccionado(VPPredioAvaluoDecreto pPredioAvaluoCatastralSeleccionado) {
		this.pPredioAvaluoCatastralSeleccionado = pPredioAvaluoCatastralSeleccionado;
	}

	public List<SelectItem> getAniosCalculoAvaluoPorRango() {
		return aniosCalculoAvaluoPorRango;
	}

	public void setAniosCalculoAvaluoPorRango(List<SelectItem> aniosCalculoAvaluoPorRango) {
		this.aniosCalculoAvaluoPorRango = aniosCalculoAvaluoPorRango;
	}

	public boolean isActivarCalcularAvaluoPorRangoDeAnios() {
		return activarCalcularAvaluoPorRangoDeAnios;
	}

	public void setActivarCalcularAvaluoPorRangoDeAnios(boolean activarCalcularAvaluoPorRangoDeAnios) {
		this.activarCalcularAvaluoPorRangoDeAnios = activarCalcularAvaluoPorRangoDeAnios;
	}

	public List<VPPredioAvaluoDecreto> getPrediosAvaluoDecretoCalculoManual() {
		return prediosAvaluoDecretoCalculoManual;
	}

	public void setPrediosAvaluoDecretoCalculoManual(List<VPPredioAvaluoDecreto> prediosAvaluoDecretoCalculoManual) {
		this.prediosAvaluoDecretoCalculoManual = prediosAvaluoDecretoCalculoManual;
	}

	public TramiteDepuracion getTramiteDepuracion() {
		return tramiteDepuracion;
	}

	public boolean isHabilitarBotonesViaGubernativa() {
		return habilitarBotonesViaGubernativa;
	}

	/**
	 * Metodo para inicializar los datos relacionados con el englobe virtual
	 *
	 *
	 * @author felipe.cadena
	 */
	public void inicializaEnglobeVirtual() {

		this.banderaEnglobeVirtual = true;
		this.validarGuardarEVOK = false;

	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que guarda la ubicación del predio
	 *
	 * @author javier.aponte
	 */
	public void guardarUbicacionPredio() throws Exception {
		LOGGER.debug("Entro al metodo guardar ubicacion del predio");

		try {
			this.getConservacionService().guardarProyeccion(this.usuario, this.predioSeleccionado);
			if (this.tramite.isSegundaDesenglobe()) {
				if (this.entradaProyeccionMB == null) {
					this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
				}
				if (this.entradaProyeccionMB != null) {
					this.cargarPrediosDesenglobe();
					this.entradaProyeccionMB.setPrediosResultantes(this.pPrediosDesenglobe);
				}
			}
			this.addMensajeInfo("Se guardaron los datos generales del predio correctamente!");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError(e);
		}
	}

	public List<SelectItem> getPaises() {

		this.paises = new ArrayList<SelectItem>();
		paises.add(new SelectItem("", super.DEFAULT_COMBOS));

		List<Pais> paisesP = this.getGeneralesService().getCachePaises();
		if (this.ordenPaises.equals(EOrden.CODIGO)) {
			Collections.sort(paisesP);
		} else {
			Collections.sort(paisesP, Pais.getComparatorNombre());
		}

		for (Pais pais : paisesP) {
			if (this.ordenPaises.equals(EOrden.CODIGO)) {
				paises.add(new SelectItem(pais.getCodigo(), pais.getCodigo() + "-" + pais.getNombre()));
			} else {
				paises.add(new SelectItem(pais.getCodigo(), pais.getNombre()));
			}
		}
		return paises;
	}

	public List<SelectItem> getDepartamentos() {
		List<SelectItem> departamentos = new ArrayList<SelectItem>();
		departamentos.add(new SelectItem("", super.DEFAULT_COMBOS));
		if (this.propietarioSeleccionado != null
				&& this.propietarioSeleccionado.getPPersona().getDireccionPais() != null) {
			List<Departamento> dptos = this.getGeneralesService().getCacheDepartamentosPorPais(
					this.propietarioSeleccionado.getPPersona().getDireccionPais().getCodigo());
			if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
				Collections.sort(dptos);
			} else {
				Collections.sort(dptos, Departamento.getComparatorNombre());
			}
			for (Departamento d : dptos) {
				if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
					departamentos.add(new SelectItem(d.getCodigo(), d.getCodigo() + "-" + d.getNombre()));
				} else {
					departamentos.add(new SelectItem(d.getCodigo(), d.getNombre()));
				}
			}
		}
		return departamentos;
	}

	public List<SelectItem> getMunicipios() {
		List<SelectItem> municipiosC = new ArrayList<SelectItem>();
		municipiosC.add(new SelectItem("", super.DEFAULT_COMBOS));
		if (this.propietarioSeleccionado != null
				&& this.propietarioSeleccionado.getPPersona().getDireccionPais() != null
				&& this.propietarioSeleccionado.getPPersona().getDireccionDepartamento() != null) {
			List<Municipio> municipios = this.getGeneralesService().getCacheMunicipiosPorDepartamento(
					this.propietarioSeleccionado.getPPersona().getDireccionDepartamento().getCodigo());
			if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
				Collections.sort(municipios);
			} else {
				Collections.sort(municipios, Municipio.getComparatorNombre());
			}
			for (Municipio m : municipios) {
				if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
					municipiosC.add(new SelectItem(m.getCodigo(), m.getCodigo() + "-" + m.getNombre()));
				} else {
					municipiosC.add(new SelectItem(m.getCodigo(), m.getNombre()));
				}
			}
		}
		return municipiosC;
	}

	public void cambiarOrdenPaises() {
		if (this.ordenPaises.equals(EOrden.CODIGO)) {
			this.ordenPaises = EOrden.NOMBRE;
		} else {
			this.ordenPaises = EOrden.CODIGO;
		}
	}

	public void cambiarOrdenDepartamentos() {
		if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
			this.ordenDepartamentos = EOrden.NOMBRE;
		} else {
			this.ordenDepartamentos = EOrden.CODIGO;
		}
	}

	public void cambiarOrdenMunicipios() {
		if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
			this.ordenMunicipios = EOrden.NOMBRE;
		} else {
			this.ordenMunicipios = EOrden.CODIGO;
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Función que elimina un predio o una servidumbre
	 *
	 * @author javier.aponte
	 */
	/*
	 * @modified by leidy.gonzalez:: 11-05-2015:: 11117:: Se modifica manera de
	 * eliminar la servidumbre
	 */
	public void eliminarServidumbre() {

		if (this.opcionEliminar.equals("servidumbres")) {

			this.predioSeleccionado.getPPredioServidumbre().remove(selectedPredioServidumbre);

			this.getConservacionService().borrarPPredioServidumbres1(selectedPredioServidumbre.getId());

			this.getConservacionService().guardarActualizarPPredio(this.predioSeleccionado);

		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Función encargada de eliminar la PUnidadConstrucción que se encuentre
	 * seleccionada
	 *
	 * @author fabio.navarrete
	 */
	public void eliminarPUnidadConstruccion() {
		try {
			LOGGER.info("eliminarPUnidadConstruccion");

			// Borrar o cancelar fotos asociadas a la unidad de construcción
			this.verificarEliminacionFotosDePUnidadConstruccion();

			PUnidadConstruccion auxpuc = (PUnidadConstruccion) this.getConservacionService().eliminarProyeccion(usuario,
					this.unidadConstruccionSeleccionada);

			if (auxpuc != null) {
				this.actualizarPUnidadsConstruccion(auxpuc);
			} else {
				this.predioSeleccionado.getPUnidadConstruccions().remove(this.unidadConstruccionSeleccionada);
			}
			// felipe.cadena::18-01-2017::actualiza contrucciones disponibles para
			// asociacion
			if (this.tramite.isCancelacionMasiva()) {
				this.getGestionDetalleAvaluoMB().cargarConstruccionesCancelacionMasiva();
			}

			// leidy.gonzalez::27036::04-07-2017::actualiza contrucciones disponibles para
			// asociacion
			// del tramite de englobe virtual
			if (this.tramite.isEnglobeVirtual()) {

				this.getGestionDetalleAvaluoMB()
						.eliminarConstruccionesEnglobeVirtual(this.unidadConstruccionSeleccionada);
			}

			if (this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.getCondicionPropiedad()
					.equals(EPredioCondicionPropiedad.CP_0.getCodigo()) && !this.tramite.isEnglobeVirtual()) {
				this.cargarConstruccionesDesenglobe();
			} else if (this.predioSeleccionado.getCondicionPropiedad()
					.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
				this.cargarConstruccionesDesenglobeMejora();
			}

			// ACTUALIZAR AVALÚO
			if (this.banderaPredioFiscal) {
				this.guardarAvaluoPredioFiscal();
			} else {
				this.recalcularAvaluo(true);
			}
			// CONSULTAR PREDIO ACTUALIZADO
			if (this.predioSeleccionado != null) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
			}
			this.setAvaluo();
			// Se actualiza el valor de las áreas
			this.guardarAreas();

			if (this.tramite.isTercera() || this.tramite.isSegundaDesenglobe() || this.tramite.isQuinta()) {
				this.obligarGeografico();
			}

			if (this.tramite.isSegundaDesenglobe() && !this.tramite.isDesenglobeEtapas()) {
				this.cargarConstruccionesAsociadasPredioOrigen();
			}

			if (this.tramite.isEnglobeVirtual() && this.predioSeleccionado.isEsPredioFichaMatriz()) {
				this.getFichaMatrizMB().init();
			}

		} catch (Exception e) {
			this.addMensajeError(e);
			LOGGER.error(e.getMessage());
		}
	}

	/**
	 * Método que verifica si una foto se debe eliminar de la tabla de proyección, o
	 * colocar en estado cancelado (Son comportamientos diferentes en Aplicar
	 * cambios)
	 *
	 * @author david.cifuentes
	 */
	private void verificarEliminacionFotosDePUnidadConstruccion() {

		try {
			if (this.unidadConstruccionSeleccionada != null) {
				if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo()
						.equals(this.unidadConstruccionSeleccionada.getCancelaInscribe())) {

					// Si la unidad esta inscrita quiere decir que es una nueva
					// unidad, es decir, las fotos asociadas a la misma se deben
					// borrar.
					this.getConservacionService()
							.borrarPFotosDeLaPUnidadConstruccion(this.unidadConstruccionSeleccionada.getId());

				} else if (EProyeccionCancelaInscribe.CANCELA.getCodigo()
						.equals(this.unidadConstruccionSeleccionada.getCancelaInscribe())
						|| EProyeccionCancelaInscribe.MODIFICA.getCodigo()
								.equals(this.unidadConstruccionSeleccionada.getCancelaInscribe())) {

					// CANCELA: Partiendo que ninguna unidad cancelada se puede
					// cancelar,
					// este caso no se debería presentar, más sin embargo se
					// valida
					// que las fotos también queden canceladas.
					// MODIFICA: Si la unidad a cancelar estaba en modifica,
					// significa que viene de estar en firme, es decir se deben
					// cancelar también sus fotos, y colocarlas en el estado
					// inicial
					// de la proyección, que es CANCELA.
					boolean persistirCambiosBool = false;
					List<PFoto> pFotosUnidad = this.getConservacionService()
							.buscarPFotosUnidadConstruccion(this.unidadConstruccionSeleccionada.getId(), false);

					if (pFotosUnidad != null && !pFotosUnidad.isEmpty()) {
						for (PFoto pf : pFotosUnidad) {
							if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pf.getCancelaInscribe())) {
								pf.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
								persistirCambiosBool = true;
							}
						}
					}

					if (persistirCambiosBool) {
						this.getConservacionService().actualizarPFotosUnidadConstruccion(pFotosUnidad);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("ERROR: en verificarEliminacionFotosDePUnidadConstruccion: " + e.getMessage());
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Refrezca los datos para la tabla de unidades de construcción tras una
	 * modificación
	 *
	 * @author fabio.navarrete
	 */
	public void actualizarPUnidadsConstruccion(PUnidadConstruccion puc) {
		for (int i = 0; i < this.predioSeleccionado.getPUnidadConstruccions().size(); i++) {
			if (this.predioSeleccionado.getPUnidadConstruccions().get(i).getId().longValue() == puc.getId()
					.longValue()) {
				this.predioSeleccionado.getPUnidadConstruccions().remove(i);
				this.predioSeleccionado.getPUnidadConstruccions().add(i, puc);
				return;
			}
		}
		this.predioSeleccionado.getPUnidadConstruccions().add(puc);
	}

	// --------------------------------------------------------------------- //
	/**
	 * Metodo que adiciona una servidumbre asociada a un predio
	 *
	 * @author javier.aponte
	 */
	/*
	 * @modified by leidy.gonzalez :: 30-04-2015 :: adición de validación para que
	 * almacene en la Base de Datos la Servidumbre generando un clone de la
	 * servidumbre
	 */
	public void adicionarServidumbre() {

		LOGGER.debug(
				"Entro a ProyectarConservacion - adicionar servidumbre: " + this.servidumbreAdicional.getServidumbre());
		// Validar que no se haya adicionado esta servidumbre para el predio
		// seleccionado
		if (validarAdicionarServidumbre()) {

			this.servidumbreAdicional.setPPredio(this.predioSeleccionado);
			this.servidumbreAdicional.setFechaInscripcionCatastral(new Date());
			this.servidumbreAdicional.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
			this.servidumbreAdicional.setUsuarioLog(this.usuario.getLogin());
			this.servidumbreAdicional.setFechaLog(new Date());
			PPredioServidumbre pPredioServidumbreTemp;
			pPredioServidumbreTemp = (PPredioServidumbre) this.servidumbreAdicional.clone();

			this.servidumbreAdicional = this.getConservacionService().adicionarServidumbre((pPredioServidumbreTemp));

			this.selectedPredioServidumbre = this.servidumbreAdicional;

			try {

				this.predioSeleccionado.getPPredioServidumbre().add(this.servidumbreAdicional);
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				this.addMensajeError(e);
			}
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que realiza las validaciones al adicionar un
	 * {@link PPredioServidumbre} al {@link PPredio}
	 *
	 * @return
	 */
	public boolean validarAdicionarServidumbre() {
		boolean validate = true;
		if (this.predioSeleccionado != null && this.predioSeleccionado.getPPredioServidumbre() != null
				&& !this.predioSeleccionado.getPPredioServidumbre().isEmpty()) {
			for (PPredioServidumbre pps : this.predioSeleccionado.getPPredioServidumbre()) {
				if (pps.getServidumbre().equals(this.servidumbreAdicional.getServidumbre())) {
					this.addMensajeError(
							"La servidumbre ya se encuentra asociada al predio, por favor asocie una diferente.");
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					validate = false;
					break;
				}
			}
		}
		return validate;
	}

	// --------------------------------------------------------------------- //
	/**
	 * @author fabio.navarrete Determina
	 * @return
	 */
	public boolean isGestionUnidadHabilitado() {
		if (this.tramite.getEstado().equals("TODO:PENDIENTE DE VALIDACIÓN DE ESTADOS")) {
			return true;
		}
		// TODO :: fredy.wilches :: revisar el siguiente TODO :: pedro.garcia
		// TODO:PENDIENTE DE VALIDACIÓN DE ESTADOS
		// return false;
		return true;
	}

	// --------------------------------------------------------------------- //
	/**
	 * @author fabio.navarrete
	 * @return
	 */
	public boolean isModificarAreasHabilitado() {
		// TODO :: fredy.wilches :: Modificado por fredy.wilches mientras se
		// define cuando son de edicion,
		return false;
		/*
		 * this.tramite.getTipoTramite().equals(
		 * ETramiteTipoTramite.AUTOESTIMACION.getCodigo()) |
		 * this.predioSeleccionado.getTipoCatastro().equals(
		 * EPredioTipoCatastro.LEY_14.getCodigo());
		 */
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que guarda los datos referentes a la pantalla de gestión de avalúo.
	 *
	 * @author fabio.navarrete
	 */
	/*
	 * @modified by juanfelipe.garcia :: 06-09-2013 :: adición de validación campo
	 * autoestimaciones seleccionado
	 */
	public void guardarGestionAvaluo() {

		try {
			// Validaciones del área de las construcciones asociadas
			if (this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.isPHCondominio()) {
				if (!this.validarAreasConstruccionesAsociadasEnDetalleAvaluo(this.predioSeleccionado)) {
					return;
				} else {
					this.actualizarAreasFichaMatriz(true);
				}
			}

			boolean valido = this.validarAvaluo(this.predioSeleccionado);

			if (!valido) {
				return;
			}

			if (this.avaluo != null && !ESiNo.SI.getCodigo().equals(this.avaluo.getAutoavaluo())
					&& this.tramite.isCuarta()
					&& !this.modeloResolucionSeleccionado.getTextoTitulo().contains("RECHAZA")) {
				this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
				this.addMensajeInfo("Detalle del avalúo almacenado de forma exitosa");
			}
		} catch (Exception e) {
			LOGGER.error("error en ProyectarConservacionMB#guardarGestionAvaluo");
		}
	}

	public void setPaisPropietario(String codigo) {
		this.propietarioSeleccionado.getPPersona().setDireccionDepartamento(new Departamento());
		Pais paisSelected = null;
		if (codigo != null) {
			paisSelected = new Pais();
			paisSelected.setCodigo(codigo);
		}
		this.propietarioSeleccionado.getPPersona().setDireccionPais(paisSelected);
	}

	public String getPaisPropietario() {
		if (this.propietarioSeleccionado != null
				&& this.propietarioSeleccionado.getPPersona().getDireccionPais() != null) {
			return this.propietarioSeleccionado.getPPersona().getDireccionPais().getCodigo();
		}
		return null;
	}

	public void setDepartamentoPropietario(String codigo) {
		Departamento dpto = null;
		if (codigo != null) {
			dpto = new Departamento();
			dpto.setCodigo(codigo);
		}
		this.propietarioSeleccionado.getPPersona().setDireccionDepartamento(dpto);
	}

	public String getDepartamentoPropietario() {
		if (this.propietarioSeleccionado != null
				&& propietarioSeleccionado.getPPersona().getDireccionDepartamento() != null) {
			return this.propietarioSeleccionado.getPPersona().getDireccionDepartamento().getCodigo();
		}
		return null;
	}

	public void setMunicipioPropietario(String codigo) {
		Municipio muni = null;
		if (codigo != null) {
			muni = new Municipio();
			muni.setCodigo(codigo);
		}
		this.propietarioSeleccionado.getPPersona().setDireccionMunicipio(muni);
	}

	public String getMunicipioPropietario() {
		if (this.propietarioSeleccionado != null
				&& this.propietarioSeleccionado.getPPersona().getDireccionMunicipio() != null) {
			return this.propietarioSeleccionado.getPPersona().getDireccionMunicipio().getCodigo();
		}
		return null;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * ... como en el combo de uso de unidad se usó el id como valor, se define el
	 * uso de unidad seleccionado como objeto del tipo definido para ese atributo
	 *
	 * @param selectedUsoUnidadConstruccionId
	 */
	public void setSelectedUsoUnidadConstruccionId(Long selectedUsoUnidadConstruccionId) {

		this.selectedUsoUnidadConstruccionId = selectedUsoUnidadConstruccionId;

		for (VUsoConstruccionZona uc : this.usosConstruccionList) {
			if (uc.getId().longValue() == selectedUsoUnidadConstruccionId.longValue()) {
				this.selectedUsoUnidadConstruccion = this.getGeneralesService().getCacheUsoConstruccionById(uc.getId());
				break;
			}
		}
		if (this.selectedUsoUnidadConstruccion != null) {
			if (this.unidadConstruccionSeleccionada != null) {
				this.unidadConstruccionSeleccionada.setTipoCalificacion(EUnidadConstruccionTipoCalificacion
						.getCodigoByNombre(this.selectedUsoUnidadConstruccion.getDestinoEconomico()));
			}
			if (this.unidadDelModeloDeConstruccionSeleccionada != null) {
				this.unidadDelModeloDeConstruccionSeleccionada.setTipoCalificacion(EUnidadConstruccionTipoCalificacion
						.getCodigoByNombre(this.selectedUsoUnidadConstruccion.getDestinoEconomico()));
			}
		}
	}

	// --------------------------------------------------------------------------------------------------
	public TreeNode getTreeComponentesConstruccion() {
		return this.treeComponentesConstruccion;
	}

	// --------------------------------------------------------------------------------------------------
	public void setTreeComponentesConstruccion(TreeNode treeComponentesConstruccion) {
		this.treeComponentesConstruccion = treeComponentesConstruccion;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * @param calificacionConstruccionElements lista de objetos de tipo
	 *                                         CalificacionConstruccion obtenida de
	 *                                         la consulta a la BD
	 *
	 *                                         OJO: por el modo en que se arma el
	 *                                         árbol (adicionando objetos que se
	 *                                         traen de la consulta) hay que hacer
	 *                                         la consulta cada vez que se vaya a
	 *                                         armar cada uno de los árboles. Si no
	 *                                         se hace los árboles quedan con los
	 *                                         datos "pegados" del primero
	 */
	private TreeNode armarArbolCalificacionConstruccion(String tipoCalificacion) {

		// D: obtener los registros de la tabla que sirve para armar el árbol
		List<CalificacionConstruccion> calificacionConstruccionElements = this.getConservacionService()
				.obtenerTodosCalificacionConstruccion();

		TreeNode answer;
		answer = new DefaultTreeNode("root" + tipoCalificacion, null);

		List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
		TreeNode componenteToAdd;
		String currentComponenteName, tempComponenteName;
		String currentElementoName, tempElementoName;
		CalificacionConstruccion incompleteToAdd;
		CalificacionConstruccionTreeNode ccNode, cctnTemp, cctnTempL2;
		int puntosPorCalificacionConstruccion;
		boolean addedNode = false;

		// D: armado de la columna "componente construcción"
		currentComponenteName = "";
		for (CalificacionConstruccion comp : calificacionConstruccionElements) {
			tempComponenteName = comp.getComponente();

			// D: no se adicionan como nodos los "componente" que no tengan por
			// lo menos un "elemento" cuyos puntos sean >= 0
			puntosPorCalificacionConstruccion = getPuntosPorCalificacionConstruccion(comp, tipoCalificacion);

			if (tempComponenteName.compareToIgnoreCase(currentComponenteName) != 0) {

				if (puntosPorCalificacionConstruccion >= 0 && !(tipoCalificacion
						.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())
						&& tempComponenteName.equals(EComponenteConstruccionCom.COMPLEMENTO_INDUSTRIA.getCodigo()))) {
					incompleteToAdd = new CalificacionConstruccion();
					incompleteToAdd.setComponente(tempComponenteName);
					ccNode = new CalificacionConstruccionTreeNode();
					ccNode.setCalificacionConstruccion(incompleteToAdd);
					ccNode.setMostrarIconoSubirImagen(true);
					// ccNode.setRutaArchivoLocal(this.rutaWebImagenBase);
					ccNode.setUrlArchivoWebTemporal(this.rutaWebImagenBase);
					componenteToAdd = new DefaultTreeNode("componente", ccNode, answer);

					addedNode = true;
				} else {
					addedNode = false;
				}

				currentComponenteName = tempComponenteName;
			} else {
				if (puntosPorCalificacionConstruccion >= 0 && !(tipoCalificacion
						.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())
						&& tempComponenteName.equals(EComponenteConstruccionCom.COMPLEMENTO_INDUSTRIA.getCodigo()))) {
					if (!addedNode) {
						incompleteToAdd = new CalificacionConstruccion();
						incompleteToAdd.setComponente(tempComponenteName);
						ccNode = new CalificacionConstruccionTreeNode();
						ccNode.setCalificacionConstruccion(incompleteToAdd);
						ccNode.setMostrarIconoSubirImagen(true);
						// ccNode.setRutaArchivoLocal(this.rutaWebImagenBase);
						ccNode.setUrlArchivoWebTemporal(this.rutaWebImagenBase);
						componenteToAdd = new DefaultTreeNode("componente", ccNode, answer);

						addedNode = true;
					}
				}
			}
		}

		// D: armado de los elementos de la columna "elemento calificación"
		currentComponenteName = "";
		currentElementoName = "";
		calificacionConstruccionNodesLevel1 = answer.getChildren();
		for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
			cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
			tempComponenteName = cctnTemp.getCalificacionConstruccion().getComponente();
			currentComponenteNode.setExpanded(true);
			if (tempComponenteName.compareToIgnoreCase(currentComponenteName) != 0) {
				for (CalificacionConstruccion comp : calificacionConstruccionElements) {
					tempElementoName = comp.getElemento();
					if (tempElementoName.compareToIgnoreCase(currentElementoName) != 0
							&& comp.getComponente().compareToIgnoreCase(tempComponenteName) == 0) {

						puntosPorCalificacionConstruccion = getPuntosPorCalificacionConstruccion(comp,
								tipoCalificacion);

						// D: no se adicionan los "elemento" que no apliquen
						// para el "componente";
						// es decir, que tengan puntos < 0
						if (puntosPorCalificacionConstruccion >= 0) {
							incompleteToAdd = new CalificacionConstruccion();
							incompleteToAdd.setElemento(tempElementoName);
							ccNode = new CalificacionConstruccionTreeNode();
							ccNode.setCalificacionConstruccion(incompleteToAdd);
							componenteToAdd = new DefaultTreeNode("elemento", ccNode, currentComponenteNode);
						}

						currentElementoName = tempElementoName;
					}
				}
				currentComponenteName = tempComponenteName;
				currentElementoName = "";
			}
		}
		// D: adición de los combos a cada elemento de nivel 2
		ArrayList<CalificacionConstruccion> comboelements;
		for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
			cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
			currentComponenteName = cctnTemp.getCalificacionConstruccion().getComponente();
			calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
			currentComponenteNode.setExpanded(true);
			for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
				cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
				currentElementoName = cctnTempL2.getCalificacionConstruccion().getElemento();
				comboelements = new ArrayList<CalificacionConstruccion>();
				currentElementoNode.setExpanded(true);
				for (CalificacionConstruccion comp : calificacionConstruccionElements) {
					tempComponenteName = comp.getComponente();
					tempElementoName = comp.getElemento();
					if (currentComponenteName.compareToIgnoreCase(tempComponenteName) == 0
							&& currentElementoName.compareToIgnoreCase(tempElementoName) == 0) {
						comboelements.add(comp);
					}
				}
				// cctnTempL2.setTipoCalificacion(this.currentUnidadConstruccion.getTipoCalificacion());
				cctnTempL2.setTipoCalificacion(tipoCalificacion);

				// D: se cambian los elementos mismos (de nivel 2)
				// adicionándoles el combo
				cctnTempL2.armarComboDetalleCalificacion(comboelements);
			}
		}

		return answer;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * calcula ese valor dependiendo del árbol que se esté trabajando
	 *
	 * @author pedro.garcia
	 * @param calificacionConstruccion
	 * @param tipoCalificacion
	 * @return
	 */
	private int getPuntosPorCalificacionConstruccion(CalificacionConstruccion calificacionConstruccion,
			String tipoCalificacion) {

		int answer = -1;

		if (tipoCalificacion.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
			answer = calificacionConstruccion.getPuntosComercial().intValue();
		} else if (tipoCalificacion.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
			answer = calificacionConstruccion.getPuntosIndustrial().intValue();
		} else if (tipoCalificacion.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
			answer = calificacionConstruccion.getPuntosResidencial().intValue();
		}

		return answer;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * @author pedro.garcia
	 */
	public int getTotalPuntosConstruccion() {

		int answer = 0;
		this.validarCalificacionComponenteBool = false;
		List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
		CalificacionConstruccionTreeNode cctnTempL2;
		if ((this.tramite.isEsComplementacion()
				|| (this.informacionMigracionIncompleta && !this.tramite.isSegundaDesenglobe()))
				&& !(this.tramite.isTercera() || this.tramite.isQuinta())
				&& this.unidadConstruccionSeleccionada.getTotalPuntaje() != null) {
			return this.unidadConstruccionSeleccionada.getTotalPuntaje().intValue();
		} else {
			if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
				this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
			} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
				this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
			} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
				this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
			}

			calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion.getChildren();
			for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
				calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
				for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
					cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
					answer += (cctnTempL2.getSelectedDetalle() == null) ? 0
							: cctnTempL2.getSelectedDetalle().getPuntos();
				}
			}
			return answer;
		}
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * OJO: el maldito jsf acepta armar SelectItem con cualquier clase de objeto,
	 * pero al hacer la selección, NO FUNCIONA!!. Por lo tanto hay que armarlo con
	 * tipo Java simple y hacer doble trabajo de búsqueda.
	 *
	 * modified by javier.aponte: Se concatena el id del uso de la unidad de
	 * construcción con el nombre en el label del SelectItem
	 *
	 * @author pedro.garcia
	 * @modified javier.aponte
	 * @modified felipe.cadena - se filtra el tipo de uso de unidad para
	 *           rectificaciones y complemetaciones
	 * @param usosConstruccionList
	 */
	public void setUsosUnidadConstruccionItemList(List<VUsoConstruccionZona> usosConstruccionList) {

		if (this.tramite.isEsComplementacion() || this.tramite.isEsRectificacion()) {

			this.selectedUsoUnidadConstruccionId = this.unidadConstruccionSeleccionada.getUsoConstruccion().getId();
			this.banderaConvencionalRectifica = this.unidadConstruccionSeleccionada.getTipoConstruccion()
					.equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());
			if (this.banderaConvencionalRectifica) {
				for (VUsoConstruccionZona uc : usosConstruccionList) {
					if (uc.getTipoUso().equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
						this.usosUnidadConstruccionItemList
								.add(new SelectItem(uc.getId(), uc.getId() + "-" + uc.getNombre()));
					}
				}
			} else {
				for (VUsoConstruccionZona uc : usosConstruccionList) {
					if (uc.getTipoUso().equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
						this.usosUnidadConstruccionItemList
								.add(new SelectItem(uc.getId(), uc.getId() + "-" + uc.getNombre()));
					}
				}
			}
		} else {
			for (VUsoConstruccionZona uc : usosConstruccionList) {
				this.usosUnidadConstruccionItemList.add(new SelectItem(uc.getId(), uc.getId() + "-" + uc.getNombre()));

			}
		}
	}

	// --------------------------------------------------------------------------------------------------
	public PReferenciaCartografica getReferenciaCartograficaSeleccionada() {
		return this.referenciaCartograficaSeleccionada;
	}

	public void setReferenciaCartograficaSeleccionada(PReferenciaCartografica referenciaCartograficaSeleccionada) {
		this.referenciaCartograficaSeleccionada = referenciaCartograficaSeleccionada;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * retorna la tipificación de la unidad de construcción dependiendo del puntaje
	 * total y de la clasificación correspondiente en la tabla
	 * TIPIFICACION_UNIDAD_CONST
	 *
	 * @author pedro.garcia
	 */
	public String getTipificacionUnidadConstruccion() {
		TipificacionUnidadConst tuc = this.getConservacionService()
				.obtenerTipificacionUnidadConstrPorPuntos(this.getTotalPuntosConstruccion());
		return tuc.getTipificacion();
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * acción del botón 'guardar cambios' en la ventane de detalles de la unidad de
	 * construcción. Guarda en la tabla P_UNIDAD_CONSTRUCCION_COMP los valores de la
	 * tabla de componente, elemento, detalle, puntos Guarda las fotos de los
	 * componentes de la unidad de construcción nueva o modificada.
	 *
	 * @author pedro.garcia
	 */
	/*
	 * @modified by fabio.navarrete
	 *
	 * @modified pedro.garcia 08-05-2013 orden, eliminación de chambonadas
	 */
	public void guardarDatosUnidadConstruccionYComponentes() {

		boolean isAnexo = true;
		List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
		List<PUnidadConstruccionComp> newUnidadesConstruccionComp;
		PUnidadConstruccionComp toInsert;
		CalificacionConstruccionTreeNode cctnTempL1, cctnTempL2;
		CalificacionConstruccion selectedDetalle;
		String currentComponenteName, currentElementoName;
		Date currentDate;
		Dominio valorDominio;

		// felipe.cadena::30-01-2017::cancelacion masiva:: validaciones
		if (this.tramite.isCancelacionMasiva()) {
			if (!this.getGestionDetalleAvaluoMB()
					.validacionesConstruccionesCancelacionMasiva(this.unidadConstruccionSeleccionada)) {
				return;
			}
		}

		// leidy.gonzalez::10-07-2017::englobe Virtual:: validaciones
		if (this.tramite.isEnglobeVirtual()) {
			if (!this.getGestionDetalleAvaluoMB()
					.validacionesConstruccionesEnglobeVirtual(this.unidadConstruccionSeleccionada)) {
				return;
			}
		}

		// hector.arias::26-11-2018::Segunda desenglobe
		// no validar el año de construcción
		if (this.tramite.isSegundaDesenglobe()
				&& this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0)) {
			if (this.unidadConstruccionSeleccionada.getAnioConstruccion() == null) {
				this.unidadConstruccionSeleccionada.setAnioConstruccion(null);
			}
		}

		// felipe.cadena::30-07-2015::rectificacion masiva:: validaciones
		// rectificacion masiva
		if (this.tramite.isRectificacionMatriz()) {

			boolean rectificaAnio = false;
			for (TramiteRectificacion re : this.tramite.getTramiteRectificacions()) {
				if (re.getDatoRectificar().getColumna().equals(Constantes.ANIO_CONSTRUCCION)) {
					rectificaAnio = true;
				}
			}

			if (rectificaAnio) {

				// valida año actual
				Calendar c = Calendar.getInstance();
				int anioActual = c.get(Calendar.YEAR);

				if (this.unidadConstruccionSeleccionada.getAnioConstruccion() > anioActual) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError(ECodigoErrorVista.RECTIFICACION_ANIO_MASIVO_001.getMensajeUsuario());
					LOGGER.warn(ECodigoErrorVista.RECTIFICACION_ANIO_MASIVO_001.getMensajeTecnico());
					return;
				}

				// valida si el año cambió
				int anioEnFirme = 0;

				if (this.fichaMatrizMB == null) {
					this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
				}

				List<PUnidadConstruccion> construccionesCVigentes = this.fichaMatrizMB
						.getUnidadConstruccionConvencionalVigentes();
				List<PUnidadConstruccion> construccionesNCVigentes = this.fichaMatrizMB
						.getUnidadConstruccionNoConvencionalVigentes();

				for (PUnidadConstruccion puc : construccionesCVigentes) {
					if (puc.getUnidad().equals(this.unidadConstruccionSeleccionada.getUnidad())) {
						anioEnFirme = puc.getAnioConstruccion();
						break;
					}
				}

				for (PUnidadConstruccion puc : construccionesNCVigentes) {
					if (puc.getUnidad().equals(this.unidadConstruccionSeleccionada.getUnidad())) {
						anioEnFirme = puc.getAnioConstruccion();
						break;
					}
				}

				if (this.unidadConstruccionSeleccionada.getAnioConstruccion() == anioEnFirme) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError(ECodigoErrorVista.RECTIFICACION_ANIO_MASIVO_002.getMensajeUsuario());
					LOGGER.warn(ECodigoErrorVista.RECTIFICACION_ANIO_MASIVO_002.getMensajeTecnico());
					return;
				}
			}
		}

		// D: verifica si se trata de un anexo
		if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())
				|| this.unidadConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())
				|| this.unidadConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {

			// se adiciona para no persistir el detalle cuando es predio fiscal
			if (this.banderaPredioFiscal) {
				isAnexo = true;
			} else {
				isAnexo = false;
			}

			if (this.unidadConstruccionSeleccionada.getAreaConstruida() != null
					&& (this.unidadConstruccionSeleccionada.getAreaConstruida().doubleValue() < 0.0
							|| this.unidadConstruccionSeleccionada.getAreaConstruida() > 999999.0)) {

				this.addErrorCallback();
				if (this.unidadConstruccionSeleccionada.getAreaConstruida().doubleValue() == 0.0) {
					this.addMensajeError(ECodigoErrorVista.DESENGLOBE_VAL_UNIDAD_CONSTRUCCION__002
							.getMensajeUsuario(this.unidadConstruccionSeleccionada.getUnidad()));
					LOGGER.warn(ECodigoErrorVista.DESENGLOBE_VAL_UNIDAD_CONSTRUCCION__002.getMensajeTecnico());
					return;
				} else {
					this.addMensajeError(ECodigoErrorVista.DESENGLOBE_VAL_UNIDAD_CONSTRUCCION__003
							.getMensajeUsuario(this.unidadConstruccionSeleccionada.getUnidad()));
					LOGGER.warn(ECodigoErrorVista.DESENGLOBE_VAL_UNIDAD_CONSTRUCCION__003.getMensajeTecnico());
					return;
				}
			}
		}

		// Valor para la unidad no puede ser repetido
		if (this.tramite.isSegundaDesenglobe() && this.predioSeleccionado != null) {
			List<PUnidadConstruccion> unidadesConstruccionNuevas = new ArrayList<PUnidadConstruccion>();
			if (this.predioSeleccionado.getPUnidadesConstruccionConvencionalNuevas() != null
					&& !this.predioSeleccionado.getPUnidadesConstruccionConvencionalNuevas().isEmpty()) {
				unidadesConstruccionNuevas.addAll(this.predioSeleccionado.getPUnidadesConstruccionConvencionalNuevas());
			}
			if (this.predioSeleccionado.getPUnidadesConstruccionNoConvencionalNuevas() != null
					&& !this.predioSeleccionado.getPUnidadesConstruccionNoConvencionalNuevas().isEmpty()) {
				unidadesConstruccionNuevas
						.addAll(this.predioSeleccionado.getPUnidadesConstruccionNoConvencionalNuevas());
			}
			for (PUnidadConstruccion puc : unidadesConstruccionNuevas) {
				if (this.unidadConstruccionSeleccionada.getId() == null
						|| !this.unidadConstruccionSeleccionada.getId().equals(puc.getId())) {
					if (this.unidadConstruccionSeleccionada.getUnidad().equals(puc.getUnidad())) {
						this.addMensajeError(ECodigoErrorVista.DESENGLOBE_VAL_UNIDAD_CONSTRUCCION__001
								.getMensajeUsuario(this.predioSeleccionado.getNumeroPredial()));
						LOGGER.warn(ECodigoErrorVista.DESENGLOBE_VAL_UNIDAD_CONSTRUCCION__001.getMensajeTecnico());
						this.addErrorCallback();
						return;
					}
				}
			}
		}

		if (!this.tramite.isRectificacionAreaConstruccion() && !this.tramite.isRectificacionDetalleCalificacion()) {
			// D: valida número de habitaciones mínimo para una construcción
			// residencial
			if (!this.banderaPredioFiscal) {
				if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
					if (this.unidadConstruccionSeleccionada.getTotalHabitaciones() == null
							|| this.unidadConstruccionSeleccionada.getTotalHabitaciones() == 0) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("La construcción por ser residencial debe tener al menos una habitación");
						return;
					}
				}
			}
			// D: valida condiciones de baños
			if (!isAnexo && !this.tramite.isEsComplementacion() && !this.tramite.isRectificacionAreaConstruccion()) {
				if (this.isSinBano() && this.unidadConstruccionSeleccionada.getTotalBanios() != 0) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError("La construcción no tiene baños, así que el total de baños debe ser 0");
					return;
				}
				if (this.isCalificaBano() && !this.isSinBano()
						&& this.unidadConstruccionSeleccionada.getTotalBanios() == 0) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError(
							"La construcción tiene baños, así que el total de baños debe ser diferente de 0");
					return;
				}
			}
		}

		// D: seteo de algunos valores que dependen del tipo de construcción
		if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
			this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
			this.unidadConstruccionSeleccionada.setTipificacion(EUnidadConstruccionTipificacion.NA.getCodigo());
		} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
			this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
			this.unidadConstruccionSeleccionada.setTipificacion(EUnidadConstruccionTipificacion.NA.getCodigo());
		} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
			this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
			this.unidadConstruccionSeleccionada.setTipificacion(
					EUnidadConstruccionTipificacion.ANY.getCodigoByValor(this.getTipificacionUnidadConstruccion()));
		} else {

			this.unidadConstruccionSeleccionada
					.setTipoConstruccion(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo());
			this.unidadConstruccionSeleccionada.setTotalPuntaje(Double.valueOf(0));
			this.unidadConstruccionSeleccionada.setTipificacion(EUnidadConstruccionTipificacion.NA.getCodigo());
			this.unidadConstruccionSeleccionada.setAreaConstruida(this.areaNoConvencional);
			this.unidadConstruccionSeleccionada.setPisoUbicacion("");
			if (this.selectedCalificacionAnexo != null) {
				this.unidadConstruccionSeleccionada.setCalificacionAnexoId(this.selectedCalificacionAnexo.getId());
				this.unidadConstruccionSeleccionada.setDescripcion(this.selectedCalificacionAnexo.getDescripcion());
			}
		}

		// Validaciones asociadas al área de las construcciones asociadas
		if (this.tramite.isSegundaDesenglobe() && !this.tramite.isEnglobeVirtual()
				&& !this.validarAreaConstruccionAsociadasEnGuardarDetalleAvaluo(this.predioSeleccionado)) {
			Double areaReplica = this.unidadConstruccionSeleccionada.getAreaConstruida();
			this.initGestionDetalleUnidadConstruccion();
			this.unidadConstruccionSeleccionada.setAreaConstruida(areaReplica);
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			return;
		}

		try {
			// Set del valor correspondiente del dominio para la tipificación
			valorDominio = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
					EDominio.UNIDAD_CONSTRUCION_TIPIFICA, this.unidadConstruccionSeleccionada.getTipificacion());

			if (valorDominio != null) {
				this.unidadConstruccionSeleccionada.setTipificacionValor(valorDominio.getValor());
			}

			if (!isAnexo) {
				this.unidadConstruccionSeleccionada
						.setTipoConstruccion(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());

				if (this.tramite.isEsComplementacion()) {
					this.unidadConstruccionSeleccionada
							.setTotalPuntaje(this.unidadConstruccionSeleccionada.getTotalPuntaje());
				} else {

					if (this.banderaDesenglobeMejora || this.tramite.isSegundaDesenglobe()) {
						PUnidadConstruccion puc = this.getConservacionService()
								.buscarUnidadDeConstruccionPorSuId(this.unidadConstruccionSeleccionada.getId());

						if (puc.getTotalPuntaje() != this.getTotalPuntosConstruccion()) {

							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							this.addMensajeError(
									"El puntaje debe coincidir con el puntaje original de la construcción : "
											+ puc.getTotalPuntaje());
							return;
						}
					}
					if (this.banderaPredioFiscal) {
						this.unidadConstruccionSeleccionada.setTotalPuntaje(0.0);
						this.guardarAvaluoPredioFiscal();
					} else {
						this.unidadConstruccionSeleccionada
								.setTotalPuntaje(Double.valueOf(this.getTotalPuntosConstruccion()));
					}
				}
			} else {

				if (this.banderaPredioFiscal) {
					this.unidadConstruccionSeleccionada.setTotalPuntaje(0.0);
					if (this.tramite.isSegundaDesenglobe() || this.tramite.isTercera() || this.tramite.isQuinta()) {
						this.unidadConstruccionSeleccionada
								.setValorM2Construccion(this.unidadConstruccionSeleccionada.getAvaluo()
										/ this.unidadConstruccionSeleccionada.getAreaConstruida());
					}

				} else {
					this.unidadConstruccionSeleccionada
							.setTotalPuntaje(Double.valueOf(this.tipoDescripcionConstruccion));
				}
			}

			this.unidadConstruccionSeleccionada.setUsoConstruccion(this.selectedUsoUnidadConstruccion);

			// D: si se creó una nueva unidad de construcción, se guarda en la
			// bd y se obtiene el
			// id para poder guardar los datos en la tabla relacionada.
			// Si no tiene 'unidad' se supone que es nueva
			if (this.unidadConstruccionSeleccionada.getUnidad() != null) {
				this.unidadConstruccionSeleccionada
						.setUnidad(this.unidadConstruccionSeleccionada.getUnidad().toUpperCase());
			}

			LOGGER.debug("Almacenando datos de la unidad de construcción...");
			PUnidadConstruccion auxPUnidadConstruccion = (PUnidadConstruccion) this.getConservacionService()
					.guardarProyeccion(this.usuario, this.unidadConstruccionSeleccionada);
			LOGGER.debug("... Datos de la unidad de construcción almacenados.");
			this.unidadConstruccionSeleccionada.setId(auxPUnidadConstruccion.getId());

			// D: ... si es anexo (no convencional), puede que antes haya sido
			// convencional, lo que implicaría que existan registros en la tabla
			// P_UNIDAD_CONSTRUCCION_COMP. Se revisa si hay y se borran.
			if (!this.tramite.isEsComplementacion() && !(this.informacionMigracionIncompleta
					&& this.tramite.isSegundaDesenglobe() && EPredioCondicionPropiedad.CP_0.getCodigo()
							.equals(this.tramite.getPredio().getCondicionPropiedad()))) {

				this.getConservacionService()
						.borrarPUnidadConstruccionCompRegistros(this.unidadConstruccionSeleccionada.getId());

				// D: si no es un anexo, se guardan los registros
				// correspondientes en la tabla
				// P_UNIDAD_CONSTRUCCION_COMP
				if (!isAnexo) {
					currentDate = new Date();
					newUnidadesConstruccionComp = new ArrayList<PUnidadConstruccionComp>();
					calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion.getChildren();
					for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {

						cctnTempL1 = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
						currentComponenteName = cctnTempL1.getCalificacionConstruccion().getComponente();

						calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
						for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
							cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
							currentElementoName = cctnTempL2.getCalificacionConstruccion().getElemento();
							selectedDetalle = cctnTempL2.getSelectedDetalle();

							// D: armar el objeto que se va a insertar...
							toInsert = new PUnidadConstruccionComp();
							toInsert.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);
							toInsert.setComponente(currentComponenteName);
							toInsert.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
							toInsert.setElementoCalificacion(currentElementoName);
							toInsert.setDetalleCalificacion(selectedDetalle.getDetalleCalificacion());
							toInsert.setPuntos(new Double(selectedDetalle.getPuntos()));
							toInsert.setFechaLog(currentDate);
							toInsert.setUsuarioLog(this.usuario.getLogin());

							newUnidadesConstruccionComp.add(toInsert);
						}
					}

					// D: se insertan o modifican los registros de la tabla
					// P_UNIDAD_CONSTRUCCION_COMP
					LOGGER.debug("insertando o actualizando registros en P_UNIDAD_CONSTRUCCION_COMP ...");
					if (this.tramite.isSegundaDesenglobe() && this.isCondicion8o9()) {
						newUnidadesConstruccionComp = this.getConservacionService()
								.actualizarListaUnidadConstruccionComp(newUnidadesConstruccionComp);
						this.unidadConstruccionSeleccionada.setPUnidadConstruccionComps(newUnidadesConstruccionComp);
					} else if (this.esNuevaUnidadConstruccion) {
						newUnidadesConstruccionComp = this.getConservacionService()
								.insertarPUnidadConstruccionCompRegistros(newUnidadesConstruccionComp);
						this.unidadConstruccionSeleccionada.setPUnidadConstruccionComps(newUnidadesConstruccionComp);
					} else {
						newUnidadesConstruccionComp = this.getConservacionService()
								.actualizarPUnidadConstruccionCompRegistros(newUnidadesConstruccionComp);
						this.unidadConstruccionSeleccionada.setPUnidadConstruccionComps(newUnidadesConstruccionComp);
					}
					LOGGER.debug("... registros insertados o actualizados en P_UNIDAD_CONSTRUCCION_COMP.");
				}
			}

			// felipe.cadena:: 15-03-2015:: #16500 : Para construcciones
			// convencionales siempre debe permanecer el año de construccion en
			// nulo
			if (this.unidadConstruccionSeleccionada.getTipoConstruccion()
					.equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
				this.unidadConstruccionSeleccionada.setAnioConstruccion(null);
			}

			// D: se actualiza la unidad de construcción
			LOGGER.debug("modificando unidad de construcción ...");
			this.getConservacionService().guardarProyeccion(this.usuario, this.unidadConstruccionSeleccionada);
			LOGGER.debug("UC modificada");

			// N: OJO con esto. NO es necesario hacerlo, y si se hace causa
			// errores this.unidadConstruccionSeleccionada = null;
			this.addMensajeInfo("Unidad de construcción guardada exitosamente.");

			// D: si es una nueva unidad de construcción no se tenía el id de
			// esta sino hasta que se creó aquí antes, por lo que hay que
			// definirla en las PFoto.
			// Si la unidad de construcción no es nueva, y se adicionaron PFotos
			// también se les
			// debe asignar ese dato. Se hace aquí para centralizar esa
			// operación
			// ======================================= //
			// Guardar fotos pendientes por guardar
			// D: si se adicionó o modificó alguna foto para la unidad de
			// construcción
			if (this.pFotosList != null && !this.pFotosList.isEmpty()) {

				for (PFoto pfoto : this.pFotosList) {
					if (null == pfoto.getPUnidadConstruccion() || null == pfoto.getPUnidadConstruccion().getId()) {
						pfoto.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);
					}

					// D: al llamar a este método se crea o modifica el
					// Documento asociado, y se
					// crea o modifica la PFoto.
					// D: si el archivo local del Documento de la PFoto está
					// vacío quiere decir que no fue cargado
					// como foto, es decir que no es nueva foto, y por lo tanto
					// no se debe guardar
					if (null != pfoto.getDocumento().getRutaArchivoLocal()
							&& !pfoto.getDocumento().getRutaArchivoLocal().isEmpty()) {
						pfoto = this.getConservacionService().guardarPFoto(pfoto, this.usuario);
					}
				}
			}

			// D: eliminar las PFotos modificadas. Para borrar los registros
			// antiguos y los archivos
			// correspondientes en el gestor documental
			if (this.listaPFotosABorrar != null && !this.listaPFotosABorrar.isEmpty()) {
				this.getConservacionService().borrarPFotos(this.listaPFotosABorrar);
			}

			if (this.esNuevaUnidadConstruccion) {
				this.esNuevaUnidadConstruccion = false;
			}

			if (!this.getTramite().isEsComplementacion()) {

				// / ACTUALIZAR AVALÚO
				if (!this.banderaPredioFiscal) {
					if (this.tramite.isSegundaDesenglobe() && EPredioCondicionPropiedad.CP_0.getCodigo()
							.equals(this.tramite.getPredio().getCondicionPropiedad())) {
						this.recalcularAvaluoConstruccion(true);
					} else {
						recalcularAvaluo(true);
					}
				}

				// CONSULTAR PREDIO ACTUALIZADO
				if (this.predioSeleccionado != null) {
					this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
				}

				setAvaluo();

				if ((this.tramite.isSegundaDesenglobe() || this.tramite.isTerceraMasiva()) && this.isCondicion8o9()) {

					// Llama a la función que refresca las unidades de
					// construcción en la ficha matriz
					if (this.fichaMatrizMB == null) {
						this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
					}
					// manejo de unidades de construcción
					if (this.tramite.isTerceraMasiva()) {
						this.fichaMatrizMB.setupUnidadesDeConstruccionTerceraMasiva();
					} else {
						this.fichaMatrizMB.setupUnidadesDeConstruccion();
					}

					// Si es una mutación de segunda se realiza una
					// actualización del avalúo total de la ficha y sus zonas.
					if (this.fichaMatrizSeleccionada != null) {

						if (this.actualizarAreasFichaMatrizBool) {
							actualizarAvaluosFichaMatriz();
						}

						// Actualizar el valor de las zonas
						if (this.predioSeleccionado.getCondicionPropiedad() != null && this.predioSeleccionado
								.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
							this.fichaMatrizMB.consultarZonas();
						}
					}
				}

				// Se actualiza el valor de las áreas
				this.guardarAreas();
			} else {
				// Recargar avaluo y construcciones
				if (this.predioSeleccionado != null) {
					this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
					setAvaluo();
				}
			}

			if (this.banderaPredioFiscal && !this.tramite.isQuintaOmitido()) {
				this.guardarAvaluoPredioFiscal();
			}
			// Una vez ejecutado el procedimiento que calcula los avalúos
			// para el predio y sus unidades de construcción, se realiza la
			// consulta de la unidad de construcción
			// para actualizar la tabla de unidades de contrucción con el valor
			// del avalúo para dicha unidad.
			PUnidadConstruccion puc = this.getConservacionService()
					.buscarUnidadDeConstruccionPorSuId(this.unidadConstruccionSeleccionada.getId());
			this.unidadConstruccionSeleccionada.setAvaluo(puc.getAvaluo());

			if (this.tramite.isTercera() || this.tramite.isSegunda() || this.tramite.isQuinta()) {
				this.obligarGeografico();
			}
			
			// Se valida si la unidad existe en otras construcciones
			if (this.unidadConstruccionSeleccionada.getTipoConstruccion().trim()
	                 .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
			List<PUnidadConstruccion> unidadesExistentes = this.predioSeleccionado
					.getPUnidadesConstruccionConvencionalNuevas();
			for (PUnidadConstruccion puce : unidadesExistentes) {
				String idPUC = puce.getUnidad();
				String idNUC = this.unidadConstruccionSeleccionada.getUnidad();
				if (idPUC.equalsIgnoreCase(idNUC) && (this.unidadConstruccionSeleccionada.getId()==null || !this.unidadConstruccionSeleccionada.getId().equals(puce.getId()))) {
					this.addMensajeWarn("Ya existe una construcción con la unidad  "
							+ this.unidadConstruccionSeleccionada.getUnidad());
					break;
				}
			}
			 }else {
				 
				 List<PUnidadConstruccion> unidadesExistentes = this.predioSeleccionado
							.getPUnidadesConstruccionNoConvencionalNuevas();
					for (PUnidadConstruccion puce : unidadesExistentes) {
						String idPUC = puce.getUnidad();
						String idNUC = this.unidadConstruccionSeleccionada.getUnidad();
						if (idPUC.equalsIgnoreCase(idNUC) && (this.unidadConstruccionSeleccionada.getId()==null || !this.unidadConstruccionSeleccionada.getId().equals(puce.getId()))) {
							this.addMensajeWarn("Ya existe una construcción con la unidad  "
									+ this.unidadConstruccionSeleccionada.getUnidad());
							break;
						}
					} 
				 
			 }

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			this.addMensajeError("Error interno al guardar la unidad de construcción.");
		}

		// felipe.cadena:: 28/08/2014:: #9241 ::Validaciones iniciales para
		// rectificaciones de area de construccion
		this.banderaRectAreaConstruccion = true;
		if (this.tramite.isRectificacion()) {
			this.actualizarBanderaModificacion();
		}

		if (this.tramite.isEnglobeVirtual()) {
			this.getGestionDetalleAvaluoMB().cargarConstruccionesEnglobeVirtual();
		}

		this.cargarConstruccionesAsociadasPredioOrigen();
		this.listaPFotosABorrar = null;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Método que actualiza el avalúo para una construcción específica.
	 * 
	 * @author david.cifuentes
	 */
	public void recalcularAvaluoConstruccion(boolean adicionarMensaje) {

		try {
			Double valorUnidad = this.getConservacionService().obtenerValorUnidadConstruccionPorProcedimientoDB(
					this.predioSeleccionado.getNumeroPredial().substring(0, 7), this.predioSeleccionado.getSector(),
					this.predioSeleccionado.getDestino(),
					this.unidadConstruccionSeleccionada.getUsoConstruccion().getIdConFormato(), null,
					this.unidadConstruccionSeleccionada.getTotalPuntaje().longValue(), this.predioSeleccionado.getId());

			if (valorUnidad == null) {
				valorUnidad = 0.0;
			}

			this.unidadConstruccionSeleccionada.setValorM2Construccion(valorUnidad);
			this.unidadConstruccionSeleccionada
					.setAvaluo(valorUnidad * this.unidadConstruccionSeleccionada.getAreaConstruida());
			this.getConservacionService().guardarProyeccion(this.usuario, this.unidadConstruccionSeleccionada);

			if (adicionarMensaje) {
				this.addMensajeInfo("Se procesó correctamente el cálculo de los avalúos para este predio.");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			this.addMensajeError("Error al procesar el cálculo de los avaluos para este predio.");
		}
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Método que recalcula el avalúo del ppredio seleccionado.
	 */
	@SuppressWarnings("unchecked")
	public void recalcularAvaluo(boolean adicionarMensaje) {
		Object[] errors = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(this.tramite.getId(),
				this.predioSeleccionado.getId(), new Date(), this.usuario);
		String mensajeError = "";
		if (errors == null) {
			if (adicionarMensaje) {
				this.addMensajeInfo("Se procesó correctamente el cálculo de los avalúos para este predio.");
			}
		} else if (errors.length > 0) {
			for (int i = 0; i < errors.length; i++) {
				if (((ArrayList<Object>) errors[i]).isEmpty()) {
					LOGGER.debug("Se procesó correctamente el cálculo de los avalúos para este predio.");
					if (adicionarMensaje) {
						this.addMensajeInfo("Se procesó correctamente el cálculo de los avalúos para este predio.");
					}
					break;
				} else {

					int numeroErrores = 0;
					String mensajes = "";

					for (Object errorI : ((ArrayList<Object>) errors[i])) {
						if (((Object[]) errorI)[0].equals("0")) {
							mensajes += (((Object[]) errorI)[0] != null) ? ((Object[]) errorI)[0].toString() : "";
							mensajes += (((Object[]) errorI)[1] != null) ? ((Object[]) errorI)[1].toString() : "";
							mensajes += (((Object[]) errorI)[2] != null) ? ((Object[]) errorI)[2].toString() : "";
							mensajes += (((Object[]) errorI)[3] != null) ? ((Object[]) errorI)[3].toString() : "";

						} else {
							numeroErrores++;
							mensajeError += (((Object[]) errorI)[0] != null) ? ((Object[]) errorI)[0].toString() : "";
							mensajeError += (((Object[]) errorI)[1] != null) ? ((Object[]) errorI)[1].toString() : "";
							mensajeError += (((Object[]) errorI)[2] != null) ? ((Object[]) errorI)[2].toString() : "";
							mensajeError += (((Object[]) errorI)[3] != null) ? ((Object[]) errorI)[3].toString() : "";
						}
					}

					if (numeroErrores == 0) {
						if (adicionarMensaje) {
							this.addMensajeInfo("Se procesó correctamente el cálculo de los avalúos para este predio.");
						}
						LOGGER.debug(mensajes);
					} else {
						this.addMensajeError("Error al procesar el cálculo de los avaluos para este predio.");
						this.addMensajeError(mensajeError);
						LOGGER.error(mensajeError);
						return;
					}
				}
			}
		}
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Método que actualiza los avalúos de la ficha matriz al editar, eliminar o
	 * asociar una unidad de construccion.
	 */
	public void actualizarAvaluosFichaMatriz() {

		Double valorTotalAvaluoCatastral = this.getConservacionService()
				.calcularSumaAvaluosTotalesDePrediosFichaMatriz(this.tramite.getId());

		if (this.fichaMatrizSeleccionada != null) {
			this.fichaMatrizSeleccionada.setValorTotalAvaluoCatastral(valorTotalAvaluoCatastral);
		} else {
			this.fichaMatrizSeleccionada = this.getConservacionService()
					.buscarPFichaMatrizPorPredioId(this.getPredioSeleccionado().getId());
			if (this.fichaMatrizSeleccionada != null) {
				this.fichaMatrizSeleccionada.setValorTotalAvaluoCatastral(valorTotalAvaluoCatastral);
			}
		}
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Encargado de actualizar en el predio las areas y en la base de datos, al
	 * editar, eliminar o asociar una construccion
	 *
	 * @modified by leidy.gonzalez 25/07/2014 Se valida que pPrediosDesenglobe no
	 *           sea nulo para realizar el for.
	 */
	private void guardarAreas() {

		// Se adiciona esta validación debido a que el objeto pPrediosDesenglobe no
		// siempre tiene los cambios pues usualmente se trabaja sobre el predio
		// seleccionado.
		if (this.tramite.isTerceraMasiva()) {
			this.cargarPrediosTerceraRectificacionMasiva();
		} else {
			this.cargarPrediosDesenglobe();
		}

		// felipe.cadena redmine #7881
		if (this.pPrediosDesenglobe != null && !this.pPrediosDesenglobe.isEmpty()) {

			for (PPredio pp : this.pPrediosDesenglobe) {
				if (pp != null && pp.getId().equals(this.tramite.getPredio().getId())) {
					Double areaTotal = this.calcularAreaTotalPredios(pp, null);
					pp.setAreaConstruccion(areaTotal);
					if (this.tramite.isEnglobeVirtual()) {
						if (!pp.getAreaConstruccion().equals(areaTotal)) {
							pp = this.getConservacionService().guardarActualizarPPredio(pp);
						}

					} else {
						pp = this.getConservacionService().guardarActualizarPPredio(pp);
					}
				}
			}
		}

		// Se comenta porque el área de terreno se cálcula desde el editor
		// this.predioSeleccionado.setAreaTerreno(new
		// Double(this.getAreaTotalTerreno()));
		this.predioSeleccionado.setAreaConstruccion(new Double(this.getAreaTotalConstruccion(this.predioSeleccionado)));
		if (!tramite.isEnglobeVirtual()) {
			this.getConservacionService().guardarProyeccion(this.usuario, this.predioSeleccionado);
		}
		if (this.tramite.isTerceraMasiva()) {
			this.cargarPrediosTerceraRectificacionMasiva();
		} else {
			this.cargarPrediosDesenglobe();
		}

		// Si la actualización de áreas se está realizando desde la ficha matriz
		// esta actualización debe modificar las áreas de la entidad
		// PFichaMatriz.
		this.guardarAreasBool = true;
		// #15193 :: david.cifuentes :: Para todos los trámites en los que se
		// modifiquen las áreas y esté asociado a un PH o Condominio se va a
		// realizar una actualización de áreas en la ficha matriz :: 22/12/2015
		// if (this.actualizarAreasFichaMatrizBool) {
		if (this.predioSeleccionado.isPHCondominio() && this.fichaMatrizSeleccionada != null) {
			actualizarAreasFichaMatriz(this.guardarAreasBool);
		}
		// }
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Método que actualiza el valor de las áreas de la ficha matriz repecto a las
	 * unidades de construcción editadas.
	 *
	 * @author david.cifuentes
	 */
	public void actualizarAreasFichaMatriz(boolean guardarAreasBool) {

		if (this.fichaMatrizMB == null) {
			this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
		}

		if (this.guardarAreasBool) {

			this.fichaMatrizMB.guardarAreasPPredio();
			// Consultar la ficha matriz actualizada
			PFichaMatriz fichaMatriz = new PFichaMatriz();
			if (this.fichaMatrizSeleccionada != null && this.fichaMatrizSeleccionada.getPPredio() != null) {

				fichaMatriz = this.getConservacionService()
						.buscarPFichaMatrizPorPredioId(this.fichaMatrizSeleccionada.getPPredio().getId());
			} else {
				this.fichaMatrizSeleccionada = this.getConservacionService()
						.buscarPFichaMatrizPorPredioId(this.getPredioSeleccionado().getId());

				if (this.fichaMatrizSeleccionada != null && this.fichaMatrizSeleccionada.getPPredio() != null) {

					fichaMatriz = this.getConservacionService()
							.buscarPFichaMatrizPorPredioId(this.fichaMatrizSeleccionada.getPPredio().getId());
				}
			}
			if (fichaMatriz != null && fichaMatriz.getId() != null) {
				this.fichaMatrizSeleccionada = fichaMatriz;
				fichaMatrizMB.setFichaMatriz(fichaMatriz);
				this.verificarTorresEnFirme();
			}
		} else {
			if (this.fichaMatrizSeleccionada == null) {
				this.consultarFichaMatriz();
			}
			this.fichaMatrizMB.convertirAreasAHaM2();
		}

		this.actualizarAreasFichaMatrizBool = false;

		// Actualización de totales de áreas en la FM
		this.fichaMatrizMB.actualizarTotalesAreasEnFichaMatriz();
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Método que cancela una PUnidad de construccion de la ficha matriz y actualiza
	 * las areas y los avalúos correspondientes a la ficha matriz.
	 */
	public void cancelarPUnidadConstruccionFichaMatriz() {

		try {
			if (this.fichaMatrizMB == null) {
				this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
			}

			this.fichaMatrizMB.cancelarPUnidadConstruccion();

			// Consultar la ficha matriz actualizada
			this.fichaMatrizSeleccionada = this.getConservacionService()
					.buscarPFichaMatrizPorPredioId(this.predioSeleccionado.getId());
			this.verificarTorresEnFirme();

			this.fichaMatrizMB.setFichaMatriz(this.fichaMatrizSeleccionada);

			// Actualizar avalúos del predio.
			recalcularAvaluo(true);

			// Actualización del avalúo total de la ficha.
			actualizarAvaluosFichaMatriz();

			this.fichaMatrizMB.setFichaMatriz(this.fichaMatrizSeleccionada);

			// Consultar predio con sus avalúos actualizados
			if (this.predioSeleccionado != null) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
				setAvaluo();
			}

			// Se actualiza el valor de las áreas
			guardarAreas();

			// manejo de unidades de construcción
			if (this.tramite.isRectificacionMatriz()) {
				this.fichaMatrizMB.setupUnidadesDeConstruccionRectMatriz();
			} else if (this.tramite.isTerceraMasiva()) {
				this.fichaMatrizMB.setupUnidadesDeConstruccionTerceraMasiva();
			} else {
				this.fichaMatrizMB.setupUnidadesDeConstruccion();
			}

		} catch (Exception e) {
			LOGGER.error("error en cancelarPUnidadConstruccionFichaMatriz: " + e.getMessage());
		}

	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * action del botón "Editar unidad de construcción". Este método también se
	 * llama desde el método crearNuevaUnidadConstruccion
	 *
	 * @author pedro.garcia
	 */
	public void initGestionDetalleUnidadConstruccion() {

		// ----- init CU-NP-CO-121
		TreeNode calificacionConstruccionNodesLevel1 = null;

		this.puntajeAnterior = 0;

		this.usosUnidadConstruccionItemList = new ArrayList<SelectItem>();
		if (this.usosConstruccionList == null || this.usosConstruccionList.isEmpty()) {
			this.usosConstruccionList = this.getConservacionService()
					.obtenerVUsosConstruccionZona(this.predioSeleccionado.getNumeroPredial().substring(0, 7));
		}

		if (this.tramite.isCancelacionMasiva()) {
			this.setUsosUnidadConstruccionItemList(this.getGestionDetalleAvaluoMB()
					.obtenerUsosCancelacionMasiva(this.unidadConstruccionSeleccionada.getUsoConstruccion()));
			this.actualizarArbolDeComponentes();
		} else {
			this.setUsosUnidadConstruccionItemList(this.usosConstruccionList);
		}

		if (this.tramite.isEnglobeVirtual() && this.unidadConstruccionSeleccionada != null) {
			if (!this.getGestionDetalleAvaluoMB()
					.obtenerReplicaConstruccionSeleccionada(this.unidadConstruccionSeleccionada)) {
				return;
			}
		}

		// D: si la unidad de construcción tiene un uso de construcción
		// asignado, se le asignan esos
		// valores a las variables de clase correspondientes
		if (this.unidadConstruccionSeleccionada.getUsoConstruccion() != null) {
			this.selectedUsoUnidadConstruccionId = this.unidadConstruccionSeleccionada.getUsoConstruccion().getId();
			this.selectedUsoUnidadConstruccion = this.unidadConstruccionSeleccionada.getUsoConstruccion();
		} // ... si no, se le asigna 0l
		else {
			if (this.usosUnidadConstruccionItemList.size() > 0
					&& !this.usosUnidadConstruccionItemList.get(0).getLabel().equals(this.DEFAULT_COMBOS)) {
				this.usosUnidadConstruccionItemList.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
			}
			this.selectedUsoUnidadConstruccionId = 0l;

		}

		// Set del id de la calificación anexo
		if (this.unidadConstruccionSeleccionada.getCalificacionAnexoId() != null) {
			this.calificacionAnexoIdSelected = this.unidadConstruccionSeleccionada.getCalificacionAnexoId();
		}

		// D: armar el TreeNode para mostrar los componentes de construcción
		// D: se arman los tres posibles desde el principio porque si se intenta
		// hacer que en el momento en que cambie el combo se arme y se refresque
		// el nuevo árbol, no
		// funciona (se hace el refresco por cada línea de la tabla que muestra
		// el árbol, y eso daña los
		// valores de la columna "puntos")
		this.treeComponentesConstruccion2 = armarArbolCalificacionConstruccion(
				EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo());
		this.treeComponentesConstruccion3 = armarArbolCalificacionConstruccion(
				EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo());
		this.treeComponentesConstruccion1 = armarArbolCalificacionConstruccion(
				EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo());

		// N: OJO con esto. NO es necesario hacerlo, y si se hace causa errores
		// this.selectedCalificacionAnexo = null;
		// D: como este método también se llama desde el método
		// crearNuevaUnidadConstruccion, se verifica que no se trate de una
		// nueva unidad de construcción antes de hacer la consulta de las PFoto
		// asociadas a esta unidad de construcción
		if (!this.esNuevaUnidadConstruccion) {

			CalificacionConstruccionTreeNode cctnTemp;
			String tempComponenteNameNode, tempContainedComponenteNamePFoto;

			// ========================================== //
			// Se fuerza la consulta de la unidad de construcción y sus
			// componentes.
			if (!this.tramite.isRectificacionMatriz()) {
				this.unidadConstruccionSeleccionada = this.getConservacionService()
						.buscarUnidadDeConstruccionPorSuId(this.unidadConstruccionSeleccionada.getId());
			}
			// ========================================== //

			// D: se cargan las PFotos de la PUnidadConstruccion
			this.pFotosList = this.getConservacionService()
					.buscarPFotosUnidadConstruccion(this.unidadConstruccionSeleccionada.getId(), true);

			if (!this.unidadConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo())) {

				this.isAnexoCurrentOrNewUnidadConstruccion = false;

				if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
					calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion2;
				} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
					calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion3;
				} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
					calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion1;
				}

				if (this.pFotosList != null && !this.pFotosList.isEmpty()) {

					// D: ... luego de tener estos datos hay que recorrer el
					// árbol y
					// asignarle a cada nodo los datos de foto que se obtuvieron
					for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1.getChildren()) {
						cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
						tempComponenteNameNode = cctnTemp.getCalificacionConstruccion().getComponente();
						for (PFoto currentPFoto_i : this.pFotosList) {
							tempContainedComponenteNamePFoto = currentPFoto_i.getDescripcion();
							if (tempContainedComponenteNamePFoto.contains(tempComponenteNameNode)) {
								cctnTemp.setUrlArchivoWebTemporal(currentPFoto_i.getDocumento().getRutaArchivoWeb());
								cctnTemp.setMostrarIconoSubirImagen(false);
							}
						}
					}
				}
				// Validación para cuando se han asociado las construcciones y
				// dichas construcciones tienen sus componentes en lazy.
				if (this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps() == null
						|| this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps().isEmpty()) {
					if (!this.tramite.isRectificacionMatriz()) {
						this.unidadConstruccionSeleccionada = this.getConservacionService()
								.buscarUnidadDeConstruccionPorSuId(this.unidadConstruccionSeleccionada.getId());
					}
				}

				// Hacer un recorrido de los componentes de la unidad de
				// construcción seleccionada y setearselos a los combo box del
				// arbol.
				cargarComponentesDeLaUnidadDeConstruccion(calificacionConstruccionNodesLevel1,
						this.unidadConstruccionSeleccionada.getTipoCalificacion());

				if (this.tramite.isTercera() || this.tramite.isSegundaDesenglobe()
						|| this.tramite.isComplementacionDetalleCalificacion() || this.tramite.isCancelacionMasiva()) {
					if (!this.selectedUsoUnidadConstruccion.getDestinoEconomico()
							.equals(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo())) {

						if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
								.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
							this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
						} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
								.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
							this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
						} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
								.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
							this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
						}

						if (tramite.isSegundaDesenglobe()
								&& this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps().size() <= 0) {
							List<TreeNode> calificacionConstruccionNodesLevelG1, calificacionConstruccionNodesLevelG2;
							List<PUnidadConstruccionComp> newUnidadesConstruccionComp;
							PUnidadConstruccionComp toInsert;
							CalificacionConstruccionTreeNode cctnTempL1, cctnTempL2;
							CalificacionConstruccion selectedDetalle;
							String currentComponenteName, currentElementoName;
							Date currentDate;

							currentDate = new Date();
							newUnidadesConstruccionComp = new ArrayList<PUnidadConstruccionComp>();
							calificacionConstruccionNodesLevelG1 = this.treeComponentesConstruccion.getChildren();
							for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevelG1) {

								cctnTempL1 = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
								currentComponenteName = cctnTempL1.getCalificacionConstruccion().getComponente();

								calificacionConstruccionNodesLevelG2 = currentComponenteNode.getChildren();
								for (TreeNode currentElementoNode : calificacionConstruccionNodesLevelG2) {
									cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
									currentElementoName = cctnTempL2.getCalificacionConstruccion().getElemento();
									selectedDetalle = cctnTempL2.getSelectedDetalle();

									// D: armar el objeto que se va a insertar...
									toInsert = new PUnidadConstruccionComp();
									toInsert.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);
									toInsert.setComponente(currentComponenteName);
									toInsert.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
									toInsert.setElementoCalificacion(currentElementoName);
									toInsert.setDetalleCalificacion(selectedDetalle.getDetalleCalificacion());
									toInsert.setPuntos(new Double(selectedDetalle.getPuntos()));
									toInsert.setFechaLog(currentDate);
									toInsert.setUsuarioLog(this.usuario.getLogin());

									newUnidadesConstruccionComp.add(toInsert);
								}
							}

							// se insertan o modifican los registros de la tabla
							// P_UNIDAD_CONSTRUCCION_COMP
							LOGGER.debug("insertando o actualizando registros en P_UNIDAD_CONSTRUCCION_COMP ...");
							if (this.tramite.isSegundaDesenglobe() && this.isCondicion8o9()) {
								newUnidadesConstruccionComp = this.getConservacionService()
										.actualizarListaUnidadConstruccionComp(newUnidadesConstruccionComp);
								this.unidadConstruccionSeleccionada
										.setPUnidadConstruccionComps(newUnidadesConstruccionComp);
							} else if (this.esNuevaUnidadConstruccion) {
								newUnidadesConstruccionComp = this.getConservacionService()
										.insertarPUnidadConstruccionCompRegistros(newUnidadesConstruccionComp);
								this.unidadConstruccionSeleccionada
										.setPUnidadConstruccionComps(newUnidadesConstruccionComp);
							} else {
								newUnidadesConstruccionComp = this.getConservacionService()
										.actualizarPUnidadConstruccionCompRegistros(newUnidadesConstruccionComp);
								this.unidadConstruccionSeleccionada
										.setPUnidadConstruccionComps(newUnidadesConstruccionComp);
							}
							LOGGER.debug("... registros insertados o actualizados en P_UNIDAD_CONSTRUCCION_COMP.");
							initGestionDetalleUnidadConstruccion();
						}
						if (this.tramite.isCancelacionMasiva()) {
							this.puntajeAnterior = this.unidadConstruccionSeleccionada.getTotalPuntaje().intValue();
						} else {
							this.puntajeAnterior = this.getTotalPuntosConstruccion();
						}
						if (this.puntajeAnterior <= 1) {
							this.puntajeAnterior = this.unidadConstruccionSeleccionada.getTotalPuntaje().intValue();
						}
					}
				}
			} // D: para la foto de la unidad de construcción que es ANEXO.
				// La lista de PFotos, si tiene elementos, debe tener 1 sola
			else {
				this.isAnexoCurrentOrNewUnidadConstruccion = true;
				if (this.pFotosList != null && !this.pFotosList.isEmpty()) {
					this.rutaWebTemporalFotoAnexo = this.pFotosList.get(0).getDocumento().getRutaArchivoWeb();
				} else {
					this.rutaWebTemporalFotoAnexo = this.rutaWebImagenBase;
				}
			}
		} else {
			this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion1;
			actualizarArbolDeComponentes();
		}
		// Cargar piso de la unidad.
		this.cargarPisoUnidadConstruccion();

		// Definir si pertenece al predio proyectado
		if (this.unidadConstruccionSeleccionada.getProvienePredio() != null || this.tramite.isTerceraMasiva()
				|| this.tramite.isRectificacionMatriz()
				|| ESiNo.SI.getCodigo().equals(this.unidadConstruccionSeleccionada.getOriginalTramite())) {
			this.unidadConstruccionSeleccionada.setPertenecePredioProyectado(true);
		}
		if (this.tramite.isCancelacionMasiva()) {
			this.unidadConstruccionSeleccionada.setPertenecePredioProyectado(false);
		}

		if (this.tramite.isQuintaOmitido() && this.banderaPredioFiscal) {
			this.datosConstruccionSoloLectura = true;
		} else if (this.tramite.isQuinta() && this.tramite.isTercera()) {
			this.datosConstruccionSoloLectura = false;
		}

		if (this.tramite.isSegundaDesenglobe()) {
			if (this.unidadConstruccionSeleccionada.getAnioConstruccion() == 0) {
				this.unidadConstruccionSeleccionada.setAnioConstruccion(null);
			}
		}
		/*
		 * @modified by leidy.gonzalez::#15131::12/05/2016 Se valida si el predio tiene
		 * condicion de propiedad 0 para habilitar el campo uso de unidad. if
		 * (this.predioSeleccionado != null &&
		 * this.predioSeleccionado.getCondicionPropiedad().equals(
		 * EPredioCondicionPropiedad.CP_0.getCodigo())) {
		 * this.banderaCondicionProdpiedadCero = true; }else{
		 * this.banderaCondicionProdpiedadCero = true; }
		 */

		/*
		 * @modified by wilmanjose.vega::#15131::31/05/2016 Se agrega en el enabled de
		 * la unidad si es segunda de desenglobe y se desactiva el uso
		 */
		if (this.tramite != null && this.tramite.isSegundaDesenglobe()) {
			this.banderaCondicionProdpiedadCero = true;
		}
		puntajeTotal = getTotalPuntosConstruccion();
	}

	// ------------------------------------------------------------------- //
	/**
	 * Método que carga los componentes para el arbol de la
	 * {@link PUnidadConstruccion} seleccionada.
	 *
	 * @author david.cifuentes
	 */
	@SuppressWarnings("unused")
	public void cargarComponentesDeLaUnidadDeConstruccion(TreeNode calificacionConstruccionNodesLevel1,
			String tipoCalificacion) {

		this.treeComponentesConstruccion = null;

		/**
		 * 1. Recorrer la lista de componentes de la unidad de construcción. 2.Para el
		 * componente n de la lista hallar el componente, detalleCalificación, elemento
		 * y recuperar la calificacionConstruccion de éste. 3. Con esta
		 * calificacionConstruccion recorrer el TreeNode que se recibe como parametro, y
		 * al encontrar el nodo, setear los datos recuperados de la unidad de
		 * construcción. 4. Reasignar el TreeNode actualizado con el detalle de la
		 * unidad del modelo de construcción.
		 */
		if (calificacionConstruccionNodesLevel1 != null && calificacionConstruccionNodesLevel1.getChildren() != null
				&& calificacionConstruccionNodesLevel1.getChildren().size() > 0) {

			String componente, detalleCalificacion, elementoCalificacion;
			CalificacionConstruccion auxCalificacionConstruccion, calificacionConstruccionDelComponente;
			if (this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps() != null
					&& !this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps().isEmpty()) {

				for (PUnidadConstruccionComp pucc : this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps()) {
					componente = pucc.getComponente();
					elementoCalificacion = pucc.getElementoCalificacion();
					detalleCalificacion = pucc.getDetalleCalificacion();
					calificacionConstruccionDelComponente = buscarCalificacionConstruccion(componente,
							elementoCalificacion, detalleCalificacion);

					if (calificacionConstruccionDelComponente != null) {
						for (TreeNode tn : calificacionConstruccionNodesLevel1.getChildren()) {

							// Leer el nodo y comparar con el valor de
							// componente.
							tn.setExpanded(true);

							auxCalificacionConstruccion = ((CalificacionConstruccionTreeNode) tn.getData())
									.getCalificacionConstruccion();
							if (auxCalificacionConstruccion != null
									&& auxCalificacionConstruccion.getComponente() != null
									&& auxCalificacionConstruccion.getComponente().equals(componente)) {

								// Cuando encuentre el componente en el arbol,
								// se recorren las calificaciones del componente
								// y se hace el set del detalle a cada una.
								int aux = 0;
								int puntos;
								for (TreeNode tnElemento : tn.getChildren()) {
									tnElemento.setExpanded(true);
									if (((CalificacionConstruccionTreeNode) tnElemento.getData())
											.getCalificacionConstruccion().getElemento().equals(elementoCalificacion)) {

										aux++;

										// Se actualizan los puntos de
										// calificacion para el elemento
										// CalificacionConstruccionDelComponente.
										puntos = getPuntosPorCalificacionConstruccion(
												calificacionConstruccionDelComponente, tipoCalificacion);
										calificacionConstruccionDelComponente.setPuntos(puntos);

										// Se setea el objeto calificacion
										// construccion de la unidad al
										// selected.
										((CalificacionConstruccionTreeNode) tnElemento.getData())
												.setSelectedDetalle(calificacionConstruccionDelComponente);
										LOGGER.debug("SETEANDO " + calificacionConstruccionDelComponente);
										// break;
									}
								}
								// break;
							}
						}
					}
				}
				// Reasignación del TreeNode.
				this.treeComponentesConstruccion = calificacionConstruccionNodesLevel1;
				mostrarPuntos(this.treeComponentesConstruccion);
			} else {
				// #6341 no esta permitiendo modificar el detalle en aquellas
				// construcciones que no tiene detalle de la construcción
				// felipe.cadena:: #9246 :: en caso de rectificaciones se carga
				// solo para la rectificacion de detalle de calificacion
				if ((this.getTramite() != null && this.getTramite().getPredio() != null
						&& this.getTramite().getPredio().isMejora())
						|| (this.getTramite() != null && (this.getTramite().getTipoTramite()
								.equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo())
								|| this.getTramite().isRectificacionDetalleCalificacion()))) {
					this.actualizarArbolDeComponentes();
				}
			}
		}
	}

	// -----------------------------------------------------------------------------
	// //
	/**
	 * Método que devuelve un valor {@link Integer} con el valor del año actual.
	 *
	 * @return
	 */
	private Integer getAnoActual() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		return new Integer(sdf.format(new Date()));
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * acción del botón "Adicionar unidad" de gestionDetalleAvaluo
	 *
	 * Crea una unidad de construcción por defecto para pintar algo inicial en la
	 * forma. Por defecto es una unidad de construcción convencional residencial.
	 */
	public void crearNuevaUnidadConstruccion() {

		String unidadConstruccionUnidad;

		// D: se crea una nueva unidad de construcción y se le asignan valores
		this.unidadConstruccionSeleccionada = new PUnidadConstruccion();

		if (this.tipoDominioComun) {
			this.unidadConstruccionSeleccionada.setTipoDominio(EUnidadTipoDominio.COMUN.getCodigo());
			this.tipoDominioComun = false;
		} else {
			this.unidadConstruccionSeleccionada.setTipoDominio(EUnidadTipoDominio.PRIVADO.getCodigo());
		}

		this.setEsNuevaUnidadConstruccion(true);
		this.isAnexoCurrentOrNewUnidadConstruccion = false;

		unidadConstruccionUnidad = calcularUnidadConstruccionUnidad();
		this.unidadConstruccionSeleccionada.setUnidad(unidadConstruccionUnidad);

		// D: datos temporales (los que no pueden ser nulos y no tienen default)
		this.unidadConstruccionSeleccionada
				.setTipoCalificacion(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo());
		this.unidadConstruccionSeleccionada.setTipificacion(EUnidadConstruccionTipificacion.ANY.getCodigo());
		this.unidadConstruccionSeleccionada.setTipoConstruccion(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());

		this.unidadConstruccionSeleccionada.setAnioConstruccion(this.getAnoActual());
		this.unidadConstruccionSeleccionada.setTotalPisosConstruccion(0);

		this.unidadConstruccionSeleccionada.setTotalPisosUnidad(0);
		this.unidadConstruccionSeleccionada.setAreaConstruida(0D);

		// D: ... y los que tienen default también porque al generarse los
		// entities no queda con esa definición
		this.unidadConstruccionSeleccionada.setAvaluo(Double.valueOf(0));
		this.unidadConstruccionSeleccionada.setValorM2Construccion(Double.valueOf(1));

		this.unidadConstruccionSeleccionada.setPPredio(this.predioSeleccionado);
		this.unidadConstruccionSeleccionada.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
		this.unidadConstruccionSeleccionada.setUsuarioLog(this.usuario.getLogin());

		this.calificacionAnexoIdSelected = null;

		if (this.usosUnidadConstruccionItemList == null || this.usosUnidadConstruccionItemList.isEmpty()) {

			this.usosUnidadConstruccionItemList = new ArrayList<SelectItem>();
			if (this.usosConstruccionList == null || this.usosConstruccionList.isEmpty()) {
				this.usosConstruccionList = this.getConservacionService()
						.obtenerVUsosConstruccionZona(this.predioSeleccionado.getNumeroPredial().substring(0, 7));
			}
			this.setUsosUnidadConstruccionItemList(this.usosConstruccionList);
		}

		this.initGestionDetalleUnidadConstruccion();

		this.areaNoConvencional = 0D;

		this.pFotosList = new ArrayList<PFoto>();

		this.rutaWebTemporalFotoAnexo = this.rutaWebImagenBase;

	}

	// -------------------------------------------------------------------------------------------------
	/**
	 * calcula el identificador que se le da a la unidad de construcción que se va a
	 * crear
	 *
	 * @author pedro.garcia
	 */
	public String calcularUnidadConstruccionUnidad() {

		String currentUnidad, answer, lastUnidad;

		lastUnidad = "";

		List<PUnidadConstruccion> unidadesConstruccionTemp = this.getConservacionService()
				.buscarUnidadesDeConstruccionPorPPredioId(this.predioSeleccionado.getId(), false);

		if (unidadesConstruccionTemp != null && !unidadesConstruccionTemp.isEmpty()) {
			for (IModeloUnidadConstruccion ucTemp : unidadesConstruccionTemp) {
				currentUnidad = ucTemp.getUnidad();
				if (currentUnidad.compareToIgnoreCase(lastUnidad) > 0) {
					lastUnidad = currentUnidad;
				}
			}
		}

		answer = UtilidadesWeb.calcularSiguienteUnidadUnidadConstruccion(lastUnidad);

		return answer;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * action del command link de foto que aparece en cada componente de
	 * construcción de una construcción convencional, y en la foto de una
	 * construcción anexo
	 *
	 * @author pedro.garcia
	 */
	public void initCargarFoto() {

		String rutaTemp;
		this.currentPFoto = new PFoto();
		this.currentPFoto.setFecha(new Date());

		// D: se hace aquí siempre. Si resulta que es modificación de la foto,
		// más adelante se valida aquello y se cambia el valor
		this.currentPFoto.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

		// D: es por el campo 'descripcion' que se distinguen las fotos de
		// componentes una unidad de construcción.
		if (!this.isAnexoCurrentOrNewUnidadConstruccion) {
			this.currentPFoto.setDescripcion(
					this.currentCalificacionConstruccionNode.getCalificacionConstruccion().getComponente());
		} else {
			this.currentPFoto.setDescripcion(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo());
			// " - " + this.selectedUsoUnidadConstruccion.getNombre());
		}

		rutaTemp = "";
		if (this.currentCalificacionConstruccionNode != null
				&& null != this.currentCalificacionConstruccionNode.getUrlArchivoWebTemporal()) {
			rutaTemp = this.currentCalificacionConstruccionNode.getUrlArchivoWebTemporal();
		}

		// Revisar si la imagen que se tiene para esta calificación construcción
		// es diferente de la imagen base, de ser así mostrarla.
		this.existeImagen = false;
		this.existeImagen = ((!rutaTemp.isEmpty() && rutaTemp.compareToIgnoreCase(this.rutaWebImagenBase) != 0
				&& !this.isAnexoCurrentOrNewUnidadConstruccion)
				|| (null != this.rutaWebTemporalFotoAnexo && !this.rutaWebTemporalFotoAnexo.isEmpty()
						&& this.rutaWebTemporalFotoAnexo.compareToIgnoreCase(this.rutaWebImagenBase) != 0
						&& this.isAnexoCurrentOrNewUnidadConstruccion)) ? true : false;

		if (this.isAnexoCurrentOrNewUnidadConstruccion) {
			this.rutaWebTemporalFoto = this.rutaWebTemporalFotoAnexo;
		} else {
			this.rutaWebTemporalFoto = rutaTemp;
		}

		this.banderaBotonAceptarCargarFoto = false;
		this.errorConfirmarDatosCargarFotografia = false;
		this.setMensajeFotos("La imagen no tiene la información requerida");

	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Listener del fileupload que permite cargar imágenes en la gestión de unidades
	 * de construcción. (metodo validador de carga de imagenes)
	 *
	 * @author juan.agudelo
	 */
	public void imagenUpload(FileUploadEvent eventoCarga) {

		File f1;
		IDocumentosService servicioDocumental;

		DocumentoVO documentoVo;

		// D: se carga el archivo en la carpeta temporal del sistema
		f1 = this.copiarArchivoUploaded(eventoCarga, false);
		this.rutaTemporalFoto = UtilidadesWeb.obtenerRutaTemporalArchivos() + separadorCarpeta + f1.getName();

		// D: se carga el archivo en la ruta web de previsualización
		servicioDocumental = DocumentalServiceFactory.getService();
		documentoVo = servicioDocumental.cargarDocumentoWorkspacePreview(this.rutaTemporalFoto);
		if (documentoVo != null) {
			this.rutaWebTemporalFoto = documentoVo.getUrlPublico();
		}

		this.existeImagen = true;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * metodo action del botón "guardar" de la ventana de cargue de fotos
	 *
	 * @author juan.agudelo
	 */
	public void guardarFoto() {

		// this.rutaWebImagenBase = rutaMostrar;
		// this.existeImagen = false;
		this.existeImagen = true;
		this.errorConfirmarDatosCargarFotografia = true;
		this.mensajeFotos = "La imagen fue cargada con exito!";

	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * metodo para cancelar la subida de una foto
	 *
	 * @author juan.agudelo
	 */
	public void cancelarFoto() {
		// this.currentCalificacionConstruccionNode.setUrlArchivoWebTemporal(this.rutaWebImagenBase);
		// this.existeImagen = false;

	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Action del botón "aceptar" en la ventana de confirmación de upload de foto.
	 * Método para terminar la carga de una foto.
	 *
	 * @author juan.agudelo
	 */
	/*
	 * @modified by fredy.wilches
	 *
	 * @modified pedro.garcia Cambio de nombre del método porque no decía lo que
	 * hace. Las fotos no se guardan en este punto, sino cuando se guarda la unidad
	 * de construcción.
	 */
	public void terminarCargaFoto() {

		String tipoFoto;
		Documento documento;

		// D: setear valores a la nueva PFoto
		this.currentPFoto.setFechaLog(new Date(System.currentTimeMillis()));
		this.currentPFoto.setUsuarioLog(this.usuario.getLogin());
		this.currentPFoto.setPPredio(this.predioSeleccionado);

		tipoFoto = this.getTipoFotoPorComponenteConstruccion();
		// D: es por el tipo de foto que se diferencian las fotos de cada
		// componente-elemento
		this.currentPFoto.setTipo(tipoFoto);
		this.currentPFoto.setTramiteId(this.tramite.getId());

		if (!this.isAnexoCurrentOrNewUnidadConstruccion) {
			// D: le asigna al nodo actual la ruta del archivo local para que se
			// pueda ver la miniatura de la foto en el árbol
			this.currentCalificacionConstruccionNode.setRutaArchivoLocal(this.rutaTemporalFoto);
			this.currentCalificacionConstruccionNode.setUrlArchivoWebTemporal(this.rutaWebTemporalFoto);

		} else {
			this.rutaWebTemporalFotoAnexo = this.rutaWebTemporalFoto;
			this.rutaLocalFotoAnexo = this.rutaTemporalFoto;
		}

		// D: se crea el Documento que irá asociado a la PFoto
		documento = nuevoDocumento();
		documento.setArchivo(UtilidadesWeb.obtenerNombreSinTemporalArchivo(this.rutaTemporalFoto));
		documento.setRutaArchivoLocal(this.rutaTemporalFoto);

		this.currentPFoto.setDocumento(documento);
		this.currentPFoto.setPUnidadConstruccion(this.unidadConstruccionSeleccionada);

		adicionarPFotoALista();

		this.existeImagen = false;
		this.errorConfirmarDatosCargarFotografia = false;
	}

	// -----------------------------------------------------------------------------
	/**
	 * Crea un nuevo documento con la ruta de la imagen en alfresco
	 */
	private Documento nuevoDocumento() {

		Documento doc = new Documento();

		doc.setEstado(Constantes.DOMINIO_DOCUMENTO_ESTADO_RECIBIDO);
		doc.setBloqueado(ESiNo.NO.getCodigo());
		doc.setUsuarioCreador(this.usuario.getLogin());
		doc.setPredioId(this.predioSeleccionado.getId());
		doc.setTramiteId(this.tramite.getId());
		doc.setUsuarioCreador(this.usuario.getLogin());
		doc.setFechaRadicacion(new Date(System.currentTimeMillis()));
		doc.setUsuarioLog(this.usuario.getLogin());
		doc.setFechaLog(new Date(System.currentTimeMillis()));
		TipoDocumento td = new TipoDocumento();
		td.setId(Constantes.TIPO_DOCUMENTO_FOTOGRAFIA);
		doc.setTipoDocumento(td);
		return doc;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * adiciona una PFoto -la actual- a la lista que se va a guardar. Revisa si, por
	 * nombre de componente, ya existe una foto para éste, en cuyo caso la elimina.
	 *
	 * Al guardar, primero se borran todos los registros de PFoto y luego se
	 * insertan los nuevos
	 *
	 * @author pedro.garcia
	 */
	private void adicionarPFotoALista() {

		String currentPFotoComponenteName, tempPFotoComponenteName;
		String[] descripcionPartida;

		descripcionPartida = this.currentPFoto.getDescripcion().split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);
		currentPFotoComponenteName = descripcionPartida[0];

		if (this.pFotosList == null) {
			this.pFotosList = new ArrayList<PFoto>();
		}

		for (PFoto tempPFoto : this.pFotosList) {
			descripcionPartida = tempPFoto.getDescripcion().split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);
			tempPFotoComponenteName = descripcionPartida[0];
			if (tempPFotoComponenteName.equals(currentPFotoComponenteName)) {

				// D: si la nueva PFoto reemplaza a una anterior, se debe, al
				// terminar la operación de
				// creación o modificación de la unidad de construcción, borrar
				// la que existía (en bd y en alfresco)
				if (this.listaPFotosABorrar == null) {
					this.listaPFotosABorrar = new ArrayList<PFoto>();
				}
				this.listaPFotosABorrar.add(tempPFoto);
				this.pFotosList.remove(tempPFoto);
				if (!this.esNuevaUnidadConstruccion) {
					this.currentPFoto.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				}
				break;
			}
		}
		this.pFotosList.add(this.currentPFoto);

	}

	// -----------------------------------------------------------------------------
	// //
	/**
	 * Método que consulta las plantillas de texto resolucion según el tipo del
	 * trámite.
	 *
	 * @author david.cifuentes
	 *
	 * @modified by juanfelipe.garcia :: 05-09-2013 :: se ajusto la logica para
	 *           manejar los textos de autoestimación
	 *
	 * @modified by javier.aponte 21/02/2014
	 *
	 */

	// -----------------------------------------------------------------------------
	// //
	/**
	 * Método que consulta las plantillas de texto resolucion según el tipo del
	 * trámite.
	 *
	 * @author david.cifuentes
	 *
	 * @modified by juanfelipe.garcia :: 05-09-2013 :: se ajusto la logica para
	 *           manejar los textos de autoestimación
	 *
	 * @modified by javier.aponte 21/02/2014
	 *
	 */
	public void consultarModeloResolucionSegunTipoDeTramite() {

		LOGGER.debug("Entra a consultarModeloResolucionSegunTipoDeTramite");

		this.titulosResolucion = new ArrayList<SelectItem>();

		this.modeloSinPlantilla = this.getTramiteService().findModelosResolucionByTipoTramite("0", "0", null);
		if (this.modeloSinPlantilla != null) {
			this.titulosResolucion.add(new SelectItem(this.modeloSinPlantilla.get(0).getId(),
					this.modeloSinPlantilla.get(0).getTextoTitulo()));
		}

		// Busqueda de los Modelos de Resolucion para
		// el trámite seleccionado.
		if (this.tramite.isRectificacionCancelacionDobleInscripcion()) {
			this.modeloResolucionList = this.getTramiteService().findModelosResolucionByTipoTramite(
					this.tramite.getTipoTramite(), this.tramite.getClaseMutacion(),
					Constantes.TRAMITE_SUBTIPO_CANCELACION);
		} else {
			this.modeloResolucionList = this.getTramiteService().findModelosResolucionByTipoTramite(
					this.tramite.getTipoTramite(), this.tramite.getClaseMutacion(), null);

			List<ModeloResolucion> modelosResolucionTemp = new ArrayList<ModeloResolucion>();
			modelosResolucionTemp.addAll(this.modeloResolucionList);

			if (this.tramite.isTipoTramiteViaGubernativaModificado()
					|| this.tramite.isTipoTramiteViaGubModSubsidioApelacion()) {

				String tipoTramite = null;
				if (this.tramite.getObservacionesRadicacion() != null
						&& !this.tramite.getObservacionesRadicacion().isEmpty()) {
					if (this.tramite.getObservacionesRadicacion()
							.equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.getNombre())) {
						tipoTramite = ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo();
					} else if (this.tramite.getObservacionesRadicacion()
							.equals(ETramiteTipoTramite.RECURSO_DE_APELACION.getNombre())) {
						tipoTramite = ETramiteTipoTramite.RECURSO_DE_APELACION.getCodigo();
					} else if (this.tramite.getObservacionesRadicacion()
							.equals(ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getNombre())) {
						tipoTramite = ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getCodigo();
					}

					this.modeloResolucionList = this.getTramiteService().findModelosResolucionByTipoTramite(tipoTramite,
							null, null);
				}
			} else {
				this.modeloResolucionList = this.getTramiteService().findModelosResolucionByTipoTramite(
						this.tramite.getTipoTramite(), this.tramite.getClaseMutacion(), null);

				if (this.tramite.isRectificacion()) {
					modelosResolucionTemp = new ArrayList<ModeloResolucion>();
					modelosResolucionTemp.addAll(this.modeloResolucionList);

					for (ModeloResolucion mrTemp : modelosResolucionTemp) {
						if (Constantes.TRAMITE_SUBTIPO_CANCELACION.equals(mrTemp.getSubtipo())) {
							if (this.modeloResolucionList.contains(mrTemp)) {
								this.modeloResolucionList.remove(mrTemp);
							}
						}
					}

				}
			}

			if (this.tramite.isViaGubernativa()) {

				String tipoTramite = null;

				tipoTramite = ETramiteTipoTramite.VIA_GUBERNATIVA.getCodigo();

				this.modeloResolucionList
						.addAll(this.getTramiteService().findModelosResolucionByTipoTramite(tipoTramite, null, null));
			}

			// se define si el avaluó fue confirmado o modificado
			// o si la autoestimación fue aceptada o rechazada
			if (this.tramite.isRevisionAvaluo() || this.tramite.isCuarta()) {
				List<ComisionTramite> cml = this.tramite.getComisionTramites();
				this.banderaConfirmaAvaluo = false;
				// se coloca esta condición para que habilite el botón de enviar al coordinador
				// en
				// revisión de avaluó
				if (this.tramite.isCuarta()) {
					this.banderaAceptaAutoestimacion = false;
				}

				if (cml != null) {
					if (cml.size() > 0) {
						String resultado = cml.get(0).getResultado();
						if (resultado.equals(EComisionTramiteResultAv.CONFIRMA.getCodigo())) {
							this.banderaConfirmaAvaluo = true;
						} else if (resultado.equalsIgnoreCase(EComisionTramiteResultAv.ACEPTA.getCodigo())) {
							this.banderaAceptaAutoestimacion = true;
						}
					}
				}
			}

			// Se filtra la lista cuando es un trámite de revisión de avaluo.
			if (this.tramite.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {
				this.modeloResolucionList = this.obtenerListaModelosAvaluos(this.modeloResolucionList);
			}

			// Se filtra la lista cuando es un trámite de revisión de avaluo.
			if (this.tramite.isCuarta()) {
				this.modeloResolucionList = this.obtenerListaModelosAutoestimacion(this.modeloResolucionList);
			}

			if (this.modeloResolucionList != null && !this.modeloResolucionList.isEmpty()) {
				for (ModeloResolucion modelo : this.modeloResolucionList) {
					this.titulosResolucion.add(new SelectItem(modelo.getId(), modelo.getTextoTitulo()));
				}
			}

			this.tramiteTextoResolucion = this.getTramiteService()
					.buscarTramiteTextoResolucionPorIdTramite(this.tramite.getId());

			this.modeloResolucionSeleccionado = new ModeloResolucion();

			// Si el trámite ya tenía un modelo, este se establece.
			if (this.tramiteTextoResolucion != null && !(this.tramite.isViaGubernativa())) {

				if (this.tramiteTextoResolucion.getModeloResolucion() != null) {
					this.modeloResolucionSeleccionado.setId(this.tramiteTextoResolucion.getModeloResolucion().getId());
				}

				this.modeloResolucionSeleccionado.setTextoTitulo(this.tramiteTextoResolucion.getTitulo());

				this.modeloResolucionSeleccionado.setTextoConsiderando(this.tramiteTextoResolucion.getConsiderando());

				this.modeloResolucionSeleccionado.setTextoResuelve(this.tramiteTextoResolucion.getResuelve());

			} else if (this.tramite.isViaGubernativa()) {

				if (!this.modeloResolucionList.isEmpty()) {

					for (ModeloResolucion modelo : this.modeloResolucionList) {

						if (modelo.getId() != null) {
							this.modeloResolucionSeleccionado.setId(modelo.getId());
						}

						this.modeloResolucionSeleccionado.setTextoTitulo(modelo.getTextoTitulo());

						this.modeloResolucionSeleccionado.setTextoConsiderando(modelo.getTextoConsiderando());

						this.modeloResolucionSeleccionado.setTextoResuelve(modelo.getTextoResuelve());
					}

				}

			} else {
				this.tramiteTextoResolucion = new TramiteTextoResolucion();
				this.tramiteTextoResolucion.setFechaLog(new Date());
				if (this.modeloSinPlantilla.get(0) != null) {
					this.modeloResolucionSeleccionado = (ModeloResolucion) this.modeloSinPlantilla.get(0).clone();
				} else {
					this.modeloResolucionSeleccionado.setTextoTitulo("");
					this.modeloResolucionSeleccionado.setTextoConsiderando("");
					this.modeloResolucionSeleccionado.setTextoResuelve("");
				}
			}

			if (this.modeloResolucionList != null) {
				if (this.modeloSinPlantilla != null && this.modeloSinPlantilla.get(0) != null) {
					this.modeloResolucionList.add(this.modeloSinPlantilla.get(0));
				}
			}
		}
		if (this.usuario.getDescripcionTerritorial().contains(Constantes.PREFIJO_DELEGADOS)
				&& this.tramite.isActualizacion()) {
			List<ModeloResolucion> modelosDelegada = this.getTramiteService().findModelosResolucionByTipoTramite(
					this.tramite.getTipoTramite(), this.tramite.getClaseMutacion(), null,
					this.usuario.getCodigoEstructuraOrganizacional());
			for (ModeloResolucion modelosDelegada1 : modelosDelegada) {
				this.titulosResolucion.add(new SelectItem(modelosDelegada1.getId(), modelosDelegada1.getTextoTitulo()));
			}
		}
	}

	private List<ModeloResolucion> obtenerListaModelosAvaluos(List<ModeloResolucion> listaInicial) {

		List<ModeloResolucion> resultado = new ArrayList<ModeloResolucion>();

		for (ModeloResolucion md : listaInicial) {
			if (this.banderaConfirmaAvaluo) {
				if (md.getTextoTitulo().contains("RECHAZA")) {
					resultado.add(md);
				} else {
					continue;
				}
			}
			if (!this.banderaConfirmaAvaluo) {
				if (md.getTextoTitulo().contains("ACEPTA")) {
					resultado.add(md);
				} else {
					continue;
				}
			}
		}
		return resultado;
	}

	/**
	 * Metodo para filtrar las opciones de plantillas dependiendo si la
	 * autoestimación se acepta o se rechaza.
	 *
	 * @author juanfelipe.garcia
	 * @param listaInicial
	 * @return
	 */
	private List<ModeloResolucion> obtenerListaModelosAutoestimacion(List<ModeloResolucion> listaInicial) {

		List<ModeloResolucion> resultado = new ArrayList<ModeloResolucion>();

		for (ModeloResolucion md : listaInicial) {
			if (!this.banderaAceptaAutoestimacion) {
				if (md.getTextoTitulo().contains("RECHAZA")) {
					resultado.add(md);
				} else {
					continue;
				}
			}
			if (this.banderaAceptaAutoestimacion) {
				if (md.getTextoTitulo().contains("ACEPTA")) {
					resultado.add(md);
				} else {
					continue;
				}
			}
		}
		return resultado;
	}

	// ------------------------------------------------------------------------
	// //
	/**
	 * Método encargado de actualizar el modelo resolución seleccionado cuando se
	 * elige el tab de texto motivado
	 *
	 * @author javier.aponte
	 */
	public void actualizarModeloResolucionSeleccionado() {

		if (this.tramiteTextoResolucion == null) {
			this.tramiteTextoResolucion = this.getTramiteService()
					.buscarTramiteTextoResolucionPorIdTramite(this.tramite.getId());
		}

		if (this.modeloResolucionSeleccionado == null) {
			this.modeloResolucionSeleccionado = new ModeloResolucion();
		}

		// Si el trámite ya tenía un modelo, este se establece.
		if (this.tramiteTextoResolucion != null) {

			String considerando = this.tramiteTextoResolucion.getConsiderando();

			StringBuilder considerandoFinal = new StringBuilder();

			if (considerando != null && !considerando.isEmpty()) {
				considerandoFinal.append(considerando);
				int indexIniciaDocumentosJustificativos = considerando
						.indexOf(ConstantesResolucionesTramites.DOCUMENTOS_JUSTIFICATIVOS);
				String considerandoParteUno;
				String considerandoParteDos;
				String considerandoParteTres = null;
				String documentosJustificativos;
				int indexFinalizaDocumentosJustificativos;

				if (indexIniciaDocumentosJustificativos > 0) {
					considerandoParteUno = considerando.substring(0, indexIniciaDocumentosJustificativos);
					considerandoParteDos = considerando.substring(indexIniciaDocumentosJustificativos);
					indexFinalizaDocumentosJustificativos = considerandoParteDos.indexOf("<br>");
					if (indexFinalizaDocumentosJustificativos > 0) {
						considerandoParteTres = considerandoParteDos.substring(indexFinalizaDocumentosJustificativos);
						if (considerandoParteTres.startsWith("<br>.")) {
							considerandoParteTres = considerandoParteTres.substring(4);
						}
					}
					documentosJustificativos = this.formarDocumentosJustificativos();

					considerandoFinal = new StringBuilder();
					considerandoFinal.append(considerandoParteUno);
					considerandoFinal.append(" " + ConstantesResolucionesTramites.DOCUMENTOS_JUSTIFICATIVOS);
					considerandoFinal.append(documentosJustificativos);
					if (considerandoParteTres != null) {
						considerandoFinal.append(considerandoParteTres);
					}
				}

			}

			if (this.tramiteTextoResolucion.getModeloResolucion() != null) {
				this.modeloResolucionSeleccionado.setId(this.tramiteTextoResolucion.getModeloResolucion().getId());
			} else {
				this.modeloResolucionSeleccionado.setId(this.modeloSinPlantilla.get(0).getId());
			}
			if (this.tramiteTextoResolucion.getTitulo() == null || this.tramiteTextoResolucion.getTitulo().isEmpty()) {

			}

			this.modeloResolucionSeleccionado.setTextoTitulo(this.tramiteTextoResolucion.getTitulo());
			this.modeloResolucionSeleccionado.setTextoConsiderando(considerandoFinal.toString());
			this.modeloResolucionSeleccionado.setTextoResuelve(this.tramiteTextoResolucion.getResuelve());

		} else {
			this.tramiteTextoResolucion = new TramiteTextoResolucion();
			this.tramiteTextoResolucion.setFechaLog(new Date());
			if (this.modeloSinPlantilla.get(0) != null) {
				this.modeloResolucionSeleccionado = (ModeloResolucion) this.modeloSinPlantilla.get(0).clone();
			} else {
				this.modeloResolucionSeleccionado.setTextoTitulo(" ");
				this.modeloResolucionSeleccionado.setTextoConsiderando(" ");
				this.modeloResolucionSeleccionado.setTextoResuelve(" ");
			}
		}

	}

	// ------------------------------------------------------------------------
	// //
	/**
	 * Método que establece en el modelo de resolución el texto considerando y el
	 * texto resuelve de a plantilla de texto resolucion seleccionada.
	 *
	 * @author david.cifuentes
	 */
	public void establecerConsiderandoYResuelve() {

		LOGGER.debug("Entra a consultar establecerConsiderandoYResuelve");
		if (this.modeloResolucionList != null && !this.modeloResolucionList.isEmpty()
				&& this.modeloResolucionSeleccionado != null) {
			for (ModeloResolucion modelo : this.modeloResolucionList) {
				if (modelo.getId().longValue() == this.modeloResolucionSeleccionado.getId().longValue()) {
					this.modeloResolucionSeleccionado.setTextoTitulo(modelo.getTextoTitulo());
					this.modeloResolucionSeleccionado.setTextoConsiderando(modelo.getTextoConsiderando());
					this.modeloResolucionSeleccionado.setTextoResuelve(modelo.getTextoResuelve());
				}
			}
		}
		this.reemplazarParametrosTextoConsiderandoResuelve();
	}

	// ------------------------------------------------------------------------
	// //
	/**
	 * Método que reemplaza los parametros del texto considerando y del texto
	 * resuelve de la resolución dependiendo del tipo de trámite, la clase de
	 * mutación y el subtipo, el texto viene de la tabla MODELO_RESOLUCION, los
	 * parametros se caracterizan por el patron {#} de una variable en java
	 *
	 * @author javier.aponte
	 */
	/*
	 * @modified by Se agrega Resolucion de Rectificacion No avaluo 04/12/2015
	 */
	public void reemplazarParametrosTextoConsiderandoResuelve() {

		if (!this.modeloResolucionSeleccionado.getTextoTitulo().equals(ConstantesResolucionesTramites.SIN_PLANTILLA)) {
			String tipoTramite = this.tramite.getTipoTramite();

			Locale colombiaLocale = new Locale("ES", "es_CO");

			String estructuraOrganizacional = null;
			String responsableConservacion = null;

			Object parametrosTextoConsiderando[] = null;
			Object parametrosTextoResuelve[] = null;

			if (this.usuario.isTerritorial()) {
				estructuraOrganizacional = ConstantesResolucionesTramites.DIRECCION_TERRITORIAL
						+ this.usuario.getDescripcionEstructuraOrganizacional().toUpperCase();
				responsableConservacion = ConstantesResolucionesTramites.RESPONSABLE_DIRECCION_TERRITORIAL;
			}
			if (this.usuario.isUoc()) {
				estructuraOrganizacional = ConstantesResolucionesTramites.UNIDAD_OPERATIVA
						+ this.usuario.getDescripcionEstructuraOrganizacional().replaceAll("UOC_", "").toUpperCase();
				responsableConservacion = ConstantesResolucionesTramites.RESPONSABLE_UNIDAD_OPERATIVA;
			}

			StringBuilder solicitantes = new StringBuilder();
			StringBuilder identificaciones = new StringBuilder();
			StringBuilder condiciones = new StringBuilder();
			StringBuilder radicar = new StringBuilder();

			for (SolicitanteSolicitud solicitanteSolicitud : this.tramite.getSolicitud().getSolicitanteSolicituds()) {
				solicitantes
						.append(solicitanteSolicitud.getNombreCompleto()
								.substring(0, solicitanteSolicitud.getNombreCompleto().length() - 1).toUpperCase())
						.append(", ");
				identificaciones.append(solicitanteSolicitud.getTipoIdentificacion()).append(". No. ")
						.append(solicitanteSolicitud.getNumeroIdentificacion()).append(", ");
				condiciones.append(solicitanteSolicitud.getRelacion().toUpperCase()).append(", ");
			}

			StringBuilder senorIdentificacion = new StringBuilder();
			if (this.tramite.getSolicitud().getSolicitanteSolicituds().size() > 1) {
				senorIdentificacion.append(ConstantesResolucionesTramites.SENIORES).append(solicitantes)
						.append(ConstantesResolucionesTramites.IDENTIFICADOS).append(identificaciones)
						.append(ConstantesResolucionesTramites.CONDICIONES).append(condiciones)
						.append(ConstantesResolucionesTramites.RESPECTIVAMENTE);
				radicar.append(ConstantesResolucionesTramites.RADICARON);
			}
			if (this.tramite.getSolicitud().getSolicitanteSolicituds().size() == 1) {
				senorIdentificacion.append(ConstantesResolucionesTramites.SENIOR).append(solicitantes)
						.append(ConstantesResolucionesTramites.IDENTIFICADO).append(identificaciones)
						.append(ConstantesResolucionesTramites.CONDICION).append(condiciones);
				radicar.append(ConstantesResolucionesTramites.RADICO);
			}

			radicar.append(" " + ConstantesResolucionesTramites.NUMERO_RADICADO)
					.append(this.tramite.getNumeroRadicacion()).append(", ");

			List<Dominio> tiposSolicitud = this.getGeneralesService().getCacheDominioPorNombre(EDominio.SOLICITUD_TIPO);
			String tipoSolicitudValor = null;
			for (Dominio tmp : tiposSolicitud) {
				if (tmp.getCodigo().equals(this.tramite.getSolicitud().getTipo())) {
					tipoSolicitudValor = tmp.getValor().toUpperCase();
					break;
				}
			}
			String documentosJustificativos = null;

			documentosJustificativos = this.formarDocumentosJustificativos();

			if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo())
					|| tipoTramite.equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())
					|| tipoTramite.equals(ETramiteTipoTramite.RECTIFICACION.getCodigo())
					|| tipoTramite.equals(ETramiteTipoTramite.CANCELACION_DE_PREDIO.getCodigo())
					|| tipoTramite.equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo())
					|| tipoTramite.equals(ETramiteTipoTramite.MODIFICACION_INSCRIPCION_CATASTRAL.getCodigo())
					|| this.tramite.getSolicitud().getTipo().equals(ESolicitudTipo.VIA_GUBERNATIVA.getCodigo())) {

				parametrosTextoConsiderando = new Object[14];
				parametrosTextoResuelve = new Object[8];

				parametrosTextoConsiderando[0] = estructuraOrganizacional;
				parametrosTextoConsiderando[1] = responsableConservacion;
				parametrosTextoConsiderando[2] = senorIdentificacion;
				parametrosTextoConsiderando[3] = (this.tramite.getPredio() == null) ? ""
						: this.tramite.getPredio().getNumeroPredial();
				parametrosTextoConsiderando[4] = (this.tramite.getMunicipio() == null) ? ""
						: this.tramite.getMunicipio().getNombre().toUpperCase();
				parametrosTextoConsiderando[5] = radicar;
				parametrosTextoConsiderando[6] = tipoSolicitudValor;
				parametrosTextoConsiderando[7] = documentosJustificativos;

				parametrosTextoConsiderando[8] = this.tramite.getTipoTramiteCadenaCompleto().toUpperCase();

				parametrosTextoResuelve[0] = responsableConservacion;
				parametrosTextoResuelve[1] = 2;
				parametrosTextoResuelve[2] = 3;
				parametrosTextoResuelve[3] = 4;
				parametrosTextoResuelve[4] = 5;

				if (tipoTramite.equals(ETramiteTipoTramite.RECTIFICACION.getCodigo())
						&& this.tramite.getTramiteRectificacions() != null) {
					String parametroTextoConsiderandoDiez = null;

					for (TramiteRectificacion tr : this.tramite.getTramiteRectificacions()) {

						if (tr.getDatoRectificar() != null
								&& (EDatoRectificarNombre.PARTICIPACION.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.FOTO.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.DIRECCION_PRINCIPAL.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.TIPO_IDENTIFICACION.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.NUMERO_IDENTIFICACION.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.PRIMER_APELLIDO.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.SEGUNDO_APELLIDO.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.PRIMER_NOMBRE.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.SEGUNDO_NOMBRE.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.RAZON_SOCIAL.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
										|| EDatoRectificarNombre.SIGLA.getCodigo()
												.equalsIgnoreCase(tr.getDatoRectificar().getNombre()))
								|| Constantes.DATOS_RECTIFICAR_USO.equals(tr.getDatoRectificar().getColumna())
								|| Constantes.MATRICULA_INMOBILIARIA.equals(tr.getDatoRectificar().getColumna())
								|| EDatoRectificarNombre.DIRECCION.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.TIPO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.DESTINO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.MODO_ADQUISICION.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.VALOR.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.TIPO_TITULO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.ENTIDAD_EMISORA.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.DEPARTAMENTO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.MUNICIPIO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.NUMERO_TITULO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.FECHA_TITULO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.FECHA_REGISTRO.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.ANIO_CONSTRUCCION.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.TOTAL_PISOS_CONSTRUCCION.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.TOTAL_HABITACIONES.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.TOTAL_BANIOS.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())
								|| EDatoRectificarNombre.TOTAL_LOCALES.getCodigo()
										.equalsIgnoreCase(tr.getDatoRectificar().getNombre())) {

							if (parametroTextoConsiderandoDiez != null) {
								parametroTextoConsiderandoDiez = parametroTextoConsiderandoDiez + ", "
										+ tr.getDatoRectificar().getNombre();
							} else {
								parametroTextoConsiderandoDiez = tr.getDatoRectificar().getNombre();
							}

							parametrosTextoConsiderando[10] = parametroTextoConsiderandoDiez;

						}
					}

				}

				if (this.tramite.getSolicitud().isViaGubernativa()) {
					Tramite tramiteViaGubernativa = this.buscarTramiteAsociadoViaGubernativa(
							this.tramite.getSolicitud().getTramiteNumeroRadicacionRef());

					parametrosTextoConsiderando[8] = this.tramite.getTipoTramiteCadenaCompleto().toUpperCase();
					parametrosTextoConsiderando[9] = tramiteViaGubernativa.getResultadoDocumento().getNumeroDocumento();
					parametrosTextoConsiderando[10] = Constantes.FORMAT_FECHA_RESOLUCION
							.format(tramiteViaGubernativa.getResultadoDocumento().getFechaDocumento()).toUpperCase();
					parametrosTextoConsiderando[11] = tramiteViaGubernativa.getSolicitud().getNumero();
					parametrosTextoConsiderando[12] = Constantes.FORMAT_FECHA_RESOLUCION
							.format(tramiteViaGubernativa.getSolicitud().getFecha()).toUpperCase();
					parametrosTextoConsiderando[13] = tramiteViaGubernativa.getNombreFuncionarioEjecutor()
							.toUpperCase();
					parametrosTextoConsiderando[10] = Constantes.FORMAT_FECHA_RESOLUCION
							.format(tramiteViaGubernativa.getResultadoDocumento().getFechaDocumento()).toUpperCase();
					parametrosTextoConsiderando[11] = tramiteViaGubernativa.getSolicitud().getNumero();
					parametrosTextoConsiderando[12] = Constantes.FORMAT_FECHA_RESOLUCION
							.format(tramiteViaGubernativa.getSolicitud().getFecha()).toUpperCase();
					parametrosTextoConsiderando[13] = tramiteViaGubernativa.getNombreFuncionarioEjecutor()
							.toUpperCase();

					parametrosTextoResuelve[5] = tramiteViaGubernativa.getResultadoDocumento().getNumeroDocumento();
					parametrosTextoResuelve[6] = Constantes.FORMAT_FECHA_RESOLUCION_2
							.format(tramiteViaGubernativa.getResultadoDocumento().getFechaDocumento()).toUpperCase();
					parametrosTextoResuelve[7] = solicitantes.toString();
				}
			}
			if (parametrosTextoConsiderando != null) {
				this.modeloResolucionSeleccionado.setTextoConsiderando(
						new MessageFormat(this.modeloResolucionSeleccionado.getTextoConsiderando(), colombiaLocale)
								.format(parametrosTextoConsiderando));
			}
			if (parametrosTextoResuelve != null) {
				this.modeloResolucionSeleccionado.setTextoResuelve(
						new MessageFormat(this.modeloResolucionSeleccionado.getTextoResuelve(), colombiaLocale)
								.format(parametrosTextoResuelve));
			}
		}
	}

	// --------------------------------------------------------------------------------------------------
	private String formarDocumentosJustificativos() {

		String answer = "";

		List<PPersonaPredioPropiedad> pPersonaPredioPropiedad;

		pPersonaPredioPropiedad = new ArrayList<PPersonaPredioPropiedad>();

		boolean agregarDocumentoJustificativo;

		StringBuilder documentosJustificativos = new StringBuilder();

		if (this.predioSeleccionado != null && this.predioSeleccionado.getPPersonaPredios() != null) {
			for (PPersonaPredio persPred : predioSeleccionado.getPPersonaPredios()) {
				for (PPersonaPredioPropiedad ppp : persPred.getPPersonaPredioPropiedads()) {
					if (ppp.getCancelaInscribe() != null
							&& (ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo()) || ppp
									.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {

						agregarDocumentoJustificativo = true;
						for (PPersonaPredioPropiedad pppTemp : pPersonaPredioPropiedad) {
							if (pppTemp.getId() != ppp.getId() && pppTemp.getNumeroTitulo() != null
									&& pppTemp.getFechaTitulo() != null
									&& pppTemp.getNumeroTitulo().equals(ppp.getNumeroTitulo())
									&& pppTemp.getFechaTitulo().equals(ppp.getFechaTitulo())) {
								agregarDocumentoJustificativo = false;
							}
						}

						if (agregarDocumentoJustificativo && ppp.getTipoTitulo() != null
								&& ppp.getNumeroTitulo() != null) {
							documentosJustificativos
									.append(((ppp.getTipoTitulo() != null) ? ppp.getTipoTitulo().toUpperCase() : "")
											+ " NO. " + ((ppp.getNumeroTitulo() != null) ? ppp.getNumeroTitulo() : "")
											+ " DE FECHA "
											+ ((ppp.getFechaTitulo() != null)
													? formatoFechaTituloResolucion.format(ppp.getFechaTitulo())
															.toUpperCase()
													: "")
											+ ", " + ConstantesResolucionesTramites.NOTARIA + " "
											+ (ppp.getEntidadEmisora() == null
													? ""
													: ppp.getEntidadEmisora().toUpperCase())
											+ " DE "
											+ (ppp.getMunicipio() == null ? ""
													: ppp.getMunicipio().getNombre().toUpperCase())
											+ ", "
											+ (ppp.getDepartamento() == null ? ""
													: ppp.getDepartamento().getNombre().toUpperCase())
											+ ", " + ConstantesResolucionesTramites.MATRICULA_INMOBILIARIA + ", "
											+ (this.predioSeleccionado.getNumeroRegistro() != null
													? this.predioSeleccionado.getNumeroRegistro()
													: "")
											+ ",<br>");
							pPersonaPredioPropiedad.add(ppp);
						}
					}
				}
			}
		}

		if (documentosJustificativos.length() > 5) {
			answer = documentosJustificativos
					.delete(documentosJustificativos.length() - 5, documentosJustificativos.length()).toString();
		}

		return answer;
	}

	// ------------------------------------------------------------------------
	// //
	/**
	 * Método que guarda el tramite texto resolucion que se modifico a partir de la
	 * plantilla.
	 *
	 * @author david.cifuentes
	 *
	 * @modified by javier.aponte #7249 se genera la resolución sin marca de agua
	 *           11/03/2014
	 * @modified by leidy.gonzalez #9954 22/10/2014 Se guarda la fecha de
	 *           inscripción del predio proyectado para tramites de tercera,
	 *           complementación, modificacion de inscripción catastral y revisión
	 *           de avaluo.
	 */
	/**
	 * Método que guarda el tramite texto resolucion que se modifico a partir de la
	 * plantilla.
	 *
	 * @author david.cifuentes
	 *
	 * @modified by javier.aponte #7249 se genera la resolución sin marca de agua
	 *           11/03/2014
	 * @modified by leidy.gonzalez #9954 22/10/2014 Se guarda la fecha de
	 *           inscripción del predio proyectado para tramites de tercera,
	 *           complementación, modificacion de inscripción catastral y revisión
	 *           de avaluo.
	 */
	public void guardarTramiteTextoResolucion() {

		String txtConsiderando, txtResuelve;
		FirmaUsuario firma = null;
		String documentoFirma = null;
		LOGGER.debug("Entra a Guardar Tramite Texto Resolucion");

		if (this.modeloResolucionSeleccionado.getTextoConsiderando().isEmpty()) {
			this.addMensajeError("El texto considerando debe tener un valor");
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			return;
		}

		if (this.tramite != null && this.modeloResolucionSeleccionado != null) {

			// javier.aponte::06-07-2015::Tercera Másiva::Se determinan los predios
			// modificados para trámites de tercera másivos
			if (this.tramite.isTerceraMasiva()) {
				this.determinarPrediosModificadosTerceraMasiva();
			}

			if (this.tramite.isTercera() || this.tramite.isEsComplementacion()
					|| this.tramite.isModificacionInscripcionCatastral() || this.tramite.isRevisionAvaluo()) {
				fechaInscripcionCatastral = new Date(System.currentTimeMillis());
				this.predioSeleccionado.setFechaInscripcionCatastral(fechaInscripcionCatastral);
				this.getConservacionService().guardarActualizarPPredio(this.predioSeleccionado);
			}

			// Campos de USUARIO
			this.tramiteTextoResolucion.setFechaLog(new Date(System.currentTimeMillis()));
			this.tramiteTextoResolucion.setUsuarioLog(this.usuario.getLogin());

			this.tramiteTextoResolucion.setTitulo(this.modeloResolucionSeleccionado.getTextoTitulo());

			// Se debe hacer un replace de los estilos debido a que el editor los guarda de
			// una manera y
			// al generar el reporte, jasper los traduce de otra, por tal motivo hay que
			// reemplazarlos manualmente.
			txtConsiderando = reemplazarEstilosDelEditor(this.modeloResolucionSeleccionado.getTextoConsiderando());
			txtResuelve = reemplazarEstilosDelEditor(this.modeloResolucionSeleccionado.getTextoResuelve());

			this.modeloResolucionSeleccionado.setTextoConsiderando(txtConsiderando);
			this.modeloResolucionSeleccionado.setTextoResuelve(txtResuelve);
			this.tramiteTextoResolucion.setConsiderando(txtConsiderando);
			this.tramiteTextoResolucion.setResuelve(txtResuelve);

			this.tramiteTextoResolucion.setTramite(this.tramite);
			this.tramiteTextoResolucion.setModeloResolucion(this.modeloResolucionSeleccionado);

			try {
				// Guardar Texto Resolución
				this.tramiteTextoResolucion = this.getTramiteService()
						.guardarTramiteTextoResolucion(this.tramiteTextoResolucion);
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
				this.addMensajeError("Hubo un error al guardar el texto motivo, por favor intente nuevamente");
				return;
			}

			// ------ REPORTE ------//
			EReporteServiceSNC enumeracionReporte = this.getTramiteService()
					.obtenerUrlReporteResoluciones(this.tramite.getId());

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("TRAMITE_ID", "" + this.tramite.getId());
			parameters.put("COPIA_USO_RESTRINGIDO", "false");

			if (this.tramite.isTipoTramiteViaGubernativaModificado()
					|| this.tramite.isTipoTramiteViaGubModSubsidioApelacion() || this.tramite.isRecursoApelacion()
					|| this.tramite.isViaGubernativa()) {

				if (!this.tramite.getNumeroRadicacionReferencia().isEmpty()) {

					Tramite tramiteReferencia = this.getTramiteService()
							.buscarTramitePorNumeroRadicacion(this.tramite.getNumeroRadicacionReferencia());

					if (tramiteReferencia != null) {
						parameters.put("NOMBRE_SUBREPORTE_RESUELVE", this.getTramiteService()
								.obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.getId()));
					}
				}

			}

			UsuarioDTO responsableConservacion = null;
			UsuarioDTO director = null;
			List<UsuarioDTO> directoresTerritoriales = null;

			directoresTerritoriales = (List<UsuarioDTO>) this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
					this.usuario.getDescripcionEstructuraOrganizacional(), ERol.DIRECTOR_TERRITORIAL);

			if (directoresTerritoriales != null && !directoresTerritoriales.isEmpty()
					&& directoresTerritoriales.get(0) != null) {
				for (UsuarioDTO udto : directoresTerritoriales) {

					if (ELDAPEstadoUsuario.ACTIVO.codeExist(udto.getEstado())) {
						director = udto;
					}
				}
			}

			// Se valida si el usuario logeado es el director territorial
			// Si no lo es se envia como parametro el responsable de Conservacion
			if (director != null && director.getNombreCompleto() != null && this.usuario.getLogin() != null
					&& this.usuario.getLogin().equals(director.getLogin())) {

				parameters.put("DIRECTOR_TERRITORIAL", director.getNombreCompleto());

			} else {

				if (this.responsablesConservacion == null) {
					this.responsablesConservacion = (List<UsuarioDTO>) this.getTramiteService()
							.buscarFuncionariosPorRolYTerritorial(this.usuario.getDescripcionEstructuraOrganizacional(),
									ERol.RESPONSABLE_CONSERVACION);
				}

				// Se consulta el responsable de conservación asociado a la territorial
				if (this.responsablesConservacion == null) {
					this.responsablesConservacion = (List<UsuarioDTO>) this.getTramiteService()
							.buscarFuncionariosPorRolYTerritorial(this.usuario.getDescripcionEstructuraOrganizacional(),
									ERol.RESPONSABLE_CONSERVACION);
				}

				// Se captura el nombre del responsable de conservación para
				// posteriormente enviar comom parámetro.
				// Para el caso en que no lo encuentre (Éste caso no se debería presentar)
				// el reporte controlará que no se muestren valores nulos, en éste caso
				// se mostrará unicamente el rol y la territorial.
				if (this.responsablesConservacion != null && !this.responsablesConservacion.isEmpty()
						&& this.responsablesConservacion.get(0) != null) {
					for (UsuarioDTO udto : this.responsablesConservacion) {

						if (ELDAPEstadoUsuario.ACTIVO.codeExist(udto.getEstado())) {
							responsableConservacion = udto;
						}
					}
				}

				// Siempre debe existir un responsable de conservación porque es quien firma el
				// documento
				// en caso que haya no se debe generar el reporte
				if (responsableConservacion != null && responsableConservacion.getNombreCompleto() != null) {
					parameters.put("NOMBRE_RESPONSABLE_CONSERVACION", responsableConservacion.getNombreCompleto());
				}
			}

			if (this.tramite.getObservacionesRadicacion() != null
					&& !this.tramite.getObservacionesRadicacion().isEmpty()
					&& this.tramite.getObservacionesRadicacion()
							.contains("Recurso de reposición en subsidio de apelación")
					&& director != null && this.usuario.getLogin() != null
					&& this.usuario.getLogin().equals(director.getLogin())) {

				firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(this.usuario.getNombreCompleto());

				if (firma != null && this.usuario.getNombreCompleto().equals(firma.getNombreFuncionarioFirma())) {

					documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
							firma.getDocumentoId().getIdRepositorioDocumentos());

					parameters.put("FIRMA_USUARIO", documentoFirma);
				}

			} else if (this.usuario != null && this.usuario.getNombreCompleto() != null) {

				firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(this.usuario.getNombreCompleto());

				if (firma != null && this.usuario.getNombreCompleto().equals(firma.getNombreFuncionarioFirma())) {

					documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
							firma.getDocumentoId().getIdRepositorioDocumentos());

					parameters.put("FIRMA_USUARIO", documentoFirma);
				}
			}

			this.reporteResoluciones = reportsService.generarReporte(parameters, enumeracionReporte, this.usuario);

			if (this.reporteResoluciones != null) {
				// Mensaje satisfactorio
				this.addMensajeInfo("Se almacenó el texto motivo satisfactoriamente.");
			}
		}
	}

	// ------------------------------------------------------------------------
	// //
	/**
	 * Método que reemplaza los estilos de negrilla y cursiva guardados por el
	 * editor
	 *
	 * @author david.cifuentes
	 * @param txt
	 */
	public String reemplazarEstilosDelEditor(String txt) {
		if (txt != null && !txt.trim().isEmpty()) {
			txt = txt.replaceAll("<strong>", "<b>");
			txt = txt.replaceAll("</strong>", "</b>");
			txt = txt.replaceAll("<em>", "<i>");
			txt = txt.replaceAll("</em>", "</i>");
		}
		return txt;
	}

	// -------------------------------------------------------------------- //
	/**
	 * Método que reemplaza los parámetros y realiza el kkamado al método que genera
	 * el reporte del texto motivado
	 *
	 * @modified by javier.aponte #7249 se genera la resolución sin marca de agua
	 *           11/03/2014
	 */
	public void generarReporteTextoMotivado() {

		try {
			EReporteServiceSNC enumeracionReporte = this.getTramiteService()
					.obtenerUrlReporteResoluciones(this.tramite.getId());

			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("TRAMITE_ID", "" + this.tramite.getId());
			parameters.put("COPIA_USO_RESTRINGIDO", "false");
			if (this.usuario.getCodigoEstructuraOrganizacional() != null
					&& !this.usuario.getCodigoEstructuraOrganizacional().isEmpty()) {
				parameters.put("CODIGO_ESTRUCTURA_ORGANIZACIONAL", this.usuario.getCodigoEstructuraOrganizacional());
			}

			this.reporteResoluciones = reportsService.generarReporte(parameters, enumeracionReporte, this.usuario);
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que elimina una {@link PPredioDireccion} seleccionada.
	 *
	 * @author david.cifuentes
	 */
	public void eliminarDireccion() {

		if (this.pPredioDireccionSeleccionado != null) {

			this.predioSeleccionado.getPPredioDireccions().remove(this.pPredioDireccionSeleccionado);
			this.pPredioDireccionSeleccionado = (PPredioDireccion) this.getConservacionService()
					.eliminarProyeccion(this.usuario, this.pPredioDireccionSeleccionado);
			this.actualizarListaDeDirecciones();
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que realiza una validaciones sobre las {@link PPredioDireccion} del
	 * {@link PPredio} para no mostrar en pantalla las que se encuentran en estado
	 * cancelado.
	 *
	 * @author david.cifuentes
	 */
	public void actualizarListaDeDirecciones() {

		this.validacionDireccionBool = false;
		this.predioSeleccionadoDirecciones = new ArrayList<PPredioDireccion>();
		List<PredioDireccion> direccionesEnFirme = new ArrayList<PredioDireccion>();

		Predio predioActual = new Predio();

		if (this.predioSeleccionado != null && this.predioSeleccionado.getPPredioDireccions() != null
				&& !this.predioSeleccionado.getPPredioDireccions().isEmpty()) {
			for (PPredioDireccion ppd : this.predioSeleccionado.getPPredioDireccions()) {
				if (ppd.getCancelaInscribe() == null
						|| !ppd.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
					if (!this.predioSeleccionadoDirecciones.contains(ppd)) {
						this.predioSeleccionadoDirecciones.add(ppd);
					}
				}
			}
		}

		// felipe.cadena::22-03-2016::#16576::Es necesario determinar si
		// las direcciones son nuevas o originales para los tramites de
		// complementacion
		if (this.tramite.isEsComplementacion()) {
			predioActual.setId(this.predioSeleccionado.getId());
			direccionesEnFirme = this.getConservacionService().obtenerDireccionesPredio(predioActual);
			for (PPredioDireccion ppd : this.predioSeleccionadoDirecciones) {
				for (PredioDireccion pd : direccionesEnFirme) {
					if (pd.getDireccion().equals(ppd.getDireccion())) {
						ppd.setOriginal(true);
					}
				}
			}
		}

		if (this.predioSeleccionadoDirecciones.size() == 1) {
			this.setValidacionDireccionBool(true);
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que actualiza la {@link PPredioDireccion} de un {@link PPredio}
	 * marcada como principal, deseleccionando la anterior.
	 *
	 * @author david.cifuentes
	 */
	/*
	 * @modified felipe.cadena::29-06-2016::#18317::Solo se pasan a Modifica (M) las
	 * direcciones que no estan en estado Inscribe (I), ya que estas realmente no
	 * existen en firme y por lo tanto se generan problemas en la aplicacion de
	 * cambios
	 */
	public void cambiarPrincipal() {
		if (this.pPredioDireccionSeleccionado != null) {

			for (PPredioDireccion pd : this.predioSeleccionado.getPPredioDireccions()) {

				if (pd.getPrincipal().equals(ESiNo.SI.getCodigo())) {
					pd.setPrincipal(ESiNo.NO.getCodigo());
					if (!EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pd.getCancelaInscribe())) {
						pd.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
					}
				}
				if (this.pPredioDireccionSeleccionado.getId().longValue() == pd.getId().longValue()) {
					pd.setPrincipal(ESiNo.SI.getCodigo());
					if (!EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(pd.getCancelaInscribe())) {
						pd.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
					}
				}
			}
		}
		try {
			this.getConservacionService().actualizarPPredioDirecciones(this.predioSeleccionado.getPPredioDireccions());
			this.actualizarListaDeDirecciones();
			if (this.tramite.isDesenglobeEtapas() && this.predioSeleccionado.isEsPredioFichaMatriz()) {
				this.addMensajeWarn(
						"Se modificó la dirección principal satisfactoriamente. Recuerde realizar la actualización de la dirección manualmente para cada uno de los predios generados a partir de la ficha.");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError(e);
		}
	}

	// --------------------------------------------------- //
	/**
	 * action del botón "generar" en la ventana en la que se adiciona una dirección
	 * para el predio. Metodo que normaliza el valor de una dirección.
	 *
	 * @author david.cifuentes
	 */
	public void normalizarDireccion() {
		this.direccionNormalizada = this.getConservacionService().normalizarDireccion(this.direccion);
		// lorena.salamanca :: 26-10-2015 :: Se agrega esta logica para dar
		// solucion al redmine 14876
		if (this.tramite.isRectificacion() || this.tramite.isTercera() || this.tramite.isSegunda()
				|| this.tramite.isQuinta() || this.tramite.isEsComplementacion()) {
			if (this.direccionNormalizada == null || this.direccionNormalizada.isEmpty()) {
				this.banderaAceptarDireccionNormalizada = true;
				this.direccionNormalizada = this.direccion;
			} else {
				this.banderaAceptarDireccionNormalizada = false;
			}
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método usado para reiniciar algunas variables cuando se usa la pantalla de
	 * edición de direcciones y referencias.
	 *
	 * @author david.cifuentes
	 */
	/*
	 * @modified pedro.garcia 13-08-2013 cambio de nombre del método porque no
	 * corresponde con el estándar
	 */
	public void entrarAModoEdicionDyR() {
		// Direcciones
		if (this.pPredioDireccionSeleccionado != null && this.editModeBool) {
			this.direccion = this.pPredioDireccionSeleccionado.getDireccion();
			this.normalizadaBool = false;
			this.direccionNormalizada = "";
			this.codigoPostalDireccion = this.pPredioDireccionSeleccionado.getCodigoPostal();

			if (this.pPredioDireccionSeleccionado.getPrincipal().equals(ESiNo.SI.getCodigo())) {
				this.principalBool = true;
			} else {
				this.principalBool = false;
			}
		}
		// Referencias
		if (this.referenciaCartograficaSeleccionada == null || !this.editModeBool) {
			this.referenciaCartograficaSeleccionada = new PReferenciaCartografica();
		}
	}

	// --------------------------------------------------- /
	/**
	 * Método que realiza diferentes validaciones para una {@link PPredioDireccion}
	 * que se desea actualizar.
	 *
	 * @author david.cifuentes
	 */
	/*
	 * @modified pedro.garcia 13-08-2013 el método se convierte en privado. Se
	 * corrige validación mal hecha sobre el código postal
	 */
	private boolean validarActualizarDireccion() {
		boolean validate = true;

		// se valida el codigo postal
		RequestContext context = RequestContext.getCurrentInstance();
		if (this.codigoPostalDireccion != null && !this.codigoPostalDireccion.isEmpty()) {

			if (this.codigoPostalDireccion.length() < 6) {
				this.addMensajeError("El Código postal debe tener por lo menos 6 digitos");
				context.addCallbackParam("error", "error");
				return false;
			}
			String initCodigoPostal = this.codigoPostalDireccion.substring(0, 2);

			if (!this.predioSeleccionado.getDepartamento().getCodigo().equals(initCodigoPostal)) {
				this.addMensajeError("Código postal no corresponde con el código del departamento seleccionado");
				context.addCallbackParam("error", "error");
				return false;
			}

		}

		if (this.getDireccion() != null && !this.direccion.trim().isEmpty()) {

			// Validar que no se repita la dirección.
			if (this.predioSeleccionadoDirecciones != null && this.predioSeleccionadoDirecciones.size() > 0) {

				// Si es una nueva dirección verificar en las que están que no
				// se encuentre repetida
				if (!this.editModeBool) {
					for (PPredioDireccion ppd : this.predioSeleccionadoDirecciones) {
						if (this.direccion.equals(ppd.getDireccion())) {

							this.addMensajeError(
									"La dirección que se quiere ingresar ya se encuentra registrada, por favor verifiquela.");

							context.addCallbackParam("error", "error");
							this.reiniciarVariables();
							validate = false;
						}
					}
				}
			}
		} else {
			this.addMensajeError("Ingrese un valor para la dirección.");
			context.addCallbackParam("error", "error");
			this.reiniciarVariables();
			validate = false;
		}
		return validate;
	}

	// --------------------------------------------------- /
	/**
	 * Método que actualiza una lista de {@link PPredioDireccion} de un
	 * {@link PPredio}
	 *
	 * @author david.cifuentes
	 */
	public void actualizarDireccion() {

		PPredioDireccion nuevaDireccion = new PPredioDireccion();

		List<PPredioDireccion> nuevasDirecciones = new ArrayList<PPredioDireccion>();

		this.predioSeleccionadoDirecciones = new ArrayList<PPredioDireccion>();

		if (validarActualizarDireccion()) {

			// Actualizacion de la direccion
			try {

				// Edición de una direccion seleccionada
				if (this.editModeBool && this.pPredioDireccionSeleccionado != null) {
					// integrado versión 1.1.6
					// felipe.cadena:: redmine #7572 :: Se valida mantener la
					// condicion de inscribe para direcciones nuevas

					// hector.arias::redmine #21380 ::
					// Cuando el predio exista en firme y la direccion se ha modificada
					// se debe cancelar la direccion, e inscribir la nueva direccion, para tramites
					// de:
					// segunda englobe, segunda desenglobe (NPH, PH, condominio),Tercera,
					// Quinta (NPH, PH, condominio),Rectificacion (dirección),complementación
					// (direcciòn)

					this.direccionesEditadas = new ArrayList<PPredioDireccion>();

					if (this.tramite.isDesenglobeEtapas() || this.tramite.isSegundaEnglobe()
							|| this.tramite.isSegundaDesenglobe() || this.tramite.isTercera()
							|| this.tramite.isQuintaOmitido() || this.tramite.isQuintaMasivo()
							|| this.tramite.isRectificacionDireccion()) {

						Predio p = this.getConservacionService()
								.obtenerPredioPorId(this.pPredioDireccionSeleccionado.getPPredio().getId());

						if (p != null) {
							// Se coloca en CANCELA el predio direccion seleccionado
							this.pPredioDireccionSeleccionado
									.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());

							this.pPredioDireccionSeleccionado.setCodigoPostal(this.codigoPostalDireccion);
							this.pPredioDireccionSeleccionado.setPPredio(this.predioSeleccionado);
							this.pPredioDireccionSeleccionado.setFechaLog(new Date(System.currentTimeMillis()));
							this.pPredioDireccionSeleccionado.setUsuarioLog(this.usuario.getLogin());

							this.direccionesEditadas.add(pPredioDireccionSeleccionado);

							// Se inserta la nueva dirreccion
							// Hago el set de la direccion dependiendo si se acepta o no la
							// normalizada.
							if (this.normalizadaBool) {
								nuevaDireccion.setDireccion(this.direccionNormalizada);
							} else {
								nuevaDireccion.setDireccion(this.direccion);
							}

							if (this.codigoPostalDireccion != null) {
								nuevaDireccion.setCodigoPostal(this.codigoPostalDireccion);
							}

							// Selecciono a la direccion nueva como principal
							if (this.principalBool) {

								nuevaDireccion.setPrincipal(ESiNo.SI.getCodigo());
								for (PPredioDireccion pPredioDir : this.predioSeleccionado.getPPredioDireccions()) {
									if (pPredioDir.getPrincipal().equals(ESiNo.SI.getCodigo())) {
										pPredioDir.setPrincipal(ESiNo.NO.getCodigo());
									}
								}
							} else {
								nuevaDireccion.setPrincipal(ESiNo.NO.getCodigo());
							}
							nuevaDireccion.setId(null);
							nuevaDireccion.setPPredio(this.predioSeleccionado);
							nuevaDireccion.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
							nuevaDireccion.setFechaLog(new Date(System.currentTimeMillis()));
							nuevaDireccion.setUsuarioLog(this.usuario.getLogin());

							this.direccionesEditadas.add(nuevaDireccion);

							this.direccionesEditadas = this.getConservacionService()
									.actualizarPPredioDirecciones(this.direccionesEditadas);

							for (PPredioDireccion de : this.direccionesEditadas) {
								this.predioSeleccionado.getPPredioDireccions().add(de);
							}

							for (PPredioDireccion nuevasDireccione : this.predioSeleccionado.getPPredioDireccions()) {
								if (nuevasDireccione.getCancelaInscribe() == null) {
									nuevasDireccione.setPPredio(this.predioSeleccionado);
									nuevasDireccione.setFechaLog(new Date(System.currentTimeMillis()));
									nuevasDireccione.setUsuarioLog(this.usuario.getLogin());
								} else {
									continue;
								}
							}

							this.getConservacionService()
									.actualizarPPredioDirecciones(this.predioSeleccionado.getPPredioDireccions());

						}
					} else {
						if (!EProyeccionCancelaInscribe.INSCRIBE.getCodigo()
								.equals(this.pPredioDireccionSeleccionado.getCancelaInscribe())) {
							this.pPredioDireccionSeleccionado
									.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
						}
					}
					// Hago el set de la direccion dependiendo si se acepta o no la
					// normalizada.
					if (this.tramite.isDesenglobeEtapas() || this.tramite.isSegundaEnglobe()
							|| this.tramite.isSegundaDesenglobe() || this.tramite.isTercera()
							|| this.tramite.isQuintaOmitido() || this.tramite.isQuintaMasivo()
							|| this.tramite.isRectificacionDireccion()) {

						if (this.normalizadaBool) {
							this.pPredioDireccionSeleccionado.setDireccion(this.direccionNormalizada);
						} else {
							this.pPredioDireccionSeleccionado.setDireccion(this.direccion);
						}

					}

				} else {

					// Insercion de una nueva direccion
					// Hago el set de la direccion dependiendo si se acepta o no la
					// normalizada.
					if (this.normalizadaBool) {
						nuevaDireccion.setDireccion(this.direccionNormalizada);
					} else {
						nuevaDireccion.setDireccion(this.direccion);
					}

					if (this.codigoPostalDireccion != null) {
						nuevaDireccion.setCodigoPostal(this.codigoPostalDireccion);
					}
                    // Selecciono a la direccion nueva como principal
					if (this.principalBool || (this.tramite.isEsComplementacion() && this.predioSeleccionado.getPPredioDireccions().size()==0)) {
						nuevaDireccion.setPrincipal(ESiNo.SI.getCodigo());
						for (PPredioDireccion pPredioDir : this.predioSeleccionado.getPPredioDireccions()) {
							if (pPredioDir.getPrincipal().equals(ESiNo.SI.getCodigo())) {
								pPredioDir.setPrincipal(ESiNo.NO.getCodigo());
							}
						}
					} else {
						nuevaDireccion.setPrincipal(ESiNo.NO.getCodigo());
					}
					nuevaDireccion.setPPredio(this.predioSeleccionado);
					nuevaDireccion.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
					nuevaDireccion.setFechaLog(new Date(System.currentTimeMillis()));
					nuevaDireccion.setUsuarioLog(this.usuario.getLogin());

					nuevasDirecciones.add(nuevaDireccion);

					nuevasDirecciones = this.getConservacionService().actualizarPPredioDirecciones(nuevasDirecciones);

					for (PPredioDireccion nd : nuevasDirecciones) {
						this.predioSeleccionado.getPPredioDireccions().add(nd);
					}

					for (PPredioDireccion nuevasDireccione : this.predioSeleccionado.getPPredioDireccions()) {
						if (nuevasDireccione.getCancelaInscribe() == null) {
							nuevasDireccione.setPPredio(this.predioSeleccionado);
							nuevasDireccione.setFechaLog(new Date(System.currentTimeMillis()));
							nuevasDireccione.setUsuarioLog(this.usuario.getLogin());
						} else {
							continue;
						}
					}

					this.getConservacionService()
							.actualizarPPredioDirecciones(this.predioSeleccionado.getPPredioDireccions());

				}

				if (this.predioSeleccionado != null && this.predioSeleccionado.getPPredioDireccions() != null
						&& !this.predioSeleccionado.getPPredioDireccions().isEmpty()) {
					for (PPredioDireccion ppd : this.predioSeleccionado.getPPredioDireccions()) {
						if (ppd.getCancelaInscribe() == null
								|| !ppd.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
							if (!this.predioSeleccionadoDirecciones.contains(ppd)) {
								this.predioSeleccionadoDirecciones.add(ppd);
							}
						}
					}
				}

				this.reiniciarVariables();
				if ((this.tramite.isDesenglobeEtapas() || this.tramite.isTerceraMasiva())
						&& this.predioSeleccionado.isEsPredioFichaMatriz()) {
					this.addMensajeWarn(
							"Se guardó la dirección satisfactoriamente. Recuerde realizar la actualización de la dirección manualmente para cada uno de los predios generados a partir de la ficha matriz.");
				}
				this.actualizarListaDeDirecciones();
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				this.addMensajeError("Error al guardar la dirección.");
			}

		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que reinicia las variables usadas en la pantalla de gestión de
	 * ubicación del predio.
	 *
	 * @author david.cifuentes
	 */
	public void reiniciarVariables() {

		// @modified by leidy.gonzalez:: #42687::01/11/2017
		// Se ajusta que desde la pantalla de entrada de proyeccion cada vez que se
		// modifique la direccion
		// de un predio, se visualice la direccion modificada y no la del predio de
		// ficha matriz
		if (this.tramite.isDesenglobeEtapas()) {
			if (this.predioSeleccionado.isEsPredioFichaMatriz()) {

				pPredioDireccionSeleccionado = this.getConservacionService()
						.obtenerDireccionPrincipalFM(this.predioSeleccionado.getNumeroPredial().substring(0, 21));

			} else {

				List<PPredioDireccion> direccionesProy = this.getConservacionService()
						.obtenerPpredioDirecionesPorPredioId(this.predioSeleccionado.getId());

				if (direccionesProy != null && !direccionesProy.isEmpty()) {
					for (PPredioDireccion pPredioDireccion : direccionesProy) {

						if (ESiNo.SI.getCodigo().equals(pPredioDireccion.getPrincipal())) {
							pPredioDireccionSeleccionado = pPredioDireccion;
						}
					}

				} else {
					pPredioDireccionSeleccionado = this.getConservacionService()
							.obtenerDireccionPrincipalFM(this.predioSeleccionado.getNumeroPredial().substring(0, 21));
				}

			}

			if (pPredioDireccionSeleccionado != null) {
				this.direccion = pPredioDireccionSeleccionado.getDireccion();

				if (ESiNo.SI.getCodigo().equals(pPredioDireccionSeleccionado.getPrincipal())) {
					this.principalBool = true;
				} else {
					this.principalBool = false;
				}

				this.codigoPostalDireccion = pPredioDireccionSeleccionado.getCodigoPostal();
			} else {
				this.direccion = "";
				this.principalBool = false;
				this.direccionNormalizada = "";
				this.normalizadaBool = false;
				this.editModeBool = false;
				this.referenciaCartograficaSeleccionada = new PReferenciaCartografica();
			}

		} else {

			this.direccion = "";
			this.principalBool = false;
			this.direccionNormalizada = "";
			this.normalizadaBool = false;
			this.editModeBool = false;
			this.referenciaCartograficaSeleccionada = new PReferenciaCartografica();
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que se ejecuta al cerrar los dialog asociados a la pantalla de
	 * gestionar ubicación del predio, con el fin de que se reinicien las variables
	 * en caso de que se cierren estos dialog sin realizar operaciones sobre la BD.
	 *
	 * @param event
	 */
	public void cerrar(org.primefaces.event.CloseEvent event) {
		reiniciarVariables();
	}

	// --------------------------------------------------- //
	/**
	 * Método que carga los datos de una matrícula inmobiliaria para su
	 * modificación.
	 *
	 * @author david.cifuentes
	 */
	public void cargarMatricula() {

		if (this.predioSeleccionado.getNumeroRegistro() != null) {
			numeroRegistroInfoMatriculaNueva = this.predioSeleccionado.getNumeroRegistro();
			this.matriculaVaciaBool = false;
		} else {
			numeroRegistroInfoMatriculaNueva = "";
			this.matriculaVaciaBool = true;
		}

		// Set del circulo registral
		if (this.predioSeleccionado.getCirculoRegistral() != null) {
			for (SelectItem i : itemListCirculosRegistrales) {
				if (this.predioSeleccionado.getCirculoRegistral().getCodigo() != null
						&& this.predioSeleccionado.getCirculoRegistral().getCodigo().equals(i.getValue())) {
					this.circuloRegistralSeleccionado = this.predioSeleccionado.getCirculoRegistral().getCodigo();
				}
			}
		}
	}

	// --------------------------------------------------- //
	/**
	 * Guarda la matrícula inmobiliaria actualizada
	 *
	 * @author david.cifuentes
	 */
	public void guardarMatriculaInmobiliaria() {

		try {
			if (this.numeroRegistroInfoMatriculaNueva != null && !this.numeroRegistroInfoMatriculaNueva.trim().isEmpty()
					&& this.circuloRegistralSeleccionado != null
					&& !this.circuloRegistralSeleccionado.trim().isEmpty()) {

				if (this.predioSeleccionado.getNumeroRegistro() != null) {
					this.predioSeleccionado.setNumeroRegistroAnterior(this.predioSeleccionado.getNumeroRegistro());
				}

				this.predioSeleccionado.setNumeroRegistro(this.numeroRegistroInfoMatriculaNueva);

				// Set del circulo registral
				if (this.circuloRegistralSeleccionado != null) {
					for (CirculoRegistral cr : listCirculosRegistrales) {
						if (this.circuloRegistralSeleccionado.equals(cr.getCodigo())) {
							this.predioSeleccionado.setCirculoRegistral(cr);
						}
					}
				}
				this.getConservacionService().guardarProyeccion(this.usuario, this.predioSeleccionado);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.debug("Error guardando la matricula inmobiliaria.");
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que elimina una {@link PReferenciaCartografica} seleccionada.
	 *
	 * @author david.cifuentes
	 */
	public void eliminarReferencia() {

		if (this.referenciaCartograficaSeleccionada != null) {

			this.predioSeleccionado.getPReferenciaCartograficas().remove(this.referenciaCartograficaSeleccionada);
			this.getConservacionService().eliminarReferenciaCartografica(this.referenciaCartograficaSeleccionada);
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que guarda una {@link PReferenciaCartografica} seleccionada.
	 *
	 * @author david.cifuentes
	 */
	public void guardarReferenciaCartografica() {

		if (this.referenciaCartograficaSeleccionada != null) {

			if (this.editModeBool) {
				this.predioSeleccionado.getPReferenciaCartograficas().remove(this.referenciaCartograficaSeleccionada);
			}

			this.referenciaCartograficaSeleccionada.setUsuarioLog(usuario.getLogin());
			this.referenciaCartograficaSeleccionada.setFechaLog(new Date());
			this.referenciaCartograficaSeleccionada.setPPredio(this.predioSeleccionado);

			if (this.referenciaCartograficaSeleccionada.getCancelaInscribe() == null
					|| this.referenciaCartograficaSeleccionada.getCancelaInscribe().trim().length() == 0
					|| this.referenciaCartograficaSeleccionada.getCancelaInscribe()
							.equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

				this.referenciaCartograficaSeleccionada
						.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
			}

			PReferenciaCartografica referencia = this.getConservacionService()
					.guardarPReferenciaCartografica(this.referenciaCartograficaSeleccionada);
			this.predioSeleccionado.getPReferenciaCartograficas().add(referencia);
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que busca el historico de avaluos hechos a un predio, para mutación de
	 * primera, segunda y quinta nueva, también consulta los títulos
	 * correspondientes a la justificación de propiedad.
	 *
	 * @author david.cifuentes
	 * @modified by leidy.gonzalez Se agrega validacion para que actualice la fecha
	 *           de inscripción catastral para los tramites de Quinta Omitido en la
	 *           Base de Datos
	 */
	public void buscarInscripcionesYDecretosAsociadosAlPredio() {

		this.tramiteSinTitulos = false;
		this.actualizacionEncontradaBool = false;

		List<String> fechasDisponibles = new ArrayList<String>();

		try {

			if (this.tramite.isPrimera() || this.tramite.isSegunda() || this.tramite.isQuintaNuevo()
					|| this.tramite.isQuintaOmitidoNuevo() || this.tramite.isQuintaOmitido()
					|| this.tramite.isQuintaMasivo() || this.tramite.isCancelacionMasiva()) {//

				// Se buscan los títulos correspondientes a los diferentes
				// documentos asociados en la justificación de propiedad
				// para este predio.
				this.fechasTitulos = new ArrayList<SelectItem>();
				SelectItem itemTitulo;

				List<PPersonaPredioPropiedad> personaPredioPropiedad = new ArrayList<PPersonaPredioPropiedad>();
				if (this.predioSeleccionado != null && this.predioSeleccionado.getPPersonaPredios() != null) {

					if (this.predioSeleccionado.getPPersonaPredios().size() > 0) {

						for (PPersonaPredio persPred : predioSeleccionado.getPPersonaPredios()) {
							for (PPersonaPredioPropiedad ppp : persPred.getPPersonaPredioPropiedads()) {

								// felipe.cadena::06-04-2016::#16717::Para los
								// tramites de quinta omitido no se valida el
								// estado del cancela/inscribe
								// ya que siempre se toman los datos ya
								// existentes del predio
								if (this.tramite.isQuintaOmitido()) {
									personaPredioPropiedad.add(ppp);
									continue;
								}

								// Tener en cuenta unicamente los
								// PPersonaPredioPropiedad
								// que se estén inscribiendo o modificando.
								if (ppp.getCancelaInscribe() != null && !ppp.getCancelaInscribe().trim().isEmpty()
										&& (ppp.getCancelaInscribe()
												.equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())
												|| ppp.getCancelaInscribe()
														.equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {

									personaPredioPropiedad.add(ppp);
								}
							}
						}

						if (!personaPredioPropiedad.isEmpty()) {

							String fecha;
							SimpleDateFormat formatoFecha = new SimpleDateFormat(Constantes.FORMATO_FECHA_CALENDAR);

							this.banderaAnioTituloEsAnioVigente = true;
							for (PPersonaPredioPropiedad ppp : personaPredioPropiedad) {
								if (ppp.getFechaTitulo() != null
										&& UtilidadesWeb.getAnioDeUnDate(ppp.getFechaTitulo()) != UtilidadesWeb
												.getAnioDeUnDate(new Date())) {
									this.banderaAnioTituloEsAnioVigente = false;
									break;
								}
							}

							for (PPersonaPredioPropiedad ppp : personaPredioPropiedad) {

								if ((ppp.getFechaTitulo() != null && ppp.getNumeroTitulo() != null)
										|| ppp.getTipoTitulo()
												.equals(EPersonaPredioPropiedadTipoTitulo.CARTA_VENTA.getValor())) {
									fecha = formatoFecha.format(ppp.getFechaTitulo());
									// felipe.cadena::03-10-2016::Se evita que
									// se agreguen fechas repetidas
									if (!fechasDisponibles.contains(fecha)) {
										fechasDisponibles.add(fecha);
									}
									// @modified by leidy.gonzalez:: #18225::
									// 06/07/2016 Se verifica que la fecha de
									// titulo
									// a visualizar sea la fecha de inscripcion
									// del predio seleccionado
									// if
									// (this.predioSeleccionado.getFechaInscripcionCatastral()
									// != null) {
									//
									// fecha =
									// formatoFecha.format(this.predioSeleccionado.getFechaInscripcionCatastral());
									// }

								}
							}

							for (String item : fechasDisponibles) {
								itemTitulo = new SelectItem();
								itemTitulo.setLabel(item);
								itemTitulo.setValue(item);
								this.fechasTitulos.add(itemTitulo);
							}
						} else {
							this.addMensajeError("El predio no tiene registros de títulos, "
									+ "por favor asocielos en la justificación de propiedad.");
							this.tramiteSinTitulos = true;
							return;
						}
					} else {
						this.addMensajeError("Por favor verifique que se hayan asociado "
								+ "los documentos justificativos en la justificación de propiedad.");
						this.tramiteSinTitulos = true;
						return;
					}
				}
			}

			if (this.tramite.isTercera() || this.tramite.isCuarta() || this.tramite.isQuintaOmitido()
					|| this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()) {

				boolean revisionAvaluoModifica = false;

				if (this.tramite.isRevisionAvaluo() && this.comisionesTramiteDato != null
						&& !this.comisionesTramiteDato.isEmpty()) {
					for (ComisionTramiteDato ctd : this.comisionesTramiteDato) {
						if (ctd.getModificado().equals(ESiNo.SI.getCodigo())) {
							revisionAvaluoModifica = true;
							break;
						}
					}
				}

				if (this.tramite.isTercera()
						|| (revisionAvaluoModifica && !this.tramite.isRevisionAvaluoAreaOUsoOPuntaje())) {
					fechaInscripcionCatastral = new Date();
				}

				if (this.tramite.isCuarta()) {
					SimpleDateFormat formatoFecha1 = new SimpleDateFormat("30/11/yyyy");
					fechaInscripcionCatastral = new Date();
					String strFecha = formatoFecha1.format(fechaInscripcionCatastral);
					SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
					try {
						fechaInscripcionCatastral = formatoDelTexto.parse(strFecha);

					} catch (ParseException ex) {
						LOGGER.error("Error haciendo parsing de la fecha");
					}
				}

				if (this.tramite.isQuintaOmitido() || this.tramite.isRectificacion()
						|| (revisionAvaluoModifica && this.tramite.isRevisionAvaluoAreaOUsoOPuntaje())) {

					if (this.tramite.getPredio() != null) {

						String zona;
						zona = this.tramite.getPredio().getNumeroPredial().substring(0, 7);

						fechaInscripcionCatastral = this.getConservacionService()
								.obtenerVigenciaUltimaActualizacionPorZona(zona);
						if (fechaInscripcionCatastral != null) {
							this.actualizacionEncontradaBool = true;
						} else {
							fechaInscripcionCatastral = null;
							this.addMensajeWarn(
									"No se encontró una fecha de actualización para este predio, por favor ingrésela.");
							this.actualizacionEncontradaBool = false;
						}
					} else {
						this.addMensajeWarn("No se encontró un predio asociado al trámite");
						this.actualizacionEncontradaBool = false;
					}

				}

				if (this.tramite.isRevisionAvaluo() && !revisionAvaluoModifica) {
					fechaInscripcionCatastral = null;
					this.actualizacionEncontradaBool = false;
				}

				// felipe.cadena::02/10/2015::#14436::Solo se asigna la fecha de
				// la actualizacion al predio cuando aun no se ha asignado una
				// fecha al predio
				if (this.predioSeleccionado.getFechaInscripcionCatastral() == null) {
					this.predioSeleccionado.setFechaInscripcionCatastral(fechaInscripcionCatastral);
					this.getConservacionService().guardarActualizarPPredio(this.predioSeleccionado);
				}

			}

			this.mostrarFechaTituloEnGestionarInscripcionesYDecretos();

			this.pPrediosAvaluosCatastrales = this.getFormacionService()
					.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que valida la fecha de inscripción catastral antes de recalcular los
	 * avaluos.
	 *
	 * @author david.cifuentes
	 *
	 * @modified by javier.aponte 21/02/2014
	 *
	 */
	public boolean validateFechaInscripcion() {

		if (this.predioSeleccionado == null) {
			this.addMensajeError("No existe un predio seleccionado.");
			return false;
		} else if (this.predioSeleccionado.getFechaInscripcionCatastral() == null) {
			this.addMensajeError("Debe diligenciar la fecha de inscripción catastral.");
			return false;
		}

		if (this.tramite.isRectificacionCancelacionDobleInscripcion()) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, -1);
			Calendar c2 = Calendar.getInstance();
			c2.setTime(this.predioSeleccionado.getFechaInscripcionCatastral());
			if (c.before(c2)) {
				this.addMensajeError("La fecha de inscripción debe ser inferior al día de hoy.");
				return false;
			}
			if (this.fechaInscripcionCatastral != null) {
				Calendar c3 = Calendar.getInstance();
				c3.setTime(this.fechaInscripcionCatastral);
				if (c2.before(c3)) {
					this.addMensajeError("La fecha debe ser inferior al día de hoy y mayor a la última actualización.");
					return false;
				}
			}
		} else {
			Calendar fechaAvaluo = Calendar.getInstance();
			fechaAvaluo.setTime(this.predioSeleccionado.getFechaInscripcionCatastral());

			if (fechaAvaluo.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
				this.addMensajeInfo("No se puede realizar el cálculo de avalúos para un año vigente.");
				this.pPrediosAvaluosCatastrales = null;
				return true;
			}
		}
		return true;
	}

	// --------------------------------------------------- //
	/**
	 * Método que hace un recálculo de los avaluos de un predio
	 *
	 * @author david.cifuentes
	 */
	@SuppressWarnings("unchecked")
	public void procesarInscripcionesYDecretos() {

		PPredioAvaluoCatastral avaluoVigenciaActual = null;
		List<PPredioZona> zonas = null;

		if (this.tramite.isTramiteGeografico() && !this.tramite.tieneReplica() && !this.banderaPredioFiscal) {
			this.addMensajeError("Como es un trámite geográfico, antes se requiere "
					+ "hacer la modificación geográfica para poder liquidar el avalúo");
			return;
		}

		if (this.tramite.isRectificacion()) {
			int diasDiferencia = UtilidadesWeb.calcularNumeroDiasEntreFechas(new Date(),
					this.predioSeleccionado.getFechaInscripcionCatastral());
			if (diasDiferencia > 3) {
				this.addMensajeError("La fecha de inscripción catastral no puede "
						+ "ser mayor a 3 días respecto a la fecha actual.");
				return;
			}
		}

		if (this.tramite.isModificacionInscripcionCatastral()) {
			if (this.predioSeleccionado.getFechaInscripcionCatastral().compareTo(new Date()) > 0) {
				this.addMensajeError(
						"La fecha de inscripción catastral " + "debe ser menor o igual a la fecha del sistema.");
				return;
			}
		}

		// @modified by leidy.gonzalez::26867::04/08/2017:: Valida que La sumatoria de
		// las áreas de
		// terreno de las zonas para la vigencia registrada es mayor que sumatoria de
		// las áreas de
		// terreno de las zonas calculadas para el trámite (vigencia en que se está
		// realizando el trámite)
		if (this.tramite.isEnglobeVirtual()) {
			if (this.predioSeleccionado.getFechaInscripcionCatastral() != null) {

				Double sumatoriaAreaZonasVigencia = 0.0;
				Double sumatoriaAreaZonasPredio = 0.0;

				zonas = this.getConservacionService().buscarZonasVigentes(this.predioSeleccionado.getId());

				List<PPredioZona> zonasPorPPredioVigencia = this.getConservacionService()
						.buscarZonasPorPPredioIdYVigencia(this.predioSeleccionado.getId(),
								this.predioSeleccionado.getFechaInscripcionCatastral());

				// Sumatoria de areas de la vigencia: fecha de Inscripcion
				if (zonasPorPPredioVigencia != null) {

					for (PPredioZona pPredioZona : zonasPorPPredioVigencia) {

						if (pPredioZona.getCancelaInscribe() != null && !EProyeccionCancelaInscribe.CANCELA.getCodigo()
								.equals(pPredioZona.getCancelaInscribe())) {

							sumatoriaAreaZonasVigencia += sumatoriaAreaZonasVigencia + pPredioZona.getArea();

						} else if (pPredioZona.getCancelaInscribe() == null) {

							sumatoriaAreaZonasVigencia += sumatoriaAreaZonasVigencia + pPredioZona.getArea();
						}
					}
				}

				// Sumatoria de areas diferentes de la vigencia: fecha de Inscripcion
				if (zonas != null) {

					for (PPredioZona pPredioZonaDifFechaInsc : zonas) {

						if (pPredioZonaDifFechaInsc.getVigencia() != null && !this.predioSeleccionado
								.getFechaInscripcionCatastral().equals(pPredioZonaDifFechaInsc.getVigencia())) {

							if (pPredioZonaDifFechaInsc.getCancelaInscribe() != null
									&& !EProyeccionCancelaInscribe.CANCELA.getCodigo()
											.equals(pPredioZonaDifFechaInsc.getCancelaInscribe())) {

								sumatoriaAreaZonasPredio += sumatoriaAreaZonasPredio
										+ pPredioZonaDifFechaInsc.getArea();

							} else if (pPredioZonaDifFechaInsc.getCancelaInscribe() == null) {

								sumatoriaAreaZonasPredio += sumatoriaAreaZonasPredio
										+ pPredioZonaDifFechaInsc.getArea();
							}

						}
					}
				}

				if (sumatoriaAreaZonasPredio > 0 && sumatoriaAreaZonasVigencia > 0
						&& sumatoriaAreaZonasPredio > sumatoriaAreaZonasVigencia) {

					this.addMensajeError("La sumatoria de las  áreas de terreno de las zonas registradas "
							+ "para la vigencia:  " + this.predioSeleccionado.getFechaInscripcionCatastral()
							+ " es mayor a la sumatoria de las áreas de terreno de las zonas registradas para la vigencia actual");
					return;
				}

			} else {
				this.addMensajeError("La fecha de inscripción catastral "
						+ "es un campo obligatorio para la liquidacion de avaluos, por favor verfique.");
				return;
			}
		}

		String mensajeError = "";
		try {
			if (validateFechaInscripcion()) {
				// leidy.gonzalez:: #17932:: 15/06/2016:: Se desactiva varible
				// ficha matriz
				if (this.tramite.isEnglobeVirtual()) {
					this.predioSeleccionado.setPPredioZonas(zonas);
					this.predioSeleccionado.getPPredioAvaluoCatastrals().clear();
				}

				this.predioSeleccionado = this.getConservacionService()
						.guardarActualizarPPredio(this.predioSeleccionado);

				Date fechaDelCalculo = this.predioSeleccionado.getFechaInscripcionCatastral();

				// felipe.cadena :: #9275 :: manejo de avaluos para englobes
				if (this.tramite.isSegundaEnglobe()) {
					Calendar vigenciaActual = Calendar.getInstance();

					for (PPredioAvaluoCatastral pac : this.predioSeleccionado.getPPredioAvaluoCatastrals()) {
						Calendar vigenciaPac = Calendar.getInstance();
						vigenciaPac.setTime(pac.getVigencia());
						if (vigenciaPac.get(Calendar.YEAR) == vigenciaActual.get(Calendar.YEAR)) {

							avaluoVigenciaActual = pac;
						}
					}
					this.getFormacionService().revertirProyeccionAvaluosParaUnPredio(this.tramite.getId(),
							this.predioSeleccionado.getId());
				}

				Object[] errors = this.getFormacionService().liquidarAvaluosParaUnPredio(this.tramite.getId(),
						this.predioSeleccionado.getId(), fechaDelCalculo, this.usuario);

				if (errors == null) {

					this.addMensajeInfo("Se procesó correctamente el cálculo de los avalúos para este predio.");
				} else if (errors.length > 0) {
					for (int i = 0; i < errors.length; i++) {
						if (((ArrayList<Object>) errors[i]).isEmpty()) {
							this.addMensajeInfo("Se procesó correctamente el cálculo de los avalúos para este predio.");
							break;
						} else {

							int numeroErrores = 0;
							String mensajes = "";

							for (Object errorI : ((ArrayList<Object>) errors[i])) {
								if (((Object[]) errorI)[0].equals("0")) {
									mensajes += "Nota: ";
									// mensajes += (((Object[])errorI)[0] !=
									// null)
									// ?((Object[])errorI)[0].toString():"";
									mensajes += (((Object[]) errorI)[1] != null) ? ((Object[]) errorI)[1].toString()
											: "";
									mensajes += (((Object[]) errorI)[2] != null) ? ((Object[]) errorI)[2].toString()
											: "";
									mensajes += (((Object[]) errorI)[3] != null) ? ((Object[]) errorI)[3].toString()
											: "";

								} else {

									mensajes += "Error: ";
									mensajeError += (((Object[]) errorI)[0] != null) ? ((Object[]) errorI)[0].toString()
											: "";
									mensajeError += (((Object[]) errorI)[1] != null) ? ((Object[]) errorI)[1].toString()
											: "";
									mensajeError += (((Object[]) errorI)[2] != null) ? ((Object[]) errorI)[2].toString()
											: "";
									mensajeError += (((Object[]) errorI)[3] != null) ? ((Object[]) errorI)[3].toString()
											: "";

									if (!this.tramite.isQuintaMasivo()) {
										numeroErrores++;
									} else if (this.tramite.isQuintaMasivo()
											&& "FICHA_MATRIZ".equals(((Object[]) errorI)[2].toString())) {
										mensajeError = "";
									}
								}
							}

							if (numeroErrores == 0) {
								this.addMensajeInfo(
										"Se procesó correctamente el cálculo de los avalúos para este predio.");
								LOGGER.debug(mensajes);
							} else {
								this.addMensajeError("Error al procesar el cálculo de los avaluos para este predio.");
								this.addMensajeError(mensajeError);
								LOGGER.error(mensajeError);
								return;
							}

						}
					}

					this.pPrediosAvaluosCatastrales = this.getFormacionService()
							.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());

					for (VPPredioAvaluoDecreto vppad : this.pPrediosAvaluosCatastrales) {
						System.out.println("DEBUG FGWL ************* " + vppad.getCancelaInscribe());
					}
				}
			}

			// #11261 david.cifuentes Se habilita el registro manual de avaluos
			// para quinta nuevo cuando la fecha de inscripción es menor a la
			// fecha de actualización
			if (this.tramite.isQuintaNuevo() && this.predioSeleccionado != null
					&& this.predioSeleccionado.getFechaInscripcionCatastral() != null
					&& this.validarYCargarAniosParaCalculoManualQuintaNuevo()) {
				this.crearAvaluosParaCalculoManualQuintaNuevo();
			}

			// felipe.cadena :: #9275 :: manejo de avaluos para englobes
			if (avaluoVigenciaActual != null) {

				Calendar vigenciaActual = Calendar.getInstance();
				PPredioAvaluoCatastral avaluoGenerado = null;

				for (PPredioAvaluoCatastral pac : this.predioSeleccionado.getPPredioAvaluoCatastrals()) {
					Calendar vigenciaPac = Calendar.getInstance();
					vigenciaPac.setTime(pac.getVigencia());
					if (vigenciaPac.get(Calendar.YEAR) == vigenciaActual.get(Calendar.YEAR)) {

						avaluoGenerado = pac;
					}
				}

				if (avaluoGenerado == null) {
					avaluoVigenciaActual = this.getConservacionService()
							.actualizarPPredioAvaluoCatastral(avaluoVigenciaActual);
				} else {
					avaluoGenerado = this.getConservacionService().actualizarPPredioAvaluoCatastral(avaluoGenerado);
				}

				this.pPrediosAvaluosCatastrales = this.getFormacionService()
						.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());

			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al procesar el cálculo de los avaluos para este predio.");
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que realiza un llamado al método que recalcula los avaluos de los
	 * predios.
	 */
	public void procesarInscripcionesYDecretosTodos() {
		// PPredio seleccionadoOriginalmente = this.predioSeleccionado;
		Long idPredioSeleccionadoOriginalmente = this.predioSeleccionado.getId();
		List<PPredio> prediosProcesar = null;
		if (this.tramite.isQuintaMasivo()) {
			prediosProcesar = this.getConservacionService()
					.buscarPPrediosCompletosPorTramiteIdPHCondominio(this.tramite.getId());
		} else {
			prediosProcesar = entradaProyeccionMB.getPrediosResultantes();
		}

		for (PPredio p : prediosProcesar) {
			this.predioSeleccionado = p;
			this.procesarInscripcionesYDecretos();
		}

		for (PPredio p : prediosProcesar) {
			if (idPredioSeleccionadoOriginalmente.longValue() == p.getId().longValue()) {
				this.predioSeleccionado = p;
				this.procesarInscripcionesYDecretos();
			}
		}

	}

	/**
	 * Método que realiza ciertas validaciones acerca de la fehca de inscripción
	 * catastral y guarda los avaluos recalculados.
	 */
	public void guardarInscripcionesYDecretos() {
		
		Date nuevaFecha = this.predioSeleccionado.getFechaInscripcionCatastral();
		LOGGER.debug("guardarInscripcionesYDecretos : +++++++++++++++++++++++++++++++++: "+nuevaFecha);
		try {
		if (this.tramite.isRectificacion()) {
			int diasDiferencia = UtilidadesWeb.calcularNumeroDiasEntreFechas(new Date(),
					this.predioSeleccionado.getFechaInscripcionCatastral());
			if (diasDiferencia > 3) {
				this.addMensajeError("La fecha de inscripción catastral no puede "
						+ "ser mayor a 3 días respecto a la fecha actual.");
				return;
			}
		}

		// felipe.cadena::01/10/2015::#14443::Se debe volver al editor si la
		// fecha
		// de inscripcion catastral se ha modificado para tramites de
		// rectificacion de area
		// y de area de construccion.
		if (this.tramite.isRectificacionArea() || this.tramite.isRectificacionAreaConstruccion()) {
			PPredio predioBD = this.getConservacionService()
					.getPPredioFetchPPersonaPredioPorId(this.predioSeleccionado.getId());
			Date fechaActual = predioBD.getFechaInscripcionCatastral();
			if (this.entradaProyeccionMB == null) {
				this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
			}
			if(this.entradaProyeccionMB.getPrediosResultantes()!=null)
			for (PPredio pPredio : this.entradaProyeccionMB.getPrediosResultantes()) {
				if(!pPredio.getId().equals(this.predioSeleccionado.getId())) {
				 pPredio.setFechaInscripcionCatastral(nuevaFecha);
				 pPredio.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				   this.getConservacionService().guardarPPredio(pPredio);
				}
			}
			
			
			if (!fechaActual.equals(nuevaFecha)) {
				this.obligarGeografico();
				//this.validarGuardar();
			}
			
		}
		
	
			if (this.predioSeleccionado.getFechaInscripcionCatastral().after(new Date())) {
				
				this.addMensajeError(
						"La fecha de inscripción debe corresponder a la fecha de expedición del acto administrativo y no puede ser superior a hoy.");
				LOGGER.debug("La fecha de inscripción debe corresponder a la fecha de expedición del acto administrativo y no puede ser superior a hoy.");
				
				return;
			}

			// felipe.cadena::06-08-2015::#13359::Se regsitra la fecha de
			// inscripcion para los predios de una rectificacion de area con
			// linderos
			if (this.tramite.isRectificacionArea() && !this.tramite.isRectificacionMatriz()) {
				if (!this.registrarFechaRectificacionAreaLinderos()) {
					return;
				}
			}
			
			// felipe.cadena::DESENGLOBE_ETAPAS::para los desenglobes de etapas
			// se copia la fecha de inscripcion a todos los predios que no la
			// tengan aun
			if (this.tramite.isDesenglobeEtapas() && !this.tramite.isEnglobeVirtual()) {
				List<PPredio> prediosTramite = this.getConservacionService()
						.buscarPPrediosPorTramiteId(this.tramite.getId());
				for (PPredio pp : prediosTramite) {
					if (pp.getFechaInscripcionCatastral() == null) {
						pp.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
						pp = this.getConservacionService().actualizarPPredio(pp);
					}
				}
			} else if (this.tramite.isEnglobeVirtual()) {// para englobe vitual se modifican todas las fechas de los
															// predios activos.
				List<PPredio> prediosTramite = this.getConservacionService()
						.buscarPPrediosPorTramiteId(this.tramite.getId());
				for (PPredio pp : prediosTramite) {
					pp.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
				}
				this.getConservacionService().guardarPPredios(prediosTramite);
			}
			
			// felipe.cadena::rectificacion matriz && cancelacion masiva::
			// se copia la fecha de inscripcion a todos los predios que no la
			// tengan aun
			if (this.tramite.isRectificacionMatriz() || this.tramite.isCancelacionMasiva()) {
				List<PPredio> prediosTramite = this.getConservacionService()
						.buscarPPrediosPorTramiteId(this.tramite.getId());

				for (PPredio pp : prediosTramite) {
					pp.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
					pp = this.getConservacionService().actualizarPPredio(pp);
				}
			}

			// QUINTAS MASIVAS
			// Se copia la fecha de inscripcion a todos los predios que no la
			// tengan aun
			if (this.tramite.isQuintaMasivo()) {
				this.cargarPrediosQuintaMasivo();
				if (this.pPrediosQuintaMasivo != null) {
					for (PPredio pp : this.pPrediosQuintaMasivo) {
						if (pp.getFechaInscripcionCatastral() == null) {
							pp.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
							pp = this.getConservacionService().actualizarPPredio(pp);
						}
					}
				}
			}
				
			// Se consultan nuevamente los avalúos actualizados del predio.
			List<PPredioAvaluoCatastral> avaluosActualizadosPPredio = this.getConservacionService()
					.obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());
			this.predioSeleccionado.setPPredioAvaluoCatastrals(avaluosActualizadosPPredio);
			this.getConservacionService().guardarProyeccion(this.usuario, this.predioSeleccionado);
			
			this.addMensajeInfo("Se guardaron los cambios al predio satisfactoriamente.");
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getStackTrace().toString());
			this.addMensajeError("Error al asociar la observación del recálculo. ");
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que verifica en los estados del trámite si el trámites es rechazado.
	 *
	 * @return
	 */
	public boolean isRechazado() {
		if (this.tramite != null) {
			if (this.tramite.getEstado().equals(ETramiteEstado.RECHAZADO.getCodigo())) {
				return true;
			}
		}
		return false;
	}

	// --------------------------------------------------- //
	/**
	 * Método que realiza un llamado al método de validación de las diferentes
	 * secciones de la proyeción y de resultar válidas, avanza el proceso al
	 * coordinador.
	 *
	 * @return
	 */
	/*
	 * @modified by Se valida que los tramites de complementacion, modificacion de
	 * inscripcion catastral, revisión de avaluo, tercera, cancelación de predio y
	 * rectificaciones de construcción y de terreno que no tengan fecha de
	 * inscripcion catastral no se pedan enviar al coordinador.
	 */
	public String enviarACoordinador() {

		// felipe.cadena::09-11-2016::valida si se cumplen las restricciones asociadas a
		// la cancelacion masiva
		if (this.tramite.isCancelacionMasiva() && !this.entradaProyeccionMB.isBanderaCoordinador()) {
			this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_008.getMensajeUsuario());
			LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_008.getMensajeTecnico());
			return null;
		}

		// felipe.cadena::17-05-2017::Guardar estado ficha y complementos de ficha para
		// cancelaciones
		if (this.tramite.isCancelacionMasiva()) {
			this.getFichaMatrizSeleccionada().setEstado(EPredioEstado.CANCELADO.getCodigo());
			this.getFichaMatrizSeleccionada().setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());

			for (PFichaMatrizTorre pft : this.getFichaMatrizSeleccionada().getPFichaMatrizTorres()) {
				pft.setEstado(EPredioEstado.CANCELADO.getCodigo());
				pft.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
			}

			for (PFichaMatrizPredio pfp : this.getFichaMatrizSeleccionada().getPFichaMatrizPredios()) {
				pfp.setEstado(EPredioEstado.CANCELADO.getCodigo());
				pfp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
			}

			for (PFichaMatrizModelo pfm : this.getFichaMatrizSeleccionada().getPFichaMatrizModelos()) {
				pfm.setEstado(EPredioEstado.CANCELADO.getCodigo());
				pfm.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
			}

			this.getConservacionService().actualizarFichaMatriz(this.getFichaMatrizSeleccionada());
		}

		// felipe.cadena::#11867::Se valida la existencia de la replica final de
		// edicion.
		GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
		if (!this.banderaPredioFiscal) {
			if (!generalMB.validarExistenciaCopiaFinalEdicion(this.tramite)) {
				this.addMensajeError(
						"Este trámite no tiene replica que muestre las modificaciones geográficas realizadas en la depuración. Codigo <ERFXXX> ");
				return null;
			}
		}

		this.validarGuardar();

		if (this.tramite.isEsComplementacion() || this.tramite.isModificacionInscripcionCatastral()
				|| this.tramite.isRevisionAvaluo() || (this.tramite.isTercera() && !this.tramite.isTerceraMasiva())) {
			if (this.predioSeleccionado.getFechaInscripcionCatastral() == null) {
				this.addMensajeError(
						"No se puede enviar al coordinador debido a que hace falta el ingreso de la fecha de inscripción catastral, por favor genere y guarde Texto Motivado. Y despues Valide y Guarde");
				return null;
			}

		}

		// felipe.cadena::01-07-2015::Rectificacion Masiva::Se determinan los
		// predios modificados para rectificaciones masivas
		if (!this.tramite.isRectificacionMatriz()) {
			if ((this.tramite.isRectificacion() && (!this.tramite.isRectificacionGeograficaAreaConstruccion()
					|| !this.tramite.isRectificacionGeograficaAreaTerreno())) ||

					this.tramite.isCancelacionPredio()) {
				if (this.predioSeleccionado != null && this.predioSeleccionado.getFechaInscripcionCatastral() == null) {

					this.addMensajeError(
							"No se puede enviar al coordinador debido a que hace falta el ingreso de la fecha de inscripción catastral, por favor genere Inscripciones/Decretos y guarde. Y despues Valide y Guarde");
					return null;
				}
			}
		} else {
			// felipe.cadena::rectificacion matriz::validacion fecha inscripcion
			// catastral
			if (this.tramite.isRectificacionMatriz()) {
				if (!this.validarFechasRectificacionMatriz()) {
					return null;
				}
			}
		}

		if (!this.isTodasSeccionesValidas()) {
			this.addMensajeError("El trámite no es enviado al coordinador, mientras no sea válido");
			return null;
		}

		// Se consolidan los avaluos manuales
		this.consolidarProyeccionAvaluosManuales();

		// felipe.cadena::01-07-2015::Rectificacion Masiva::Se determinan los
		// predios modificados para rectificaciones masivas
		if (this.tramite.isRectificacionMatriz()) {
			this.determinarPrediosModificadosRectificacion();
		}

		// javier.aponte::06-07-2015::Tercera Másiva::Se determinan los predios
		// modificados para trámites de tercera másivos
		if (this.tramite.isTerceraMasiva()) {
			this.determinarPrediosModificadosTerceraMasiva();
		}

		// lorena.salamanca :: 24-09-2015 :: En quinta masivo se cambia el
		// estado de los predios reactivados por activos. Solo aplica para
		// Quinta Omitido.
		if (this.tramite.isQuintaMasivo()) {
			FichaMatriz fichaMatriz = this.getConservacionService()
					.buscarFichaMatrizPorPredioId(this.tramite.getPredio().getId());
			List<PFichaMatrizPredio> pfmpList = this.getConservacionService()
					.buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(EPredioEstado.REACTIVADO.getCodigo(),
							fichaMatriz.getId());
			for (PFichaMatrizPredio pfmp : pfmpList) {
				pfmp.setEstado(EPredioEstado.ACTIVO.getCodigo());
			}
			this.getConservacionService().guardarPrediosFichaMatriz(pfmpList);
		}

		// Validación de valores cancela inscribe de la proyección
		this.validacionProyeccionService.validarCancelaInscribeProyeccion(this.tramite, this.usuario);

		// leidy.gonzalez::18/07/2018::#62890
		// Valida que no se vayan zonas creadas en M
		/*
		 * if (this.tramite.isSegundaDesenglobe()) {
		 * this.compararZonasFirmeYProyectadas(); }
		 */

		// leidy.gonzalez::25/05/2018::#62730
		// Valida que no se vayan construcciones creadas en M
		if ((this.tramite.isTercera() && !this.tramite.isTerceraMasiva())
				|| (this.tramite.isQuinta() && !this.tramite.isQuintaMasivo())) {
			this.compararConstruccionesFirmeYProyectadas();
		}

		this.getConservacionService().enviarACoordinador(this.actividad, this.usuario);
		tareasPendientesMB.init();
		UtilidadesWeb.removerManagedBean("proyectarConservacion");
		return "index";
	}

	/**
	 * Se consolidan los avaluos que se ingresaron de manera manual para la
	 * proyeccion
	 *
	 * @author felipe.cadena
	 */
	private void consolidarProyeccionAvaluosManuales() {

		if (this.predioSeleccionado != null) {
			this.pPrediosAvaluosCatastrales = this.getFormacionService()
					.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());

			List<PPredioAvaluoCatastral> ppacs = this.getConservacionService()
					.obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());
			if (ppacs != null) {
				for (PPredioAvaluoCatastral ppac : ppacs) {
					if (ppac.getCancelaInscribe() != null
							&& ppac.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo())) {

						// Verificar la existencia del avalúo
						PredioAvaluoCatastral pac = this.getConservacionService()
								.getPredioAvaluoCatastralById(ppac.getId());
						if (pac != null) {
							ppac.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
						} else {
							ppac.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
						}
						this.getConservacionService().actualizarPPredioAvaluoCatastral(ppac);
					}
				}
			}
		}

		List<PPredio> prediosProyeccion = this.getConservacionService()
				.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());

		for (PPredio pp : prediosProyeccion) {

			List<PPredioAvaluoCatastral> avaluosActualizadosPPredio = this.getConservacionService()
					.obtenerListaPPredioAvaluoCatastralPorIdPPredio(pp.getId());
			if (avaluosActualizadosPPredio != null) {
				for (PPredioAvaluoCatastral ppac : avaluosActualizadosPPredio) {
					if (ppac.getCancelaInscribe() != null
							&& ppac.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo())) {
						ppac.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
						this.getConservacionService().actualizarPPredioAvaluoCatastral(ppac);
					}
				}
			}
		}

	}

	// --------------------------------------------------- //
	/**
	 * Método que avanza el proceso la actividad Generar resolución.
	 *
	 * @modified david.cifuentes :: #11987 - CC-NP-CO-071 :: Se adiciona el llamado
	 *           a la validación de cancela/inscribe de una proyección, se realiza
	 *           únicamente desde este punto debido a que es el último paso en
	 *           modificar información alfanumérica en la que se pueden modificar
	 *           los valores C/I de la proyección :: 24/04/2015
	 * @return
	 */
	public String enviarAGenerarResolucion() {

		// Validación de valores cancela inscribe de la proyección
		this.validacionProyeccionService.validarCancelaInscribeProyeccion(this.tramite, this.usuario);

		this.getConservacionService().enviarAGenerarResolucion(this.actividad, this.usuario);

		this.tareasPendientesMB.init();

		UtilidadesWeb.removerManagedBean("proyectarConservacion");
		return "index";
	}

	// --------------------------------------------------- //
	public boolean isExisteTablaLiquidacion() {

		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, 1);
		c.set(Calendar.MONTH, 0);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date vigencia = new Date(c.getTimeInMillis());

		if (this.tramite.getPredio() != null && !this.getTramite().getPredio().isMejora()
				&& !this.getTramite().isQuinta()) {
			String zona = this.getTramite().getPredio().getMunicipio().getCodigo()
					+ this.getTramite().getPredio().getZona();
			List<TablaTerreno> tabla = this.getFormacionService().findByZonaVigencia(zona, vigencia);
			if (tabla == null || tabla.isEmpty()) {
				this.addMensajeError("No existe la tabla de liquidación para la zona " + zona + " y vigencia "
						+ c.get(Calendar.YEAR));
				return false;
			}
		}
		if (this.getTramite().isQuinta()) {
			if (this.entradaProyeccionMB == null) {
				this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
			}
			if (this.entradaProyeccionMB != null) {
				if (this.entradaProyeccionMB.getPrediosResultantes() == null
						|| this.entradaProyeccionMB.getPrediosResultantes().size() == 0) {
					this.addMensajeError("Debe existir al menos un predio resultante");
					return false;
				}
				String zona = this.entradaProyeccionMB.getPrediosResultantes().get(0).getMunicipio().getCodigo()
						+ this.entradaProyeccionMB.getPrediosResultantes().get(0).getZona();
				List<TablaTerreno> tabla = this.getFormacionService().findByZonaVigencia(zona, vigencia);
				if (tabla == null || tabla.size() == 0) {
					this.addMensajeError("No existe la tabla de liquidación para la zona " + zona + " y vigencia "
							+ c.get(Calendar.YEAR));
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	// --------------------------------------------------- //
	/**
	 * Método que verifica si el trámite debe ser enviado a la geográfica, de ser
	 * así avanza el proceso a ésta actividad.
	 *
	 * @modified by leidy.gonzalez Se agrego validacion para verificar que el
	 *           tramite se enviara a modificar informacion geografica cuando en la
	 *           tabla PPredio exista la fecha de inscripcion para tramites de
	 *           Quinta nuevo, nuevo omitido y Segunda Englobe
	 */
	/*
	 * @modified by leidy.gonzalez 22/10/2014 Se valida que no permita enviar a la
	 * geografica tramites de rectificación de terreno y construcción, de
	 * cancelacion de predio, teycera y complementación que no tengan fechas de
	 * inscripcion catastral
	 */
	/*
	 * @modified by leidy.gonzalez::#17785::08/06/2016:: Se ajusta validacion para
	 * insertar los datos en la tabla: TRAMITE_DIGITALIZACION
	 */
	public String enviarAModificarInfoGeografica() {

		try {
			// validacion de coeficientes antes de enviar a geografico
			if (this.tramite.isRectificacionMatriz() || this.tramite.isDesenglobeEtapas()
					|| this.tramite.isQuintaMasivo()) {
				if (!this.validarCoeficientes(this.tramite.getPredio().getId(), true)) {
					return null;
				}
			} else if (this.tramite.isTipoInscripcionPH() || this.tramite.isTipoInscripcionCondominio()) {
				if (!this.validarCoeficientes(this.fichaMatrizSeleccionada.getPPredio().getId(), true)) {
					return null;
				}
			}

			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
			// validacion de manzanas cuando se crean manzanas
			if (this.entradaProyeccionMB.isCreaManzanas()) {
				if (!this.entradaProyeccionMB.validarPrediosManzanas()) {
					return null;
				}
			}

			if ((this.tramite.isRectificacionGeograficaAreaTerreno()
					|| this.tramite.isRectificacionGeograficaAreaConstruccion())
					&& !this.tramite.isRectificacionMatriz()) {
				if (this.predioSeleccionado != null) {
					if (this.predioSeleccionado.getFechaInscripcionCatastral() == null) {
						this.addMensajeError(
								"No se puede enviar a modificar información geografica debido a que hace falta el ingreso de la fecha de inscripción catastral");
						return null;
					}
				}
			}

			if (this.tramite.isSegunda() || this.tramite.isQuintaNuevo() || this.tramite.isQuintaOmitidoNuevo()
					|| this.tramite.isCancelacionPredio() || this.tramite.isEsComplementacion()) {
				if (entradaProyeccionMB.getPrediosResultantes() != null) {
					if (!this.validarFechasInscripcion()) {
						return null;
					}
				}
			}

			// felipe.cadena::rectificacion matriz::validacion fecha inscripcion catastral
			if (this.tramite.isRectificacionMatriz()) {
				if (!this.validarFechasRectificacionMatriz()) {
					return null;
				}
			}

			// #13500 :: david.cifuentes :: Asociado a #11563
			if (this.predioSeleccionado != null && this.predioSeleccionado.getFechaInscripcionCatastral() == null
					&& this.tramite.isTercera()) {
				this.predioSeleccionado.setFechaInscripcionCatastral(new Date(System.currentTimeMillis()));
			}

			GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
			if (!generalMB.validarEnvioModificacionGeografica(this.tramite)) {
				return "";
			}

			// Se requiere que si las construcciones tengan el estado en CANCELA,
			// lleven fechas de inscripción catastral, así como que el predio para
			// las mutaciones de tercera tenga en su estado cancelaInscribe el valor
			// MODIFICA
			if (this.tramite.isTercera() && !this.tramite.isTerceraMasiva()) {
				this.validacionesMutacionTercera();
				// felipe.cadena:: #9624 :: 30/09/2014 ::se ingresa el año de cancelacion para
				// las construcciones canceladas
				if (!this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
					for (PUnidadConstruccion pu : this.predioSeleccionado.getPUnidadConstruccions()) {
						if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pu.getCancelaInscribe())) {

							Calendar fechaActual = Calendar.getInstance();
							int year = fechaActual.get(Calendar.YEAR);
							pu.setAnioCancelacion(year);

						}
					}
				}
				this.predioSeleccionado = this.getConservacionService()
						.guardarActualizarPPredio(this.predioSeleccionado);

				// se calcula el avaluo del predio
				Object[] errores = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(this.tramite.getId(),
						this.predioSeleccionado.getId(), new Date(), this.usuario);
				if (Utilidades.hayErrorEnEjecucionSP(errores)) {
					LOGGER.error("Error al calcular el avaluo para el tramite de tercera :" + tramite.getId());
				}
			}

			// Se requiere que si las construcciones tengan el estado en CANCELA,
			// lleven fechas de inscripción catastral, así como que el predio para
			// las mutaciones de tercera tenga en su estado cancelaInscribe el valor
			// MODIFICA
			if (this.tramite.isTerceraMasiva()) {

				// Si las PUnidadConstruccion están como canceladas, en el campo
				// "FECHA_INSCRIPCION_CATASTRAL" se les debe asignar la fecha del
				// sistema.
				if (this.pPrediosTerceraRectificacionMasiva != null
						&& !this.pPrediosTerceraRectificacionMasiva.isEmpty()) {
					Date fechaActual = new Date(System.currentTimeMillis());

					for (PPredio predioTemp : this.pPrediosTerceraRectificacionMasiva) {

						if (!predioTemp.getPUnidadConstruccions().isEmpty()) {

							for (PUnidadConstruccion pu : predioTemp.getPUnidadConstruccions()) {

								if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pu.getCancelaInscribe())) {

									pu.setFechaInscripcionCatastral(fechaActual);

									// felipe.cadena:: 30/09/2014 ::se debe asignar el año de cancelacion.
									int year = Calendar.getInstance().get(Calendar.YEAR);
									pu.setAnioCancelacion(year);

								}

							}

							predioTemp = this.getConservacionService().guardarActualizarPPredio(predioTemp);

						}

						// se calcula el avaluo del predio
						Object[] errores = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(
								this.tramite.getId(), predioTemp.getId(), fechaActual, this.usuario);

						if (Utilidades.hayErrorEnEjecucionSP(errores)) {
							LOGGER.error(
									"Error al calcular el avaluo para el predio :" + predioTemp.getNumeroPredial());
						}

					}
				}
			}

			// lorena.salamanca :: 08-09-2015 :: Validaciones para Quinta Masivo. Si no
			// tiene fecha de inscripcion catastral para cada predio no se deja mover.
			if (this.tramite.isQuintaMasivo()) {
				this.cargarPrediosQuintaMasivo();
				if (this.pPrediosQuintaMasivo != null || !this.pPrediosQuintaMasivo.isEmpty()) {
					for (PPredio pp : this.pPrediosQuintaMasivo) {
						if (pp.getFechaInscripcionCatastral() == null) {
							this.addMensajeError(
									"No se puede enviar a modificar información geografica debido a que hace falta el ingreso de la fecha de inscripción catastral, por favor genere Inscripciones/Decretos y guarde. Y despues Valide y Guarde");
							return null;
						}

					}
				}
			}

			if ((this.tramite.isTercera() && !this.tramite.isTerceraMasiva())
					|| (this.tramite.isQuinta() && !this.tramite.isQuintaMasivo())) {
				this.compararConstruccionesFirmeYProyectadas();
			}

			if (this.digitalizadorAsignado != null) {

				this.digitalizador = this.getGeneralesService().getCacheUsuario(this.digitalizadorAsignado);

				this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(this.actividad,
						this.digitalizador);
			} else {
				this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(this.actividad,
						this.usuario);
			}

			tareasPendientesMB.init();
			UtilidadesWeb.removerManagedBean("proyectarConservacion");
			return "index";
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Ocurrió un error al enviar el trámite a modificar información geografica");
			return "";
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que avanza el proceso a la actividad Revisar proyección resolución.
	 *
	 * @return
	 */
	public String enviarARevisionProyeccionResolucion() {

		// Validación de valores cancela inscribe de la proyección
		this.validacionProyeccionService.validarCancelaInscribeProyeccion(this.tramite, this.usuario);

		this.getConservacionService().enviarARevisionProyeccionDeResolucion(this.actividad, this.usuario);

		tareasPendientesMB.init();
		UtilidadesWeb.removerManagedBean("proyectarConservacion");
		return "index";
	}

	// --------------------------------------------------- //
	/**
	 * Acción del botón cerrar de la pantalla proyectarConservación, retorna al
	 * arbol de tareas.
	 *
	 * @return
	 */
	public String cerrar() {

		if (this.entradaProyeccionMB == null) {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
		}
		if (this.entradaProyeccionMB.isCreaManzanas()) {
			this.validarGuardar();
		}
		this.tareasPendientesMB.init();
		UtilidadesWeb.removerManagedBean("proyectarConservacion");
		// @modified by leidy.gonzalez::#59843::23/04/2018
		// Se cierra la sesion del MB para que permita el ingreso
		UtilidadesWeb.removerManagedBean("tareasPendientes");

		return "index";
	}

	// --------------------------------------------------- //
	/**
	 * Devuelve la cadena que debe quedar como "tipo" de la foto del componente.
	 *
	 * @return
	 */
	/*
	 * @modified pedro.garcia 08-05-2012 se tiene en cuenta ahora el tipo de foto
	 * para los anexos
	 */
	private String getTipoFotoPorComponenteConstruccion() {

		String answer = "";

		if (!this.selectedUsoUnidadConstruccion.getDestinoEconomico()
				.equals(EUnidadConstruccionTipoCalificacion.ANEXO.getNombre())) {

			String componente = this.currentCalificacionConstruccionNode.getCalificacionConstruccion().getComponente();

			if (componente.equals(EComponenteConstruccionCom.ACABADOS_PRINCIPALES.getCodigo())) {
				answer = EFotoTipo.ACABADOS_PRINCIPALES_FACHADAS.getCodigo();
			} else if (componente.equals(EComponenteConstruccionCom.BANIO.getCodigo())) {
				answer = EFotoTipo.BANIO.getCodigo();
			} else if (componente.equals(EComponenteConstruccionCom.COCINA.getCodigo())) {
				answer = EFotoTipo.COCINA.getCodigo();
			} else if (componente.equals(EComponenteConstruccionCom.COMPLEMENTO_INDUSTRIA.getCodigo())) {
				answer = EFotoTipo.COMPLEMENTO_INDUSTRIA.getCodigo();
			} else if (componente.equals(EComponenteConstruccionCom.ESTRUCTURA.getCodigo())) {
				answer = EFotoTipo.ESTRUCTURA.getCodigo();
			}
		} else {
			answer = EFotoTipo.ANEXO.getCodigo();
		}

		return answer;
	}

	// --------------------------------------------------------------------------------------------------
	public boolean isBanderaCondicionProdpiedadCero() {
		return banderaCondicionProdpiedadCero;
	}

	public void setBanderaCondicionProdpiedadCero(boolean banderaCondicionProdpiedadCero) {
		this.banderaCondicionProdpiedadCero = banderaCondicionProdpiedadCero;
	}

	public boolean isDatosConstruccionSoloLectura() {
		return this.datosConstruccionSoloLectura;
	}

	public void setDatosConstruccionSoloLectura(boolean datosUnidadConstruccionSoloLectura) {
		this.datosConstruccionSoloLectura = datosUnidadConstruccionSoloLectura;
	}

	public List<PFoto> getpFotosList() {
		return pFotosList;
	}

	public void setpFotosList(List<PFoto> pFotosList) {
		this.pFotosList = pFotosList;
	}

	public List<PFichaMatrizModelo> getListModelosUnidadDeConstruccion() {
		return listModelosUnidadDeConstruccion;
	}

	public void setListModelosUnidadDeConstruccion(List<PFichaMatrizModelo> listModelosUnidadDeConstruccion) {
		this.listModelosUnidadDeConstruccion = listModelosUnidadDeConstruccion;
	}

	public PFichaMatrizModelo getModeloUnidadDeConstruccionSeleccionado() {
		return modeloUnidadDeConstruccionSeleccionado;
	}

	public void setModeloUnidadDeConstruccionSeleccionado(PFichaMatrizModelo modeloUnidadDeConstruccionSeleccionado) {
		this.modeloUnidadDeConstruccionSeleccionado = modeloUnidadDeConstruccionSeleccionado;
	}

	public String getTipoDeModelo() {
		return tipoDeModelo;
	}

	public void setTipoDeModelo(String tipoDeModelo) {
		this.tipoDeModelo = tipoDeModelo;
	}

	public List<PFmModeloConstruccion> getListUnidadesDelModeloDeConstruccion() {
		return listUnidadesDelModeloDeConstruccion;
	}

	public void setListUnidadesDelModeloDeConstruccion(
			List<PFmModeloConstruccion> listUnidadesDelModeloDeConstruccion) {
		this.listUnidadesDelModeloDeConstruccion = listUnidadesDelModeloDeConstruccion;
	}

	public PFmModeloConstruccion getUnidadDelModeloDeConstruccionSeleccionada() {
		return unidadDelModeloDeConstruccionSeleccionada;
	}

	public void setUnidadDelModeloDeConstruccionSeleccionada(
			PFmModeloConstruccion unidadDelModeloDeConstruccionSeleccionada) {
		this.unidadDelModeloDeConstruccionSeleccionada = unidadDelModeloDeConstruccionSeleccionada;
	}

	public List<SelectItem> getFechasTitulos() {
		return fechasTitulos;
	}

	public void setFechasTitulos(List<SelectItem> fechasTitulos) {
		this.fechasTitulos = fechasTitulos;
	}

	public boolean isAccion() {
		return true;
	}

	public boolean isAccion(Integer i) {
		return true;
	}

	// --------------------------------------------------- //
	/**
	 * Método que se encarga de obtener los predios que son del englobe cuando el
	 * trámite es un englobe.
	 *
	 * @author fabio.navarrete
	 */
	public void cargarPrediosEnglobe() {
		// Manejo de englobes, los predios a manejar se llenan a continuación.
		if (tramite.isSegundaEnglobe()) {
			this.pPrediosEnglobe = this.getConservacionService()
					.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que carga los predios del desenglobe si el trámite es de este tipo
	 *
	 * @author fabio.navarrete
	 */
	public void cargarPrediosDesenglobe() {
		if (tramite.isSegundaDesenglobe()) {

			// felipe.cadena:: para tramites de desenglobe por etapas se ignoran
			// los predios cancelados
			if (this.tramite.isDesenglobeEtapas()) {
				this.pPrediosDesenglobe = new LinkedList<PPredio>();
				List<PPredio> prediosProyectados = this.getConservacionService()
						.buscarPPrediosCompletosPorTramiteIdPHCondominio(this.tramite.getId());

				for (int i = 0; i < prediosProyectados.size(); i++) {
					PPredio pp = prediosProyectados.get(i);
					if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pp.getCancelaInscribe())) {

						pp.setEstadoEdicionEtapas(this.determinarEstadoEdicion(pp));
						if (pp.isPredioOriginalTramite()) {
							// Se obtienen el numero predial antiguo para los
							// predio originales
							Predio predioOriginal = this.getConservacionService().obtenerPredioPorId(pp.getId());
							if (predioOriginal != null) {
								pp.setNumeroPredialOriginal(predioOriginal.getNumeroPredial());
							}
						}
						this.pPrediosDesenglobe.add(pp);
					}
				}
			} else {
				this.pPrediosDesenglobe = this.getConservacionService()
						.buscarPPrediosCompletosPorTramiteIdPHCondominio(this.tramite.getId());
			}
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método que carga los para los trámites de tercera másivos y rectificación
	 * matriz.
	 *
	 * @author javier.aponte
	 */
	public void cargarPrediosTerceraRectificacionMasiva() {

		if (this.tramite.isTerceraMasiva() || this.tramite.isRectificacionMatriz()) {

			this.pPrediosTerceraRectificacionMasiva = this.getConservacionService()
					.buscarPPrediosCompletosPorTramiteIdPHCondominio(this.tramite.getId());
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método encargado de guardar el {@link PPredio} de la proyección
	 *
	 * @david.cifuentes
	 */
	public void guardarPPredioProyeccion() {

		try {
			if (this.predioSeleccionado != null && this.predioSeleccionado.isEsPredioFichaMatriz()) {

				if (this.fichaMatrizMB.getFichaMatriz() != null) {
					List<PFichaMatriz> pFichaMatrizs = new ArrayList<PFichaMatriz>();
					pFichaMatrizs.add(this.fichaMatrizMB.getFichaMatriz());
					this.predioSeleccionado.setPFichaMatrizs(pFichaMatrizs);
				}

				// Actualizar los nombres de los predios asociados a la ficha
				// matriz.
				actualizarNombresPrediosPHCondominio();
				// Guardar la proyección del predio seleccionado.
				this.getConservacionService().actualizarPPredio(this.predioSeleccionado);

				if (this.pPrediosDesenglobe != null) {
					for (PPredio pp : this.pPrediosDesenglobe) {
						if (this.fichaMatrizMB.getFichaMatriz() != null) {
							List<PFichaMatriz> pFichaMatrizs = new ArrayList<PFichaMatriz>();
							pFichaMatrizs.add(this.fichaMatrizMB.getFichaMatriz());
							pp.setPFichaMatrizs(pFichaMatrizs);
						}
					}
				}
				// Guardar la proyección de los predios asociados a la ficha
				// matriz.
				this.getConservacionService().guardarPPredios(this.pPrediosDesenglobe);
				this.cargarPrediosDesenglobe();

			} else {
				// Guardar la proyección del predio seleccionado.
				this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError(e);
		}
	}

	// --------------------------------------------------- //
	/**
	 * Método para actualizar el nombre de todos los predios asociados al trámite
	 * cuando el nombre se actualiza desde una ficha matriz.
	 *
	 * @author david.cifuentes
	 */
	public void actualizarNombresPrediosPHCondominio() {
		if (this.pPrediosDesenglobe != null && !this.pPrediosDesenglobe.isEmpty()) {
			for (PPredio pp : this.pPrediosDesenglobe) {
				pp.setNombre(this.nombrePredioFichaMatriz);
			}
			this.predioSeleccionado.setNombre(this.nombrePredioFichaMatriz);
		}
	}

	// =================================================================== //
	// ============== MÉTODOS DE MODELOS DE CONSTRUCCIÓN ================= //
	// =================================================================== //
	/**
	 * Método que carga los FmModelosConstruccion del PFichaMatrizModelo
	 *
	 * @author david.cifuentes
	 */
	public void cargarModeloDeConstruccion() {
		try {
			LOGGER.debug("Cargando modelo de construcción...");
			PFichaMatrizModelo auxModeloConstruccion = this.getConservacionService()
					.cargarModeloDeConstruccion(this.modeloUnidadDeConstruccionSeleccionado.getId());
			if (auxModeloConstruccion != null) {
				this.modeloUnidadDeConstruccionSeleccionado = auxModeloConstruccion;
			}
			if (this.modeloUnidadDeConstruccionSeleccionado != null) {

				this.tipoDeModelo = this.modeloUnidadDeConstruccionSeleccionado.getNombre();
				if (this.modeloUnidadDeConstruccionSeleccionado.getPFmModeloConstruccions() != null
						&& !this.modeloUnidadDeConstruccionSeleccionado.getPFmModeloConstruccions().isEmpty()) {
					this.listUnidadesDelModeloDeConstruccion = this.modeloUnidadDeConstruccionSeleccionado
							.getPFmModeloConstruccions();
				}
				LOGGER.debug("Modelo de construcción cargado.");

				listaElementosCalificacionConstruccion = this.getConservacionService()
						.obtenerTodosCalificacionConstruccion();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al cargar las unidades de construcción del modelo.");
		}
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que elimina un PFichaMatrizModelo de construcción con sus
	 * FmModelosConstruccion asociados.
	 *
	 * @author david.cifuentes
	 */
	public void eliminarModeloDeConstruccion() {
		// si era null queda C
		// si era I se elimina
		// si era C se queda C No deberia darse
		// si era M queda C
		try {
			PFichaMatrizModelo pFichaMatrizModeloAux = (PFichaMatrizModelo) this.getConservacionService()
					.eliminarProyeccion(MenuMB.getMenu().getUsuarioDto(), this.modeloUnidadDeConstruccionSeleccionado);
			if (pFichaMatrizModeloAux == null) {
				if (this.listModelosUnidadDeConstruccion.contains(this.modeloUnidadDeConstruccionSeleccionado)) {
					this.listModelosUnidadDeConstruccion.remove(this.modeloUnidadDeConstruccionSeleccionado);
				}
				this.modeloUnidadDeConstruccionSeleccionado = new PFichaMatrizModelo();
				this.addMensajeInfo("Se eliminó el modelo satisfactoriamente!");
			} else {
				this.modeloUnidadDeConstruccionSeleccionado
						.setCancelaInscribe(pFichaMatrizModeloAux.getCancelaInscribe());
				this.addMensajeInfo("Se canceló el modelo satisfactoriamente!");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al eliminar el modelo de construcción.");
		}

		// Revisar si la ficha matriz quedó con modelos asociados.
		if (this.listModelosUnidadDeConstruccion != null && this.listModelosUnidadDeConstruccion.size() > 0) {
			this.existenModelosBool = true;
		} else {
			this.existenModelosBool = false;
		}
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que elimina un PFmModeloConstruccion seleccionado de los asociados al
	 * modelo de constucción.
	 *
	 * @author david.cifuentes
	 */
	public void eliminarUnidadDelModeloDeConstruccion() {
		// si era null queda C
		// si era I se elimina
		// si era C se queda C No deberia darse
		// si era M queda C
		try {

			// Borrar fotos asociadas a la unidad del modelo de construcción
			this.getConservacionService().borrarPModeloConstruccionFotosDelPFmModeloConstruccion(
					this.unidadDelModeloDeConstruccionSeleccionada.getId());

			PFmModeloConstruccion pFmModeloConstruccionAux = (PFmModeloConstruccion) this.getConservacionService()
					.eliminarProyeccion(MenuMB.getMenu().getUsuarioDto(),
							this.unidadDelModeloDeConstruccionSeleccionada);
			if (pFmModeloConstruccionAux == null) {
				if (this.listUnidadesDelModeloDeConstruccion.contains(this.unidadDelModeloDeConstruccionSeleccionada)) {
					this.listUnidadesDelModeloDeConstruccion.remove(this.unidadDelModeloDeConstruccionSeleccionada);
				}
				this.unidadDelModeloDeConstruccionSeleccionada = new PFmModeloConstruccion();
				this.addMensajeInfo("Se eliminó la unidad satisfactoriamente!");
			} else {
				this.unidadDelModeloDeConstruccionSeleccionada
						.setCancelaInscribe(pFmModeloConstruccionAux.getCancelaInscribe());
				this.addMensajeInfo("Se canceló la unidad satisfactoriamente!");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al eliminar la unidad de construcción del modelo.");
		}
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que inicializa las variables y componentes de la unidad del modelo.
	 * Acción sobre el botón "modificarUnidadDelModeloDeConstruccionBtn"
	 *
	 * @author david.cifuentes
	 */
	public void cargarUnidadDelModelo() {
		try {

			// Init de la unidad del modelo de construcción //
			TreeNode calificacionConstruccionNodesLevel1 = null;
			CalificacionConstruccionTreeNode cctnTemp;
			String tempComponenteNameNode, tempContainedComponenteNamePModeloConstruccionFoto;

			// ========================================== //
			// Se forza la consulta el modelo de construcción actualizado, para
			// que traiga sus componentes.
			if (this.unidadDelModeloEditModeBool) {
				if (!this.tramite.isTercera()) {
					this.unidadDelModeloDeConstruccionSeleccionada = this.getConservacionService()
							.buscarUnidadDelModeloDeConstruccionPorSuId(
									this.unidadDelModeloDeConstruccionSeleccionada.getId());
				}
			}
			// ========================================== //
			if (this.usosUnidadConstruccionItemList == null || this.usosUnidadConstruccionItemList.isEmpty()) {

				this.usosUnidadConstruccionItemList = new ArrayList<SelectItem>();
				if (this.predioSeleccionado != null) {
					this.usosConstruccionList = this.getConservacionService()
							.obtenerVUsosConstruccionZona(this.predioSeleccionado.getNumeroPredial().substring(0, 7));
				} else if (this.fichaMatrizMB.getFichaMatriz() != null) {

					List<PFichaMatriz> pFichaMatrizs = new ArrayList<PFichaMatriz>();
					pFichaMatrizs.add(this.fichaMatrizMB.getFichaMatriz());
					this.predioSeleccionado = new PPredio();
					this.predioSeleccionado = this.fichaMatrizMB.getFichaMatriz().getPPredio();
					Departamento departamento = this.getGeneralesService().getCacheDepartamentoByCodigo(
							this.fichaMatrizMB.getFichaMatriz().getPPredio().getNumeroPredial().substring(0, 2));
					this.predioSeleccionado.setDepartamento(departamento);
					Municipio municipio = this.getGeneralesService().getCacheMunicipioByCodigo(
							this.fichaMatrizMB.getFichaMatriz().getPPredio().getNumeroPredial().substring(0, 5));
					this.predioSeleccionado.setMunicipio(municipio);
					this.predioSeleccionado.setPFichaMatrizs(pFichaMatrizs);

					this.usosConstruccionList = this.getConservacionService()
							.obtenerVUsosConstruccionZona(this.predioSeleccionado.getNumeroPredial().substring(0, 7));
				}

				this.setUsosUnidadConstruccionItemList(this.usosConstruccionList);
			}

			if (this.unidadDelModeloDeConstruccionSeleccionada.getUsoConstruccion() != null) {
				this.selectedUsoUnidadConstruccionId = this.unidadDelModeloDeConstruccionSeleccionada
						.getUsoConstruccion().getId();
				this.selectedUsoUnidadConstruccion = this.unidadDelModeloDeConstruccionSeleccionada
						.getUsoConstruccion();
			} else {
				if (!this.usosUnidadConstruccionItemList.get(0).getLabel().equals(this.DEFAULT_COMBOS)) {
					this.usosUnidadConstruccionItemList.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
				}
				this.selectedUsoUnidadConstruccionId = 0l;

				// D: si no inicializo esto, en el xhtml no se reconocen las
				// propiedades de este objeto:
				// al referirme a un atributo que no existe en el objeto, no
				// sale error
				// this.selectedUsoUnidadConstruccion = new UsoConstruccion();
				// ... falso. No funcionaba por no usar tipos java simple
			}

			// Set del id de la calificación anexo
			if (this.unidadDelModeloDeConstruccionSeleccionada.getCalificacionAnexoId() != null) {
				this.calificacionAnexoIdSelected = this.unidadDelModeloDeConstruccionSeleccionada
						.getCalificacionAnexoId();
			}

			// D: armar el TreeNode para mostrar los componentes de construcción
			// D: se arman los tres posibles desde el principio porque si se
			// intenta hacer que en el momento en que cambie el combo se arme
			// y se refresque el nuevo árbol, no funciona (se hace el refresco
			// por cada línea de la tabla que muestra el árbol, y eso daña
			// los valores de la columna "puntos"
			this.treeComponentesConstruccion2 = armarArbolCalificacionConstruccion(
					EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo());
			this.treeComponentesConstruccion3 = armarArbolCalificacionConstruccion(
					EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo());
			this.treeComponentesConstruccion1 = armarArbolCalificacionConstruccion(
					EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo());

			// N: OJO con esto. NO es necesario hacerlo, y si se hace causa
			// errores this.selectedCalificacionAnexo = null;
			// D: Se verifica que no se trate de una nueva unidad de
			// construcción antes de hacer la consulta de las
			// PModeloConstruccionFoto asociadas a esta unidad del modelo
			// (pFmModeloConstruccion)
			if (this.unidadDelModeloEditModeBool) {

				this.areaNoConvencional = this.unidadDelModeloDeConstruccionSeleccionada.getAreaConstruida();

				calificacionConstruccionNodesLevel1 = new DefaultTreeNode();

				// D: ... luego de tener estos datos hay que recorrer el árbol y
				// asignarle a cada nodo los datos de foto obtenidos.
				if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
						.compareToIgnoreCase(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo()) != 0) {

					this.isAnexoONuevaUnidadDelModelo = false;

					if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
							.compareToIgnoreCase(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo()) == 0) {

						calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion2;

					} else if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
							.compareToIgnoreCase(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo()) == 0) {

						calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion3;

						Collections.copy(calificacionConstruccionNodesLevel1.getChildren(),
								this.treeComponentesConstruccion3.getChildren());

					} else if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
							.compareToIgnoreCase(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo()) == 0) {

						calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion1;

					}

					try {
						// Consulta de las fotos de la unidad del modelo
						this.listPFotosUnidadDelModelo = this.getConservacionService()
								.buscarPModeloConstruccionFotosPorIdUnidad(
										this.unidadDelModeloDeConstruccionSeleccionada.getId(), true);

						if (this.listPFotosUnidadDelModelo != null && !this.listPFotosUnidadDelModelo.isEmpty()) {

							// Se recorre el árbol y se le asigna
							// a cada nodo los datos de foto obtenida
							if (calificacionConstruccionNodesLevel1 != null
									&& calificacionConstruccionNodesLevel1.getChildren() != null
									&& calificacionConstruccionNodesLevel1.getChildren().size() > 0) {

								for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1
										.getChildren()) {
									cctnTemp = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
									tempComponenteNameNode = cctnTemp.getCalificacionConstruccion().getComponente();
									for (PModeloConstruccionFoto currentPModeloConstruccionFotoAux : this.listPFotosUnidadDelModelo) {
										tempContainedComponenteNamePModeloConstruccionFoto = currentPModeloConstruccionFotoAux
												.getDescripcion();
										if (tempContainedComponenteNamePModeloConstruccionFoto != null) {
											if (tempContainedComponenteNamePModeloConstruccionFoto
													.contains(tempComponenteNameNode)) {
												cctnTemp.setUrlArchivoWebTemporal(currentPModeloConstruccionFotoAux
														.getFotoDocumento().getRutaArchivoWeb());
											}
										}
									}

								}
							}
						}
					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}

					// Hacer un recorrido de los componentes de la unidad de
					// construcción seleccionada y setearselos a los combo box
					// del arbol.
					cargarComponentesDeLaUnidadDelModelo(calificacionConstruccionNodesLevel1,
							this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion());

				} // D: para la foto de la unidad del modelo de construcción que
					// es ANEXO.
					// La lista de PModeloConstruccionFotos, si tiene elementos,
					// debe tener 1 sola
				else {
					this.isAnexoONuevaUnidadDelModelo = true;
					if (this.listPFotosUnidadDelModelo != null && !this.listPFotosUnidadDelModelo.isEmpty()) {
						this.rutaWebTemporalFotoAnexo = this.listPFotosUnidadDelModelo.get(0).getFotoDocumento()
								.getRutaArchivoWeb();
					} else {
						this.rutaWebTemporalFotoAnexo = this.rutaWebImagenBase;
					}
				}
			} else {
				// Si es una nueva construcción se inicializa el arbol de
				// componentes.
				this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion1;
				this.actualizarArbolDeComponentes();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al cargar la información de la unidad seleccionada.");
		}
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método encargado de actualizar el arbol de componentes inicial con los
	 * valores de los componentes perteneciantes a la una unidad de construcción
	 * seleccionada.
	 *
	 * @author david.cifuentes
	 */
	public void cargarComponentesDeLaUnidadDelModelo(TreeNode calificacionConstruccionNodesLevel1,
			String tipoCalificacion) {

		this.treeComponentesConstruccionParaModelo = null;
		/**
		 * 1. Recorrer la lista de componentes de la unidad de construcción. 2. Para el
		 * componente n de la lista hallar el componente, detalleCalificación, elemento
		 * y recuperar la calificacionConstruccion de éste. 3. Con esta
		 * calificacionConstruccion recorrer el TreeNode que se recibe como parametro, y
		 * al encontrar el nodo, setear los datos recuperados de la unidad de
		 * construcción. 4. Reasignar el TreeNode actualizado con el detalle de la
		 * unidad del modelo de construcción.
		 */

		if (calificacionConstruccionNodesLevel1 != null && calificacionConstruccionNodesLevel1.getChildren() != null
				&& calificacionConstruccionNodesLevel1.getChildren().size() > 0) {

			String componente, detalleCalificacion, elementoCalificacion;
			CalificacionConstruccion auxCalificacionConstruccion, calificacionConstruccionDelComponente;
			if (this.unidadDelModeloDeConstruccionSeleccionada.getPFmConstruccionComponentes() != null
					&& !this.unidadDelModeloDeConstruccionSeleccionada.getPFmConstruccionComponentes().isEmpty()) {
				for (PFmConstruccionComponente pfmcc : this.unidadDelModeloDeConstruccionSeleccionada
						.getPFmConstruccionComponentes()) {
					componente = pfmcc.getComponente();
					elementoCalificacion = pfmcc.getElementoCalificacion();
					detalleCalificacion = pfmcc.getDetalleCalificacion();
					calificacionConstruccionDelComponente = buscarCalificacionConstruccion(componente,
							elementoCalificacion, detalleCalificacion);

					if (calificacionConstruccionDelComponente != null) {
						for (TreeNode tn : calificacionConstruccionNodesLevel1.getChildren()) {

							// Leer el nodo y comparar con el valor de
							// componente.
							auxCalificacionConstruccion = ((CalificacionConstruccionTreeNode) tn.getData())
									.getCalificacionConstruccion();
							if (auxCalificacionConstruccion != null
									&& auxCalificacionConstruccion.getComponente() != null
									&& auxCalificacionConstruccion.getComponente().equals(componente)) {

								// Cuando encuentre el componente en el arbol,
								// se recorren las calificaciones del componente
								// y se hace el set del detalle a cada una.
								int puntos = 0;
								for (TreeNode tnElemento : tn.getChildren()) {
									if (((CalificacionConstruccionTreeNode) tnElemento.getData())
											.getCalificacionConstruccion().getElemento().equals(elementoCalificacion)) {

										// Se actualizan los puntos de
										// calificacion para el elemento
										// CalificacionConstruccionDelComponente.
										puntos = getPuntosPorCalificacionConstruccion(
												calificacionConstruccionDelComponente, tipoCalificacion);
										calificacionConstruccionDelComponente.setPuntos(puntos);

										// Se setea el objeto calificacion
										// construccion de la unidad al
										// selected.
										((CalificacionConstruccionTreeNode) tnElemento.getData())
												.setSelectedDetalle(calificacionConstruccionDelComponente);
									}
								}
							}
						}
					}
				}
				// Reasignación del TreeNode.
				this.treeComponentesConstruccionParaModelo = calificacionConstruccionNodesLevel1;
			}
		}
	}

	// ------------------------------------------------------------ //
	/**
	 * Método que carga el puntaje total de construcción para la unidad del modelo.
	 *
	 * @author david.cifuentes
	 */
	public int getTotalPuntosConstruccionUnidadDelModelo() {

		int answer = 0;
		List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
		CalificacionConstruccionTreeNode cctnTempL2;
		if ((this.tramite.isEsComplementacion()
				|| this.unidadDelModeloDeConstruccionSeleccionada.getPFmConstruccionComponentes() == null
				|| this.unidadDelModeloDeConstruccionSeleccionada.getPFmConstruccionComponentes().isEmpty())
				&& this.unidadDelModeloDeConstruccionSeleccionada.getTotalPuntaje() != null) {
			return this.unidadDelModeloDeConstruccionSeleccionada.getTotalPuntaje().intValue();
		} else {
			if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
				this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
			} else if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
				this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
			} else if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
				this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
			}

			calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion.getChildren();
			for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
				calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
				for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
					cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
					answer += (cctnTempL2.getSelectedDetalle() == null) ? 0
							: cctnTempL2.getSelectedDetalle().getPuntos();
				}
			}
			return answer;
		}
	}

	// ------------------------------------------------------------ //
	/**
	 * Método que carga los modelos de construcción existentes.
	 *
	 * @author david.cifuentes
	 */
	public void cargarModelosDeConstruccionExistentes() {

		if ((this.tramite.isTercera() || this.tramite.isTerceraMasiva()) && this.tramite.getPredio().isEsPredioEnPH()) {
			this.consultarModelosMutacionTercera();
		} else {
			if (this.fichaMatrizMB == null) {
				this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
			}

			if (this.fichaMatrizMB.getFichaMatriz() != null) {
				this.fichaMatrizMB.setupModelosDeConstruccion();
			}

		}

	}

	// ------------------------------------------------------------ //
	/**
	 * Método que toma las unidades de construcción de un modelo seleccionado y se
	 * las asocia al predio seleccionado.
	 *
	 * @author david.cifuentes
	 */
	public void asociarUnidadesDelModelo() {
		try {
			if (this.modeloUnidadDeConstruccionSeleccionado != null) {
				// Recorrer las unidades del modelo de construcción
				// (PFmModelosConstruccion) y asociar al predio por cada una de
				// ellas una unidad de construcción (PUnidadConstruccion)

				if (!this.tramite.isTercera()) {
					this.modeloUnidadDeConstruccionSeleccionado = this.getConservacionService()
							.cargarModeloDeConstruccion(this.modeloUnidadDeConstruccionSeleccionado.getId());
				}

				boolean mensajeAdicionado = false;
				if (this.modeloUnidadDeConstruccionSeleccionado.getPFmModeloConstruccions() != null
						&& !this.modeloUnidadDeConstruccionSeleccionado.getPFmModeloConstruccions().isEmpty()) {

					PUnidadConstruccion pUnidadConstruccion = null;

					List<PUnidadConstruccion> unidadesConstruccionDelModelo = new ArrayList<PUnidadConstruccion>();

					// Cálculo del valor de la unidad para la nueva
					// PUnidadConstruccion
					String unidadConstruccionUnidadSiguiente = calcularUnidadConstruccionUnidad();

					for (PFmModeloConstruccion pfmmc : this.modeloUnidadDeConstruccionSeleccionado
							.getPFmModeloConstruccions()) {

						pUnidadConstruccion = crearUnidadDeConstruccionAPartirDeUnidadDelModelo(pfmmc,
								unidadConstruccionUnidadSiguiente);

						if (pUnidadConstruccion != null && pUnidadConstruccion.getId() != null) {
							unidadesConstruccionDelModelo.add(pUnidadConstruccion);
						}

						// Recalcular el valor de la unidad para la siguiente
						// unidad de contruccion en base a la última asignada.
						unidadConstruccionUnidadSiguiente = UtilidadesWeb
								.calcularSiguienteUnidadUnidadConstruccion(unidadConstruccionUnidadSiguiente);

					}

					// ACTUALIZAR AVALÚO
					recalcularAvaluo(!mensajeAdicionado);

					// CONSULTAR PREDIO ACTUALIZADO
					if (this.predioSeleccionado != null) {
						this.predioSeleccionado = this.getConservacionService()
								.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
						setAvaluo();
					}

					// ACTUALIZAR EL VALOR DE LAS ÁREAS
					guardarAreas();
					if (!mensajeAdicionado) {
						this.addMensajeInfo(
								"Se asociaron las unidades de construcción correctamente, recuerde editar el campo 'Piso ubicación' y demás campos necesarios para cada una de ellas.");
					}
					mensajeAdicionado = true;
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al asociar las unidades del modelo de construcción.");
		}
	}

	/**
	 * Método que toma las unidades de construcción de un modelo seleccionado y se
	 * las asocia al predio seleccionado, este método es para trámites de tercera
	 * másivos
	 *
	 * @author javier.aponte
	 */
	public void asociarUnidadesDelModeloTerceraMasiva() {
		try {

			// javier.aponte :: Se consulta el
			// modeloUnidadDeConstruccionSeleccionado para que traiga
			// la lista de getPFmModeloConstruccions :: 3/07/2015
			if (this.modeloUnidadDeConstruccionSeleccionado != null) {
				PFichaMatrizModelo auxModeloConstruccion = this.getConservacionService()
						.cargarModeloDeConstruccion(this.modeloUnidadDeConstruccionSeleccionado.getId());
				this.modeloUnidadDeConstruccionSeleccionado = auxModeloConstruccion;

				if (this.modeloUnidadDeConstruccionSeleccionado != null) {
					// Recorrer las unidades del modelo de construcción
					// (PFmModelosConstruccion) y asociar al predio por cada una
					// de
					// ellas una unidad de construcción (PUnidadConstruccion)

					boolean mensajeAdicionado = false;
					if (this.modeloUnidadDeConstruccionSeleccionado.getPFmModeloConstruccions() != null
							&& !this.modeloUnidadDeConstruccionSeleccionado.getPFmModeloConstruccions().isEmpty()) {

						if (this.entradaProyeccionMB == null) {
							this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb
									.getManagedBean("entradaProyeccion");
						}

						for (PPredio pPredioTemp : this.entradaProyeccionMB.getPrediosResultantes()) {

							if (pPredioTemp.isSelected()) {

								if (pPredioTemp.getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END)
										|| pPredioTemp.getNumeroPredial().endsWith(Constantes.FM_PH_END)) {
									this.addMensajeInfo("A La ficha matriz no puede asociarse modelos");
									return;
								}

								this.predioSeleccionado = pPredioTemp;

								PUnidadConstruccion pUnidadConstruccion = null;

								List<PUnidadConstruccion> unidadesConstruccionDelModelo = new ArrayList<PUnidadConstruccion>();

								// Cálculo del valor de la unidad para la nueva
								// PUnidadConstruccion
								String unidadConstruccionUnidadSiguiente = calcularUnidadConstruccionUnidad();

								for (PFmModeloConstruccion pfmmc : this.modeloUnidadDeConstruccionSeleccionado
										.getPFmModeloConstruccions()) {

									pUnidadConstruccion = crearUnidadDeConstruccionAPartirDeUnidadDelModelo(pfmmc,
											unidadConstruccionUnidadSiguiente);

									if (pUnidadConstruccion != null && pUnidadConstruccion.getId() != null) {
										unidadesConstruccionDelModelo.add(pUnidadConstruccion);
									}

									// Recalcular el valor de la unidad para la
									// siguiente
									// unidad de contruccion en base a la última
									// asignada.
									unidadConstruccionUnidadSiguiente = UtilidadesWeb
											.calcularSiguienteUnidadUnidadConstruccion(
													unidadConstruccionUnidadSiguiente);

								}

								// ACTUALIZAR AVALÚO
								recalcularAvaluo(!mensajeAdicionado);

								// CONSULTAR PREDIO ACTUALIZADO
								if (this.predioSeleccionado != null) {
									this.predioSeleccionado = this.getConservacionService()
											.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
									setAvaluo();
								}

								// ACTUALIZAR EL VALOR DE LAS ÁREAS
								guardarAreas();
								if (!mensajeAdicionado) {
									this.addMensajeInfo(
											"Se asociaron las unidades de construcción correctamente, recuerde editar el campo 'Piso ubicación' y demás campos necesarios para cada una de ellas.");
								}
								mensajeAdicionado = true;
							}
						}
						if (this.entradaProyeccionMB.getPrediosResultantesSeleccionados() != null
								&& !this.entradaProyeccionMB.getPrediosResultantesSeleccionados().isEmpty()
								&& this.entradaProyeccionMB.getPrediosResultantesSeleccionados().size() > 1) {
							this.predioSeleccionado = null;
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al asociar las unidades del modelo de construcción.");
		}
	}

	// ----------------------------------------------------------------- //
	/**
	 * Método que crea una PUnidadConstruccion a partir de una
	 * PFmModeloConstruccion.
	 *
	 * @return
	 *
	 * @author david.cifuentes
	 */
	public PUnidadConstruccion crearUnidadDeConstruccionAPartirDeUnidadDelModelo(
			PFmModeloConstruccion pFmModeloConstruccion, String unidadConstruccionUnidadSiguiente) {
		PUnidadConstruccion pUnidadConstruccion = new PUnidadConstruccion();

		// ======================================================== //
		// ========== CREACIÓN DE LA PUnidadConstruccion ========== //
		// ========== A PARTIR DEL PFmModeloConstruccion ========== //
		// ======================================================== //
		pUnidadConstruccion.setPPredio(this.predioSeleccionado);
		pUnidadConstruccion.setUsoConstruccion(pFmModeloConstruccion.getUsoConstruccion());
		pUnidadConstruccion.setObservaciones(pFmModeloConstruccion.getObservaciones());
		pUnidadConstruccion.setAnioConstruccion(pFmModeloConstruccion.getAnioConstruccion());
		pUnidadConstruccion.setTotalPuntaje(pFmModeloConstruccion.getTotalPuntaje());
		pUnidadConstruccion.setTipoConstruccion(pFmModeloConstruccion.getTipoConstruccion());
		pUnidadConstruccion.setTipoCalificacion(pFmModeloConstruccion.getTipoCalificacion());
		pUnidadConstruccion.setDescripcion(pFmModeloConstruccion.getDescripcion());
		pUnidadConstruccion.setCalificacionAnexoId(pFmModeloConstruccion.getCalificacionAnexoId());
		pUnidadConstruccion.setTotalLocales(pFmModeloConstruccion.getTotalLocales());
		pUnidadConstruccion.setTotalBanios(pFmModeloConstruccion.getTotalBanios());
		pUnidadConstruccion.setTotalHabitaciones(pFmModeloConstruccion.getTotalHabitaciones());
		pUnidadConstruccion.setTotalPisosConstruccion(pFmModeloConstruccion.getTotalPisosConstruccion());
		pUnidadConstruccion.setTotalPisosUnidad(pFmModeloConstruccion.getTotalPisosUnidad());
		pUnidadConstruccion.setTipoDominio(EUnidadTipoDominio.PRIVADO.getCodigo());
		pUnidadConstruccion.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
		pUnidadConstruccion.setFechaLog(new Date(System.currentTimeMillis()));
		pUnidadConstruccion.setUsuarioLog(this.usuario.getLogin());
		pUnidadConstruccion.setAvaluo(0D);
		pUnidadConstruccion.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

		pUnidadConstruccion.setTipificacion(pFmModeloConstruccion.getTipificacion());

		// Set del valor correspondiente del dominio para la tipificación
		Dominio valorDominio = this.getGeneralesService().buscarValorDelDominioPorDominioYCodigo(
				EDominio.UNIDAD_CONSTRUCION_TIPIFICA, this.unidadConstruccionSeleccionada.getTipificacion());

		if (valorDominio != null) {
			pUnidadConstruccion.setTipificacionValor(valorDominio.getValor());
		}

		if (pFmModeloConstruccion.getTipoConstruccion() != null && pFmModeloConstruccion.getTipoConstruccion()
				.equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
			pUnidadConstruccion.setAreaConstruida(pFmModeloConstruccion.getAreaConstruida());
		} else {
			pUnidadConstruccion.setAreaConstruida(pFmModeloConstruccion.getDimension());
			pUnidadConstruccion.setAreaConstruida(pFmModeloConstruccion.getAreaConstruida());
		}

		if (pUnidadConstruccion.getAreaConstruida() == null) {
			pUnidadConstruccion.setAreaConstruida(0D);
		}

		pUnidadConstruccion.setUnidad(unidadConstruccionUnidadSiguiente);

		// Guardar la unidad de construcción
		pUnidadConstruccion = (PUnidadConstruccion) this.getConservacionService().guardarProyeccion(this.usuario,
				pUnidadConstruccion);

		// ======================================================== //
		// ============== ASOCIACIÓN DE COMPONENTES =============== //
		// ======================================================== //
		// Consultar componentes del modelo
		if (!this.tramite.isTercera()) {
			pFmModeloConstruccion = this.getConservacionService()
					.buscarUnidadDelModeloDeConstruccionPorSuId(pFmModeloConstruccion.getId());
		}

		// Creación de los PUnidadConstruccionComp a partir de los
		// PFmConstruccionComponente.
		if (pUnidadConstruccion != null && pFmModeloConstruccion.getPFmConstruccionComponentes() != null
				&& !pFmModeloConstruccion.getPFmConstruccionComponentes().isEmpty()) {

			PUnidadConstruccionComp pucc = null;
			List<PUnidadConstruccionComp> listaComponentes = new ArrayList<PUnidadConstruccionComp>();

			for (PFmConstruccionComponente pfmcc : pFmModeloConstruccion.getPFmConstruccionComponentes()) {
				pucc = crearUnidadDeConstruccionComponenteAPartirDeComponenteDelModelo(pfmcc, pUnidadConstruccion);
				listaComponentes.add(pucc);
			}

			// Guardar lista de componentes de la unidad de constucción creada.
			if (!listaComponentes.isEmpty()) {
				listaComponentes = this.getConservacionService()
						.actualizarPUnidadConstruccionCompRegistros(listaComponentes);
			}
			pUnidadConstruccion.setPUnidadConstruccionComps(listaComponentes);
			pUnidadConstruccion = (PUnidadConstruccion) this.getConservacionService().guardarProyeccion(this.usuario,
					pUnidadConstruccion);
		}

		// ================================================== //
		// ============== ASOCIACIÓN DE FOTOS =============== //
		// ================================================== //
		// Consulta de fotos de la unidad del modelo
		List<PModeloConstruccionFoto> listPFotosUnidadDelModelo = this.getConservacionService()
				.buscarPModeloConstruccionFotosPorIdUnidad(pFmModeloConstruccion.getId(), true);

		if (pUnidadConstruccion != null && listPFotosUnidadDelModelo != null && !listPFotosUnidadDelModelo.isEmpty()) {

			PFoto pf = null;
			List<PFoto> listaPFotos = new ArrayList<PFoto>();
			try {
				for (PModeloConstruccionFoto pmcf : listPFotosUnidadDelModelo) {
					pf = crearPFotoAPartirDePModeloConstruccionFoto(pmcf, pUnidadConstruccion);
					listaPFotos.add(pf);
				}
				// Guardar lista de fotos.
				if (listaPFotos != null && !listaPFotos.isEmpty()) {
					this.getConservacionService().guardarPFotos(listaPFotos);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			}
		}

		return pUnidadConstruccion;
	}

	// ----------------------------------------------------------------- //
	/**
	 * Método que crea una PUnidadConstruccionComp a partir de una
	 * {@link PFmConstruccionComponente}.
	 *
	 * @return
	 *
	 * @author david.cifuentes
	 */
	public PUnidadConstruccionComp crearUnidadDeConstruccionComponenteAPartirDeComponenteDelModelo(
			PFmConstruccionComponente pFmConstruccionComponente, PUnidadConstruccion pUnidadConstruccion) {

		PUnidadConstruccionComp pUnidadConstruccionComp = null;

		if (pUnidadConstruccion != null) {
			pUnidadConstruccionComp = new PUnidadConstruccionComp();
			pUnidadConstruccionComp.setPUnidadConstruccion(pUnidadConstruccion);
			pUnidadConstruccionComp.setComponente(pFmConstruccionComponente.getComponente());
			pUnidadConstruccionComp.setPuntos(pFmConstruccionComponente.getPuntos());
			pUnidadConstruccionComp.setElementoCalificacion(pFmConstruccionComponente.getElementoCalificacion());
			pUnidadConstruccionComp.setDetalleCalificacion(pFmConstruccionComponente.getDetalleCalificacion());
			pUnidadConstruccionComp.setFechaLog(new Date(System.currentTimeMillis()));
			pUnidadConstruccionComp.setUsuarioLog(this.usuario.getLogin());
			pUnidadConstruccionComp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
		}

		return pUnidadConstruccionComp;
	}

	// ----------------------------------------------------------------- //
	/**
	 * Método que crea una {@link PFoto} a partir de una
	 * {@link PModeloConstruccionFoto}
	 *
	 * @return
	 *
	 * @author david.cifuentes
	 */
	public PFoto crearPFotoAPartirDePModeloConstruccionFoto(PModeloConstruccionFoto pModeloConstruccionFoto,
			PUnidadConstruccion pUnidadConstruccion) {
		PFoto pFoto = new PFoto();

		pFoto.setPPredio(this.predioSeleccionado);
		pFoto.setPUnidadConstruccion(pUnidadConstruccion);
		pFoto.setTramiteId(this.tramite.getId());
		pFoto.setTipo(pModeloConstruccionFoto.getTipo());
		pFoto.setDescripcion(pModeloConstruccionFoto.getDescripcion());
		pFoto.setFecha(new Date(System.currentTimeMillis()));
		pFoto.setUsuarioLog(this.usuario.getLogin());
		pFoto.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
		pFoto.setFechaLog(new Date(System.currentTimeMillis()));
		pFoto.setDocumento(pModeloConstruccionFoto.getFotoDocumento());

		return pFoto;
	}

	// ----------------------------------------------------------------------------------------
	/**
	 * Método que busca el objeto {@link CalificacionConstruccion} que tenga los
	 * paramentros ingresados, y que se encuentre en la lista
	 * listaElementosCalificacionConstruccion.
	 *
	 * @return
	 *
	 * @author david.cifuentes
	 */
	public CalificacionConstruccion buscarCalificacionConstruccion(String componente, String elementoCalificacion,
			String detalleCalificacion) {

		CalificacionConstruccion ccanswer = null;

		// Verificar que la lista de elementos calificacion construccion no esté
		// vacia.
		if (this.listaElementosCalificacionConstruccion == null
				|| this.listaElementosCalificacionConstruccion.isEmpty()) {

			listaElementosCalificacionConstruccion = this.getConservacionService()
					.obtenerTodosCalificacionConstruccion();
		}
		// Búsqueda del elemento calificación construcción.
		if (this.listaElementosCalificacionConstruccion != null
				&& !this.listaElementosCalificacionConstruccion.isEmpty()) {

			if (componente != null && elementoCalificacion != null && detalleCalificacion != null) {
				for (CalificacionConstruccion cc : this.listaElementosCalificacionConstruccion) {
					if (cc.getComponente().equals(componente) && cc.getElemento().equals(elementoCalificacion)
							&& cc.getDetalleCalificacion().equals(detalleCalificacion)) {
						ccanswer = cc;
						break;
					}
				}
			}
		}
		return ccanswer;
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que inicializa las variables para un nuevo modelo de construcción
	 *
	 * @author david.cifuentes
	 */
	public void setupAdicionUnidadAlModelo() {
		this.tipoDominioComun = true;
		this.areaNoConvencional = 0D;
		crearNuevaUnidadParaElModeloDeConstruccion();
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que inicializa las variables para crear una nueva unidad para el
	 * modelo de construcción. Acción del botón "Adicionar unidad de construcción"
	 * de tablaUnidadesDelModelo
	 *
	 * @author david.cifuentes
	 */
	public void crearNuevaUnidadParaElModeloDeConstruccion() {

		String identificadorUnidadDelModelo;

		// D: se crea una nueva unidad para el modelo de construcción y se le
		// asignan valores
		this.unidadDelModeloDeConstruccionSeleccionada = new PFmModeloConstruccion();

		this.unidadDelModeloEditModeBool = false;

		// D: datos temporales (los que no pueden ser nulos y no tienen default)
		this.unidadDelModeloDeConstruccionSeleccionada.setTipificacion(EUnidadConstruccionTipificacion.ANY.getCodigo());
		this.unidadDelModeloDeConstruccionSeleccionada.setAnioConstruccion(this.getAnoActual());
		this.unidadDelModeloDeConstruccionSeleccionada
				.setTipoConstruccion(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());
		this.unidadDelModeloDeConstruccionSeleccionada
				.setTipoCalificacion(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo());

		this.unidadDelModeloDeConstruccionSeleccionada
				.setPFichaMatrizModelo(this.modeloUnidadDeConstruccionSeleccionado);

		identificadorUnidadDelModelo = calcularIdentificadorUnidadDelModelo(
				this.modeloUnidadDeConstruccionSeleccionado);
		this.unidadDelModeloDeConstruccionSeleccionada.setUnidad(identificadorUnidadDelModelo);

		this.calificacionAnexoIdSelected = null;

		// D: ... y los que tienen default también porque al generarse los
		// entities no queda con esa definición
		this.unidadDelModeloDeConstruccionSeleccionada.setTotalPisosUnidad(1);
		this.unidadDelModeloDeConstruccionSeleccionada.setTotalPisosConstruccion(1);
		this.unidadDelModeloDeConstruccionSeleccionada.setTotalPisosConstruccion(1);
		this.unidadDelModeloDeConstruccionSeleccionada.setAreaConstruida(0D);

		this.unidadDelModeloDeConstruccionSeleccionada
				.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

		this.cargarUnidadDelModelo();

		// Set del uso de construcción
		// this.setSelectedUsoUnidadConstruccionId(0L);
		// this.actualizarArbolDeComponentes();
		this.listPFotosUnidadDelModelo = new ArrayList<PModeloConstruccionFoto>();

		this.listaPModeloConstruccionFotoAGuardar = new ArrayList<PModeloConstruccionFoto>();
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que inicializa las variables para crear un modelo de construcción
	 * nuevo.
	 *
	 * @author david.cifuentes
	 */
	public void setupAdicionModeloNuevo() {

		this.tipoModelo = "";
		this.tipoDeModelo = "";

		// D: se crea un nuevo modelo de construcción y se le
		// asignan valores.
		this.modeloUnidadDeConstruccionSeleccionado = new PFichaMatrizModelo();

		// D: datos temporales (los que no pueden ser nulos y no tienen default)
		if (this.fichaMatrizMB == null) {
			this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
		}

		if (this.fichaMatrizMB.getFichaMatriz() != null) {

			this.modeloUnidadDeConstruccionSeleccionado.setPFichaMatriz(this.fichaMatrizMB.getFichaMatriz());
		} else {
			List<PFichaMatriz> fichasMatricesTramite = this.getTramiteService()
					.obtenerFichasAsociadasATramite(this.tramite.getId());

			if (fichasMatricesTramite != null && !fichasMatricesTramite.isEmpty()) {
				for (PFichaMatriz pFichaMatriz : fichasMatricesTramite) {
					if (pFichaMatriz.getEstado() != null
							&& EPredioEstado.ACTIVO.getCodigo().equals(pFichaMatriz.getEstado())) {

						this.modeloUnidadDeConstruccionSeleccionado.setPFichaMatriz(pFichaMatriz);
						break;
					}
				}
			}
		}

		this.modeloUnidadDeConstruccionSeleccionado.setNombre("");
		this.modeloUnidadDeConstruccionSeleccionado.setAreaConstruida(0D);
		this.modeloUnidadDeConstruccionSeleccionado.setAreaTerreno(0D);
		this.modeloUnidadDeConstruccionSeleccionado.setNumeroUnidades(0D);

		this.modeloUnidadDeConstruccionSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
		this.listUnidadesDelModeloDeConstruccion = new ArrayList<PFmModeloConstruccion>();

	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que se encarga de actualizar el arbol de componentes mostrado en la
	 * pantalla cada vez que se cambie el tipo de calificación de la unidad del
	 * modelo de construcción.
	 *
	 * @author david.cifuentes
	 */
	public void actualizarArbolDeComponentes() {

		if (this.selectedUsoUnidadConstruccion != null
				&& this.selectedUsoUnidadConstruccion.getDestinoEconomico() != null) {

			if (!this.selectedUsoUnidadConstruccion.getDestinoEconomico()
					.equals(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo())) {

				this.isAnexoCurrentOrNewUnidadConstruccion = false;
				this.isAnexoONuevaUnidadDelModelo = false;

				if (this.selectedUsoUnidadConstruccion.getDestinoEconomico()
						.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {

					this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion2;
					this.treeComponentesConstruccion = this.treeComponentesConstruccion2;

				} else if (this.selectedUsoUnidadConstruccion.getDestinoEconomico()
						.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {

					this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion3;
					this.treeComponentesConstruccion = this.treeComponentesConstruccion3;

				} else if (this.selectedUsoUnidadConstruccion.getDestinoEconomico()
						.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {

					this.treeComponentesConstruccionParaModelo = this.treeComponentesConstruccion1;
					this.treeComponentesConstruccion = this.treeComponentesConstruccion1;

				}

				if (this.unidadConstruccionSeleccionada != null
						&& this.unidadConstruccionSeleccionada.getAnioConstruccion() == null) {

					this.unidadConstruccionSeleccionada.setAnioConstruccion(this.getAnoActual());
				}
				if (this.unidadDelModeloDeConstruccionSeleccionada != null
						&& this.unidadDelModeloDeConstruccionSeleccionada.getAnioConstruccion() == null) {
					this.unidadDelModeloDeConstruccionSeleccionada.setAnioConstruccion(this.getAnoActual());
				}

				/*
				 * Pone por defecto los mas altos valores
				 */
				for (int i = 0; i < this.treeComponentesConstruccion.getChildCount(); i++) {
					TreeNode tn = this.treeComponentesConstruccion.getChildren().get(i);
					for (int j = 0; j < tn.getChildCount(); j++) {
						DefaultTreeNode dtn = (DefaultTreeNode) tn.getChildren().get(j);
						CalificacionConstruccionTreeNode cctn = (CalificacionConstruccionTreeNode) dtn.getData();

						if (cctn.getDetallesCalificacionItemList().size() > 0) {
							CalificacionConstruccion calificacionConstruccion = new CalificacionConstruccion();
							SelectItem si = cctn.getDetallesCalificacionItemList()
									.get(cctn.getDetallesCalificacionItemList().size() - 1);
							String[] objectAttributes = si.getValue().toString()
									.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

							calificacionConstruccion.setId(new Long(objectAttributes[0]));
							calificacionConstruccion.setComponente(objectAttributes[1]);
							calificacionConstruccion.setElemento(objectAttributes[2]);
							calificacionConstruccion.setDetalleCalificacion(objectAttributes[3]);
							calificacionConstruccion.setPuntos(new Integer(objectAttributes[4]).intValue());

							cctn.setSelectedDetalle(calificacionConstruccion);
						}
					}
				}
				/*
				 * Poner los componentes más altos para el modelo de construcción
				 */
				this.setupComponentesMayorValorUnidadDelModelo();
			} else {
				// this.unidadConstruccionSeleccionada.setAnioConstruccion(null);
				this.isAnexoCurrentOrNewUnidadConstruccion = true;
				this.isAnexoONuevaUnidadDelModelo = true;
			}
		}
		// this.informacionMigracionIncompleta = false;
		if (this.unidadConstruccionSeleccionada != null
				&& (this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps() == null
						|| this.unidadConstruccionSeleccionada.getPUnidadConstruccionComps().isEmpty())) {
			this.informacionMigracionIncompleta = true;
		} else {
			this.informacionMigracionIncompleta = false;
		}
	}

	// --------------------------------------------------------------------------
	// //
	/**
	 * Método que inicializa el arbol de componentes de una unidad del modelo con
	 * los valores más altos.
	 *
	 * @author david.cifuentes
	 */
	public void setupComponentesMayorValorUnidadDelModelo() {
		for (int i = 0; i < this.treeComponentesConstruccionParaModelo.getChildCount(); i++) {
			TreeNode tn = this.treeComponentesConstruccionParaModelo.getChildren().get(i);
			for (int j = 0; j < tn.getChildCount(); j++) {
				DefaultTreeNode dtn = (DefaultTreeNode) tn.getChildren().get(j);
				CalificacionConstruccionTreeNode cctn = (CalificacionConstruccionTreeNode) dtn.getData();

				if (cctn.getDetallesCalificacionItemList().size() > 0) {
					CalificacionConstruccion calificacionConstruccion = new CalificacionConstruccion();

					// Ahora se cambio y se quiere que aparezca seleccionado
					// siempre el valor por
					// defecto de seleccione javier.aponte 08-07-2013
					// Mofified by fredy.wilches 20140201 para que no aparezca
					// por defecto Seleccione, sino el primer elemento con valor
					// de la lista
					// SelectItem si =
					// cctn.getDetallesCalificacionItemList().get(
					// cctn.getDetallesCalificacionItemList().size() - 1);
					SelectItem si = cctn.getDetallesCalificacionItemList().size() > 1
							? cctn.getDetallesCalificacionItemList().get(1)
							: cctn.getDetallesCalificacionItemList().get(0);

					String[] objectAttributes = si.getValue().toString()
							.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);

					calificacionConstruccion.setId(new Long(objectAttributes[0]));
					calificacionConstruccion.setComponente(objectAttributes[1]);
					calificacionConstruccion.setElemento(objectAttributes[2]);
					calificacionConstruccion.setDetalleCalificacion(objectAttributes[3]);
					calificacionConstruccion.setPuntos(new Integer(objectAttributes[4]).intValue());

					cctn.setSelectedDetalle(calificacionConstruccion);
				}
			}
		}
	}

	// -------------------------------------------------------------------------
	// //
	/**
	 * Método que recupera el modelo de construcción en la selección del modelo de
	 * construcción. Nota: Hubo la necesidad de hacerlo de ésta manera.
	 *
	 * @author david.cifuentes
	 * @param event
	 */
	public void onSelectModeloRow(SelectEvent event) {
		this.modeloUnidadDeConstruccionSeleccionado = (PFichaMatrizModelo) event.getObject();
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método para la desSeleccion del modelo de construcción.
	 *
	 * @author david.cifuentes
	 * @param event
	 */
	public void onUnselectModeloRow(UnselectEvent event) {
		this.modeloUnidadDeConstruccionSeleccionado = null;
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que guarda la unidad del modelo de construcción y sus componentes.
	 * Acción sobre el botón "Guardar cambios" de la pantalla
	 * gestionDetalleUnidadDelModeloDeConstruccion.
	 *
	 * @author david.cifuentes
	 */
	public void guardarUnidadDelModeloDeConstruccion() {
		try {

			List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
			List<PFmConstruccionComponente> newPFmConstruccionComponenteList;
			PFmConstruccionComponente toInsert;
			CalificacionConstruccionTreeNode cctnTempL1, cctnTempL2;
			CalificacionConstruccion selectedDetalle;
			String currentComponenteName, currentElementoName;
			Date currentDate;
			boolean isAnexo = false;

			this.unidadDelModeloDeConstruccionSeleccionada.setUsoConstruccion(
					this.getGeneralesService().getCacheUsoConstruccionById(this.selectedUsoUnidadConstruccion.getId()));
			this.unidadDelModeloDeConstruccionSeleccionada.setTipoCalificacion(EUnidadConstruccionTipoCalificacion
					.getCodigoByNombre(this.selectedUsoUnidadConstruccion.getDestinoEconomico()));

			if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
					.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())
					|| this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
							.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())
					|| this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
							.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
				isAnexo = false;
			} else {
				isAnexo = true;
				this.unidadDelModeloDeConstruccionSeleccionada
						.setTipoConstruccion(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo());
				this.unidadDelModeloDeConstruccionSeleccionada.setTotalPuntaje(Double.valueOf(0));
				this.unidadDelModeloDeConstruccionSeleccionada
						.setTipificacion(EUnidadConstruccionTipificacion.NA.getCodigo());
				this.unidadDelModeloDeConstruccionSeleccionada.setAreaConstruida(this.areaNoConvencional);
				if (this.selectedCalificacionAnexo != null) {
					this.unidadDelModeloDeConstruccionSeleccionada
							.setCalificacionAnexoId(this.selectedCalificacionAnexo.getId());
					this.unidadDelModeloDeConstruccionSeleccionada
							.setDescripcion(this.selectedCalificacionAnexo.getDescripcion());
				}
			}

			if (!isAnexo) {
				this.unidadDelModeloDeConstruccionSeleccionada
						.setTipoConstruccion(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo());
				this.unidadDelModeloDeConstruccionSeleccionada
						.setTotalPuntaje(Double.valueOf(this.getTotalPuntosConstruccionUnidadDelModelo()));

				if (this.unidadDelModeloDeConstruccionSeleccionada.getTipoCalificacion()
						.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {

					this.unidadDelModeloDeConstruccionSeleccionada.setTipificacion(EUnidadConstruccionTipificacion.ANY
							.getCodigoByValor(this.getTipificacionUnidadDelModeloDeConstruccion()));
				} else {
					this.unidadDelModeloDeConstruccionSeleccionada
							.setTipificacion(EUnidadConstruccionTipificacion.NA.getCodigo());
				}
			} else {
				this.unidadDelModeloDeConstruccionSeleccionada
						.setTotalPuntaje(Double.valueOf(this.tipoDescripcionConstruccion));
			}

			// D: si se creó una nueva unidad para el modelo de construcción,
			// se guarda en la bd y se obtiene el id para poder guardar los
			// datos en la tabla relacionada
			LOGGER.debug("almacenando datos de la unidad del modelo de construcción...");
			this.unidadDelModeloDeConstruccionSeleccionada = (PFmModeloConstruccion) this.getConservacionService()
					.guardarProyeccion(this.usuario, this.unidadDelModeloDeConstruccionSeleccionada);
			LOGGER.debug("... Datos de la unidad del modelo de construcción almacenados.");

			// ======================================= //
			// Guardar fotos pendientes por guardar
			if (this.listaPModeloConstruccionFotoAGuardar != null
					&& !this.listaPModeloConstruccionFotoAGuardar.isEmpty()) {
				for (PModeloConstruccionFoto pmcf : this.listaPModeloConstruccionFotoAGuardar) {
					pmcf.setPFmModeloConstruccion(this.unidadDelModeloDeConstruccionSeleccionada);
					currentPModeloConstruccionFoto = this.getConservacionService().guardarPModeloConstruccionFoto(pmcf,
							this.usuario);

					adicionarPModeloConstruccionFotoALista();
				}
			}
			// ======================================= //

			// D: si no es un anexo, se guardan los registros correspondientes
			// en la tabla P_FM_CONSTRUCCION_COMPONENTE
			if (!isAnexo) {
				currentDate = new Date();
				newPFmConstruccionComponenteList = new ArrayList<PFmConstruccionComponente>();
				calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccionParaModelo.getChildren();

				for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {

					cctnTempL1 = (CalificacionConstruccionTreeNode) currentComponenteNode.getData();
					currentComponenteName = cctnTempL1.getCalificacionConstruccion().getComponente();

					calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
					for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
						cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
						currentElementoName = cctnTempL2.getCalificacionConstruccion().getElemento();
						selectedDetalle = cctnTempL2.getSelectedDetalle();

						// D: armar el objeto que se va a insertar...
						toInsert = new PFmConstruccionComponente();
						toInsert.setPFmModeloConstruccion(this.unidadDelModeloDeConstruccionSeleccionada);
						toInsert.setComponente(currentComponenteName);
						toInsert.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
						toInsert.setElementoCalificacion(currentElementoName);
						toInsert.setDetalleCalificacion(selectedDetalle.getDetalleCalificacion());
						toInsert.setPuntos(new Double(selectedDetalle.getPuntos()));
						toInsert.setFechaLog(currentDate);
						toInsert.setUsuarioLog(this.usuario.getLogin());

						newPFmConstruccionComponenteList.add(toInsert);
					}
				}

				// D: se insertan o modifican los registros de la tabla
				// P_FM_CONSTRUCCION_COMPONENTE
				LOGGER.debug("insertando o actualizando registros en P_FM_CONSTRUCCION_COMPONENTE ...");
				if (!this.unidadDelModeloEditModeBool) {
					this.getConservacionService()
							.insertarPFmConstruccionComponenteRegistros(newPFmConstruccionComponenteList);
				} else {
					this.getConservacionService()
							.actualizarPFmConstruccionComponenteRegistros(newPFmConstruccionComponenteList);
				}
				LOGGER.debug("... registros insertados o actualizados en P_FM_CONSTRUCCION_COMPONENTE.");
			} // D: ... si es anexo (no convencional), puede que antes haya sido
				// convencional, lo que implicaría que existan registros en la tabla
				// P_FM_CONSTRUCCION_COMPONENTE.. Se revisa si hay y se borran.
			else {
				this.getConservacionService().borrarPFmConstruccionComponenteRegistros(
						this.unidadDelModeloDeConstruccionSeleccionada.getId());
			}

			// D: se adiciona a la lista de las PUnidadConstruccion que se
			// pintan en gestionDetalleAvaluo
			this.listUnidadesDelModeloDeConstruccion = this.modeloUnidadDeConstruccionSeleccionado
					.getPFmModeloConstruccions();
			if (this.unidadDelModeloDeConstruccionSeleccionada != null
					&& this.modeloUnidadDeConstruccionSeleccionado != null) {
				if (this.listUnidadesDelModeloDeConstruccion == null
						|| this.listUnidadesDelModeloDeConstruccion.isEmpty()) {
					this.modeloUnidadDeConstruccionSeleccionado
							.setPFmModeloConstruccions(new ArrayList<PFmModeloConstruccion>());
					this.listUnidadesDelModeloDeConstruccion = this.modeloUnidadDeConstruccionSeleccionado
							.getPFmModeloConstruccions();
				}
				if (this.unidadDelModeloEditModeBool) {
					PFmModeloConstruccion auxRemove = null;
					for (PFmModeloConstruccion pfmmc : this.listUnidadesDelModeloDeConstruccion) {
						if (pfmmc.getId().longValue() == this.unidadDelModeloDeConstruccionSeleccionada.getId()
								.longValue()) {
							auxRemove = pfmmc;
							break;
						}
					}
					if (auxRemove != null) {
						this.listUnidadesDelModeloDeConstruccion.remove(auxRemove);
					}
					this.listUnidadesDelModeloDeConstruccion.add(this.unidadDelModeloDeConstruccionSeleccionada);
					this.addMensajeInfo("Unidad del modelo de construcción modificada exitosamente");
				} else {
					this.listUnidadesDelModeloDeConstruccion.add(this.unidadDelModeloDeConstruccionSeleccionada);
					this.addMensajeInfo("Unidad del modelo de construcción creada exitosamente");
				}
			}

			// D: si se adicionó o modificó alguna foto para la unidad de
			// construcción
			if (this.listPFotosUnidadDelModelo != null && !this.listPFotosUnidadDelModelo.isEmpty()) {

				for (PModeloConstruccionFoto pFotoUnidadDelModelo : this.listPFotosUnidadDelModelo) {
					if (null == pFotoUnidadDelModelo.getPFmModeloConstruccion()
							|| null == pFotoUnidadDelModelo.getPFmModeloConstruccion().getId()) {
						pFotoUnidadDelModelo.setPFmModeloConstruccion(this.unidadDelModeloDeConstruccionSeleccionada);
					}

					// D: al llamar a este método se crea o modifica el
					// Documento asociado, y se crea o modifica la PFoto.
					// D: si el archivo local del Documento de la
					// PModeloConstruccionFoto está vacío quiere decir que no
					// fue cargado como foto, es decir que no es nueva foto, y
					// por lo tanto no se debe guardar
					if (null != pFotoUnidadDelModelo.getFotoDocumento().getRutaArchivoLocal()
							&& !pFotoUnidadDelModelo.getFotoDocumento().getRutaArchivoLocal().isEmpty()) {
						pFotoUnidadDelModelo = this.getConservacionService()
								.guardarPFotoUnidadDelModelo(pFotoUnidadDelModelo, this.usuario);
					}
				}
			}

			// D: eliminar las PModeloConstruccionFotos modificadas. Para borrar
			// los registros antiguos y los archivos correspondientes en el
			// gestor documental
			if (this.listaPModeloConstFotoABorrar != null && !this.listaPModeloConstFotoABorrar.isEmpty()) {
				this.getConservacionService().borrarPModeloConstruccionFotos(this.listaPModeloConstFotoABorrar);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			this.addMensajeError("Error interno al guardar la unidad de construcción para éste modelo.");
		}
	}

	// ---------------------------------------------------------------------------
	/**
	 * Método que guarda el {@link PFichaMatrizModelo}.
	 *
	 * @author david.cifuentes
	 */
	public void guardarModeloDeConstruccion() {
		try {
			if (this.tipoDeModelo == null || this.tipoDeModelo.trim().isEmpty()) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.addCallbackParam("error", "error");
				this.addMensajeError("Por favor ingrese un valor para el tipo de modelo");
				return;
			}

			if (this.listModelosUnidadDeConstruccion != null && !this.listModelosUnidadDeConstruccion.isEmpty()) {

				for (PFichaMatrizModelo pFichaMatrizModelo : this.listModelosUnidadDeConstruccion) {
					if (pFichaMatrizModelo.getNombre().equals(this.tipoDeModelo)) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("El modelo de construcción ya ha sido asignado anteriormente, verifique.");
						return;

					}
				}
			}

			this.modeloUnidadDeConstruccionSeleccionado.setNombre(this.tipoDeModelo);

			this.modeloUnidadDeConstruccionSeleccionado.setUsuarioLog(this.usuario.getLogin());
			this.modeloUnidadDeConstruccionSeleccionado.setFechaLog(new Date(System.currentTimeMillis()));
			this.modeloUnidadDeConstruccionSeleccionado = this.getConservacionService()
					.guardarModeloDeConstruccion(this.modeloUnidadDeConstruccionSeleccionado);

			if (this.modeloUnidadDeConstruccionSeleccionado != null) {
				if (this.listModelosUnidadDeConstruccion == null) {
					this.listModelosUnidadDeConstruccion = new ArrayList<PFichaMatrizModelo>();
				}

				if (editModeloBool) {
					// Remover el modelo desactualizado
					PFichaMatrizModelo auxRemove = null;
					for (PFichaMatrizModelo pfmm : this.listModelosUnidadDeConstruccion) {
						if (pfmm.getId().longValue() == this.modeloUnidadDeConstruccionSeleccionado.getId()
								.longValue()) {
							auxRemove = pfmm;
							break;
						}
					}
					if (auxRemove != null) {
						this.listModelosUnidadDeConstruccion.remove(auxRemove);
					}
				}

				// Adicionar el modelo guardado
				this.listModelosUnidadDeConstruccion.add(this.modeloUnidadDeConstruccionSeleccionado);

				getListModelosUnidadDeConstruccion();

				if (this.fichaMatrizMB == null) {
					this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
				}

				if (this.fichaMatrizMB.getFichaMatriz() != null) {

					this.modeloUnidadDeConstruccionSeleccionado.setPFichaMatriz(this.fichaMatrizMB.getFichaMatriz());
				} else {
					List<PFichaMatriz> fichasMatricesTramite = this.getTramiteService()
							.obtenerFichasAsociadasATramite(this.tramite.getId());

					if (fichasMatricesTramite != null && !fichasMatricesTramite.isEmpty()) {
						for (PFichaMatriz pFichaMatriz : fichasMatricesTramite) {
							if (pFichaMatriz.getEstado() != null
									&& EPredioEstado.ACTIVO.getCodigo().equals(pFichaMatriz.getEstado())) {

								this.fichaMatrizMB.getFichaMatriz();
								break;
							}
						}
					}
				}
				this.fichaMatrizMB.getFichaMatriz().setPFichaMatrizModelos(this.listModelosUnidadDeConstruccion);

				if (tramite.isEnglobeVirtual()) {
					this.cargarModelosDeConstruccionExistentes();
				}

				this.addMensajeInfo("Se guarda el modelo de construcción satisfactoriamente!");

				this.editModeloBool = true;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al guardar el modelo de construcción.");
		}

		// Revisar si la ficha matriz quedó con modelos asociados.
		if (this.listModelosUnidadDeConstruccion != null && this.listModelosUnidadDeConstruccion.size() > 0) {
			this.existenModelosBool = true;
		} else {
			this.existenModelosBool = false;
		}
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que retorna la tipificación de la unidad del modelo de construcción
	 * dependiendo del puntaje total y de la clasificación correspondiente en la
	 * tabla TIPIFICACION_UNIDAD_CONST
	 *
	 * @author david.cifuentes
	 */
	public String getTipificacionUnidadDelModeloDeConstruccion() {
		TipificacionUnidadConst tuc = this.getConservacionService()
				.obtenerTipificacionUnidadConstrPorPuntos(this.getTotalPuntosConstruccionUnidadDelModelo());
		return tuc.getTipificacion();
	}

	// --------------------------------------------------------------------- //
	/**
	 * Método que inicializa las variables para el cargue de una
	 * PModeloConstruccionFoto.
	 *
	 * @author david.cifuentes
	 */
	public void initCargarFotoUnidadDelModelo() {

		String rutaTemp;
		this.currentPModeloConstruccionFoto = new PModeloConstruccionFoto();
		this.currentPModeloConstruccionFoto.setFecha(new Date());

		// D: se hace aquí siempre. Si resulta que es modificación de la foto,
		// más adelante se valida aquello y se cambia el valor
		this.currentPModeloConstruccionFoto.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

		// D: es por el campo 'descripcion' que se distinguen las fotos de
		// componentes una unidad de construcción.
		if (!this.isAnexoONuevaUnidadDelModelo) {
			this.currentPModeloConstruccionFoto.setDescripcion(
					this.currentCalificacionConstruccionNode.getCalificacionConstruccion().getComponente());
		} else {
			this.currentPModeloConstruccionFoto.setDescripcion(EUnidadConstruccionTipoCalificacion.ANEXO.getCodigo());
		}

		rutaTemp = "";
		if (this.currentCalificacionConstruccionNode != null
				&& null != this.currentCalificacionConstruccionNode.getUrlArchivoWebTemporal()) {
			rutaTemp = this.currentCalificacionConstruccionNode.getUrlArchivoWebTemporal();
		}

		// Revisar si la imagen que se tiene para esta calificación construcción
		// es diferente de la imagen base, de ser así mostrarla.
		this.existeImagen = false;
		this.existeImagen = ((!rutaTemp.isEmpty() && rutaTemp.compareToIgnoreCase(this.rutaWebImagenBase) != 0
				&& !this.isAnexoONuevaUnidadDelModelo)
				|| (null != this.rutaWebTemporalFotoAnexo && !this.rutaWebTemporalFotoAnexo.isEmpty()
						&& this.rutaWebTemporalFotoAnexo.compareToIgnoreCase(this.rutaWebImagenBase) != 0
						&& this.isAnexoONuevaUnidadDelModelo)) ? true : false;

		if (this.isAnexoONuevaUnidadDelModelo) {
			this.rutaWebTemporalFoto = this.rutaWebTemporalFotoAnexo;
		} else {
			this.rutaWebTemporalFoto = rutaTemp;
		}

		this.banderaBotonAceptarCargarFoto = false;
		this.errorConfirmarDatosCargarFotografia = false;
		this.setMensajeFotos("La imagen no tiene la información requerida");

	}

	// ----------------------------------------------------------- //
	/**
	 * Método que termina de guardar una foto asociada a un componente de la unidad
	 * del modelo de construcción.
	 *
	 * @author david.cifuentes
	 */
	public void terminarGuardadoFotoCompUnidadDelModelo() {
		try {

			String tipoFoto;
			Documento documento;

			// D: setear valores a la nueva PFoto
			this.currentPModeloConstruccionFoto.setFechaLog(new Date(System.currentTimeMillis()));
			this.currentPModeloConstruccionFoto.setUsuarioLog(this.usuario.getLogin());

			tipoFoto = this.getTipoFotoPorComponenteConstruccion();
			// D: es por el tipo de foto que se diferencian las fotos de cada
			// componente-elemento
			this.currentPModeloConstruccionFoto.setTipo(tipoFoto);

			if (!this.isAnexoONuevaUnidadDelModelo) {
				// D: le asigna al nodo actual la ruta del archivo local para
				// que se
				// pueda ver la miniatura de la foto en el árbol
				this.currentCalificacionConstruccionNode.setRutaArchivoLocal(this.rutaTemporalFoto);
				this.currentCalificacionConstruccionNode.setUrlArchivoWebTemporal(this.rutaWebTemporalFoto);

			} else {
				this.rutaWebTemporalFotoAnexo = this.rutaWebTemporalFoto;
				this.rutaLocalFotoAnexo = this.rutaTemporalFoto;
			}

			// D: se crea el Documento que irá asociado a la PFoto
			documento = nuevoDocumento();
			documento.setArchivo(UtilidadesWeb.obtenerNombreSinTemporalArchivo(this.rutaTemporalFoto));
			documento.setRutaArchivoLocal(this.rutaTemporalFoto);

			this.currentPModeloConstruccionFoto.setFotoDocumento(documento);
			this.currentPModeloConstruccionFoto
					.setPFmModeloConstruccion(this.unidadDelModeloDeConstruccionSeleccionada);

			adicionarPModeloConstruccionFotoALista();

			this.existeImagen = false;
			this.errorConfirmarDatosCargarFotografia = false;

		} catch (Exception e) {
			LOGGER.debug(e.getMessage(), e);
		}
	}

	// ---------------------------------------------------------------- //
	/**
	 * Método que adiciona una PModeloConstruccionFoto a la lista que se va a
	 * guardar. Revisa si, por nombrede componente, ya existe una foto para éste, en
	 * cuyo caso la elimina. Al guardar, primero se borran todos los registros de
	 * PModeloConstruccionFoto y luego se insertan los nuevos.
	 *
	 * @author david.cifuentes
	 */
	private void adicionarPModeloConstruccionFotoALista() {

		String currentPFotoComponenteName, tempPFotoComponenteName;
		String[] descripcionPartida;

		descripcionPartida = this.currentPModeloConstruccionFoto.getDescripcion()
				.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);
		currentPFotoComponenteName = descripcionPartida[0];

		if (this.listPFotosUnidadDelModelo == null) {
			this.listPFotosUnidadDelModelo = new ArrayList<PModeloConstruccionFoto>();
		}
		for (PModeloConstruccionFoto tempPModeloConstruccionFoto : this.listPFotosUnidadDelModelo) {
			descripcionPartida = tempPModeloConstruccionFoto.getDescripcion()
					.split(Constantes.CONVERTERS_TOSTRING_STRINGSEPARATOR);
			tempPFotoComponenteName = descripcionPartida[0];

			if (tempPFotoComponenteName.equals(currentPFotoComponenteName)) {

				// D: si la nueva PFoto reemplaza a una anterior, se debe, al
				// terminar la operación de
				// creación o modificación de la unidad de construcción, borrar
				// la que existía (en bd y en alfresco)
				if (this.listaPModeloConstFotoABorrar == null) {
					this.listaPModeloConstFotoABorrar = new ArrayList<PModeloConstruccionFoto>();
				}
				this.listaPModeloConstFotoABorrar.add(tempPModeloConstruccionFoto);
				this.listPFotosUnidadDelModelo.remove(tempPModeloConstruccionFoto);
				if (!this.unidadDelModeloEditModeBool) {
					this.currentPModeloConstruccionFoto
							.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				}
			}
		}
		this.listPFotosUnidadDelModelo.add(this.currentPModeloConstruccionFoto);
	}

	// --------------------------------------------------------------- //
	/**
	 * Método que controla el evento cerrar de la pantalla modal
	 * tablaUnidadesDelModeloDeConstruccion usado para reiniciar algunas variables.
	 *
	 * @author david.cifuentes
	 */
	public void cerrarModelosConstruccion(org.primefaces.event.CloseEvent event) {
		this.tipoDeModelo = "";
		this.modeloUnidadDeConstruccionSeleccionado = null;
	}

	// ---------------------------------------------------------- //
	// ----------------------- SECCIONES ------------------------ //
	// ---------------------------------------------------------- //
	/**
	 * Listener ejecutado en el cambio de tab en la página de proyección de una
	 * mutación
	 *
	 * @param event
	 */
	public void onTabChange(TabChangeEvent event) {

		String tabId = event.getTab().getId();

		this.pestanaFichaMatrizSeleccionada = false;

		if (tabId.equals("tabPropietarios1")) {
			tab = 4;
			((CambioPropietarioMB) UtilidadesWeb.getManagedBean("cambioPropietario")).init();
		}
		if (tabId.equals("tabFichaMatriz3")) {
			tab = 7;
			this.pestanaFichaMatrizSeleccionada = true;
			if (this.fichaMatrizSeleccionada != null) {

				if (this.fichaMatrizMB == null) {
					this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
				}

				// construcciones comunes paa los englobes virtuales
				if (this.tramite.isEnglobeVirtual()) {
					this.getGestionDetalleAvaluoMB().cargarConstruccionesEnglobeVirtual();
				}

				this.fichaMatrizMB.init();
				this.fichaMatrizMB.cargarMatrizNumerosPrediales();
				this.fichaMatrizMB.convertirAreasAHaM2();
				this.activeAsociarUnidadesBool = false;
			//	if (this.tramite.isEnglobeVirtual()) {
					// Carga Valores de Zonas a registrar e Historico
					this.getGestionDetalleAvaluoMB().cargaZonasVigentesPredio();
			//	}
			}
		}
		if (tabId.equals("tabTextoMotivado5")) {
			tab = 10;
			this.actualizarModeloResolucionSeleccionado();
		}
		if (tabId.equals("tabInscripcionesDecretos6")) {
			tab = 9;
			this.buscarInscripcionesYDecretosAsociadosAlPredio();
		}
		if (tabId.equals("tabAsociarDocumentos7")) {
			tab = 11;
			((AsociarDocumentosATramiteExistenteMB) UtilidadesWeb.getManagedBean("asociacionDocsTramExist")).init();
		}
		if (tabId.equals("tabProyeccion8")) {
			tab = 1;

			if (!this.tramite.isCancelacionPredio() && !this.tramite.isQuinta() && !this.tramite.isSegundaEnglobe()
					&& !this.tramite.isModificacionInscripcionCatastral() && !this.tramite.isRectificacionCDI()) {
				this.predioSeleccionado = null;
			}

			if (tramite.isSegundaDesenglobe()) {
				cargarPrediosDesenglobe();
			} else if (tramite.isSegundaEnglobe()) {
				cargarPrediosEnglobe();
			} else if (tramite.isTerceraMasiva() || tramite.isRectificacionMatriz()) {
				cargarPrediosTerceraRectificacionMasiva();
			} else if (tramite.isCancelacionMasiva()) {
				this.mostrarFichaMatrizRectificacion = false;
				this.predioSeleccionado = null;
			} else if (tramite.isQuintaMasivo()) {
				cargarPrediosQuintaMasivo();
				this.predioSeleccionado = null;
			}
			((EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion")).init();
		}
		if (tabId.equals("tabDetalleAvaluo4")) {
			tab = 8;
			this.existenConstruccionesVigentesBool = false;
			if (this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.getCondicionPropiedad()
					.equals(EPredioCondicionPropiedad.CP_0.getCodigo())) {
				this.cargarConstruccionesDesenglobe();
			} else if (this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.getCondicionPropiedad()
					.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
				this.cargarConstruccionesDesenglobeMejora();
			} else if (this.tramite.isTerceraMasiva()) {

				// javier.aponte :: Agrega las unidades de construcción vigentes
				this.unidadesConstruccionConvencional = new ArrayList<UnidadConstruccion>();
				this.unidadesConstruccionNoConvencional = new ArrayList<UnidadConstruccion>();

				this.predioOrigen = this.getConservacionService()
						.obtenerPredioConPersonasPorPredioId(this.predioSeleccionado.getId());

				if (this.predioOrigen.getUnidadesConstruccionConvencional() != null
						&& !this.predioOrigen.getUnidadesConstruccionConvencional().isEmpty()) {
					for (UnidadConstruccion uni : this.predioOrigen.getUnidadesConstruccionConvencional()) {
						if (uni.getAnioCancelacion() == null) {
							this.unidadesConstruccionConvencional.add(uni);
						}
					}
				}

				if (this.predioOrigen.getUnidadesConstruccionNoConvencional() != null
						&& !this.predioOrigen.getUnidadesConstruccionNoConvencional().isEmpty()) {
					for (UnidadConstruccion uni : this.predioOrigen.getUnidadesConstruccionNoConvencional()) {
						if (uni.getAnioCancelacion() == null) {
							this.unidadesConstruccionNoConvencional.add(uni);
						}
					}
				}

				revisarExistenciaConstruccionesVigentes();
			} else if (this.tramite.isCancelacionMasiva()) {

				this.getGestionDetalleAvaluoMB().cargarConstruccionesCancelacionMasiva();

			} else if (this.tramite.isEnglobeVirtual()) {

				this.getGestionDetalleAvaluoMB().cargarConstruccionesEnglobeVirtual();
				// Carga Valores de Zonas a registrar e Historico
				this.getGestionDetalleAvaluoMB().cargaZonasVigentesPredio();

			} else if (this.tramite.isRectificacionArea()) {
				// Carga Valores de Zonas a registrar e Historico
				this.getGestionDetalleAvaluoMB().cargaZonasVigentesPredio();
			} else {
				revisarExistenciaConstruccionesVigentes();
			}
			// Validacion necesaria con predios fiscales para habilitar los
			// datos de entrada
			// de los avaluós
			if (this.banderaPredioFiscal) {

				if (!this.tramite.isQuintaNuevo()) {
					List<PredioAvaluoCatastral> lista = this.getConservacionService()
							.obtenerHistoricoAvaluoPredio(this.tramite.getPredio().getId());
					PredioAvaluoCatastral av = new PredioAvaluoCatastral();
					if (!lista.isEmpty()) {
						av = lista.get(lista.size() - 1);

						if (av.getValorTerreno() == 0 && av.getValorTotalConstruccion() == 0
								&& !this.tramite.isEsComplementacion() && !this.tramite.isRevisionAvaluo()
								&& !this.tramite.isRectificacion()) {

							this.banderaAvaluoManualFiscales = true;
						}

						if (av.getValorTerreno() != 0 && this.tramite.isSegundaDesenglobe()) {
							this.banderaAvaluoSoloArea = true;
							this.banderaAvaluoManualFiscales = true;

							if (this.getPredioSeleccionado().isEsPredioEnPH()) {
								this.banderaAvaluoSoloArea = false;
								this.banderaAvaluoManualFiscales = false;
							}
						} else {
							if (this.getPredioSeleccionado().isEsPredioEnPH()) {
								this.banderaAvaluoSoloArea = false;
								this.banderaAvaluoManualFiscales = false;
							}
						}

						if (this.banderaPredioFiscal && this.tramite.isRectificacionArea()) {
							this.banderaAvaluoSoloArea = true;
							this.banderaAvaluoManualFiscales = true;
						}
					}

					if (this.predioSeleccionado.getAreaTerreno() == 0) {
						this.valorMetroFiscal = 0.0;
					} else {
						this.areaTotalTerrenoUnidadF = this.predioSeleccionado.getAreaTerreno();
						this.getAreaTotalTerrenoUnidad();
						// this.areaTotalTerrenoUnidad=this.predioSeleccionado.getAreaTerreno();
						this.valorMetroFiscal = this.avaluo.getValorTerreno()
								/ this.predioSeleccionado.getAreaTerreno();
					}
				} else {
					if (this.avaluo.getValorTerreno() != null && this.predioSeleccionado.getAreaTerreno() != null) {
						if (this.predioSeleccionado.getAreaTerreno() == 0) {
							this.valorMetroFiscal = 0.0;
						} else {
							this.valorMetroFiscal = this.avaluo.getValorTerreno()
									/ this.predioSeleccionado.getAreaTerreno();
						}
					}
				}

				/*
				 * Para los tramites de quinta omitido de los cuales se logra recuperar la fecha
				 * de inscripcion catrastral, se realiza la liquidacion hasta el año vigente.
				 * Para los que no se debe hacer manualmente.
				 */
				// if (this.tramite.isQuintaOmitido()) {
				// Predio pred =
				// this.getConservacionService().obtenerPredioPorId(this.predioSeleccionado.getId());
				// LiquidacionAvaluoPredioFiscalMB liquidacionFiscal = new
				// LiquidacionAvaluoPredioFiscalMB();
				//
				// if (pred.getFechaInscripcionCatastral() != null) {
				//
				// this.avaluo = liquidacionFiscal
				// .calcAvaluoQuintaOmitido(this.areaTotalTerrenoUnidadF,
				// this.tramite, this.predioSeleccionado,
				// this.usuario, this.avaluo,
				// pred.getFechaInscripcionCatastral().toString());
				// } else if (!getConservacionService()
				// .obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId()).isEmpty())
				// {
				//
				// //verifica si tiene avalúos proyectados y lo lleva hasta el
				// año actual
				// List<PPredioAvaluoCatastral> listaAvaluos =
				// getConservacionService()
				// .obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());
				// PPredioAvaluoCatastral avaluoUltimaVigencia =
				// listaAvaluos.get(0);
				//
				// this.avaluo = liquidacionFiscal
				// .calcAvaluoQuintaOmitido(this.areaTotalTerrenoUnidadF,
				// this.tramite, this.predioSeleccionado,
				// this.usuario, this.avaluo,
				// avaluoUltimaVigencia.getVigencia().toString());
				// this.banderaSinFechaInscripcionFiscal = false;
				// } else {
				// this.banderaSinFechaInscripcionFiscal = true;
				// this.banderaAvaluoManualFiscales = true;
				// }
				// }
				// if (this.predioSeleccionado.isPredioRural()) {
				// this.areaTotalTerrenoUnidadF =
				// Double.parseDouble(this.getAreaTotalTerrenoUnidad().replace("ha",
				// ""));
				// } else {
				// this.areaTotalTerrenoUnidadF =
				// Double.parseDouble(this.getAreaTotalTerrenoUnidad().replace("m2",
				// ""));
				// }
			}

			if (this.tramite.isEnglobeVirtual()) {
				// Carga Valores de Zonas a registrar e Historico
				this.getGestionDetalleAvaluoMB().cargaZonasVigentesPredio();
			}

		}
		if (tabId.equals("tabHistoricoModificaciones9")) {
			tab = 0;
		}
		if (tabId.equals("tabProyeccion8b")) {
			tab = 2;
		}
		if (tabId.equals("tabProyeccion8c")) {
			tab = 3;
		}
		if (tabId.equals("tabJustificacion0")) {
			tab = 5;
		}
		if (tabId.equals("tabDatosGenerales2")) {
			tab = 6;
		}
		if (tabId.equals("tabImagenes")) {
			tab = 12;
		}
		// Se invoca para que calcule solo una vez
		this.getTabs();
		int tabOriginal = tab;
		for (int i = 0; i < tabOriginal; i++) {
			if (!this.tabs[i]) {
				tab--;
			}
		}

	}

	// ---------------------------------------------------------- //
	/**
	 * Método que inicializa un HashMap de {@link ESeccionEjecucion} con las
	 * secciones y su validación.
	 */
	public void inicializarValidezSecciones() {
		this.getTabs();
		this.validezSeccion = new HashMap<ESeccionEjecucion, Boolean>();

		for (ESeccionEjecucion ese : ESeccionEjecucion.values()) {

			this.validezSeccion.put(ese, false);
			if (ese.toString().equals(ESeccionEjecucion.AVALUOS.toString()) && !tabs[8]) {
				this.validezSeccion.put(ese, true);
			}
			// La siguiente condicion se incluye porque puede que en la bd se
			// parametrice ficha matriz en SI para que aparezca pero solo debe
			// aparecer para ciertas condiciones
			if (ese.toString().equals(ESeccionEjecucion.FICHA_MATRIZ.toString()) && (!tabs[7] || (tabs[7]
					&& this.predioSeleccionado != null && !this.predioSeleccionado.isEsPredioFichaMatriz()))) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD.toString()) && !tabs[5]) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.PROPIETARIOS.toString()) && !tabs[4]) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.UBICACION.toString()) && !tabs[6]) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.TEXTO_MOTIVADO.toString()) && !tabs[10]) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.INSCRIPCIONES_DECRETOS.toString()) && !tabs[9]) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.PROYECCION_TRAMITE.toString()) && !tabs[2] && !tabs[3]
					&& !tabs[1]) {
				this.validezSeccion.put(ese, true);
			}

			// ASOCIAR_DOCUMENTOS aparece para todo tipo de tramite
			if (ese.toString().equals(ESeccionEjecucion.ASOCIAR_DOCUMENTOS.toString())) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionEjecucion.HISTORICO_MODIFICACIONES.toString())) {
				this.validezSeccion.put(ese, true);
			}
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que realiza un recorrido del HashTablde de {@link ESeccionEjecucion}
	 * para saber si las secciones son o no validas.
	 *
	 * @return
	 */
	/*
	 * @modified pedro.garcia 29-11-2013 cambio en la forma de recorrer las llaves
	 * por cambio en el tipo de variable
	 */
	public boolean isTodasSeccionesValidas() {
		boolean validasTodas = true;

		// Validación para trámites de via gubernativa que confirman
		if (this.tramite.isViaGubernativa() && !this.tramite.isTipoTramiteViaGubernativaModificado()) {
			this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, true);
		}

		for (ESeccionEjecucion ese : this.validezSeccion.keySet()) {
			Boolean valido = this.validezSeccion.get(ese);
			if (!valido) {
				LOGGER.debug("Sección no válida " + ese.toString());
				validasTodas = false;
			}

		}
		return validasTodas;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que guarda el trámite y realiza un llamado a los métodos
	 * correspondientes para la validación de las secciones activas en la proyección
	 * del trámite.
	 *
	 */
	/*
	 * @modified felipe.cadena -- Se agregan validaciones para complementaciones y
	 * rectificaciones.
	 * 
	 * @modified felipe.cadena::RM#11986::21-04-2015::Se agregan validaciones de
	 * area y avaluos para rectificaciones.
	 * 
	 * @modified juanfelipe.garcia :: 07-03-2014 :: refs 7185
	 */
	public void validarGuardar() {
		if (this.tramite.isEnglobeVirtual()) {
			ValidadorEnglobeVirtual validador = new ValidadorEnglobeVirtual(this.tramite);
			List<ValidacionProyeccionDelTramiteDTO> errores;
			if (this.tramite.tieneReplicaFinal()) {
				errores = validador.validar(true);
			} else {
				errores = validador.validar(false);
			}
			if (errores != null && !errores.isEmpty()) {
				this.listaValidacionesTramiteSegundaDesenglobe = errores;
				this.validarGuardarEVOK = validador.isValidacionPreGeograficaOK();
				// this.addErrorCallback();
				return;
			} else {
				this.validarGuardarEVOK = true;
			}
		}

		// felipe.cadena::RM#11986::21-04-2015::Se agregan validaciones de area y
		// avaluos para rectificaciones.
		if (this.tramite.isRectificacion()) {
			ValidadorAvaluoAreaRectificacion validador = new ValidadorAvaluoAreaRectificacion();
			PPredio predioValidar;
			if (this.predioSeleccionado == null) {
				try {
					predioValidar = this.getConservacionService()
							.obtenerPPredioCompletoById(this.tramite.getPredio().getId());
				} catch (Exception e) {
					this.addMensajeError(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_06.toString());
					LOGGER.error(ECodigoErrorVista.VALIDACION_AVALUOS_CC083_06.getMensajeTecnico(), e);
					return;
				}
			} else {
				predioValidar = this.predioSeleccionado;
			}
			if (!validador.validar(predioValidar, this.tramite.getTramiteRectificacions())) {
				return;
			}
		}

		// felipe.cadena :: Se valida no ingresar identificaciones de
		// propietarios en ceros #6747
		List<PPersonaPredio> propietariosNuevos = new ArrayList<PPersonaPredio>();
		if (this.getPredioSeleccionado() != null && this.getPredioSeleccionado().getPPersonaPredios() != null
				&& !this.getPredioSeleccionado().getPPersonaPredios().isEmpty()) {
			for (PPersonaPredio ppp : this.getPredioSeleccionado().getPPersonaPredios()) {
				if (ppp.getCancelaInscribe() == null
						|| ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo())
						|| ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
					propietariosNuevos.add(ppp);
				}
			}
		}
		for (PPersonaPredio ppp : propietariosNuevos) {
			// javier.aponte :: #13446 Se agrega validación para que el usuario
			// log de PPersona no sea MIG2015 :: 14-07-2015
			if (!ppp.getPPersona().getUsuarioLog().equals(Constantes.USUARIO_LOG_MIGRADOS)
					&& !Constantes.USUARIO_LOG_MIGRADOS_2015.equals(ppp.getPPersona().getUsuarioLog())) {
				String id = ppp.getPPersona().getNumeroIdentificacion();
				boolean ceros = Pattern.matches("0*", id);
				// Para tipo de identificación cedula de ciudadana se
				// permite que registre en el número de identificación el valor
				// cero (0)
				if (id.equals("0") // v1.1.9
						&& (ppp.getPPersona().getTipoIdentificacion()
								.equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo())
								|| ppp.getPPersona().getTipoIdentificacion()
										.equals(EPersonaTipoIdentificacion.NIT.getCodigo()))) {

					// @modified by leidy.gonzalez:: #17238 :: 20/06/2016
					// Se valida que los tramites: revisión de avalúo, mutación
					// tercera, complementación, cancelación de predio,
					// rectificación, autoestimaciones y primeras
					// no debe realizar la validación
					// "El número de identificación del propietario no puede contener solo ceros"
				} else if (ceros && !this.getTramite().isEsRectificacion() && !this.getTramite().isTercera()
						&& !this.getTramite().isPrimera() && !this.getTramite().isEsTramiteDeRevisionDeAvaluo()
						&& !this.getTramite().isEsComplementacion() && !this.getTramite().isCancelacionPredio()
						&& !this.getTramite().isTramiteDeAutoavaluo()) {
					this.addMensajeError("El número de identificación del propietario " + ppp.getPPersona().getNombre()
							+ " no puede contener solo ceros");
					return;
				}
			}
		}

		// Validación de suma de áreas de las construcciones
		if (this.tramite.isSegundaDesenglobe() && !this.validarAreasConstruccionesAsociadas()) {
			RequestContext context = RequestContext.getCurrentInstance();
			if (context != null) {
				context.addCallbackParam("error", "error");
			}
			return;
		}

		// #15193 :: david.cifuentes :: Para todos los trámites en los que se
		// modifiquen las áreas y esté asociado a un PH o Condominio se va a
		// realizar una actualización de áreas en la ficha matriz :: 22/12/2015
		if (this.tramite.isSegundaDesenglobe()
				&& (this.tramite.isTipoInscripcionPH() || this.tramite.isTipoInscripcionCondominio())
				&& this.fichaMatrizSeleccionada != null) {
			this.actualizarAreasFichaMatriz(true);
		}

		if (!this.validarTextoMotivo()) {
			return;
		}

		// Se establece la fecha de inscripción catastral que tiene el predio
		// seleccionado en el campo fecha inscripción catastral del trámite
		if (this.predioSeleccionado != null) {
			this.tramite.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
		}
		if (this.tramite.isCuarta()) {
			if (!this.avaluo.getAutoavaluo().equals(ESiNo.SI.getCodigo())
					&& !this.modeloResolucionSeleccionado.getTextoTitulo().contains("RECHAZA")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.addCallbackParam("error", "error");
				this.addMensajeError(
						"El campo Autoestimaciones debe ser seleccionado, para actualizar los valores de terreno");
				this.banderaAceptaAutoestimacion = false;
				return;
			} else {
				this.banderaAceptaAutoestimacion = true;
			}
		}
		if (this.tramite.isTercera()) {
			if (this.tramite.getPredio().isMejora()) {
				if (this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError("Los predios de mejora deben tener al menos una construcción asociada");
					return;
				} else {
					int k = 0;
					for (PUnidadConstruccion puc : this.predioSeleccionado.getPUnidadConstruccions()) {
						if (!puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
							k++;
						}
					}
					if (k == 0) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("Los predios de mejora deben tener al menos una construcción asociada");
						return;
					}
				}
			}
		}

		if (this.tramite.isEsRectificacion() || this.tramite.isEsComplementacion()) {
			// Se realizan las validaciones de complementación y rectificación
			ValidarCompYRect validador = new ValidarCompYRect();

			PPredio predioAux;

			if (!this.tramite.isRectificacionMatriz()) {
				predioAux = validador.validar(this.predioSeleccionado, this.tramite.getId());
			} else {
				PPredio predioMatriz;
				try {
					predioMatriz = this.getConservacionService()
							.obtenerPPredioCompletoById(this.tramite.getPredio().getId());
				} catch (Exception ex) {
					LOGGER.error("Error al consultar el predio ");
					return;
				}
				
				predioAux = validador.validar(predioMatriz, this.tramite.getId());
			}

			if (predioAux == null) {
				this.addMensajeError(validador.getMensajeError());
				return;
			} else {
				this.predioSeleccionado = predioAux;
			}
			if (this.tramite.isRectificacionZona()) {
				List<ProductoCatastralJob> jobsZonas = this.getGeneralesService().obtenerJobsPorEstadoTramiteIdTipo(
						null, this.tramite.getId(), ProductoCatastralJob.SIG_JOB_EXPORTAR_DATOS);
				if (jobsZonas != null && !jobsZonas.isEmpty()) {

					List<String> estados = new ArrayList<String>();
					estados.add(ProductoCatastralJob.AGS_EN_EJECUCION);
					estados.add(ProductoCatastralJob.AGS_ESPERANDO);
					estados.add(ProductoCatastralJob.AGS_TERMINADO);
					estados.add(ProductoCatastralJob.AGS_ERROR);
					estados.add(ProductoCatastralJob.SNC_ERROR);
					estados.add(ProductoCatastralJob.SNC_EN_EJECUCION);
					if (estados.contains(jobsZonas.get(jobsZonas.size() - 1).getEstado())) {
						this.addMensajeWarn(ECodigoErrorVista.RECTIFICACION_ZONAS_MASIVO_001.toString());
						LOGGER.warn(ECodigoErrorVista.RECTIFICACION_ZONAS_MASIVO_001.getMensajeTecnico());
						return;
					}
				} else {
					this.addMensajeError("Es necesario calcular las zonas.");
					return;
				}
			}
		}
		
		this.getTramiteService().guardarActualizarTramite(tramite);
		
		this.inicializarValidezSecciones();
		
		if (this.configuracion.getPropietarios().equals(ESiNo.SI.toString()) && this.propietariosMB == null) {
			this.propietariosMB = (CambioPropietarioMB) UtilidadesWeb.getManagedBean("cambioPropietario");
		}
		if (this.configuracion.getJustificacionPropiedad().equals(ESiNo.SI.toString())
				&& this.justificacionesMB == null) {
			this.justificacionesMB = (JustificacionProyeccionMB) UtilidadesWeb
					.getManagedBean("justificacionProyeccion");
		}
		if ((tabs[1] || tabs[2] || tabs[3]) && this.entradaProyeccionMB == null) {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
		}
		if (this.configuracion.getFichaMatriz().equals(ESiNo.SI.toString()) && this.fichaMatrizMB == null) {
			this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
		}

//		if (this.tramite.getTipoTramite().equals(
//				ETramiteTipoTramite.RECURSO_DE_REPOSICION.getCodigo())) {
		if (this.tramite.isViaGubernativa()) {
			if (tabs[10]) {
				this.guardarTramiteTextoResolucion();
				boolean resultadoValidacion = this.validarTextoMotivo();
				this.validezSeccion.put(ESeccionEjecucion.TEXTO_MOTIVADO, resultadoValidacion);
			}
		}

		if (this.predioSeleccionado != null) {

			if (!this.predioSeleccionado.isEsPredioFichaMatriz()) {
				if (tabs[4]) {
					this.propietariosMB.validarGuardar();
				}

				if (tabs[5]) {
					this.justificacionesMB.validarGuardar();
				}

				if (tabs[9]) {
					boolean resultadoValidacion = this.validarInscripcionesDecretos();
					this.validezSeccion.put(ESeccionEjecucion.INSCRIPCIONES_DECRETOS, resultadoValidacion);
				}

				if (tabs[10]) {
					this.guardarTramiteTextoResolucion();
					boolean resultadoValidacion = this.validarTextoMotivo();
					this.validezSeccion.put(ESeccionEjecucion.TEXTO_MOTIVADO, resultadoValidacion);
				}

				if (tabs[6]) {
					boolean resultadoValidacion = this.validarGenerales();
					this.validezSeccion.put(ESeccionEjecucion.UBICACION, resultadoValidacion);
				}

				if (tabs[8]) {
					boolean resultadoValidacion = this.validarAvaluo(this.predioSeleccionado);
					this.validezSeccion.put(ESeccionEjecucion.AVALUOS, resultadoValidacion);
				}

				if (tabs[1] || tabs[2] || tabs[3]) {
					this.entradaProyeccionMB.validarGuardar();
				}
			} else {
				if (tabs[7] && this.predioSeleccionado.isEsPredioFichaMatriz() && !this.banderaDesenglobeMejora) {
					this.fichaMatrizMB.validarSeccionFichaMatriz();
				}
			}

			if (this.tramite.isRectificacionMatriz()) {
				this.validarProyeccion();
			}

			// Validaciones para englobes con predios fiscales ya que las
			// geograficas no aplican
			// se aclara que únicamente son validaciones con respecto al avalúo,
			// las correspondientes
			// a los demas TABs funcionan correctamente
			if (this.banderaPredioFiscal && this.tramite.isSegundaEnglobe()) {

				double totalAreaProyectadas = 0.0;
				double totalAreaOriginales = 0.0;
				double totalAvaluoOriginal = 0.0;

				for (Predio p : this.tramite.getPredios()) {
					totalAreaOriginales += p.getAreaTerreno();
					totalAvaluoOriginal += p.getAvaluoCatastral();
				}

				for (PPredio pp : this.pPrediosEnglobe) {
					totalAreaProyectadas += pp.getAreaTerreno();
				}
				if (!this.tramite.getPredio().isMejora()) {
					if (totalAreaOriginales != totalAreaProyectadas) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError(
								"La sumatoria de las áreas " + "proyectadas no es igual al área del predio original.");
						this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
					}
				}

				List<PPredio> prediosResultantes = this.getConservacionService()
						.obtenerPPrediosPorTramiteIdActAvaluos(this.tramite.getId());
				double totalPAvaluo = 0.0;
				for (PPredio pPredio : prediosResultantes) {
					if (pPredio.getAvaluoCatastral() != null) {
						totalPAvaluo += pPredio.getAvaluoCatastral();
					}
				}

				if ((totalPAvaluo == 0)) {

					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError("El valor del avalúo no puede ser cero.");
					this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
				}

				if ((totalPAvaluo < totalAvaluoOriginal - 1000) || (totalPAvaluo > totalAvaluoOriginal + 1000)) {

					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError("La sumatoria de los avalúos del "
							+ "predio englobado es diferente al avalúo de los predios iniciales");
					this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
				}
			}

			// Validaciones para quinta nuevo con predios fiscales ya que las
			// geograficas no aplican
			// se aclara que únicamente son validaciones con respecto al avalúo,
			// las correspondientes
			// a los demas componentes funcionan correctamente
			if (this.banderaPredioFiscal && this.tramite.isQuintaNuevo()) {

				if (this.avaluo.getValorTerreno() == null) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError("Debe diligenciar la seccion de avalúos para continuar.");
					this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
				} else {
					if (this.predioSeleccionado.isMejora()) {
						if (this.avaluo.getValorTerreno() != 0) {
							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							this.addMensajeError("Las mejoras no pueden tener valor de terreno mayor a cero.");
							this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
						}

						if (this.avaluo.getValorTotalConstruccion() == 0) {
							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							this.addMensajeError("Las mejoras no pueden tener valor de construccion igual a cero.");
							this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
						}
					} else {
						if (this.predioSeleccionado.getAreaTerreno() == 0
								|| this.predioSeleccionado.getAreaTerreno() == null) {
							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							this.addMensajeError("El área de terreno debe ser mayor a cero.");
							this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
						}

						if (this.avaluo.getValorTerreno() == null || this.avaluo.getValorTerreno() == 0) {
							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							this.addMensajeError("El valor del área de terreno debe ser mayor a cero.");
							this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
						}
					}
				}
			}

			// Validaciones para quinta omitido con predios fiscales ya que las
			// geograficas no aplican
			// se aclara que únicamente son validaciones con respecto al avalúo,
			// las correspondientes
			// a los demas componentes funcionan correctamente
			if (this.banderaPredioFiscal && this.tramite.isQuintaOmitido()) {

				if ((this.avaluo.getValorTotalConstruccion() + this.avaluo.getValorTerreno()) < this.avaluo
						.getValorTotalAvaluoCatastral() - 1000
						|| (this.avaluo.getValorTotalConstruccion() + this.avaluo.getValorTerreno()) > this.avaluo
								.getValorTotalAvaluoCatastral() + 1000) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					this.addMensajeError("La sumatoria de los avalúos de terreno y construcción es"
							+ " diferente al valor del avalúo del predio inicial.");
					this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
				}
			}

			// Si el trámite es de segunda desenglobe o de tercera masivo, y se
			// seleccionó único
			// predio, no se debe abrir la pestaña de validación masiva de
			// predios, por tal motivo se instancia el siguiente parametro de
			// error.
			// @modified by leidy.gonzalez::#13170:: 25/06/2015 Se valida que se
			// obtenga la instancia cuando sea un desenglobe diferente a
			// desenglobe de manzana
			if ((this.tramite.isSegundaDesenglobe() && !this.entradaProyeccionMB.isCreaManzanas())
					|| this.tramite.isTerceraMasiva()) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.addCallbackParam("error", "error");
				this.addMensajeWarn(
						"Para validar la proyección de todos los predios por favor seleccionelos y de click sobre el botón validar y guardar.");
				if (tabs[1] || tabs[2] || tabs[3]) {
					this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, false);
				}
			}

			this.banderaTramitePrimeraSinInscripcionYDecretos = false;
			if (!this.banderaPredioFiscal) {
				if (this.tramite.isPrimera() && this.predioSeleccionado.getFechaInscripcionCatastral() != null
						&& (UtilidadesWeb.getAnioDeUnDate(new Date()) != UtilidadesWeb
								.getAnioDeUnDate(this.predioSeleccionado.getFechaInscripcionCatastral()))
						&& ((UtilidadesWeb.getAnioDeUnDate(new Date()) - 1) != UtilidadesWeb
								.getAnioDeUnDate(this.predioSeleccionado.getFechaInscripcionCatastral()))) {

					boolean valido = false;

					this.pPrediosAvaluosCatastrales = this.getFormacionService()
							.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());

					if (this.pPrediosAvaluosCatastrales != null && !this.pPrediosAvaluosCatastrales.isEmpty()) {
						for (VPPredioAvaluoDecreto vppad : this.pPrediosAvaluosCatastrales) {
							if (vppad.getCancelaInscribe() != null) {
								valido = true;
								break;
							}
						}
					}

					if (!valido) {
						this.banderaTramitePrimeraSinInscripcionYDecretos = true;
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("No se han procesado las inscripciones y/o decretos");
						return;
					}
				}
			}
			// Validaciones realizadas a las construcciones y al predio para
			// mutación de tercera.
			if (this.isTodasSeccionesValidas() && this.tramite.isTercera()) {
				this.validacionesMutacionTercera();
			}

			// felipe.cadena::31-07-2015::Rctificacion matriz::Se recalculan los
			// avaluos de los predios cuano asi se requiera
			if (this.isTodasSeccionesValidas() && this.tramite.isRectificacionMatriz()) {
				this.recalcularAvaluosRectificacionMatriz();
				// javier.aponte:: refs#13668 Se establece el predio
				// seleccionado para que no se active la pestaña de ficha matriz
				// leidy.gonzalez:: #17932:: 15/06/2016:: Se desactiva varible
				// ficha matriz
				if (!this.pestanaFichaMatrizSeleccionada) {
					this.mostrarFichaMatrizRectificacion = false;
				}
			}

			if (this.tramite.isQuintaMasivo()) {
				this.validacionQuintaMasivoTodos = false;
			}

		} else if (this.tramite.isSegundaDesenglobe()) {

			Date vigencia;
			Date hoy = new Date();
			boolean resultadoValidacionAvaluo = true;
			double totalAreaProyectadas = 0.0;

			try {
				for (PPredio pp : this.pPrediosDesenglobe) {

					pp = this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoByIdyTramite(pp.getId(), this.tramite.getId());

					// Se valida el avalúo de los predios
					vigencia = null;

					this.avaluo = new PPredioAvaluoCatastral();
					if (pp != null && pp.getPPredioAvaluoCatastrals() != null) {
						for (PPredioAvaluoCatastral pac : pp.getPPredioAvaluoCatastrals()) {
							if (vigencia == null) {
								this.avaluo = pac;
								vigencia = this.avaluo.getVigencia();
							} else {
								if (pac.getVigencia().after(vigencia) && pac.getVigencia().before(hoy)) {
									this.avaluo = pac;
									vigencia = this.avaluo.getVigencia();
								}
							}
						}
					}

					if (!this.validarAvaluo(pp)) {
						resultadoValidacionAvaluo = false;
					}

					// Calculo de areas necesario para validaciones de predios
					// fiscales
					if (this.banderaPredioFiscal) {
						totalAreaProyectadas += pp.getAreaTerreno();
					}
				}

			} catch (Exception ex) {
				LOGGER.debug("Error en el método obtenerPPredioCompletoByIdyTramite");
				this.addMensajeError("Error al realiza la validación sobre los avalúos prediales");
				return;
			}

			this.listaValidacionesTramiteSegundaDesenglobe = new ArrayList<ValidacionProyeccionDelTramiteDTO>();

			List<PUnidadConstruccion> construccionesProyectadas = new LinkedList<PUnidadConstruccion>();
			List<String> construccionesAsociadas = new ArrayList<String>();
			List<UnidadConstruccion> construccionesOriginales = this.getConservacionService()
					.obtenerUnidadesNoCanceladasByPredioId(this.tramite.getPredio().getId());

			// #9029 :: felipe.cadena :: 27/08/2014::se descartan las
			// construcciones canceladas
			// for (int i = 0; i < construccionesOriginales.size(); i++) {
			// UnidadConstruccion uc = construccionesOriginales.get(i);
			// if(uc.getAnioCancelacion()!=null){
			// construccionesOriginales.remove(uc);
			// }
			// }
			Long idPredioFM = 0L;
			// Se determinan todas la construcciones proyectadas
			for (PPredio pp : this.pPrediosDesenglobe) {
				for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
					// Se comenta la siguiente sección de código, aparentemente
					// no siempre se da
					// if(puc.getCancelaInscribe() != null &&
					// !puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())){
					// construccionesProyectadas.add(puc);
					// }
					if (puc.getProvieneUnidad() != null
							&& !construccionesProyectadas.contains(puc.getProvieneUnidad())) {
						construccionesAsociadas.add(puc.getUnidad());
					}
				}
			}
			// validacion necesaria con predios fiscales
			if (this.banderaPredioFiscal) {
				if (this.tramite.getPredio().getAreaTerreno() != totalAreaProyectadas) {
					ValidacionProyeccionDelTramiteDTO validacionDelTramite;
					validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
					validacionDelTramite.setMensajeError(
							"La sumatoria de las áreas proyectadas no es igual al área del predio original.");
					validacionDelTramite.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
					this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
				}

				List<PPredio> prediosResultantes = this.getConservacionService()
						.obtenerPPrediosPorTramiteIdActAvaluos(this.tramite.getId());
				double totalPAvaluo = 0.0;
				for (PPredio pPredio : prediosResultantes) {
					if (pPredio.getAvaluoCatastral() != null) {
						totalPAvaluo += pPredio.getAvaluoCatastral();
					}
				}
				if (!this.tramite.getPredio().isMejora()) {
					if ((totalPAvaluo < this.tramite.getPredio().getAvaluoCatastral() - 1000)
							|| (totalPAvaluo > this.tramite.getPredio().getAvaluoCatastral() + 1000)) {

						ValidacionProyeccionDelTramiteDTO validacionDelTramite;
						validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
						validacionDelTramite.setMensajeError("La sumatoria de los avalúos de los "
								+ "predios desenglobados es diferente al avalúo del predio inicial");
						validacionDelTramite.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
						this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
					}
				}
			}
			if (!this.tramite.isDesenglobeEtapas()
					&& construccionesOriginales.size() > construccionesAsociadas.size()) {
				ValidacionProyeccionDelTramiteDTO validacionDelTramite;
				validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
				validacionDelTramite.setMensajeError(
						"Algunas de las construcciones del predio original no estan asociadas a los predios del desenglobe");
				validacionDelTramite.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
				this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
			}

			ValidacionProyeccionDelTramiteDTO validacionDelTramite;
			// felipe.cadena validacion para desenglobe de manzanas
			if ((this.entradaProyeccionMB.getManzanasDesenglobe() == null
					|| this.entradaProyeccionMB.getManzanasDesenglobe().isEmpty())
					&& (this.tramite.getManzanasNuevas() != null && this.tramite.getManzanasNuevas() > 0)) {

				validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
				validacionDelTramite.setMensajeError("Se deben registrar las manzanas del desenglobe");
				validacionDelTramite.setSeccion(ESeccionEjecucion.PROYECCION_TRAMITE.getNombre());
				this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
			}

			for (PManzanaVereda pmv : this.entradaProyeccionMB.getManzanasDesenglobe()) {

				int nPredios = this.entradaProyeccionMB.obtenerPrediosManzana(pmv.getManzanaCodigo()).size();
				if (nPredios == 0) {
					validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
					validacionDelTramite.setMensajeError(
							"Existe la manzana " + pmv.getManzanaCodigo() + "  que no tiene predios asociados");
					validacionDelTramite.setSeccion(ESeccionEjecucion.PROYECCION_TRAMITE.getNombre());
					this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
				}
			}
			if (!this.banderaPredioFiscal
					&& !(this.tramite.isSegundaDesenglobe() && this.tramite.getPredio().isMejora())) {
				this.validarProyeccion();
			}

			// Establece el valor de validación de avalúos haciendo un and entre
			// el resultado de validar proyección
			// y el resultado de validación de avalúo
			this.validezSeccion.put(ESeccionEjecucion.AVALUOS,
					this.validezSeccion.get(ESeccionEjecucion.AVALUOS) && resultadoValidacionAvaluo);

			if (this.banderaDesenglobeMejora || this.banderaPredioFiscal) {
				this.validarConstruccionesMejoras();
			}

			if (this.listaValidacionesTramiteSegundaDesenglobe.isEmpty() && this.banderaPredioFiscal) {
				this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, true);
			}

			if (this.entradaProyeccionMB.getCondicionPropiedadSeleccionada() != null
					&& !this.entradaProyeccionMB.getCondicionPropiedadSeleccionada().equals("0")) {

				if (tabs[7]) {
					this.fichaMatrizMB.validarGuardar();

					// Validación de los números de matrícula de los predios
					// generados.
					this.validarMatriculasDuplicadas();
				}
			}

			if (this.tramite.isSegundaDesenglobe() && idPredioFM.longValue() > 0) {
				this.validarCoeficientes(idPredioFM, false);
				if (this.listaValidacionesTramiteSegundaDesenglobe != null
						&& !this.listaValidacionesTramiteSegundaDesenglobe.isEmpty()) {
					this.validezSeccion.put(ESeccionEjecucion.FICHA_MATRIZ, false);
				} else {
					this.addMensajeInfo("Se validó la proyección del trámite correctamente.");
				}
			}

			this.predioSeleccionado = null;
		} else if (this.tramite.isTerceraMasiva()) {

			Date vigencia;
			Date hoy = new Date();
			boolean resultadoValidacionAvaluo = true;

			try {
				for (PPredio pp : this.pPrediosTerceraRectificacionMasiva) {

					pp = this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoByIdyTramite(pp.getId(), this.tramite.getId());

					// Se valida el avalúo de los predios
					vigencia = null;

					this.avaluo = new PPredioAvaluoCatastral();
					if (pp != null && pp.getPPredioAvaluoCatastrals() != null) {
						for (PPredioAvaluoCatastral pac : pp.getPPredioAvaluoCatastrals()) {
							if (vigencia == null) {
								this.avaluo = pac;
								vigencia = this.avaluo.getVigencia();
							} else {
								if (pac.getVigencia().after(vigencia) && pac.getVigencia().before(hoy)) {
									this.avaluo = pac;
									vigencia = this.avaluo.getVigencia();
								}
							}
						}
					}

					if (!this.validarAvaluo(pp)) {
						resultadoValidacionAvaluo = false;
					}

					this.validacionesMutacionTerceraMasiva();
				}
			} catch (Exception ex) {
				LOGGER.debug("Error en el método obtenerPPredioCompletoByIdyTramite");
			}

			this.listaValidacionesTramiteSegundaDesenglobe = new ArrayList<ValidacionProyeccionDelTramiteDTO>();

			this.validarProyeccion();

			// Establece el valor de validación de avalúos haciendo un and entre
			// el resultado de validar proyección
			// y el resultado de validación de avalúo
			this.validezSeccion.put(ESeccionEjecucion.AVALUOS,
					this.validezSeccion.get(ESeccionEjecucion.AVALUOS) && resultadoValidacionAvaluo);

			if (this.entradaProyeccionMB.getCondicionPropiedadSeleccionada() != null
					&& !this.entradaProyeccionMB.getCondicionPropiedadSeleccionada().equals("0")) {

				if (tabs[7]) {
					this.fichaMatrizMB.validarGuardar();

					// Validación de los números de matrícula de los predios
					// generados.
					this.validarMatriculasDuplicadas();
				}
			}

			this.predioSeleccionado = null;
		} else if (this.tramite.isQuintaMasivo()) {

			this.cargarPrediosQuintaMasivo();
			boolean textoMotivoAlmacenado = false;
			boolean justificacionesAlmacenadas = false;
			boolean propietariosAlmacenados = false;
			boolean validacionJustificacionTemp = false;
			boolean validacionPropietariosTemp = false;
			boolean validacionInscripcionesTemp = false;
			boolean validacionTextoMotivoTemp = false;
			boolean validacionUbicacionTemp = false;
			boolean validacionAvaluosTemp = false;

			this.inicializarValidezSecciones();
			for (PPredio pp : this.pPrediosQuintaMasivo) {

				try {
					this.predioSeleccionado = this.getConservacionService().obtenerPPredioCompletoById(pp.getId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				this.getTabs();

				if (this.predioSeleccionado != null && !this.predioSeleccionado.isEsPredioFichaMatriz()) {

					if (tabs[4] && !this.predioSeleccionado.isPredioOriginalTramite()) {
						validacionPropietariosTemp = this.getValidezSeccion().get(ESeccionEjecucion.PROPIETARIOS);

						boolean resultadoValidacion = this.propietariosMB.validarSeccion();
						this.validezSeccion.put(ESeccionEjecucion.PROPIETARIOS, resultadoValidacion);
						if (!validacionPropietariosTemp) {
							this.getValidezSeccion().put(ESeccionEjecucion.PROPIETARIOS, false);
						}
						propietariosAlmacenados = true;
						if (!this.getValidezSeccion().get(ESeccionEjecucion.PROPIETARIOS)) {
							propietariosAlmacenados = false;
						}
					}

					if (tabs[5] && !this.predioSeleccionado.isPredioOriginalTramite()) {
						validacionJustificacionTemp = this.getValidezSeccion()
								.get(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD);
						boolean resultadoValidacion = this.justificacionesMB.validarSeccion();
						this.validezSeccion.put(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD, resultadoValidacion);
						if (!validacionJustificacionTemp) {
							this.getValidezSeccion().put(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD, false);
						}
						justificacionesAlmacenadas = true;
						if (!this.getValidezSeccion().get(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD)) {
							justificacionesAlmacenadas = false;
						}
					}

					if (tabs[9]) {
						validacionInscripcionesTemp = this.getValidezSeccion()
								.get(ESeccionEjecucion.INSCRIPCIONES_DECRETOS);
						boolean resultadoValidacion = this.validarInscripcionesDecretos();
						this.validezSeccion.put(ESeccionEjecucion.INSCRIPCIONES_DECRETOS, resultadoValidacion);
						if (!validacionInscripcionesTemp) {
							this.getValidezSeccion().put(ESeccionEjecucion.INSCRIPCIONES_DECRETOS, false);
						}
					}

					if (tabs[10] && !textoMotivoAlmacenado) {
						validacionTextoMotivoTemp = this.getValidezSeccion().get(ESeccionEjecucion.TEXTO_MOTIVADO);
						this.guardarTramiteTextoResolucion();
						boolean resultadoValidacion = this.validarTextoMotivo();
						this.validezSeccion.put(ESeccionEjecucion.TEXTO_MOTIVADO, resultadoValidacion);
						textoMotivoAlmacenado = true;
						if (!validacionTextoMotivoTemp) {
							this.getValidezSeccion().put(ESeccionEjecucion.TEXTO_MOTIVADO, false);
						}
					}

					if (tabs[6] && !this.predioSeleccionado.isPredioOriginalTramite()) {
						validacionUbicacionTemp = this.getValidezSeccion().get(ESeccionEjecucion.UBICACION);
						boolean resultadoValidacion = this.validarGenerales();
						this.validezSeccion.put(ESeccionEjecucion.UBICACION, resultadoValidacion);
						if (!validacionUbicacionTemp) {
							this.getValidezSeccion().put(ESeccionEjecucion.UBICACION, false);
						}
					}

					if (tabs[8]) {
						validacionAvaluosTemp = this.getValidezSeccion().get(ESeccionEjecucion.AVALUOS);

						// Se valida el avalúo de los predios
						Date vigencia;
						Date hoy = new Date();
						vigencia = null;
						this.avaluo = new PPredioAvaluoCatastral();
						if (pp != null && pp.getPPredioAvaluoCatastrals() != null) {
							for (PPredioAvaluoCatastral pac : pp.getPPredioAvaluoCatastrals()) {
								if (vigencia == null) {
									this.avaluo = pac;
									vigencia = this.avaluo.getVigencia();
								} else {
									if (pac.getVigencia().after(vigencia) && pac.getVigencia().before(hoy)) {
										this.avaluo = pac;
										vigencia = this.avaluo.getVigencia();
									}
								}
							}
						}

						// Si el resultado es false, el avalúo del predio no es
						// válido
						boolean resultadoValidacion = this.validarAvaluo(this.predioSeleccionado);

						this.validezSeccion.put(ESeccionEjecucion.AVALUOS, resultadoValidacion);
						if (!validacionAvaluosTemp) {
							this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
						}
					}

					if (tabs[1] || tabs[2] || tabs[3]) {
						this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, true);
					}
				} else {
					if (!this.banderaDesenglobeMejora) {

						if (tabs[7]) {
							this.fichaMatrizMB.validarSeccionFichaMatriz();
						}
						if (tabs[0]) {
							this.validezSeccion.put(ESeccionEjecucion.HISTORICO_MODIFICACIONES, true);
						}
						if (tabs[11]) {
							this.validezSeccion.put(ESeccionEjecucion.ASOCIAR_DOCUMENTOS, true);
						}
					}
				}
			}
			if (justificacionesAlmacenadas) {
				this.justificacionesMB.validarGuardar();
				this.addMensajeInfo("Justificación de los derechos de propiedad almacenados de forma exitosa");
			}
			if (propietariosAlmacenados) {
				this.propietariosMB.validarGuardar();
				this.addMensajeInfo("Propietarios/poseedores almacenados de forma exitosa");
			}

			this.predioSeleccionado = null;
			this.validacionQuintaMasivoTodos = true;
		}

		if (this.tramite.isRectificacionMatriz()) {

			try {
				for (PPredio pp : this.pPrediosTerceraRectificacionMasiva) {

					pp = this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoByIdyTramite(pp.getId(), this.tramite.getId());

					if (!this.validarAvaluo(pp)) {
						return;
					}

				}
			} catch (Exception ex) {
				LOGGER.debug("Error en el método obtenerPPredioCompletoByIdyTramite", ex);
			}

		}

		if (this.tramite.isCancelacionMasiva()) {
			this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, true);
		}

		// Las validaciones de proyeccion para los englobes virtuales se realizan en
		// otro punto
		if (this.tramite.isEnglobeVirtual()) {
			this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, true);
		}

		// CC-NP-CO-103 Realizar ajuste para proyección de trámite ::
		// david.cifuentes
		// Llamado a la verificación de datos para la proyección del trámite
		if (this.isTodasSeccionesValidas()) {
			this.verificacionDatosProyeccion = this.validacionProyeccionService.verificarDatosProyeccion(this.tramite);

			if (!this.verificacionDatosProyeccion) {
				RequestContext context = RequestContext.getCurrentInstance();
				if (context != null) {
					context.addCallbackParam("error", "error");
					List<String> mensajes = this.validacionProyeccionService.getMensajeValidacion();
					if (mensajes != null && !mensajes.isEmpty()) {
						for (String mensaje : mensajes) {
							this.addMensajeError(mensaje);
						}
					}
				}
			} else {
				// felipe.cadena::07-04-2016::#16250::Para todos los tramites de
				// rectificacion y complementacion siempre se coloca C/I en M,
				// ya que por lo menos se modifica la fecha de inscripcion
				// catastral
				// leidy.gonzalez:: #17932:: 15/06/2016:: Para actializar el
				// predio proyectado se verifica que antes no se haya realizado
				// una actualizacion
				if (this.tramite.isEsComplementacion() || this.tramite.isEsRectificacion()) {
					if (this.predioSeleccionado != null && this.predioSeleccionado.getCancelaInscribe() != null
							&& !this.predioSeleccionado.getCancelaInscribe()
									.equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo())) {

						this.predioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
						this.predioSeleccionado = this.getConservacionService()
								.guardarActualizarPPredio(this.predioSeleccionado);
					}

				}
			}
		}

		if (this.tramite.isCancelacionMasiva()) {
			this.validarMatriculasDuplicadas();
		}

		if ((this.tramite.isTercera() && !this.tramite.isTerceraMasiva())
				|| (this.tramite.isQuinta() && !this.tramite.isQuintaMasivo())) {
			this.compararConstruccionesFirmeYProyectadas();
		}

	}

	public boolean validarCoeficientes(Long idPredioFM, boolean envioGeografico) {

		boolean coeficienteCero = true;
		Double sumaCoeficientes = 0D;
		PFichaMatriz pfm = this.getConservacionService().buscarPFichaMatrizPorPredioId(idPredioFM);
		for (PFichaMatrizPredio pfmp : pfm.getPFichaMatrizPredios()) {
			if ((pfmp.getCoeficiente() == null || pfmp.getCoeficiente() <= 0D)
					&& !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.getCancelaInscribe())) {
				coeficienteCero = false;
				break;
			} else if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmp.getCancelaInscribe())) {
				sumaCoeficientes += pfmp.getCoeficiente();
			}
		}

		ValidacionProyeccionDelTramiteDTO validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
		validacionDelTramite.setSeccion(ESeccionEjecucion.FICHA_MATRIZ.getNombre());

		if (!coeficienteCero) {
			validacionDelTramite.setMensajeError(
					"Existe uno o más predios generados a los cuales no se les ha asignado un coeficiente de copropiedad.");
			this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
		}

		String mensajeMayor, mensajeMenor;
		if (sumaCoeficientes > 1 + 0.000000001 || sumaCoeficientes < 1 - 0.000000001) {
			mensajeMayor = (sumaCoeficientes > 1) ? " mayor " : "";
			mensajeMenor = (sumaCoeficientes < 1) ? " menor " : "";
			String mensaje = new String("Por favor revise los coeficientes de copropiedad, la suma de ellos es "
					+ sumaCoeficientes + " este valor es" + mensajeMayor + mensajeMenor + " a 1");
			validacionDelTramite.setMensajeError(mensaje);
			this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
		}
		return true;
	}

	/**
	 * Método para validar que el area de las construcciones iniciales sea
	 * consistente con las construcciones originales
	 *
	 * @author felipe.cadena
	 *
	 */
	private void validarConstruccionesMejoras() {
		ValidacionProyeccionDelTramiteDTO validacionDelTramite;
		PPredio predioOriginal = this.getConservacionService()
				.obtenerPPredioDatosAvaluo(this.tramite.getPredio().getId());

		List<PUnidadConstruccion> construccionesOriginales = new LinkedList<PUnidadConstruccion>();
		List<PPredio> prediosResultantes = this.getConservacionService()
				.obtenerPPrediosPorTramiteIdActAvaluos(this.tramite.getId());
		List<PUnidadConstruccion> construccionesNuevas = new LinkedList<PUnidadConstruccion>();

		// Se cargan las construcciones originales
		if (predioOriginal.getPUnidadConstruccions() != null) {
			for (PUnidadConstruccion puco : predioOriginal.getPUnidadConstruccions()) {
				if (puco.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
					construccionesOriginales.add(puco);
				}
			}
		}

		// Se cargan las construcciones nuevas
		for (PPredio predio : prediosResultantes) {
			int construccionesPredio = 0;
			for (PUnidadConstruccion pUnidadConstruccion : predio.getPUnidadConstruccions()) {
				if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pUnidadConstruccion.getCancelaInscribe())) {
					construccionesNuevas.add(pUnidadConstruccion);
					construccionesPredio++;
				}
			}
			// Se valida que todos los predios tengan por lo menos una
			// construccion
			if (this.banderaPredioFiscal && this.tramite.getPredio().isMejora()) {
				if (construccionesPredio == 0) {
					validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
					validacionDelTramite.setMensajeError(
							"El predio " + predio.getNumeroPredial() + " no tiene construcciones asociadas");
					validacionDelTramite.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
					this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
				}
			}
		}
		Double avaluoNuevo = 0.0;
		for (PUnidadConstruccion pUnidadConstruccion : construccionesOriginales) {
			Double areaInicial = pUnidadConstruccion.getAreaConstruida();
			Double areaNueva = 0.0;

			for (PUnidadConstruccion puNueva : construccionesNuevas) {
				String puProviene = puNueva.getProvieneUnidad();
				String puOrigen = pUnidadConstruccion.getUnidad();
				if (puProviene.equals(puOrigen)) {
					areaNueva += puNueva.getAreaConstruida();
					avaluoNuevo += puNueva.getAvaluo();
				}
			}
			if (!areaInicial.equals(areaNueva)) {
				validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
				validacionDelTramite.setMensajeError("El area de la construcción " + pUnidadConstruccion.getUnidad()
						+ " - " + pUnidadConstruccion.getUsoConstruccion().getNombre()
						+ " es diferente a las áreas desenglobadas");
				validacionDelTramite.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
				this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
			}
		}

		if (this.banderaPredioFiscal && this.tramite.getPredio().isMejora()) {
			if ((avaluoNuevo < this.tramite.getPredio().getAvaluoCatastral() - 1000)
					|| (avaluoNuevo > this.tramite.getPredio().getAvaluoCatastral() + 1000)) {

				validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
				validacionDelTramite.setMensajeError("La sumatoria de los avalúos de los predios"
						+ " desenglobados es diferente al avalúo del predio inicial");
				validacionDelTramite.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
				this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
			}
		}
		// felip.cadena:
		// Actualizar areas de construccion
		List<PPredio> prediosProyeccion = this.getConservacionService()
				.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
		for (PPredio pp : prediosProyeccion) {

			double areaConstruccionPredio = 0.0;

			for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
				if (!puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
					areaConstruccionPredio += puc.getAreaConstruida();
				}
			}
			pp.setAreaConstruccion(areaConstruccionPredio);
			pp = this.getConservacionService().actualizarPPredio(pp);
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que realiza las validaciones correspondientes a la sección de texto
	 * motivado.
	 *
	 * @return
	 */
	public boolean validarTextoMotivo() {
		boolean valido = true;
		if (this.modeloResolucionSeleccionado == null || this.modeloResolucionSeleccionado.getId() == 0
				|| this.modeloResolucionSeleccionado.getTextoConsiderando() == null
				|| this.modeloResolucionSeleccionado.getTextoConsiderando().trim().equals("")
				|| this.modeloResolucionSeleccionado.getTextoResuelve() == null
				|| this.modeloResolucionSeleccionado.getTextoResuelve().trim().equals("")) {
			this.addMensajeError("No se ha diligenciado completamente el texto motivado");
			valido = false;
		}
		return valido;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que realiza las validaciones correspondientes a la sección de
	 * inscripciones y decretos.
	 *
	 * @return
	 */
	public boolean validarInscripcionesDecretos() {
		boolean valido = true;
		// Si no se mostró la pestaña de inscripciones y decretos es necesario
		// actualizar el p_predio
		if (ESiNo.NO.getCodigo().equals(this.configuracion.getInscripcionesDecretos())
				&& this.predioSeleccionado != null) {
			this.getConservacionService().guardarActualizarPPredio(this.predioSeleccionado);
		}
		return valido;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que realiza las validaciones correspondientes a la sección de datos
	 * generales del predio.
	 *
	 * @return
	 */
	public boolean validarGenerales() {
		boolean valido = true;
		if (this.predioSeleccionado.getTipo() == null || this.predioSeleccionado.getTipo().trim().equals("")) {
			this.addMensajeError("El predio debe tener un tipo (Pestaña Datos generales del predio)");
			valido = false;
		}
		if (this.predioSeleccionado.getDestino() == null || this.predioSeleccionado.getDestino().trim().equals("")) {
			this.addMensajeError("El predio debe tener un destino económico (Pestaña Datos generales del predio)");
			valido = false;
		}
		// felipe.cadena:: #9062 :: 28-08-2014 :: Se valida la matricula
		// inmoviliaria para tramites de englobe
		if (this.tramite.isSegundaEnglobe()) {
			if (this.predioSeleccionado != null && (this.predioSeleccionado.getNumeroRegistro() == null
					|| this.predioSeleccionado.getNumeroRegistro().equals(""))) {

				// felipe.cadena::27-03-2014::redmine #9062:: No se valida
				// matricula inmobiliaria para mejoras
				if (!this.predioSeleccionado.isMejora()) {
					this.addMensajeError(
							"El predio debe tener matrícula inmobiliaria (Pestaña Datos generales del predio)");
					valido = false;
				}
			}
		}

		if (this.tramite.isQuinta() || this.tramite.isCancelacionMasiva()) {
			if (this.predioSeleccionado != null && (this.predioSeleccionado.getPPredioDireccions() == null
					|| this.predioSeleccionado.getPPredioDireccions().size() == 0)) {
				this.addMensajeError("El predio debe tener una dirección (Pestaña Datos generales del predio)");
				valido = false;
			}
			if (this.predioSeleccionado != null
					&& (this.predioSeleccionado.getNumeroRegistro() == null
							|| this.predioSeleccionado.getNumeroRegistro().equals(""))
					&& !this.tramite.isQuintaOmitido()) {
				// version 1.1.4 - felipe.cadena_7286
				// felipe.cadena::27-03-2014::redmine #7488:: No se valida
				// matricula inmobiliaria para mejoras
				if (!this.predioSeleccionado.isMejora()) {
					this.addMensajeError(
							"El predio debe tener matrícula inmobiliaria (Pestaña Datos generales del predio)");
					valido = false;
				}

				if (this.tramite.isQuintaNuevo() && this.predioSeleccionado.isMejora()) {
					this.quintaNuevoCond5 = true;
					this.setQuintaNuevoCond5(true);
				} else {
					this.quintaNuevoCond5 = false;
				}
			}
		}
		return valido;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que realiza las validaciones correspondientes a la sección de detalle
	 * del avalúo.
	 *
	 * @author javier.aponte
	 * @return
	 */
	public boolean validarAvaluo(PPredio predioAValidar) {
		boolean valido = true;
		if (this.tramite.isCuarta()) {
			if (!this.isAutoavaluo() && !this.modeloResolucionSeleccionado.getTextoTitulo().contains("RECHAZA")) {
				this.addMensajeError(
						"El campo Autoestimaciones debe ser seleccionado, para actualizar los valores de terreno");
				valido = false;
			} else {
				try {
					PPredioAvaluoCatastral auxAval = this.getConservacionService()
							.actualizarPPredioAvaluoCatastral(this.avaluo);
					this.avaluo.setId(auxAval.getId());
					this.predioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
					this.predioSeleccionado.setUsuarioLog(this.usuario.getLogin());
					this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
				} catch (Exception e) {
					LOGGER.error("error en ProyectarConservacionMB#guardarGestionAvaluo");
				}
			}
		}

		// Por ahora no se validan predios que tengan condición de propiedad 8,
		// mientras se
		// define un requerimiento
		if (predioAValidar.isCondominio() && !this.tramite.isQuintaMasivo()) {
			return valido;
		}

		// Si realizan las validaciones si el trámite es geográfico y no es de
		// tercera y no es rectificaciòn de área de construcción
		// La validación se realiza hasta que vuelva del editor
		// la forma de saber que ya regresó es que exista el documento de copia
		// de BD geográfica final
		// @modified.by jonathan.chacon :: excepto para tramites con predios
		// fiscales
		if (this.tramite.isTramiteGeografico() && !this.banderaPredioFiscal) {
			List<TramiteDocumento> copiasBDGeograficaFinal = this.getTramiteService()
					.obtenerTramiteDocumentosDeTramitePorTipoDoc(this.tramite.getId(),
							ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());
			if (copiasBDGeograficaFinal == null || copiasBDGeograficaFinal.isEmpty()) {
				return valido;
			}
		} else {
			return valido;
		}

		boolean existenUnidadesConvencionales = false;
		boolean existenUnidadesNoConvencionales = false;

		if (this.tramite.isCancelacionMasiva()) {
			predioAValidar.setTramite(this.tramite);
		}

		List<PUnidadConstruccion> uccTemp = predioAValidar.getPUnidadesConstruccionConvencionalNuevas();
		List<PUnidadConstruccion> ucncTemp = predioAValidar.getPUnidadesConstruccionNoConvencionalNuevas();

		Double avaluoUnidadesContruccionConvencionales = Double.valueOf(0);

		if (uccTemp != null && !uccTemp.isEmpty()) {
			existenUnidadesConvencionales = true;
			for (PUnidadConstruccion uc : uccTemp) {
				avaluoUnidadesContruccionConvencionales += uc.getAvaluo();
			}
		}

		Double avaluoUnidadesContruccionNoConvencionales = Double.valueOf(0);

		if (ucncTemp != null && !ucncTemp.isEmpty()) {
			existenUnidadesNoConvencionales = true;
			for (PUnidadConstruccion unc : ucncTemp) {
				avaluoUnidadesContruccionNoConvencionales += unc.getAvaluo();
			}
		}

		// Si el predio es de condición de propiedad cero
		if (EPredioCondicionPropiedad.CP_0.getCodigo().equals(predioAValidar.getCondicionPropiedad())) {

			valido = this.validarAvaluoCondicion0(predioAValidar, existenUnidadesConvencionales,
					existenUnidadesNoConvencionales);

		} // Si el predio es condición de propiedad cinco
		else if (EPredioCondicionPropiedad.CP_5.getCodigo().equals(predioAValidar.getCondicionPropiedad())) {

			valido = this.validarAvaluoCondicion5(predioAValidar);

		} // Si el predio resultante es 8 u 9
		else if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(predioAValidar.getCondicionPropiedad())
				|| EPredioCondicionPropiedad.CP_9.getCodigo().equals(predioAValidar.getCondicionPropiedad())) {

			valido = this.validarAvaluoCondicion8o9(predioAValidar, existenUnidadesConvencionales,
					existenUnidadesNoConvencionales);

		}

		// Si existen construcciones convencionales el valor del avalúo de
		// construcciones convencionales debe ser diferente de cero
		if (existenUnidadesConvencionales && Double.valueOf(0).equals(avaluoUnidadesContruccionConvencionales)) {
			valido = false;
			this.addMensajeError("Error al calcular el valor del avalúo de las construcciones convencionales");
		}

		// Si existen construcciones no convencionales el valor del avalúo de
		// construcciones no convencionales debe ser diferente de cero
		// validacion no aplica para englobes virtuales
		if (!this.tramite.isEnglobeVirtual()) {
			if (existenUnidadesConvencionales && Double.valueOf(0).equals(avaluoUnidadesContruccionConvencionales)) {
				valido = false;
				this.addMensajeError("Error al calcular el valor del avalúo de las construcciones convencionales");
			}
		}

		// El valor del avalúo para cualquier predio debe ser diferente a cero
		if (Double.valueOf(0).equals(predioAValidar.getAvaluoCatastral())) {
			valido = false;
			this.addMensajeError("Error al calcular el avalúo para el predio: " + predioAValidar.getNumeroPredial());
		}

		// Si el predio es condición 0, 9 u 8
		// El área total Terreno debe ser igual a la sumatoria de las áreas para
		// cada una de las zonas del predio
		if (!this.banderaPredioFiscal) {

			// javier.aponte refs#17313, la validación se realiza bajo las
			// condiciones que se especifican en la incidencia refs#17313 ::
			// 11/05/2016
			if (EPredioCondicionPropiedad.CP_0.getCodigo().equals(predioAValidar.getCondicionPropiedad())
					|| EPredioCondicionPropiedad.CP_8.getCodigo().equals(predioAValidar.getCondicionPropiedad())
					|| EPredioCondicionPropiedad.CP_9.getCodigo().equals(predioAValidar.getCondicionPropiedad())) {

				if (this.tramite.isSegunda() || this.tramite.isQuinta()
						|| (this.tramite.isRectificacion() && (this.tramite.isRectificacionGeograficaAreaTerreno()
								|| this.tramite.isRectificacionZona() || this.tramite.isRectificacionZonaMatriz()))
						|| this.tramite.isModificacionInscripcionCatastral()) {
					Parametro p = this.getGeneralesService()
							.getCacheParametroPorNombre(EParametro.DIFERENCIA_AREA_TERRENO_PERMITIDA.name());
					double diferenciaAreaTerrenoPermitida = p.getValorNumero();

					// El área total Terreno debe ser igual a la sumatoria de
					// las áreas para cada una de las zonas del predio
					if (Math.abs(Utilidades.redondearNumeroADosCifrasDecimales(predioAValidar.getAreaTerreno()
							- this.getAreaTotalTerrenoZonas(predioAValidar))) > diferenciaAreaTerrenoPermitida) {
						valido = false;
						this.addMensajeError(
								"Existe una diferencia en el área de terreno y en la sumatoria de las áreas de las zonas para el predio: "
										+ predioAValidar.getNumeroPredial());
					}
				}
			}
		}
		return valido;
	}

	/**
	 * Método encargado de hacer la validación para el cálculo de avalúo con predios
	 * de condición cero
	 *
	 * @author javier.aponte
	 */
	public boolean validarAvaluoCondicion0(PPredio predioAValidar, boolean existenUnidadesConvencionales,
			boolean existenUnidadesNoConvencionales) {

		boolean valido = true;

		// El área Total de terreno debe ser mayor a cero
		if (Double.valueOf(0).compareTo(Double.valueOf(predioAValidar.getAreaTerreno())) >= 0) {
			valido = false;
			this.addMensajeError(
					"Existe  un error en el área de terreno para el predio: " + predioAValidar.getNumeroPredial());
		}

		// el valor del avalúo de Terreno debe ser mayor a cero
		if (this.avaluo.getValorTerreno() == null
				|| (Double.valueOf(0).compareTo(this.avaluo.getValorTerreno()) >= 0)) {
			valido = false;
			this.addMensajeError("Error al calcular el valor del avalúo de terreno para el predio: "
					+ predioAValidar.getNumeroPredial());
		}

		// La sumatoría de las áreas de las unidades de construcción
		// convencionales y no convencionales no puede ser diferente del
		// área total de construcción del predio
		if (!Double.valueOf(this.getAreaTotalConstruccion(predioAValidar))
				.equals(Double.valueOf(predioAValidar.getAreaConstruccion()))) {
			valido = false;
			this.addMensajeError(
					"Error al calcular el área total construida para el predio: " + predioAValidar.getNumeroPredial());
		}

		if (!this.tramite.isSegundaDesenglobe()) {

			if (existenUnidadesConvencionales || existenUnidadesNoConvencionales) {
				// La sumatoría de las áreas de las unidades de construcción
				// convencionales y no convencionales no pueden ser cero
				// EL área total de construcción del predio tampoco puede ser
				// cero
				if (Double.valueOf(0).equals(Double.valueOf(this.getAreaTotalConstruccion(predioAValidar)))
						|| Double.valueOf(0).equals(Double.valueOf(predioAValidar.getAreaConstruccion()))) {
					valido = false;
					this.addMensajeError("El área total construida debe ser diferente de cero para el predio: "
							+ predioAValidar.getNumeroPredial());
				}

				// El valor de avalúo de construcción debe ser diferente de cero
				if (Double.valueOf(0).equals(this.avaluo.getValorTotalConstruccion())) {
					valido = false;
					this.addMensajeError("Error al calcular el valor del avalúo de las construcciones para el predio: "
							+ predioAValidar.getNumeroPredial());
				}
			}
		}

		return valido;
	}

	/**
	 * Método encargado de hacer la validación para el cálculo de avalúo con predios
	 * de condición cinco
	 *
	 * @author javier.aponte
	 */
	public boolean validarAvaluoCondicion5(PPredio predioAValidar) {

		boolean valido = true;

		// El área de terreno debe ser cero
		if (!Double.valueOf(predioAValidar.getAreaTerreno()).equals(Double.valueOf(0))) {
			valido = false;
			this.addMensajeError(
					"Existe  un error en el área de terreno para el predio: " + predioAValidar.getNumeroPredial());
		}

		// //El valor de avalúo de terreno debe ser cero
		if (!Double.valueOf(0).equals(Double.valueOf(this.avaluo.getValorTerreno()))) {
			valido = false;
			this.addMensajeError("Error al calcular el valor del avalúo de terreno para el predio: "
					+ predioAValidar.getNumeroPredial());
		}

		return valido;

	}

	/**
	 * Método encargado de hacer la validación para el cálculo de avalúo con predios
	 * de condición ocho o nueve
	 *
	 * @author javier.aponte
	 */
	public boolean validarAvaluoCondicion8o9(PPredio predioAValidar, boolean existenUnidadesConvencionales,
			boolean existenUnidadesNoConvencionales) {

		boolean valido = true;

		if (!predioAValidar.getNumeroPredial().endsWith(Constantes.FM_CONDOMINIO_END)
				&& !predioAValidar.getNumeroPredial().endsWith(Constantes.FM_PH_END)) {
			if (this.avaluo.getValorTerrenoComun() == null
					|| Double.valueOf(0).compareTo(Double.valueOf(this.avaluo.getValorTerrenoComun())) >= 0) {
				valido = false;
				this.addMensajeError("Error al calcular el valor del avalúo de Terreno común para el predio: "
						+ predioAValidar.getNumeroPredial());
			}
		}

		// Si el predio resultante es 8
		if (EPredioCondicionPropiedad.CP_8.getCodigo().equals(predioAValidar.getCondicionPropiedad())) {

			// El valor del avalúo de terreno privado debe ser mayor a cero
			if (this.avaluo.getValorTerrenoPrivado() == null
					|| Double.valueOf(0).compareTo(Double.valueOf(this.avaluo.getValorTerrenoPrivado())) >= 0) {
				valido = false;
				this.addMensajeError("Error al calcular el valor del avalúo de terreno privado para el predio: "
						+ predioAValidar.getNumeroPredial());
			}

		}

		if (this.tramite.isSegundaDesenglobe()) {

			// Método para validar las áreas totales construidas y comunes
			// javier.aponte refs#15624 La validación se realiza sobre el área
			// de construcción del predio para el área de construcción privada
			// y el área de construccion común de la ficha matriz:: 13/01/16
			if (this.fichaMatrizSeleccionada != null && !predioAValidar.isEsPredioFichaMatriz()) {
				valido = validarAreasTotalesConstruidasPrivadasYComunes(predioAValidar.getAreaConstruccion(),
						this.fichaMatrizSeleccionada.getAreaTotalConstruidaComun(), predioAValidar.getNumeroPredial());
			}

		}

		// Si tiene construcciones convencionales o no convencionales privadas
		// entonces el valor del avalúo de construcción debe ser mayor a cero
		if ((existenUnidadesConvencionales || existenUnidadesNoConvencionales) && (this.avaluo
				.getValorTotalConstruccion() == null
				|| (Double.valueOf(0).compareTo(Double.valueOf(this.avaluo.getValorTotalConstruccion()))) >= 0)) {
			valido = false;
			this.addMensajeError("Error al calcular el valor del avalúo de las construcciones para el predio: "
					+ predioAValidar.getNumeroPredial());
		}

		if (this.tramite.isSegundaDesenglobe()) {
			if (!predioAValidar.isEsPredioFichaMatriz()) {
				// Se consultan las construcciones que están asociadas a la
				// ficha matriz
				boolean existenUnidadesConvencionalesComunes = false;
				boolean existenUnidadesNoConvencionalesComunes = false;

				PPredio predioFichaMatriz = null;
				try {
					if (this.fichaMatrizSeleccionada != null) {
						predioFichaMatriz = this.getConservacionService()
								.obtenerPPredioCompletoById(this.fichaMatrizSeleccionada.getPPredio().getId());
					}
				} catch (Exception e) {
					LOGGER.error("No fue posible encontrar el predio asociado a la ficha matriz");
				}

				if (predioFichaMatriz != null) {

					List<PUnidadConstruccion> uccTemp = predioFichaMatriz.getPUnidadesConstruccionConvencionalNuevas();
					List<PUnidadConstruccion> ucncTemp = predioFichaMatriz
							.getPUnidadesConstruccionNoConvencionalNuevas();

					Double avaluoUnidadesContruccionConvencionalesComunes = Double.valueOf(0);

					if (uccTemp != null && !uccTemp.isEmpty()) {
						existenUnidadesConvencionalesComunes = true;
						for (PUnidadConstruccion uc : uccTemp) {
							avaluoUnidadesContruccionConvencionalesComunes += uc.getAvaluo();
						}
					}

					Double avaluoUnidadesContruccionNoConvencionalesComunes = Double.valueOf(0);

					if (ucncTemp != null && !ucncTemp.isEmpty()) {
						existenUnidadesNoConvencionalesComunes = true;
						for (PUnidadConstruccion unc : ucncTemp) {
							avaluoUnidadesContruccionNoConvencionalesComunes += unc.getAvaluo();
						}
					}

					// Si tiene construcciones convencionales o no
					// convencionales comunes entonces el valor del avalúo de
					// construcción debe ser mayor a cero
					if ((existenUnidadesConvencionalesComunes || existenUnidadesNoConvencionalesComunes)
							&& (this.avaluo.getValorTotalConstruccion() == null || (Double.valueOf(0)
									.compareTo(Double.valueOf(this.avaluo.getValorTotalConstruccion()))) >= 0)) {
						valido = false;
						this.addMensajeError(
								"Error al calcular el valor del avalúo de las construcciones para el predio: "
										+ predioAValidar.getNumeroPredial());
					}

				}
			}
		} else {

			// Si tiene construcciones convencionales o no convencionales
			// comunes entonces el valor del avalúo de construcción debe ser
			// mayor a cero
			if ((existenUnidadesConvencionales || existenUnidadesNoConvencionales) && (this.avaluo
					.getValorTotalConstruccion() == null
					|| (Double.valueOf(0).compareTo(Double.valueOf(this.avaluo.getValorTotalConstruccion()))) >= 0)) {
				valido = false;
				this.addMensajeError("Error al calcular el valor del avalúo de las construcciones para el predio: "
						+ predioAValidar.getNumeroPredial());
			}

			// La sumatoría de las áreas de las unidades de construcción
			// convencionales y no convencionales no puede ser diferente del
			// área total de construcción del predio
			if (!Double.valueOf(this.getAreaTotalConstruccion(predioAValidar))
					.equals(Double.valueOf(predioAValidar.getAreaConstruccion()))) {
				valido = false;
				this.addMensajeError("Error al calcular el área total construida para el predio: "
						+ predioAValidar.getNumeroPredial());
			}

			if (existenUnidadesConvencionales || existenUnidadesNoConvencionales) {

				// La sumatoría de las áreas de las unidades de construcción
				// convencionales y no convencionales no pueden ser cero
				// EL área total de construcción del predio tampoco puede ser
				// cero
				if (Double.valueOf(0).equals(Double.valueOf(this.getAreaTotalConstruccion(predioAValidar)))
						|| Double.valueOf(0).equals(Double.valueOf(predioAValidar.getAreaConstruccion()))) {
					valido = false;
					this.addMensajeError("El área total construida debe ser diferente de cero para el predio: "
							+ predioAValidar.getNumeroPredial());
				}

			}

		}

		return valido;
	}

	/**
	 * Método encargado de hacer validaciones para el área total construida privada
	 * y el área total construida común
	 *
	 * @author javier.aponte
	 */
	private boolean validarAreasTotalesConstruidasPrivadasYComunes(Double areaTotalConstruidaPrivada,
			Double areaTotalConstruidaComun, String numeroPredial) {
		boolean valido = true;
		// Si el área total construida (privada) es diferente de cero entonces
		// el valor de avalúo de construcción privada debe ser mayor de cero
		if (!Double.valueOf(0).equals(areaTotalConstruidaPrivada)
				&& (Double.valueOf(0).compareTo(Double.valueOf(this.avaluo.getValorTotalConstruccionPrivada()))) >= 0) {
			valido = false;
			this.addMensajeError("Error al calcular el valor del avalúo de las construcciones privadas para el predio: "
					+ numeroPredial);
		}

		// Si el área total construida (común) es diferente de cero, entonces el
		// valor del avalúo de construcción común debe ser mayor a cero
		if (!Double.valueOf(0).equals(areaTotalConstruidaComun)
				&& (Double.valueOf(0).compareTo(Double.valueOf(this.avaluo.getValorConstruccionComun()))) >= 0) {
			valido = false;
			this.addMensajeError("Error al calcular el valor del avalúo de las construcciones comunes para el predio: "
					+ numeroPredial);
		}
		return valido;
	}

	/**
	 * Método encargado de obtener el área total terreno a partir de las zonas del
	 * predio.
	 *
	 * @author javier.aponte
	 */
	private Double getAreaTotalTerrenoZonas(PPredio predio) {

		LOGGER.info("getAreaTotalTerrenoZonas");

		Double areaTotalTerreno = 0.0;

		int anioVigenciaMaxima, anioVigenciaTemp;

		if (predio.getPPredioZonas() != null && !predio.getPPredioZonas().isEmpty()) {

			anioVigenciaMaxima = UtilidadesWeb.getAnioDeUnDate(predio.getPPredioZonas().get(0).getVigencia());

			for (PPredioZona predZona : predio.getPPredioZonas()) {

				if (predZona.getCancelaInscribe() == null
						|| !predZona.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

					anioVigenciaTemp = UtilidadesWeb.getAnioDeUnDate(predZona.getVigencia());

					if (anioVigenciaMaxima < anioVigenciaTemp) {
						anioVigenciaMaxima = anioVigenciaTemp;
					}

				}
			}

			List<PPredioZona> zonasAreaTerreno = null;
			if (this.tramite.isQuintaMasivo()) {
				zonasAreaTerreno = this.verificarZonaVigente(predio.getPPredioZonas());
			} else {
				zonasAreaTerreno = predio.getPPredioZonas();
			}

			for (PPredioZona predZona : zonasAreaTerreno) {

				if (this.tramite.isQuintaMasivo()) {
					if (predZona.getCancelaInscribe() != null
							&& predZona.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {

						anioVigenciaTemp = UtilidadesWeb.getAnioDeUnDate(predZona.getVigencia());
						if (anioVigenciaMaxima == anioVigenciaTemp) {
							areaTotalTerreno += predZona.getArea();
						}
					}
				} else {
					if (predZona.getCancelaInscribe() == null
							|| !predZona.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

						anioVigenciaTemp = UtilidadesWeb.getAnioDeUnDate(predZona.getVigencia());

						if (anioVigenciaMaxima == anioVigenciaTemp) {
							areaTotalTerreno += predZona.getArea();
						}
					}
				}
			}
		}
		return areaTotalTerreno;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método para asociar las construcciónes antiguas y no canceladas del predio
	 * como construcciones que perduran en el predio nuevo.
	 */
	public void asociarConstrucciones() {

		try {

			GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

			if (this.tramite.isCancelacionMasiva()) {

				boolean validoConv = true;
				boolean validoNoConv = true;
				List<UnidadConstruccion> auxConvencionales = new ArrayList<UnidadConstruccion>();
				List<UnidadConstruccion> auxNoConvencionales = new ArrayList<UnidadConstruccion>();

				if (this.unidadesConstruccionConvencionalSeleccionadas != null) {
					auxConvencionales.addAll(Arrays.asList(this.unidadesConstruccionConvencionalSeleccionadas));
				}
				if (this.unidadesConstruccionConvencionalSeleccionadasFM != null) {
					auxConvencionales.addAll(Arrays.asList(this.unidadesConstruccionConvencionalSeleccionadasFM));
				}

				// felipe.cadena::23-01-2017::validaciones en asociacion de construccion
				// convencionales
				if (this.getGestionDetalleAvaluoMB().validacionesConstruccionesAsociacionCM(auxConvencionales)) {
					this.unidadesConstruccionConvencionalSeleccionadas = auxConvencionales
							.toArray(this.unidadesConstruccionConvencionalSeleccionadas);
				} else {
					validoConv = false;
				}

				if (this.unidadesConstruccionNoConvencionalSeleccionadas != null) {
					auxNoConvencionales.addAll(Arrays.asList(this.unidadesConstruccionNoConvencionalSeleccionadas));
				}
				if (this.unidadesConstruccionNoConvencionalSeleccionadasFM != null) {
					auxNoConvencionales.addAll(Arrays.asList(this.unidadesConstruccionNoConvencionalSeleccionadasFM));
				}

				// felipe.cadena::23-01-2017::validaciones en asociacion de construccion no
				// convencionales
				if (this.getGestionDetalleAvaluoMB().validacionesConstruccionesAsociacionCM(auxNoConvencionales)) {
					this.unidadesConstruccionNoConvencionalSeleccionadas = auxNoConvencionales
							.toArray(this.unidadesConstruccionNoConvencionalSeleccionadas);
				} else {
					validoNoConv = false;
				}

				if (!validoNoConv || !validoConv) {
					return;
				}
			}

			if (this.tramite.isEnglobeVirtual()) {
				if (!this.getGestionDetalleAvaluoMB().validacionesConstruccionesAsociacionEV()) {
					return;
				}
			} else if (!this.tramite.isEnglobeVirtual()
					&& this.unidadesConstruccionConvencionalSeleccionadas.length == 0
					&& this.unidadesConstruccionNoConvencionalSeleccionadas.length == 0) {
				this.addMensajeWarn("Debe seleccionar al menos una construcción para asociarla");
				RequestContext context = RequestContext.getCurrentInstance();
				context.addCallbackParam("error", "error");
				return;
			} else if (this.tramite.isSegundaDesenglobe() && this.predioSeleccionado.getCondicionPropiedad()
					.equals(EPredioCondicionPropiedad.CP_0.getCodigo())) {

				this.asociarConstruccionesDesenglobeCondicionCero();
				if (this.messageError) {
					return;
				}
				this.cargarConstruccionesDesenglobe();
			} else if (this.banderaDesenglobeMejora) {
				// Se asocian las construcciones para mejoras
				for (UnidadConstruccion pVigenteSelect : unidadesConstruccionConvencionalSeleccionadas) {

					this.predioSeleccionado.setTramite(this.tramite);
					List<PUnidadConstruccion> unidadesExistentes = this.predioSeleccionado
							.getPUnidadesConstruccionConvencionalNuevas();
					boolean previa = false;
					for (PUnidadConstruccion puce : unidadesExistentes) {
						String idPUC = puce.getProvieneUnidad();
						String idNUC = pVigenteSelect.getUnidad();
						if (idPUC.equals(idNUC)) {
							this.addMensajeError("La construcción " + pVigenteSelect.getUsoConstruccion().getNombre()
									+ " ya ha sido asociada.");
							previa = true;
							break;
						}
					}

					if (!previa) {
						PUnidadConstruccion pVigenteSelectNueva = generalMB
								.copiarUnidadConstruccionAPUnidadConstruccion(pVigenteSelect);
						pVigenteSelectNueva.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
						pVigenteSelectNueva.setUsuarioLog(this.usuario.getLogin());
						pVigenteSelectNueva.setFechaLog(new Date());
						pVigenteSelectNueva.setAreaConstruida(0.0);
						pVigenteSelectNueva.setPPredio(this.predioSeleccionado);
						pVigenteSelectNueva.setId(null);
						pVigenteSelectNueva.setProvieneUnidad(pVigenteSelect.getUnidad());
						this.predioSeleccionado.getPUnidadConstruccions().add(pVigenteSelectNueva);
					}

				}
				for (UnidadConstruccion pVigenteSelect : unidadesConstruccionNoConvencionalSeleccionadas) {

					this.predioSeleccionado.setTramite(this.tramite);
					List<PUnidadConstruccion> unidadesExistentes = this.predioSeleccionado
							.getPUnidadesConstruccionNoConvencionalNuevas();
					boolean previa = false;
					for (PUnidadConstruccion puce : unidadesExistentes) {
						String idPUC = puce.getProvieneUnidad();
						String idNUC = pVigenteSelect.getUnidad();
						if (idPUC.equals(idNUC)) {
							this.addMensajeError("La construcción con uso "
									+ pVigenteSelect.getUsoConstruccion().getNombre() + " ya ha sido asociada.");
							previa = true;
							break;
						}
					}

					if (!previa) {
						PUnidadConstruccion pVigenteSelectNueva = generalMB
								.copiarUnidadConstruccionAPUnidadConstruccion(pVigenteSelect);

						pVigenteSelectNueva.setUsuarioLog(this.usuario.getLogin());
						pVigenteSelectNueva.setFechaLog(new Date());
						pVigenteSelectNueva.setAreaConstruida(0.0);
						pVigenteSelectNueva.setPPredio(this.predioSeleccionado);
						pVigenteSelectNueva.setId(null);
						pVigenteSelectNueva.setProvieneUnidad(pVigenteSelect.getUnidad());
						this.predioSeleccionado.getPUnidadConstruccions().add(pVigenteSelectNueva);
					}
				}
				boolean isUltimo = this.predioSeleccionado.isUltimoDesenglobe();
				this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
				try {
					this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
				} catch (Exception ex) {
					LOGGER.error("Error al actualizar el predio proyectado");
				}
				this.predioSeleccionado.setUltimoDesenglobe(isUltimo);

			} else if (this.tramite.isCancelacionMasiva()) {
				this.asociarConstruccionesCancelacionMasiva();
			} else {
				List<PUnidadConstruccion> unidadesExistentes = this.predioSeleccionado
						.getPUnidadesConstruccionConvencionalNuevas();
				for (UnidadConstruccion pVigenteSelect : unidadesConstruccionConvencionalSeleccionadas) {
					for (PUnidadConstruccion puce : unidadesExistentes) {
						String idPUC = puce.getUnidad();
						String idNUC = pVigenteSelect.getUnidad();
						if (idPUC.equalsIgnoreCase(idNUC)) {
							this.addMensajeWarn("Ya existe una construcción con la unidad  "
									+ pVigenteSelect.getUnidad());
							break;
						}
					}
					for (PUnidadConstruccion pNuevo : this.predioSeleccionado.getPUnidadesConstruccionConvencional()) {
						if (pVigenteSelect.getId().equals(pNuevo.getId())) {
							pNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
							pNuevo.setUsuarioLog(this.usuario.getLogin());
							pNuevo.setFechaLog(new Date());
							break;
							// this.cambiarCancelaInscribeHijos(pNuevo,
							// EProyeccionCancelaInscribe.MODIFICA.getCodigo(),
							// true);
						}
					}
				}
				List<PUnidadConstruccion> unidadesExistentesNo = this.predioSeleccionado
						.getPUnidadesConstruccionNoConvencionalNuevas();
				for (UnidadConstruccion pVigenteSelect : unidadesConstruccionNoConvencionalSeleccionadas) {
					for (PUnidadConstruccion puce : unidadesExistentesNo) {
						String idPUC = puce.getUnidad();
						String idNUC = pVigenteSelect.getUnidad();
						if (idPUC.equalsIgnoreCase(idNUC)) {
							this.addMensajeWarn("Ya existe una construcción con la unidad  "
									+ pVigenteSelect.getUnidad());
							break;
						}
					}
					for (PUnidadConstruccion pNuevo : this.getPredioSeleccionado()
							.getPUnidadesConstruccionNoConvencional()) {
						if (pVigenteSelect.getId().equals(pNuevo.getId())) {
							pNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
							pNuevo.setUsuarioLog(this.usuario.getLogin());
							pNuevo.setFechaLog(new Date());
							break;
							// this.cambiarCancelaInscribeHijos(pNuevo,
							// EProyeccionCancelaInscribe.MODIFICA.getCodigo(),
							// true);
						}
					}
				}
			}
			// Si es una mutación de segunda se realiza una actualización del
			// avalúo total de la ficha.
			if (this.tramite.isSegundaDesenglobe() && this.isCondicion8o9()) {
				this.actualizarAvaluosFichaMatriz();
			}
			// Se actualiza el valor de las áreas
			if (!this.banderaDesenglobeMejora) {
				this.guardarAreas();
			}

			if (this.tramite.isTercera() || this.tramite.isSegundaDesenglobe() || this.tramite.isQuinta()) {
				this.obligarGeografico();
			}

			if (!this.banderaPredioFiscal) {
				this.recalcularAvaluo(true);
				this.setAvaluo();
			} else {
				this.guardarAvaluoPredioFiscal();
			}

			this.verificarCIFotosDeUnidadesConstruccion();

			if (this.predioSeleccionado != null) {
				try {
					this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
				} catch (Exception e) {
					this.addMensajeError(e);
					LOGGER.error(e.getMessage());
				}
			}
			
		} catch (Exception e) {
			this.addMensajeError("Error al intentar asociar las unidades.");
			LOGGER.debug(e.getMessage());
		}

	}

	/**
	 * Método que verifica los valores de CI de las fotos basado en el valor CI de
	 * las unidades de construcción.
	 *
	 * @author david.cifuentes
	 */
	private void verificarCIFotosDeUnidadesConstruccion() {

		if (this.predioSeleccionado != null) {
			try {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());

				if (this.predioSeleccionado.getPUnidadConstruccions() != null) {

					boolean persistirCambiosBool = false;

					for (PUnidadConstruccion puc : this.predioSeleccionado.getPUnidadConstruccions()) {

						List<PFoto> pFotosUnidad = this.getConservacionService()
								.buscarPFotosUnidadConstruccion(puc.getId(), false);

						if (pFotosUnidad != null) {
							for (PFoto pf : pFotosUnidad) {
								if (puc.getCancelaInscribe() != null
										&& !puc.getCancelaInscribe().equals(pf.getCancelaInscribe())) {
									pf.setCancelaInscribe(puc.getCancelaInscribe());
									persistirCambiosBool = true;
								} else if (puc.getCancelaInscribe() == null && pf.getCancelaInscribe() != null) {
									pf.setCancelaInscribe(puc.getCancelaInscribe());
									persistirCambiosBool = true;
								}
							}
						}

						if (persistirCambiosBool) {
							this.getConservacionService().actualizarPFotosUnidadConstruccion(pFotosUnidad);
							persistirCambiosBool = false;
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			}
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Método encargado de establecer las condiciones iniciales para las unidades de
	 * construccion, las que tengan CANCELA_INSCRIBE en null pasan a C
	 */
	public void setupUnidadesConstruccion() {

		// javier.aponte :: CU-NP-CO-242 No se les hace nada a las unidades de
		// construcción en el cancela_inscribe si el
		// trámite es de tercera masiva :: 16-07-2015
		if (!this.getTramite().isSegundaEnglobe() && !this.tramite.isCuarta() && !this.tramite.isTerceraMasiva()
				&& this.getPredioSeleccionado() != null
				&& this.getPredioSeleccionado().getPUnidadConstruccions() != null) {
			for (PUnidadConstruccion ppp : this.getPredioSeleccionado().getPUnidadConstruccions()) {
				if (ppp.getCancelaInscribe() == null) {
					if (this.tramite.isPrimera() || this.tramite.isQuintaOmitido()) {
						// No hacerles nada
					} else if (this.tramite.isCancelacionPredio() || this.tramite.isCuarta()) {
						ppp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
					} else if (this.tramite.isEsComplementacion()) {
						if (ppp.getTipoConstruccion().equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
							// poner M a las convencionales
							ppp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
						} else {
							// dejar en null los anexos
						}
					} else if (this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()) {
						ppp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
					} else if (!this.tramite.isEnglobeVirtual()) {
						ppp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
						ppp.setFechaInscripcionCatastral(new Date());
					}
					this.getConservacionService().actualizarPPredio(this.getPredioSeleccionado());
					// cambiarCancelaInscribeHijos(ppp,
					// EProyeccionCancelaInscribe.CANCELA.getCodigo());
				}
			}
		}
		// felipe.cadena::RM#15185::19-11-2015::Se asociacian automaticamente
		// las construcciones de los predios originales del tramite.
		// leidy.gonzalez::RM#27036::11-07-2016::Para el englobe virtual
		// el cancela/inscribe no se modifica
		if (this.getTramite().isDesenglobeEtapas()) {
			List<PPredio> prediosTramite = this.getConservacionService()
					.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());

			for (PPredio pp : prediosTramite) {
				if (pp.isPredioOriginalTramite()) {
					for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
						if (!this.getTramite().isEnglobeVirtual()) {
							puc.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
						}
					}
					pp = this.getConservacionService().actualizarPPredio(pp);
				}
			}
		}

	}

	// ---------------------------------------------------------- //
	/**
	 * Método que asocia las construcciones del predio base al predio seleccionado
	 * para tramites de cancelación Masiva.
	 *
	 * @author david.cifuentes
	 */
	public void asociarConstruccionesCancelacionMasiva() {

		GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

		try {

			// Remover las unidades seleccionadas de la lista actual
			if (this.listUnidadesConstruccionConvencionalVigentes != null) {
				this.listUnidadesConstruccionConvencionalVigentes
						.removeAll(Arrays.asList(this.unidadesConstruccionConvencionalSeleccionadas));
			}
			if (this.listUnidadesConstruccionConvencionalVigentes != null) {
				this.listUnidadesConstruccionNoConvencionalVigentes
						.removeAll(Arrays.asList(this.unidadesConstruccionNoConvencionalSeleccionadas));
			}
			// aplica para tramites de cancelacion masiva
			if (this.tramite.isCancelacionMasiva()) {
				if (this.unidadesConstruccionConvencionalFM != null) {
					this.unidadesConstruccionConvencionalFM
							.removeAll(Arrays.asList(this.unidadesConstruccionConvencionalSeleccionadasFM));
				}
				if (this.unidadesConstruccionNoConvencionalFM != null) {
					this.unidadesConstruccionNoConvencionalFM
							.removeAll(Arrays.asList(this.unidadesConstruccionNoConvencionalSeleccionadasFM));
				}
				if (this.unidadesConstruccionConvencional != null) {
					this.unidadesConstruccionConvencional
							.removeAll(Arrays.asList(this.unidadesConstruccionConvencionalSeleccionadas));
				}
				if (this.unidadesConstruccionNoConvencional != null) {
					this.unidadesConstruccionNoConvencional
							.removeAll(Arrays.asList(this.unidadesConstruccionNoConvencionalSeleccionadas));
				}
			}

			// Consulta del predio base con sus construcciones.
			PPredio pPredioBase = this.getConservacionService()
					.obtenerPPredioCompletoById(this.tramite.getPredio().getId());

			// Consulta del predio asociado al ppredio
			Predio predioBase = this.getConservacionService().obtenerPredioPorId(this.tramite.getPredio().getId());

			// Entidad PUnidadConstruccion
			PUnidadConstruccion pUnidadConstruccionTemp;

			if (this.predioSeleccionado != null && pPredioBase != null) {

				// Mantiene nuúmero predial
				if (this.predioSeleccionado.getId().longValue() == pPredioBase.getId().longValue()) {

					// Modificar las construcciones seleccionadas del predio
					// base, para posteriormente asociarselas al predios
					// selcccionado.
					if (this.unidadesConstruccionConvencionalSeleccionadas != null
							&& this.unidadesConstruccionConvencionalSeleccionadas.length > 0) {
						List<PUnidadConstruccion> pUnidadesConstruccionConvencionalTemp = new ArrayList<PUnidadConstruccion>();
						for (UnidadConstruccion pConv : unidadesConstruccionConvencionalSeleccionadas) {

							pUnidadConstruccionTemp = generalMB.copiarUnidadConstruccionAPUnidadConstruccion(pConv);

							pUnidadConstruccionTemp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
							pUnidadConstruccionTemp.setUsuarioLog(this.usuario.getLogin());
							pUnidadConstruccionTemp.setFechaLog(new Date());
							pUnidadConstruccionTemp.setProvienePredio(predioBase);
							pUnidadConstruccionTemp.setProvieneUnidad(pConv.getUnidad());

							pUnidadesConstruccionConvencionalTemp.add(pUnidadConstruccionTemp);
						}
						this.getConservacionService()
								.guardarListaPUnidadConstruccion(pUnidadesConstruccionConvencionalTemp);
					}

					if (this.unidadesConstruccionNoConvencionalSeleccionadas != null
							&& this.unidadesConstruccionNoConvencionalSeleccionadas.length > 0) {
						List<PUnidadConstruccion> pUnidadesConstruccionNoConvencionalTemp = new ArrayList<PUnidadConstruccion>();
						for (UnidadConstruccion pNoConv : unidadesConstruccionNoConvencionalSeleccionadas) {

							pUnidadConstruccionTemp = generalMB.copiarUnidadConstruccionAPUnidadConstruccion(pNoConv);

							pUnidadConstruccionTemp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
							pUnidadConstruccionTemp.setUsuarioLog(this.usuario.getLogin());
							pUnidadConstruccionTemp.setFechaLog(new Date());
							pUnidadConstruccionTemp.setProvienePredio(predioBase);
							pUnidadConstruccionTemp.setProvieneUnidad(pNoConv.getUnidad());

							pUnidadesConstruccionNoConvencionalTemp.add(pUnidadConstruccionTemp);
						}
						this.getConservacionService()
								.guardarListaPUnidadConstruccion(pUnidadesConstruccionNoConvencionalTemp);
					}

				} else {

					// Obtener una copia del arreglo de construcciones
					// seleccionadas, para posteriormente modificarlas.
					List<PUnidadConstruccion> convencionalesAAsociar = new ArrayList<PUnidadConstruccion>();
					if (this.unidadesConstruccionConvencionalSeleccionadas != null
							&& this.unidadesConstruccionConvencionalSeleccionadas.length > 0) {
						for (UnidadConstruccion puc : this.unidadesConstruccionConvencionalSeleccionadas) {
							convencionalesAAsociar.add(generalMB.copiarUnidadConstruccionAPUnidadConstruccion(puc));
						}
					}

					List<PUnidadConstruccion> noConvencionalesAAsociar = new ArrayList<PUnidadConstruccion>();
					if (this.unidadesConstruccionNoConvencionalSeleccionadas != null
							&& this.unidadesConstruccionNoConvencionalSeleccionadas.length > 0) {
						for (UnidadConstruccion puc : unidadesConstruccionNoConvencionalSeleccionadas) {
							noConvencionalesAAsociar.add(generalMB.copiarUnidadConstruccionAPUnidadConstruccion(puc));
						}
					}

					// Cancelar las construcciones seleccionadas del predio
					// base, para posteriormente asociarselas al predios
					// selcccionado.
					if (!this.tramite.isCancelacionMasiva()) {
						if (this.unidadesConstruccionConvencionalSeleccionadas != null
								&& this.unidadesConstruccionConvencionalSeleccionadas.length > 0) {

							List<PUnidadConstruccion> pUnidadesConstruccionConvencionalTemp = new ArrayList<PUnidadConstruccion>();
							for (UnidadConstruccion pConv : unidadesConstruccionConvencionalSeleccionadas) {

								pUnidadConstruccionTemp = generalMB.copiarUnidadConstruccionAPUnidadConstruccion(pConv);
								pUnidadConstruccionTemp
										.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
								pUnidadConstruccionTemp.setUsuarioLog(this.usuario.getLogin());
								pUnidadConstruccionTemp.setFechaLog(new Date());
								pUnidadConstruccionTemp.setProvienePredio(predioBase);
								pUnidadesConstruccionConvencionalTemp.add(pUnidadConstruccionTemp);
							}
							this.getConservacionService()
									.guardarListaPUnidadConstruccion(pUnidadesConstruccionConvencionalTemp);
						}

						if (this.unidadesConstruccionNoConvencionalSeleccionadas != null
								&& this.unidadesConstruccionNoConvencionalSeleccionadas.length > 0) {
							List<PUnidadConstruccion> pUnidadesConstruccionNoConvencionalTemp = new ArrayList<PUnidadConstruccion>();
							for (UnidadConstruccion pNoConv : unidadesConstruccionNoConvencionalSeleccionadas) {

								pUnidadConstruccionTemp = generalMB
										.copiarUnidadConstruccionAPUnidadConstruccion(pNoConv);
								pUnidadConstruccionTemp
										.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
								pUnidadConstruccionTemp.setUsuarioLog(this.usuario.getLogin());
								pUnidadConstruccionTemp.setFechaLog(new Date());
								pUnidadConstruccionTemp.setProvienePredio(predioBase);
								pUnidadesConstruccionNoConvencionalTemp.add(pUnidadConstruccionTemp);
							}
							this.getConservacionService()
									.guardarListaPUnidadConstruccion(pUnidadesConstruccionNoConvencionalTemp);
						}
					}

					// Actualizar las unidades convencionales antes de
					// asociarselas al predio seleccionado.
					if (convencionalesAAsociar.size() > 0) {

						// Actualización de unidades convencionales
						for (PUnidadConstruccion ucAsociar : convencionalesAAsociar) {
							ucAsociar.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
							ucAsociar.setUsuarioLog(this.usuario.getLogin());
							ucAsociar.setFechaLog(new Date());
							ucAsociar.setId(null);
							if (this.tramite.isCancelacionMasiva()) {
								Predio predioOriginal = new Predio();
								predioOriginal.setId(ucAsociar.getPPredio().getId());
								ucAsociar.setProvienePredio(predioOriginal);
							} else {
								ucAsociar.setProvienePredio(predioBase);
							}
							ucAsociar.setPPredio(this.predioSeleccionado);
							ucAsociar.setProvieneUnidad(ucAsociar.getUnidad());
						}

						convencionalesAAsociar = this.getConservacionService()
								.guardarListaPUnidadConstruccion(convencionalesAAsociar);

						if (convencionalesAAsociar != null) {
							// Actualización de los componentes de las unidades
							// actualizadas para que tengan la misma unidad de
							// construccion padre.
							for (PUnidadConstruccion puc : convencionalesAAsociar) {
								if (puc.getPUnidadConstruccionComps() != null
										&& !puc.getPUnidadConstruccionComps().isEmpty()) {
									for (PUnidadConstruccionComp pucc : puc.getPUnidadConstruccionComps()) {
										pucc.setPUnidadConstruccion(puc);
									}
								}
							}
							// Actualizar la unidad de construcción con los
							// componentes actualizados.
							convencionalesAAsociar = this.getConservacionService()
									.guardarListaPUnidadConstruccion(convencionalesAAsociar);
						}
					}

					// Actualizar las unidades no convencionales antes de
					// asociarselas al predio seleccionado.
					if (noConvencionalesAAsociar.size() > 0) {

						// Actualización de unidades no convencionales
						for (PUnidadConstruccion ucAsociar : noConvencionalesAAsociar) {
							ucAsociar.setPPredio(this.predioSeleccionado);
							ucAsociar.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
							ucAsociar.setUsuarioLog(this.usuario.getLogin());
							ucAsociar.setFechaLog(new Date());
							ucAsociar.setId(null);
							ucAsociar.setProvienePredio(predioBase);
							ucAsociar.setProvieneUnidad(ucAsociar.getUnidad());
						}

						noConvencionalesAAsociar = this.getConservacionService()
								.guardarListaPUnidadConstruccion(noConvencionalesAAsociar);
						if (noConvencionalesAAsociar != null) {
							// Actualización de los componentes de las unidades
							// actualizadas para que tengan la misma unidad de
							// construccion padre.
							for (PUnidadConstruccion puc : noConvencionalesAAsociar) {
								if (puc.getPUnidadConstruccionComps() != null
										&& !puc.getPUnidadConstruccionComps().isEmpty()) {
									for (PUnidadConstruccionComp pucc : puc.getPUnidadConstruccionComps()) {
										pucc.setPUnidadConstruccion(puc);
									}
								}
							}
							noConvencionalesAAsociar = this.getConservacionService()
									.guardarListaPUnidadConstruccion(noConvencionalesAAsociar);

						}
					}
				}

				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
			}

		} catch (Exception e) {
			this.addMensajeError("Error al intentar asociar las unidades al predio seleccionado.");
			LOGGER.debug(e.getMessage());
		}

	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 8 o 9.
	 *
	 * @return
	 */
	public boolean isCondicion0o8o9() {
		if (this.predioSeleccionado != null
				&& (this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo()))) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 5.
	 *
	 * @return
	 */
	public boolean isCondicion5() {
		if (this.predioSeleccionado != null && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_5.getCodigo()))) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 5 o 6.
	 *
	 * @return
	 */
	public boolean isCondicion5o6() {
		if (this.predioSeleccionado != null
				&& (this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_6.getCodigo()))) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 6, 8 o 9.
	 *
	 * @return
	 */
	public boolean isCondicion6o8o9() {
		if (this.predioSeleccionado != null
				&& (this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_6.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo()))) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 8 o 9.
	 *
	 * @return
	 */
	public boolean isCondicion8o9() {
		if (this.predioSeleccionado != null
				&& (this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_1.getCodigo()))) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 2, 5, 6 o 9.
	 *
	 * @return
	 */
	public boolean isCondicion2o5o6o9() {
		if (this.predioSeleccionado != null && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_6.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_9.getCodigo()))) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que retorna si un predio tiene condición de propiedad 7.
	 *
	 * @return
	 */
	public boolean isCondicionCementerio() {
		if (this.predioSeleccionado != null
				&& this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_7.getCodigo())) {
			return true;
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que verifica si se debe o no mostrar el coeficiente de copropiedad de
	 * un predio.
	 *
	 * @return
	 */
	public boolean isMostrarCoeficiente() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitido() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitidoNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que verifica si se debe o no mostrar el total del avalúo catastral
	 * para un predio dependiendo de la mutación del trámite y de su condición de
	 * propiedad.
	 *
	 * @return
	 */
	public boolean isMostrarTotalAvaluoCatastral() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaEnglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_2.getCodigo())))
				|| (this.banderaSoloMejorasEnglobe) || (this.banderaDesenglobeMejora)
				|| (this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| this.tramite.isQuintaOmitido() || this.tramite.isQuintaNuevo() || this.tramite.isQuintaOmitidoNuevo()
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que determina si se debe o no mostrar el total del terreno de un
	 * predio dependiendo de la mutación del trámite y de la condición de propiedad
	 * del predio.
	 *
	 * @return
	 */
	public boolean isMostrarTotalTerreno() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaEnglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_2.getCodigo())))
				|| (this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.banderaSoloMejorasEnglobe) || (this.banderaDesenglobeMejora)
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())))
				|| this.tramite.isQuintaOmitido() || this.tramite.isQuintaNuevo() || this.tramite.isQuintaOmitidoNuevo()
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que determina si se debe o no mostrar el total del terreno común de un
	 * predio dependiendo de la mutación del trámite y de la condición de propiedad
	 * del predio.
	 *
	 * @return
	 */
	public boolean isMostrarTotalTerrenoComun() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitido() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitidoNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que determina si se debe o no mostrar el total del terreno privado de
	 * un predio dependiendo de la mutación del trámite y de la condición de
	 * propiedad del predio.
	 *
	 * @return
	 */
	public boolean isMostrarTotalTerrenoPrivado() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())))
				|| (this.tramite.isQuintaOmitido() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitidoNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que determina si se debe o no mostrar el total de las construcciones
	 * de un predio dependiendo de la mutación del trámite y de la condición de
	 * propiedad del predio.
	 *
	 * @return
	 */
	public boolean isMostrarTotalConstrucciones() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaEnglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_2.getCodigo())))
				|| (this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.banderaSoloMejorasEnglobe) || (this.banderaDesenglobeMejora)
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| this.tramite.isQuintaOmitido() || this.tramite.isQuintaNuevo() || this.tramite.isQuintaOmitidoNuevo()
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_0.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_2.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_5.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_6.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	/**
	 * Método que verifica si se deben o no mostrar las construcciones de un predio.
	 * Por su similitud es el mismo para privadas convencionales, no convencionales,
	 * total privada y construccion comun
	 *
	 * @return
	 */
	public boolean isMostrarConstrucciones() {
		boolean mostrar = false;
		if ((this.tramite.isSegundaDesenglobe() && (this.predioSeleccionado.getCondicionPropiedad()
				.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
				|| this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isTercera() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isCuarta() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitido() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| (this.tramite.isQuintaOmitidoNuevo() && (this.predioSeleccionado.getCondicionPropiedad()
						.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
						|| this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))
				|| ((this.tramite.isRectificacion() || this.tramite.isRevisionAvaluo()
						|| this.tramite.isEsComplementacion())
						&& (this.predioSeleccionado.getCondicionPropiedad()
								.equals(EPredioCondicionPropiedad.CP_8.getCodigo())
								|| this.predioSeleccionado.getCondicionPropiedad()
										.equals(EPredioCondicionPropiedad.CP_9.getCodigo())))) {
			mostrar = true;

		}
		return mostrar;
	}

	// ---------------------------------------------------------- //
	/**
	 * Indica cuando es editable el valor total del terreno
	 *
	 * @return
	 */
	public boolean isModificaValorTerreno() {
		return this.tramite.isCuarta()
				&& this.predioSeleccionado.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_8.getCodigo());
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * Indica cuando es editable el valor total de las construcciones
	 *
	 * @return
	 */
	public boolean isModificaValorConstrucciones() {
		// TODO :: fredy.wilches :: revisar esto. Si siempre retorna false, para
		// qué se deja??? :: pedro.garcia
		/*
		 * return this.tramite.isCuarta() &&
		 * (this.predioSeleccionado.getCondicionPropiedad
		 * ().equals(EPredioCondicionPropiedad.CP_0.getCodigo()) ||
		 * this.predioSeleccionado
		 * .getCondicionPropiedad().equals(EPredioCondicionPropiedad .CP_2.getCodigo())
		 * || this.predioSeleccionado.getCondicionPropiedad().
		 * equals(EPredioCondicionPropiedad.CP_5.getCodigo()) || this.predioSeleccionado
		 * .getCondicionPropiedad().equals(EPredioCondicionPropiedad .CP_6.getCodigo())
		 * || this.predioSeleccionado.getCondicionPropiedad().
		 * equals(EPredioCondicionPropiedad.CP_8.getCodigo()) || this.predioSeleccionado
		 * .getCondicionPropiedad().equals(EPredioCondicionPropiedad
		 * .CP_9.getCodigo()));
		 */
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que crea una nueva servidumbre para poder ser agregada después de la
	 * validación
	 */
	public void inicializarNuevaServidumbre() {
		this.servidumbreAdicional = new PPredioServidumbre();
	}

	public boolean isValorM2zonaFlag() {
		return valorM2zonaFlag;
	}

	public void setValorM2zonaFlag(boolean valorM2zonaFlag) {
		this.valorM2zonaFlag = valorM2zonaFlag;
	}

	public boolean isCalculozonaFlag() {
		return calculozonaFlag;
	}

	public void setCalculozonaFlag(boolean calculozonaFlag) {
		this.calculozonaFlag = calculozonaFlag;
	}

	// ---------------------------------------------------------- //
	/**
	 * Validación para habilitar o no el boton de envio a informacion geografica
	 *
	 * @return
	 */
	public boolean isValidoPregeografico() {
		boolean valido = true;
		if (this.tramite.isCuarta()) {
			return false;
		}
		// felipe.cadena::05-08-2015::#13709::Se valida que los tramites de
		// quinta no se vayan al editor son realizar la proyeccion
		if (this.tramite.isQuinta()) {
			List<PPredio> prediosTramite = this.getConservacionService()
					.buscarPPrediosPorTramiteId(this.tramite.getId());
			if (prediosTramite == null || prediosTramite.isEmpty()) {
				return false;
			} else {
				for (PPredio pp : prediosTramite) {
					if (pp.getFechaInscripcionCatastral() == null) {
						return false;
					}
				}
				return true;
			}
		}
		if (!(this.tramite.isSegunda() || this.tramite.isTercera() || this.tramite.isRectificacionArea()
				|| this.tramite.isCancelacionMasiva() || this.tramite.isRectificacionUso()
				|| this.tramite.isRevisionAvaluoAreaOUsoOPuntaje() || this.tramite.isRectificacionCDI())) {
			return false;
		}
		if (this.tramite.isSegundaDesenglobe()) {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
			if (this.entradaProyeccionMB != null) {
				if (this.entradaProyeccionMB.getPrediosResultantes() == null
						|| this.entradaProyeccionMB.getPrediosResultantes().isEmpty()
						|| this.entradaProyeccionMB.getPrediosResultantes().size() < 2) {
					valido = false;
				} else {
					// PPredio predioSeleccionadoActual=this.predioSeleccionado;
					/*
					 * Comentado por FGWL 20131119. No tiene sentido validar que en un desenglobe
					 * todos los predios resultantes tengan almenos una construccion for (PPredio p
					 * : this.entradaProyeccionMB .getPrediosResultantes()) { if
					 * (p.getPUnidadesConstruccionConvencionalNuevas() == null ||
					 * p.getPUnidadesConstruccionNoConvencionalNuevas() == null ||
					 * p.getPUnidadesConstruccionConvencionalNuevas() .size() +
					 * p.getPUnidadesConstruccionNoConvencionalNuevas() .size() == 0) { valido =
					 * false; break; } }
					 */
				}
			}
		}

		if (this.tramite.isSegundaEnglobe()) {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
			if (this.entradaProyeccionMB != null) {
				if (this.entradaProyeccionMB.getPrediosResultantes() == null
						|| this.entradaProyeccionMB.getPrediosResultantes().isEmpty()) {
					valido = false;
				}
			}
		}
		return valido;
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que permite determinar si se muestra o nó la fecha de título (Fecha de
	 * inscripción catastral) en la pantalla de gestionar inscripciones y decretos
	 *
	 * @author javier.aponte
	 */
	public void mostrarFechaTituloEnGestionarInscripcionesYDecretos() {
		this.banderaMostrarFechaTitulo = false;
		this.banderaMostrarFechaInscripcion = false;
		this.banderaIngresarFechaInscripcion = false;
		if (this.tramite.isPrimera() || this.tramite.isSegunda() || this.tramite.isQuintaNuevo()
				|| this.tramite.isQuintaOmitidoNuevo()) {
			this.banderaMostrarFechaTitulo = true;
		}
		if (this.tramite.isTercera() || this.tramite.isCuarta() || this.tramite.isQuintaOmitido()
				|| this.tramite.isRevisionAvaluo()) {
			if (this.actualizacionEncontradaBool || this.tramite.isCuarta()) {
				this.banderaMostrarFechaInscripcion = true;
			} else {
				this.banderaIngresarFechaInscripcion = true;
			}
		}
		if (this.tramite.isRectificacion() || this.tramite.isModificacionInscripcionCatastral()) {
			this.banderaIngresarFechaInscripcion = true;
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Metodo invocado desde el detalle del avaluo al hacer click en el check
	 * Autoavaluo
	 */
	public void setearValoresAutoavaluo() {

		if (this.avaluo.getAutoavaluo().equals(ESiNo.SI.getCodigo())) {

			boolean registroVigenciaYaExiste = false;

			Calendar c = Calendar.getInstance();
			c.set(Calendar.DATE, 1);
			c.set(Calendar.MONTH, 0);
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			c.add(Calendar.YEAR, 1);

			for (PPredioAvaluoCatastral pac : this.predioSeleccionado.getPPredioAvaluoCatastrals()) {
				if (UtilidadesWeb.getAnioDeUnDate(pac.getVigencia()) == UtilidadesWeb.getAnioDeUnDate(c.getTime())) {
					registroVigenciaYaExiste = true;
					this.avaluo = pac;
				}
			}

			if (this.avaluo == null || !registroVigenciaYaExiste) {
				this.avaluo = new PPredioAvaluoCatastral();
			}

			this.avaluo.setAutoavaluo(ESiNo.SI.getCodigo());

			this.avaluo.setValorTotalConsConvencional(0.0);
			this.avaluo.setValorTerrenoPrivado(0.0);
			this.avaluo.setValorTerrenoComun(0.0);
			this.avaluo.setValorTotalConstruccionPrivada(0.0);
			this.avaluo.setValorConstruccionComun(0.0);
			this.avaluo.setValorTotalConsNconvencional(0.0);

			this.avaluo.setValorTerreno(this.tramite.getAutoavaluoTerreno());
			this.avaluo.setValorTotalConstruccion(this.tramite.getAutoavaluoConstruccion());
			this.avaluo.setValorTotalAvaluoCatastral(this.avaluo.getValorTerreno().doubleValue()
					+ this.avaluo.getValorTotalConstruccion().doubleValue());
			this.avaluo.setValorTotalAvaluoVigencia(this.avaluo.getValorTotalAvaluoCatastral());

			this.avaluo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
			this.avaluo.setUsuarioLog(this.usuario.getLogin());
			this.avaluo.setFechaLog(new Date());

			this.avaluo.setVigencia(c.getTime());
			this.avaluo.setPPredio(this.predioSeleccionado);

			this.predioSeleccionado.setAvaluoCatastral(this.avaluo.getValorTotalAvaluoCatastral());
			this.avaluo = this.getConservacionService().actualizarPPredioAvaluoCatastral(this.avaluo);

		} else {

			this.getConservacionService().eliminarPPredioAvaluoCatastral(this.avaluo);
			this.avaluo.setAutoavaluo(ESiNo.NO.getCodigo());

			// Se consultan nuevamente los avalúos actualizados del predio.
			List<PPredioAvaluoCatastral> avaluosActualizadosPPredio = this.getConservacionService()
					.obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());

			this.predioSeleccionado.setPPredioAvaluoCatastrals(avaluosActualizadosPPredio);

			this.setAvaluo();

			/*
			 * this.avaluo.setValorTerreno(pac.getValorTerreno());
			 * this.avaluo.setValorTotalConstruccion(pac .getValorTotalConstruccion());
			 * this.avaluo.setValorTotalAvaluoCatastral(pac
			 * .getValorTotalAvaluoCatastral());
			 * this.predioSeleccionado.setAvaluoCatastral(pac
			 * .getValorTotalAvaluoCatastral()); this.avaluo.setCancelaInscribe(null);
			 */
		}

	}

	// ---------------------------------------------------------- //
	/**
	 * Método que verifica si se debe o no mostrar el destino de un predio.
	 *
	 * @return
	 */
	public boolean isMostrarMensajeDestino() {
		this.predioSeleccionado.setTramite(this.tramite);
		return this.predioSeleccionado != null
				&& (this.predioSeleccionado.getDestino().equals("Q") || this.predioSeleccionado.getDestino().equals("R")
						|| this.predioSeleccionado.getDestino().equals("S"))
				&& (this.predioSeleccionado.getPUnidadesConstruccionConvencionalNuevas().size() > 0
						|| this.predioSeleccionado.getPUnidadesConstruccionNoConvencionalNuevas().size() > 0);
	}

	// ---------------------------------------------------------- //
	/**
	 * Método para mostrar el puntaje del arbol de componentes de la c construcción.
	 *
	 * @param treeComponentesConstruccion
	 */
	public void mostrarPuntos(TreeNode treeComponentesConstruccion) {

		int answer = 0;

		List<TreeNode> calificacionConstruccionNodesLevel2;
		CalificacionConstruccionTreeNode cctnTempL2;

		for (TreeNode currentComponenteNode : treeComponentesConstruccion.getChildren()) {
			calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
			for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
				cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
				answer += (cctnTempL2.getSelectedDetalle() == null) ? 0 : cctnTempL2.getSelectedDetalle().getPuntos();
				LOGGER.debug("DETALLE " + cctnTempL2.getSelectedDetalle());
			}
		}
		LOGGER.debug("TOTAL " + answer);

	}

	// ---------------------------------------------------------- //
	/**
	 * Método que verifica si la construcción debe o no tener baño.
	 *
	 * @return
	 */
	public boolean isSinBano() {
		List<TreeNode> calificacionConstruccionNodesLevel2;
		CalificacionConstruccionTreeNode cctnTempL2;
		if (treeComponentesConstruccion == null || treeComponentesConstruccion.getChildren() == null) {
			// Por migracion puede no haber componentes
			return false;
		}
		for (TreeNode currentComponenteNode : treeComponentesConstruccion.getChildren()) {
			calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
			for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
				cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
				if (((CalificacionConstruccionTreeNode) currentComponenteNode.getData()).getCalificacionConstruccion()
						.getComponente().equals("BAÑO")
						&& cctnTempL2.getCalificacionConstruccion().getElemento().equals("TAMAÑO")) {
					if (cctnTempL2.getSelectedDetalle().getDetalleCalificacion().equals("SIN BAÑO")) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 *
	 * @return
	 */
	public boolean isCalificaBano() {
		List<TreeNode> calificacionConstruccionNodesLevel2;
		CalificacionConstruccionTreeNode cctnTempL2;
		if (treeComponentesConstruccion == null || treeComponentesConstruccion.getChildren() == null) {
			// Por migracion puede no haber componentes
			return false;
		}
		for (TreeNode currentComponenteNode : treeComponentesConstruccion.getChildren()) {
			calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
			for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
				cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
				if (((CalificacionConstruccionTreeNode) currentComponenteNode.getData()).getCalificacionConstruccion()
						.getComponente().equals("BAÑO")
						&& cctnTempL2.getCalificacionConstruccion().getElemento().equals("TAMAÑO")) {
					return true;
				}
			}
		}
		return false;
	}

	// ---------------------------------------------------------- //
	/**
	 * Copia el archivo seleccionado en una carpeta temporal
	 *
	 * @param eventoCarga
	 * @param toWebTemp   indica que el archivo se va a cargar en la ruta web de
	 *                    imágenes temporales
	 * @return
	 */
	/*
	 * modified pedro.garcia 07-05-2013 se usa método de UtilidadesWeb para carga
	 * del archivo
	 */
	private File copiarArchivoUploaded(FileUploadEvent eventoCarga, boolean toWebTemp) {

		File archivoTemporal = null;
		Object[] cargaArchivo;

		cargaArchivo = new Object[3];
		if (!toWebTemp) {
			cargaArchivo = UtilidadesWeb.cargarArchivoATemp(eventoCarga, true);
		}

		if (cargaArchivo[0] == null) {
			LOGGER.error("ERROR: " + cargaArchivo[2]);
			this.addMensajeError((String) cargaArchivo[1]);
		} else {
			archivoTemporal = (File) cargaArchivo[0];
		}
		return archivoTemporal;

	}

	// --------------------------------------------------------------------------------------------------
	public int getAnchoImagen() {
		return this.anchoImagen;
	}

	public void setAnchoImagen(int anchoImagen) {
		this.anchoImagen = anchoImagen;
	}

	public int getAltoImagen() {
		return this.altoImagen;
	}

	public void setAltoImagen(int altoImagen) {
		this.altoImagen = altoImagen;
	}

	// --------------------------------------------------------------------------------------------------
	// public static void main(String args[]) {
	// ImageIcon ii;
	//
	// java.net.URL imgURL;
	// try {
	// TODO :: fredy.wilches :: ¿por qué está quemada una url? ¿para qué se
	// hace esto?. Verificar si el código comentado debe quedar::
	// pedro.garcia
	// imgURL = new URL(
	// "http://172.17.128.100/snc-web/2012.08.22_12.06.24_Imagen_geografica_del_predio_antes_del_tramite.jpg");
	// if (imgURL != null) {
	// ii = new ImageIcon(imgURL, "");
	// System.out.println(ii.getIconWidth());
	// System.out.println(ii.getIconHeight());
	//
	// }
	//
	// } catch (MalformedURLException e) {
	// LOGGER.error("error en al main obteniendo la url de 'antes' del predio: "
	// + e.getMessage());
	// }

	// --------------------------------------------------------------------------------------------------
	public List<PPredioZona> getZonasNuevas() {

		// ----------------------------------------- //
		boolean tieneReplicaBool = false;
		boolean zonasConValorCI = false;

		// #15871 :: Para trámites geográficos se muestran las zonas sólo cuando
		// se ha ido a geográfico :: david.cifuentes :: 23/02/2016
		if (this.tramite.isTramiteGeografico() && this.tramite.getTramiteDocumentos() != null) {
			for (TramiteDocumento td : this.tramite.getTramiteDocumentos()) {
				if (td.getDocumento() != null && ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId()
						.equals(td.getDocumento().getTipoDocumento().getId())) {
					tieneReplicaBool = true;
					break;
				}
			}
			if (!tieneReplicaBool) {
				return new ArrayList<PPredioZona>();
			}
		}

		// #15871 :: Para rectificación de zonas se muestran las zonas sólo
		// después de realizar el cálculo de zonas :: david.cifuentes ::
		// 23/02/2016
		if (this.tramite.isRectificacionZona() && this.predioSeleccionado.getPPredioZonas() != null) {
			for (PPredioZona ppz : this.predioSeleccionado.getPPredioZonas()) {
				if (ppz.getCancelaInscribe() != null) {
					zonasConValorCI = true;
				}
			}
			if (!zonasConValorCI) {
				return new ArrayList<PPredioZona>();
			}
		}

		// ----------------------------------------- //
		List<PPredioZona> zonas = new ArrayList<PPredioZona>();
		if (this.banderaSoloMejorasEnglobe || this.banderaDesenglobeMejora) {
			PPredioZona ppz = new PPredioZona();
			ppz.setArea(0.0);
			ppz.setZonaFisica("00");
			ppz.setZonaGeoeconomica("00");
			ppz.setValorM2Terreno(0.0);
			ppz.setAvaluo(0.0);
			ppz.setPPredio(this.predioSeleccionado);
			zonas.add(ppz);

		} else {
			if (this.predioSeleccionado != null && this.predioSeleccionado.getPPredioZonas() != null) {
				if (!this.tramite.isRectificacion()) {
					for (PPredioZona ppz : this.predioSeleccionado.getPPredioZonas()) {
						// v1.1.8
						if (ppz.getCancelaInscribe() != null
								&& ppz.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
							zonas.add(ppz);
							// Se valida que para los quinta omitido no
							// verifique el Inscribe
						} else if (this.tramite.isQuintaOmitido() && ppz.getCancelaInscribe() != null
								&& (ppz.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo())
										|| ppz.getCancelaInscribe()
												.equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo()))) {
							zonas.addAll(this.predioSeleccionado.getPPredioZonas());
						}

					}
					// Adicionalmente, se deben mostrar únicamente las zonas
					// vigentes, es decir, las del año actual, en caso de que no
					// existieran, se deben mostrar las del año anterior
					if (!zonas.isEmpty()) {
						zonas = this.verificarZonaVigente(zonas);
					}
				} else {
					// #12224 :: david.cifuentes :: Se visualizan las zonas de
					// la última vigencia sin importar el cancela inscribe que
					// tengan, si ya se fue a la geográfica se deben visualizar
					// únicamente las que tengan cancela inscribe en I o M ::
					// 20/04/2015
					List<PPredioZona> zonasVigentes = this
							.verificarZonaVigente(this.predioSeleccionado.getPPredioZonas());

					if (zonasVigentes != null && !zonasVigentes.isEmpty()) {
						for (PPredioZona ppz : zonasVigentes) {
							if ((EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(ppz.getCancelaInscribe())
									|| EProyeccionCancelaInscribe.MODIFICA.getCodigo()
											.equals(ppz.getCancelaInscribe())) && ppz.getArea()>0 ) {
								zonas.add(ppz);
							}
						}
						if (zonas.isEmpty()) {
							zonas = this.verificarZonaVigente(this.predioSeleccionado.getPPredioZonas());
						}
					}
				}
			}
		}
		return zonas;
	}

	// -------------------------------------------------------------------------------------------------
	/**
	 * Calcula el identificador que se le da a la unidad de del modelo de
	 * contrucción que se va a crear.
	 *
	 * @author david.cifuentes
	 *
	 *         Copia del método calcularUnidadConstruccionUnidad ajustada para
	 *         PFichaMatrizModelo
	 */
	private String calcularIdentificadorUnidadDelModelo(PFichaMatrizModelo pFichaMatrizModelo) {

		String currentUnidad, answer, lastUnidad;

		lastUnidad = "";
		if (pFichaMatrizModelo.getPFmModeloConstruccions() != null
				&& !pFichaMatrizModelo.getPFmModeloConstruccions().isEmpty()) {
			for (PFmModeloConstruccion pfmmcTemp : pFichaMatrizModelo.getPFmModeloConstruccions()) {
				currentUnidad = pfmmcTemp.getUnidad();
				if (currentUnidad != null && currentUnidad.compareToIgnoreCase(lastUnidad) > 0) {
					lastUnidad = currentUnidad;
				}
			}
		}
		answer = UtilidadesWeb.calcularSiguienteUnidadUnidadConstruccion(lastUnidad);

		return answer;
	}

	// ---------------------------------------------------------------------- //
	/**
	 * Método que realiza el cargue de las construcciones del predio base en
	 * mutación segunda desenglobe
	 *
	 * @author david.cifuentes.
	 */
	public void cargarConstruccionesDesenglobe() {

		GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

		this.listUnidadesConstruccionConvencionalVigentes = new ArrayList<UnidadConstruccion>();
		this.listUnidadesConstruccionNoConvencionalVigentes = new ArrayList<UnidadConstruccion>();
		this.unidadesConstruccionConvencionalSeleccionadas = null;
		this.unidadesConstruccionNoConvencionalSeleccionadas = null;

		try {
			if (ESiNo.NO.getCodigo().equals(this.tramite.getMantieneNumeroPredial())) {
				// Consulta del predio base con sus construcciones
				PPredio pPredioBase = this.getConservacionService()
						.obtenerPPredioCompletoById(this.tramite.getPredio().getId());

				if (pPredioBase != null && pPredioBase.getPUnidadConstruccions() != null
						&& !pPredioBase.getPUnidadConstruccions().isEmpty()) {

					// Construcciones convencionales
					if (pPredioBase.getPUnidadesConstruccionConvencional() != null
							&& !pPredioBase.getPUnidadesConstruccionConvencional().isEmpty()) {
						for (PUnidadConstruccion puc : pPredioBase.getPUnidadesConstruccionConvencional()) {

							if (puc.getProvienePredio() == null) {
								puc.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
								this.listUnidadesConstruccionConvencionalVigentes
										.add(generalMB.copiarPUnidadConstruccionAUnidadConstruccion(puc));
							}
						}
					}
					// Construcciones no convencionales
					if (pPredioBase.getPUnidadesConstruccionNoConvencional() != null
							&& !pPredioBase.getPUnidadesConstruccionNoConvencional().isEmpty()) {
						for (PUnidadConstruccion puc : pPredioBase.getPUnidadesConstruccionNoConvencional()) {

							if (puc.getProvienePredio() == null) {
								puc.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
								this.listUnidadesConstruccionNoConvencionalVigentes
										.add(generalMB.copiarPUnidadConstruccionAUnidadConstruccion(puc));
							}
						}
					}
				}
			} else {
				this.listUnidadesConstruccionConvencionalVigentes = new ArrayList<UnidadConstruccion>();
				this.listUnidadesConstruccionNoConvencionalVigentes = new ArrayList<UnidadConstruccion>();

				// Cargue de las construcciones Originales
				List<UnidadConstruccion> unidadesConstruccionPredioOrigen = this.getConservacionService()
						.obtenerUnidadesNoCanceladasByPredioId(this.tramite.getPredio().getId());

				if (unidadesConstruccionPredioOrigen != null && !unidadesConstruccionPredioOrigen.isEmpty()) {
					for (UnidadConstruccion uc : unidadesConstruccionPredioOrigen) {
						if (EUnidadTipoConstruccion.CONVENCIONAL.getCodigo().equals(uc.getTipoConstruccion().trim())
								&& uc.getAnioCancelacion() == null) {

							if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
								this.listUnidadesConstruccionConvencionalVigentes.add(uc);
							}
						}
						if (EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()
								.equals(uc.getTipoConstruccion().trim())) {
							if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
								this.listUnidadesConstruccionNoConvencionalVigentes.add(uc);
							}
						}
					}
				}
			}

			// Revisar activación del botón asociar construcciones
			int activate = this.listUnidadesConstruccionConvencionalVigentes.size()
					+ this.listUnidadesConstruccionNoConvencionalVigentes.size();
			if (activate > 0) {
				this.existenConstruccionesVigentesBool = true;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	public void cargarConstruccionesDesenglobeMejora() {
		this.unidadesConsVigentesDesenglobeMejora = new ArrayList<UnidadConstruccion>();
		this.unidadesConsNoConvencionalesVigentesDesenglobeMejora = new ArrayList<UnidadConstruccion>();
		this.unidadesConstruccionConvencionalSeleccionadas = null;
		this.unidadesConstruccionNoConvencionalSeleccionadas = null;
		try {
			GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

			PPredio pp = this.getConservacionService().obtenerPPredioDatosAvaluo(this.tramite.getPredio().getId());

			if (pp.getPUnidadConstruccions() != null) {
				List<UnidadConstruccion> UCVigententes = new ArrayList<UnidadConstruccion>();
				for (PUnidadConstruccion puc : pp.getPUnidadesConstruccionConvencional()) {
					if (puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
						UnidadConstruccion uc = generalMB.copiarPUnidadConstruccionAUnidadConstruccion(puc);
						if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
							UCVigententes.add(uc);
						}
					}
				}
				this.setUnidadesConsVigentesDesenglobeMejora(UCVigententes);

				List<UnidadConstruccion> UCVigententesNC = new ArrayList<UnidadConstruccion>();
				for (PUnidadConstruccion puc : pp.getPUnidadesConstruccionNoConvencional()) {
					if (puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
						UnidadConstruccion uc = generalMB.copiarPUnidadConstruccionAUnidadConstruccion(puc);
						if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
							UCVigententes.add(uc);
						}
					}
				}
				this.setUnidadesConsNoConvencionalesVigentesDesenglobeMejora(UCVigententesNC);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}

	}

	/**
	 * Método que ejecuta el procedimiento que valida la proyección.
	 *
	 * @author david.cifuentes
	 *
	 */
	@SuppressWarnings("rawtypes")
	public void validarProyeccion() {

		// No se realiza validacion para tramites de englobe virtual
		if (this.tramite.isEnglobeVirtual()) {
			return;
		}

		// ============================================================= //
		// == Llamado al procedimiento de validación de la proyección == //
		// ============================================================= //
		try {
			Long idTramiteSeccionDatos = null;
			if (this.configuracion != null) {

				if (this.listaValidacionesTramiteSegundaDesenglobe == null) {

					this.listaValidacionesTramiteSegundaDesenglobe = new ArrayList<ValidacionProyeccionDelTramiteDTO>();
				}

				idTramiteSeccionDatos = this.configuracion.getId();

				Object[] validarProyeccionObject = this.getTramiteService().validarProyeccion(this.tramite.getId(),
						idTramiteSeccionDatos);

				if (validarProyeccionObject != null && validarProyeccionObject.length > 0) {

					int sizeObject = ((ArrayList) validarProyeccionObject[0]).size();
					if (sizeObject > 0) {
						Object[] nuevoArrayObject = new Object[sizeObject];
						ValidacionProyeccionDelTramiteDTO validacionDelTramite;
						for (int row = 0; row < sizeObject; row++) {
							nuevoArrayObject = (Object[]) ((ArrayList) validarProyeccionObject[0]).get(row);
							validacionDelTramite = new ValidacionProyeccionDelTramiteDTO();
							if (nuevoArrayObject[0] != null) {
								validacionDelTramite
										.setTramiteId(Long.parseLong(nuevoArrayObject[0].toString().trim()));
							}
							if (nuevoArrayObject[1] != null) {
								validacionDelTramite.setPredioId(Long.parseLong(nuevoArrayObject[1].toString().trim()));
							}
							if (nuevoArrayObject[2] != null) {
								validacionDelTramite.setSeccion(nuevoArrayObject[2].toString());
							}
							if (nuevoArrayObject[3] != null) {
								validacionDelTramite.setMensajeError(nuevoArrayObject[3].toString());
							}
							this.listaValidacionesTramiteSegundaDesenglobe.add(validacionDelTramite);
						}
					}
				}

				// Llamado al método que actualiza los mensajes de
				// validación para mostrarselos al usuario
				actualizarMensajesDeValidacion();
			}

			if ((this.listaValidacionesTramiteSegundaDesenglobe != null
					&& this.listaValidacionesTramiteSegundaDesenglobe.isEmpty()) || this.tramite.isTerceraMasiva()
					|| this.tramite.isRectificacionMatriz()) {

				/**
				 * Si la lista de validaciones masiva no tiene errores, la validación del
				 * trámite es correcta. Para el caso de los trámites de tercera masiva esta
				 * lista no se llena por lo tanto no hay errores y entra a este condicional
				 */
				if (!this.tramite.isSegundaDesenglobe()) {
					this.addMensajeInfo("Se validó la proyección del trámite correctamente.");
				}

				if (this.configuracion.getPropietarios().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.PROPIETARIOS, true);
				}
				if (this.configuracion.getJustificacionPropiedad().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD, true);
				}
				if (this.configuracion.getInscripcionesDecretos().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.INSCRIPCIONES_DECRETOS, true);
				}
				if (this.configuracion.getTextoMotivado().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.TEXTO_MOTIVADO, true);
				}
				if (this.configuracion.getUbicacion().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.UBICACION, true);
				}
				if (this.configuracion.getAvaluos().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.AVALUOS, true);
				}
				if (this.configuracion.getProyeccionTramite().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.PROYECCION_TRAMITE, true);
				}
				if (this.configuracion.getFichaMatriz().equals(ESiNo.SI.toString())) {
					this.validezSeccion.put(ESeccionEjecucion.FICHA_MATRIZ, true);
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Error al realizar las validación de secciones para los predios del trámite.");
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que actualiza el objeto ValidacionProyeccionDelTramiteDTOque tiene los
	 * mensajes a mostrar al usuario los errores de validación de la proyección del
	 * trámite.
	 *
	 * @author david.cifuentes
	 */
	public void actualizarMensajesDeValidacion() {
		if (this.listaValidacionesTramiteSegundaDesenglobe != null
				&& !this.listaValidacionesTramiteSegundaDesenglobe.isEmpty()) {
			for (ValidacionProyeccionDelTramiteDTO vpt : this.listaValidacionesTramiteSegundaDesenglobe) {
				// Actualización del nombre de las secciones
				if (vpt.getSeccion() != null) {
					if (vpt.getSeccion().equals(ESeccionEjecucion.ASOCIAR_DOCUMENTOS.toString())) {
						vpt.setSeccion(ESeccionEjecucion.ASOCIAR_DOCUMENTOS.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.AVALUOS.toString())) {
						vpt.setSeccion(ESeccionEjecucion.AVALUOS.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.FICHA_MATRIZ.toString())) {
						vpt.setSeccion(ESeccionEjecucion.FICHA_MATRIZ.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.UBICACION.toString())) {
						vpt.setSeccion(ESeccionEjecucion.UBICACION.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.HISTORICO_MODIFICACIONES.toString())) {
						vpt.setSeccion(ESeccionEjecucion.HISTORICO_MODIFICACIONES.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.INSCRIPCIONES_DECRETOS.toString())) {
						vpt.setSeccion(ESeccionEjecucion.INSCRIPCIONES_DECRETOS.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD.toString())) {
						vpt.setSeccion(ESeccionEjecucion.JUSTIFICACION_PROPIEDAD.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.PROPIETARIOS.toString())) {
						vpt.setSeccion(ESeccionEjecucion.PROPIETARIOS.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.PROYECCION_TRAMITE.toString())) {
						vpt.setSeccion(ESeccionEjecucion.PROYECCION_TRAMITE.getNombre());
					}
					if (vpt.getSeccion().equals(ESeccionEjecucion.TEXTO_MOTIVADO.toString())) {
						vpt.setSeccion(ESeccionEjecucion.TEXTO_MOTIVADO.getNombre());
					}
				}
			}
		}
	}

	// ------------------------------------------------------------- //
	/**
	 * Método que actualiza los avalúos y las áreas de la ficha matriz.
	 *
	 * @author david.cifuentes
	 */
	public void actualizarAvaluosYAreasFichaMatriz() {

		try {
			if (this.fichaMatrizMB == null) {
				this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
			}

			if (this.predioSeleccionado != null && this.fichaMatrizSeleccionada != null) {
				// Consultar la ficha matriz actualizada
				this.fichaMatrizSeleccionada = this.getConservacionService()
						.buscarPFichaMatrizPorPredioId(this.predioSeleccionado.getId());
				this.verificarTorresEnFirme();

				this.fichaMatrizMB.setFichaMatriz(this.fichaMatrizSeleccionada);

				// Actualizar avalúos del predio.
				recalcularAvaluo(true);

				// Actualización del avalúo total de la ficha.
				if (this.tramite.isSegundaDesenglobe()) {
					actualizarAvaluosFichaMatriz();
				}
				this.fichaMatrizMB.setFichaMatriz(this.fichaMatrizSeleccionada);

				// Consultar predio con sus avalúos actualizados
				if (this.predioSeleccionado != null) {
					this.predioSeleccionado = this.getConservacionService()
							.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
					setAvaluo();
				}

				// Se actualiza el valor de las áreas
				this.actualizarAreasFichaMatrizBool = true;
				guardarAreas();
			}
		} catch (Exception e) {
			LOGGER.error("ERROR: " + e.getMessage());
		}
	}

	public void editaConstruccion() {
		if (this.tramite.isTercera() || this.tramite.isSegundaDesenglobe()) {
			this.informacionMigracionIncompleta = false;
		}
		this.validacionDetalleCalificacionConstruc();
		puntajeTotal = getTotalPuntosConstruccion();
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que carga el piso de la unidad en el combo box de pisos en la pantalla
	 * de gestionDetalleUnidadContruccion.
	 *
	 * @author david.cifuentes
	 */
	public void cargarPisoUnidadConstruccion() {

		// Si se tiene la ubicación del piso se carga.
		if (this.unidadConstruccionSeleccionada == null
				|| this.unidadConstruccionSeleccionada.getPisoUbicacion() == null
				|| this.unidadConstruccionSeleccionada.getPisoUbicacion().trim().isEmpty()) {

			if (this.predioSeleccionado != null && this.predioSeleccionado.isEsPredioEnPH()) {
				// Si no se tiene la ubicación del piso se carga
				// basandose en el número predial.
				if (this.predioSeleccionado.getPiso() != null && !this.predioSeleccionado.getPiso().trim().isEmpty()
						&& this.predioSeleccionado.getNumeroUnidad() != null
						&& !this.predioSeleccionado.getNumeroUnidad().trim().isEmpty()) {

					String pisoUbicacion = this.predioSeleccionado.getPiso();

					if (pisoUbicacion.substring(0, 1).equals("9") || pisoUbicacion.substring(0, 1).equals("8")) {
						int numeroSotano = 100 - Integer.valueOf(pisoUbicacion);
						// Es un sotano
						if (numeroSotano < 10) {
							pisoUbicacion = "ST-0".concat("" + numeroSotano);
						} else {
							pisoUbicacion = "ST-".concat("" + numeroSotano);
						}
					} else {
						// Es un piso
						if (pisoUbicacion.equals("00")) {
							pisoUbicacion = EUnidadPisoUbicacion.PISO_1.getCodigo();
						} else {
							pisoUbicacion = "PS-".concat(pisoUbicacion);
						}
					}

					for (EUnidadPisoUbicacion e : EUnidadPisoUbicacion.values()) {
						if (e.getCodigo().equals(pisoUbicacion)) {
							pisoUbicacion = e.getCodigo();
							break;
						}
					}
					this.unidadConstruccionSeleccionada.setPisoUbicacion(pisoUbicacion);

				} else {
					// Se toma por defecto el piso 1
					this.unidadConstruccionSeleccionada.setPisoUbicacion(EUnidadPisoUbicacion.PISO_1.getCodigo());
				}
			}
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que revisa si existen unidades de construcción vigentes para el predio
	 * seleccionado.
	 *
	 * @author david.cifuentes
	 */
	/*
	 * @modified jonathan.chacon :: se realiza ajuste para habilitar el boton en
	 * predios fiscales ya que el transient no lo permite
	 */
	public void revisarExistenciaConstruccionesVigentes() {

		int activate = 0;

		if (this.banderaPredioFiscal && this.tramite.isTercera()) {

			if (this.predioSeleccionado.getPUnidadConstruccions() != null) {
				activate = this.predioSeleccionado.getPUnidadConstruccions().size();

			}

		} else {
			// Revisar activación del botón asociar construcciones
			activate = this.predioSeleccionado.getPUnidadesConstruccionConvencionalVigentes().size()
					+ this.predioSeleccionado.getPUnidadesConstruccionNoConvencionalVigentes().size();
		}
		if (activate > 0) {
			this.existenConstruccionesVigentesBool = true;
		}
	}

	// ---------------------------------------------------------- //
	public String getRutaLocalFotoAnexo() {
		return this.rutaLocalFotoAnexo;
	}

	public void setRutaLocalFotoAnexo(String rutaLocalFotoAnexo) {
		this.rutaLocalFotoAnexo = rutaLocalFotoAnexo;
	}

	public String getRutaWebTemporalFotoAnexo() {
		return this.rutaWebTemporalFotoAnexo;
	}

	public void setRutaWebTemporalFotoAnexo(String rutaWebTemporalFotoAnexo) {
		this.rutaWebTemporalFotoAnexo = rutaWebTemporalFotoAnexo;
	}

	public boolean isIsAnexoCurrentOrNewUnidadConstruccion() {
		return this.isAnexoCurrentOrNewUnidadConstruccion;
	}

	public void setIsAnexoCurrentOrNewUnidadConstruccion(boolean isAnexoCurrentOrNewUnidadConstruccion) {
		this.isAnexoCurrentOrNewUnidadConstruccion = isAnexoCurrentOrNewUnidadConstruccion;
	}

	public String getRutaWebTemporalFoto() {
		return this.rutaWebTemporalFoto;
	}

	public void setRutaWebTemporalFoto(String rutaWebTemporalFoto) {
		this.rutaWebTemporalFoto = rutaWebTemporalFoto;
	}

	public String getRutaTemporalFoto() {
		return this.rutaTemporalFoto;
	}

	public void setRutaTemporalFoto(String rutaTemporalFoto) {
		this.rutaTemporalFoto = rutaTemporalFoto;
	}

	// --------------------------------------------------------------------------------------------------
	/**
	 * action del botón "cerrar" en la ventana de detalle de la unidad de
	 * construcción.
	 *
	 * Vacía la lista de pfotos a borrar, ya que se pudo haber hecho el cambio de
	 * una foto, pero al final no se guardan los cambios, yb si se sigue trabajando
	 * el el mismo cu se puede llegar a que se borran las que no se debe
	 */
	public void cerrarGestionDetallesUC() {

		this.listaPFotosABorrar = null;
		if (this.fichaMatrizMB == null) {
			this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
		}

		if (this.tramite.isRectificacionMatriz()) {
			this.fichaMatrizMB.setupUnidadesDeConstruccionRectMatriz();
		} else if (this.tramite.isTerceraMasiva()) {
			this.fichaMatrizMB.setupUnidadesDeConstruccionTerceraMasiva();
		} else {
			this.fichaMatrizMB.setupUnidadesDeConstruccion();
		}
	}

	// --------------Métodos caso de uso liquidar avalúos de trámites
	/**
	 * Método que se ejecuta cuando se desea calcular el avalúo manual en forma
	 * retroactiva
	 *
	 * @author javier.aponte
	 */
	public void calcularAvaluoEnFormaRetroactiva() {

		this.mostrarCalculoAvaluoPorRangoDeAnios = false;
		this.mostrarCalculoAvaluoPorAnio = false;
		if (ESiNo.SI.getCodigo().equals(this.calculoAvaluoEnFormaRetroactiva)) {
			this.mostrarCalculoAvaluoPorRangoDeAnios = true;
		} else if (ESiNo.NO.getCodigo().equals(this.calculoAvaluoEnFormaRetroactiva)) {

			this.aniosCalculoAvaluoPorRango.clear();

			for (VPPredioAvaluoDecreto vppad : this.pPrediosAvaluosCatastrales) {

				if (vppad.getValorTotalAvaluoCatastral().doubleValue() == 0 && vppad.getVigencia() != null) {

					String anioDecretoFecha = String.valueOf(UtilidadesWeb.getAnioDeUnDate(vppad.getVigencia()));

					this.aniosCalculoAvaluoPorRango.add(new SelectItem(anioDecretoFecha, anioDecretoFecha));
				}
			}
			this.mostrarCalculoAvaluoPorAnio = true;
		}
	}

	/**
	 * Método para cargar datos cuando se desea calcular el avalúo manual por rango
	 * de años
	 *
	 * @author javier.aponte
	 */
	public void cargarDatosCalcularAvaluoPorRangoDeAnios() {

		if (this.calculoAvaluoPorRangoDeAnios) {
			this.aniosCalculoAvaluoPorRango.clear();

			this.prediosAvaluoDecretoCalculoManual = new ArrayList<VPPredioAvaluoDecreto>();

			for (VPPredioAvaluoDecreto vppad : this.pPrediosAvaluosCatastrales) {

				if (vppad.getValorTotalAvaluoCatastral().doubleValue() == 0) {
					String anioCalculoPorRango = String.valueOf(UtilidadesWeb.getAnioDeUnDate(vppad.getVigencia()));
					this.aniosCalculoAvaluoPorRango.add(new SelectItem(anioCalculoPorRango, anioCalculoPorRango));
					this.prediosAvaluoDecretoCalculoManual.add((VPPredioAvaluoDecreto) vppad.clone());
				}

			}

			this.activarCalcularAvaluoPorRangoDeAnios = true;
		} else {
			this.activarCalcularAvaluoPorRangoDeAnios = false;
		}

	}

	/**
	 * Método que se ejecuta cuando se desea calcular el avalúo manual por rango de
	 * años
	 *
	 * @author javier.aponte
	 */
	public void calcularAvaluoPorRangoDeAnios() {

		int anioDesdeCalculoAvaluoPorRangoNumero = Integer.parseInt(this.anioInicialCalculoAvaluoPorRango);
		int anioHastaCalculoAvaluoPorRangoNumero = Integer.parseInt(this.anioFinalCalculoAvaluoPorRango);

		boolean hayDatoValorAvaluoCatastral = false;
		double valorAvaluoAnioSiguiente = 0;
		double valorAvaluoAnioActual;
		BigDecimal valorRedondeado;

		if (anioDesdeCalculoAvaluoPorRangoNumero < anioHastaCalculoAvaluoPorRangoNumero) {
			this.addMensajeError(
					"El dato Hasta año debe ser menor o igual que el dato Desde año, porque el cálculo es en forma retroactiva");
			return;
		}

		int k = 0;
		while (UtilidadesWeb.getAnioDeUnDate(
				pPrediosAvaluosCatastrales.get(k).getVigencia()) > anioDesdeCalculoAvaluoPorRangoNumero) {
			k++;
		}
		if (k > 0) {
			k--;
		}

		if (this.pPrediosAvaluosCatastrales.get(k).getValorTotalAvaluoCatastral().doubleValue() != 0) {
			hayDatoValorAvaluoCatastral = true;
			valorAvaluoAnioSiguiente = this.pPrediosAvaluosCatastrales.get(k).getValorTotalAvaluoCatastral()
					.doubleValue();
		}

		if (!hayDatoValorAvaluoCatastral) {
			this.addMensajeError("No puede realizarse el cálculo retroactivo");
			return;
		}

		double porcentaje;

		for (VPPredioAvaluoDecreto vppad : this.prediosAvaluoDecretoCalculoManual) {
			porcentaje = (this.getGeneralesService().obtenerPorcentajeIncremento(vppad.getVigencia(),
					this.predioSeleccionado.getId())) / 100;
			if (UtilidadesWeb.getAnioDeUnDate(vppad.getVigencia()) >= anioHastaCalculoAvaluoPorRangoNumero) {
				valorAvaluoAnioActual = valorAvaluoAnioSiguiente / (1 + porcentaje);

				valorRedondeado = (new BigDecimal(valorAvaluoAnioActual)).setScale(0, RoundingMode.HALF_UP);

				double result = valorRedondeado.doubleValue() / 1000;

				valorRedondeado = (new BigDecimal(result)).setScale(0, RoundingMode.HALF_UP);

				vppad.setValorTotalAvaluoCatastral(valorRedondeado.doubleValue() * 1000);

				valorAvaluoAnioSiguiente = valorAvaluoAnioActual;
			} else {
				break;
			}

		}

		this.mostrarTablaInscripcionesYDecretos = true;
		this.activarCalcularAvaluoPorRangoDeAnios = false;

	}

	/**
	 * Método que se ejecuta cuando se desea calcular el avalúo manual por año
	 *
	 * @author javier.aponte
	 */
	public void calcularAvaluoPorAnio() {

		Integer anioCalculoAvaluoNumero = Integer.parseInt(this.anioCalculoAvaluo);

		if (this.valorAvaluoCalculoAvaluo == 0) {
			this.addMensajeError("El valor del avalúo no puede ser cero");
			return;
		}

		for (VPPredioAvaluoDecreto vppad : this.pPrediosAvaluosCatastrales) {
			if (vppad.getValorTotalAvaluoCatastral().doubleValue() == 0
					&& UtilidadesWeb.getAnioDeUnDate(vppad.getVigencia()) > anioCalculoAvaluoNumero) {

				this.addMensajeError("Debe ingresar los valores en orden cronológico descendente");
				return;
			}
		}

		this.prediosAvaluoDecretoCalculoManual = new ArrayList<VPPredioAvaluoDecreto>();

		// redondeo a miles
		this.valorAvaluoCalculoAvaluo = this.valorAvaluoCalculoAvaluo / 1000;
		BigDecimal valorRedondeado = (new BigDecimal(this.valorAvaluoCalculoAvaluo)).setScale(0, RoundingMode.HALF_UP);
		this.pPredioAvaluoCatastralSeleccionado.setValorTotalAvaluoCatastral(valorRedondeado.doubleValue() * 1000);
		this.prediosAvaluoDecretoCalculoManual
				.add((VPPredioAvaluoDecreto) this.pPredioAvaluoCatastralSeleccionado.clone());

		this.mostrarTablaInscripcionesYDecretos = true;
	}

	/**
	 * Método que se ejecuta cuando se desea guardar el avalúo manual
	 *
	 * @author javier.aponte
	 */
	public void guardarCalculoAvaluoManual() {

		// Integrado versión 1.1.6
		// felipe.cadena:: redmine #7538:: Se actualiza el predio para detectar
		// los cambios en avaluos
		try {
			this.predioSeleccionado = this.getConservacionService()
					.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
		} catch (Exception e) {
			this.addMensajeError("Error al consultar el predio");
		}

		if (!this.tramite.isQuintaNuevo()) {
			for (VPPredioAvaluoDecreto vppad : this.prediosAvaluoDecretoCalculoManual) {

				for (PPredioAvaluoCatastral ppac : this.predioSeleccionado.getPPredioAvaluoCatastrals()) {

					if (vppad.getId().equals(ppac.getId())) {
						ppac.setValorTotalAvaluoCatastral(vppad.getValorTotalAvaluoCatastral());
						ppac.setValorTotalAvaluoVigencia(vppad.getValorTotalAvaluoCatastral());
						ppac.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
						this.getConservacionService().actualizarPPredioAvaluoCatastral(ppac);
						break;
					}
				}
			}
		} else {
			// #11261 :: david.cifuentes :: Se ajusta el registro de avaluos
			// manuales para mutaciones de quinta nuevo :: 17/03/2015
			for (VPPredioAvaluoDecreto vppad : this.prediosAvaluoDecretoCalculoManual) {
				boolean editBool = false;
				for (PPredioAvaluoCatastral ppac : this.predioSeleccionado.getPPredioAvaluoCatastrals()) {
					if (!this.tramite.isQuintaMasivo()) {
						if (vppad.getId() != null && vppad.getId().equals(ppac.getId())) {
							ppac.setValorTotalAvaluoCatastral(vppad.getValorTotalAvaluoCatastral());
							ppac.setValorTotalAvaluoVigencia(vppad.getValorTotalAvaluoCatastral());
							ppac.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
							this.getConservacionService().actualizarPPredioAvaluoCatastral(ppac);
							editBool = true;
							break;
						}
					} else {
						if (vppad.getPPredio().getId() != null
								&& vppad.getPPredio().getId().equals(ppac.getPPredio().getId())
								&& vppad.getVigencia() != null && ppac.getVigencia() != null) {

							SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
							String fecha1 = formateador.format(ppac.getVigencia());
							String fecha2 = formateador.format(vppad.getVigencia());

							if (fecha1.equals(fecha2)) {
								PPredioAvaluoCatastral ppavTemp = this.getConservacionService()
										.obtenerPPredioAvaluosCatastral(ppac.getId());
								ppavTemp.setValorTotalAvaluoCatastral(vppad.getValorTotalAvaluoCatastral());
								ppavTemp.setValorTotalAvaluoVigencia(vppad.getValorTotalAvaluoCatastral());
								ppavTemp.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
								this.getConservacionService().actualizarPPredioAvaluoCatastral(ppavTemp);
								editBool = true;
								break;
							}
						}
					}
				}
				if (!editBool) {
					PPredioAvaluoCatastral ppacNuevo = new PPredioAvaluoCatastral();
					ppacNuevo.setValorTotalAvaluoCatastral(vppad.getValorTotalAvaluoCatastral());
					ppacNuevo.setValorTotalAvaluoVigencia(vppad.getValorTotalAvaluoCatastral());
					ppacNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
					ppacNuevo.setAutoavaluo(ESiNo.NO.getCodigo());
					ppacNuevo.setValorConstruccionComun(0D);
					ppacNuevo.setValorTerreno(0D);
					ppacNuevo.setValorTerrenoComun(0D);
					ppacNuevo.setValorTerrenoPrivado(0D);
					ppacNuevo.setValorTotalConsConvencional(0D);
					ppacNuevo.setValorTotalConsNconvencional(0D);
					ppacNuevo.setValorTotalConstruccion(0D);
					ppacNuevo.setValorTotalConstruccionPrivada(0D);
					ppacNuevo.setVigencia(vppad.getVigencia());
					ppacNuevo.setPPredio(vppad.getPPredio());
					ppacNuevo.setUsuarioLog(this.usuario.getLogin());
					ppacNuevo.setFechaLog(new Date());
					this.getConservacionService().actualizarPPredioAvaluoCatastral(ppacNuevo);
				}
			}
		}

		this.pPrediosAvaluosCatastrales = this.getFormacionService()
				.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());
	}

	/**
	 * Método que se ejecuta cuando se desea cancelar el avalúo manual
	 *
	 * @author javier.aponte
	 */
	public void cancelarCalculoAvaluoManual() {

		if (ESiNo.NO.getCodigo().equals(this.calculoAvaluoEnFormaRetroactiva)) {
			this.pPredioAvaluoCatastralSeleccionado.setValorTotalAvaluoCatastral(Double.valueOf(0));
			this.prediosAvaluoDecretoCalculoManual.clear();
		}

	}

	/**
	 * Método para reiniciar las variables al dar click en el bóton cálculo manual
	 *
	 * @author javier.aponte
	 */
	public void reiniciarValoresCalculoManual() {

		this.mostrarTablaInscripcionesYDecretos = false;
		this.valorAvaluoCalculoAvaluo = 0;
		this.anioCalculoAvaluo = null;
		this.calculoAvaluoEnFormaRetroactiva = null;
		this.mostrarCalculoAvaluoPorAnio = false;
		this.mostrarCalculoAvaluoPorRangoDeAnios = false;
		this.anioInicialCalculoAvaluoPorRango = null;
		this.anioFinalCalculoAvaluoPorRango = null;
		this.calculoAvaluoPorRangoDeAnios = false;
	}

	/**
	 * Método usado para cargar los anios entre la fecha de actualización y la fecha
	 * de inscripción catastral para el registro de valores de avalúos manualmente
	 * cuando son trámites de quinta nuevo.
	 *
	 * @note #11261 :: Permitir registrar avaluo para tramites de quinta ::
	 *       17/03/2015
	 *
	 * @author david.cifuentes
	 * @return
	 */
	public boolean validarYCargarAniosParaCalculoManualQuintaNuevo() {

		Date fechaUltimaActualizacion;
		String zona;
		zona = this.tramite.getNumeroPredial().substring(0, 7);
		fechaUltimaActualizacion = this.getConservacionService().obtenerVigenciaUltimaActualizacionPorZona(zona);

		if (this.tramite.isQuintaNuevo() && this.predioSeleccionado != null
				&& this.predioSeleccionado.getFechaInscripcionCatastral() != null && fechaUltimaActualizacion != null
				&& fechaUltimaActualizacion.after(this.predioSeleccionado.getFechaInscripcionCatastral())) {

			this.aniosCalculoAvaluoPorRango = new ArrayList<SelectItem>();

			Calendar calUltimaActualizacion = Calendar.getInstance();
			calUltimaActualizacion.setTime(fechaUltimaActualizacion);
			int yearUltimaActualizacion = calUltimaActualizacion.get(Calendar.YEAR);

			Calendar calInscripcionCatastral = Calendar.getInstance();
			calInscripcionCatastral.setTime(this.predioSeleccionado.getFechaInscripcionCatastral());
			int yearInscripcionCatastral = calInscripcionCatastral.get(Calendar.YEAR);

			while (true) {
				if (yearInscripcionCatastral >= yearUltimaActualizacion) {
					break;
				} else {
					yearInscripcionCatastral++;
					this.aniosCalculoAvaluoPorRango
							.add(new SelectItem("" + yearInscripcionCatastral, "" + yearInscripcionCatastral));
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Método usado para crear los registros de avaluo para los anios entre la fecha
	 * de actualización y la fecha de inscripción catastral para el registro de
	 * valores de avalúos manualmente cuando son trámites de quinta nuevo.
	 *
	 * @note #11261 :: 17/03/2015
	 *
	 * @author david.cifuentes
	 * @return
	 */
	public void crearAvaluosParaCalculoManualQuintaNuevo() {
		try {
			if (this.tramite.isQuintaNuevo() && this.aniosCalculoAvaluoPorRango != null
					&& !this.aniosCalculoAvaluoPorRango.isEmpty()) {

				this.prediosAvaluoDecretoCalculoManual = new ArrayList<VPPredioAvaluoDecreto>();

				String zona = this.tramite.getNumeroPredial().substring(0, 7);
				Date fechaUltimaActualizacion = this.getConservacionService()
						.obtenerVigenciaUltimaActualizacionPorZona(zona);

				Calendar calUltimaActualizacion = Calendar.getInstance();
				calUltimaActualizacion.setTime(fechaUltimaActualizacion);
				int yearUltimaActualizacion = calUltimaActualizacion.get(Calendar.YEAR);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
				List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastralesTemp = this.getFormacionService()
						.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());

				List<String> decretos = new ArrayList<String>();
				for (VPPredioAvaluoDecreto vppad : pPrediosAvaluosCatastralesTemp) {
					Calendar calInscripcionCatastral = Calendar.getInstance();
					calInscripcionCatastral.setTime(vppad.getVigencia());
					int year = calInscripcionCatastral.get(Calendar.YEAR);
					decretos.add("" + year);
				}

				for (SelectItem si : this.aniosCalculoAvaluoPorRango) {

					this.anioCalculoAvaluo = (String) si.getLabel();

					if (!decretos.contains(this.anioCalculoAvaluo)) {

						int anioDecretoIterator = new Integer(this.anioCalculoAvaluo).intValue();
						VPPredioAvaluoDecreto pPredioAvaluoDecreto = new VPPredioAvaluoDecreto();
						pPredioAvaluoDecreto.setValorTotalAvaluoCatastral(new Double(0));

						String dateInString = "01/01/" + (anioDecretoIterator);
						Date fechaInscripcionCatastralDecreto = sdf.parse(dateInString);

						// Busqueda del Decreto para la vigencia seleccionada
						DecretoAvaluoAnual decreto = this.getFormacionService()
								.buscarDecretoPorVigencia(this.anioCalculoAvaluo);
						if (decreto != null) {
							pPredioAvaluoDecreto.setDecretoNumero(decreto.getDecretoNumero());
							pPredioAvaluoDecreto.setDecretoFecha(decreto.getDecretoFecha());
							pPredioAvaluoDecreto.setPPredio(this.predioSeleccionado);
							pPredioAvaluoDecreto.setVigencia(decreto.getVigencia());
							pPredioAvaluoDecreto.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
							pPredioAvaluoDecreto.setFechaInscripcionCatastral(fechaInscripcionCatastralDecreto);
							if (yearUltimaActualizacion == anioDecretoIterator) {
								pPredioAvaluoDecreto.setActualizacion(ESiNo.SI.getCodigo());
							} else {
								pPredioAvaluoDecreto.setActualizacion(ESiNo.NO.getCodigo());
							}
							this.prediosAvaluoDecretoCalculoManual.add(pPredioAvaluoDecreto);
						} else {
							this.addMensajeError("No fue posible asociar el valor del avaluo para la vigencia "
									+ this.anioCalculoAvaluo);
							return;
						}
					}
				}
				this.guardarCalculoAvaluoManual();
			}
		} catch (Exception e) {
			this.addMensajeError(
					"No fue posible procesar la totalidad de los avaluós para los años entre la fecha de actualización y la fecha de inscripción catastral");
			LOGGER.debug("ERROR: " + e.getMessage());
			return;
		}
	}

	/** -------------------------------- */
	/** ----------- EVENTOS ------------ */
	/** Caso de uso liquidar avalúos de trámites */
	/**
	 * Método que gestiona la activación de los botones de acción sobre las
	 * proyecciones inscripciones y decretos.
	 *
	 * @author javier.aponte
	 * @param SelectEvent ev
	 */
	public void gestionActivacionBotonesSelect(SelectEvent ev) {

		VPPredioAvaluoDecreto avaluoSel = (VPPredioAvaluoDecreto) ev.getObject();
		if ((this.pPredioAvaluoCatastralSeleccionado.getValorTotalAvaluoCatastral().doubleValue() == 0)
				|| (avaluoSel.getCancelaInscribe() != null
						&& avaluoSel.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo()))) {
			this.activarCalculoAvaluoManual = true;
			this.activarEliminarCalculoAvaluoManual = true;
		} else {
			this.activarCalculoAvaluoManual = false;
			this.activarEliminarCalculoAvaluoManual = false;
		}

	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que gestiona la activación de botones al deseleccionar las
	 * proyecciones inscripciones y decretos.
	 *
	 * @author javier.aponte
	 * @param UnselectEvent ev
	 */
	public void gestionActivacionBotonesUnselect(UnselectEvent ev) {
		this.activarCalculoAvaluoManual = false;
		this.activarEliminarCalculoAvaluoManual = false;
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que valida el envio de tramites a depuración.
	 *
	 * @author leidy.gonzalez
	 */
	public void envioDepuracion() {

		// TODO:: felipe.cadena:: buscar la manera de evitar este metodo vacio
		// desde la vista
		// GeneralMB generalMB = (GeneralMB)
		// UtilidadesWeb.getManagedBean("general");
		// Se retira validacion redundante
		// UsuarioDTO usuarioDestino =
		// generalMB.validarEnvioDepuracion(this.tramite, this.usuario,
		// this.actividad);
		// if (usuarioDestino == null) {
		// return;
		// }
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método para mover el tramite al proceso de depuración
	 *
	 * @author felipe.cadena
	 *
	 */
	/*
	 * @modified by leidy.gonzalez 21/10/2014 Se elimina inicializacion de tareas en
	 * el metodo enviarADepuracion debido a que en el metodo cerrar es llamado.
	 */
	public String enviarADepuracion() {

		GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

		UsuarioDTO usuarioDestino = generalMB.validarEnvioDepuracion(this.tramite, this.usuario, this.actividad);
		if (usuarioDestino == null) {
			return null;
		}

		generalMB.crearRegistroTramiteDepuracion(this.tramite, this.usuario);

		try {
			// this.getConservacionService().generarReplicaDeConsultaTramiteYAvanzarProceso(
			// this.actividad,
			// this.usuario,
			// usuarioDestino,
			// true);

			// felipe.cadena::14494::23-09-2015::se eliminan las replicas de
			// edicion si estan existen.
			generalMB.eliminarReplicasDeEdicion(this.tramite);

			// felipe.cadena::ACCU::Se cambia la forma de generar la replica de
			// consulta
			RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb
					.getManagedBean("recuperarTramites");
			recuperarTamitesMB.generarReplicaConsulta(this.tramite, this.usuario, usuarioDestino, 0);
		} catch (ExcepcionSNC e) {
			LOGGER.error(e.getMensaje(), e);
			this.addMensajeError("Error al avanzar el proceso.");
			return "";
		}

		return cerrar();

	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método para mover el tramite a edicion geografica con un digitalizador
	 * asociado
	 *
	 * @author felipe.cadena
	 */
	/*
	 * @modified by leidy.gonzalez 21/10/2014 Se elimina inicializacion de tareas en
	 * el metodo enviarADepuracion debido a que en el metodo cerrar es llamado.
	 */
	/*
	 * @modified by leidy.gonzalez::#17785::08/06/2016:: Se ajusta validacion para
	 * insertar los datos en la tabla: TRAMITE_DIGITALIZACION
	 */
	public String asignarDigitalizador() {

		if (this.banderaDigitalizacionAsignada) {

			this.digitalizador = this.getGeneralesService().getCacheUsuario(this.digitalizadorAsignado);

			if (this.digitalizadorAsignado != null && this.digitalizadorSeleccionado[0] != null
					&& !this.digitalizadorAsignado.equals(this.digitalizadorSeleccionado[0])) {

				this.digitalizador = (UsuarioDTO) this.digitalizadorSeleccionado[0];
			}
		} else {
			this.digitalizador = (UsuarioDTO) this.digitalizadorSeleccionado[0];
		}

		if (this.tramite.isTercera() && !this.tramite.isTerceraMasiva()) {
			this.validacionesMutacionTercera();

			// felipe.cadena:: #9624 :: 30/09/2014 ::se ingresa el año de
			// cancelacion para las construcciones canceladas
			if (!this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
				for (PUnidadConstruccion pu : this.predioSeleccionado.getPUnidadConstruccions()) {
					if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pu.getCancelaInscribe())) {

						Calendar fechaActual = Calendar.getInstance();
						int year = fechaActual.get(Calendar.YEAR);
						pu.setAnioCancelacion(year);

					}
				}
			}
			this.predioSeleccionado = this.getConservacionService().guardarActualizarPPredio(this.predioSeleccionado);

			// se calcula el avaluo del predio
			Object[] errores = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(this.tramite.getId(),
					this.predioSeleccionado.getId(), new Date(), this.usuario);
			if (Utilidades.hayErrorEnEjecucionSP(errores)) {
				LOGGER.error("Error al calcular el avaluo para el tramite de tercera :" + tramite.getId());
			}
		}

		// Se requiere que si las construcciones tengan el estado en CANCELA,
		// lleven fechas de inscripción catastral, así como que el predio para
		// las mutaciones de tercera tenga en su estado cancelaInscribe el valor
		// MODIFICA
		if (this.tramite.isTerceraMasiva()) {

			// Si las PUnidadConstruccion están como canceladas, en el campo
			// "FECHA_INSCRIPCION_CATASTRAL" se les debe asignar la fecha del
			// sistema.
			if (this.pPrediosTerceraRectificacionMasiva != null && !this.pPrediosTerceraRectificacionMasiva.isEmpty()) {
				Date fechaActual = new Date(System.currentTimeMillis());

				for (PPredio predioTemp : this.pPrediosTerceraRectificacionMasiva) {
					if (!predioTemp.getPUnidadConstruccions().isEmpty()) {
						for (PUnidadConstruccion pu : predioTemp.getPUnidadConstruccions()) {
							if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pu.getCancelaInscribe())) {
								pu.setFechaInscripcionCatastral(fechaActual);

								// felipe.cadena:: 30/09/2014 ::se debe asignar
								// el año de cancelacion.
								int year = Calendar.getInstance().get(Calendar.YEAR);
								pu.setAnioCancelacion(year);

							}
						}
						predioTemp = this.getConservacionService().guardarActualizarPPredio(predioTemp);
					}
					// se calcula el avaluo del predio
					Object[] errores = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(
							this.tramite.getId(), predioTemp.getId(), fechaActual, this.usuario);
					if (Utilidades.hayErrorEnEjecucionSP(errores)) {
						LOGGER.error("Error al calcular el avaluo para el predio :" + predioTemp.getNumeroPredial());
					}
				}
			}
		}

		this.getConservacionService().enviarAModificacionInformacionGeograficaSincronica(this.actividad, digitalizador);

		// Se verifica que el dgitalizador seleccionado no sea el mismo que
		// asigno anteriormente
		if ((this.digitalizadorAsignado != null && this.digitalizadorSeleccionado[0] != null
				&& !this.digitalizadorAsignado.equals(this.digitalizadorSeleccionado[0]))
				|| this.digitalizadorAsignado == null) {

			this.digitalizadorAsignado = this.digitalizador.getLogin();

			TramiteDigitalizacion td = new TramiteDigitalizacion();

			td.setTramite(this.tramite);
			td.setUsuarioDigitaliza(this.digitalizador.getLogin());
			td.setUsuarioEnvia(this.usuario.getLogin());
			td.setUsuarioLog(this.usuario.getLogin());
			td.setFechaLog(new Date());
			td.setFechaEnvia(new Date());
			this.getTramiteService().guardarActualizarTramiteDigitalizacion(td);

		}

		UtilidadesWeb.removerManagedBean("proyectarConservacion");
		return cerrar();

	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método para cargar los digitalizadores disponibles en la territorial
	 *
	 * @author felipe.cadena
	 * @modified by leidy.gonzalez Se agrego validacion para verificar que el
	 *           tramite se enviara a modificar informacion geografica cuando en la
	 *           tabla PPredio exista la fecha de inscripcion para tramites de
	 *           Quinta nuevo, nuevo omitido y Segunda Englobe
	 */
	public void inicializarDigitalizadores() {
		try {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
			// validacion de manzanas cuando se crean manzanas
			if (this.entradaProyeccionMB.isCreaManzanas()) {
				if (!this.entradaProyeccionMB.validarPrediosManzanas()) {

					RequestContext context = RequestContext.getCurrentInstance();
					context.addCallbackParam("error", "error");
					return;
				}
			}

			if (this.tramite.isSegunda() || this.tramite.isQuintaNuevo() || this.tramite.isQuintaOmitidoNuevo()
					|| this.getTramite().isCancelacionPredio()) {
				if (entradaProyeccionMB.getPrediosResultantes() != null) {
					for (PPredio pp : entradaProyeccionMB.getPrediosResultantes()) {
						if (pp.getFechaInscripcionCatastral() == null) {
							this.addMensajeError(
									"No se puede enviar a modificar información geografica debido a que hace falta el ingreso de la fecha de inscripción catastral, agregar campos obligatorios en Justificación de Propiedad. Y despues Valide y Guarde");
							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							return;
						}
					}
				}
			}

			GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
			if (!generalMB.validarEnvioModificacionGeografica(this.tramite)) {
				return;
			}

			List<UsuarioDTO> dgs = new ArrayList<UsuarioDTO>();
			if (this.usuario.getDescripcionUOC() != null) {
				dgs = this.getTramiteService()
						.buscarFuncionariosDigitalizadoresPorTerritorialDTO(this.usuario.getDescripcionUOC());
			}

			// si no existe digitalizador en la UOC se buscan en la territorial.
			if (dgs == null || dgs.isEmpty()) {
				dgs = this.getTramiteService()
						.buscarFuncionariosDigitalizadoresPorTerritorialDTO(this.usuario.getDescripcionTerritorial());
			}

			this.digitalizadores = new ArrayList<Object[]>();

			if (dgs != null && !dgs.isEmpty()) {
				for (UsuarioDTO usuarioDTO : dgs) {

					if (!usuarioDTO.getFechaExpiracion().before(new Date())
							&& ELDAPEstadoUsuario.ACTIVO.codeExist(usuarioDTO.getEstado())) {
						Object[] obj = new Object[3];
						obj[0] = usuarioDTO;
						obj[1] = this.getTramiteService()
								.contarTramiteDigitalizacionPorDigitalizador(usuarioDTO.getLogin());
						obj[2] = 3;

						this.digitalizadores.add(obj);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			this.addMensajeError("Ocurrio un error consultando los digitalizadores, por favor intente nuevamente");
		}
	}

	public List<SelectItem> getListaModelosUnidadDeConstruccion() {
		List<SelectItem> modelos = new ArrayList<SelectItem>();

		if (this.getFichaMatrizSeleccionada() == null) {
			this.consultarFichaMatriz();
		}

		if (this.getFichaMatrizSeleccionada() != null
				&& this.getFichaMatrizSeleccionada().getPFichaMatrizModelos() != null) {
			for (PFichaMatrizModelo fmm : this.getFichaMatrizSeleccionada().getPFichaMatrizModelos()) {
				modelos.add(new SelectItem(fmm.getId(), fmm.getNombre()));
			}
		}
		return modelos;
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que realiza la validación para los predios generados mediante la ficha
	 * matriz de si existen o no números de matrículas duplicadas.
	 *
	 * @author david.cifuentes
	 */
	public void validarMatriculasDuplicadas() {
		if (this.entradaProyeccionMB == null) {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
		}

		if (this.entradaProyeccionMB.getPrediosResultantes() != null
				&& !this.entradaProyeccionMB.getPrediosResultantes().isEmpty()) {
			List<String> auxDuplicates = new ArrayList<String>();
			List<PPredio> auxPPredioDuplicatesMatricula = new ArrayList<PPredio>();
			for (PPredio pp : this.entradaProyeccionMB.getPrediosResultantes()) {
				if (pp.getNumeroRegistro() != null) {
					if (!auxDuplicates.contains(pp.getNumeroRegistro())) {
						auxDuplicates.add(pp.getNumeroRegistro());
					} else {
						auxPPredioDuplicatesMatricula.add(pp);
					}
				} else {

				}
			}

			List<ValidacionProyeccionDelTramiteDTO> listValidacionesMatricula = new ArrayList<ValidacionProyeccionDelTramiteDTO>();
			if (!auxPPredioDuplicatesMatricula.isEmpty()) {
				ValidacionProyeccionDelTramiteDTO validacionAdicional;
				for (PPredio pp : auxPPredioDuplicatesMatricula) {
					validacionAdicional = new ValidacionProyeccionDelTramiteDTO();
					validacionAdicional.setPredioId(pp.getId());
					validacionAdicional.setTramiteId(this.tramite.getId());
					validacionAdicional.setSeccion(ESeccionEjecucion.UBICACION.getNombre());
					validacionAdicional.setMensajeError(
							"Matrícula inmobiliaria duplicada para el predio " + pp.getNumeroPredial() + ".");
					listValidacionesMatricula.add(validacionAdicional);
				}
			}

			if (!listValidacionesMatricula.isEmpty()) {
				if (this.listaValidacionesTramiteSegundaDesenglobe == null
						|| this.listaValidacionesTramiteSegundaDesenglobe.isEmpty()) {
					this.listaValidacionesTramiteSegundaDesenglobe = new ArrayList<ValidacionProyeccionDelTramiteDTO>();
				}
				this.listaValidacionesTramiteSegundaDesenglobe.addAll(listValidacionesMatricula);
			}
		}
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que realiza unas validaciones y actualizaciones al predio seleccionado
	 * para los trámites de mutación de tercera.
	 *
	 * @author david.cifuentes
	 */
	public void validacionesMutacionTercera() {
		if (this.tramite != null && this.predioSeleccionado != null) {
			// Si las PUnidadConstruccion están como canceladas, en el campo
			// "FECHA_INSCRIPCION_CATASTRAL" se les debe asignar la fecha del
			// sistema.
			if (!this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
				for (PUnidadConstruccion pu : this.predioSeleccionado.getPUnidadConstruccions()) {
					if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pu.getCancelaInscribe())) {
						pu.setFechaInscripcionCatastral(new Date(System.currentTimeMillis()));

						// felipe.cadena:: 30/09/2014 ::se debe asignar el año
						// de cancelacion.
						Calendar fechaActual = Calendar.getInstance();
						int year = fechaActual.get(Calendar.YEAR);
						pu.setAnioCancelacion(year);

					}
				}
			}

			// Para las mutaciones de tercera el
			// predio del trámite debe quedar como modificado
			// puesto que se debió realizar una acción de modificación,
			// eliminación o adición sobre las unidades de construcción del
			// mismo.
			if (!EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(this.predioSeleccionado.getCancelaInscribe())) {
				this.predioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				this.predioSeleccionado = this.getConservacionService()
						.guardarActualizarPPredio(this.predioSeleccionado);
			}

			// Validaciones para predios fiscales
			if (this.banderaPredioFiscal) {

				if (this.tramite.getPredio().getUnidadConstruccions() != null) {
					if (this.avaluo.getValorTerreno() >= this.tramite.getPredio().getAvaluoCatastral()) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("El valor del terreno presenta un "
								+ "valor alto respecto al avalúo inicial " + "del predio, por favor verifique");
						this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
					}
				}

				// Validaciones Mejoras fiscales
				if (this.tramite.getPredio().isMejora()) {

					if (this.avaluo.getValorTerreno() != 0) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("El valor del avalúo del terreno deber ser igual a cero.");
						this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
					}

					if (this.predioSeleccionado.getAreaTerreno() != 0) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.addCallbackParam("error", "error");
						this.addMensajeError("Él área total del terreno debe ser igual a cero.");
						this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
					}

					if (this.predioSeleccionado.getPUnidadConstruccions() != null) {
						Double areaTotalMejora = 0.0;

						for (PUnidadConstruccion unidadMejora : this.predioSeleccionado.getPUnidadConstruccions()) {
							areaTotalMejora += unidadMejora.getAreaConstruida();
						}

						if (areaTotalMejora.equals(0.0)) {
							RequestContext context = RequestContext.getCurrentInstance();
							context.addCallbackParam("error", "error");
							this.addMensajeError("Una mutación de tercera de una mejora no "
									+ "puede quedar sin área de construcción.");
							this.validezSeccion.put(ESeccionEjecucion.AVALUOS, false);
						}

					}

				}
			}
		}
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que realiza unas validaciones y actualizaciones al predio seleccionado
	 * para los trámites de mutación de tercera masiva.
	 *
	 * @author javier.aponte
	 */
	public void validacionesMutacionTerceraMasiva() {
		if (this.tramite != null && this.predioSeleccionado != null) {

			// Si las PUnidadConstruccion están como canceladas, en el campo
			// "FECHA_INSCRIPCION_CATASTRAL" se les debe asignar la fecha del
			// sistema.
			if (this.predioSeleccionado.getPUnidadConstruccions() != null
					&& !this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {

				for (PUnidadConstruccion pu : this.predioSeleccionado.getPUnidadConstruccions()) {
					if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pu.getCancelaInscribe())) {
						pu.setFechaInscripcionCatastral(new Date(System.currentTimeMillis()));

						// felipe.cadena:: 30/09/2014 ::se debe asignar el año
						// de cancelacion.
						Calendar fechaActual = Calendar.getInstance();
						int year = fechaActual.get(Calendar.YEAR);
						pu.setAnioCancelacion(year);

					}
					this.predioSeleccionado = this.getConservacionService()
							.guardarActualizarPPredio(this.predioSeleccionado);
				}

			}

			// Para las mutaciones de tercera el
			// predio del trámite debe quedar como modificado
			// puesto que se debió realizar una acción de modificación,
			// eliminación o adición sobre las unidades de construcción del
			// mismo.
			if (!EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(this.predioSeleccionado.getCancelaInscribe())) {

				this.predioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				this.predioSeleccionado = this.getConservacionService()
						.guardarActualizarPPredio(this.predioSeleccionado);

			}
		}
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que realiza las validaciones a los trámites de vía gubernativa
	 *
	 * @author juanfelipe.garcia
	 */
	public String proyeccionViaGubernativa() {

		Tramite tramiteRef = this.getTramiteService()
				.buscarTramitePorNumeroRadicacion(this.tramite.getSolicitud().getTramiteNumeroRadicacionRef());
		if (modResultResolucionInicial) {
			if (tramiteRef != null) {
				String tipo = "R";
				Object[] resp = this.getTramiteService().recurrirProyeccion(this.tramite.getId(), tramiteRef.getId(),
						tipo, this.usuario.getLogin());

				if (resp != null) {

					if (resp.length > 0) {
						ArrayList msg = (ArrayList) resp[0];
						if (msg.size() > 0) {
							this.addMensajeError("Error en la proyección");
							return "";
						}
						for (Object string : msg) {
							LOGGER.debug(string.toString());
						}

					}
				}

				try {
					String tipoTramiteTemp = this.tramite.getTipoTramiteCadenaCompleto();
					this.tramite.setTipoTramite(tramiteRef.getTipoTramite());
					this.tramite.setClaseMutacion(tramiteRef.getClaseMutacion());
					this.tramite.setSubtipo(tramiteRef.getSubtipo());
					this.tramite.setNombreFuncionarioEjecutor(this.usuario.getLogin());
					this.tramite.setObservacionesRadicacion(tipoTramiteTemp);

					if (this.tramite.isRectificacion()) {
						List<TramiteRectificacion> tramiteRectificaciones = this.getTramiteService()
								.obtenerTramiteRectificacionesDeTramite(tramiteRef.getId());
						if (tramiteRectificaciones != null && !tramiteRectificaciones.isEmpty()) {
							for (TramiteRectificacion tr : tramiteRectificaciones) {
								tr.setId(null);
								tr.setFechaLog(new Date());
								tr.setUsuarioLog(this.usuario.getLogin());
								tr.setTramite(this.tramite);
							}
							this.tramite.setTramiteRectificacions(tramiteRectificaciones);
						}
					}

					// tramite_texto_resolucion
					TramiteTextoResolucion listaTramTextRes = this.getTramiteService()
							.buscarTramiteTextoResolucionPorIdTramite(tramiteRef.getId());
					listaTramTextRes.setUsuarioLog(this.usuario.getLogin());
					listaTramTextRes.setId(null);
					listaTramTextRes.setFechaLog(new Date());
					listaTramTextRes.setTramite(this.tramite);
					listaTramTextRes = this.getTramiteService().guardarTramiteTextoResolucion(listaTramTextRes);

					if (listaTramTextRes != null) {
						// PPersonaPredioPropiedad documentoSoporte
					}

					// -->documentos
					boolean resultado = this.getTramiteService().actualizarTramite2(this.tramite, this.usuario);
				} catch (Exception ex) {

				}
			}

			return this.cerrarPagina();
		} else {

			this.habilitarBotonesViaGubernativa = true;
			return "";

		}
	}

	/**
	 * Método que busca el tramite asociado a uno de vía gubernativa, por el numero
	 * de radicación
	 *
	 * @author juanfelipe.garcia
	 * @param numeroRadicacionAsociado
	 * @return
	 */
	private Tramite buscarTramiteAsociadoViaGubernativa(String numeroRadicacionAsociado) {
		Tramite respuesta = null;
		Tramite resultado = this.getTramiteService().buscarTramitePorNumeroRadicacion(numeroRadicacionAsociado);
		if (resultado != null) {
			respuesta = resultado;
		}
		return respuesta;
	}

	public boolean isBanderaAceptaCancelacionDobleInscripcion() {
		return banderaAceptaCancelacionDobleInscripcion;
	}

	public void setBanderaAceptaCancelacionDobleInscripcion(boolean banderaAceptaCancelacionDobleInscripcion) {
		this.banderaAceptaCancelacionDobleInscripcion = banderaAceptaCancelacionDobleInscripcion;
	}

	public boolean isBanderaCancelacionDobleInscripcion() {
		return this.tramite != null && this.tramite.isRectificacionCancelacionDobleInscripcion();
	}

	public void setBanderaCancelacionDobleInscripcion(boolean banderaCancelacionDobleInscripcion) {
		this.banderaCancelacionDobleInscripcion = banderaCancelacionDobleInscripcion;
	}

	/**
	 * Metodo de cancelacion para tramites de cancelacion por doble inscripcion
	 *
	 * @author ??
	 */
	public void cancelarPredio() {
		try {
			if (this.isBanderaAceptaCancelacionDobleInscripcion()) {
				this.getPredioSeleccionado().setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				this.getPredioSeleccionado().setEstado(EPredioEstado.CANCELADO.getCodigo());
				this.inicializarEstadoGDB();
				this.banderaPredioCanceladoCDI = true;
				this.getTramiteService().actualizarTramiteDocumentos(this.tramite, this.usuario);
			} else {
				this.getPredioSeleccionado().setCancelaInscribe(null);
				this.getPredioSeleccionado().setEstado(EPredioEstado.ACTIVO.getCodigo());
			}
			this.getPredioSeleccionado().setUsuarioLog(this.usuario.getLogin());
			this.getPredioSeleccionado().setFechaLog(new Date());

			this.setPredioSeleccionado(
					this.getConservacionService().guardarActualizarPPredio(this.getPredioSeleccionado()));
			this.recalcularAvaluo(true);

			this.predioSeleccionado = this.getConservacionService()
					.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
			this.pPrediosAvaluosCatastrales = this.getFormacionService()
					.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());
		} catch (Exception ex) {
			this.addMensajeError("Error al cancelar al predio");
			LOGGER.error("Error en la canclacion del predio " + ex.getCause().toString());
		}

	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que valida si la lista de {@link PPredioZona} son las del año actual,
	 * en caso de que no sean las actuales, se retornaran las del año anterior o de
	 * la última vigencia válida.
	 *
	 * @author david.cifuentes
	 *
	 * @param pPrediosZonas
	 * @return
	 */
	public List<PPredioZona> verificarZonaVigente(List<PPredioZona> pPrediosZonas) {

		if (pPrediosZonas == null || pPrediosZonas.isEmpty()) {
			return new ArrayList<PPredioZona>();
		}
		List<PPredioZona> zonasVigentes = new ArrayList<PPredioZona>();

		Calendar calendar = null;
		Calendar calendarIterator = null;
		int anioUltimaVigencia = 0;
		int anioIterator = 0;

		for (PPredioZona ppz : pPrediosZonas) {
			int anioUltimaVigenciaTemp = 0;
			if (ppz.getVigencia() != null) {
				calendar = Calendar.getInstance();
				calendar.setTime(ppz.getVigencia());
				anioUltimaVigenciaTemp = calendar.get(Calendar.YEAR);
			}
			if (anioUltimaVigenciaTemp > anioUltimaVigencia) {
				anioUltimaVigencia = anioUltimaVigenciaTemp;
			}
		}

		for (PPredioZona ppz : pPrediosZonas) {
			if (ppz.getVigencia() != null) {
				calendarIterator = Calendar.getInstance();
				calendarIterator.setTime(ppz.getVigencia());
				anioIterator = calendarIterator.get(Calendar.YEAR);
				if (anioIterator == anioUltimaVigencia) {
					zonasVigentes.add(ppz);
				}
			}
		}

		return zonasVigentes;
	}

	/** --------------------------------------------------------------------- */
	/**
	 * Método que valida si los detalles de calificación construcción seleccionados
	 * requieren de una validación, de ser así se debe mostrar un mensaje para que
	 * sea verificado el valor de la selección.
	 *
	 * @author david.cifuentes
	 */
	public void validacionDetalleCalificacionConstruc() {

		this.validarCalificacionComponenteBool = false;
		String message = "Revisar detalle de calificación.";

		List<TreeNode> calificacionConstruccionNodesLevel1, calificacionConstruccionNodesLevel2;
		CalificacionConstruccionTreeNode cctnTempL2;
		if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.COMERCIAL.getCodigo())) {
			this.treeComponentesConstruccion = this.treeComponentesConstruccion2;
		} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.INDUSTRIAL.getCodigo())) {
			this.treeComponentesConstruccion = this.treeComponentesConstruccion3;
		} else if (this.unidadConstruccionSeleccionada.getTipoCalificacion()
				.equals(EUnidadConstruccionTipoCalificacion.RESIDENCIAL.getCodigo())) {
			this.treeComponentesConstruccion = this.treeComponentesConstruccion1;
		}

		calificacionConstruccionNodesLevel1 = this.treeComponentesConstruccion.getChildren();

		boolean dynamicRefresh;
		CalificacionConstruccion ccDetalle = null;

		for (TreeNode currentComponenteNode : calificacionConstruccionNodesLevel1) {
			calificacionConstruccionNodesLevel2 = currentComponenteNode.getChildren();
			dynamicRefresh = false;
			for (TreeNode currentElementoNode : calificacionConstruccionNodesLevel2) {
				cctnTempL2 = (CalificacionConstruccionTreeNode) currentElementoNode.getData();
				// Si existe una calificacionConstruccion seleccionada que
				// requiera validación, se muestra el mensaje de
				// verificación.
				if (cctnTempL2.getSelectedDetalle() != null
						&& ESiNo.SI.getCodigo().equals(cctnTempL2.getSelectedDetalle().getValidar())) {
					this.validarCalificacionComponenteBool = true;
					this.addMensajeWarn(message);
					return;
				}

				if (dynamicRefresh) {
					for (SelectItem si : cctnTempL2.getDetallesCalificacionItemList()) {
						si.setDisabled(true);
					}
				} else {
					for (SelectItem si : cctnTempL2.getDetallesCalificacionItemList()) {
						si.setDisabled(false);
					}
				}

				// Modificación dinámica del arbol para valores particulares
				// del detalle de calificación.
				if (!dynamicRefresh && cctnTempL2.getCalificacionConstruccion() != null
						&& cctnTempL2.getCalificacionConstruccion().getElemento() != null
						&& cctnTempL2.getCalificacionConstruccion().getElemento().equals("TAMAÑO")) {

					ccDetalle = cctnTempL2.getSelectedDetalle();
					if (ccDetalle.getDetalleCalificacion() != null
							&& (ccDetalle.getDetalleCalificacion().equals("SIN BAÑO")
									|| ccDetalle.getDetalleCalificacion().equals("SIN COCINA"))) {
						dynamicRefresh = true;
					}
				}
			}
		}
	}

	// --------------------------------------------------- //
	/**
	 * Acción del botón cerrar de la pantalla proyectarConservación, retorna al
	 * arbol de tareas.
	 *
	 * @return
	 */
	public String cerrarPagina() {
		UtilidadesWeb.removerManagedBean("proyectarConservacion");
		this.tareasPendientesMB.init();
		return "index";
	}

	/**
	 * Método para copiar del predio actual, los propietarios a todos los predios
	 * del desenglobe
	 */
	public void copiarPropietarios() {

		if (this.entradaProyeccionMB == null) {
			this.entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.getManagedBean("entradaProyeccion");
		}

		// felipe.cadena::27-05-2015::para tramites de desenglobes por etapas
		// solo
		// se copian los propietarios para los predios nuevos del tramite
		// #14060 :: CC-NP-CO-118 Ajustar propietarios y poseedores en Mutación
		// de Quinta masivo PH o Condominio, sólo se copian los propietarios a
		// predios nuevos :: david.cifuentes :: 25/08/2015
		// leidy.gonzalez::24896::22/06/2017 para tramites de desenglobes virtuales
		// solo se copian los propietarios para los predios nuevos del tramite
		if (this.tramite.isDesenglobeEtapas() || this.tramite.isQuintaMasivo() || this.tramite.isSegundaEnglobe()) {
			List<PPredio> prediosACopiar = new ArrayList<PPredio>();

			for (PPredio pp : this.entradaProyeccionMB.getPrediosResultantes()) {
				if (!pp.isPredioOriginalTramite()) {
					prediosACopiar.add(pp);
				}
			}

			this.entradaProyeccionMB.setPrediosResultantes(this.getConservacionService()
					.copiarPropietarios(this.predioSeleccionado, prediosACopiar, this.usuario.getLogin()));
		} else {
			this.entradaProyeccionMB
					.setPrediosResultantes(this.getConservacionService().copiarPropietarios(this.predioSeleccionado,
							this.entradaProyeccionMB.getPrediosResultantes(), this.usuario.getLogin()));
		}
		this.addMensajeInfo("Propietarios copiados de forma exitosa");
	}

	/**
	 * Método para calcular las zonas de un tramite de rectificacion de zonas.
	 *
	 * @author felipe.cadena
	 */
	public void calcularZonas() {

		if (this.tramite.isRectificacionMatriz()) {
			// felipe.cadena::#11906::Se valida que exista una fecha de
			// inscripcion catastral
			List<PPredio> prediosTramite = this.getConservacionService()
					.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());

			for (PPredio pPredio : prediosTramite) {
				if (pPredio.getFechaInscripcionCatastral() == null) {
					this.addMensajeError(ECodigoErrorVista.FECHA_INSCRIPCION_01.toString());
					LOGGER.debug(ECodigoErrorVista.FECHA_INSCRIPCION_01.getMensajeTecnico());
					return;
				}
			}

			this.eliminarZonasInscritas(this.tramite.getId());
			// felipe.cadena::22-07-2015::Se envia el calculo de zonas de manera
			// asincronica y no retorna respuesta
			this.getConservacionService().rectificarZonas(this.tramite.getId(), this.usuario, false);

		} else {
			if (this.tipoCambioZona != ECambioZonaHomogenea.AJUSTE_ESTUDIO.getCodigo()) {

				// felipe.cadena::#11906::Se valida que exista una fecha de
				// inscripcion catastral
				if (this.predioSeleccionado.getFechaInscripcionCatastral() == null) {
					this.addMensajeError(ECodigoErrorVista.FECHA_INSCRIPCION_01.toString());
					LOGGER.debug(ECodigoErrorVista.FECHA_INSCRIPCION_01.getMensajeTecnico());
					return;
				}

				this.eliminarZonasInscritas(this.tramite.getId());
				List<PPredioZona> nuevasZonas = this.getConservacionService().rectificarZonas(this.tramite.getId(),
						this.usuario, true);

				if (nuevasZonas == null) {
					this.addMensajeError("Error al generar las zonas ");
				} else {
					// Se actualiza el area del predio.
					Double nuevaArea = 0d;
					for (PPredioZona ppz : nuevasZonas) {
						nuevaArea += ppz.getArea();
					}

					this.predioSeleccionado.setPPredioZonas(nuevasZonas);
					this.predioSeleccionado.setAreaTerreno(Utilidades.redondearNumeroADosCifrasDecimales(nuevaArea));
					this.predioSeleccionado.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
					this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
				}

				if (!this.actualizacionDeAvaluosCatastrales(this.tramite.getId(), this.usuario)) {
					this.addMensajeError("Error al recalcular el avaluo ");
				} else {
					try {
						this.predioSeleccionado = this.getConservacionService()
								.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
						this.setAvaluo();
					} catch (Exception ex) {
						LOGGER.error("Error al consultar el Predio");
					}
				}
			} else {
				this.addMensajeInfo("Se requiere un proceso paralelo para este tramite de rectificación de zonas");
				// TODO::felipe.cadena::30/01/2014::Se debe implementar el
				// proceso paralelo para este tipo de tramites
			}
		}
	}

	/**
	 * Elimina las zonas inscritas previamente
	 *
	 * @author felipe.cadena
	 * @param idTramite
	 */
	public void eliminarZonasInscritas(Long idTramite) {

		List<PPredio> prediosProyectados = this.getConservacionService().buscarPPrediosCompletosPorTramiteId(idTramite);
		List<PPredioZona> zonasInscritas = new LinkedList<PPredioZona>();

		for (PPredio pp : prediosProyectados) {

			// Eliminar zonas inscritas previamente
			zonasInscritas = this.getConservacionService().buscarZonasHomogeneasPorPPredioIdYEstadoP(pp.getId(),
					EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
			if (!zonasInscritas.isEmpty()) {
				for (PPredioZona pPredioZona : zonasInscritas) {
					this.getConservacionService().eliminarZona(pPredioZona);
				}
			}
		}
	}

	/**
	 * Método para la actualizacion de los posibles predios de un tramite geografico
	 *
	 * @author felipe.cadena
	 * @param tramiteId
	 * @param usuario
	 * @return
	 */
	private boolean actualizacionDeAvaluosCatastrales(Long tramiteId, UsuarioDTO usuario) {

		List<PPredio> predios = this.getConservacionService().obtenerPPrediosPorTramiteIdActAvaluos(tramiteId);

		if (predios != null && !predios.isEmpty()) {
			for (PPredio pp : predios) {
				Object[] errores = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(tramiteId,
						pp.getId(), new Date(), usuario);
				if (Utilidades.hayErrorEnEjecucionSP(errores)) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Metodo para eliminar el avaluo manual seleccionado
	 *
	 * @author felipe.cadena
	 */
	public void eliminarAvaluoManual() {
		if (this.pPredioAvaluoCatastralSeleccionado != null) {
			this.pPredioAvaluoCatastralSeleccionado.setValorTotalAvaluoCatastral(0d);
			this.pPredioAvaluoCatastralSeleccionado.setValorTerreno(0d);
			this.pPredioAvaluoCatastralSeleccionado.setValorTotalConsConvencional(0d);
			this.pPredioAvaluoCatastralSeleccionado.setValorTotalConsNconvencional(0d);
			this.pPredioAvaluoCatastralSeleccionado.setValorTotalConstruccion(0d);
		}
		// felipe.cadena::redmine #7538
		PPredioAvaluoCatastral pac = this.getConservacionService()
				.obtenerPPredioAvaluosCatastral(this.pPredioAvaluoCatastralSeleccionado.getId());
		pac.setValorTotalAvaluoCatastral(0d);
		pac.setValorTerreno(0d);
		pac.setValorTotalConsConvencional(0d);
		pac.setValorTotalConsNconvencional(0d);
		pac.setValorTotalConstruccion(0d);
		pac = this.getConservacionService().actualizarPPredioAvaluoCatastral(pac);

		// #11261 :: david.cifuentes :: Ajuste para el registro y eliminación de
		// avaluos manuales para mutaciones de quinta nuevo :: 17/03/2015
		if (this.tramite.isQuintaNuevo()) {

			if (this.pPredioAvaluoCatastralSeleccionado != null && pPredioAvaluoCatastralSeleccionado
					.getCancelaInscribe().equals(EProyeccionCancelaInscribe.TEMP.getCodigo())) {
				PPredioAvaluoCatastral ppac = new PPredioAvaluoCatastral();
				ppac.setId(pPredioAvaluoCatastralSeleccionado.getId());
				this.getConservacionService().eliminarPPredioAvaluoCatastral(ppac);
			}

			this.pPrediosAvaluosCatastrales = this.getFormacionService()
					.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());
		}
	}

	/**
	 * Determina si el tramite actual esta en el proceso de validacion
	 *
	 * @author felipe.cadena
	 * @return
	 */
	public void setupTramiteValidacion() {

		Actividad act = this.tareasPendientesMB.buscarActividadPorIdTramite(this.tramite.getId());

		if (act != null) {
			if (act.getNombre()
					.equals(ProcesoDeConservacion.ACT_VALIDACION_MODIFICAR_INFORMACION_ALFANUMERICA.toString())) {
				this.tramiteValidacion = true;
				return;
			}
		}

		this.tramiteValidacion = false;
	}

	/**
	 * Calcula el area construida de un predio sea convencional o no convencional
	 *
	 * redmine #7881
	 *
	 * @author felipe.cadena
	 * @param predio
	 * @param convencional
	 * @return <br/>
	 *         convencional = true : Area convencional, <br/>
	 *         convencional = false : area no convencional, <br/>
	 *         convencional = null : area total
	 */
	public Double calcularAreaTotalPredios(PPredio predio, Boolean convencional) {
		LOGGER.info("calcularAreaTotalConsConvencional");
		Double area = 0.0;
		predio.setTramite(this.tramite);
		if (convencional == null || convencional) {
			if (predio.getPUnidadesConstruccionConvencionalNuevas() != null) {
				for (IModeloUnidadConstruccion unidad : predio.getPUnidadesConstruccionConvencionalNuevas()) {

					if ((unidad.getCancelaInscribe() != null) && (unidad.getCancelaInscribe()
							.equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())
							|| unidad.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {

						area += unidad.getAreaConstruida();
					}
				}
			}
		}
		if (convencional == null || !convencional) {
			if (predio.getPUnidadesConstruccionNoConvencionalNuevas() != null) {
				for (IModeloUnidadConstruccion unidad : predio.getPUnidadesConstruccionNoConvencionalNuevas()) {
					if ((unidad.getCancelaInscribe() != null) && (unidad.getCancelaInscribe()
							.equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())
							|| unidad.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {
						area += unidad.getAreaConstruida();
					}
				}
			}
		}
		return area;
	}

	/**
	 * Determina si ya se modificaron las areas de construccion para los trámites de
	 * ete tipo.
	 *
	 * redmine #9241
	 *
	 * @author felipe.cadena
	 */
	private void actualizarBanderaModificacion() {

		boolean modificoConvencionales = false;
		boolean modificoNoConvencionales = false;
		boolean tramiteDimension = false;
		boolean tramiteAreaCons = false;

		List<PUnidadConstruccion> pucs = this.getConservacionService()
				.buscarUnidadesDeConstruccionPorPPredioId(this.tramite.getPredio().getId(), false);
		List<UnidadConstruccion> ucs = this.getConservacionService()
				.obtenerUnidadesConstruccionsPorNumeroPredial(this.tramite.getPredio().getNumeroPredial());

		for (TramiteRectificacion tr : this.tramite.getTramiteRectificacions()) {
			if (tr.getDatoRectificar().getNombre().equals(EDatoRectificarNombre.DIMENSION.getCodigo())) {
				tramiteDimension = true;
			}
			if (tr.getDatoRectificar().getNombre().equals(EDatoRectificarNombre.AREA_CONSTRUIDA.getCodigo())) {
				tramiteAreaCons = true;
			}
		}

		if (!tramiteDimension && !tramiteAreaCons) {
			this.banderaRectAreaConstruccion = true;
			return;
		}

		// se descartan las construcciones canceladas en tramites anteriores.
		for (int i = 0; i < ucs.size(); i++) {
			UnidadConstruccion uc = ucs.get(i);
			if (uc.getAnioCancelacion() != null) {
				ucs.remove(uc);
			}
		}

		for (PUnidadConstruccion puc : pucs) {
			for (UnidadConstruccion uc : ucs) {
				if (uc.getId().equals(puc.getId())) {
					if (!uc.getAreaConstruida().equals(puc.getAreaConstruida())) {
						if (puc.getTipoConstruccion().equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {
							modificoConvencionales = true;
						} else {
							modificoNoConvencionales = true;
						}
					}
				}
			}
		}

		if (tramiteDimension && !tramiteAreaCons) {
			this.banderaRectAreaConstruccion = modificoNoConvencionales;
		} else if (!tramiteDimension && tramiteAreaCons) {
			this.banderaRectAreaConstruccion = modificoConvencionales;
		} else {
			this.banderaRectAreaConstruccion = (modificoConvencionales && modificoNoConvencionales);
		}

	}

	/**
	 * Se debe obtener los modlos de la ficha matriz asociadaal predio, preo que no
	 * esta proyectada
	 *
	 * @author felipe.cadena
	 */
	public void consultarModelosMutacionTercera() {

		List<FichaMatrizModelo> modelos = null;
		List<PFichaMatrizModelo> modelosProyectados = new LinkedList<PFichaMatrizModelo>();
		if ((this.tramite.isTercera() && !this.tramite.isTerceraMasiva())
				&& this.tramite.getPredio().isEsPredioEnPH()) {

			String numPredial = this.tramite.getPredio().getNumeroPredial();
			FichaMatriz fm = this.getConservacionService().obtenerFichaMatrizOrigen(numPredial);

			// Revisar si la ficha matriz tiene modelos asociados.
			if (fm != null && fm.getFichaMatrizModelos() != null && fm.getFichaMatrizModelos().size() > 0) {
				modelos = fm.getFichaMatrizModelos();
			}
		}

		if (this.tramite.isTerceraMasiva()) {

			PFichaMatriz pfm = this.getConservacionService()
					.buscarPFichaMatrizPorPredioId(this.tramite.getPredio().getId());

			// Revisar si la ficha matriz tiene modelos asociados.
			if (pfm != null && pfm.getPFichaMatrizModelos() != null && pfm.getPFichaMatrizModelos().size() > 0) {
				modelosProyectados = pfm.getPFichaMatrizModelos();
			}
		}

		if (modelos != null) {
			for (FichaMatrizModelo fmm : modelos) {
				PFichaMatrizModelo modeloProyectado = fmm.obtenerProyectadoDesdeOriginal();
				modeloProyectado.setId(fmm.getId());
				modelosProyectados.add(modeloProyectado);
			}

			this.listModelosUnidadDeConstruccion = modelosProyectados;
		} else if (this.tramite.isTerceraMasiva() && modelosProyectados != null) {
			this.listModelosUnidadDeConstruccion = modelosProyectados;
		}
		this.modeloUnidadDeConstruccionSeleccionado = null;

	}

	/**
	 * Valida si los predios relacionados al tramite actual poseen tramites
	 * diferentes en curso
	 */
	public void validarTramitesAsociadosAPredios() {

		if (!this.tramite.isQuinta() && !this.tramite.isSegundaEnglobe() && !this.tramite.isDesenglobeEtapas()
				&& !this.tramite.isViaGubernativa() && !this.tramite.isTerceraMasiva()) {
			try {
				PPredio pp = this.getConservacionService().obtenerPPredioCompletoById(this.tramite.getPredio().getId());
				if (pp != null && !pp.getTramite().getId().equals(this.tramite.getId())) {
					this.predioEnTramite = true;
					this.mensajePredioEnTramite = "El predio: " + pp.getNumeroPredial()
							+ " está en proyectado en el trámite: " + pp.getTramite().getNumeroRadicacion()
							+ Constantes.PREDIO_EN_TRAMITE_ASOCIADO;
					return;
				}
			} catch (Exception ex) {
				LOGGER.error("Error consultando predio asociados al tramite", ex.getMessage());
			}
		} else if (this.tramite.isSegundaEnglobe() || this.tramite.isDesenglobeEtapas()
				|| this.tramite.isQuintaMasivo()) {
			try {
				List<TramiteDetallePredio> prediosEnglobe = this.getTramiteService()
						.getTramiteDetallePredioByTramiteId(this.tramite.getId());

				if (prediosEnglobe != null) {
					for (TramiteDetallePredio tdt : prediosEnglobe) {

						PPredio pp = this.getConservacionService().obtenerPPredioCompletoById(tdt.getPredio().getId());
						if (pp != null && !pp.getTramite().getId().equals(this.tramite.getId())) {
							this.predioEnTramite = true;
							this.mensajePredioEnTramite = "El predio: " + pp.getNumeroPredial()
									+ " está proyectado en el trámite: " + pp.getTramite().getNumeroRadicacion()
									+ Constantes.PREDIO_EN_TRAMITE_ASOCIADO;
							return;
						}
					}
				}
			} catch (Exception ex) {
				java.util.logging.Logger.getLogger(ProyectarConservacionMB.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else if (this.tramite.isTerceraMasiva()) {
			try {
				// Busqueda de los predios del trámite
				List<TramiteDetallePredio> prediosTramite = this.getTramiteService()
						.getTramiteDetallePredioByTramiteId(this.tramite.getId());

				if (prediosTramite != null && !prediosTramite.isEmpty()) {
					List<Long> idsPrediosTramite = new ArrayList<Long>();
					for (TramiteDetallePredio tdp : prediosTramite) {
						idsPrediosTramite.add(tdp.getPredio().getId());
					}
					// Consulta de los predios proyectados del trámite
					List<PPredio> prediosProyectados = this.getConservacionService()
							.obtenerPPrediosCompletosPorIdsPHCondominio(idsPrediosTramite);

					if (prediosProyectados != null && !prediosProyectados.isEmpty()) {
						if (prediosProyectados.size() == 1 && prediosProyectados.get(0) != null
								&& !prediosProyectados.get(0).getTramite().getId().equals(this.tramite.getId())) {
							this.mensajePredioEnTramite = "El predio: " + prediosProyectados.get(0).getNumeroPredial()
									+ " está proyectado en el trámite: "
									+ prediosProyectados.get(0).getTramite().getNumeroRadicacion()
									+ Constantes.PREDIO_EN_TRAMITE_ASOCIADO;
							this.predioEnTramite = true;
							return;
						} else {
							for (PPredio pp : prediosProyectados) {
								if (pp != null && !pp.getTramite().getId().equals(this.tramite.getId())) {
									this.mensajePredioEnTramite = "El predio: " + pp.getNumeroPredial()
											+ " está proyectado en el trámite: " + pp.getTramite().getNumeroRadicacion()
											+ Constantes.PREDIO_EN_TRAMITE_ASOCIADO;
									this.predioEnTramite = true;
									return;
								}
							}
						}
					}

				}
			} catch (Exception ex) {
				LOGGER.error("Error consultando los  predios asociados al tramite");
			}
		}
	}

	/**
	 * Determina el estado de la edicion de predios en desenglobde de manzanas
	 *
	 * @author felipe.cadena
	 */
	private int determinarEstadoEdicion(PPredio predio) {

		// estado propietarios
		List<PPersonaPredio> ppps = predio.getPPersonaPredios();
		if (ppps == null || ppps.isEmpty()) {
			return 0;
		} else {
			boolean nuevoPropietario = false;
			for (PPersonaPredio ppp : ppps) {
				if (EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(ppp.getCancelaInscribe())) {
					// estado justificaciones
					List<PPersonaPredioPropiedad> justificaciones = ppp.getPPersonaPredioPropiedads();

					if (justificaciones == null || justificaciones.isEmpty()) {
						return 0;
					} else {
						boolean existeJustificacion = false;
						for (PPersonaPredioPropiedad jus : justificaciones) {
							if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(ppp.getCancelaInscribe())) {
								existeJustificacion = true;
								break;
							}
						}
						if (existeJustificacion) {
							nuevoPropietario = true;
							break;
						}
					}
				}
			}

			if (!nuevoPropietario) {
				return 0;
			}
		}

		// estado datos generales
		if (predio.getNumeroRegistro() == null || predio.getNumeroRegistro().isEmpty()) {
			return 0;
		}
		if (predio.getTipo() == null || predio.getTipo().isEmpty()) {
			return 0;
		}
		if (predio.getDestino() == null || predio.getDestino().isEmpty()) {
			return 0;
		}
		if (predio.getNombre() == null || predio.getNombre().isEmpty()) {
			return 0;
		}
		if (predio.getDireccionPrincipal() == null || predio.getDireccionPrincipal().isEmpty()) {
			return 0;
		}

		// estado detalle del avaluo
		if (predio.getAvaluoCatastral() == null || predio.getAvaluoCatastral() <= 0) {
			return 0;
		}

		// estado Inscripciones y decretos
		List<PPredioAvaluoCatastral> avaluos = predio.getPPredioAvaluoCatastrals();
		if (avaluos == null || avaluos.isEmpty()) {
			return 0;
		}

		return 1;
	}

	/**
	 * Método que realiza el set de las torres que están en firme en la ficha
	 * matriz.
	 *
	 * @author david.cifuentes
	 */
	private void verificarTorresEnFirme() {
		if (this.tramite.isDesenglobeEtapas()) {
			// Set de torres que se encuentran en firme
			if (this.fichaMatrizSeleccionada != null && this.fichaMatrizSeleccionada.getPFichaMatrizTorres() != null
					&& !this.fichaMatrizSeleccionada.getPFichaMatrizTorres().isEmpty()) {

				for (PFichaMatrizTorre pfmt : this.fichaMatrizSeleccionada.getPFichaMatrizTorres()) {
					if (ESiNo.SI.getCodigo().equals(pfmt.getOriginalTramite())) {
						pfmt.setEstadoEtapas(EEstadoPFichaMatrizTorreEtapas.EN_FIRME.getEstado());
					} else if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pfmt.getCancelaInscribe())) {
						pfmt.setEstadoEtapas(EEstadoPFichaMatrizTorreEtapas.CANCELADA.getEstado());
					} else {
						pfmt.setEstadoEtapas(EEstadoPFichaMatrizTorreEtapas.NUEVA.getEstado());
					}
				}
			}
		}
	}

	/**
	 * Metodo para validar las fechas de inscripcion de los predios asociados a un
	 * desenglobe por etapas
	 *
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean validarFechaInscripcionEtapas() {

		Date fechaPHCondominio = this.fichaMatrizSeleccionada.getPPredio().getFechaInscripcionCatastral();

		// Solo se realiza validacion si la fecha del predio ficha matriz existe
		if (fechaPHCondominio != null) {
			if (entradaProyeccionMB.getPrediosResultantes() != null) {
				for (PPredio pp : entradaProyeccionMB.getPrediosResultantes()) {
					// Solo se valida para los predios nuevos del tramite
					if (!pp.isPredioOriginalTramite()) {
						if (fechaPHCondominio.after(pp.getFechaInscripcionCatastral())) {
							this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_014.toString());
							LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_014.getMensajeTecnico());
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Determina los predios modificados en un tramite de rectificacion masiva
	 *
	 * @author felipe.cadena
	 */
	private void determinarPrediosModificadosRectificacion() {

		List<TramiteRectificacion> rectificacionesTramite = this.tramite.getTramiteRectificacions();
		boolean soloRectificaDireccion = false;
		boolean soloRectificaMatricula = false;

		// Determina si solo se esta rectificando la matricula
		for (TramiteRectificacion tr : rectificacionesTramite) {
			if (Constantes.MATRICULA_INMOBILIARIA.equals(tr.getDatoRectificar().getColumna())) {
				soloRectificaMatricula = true;
			} else {
				soloRectificaMatricula = false;
				break;
			}
		}

		// Si solo se esta rectificando la matricula no se considera ningun
		// predio como modificado
		if (soloRectificaMatricula) {
			return;
		}

		// Determina si solo se esta rectificando la direccion y la matricula
		for (TramiteRectificacion tr : rectificacionesTramite) {
			if (Constantes.DIRECCION.contains(tr.getDatoRectificar().getColumna())
					|| Constantes.MATRICULA_INMOBILIARIA.equals(tr.getDatoRectificar().getColumna())) {
				soloRectificaDireccion = true;
			} else {
				soloRectificaDireccion = false;
				break;
			}
		}

		// Se consultan los predios para actualizar si fueron modificados o no
		List<PPredio> prediosTramite = this.getConservacionService().buscarPPrediosPorTramiteId(this.tramite.getId());

		// Si solo se rectifica direccion se verifica que predios
		// fueron modificados, de lo contrario todos los predios se consideran
		// como modificados
		if (soloRectificaDireccion) {
			TreeMap<Long, Boolean> prediosModificados = this.getConservacionService()
					.obtenerPredioModificadoPorTamite(this.tramite.getId(), Constantes.TABLA_DIRECCION_PROYECTADA);

			// javier.aponte :: refs#14086 Se valida si el número predial del
			// predio fue modificado y en ese
			// caso también se establece en original trámite en 'NO' para el
			// p_predio
			TreeMap<String, Boolean> prediosConNumeroPredialModificado = this.getConservacionService()
					.obtenerPredioConNumeroPredialModificadoPorTamite(this.tramite.getId());

			Boolean modificado;

			for (PPredio predio : prediosTramite) {
				modificado = prediosModificados.get(predio.getId());
				if (modificado != null && modificado) {
					predio.setOriginalTramite(ESiNo.NO.getCodigo());
					predio.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
				} else {
					modificado = prediosConNumeroPredialModificado.get(predio.getId().toString());
					if (modificado != null && modificado.booleanValue()) {
						predio.setOriginalTramite(ESiNo.NO.getCodigo());
						predio.getOriginalTramite();
						this.getConservacionService().guardarPPredio(predio);
					}
				}
			}
		} else {
			for (PPredio predio : prediosTramite) {
				predio.setOriginalTramite(ESiNo.NO.getCodigo());
				predio.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
			}
		}
		// Se geuardan los predios actualizados en la base de datos
		this.getConservacionService().guardarPPredios(prediosTramite);

	}

	/**
	 * Determina los predios modificados en un tramite de tercera masiva
	 *
	 * @author javier.aponte
	 */
	private void determinarPrediosModificadosTerceraMasiva() {

		TreeMap<Long, Boolean> prediosModificados = this.getConservacionService().obtenerPredioModificadoPorTamite(
				this.tramite.getId(), Constantes.TABLA_UNIDAD_CONSTRUCCION_PROYECTADA);

		// javier.aponte :: refs#14086 Se valida si el número predial del predio
		// fue modificado y en ese
		// caso también se establece en original trámite en 'NO' para el
		// p_predio
		TreeMap<String, Boolean> prediosConNumeroPredialModificado = this.getConservacionService()
				.obtenerPredioConNumeroPredialModificadoPorTamite(this.tramite.getId());

		Boolean modificado;

		// leidy.gonzalez :: refs#16299 Se modifica el tipo de dato que se
		// envia al metodo prediosConNumeroPredialModificado
		for (PPredio predio : this.pPrediosTerceraRectificacionMasiva) {
			modificado = prediosModificados.get(predio.getId());
			if (modificado != null && modificado.booleanValue()) {
				predio.setOriginalTramite(ESiNo.NO.getCodigo());
				this.getConservacionService().guardarPPredio(predio);
			} else {
				modificado = prediosConNumeroPredialModificado.get(predio.getId().toString());
				if (modificado != null && modificado.booleanValue()) {
					predio.setOriginalTramite(ESiNo.NO.getCodigo());
					this.getConservacionService().guardarPPredio(predio);
				}
			}
		}

		if (this.predioSeleccionado != null) {

			modificado = prediosModificados.get(this.predioSeleccionado.getId());
			if (modificado != null && modificado.booleanValue()) {
				this.predioSeleccionado.setOriginalTramite(ESiNo.NO.getCodigo());
			} else {
				modificado = prediosConNumeroPredialModificado.get(this.predioSeleccionado.getId().toString());
				if (modificado != null && modificado.booleanValue()) {
					this.predioSeleccionado.setOriginalTramite(ESiNo.NO.getCodigo());
				}
			}
		}
	}

	/**
	 *
	 * @author felipe.cadena recalcula los avaluos para los predios de rectificacion
	 *         matriz
	 */
	public void recalcularAvaluosRectificacionMatriz() {

		List<TramiteRectificacion> trs = this.tramite.getTramiteRectificacions();
		boolean requiereCalculo = false;

		try {
			for (TramiteRectificacion tramiteRectificacion : trs) {

				if (Constantes.DATOS_MODIFICAN_AVALUO.contains(tramiteRectificacion.getDatoRectificar().getId())) {
					requiereCalculo = true;
					break;
				}
			}

			if (requiereCalculo) {
				List<PPredio> prediosTramite = this.pPrediosTerceraRectificacionMasiva;

				for (PPredio pPredio : prediosTramite) {
					this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(this.tramite.getId(),
							pPredio.getId(), new Date(), this.usuario);
				}
			}
		} catch (Exception e) {

			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			this.addMensajeError(ECodigoErrorVista.RECTIFICACION_CALCULO_AVALUO_001.getMensajeUsuario());
			LOGGER.error(ECodigoErrorVista.RECTIFICACION_CALCULO_AVALUO_001.getMensajeTecnico(), e);
		}
	}

	/**
	 * Valida que todos los predios de la rectificacion matriz tengan la fecha de
	 * inscripcion catastral
	 *
	 * @author felipe.cadena
	 * @return
	 */
	private boolean validarFechasRectificacionMatriz() {

		List<PPredio> prediosTramite = this.getConservacionService().buscarPPrediosPorTramiteId(this.tramite.getId());
		if (this.tramite.isRectificacionMatriz()) {
			for (PPredio pp : prediosTramite) {
				if (pp.getFechaInscripcionCatastral() == null) {

					this.addMensajeError(ECodigoErrorVista.RECTIFICACION_FECHA_INSCRIPCION_001.getMensajeUsuario());
					LOGGER.warn(ECodigoErrorVista.RECTIFICACION_FECHA_INSCRIPCION_001.getMensajeTecnico());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Valida que todos los predios del desenglobe tenga fecha de inscipcion
	 *
	 * @author felipe.cadena
	 * @return
	 */
	private boolean validarFechasInscripcion() {

		List<PPredio> prediosTramite = this.getConservacionService().buscarPPrediosPorTramiteId(this.tramite.getId());
		// juan.cruz: #24139 Se agrega validación fecha inscripcion catastral
		// para ambos tramites de segunda (Englobe y Desenglobe)
		if (this.tramite.isSegunda()) {
			for (PPredio pp : prediosTramite) {
				if (pp.getFechaInscripcionCatastral() == null) {
					this.addMensajeError(ECodigoErrorVista.NO_FECHA_INSCRIPCION_001.getMensajeUsuario());
					LOGGER.warn(ECodigoErrorVista.NO_FECHA_INSCRIPCION_001.getMensajeTecnico());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Persiste el predio como fiscal y actualiza las banderas para su
	 * funcionamiento
	 */
	public void guardarPredioFiscalQuintaNuevo() {
		LOGGER.debug("Estado de la variable : " + this.predioFiscalQuintaNuevo);

		if (this.predioFiscalQuintaNuevo) {
			this.predioSeleccionado.setTipoCatastro(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo());
			this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
			this.banderaPredioFiscal = true;
			this.banderaAvaluoManualFiscales = true;
		} else {
			this.predioSeleccionado.setTipoCatastro(EPredioTipoCatastro.LEY_14.getCodigo());
			this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
			this.banderaPredioFiscal = false;
			this.banderaAvaluoManualFiscales = false;
		}
		List<PPredioAvaluoCatastral> avaluoLista = this.getConservacionService()
				.obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());
		if (!avaluoLista.isEmpty()) {
			this.getConservacionService().eliminarPPredioAvaluoCatastralCompleto(avaluoLista);
		}
		this.avaluo = new PPredioAvaluoCatastral();
	}

	// ------------------------------------------------------------- //
	/**
	 * Método encargado de realizar las validaciones de áreas de una única
	 * construcción seleccionada que ha sido asociada del predio original.
	 *
	 * @author david.cifuentes
	 * @param pPredioSeleccionado
	 * @return
	 */
	private boolean validarAreaConstruccionAsociadasEnGuardarDetalleAvaluo(PPredio pPredioSeleccionado) {

		PPredio predioOrigen = this.getConservacionService()
				.obtenerPPredioDatosAvaluo(this.tramite.getPredio().getId());

		if (predioOrigen != null && predioOrigen.getPUnidadConstruccions() != null) {
			if (this.unidadConstruccionSeleccionada.getAreaConstruida() == 0) {
				this.addMensajeError("El valor de área construida no puede ser cero para la unidad "
						+ this.unidadConstruccionSeleccionada.getUnidad() + ".");
				return false;
			}

			// Únicamente se puede asociar la misma unidad origen al predio
			if (pPredioSeleccionado.getPUnidadConstruccions() != null
					&& !pPredioSeleccionado.getPUnidadConstruccions().isEmpty()) {

				List<String> unidadesAsociadasPredio = new ArrayList<String>();
				for (PUnidadConstruccion puc : pPredioSeleccionado.getPUnidadConstruccions()) {
					if (puc.getProvieneUnidad() != null) {
						if (!unidadesAsociadasPredio.contains(puc.getProvieneUnidad())) {
							unidadesAsociadasPredio.add(puc.getProvieneUnidad());
						} else {
							this.addMensajeError("Sólo se puede asociar una vez la unidad original "
									+ puc.getProvieneUnidad() + " al predio actual.");
							return false;
						}
					}
				}
			}

			if (!pPredioSeleccionado.isEsPredioFichaMatriz()
					&& this.unidadConstruccionSeleccionada.getProvieneUnidad() != null
					&& predioOrigen.getPUnidadConstruccions() != null) {
				for (PUnidadConstruccion pucOrigen : predioOrigen.getPUnidadConstruccions()) {
					if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pucOrigen.getCancelaInscribe())
							&& this.unidadConstruccionSeleccionada.getProvieneUnidad().equals(pucOrigen.getUnidad())) {
						Double areaOriginal = pucOrigen.getAreaConstruida();
						if (ESiNo.SI.getCodigo().equals(this.tramite.getMantieneNumeroPredial())
								|| this.unidadConstruccionSeleccionada.getId().doubleValue() == pucOrigen.getId()
										.doubleValue()) {
							UnidadConstruccion ucOriginal = this.getConservacionService()
									.findUnidadConstruccionById(pucOrigen.getId());
							if (ucOriginal != null) {
								areaOriginal = ucOriginal.getAreaConstruida();
							}
						}
						if (this.unidadConstruccionSeleccionada.getAreaConstruida() > areaOriginal) {
							this.addMensajeError("El valor de área construida ingresado para la unidad "
									+ this.unidadConstruccionSeleccionada.getUnidad()
									+ " supera el área disponible para asociar de la construcción original.");
							return false;
						}
					}
				}
			}

			HashMap<String, Double> areasRestantes = new HashMap<String, Double>();

			// Cargue de las construcciones Originales
			List<UnidadConstruccion> unidadesConstruccionPredioOrigen = this.getConservacionService()
					.obtenerUnidadesNoCanceladasByPredioId(this.tramite.getPredio().getId());

			DecimalFormat formatoAreaConstruida = new DecimalFormat(Constantes.FORMATO_AREA_CONSTRUCCION);
			String areaRestanteRedondeada;

			if (this.pPrediosDesenglobe != null) {
				for (UnidadConstruccion ucOrigen : unidadesConstruccionPredioOrigen) {
					Double areaRestante = ucOrigen.getAreaConstruida();
					for (PPredio pp : this.pPrediosDesenglobe) {
						if (pp.getId().longValue() != pPredioSeleccionado.getId().longValue()) {
							if (pp.getPUnidadConstruccions() != null) {
								for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
									if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.getCancelaInscribe())
											&& ucOrigen.getUnidad().equals(puc.getProvieneUnidad())) {
										areaRestante = Utilidades.redondearNumeroADosCifrasDecimales(
												areaRestante - puc.getAreaConstruida());
									}
								}
							}
						} else {
							if (ucOrigen.getUnidad().equals(this.unidadConstruccionSeleccionada.getProvieneUnidad())) {
								areaRestante = Utilidades.redondearNumeroADosCifrasDecimales(
										areaRestante - this.unidadConstruccionSeleccionada.getAreaConstruida());
							}
						}
					}
					areasRestantes.put(ucOrigen.getUnidad(), areaRestante);
				}

				if (this.unidadConstruccionSeleccionada.getProvieneUnidad() != null
						&& areasRestantes.get(this.unidadConstruccionSeleccionada.getProvieneUnidad()) != null
						&& areasRestantes.get(this.unidadConstruccionSeleccionada.getProvieneUnidad()) < 0) {
					this.addMensajeError("El valor de área construida ingresado para la unidad "
							+ this.unidadConstruccionSeleccionada.getUnidad()
							+ " supera el área disponible para asociar de la construcción original.");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Método que realiza el cargue de las construcciones del predio origen,
	 * escluyendo las construcciones en las cuales su área se haya asociado en su
	 * totalidad
	 *
	 * @author david.cifuentes -- fix cmf
	 */
	public void cargarConstruccionesAsociadasPredioOrigen() {

		if (this.getTramite().isSegundaDesenglobe()) {
			try {

				this.unidadConstruccionConvencionalPredioOriginal = new ArrayList<UnidadConstruccion>();
				this.unidadConstruccionNoConvencionalPredioOriginal = new ArrayList<UnidadConstruccion>();

				// Cargue de las construcciones Originales
				List<UnidadConstruccion> unidadesConstruccionPredioOrigen = this.getConservacionService()
						.obtenerUnidadesNoCanceladasByPredioId(this.tramite.getPredio().getId());

				if (unidadesConstruccionPredioOrigen != null && !unidadesConstruccionPredioOrigen.isEmpty()) {
					for (UnidadConstruccion uc : unidadesConstruccionPredioOrigen) {
						if (EUnidadTipoConstruccion.CONVENCIONAL.getCodigo().equals(uc.getTipoConstruccion().trim())
								&& uc.getAnioCancelacion() == null) {

							if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
								this.unidadConstruccionConvencionalPredioOriginal.add(uc);
							}
						}
						if (EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo()
								.equals(uc.getTipoConstruccion().trim())) {
							if (!this.validarAreaCompletaAsignadaConstruccionOriginal(uc)) {
								this.unidadConstruccionNoConvencionalPredioOriginal.add(uc);
							}
						}
					}

					this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = null;
					this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = null;

					if (this.fichaMatrizMB == null) {
						this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
					}

					this.fichaMatrizMB.setUnidadConstruccionConvencionalPredioOriginal(
							this.unidadConstruccionConvencionalPredioOriginal);
					this.fichaMatrizMB.setUnidadConstruccionNoConvencionalPredioOriginal(
							this.unidadConstruccionNoConvencionalPredioOriginal);
					this.fichaMatrizMB.setUnidadConstruccionConvencionalPredioOriginalSeleccionadas(null);
					this.fichaMatrizMB.setUnidadConstruccionNoConvencionalPredioOriginalSeleccionadas(null);
				}

				this.verificarAreasRestantesConstruccionesPredioOriginal();

			} catch (Exception e) {
				LOGGER.error("Error realizando la búsqueda de las unidades del predio origen': " + e.getMessage());
			}
		}
	}

	// ---------------------------------------------------------------- //
	/**
	 * Método que realiza el cálculo de las áreas restantes para cada una de las
	 * {@link UnidadConstruccion} del predio Origen.
	 *
	 * @author david.cifuentes
	 */
	public void verificarAreasRestantesConstruccionesPredioOriginal() {

		// Cálculo de áreas restantes para unidades convencionales
		if (this.unidadConstruccionConvencionalPredioOriginal != null
				&& !this.unidadConstruccionConvencionalPredioOriginal.isEmpty()) {
			for (UnidadConstruccion uc : this.unidadConstruccionConvencionalPredioOriginal) {

				List<PUnidadConstruccion> pucAsociadas = this.getConservacionService()
						.buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(this.getTramite().getId(), uc);

				if (pucAsociadas != null) {

					Double areaAsignada = new Double(0);

					for (PUnidadConstruccion puc : pucAsociadas) {
						if (puc.getUnidad() != null
								&& !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.getCancelaInscribe())
								&& puc.getProvieneUnidad() != null && puc.getProvieneUnidad().equals(uc.getUnidad())) {

							areaAsignada = Utilidades
									.redondearNumeroADosCifrasDecimales(areaAsignada + puc.getAreaConstruida());

						}
					}

					uc.setAreaConstruidaRestante(
							Utilidades.redondearNumeroADosCifrasDecimales(uc.getAreaConstruida() - areaAsignada));
				}
			}
		}

		// Cálculo de áreas restantes para unidades no convencionales
		if (this.unidadConstruccionNoConvencionalPredioOriginal != null
				&& !this.unidadConstruccionNoConvencionalPredioOriginal.isEmpty()) {
			for (UnidadConstruccion uc : this.unidadConstruccionNoConvencionalPredioOriginal) {

				List<PUnidadConstruccion> pucAsociadas = this.getConservacionService()
						.buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(this.getTramite().getId(), uc);

				if (pucAsociadas != null) {

					Double areaAsignada = new Double(0);

					for (PUnidadConstruccion puc : pucAsociadas) {
						if (puc.getUnidad() != null
								&& !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.getCancelaInscribe())
								&& puc.getProvieneUnidad() != null && puc.getProvieneUnidad().equals(uc.getUnidad())) {
							areaAsignada = Utilidades
									.redondearNumeroADosCifrasDecimales(areaAsignada + puc.getAreaConstruida());
						}
					}
					uc.setAreaConstruidaRestante(
							Utilidades.redondearNumeroADosCifrasDecimales(uc.getAreaConstruida() - areaAsignada));
				}
			}
		}
	}

	// ---------------------------------------------------------------- //
	/**
	 * Método usado para validar el área asignada de una de las construcciones del
	 * predio origen, ha sido asignada en su totalidad a los predios resultantes en
	 * el trámite de desenglobe
	 *
	 * @author david.cifuentes
	 * @param unidadConstruccionOrigen
	 * @return true - Si el área original ha sido asignada en su totalidad false -
	 *         Si el parte del área total aún se ha asignado
	 */
	public boolean validarAreaCompletaAsignadaConstruccionOriginal(UnidadConstruccion unidadConstruccionOrigen) {

		// Se verifica si la unidad ha sido asociada o no, si no se ha asociado
		// se considera válida.
		List<PUnidadConstruccion> pucAsociadas = this.getConservacionService()
				.buscarUnidadesDeConstruccionAsociadasPorTramiteYUnidad(this.getTramite().getId(),
						unidadConstruccionOrigen);

		if (unidadConstruccionOrigen != null) {

			Double areaAsignada = new Double(0);

			for (PUnidadConstruccion puc : pucAsociadas) {
				if (puc.getUnidad() != null
						&& !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.getCancelaInscribe())
						&& puc.getProvieneUnidad() != null
						&& puc.getProvieneUnidad().equals(unidadConstruccionOrigen.getUnidad())) {
					areaAsignada += puc.getAreaConstruida();
				}
			}
			// Verificar si la suma de áreas asignadas completa la totalidad del
			// área de la construcción origen
			if (unidadConstruccionOrigen != null) {
				if (areaAsignada.doubleValue() >= unidadConstruccionOrigen.getAreaConstruida().doubleValue()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Método que realiza un llamado al método del mismo nombre en
	 * {@link FichaMatrizMB} para asociar las construcciones originales del predio
	 * que hayan sido seleccionadas (convencionales y no convencionales) a las
	 * nuevas.
	 *
	 * @author david.cifuentes
	 */
	public void asociarConstruccionesPredioOriginal() {
		try {

			if (this.fichaMatrizMB == null) {
				this.fichaMatrizMB = (FichaMatrizMB) UtilidadesWeb.getManagedBean("fichaMatriz");
			}

			this.fichaMatrizMB
					.setUnidadConstruccionConvencionalPredioOriginal(this.unidadConstruccionConvencionalPredioOriginal);
			this.fichaMatrizMB.setUnidadConstruccionNoConvencionalPredioOriginal(
					this.unidadConstruccionNoConvencionalPredioOriginal);
			this.fichaMatrizMB.asociarConstruccionesPredioOriginal();

			this.verificarCIFotosDeUnidadesConstruccion();

			if (this.predioSeleccionado != null) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
			}

			this.unidadConstruccionConvencionalPredioOriginal = this.fichaMatrizMB
					.getUnidadConstruccionConvencionalPredioOriginal();
			this.unidadConstruccionNoConvencionalPredioOriginal = this.fichaMatrizMB
					.getUnidadConstruccionNoConvencionalPredioOriginal();
			;
			this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = null;
			this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = null;
			this.setupUnidadesConstruccion();

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			this.addMensajeInfo("Ocurrió un error al asociar las construcciones al predio.");
		}
	}

	// ------------------------------------------------------------- //
	/**
	 * Método encargado de realizar las validaciones de áreas de las construcciones
	 * del predio seleccionado, cuando las construcciones han sido asociadas del
	 * predio original.
	 *
	 * @author david.cifuentes
	 * @return
	 */
	private boolean validarAreasConstruccionesAsociadas() {

		// Cargue de las construcciones Originales
		List<UnidadConstruccion> unidadesConstruccionPredioOrigen = this.getConservacionService()
				.obtenerUnidadesNoCanceladasByPredioId(this.tramite.getPredio().getId());

		// @modified by leidy.gonzalez::RM#27036::13-07-2017::
		// Se adicionan las unidades de construccion de los predios codicion 0 y 5
		if (this.tramite.isEnglobeVirtual()) {

			List<Predio> prediosOriginales = this.getConservacionService()
					.buscarPrediosPanelAvaluoPorTramiteId(this.tramite.getId());

			for (Predio predio : prediosOriginales) {

				if (predio.getCondicionPropiedad() != null && !predio.getCondicionPropiedad().isEmpty()
						&& (EPredioCondicionPropiedad.CP_5.getCodigo().equals(predio.getCondicionPropiedad())
								|| EPredioCondicionPropiedad.CP_0.getCodigo().equals(predio.getCondicionPropiedad()))) {

					// Se carga Unidades Costruccion
					if (predio.getUnidadConstruccions() != null && !predio.getUnidadConstruccions().isEmpty()) {

						for (UnidadConstruccion unidadCons : predio.getUnidadConstruccions()) {

							if (unidadCons.getAnioCancelacion() == null) {

								unidadesConstruccionPredioOrigen.addAll(predio.getUnidadConstruccions());
							}

						}
					}

				}
			}
		}

		if (this.pPrediosDesenglobe != null && unidadesConstruccionPredioOrigen != null
				&& !this.tramite.isEnglobeVirtual()) {

			if (!this.pPrediosDesenglobe.isEmpty()
					&& !(this.pPrediosDesenglobe.get(0).isPHCondominio() || EPredioCondicionPropiedad.CP_0.getCodigo()
							.equals(this.pPrediosDesenglobe.get(0).getCondicionPropiedad()))) {
				return true;
			}

			HashMap<String, Double> areasRestantes = new HashMap<String, Double>();
			List<String> construccionesAsociadas = new ArrayList<String>();
			List<String> construccionesCero = new ArrayList<String>();
			String areaRestanteRendodeada;

			for (UnidadConstruccion ucOrigen : unidadesConstruccionPredioOrigen) {
				Double areaRestante = ucOrigen.getAreaConstruida();
				DecimalFormat formatoArea = new DecimalFormat(Constantes.FORMATO_AREA_CONSTRUCCION);

				boolean construccionAsociadaBool = false;
				for (PPredio pp : this.pPrediosDesenglobe) {
					if (pp.getPUnidadConstruccions() != null) {
						for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
							if (ucOrigen.getUnidad().equals(puc.getProvieneUnidad())) {
								areaRestante -= puc.getAreaConstruida();
								areaRestanteRendodeada = formatoArea.format(areaRestante);
								areaRestanteRendodeada = areaRestanteRendodeada.replace(",", ".");
								areaRestante = Double.valueOf(areaRestanteRendodeada);
								construccionAsociadaBool = true;
								if (!construccionesAsociadas.contains(ucOrigen.getUnidad())) {
									construccionesAsociadas.add(ucOrigen.getUnidad());
								}
							}
							if (puc.getAreaConstruida().doubleValue() == 0L) {
								construccionesCero.add(puc.getUnidad());
							}
						}
					}
				}
				if (construccionAsociadaBool) {
					areasRestantes.put(ucOrigen.getUnidad(), areaRestante);
				}
			}

			// felipe.cadena::RM#15185::19-11-2015::Se evita la validacion de
			// construcciones para desenglobes de etapas
			if ((unidadesConstruccionPredioOrigen.size() > construccionesAsociadas.size())
					&& !this.tramite.isDesenglobeEtapas()) {
				this.addMensajeError(
						"Algunas de las construcciones del predio original no estan asociadas a los predios del desenglobe.");
				return false;
			}

			if ((unidadesConstruccionPredioOrigen.size() > construccionesAsociadas.size())
					&& this.tramite.isEnglobeVirtual()) {
				this.addMensajeError(
						"Algunas de las construcciones del predio original no estan asociadas a los predios del desenglobe.");
				return false;
			}

			if (!construccionesCero.isEmpty()) {
				this.addMensajeError(
						"Algunas de las construcciones asociadas tienen un valor de Área de construcción de cero, por favor verifique.");
				return false;
			}

			boolean validacionAreasBool = true;
			String unidadesAreasMenores = new String("");
			String unidadesAreasMayores = new String("");

			for (UnidadConstruccion uc : unidadesConstruccionPredioOrigen) {
				if (areasRestantes.get(uc.getUnidad()) != null) {
					if (areasRestantes.get(uc.getUnidad()) < 0) {
						unidadesAreasMayores = unidadesAreasMayores + uc.getUnidad() + ", ";
						validacionAreasBool = false;
					} else if (areasRestantes.get(uc.getUnidad()) > 0) {
						unidadesAreasMenores = unidadesAreasMenores + uc.getUnidad() + ", ";
						validacionAreasBool = false;
					}
				}
			}

			if (!validacionAreasBool) {
				if (unidadesAreasMayores.length() > 1) {
					this.addMensajeError("El  Área construida ingresada de la unidad " + unidadesAreasMayores
							+ "  es mayor al Área total origen.");
				}
				if (unidadesAreasMenores.length() > 1) {
					this.addMensajeError("El Área de construcción para la unidad " + unidadesAreasMenores
							+ " del predio origen, no se ha asignado en su totalidad.");
				}
				return false;
			}
		}
		return true;
	}

	// ------------------------------------------------------------- //
	/**
	 * Método encargado de realizar las validaciones de áreas de las construcciones
	 * del predio seleccionado, cuando las construcciones han sido asociadas del
	 * predio original.
	 *
	 * @author david.cifuentes
	 * @param pPredioSeleccionado
	 * @return
	 */
	private boolean validarAreasConstruccionesAsociadasEnDetalleAvaluo(PPredio pPredioSeleccionado) {

		PPredio predioOrigen = this.getConservacionService()
				.obtenerPPredioDatosAvaluo(this.tramite.getPredio().getId());
		List<String> unidadesAreaCero = new ArrayList<String>();

		if (pPredioSeleccionado.getPUnidadConstruccions() != null && predioOrigen != null
				&& predioOrigen.getPUnidadConstruccions() != null) {
			for (PUnidadConstruccion puc : pPredioSeleccionado.getPUnidadConstruccions()) {
				if (puc.getAreaConstruida() == 0) {
					unidadesAreaCero.add(puc.getUnidad());
				}

				if (puc.getProvieneUnidad() != null && predioOrigen.getPUnidadConstruccions() != null) {
					for (PUnidadConstruccion pucOrigen : predioOrigen.getPUnidadConstruccions()) {
						if (EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pucOrigen.getUnidad())
								&& puc.getProvieneUnidad().equals(pucOrigen.getUnidad())) {
							Double areaOriginal = pucOrigen.getAreaConstruida();
							if (ESiNo.SI.getCodigo().equals(this.tramite.getMantieneNumeroPredial())) {
								UnidadConstruccion ucOriginal = this.getConservacionService()
										.findUnidadConstruccionById(pucOrigen.getId());
								if (ucOriginal != null) {
									areaOriginal = ucOriginal.getAreaConstruida();
								}
							}
							if (puc.getAreaConstruida() > areaOriginal) {
								this.addMensajeError("El valor de área construida ingresado para la unidad "
										+ puc.getUnidad()
										+ " supera el área disponible para asociar de la construcción original.");
								return false;
							}
						}
					}
				}
			}

			if (unidadesAreaCero.size() > 0) {
				if (unidadesAreaCero.size() == 1) {
					this.addMensajeError("El valor de área construida no puede ser cero para la unidad "
							+ unidadesAreaCero.get(0) + ".");
				} else {
					String unidades = new String("");
					for (String i : unidadesAreaCero) {
						unidades = unidades + i + ", ";
					}
					this.addMensajeError("El valor de área construida no puede ser cero para las unidades "
							+ unidades.substring(0, unidades.length() - 2) + ".");
				}
			}

			HashMap<String, Double> areasRestantes = new HashMap<String, Double>();

			if (this.pPrediosDesenglobe != null) {
				for (PUnidadConstruccion pucOrigen : predioOrigen.getPUnidadConstruccions()) {
					Double areaRestante = pucOrigen.getAreaConstruida();
					for (PPredio pp : this.pPrediosDesenglobe) {
						if (pp.getId().longValue() != pPredioSeleccionado.getId().longValue()) {
							if (pp.getPUnidadConstruccions() != null) {
								for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
									if (pucOrigen.getUnidad().equals(puc.getProvieneUnidad())) {
										areaRestante -= puc.getAreaConstruida();
									}
								}
							}
						} else {
							if (pPredioSeleccionado.getPUnidadConstruccions() != null) {
								for (PUnidadConstruccion puc : pPredioSeleccionado.getPUnidadConstruccions()) {
									if (pucOrigen.getUnidad().equals(puc.getProvieneUnidad())) {
										areaRestante -= puc.getAreaConstruida();
									}
								}
							}
						}
					}

					areasRestantes.put(pucOrigen.getUnidad(), areaRestante);
				}

				for (PUnidadConstruccion puc : pPredioSeleccionado.getPUnidadConstruccions()) {
					if (puc.getProvieneUnidad() != null && areasRestantes.get(puc.getProvieneUnidad()) != null
							&& areasRestantes.get(puc.getProvieneUnidad()) < 0) {
						this.addMensajeError("El valor de área construida ingresado para la unidad " + puc.getUnidad()
								+ " supera el área disponible para asociar de la construcción original.");
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Se asigna la misma fecha de inscripcion para todos los predios involucrados
	 * en la rectificacion de area
	 *
	 * @author felipe.cadena
	 */
	private boolean registrarFechaRectificacionAreaLinderos() {
		LOGGER.debug("registrarFechaRectificacionAreaLinderos. +++++++++++  "+this.predioSeleccionado.getFechaInscripcionCatastral());
		
		try {
			List<PPredio> prediosTramite = this.getConservacionService()
					.buscarPPrediosPorTramiteId(this.tramite.getId());

			for (PPredio pp : prediosTramite) {
				pp.setFechaInscripcionCatastral(this.predioSeleccionado.getFechaInscripcionCatastral());
			}

			prediosTramite = this.getConservacionService().guardarPPredios(prediosTramite);

		} catch (ExcepcionSNC e) {
			this.addMensajeError(ECodigoErrorVista.RECTIFICACION_LINDERO_001.toString());
			LOGGER.debug(ECodigoErrorVista.RECTIFICACION_LINDERO_001.getMensajeTecnico(), e);
			return false;
		}

		return true;

	}

	/**
	 * Método que carga los PPredio ACTIVOS para los trámites de quinta másivos
	 *
	 * @author lorena.salamanca
	 */
	public void cargarPrediosQuintaMasivo() {

		if (this.tramite.isQuintaMasivo()) {
			this.pPrediosQuintaMasivo = this.getConservacionService()
					.buscarPPrediosCompletosPorTramiteIdPHCondominio(this.tramite.getId());
		}
	}

	/**
	 * Método que carga los PPredio ACTIVOS para los trámites de cancelacion masiva
	 *
	 * @author leidy.gonzalez
	 */
	public void cargarPrediosCancelacionMasivo() {

		this.pPrediosCancelacionMasivo = this.getConservacionService()
				.buscarPPrediosCompletosPorTramiteId(this.tramite.getId());
	}

	/**
	 * Método que carga los PPredioPropietario para los predios de cancelacion
	 * masiva
	 *
	 * @author leidy.gonzalez
	 */
	public void cargarPropietariosCancelacionMasiva() {

		List<Long> idPredios = new ArrayList<Long>();

		if (this.pPrediosCancelacionMasivo != null && !this.pPrediosCancelacionMasivo.isEmpty()) {

			for (PPredio ppredioTemp : this.pPrediosCancelacionMasivo) {

				if (ppredioTemp.getId() != null) {
					idPredios.add(ppredioTemp.getId());
				}
			}

			if (idPredios != null && !idPredios.isEmpty()) {
				this.pPersonaPrediosCancelacionM = this.getConservacionService()
						.buscarPPersonaPrediosPorIdPredios(idPredios);
			}

		}
	}

	/**
	 * Método que calcula el valor de los avaluos proyectados para los trámites con
	 * predios fiscales
	 *
	 * @author jonathan.chacon :: 21/09/2015 :: 14052
	 */
	public void guardarAvaluoPredioFiscal() {

		LiquidacionAvaluoPredioFiscalMB liquidacion = new LiquidacionAvaluoPredioFiscalMB();

		// Revisar si los avaluos vienen en cero y activar las vistas de ingreso
		// manual
		// de los avaluos..
		// SEGUNDA
		if (this.tramite.isSegundaDesenglobe() && !this.tramite.getPredio().isMejora()
				&& !this.tramite.getPredio().isEsPredioEnPH()) {

			if (this.avaluo.getId() == null) {
				this.banderaAvaluoManualFiscales = true;
			}

			if (this.banderaAvaluoManualFiscales) {
				double valorMtTerrenoP = 0.0;
				double totalAreaProyectados = 0.0;
				// validaciones para calculo manual

				List<PPredio> prediosP = this.getConservacionService()
						.obtenerPPrediosPorTramiteIdActAvaluos(tramite.getId());

				for (PPredio predio : prediosP) {
					if (this.predioSeleccionado != null) {
						if (!this.predioSeleccionado.getId().equals(predio.getId())) {
							valorMtTerrenoP += predio.getAreaTerreno();
						}

						// valida si tiene construcciones asociadas con avalúo
						// en cero
						for (PUnidadConstruccion unidadN : this.predioSeleccionado
								.getPUnidadesConstruccionConvencionalNuevas()) {
							if (unidadN.getAvaluo() == 0) {
								addMensajeError("Alguna de las construcciones asociadas no tiene registrado"
										+ " el valor del avalúo.");
								return;
							}
						}

					}

					totalAreaProyectados += predio.getAreaTerreno();
				}

				if ((valorMtTerrenoP + this.areaTotalTerrenoUnidadF) > this.tramite.getPredio().getAreaTerreno()) {
					addMensajeError("El área ingresada no es valida. El valor máximo es de : "
							+ (this.tramite.getPredio().getAreaTerreno() - valorMtTerrenoP));
					return;
				}

				if (totalAreaProyectados != this.tramite.getPredio().getAreaTerreno()) {
					if ((this.areaTotalTerrenoUnidadF <= 0
							|| this.areaTotalTerrenoUnidadF >= this.tramite.getPredio().getAreaTerreno())
							&& !this.tramite.getPredio().isMejora()) {
						addMensajeError("El área ingresada no es valida.");
						return;
					}
				}

				try {

					if (this.avaluo.getId() == null) {
						this.avaluo = liquidacion.calcDesenglobeCondCero(this.areaTotalTerrenoUnidadF, this.tramite,
								this.predioSeleccionado, this.usuario, this.avaluo);

						List<PPredioAvaluoCatastral> listapavaluo = this.getConservacionService()
								.obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predioSeleccionado.getId());
						this.avaluo = listapavaluo.get(listapavaluo.size() - 1);
					}

					this.predioSeleccionado.setAreaTerreno(this.areaTotalTerrenoUnidadF);
					this.predioSeleccionado.setAvaluoCatastral(this.avaluo.getValorTotalAvaluoCatastral());

					if (this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {
						this.avaluo.setValorTotalConstruccion(0.0);
						this.avaluo.setValorTotalConsConvencional(0.0);
						this.avaluo.setValorTotalConsNconvencional(0.0);
						this.avaluo.setValorTotalConstruccionPrivada(0.0);
					} else {
						double totalConvencional = 0.0;
						double totalNoConvencional = 0.0;
						double totalprivada = 0.0;

						for (PUnidadConstruccion pUnidadConstruccion : predioSeleccionado.getPUnidadConstruccions()) {

							if (pUnidadConstruccion.getTipoConstruccion()
									.equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())
									&& !pUnidadConstruccion.getCancelaInscribe()
											.equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
								totalConvencional += pUnidadConstruccion.getAvaluo();
							}

							if (pUnidadConstruccion.getTipoConstruccion()
									.equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())
									&& !pUnidadConstruccion.getCancelaInscribe()
											.equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
								totalNoConvencional += pUnidadConstruccion.getAvaluo();
							}

							if (pUnidadConstruccion.getTipoConstruccion().equals(EUnidadTipoDominio.PRIVADO.getCodigo())
									&& !pUnidadConstruccion.getCancelaInscribe()
											.equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
								totalprivada += pUnidadConstruccion.getAvaluo();
							}
						}

						avaluo.setValorTotalConsConvencional(totalConvencional);
						avaluo.setValorTotalConsNconvencional(totalNoConvencional);
						avaluo.setValorTotalConstruccionPrivada(totalprivada);
						avaluo.setValorTotalConstruccion(totalConvencional + totalNoConvencional);
					}

					this.avaluo
							.setValorTerreno((double) Math.round(this.areaTotalTerrenoUnidadF * this.valorMetroFiscal));
					this.avaluo.setValorTotalAvaluoCatastral(
							this.avaluo.getValorTerreno() + this.avaluo.getValorTotalConstruccion());
					this.avaluo.setCancelaInscribe(this.predioSeleccionado.getCancelaInscribe());

					liquidacion.redondeoAvaluosPrediosFiscales(this.predioSeleccionado, this.avaluo);
					this.predioSeleccionado.setAvaluoCatastral(this.avaluo.getValorTotalAvaluoCatastral());

					PPredioAvaluoCatastral auxAval = this.getConservacionService()
							.actualizarPPredioAvaluoCatastral(this.avaluo);
					this.avaluo.setId(auxAval.getId());
					this.getConservacionService().actualizarPPredio(this.predioSeleccionado);
					this.addMensajeInfo("Detalle del avalúo almacenado de forma exitosa");
				} catch (Exception e) {
					LOGGER.error("error en ProyectarConservacionMB#guardarAvaluoPredioFiscal");
				}
			} else {
				this.avaluo = liquidacion.calcDesenglobeCondCero(this.areaTotalTerrenoUnidadF, this.tramite,
						this.predioSeleccionado, this.usuario, this.avaluo);
				if (this.avaluo != null) {
					valorMetroFiscal = (double) Math
							.round(this.avaluo.getValorTerreno() / this.areaTotalTerrenoUnidadF);
				}
			}
		}

		// SEGUNDA MEJORA
		if (this.tramite.isSegundaDesenglobe() && this.tramite.getPredio().isMejora()) {
			if (this.predioSeleccionado != null) {
				this.avaluo = liquidacion.calcDesenglobeMejora(this.areaTotalTerrenoUnidadF, this.tramite,
						this.predioSeleccionado, this.usuario, this.avaluo);
			}

		}

		// TERCERA
		if (this.tramite.isTercera()) {
			this.avaluo = liquidacion.calcMutacionTercera(this.valorMetroFiscal, this.tramite, this.predioSeleccionado,
					this.usuario, this.avaluo);
		}

		// ENGLOBE
		if (this.tramite.isSegundaEnglobe()) {

			if (this.avaluo.getValorTotalConstruccion() == null) {
				this.setAvaluo();
			}

			List<PUnidadConstruccion> unidades = this.predioSeleccionado.getPUnidadConstruccions();

			this.avaluo = liquidacion.calcAvaluoEnglobe(this.tramite, this.predioSeleccionado, this.usuario,
					this.avaluo);

			unidades = this.predioSeleccionado.getPUnidadConstruccions();

			for (PUnidadConstruccion unidad : unidades) {
				unidad.setValorM2Construccion(this.avaluo.getValorTotalConstruccion()
						/ Double.parseDouble(this.getAreaTotalConstruccion().replace("m2", "")));
				unidad.setAvaluo(liquidacion.trimDouble(unidad.getAreaConstruida() * unidad.getValorM2Construccion()));
				unidad.setTotalPuntaje(0.0);
				if (this.tramite.getPredio().isMejora()) {
					if (unidad.getAreaConstruida() == 0) {
						this.addMensajeError("Debe Cambiar el área de construcción de las unidades.");
						return;
					}
				}
				this.getConservacionService().actualizarPUnidadDeConstruccion(unidad);
			}
		}

		// Quinta Omitido
		if (this.tramite.isQuintaOmitido()) {

			if (this.tramite.isQuintaOmitidoNuevo()) {
				this.avaluo = liquidacion.calcAvaluoQuintaNuevo(this.areaTotalTerrenoUnidadF, this.tramite,
						this.predioSeleccionado, this.usuario, this.avaluo);
			} else {
				this.avaluo = liquidacion.calcAvaluoQuintaOmitido(this.areaTotalTerrenoUnidadF, this.tramite,
						this.predioSeleccionado, this.usuario, this.avaluo,
						this.avaluo.getFechaInscripcionCatastral().toString());

				// this.setAvaluo();
				// try {
				// PPredioAvaluoCatastral auxAval =
				// this.getConservacionService()
				// .actualizarPPredioAvaluoCatastral(this.avaluo);
				// this.avaluo.setId(auxAval.getId());
				// this.predioSeleccionado.setAvaluoCatastral(this.avaluo.getValorTotalAvaluoCatastral());
				// this.getConservacionService()
				// .actualizarPPredio(this.predioSeleccionado);
				// this.addMensajeInfo("Detalle del avalúo almacenado de forma exitosa");
				// } catch (Exception e) {
				// LOGGER.error("error en ProyectarConservacionMB#guardarGestionAvaluo");
				// }
			}
		}

		// Quinta nuevo
		if (this.tramite.isQuintaNuevo()) {
			this.avaluo = liquidacion.calcAvaluoQuintaNuevo(this.areaTotalTerrenoUnidadF, this.tramite,
					this.predioSeleccionado, this.usuario, this.avaluo);
		}

		// Rectificación área de terreno
		if (this.tramite.isRectificacionArea()) {

			double valorMtTerrenoP = 0.0;
			double totalAreaProyectados = 0.0;

			if (this.avaluo.getValorTotalConstruccion() == null) {
				this.setAvaluo();
			}

			this.avaluo = liquidacion.calcAvaluoRectificacionAreaTerreno(this.areaTotalTerrenoUnidadF, this.tramite,
					this.predioSeleccionado, this.usuario, this.avaluo);

			if (this.avaluo.getId() == null) {
				this.banderaAvaluoManualFiscales = true;
			}

			if (this.areaTotalTerrenoUnidadF == 0) {
				addMensajeError("El área ingresada no es válida. El valor debe ser mayor que " + "cero");
				return;
			}

			valorMtTerrenoP += predioSeleccionado.getAreaTerreno();
		}

		if (this.avaluo == null) {
			// this.setAvaluo();
			this.avaluo = new PPredioAvaluoCatastral();
		}
	}

	/**
	 * Retorna la instancia del MB GestionDetalleAvaluoMB
	 *
	 * @return
	 */
	private GestionDetalleAvaluoMB getGestionDetalleAvaluoMB() {
		return (GestionDetalleAvaluoMB) UtilidadesWeb.getManagedBean("gestionDetalleAvaluo");
	}

	// end of class
	public void cancelarPropietario() {

		if (this.tramite.isPrimera() && this.getPredioSeleccionado() != null
				&& this.getPredioSeleccionado().getPPersonaPredios() != null
				&& !this.getPredioSeleccionado().getPPersonaPredios().isEmpty()) {
			for (PPersonaPredio ppp : this.getPredioSeleccionado().getPPersonaPredios()) {
				if (ppp.getCancelaInscribe() == null
						|| ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo())
						|| ppp.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {

					// @modified by leidy.gonzalez::#12893:: Se ajusta eliminando el Propietario del
					// Predio seleccionado
					this.getConservacionService().eliminarPPersonaPredio(ppp);
					this.predioSeleccionado.getPPersonaPredios().remove(ppp);
					this.getPredioSeleccionado().getPPersonaPredios().remove(ppp);

				}
			}
		}
	}

	/*
	 * Se verifica si las construcciones que se crearon no quedaron con C/I en M
	 *
	 * @author leidy.gonzalez
	 */
	public void compararConstruccionesFirmeYProyectadas() {

		this.pUnidadesConstruccion = new ArrayList<PUnidadConstruccion>();
		this.unidadesConstruccion = new ArrayList<UnidadConstruccion>();
		List<Long> idPUnidadMod = new ArrayList<Long>();
		List<Long> idUnidaCons = new ArrayList<Long>();
		List<Long> idConstrExistente = new ArrayList<Long>();
		List<PUnidadConstruccion> pUnidadesExisteFirme = new ArrayList<PUnidadConstruccion>();

		if (this.predioSeleccionado != null) {

			if (this.predioSeleccionado.getPUnidadConstruccions() != null
					&& !this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {

				for (PUnidadConstruccion consEnModifica : this.predioSeleccionado.getPUnidadConstruccions()) {

					if (EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(consEnModifica.getCancelaInscribe())) {
						this.pUnidadesConstruccion.add(consEnModifica);
						idPUnidadMod.add(consEnModifica.getId());
					}

				}
			}
		}

		// Consulta Unidades en Firme
		if (idPUnidadMod != null && !idPUnidadMod.isEmpty()) {
			this.unidadesConstruccion = this.getConservacionService()
					.obtenerUnidadesConstruccionPorListaId(idPUnidadMod);
		}

		// Verifica si existen Unidades de Construccion en firme
		if (this.unidadesConstruccion != null && !this.unidadesConstruccion.isEmpty()) {

			for (UnidadConstruccion unCons : this.unidadesConstruccion) {
				idUnidaCons.add(unCons.getId());
			}

			if (idUnidaCons != null && !idUnidaCons.isEmpty()) {
				// Si existe se compara con las proyectadas
				for (Long idPUnidad : idPUnidadMod) {
					if (!idUnidaCons.contains(idPUnidad)) {
						idConstrExistente.add(idPUnidad);

					}
				}

				if (idConstrExistente != null && !idConstrExistente.isEmpty()) {
					pUnidadesExisteFirme = this.getConservacionService()
							.obtenerPUnidadConstruccionPorIds(idConstrExistente);
				}
			}

		} else {

			for (PUnidadConstruccion pUn : this.pUnidadesConstruccion) {
				pUn.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
				this.getConservacionService().actualizarPUnidadDeConstruccion(pUn);
			}
		}

		// Verifica Unidades existen en Firme y Cmbia el Cancela Inscribe a I
		if (pUnidadesExisteFirme != null && !pUnidadesExisteFirme.isEmpty()) {
			for (PUnidadConstruccion pUnEx : pUnidadesExisteFirme) {
				pUnEx.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
				pUnEx = this.getConservacionService().actualizarPUnidadDeConstruccion(pUnEx);
			}
		}
	}

	public void compararZonasFirmeYProyectadas() {

		List<PPredioZona> pPredioZona = new ArrayList<PPredioZona>();
		List<PredioZona> predioZona = new ArrayList<PredioZona>();
		List<Long> idPZonaMod = new ArrayList<Long>();
		List<Long> idZonaMod = new ArrayList<Long>();
		List<Long> idZonaExistente = new ArrayList<Long>();
		List<PPredioZona> pPredioZonaExisteFirme = new ArrayList<PPredioZona>();

		if (this.pPrediosDesenglobe != null && !this.pPrediosDesenglobe.isEmpty()) {

			for (PPredio pPredio : this.pPrediosDesenglobe) {

				if (pPredio.getPPredioZonas() != null && !pPredio.getPPredioZonas().isEmpty()) {

					for (PPredioZona zonEnModifica : pPredio.getPPredioZonas()) {

						if (EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(zonEnModifica.getCancelaInscribe())
								|| zonEnModifica.getCancelaInscribe() == null) {

							pPredioZona.add(zonEnModifica);
							idPZonaMod.add(zonEnModifica.getId());
						}

					}
				}

			}
		}

		// Consulta Unidades en Firme
		if (idPZonaMod != null && !idPZonaMod.isEmpty()) {
			predioZona = this.getConservacionService().obtenerZonasPorIds(idPZonaMod);
		}

		// Verifica si existen Unidades de Construccion en firme
		if (predioZona != null && !predioZona.isEmpty()) {

			for (PredioZona pZon : predioZona) {
				idZonaMod.add(pZon.getId());
			}

			if (idZonaMod != null && !idZonaMod.isEmpty()) {
				// Si existe se compara con las proyectadas
				for (Long idPZona : idPZonaMod) {
					if (!idZonaMod.contains(idPZona)) {
						idZonaExistente.add(idPZona);

					}
				}

				if (idZonaExistente != null && !idZonaExistente.isEmpty()) {
					pPredioZonaExisteFirme = this.getConservacionService().obtenerPZonasPorIds(idZonaExistente);
				}
			}

		} else {

			for (PPredioZona pZon : pPredioZona) {
				pZon.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
				this.getConservacionService().guardarActualizarZona(pZon);
			}
		}

		// Verifica Unidades existen en Firme y Cmbia el Cancela Inscribe a I
		if (pPredioZonaExisteFirme != null && !pPredioZonaExisteFirme.isEmpty()) {
			for (PPredioZona pZonEx : pPredioZonaExisteFirme) {
				pZonEx.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
				this.getConservacionService().guardarActualizarZona(pZonEx);
			}
		}
	}

	// ---------------------------------------------------------- //
	/**
	 * Método que asocia las construcciones del predio base al predio * seleccionado
	 * para NPHs
	 *
	 * @author david.cifuentes
	 */
	public void asociarConstruccionesDesenglobeCondicionCero() {

		try {
			int cantidadConvencional = 0, cantidadNoConvencional = 0, cantidadUnidades;

			// LLamado al método que realiza el set de las unidades de
			// construcción seleccionadas del predio original
			this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = unidadesConstruccionConvencionalSeleccionadas;
			this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = unidadesConstruccionNoConvencionalSeleccionadas;

			if (this.unidadConstruccionConvencionalPredioOriginalSeleccionadas != null
					&& this.unidadConstruccionConvencionalPredioOriginalSeleccionadas.length > 0) {
				cantidadConvencional = this.unidadConstruccionConvencionalPredioOriginalSeleccionadas.length;
			}
			if (this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas != null
					&& this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas.length > 0) {
				cantidadNoConvencional = this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas.length;
			}

			cantidadUnidades = cantidadNoConvencional + cantidadConvencional;

			if (cantidadUnidades < 1) {
				this.addMensajeWarn("Por favor seleccione las unidades a asociar.");
				return;
			} else {
				this.messageError = false;
				if (cantidadNoConvencional > 0) {
					asociarConstruccionesNoConvencionalesPredioOriginal();
				}
				if (cantidadConvencional > 0) {
					asociarConstruccionesConvencionalesPredioOriginal();
				}
				if (this.messageError) {
					return;
				}
			}

			this.cargueUnidadesDeConstruccionNPH();

			this.verificarCIFotosDeUnidadesConstruccion();

			if (this.predioSeleccionado != null) {
				this.predioSeleccionado = this.getConservacionService()
						.obtenerPPredioCompletoById(this.predioSeleccionado.getId());
			}

			this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = null;
			this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = null;
			this.unidadesConstruccionConvencionalSeleccionadas = null;
			this.unidadesConstruccionNoConvencionalSeleccionadas = null;

		} catch (Exception e) {
			this.addMensajeError("Error al intentar asociar las unidades al predio seleccionado.");
			LOGGER.debug(e.getMessage());
		}

	}

	// ------------------------------------------------------------- //
	/**
	 * Método que carga las unidades seleccionadas de la tabla de unidades de
	 * construcciónn del predio origen.
	 *
	 * @author david.cifuentes
	 */
	public void cargarUnidadesSeleccionadasPredioOriginal() {

		List<UnidadConstruccion> aux;
		this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = null;
		this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = null;

		if (this.unidadConstruccionConvencionalPredioOriginal != null
				&& !this.unidadConstruccionConvencionalPredioOriginal.isEmpty()) {

			aux = new ArrayList<UnidadConstruccion>();
			for (UnidadConstruccion uccv : this.unidadConstruccionConvencionalPredioOriginal) {
				if (uccv.isSelected()) {
					aux.add(uccv);
				}
			}
			if (aux.size() > 0) {
				this.unidadConstruccionConvencionalPredioOriginalSeleccionadas = new UnidadConstruccion[aux.size()];
				for (int i = 0; i < aux.size(); i++) {
					this.unidadConstruccionConvencionalPredioOriginalSeleccionadas[i] = aux.get(i);
				}
			}
		}
		if (this.unidadConstruccionNoConvencionalPredioOriginal != null
				&& !this.unidadConstruccionNoConvencionalPredioOriginal.isEmpty()) {

			aux = new ArrayList<UnidadConstruccion>();
			for (UnidadConstruccion ucncv : this.unidadConstruccionNoConvencionalPredioOriginal) {
				if (ucncv.isSelected()) {
					aux.add(ucncv);
				}
			}
			if (aux.size() > 0) {
				this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas = new UnidadConstruccion[aux.size()];
				for (int i = 0; i < aux.size(); i++) {
					this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas[i] = aux.get(i);
				}
			}
		}
	}

	// ------------------------------------------------------------- //
	/**
	 * Método que asocia las construcciones convecionales del predio original a las
	 * nuevas construcciones.
	 *
	 * @author david.cifuentes
	 */
	public void asociarConstruccionesConvencionalesPredioOriginal() {
		if (unidadConstruccionConvencionalNuevas == null) {
			unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
		}
		this.asociarConstruccionesPredioOriginal(this.unidadConstruccionConvencionalPredioOriginalSeleccionadas,
				this.unidadConstruccionConvencionalNuevas);
	}

	// ------------------------------------------------------------- //
	/**
	 * Método que asocia las construcciones no convencionales del predio original a
	 * las nuevas construcciones.
	 *
	 * @author david.cifuentes
	 */
	public void asociarConstruccionesNoConvencionalesPredioOriginal() {
		if (unidadConstruccionNoConvencionalNuevas == null) {
			unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
		}
		this.asociarConstruccionesPredioOriginal(this.unidadConstruccionNoConvencionalPredioOriginalSeleccionadas,
				this.unidadConstruccionNoConvencionalNuevas);
	}

	// ------------------------------------------------------------- //
	/**
	 * Método que asocia las construcciones originales del predio que han sido
	 * enviadas como parámetro a las nuevas construcciones.
	 *
	 * @author david.cifuentes
	 */
	public void asociarConstruccionesPredioOriginal(UnidadConstruccion[] unidadConstruccionSeleccionadas,
			List<PUnidadConstruccion> pUnidadConstruccionNuevas) {

		if (unidadConstruccionSeleccionadas.length <= 0) {
			this.addMensajeWarn("Debe seleccionar al menos una unidad de construccion a asociar");
			this.messageError = true;
		} else {
			try {

				GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");

				List<PUnidadConstruccion> unidadesAAsociar = new ArrayList<PUnidadConstruccion>();

				// Consulta del predio base con sus construcciones.
				PPredio pPredioBase = this.getConservacionService()
						.obtenerPPredioCompletoById(this.tramite.getPredio().getId());

				// Consulta del predio asociado al ppredio
				Predio predioBase = this.getConservacionService().obtenerPredioPorId(this.tramite.getPredio().getId());

				if (this.predioSeleccionado != null && pPredioBase != null) {

					// Únicamente se puede asociar la misma unidad origen al predio
					if (unidadConstruccionSeleccionadas != null
							&& !this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {

						List<String> unidadesAsociadasPredio = new ArrayList<String>();
						for (PUnidadConstruccion puc : this.predioSeleccionado.getPUnidadConstruccions()) {
							if (puc.getProvieneUnidad() != null) {
								if (!unidadesAsociadasPredio.contains(puc.getProvieneUnidad())) {
									unidadesAsociadasPredio.add(puc.getProvieneUnidad());
								}
							}
						}
						for (UnidadConstruccion uc : unidadConstruccionSeleccionadas) {
							if (unidadesAsociadasPredio.contains(uc.getUnidad())) {
								this.addMensajeError("Sólo se puede asociar una vez la unidad original "
										+ uc.getUnidad() + " al predio actual.");
								this.messageError = true;
								return;
							}
						}
					}

					// Obtener una copia del arreglo de construcciones
					// seleccionadas, para posteriormente modificarlas.
					if (unidadConstruccionSeleccionadas != null && unidadConstruccionSeleccionadas.length > 0) {
						for (UnidadConstruccion uc : unidadConstruccionSeleccionadas) {
							unidadesAAsociar.add(generalMB.copiarUnidadConstruccionAPUnidadConstruccion(uc));
						}
					}

					// Actualizar las unidades convencionales antes de
					// asociarselas al predio seleccionado.
					if (unidadesAAsociar.size() > 0) {

						// Actualización de unidades convencionales
						for (PUnidadConstruccion ucAsociar : unidadesAAsociar) {

							List<PUnidadConstruccion> auxUnidadesAsociar = new ArrayList<PUnidadConstruccion>();
							ucAsociar.setPPredio(predioSeleccionado);
							ucAsociar.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
							ucAsociar.setUsuarioLog(this.usuario.getLogin());
							ucAsociar.setFechaLog(new Date());
							ucAsociar.setId(null);
							ucAsociar.setProvienePredio(predioBase);
							ucAsociar.setProvieneUnidad(ucAsociar.getUnidad());
							ucAsociar.setUnidad(this.calcularUnidadConstruccionUnidad());

							// El área se asocia con un valor de cero para
							// ser modificada según el área que se quiera
							// asignar como común
							ucAsociar.setAreaConstruida(new Double(0));

							auxUnidadesAsociar.add(ucAsociar);
							auxUnidadesAsociar = this.getConservacionService()
									.guardarListaPUnidadConstruccion(auxUnidadesAsociar);

							if (auxUnidadesAsociar != null) {
								ucAsociar.setId(auxUnidadesAsociar.get(0).getId());
							}
						}

						if (unidadesAAsociar != null) {
							// Actualización de los componentes de las
							// unidades actualizadas para que tengan la
							// misma unidad
							// de construccion padre.
							for (PUnidadConstruccion puc : unidadesAAsociar) {
								if (puc.getPUnidadConstruccionComps() != null
										&& !puc.getPUnidadConstruccionComps().isEmpty()) {
									for (PUnidadConstruccionComp pucc : puc.getPUnidadConstruccionComps()) {
										pucc.setPUnidadConstruccion(puc);
									}
								}
							}
							// Actualizar la unidad de construcción con los
							// componentes actualizados.
							unidadesAAsociar = this.getConservacionService()
									.guardarListaPUnidadConstruccion(unidadesAAsociar);
						}
					}
				}

			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				this.addMensajeInfo("Error actualizando las áreas en la ficha matriz.");
			}

		}
	}

	// ------------------------------------------------------------- //
	/**
	 * Método que inicializa las unidades de construcción a mostrar
	 *
	 * @author david.cifuentes
	 */
	@SuppressWarnings("unchecked")
	public void cargueUnidadesDeConstruccionNPH() {

		// Inicializar variables de construcciones
		this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
		this.unidadConstruccionNoConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
		this.unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
		this.unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
		this.unidadConstruccionConvencionalVigentes = new ArrayList<PUnidadConstruccion>();
		this.unidadConstruccionNoConvencionalVigentes = new ArrayList<PUnidadConstruccion>();

		try {
			// Consulta del predio base con sus construcciones
			// Cargue de las construcciones vigentes
			PPredio pPredioBase = this.getConservacionService()
					.obtenerPPredioCompletoById(this.tramite.getPredio().getId());

			if (pPredioBase != null && pPredioBase.getPUnidadConstruccions() != null
					&& !pPredioBase.getPUnidadConstruccions().isEmpty()) {

				// Construcciones convencionales
				if (pPredioBase.getPUnidadesConstruccionConvencional() != null
						&& !pPredioBase.getPUnidadesConstruccionConvencional().isEmpty()) {
					for (PUnidadConstruccion puc : pPredioBase.getPUnidadesConstruccionConvencional()) {
						if (puc.getProvienePredio() == null
								&& !EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(puc.getCancelaInscribe())
								&& !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.getCancelaInscribe())) {

							this.unidadConstruccionConvencionalVigentes.add(puc);
						}
					}
				}
				// Construcciones no convencionales
				if (pPredioBase.getPUnidadesConstruccionNoConvencional() != null
						&& !pPredioBase.getPUnidadesConstruccionNoConvencional().isEmpty()) {
					for (PUnidadConstruccion puc : pPredioBase.getPUnidadesConstruccionNoConvencional()) {
						if (puc.getProvienePredio() == null
								&& !EProyeccionCancelaInscribe.INSCRIBE.getCodigo().equals(puc.getCancelaInscribe())
								&& !EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(puc.getCancelaInscribe())) {

							this.unidadConstruccionNoConvencionalVigentes.add(puc);
						}
					}
				}
			}

			this.predioSeleccionado = this.getConservacionService()
					.obtenerPPredioCompletoById(this.predioSeleccionado.getId());

			if (this.predioSeleccionado.getPUnidadConstruccions() != null
					&& !this.predioSeleccionado.getPUnidadConstruccions().isEmpty()) {

				if (this.predioSeleccionado.getPUnidadesConstruccionConvencional() != null
						&& !this.predioSeleccionado.getPUnidadesConstruccionConvencional().isEmpty()) {

					for (PUnidadConstruccion pucConv : this.predioSeleccionado.getPUnidadesConstruccionConvencional()) {

						if (pucConv.getCancelaInscribe() != null
								&& (pucConv.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())
										|| pucConv.getCancelaInscribe()
												.equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {

							this.unidadConstruccionConvencionalNuevas.add(pucConv);
						}
					}
				}
				if (this.predioSeleccionado.getPUnidadesConstruccionNoConvencional() != null
						&& !this.predioSeleccionado.getPUnidadesConstruccionNoConvencional().isEmpty()) {

					for (PUnidadConstruccion pucNoConv : this.predioSeleccionado
							.getPUnidadesConstruccionNoConvencional()) {
						if (pucNoConv.getCancelaInscribe() != null && (pucNoConv.getCancelaInscribe()
								.equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())
								|| pucNoConv.getCancelaInscribe()
										.equals(EProyeccionCancelaInscribe.MODIFICA.getCodigo()))) {

							this.unidadConstruccionNoConvencionalNuevas.add(pucNoConv);
						}
					}
				}

				// Debido a que no se tiene la manera explicita de saber si
				// una unidad es la misma que la del predio base, se realiza
				// un ordenamiento de las unidades proyectadas y se toma que
				// las primeras unidades son las del predio base, teniendo
				// encuenta la cantidad de unidades inicial.
				int cantidadUnidadesPredioBase;
				int i = 0;

				// Cargue de los id de las unidades construcción del predio
				// base
				List<UnidadConstruccion> unidadConstruccionPredioBase = this.getConservacionService()
						.obtenerUnidadesConstruccionsPorNumeroPredial(this.tramite.getPredio().getNumeroPredial());

				if (unidadConstruccionPredioBase != null) {

					cantidadUnidadesPredioBase = unidadConstruccionPredioBase.size();
					Collections.sort(this.predioSeleccionado.getPUnidadConstruccions());
					for (PUnidadConstruccion pu : this.predioSeleccionado.getPUnidadConstruccions()) {
						pu.setPertenecePredioProyectado(true);
						i++;
						if (i == cantidadUnidadesPredioBase)
							break;
					}
				}
			}

			this.updateConstruccionesNuevasNoCanceladas();
			cargarConstruccionesAsociadasPredioOrigen();

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	// ------------------------------------------------------------- //
	/**
	 * Método que actualiza las listas de unidades de construcción nuevas - no
	 * canceladas
	 *
	 * @author david.cifuentes
	 */
	private void updateConstruccionesNuevasNoCanceladas() {

		this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
		for (PUnidadConstruccion puc : this.unidadConstruccionConvencionalNuevas) {
			if (puc.getCancelaInscribe() == null
					|| !puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

				if (puc.getProvienePredio() != null || this.tramite.isTerceraMasiva()
						|| this.tramite.isRectificacionMatriz()
						|| ESiNo.SI.getCodigo().equals(puc.getOriginalTramite())) {
					puc.setPertenecePredioProyectado(true);
				}
				this.unidadConstruccionConvencionalNuevasNoCanceladas.add(puc);

			}
		}

		this.unidadConstruccionNoConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
		for (PUnidadConstruccion puc : this.unidadConstruccionNoConvencionalNuevas) {
			if (puc.getCancelaInscribe() == null
					|| !puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.getCodigo())) {

				if (puc.getProvienePredio() != null || this.tramite.isTerceraMasiva()
						|| this.tramite.isRectificacionMatriz()
						|| ESiNo.SI.getCodigo().equals(puc.getOriginalTramite())) {
					puc.setPertenecePredioProyectado(true);
				}
				this.unidadConstruccionNoConvencionalNuevasNoCanceladas.add(puc);
			}
		}

	}

	// end of class
}
