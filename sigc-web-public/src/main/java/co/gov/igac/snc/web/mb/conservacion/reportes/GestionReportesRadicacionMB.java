package co.gov.igac.snc.web.mb.conservacion.reportes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReportesRadicacionesDinamicosSNC;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionDetalle;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.util.EConservacionSubproceso;
import co.gov.igac.snc.persistence.util.EConservacionSubprocesoActividad;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.persistence.util.ERepReporteConsultaEspecialParametrizacion;
import co.gov.igac.snc.persistence.util.ERepReporteEjecucionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaReportesRadicacion;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.jobs.ConsultarReportesBDRunnableJob;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.multipdf.Splitter;
import java.util.Iterator;

/**
 * @description Managed bean para gestionar la consulta y generación de diferentes tipos de reportes
 * de las radicaciones.
 *
 * @cu CU-NP-CO-254 Generar reporte de radicaciones
 *
 * @version 1.0
 *
 * @author david.cifuentes
 */
@Component("gestionReporteRadicacion")
@Scope("session")
public class GestionReportesRadicacionMB extends SNCManagedBean implements
    Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -2005846748490563205L;
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionReportesRadicacionMB.class);
    /**
     * ----------------------------------
     */
    /**
     * ----------- SERVICIOS ------------
     */
    /**
     * ----------------------------------
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    private ReportesUtil reportsService = ReportesUtil.getInstance();
    /**
     * ----------------------------------
     */
    /**
     * ----------- CONSTANTES -----------
     */
    /**
     * ----------------------------------
     */
    private static final BigDecimal TAMANO_MAX_DESCARGAS = new BigDecimal(2048);
    /**
     * ----------------------------------
     */
    /**
     * ----------- VARIABLES ------------
     */
    /**
     * ----------------------------------
     */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;
    /**
     * Lista que almacena los reportes que han sido generados y cumplen con los criterios de
     * búsqueda
     */
    private List<RepReporteEjecucion> reportesRadicacionGenerados;
    /**
     * Variable que almacena la parametrización del usuario la cual es usada para armar los menus de
     * consulta
     */
    private List<RepUsuarioRolReporte> parametrizacionUsuario;
    /**
     * Variable usada para capturar el código de una territorial seleccionada de la lista de
     * territoriales.
     */
    private String territorialSeleccionadaCodigo;
    /**
     * Variable que almacena la territorial seleccionada.
     */
    private EstructuraOrganizacional territorialSeleccionada;
    /**
     * Lista de unidades operativas de catastro
     */
    private List<EstructuraOrganizacional> unidadesOperativasCatastroObj;
    /**
     * Lista de select items de las UOC.
     */
    private List<SelectItem> unidadesOperativasCatastroItemList;
    /**
     * Lista de select items de las territoriales
     */
    private ArrayList<SelectItem> territorialesItemList;
    /**
     * Variable usada para capturar el código de una UOC seleccionada de la lista de UOCs de la
     * territorial.
     */
    private String unidadOperativaSeleccionadaCodigo;
    /**
     * Variable que almacena la unidad operativa seleccionada.
     */
    private EstructuraOrganizacional unidadOperativaSeleccionada;
    /**
     * Variable usada para almacenar el còdigo del tipo de reporte seleccionado
     */
    private Long tipoReporteId;
    /**
     * Lista de select items de los tipos de reportes
     */
    private List<SelectItem> tipoReportesItemList;
    /**
     * Variable usada para almacenar el reporte seleccionado
     */
    private RepReporte repReporteSeleccionado;
    /**
     * Variable que almacena el formato seleccionado para la busqueda
     */
    private String formatoSeleccionado;
    /**
     * Variable que almacena el código del departamento seleccionado
     */
    private String departamentoSeleccionadoCodigo;
    /**
     * Lista de select items de los departamentos parametrizados
     */
    private List<SelectItem> departamentosItemList;
    /**
     * Variable que almacena el código del municipio seleccionado
     */
    private String municipioSeleccionadoCodigo;
    /**
     * Lista de select items de los municipios parametrizados
     */
    private List<SelectItem> municipiosItemList;
    /**
     * Fechas de inicio y fin para la generación o consulta del informe
     */
    private Date fechaInicio;
    private Date fechaFin;
    private Date minDate;
    private Date maxDate;
    /**
     * Variable que almacena el login del funcionario seleccionado
     */
    private String rolSeleccionado;
    /**
     * Lista de select items de los funcionarios parametrizados
     */
    private List<SelectItem> rolesItemList;
    /**
     * Variable que almacena el login del funcionario seleccionado
     */
    private String funcionarioSeleccionado;
    /**
     * Lista de select items de los funcionarios parametrizados
     */
    private List<SelectItem> funcionariosItemList;
    /**
     * Variable que almacena el nombre del subproceso seleccionado
     */
    private EConservacionSubproceso subprocesoSeleccionado;
    /**
     * Lista de select items de los subprocesos de Conservación
     */
    private List<SelectItem> subprocesosItemList;
    /**
     * Variable que almacena el nombre de la actividad seleccionada
     */
    private EConservacionSubprocesoActividad actividadSeleccionada;
    /**
     * Lista de select items de las actividades del subproceso de conservación seleccionado
     */
    private List<SelectItem> actividadesItemList;
    /**
     * Variable que almacena el nombre de la clasificación seleccionada
     */
    private String clasificacionSeleccionada;
    /**
     * Lista de select items de las clasificaciones de un trámite
     */
    private List<SelectItem> clasificacionesItemList;
    /**
     * Variable que almacena el rango de los días transcurridos al realizar la consulta o generación
     * de reportes de radicación.
     */
    private String rangoDiasTranscurridosSeleccionado;
    /**
     * Variable usada para almacenar el código único del reporte
     */
    private String codigoReporte;
    /**
     * Variables usadas para almacenar el año y consecutivo del reporte
     */
    private String anioReporteCod;
    private String codigoReporteCod;
    /**
     * Variable utilizada para habilitar la opción de generar reporte de radicación
     */
    private boolean habilitarGenerarReporteBool;
    /**
     * Variable que tiene los campos de consulta de la parametrización
     */
    private FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion;
    /**
     * Variable para seleccionar todos los reportes que se encuentran en la table de resultados de
     * búsqueda
     */
    private boolean seleccionarTodosBool;
    /**
     * Variables para habilitar la busqueda y generación de reportes según la parametrización del
     * usuario.
     */
    private boolean permisoBuscarReporteBool;
    private boolean permisoGenerarReporteBool;
    /**
     * Variable que almacena el reporte seleccionado a visualizar
     */
    private RepReporteEjecucion reporteVisualizacionSeleccionado;
    /**
     * Variable para setear el Documento del reporte seleccionado para su visualización
     */
    private String rutaPrevisualizacionReporte;
    /**
     * Variable de tipo streamed content que contiene el archivo con los reportes seleccionados para
     * la descarga.
     */
    private StreamedContent reportesDescarga;
    /**
     * Variable que almacena el histórico de las previsualizaciones vistas por el usuario.
     */
    private Map<Long, String> listaRutasPrevisualizacionesReportes;
    /**
     * Variable que almacena la información del reporte a generar.
     */
    private RepReporteEjecucion repReporteEjecucion;
    /**
     * Reporte a ser visualizado con las caracteristicas ingresadas por el usuario
     */
    private RepReporteEjecucionDetalle repReporteEjecucionDetalle;
    /**
     * Variable usada para la consulta de jobs en la base de datos
     */
    private ConsultarReportesBDRunnableJob consultarReportesBDRunnableJob;
    /**
     * Variable usada para habilitar o deshabilitar campos de consulta según el tipo de reporte
     * seleccionado
     */
    private boolean deshabilitarCamposConsultaBool;
    /**
     * Variable usada para la consulta de jobs en la base de datos
     */
    private String estructuraOrganizacionalCod;
    /**
     * Variable usada para almacenar los Usuarios asociados a los reportes.
     */
    private RepReporteEjecucionUsuario repReporteEjecucionUsuario;
    /**
     * Variable usada para habilitar la asociacion de un reporte a un usuario
     */
    private boolean habilitaAsociarReporteUsuario;
    /**
     * Variable usada para almacenar los usuarios asociados a un reporte
     */
    private List<RepReporteEjecucionUsuario> reportesEjecucionUsuarios;
    /**
     * Variable usada para verificar el tipo de formato a visualizar
     */
    private List<SelectItem> repFormatosDescarga;
    private boolean deshabilitaCampos;
    /**
     * Determina si se habilita o no la opciones Exportar
     */
    private boolean banderaExportarReporte;

    /**
     * Determina si se habilita o no la opciones Actualizar
     */
    private boolean banderaActualizarReporte;

    /**
     * Determina los reportes seleccionados por el usuario
     */
    private RepReporteEjecucion[] reportesSeleccionados;

    /**
     * ----------------------------------
     */
    /**
     * -------------- INIT --------------
     */
    /**
     * ----------------------------------
     */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - GestionReportesRadicacionesMB");

        try {
            this.usuario = MenuMB.getMenu().getUsuarioDto();

            this.estructuraOrganizacionalCod = this.usuario.getCodigoEstructuraOrganizacional();

            // Consulta de parametrización del usuario
            this.parametrizacionUsuario = this.getConservacionService()
                .buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
                    this.usuario, ERepReporteCategoria.REPORTES_RADICACION.getCategoria());

            // Se ordena la lista de parametrización del usuario
            Collections.sort(this.parametrizacionUsuario, new Comparator<RepUsuarioRolReporte>() {
                @Override
                public int compare(RepUsuarioRolReporte rurr1, RepUsuarioRolReporte rurr2) {
                    return rurr1.getRepReporte().getNombre().compareTo(rurr2.getRepReporte().
                        getNombre());
                }
            });

            // Inicialización de variables
            this.territorialesItemList = new ArrayList<SelectItem>();
            this.unidadesOperativasCatastroItemList = new ArrayList<SelectItem>();
            this.departamentosItemList = new ArrayList<SelectItem>();
            this.municipiosItemList = new ArrayList<SelectItem>();
            this.tipoReportesItemList = new ArrayList<SelectItem>();
            this.rolesItemList = new ArrayList<SelectItem>();
            this.funcionariosItemList = new ArrayList<SelectItem>();
            this.habilitaAsociarReporteUsuario = false;
            this.repReporteEjecucionUsuario = new RepReporteEjecucionUsuario();
            this.reportesEjecucionUsuarios = new ArrayList<RepReporteEjecucionUsuario>();
            this.repFormatosDescarga = new ArrayList<SelectItem>();
            this.deshabilitaCampos = true;
            this.banderaExportarReporte = false;
            this.banderaActualizarReporte = false;

            // Cargue de menus
            this.cargarTiposDeReporte();
            this.cargarTerritoriales();
            this.cargarUOC();
            this.cargarSubprocesosConservacion();
            this.cargarClasificacion();

            // Inicialización de una fecha mínima y máxima para el intervalo de consulta
            Calendar hoy = Calendar.getInstance();
            hoy.add(Calendar.DAY_OF_YEAR, -1);
            this.maxDate = hoy.getTime();

            SimpleDateFormat sm = new SimpleDateFormat("dd-mm-yyyy");
            this.minDate = sm.parse("01-01-2014");

            // Aunque la búsqueda se establecio que estaría habilitada se crea
            // la variable para que consuma las tablas de parametrización si se
            // quiere.
            this.permisoBuscarReporteBool = true;

            // Lista de previsualizaciones consultadas
            this.listaRutasPrevisualizacionesReportes = new HashMap<Long, String>();
            this.reportesSeleccionados = new RepReporteEjecucion[]{};
            this.reportesRadicacionGenerados = new ArrayList<RepReporteEjecucion>();
            this.consultarReportesBDRunnableJob = new ConsultarReportesBDRunnableJob();
            this.codigoReporte = null;
            this.codigoReporteCod = null;
            this.anioReporteCod = null;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en el cargue de la parametrización del usuario.");
        }
    }

    /**
     * ----------------------------------
     */
    /**
     * ------- GETTERS Y SETTERS --------
     */
    /**
     * ----------------------------------
     */
    public RepReporteEjecucion[] getReportesSeleccionados() {
        return reportesSeleccionados;
    }

    public void setReportesSeleccionados(RepReporteEjecucion[] reportesSeleccionados) {
        this.reportesSeleccionados = reportesSeleccionados;
    }

    public boolean isBanderaActualizarReporte() {
        return banderaActualizarReporte;
    }

    public void setBanderaActualizarReporte(boolean banderaActualizarReporte) {
        this.banderaActualizarReporte = banderaActualizarReporte;
    }

    public boolean isBanderaExportarReporte() {
        return banderaExportarReporte;
    }

    public void setBanderaExportarReporte(boolean banderaExportarReporte) {
        this.banderaExportarReporte = banderaExportarReporte;
    }

    public boolean isDeshabilitaCampos() {
        return deshabilitaCampos;
    }

    public void setDeshabilitaCampos(boolean deshabilitaCampos) {
        this.deshabilitaCampos = deshabilitaCampos;
    }

    public RepReporteEjecucionUsuario getRepReporteEjecucionUsuario() {
        return repReporteEjecucionUsuario;
    }

    public List<SelectItem> getRepFormatosDescarga() {
        return repFormatosDescarga;
    }

    public void setRepFormatosDescarga(List<SelectItem> repFormatosDescarga) {
        this.repFormatosDescarga = repFormatosDescarga;
    }

    public boolean isHabilitaAsociarReporteUsuario() {
        return habilitaAsociarReporteUsuario;
    }

    public void setHabilitaAsociarReporteUsuario(
        boolean habilitaAsociarReporteUsuario) {
        this.habilitaAsociarReporteUsuario = habilitaAsociarReporteUsuario;
    }

    public void setRepReporteEjecucionUsuario(
        RepReporteEjecucionUsuario repReporteEjecucionUsuario) {
        this.repReporteEjecucionUsuario = repReporteEjecucionUsuario;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<RepUsuarioRolReporte> getParametrizacionUsuario() {
        return parametrizacionUsuario;
    }

    public void setParametrizacionUsuario(
        List<RepUsuarioRolReporte> parametrizacionUsuario) {
        this.parametrizacionUsuario = parametrizacionUsuario;
    }

    public String getAnioReporteCod() {
        return anioReporteCod;
    }

    public void setAnioReporteCod(String anioReporteCod) {
        this.anioReporteCod = anioReporteCod;
    }

    public String getCodigoReporteCod() {
        return codigoReporteCod;
    }

    public void setCodigoReporteCod(String codigoReporteCod) {
        this.codigoReporteCod = codigoReporteCod;
    }

    public Long getTipoReporteId() {
        return tipoReporteId;
    }

    public void setTipoReporteId(Long tipoReporteId) {
        this.tipoReporteId = tipoReporteId;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public EstructuraOrganizacional getTerritorialSeleccionada() {
        return territorialSeleccionada;
    }

    public void setTerritorialSeleccionada(
        EstructuraOrganizacional territorialSeleccionada) {
        this.territorialSeleccionada = territorialSeleccionada;
    }

    public RepReporteEjecucionDetalle getRepReporteEjecucionDetalle() {
        return repReporteEjecucionDetalle;
    }

    public void setRepReporteEjecucionDetalle(
        RepReporteEjecucionDetalle repReporteEjecucionDetalle) {
        this.repReporteEjecucionDetalle = repReporteEjecucionDetalle;
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public List<RepReporteEjecucion> getReportesRadicacionGenerados() {
        return reportesRadicacionGenerados;
    }

    public void setReportesRadicacionGenerados(
        List<RepReporteEjecucion> reportesRadicacionGenerados) {
        this.reportesRadicacionGenerados = reportesRadicacionGenerados;
    }

    public boolean isDeshabilitarCamposConsultaBool() {
        return deshabilitarCamposConsultaBool;
    }

    public void setDeshabilitarCamposConsultaBool(
        boolean deshabilitarCamposConsultaBool) {
        this.deshabilitarCamposConsultaBool = deshabilitarCamposConsultaBool;
    }

    public String getRutaPrevisualizacionReporte() {
        return rutaPrevisualizacionReporte;
    }

    public void setRutaPrevisualizacionReporte(String rutaPrevisualizacionReporte) {
        this.rutaPrevisualizacionReporte = rutaPrevisualizacionReporte;
    }

    public Map<Long, String> getListaRutasPrevisualizacionesReportes() {
        return listaRutasPrevisualizacionesReportes;
    }

    public void setListaRutasPrevisualizacionesReportes(
        Map<Long, String> listaRutasPrevisualizacionesReportes) {
        this.listaRutasPrevisualizacionesReportes = listaRutasPrevisualizacionesReportes;
    }

    public boolean isSeleccionarTodosBool() {
        return seleccionarTodosBool;
    }

    public void setSeleccionarTodosBool(boolean seleccionarTodosBool) {
        this.seleccionarTodosBool = seleccionarTodosBool;
    }

    public StreamedContent getReportesDescarga() {
        return reportesDescarga;
    }

    public void setReportesDescarga(StreamedContent reportesDescarga) {
        this.reportesDescarga = reportesDescarga;
    }

    public boolean isHabilitarGenerarReporteBool() {
        return habilitarGenerarReporteBool;
    }

    public void setHabilitarGenerarReporteBool(boolean habilitarGenerarReporteBool) {
        this.habilitarGenerarReporteBool = habilitarGenerarReporteBool;
    }

    public RepReporteEjecucion getReporteVisualizacionSeleccionado() {
        return reporteVisualizacionSeleccionado;
    }

    public void setReporteVisualizacionSeleccionado(
        RepReporteEjecucion reporteVisualizacionSeleccionado) {
        this.reporteVisualizacionSeleccionado = reporteVisualizacionSeleccionado;
    }

    public EConservacionSubproceso getSubprocesoSeleccionado() {
        return subprocesoSeleccionado;
    }

    public void setSubprocesoSeleccionado(EConservacionSubproceso subprocesoSeleccionado) {
        this.subprocesoSeleccionado = subprocesoSeleccionado;
    }

    public String getCodigoReporte() {
        return codigoReporte;
    }

    public void setCodigoReporte(String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }

    public EConservacionSubprocesoActividad getActividadSeleccionada() {
        return actividadSeleccionada;
    }

    public void setActividadSeleccionada(
        EConservacionSubprocesoActividad actividadSeleccionada) {
        this.actividadSeleccionada = actividadSeleccionada;
    }

    public boolean isPermisoBuscarReporteBool() {
        return permisoBuscarReporteBool;
    }

    public void setPermisoBuscarReporteBool(boolean permisoBuscarReporteBool) {
        this.permisoBuscarReporteBool = permisoBuscarReporteBool;
    }

    public boolean isPermisoGenerarReporteBool() {
        return permisoGenerarReporteBool;
    }

    public void setPermisoGenerarReporteBool(boolean permisoGenerarReporteBool) {
        this.permisoGenerarReporteBool = permisoGenerarReporteBool;
    }

    public FiltroDatosConsultaReportesRadicacion getDatosConsultaReportesRadicacion() {
        return datosConsultaReportesRadicacion;
    }

    public void setDatosConsultaReportesRadicacion(
        FiltroDatosConsultaReportesRadicacion datosConsultaReportesRadicacion) {
        this.datosConsultaReportesRadicacion = datosConsultaReportesRadicacion;
    }

    public String getRangoDiasTranscurridosSeleccionado() {
        return rangoDiasTranscurridosSeleccionado;
    }

    public void setRangoDiasTranscurridosSeleccionado(
        String rangoDiasTranscurridosSeleccionado) {
        this.rangoDiasTranscurridosSeleccionado = rangoDiasTranscurridosSeleccionado;
    }

    public String getClasificacionSeleccionada() {
        return clasificacionSeleccionada;
    }

    public void setClasificacionSeleccionada(
        String clasificacionSeleccionada) {
        this.clasificacionSeleccionada = clasificacionSeleccionada;
    }

    public List<SelectItem> getClasificacionesItemList() {
        return clasificacionesItemList;
    }

    public void setClasificacionesItemList(List<SelectItem> clasificacionesItemList) {
        this.clasificacionesItemList = clasificacionesItemList;
    }

    public List<SelectItem> getActividadesItemList() {
        return actividadesItemList;
    }

    public void setActividadesItemList(List<SelectItem> actividadesItemList) {
        this.actividadesItemList = actividadesItemList;
    }

    public List<SelectItem> getSubprocesosItemList() {
        return subprocesosItemList;
    }

    public void setSubprocesosItemList(List<SelectItem> subprocesosItemList) {
        this.subprocesosItemList = subprocesosItemList;
    }

    public List<SelectItem> getRolesItemList() {
        return rolesItemList;
    }

    public void setRolesItemList(List<SelectItem> rolesItemList) {
        this.rolesItemList = rolesItemList;
    }

    public List<SelectItem> getTipoReportesItemList() {
        return tipoReportesItemList;
    }

    public void setTipoReportesItemList(List<SelectItem> tipoReportesItemList) {
        this.tipoReportesItemList = tipoReportesItemList;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFuncionarioSeleccionado() {
        return funcionarioSeleccionado;
    }

    public void setFuncionarioSeleccionado(String funcionarioSeleccionado) {
        this.funcionarioSeleccionado = funcionarioSeleccionado;
    }

    public List<SelectItem> getFuncionariosItemList() {
        return funcionariosItemList;
    }

    public void setFuncionariosItemList(List<SelectItem> funcionariosItemList) {
        this.funcionariosItemList = funcionariosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getMinDate() {
        return minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public String getFormatoSeleccionado() {
        return formatoSeleccionado;
    }

    public void setFormatoSeleccionado(String formatoSeleccionado) {
        this.formatoSeleccionado = formatoSeleccionado;
    }

    public List<EstructuraOrganizacional> getUnidadesOperativasCatastroObj() {
        return unidadesOperativasCatastroObj;
    }

    public List<SelectItem> getUnidadesOperativasCatastroItemList() {
        return unidadesOperativasCatastroItemList;
    }

    public String getUnidadOperativaSeleccionadaCodigo() {
        return unidadOperativaSeleccionadaCodigo;
    }

    public void setUnidadOperativaSeleccionadaCodigo(
        String unidadOperativaSeleccionadaCodigo) {
        this.unidadOperativaSeleccionadaCodigo = unidadOperativaSeleccionadaCodigo;
    }

    public ArrayList<SelectItem> getTerritorialesItemList() {
        return this.territorialesItemList;
    }

    public void setTerritorialesItemList(
        ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public RepReporte getRepReporteSeleccionado() {
        return repReporteSeleccionado;
    }

    public void setRepReporteSeleccionado(RepReporte repReporteSeleccionado) {
        this.repReporteSeleccionado = repReporteSeleccionado;
    }

    public RepReporteEjecucion getRepReporteEjecucion() {
        return repReporteEjecucion;
    }

    public void setRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion) {
        this.repReporteEjecucion = repReporteEjecucion;
    }

    public EstructuraOrganizacional getUnidadOperativaSeleccionada() {
        return unidadOperativaSeleccionada;
    }

    public String getDepartamentoSeleccionadoCodigo() {
        return departamentoSeleccionadoCodigo;
    }

    public void setDepartamentoSeleccionadoCodigo(
        String departamentoSeleccionadoCodigo) {
        this.departamentoSeleccionadoCodigo = departamentoSeleccionadoCodigo;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public String getMunicipioSeleccionadoCodigo() {
        return municipioSeleccionadoCodigo;
    }

    public void setMunicipioSeleccionadoCodigo(String municipioSeleccionadoCodigo) {
        this.municipioSeleccionadoCodigo = municipioSeleccionadoCodigo;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setUnidadesOperativasCatastroObj(
        List<EstructuraOrganizacional> unidadesOperativasCatastroObj) {
        this.unidadesOperativasCatastroObj = unidadesOperativasCatastroObj;
    }

    public void setUnidadesOperativasCatastroItemList(
        List<SelectItem> unidadesOperativasCatastroItemList) {
        this.unidadesOperativasCatastroItemList = unidadesOperativasCatastroItemList;
    }

    public void setUnidadOperativaSeleccionada(
        EstructuraOrganizacional unidadOperativaSeleccionada) {
        this.unidadOperativaSeleccionada = unidadOperativaSeleccionada;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    /**
     * --------------------------------
     */
    /**
     * ----------- MÉTODOS ------------
     */
    /**
     * --------------------------------
     */
    /**
     * Método que realiza la inicialización de los menus en pantalla
     *
     * @author david.cifuentes
     */
    public void cargarTiposDeReporte() {

        if (this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()) {

            List<String> tipoReportesItemListTemp = new ArrayList<String>();

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                // Tipos de reporte
                if (repUsuarioRol.getRepReporte() != null) {

                    if (!tipoReportesItemListTemp.
                        contains(repUsuarioRol.getRepReporte().getNombre())) {
                        this.tipoReportesItemList.add(new SelectItem(
                            repUsuarioRol.getRepReporte().getId(),
                            repUsuarioRol.getRepReporte().getNombre()));
                        tipoReportesItemListTemp.add(repUsuarioRol.getRepReporte().getNombre());
                    }
                }
            }
        }
    }

    /**
     * Método que habilita/deshabilita campos de consulta dependiendo el tipo de reporte.
     *
     * @author leidy.gonzalez
     */
    public void habilitarCamposPorTipoReporte() {
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar una territorial para la carga de unidades operativas.
     *
     * @author david.cifuentes
     */
    public void cargarTerritoriales() {

        this.territorialesItemList.clear();
        List<String> territorialesItemListTemp = new ArrayList<String>();

        if (this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepTerritorialReportes() != null) {
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {
                        if (rtr.getTerritorial() != null) {
                            SelectItem territorial = new SelectItem(
                                rtr.getTerritorial().getCodigo(),
                                rtr.getTerritorial().getNombre());

                            if (!territorialesItemListTemp.
                                contains(rtr.getTerritorial().getCodigo())) {
                                this.territorialesItemList.add(territorial);
                                territorialesItemListTemp.add(rtr.getTerritorial().getCodigo());
                            }

//                            //Se selecciona la territorial a la cual pertenece el usuario 
//                            if (rtr.getTerritorial().getCodigo().equals(this.usuario.getCodigoTerritorial())) {
//                                this.territorialSeleccionadaCodigo = this.usuario.getCodigoTerritorial();
//                            }
                        }
                    }
                }
            }
        }

        this.cargarUOC();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar una territorial para la carga de unidades operativas.
     *
     * @author david.cifuentes
     */
    public void cargarUOC() {
        if (this.territorialSeleccionadaCodigo != null) {
            this.territorialSeleccionada = this.
                buscarTerritorial(this.territorialSeleccionadaCodigo);
            this.unidadOperativaSeleccionadaCodigo = null;

            if (this.territorialSeleccionada != null) {
                this.cargarListaUOC();
                this.cargarUOCSeleccionada();
                this.cargarDepartamentos();
                this.cargarRoles();
            }
        } else {
            this.rolesItemList = null;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la actualización de los roles al seleccionar una territorial.
     *
     * @author david.cifuentes
     */
    public void cargarRoles() {

        this.rolesItemList = new ArrayList<SelectItem>();
        List<SelectItem> rolesItemListTemp = new ArrayList<SelectItem>();
        List<String> rolesListTemp = new ArrayList<String>();
        this.rolSeleccionado = null;

        if ((this.territorialSeleccionadaCodigo != null &&
             !this.territorialSeleccionadaCodigo.isEmpty()) ||
            (this.unidadOperativaSeleccionadaCodigo != null &&
             !this.unidadOperativaSeleccionadaCodigo.isEmpty())) {

            Map<String, List<SelectItem>> rolesEstructuraOrganizacional =
                new HashMap<String, List<SelectItem>>();

            if (this.unidadOperativaSeleccionadaCodigo != null &&
                 !this.unidadOperativaSeleccionadaCodigo.isEmpty()) {
                EstructuraOrganizacional uoc = this.buscarTerritorial(
                    this.unidadOperativaSeleccionadaCodigo);

                if (uoc != null) {
                    rolesEstructuraOrganizacional = this
                        .cargarRolesEstructuraOrganizacional(
                            new EstructuraOrganizacional(
                                this.unidadOperativaSeleccionadaCodigo,
                                uoc.getNombre(), this.territorialSeleccionadaCodigo, null,
                                null), rolesEstructuraOrganizacional,
                            false);

                    if (rolesEstructuraOrganizacional != null &&
                        
                        rolesEstructuraOrganizacional.get(this.unidadOperativaSeleccionadaCodigo) !=
                        null &&
                         !rolesEstructuraOrganizacional.get(this.unidadOperativaSeleccionadaCodigo).
                            isEmpty()) {
                        rolesItemListTemp.addAll(rolesEstructuraOrganizacional
                            .get(this.unidadOperativaSeleccionadaCodigo));
                    }
                }
            } else {

                List<EstructuraOrganizacional> listaTerritorialYUOC =
                    new ArrayList<EstructuraOrganizacional>();
                boolean esTerritorial = false;

                EstructuraOrganizacional territorial = this.buscarTerritorial(
                    this.territorialSeleccionadaCodigo);
                if (territorial != null) {
                    listaTerritorialYUOC.add(territorial);
                }

                List<EstructuraOrganizacional> UOCs = this
                    .getGeneralesService()
                    .getCacheEstructuraOrganizacionalPorTipoYPadre(
                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                        this.territorialSeleccionadaCodigo);
                if (UOCs != null && !UOCs.isEmpty()) {
                    listaTerritorialYUOC.addAll(UOCs);
                }

                for (EstructuraOrganizacional eo : listaTerritorialYUOC) {
                    if (eo != null) {
                        if (eo.getCodigo().equals(this.territorialSeleccionadaCodigo)) {
                            esTerritorial = true;
                        } else {
                            esTerritorial = false;
                        }
                        rolesEstructuraOrganizacional = this
                            .cargarRolesEstructuraOrganizacional(
                                new EstructuraOrganizacional(
                                    eo.getCodigo(),
                                    eo.getNombre(), this.territorialSeleccionadaCodigo, null,
                                    null), rolesEstructuraOrganizacional,
                                esTerritorial);

                        if (rolesEstructuraOrganizacional != null &&
                             rolesEstructuraOrganizacional.get(eo.getCodigo()) != null &&
                             !rolesEstructuraOrganizacional.get(eo.getCodigo()).isEmpty()) {
                            for (SelectItem si : rolesEstructuraOrganizacional.get(eo.getCodigo())) {
                                if (!rolesListTemp.contains(si.getValue().toString())) {
                                    rolesItemListTemp.add(si);
                                    rolesListTemp.add(si.getValue().toString());
                                }
                            }
                        }
                    }
                }
            }
        }

        if (rolesItemListTemp != null &&
             !rolesItemListTemp.isEmpty()) {
            Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem s1, SelectItem s2) {
                    return s1.getValue().toString().compareTo(s2.getValue().toString());
                }
            };
            Collections.sort(rolesItemListTemp, comparator);
            this.rolesItemList = rolesItemListTemp;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de roles de una estructura organizacional seleccionada
     *
     * @param estructuraOrganizacional
     * @author david.cifuentes
     * @return
     */
    public Map<String, List<SelectItem>> cargarRolesEstructuraOrganizacional(
        EstructuraOrganizacional estructuraOrganizacional,
        Map<String, List<SelectItem>> rolesEstructuraOrganizacional,
        boolean esTerritorial) {

        List<SelectItem> rolesTerritorial = new ArrayList<SelectItem>();
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        String nombreEO;

        this.rolesItemList = new ArrayList<SelectItem>();
        List<String> roles = new ArrayList<String>();

        nombreEO = this.getGeneralesService().obtenerCodigoHomologadoNombreTerritorial(
            estructuraOrganizacional.getCodigo());
        if (nombreEO == null || estructuraOrganizacional.getCodigo().equals(nombreEO)) {
            nombreEO = estructuraOrganizacional.getNombre().replace(' ', '_');
        }

        if (estructuraOrganizacional != null && estructuraOrganizacional.getCodigo() != null) {
            if (!esTerritorial) {
                EstructuraOrganizacional territorial = buscarTerritorial(estructuraOrganizacional.
                    getTipo());
                String nombreTerritorialCodHom = this.getGeneralesService().
                    obtenerCodigoHomologadoNombreTerritorial(territorial.getCodigo());
                if (!estructuraOrganizacional.getTipo().equals(nombreTerritorialCodHom)) {
                    nombreTerritorialCodHom = territorial.getNombre().replace(' ', '_');
                    usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(
                        nombreTerritorialCodHom, nombreEO);
                }
            } else {
                usuarios = this.getTramiteService().buscarFuncionariosPorTerritorial(nombreEO, null);
            }
        }

        if (usuarios == null) {
            this.addMensajeError("Error al consultar los usuarios para " +
                 estructuraOrganizacional.getNombre());
            return null;
        }

        for (UsuarioDTO user : usuarios) {
            if (user.getRoles() != null) {
                for (String rol : user.getRoles()) {

                    if (!roles.contains(rol)) {
                        roles.add(rol);
                    }

                    if (roles != null) {
                        Collections.sort(roles);
                    }
                }
            }
        }

        if (roles != null) {
            for (String rol : roles) {

                if (rol.contains("CONSULTA_")) {
                    continue;
                }

                rolesTerritorial.add(new SelectItem(rol, rol));
            }
            rolesEstructuraOrganizacional.
                put(estructuraOrganizacional.getCodigo(), rolesTerritorial);
        }
        return rolesEstructuraOrganizacional;
    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @author david.cifuentes
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {
        if (codigoTerritorial != null) {
            EstructuraOrganizacional estructuraOrganizacional, answer = null;
            for (EstructuraOrganizacional eo : this.getGeneralesService()
                .buscarTodasTerritoriales()) {
                if (eo.getCodigo().equals(codigoTerritorial)) {
                    answer = eo;
                    break;
                }
            }
            if (answer != null) {
                estructuraOrganizacional = this
                    .getGeneralesService()
                    .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
                if (estructuraOrganizacional != null) {
                    return estructuraOrganizacional;
                } else {
                    return answer;
                }
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Mètodo que realiza el cargue de unidades operativas de catastro cuando se ha seleccionado una
     * territorial
     *
     * @author david.cifuentes
     */
    public void cargarListaUOC() {

        if (this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()) {

            this.unidadesOperativasCatastroItemList.clear();
            List<String> unidadesOperativasCatastroItemListTemp = new ArrayList<String>();

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepTerritorialReportes() != null) {
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        // UOC
                        if (rtr.getTerritorial().getCodigo().equals(
                            this.territorialSeleccionadaCodigo)) {
                            if (rtr.getUOC() != null) {
                                SelectItem uoc = new SelectItem(rtr.getUOC().getCodigo(),
                                    rtr.getUOC().getNombre());

                                if (!unidadesOperativasCatastroItemListTemp.contains(rtr.getUOC().
                                    getCodigo())) {
                                    this.unidadesOperativasCatastroItemList.add(uoc);
                                    unidadesOperativasCatastroItemListTemp.add(rtr.getUOC().
                                        getCodigo());
                                }
                            } else {
                                this.unidadesOperativasCatastroObj = this.getGeneralesService()
                                    .getCacheEstructuraOrganizacionalPorTipoYPadre(
                                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                                        this.territorialSeleccionadaCodigo);

                                this.unidadesOperativasCatastroItemList.clear();

                                for (EstructuraOrganizacional eo
                                    : this.unidadesOperativasCatastroObj) {
                                    this.unidadesOperativasCatastroItemList.add(new SelectItem(eo.
                                        getCodigo(), eo.getNombre()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el set por default de la UOC del usuario.
     *
     * @author david.cifuentes
     */
    public void cargarUOCSeleccionada() {
        if (this.usuario.isUoc()) {
            this.unidadOperativaSeleccionada = null;

            if (this.unidadesOperativasCatastroObj != null) {

                this.unidadOperativaSeleccionadaCodigo = this.usuario.getCodigoUOC();

                for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
                    if (eo.getCodigo().equals(
                        this.unidadOperativaSeleccionadaCodigo)) {
                        this.unidadOperativaSeleccionada = eo;
                        break;
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de la lista de {@link Departamento} respecto a la
     * parametrización del usuario
     *
     * @author david.cifuentes
     */
    public void cargarDepartamentos() {

        this.departamentosItemList.clear();
        List<String> departamentosListTemp = new ArrayList<String>();
        this.departamentoSeleccionadoCodigo = new String();
        this.municipioSeleccionadoCodigo = new String();

        this.cargarRoles();

        if (this.tipoReporteId != null &&
             this.territorialSeleccionadaCodigo != null &&
             this.parametrizacionUsuario != null &&
             !this.parametrizacionUsuario.isEmpty()) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepReporte().getId().longValue() == this.tipoReporteId.
                    longValue() &&
                     repUsuarioRol.getRepTerritorialReportes() != null) {

                    // Se filtran los RepTerritorialReporte de la territorial seleccionada
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        if (rtr.getTerritorial().getCodigo().equals(
                            this.territorialSeleccionadaCodigo)) {

                            // Si el RepTerritorialReporte tiene el campo de código departamento 
                            // vacío quiere decir que se tiene permisos para todos los departamentos
                            // de esa territorial
                            if (rtr.getDepartamento() == null) {

                                List<Departamento> departamentosList = this.getGeneralesService()
                                    .obtenerRepTerritorialReporte(this.territorialSeleccionadaCodigo,
                                        this.unidadOperativaSeleccionadaCodigo);

                                if (departamentosList != null && !departamentosList.isEmpty()) {
                                    this.departamentosItemList.clear();
                                    for (Departamento departamento : departamentosList) {
                                        if (!departamentosListTemp.
                                            contains(departamento.getCodigo())) {
                                            this.departamentosItemList.add(new SelectItem(
                                                departamento.getCodigo(),
                                                departamento.getNombre()));
                                            departamentosListTemp.add(departamento.getCodigo());
                                        }
                                    }
                                }
                                break;
                            } else {

                                if (this.unidadOperativaSeleccionadaCodigo != null &&
                                     !this.unidadOperativaSeleccionadaCodigo.isEmpty()) {

                                    List<Departamento> departamentosList = this.
                                        getGeneralesService()
                                        .obtenerRepTerritorialReporte(
                                            this.territorialSeleccionadaCodigo,
                                            this.unidadOperativaSeleccionadaCodigo);

                                    this.departamentosItemList.clear();
                                    for (Departamento departamento : departamentosList) {
                                        if (!departamentosListTemp.
                                            contains(departamento.getCodigo())) {
                                            this.departamentosItemList.add(new SelectItem(
                                                departamento.getCodigo(),
                                                departamento.getNombre()));
                                            departamentosListTemp.add(departamento.getCodigo());
                                        }
                                    }
                                    break;

                                } else if (!departamentosListTemp.contains(rtr.getDepartamento().
                                    getCodigo())) {
                                    this.departamentosItemList.add(new SelectItem(rtr.
                                        getDepartamento().getCodigo(),
                                        rtr.getDepartamento().getCodigo() + "-" +
                                         rtr.getDepartamento().getNombre()));
                                    departamentosListTemp.add(rtr.getDepartamento().getCodigo());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar el tipo de reporte a buscar, éste principalmente recarga
     * la lista de los menus que dependen del tipo de reporte
     *
     * @author david.cifuentes
     */
    public void cambioTipoReporte() {

        if (this.tipoReporteId != null &&
             !this.parametrizacionUsuario.isEmpty()) {
            for (RepUsuarioRolReporte urr : this.parametrizacionUsuario) {
                if (urr.getRepReporte() != null &&
                     urr.getRepReporte().getId() != null &&
                     urr.getRepReporte().getId().longValue() == this.tipoReporteId.longValue()) {
                    this.repReporteSeleccionado = urr.getRepReporte();
                    break;
                }
            }

            this.deshabilitarCamposConsultaBool = false;
            // Verificar la si se deben deshabilitar campos de consulta según tipo de reporte
            for (ERepReporteConsultaEspecialParametrizacion rpce
                : ERepReporteConsultaEspecialParametrizacion.values()) {
                if (rpce.getId().longValue() == this.tipoReporteId.longValue()) {
                    this.deshabilitarCamposConsultaBool = true;
                    break;
                }
            }

            //Valida que formato se debe visualizar dependiendo del tipo de Reporte
            this.repFormatosDescarga = new ArrayList<SelectItem>();
            if (this.tipoReporteId.equals(
                EReportesRadicacionesDinamicosSNC.RADICACIONES_ROL_FUNCIONARIO_CONSOLIDADO.
                    getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(
                    EReportesRadicacionesDinamicosSNC.RADICACIONES_ROL_FUNCIONARIO_DETALLADO.
                        getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(
                    EReportesRadicacionesDinamicosSNC.RADICACIONES_CONSOLIDADO.
                        getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(EReportesRadicacionesDinamicosSNC.RADICACIONES_DETALLADO.
                    getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(
                    EReportesRadicacionesDinamicosSNC.RADICACIONES_TRAMITES_Y_ACTIVIDADES.
                        getIdConfiguracionReporte())) {

                this.adicionarDatosV(repFormatosDescarga, this.getGeneralesService()
                    .getCacheDominioPorNombre(EDominio.REP_FORMATOS_DESCARGA));

            } else {
                this.adicionarDatosV(repFormatosDescarga, this.getGeneralesService()
                    .getCacheDominioPorNombre(EDominio.FORMATO_PDF));
            }

            if (this.tipoReporteId.equals(
                EReportesRadicacionesDinamicosSNC.RADICACIONES_TIPO_TRAMITE.
                    getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(
                    EReportesRadicacionesDinamicosSNC.RADICACIONES_CONSOLIDADO.
                        getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(EReportesRadicacionesDinamicosSNC.RADICACIONES_DETALLADO.
                    getIdConfiguracionReporte()) ||
                 this.tipoReporteId.equals(
                    EReportesRadicacionesDinamicosSNC.PREDIOS_CON_MATRICULAS_REGISTRALES.
                        getIdConfiguracionReporte())) {

                this.deshabilitaCampos = false;
            } else {
                this.deshabilitaCampos = true;
            }

        }

        // Inicialización de menús que dependen del tipo de informe
        this.territorialesItemList.clear();
        this.unidadesOperativasCatastroItemList.clear();
        this.departamentosItemList.clear();
        this.municipiosItemList.clear();

        this.cargarTerritoriales();
    }

    private void adicionarDatosV(List<SelectItem> lista,
        List<Dominio> listDominio) {
        for (Dominio dominio : listDominio) {
            repFormatosDescarga.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la actualización de los municipios al seleccionar un departamento.
     *
     * @author david.cifuentes
     */
    public void cargarMunicipios() {

        this.municipiosItemList.clear();

        if (this.departamentoSeleccionadoCodigo != null) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepReporte().getId().longValue() == this.tipoReporteId.
                    longValue() &&
                     repUsuarioRol.getRepTerritorialReportes() != null) {

                    // Se filtran los RepTerritorialReporte de la territorial seleccionada
                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        if (rtr.getTerritorial().getCodigo().equals(
                            this.territorialSeleccionadaCodigo) &&
                             rtr.getDepartamento().getCodigo().equals(
                                this.departamentoSeleccionadoCodigo)) {

                            // Si el RepTerritorialReporte tiene el campo de código municipio 
                            // vacío quiere decir que se tiene permisos para todos los municipios
                            // del departamento
                            if (rtr.getMunicipio() == null) {

                                List<Municipio> municipiosList = this.getGeneralesService()
                                    .getCacheMunicipiosByDepartamento(
                                        this.departamentoSeleccionadoCodigo);

                                if (municipiosList != null && !municipiosList.isEmpty()) {
                                    this.municipiosItemList.clear();
                                    for (Municipio municipio : municipiosList) {
                                        this.municipiosItemList.add(new SelectItem(municipio
                                            .getCodigo(), municipio.getNombre()));
                                    }
                                }
                                break;
                            } else {
                                this.municipiosItemList.add(new SelectItem(
                                    rtr.getMunicipio().getCodigo(),
                                    rtr.getMunicipio().getCodigo() + "-" +
                                     rtr.getMunicipio().getNombre()));
                            }
                        }
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método encargado de cargar los funcionarios en una lista de de SelecItem asociados a la
     * territorial del usuario en sesion.
     */
    public void cargarFuncionarios() {

        this.funcionariosItemList.clear();
        if (this.rolSeleccionado != null && !this.rolSeleccionado.isEmpty()) {
            List<UsuarioDTO> funcionarios;
            String codigoUOSeleccionada;

            if (this.territorialSeleccionada != null) {
                if (this.unidadOperativaSeleccionada != null) {
                    codigoUOSeleccionada = this.unidadOperativaSeleccionada
                        .getNombre();
                    codigoUOSeleccionada = codigoUOSeleccionada.toUpperCase()
                        .replace("UOC ", "UOC_");
                } else {
                    codigoUOSeleccionada = this.territorialSeleccionada.getNombre()
                        .toUpperCase();
                }

                codigoUOSeleccionada = Utilidades.delTildes(codigoUOSeleccionada);

            } else {
                codigoUOSeleccionada = this.usuario
                    .getDescripcionEstructuraOrganizacional();
            }

            funcionarios = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(codigoUOSeleccionada,
                    ERol.valueOf(this.rolSeleccionado));

            if (funcionarios != null && !funcionarios.isEmpty()) {
                for (UsuarioDTO funcionario : funcionarios) {
                    this.funcionariosItemList.add(new SelectItem(funcionario
                        .getLogin(), funcionario.getNombreCompleto()));
                }
            }

            Collections.sort(this.funcionariosItemList,
                new Comparator<SelectItem>() {
                @Override
                public int compare(SelectItem sItem1, SelectItem sItem2) {
                    String sItem1Label = sItem1.getLabel();
                    String sItem2Label = sItem2.getLabel();
                    return (sItem1Label.compareToIgnoreCase(sItem2Label));
                }
            });
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue del menú de subprocesos de Conservación.
     *
     * @author david.cifuentes
     */
    public void cargarSubprocesosConservacion() {

        this.subprocesosItemList = new ArrayList<SelectItem>();

        for (EConservacionSubproceso cs : EConservacionSubproceso.values()) {
            this.subprocesosItemList.add(new SelectItem(cs, cs.getNombre()));
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método encargado de cargar las actividades en process de un subproceso seleccionado
     *
     * @author david.cifuentes
     */
    public void cargarActividades() {
        this.actividadesItemList = new ArrayList<SelectItem>();
        if (this.subprocesoSeleccionado != null) {
            for (EConservacionSubprocesoActividad csa : EConservacionSubprocesoActividad.values()) {
                if (csa.getSubproceso().equals(this.subprocesoSeleccionado)) {
                    SelectItem actividad = new SelectItem(csa, csa.getNombreActividad());
                    if (!this.actividadesItemList.contains(actividad)) {
                        this.actividadesItemList.add(actividad);
                    }
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el cargue de las posibles clasificaciones de un trámite.
     *
     * @author david.cifuentes
     */
    public void cargarClasificacion() {

        this.clasificacionesItemList = new ArrayList<SelectItem>();

        for (ETramiteClasificacion tc : ETramiteClasificacion.values()) {
            this.clasificacionesItemList.add(new SelectItem(tc.name(), tc.name()));
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza la búsqueda de los reportes de radicación generados con base en los
     * parámetros seleccionados.
     *
     * @author david.cifuentes
     */
    public void cargarParametrosConsulta() {

        this.datosConsultaReportesRadicacion = new FiltroDatosConsultaReportesRadicacion();
        this.datosConsultaReportesRadicacion.setTipoReporteId(this.tipoReporteId);
        this.datosConsultaReportesRadicacion.setFormato(this.formatoSeleccionado);
        this.datosConsultaReportesRadicacion.
            setTerritorialCodigo(this.territorialSeleccionadaCodigo);
        this.datosConsultaReportesRadicacion.setUOCCodigo(this.unidadOperativaSeleccionadaCodigo);
        this.datosConsultaReportesRadicacion.setDepartamentoCodigo(
            this.departamentoSeleccionadoCodigo);
        this.datosConsultaReportesRadicacion.setMunicipioCodigo(this.municipioSeleccionadoCodigo);
        this.datosConsultaReportesRadicacion.setFechaInicio(this.fechaInicio);
        this.datosConsultaReportesRadicacion.setFechaFin(this.fechaFin);
        this.datosConsultaReportesRadicacion.setRol(this.rolSeleccionado);
        this.datosConsultaReportesRadicacion.setFuncionario(this.funcionarioSeleccionado);
        this.datosConsultaReportesRadicacion.setClasificacion(this.clasificacionSeleccionada);
        this.datosConsultaReportesRadicacion.setDiasTranscurridos(
            this.rangoDiasTranscurridosSeleccionado);
        if (this.codigoReporteCod != null &&
             !this.codigoReporteCod.isEmpty() &&
             this.anioReporteCod != null &&
             !this.codigoReporteCod.isEmpty()) {
            this.codigoReporte = "REP-RAD-" + this.codigoReporteCod + "-" + this.anioReporteCod;
            this.datosConsultaReportesRadicacion.setCodigoReporte(this.codigoReporte);
        }
        if (this.actividadSeleccionada != null) {
            this.datosConsultaReportesRadicacion.setActividad(this.actividadSeleccionada.
                getNombreActividad());
        }
        if (this.subprocesoSeleccionado != null) {
            this.datosConsultaReportesRadicacion.setSubproceso(this.subprocesoSeleccionado.
                getNombre());
        }

    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que valida la obligatoriedad de los parametros ingresados.
     *
     * @author leidy.gonzalez
     */
    public void validarObligatoriedadCampos() {

        if (codigoReporteCod.isEmpty()) {

            if (this.tipoReporteId == null) {

                this.addMensajeError(
                    "Tipo reporte: Error de validación: Tipo reporte necesita un valor.");
                return;
            }
            if (this.territorialSeleccionadaCodigo.isEmpty()) {

                this.addMensajeError(
                    "Territorial: Error de validación: Territorial necesita un valor.");
                return;
            }

            if (this.formatoSeleccionado.isEmpty()) {

                this.addMensajeError("Formato: Error de validación: Formato necesita un valor.");
                return;
            }

            if (this.fechaInicio == null) {

                this.addMensajeError(
                    "Fecha inicio: Error de validación: Fecha inicio necesita un valor.");
                return;
            }

            if (this.fechaFin == null) {

                this.addMensajeError("Fecha fin: Error de validación: Fecha fin necesita un valor.");
                return;
            }

            // Validación de fechas
            if (this.fechaInicio != null && this.fechaFin != null &&
                 this.fechaFin.before(this.fechaInicio)) {
                this.
                    addMensajeError("La fecha de inicio es mayor a la fecha fin, por favor revise.");
                return;
            }
        } else {

            if (this.codigoReporteCod != null &&
                 !this.codigoReporteCod.isEmpty() &&
                 this.anioReporteCod != null &&
                 !this.codigoReporteCod.isEmpty()) {
                this.codigoReporte = "REP-RAD-" + this.codigoReporteCod + "-" + this.anioReporteCod;
            }
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza la búsqueda de los reportes de radicación generados con base en los
     * parámetros seleccionados.
     *
     * @author david.cifuentes
     */
    public void buscarReporte() {

        try {

            if (codigoReporteCod.isEmpty()) {

                if (this.tipoReporteId == null) {

                    this.addMensajeError(
                        "Tipo reporte: Error de validación: Tipo reporte necesita un valor.");
                    return;
                }
                if (this.territorialSeleccionadaCodigo.isEmpty()) {

                    this.addMensajeError(
                        "Territorial: Error de validación: Territorial necesita un valor.");
                    return;
                }

                if (this.formatoSeleccionado.isEmpty()) {

                    this.addMensajeError("Formato: Error de validación: Formato necesita un valor.");
                    return;
                }

                if (this.fechaInicio == null) {

                    this.addMensajeError(
                        "Fecha inicio: Error de validación: Fecha inicio necesita un valor.");
                    return;
                }

                if (this.fechaFin == null) {

                    this.addMensajeError(
                        "Fecha fin: Error de validación: Fecha fin necesita un valor.");
                    return;
                }

                // Validación de fechas
                if (this.fechaInicio != null && this.fechaFin != null &&
                     this.fechaFin.before(this.fechaInicio)) {
                    this.addMensajeError(
                        "La fecha de inicio es mayor a la fecha fin, por favor revise.");
                    return;
                }

                //Valida que sean obligatorios campos_ Departamento y Municipio para el reporte:
                //Predios y matriculas registro
                if (this.tipoReporteId.equals(
                    EReportesRadicacionesDinamicosSNC.PREDIOS_CON_MATRICULAS_REGISTRALES.
                        getIdConfiguracionReporte())) {
                    if (this.departamentoSeleccionadoCodigo == null ||
                         this.departamentoSeleccionadoCodigo.isEmpty() ||
                         this.municipioSeleccionadoCodigo == null ||
                         this.municipioSeleccionadoCodigo.isEmpty()) {
                        this.addMensajeError(
                            "Los campos: Departamento y Municipio son obligatorios para el tipo de Reporte: " +
                             " RADICACIONES DE PREDIOS CON MATRÍCULAS REGISTRALES.");
                        return;
                    }
                }

                this.cargarParametrosConsulta();

                this.reportesRadicacionGenerados = this.getConservacionService()
                    .buscarReportesRadicacionGenerados(this.datosConsultaReportesRadicacion);

                this.revisarPermisoGenerarReporte();

                if (this.reportesRadicacionGenerados == null ||
                     this.reportesRadicacionGenerados.isEmpty()) {
                    this.addMensajeWarn(
                        "No se encontraron reportes que cumplan con estos filtros de búsqueda.");

                } else {
                    //Valida si se habilita opcion de asociar reporte a Usuario
                    for (RepReporteEjecucion reporteEjecucionGenerados
                        : this.reportesRadicacionGenerados) {
                        this.buscarSubscripcionPorUsuarioIdReporte(reporteEjecucionGenerados);
                    }
                    this.banderaActualizarReporte = true;
                }

            } else {

                if (!this.codigoReporteCod.isEmpty() &&
                     this.anioReporteCod != null &&
                     !this.codigoReporteCod.isEmpty()) {
                    this.codigoReporte = "REP-RAD-" + this.codigoReporteCod + "-" +
                        this.anioReporteCod;
                }

                //consulta reporte por codigo
                if (!this.codigoReporte.isEmpty()) {

                    this.reportesRadicacionGenerados = this.getConservacionService().
                        buscarReportesPorCodigo(this.codigoReporte);

                    this.revisarPermisoGenerarReporte();

                    if (this.reportesRadicacionGenerados.isEmpty()) {
                        this.addMensajeWarn(
                            "No se encontraron reportes que cumplan con estos filtros de búsqueda.");
                        return;
                    } else {
                        for (RepReporteEjecucion reporteEjecucionGenerados
                            : this.reportesRadicacionGenerados) {
                            this.buscarSubscripcionPorUsuarioIdReporte(reporteEjecucionGenerados);
                        }
                        this.banderaActualizarReporte = true;
                    }
                }
            }

            this.habilitarGenerarReporteBool = true;

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en la búsqueda de los reportes generados.");
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que consulta si existe alguna subscripcion del reporte consultado con el usuario
     * logeado
     *
     * @author leidy.gonzalez
     */
    public void buscarSubscripcionPorUsuarioIdReporte(RepReporteEjecucion reporteEjecucionGenerados) {

        //Se consulta si el usuario ya fue asociado
        this.reportesEjecucionUsuarios = this.getConservacionService().
            buscarSubscripcionPorUsuarioIdReporte(this.usuario.getLogin(),
                reporteEjecucionGenerados.getId());

        if (reportesEjecucionUsuarios.isEmpty()) {

            this.habilitaAsociarReporteUsuario = true;
        } else {
            this.habilitaAsociarReporteUsuario = false;
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza el llamado al job para generar el reporte de radicación con base en los
     * parámetros seleccionados.
     *
     * @ref ConsultaReportesPredialesMB.generarReporte - Leidy Gonzalez
     * @author david.cifuentes
     */
    public void generarReporte() {

        try {
            if (this.reportesRadicacionGenerados == null || this.reportesRadicacionGenerados.
                isEmpty()) {
                this.reportesRadicacionGenerados = new ArrayList<RepReporteEjecucion>();
            }

            //Valida Obligatoriedad de Campos
            if (this.tipoReporteId == null) {

                this.addMensajeError(
                    "Tipo reporte: Error de validación: Tipo reporte necesita un valor.");
                return;
            }
            if (this.territorialSeleccionadaCodigo.isEmpty()) {

                this.addMensajeError(
                    "Territorial: Error de validación: Territorial necesita un valor.");
                return;
            }

            if (this.formatoSeleccionado.isEmpty()) {

                this.addMensajeError("Formato: Error de validación: Formato necesita un valor.");
                return;
            }

            if (this.fechaInicio == null) {

                this.addMensajeError(
                    "Fecha inicio: Error de validación: Fecha inicio necesita un valor.");
                return;
            }

            if (this.fechaFin == null) {

                this.addMensajeError("Fecha fin: Error de validación: Fecha fin necesita un valor.");
                return;
            }

            // Validación de fechas
            if (this.fechaInicio != null && this.fechaFin != null &&
                 this.fechaFin.before(this.fechaInicio)) {
                this.
                    addMensajeError("La fecha de inicio es mayor a la fecha fin, por favor revise.");
                return;
            }

            //Valida que sean obligatorios campos_ Departamento y Municipio para el reporte:
            //Predios y matriculas registro
            if (this.tipoReporteId.equals(
                EReportesRadicacionesDinamicosSNC.PREDIOS_CON_MATRICULAS_REGISTRALES.
                    getIdConfiguracionReporte())) {
                if (this.departamentoSeleccionadoCodigo == null ||
                     this.departamentoSeleccionadoCodigo.isEmpty() ||
                     this.municipioSeleccionadoCodigo == null ||
                     this.municipioSeleccionadoCodigo.isEmpty()) {

                    this.addMensajeError(
                        "Los campos: Departamento y Municipio son obligatorios para el tipo de Reporte: " +
                         " RADICACIONES DE PREDIOS CON MATRÍCULAS REGISTRALES.");
                    return;
                }
            }
            this.cargarParametrosConsulta();
            this.generarCodigoReporte();
            this.repReporteEjecucion = this.actualizarReporteEjecucion();
            this.actualizarReporteEjecucionUsuario(this.repReporteEjecucion);
            this.repReporteEjecucionDetalle = this.actualizarReporteEjecucionDetalle(
                this.repReporteEjecucion);

            // Generación del reporte
            if (this.repReporteEjecucion != null) {

                this.getConservacionService().generarReporteRadicacion(this.repReporteEjecucion);

                this.reportesRadicacionGenerados.add(this.repReporteEjecucion);

                this.addMensajeInfo("Se inició la generación del reporte, su código es: " +
                    this.codigoReporte);
            }

            this.actualizarTabla();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Ocurrió un error en la generación del reporte.");
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza el set de los valores seleccionados en los filtros, para generar el
     * reporte asociado
     *
     * @ref ConsultaReportesPredialesMB.actualizarReporteEjecucion - Leidy Gonzalez
     * @author david.cifuentes
     */
    public RepReporteEjecucion actualizarReporteEjecucion() {

        this.repReporteEjecucion = new RepReporteEjecucion();

        if (this.datosConsultaReportesRadicacion != null) {

            if (this.datosConsultaReportesRadicacion.getDepartamentoCodigo() != null &&
                 !this.datosConsultaReportesRadicacion.getDepartamentoCodigo().isEmpty()) {
                this.repReporteEjecucion.setDepartamentoCodigo(this.datosConsultaReportesRadicacion.
                    getDepartamentoCodigo());
            }

            if (this.datosConsultaReportesRadicacion.getMunicipioCodigo() != null &&
                 !this.datosConsultaReportesRadicacion.getMunicipioCodigo().isEmpty()) {
                this.repReporteEjecucion.setMunicipioCodigo(this.datosConsultaReportesRadicacion.
                    getMunicipioCodigo());
            }

            if (this.datosConsultaReportesRadicacion.getFechaInicio() != null) {
                this.repReporteEjecucion.setFechaInicio(this.datosConsultaReportesRadicacion.
                    getFechaInicio());
            }

            if (this.datosConsultaReportesRadicacion.getFechaFin() != null) {
                this.repReporteEjecucion.setFechaFin(this.datosConsultaReportesRadicacion.
                    getFechaFin());
            }

            if (this.repReporteSeleccionado != null) {
                this.repReporteEjecucion
                    .setRepReporte(this.repReporteSeleccionado);
            }

            if (this.territorialSeleccionada != null) {
                this.repReporteEjecucion.setTerritorial(this.territorialSeleccionada);

            }
            if (this.unidadOperativaSeleccionadaCodigo != null) {
                this.unidadOperativaSeleccionada = this.buscarTerritorial(
                    this.unidadOperativaSeleccionadaCodigo);
                this.repReporteEjecucion.setUOC(this.unidadOperativaSeleccionada);
            }

            if (this.codigoReporte != null && !this.codigoReporte.isEmpty()) {
                this.repReporteEjecucion.setCodigoReporte(this.codigoReporte);
            }

            if (this.datosConsultaReportesRadicacion.getFormato() != null) {
                this.repReporteEjecucion.setFormato(this.datosConsultaReportesRadicacion.
                    getFormato());
            }

            this.repReporteEjecucion.setEstado(ERepReporteEjecucionEstado.INICIADO.getCodigo());
            this.repReporteEjecucion.setUsuario(this.usuario.getLogin());
            this.repReporteEjecucion.setUsuarioLog(this.usuario.getLogin());
            this.repReporteEjecucion.setFechaLog(new Date());
            this.repReporteEjecucion.setReporteVacio("I");
            this.repReporteEjecucion = this.getConservacionService().actualizarRepReporteEjecucion(
                this.repReporteEjecucion);
        }
        return this.repReporteEjecucion;
    }

    /**
     * Metodo para actualizar los datos del reporte con los detalles ingresados por el usuario
     *
     * @ref ConsultaReportesPredialesMB.actualizarReporteEjecucionDetalle - Leidy Gonzalez
     * @author david.cifuentes
     */
    public RepReporteEjecucionDetalle actualizarReporteEjecucionDetalle(
        RepReporteEjecucion repReporteEjecucion) {

        if (repReporteEjecucion != null) {

            this.repReporteEjecucionDetalle = new RepReporteEjecucionDetalle();

            this.repReporteEjecucionDetalle
                .setRepReporteEjecucion(repReporteEjecucion);

            if (repReporteEjecucion.getMunicipioCodigo() != null &&
                 !repReporteEjecucion.getMunicipioCodigo().isEmpty()) {
                this.repReporteEjecucionDetalle
                    .setMunicipioCodigo(repReporteEjecucion
                        .getMunicipioCodigo());
            }

            if (repReporteEjecucion.getDepartamentoCodigo() != null &&
                 !repReporteEjecucion.getDepartamentoCodigo().isEmpty()) {

                this.repReporteEjecucionDetalle
                    .setDepartamentoCodigo(repReporteEjecucion
                        .getDepartamentoCodigo());
            }

            if (this.datosConsultaReportesRadicacion.getFechaInicio() != null) {

                this.repReporteEjecucionDetalle
                    .setFechaInicial(this.datosConsultaReportesRadicacion.getFechaInicio());
            }

            if (this.datosConsultaReportesRadicacion.getFechaFin() != null) {

                this.repReporteEjecucionDetalle
                    .setFechaFinal(this.datosConsultaReportesRadicacion.getFechaFin());
            }

            if (this.datosConsultaReportesRadicacion.getClasificacion() != null &&
                 !this.datosConsultaReportesRadicacion.getClasificacion().isEmpty()) {

                this.repReporteEjecucionDetalle
                    .setClasificacion(this.datosConsultaReportesRadicacion.getClasificacion());
            }

            if (this.datosConsultaReportesRadicacion.getDiasTranscurridos() != null &&
                 !this.datosConsultaReportesRadicacion.getDiasTranscurridos().isEmpty()) {

                this.repReporteEjecucionDetalle
                    .setDiasTranscurridos(this.datosConsultaReportesRadicacion.
                        getDiasTranscurridos());
            }

            if (this.territorialSeleccionada != null) {
                this.repReporteEjecucion.setTerritorial(this.territorialSeleccionada);

            }
            if (this.unidadOperativaSeleccionadaCodigo != null &&
                 !this.unidadOperativaSeleccionadaCodigo.isEmpty()) {

                this.repReporteEjecucionDetalle.setuOCCodigo(this.unidadOperativaSeleccionadaCodigo);

            }
            if (repReporteEjecucion.getTerritorial() != null) {

                this.repReporteEjecucionDetalle
                    .setTerritorialCodigo(repReporteEjecucion
                        .getTerritorial().getCodigo());
            }

            if (this.funcionarioSeleccionado != null &&
                 !this.funcionarioSeleccionado.trim().isEmpty()) {

                this.repReporteEjecucionDetalle
                    .setNombreFuncionario(this.funcionarioSeleccionado);
            }

            if (this.actividadSeleccionada != null) {

                this.repReporteEjecucionDetalle
                    .setActividad(this.actividadSeleccionada.getNombreActividad());
            }

            if (this.subprocesoSeleccionado != null) {

                this.repReporteEjecucionDetalle
                    .setSubproceso(this.subprocesoSeleccionado.getNombre());
            }

            if (this.rolSeleccionado != null &&
                 !this.rolSeleccionado.trim().isEmpty()) {

                this.repReporteEjecucionDetalle
                    .setRol(this.rolSeleccionado);
            }

            this.repReporteEjecucionDetalle.setUsuarioLog(this.usuario.getLogin());
            this.repReporteEjecucionDetalle.setFechaLog(new Date());

            this.repReporteEjecucionDetalle = this.getConservacionService()
                .actualizarRepReporteEjecucionDetalle(
                    this.repReporteEjecucionDetalle);
        }
        return this.repReporteEjecucionDetalle;
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza el set de los valores seleccionados en los filtros, para generar el
     * reporte asociado
     *
     * @ref ConsultaReportesPredialesMB.actualizarReporteEjecucionUsuario - Leidy Gonzalez
     * @author leidy.gonzalez
     */
    public RepReporteEjecucionUsuario actualizarReporteEjecucionUsuario(
        RepReporteEjecucion repReporteEjecucion) {

        if (repReporteEjecucion != null) {

            this.repReporteEjecucionUsuario.setUsuarioGenera(this.usuario.getLogin());
            this.repReporteEjecucionUsuario.setReporteEjecucionId(repReporteEjecucion.getId());
            this.repReporteEjecucionUsuario.setUsuarioLog(repReporteEjecucion.getUsuario());
            this.repReporteEjecucionUsuario.setFechaLog(new Date());

            this.repReporteEjecucionUsuario = this.getConservacionService().
                actualizarRepReporteEjecucionUsuario(
                    this.repReporteEjecucionUsuario);

            if (this.repReporteEjecucionUsuario == null) {
                this.addMensajeError(
                    "No se ha podido realizar su asociación al reporte. Por favor comuníquese con el administrador.");
                return null;
            }
        }

        return this.repReporteEjecucionUsuario;
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que asocia un reporte a el usuario logeado
     *
     * @author leidy.gonzalez
     */
    public void asociarReporteAUsuario() {

        this.repReporteEjecucionUsuario = new RepReporteEjecucionUsuario();

        if (this.reportesRadicacionGenerados != null &&
             !this.reportesRadicacionGenerados.isEmpty()) {

            for (RepReporteEjecucion repReporteEjecucionAsociar : this.reportesRadicacionGenerados) {

                //Se consulta si el usuario ya fue asociado
                this.reportesEjecucionUsuarios = this.getConservacionService().
                    buscarSubscripcionPorUsuarioIdReporte(this.usuario.getLogin(),
                        repReporteEjecucionAsociar.getId());

                if (reportesEjecucionUsuarios.isEmpty()) {

                    this.actualizarReporteEjecucionUsuario(repReporteEjecucionAsociar);

                    if (this.repReporteEjecucionUsuario != null) {

                        this.addMensajeInfo("El reporte con codigo:" + repReporteEjecucionAsociar.
                            getCodigoReporte() + "  fue asociado al usuario: " + this.usuario.
                                getLogin());
                        this.habilitaAsociarReporteUsuario = false;
                    }

                } else {
                    this.addMensajeWarn("El usuario que ya está inscrito al reporte.");
                }

            }

        }

    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que empaqueta los reportes seleccionados para que puedan ser descargados
     *
     * @author david.cifuentes
     */
    public void exportarReportes() {

        List<String> rutaReportesADescargar = new ArrayList<String>();

        if (this.reportesSeleccionados == null && this.reportesSeleccionados.length == 0) {
            this.addMensajeWarn("Por favor seleccione al menos un reporte a exportar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        if (this.validarExportarReportes(this.reportesSeleccionados)) {

            for (RepReporteEjecucion rre : this.reportesSeleccionados) {

                if (rre.getRutaReporteGenerado() != null &&
                     !rre.getRutaReporteGenerado().trim().isEmpty()) {

                    ReporteDTO documentoDTO = this.reportsService.
                        consultarReporteDeGestorDocumental(rre.getRutaReporteGenerado());

                    if (documentoDTO != null && documentoDTO.getRutaCargarReporteAAlfresco() != null) {
                        rutaReportesADescargar.add(documentoDTO.getRutaCargarReporteAAlfresco());

                        //Se consulta si el usuario ya fue asociado
                        this.reportesEjecucionUsuarios = this.getConservacionService().
                            buscarSubscripcionPorUsuarioIdReporte(this.usuario.getLogin(), rre.
                                getId());

                        if (reportesEjecucionUsuarios.isEmpty()) {
                            //Se asocia el reporte al usuario seleccionado
                            this.actualizarReporteEjecucionUsuario(rre);
                        } else {
                            LOGGER.warn("El usuario que ya está inscrito al reporte.");
                        }
                    }
                }
            }

            try {
                if (!rutaReportesADescargar.isEmpty()) {

                    String rutaZipDocumentos = this.getGeneralesService().
                        crearZipAPartirDeRutaDeDocumentos(rutaReportesADescargar);

                    if (rutaZipDocumentos != null) {
                        InputStream stream;
                        File archivoReportesDescarga = new File(rutaZipDocumentos);
                        if (!archivoReportesDescarga.exists()) {
                            LOGGER.error("Error el archivo de origen no existe");
                            this.addMensajeError("Ocurrió un error al descargar los archivos.");
                            return;
                        }

                        try {
                            stream = new FileInputStream(archivoReportesDescarga);
                            this.reportesDescarga = new DefaultStreamedContent(stream,
                                Constantes.TIPO_MIME_ZIP, generarNombreZip() + ".zip");

                        } catch (FileNotFoundException e) {
                            LOGGER.error(
                                "Archivo no encontrado, no se pudieron descargar los reportes seleccionados. " +
                                e.getMessage());
                        }

                    }
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en la búsqueda de los reportes generados.");
            }
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que valida los tamaños de archivos a exportar previo a generar un .zip para estos.
     *
     * @author david.cifuentes
     */
    public String generarNombreZip() {
        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
        return "Reportes_Radicacion".concat(fechaHora.format(new Date())).trim();
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que valida los tamaños de archivos a exportar previo a generar un .zip para estos.
     *
     * @author david.cifuentes
     */
    /*
     * @modified by leidy.gonzalez:: #21465
     */
    public boolean validarExportarReportes(RepReporteEjecucion[] reportes) {

        BigDecimal sumaTamanioReportes = new BigDecimal(0);
        int resultado;

        for (RepReporteEjecucion rre : reportes) {
            // Estado de los reportes
            if (!ERepReporteEjecucionEstado.FINALIZADO.getCodigo().equals(rre.getEstado())) {
                this.addMensajeError(
                    "Uno de los reportes seleccionados, no se encuentra finalizado y no puede ser exportado");
                return false;
            }

            if (rre.getValorTamano() != null) {

                sumaTamanioReportes = sumaTamanioReportes.add(rre.getValorTamano());
            }

        }

        Parametro maxEnvioJob = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.TAMANO_MAX_DESCARGAS.toString());
        BigDecimal maxTamDescarga = new BigDecimal(maxEnvioJob.getValorNumero());

        resultado = maxTamDescarga.compareTo(sumaTamanioReportes);

        // Tamaño del archivo
        if (resultado == -1) {
            this.addMensajeError(
                "La suma del tamaño de los archivos supera el tamaño permitido para un .zip, por favor verifique");
            return false;
        }
        return true;
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que actualiza la consulta de los reportes generados.
     *
     * @author david.cifuentes
     */
    public void actualizarTabla() {
        this.buscarReporte();
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que genera el código del reporte con base en el tipo de reporte, un consecutivo y el
     * año actual.
     *
     * @author david.cifuentes
     */
    public void generarCodigoReporte() {

        Object[] numeracionReportePredial = this.getGeneralesService()
            .generarNumeracion(ENumeraciones.NUMERACION_ANIO_CONS,
                this.estructuraOrganizacionalCod, "", "", 0);
        this.codigoReporte = (String) numeracionReportePredial[1];
    }

    // ------------------------------------------------------------- //
    /**
     * Método usado en la selección de todos los reportes que se encuentran en la tabla de
     * resultados de búsqueda
     *
     * @author david.cifuentes
     */
    public void seleccionarTodosReportes() {
        if (this.reportesSeleccionados != null &&
             this.reportesSeleccionados.length != 0) {
            for (RepReporteEjecucion rre : this.reportesSeleccionados) {
                //rre.setSelected(this.seleccionarTodosBool);

                if (rre.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO
                    .getCodigo())) {
                    this.banderaExportarReporte = true;
                } else {
                    this.banderaExportarReporte = false;
                }
            }
        }
    }

    /**
     * listener de selección en la tabla de comisiones Actualiza las banderas para determinar qué
     * botones de acción se habilitan según las reglas definidas (ver la documentación de los
     * métodos para determinar cuándo se activa qué botón)
     *
     * @param event
     */
    public void manejarDesactivacionDeBotones() {

        if (this.reportesSeleccionados != null &&
             this.reportesSeleccionados.length != 0) {
            for (RepReporteEjecucion repReporteEjecucion : this.reportesSeleccionados) {

                if (repReporteEjecucion.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO
                    .getCodigo())) {
                    this.banderaExportarReporte = true;
                } else {
                    this.banderaExportarReporte = false;
                }
            }

        } else {
            this.banderaExportarReporte = false;
        }

    }

    // ------------------------------------------------------------- //
    /**
     * Método que realiza el set de la ruta de la previsualización del documento
     * {@link RepReporteEjecucion}
     *
     * @author david.cifuentes
     */
    /*
     * @modificado por leidy.gonzalez
     */
    public void cargarPrevisualizacionReporte() {

        this.rutaPrevisualizacionReporte = new String();

        if (this.reporteVisualizacionSeleccionado != null &&
             this.reporteVisualizacionSeleccionado.getRutaReporteGenerado() != null &&
             !this.reporteVisualizacionSeleccionado.getRutaReporteGenerado().isEmpty()) {

            // Consultar archivo de previsualización
            try {

                //Loading an existing PDF document
                String ruta = this.getConservacionService().
                    descargarOficioDeGestorDocumentalATempLocalMaquina(
                        this.reporteVisualizacionSeleccionado.getRutaReporteGenerado());

                File file = new File(ruta);

                PDDocument documento = PDDocument.load(file);

                //Instantiating Splitter class
                Splitter splitter = new Splitter();

                //splitting the pages of a PDF document
                List<PDDocument> Pages = splitter.split(documento);

                //Creating an iterator 
                Iterator<PDDocument> iterator = Pages.listIterator();

                //Saving each page as an individual document
                int i = 0;
                while (iterator.hasNext()) {
                    if (i == 0) {

                        PDDocument pd = iterator.next();

                        pd.save(ruta);

                        File filePrimeraPagina = new File(ruta);

                        this.rutaPrevisualizacionReporte = this.getGeneralesService().
                            publicarArchivoEnWeb(filePrimeraPagina.getPath());

                        System.out.println("Documento primera pagina: " +
                            this.rutaPrevisualizacionReporte);

                        break;
                    } else {

                        break;
                    }

                }

                documento.close();

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en la carga de la previsualización.");
            }
        } else {
            LOGGER.error("El reporte no fue almacenado en la documental.");
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método que realiza la revisión de los permisos de generación del tipo de reporte seleccionado
     * para habilitar el botón de generar reporte.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public void revisarPermisoGenerarReporte() {
        this.permisoGenerarReporteBool = false;
        int sinPermisoGenerar = 0;

        if (this.tipoReporteId != null &&
             this.territorialSeleccionadaCodigo != null &&
             !this.territorialSeleccionadaCodigo.isEmpty()) {

            for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

                if (repUsuarioRol.getRepReporte().getId().longValue() == this.tipoReporteId.
                    longValue() &&
                     repUsuarioRol.getRepTerritorialReportes() != null) {

                    for (RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()) {

                        if (rtr.getTerritorial().getCodigo().equals(
                            this.territorialSeleccionadaCodigo)) {

                            if (!repUsuarioRol.isPermisoGenerar()) {
                                sinPermisoGenerar++;
                                break;
                            } else {
                                this.permisoGenerarReporteBool = true;
                                return;
                            }

                        }
                    }
                }
            }

            if (sinPermisoGenerar > 0) {
                this.permisoGenerarReporteBool = false;
            }
        }
    }

    /**
     * ---------------------------------------------------------------------
     */
    /**
     * Método cerrar que forza realiza un llamado al método que remueve los managed bean de la
     * sesión y forza el init de tareasPendientesMB.
     *
     * @return
     */
    public String cerrar() {
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }
    // Fin de la clase
}
