/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util.procesos;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Se creó para no evitar aumentar el acoplamiento entre clases propias de lo de procesos con clases
 * que son propias del core del proyecto SNC. Por ejemplo: para evitar que un SolicitudCatastral
 * conozca a un Tramite
 *
 *
 * @author pedro.garcia
 */
public class ProcesosUtilidadesWeb {

    /**
     * llena los datos de un trámite que van en un objeto SolicitudCatastral.
     *
     * OJO: Por ahora no llena el número predial!!
     *
     * @author pedro.garcia
     * @param solicitudCatastral
     * @param tramite
     * @return
     * @deprecated porque cambiaron lo del proceso para que si solo si se quiere sobreescribie
     * valores de la solicitud catastral se haga esto
     */
    @Deprecated
    public static SolicitudCatastral setTramiteInfoEnSolicitudCatastral(
        SolicitudCatastral solicitudCatastral, Tramite tramite) {

        //solicitudCatastral.setIdentificador(tramite.getId());
        //solicitudCatastral.setFechaRadicacion(tramite.getFechaRadicacion());
        //solicitudCatastral.setNumeroRadicacion(tramite.getNumeroRadicacion());
        //solicitudCatastral.setNumeroPredial("");
        //solicitudCatastral.setTipoTramite(tramite.getTipoTramite());
        //solicitudCatastral.setNumeroSolicitud(tramite.getSolicitud().getNumero());
        return solicitudCatastral;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * retorna el objeto que se define como mensaje de los MB que extienden de ProcessFlowManager
     *
     * @author pedro.garcia
     * @param activityId ID DE LA ACTIVIDAD EN EL MOTOR DE PROCESOS
     * @param transition nombre de la transición o actividad a la que va el proceso
     * @param user usuario que se toma para obtener los datos de usuario destino. OJO: si no es el
     * mismo que inicia sesión se debe armar uno nuevo con los datos necesarios ver @see
     * UsuarioDTO#copiarEstructuraOrganizacional
     * @param observaciones
     * @return
     */
    public static ActivityMessageDTO setupIndividualProcessMessage(
        String activityId, String transition, UsuarioDTO user, String observaciones) {

        ActivityMessageDTO answer;
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        answer = new ActivityMessageDTO();
        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();
        usuariosActividad = new ArrayList<UsuarioDTO>();

        answer.setActivityId(activityId);

        usuarioDestinoActividad.copiarEstructuraOrganizacional(user);
        usuarioDestinoActividad.setLogin(user.getLogin());
        usuarioDestinoActividad.setRoles(user.getRoles());
        usuariosActividad.add(usuarioDestinoActividad);
        answer.setComment(observaciones);
        //D: según alejandro.sanchez la transición que importa es la que se define en la
        //   solicitud catastral; sin embargo, por si acaso se hace también en el answer
        answer.setTransition(transition);
        solicitudCatastral.setTransicion(transition);

        solicitudCatastral.setUsuarios(usuariosActividad);
        answer.setSolicitudCatastral(solicitudCatastral);

        return answer;
    }

//end of class
}
