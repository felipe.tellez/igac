package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso CU-SA-AC-081 Desasignar Avaluador
 *
 * @author felipe.cadena
 */
@Component("desasignacionAvaluador")
@Scope("session")
public class DesasignacionAvaluadorMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = 6277978758758305628L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaCargaTrabajoMB.class);
    /**
     * Usuario en sesión
     */
    private UsuarioDTO usuario;
    /**
     * Número de sec_radicado para la busqueda
     */
    private String secRadicado;
    /**
     * Fecha en la que se realiza la desasignación
     */
    private Date fechaDesasignacion;
    /**
     * Nombre del profesional para el patron de busqueda.
     */
    private String nombreProfesional;
    /**
     * Avaluos seleccionado para detalles.
     */
    private Long idProfesional;
    /**
     * Avaluos seleccionado para detalles.
     */
    private Avaluo avaluoSeleccionado;

    /**
     * Bandera para determinar si el CU se accede desde menu (true) o desde otro CU (false)
     */
    private Boolean banderaLlamadoMenu;
    /**
     * Lista de avalúos resultado de la busqueda
     */
    private List<Avaluo> listaAvaluos;

    /**
     * Lista de avaluadores de la territorial en sesion
     */
    private List<SelectItem> listaAvaluadores;

    /**
     * Lista de avalúos que se van a desasignar
     */
    private Avaluo[] listaAvaluosSeleccionados;

    // --------getters-setters----------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Long getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(Long idProfesional) {
        this.idProfesional = idProfesional;
    }

    public List<SelectItem> getListaAvaluadores() {
        return listaAvaluadores;
    }

    public void setListaAvaluadores(List<SelectItem> listaAvaluadores) {
        this.listaAvaluadores = listaAvaluadores;
    }

    public Boolean getBanderaLlamadoMenu() {
        return banderaLlamadoMenu;
    }

    public void setBanderaLlamadoMenu(Boolean banderaLlamadoMenu) {
        this.banderaLlamadoMenu = banderaLlamadoMenu;
    }

    public List<Avaluo> getListaAvaluos() {
        return listaAvaluos;
    }

    public void setListaAvaluos(List<Avaluo> listaAvaluos) {
        this.listaAvaluos = listaAvaluos;
    }

    public Avaluo[] getListaAvaluosSeleccionados() {
        return listaAvaluosSeleccionados;
    }

    public void setListaAvaluosSeleccionados(Avaluo[] listaAvaluosSeleccionados) {
        this.listaAvaluosSeleccionados = listaAvaluosSeleccionados;
    }

    public Avaluo getAvaluoSeleccionado() {
        return avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public String getSecRadicado() {
        return this.secRadicado;
    }

    public void setSecRadicado(String secRadicado) {
        this.secRadicado = secRadicado;
    }

    public Date getFechaDesasignacion() {
        return this.fechaDesasignacion;
    }

    public void setFechaDesasignacion(Date fechaDesasignacion) {
        this.fechaDesasignacion = fechaDesasignacion;
    }

    public String getNombreProfesional() {
        return this.nombreProfesional;
    }

    public void setNombreProfesional(String nombreProfesional) {
        this.nombreProfesional = nombreProfesional;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on DesasignacionAvaluadorMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
        this.fechaDesasignacion = Calendar.getInstance().getTime();
        this.banderaLlamadoMenu = true;
        this.cargarAvaluadoresTerritorial();
    }

    /**
     * Método para acceder al CU desde otros casos de uso, se debe enviar el nombre del profesional
     * asociado y el número de sec_radicado
     *
     * @author felipe.cadena
     */
    public void llamarDesdeOtroCU(String nombreProfesioal, String secRadicado) {
        this.nombreProfesional = nombreProfesioal;
        this.secRadicado = secRadicado;
        this.banderaLlamadoMenu = false;
    }

    /**
     * Método para desasignar los tramites seleccionados
     *
     * @author felipe.cadena
     */
    public void desasignarAvaluador() {
        LOGGER.debug("Desasignando avaluadores...");

        Long avaluadorId;

        for (Avaluo avaluo : listaAvaluosSeleccionados) {
            avaluadorId = avaluo.getAvaluoAsignacionProfesionals().
                get(0).getProfesionalAvaluo().getId();

            this.getAvaluosService().desasignarAvaluador(avaluo, avaluadorId);
            LOGGER.debug("ava>>> " + avaluo.getSecRadicado());
        }

        this.buscarAvaluos();

        LOGGER.debug("Avaluadores desasignados");
    }

    /**
     * Método para buscar los tramites a desasignar
     *
     * @author felipe.cadena
     */
    public void buscarAvaluos() {
        LOGGER.debug("Buscando avaluos...");
        this.listaAvaluos = this.getAvaluosService().
            consultarAvaluosAsignacion(this.idProfesional, this.secRadicado);
        this.filtrarActividades();
        if (!"".equals(this.nombreProfesional)) {
            this.procesarAvaluadores();
        }

        LOGGER.debug("Busqueda terminada");
    }

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los profesionales de avalúos
     * asociados a una territorial
     *
     * @author felipe.cadena
     * @return
     */
    public void cargarAvaluadoresTerritorial() {
        LOGGER.debug("cargando avaluadores...");
        this.listaAvaluadores = new ArrayList<SelectItem>();
        List<ProfesionalAvaluo> lpa = this.getAvaluosService().
            buscarAvaluadoresPorTerritorial(this.usuario.getCodigoTerritorial());
        for (ProfesionalAvaluo profesional : lpa) {
            listaAvaluadores.add(
                new SelectItem(profesional.getId(), profesional.getNombreCompleto()));
        }
        LOGGER.debug("carga terminada");

    }

    /**
     * Método para filtrar los avalúos que estan en actividad de proceso valida para poder realizar
     * la desasignación
     *
     * @author felipe.cadena
     *
     */
    private void filtrarActividades() {
        LOGGER.debug("Inicio DesasignacionAvaluadorMB#filtrarActividades");
        List<Avaluo> avaluosFiltrados = new ArrayList<Avaluo>();
        if (this.banderaLlamadoMenu) {
            for (Avaluo avaluo : this.listaAvaluos) {
//TODO :: felipe.cadena::pendiente definir las actividades del process para avalúos
                //if(avaluo.getTramite().getActividadActualTramite().getNombre().equals())
            }
        } else {

        }

        LOGGER.debug("Fin DesasignacionAvaluadorMB#filtrarActividades");
    }

    /**
     * Método para buscar los tramites a desasignar
     *
     * @author felipe.cadena
     */
    public void procesarAvaluadores() {
        LOGGER.debug("procesando avaluadores...");

        AvaluoAsignacionProfesional aapAux = null;
        for (Avaluo avaluo : this.listaAvaluos) {
            for (AvaluoAsignacionProfesional aap : avaluo.getAvaluoAsignacionProfesionals()) {
                if (aap.getProfesionalAvaluo().getId() == this.idProfesional) {
                    aapAux = aap;
                }
            }

            avaluo.getAvaluoAsignacionProfesionals().clear();
            LOGGER.debug("Avaluador >>> " + aapAux);
            avaluo.getAvaluoAsignacionProfesionals().add(aapAux);
        }

        LOGGER.debug("Proceso terminado");
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando DesasignacionAvaluadorMB#cerrar");

        UtilidadesWeb.removerManagedBean("desasignacionAvaluador");
        UtilidadesWeb.removerManagedBean("consultaInformacionPredial");
        UtilidadesWeb.removerManagedBean("consultarDptoMunpioAvaluo");

        LOGGER.debug("finalizando DesasignacionAvaluadorMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
