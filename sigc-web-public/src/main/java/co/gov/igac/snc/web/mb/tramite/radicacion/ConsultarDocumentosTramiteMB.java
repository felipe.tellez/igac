package co.gov.igac.snc.web.mb.tramite.radicacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.web.util.SNCManagedBean;

@Component("consultaDocumentosTramite")
@Scope("session")
public class ConsultarDocumentosTramiteMB extends SNCManagedBean implements Serializable {

    /**
     * Clase MB para la consulta de la lista de documentos asociados a un tramite
     *
     * Se debe recibir como entrada el nùmero de radicación
     *
     * @author juan.agudelo
     */
    private static final long serialVersionUID = -6687769632700005947L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultarDocumentosTramiteMB.class);

    // --------------------------------------------------------------------------------------------------
    private List<TramiteDocumentacion> documentos;

    private TramiteDocumentacion tramiteDocumentacion;
    private List<TramiteDocumentacion> documentosFaltantes;
    private List<TramiteDocumentacion> documentosAportados;
    private List<TramiteDocumentacion> documentosAdicionalesAportados;

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        String numeroRadicacion = "02-560-825-2009";

        tramiteDocumentacion = new TramiteDocumentacion();
        this.documentos = new ArrayList<TramiteDocumentacion>();
        this.documentosAdicionalesAportados = new ArrayList<TramiteDocumentacion>();
        this.documentosAportados = new ArrayList<TramiteDocumentacion>();
        this.documentosFaltantes = new ArrayList<TramiteDocumentacion>();

        /*
         * documentos = tramiteService.findTramiteDocumentosByNoRadicacion(
         * noRadicacion).getTramiteDocumentacions();
         */
        documentos = this.getTramiteService().buscarDocumentosTramitePorNumeroDeRadicacion(
            numeroRadicacion);

        if (documentos != null) {

            for (int i = 0; i < documentos.size(); i++) {
                TramiteDocumentacion temp = documentos.get(i);

                if (temp.getAportado().equals("NO")) {
                    documentosFaltantes.add(temp);

                } else if (temp.getAdicional().equals("NO")) {
                    documentosAportados.add(temp);
                } else {
                    documentosAdicionalesAportados.add(temp);

                }//documentosFaltantes.get(1).getTipoDocumento().getNombre();
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    public List<TramiteDocumentacion> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<TramiteDocumentacion> documentos) {
        this.documentos = documentos;
    }

    public TramiteDocumentacion getTramiteDocumentacion() {
        return tramiteDocumentacion;
    }

    public void setTramiteDocumentacion(
        TramiteDocumentacion tramiteDocumentacion) {
        this.tramiteDocumentacion = tramiteDocumentacion;
    }

    public List<TramiteDocumentacion> getDocumentosFaltantes() {
        return documentosFaltantes;
    }

    public void setDocumentosFaltantes(
        List<TramiteDocumentacion> documentosFaltantes) {
        this.documentosFaltantes = documentosFaltantes;
    }

    public List<TramiteDocumentacion> getDocumentosAportados() {
        return documentosAportados;
    }

    public void setDocumentosAportados(
        List<TramiteDocumentacion> documentosAportados) {
        this.documentosAportados = documentosAportados;
    }

    public List<TramiteDocumentacion> getDocumentosAdicionalesAportados() {
        return documentosAdicionalesAportados;
    }

    public void setDocumentosAdicionalesAportados(
        List<TramiteDocumentacion> documentosAdicionales) {
        this.documentosAdicionalesAportados = documentosAdicionales;
    }
}
