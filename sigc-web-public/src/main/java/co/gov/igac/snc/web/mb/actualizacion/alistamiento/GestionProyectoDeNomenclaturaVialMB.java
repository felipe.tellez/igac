package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionNomenclaturaVia;
import co.gov.igac.snc.persistence.entity.actualizacion.NomenclaturaVialCondicion;
import co.gov.igac.snc.persistence.entity.actualizacion.NomenclaturaVialEntidad;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @description Managed bean para la gestión y radicación de un proyecto de acuerdo de nomenclatura
 * vial con fines catastrales.
 *
 * @author david.cifuentes
 *
 */
@Component("gestionarProyectoDeNomenclaturaVial")
@Scope("session")
public class GestionProyectoDeNomenclaturaVialMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionProyectoDeNomenclaturaVialMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIO ------------- */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable usada para definir el orden de los departamentos.
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;

    /**
     * Lista de select items con los departamentos de colombia
     */
    private List<SelectItem> departamentosColombia;

    /**
     * Variable usada para definir una nueva norma.
     */
    private String nuevaNorma;

    /**
     * Lista de normas a asociar a la actualizacionNomenclaturaVia.
     */
    private List<String> normasTabla;

    /**
     * Variable que almacena la norma seleccionada
     */
    private String selectedNorma;

    /**
     * Variable que almacena el nombre del alcalde a guardar en la actualizacionNomenclaturaVia.
     */
    private String nombreAlcalde;

    /**
     * Variable que almacena la referencia para las calles.
     */
    private String referenciaCalles;

    /**
     * Variable que almacena la referencia para las carreras.
     */
    private String referenciaCarreras;

    /**
     * Variable que almacena el punto de partida.
     */
    private String puntoPartida;

    /**
     * Variable que almacena los casos conocidos de vías de uso privado.
     */
    private String casosConocidos;

    /**
     * Actualización seleccionada del arbol
     */
    private Actualizacion actualizacionSeleccionada;
    /**
     * Usuario logueado
     */
    private UsuarioDTO usuario;

    /**
     * Variable de actualizacion nomenclatura vial a persistir.
     */
    private ActualizacionNomenclaturaVia actualizacionNomenclaturaVia;

    /**
     * Variable usada para almacenar la nueva condicional
     */
    private NomenclaturaVialCondicion nuevaCondicionAdicional;

    /**
     * Lista de condicionales relacionadas
     */
    private List<NomenclaturaVialCondicion> listaCondicionesAdicionales;

    /**
     * Variable usada para almacenar la nueva entidad
     */
    private NomenclaturaVialEntidad nuevaEntidadRelacionada;

    /**
     * Lista de entidades relacionadas
     */
    private List<NomenclaturaVialEntidad> listaEntidadesRelacionadas;

    /**
     * Variable booleana para saber si se está editando o no una condición.
     */
    private boolean editModeBool;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de
     */
    private ReporteDTO reporte;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        // TODO :: Modificar el set de la actualización cuando se integre
        // process :: 24/10/12
        // Obtener la actualizacióin del arbol de tareas.
        this.actualizacionSeleccionada = this.getActualizacionService()
            .recuperarActualizacionPorId(450L);

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // Crear la nomenclatura vial sobre la cual se va a trabajar.
        if (this.actualizacionSeleccionada != null && this.usuario != null) {

            this.actualizacionNomenclaturaVia = new ActualizacionNomenclaturaVia();
            this.actualizacionNomenclaturaVia
                .setActualizacion(this.actualizacionSeleccionada);
            this.actualizacionNomenclaturaVia.setUsuarioLog(this.usuario
                .getLogin());
            this.actualizacionNomenclaturaVia.setFechaLog(new Date());

            // Guardar en la base de datos
            this.actualizacionNomenclaturaVia = this.getActualizacionService()
                .guardarYActualizarActualizacionNomenclaturaVia(
                    this.actualizacionNomenclaturaVia);
        }

        // Inicialización de variables
        normasTabla = new ArrayList<String>();
        nuevaNorma = new String();
        listaCondicionesAdicionales = new ArrayList<NomenclaturaVialCondicion>();
        listaEntidadesRelacionadas = new ArrayList<NomenclaturaVialEntidad>();
        nuevaCondicionAdicional = new NomenclaturaVialCondicion();
        nuevaEntidadRelacionada = new NomenclaturaVialEntidad();

        // Cargue de departamentos
        cargarDepartamentosColombia();
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public String getNuevaNorma() {
        return nuevaNorma;
    }

    public void setNuevaNorma(String nuevaNorma) {
        this.nuevaNorma = nuevaNorma;
    }

    public Actualizacion getActualizacionSeleccionada() {
        return actualizacionSeleccionada;
    }

    public void setActualizacionSeleccionada(
        Actualizacion actualizacionSeleccionada) {
        this.actualizacionSeleccionada = actualizacionSeleccionada;
    }

    public List<SelectItem> getDepartamentosColombia() {
        return departamentosColombia;
    }

    public void setDepartamentosColombia(List<SelectItem> departamentosColombia) {
        this.departamentosColombia = departamentosColombia;
    }

    public boolean isEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<NomenclaturaVialCondicion> getListaCondicionesAdicionales() {
        return listaCondicionesAdicionales;
    }

    public void setListaCondicionesAdicionales(
        List<NomenclaturaVialCondicion> listaCondicionesAdicionales) {
        this.listaCondicionesAdicionales = listaCondicionesAdicionales;
    }

    public ActualizacionNomenclaturaVia getActualizacionNomenclaturaVia() {
        return actualizacionNomenclaturaVia;
    }

    public void setActualizacionNomenclaturaVia(
        ActualizacionNomenclaturaVia actualizacionNomenclaturaVia) {
        this.actualizacionNomenclaturaVia = actualizacionNomenclaturaVia;
    }

    public String getSelectedNorma() {
        return selectedNorma;
    }

    public void setSelectedNorma(String selectedNorma) {
        this.selectedNorma = selectedNorma;
    }

    public NomenclaturaVialCondicion getNuevaCondicionAdicional() {
        return nuevaCondicionAdicional;
    }

    public void setNuevaCondicionAdicional(
        NomenclaturaVialCondicion nuevaCondicionAdicional) {
        this.nuevaCondicionAdicional = nuevaCondicionAdicional;
    }

    public NomenclaturaVialEntidad getNuevaEntidadRelacionada() {
        return nuevaEntidadRelacionada;
    }

    public void setNuevaEntidadRelacionada(
        NomenclaturaVialEntidad nuevaEntidadRelacionada) {
        this.nuevaEntidadRelacionada = nuevaEntidadRelacionada;
    }

    public List<NomenclaturaVialEntidad> getListaEntidadesRelacionadas() {
        return listaEntidadesRelacionadas;
    }

    public void setListaEntidadesRelacionadas(
        List<NomenclaturaVialEntidad> listaEntidadesRelacionadas) {
        this.listaEntidadesRelacionadas = listaEntidadesRelacionadas;
    }

    public String getNombreAlcalde() {
        return nombreAlcalde;
    }

    public void setNombreAlcalde(String nombreAlcalde) {
        this.nombreAlcalde = nombreAlcalde;
    }

    public List<String> getNormasTabla() {
        return normasTabla;
    }

    public void setNormasTabla(List<String> normasTabla) {
        this.normasTabla = normasTabla;
    }

    public String getReferenciaCalles() {
        return referenciaCalles;
    }

    public void setReferenciaCalles(String referenciaCalles) {
        this.referenciaCalles = referenciaCalles;
    }

    public String getReferenciaCarreras() {
        return referenciaCarreras;
    }

    public void setReferenciaCarreras(String referenciaCarreras) {
        this.referenciaCarreras = referenciaCarreras;
    }

    public String getPuntoPartida() {
        return puntoPartida;
    }

    public void setPuntoPartida(String puntoPartida) {
        this.puntoPartida = puntoPartida;
    }

    public String getCasosConocidos() {
        return casosConocidos;
    }

    public void setCasosConocidos(String casosConocidos) {
        this.casosConocidos = casosConocidos;
    }

    public ReporteDTO getReporte() {
        return reporte;
    }

    public void setReporte(ReporteDTO reporte) {
        this.reporte = reporte;
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método que adiciona una condicional a la tabla de condicionales adicionales.
     *
     * @author david.cifuentes
     */
    public void agregarCondicionAdicional() {
        if (this.nuevaCondicionAdicional.getTexto() == null ||
             this.nuevaCondicionAdicional.getTexto().trim().isEmpty()) {
            this.addMensajeWarn("Por favor ingrese el valor para la condicional.");
            return;
        }

        if (!listaCondicionesAdicionales.contains(this.nuevaCondicionAdicional)) {
            this.nuevaCondicionAdicional.setFechaLog(new Date());
            this.nuevaCondicionAdicional.setUsuarioLog(this.usuario.getLogin());
            this.nuevaCondicionAdicional
                .setActualizacionNomenclaturaVia(this.actualizacionNomenclaturaVia);

            this.listaCondicionesAdicionales.add(this.nuevaCondicionAdicional);
            this.nuevaCondicionAdicional = new NomenclaturaVialCondicion();
            this.addMensajeInfo("Condicional adicional asociada satisfactoriamente.");
            this.editModeBool = false;
        } else {
            this.nuevaCondicionAdicional = new NomenclaturaVialCondicion();
            this.addMensajeInfo("Condicional adicional modificada satisfactoriamente.");
            this.editModeBool = false;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que retira una condicional adicional de la tabla de condicionales asociadas.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public List<NomenclaturaVialCondicion> quitarCondicionAdicional() {
        listaCondicionesAdicionales.remove(this.nuevaCondicionAdicional);
        this.nuevaCondicionAdicional = new NomenclaturaVialCondicion();
        return listaCondicionesAdicionales;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que adiciona una entidad a la tabla de entidades relacionadas.
     *
     * @author david.cifuentes
     */
    public void agregarEntidadRelacionada() {

        if (this.nuevaEntidadRelacionada.getEntidadMunicipal() == null ||
             this.nuevaEntidadRelacionada.getEntidadMunicipal() == null) {
            this.addMensajeWarn("Por favor seleccione una entidad.");
            return;
        }

        if (!listaEntidadesRelacionadas.contains(this.nuevaEntidadRelacionada)) {
            this.nuevaEntidadRelacionada.setFechaLog(new Date());
            this.nuevaEntidadRelacionada.setUsuarioLog(this.usuario.getLogin());
            this.nuevaEntidadRelacionada
                .setActualizacionNomenclaturaVia(this.actualizacionNomenclaturaVia);
            listaEntidadesRelacionadas.add(this.nuevaEntidadRelacionada);
            this.nuevaEntidadRelacionada = new NomenclaturaVialEntidad();
            this.addMensajeInfo("Relación de entidad asociada satisfactoriamente.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que retira una entidad relacionada de la tabla de entidades.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public List<NomenclaturaVialEntidad> quitarEntidadRelacionada() {
        listaEntidadesRelacionadas.remove(this.nuevaEntidadRelacionada);
        this.nuevaEntidadRelacionada = new NomenclaturaVialEntidad();
        return listaEntidadesRelacionadas;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que adiciona una norma a la tabla de normas incluidas.
     *
     * @author david.cifuentes
     */
    public void agregarNorma() {

        if (this.nuevaNorma == null || this.nuevaNorma.trim().isEmpty()) {
            this.addMensajeWarn("Por favor seleccione una norma.");
            return;
        }
        if (!normasTabla.contains(this.nuevaNorma)) {
            normasTabla.add(this.nuevaNorma);
            this.nuevaNorma = "";
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que retira una norma de la tabla de normas incluidas.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public List<String> desvincularNorma() {
        normasTabla.remove(this.selectedNorma);
        return normasTabla;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el cargue de departamentos en el menú de normas.
     *
     * @author david.cifuentes
     */
    public void cargarDepartamentosColombia() {
        this.departamentosColombia = new LinkedList<SelectItem>();
        this.departamentosColombia.add(new SelectItem("", "Seleccionar..."));

        List<Departamento> departamentosDeColombia = this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(departamentosDeColombia);
        } else {
            Collections.sort(departamentosDeColombia,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : departamentosDeColombia) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                departamentosColombia.add(new SelectItem(departamento
                    .getCodigo() + "-" + departamento.getNombre(),
                    departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                departamentosColombia.add(new SelectItem(departamento
                    .getCodigo() + "-" + departamento.getNombre(),
                    departamento.getNombre()));

            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que genera el reporte del proyecto de acuerdo de nomenclatura vial.
     *
     * @author david.cifuentes
     */
    public void generarReporteProyectoDeAcuerdo() throws Exception {

        LOGGER.debug("REPORTE : GenerarReporteProyectoDeAcuerdo");
        // Nombre del reporte
        EReporteServiceSNC enumeracionReporte =
            EReporteServiceSNC.GESTION_PROYECTO_NOMENCLATURA_VIAL_PROYECTO_ACUERDO;

        // Reemplazar parametros para el reporte.
        Map<String, String> parameters = replaceReportsParameters();

        this.reporte = this.reportsService.generarReporte(
            parameters, enumeracionReporte, this.usuario);

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que reemplaza los parametros para el reporte.
     *
     * @author david.cifuentes
     */
    public Map<String, String> replaceReportsParameters() {

        LOGGER.debug("REPORTE :: Replace parameters");
        Map<String, String> parameters = new HashMap<String, String>();

        try {
            parameters.put("NUMERO_RADICADO", "");
            parameters.put("ACTUALIZACION_ID", "" +
                 this.actualizacionSeleccionada.getId());
            parameters.put("NOMBRE_ALCALDE", "REVISAR");
            parameters.put("ROL_USUARIO", "" + this.usuario.getRolesCadena());

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            return null;
        }

        return parameters;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que guarda el proyecto de acuerdo.
     *
     * @author david.cifuentes
     */
    public void guardarProyectoDeAcuerdo() {

        if (this.actualizacionNomenclaturaVia != null) {
            // Set de las variables gestionadas en la pantalla para actualizar
            // la nomenclatura vial.
            this.actualizacionNomenclaturaVia
                .setCasosViasUsoPrivado(this.casosConocidos);
            this.actualizacionNomenclaturaVia
                .setEjeReferenciaCalles(this.referenciaCalles);
            this.actualizacionNomenclaturaVia
                .setEjeReferenciaCarreras(this.referenciaCarreras);
            this.actualizacionNomenclaturaVia.setFechaLog(new Date());
            this.actualizacionNomenclaturaVia
                .setNombreAlcalde(this.nombreAlcalde);
            this.actualizacionNomenclaturaVia
                .setNomenclaturaVialCondicions(this.listaCondicionesAdicionales);
            this.actualizacionNomenclaturaVia
                .setNomenclaturaVialEntidads(this.listaEntidadesRelacionadas);
            this.actualizacionNomenclaturaVia
                .setPuntoPartidaCarreraPrimera(this.puntoPartida);

            // TODO :: david.cifuentes :: Revisar porque la pantalla lo plantea
            // como lista y el modelo de datos como atributo :: 25/10/12
            if (this.normasTabla != null && !this.normasTabla.isEmpty()) {
                // this.actualizacionNomenclaturaVia
                // .setNormasIncluidas(this.normasTabla.get(0));
            }

            try {
                // Actualización de la nomenclatura vial

                this.actualizacionNomenclaturaVia = this
                    .getActualizacionService()
                    .guardarYActualizarActualizacionNomenclaturaVia(
                        this.actualizacionNomenclaturaVia);
            } catch (Exception ex) {
                this.addMensajeError("Error al guardar los datos de la nomenclatura vial.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                throw SNCWebServiceExceptions.EXCEPCION_0006.getExcepcion(
                    LOGGER, ex);
            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que guarda la información del proyecto de auerdo en la base de datos y genera el
     * reporte para su visualización.
     *
     * @author david.cifuentes
     */
    public void guardarYVisualizarReporte() {
        try {
            // Guardar información ingreada en la base de datos
            this.guardarProyectoDeAcuerdo();
            // Generar reporte
            this.generarReporteProyectoDeAcuerdo();

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error en guardarYVisualizarReporte", e);
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para editar y actualizar una condicional.
     *
     * @author david.cifuentes
     */
    public void editarCondicional() {
        this.editModeBool = true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que radica el proyecto de acuerdo y avanza en el proceso.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String radicarYAvanzarProceso() {
        // TODO :: david.cifuentes :: Implementar avance cuando se defina
        // process :: 24/10/12

        try {
            this.tareasPendientesMB.init();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Error en el método radicar y avanzar proceso.");
            LOGGER.error(e.getMessage(), e);
        }
        return ConstantesNavegacionWeb.INDEX;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Action sobre el botón 'cerrar', redirige el arbol de tareas removiendo el managed bean
     * actual.
     *
     * @author david.cifuentes
     */
    public String cerrarPaginaPrincipal() {

        UtilidadesWeb.removerManagedBean("gestionarProyectoDeNomenclaturaVial");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

}
