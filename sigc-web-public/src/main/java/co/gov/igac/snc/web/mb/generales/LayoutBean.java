package co.gov.igac.snc.web.mb.generales;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.CloseEvent;
import org.primefaces.event.ResizeEvent;
import org.primefaces.event.ToggleEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("layoutBean")
@Scope("session")
public class LayoutBean implements Serializable {

    public void handleClose(CloseEvent event) {

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unit Closed",
            "Closed unit id:'" + event.getComponent().getId() + "'");
        addMessage(message);
    }

    public void handleToggle(ToggleEvent event) {

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().
            getId() + " toggled", "Status:" + event.getVisibility().name());
        addMessage(message);
    }

    public void handleResize(ResizeEvent event) {

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().
            getId() + " resized", "Width:" + event.getWidth() + ", Height:" + event.getHeight());
        addMessage(message);
    }

    private void addMessage(FacesMessage message) {

        FacesContext.getCurrentInstance().addMessage(null, message);

    }
}
