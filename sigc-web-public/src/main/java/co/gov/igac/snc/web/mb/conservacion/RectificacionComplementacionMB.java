/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.ComisionTramiteDato;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EComisionTramiteDatoDato;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.ValidarCompYRect;
import java.util.Arrays;

/**
 * Managed bean para habilitar y deshabilitar campos en rectificacion y complementacion
 *
 * @author fredy.wilches
 */
@Component("rc")
@Scope("session")
public class RectificacionComplementacionMB extends SNCManagedBean {

    private enum ECampoRC implements Serializable {
        PERSONA_TIPO_IDENTIFICACION(1),
        PERSONA_NUMERO_IDENTIFICACION(2),
        PERSONA_PRIMER_APELLIDO(3),
        PERSONA_SEGUNDO_APELLIDO(4),
        PERSONA_PRIMER_NOMBRE(5),
        PERSONA_SEGUNDO_NOMBRE(6),
        PERSONA_RAZON_SOCIAL(7),
        PERSONA_SIGLA(8),
        //FICHA_MATRIZ_TOTAL_UNIDADES_PRIVADAS(9),
        //FICHA_MATRIZ_TORRE_PISOS(10),
        //UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION(11),
        FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA(12),
        FICHA_MATRIZ_AREA_TOTAL_TERRENO_COMUN(13),
        //FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_PRIVADA(14),
        UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA(14),
        FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_COMUN(15),
        //PREDIO_AREA_TERRENO(16),
        //PREDIO_AREA_CONSTRUCCION(17),
        PERSONA_PREDIO_PARTICIPACION(18),
        PERSONA_PREDIO_TIPO(19),
        PREDIO_NUMERO_REGISTRO(20),
        PREDIO_TIPO(21),
        //PREDIO_ZONA_ZONA_FISICA(22),
        //PREDIO_ZONA_ZONA_GEOECONOMICA(23),
        UNIDAD_CONSTRUCCION_USO_ID(24),
        UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION_COMP(25),
        UNIDAD_CONSTRUCCION_PISO_UBICACION(26),
        UNIDAD_CONSTRUCCION_TOTAL_PISOS_UNIDAD(27),
        UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES_COMP(28),
        UNIDAD_CONSTRUCCION_TOTAL_BANIOS_COMP(29),
        UNIDAD_CONSTRUCCION_TOTAL_LOCALES_COMP(30),
        UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION_COMP(31),
        UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION(32),
        PREDIO_DESTINO(33),
        PREDIO_DIRECCION_DIRECCION(34),
        FICHA_MATRIZ_PREDIO_COEFICIENTE(35),
        PREDIO_DIRECCION_PRINCIPAL(36),
        FOTO_COMP(37),
        PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION(38),
        PERSONA_PREDIO_PROPIEDAD_VALOR(39),
        PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO(40),
        //UNIDAD_CONSTRUCCION_AREA_UNIDAD_CONSTRUIDA(41),
        PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA(41),
        PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO(42),
        PERSONA_PREDIO_PROPIEDAD_MUNICIPIO(43),
        PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO(44),
        PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO(45),
        PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO(46),
        UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION(47),
        PREDIO_ZONA_ZONA_FISICA(48),
        PREDIO_ZONA_ZONA_GEOECONOMICA(49),
        UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION(50),
        UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES(51),
        UNIDAD_CONSTRUCCION_TOTAL_BANIOS(52),
        UNIDAD_CONSTRUCCION_TOTAL_LOCALES(53),
        //UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA(54),             
        UNIDAD_CONSTRUCCION_DIMENSION(54),
        FOTO(55),
        PERSONA_TIPO_IDENTIFICACION_COMP(56),
        PERSONA_NUMERO_IDENTIFICACION_COMP(57),
        PERSONA_PRIMER_APELLIDO_COMP(58),
        PERSONA_SEGUNDO_APELLIDO_COMP(59),
        PERSONA_PRIMER_NOMBRE_COMP(60),
        PERSONA_SEGUNDO_NOMBRE_COMP(61),
        PERSONA_RAZON_SOCIAL_COMP(62),
        PERSONA_PREDIO_PARTICIPACION_COMP(63),
        PERSONA_SIGLA_COMP(64),
        PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION_COMP(65),
        PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO_COMP(66),
        PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO_COMP(67),
        PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA_COMP(68),
        PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO_COMP(69),
        PERSONA_PREDIO_PROPIEDAD_MUNICIPIO_COMP(70),
        PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO_COMP(71),
        PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO_COMP(72),
        PREDIO_NUMERO_REGISTRO_COMP(73),
        PREDIO_TIPO_COMP(74),
        UNIDAD_CONSTRUCCION_USO_ID_COMP(75),
        PREDIO_DESTINO_COMP(76),
        PREDIO_DIRECCION_DIRECCION_COMP(77),
        FICHA_MATRIZ_PREDIO_COEFICIENTE_COMP(78),
        PREDIO_DIRECCION_PRINCIPAL_COMP(79),
        PERSONA_PREDIO_PROPIEDAD_VALOR_COMP(80),
        UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION_COMP(81);

        private Integer id;

        ECampoRC(Integer id) {
            this.id = id;
        }

        public Integer getId() {
            return id;
        }

    }

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RectificacionComplementacionMB.class);

    // --------------------------------- Servicios ----------------------------
    @Autowired
    private EjecucionMB ejecucionMB;

    /**
     * Trámite seleccionado
     */
    private Tramite tramiteSeleccionado;
    private boolean rectificacionComplementacion;
    private boolean desenglobeEtapas;
    private boolean cancelacionMasiva;
    private boolean rectificacionMatriz;
    private List<TramiteRectificacion> rectificaciones;
    private List<ComisionTramiteDato> comisionesTramiteDato;
    private boolean esRC[] = new boolean[90];
    private boolean esRCMatriz[] = new boolean[90];

    private boolean habilitarTabRectificacion = true;
    private boolean englobeVirtual;

    public boolean isHabilitarTabRectificacion() {
        return habilitarTabRectificacion;
    }

    public void setHabilitarTabRectificacion(boolean habilitarTabRectificacion) {
        this.habilitarTabRectificacion = habilitarTabRectificacion;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init de RectificacionComplementacionMB");
        this.tramiteSeleccionado = ejecucionMB.getTramiteSeleccionado();
        this.rectificacionComplementacion = this.tramiteSeleccionado.getTipoTramite().equals(
            ETramiteTipoTramite.RECTIFICACION.getCodigo()) || this.tramiteSeleccionado.
            getTipoTramite().equals(ETramiteTipoTramite.COMPLEMENTACION.getCodigo()) ||
            this.tramiteSeleccionado.getTipoTramite().equals(ETramiteTipoTramite.REVISION_DE_AVALUO.
                getCodigo());
        this.desenglobeEtapas = this.tramiteSeleccionado.isDesenglobeEtapas();
        this.cancelacionMasiva = this.tramiteSeleccionado.isCancelacionMasiva();
        this.rectificacionMatriz = this.tramiteSeleccionado.isRectificacionMatriz();
        this.englobeVirtual = this.tramiteSeleccionado.isEnglobeVirtual();

        if (this.tramiteSeleccionado.isRectificacionMatriz()) {
            //felipe.cadena::#13167::22-06-2015::Inicializacion de campos para rectificaciones de ficha matriz
            rectificaciones = this.getTramiteService().obtenerTramiteRectificacionesDeTramite(
                this.tramiteSeleccionado.getId());
            for (TramiteRectificacion tr : rectificaciones) {
                esRCMatriz[tr.getDatoRectificar().getId().intValue()] = true;
                if (tr.getDatoRectificar().getId().intValue() ==
                    ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()) {
                    esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()] = true;
                }
                if (tr.getDatoRectificar().getId().intValue() ==
                    ECampoRC.UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION.getId()) {
                    esRC[ECampoRC.UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION.getId()] = true;
                }
            }

        } else if (this.rectificacionComplementacion) {
            if (this.tramiteSeleccionado.getTipoTramite().equals(ETramiteTipoTramite.RECTIFICACION.
                getCodigo()) || this.tramiteSeleccionado.getTipoTramite().equals(
                    ETramiteTipoTramite.COMPLEMENTACION.getCodigo())) {
                rectificaciones = this.getTramiteService().obtenerTramiteRectificacionesDeTramite(
                    this.tramiteSeleccionado.getId());
                for (TramiteRectificacion tr : rectificaciones) {
                    esRC[tr.getDatoRectificar().getId().intValue()] = true;
                }
            } else {
                this.comisionesTramiteDato = this.getTramiteService().
                    buscarComisionTramiteDatoPorTramiteId(this.tramiteSeleccionado.getId());
                for (ComisionTramiteDato ctd : this.comisionesTramiteDato) {
                    if (ctd.getModificado().equals(ESiNo.SI.getCodigo())) {
                        if (ctd.getDato().equals(EComisionTramiteDatoDato.LOCALIZACION_PREDIO.
                            getValor())) {
                            esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()] = true;
                        }
                        if (ctd.getDato().
                            equals(EComisionTramiteDatoDato.AREA_DE_TERRENO.getValor())) {
                            esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA.getId()] = true;
                        }
                        if (ctd.getDato().equals(
                            EComisionTramiteDatoDato.ZONAS_HOMOGENEAS_GEOECONOMICAS.getValor())) {
                            esRC[ECampoRC.PREDIO_ZONA_ZONA_GEOECONOMICA.getId()] = true;
                        }
                        if (ctd.getDato().equals(EComisionTramiteDatoDato.ZONAS_HOMOGENEAS_FISICAS.
                            getValor())) {
                            esRC[ECampoRC.PREDIO_ZONA_ZONA_FISICA.getId()] = true;
                        }
                        if (ctd.getDato().
                            equals(EComisionTramiteDatoDato.AREA_CONSTRUIDA.getValor())) {
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA.getId()] = true;
                            //esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_PRIVADA.getId()]=true;
                            //esRC[ECampoRC.UNIDAD_CONSTRUCCION_AREA_UNIDAD_CONSTRUIDA.getId()]=true;
                        }
                        if (ctd.getDato().equals(EComisionTramiteDatoDato.PUNTAJE_DE_CONSTRUCCION.
                            getValor())) {
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION.getId()] =
                                true;
                        }
                        if (ctd.getDato().equals(EComisionTramiteDatoDato.USOS_DE_LA_CONSTRUCCION.
                            getValor())) {
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_USO_ID.getId()] = true;
                        }
                        if (ctd.getDato().equals(EComisionTramiteDatoDato.DESTINOS_ECONOMICOS.
                            getValor())) {
                            esRC[ECampoRC.PREDIO_DESTINO.getId()] = true;
                        }
                        if (ctd.getDato().equals(EComisionTramiteDatoDato.CONSTRUCCIONES_NUEVAS.
                            getValor())) {
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION.getId()] =
                                true;
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_PISO_UBICACION.getId()] = true;
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_PISOS_UNIDAD.getId()] = true;
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES.getId()] = true;
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_BANIOS.getId()] = true;
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_LOCALES.getId()] = true;
                            esRC[ECampoRC.UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION_COMP.getId()] = true;
                            //esRC[ECampoRC.UNIDAD_CONSTRUCCION_DESCRIPCION_CONSTRUCCION_NO_CONVENCIONAL.getId()]=true;
                        }
                    }
                }
            }
        } else if (this.tramiteSeleccionado.isEnglobeVirtual()) {
            //Se inicializan los campos para el manejo de englobe Virtual
            this.inicializarCamposEnglobeVirtual();
        } else {
            //Se inicializan los campos para el manejo de etapas
            this.inicializarCamposParaEtapas();
        }
    }

    /**
     * Metodo para inicializar los campos que se deben habilitar para predio originales de etapas
     *
     * @author felipe.cadena
     */
    public void inicializarCamposParaEtapas() {

        //Se bloquean todos los campos
        this.esRC = new boolean[90];

        //Se habilitan solo los campos requeridos
        esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()] = true;
    }

    /**
     * Habilita los campos para los tramites que son desenglobes por etapas y son predios nuevos
     *
     * @author felipe.cadena
     */
    public void habilitarTodo() {
        Arrays.fill(this.esRC, true);
    }

    public boolean isDh() {
        return rectificacionComplementacion || this.desenglobeEtapas || this.cancelacionMasiva ||
             this.englobeVirtual;
    }

    /* // Eliminado por eficiencia FGWL private boolean esRC(long i){ for (TramiteRectificacion
     * tr:rectificaciones){ if (tr.getDatoRectificar().getId().equals(i)){ return true; } } return
     * false;
	} */
    // OK
    public boolean isDhTipoIdentificacion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_TIPO_IDENTIFICACION.getId()] ||
            esRC[ECampoRC.PERSONA_TIPO_IDENTIFICACION_COMP.getId()]);
    }

    // OK
    public boolean isDhNumeroIdentificacion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_NUMERO_IDENTIFICACION.getId()] ||
            esRC[ECampoRC.PERSONA_NUMERO_IDENTIFICACION_COMP.getId()]);
    }

    // OK
    public boolean isDhPrimerApellido() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PRIMER_APELLIDO.getId()] ||
            esRC[ECampoRC.PERSONA_PRIMER_APELLIDO_COMP.getId()]);
    }

    // OK
    public boolean isDhSegundoApellido() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_SEGUNDO_APELLIDO.getId()] ||
            esRC[ECampoRC.PERSONA_SEGUNDO_APELLIDO_COMP.getId()]);
    }

    // OK
    public boolean isDhPrimerNombre() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PRIMER_NOMBRE.getId()] ||
            esRC[ECampoRC.PERSONA_PRIMER_NOMBRE_COMP.getId()]);
    }

    // OK
    public boolean isDhSegundoNombre() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_SEGUNDO_NOMBRE.getId()] ||
            esRC[ECampoRC.PERSONA_SEGUNDO_NOMBRE_COMP.getId()]);
    }

    // OK
    public boolean isDhRazonSocial() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_RAZON_SOCIAL.getId()] ||
            esRC[ECampoRC.PERSONA_RAZON_SOCIAL_COMP.getId()]);
    }

    // OK
    public boolean isDhSigla() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_SIGLA.getId()] ||
            esRC[ECampoRC.PERSONA_SIGLA_COMP.getId()]);
    }

    //OK
    public boolean isDhAreaTotalTerrenoPrivada() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA.getId()];
    }

    //OK
    public boolean isDhAreaTotalTerrenoComun() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_COMUN.getId()];
    }

    //OK
    public boolean isDhAreaTotaCOnstruccion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_COMUN.getId()];
    }

    //OK
    public boolean isDhAreaTotalConstruidaPrivada() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        //return !esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_PRIVADA.getId()];
        return !esRC[ECampoRC.UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA.getId()];
    }

    //OK
    public boolean isDhAreaTotalConstruidaComun() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_CONSTRUIDA_COMUN.getId()];
    }

    /* public boolean isDhAreaTotalTerreno(){ if (this.tramitesConsiderados()){ return false; }
     * return !esRC[ECampoRC.PREDIO_AREA_TERRENO.getId()];
	} */
 /*
     * public boolean isDhAreaTotalConstruccion(){ if (this.tramitesConsiderados()){ return false; }
     * return !esRC[ECampoRC.PREDIO_AREA_CONSTRUCCION.getId()];
	} */
    // OK
    public boolean isDhParticipacion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PARTICIPACION.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PARTICIPACION_COMP.getId()]);
    }

    // OK
    public boolean isDhTipoPropietario() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.PERSONA_PREDIO_TIPO.getId()];
    }

    //OK
    public boolean isDhMatriculaInmobiliaria() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PREDIO_NUMERO_REGISTRO.getId()] ||
            esRC[ECampoRC.PREDIO_NUMERO_REGISTRO_COMP.getId()]);
    }

    //OK
    public boolean isDhTipoPredio() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PREDIO_TIPO.getId()] ||
            esRC[ECampoRC.PREDIO_TIPO_COMP.getId()]);
    }

    // TODO juan.mendez:Son rectificaciones espaciales:20120228:fredy.wilches	
    public boolean isDhZonaFisica() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.PREDIO_ZONA_ZONA_FISICA.getId()];
    }

    // TODO juan.mendez:Son rectificaciones espaciales:20120228:fredy.wilches
    public boolean isDhZonaGeoeconomica() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.PREDIO_ZONA_ZONA_GEOECONOMICA.getId()];
    }

    //OK
    public boolean isDhUsoUnidad() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_USO_ID.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_USO_ID_COMP.getId()]);
    }

    //OK
    public boolean isDhTotalPisosConstruccion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_PISOS_CONSTRUCCION_COMP.getId()]);
    }

    //OK
    public boolean isDhPisoUbicacionUnidad() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.UNIDAD_CONSTRUCCION_PISO_UBICACION.getId()];
    }

    //OK
    public boolean isDhTotalPisosUnidad() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_PISOS_UNIDAD.getId()];
    }

    //OK
    public boolean isDhTotalHabitaciones() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_HABITACIONES_COMP.getId()]);
    }

    //OK
    public boolean isDhTotalBanos() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_BANIOS.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_BANIOS_COMP.getId()]);
    }

    //OK
    public boolean isDhTotalLocales() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_LOCALES.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_TOTAL_LOCALES_COMP.getId()]);
    }

    //OK
    public boolean isDhAnioConstruccion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION_COMP.getId()]);
    }

    //OK
    public boolean isDhDetalleCalificacion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION.getId()] ||
            esRC[ECampoRC.UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION_COMP.getId()]);
    }

    //OK
    public boolean isDhDestinoPredio() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PREDIO_DESTINO.getId()] ||
            esRC[ECampoRC.PREDIO_DESTINO_COMP.getId()]);
    }

    //OK
    public boolean isDhDireccion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()] ||
            esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION_COMP.getId()]);
    }
    //OK

    public boolean isDhDireccionComp() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION_COMP.getId()];
    }
    //OK

    public boolean isDhDireccionRec() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()];
    }

    //OK
    public boolean isDhCoeficientePropiedad() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.FICHA_MATRIZ_PREDIO_COEFICIENTE.getId()];
    }

    //OK
    public boolean isDhDireccionPrincipal() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PREDIO_DIRECCION_PRINCIPAL.getId()] ||
            esRC[ECampoRC.PREDIO_DIRECCION_PRINCIPAL_COMP.getId()]);
    }

    /* public boolean isDhTipoFoto(){ if (this.tramitesConsiderados()){ return false; } return
     * !esRC[ECampoRC.FOTO_TIPO.getId()]; }
     *
     * public boolean isDhDescripcionFoto(){ if (this.tramitesConsiderados()){ return false; }
     * return !esRC[ECampoRC.FOTO_DESCRIPCION.getId()]; }
     *
     * public boolean isDhJustificacionPropiedad(){ if (this.tramitesConsiderados()){ return false;
     * } return !esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_JUSTIFICACION.getId()];
	} */
    //OK
    public boolean isDhDescripcionConstruccionNoConvencional() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return false;//!esRC[ECampoRC.UNIDAD_CONSTRUCCION_DESCRIPCION_CONSTRUCCION_NO_CONVENCIONAL.getId()];
    }

    //OK
    public boolean isDhAreaUnidadNoConstruida() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return false;//!esRC[ECampoRC.UNIDAD_CONSTRUCCION_AREA_UNIDAD_CONSTRUIDA.getId()];
    }
    //OK

    public boolean isDhAreaTotalConstruida() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !esRC[ECampoRC.UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA.getId()];
    }

    //OK
    public boolean isDhDimension() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.UNIDAD_CONSTRUCCION_DIMENSION.getId()]);
    }

    //OK
    public boolean isDhModoAdquisicion() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION_COMP.getId()]);
    }

    //OK
    public boolean isDhValorCompra() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_VALOR.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_VALOR_COMP.getId()]);
    }

    //OK
    public boolean isDhTipoTitulo() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO_COMP.getId()]);
    }

    //OK
    public boolean isDhEntidadEmisora() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA_COMP.getId()]);
    }

    //OK
    public boolean isDhDepartamentoTitulo() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO_COMP.getId()]);
    }

    //OK
    public boolean isDhMunicipioTitulo() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_MUNICIPIO.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_MUNICIPIO_COMP.getId()]);
    }

    //OK
    public boolean isDhNumeroTitulo() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO_COMP.getId()]);
    }

    //OK
    public boolean isDhFechaTitulo() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO_COMP.getId()]);
    }

    //OK
    public boolean isDhFechaRegsitro() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return !(esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO.getId()] ||
            esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO_COMP.getId()]);
    }
    //OK

    public boolean isDhAreaTotal() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA.getId()];
    }

    //----------------Metodos para las rectificaciones sobre la ficha matriz------------------//
    public boolean isDhAnioConstruccionMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.UNIDAD_CONSTRUCCION_ANIO_CONSTRUCCION.getId()];
    }

    public boolean isDhAreaTotalConstruccionMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA.getId()];
    }

    //OK
    public boolean isDhAreaTotalMatriz() {
        if (this.tramitesConsiderados()) {
            return false;
        }
        return esRCMatriz[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA.getId()];
    }

    public boolean isDhCoeficienteCopropiedadMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.FICHA_MATRIZ_PREDIO_COEFICIENTE.getId()];
    }

    public boolean isDhDetalleCalificacionMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.UNIDAD_CONSTRUCCION_COMP_DETALLE_CALIFICACION.getId()];
    }

    public boolean isDhDimensionMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.UNIDAD_CONSTRUCCION_DIMENSION.getId()];
    }

    public boolean isDhDireccionMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()];
    }

    public boolean isDhDireccionPrincipalMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.PREDIO_DIRECCION_PRINCIPAL.getId()];
    }

    public boolean isDhUsoUnidadMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.UNIDAD_CONSTRUCCION_USO_ID.getId()];
    }

    public boolean isDhMatriculaInmobiliariaMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.PREDIO_NUMERO_REGISTRO.getId()];
    }

    public boolean isDhZonasMatriz() {
        if (!this.rectificacionMatriz) {
            return true;
        }
        return !esRCMatriz[ECampoRC.PREDIO_ZONA_ZONA_FISICA.getId()] ||
            !esRCMatriz[ECampoRC.PREDIO_ZONA_ZONA_GEOECONOMICA.getId()];
    }

    /**
     * Método para deshabilitar la edicion de todos los campos relacionados a rectificación o
     * complementación
     *
     * @author felipe.cadena
     */
    public void deshabilitarTodo() {
        this.esRC = new boolean[90];
        this.esRC[ECampoRC.FICHA_MATRIZ_AREA_TOTAL_TERRENO_PRIVADA.getId()] = true;
    }

    /**
     * Método para deshabilitar la edicion de todos los campos relacionados a rectificación o
     * complementación
     *
     * @author felipe.cadena
     */
    public void deshabilitarPestanas() {
        this.esRC = new boolean[90];
    }

    /**
     * Determina si el tramite actual esta dentro de los considerados par el bloqueo de edicion
     *
     * @return
     */
    private boolean tramitesConsiderados() {
        return !this.rectificacionComplementacion && !this.desenglobeEtapas &&
            !this.cancelacionMasiva &&
             !this.englobeVirtual;

    }

    public static void main(String args[]) {
//		System.out.println(new Long(10)==10);
//		System.out.println(new Long(10).equals(new Long(10)));
//		System.out.println(new Long(10).equals(10l));
//		
//		System.out.println(new Long(10).equals(new Integer(10)));
//		System.out.println(new Long(10).equals(10));

        ValidarCompYRect vyc = new ValidarCompYRect();

        System.out.println(vyc.buscarCaracteresIlegales("niños"));
        System.out.println(vyc.buscarCaracteresIlegales("trámite"));
        //vyc.validarNit("39628546", "8");

    }

    /**
     * Metodo para inicializar los campos que se deben habilitar para predio originales de etapas
     *
     * @author leidy.gonzalez
     */
    public void inicializarCamposEnglobeVirtual() {

        //Se bloquean todos los campos
        this.esRC = new boolean[90];

        //Se habilitan solo los campos requeridos
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_MODO_ADQUISICION_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_NUMERO_TITULO_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_TIPO_TITULO_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_ENTIDAD_EMISORA_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_DEPARTAMENTO_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_MUNICIPIO_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_FECHA_TITULO_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_FECHA_REGISTRO_COMP.getId()] = true;
        esRC[ECampoRC.PERSONA_PREDIO_PROPIEDAD_VALOR_COMP.getId()] = true;
        esRC[ECampoRC.UNIDAD_CONSTRUCCION_AREA_CONSTRUIDA.getId()] = true;

        esRC[ECampoRC.PREDIO_NUMERO_REGISTRO.getId()] = true;
        esRC[ECampoRC.PREDIO_NUMERO_REGISTRO_COMP.getId()] = true;
        esRC[ECampoRC.PREDIO_DIRECCION_DIRECCION.getId()] = true;
        esRC[ECampoRC.PREDIO_DIRECCION_PRINCIPAL.getId()] = true;

    }
}
