package co.gov.igac.snc.web.mb.tramite.gestion;

import java.io.Serializable;
import java.sql.Array;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioCierreVigencia;
import co.gov.igac.snc.persistence.entity.actualizacion.ZonaCierreVigencia;
import co.gov.igac.snc.persistence.entity.conservacion.ListadoPredio;
import co.gov.igac.snc.persistence.entity.formacion.DecretoAvaluoAnual;
import co.gov.igac.snc.persistence.entity.formacion.DecretoCondicion;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstadoCierreMunicipio;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.EZonaCierreVigencia;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * @description Managed bean para gestionar el cierre de año para los municipios en el proceso de
 * conservación.
 *
 * @version 2.0
 *
 * @author david.cifuentes
 */
@Component("cierreAnioParaMunicipios")
@Scope("session")
public class CierreAnioParaMunicipiosMB extends SNCManagedBean implements
    Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -7311846763503557925L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(CierreAnioParaMunicipiosMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Variable {@link UsuarioDTO} que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena el {@link DecretoAvaluoAnual} seleccionado, que es el último decreto
     * registrado en la base de datos
     */
    private DecretoAvaluoAnual decretoSeleccionado;

    /**
     * Variable que almacena el código del {@link Departamento} seleccionado en el menu de
     * departamentos.
     */
    private String codigoDepartamentoSeleccionado;

    /**
     * Lista de select items con los valores del {@link Departamento} (
     * {@link String} código, {@link String} nombre)
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Lista de select items con los valores del nombre de la zona.
     */
    private List<SelectItem> zonasItemList;

    /**
     * Lista de {@link MunicipioCierreVigencia} que almacena los municipios del {@link Departamento}
     * seleccionado.
     */
    private List<MunicipioCierreVigencia> municipiosList;

    /**
     * {@link Array} usado para almacenar los {@link MunicipioCierreVigencia} seleccionados de la
     * tabla de municipios.
     */
    private MunicipioCierreVigencia[] municipiosSeleccionados;

    /**
     * Variable booleana usada para habilitar o deshabilitar ciertas pestañas.
     */
    private boolean decretoVigenciaSiguienteValida;

    /**
     * Variable booleana para seter el proceso seleccionado al momento de registrar el proceso ya se
     * de actualización o formación.
     *
     * Para actualización el valor de la variable es false. Para formación el valor de la variable
     * es true.
     */
    private boolean procesoBool;

    /**
     * Variable booleana para saber si el proceso de actualización o formación ya tuvo cierre.
     */
    private boolean procesoCerradoBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de retirar selección formación /
     * actualización.
     */
    private boolean retirarSeleccionActualizacionFormacionBtnBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de registrar actualización /
     * formación.
     */
    private boolean registroActualizacionFormacionBtnBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de cerrar municipios.
     */
    private boolean cerrarMunicipioBool;

    /**
     * {@link Array} usado para almacenar las zonas seleccionadas.
     */
    private String[] zonasSelected;

    /**
     * Variable {@link boolean} usada para habilitar el botón de confirmar cierre una vez se hayan
     * realizado las validaciones para los municipios seleccionados.
     */
    private boolean confirmarCierreBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de visualizar estadísticas.
     */
    private boolean visualizarEstadisticasBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de validar cierre para los municipios
     * seleccionados.
     */
    private boolean validarCierreBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de validar cierre para los municipios
     * seleccionados.
     */
    private boolean mismoEstadoMunSeleccionadosBool;

    /**
     * Variable {@link boolean} usada para habilitar el botón de reversar cierre.
     */
    private boolean reversarCierreBool;

    /**
     * Variable {@link boolean} usada para saber de cual proceso se quiere realizar el cierre anual,
     * si la variable es true se refiere al proceso de conservación, de ser false se refiere a
     * actualización o formación.
     */
    private boolean procesoConservacionBool;

    /**
     * Variable booleana para saber si se han seleccionado todos los municipios.
     */
    private boolean selectedAllBool;

    /**
     * Variable usada para filtrar los reportes municipales y departamentales en la ventana modal de
     * visualizar estadísticas.
     */
    private String tipoEstadisticas;

    /**
     * Variable usada para setear un municipio al momento de visualizar sus estadisticas.
     */
    private MunicipioCierreVigencia municipioSeleccionado;

    /**
     * Variable usada para habilitar el botón de estadísticas departamentales.
     */
    private boolean estadisticasDepartamentalesBool;

    /**
     * Variable usada para almacenar el año de la vigencia del decreto actual.
     */
    private Long vigenciaDecreto;

    /**
     * Variables usadas para cargar un listado de los reportes definidos para cierre anual.
     */
    private List<Dominio> listadoReportes;
    private List<Dominio> listadoReportesDepartamentales;
    private List<Dominio> listadoReportesMunicipales;
    private List<Dominio> listadoReportesProcesosMunicipales;

    /**
     * Dto que contiene los datos del reporte generado
     */
    private ReporteDTO reporteDTO;

    /**
     * Variable usada para almacenar el tipo de reporte que se quiere generar.
     */
    private Dominio dominioTipoReporte;

    /**
     * Variable {@link LazyDataModel} donde se cargan los resultados paginados de la consulta de
     * predios segun el reporte seleccionado
     */
    private LazyDataModel<ListadoPredio> lazyWorkingListadoPredios;

    /**
     * lista de los numeros prediales de predios para el municipio seleccioando
     */
    private String[] numerosPredialesListadoPredios;

    /**
     * Variable usada para cargar el tipo de filtro que se quiere hacer sobre la tabla del listado
     * de predios.
     */
    private Long tipoAvaluoFiltro;

    /**
     * Variable usada para almacentar el valor del filtro de porcentaje de avalúo sobre la tabla de
     * listado de predios.
     */
    private Long porcentajeAvaluoFiltro;

    /**
     * Variable usada para saber cuando se debe precargar la tabla con el filtro de avalúo
     */
    private boolean filtroAvaluoExistBool;

    /**
     * Variable usada en la generación de reportes para diferenciar cuando un reporte se genera para
     * precierre (1 de Enero) y poscierre (31 de Diciembre)
     */
    private String tipoCierre;

    /**
     * Usuario usado para el cargue de municipios en caso de ser delegado
     */
    private boolean usuarioDelegado;
    
    /**
     * Variables usadas para determinar que tipo de usuario se encuentra logueado y de esta manera
     * cargar los diferentes menus y opciones. 1 - UOC 2 - Territorial 3 - Sede central - 4 - Delegaada
     */
    private int tipoUsuario;


    /** ---------------------------------- */
    /** ----------- CONSTRUCTOR ---------- */
    /** ---------------------------------- */
    @SuppressWarnings("serial")
    public CierreAnioParaMunicipiosMB() {
        this.lazyWorkingListadoPredios = new LazyDataModel<ListadoPredio>() {

            @Override
            public List<ListadoPredio> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<ListadoPredio> answer = new ArrayList<ListadoPredio>();

                populateListadoPrediosLazyly(answer, first, pageSize, sortField, sortOrder.
                    toString(), filters);
                return answer;
            }
        };
    }

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {

        LOGGER.debug("Init - CierreAnioParaMunicipiosConservacionMB");

        // Get del usuario, es necesario si se realizan inserciones en la BD,
        // para el caso en que no existan municipios para el actual decreto.
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // Gestión de delegados
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();

        // Cargue del decreto
        this.cargarDecreto();

        // Validar que existan municipios para el decreto acctual,
        // en caso de no encontrarlos, insertar todos los municipios existentes
        // en la tabla MunicipioCierreVigencia con el decreto actual.
        this.validarExistenciaMunicipiosPorDecreto();
        
        // Tipo de usuario
        if (this.usuario.getCodigoUOC() != null) {
            // Usuario UOC
            this.tipoUsuario = 1;
        } else if (this.usuario.getCodigoTerritorial() != null &&
             !this.usuario.getCodigoTerritorial().equals(Constantes.DIRECCION_GENERAL_CODIGO)) {
            // Usuario Territorial
            this.tipoUsuario = 2;
        } else {
            // Sede Central
            this.tipoUsuario = 3;
        }

        // Cargue de departamentos de acuerdo a su estructura organizacional o delegada
        String eoCodigo = "";
        if (this.usuarioDelegado) {
            this.tipoUsuario = 4;
            EstructuraOrganizacional eo = this.getGeneralesService().buscarEstructuraOrganizacionalPorCodigo(this.usuario.getCodigoTerritorial());
            if(eo != null) {
                eoCodigo = eo.getCodigo();
            } else {
                eoCodigo = this.usuario.getCodigoTerritorial();
            }
        } else if (this.usuario.getCodigoTerritorial() != null
                && this.usuario.isTerritorial()) {
            eoCodigo = this.usuario.getCodigoTerritorial();
        } else if (this.usuario.getCodigoUOC() != null && this.usuario.isUoc()) {
            eoCodigo = this.usuario.getCodigoUOC();
        }

        // Cargue de departamentos de la territorial o delegada del usuario
        List<Departamento> departamentosList = this.getGeneralesService().
                getCacheDepartamentosPorTerritorial(eoCodigo);

        if (departamentosList != null) {
            
            this.departamentosItemList = new ArrayList<SelectItem>();
            this.departamentosItemList
                    .add(new SelectItem("", "Seleccionar..."));
            
            if (this.tipoUsuario == 1) {
                departamentosList = this.getGeneralesService()
                    .getCacheDepartamentosPorTerritorial(
                        this.usuario.getCodigoUOC());
            } else if (this.tipoUsuario == 2) {
                departamentosList = this.getGeneralesService()
                    .getCacheDepartamentosPorTerritorial(
                        this.usuario.getCodigoTerritorial());
                
                // Filtro de los departamentos por territorial
                for (Departamento departamento : departamentosList) {
                     if (!departamento.getNombre().equals(usuario.getDescripcionTerritorial())) {
                        departamentosList.remove(departamento);
                    }
                }
               
            } else if (this.tipoUsuario == 3) {
                 for (Departamento departamento : departamentosList) {
                    this.departamentosItemList.add(new SelectItem(departamento
                        .getCodigo(), departamento.getNombre()));
                }
            } 
            
            for (Departamento departamento : departamentosList) {
                // Excluir los departamentos de catastro descentralizado.
                if (!(departamento.getCodigo().equals("05") ||
                        departamento.getCodigo().equals("11"))) {
                    this.departamentosItemList.add(new SelectItem(departamento
                            .getCodigo(), departamento.getNombre()));
                }
            }
        }

        // Zonas
        this.zonasItemList = new ArrayList<SelectItem>();
        this.zonasItemList.add(new SelectItem(EZonaCierreVigencia.RURAL
            .getCodigo(), EZonaCierreVigencia.RURAL.getValor()));
        this.zonasItemList.add(new SelectItem(EZonaCierreVigencia.URBANA
            .getCodigo(), EZonaCierreVigencia.URBANA.getValor()));
        this.zonasItemList.add(new SelectItem(
            EZonaCierreVigencia.CORREGIMIENTOS.getCodigo(),
            EZonaCierreVigencia.CORREGIMIENTOS.getValor()));

        // Este caso no debería presentarse
        if (this.decretoSeleccionado.getDecretoCondicions().isEmpty()) {
            this.addMensajeError("El decreto presenta inconsistencias.");
        }

        // Cargue de listados de reportes de cierre
        this.listadoReportes = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.REPO_JASPER_CIERRE_LISTADO);
        this.listadoReportesDepartamentales = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.REPO_JASPER_CIERRE_DEPA);
        this.listadoReportesMunicipales = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.REPO_JASPER_CIERRE_MUNI);
        this.listadoReportesProcesosMunicipales = this.getGeneralesService().
            getCacheDominioPorNombre(EDominio.REPO_JASPER_CIERRE_PROCESO);

    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    /**
     * Método usado para activar o desactivar el botón de visualizar estadísticas. Retorna 'true'
     * cuando el estado de los municipios seleccionados es 'ESTADISTICAS PRECIERRE' O 'ESTADISTICAS
     * POSCIERRE'.
     *
     * @return
     */
    public boolean isVisualizarEstadisticasBool() {
        if (this.mismoEstadoMunSeleccionadosBool &&
             this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados[0] != null &&
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion() != null &&
             (this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.ESTADISTICAS_PRECIERRE
                        .getEstado()) || this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.ESTADISTICAS_POSCIERRE
                        .getEstado()))) {
            this.visualizarEstadisticasBool = true;
        } else {
            this.visualizarEstadisticasBool = false;
        }
        return visualizarEstadisticasBool;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método usado para activar o desactivar el botón de visualizar estadísticas departamentales,
     * retorna TRUE cuando todos los municipios de un departamento seleccionado tengan el estado
     * 'ESTADISTICAS PRECIERRE', 'ESTADISTICAS POSCIERRE', 'VALIDADO', 'CONFIRMADO' o 'CERRADO'
     *
     * @return
     */
    public boolean isEstadisticasDepartamentalesBool() {
        this.estadisticasDepartamentalesBool = true;
        if (this.codigoDepartamentoSeleccionado != null &&
             this.municipiosList != null &&
             !this.municipiosList.isEmpty()) {
            for (MunicipioCierreVigencia mcv : this.municipiosList) {
                if (!(EEstadoCierreMunicipio.ESTADISTICAS_PRECIERRE
                    .getEstado().equals(mcv.getEstadoCierreConservacion()) ||
                     EEstadoCierreMunicipio.VALIDADO
                        .getEstado().equals(mcv.getEstadoCierreConservacion()) ||
                     EEstadoCierreMunicipio.ESTADISTICAS_POSCIERRE
                        .getEstado().equals(mcv.getEstadoCierreConservacion()) ||
                     EEstadoCierreMunicipio.CONFIRMADO
                        .getEstado().equals(mcv.getEstadoCierreConservacion()) ||
                     EEstadoCierreMunicipio.CERRADO
                        .getEstado().equals(mcv.getEstadoCierreConservacion()))) {
                    this.estadisticasDepartamentalesBool = false;
                    break;
                }
            }
        } else {
            this.estadisticasDepartamentalesBool = false;
        }
        return estadisticasDepartamentalesBool;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método get usado para activar o desactivar el botón de confirmar cierre. Retorna true cuando
     * el estado de los municipios seleccionados es 'ESTADISTICAS_POSCIERRE'.
     *
     * @return
     */
    public boolean isConfirmarCierreBool() {
        if (this.mismoEstadoMunSeleccionadosBool &&
             this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados[0] != null &&
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion() != null &&
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.ESTADISTICAS_POSCIERRE.getEstado())) {
            this.confirmarCierreBool = true;
            this.retirarSeleccionActualizacionFormacionBtnBool = false;
            this.registroActualizacionFormacionBtnBool = false;
        } else {
            this.confirmarCierreBool = false;
        }
        return confirmarCierreBool;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método get usado para activar o desactivar el botón de validar cierre. Retorna true para
     * cualquiera de los estados del municipio'.
     *
     * @return
     */
    public boolean isValidarCierreBool() {
        if (this.mismoEstadoMunSeleccionadosBool &&
             this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados.length > 0 &&
             this.municipiosSeleccionados[0] != null &&
             (this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion() == null ||
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.SIN_EJECUTAR
                        .getEstado()) )) {
            this.validarCierreBool = true;
        } else {
            this.validarCierreBool = false;
        }
        return validarCierreBool;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método get usado para activar o desactivar el botón de generar estadísticas poscierre.
     * Retorna true cuando el estado de los municipios seleccionados es 'ESTADISTICAS PRECIERRE'.
     *
     * @return
     */
    public boolean isCerrarMunicipioBool() {
        if (this.mismoEstadoMunSeleccionadosBool &&
             this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados[0] != null &&
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion() != null && this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion()
                .equals(EEstadoCierreMunicipio.ESTADISTICAS_PRECIERRE.getEstado())) {

            this.cerrarMunicipioBool = true;
            this.retirarSeleccionActualizacionFormacionBtnBool = false;
            this.registroActualizacionFormacionBtnBool = false;
        } else {
            this.cerrarMunicipioBool = false;
        }
        return cerrarMunicipioBool;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método get usado para activar o desactivar el botón de reversar cierre. Retorna true cuando
     * el estado de los municipios seleccionados es 'VALIDADO'.
     *
     * @return
     */
    public boolean isReversarCierreBool() {
        if (this.mismoEstadoMunSeleccionadosBool &&
             this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados[0] != null &&
             (this.municipiosSeleccionados[0].
                getEstadoCierreConservacion() == null ||
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.ESTADISTICAS_PRECIERRE
                        .getEstado()) ||
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.SIN_EJECUTAR
                        .getEstado()) ||
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.VALIDADO
                        .getEstado()) ||
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.CONFIRMADO
                        .getEstado()) ||
             this.municipiosSeleccionados[0]
                .getEstadoCierreConservacion().equals(
                    EEstadoCierreMunicipio.ESTADISTICAS_POSCIERRE
                        .getEstado()))) {

            this.reversarCierreBool = true;
            this.retirarSeleccionActualizacionFormacionBtnBool = false;
            this.registroActualizacionFormacionBtnBool = false;
        } else {
            this.reversarCierreBool = false;
        }
        return reversarCierreBool;
    }

    /** --------------------------------------------------------------------- */

    public void setConfirmarCierreBool(boolean confirmarCierreBool) {
        this.confirmarCierreBool = confirmarCierreBool;
    }

    public void setVisualizarEstadisticasBool(boolean visualizarEstadisticasBool) {
        this.visualizarEstadisticasBool = visualizarEstadisticasBool;
    }

    public DecretoAvaluoAnual getDecretoSeleccionado() {
        return decretoSeleccionado;
    }

    public void setDecretoSeleccionado(DecretoAvaluoAnual decretoSeleccionado) {
        this.decretoSeleccionado = decretoSeleccionado;
    }

    public void setEstadisticasDepartamentalesBool(
        boolean estadisticasDepartamentalesBool) {
        this.estadisticasDepartamentalesBool = estadisticasDepartamentalesBool;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }

    public String getCodigoDepartamentoSeleccionado() {
        return codigoDepartamentoSeleccionado;
    }

    public void setCodigoDepartamentoSeleccionado(
        String codigoDepartamentoSeleccionado) {
        this.codigoDepartamentoSeleccionado = codigoDepartamentoSeleccionado;
    }

    public Dominio getDominioTipoReporte() {
        return dominioTipoReporte;
    }

    public void setDominioTipoReporte(Dominio dominioTipoReporte) {
        this.dominioTipoReporte = dominioTipoReporte;
    }

    public String[] getNumerosPredialesListadoPredios() {
        return numerosPredialesListadoPredios;
    }

    public void setNumerosPredialesListadoPredios(String[] numerosPredialesListadoPredios) {
        this.numerosPredialesListadoPredios = numerosPredialesListadoPredios;
    }

    public LazyDataModel<ListadoPredio> getLazyWorkingListadoPredios() {
        return lazyWorkingListadoPredios;
    }

    public void setLazyWorkingListadoPredios(
        LazyDataModel<ListadoPredio> lazyWorkingListadoPredios) {
        this.lazyWorkingListadoPredios = lazyWorkingListadoPredios;
    }

    public ReporteDTO getReporteDTO() {
        return reporteDTO;
    }

    public void setReporteDTO(ReporteDTO reporteDTO) {
        this.reporteDTO = reporteDTO;
    }

    public boolean isSelectedAllBool() {
        return selectedAllBool;
    }

    public void setSelectedAllBool(boolean selectedAllBool) {
        this.selectedAllBool = selectedAllBool;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<Dominio> getListadoReportes() {
        return listadoReportes;
    }

    public void setListadoReportes(List<Dominio> listadoReportes) {
        this.listadoReportes = listadoReportes;
    }

    public List<Dominio> getListadoReportesDepartamentales() {
        return listadoReportesDepartamentales;
    }

    public void setListadoReportesDepartamentales(
        List<Dominio> listadoReportesDepartamentales) {
        this.listadoReportesDepartamentales = listadoReportesDepartamentales;
    }

    public List<Dominio> getListadoReportesMunicipales() {
        return listadoReportesMunicipales;
    }

    public void setListadoReportesMunicipales(
        List<Dominio> listadoReportesMunicipales) {
        this.listadoReportesMunicipales = listadoReportesMunicipales;
    }

    public List<Dominio> getListadoReportesProcesosMunicipales() {
        return listadoReportesProcesosMunicipales;
    }

    public void setListadoReportesProcesosMunicipales(
        List<Dominio> listadoReportesProcesosMunicipales) {
        this.listadoReportesProcesosMunicipales = listadoReportesProcesosMunicipales;
    }

    public boolean isProcesoConservacionBool() {
        return procesoConservacionBool;
    }

    public void setProcesoConservacionBool(boolean procesoConservacionBool) {
        this.procesoConservacionBool = procesoConservacionBool;
    }

    public MunicipioCierreVigencia getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(
        MunicipioCierreVigencia municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public boolean isProcesoCerradoBool() {
        return procesoCerradoBool;
    }

    public void setProcesoCerradoBool(boolean procesoCerradoBool) {
        this.procesoCerradoBool = procesoCerradoBool;
    }

    public boolean isRegistroActualizacionFormacionBtnBool() {
        return registroActualizacionFormacionBtnBool;
    }

    public void setRegistroActualizacionFormacionBtnBool(
        boolean registroActualizacionFormacionBtnBool) {
        this.registroActualizacionFormacionBtnBool = registroActualizacionFormacionBtnBool;
    }

    public void setValidarCierreBool(boolean validarCierreBool) {
        this.validarCierreBool = validarCierreBool;
    }

    public void setReversarCierreBool(boolean reversarCierreBool) {
        this.reversarCierreBool = reversarCierreBool;
    }

    public List<SelectItem> getZonasItemList() {
        return zonasItemList;
    }

    public void setZonasItemList(List<SelectItem> zonasItemList) {
        this.zonasItemList = zonasItemList;
    }

    public String getTipoEstadisticas() {
        return tipoEstadisticas;
    }

    public void setTipoEstadisticas(String tipoEstadisticas) {
        this.tipoEstadisticas = tipoEstadisticas;
    }

    public List<MunicipioCierreVigencia> getMunicipiosList() {
        return municipiosList;
    }

    public void setMunicipiosList(List<MunicipioCierreVigencia> municipiosList) {
        this.municipiosList = municipiosList;
    }

    public boolean isDecretoVigenciaSiguienteValida() {
        return decretoVigenciaSiguienteValida;
    }

    public void setDecretoVigenciaSiguienteValida(
        boolean decretoVigenciaSiguienteValida) {
        this.decretoVigenciaSiguienteValida = decretoVigenciaSiguienteValida;
    }

    public boolean isMismoEstadoMunSeleccionadosBool() {
        return mismoEstadoMunSeleccionadosBool;
    }

    public void setMismoEstadoMunSeleccionadosBool(
        boolean mismoEstadoMunSeleccionadosBool) {
        this.mismoEstadoMunSeleccionadosBool = mismoEstadoMunSeleccionadosBool;
    }

    public void setCerrarMunicipioBool(boolean cerrarMunicipioBool) {
        this.cerrarMunicipioBool = cerrarMunicipioBool;
    }

    public MunicipioCierreVigencia[] getMunicipiosSeleccionados() {
        return municipiosSeleccionados;
    }

    public void setMunicipiosSeleccionados(
        MunicipioCierreVigencia[] municipiosSeleccionados) {
        this.municipiosSeleccionados = municipiosSeleccionados;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public boolean isProcesoBool() {
        return procesoBool;
    }

    public void setProcesoBool(boolean procesoBool) {
        this.procesoBool = procesoBool;
    }

    public String[] getZonasSelected() {
        return zonasSelected;
    }

    public void setZonasSelected(String[] zonasSelected) {
        this.zonasSelected = zonasSelected;
    }

    public Long getVigenciaDecreto() {
        return vigenciaDecreto;
    }

    public void setVigenciaDecreto(Long vigenciaDecreto) {
        this.vigenciaDecreto = vigenciaDecreto;
    }

    public Long getPorcentajeAvaluoFiltro() {
        return porcentajeAvaluoFiltro;
    }

    public void setPorcentajeAvaluoFiltro(Long porcentajeAvaluoFiltro) {
        this.porcentajeAvaluoFiltro = porcentajeAvaluoFiltro;
    }

    public boolean isRetirarSeleccionActualizacionFormacionBtnBool() {
        return retirarSeleccionActualizacionFormacionBtnBool;
    }

    public void setRetirarSeleccionActualizacionFormacionBtnBool(
        boolean retirarSeleccionActualizacionFormacionBtnBool) {
        this.retirarSeleccionActualizacionFormacionBtnBool =
            retirarSeleccionActualizacionFormacionBtnBool;
    }

    public Long getTipoAvaluoFiltro() {
        return tipoAvaluoFiltro;
    }

    public void setTipoAvaluoFiltro(Long tipoAvaluoFiltro) {
        this.tipoAvaluoFiltro = tipoAvaluoFiltro;
    }

    public String getTipoCierre() {
        return tipoCierre;
    }

    public void setTipoCierre(String tipoCierre) {
        this.tipoCierre = tipoCierre;
    }

    public boolean isFiltroAvaluoExistBool() {
        return filtroAvaluoExistBool;
    }

    public void setFiltroAvaluoExistBool(boolean filtroAvaluoExistBool) {
        this.filtroAvaluoExistBool = filtroAvaluoExistBool;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    
    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */

    /** --------------------------------------------------------------------- */
    /**
     * Método que carga el {@link DecretoAvaluoAnual}.
     *
     * Verifica el mes actual y si está en un mes diferente a Diciembre el decreto consultado es el
     * de la vigencia actual, de lo contrario trae el de la vigencia siguiente.
     *
     * @author david.cifuentes
     */
    public void cargarDecreto() {

        try {
            /**
             * Identificación del mes actual para verificar el decreto a mostrar. Éste debe
             * corresponder al año que entra en vigencia dicho decreto, es decir:
             *
             * Si, el sistema indica que estamos en un mes diferente a diciembre, la vigencia del
             * decreto debe ser la actual. Si el mes en efecto es Diciembre la vigencia del decreto
             * a traer es la del año siguiente.
             */
            // Identificar si está finalizando el año.
            boolean mesFinalizandoAnioBool = false;
            this.decretoVigenciaSiguienteValida = false;
            Date fechaActual = new Date(System.currentTimeMillis());
            SimpleDateFormat formatoMes = new SimpleDateFormat("MM");
            String mes = formatoMes.format(fechaActual);
            mesFinalizandoAnioBool = (mes.equals("12") || mes.equals("11") || mes.equals("10")
                    || mes.equals("01") // Se habilita en enero para contingencias
                    || mes.equals("08") || mes.equals("09") // TODO :: Se habilita Agosto y Septiembre para pruebas, no tener en cuenta en la integración
                ) ?
                true : false;

            if (mesFinalizandoAnioBool) {
                // Si el mes es Diciembre o Noviembre, verificar que ya se haya registrado
                // el decreto para la vigencia del año siguiente.
                // Si se encuentra traerlo, de lo contrario mostrar un mensaje
                // y traer el decreto con vigencia actual.
                SimpleDateFormat formatoAnio = new SimpleDateFormat("yyyy");
                Calendar hoy = Calendar.getInstance();
                hoy.add(Calendar.YEAR, 1);
                String anioSiguiente = formatoAnio.format(hoy.getTime());

                this.decretoSeleccionado = this.getFormacionService()
                    .buscarDecretoPorVigencia(anioSiguiente);

                if (this.decretoSeleccionado == null) {
                    this.addMensajeWarn("Está cerca a finalizar el año, aún no existe un decreto con vigencia del siguiente año.");

                    // Traer decreto con vigencia actual
                    this.decretoSeleccionado = this.getFormacionService()
                        .buscarDecretoActual();
                    this.decretoVigenciaSiguienteValida = true;
                }
            } else {
                // Traer decreto con vigencia actual
                this.decretoSeleccionado = this.getFormacionService()
                    .buscarDecretoActual();
                this.addMensajeWarn("Recuerde que el cierre anual se habilita únicamente durante la finalización del año.");
            }

            // Éste caso no debería presentarse
            if (this.decretoSeleccionado == null) {

                // En caso de no encontrarlo ningun decreto se inicializa el
                // decreto para que se pueda accerder a la pantalla
                this.decretoSeleccionado = new DecretoAvaluoAnual();
                if (this.decretoSeleccionado.getDecretoCondicions() == null) {
                    this.decretoSeleccionado
                        .setDecretoCondicions(new ArrayList<DecretoCondicion>());
                }
                this.addMensajeError("No existe un decreto registrado con la vigencia " +
                    "del presente año, ni del año siguiente, por favor verifique ésta inconsistencia.");
            } else {
                SimpleDateFormat formatoAnio = new SimpleDateFormat("yyyy");
                String anio = formatoAnio.format(this.decretoSeleccionado.getVigencia());
                long vigencia = Long.parseLong(anio);
                this.vigenciaDecreto = new Long(vigencia - 1);
            }

        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error en la búsqueda del decreto registrado" +
                 e.getMensaje());
        }

    }

    /**
     * Método que verifica la existencia de municipios para efectuar la validación y el cierre de
     * año para el decreto actual, en caso de no encontrar ningún municipio, se registran todos los
     * municipios en la tabla municipioCierreVigencia.
     *
     * @author david.cifuentes
     */
    public void validarExistenciaMunicipiosPorDecreto() {
        if (this.decretoSeleccionado != null) {
            try {
                this.getActualizacionService()
                    .validarExistenciaMunicipiosPorDecreto(
                        this.decretoSeleccionado, this.usuario);
            } catch (ExcepcionSNC e) {
                LOGGER.debug("Error:" + e.getMensaje());
                this.addMensajeError(
                    "Ocurrió un error al verificar los municipios que se encuentran en formación o actualización.");
            }
        }
    }

    /**
     * Método ejecutado al seleccionar un {@link Departamento}, su función es cargar los
     * {@link MunicipioCierreVigencia} correspondientes al código del departamento seleccionado con el
     * estado de sus procesos de actualización y formación.
     *
     * Adicionalmente el cargue excluye a los municipios de catastro descentralizado.
     *
     * @author david.cifuentes
     */
    public void cargarMunicipios() {

        this.municipiosList = new ArrayList<MunicipioCierreVigencia>();

        try {
            if (this.codigoDepartamentoSeleccionado != null &&
                 this.codigoDepartamentoSeleccionado.compareTo("") != 0) {


                List<String> codigosMunicipios = new ArrayList<String>();
                List<Municipio> municipiosList = new ArrayList<Municipio>();
                if(this.usuarioDelegado){
                    // Búsqueda de municipios para la delegada
                    List<Municipio> municipios = this.getGeneralesService().obtenerMunicipiosDelegados();
                    if(municipios != null) {
                        for (Municipio mun : municipios) {
                            codigosMunicipios.add(mun.getCodigo());
                        }
                    }
                } else {
                    // Búsqueda de los municipios del departamento
                    if (this.usuario.getCodigoTerritorial() != null 
                            && this.tipoUsuario == 2) {
                        municipiosList = this.getGeneralesService().
                                getCacheMunicipiosPorCodigoEstructuraOrganizacional(this.usuario.getCodigoTerritorial());

                        for (Municipio municipioTemp : municipiosList) {
                            codigosMunicipios.add(municipioTemp.getCodigo());
                        }
                    } else if (this.usuario.getCodigoUOC()!= null 
                            && this.tipoUsuario == 1) {
                        
                        List<Municipio> municipiosFilter = null;
                        municipiosList = this.getGeneralesService().
                                getCacheMunicipiosPorCodigoEstructuraOrganizacional(this.usuario.getCodigoUOC());

                        EstructuraOrganizacional uoc = this.buscarTerritorial(this.usuario.
                                getCodigoUOC());
                        List<EstructuraOrganizacional> uocList =
                                    new ArrayList<EstructuraOrganizacional>();
                                uocList.add(uoc);
                                municipiosFilter = this.filtrarMunicipioPorUOC(municipiosList,
                                    uocList);
                        
                        for (Municipio municipioTemp : municipiosList) {
                            codigosMunicipios.add(municipioTemp.getCodigo());
                        }
                    }
                }
                
                // Excluir los municipios de catastro descentralizado	
                if (codigosMunicipios != null && !codigosMunicipios.isEmpty()) {
                    this.municipiosList = this
                        .getActualizacionService()
                        .consultarMunicipiosCierreVigenciaPorDecretoAvaluoAnualYCodigosMunicipios(
                            this.decretoSeleccionado, codigosMunicipios);
                    Collections.sort(this.municipiosList);
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error: " + e.getMessage());
            this.addMensajeError("Error al cargar los municipios del departamento seleccionado.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el llamado al procedimiento que cierra el año en cornservación para los
     * municipios seleccionados.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unused")
    public void generarEstadisticasPosCierre() {

        try {
            if (this.municipiosSeleccionados != null) {

                if (this.codigoDepartamentoSeleccionado == null) {
                    this.codigoDepartamentoSeleccionado = this.municipiosSeleccionados[0]
                        .getMunicipioCodigo().substring(0, 2);
                }

                // Antes de realizar el llamado de efectuar el cierre se debe
                // cambiar el estado de cada uno de los municipios a persistir a
                // el estado 'VALIDADO'.
                for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {
                    mcv.setEstadoCierreActualizacion(EEstadoCierreMunicipio.VALIDADO
                        .getEstado());
                    mcv.setEstadoCierreConservacion(EEstadoCierreMunicipio.VALIDADO
                        .getEstado());
                }

                // Persistir municipios con los nuevos estados.
                boolean answer = this.getActualizacionService()
                    .guardarListaMunicipioCierreVigencias(
                        Arrays.asList(this.municipiosSeleccionados));
                if (answer) {
                    Object[] municipiosValidadosObject = null;

                    municipiosValidadosObject = this
                        .getActualizacionService()
                        .ejecutarProcedimientoCierreAnual(
                            EProcedimientoAlmacenadoFuncion.CREAR_JOB_CIERRE_CONSERVACION,
                            this.codigoDepartamentoSeleccionado,
                            this.municipiosSeleccionados, this.vigenciaDecreto, this.usuario.
                                getLogin());
                    this.addMensajeWarn(
                        "El proceso puede tardar algunos minutos, si desea visualizar el estado actual del proceso puede dar click en el botón 'Actualizar estado de municipios'");
                    this.addMensajeInfo(
                        "Se inició la generación de estadísticas poscierre para el los municipios seleccionados satisfactoriamente, " +
                        " proceda a verificar y confirmar el cierre para culminar el proceso de validación previo a la consolidación del cierre.");

                    // Consultar municipios con los nuevos estados.
                    this.cargarMunicipios();
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error: " + e.getMessage());
            this.addMensajeError("Error al efectuar el cierre para los municipios seleccionados.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el llamado al procedimiento que reversar el cierre del año para los
     * municipios seleccionados.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unused")
    public void reversarCierre() {

        try {
            if (this.municipiosSeleccionados != null) {

                if (this.codigoDepartamentoSeleccionado == null) {
                    this.codigoDepartamentoSeleccionado = this.municipiosSeleccionados[0]
                        .getMunicipioCodigo().substring(0, 2);
                }

                Object[] municipiosReversadosObject = null;
                municipiosReversadosObject = this
                    .getActualizacionService()
                    .ejecutarProcedimientoCierreAnual(
                        EProcedimientoAlmacenadoFuncion.CREAR_JOB_REVERSAR_CIERRE,
                        this.codigoDepartamentoSeleccionado,
                        this.municipiosSeleccionados, this.vigenciaDecreto, this.usuario.getLogin());
                this.addMensajeInfo("Se reversó satisfactoriamente el cierre para los municipios.");
                this.addMensajeWarn(
                    "El proceso puede tardar algunos minutos, si desea visualizar el estado actual del proceso puede dar click en el botón 'Actualizar estado de municipios'");

                // Consultar municipios con los nuevos estados.
                this.cargarMunicipios();

            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error: " + e.getMessage());
            this.addMensajeError("Error al reversar el cierre para los municipios seleccionados.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza las validaciones correspondientes a los municipios o departamentos antes
     * de realizar la confirmación del cierre y llama al job que generará las estadísticas del
     * precierre.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unused")
    public void generarEstadisticasPreCierre() {

        try {

            if (this.municipiosSeleccionados != null && this.municipiosSeleccionados.length > 0) {

                if (this.codigoDepartamentoSeleccionado == null) {
                    this.codigoDepartamentoSeleccionado = this.municipiosSeleccionados[0]
                        .getMunicipioCodigo().substring(0, 2);
                }

                boolean persistirEstado = false;
                boolean answer = true;

                // Antes de realizar el llamado a generar las estadísticas de preciere se debe
                // cambiar el estado de cada uno de los municipios a 'SIN EJECUTAR' 
                // en el caso en que tengan estado nulo.
                for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {
                    if (mcv.getEstadoCierreConservacion() == null) {
                        mcv.setEstadoCierreConservacion(EEstadoCierreMunicipio.SIN_EJECUTAR
                            .getEstado());
                        mcv.setEstadoCierreActualizacion(EEstadoCierreMunicipio.SIN_EJECUTAR
                            .getEstado());
                    }
                    mcv.setFechaInicioCierre(new Date(System.currentTimeMillis()));
                    persistirEstado = true;
                }

                if (persistirEstado) {
                    // Persistir municipios con el nuevo estado.
                    answer = this.getActualizacionService()
                        .guardarListaMunicipioCierreVigencias(
                            Arrays.asList(this.municipiosSeleccionados));
                }

                if (answer) {

                    // Definición del procedimiento a ejectuar dependiendo desde
                    // el proceso donde se llamó.
                    EProcedimientoAlmacenadoFuncion enumProcedimiento = null;
                    if (this.procesoConservacionBool) {
                        enumProcedimiento = EProcedimientoAlmacenadoFuncion.CREAR_JOB_INICIAR_CIERRE;
                    } else {
                        /**
                         * @note: De momento el procedimiento de iniciar cierre es el mismo para
                         * conservación y actualización, una vez se implemente actualización éste se
                         * debe reemplazar.
                         */
                        enumProcedimiento = EProcedimientoAlmacenadoFuncion.CREAR_JOB_INICIAR_CIERRE;
                    }

                    Object[] validacionesObject = null;

                    validacionesObject = this
                        .getActualizacionService()
                        .ejecutarProcedimientoCierreAnual(
                            enumProcedimiento,
                            this.codigoDepartamentoSeleccionado,
                            this.municipiosSeleccionados, this.vigenciaDecreto, this.usuario.
                                getLogin());
                    this.addMensajeWarn(
                        "El proceso puede tardar algunos minutos, si desea visualizar el estado actual del proceso puede dar click en el botón 'Actualizar estado de municipios'");
                    this.addMensajeInfo(
                        "Se inició la generación de estadísticas para los municipios satisfactoriamente, " +
                        
                        " proceda a generar las estadísticas poscierre para culminar el proceso de validación previo a la consolidación del cierre.");
                }
            } else {
                this.addMensajeWarn("Por favor seleccione un departamento o municipio" +
                     " para realizar la validación del cierre");
                return;
            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error: " + e.getMessage());
            this.addMensajeError("Error al efectuar el cierre para los municipios seleccionados.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que confirma el cierre de los municipios seleccionados.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("unused")
    public void confirmarCierre() {
        try {

            if (this.municipiosSeleccionados != null) {

                if (this.codigoDepartamentoSeleccionado == null) {
                    this.codigoDepartamentoSeleccionado = this.municipiosSeleccionados[0]
                        .getMunicipioCodigo().substring(0, 2);
                }

                // Antes de realizar el llamado de efectuar el cierre se debe
                // cambiar el estado de cada uno de los municipios a persistir a
                // el estado 'CONFIRMADO'.
                for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {
                    mcv.setEstadoCierreActualizacion(EEstadoCierreMunicipio.CONFIRMADO
                        .getEstado());
                    mcv.setEstadoCierreConservacion(EEstadoCierreMunicipio.CONFIRMADO
                        .getEstado());
                    mcv.setFechaFinCierre(new Date(System.currentTimeMillis()));
                }

                // Persistir municipios con los nuevos estados.
                boolean answer = this.getActualizacionService()
                    .guardarListaMunicipioCierreVigencias(
                        Arrays.asList(this.municipiosSeleccionados));

                if (answer) {

                    Object[] validacionesObject = null;
                    validacionesObject = this
                        .getActualizacionService()
                        .ejecutarProcedimientoCierreAnual(
                            EProcedimientoAlmacenadoFuncion.CREAR_JOB_CONFIRMAR_CIERRE,
                            this.codigoDepartamentoSeleccionado,
                            this.municipiosSeleccionados, this.vigenciaDecreto, this.usuario.
                                getLogin());

                    // Consultar municipios con los nuevos estados.
                    // this.cargarMunicipios();
                    this.addMensajeWarn(
                        "El proceso puede tardar algunos minutos, si desea visualizar el estado actual del proceso puede dar click en el botón 'Actualizar estado de municipios'");
                    this.addMensajeInfo(
                        "Se confirmó el cierre para los municipios satisfactoriamente.");

                }
            } else {
                this.addMensajeWarn("Por favor seleccione un departamento o municipio" +
                    " para realizar la confirmación del cierre");
                return;
            }
        } catch (ExcepcionSNC e) {
            LOGGER.debug("Error: " + e.getMessage());
            this.addMensajeError("Error al efectuar el cierre para los municipios seleccionados.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que retira la selección del proceso (Actualización / Formación) y de las zonas (Rural
     * / Urbano / Corregimientos) para el municipio seleccionado.
     *
     * @author david.cifuentes
     */
    public void retirarSeleccionActualizacionFormacion() {
        try {
            if (this.municipiosSeleccionados != null &&
                 this.municipiosSeleccionados[0] != null) {

                for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {
                    // Eliminar las zonas antiguas.
                    if (mcv.getZonaCierreVigencias() != null &&
                         !mcv
                            .getZonaCierreVigencias().isEmpty()) {

                        this.getActualizacionService()
                            .eliminarZonasDelMunicipioCierreVigencia(
                                mcv
                                    .getZonaCierreVigencias());
                    }

                    mcv.setActualizacionTotal(ESiNo.NO
                        .getCodigo());
                    mcv.setFormacionTotal(ESiNo.NO
                        .getCodigo());
                    mcv.setFechaFinActualizacion(null);
                    mcv.setFechaFinFormacion(null);
                    mcv.setFechaInicioFormacion(null);
                    mcv
                        .setFechaInicioActualizacion(null);
                    mcv.setUsuarioLog(this.usuario
                        .getLogin());
                    mcv.setFechaLog(new Date(System
                        .currentTimeMillis()));
                    mcv.setZonaCierreVigencias(null);

                }

                List<MunicipioCierreVigencia> listaMunicipioCierreVigencia =
                    new ArrayList<MunicipioCierreVigencia>();
                listaMunicipioCierreVigencia = Arrays
                    .asList(this.municipiosSeleccionados);
                this.getActualizacionService()
                    .guardarListaMunicipioCierreVigencias(
                        listaMunicipioCierreVigencia);
                this.municipiosSeleccionados = null;
                this.cargarMunicipios();
                this.addMensajeInfo(
                    "Se retiró satisfactoriamente la actualización/Formación registrada.");
            }
        } catch (Exception e) {
            LOGGER.debug("Error: " + e.getMessage());
            this.addMensajeError("Error al retirar la selección del municipio.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que guarda el registro de formación o actualización para la zonas y el proceso
     * seleccionado.
     *
     * @author david.cifuentes
     */
    public void guardarRegistroActualizacionOFormacion() {

        /**
         * Estados de la variable procesoBool
         *
         * ProcesoBool = TRUE - Proceso de formación ProcesoBool = FALSE - Proceso de actualización
         */
        if (this.zonasSelected != null && this.zonasSelected.length > 0) {
            // Si se seleccionaron todas las zonas, no se hace el registro de
            // las zonas, en vez de ello se marcan los municipioCierreVigencia
            // seleccionados como actualizacionTotal o formacionTotal según
            // corresponda.
            Date fecha = new Date(System.currentTimeMillis());

            // Se crean las zonas por cada uno de los municipios
            // seleccionados.
            ZonaCierreVigencia zcv = null;
            List<ZonaCierreVigencia> zonas = null;
            for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {

                zonas = new ArrayList<ZonaCierreVigencia>();
                // buscar zona para el municipio seleccionado
                for (String zona : this.zonasSelected) {
                    zcv = new ZonaCierreVigencia();
                    zcv.setZonaCodigo(zona);
                    zcv.setFechaLog(fecha);
                    zcv.setUsuarioLog(this.usuario.getLogin());
                    zcv.setMunicipioCierreVigencia(mcv);
                    if (this.procesoBool) {
                        // FORMACIÓN
                        zcv.setFechaInicioFormacion(fecha);
                        if (this.procesoCerradoBool) {
                            mcv.setFechaFinFormacion(fecha);
                        } else {
                            mcv.setFechaFinFormacion(null);
                        }
                        zcv.setFechaInicioActualizacion(null);
                        zcv.setFechaFinActualizacion(null);
                    } else {
                        // ACTUALIZACIÓN
                        zcv.setFechaInicioActualizacion(fecha);
                        if (this.procesoCerradoBool) {
                            mcv.setFechaFinActualizacion(fecha);
                        } else {
                            mcv.setFechaFinActualizacion(null);
                        }
                        zcv.setFechaInicioFormacion(null);
                        zcv.setFechaFinFormacion(null);
                    }
                    zonas.add(zcv);
                }
                if (mcv.getZonaCierreVigencias() != null &&
                     !mcv.getZonaCierreVigencias().isEmpty()) {
                    // Eliminar las zonas antiguas.
                    this.getActualizacionService()
                        .eliminarZonasDelMunicipioCierreVigencia(
                            mcv.getZonaCierreVigencias());
                }
                mcv.setZonaCierreVigencias(zonas);

                if (zonasSelected.length == 3) {
                    if (this.procesoBool) {
                        // FORMACIÓN
                        mcv.setFormacionTotal(ESiNo.SI.getCodigo());
                        mcv.setActualizacionTotal(ESiNo.NO.getCodigo());
                        mcv.setFechaInicioFormacion(fecha);
                        if (this.procesoCerradoBool) {
                            mcv.setFechaFinFormacion(fecha);
                        } else {
                            mcv.setFechaFinFormacion(null);
                        }
                        mcv.setFechaInicioActualizacion(null);
                        mcv.setFechaFinActualizacion(null);
                    } else {
                        // ACTUALIZACIÓN
                        mcv.setActualizacionTotal(ESiNo.SI.getCodigo());
                        mcv.setFormacionTotal(ESiNo.NO.getCodigo());
                        mcv.setFechaInicioActualizacion(fecha);
                        if (this.procesoCerradoBool) {
                            mcv.setFechaFinActualizacion(fecha);
                        } else {
                            mcv.setFechaFinActualizacion(null);
                        }
                        mcv.setFechaInicioFormacion(null);
                        mcv.setFechaFinFormacion(null);
                    }
                    mcv.setFechaLog(fecha);
                    mcv.setUsuarioLog(this.usuario.getLogin());
                    mcv.setSelected(false);
                } else {
                    mcv.setActualizacionTotal(ESiNo.NO.getCodigo());
                    mcv.setFormacionTotal(ESiNo.NO.getCodigo());
                }
            }

            // ******************************************* //
            // *********** IMPORTANTE :: TEMPORAL ******** //
            // ******************************************* //
            // TODO :: david.cifuentes :: Debido a que aún
            // no se tiene
            // consolidado el proceso de Actualización ni Formación
            // se realiza un set temporal de si hubo o no Actualización
            // Total, para que los programas del cierre no validen
            // información sobre los procesos de actualización o
            // formación.
            // La funcionalidad para controlar esto ya se encuentra
            // implementada, de manera que cuando se tengan los procesos
            // de actualización o formación, se debe descomentar las
            // siguientes lineas. :: 19/11/2014
            for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {
                mcv.setFechaInicioFormacion(null);
                mcv.setFechaFinFormacion(null);
                mcv.setFechaInicioActualizacion(null);
                mcv.setFechaFinActualizacion(null);
                mcv.setFormacionTotal(ESiNo.NO.getCodigo());
                mcv.setActualizacionTotal(ESiNo.NO.getCodigo());
            }
            // ******************************************* //
            // ******************************************* //

            // Converitmos el array en list para actualizar la lista.
            List<MunicipioCierreVigencia> municipiosList = Arrays
                .asList(this.municipiosSeleccionados);

            // Guardar la lista de municipios seleccionados, a los que se
            // les asignó un proceso y unas zonas determinadas.
            boolean ok = this.getActualizacionService()
                .guardarListaMunicipioCierreVigencias(municipiosList);

            if (ok) {
                this.cargarMunicipios();
                this.addMensajeInfo(
                    "Se registró el proceso y sus respectivas zonas para los municipios seleccionados.");
            } else {
                this.addMensajeError(
                    "Ocurrió un error al realizar el registro del proceso y las zonas para el municipio seleccionado.");
            }
        } else {
            this.addMensajeWarn("Para realizar el registro de actualización o formación " +
                "para los municipios selecionados debe seleccionar al menos una zona, por favor intente nuevamente.");
        }

        this.procesoCerradoBool = false;
        this.municipiosSeleccionados = null;
        this.zonasSelected = null;
        this.registroActualizacionFormacionBtnBool = false;
        this.retirarSeleccionActualizacionFormacionBtnBool = false;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el cargue de los procesos y zonas registradas, y en caso de no tener zonas
     * registradas, reinicia las variables usadas en el registro de formación o actualización para
     * los municipios selesccionados.
     *
     * @author david.cifuentes
     */
    public void cargarMunicipioProcesoZonas() {

        /**
         * Estados de la variable procesoBool
         *
         * ProcesoBool = TRUE - Proceso de formación ProcesoBool = FALSE - Proceso de actualización
         */
        // Si se ha seleccionado sólo un municipio se realiza el cargue de las
        // zonas y el proceso de actualización en el caso que se hayan
        // registrado.
        if (this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados.length == 1) {

            this.zonasSelected = null;
            // Está todo el municipio en actualización o formación.
            if (ESiNo.SI.getCodigo().equals(
                this.municipiosSeleccionados[0].getFormacionTotal())) {
                this.procesoBool = true;
                this.zonasSelected = new String[3];

                this.zonasSelected[0] = EZonaCierreVigencia.RURAL.getCodigo();
                this.zonasSelected[1] = EZonaCierreVigencia.URBANA.getCodigo();
                this.zonasSelected[2] = EZonaCierreVigencia.CORREGIMIENTOS
                    .getCodigo();
                if (this.municipiosSeleccionados[0].getFechaFinFormacion() != null) {
                    this.procesoCerradoBool = true;
                }
                return;
            }

            if (ESiNo.SI.getCodigo().equals(
                this.municipiosSeleccionados[0].getActualizacionTotal())) {
                this.procesoBool = false;
                this.zonasSelected = new String[3];
                this.zonasSelected[0] = EZonaCierreVigencia.RURAL.getCodigo();
                this.zonasSelected[1] = EZonaCierreVigencia.URBANA.getCodigo();
                this.zonasSelected[2] = EZonaCierreVigencia.CORREGIMIENTOS
                    .getCodigo();
                if (this.municipiosSeleccionados[0].getFechaFinActualizacion() != null) {
                    this.procesoCerradoBool = true;
                }
                return;
            }

            if (this.municipiosSeleccionados[0].getZonaCierreVigencias() != null &&
                 !this.municipiosSeleccionados[0]
                    .getZonaCierreVigencias().isEmpty()) {
                int i = 0;
                this.zonasSelected = new String[3];
                for (ZonaCierreVigencia zcv : this.municipiosSeleccionados[0]
                    .getZonaCierreVigencias()) {
                    if (zcv.getZonaCodigo() != null) {
                        this.zonasSelected[i] = zcv.getZonaCodigo();
                        i++;
                    }
                }

                // Se verifica para una de las zonas si el proceso de
                // actualización o formación está cerrado. Sólo se verifica para
                // una de ellas debido a que todas tienen el mismo
                // comportamiento.
                if (this.municipiosSeleccionados[0].getZonaCierreVigencias()
                    .get(0).getFechaFinActualizacion() != null ||
                     this.municipiosSeleccionados[0]
                        .getZonaCierreVigencias().get(0)
                        .getFechaFinFormacion() != null) {
                    this.procesoCerradoBool = true;
                }
            }

        } else if (this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados.length > 1) {
            // Si están dos o más municipios seleccionados y a alguno de ellos
            // ya se le ha registrado información, no se pueden editar todos
            // simultaneamente.
            for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {
                if (ESiNo.SI.getCodigo().equals(mcv.getActualizacionTotal()) ||
                     ESiNo.SI.getCodigo().equals(mcv.getFormacionTotal()) ||
                     (mcv.getZonaCierreVigencias() != null && !mcv
                    .getZonaCierreVigencias().isEmpty())) {
                    this.addMensajeWarn("A alguno de los municipios seleccionados ya se" +
                            " le ha registrado información, para poder registrar información a" +
                            " varios municipios simultaneamente deben no tener registrada información." +
                            " Si se quiere modificar alguno en particular debe seleccionarlo individualmente.");
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }
            }
            this.zonasSelected = null;
            this.procesoBool = false;
            this.procesoCerradoBool = false;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que verifica la cantidad de predios seleccionados para realizar el set de las
     * variables booleanas que permiten la activación de los botones en la pantalla.
     *
     * @author david.cifuentes
     */
    public void revisionEstadoBotones() {

        this.mismoEstadoMunSeleccionadosBool = true;

        String estadoCierreConservacion = "";
        String estadoCierreActualizacion = "";

        if (this.municipiosSeleccionados != null &&
             this.municipiosSeleccionados.length > 0) {

            if (this.municipiosSeleccionados.length == 1 &&
                EEstadoCierreMunicipio.SIN_EJECUTAR.getEstado().equals(
                    this.municipiosSeleccionados[0].getEstadoCierreConservacion())) {
                // La opción “Retirar Selección Formación/Actualización” se
                // habilita con un solo municipio seleccionado y cuando esté
                // tenga información registrada de formación o actualización
                // catastral.
                this.retirarSeleccionActualizacionFormacionBtnBool = true;
                this.registroActualizacionFormacionBtnBool = true;
                return;
            }

            // Verificar si todos los municipios tienen el mismo estado, de ser
            // así se habilitan diferentes botones en la pantalla para realizar
            // sobre los municipios seleccionados
            if (this.municipiosSeleccionados[0] != null) {
                estadoCierreConservacion = this.municipiosSeleccionados[0]
                    .getEstadoCierreConservacion();
                estadoCierreActualizacion = this.municipiosSeleccionados[0]
                    .getEstadoCierreActualizacion();

                for (MunicipioCierreVigencia mcv : this.municipiosSeleccionados) {

                    // Verificar si su estado de cierre en conservación es el
                    // mismo
                    if (estadoCierreConservacion == null ||
                         estadoCierreConservacion.trim().isEmpty()) {
                        if (mcv.getEstadoCierreConservacion() == null) {

                            // Los estados de conservación son nulos, ahora se
                            // debe verificar si su estado de actualización es
                            // el mismo
                            if (estadoCierreActualizacion == null ||
                                 estadoCierreActualizacion.trim()
                                    .isEmpty()) {
                                if (mcv.getEstadoCierreActualizacion() != null &&
                                     !estadoCierreActualizacion.trim()
                                        .isEmpty()) {
                                    this.mismoEstadoMunSeleccionadosBool = false;
                                    break;
                                }
                            } else if (!estadoCierreActualizacion.equals(mcv
                                .getEstadoCierreActualizacion())) {
                                this.mismoEstadoMunSeleccionadosBool = false;
                                break;
                            }
                        } else {
                            this.mismoEstadoMunSeleccionadosBool = false;
                            break;
                        }
                    } else if (!estadoCierreConservacion.equals(mcv
                        .getEstadoCierreConservacion())) {
                        this.mismoEstadoMunSeleccionadosBool = false;
                        break;
                    } else {
                        // Los estados de conservación son iguales, ahora se
                        // debe verificar si su estado de actualización es
                        // el mismo
                        if (estadoCierreActualizacion == null ||
                             estadoCierreActualizacion.trim().isEmpty()) {
                            if (mcv.getEstadoCierreActualizacion() != null &&
                                 !estadoCierreActualizacion.trim()
                                    .isEmpty()) {
                                this.mismoEstadoMunSeleccionadosBool = false;
                                break;
                            }
                        } else if (!estadoCierreActualizacion.equals(mcv
                            .getEstadoCierreActualizacion())) {
                            this.mismoEstadoMunSeleccionadosBool = false;
                            break;
                        }
                    }
                }
            }

            // Si los municipios seleccionados se encuentran en diferente
            // estado, no se puede realizar la validación masiva sobre los
            // seleccionados.
            if (!this.mismoEstadoMunSeleccionadosBool) {
                this.addMensajeWarn(
                    "Para realizar operaciones del cierre sobre los municipios seleccionados estos deben tener el mismo estado.");
                this.validarCierreBool = false;
                this.cerrarMunicipioBool = false;
                this.confirmarCierreBool = false;
                this.visualizarEstadisticasBool = false;
                this.retirarSeleccionActualizacionFormacionBtnBool = false;
                this.registroActualizacionFormacionBtnBool = false;
                return;
            } else if (!EEstadoCierreMunicipio.SIN_EJECUTAR.getEstado().equals(
                this.municipiosSeleccionados[0].getEstadoCierreConservacion())) {
                this.retirarSeleccionActualizacionFormacionBtnBool = false;
                this.registroActualizacionFormacionBtnBool = false;
                return;
            }
        } else {
            this.retirarSeleccionActualizacionFormacionBtnBool = false;
            this.registroActualizacionFormacionBtnBool = false;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para el manejo de la activación de los botones en el tab de cerrar municipios para el
     * proceso de conservación al seleccionar un municipio.
     *
     * @author david.cifuentes
     */
    public void seleccionarMunicipios() {
        for (MunicipioCierreVigencia mcv : this.municipiosList) {
            mcv.setSelected(this.selectedAllBool);
        }
        if (this.selectedAllBool) {
            this.municipiosSeleccionados = new MunicipioCierreVigencia[this.municipiosList
                .size()];
            for (int i = 0; i < this.municipiosList.size(); i++) {
                this.municipiosSeleccionados[i] = this.municipiosList.get(i);
            }
        } else {
            this.municipiosSeleccionados = null;
        }
        this.revisionEstadoBotones();
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método usado en la selección de uno de los municipios.
     *
     * @author david.cifuentes
     */
    public void actualizarSeleccionMunicipios() {
        List<MunicipioCierreVigencia> seleccionados = new ArrayList<MunicipioCierreVigencia>();
        for (MunicipioCierreVigencia mcv : this.municipiosList) {
            if (mcv.isSelected()) {
                seleccionados.add(mcv);
            }
        }
        if (!seleccionados.isEmpty()) {
            municipiosSeleccionados = seleccionados.toArray(
                new MunicipioCierreVigencia[seleccionados.size()]);
        } else {
            this.mismoEstadoMunSeleccionadosBool = false;
            this.municipiosSeleccionados = null;
        }
        this.revisionEstadoBotones();
    }

    /** --------------------------------------------------------------------- */
    @Override
    public void invalidarSesion() {
        super.invalidarSesion();
    }

    /**
     * Metodo que genera el reporte.
     *
     * @author david.cifuentes
     */
    public void generarReporte() {

        if (this.dominioTipoReporte != null) {

            String urlReporte = this.dominioTipoReporte.getDescripcion();

            Map<String, String> parameters = new HashMap<String, String>();

            // DEPARTAMENTO
            if (this.codigoDepartamentoSeleccionado != null &&
                 !this.codigoDepartamentoSeleccionado.trim().isEmpty()) {
                parameters.put("DEPARTAMENTO",
                    this.codigoDepartamentoSeleccionado);
            }
            // MUNICIPIO
            if (this.municipioSeleccionado != null &&
                 this.municipioSeleccionado.getMunicipioCodigo() != null) {
                parameters.put("MUNICIPIO", this.municipioSeleccionado
                    .getMunicipioCodigo().trim());
            } else {
                parameters.put("MUNICIPIO", "");
            }
            // VIGENCIA
            if (this.vigenciaDecreto != null) {
                parameters
                    .put("VIGENCIA", String.valueOf(this.vigenciaDecreto));
            }

            // TIPO DE CIERRE
            if (this.tipoCierre != null) {
                parameters
                    .put("TIPO_CIERRE", String.valueOf(this.tipoCierre));
            }

            this.reporteDTO = reportsService.generarReporte(parameters,
                urlReporte, this.usuario);
            LOGGER.debug("Terminó generarReporte");
        }
    }

    /**
     * Método que realiza la búsqueda de los predios por municipio según se seleccione en el reporte
     *
     * @author david.cifuentes
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     */

    private void populateListadoPrediosLazyly(List<ListadoPredio> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        LOGGER.debug("Inicio populateListadoPrediosLazyly");
        if (!this.filtroAvaluoExistBool) {
            List<ListadoPredio> rows;
            int sizeTotal, sizePagina;

            if (this.municipioSeleccionado != null) {
                rows = this.getConservacionService().
                    buscarListadoPrediosCierreAnualPorMunicipioVigencia(
                        this.municipioSeleccionado.getMunicipioCodigo(),
                        this.vigenciaDecreto, sortField, sortOrder, filters, first, pageSize);

                sizeTotal = this.getConservacionService().contarListadoPredios(
                    this.municipioSeleccionado.getMunicipioCodigo(),
                    this.vigenciaDecreto, filters);

                this.lazyWorkingListadoPredios.setRowCount(sizeTotal);

                sizePagina = rows.size();
                for (int i = 0; i < sizePagina; i++) {
                    answer.add(rows.get(i));
                }
            }
            this.filtroAvaluoExistBool = false;
        }

        LOGGER.debug("fin populateListadoPrediosLazyly");
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que consulta la información de los datos del reporte seleccionado y los precarga
     * paginados en un lazy data model.
     *
     * @author david.cifuentes
     */
    public void cargarInformacionReporte() {
        LOGGER.debug("Inicio cargarInformacionReporte");
        this.porcentajeAvaluoFiltro = null;
        // Cargue de los numeros prediales para el listado de predios a visualizar en el
        // reporte de cierre.
//		this.numerosPredialesListadoPredios = this.getConservacionService()
//				.cargarListadoPrediosPorMunicipioVigencia(
//						this.municipioSeleccionado.getMunicipioCodigo(),
//				this.vigenciaDecreto);
        LOGGER.debug("Terminó cargarInformacionReporte");
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que adiciona un filtro a la consulta que carga el listado de predios.
     *
     * @author david.cifuentes
     */
    @SuppressWarnings("serial")
    public void filtrarListadoPrediosPorPorcentaje() {
        this.filtroAvaluoExistBool = false;

        this.lazyWorkingListadoPredios = new LazyDataModel<ListadoPredio>() {

            @Override
            public List<ListadoPredio> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<ListadoPredio> answer = new ArrayList<ListadoPredio>();

                if (filters != null && porcentajeAvaluoFiltro != null && tipoAvaluoFiltro != null) {
                    filters.put("porcentajeAvaluoFiltro", porcentajeAvaluoFiltro.toString());
                    filters.put("tipoAvaluoFiltro", tipoAvaluoFiltro.toString());
                }

                populateListadoPrediosLazyly(answer, first, pageSize, sortField, sortOrder.
                    toString(), filters);
                return answer;
            }
        };
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método cerrar que forza realiza un llamado al método que remueve los managed bean de la
     * sesión y forza el init de tareasPendientesMB.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String cerrar() {
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }
    
    /**
     * Método que realiza la intersección de municipios de una UOC
     *
     * @param municipios
     * @param UOCs
     * @return
     */
    private List<Municipio> filtrarMunicipioPorUOC(
        List<Municipio> municipios,
        List<EstructuraOrganizacional> UOCs) {

        List<Municipio> municipiosFilter = new ArrayList<Municipio>();
        List<Municipio> municipiosUOC = null;

        for (EstructuraOrganizacional eo : UOCs) {
            municipiosUOC = this.getGeneralesService().
                findMunicipiosbyCodigoEstructuraOrganizacional(eo.getCodigo());
            if (municipiosUOC != null && !municipiosUOC.isEmpty()) {
                for (Municipio m : municipiosUOC) {
                    if (municipios.contains(m)) {
                        municipiosFilter.add(m);
                    }
                }
            }
        }

        return municipiosFilter;
    }
    
    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @author david.cifuentes
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {
        if (codigoTerritorial != null) {
            EstructuraOrganizacional estructuraOrganizacional, answer = null;
            for (EstructuraOrganizacional eo : this.getGeneralesService()
                .buscarTodasTerritoriales()) {
                if (eo.getCodigo().equals(codigoTerritorial)) {
                    answer = eo;
                    break;
                }
            }
            if (answer != null) {
                estructuraOrganizacional = this
                    .getGeneralesService()
                    .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
                if (estructuraOrganizacional != null) {
                    return estructuraOrganizacional;
                } else {
                    return answer;
                }
            }
        }
        return null;
    }
    // Fin de la clase
}
