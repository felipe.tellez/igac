/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.asignacion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioAnexo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioConstruccion;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioCultivo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioMaquinaria;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredioZona;
import co.gov.igac.snc.persistence.util.EUnidadMedidaArea;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para los casos de uso en los que se administran los detalles del avalúo
 *
 * @cu CU-SA-AC-075
 * @cu CU-SA-AC-126
 * @cu CU-SA-AC-127
 * @cu CU-SA-AC-130
 * @cu CU-SA-AC-080
 *
 * @author pedro.garcia
 */
@Component("administracionDetallesAvaluo")
@Scope("session")
public class AdministracionDetallesAvaluoMB extends SNCManagedBean {

    private static final long serialVersionUID = 256439640577161317L;

    private static final Logger LOGGER =
        LoggerFactory.getLogger(AdministracionDetallesAvaluoMB.class);

    /**
     * usuario que tiene la sesión
     */
    private UsuarioDTO loggedInUser;

    //----------   servicios
    @Autowired
    private CombosDeptosMunisMB combosDM;

    /**
     * value del botón para adicionar un nuevo detalle del avalúo. Se define dependiendo del cu
     * solicitado
     */
    private String valorBotonNuevoDetalle;

    /**
     * nombre de la página que contiene la tabla de detalles según el cu
     */
    private String paginaTablaDetalles;

    /**
     * nombre de la página que contiene el formulario para crear o modificar un detalle del avalúo,
     * según el cu
     */
    private String paginaCrearModificarDetalle;

    /**
     * nombres de páginas relacionadas a los cu de administración de detalles del avalúo
     */
    private final String PAGINA_TABLA_ANEXOS = "anexosDelAvaluoTable.xhtml";
    private final String PAGINA_CREARMODIFICAR_ANEXO = "crearModificarAnexoAvaluo.xhtml";
    private final String PAGINA_TABLA_CONSTRUCCIONES = "construccionesDelAvaluoTable.xhtml";
    private final String PAGINA_CREARMODIFICAR_CONSTRUCCION =
        "crearModificarConstruccionAvaluo.xhtml";
    private final String PAGINA_TABLA_CULTIVOS = "cultivosDelAvaluoTable.xhtml";
    private final String PAGINA_CREARMODIFICAR_CULTIVO = "crearModificarCultivoAvaluo.xhtml";
    private final String PAGINA_TABLA_MAQUINARIAS = "maquinariasDelAvaluoTable.xhtml";
    private final String PAGINA_CREARMODIFICAR_MAQUINARIA = "crearModificarMaquinariaAvaluo.xhtml";
    private final String PAGINA_TABLA_PREDIOS = "prediosDelAvaluoTable.xhtml";
    private final String PAGINA_CREARMODIFICAR_PREDIO = "crearModificarPredioAvaluo.xhtml";
    private final String PAGINA_TABLA_ZONAS = "zonasDelAvaluoTable.xhtml";
    private final String PAGINA_CREARMODIFICAR_ZONA = "crearModificarZonaAvaluo.xhtml";

    /**
     * Listas donde se consultan los diferentes detalles del avalúo segúin el cu
     */
    private ArrayList<AvaluoPredioCultivo> cultivosAvaluoList;

    private ArrayList<AvaluoPredioAnexo> anexosAvaluoList;

    private ArrayList<AvaluoPredioConstruccion> construccionesAvaluoList;

    private ArrayList<AvaluoPredioMaquinaria> maquinariasAvaluoList;

    private ArrayList<AvaluoPredio> prediosAvaluoList;

    private ArrayList<AvaluoPredioZona> zonasAvaluoList;

    /**
     * Variables que guardan el detalle seleccionado de alguna de las tablas de detalles
     */
    private AvaluoPredioAnexo selectedAnexoAvaluo;

    private AvaluoPredioConstruccion selectedConstruccionAvaluo;

    private AvaluoPredioCultivo selectedCultivoAvaluo;

    private AvaluoPredioMaquinaria selectedMaquinariaAvaluo;

    private AvaluoPredio selectedPredioAvaluo;

    private AvaluoPredioZona selectedZonaAvaluo;

    /**
     * variables con los objetos detalle de avalúo que se crean o modifican
     */
    private AvaluoPredioAnexo anexoNuevoModificado;

    private AvaluoPredioConstruccion construccionNuevaModificada;

    private AvaluoPredioCultivo cultivoNuevoModificado;

    private AvaluoPredioMaquinaria maquinariaNuevaModificada;

    private AvaluoPredio predioNuevoModificado;

    private AvaluoPredioZona zonaNuevaModificada;

    /**
     * atributos que se usan para el caso de AvaluoPredio
     */
    /**
     * se usa on objeto de esta clase porque tiene los campos para los segementos del número predial
     * y métodos para manipularlos
     */
    private FiltroDatosConsultaPredio datosPredioHelper;

    /**
     * define si se trata de una matrícula antigua
     */
    private boolean matriculaAntigua;

    //----------
    /**
     * variable que dice qué tipo de detalle se está tratando 1 = anexo 2 = construcción 3 = cultivo
     * 4 = maquinaria 5 = zona 6 = predio
     */
    private int tipoDetalleAvaluo;

    //---------------    banderas   ----
    /**
     * Indica si se deben mostrar los botones de acción, o la página es solo de consulta. Depende
     * del cu de donde se llame a este MB.
     */
    private boolean muestraBotonesAccion;

    //--------------
    private String anchoVentanaNuevoDetalle;

    //--  datos que llegan del cu que usa a este
    private long currentAvaluoId;

    /**
     * {@link Avaluo} que se consulta con el id de avaluó que llega de algún cu, o se arma aquí, o
     * llega completo
     */
    private Avaluo currentAvaluo;

    /**
     * lista de circulos registrales de la página donde se crean o modifican predios del avalúo.
     */
    private ArrayList<SelectItem> circulosRegistralesItemList;

    //------------------------------    methods    ---------------------------------
    //----------------  getters and setters   ----------------------------
    public ArrayList<SelectItem> getCirculosRegistralesItemList() {
        return this.circulosRegistralesItemList;
    }

    public void setCirculosRegistralesItemList(ArrayList<SelectItem> circulosRegistralesItemList) {
        this.circulosRegistralesItemList = circulosRegistralesItemList;
    }

    public CombosDeptosMunisMB getCombosDM() {
        return this.combosDM;
    }

    public void setCombosDM(CombosDeptosMunisMB combosDM) {
        this.combosDM = combosDM;
    }

    public boolean isMatriculaAntigua() {
        return this.matriculaAntigua;
    }

    public void setMatriculaAntigua(boolean matriculaAntigua) {
        this.matriculaAntigua = matriculaAntigua;
    }

    public FiltroDatosConsultaPredio getDatosPredioHelper() {
        return this.datosPredioHelper;
    }

    public void setDatosPredioHelper(FiltroDatosConsultaPredio datosPredioHelper) {
        this.datosPredioHelper = datosPredioHelper;
    }

    public String getAnchoVentanaNuevoDetalle() {
        return this.anchoVentanaNuevoDetalle;
    }

    public void setAnchoVentanaNuevoDetalle(String anchoVentanaNuevoDetalle) {
        this.anchoVentanaNuevoDetalle = anchoVentanaNuevoDetalle;
    }

    public ArrayList<AvaluoPredio> getPrediosAvaluoList() {
        return this.prediosAvaluoList;
    }

    public void setPrediosAvaluoList(ArrayList<AvaluoPredio> prediosAvaluoList) {
        this.prediosAvaluoList = prediosAvaluoList;
    }

    public AvaluoPredio getSelectedPredioAvaluo() {
        return this.selectedPredioAvaluo;
    }

    public void setSelectedPredioAvaluo(AvaluoPredio selectedPredioAvaluo) {
        this.selectedPredioAvaluo = selectedPredioAvaluo;
    }

    public AvaluoPredio getPredioNuevoModificado() {
        return this.predioNuevoModificado;
    }

    public void setPredioNuevoModificado(AvaluoPredio predioNuevoModificado) {
        this.predioNuevoModificado = predioNuevoModificado;
    }

    public Avaluo getCurrentAvaluo() {
        return this.currentAvaluo;
    }

    public void setCurrentAvaluo(Avaluo currentAvaluo) {
        this.currentAvaluo = currentAvaluo;
    }

    public int getTipoDetalleAvaluo() {
        return this.tipoDetalleAvaluo;
    }

    public void setTipoDetalleAvaluo(int tipoDetalleAvaluo) {
        this.tipoDetalleAvaluo = tipoDetalleAvaluo;
    }

    public boolean isMuestraBotonesAccion() {
        return this.muestraBotonesAccion;
    }

    public void setMuestraBotonesAccion(boolean muestraBotonesAccion) {
        this.muestraBotonesAccion = muestraBotonesAccion;
    }

    public AvaluoPredioCultivo getCultivoNuevoModificado() {
        return this.cultivoNuevoModificado;
    }

    public void setCultivoNuevoModificado(AvaluoPredioCultivo cultivoNuevoModificado) {
        this.cultivoNuevoModificado = cultivoNuevoModificado;
    }

    public AvaluoPredioAnexo getAnexoNuevoModificado() {
        return this.anexoNuevoModificado;
    }

    public void setAnexoNuevoModificado(AvaluoPredioAnexo anexoNuevoModificado) {
        this.anexoNuevoModificado = anexoNuevoModificado;
    }

    public AvaluoPredioConstruccion getConstruccionNuevaModificada() {
        return this.construccionNuevaModificada;
    }

    public void setConstruccionNuevaModificada(AvaluoPredioConstruccion construccionNuevaModificada) {
        this.construccionNuevaModificada = construccionNuevaModificada;
    }

    public AvaluoPredioMaquinaria getMaquinariaNuevaModificada() {
        return this.maquinariaNuevaModificada;
    }

    public void setMaquinariaNuevaModificada(AvaluoPredioMaquinaria maquinariaNuevaModificada) {
        this.maquinariaNuevaModificada = maquinariaNuevaModificada;
    }

    public AvaluoPredioZona getZonaNuevaModificada() {
        return this.zonaNuevaModificada;
    }

    public void setZonaNuevaModificada(AvaluoPredioZona zonaNuevaModificada) {
        this.zonaNuevaModificada = zonaNuevaModificada;
    }

    public String getPaginaCrearModificarDetalle() {
        return this.paginaCrearModificarDetalle;
    }

    public void setPaginaCrearModificarDetalle(String paginaCrearModificarDetalle) {
        this.paginaCrearModificarDetalle = paginaCrearModificarDetalle;
    }

    public String getPaginaTablaDetalles() {
        return this.paginaTablaDetalles;
    }

    public void setPaginaTablaDetalles(String paginaTablaDetalles) {
        this.paginaTablaDetalles = paginaTablaDetalles;
    }

    public String getValorBotonNuevoDetalle() {
        return this.valorBotonNuevoDetalle;
    }

    public void setValorBotonNuevoDetalle(String valorBotonNuevoDetalle) {
        this.valorBotonNuevoDetalle = valorBotonNuevoDetalle;
    }

    public ArrayList<AvaluoPredioCultivo> getCultivosAvaluoList() {
        return this.cultivosAvaluoList;
    }

    public void setCultivosAvaluoList(ArrayList<AvaluoPredioCultivo> cultivosAvaluoList) {
        this.cultivosAvaluoList = cultivosAvaluoList;
    }

    public ArrayList<AvaluoPredioAnexo> getAnexosAvaluoList() {
        return this.anexosAvaluoList;
    }

    public void setAnexosAvaluoList(ArrayList<AvaluoPredioAnexo> anexosAvaluoList) {
        this.anexosAvaluoList = anexosAvaluoList;
    }

    public ArrayList<AvaluoPredioConstruccion> getConstruccionesAvaluoList() {
        return this.construccionesAvaluoList;
    }

    public void setConstruccionesAvaluoList(
        ArrayList<AvaluoPredioConstruccion> construccionesAvaluoList) {
        this.construccionesAvaluoList = construccionesAvaluoList;
    }

    public ArrayList<AvaluoPredioMaquinaria> getMaquinariasAvaluoList() {
        return this.maquinariasAvaluoList;
    }

    public void setMaquinariasAvaluoList(ArrayList<AvaluoPredioMaquinaria> maquinariasAvaluoList) {
        this.maquinariasAvaluoList = maquinariasAvaluoList;
    }

    public ArrayList<AvaluoPredioZona> getZonasAvaluoList() {
        return this.zonasAvaluoList;
    }

    public void setZonasAvaluoList(ArrayList<AvaluoPredioZona> zonasAvaluoList) {
        this.zonasAvaluoList = zonasAvaluoList;
    }

    public AvaluoPredioCultivo getSelectedCultivoAvaluo() {
        return this.selectedCultivoAvaluo;
    }

    public void setSelectedCultivoAvaluo(AvaluoPredioCultivo selectedCultivoAvaluo) {
        this.selectedCultivoAvaluo = selectedCultivoAvaluo;
    }

    public AvaluoPredioAnexo getSelectedAnexoAvaluo() {
        return this.selectedAnexoAvaluo;
    }

    public void setSelectedAnexoAvaluo(AvaluoPredioAnexo selectedAnexoAvaluo) {
        this.selectedAnexoAvaluo = selectedAnexoAvaluo;
    }

    public AvaluoPredioConstruccion getSelectedConstruccionAvaluo() {
        return this.selectedConstruccionAvaluo;
    }

    public void setSelectedConstruccionAvaluo(AvaluoPredioConstruccion selectedConstruccionAvaluo) {
        this.selectedConstruccionAvaluo = selectedConstruccionAvaluo;
    }

    public AvaluoPredioMaquinaria getSelectedMaquinariaAvaluo() {
        return this.selectedMaquinariaAvaluo;
    }

    public void setSelectedMaquinariaAvaluo(AvaluoPredioMaquinaria selectedMaquinariaAvaluo) {
        this.selectedMaquinariaAvaluo = selectedMaquinariaAvaluo;
    }

    public AvaluoPredioZona getSelectedZonaAvaluo() {
        return this.selectedZonaAvaluo;
    }

    public void setSelectedZonaAvaluo(AvaluoPredioZona selectedZonaAvaluo) {
        this.selectedZonaAvaluo = selectedZonaAvaluo;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de AdministracionDetallesAvaluoMB");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();
        this.anchoVentanaNuevoDetalle = "400";

//FIXME :: datos quemados        
        this.currentAvaluoId = 172l;

        this.currentAvaluo = new Avaluo();
//FIXME :: ojo con esta variable. debería traerse el avalúo completo?
        this.currentAvaluo.setId(this.currentAvaluoId);
        this.currentAvaluo.setUnidadMedidaAreaTerreno(EUnidadMedidaArea.HECTAREA.getCodigo());

//FIXME :: este valor debe depender del cu de donde se llame a este MB
        this.muestraBotonesAccion = true;

        inicializarAdminDetalles();

        //D: consultar los detalles de avalúo según el cu
        cargarListasDetallesAvaluos();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * asigna valores a variables que determinan los valores de los botones de administración de
     * detalles, y las páginas que se abren al accionar esos botones
     */
    public void inicializarAdminDetalles() {

        switch (this.tipoDetalleAvaluo) {
            case (1): {
                this.paginaTablaDetalles = this.PAGINA_TABLA_ANEXOS;
                this.paginaCrearModificarDetalle = this.PAGINA_CREARMODIFICAR_ANEXO;
                this.valorBotonNuevoDetalle = "Registrar nuevo anexo";
                break;
            }
            case (2): {
                this.paginaTablaDetalles = this.PAGINA_TABLA_CONSTRUCCIONES;
                this.paginaCrearModificarDetalle = this.PAGINA_CREARMODIFICAR_CONSTRUCCION;
                this.valorBotonNuevoDetalle = "Registrar nueva construcción";
                break;
            }
            case (3): {
                this.paginaTablaDetalles = this.PAGINA_TABLA_CULTIVOS;
                this.paginaCrearModificarDetalle = this.PAGINA_CREARMODIFICAR_CULTIVO;
                this.valorBotonNuevoDetalle = "Registrar nuevo cultivo";
                break;
            }
            case (4): {
                this.paginaTablaDetalles = this.PAGINA_TABLA_MAQUINARIAS;
                this.paginaCrearModificarDetalle = this.PAGINA_CREARMODIFICAR_MAQUINARIA;
                this.valorBotonNuevoDetalle = "Registrar nueva maquinaria";
                break;
            }
            case (5): {
                this.paginaTablaDetalles = this.PAGINA_TABLA_ZONAS;
                this.paginaCrearModificarDetalle = this.PAGINA_CREARMODIFICAR_ZONA;
                this.valorBotonNuevoDetalle = "Registrar nueva zona";
                break;
            }
            case (6): {
                this.paginaTablaDetalles = this.PAGINA_TABLA_PREDIOS;
                this.paginaCrearModificarDetalle = this.PAGINA_CREARMODIFICAR_PREDIO;
                this.valorBotonNuevoDetalle = "Registrar nuevo predio";
                this.anchoVentanaNuevoDetalle = "550";
                break;
            }

            default:
                break;
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * action del botón de registro de nuevo detalle en la ventana principal. Crea un nuevo objeto
     * del tipo correspondiente al cu
     */
    public void registrarNuevoDetalleAvaluoInit() {

        switch (this.tipoDetalleAvaluo) {
            case (1): {
                this.anexoNuevoModificado = new AvaluoPredioAnexo();
                break;
            }
            case (2): {
                this.construccionNuevaModificada = new AvaluoPredioConstruccion();
                break;
            }
            case (3): {
                this.cultivoNuevoModificado = new AvaluoPredioCultivo();
                break;
            }
            case (4): {
                this.maquinariaNuevaModificada = new AvaluoPredioMaquinaria();
                break;
            }
            case (5): {
                this.zonaNuevaModificada = new AvaluoPredioZona();
                break;
            }
            case (6): {
                this.predioNuevoModificado = new AvaluoPredio();
                this.datosPredioHelper = new FiltroDatosConsultaPredio();
                break;
            }

            default:
                break;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "modificar" en la ventana principal. Carga el objeto seleccionado en el
     * correspondiente objeto según el caso de uso.
     */
    public void modificarNuevoDetalleAvaluoInit() {

        switch (this.tipoDetalleAvaluo) {
            case (1): {
                this.anexoNuevoModificado = this.selectedAnexoAvaluo;
                break;
            }
            case (2): {
                this.construccionNuevaModificada = this.selectedConstruccionAvaluo;
                break;
            }
            case (3): {
                this.cultivoNuevoModificado = this.selectedCultivoAvaluo;
                break;
            }
            case (4): {
                this.maquinariaNuevaModificada = this.selectedMaquinariaAvaluo;
                break;
            }
            case (5): {
                this.zonaNuevaModificada = this.selectedZonaAvaluo;
                break;
            }
            case (6): {
//FIXME :: pedro.garcia :: hace falta cargar el departamentop, municipio, num predial y círculo registral en la forma para modificar
                this.predioNuevoModificado = this.selectedPredioAvaluo;
                break;
            }

            default:
                break;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action de los botones "aceptar" en las ventanas donde se crean o modifican detalles de avalúo
     *
     * @author pedro.garcia
     */

    public void registrarModificarDetalleAvaluo() {

        boolean registroModificacionOK = true;
        Date currentDate;

//TODO :: snc.avaluos :: revisar cuando se integre quién se toma como avalúo del objeto que se inserta
        currentDate = new Date(System.currentTimeMillis());
        switch (this.tipoDetalleAvaluo) {
            case (1): {
                this.anexoNuevoModificado.setValorTotal(
                    this.anexoNuevoModificado.getValorUnidad() *
                    this.anexoNuevoModificado.getMedida());

                this.anexoNuevoModificado.setFechaLog(currentDate);
                this.anexoNuevoModificado.setUsuarioLog(this.loggedInUser.getLogin());
                this.anexoNuevoModificado.setAvaluo(this.currentAvaluo);
                registroModificacionOK = this.getAvaluosService().
                    guardarModificarDetalleAvaluo(1, this.anexoNuevoModificado);
                break;
            }
            case (2): {
                this.construccionNuevaModificada.setFechaLog(currentDate);
                this.construccionNuevaModificada.setUsuarioLog(this.loggedInUser.getLogin());
                this.construccionNuevaModificada.setAvaluo(this.currentAvaluo);
                registroModificacionOK = this.getAvaluosService().
                    guardarModificarDetalleAvaluo(2, this.construccionNuevaModificada);
                break;
            }
            case (3): {
                this.cultivoNuevoModificado.setValorTotal(
                    this.cultivoNuevoModificado.getValorUnidad() *
                    this.cultivoNuevoModificado.getCantidad());

                this.cultivoNuevoModificado.setFechaLog(currentDate);
                this.cultivoNuevoModificado.setUsuarioLog(this.loggedInUser.getLogin());
                this.cultivoNuevoModificado.setAvaluo(this.currentAvaluo);
                registroModificacionOK = this.getAvaluosService().
                    guardarModificarDetalleAvaluo(3, this.cultivoNuevoModificado);
                break;
            }
            case (4): {
                this.maquinariaNuevaModificada.setValorTotal(
                    this.maquinariaNuevaModificada.getValorUnidad() *
                    this.maquinariaNuevaModificada.getCantidad());

                this.maquinariaNuevaModificada.setFechaLog(currentDate);
                this.maquinariaNuevaModificada.setUsuarioLog(this.loggedInUser.getLogin());
                this.maquinariaNuevaModificada.setAvaluo(this.currentAvaluo);
                registroModificacionOK = this.getAvaluosService().
                    guardarModificarDetalleAvaluo(4, this.maquinariaNuevaModificada);
                break;
            }
            case (5): {
                this.zonaNuevaModificada.setValorTerreno(
                    this.zonaNuevaModificada.getValorUnitarioTerreno() *
                    this.zonaNuevaModificada.getArea());

                this.zonaNuevaModificada.setFechaLog(currentDate);
                this.zonaNuevaModificada.setUsuarioLog(this.loggedInUser.getLogin());
                this.zonaNuevaModificada.setAvaluo(this.currentAvaluo);
                registroModificacionOK = this.getAvaluosService().
                    guardarModificarDetalleAvaluo(5, this.zonaNuevaModificada);
                break;
            }
            case (6): {
                if (validarDatosPredioNuevoModificado()) {
                    Departamento departamentoPredio;
                    Municipio municipioPredio;
                    String direccionNormalizada;

                    departamentoPredio = new Departamento();
                    municipioPredio = new Municipio();
                    departamentoPredio.setCodigo(this.combosDM.getSelectedDepartamentoCod());
                    municipioPredio.setCodigo(this.combosDM.getSelectedMunicipioCod());
                    this.predioNuevoModificado.setDepartamento(departamentoPredio);
                    this.predioNuevoModificado.setMunicipio(municipioPredio);

                    //N: en la validación ya se hizo el assemble de las partes del núm. predial
                    this.predioNuevoModificado.setNumeroPredial(
                        this.datosPredioHelper.getNumeroPredial());

                    direccionNormalizada = this.getGeneralesService()
                        .obtenerDireccionNormalizada(this.predioNuevoModificado.getDireccion());
                    this.predioNuevoModificado.setDireccion(direccionNormalizada);

                    registroModificacionOK = this.getAvaluosService().
                        guardarModificarDetalleAvaluo(6, this.predioNuevoModificado);
                }
                break;
            }

            default:
                break;
        }

        if (!registroModificacionOK) {
            this.addMensajeError("Ocurrió un error al guardar o modificar el detalle del avalúo");
        } else {
            cargarListasDetallesAvaluos();
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Hace la consulta de los datos que van en las tablas de detalles del avalúo, dependiendo del
     * tipo de detalle.
     */
    private void cargarListasDetallesAvaluos() {

        switch (this.tipoDetalleAvaluo) {
            case (1): {
                this.anexosAvaluoList = (ArrayList<AvaluoPredioAnexo>) this.getAvaluosService().
                    consultarAvaluoPredioAnexoPorAvaluo(
                        this.currentAvaluoId);
                break;
            }
            case (2): {
                this.construccionesAvaluoList = (ArrayList<AvaluoPredioConstruccion>) this.
                    getAvaluosService().consultarAvaluoPredioConstruccionPorAvaluo(
                        this.currentAvaluoId);
                break;
            }
            case (3): {
                this.cultivosAvaluoList = (ArrayList<AvaluoPredioCultivo>) this.getAvaluosService().
                    consultarAvaluoPredioCultivoPorAvaluo(
                        this.currentAvaluoId);
                break;
            }
            case (4): {
                this.maquinariasAvaluoList = (ArrayList<AvaluoPredioMaquinaria>) this.
                    getAvaluosService().consultarAvaluoPredioMaquinariaPorAvaluo(
                        this.currentAvaluoId);
                break;
            }
            case (5): {
                this.zonasAvaluoList = (ArrayList<AvaluoPredioZona>) this.getAvaluosService().
                    consultarAvaluoPredioZonaPorAvaluo(
                        this.currentAvaluoId);
                break;
            }
            case (6): {
                this.prediosAvaluoList = (ArrayList<AvaluoPredio>) this.getAvaluosService().
                    consultarAvaluoPredioPorAvaluo(
                        this.currentAvaluoId);
                break;
            }

            default:
                break;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "eliminar" de la página principal. Borra un detalle de avalúo de la bd.
     */
    public void eliminarDetalleAvaluo() {

        boolean eliminado = false;

        switch (this.tipoDetalleAvaluo) {
            case (1): {
                eliminado = this.getAvaluosService().
                    eliminarDetalleAvaluo(1, this.selectedAnexoAvaluo.getId());
                break;
            }
            case (2): {
                eliminado = this.getAvaluosService().
                    eliminarDetalleAvaluo(2, this.selectedConstruccionAvaluo.getId());
                break;
            }
            case (3): {
                eliminado = this.getAvaluosService().
                    eliminarDetalleAvaluo(3, this.selectedCultivoAvaluo.getId());
                break;
            }
            case (4): {
                eliminado = this.getAvaluosService().
                    eliminarDetalleAvaluo(4, this.selectedMaquinariaAvaluo.getId());
                break;
            }
            case (5): {
                eliminado = this.getAvaluosService().
                    eliminarDetalleAvaluo(5, this.selectedZonaAvaluo.getId());
                break;
            }
            case (6): {
                eliminado = this.getAvaluosService().
                    eliminarDetalleAvaluo(6, this.selectedPredioAvaluo.getId());
                break;
            }

            default:
                break;
        }

        if (!eliminado) {
            this.addMensajeError("Ocurrió un error eliminando el objeto");
        } else {
            cargarListasDetallesAvaluos();
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action de los botones "cancelar" en las ventanas donde se crean o modifican detalles de
     * avalúo
     */
    public void cancelarRegModDetalleAvaluo() {

        this.anexoNuevoModificado = null;
        this.construccionNuevaModificada = null;
        this.cultivoNuevoModificado = null;
        this.maquinariaNuevaModificada = null;
        this.zonaNuevaModificada = null;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * valida las condiciones especiales en la creación o modificación de un {@link AvaluoPredio}
     *
     * 0. se valida que el número predial sea válido 1. el número predial debe ser único para los
     * predios asociados al avalúo 2. el predio (identificado con el número predial) debe ser
     * colidante con todos los predios asociados al avalúo
     *
     * @return
     */
    private boolean validarDatosPredioNuevoModificado() {

        boolean answer, colindante;
        String errorMessage, numeroPredial;
        ArrayList<String> numerosPredialesActuales;

        answer = true;
        errorMessage = "";
        numerosPredialesActuales = new ArrayList<String>();

        // 0:
        this.datosPredioHelper.assembleNumeroPredialFromSegments();
        numeroPredial = this.datosPredioHelper.getNumeroPredial();

        try {
            if (numeroPredial.length() < 30) {
                errorMessage = "El número predial no tiene la longitud requerida";
                throw new Exception();
            }

            // 1:
            for (AvaluoPredio ap : this.prediosAvaluoList) {
                if (ap.getNumeroPredial().equals(this.predioNuevoModificado.getNumeroPredial())) {
                    errorMessage =
                        "El número predial ya se encuentra almacenado. Intente nuevamente.";
                    numerosPredialesActuales.add(ap.getNumeroPredial());
                    throw new Exception();
                }
            }

            // 2:
            colindante = this.getGeneralesService().esColindante(
                this.predioNuevoModificado.getNumeroPredial(), numerosPredialesActuales);
            if (!colindante) {
                errorMessage =
                    "Debe ingresar un predio colindante con los predios ingresados previamente." +
                     " Intente nuevamente";
                throw new Exception();
            }
        } catch (Exception ex) {
            this.addMensajeError(errorMessage);
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            answer = false;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor en el combo de municipios de la página donde se crean o
     * modifican predios del avalúo.
     *
     * Crea la lista de círculos registrales según el municipio seleccionado
     */
    public void onChangeMunicipiosListener() {

        ArrayList<CirculoRegistral> circulosRegistrales;

        this.circulosRegistralesItemList = new ArrayList<SelectItem>();

        if (this.combosDM.getSelectedMunicipioCod() != null &&
            !this.combosDM.getSelectedMunicipioCod().isEmpty()) {
            circulosRegistrales = (ArrayList<CirculoRegistral>) this.getGeneralesService().
                getCacheCirculosRegistralesPorMunicipio(this.combosDM.getSelectedMunicipioCod());

            if (circulosRegistrales.size() > 0) {
                for (CirculoRegistral cr : circulosRegistrales) {
                    this.circulosRegistralesItemList.add(
                        new SelectItem(cr.getCodigo(), cr.getCodigo() + "-" + cr.getNombre()));
                }

            } else {
                String mensaje = "El municipio seleccionado no tiene círculos registrales asociados";
                this.addMensajeError(mensaje);
            }
        }
    }

//end of class
}
