package co.gov.igac.snc.web.mb.avaluos.ejecucion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoMovimiento;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EAvaluoMovimientoJustifica;
import co.gov.igac.snc.persistence.util.EMovimientoAvaluoTipo;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para caso de uso CU-SA-AC-050 Modificar Plazo de Ejecución de Avalúo
 *
 * @author rodrigo.hernandez
 * @cu CU-SA-AC-050
 */
@Component("modificarPlazoEjecucionAvaluo")
@Scope("session")
public class ModificacionPlazoEjecucionAvaluoMB extends SNCManagedBean {

    private static final long serialVersionUID = 8018211675080592068L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ModificacionPlazoEjecucionAvaluoMB.class);

    /**
     * Lista de avaluos
     */
    private List<Avaluo> listaAvaluos;

    /**
     * Avalúo seleccionado
     */
    private Avaluo avaluoSeleccionado;

    /**
     * Movimiento realizado
     */
    private AvaluoMovimiento avaluoMovimiento;

    /**
     * Usuario loggeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Bandera que indica si se va a mostrar o no el campo Descripcion otra justificación
     */
    private boolean banderaMostrarOtraJustificacion;

    /**
     * Bandera que indica si el tipo de avaluador escogido es externo o de nomina
     */
    private boolean banderaEsAvaluadorExterno;

    /**
     * Bandera que indica si se esta realizando la busqueda de avaluos con el fin de determinar la
     * visibilidad de la tabla de avaluos en la pagina modificarPlazoEjecucionAvaluo.xhtml
     */
    private boolean banderaEsBusquedaAvaluos;

    /**
     * Lista de los posibles intenvetores
     */
    private List<ProfesionalAvaluo> interventores;

    /**
     * Lista de los intenvetores para la busqueda
     */
    private List<SelectItem> interventoresMenu;

    /**
     * Lista de los posibles intenvetores
     */
    private List<ProfesionalAvaluo> avaluadores;

    /**
     * Lista de los intenvetores para la busqueda
     */
    private List<SelectItem> avaluadoresMenu;

    /**
     * Id del interventor seleccionado
     */
    private Long idAvaluadorSeleccionado;

    /**
     * Id del interventor seleccionado
     */
    private Long idInterventorSeleccionado;

    /*
     * --------- SETTERS Y GETTERS ----------------
     */
    public List<Avaluo> getListaAvaluos() {
        return listaAvaluos;
    }

    public void setListaAvaluos(List<Avaluo> listaAvaluos) {
        this.listaAvaluos = listaAvaluos;
    }

    public Avaluo getAvaluoSeleccionado() {
        return avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public AvaluoMovimiento getAvaluoMovimiento() {
        return avaluoMovimiento;
    }

    public void setAvaluoMovimiento(AvaluoMovimiento avaluoMovimiento) {
        this.avaluoMovimiento = avaluoMovimiento;
    }

    public boolean isBanderaMostrarOtraJustificacion() {
        return banderaMostrarOtraJustificacion;
    }

    public void setBanderaMostrarOtraJustificacion(
        boolean banderaMostrarOtraJustificacion) {
        this.banderaMostrarOtraJustificacion = banderaMostrarOtraJustificacion;
    }

    public boolean isBanderaEsAvaluadorExterno() {
        return banderaEsAvaluadorExterno;
    }

    public void setBanderaEsAvaluadorExterno(boolean banderaEsAvaluadorExterno) {
        this.banderaEsAvaluadorExterno = banderaEsAvaluadorExterno;
    }

    public List<ProfesionalAvaluo> getInterventores() {
        return interventores;
    }

    public void setInterventores(List<ProfesionalAvaluo> interventores) {
        this.interventores = interventores;
    }

    public List<SelectItem> getInterventoresMenu() {
        return interventoresMenu;
    }

    public void setInterventoresMenu(List<SelectItem> interventoresMenu) {
        this.interventoresMenu = interventoresMenu;
    }

    public List<ProfesionalAvaluo> getAvaluadores() {
        return avaluadores;
    }

    public void setAvaluadores(List<ProfesionalAvaluo> avaluadores) {
        this.avaluadores = avaluadores;
    }

    public List<SelectItem> getAvaluadoresMenu() {
        return avaluadoresMenu;
    }

    public void setAvaluadoresMenu(List<SelectItem> avaluadoresMenu) {
        this.avaluadoresMenu = avaluadoresMenu;
    }

    public Long getIdInterventorSeleccionado() {
        return idInterventorSeleccionado;
    }

    public void setIdInterventorSeleccionado(Long idInterventorSeleccionado) {
        this.idInterventorSeleccionado = idInterventorSeleccionado;
    }

    public Long getIdAvaluadorSeleccionado() {
        return idAvaluadorSeleccionado;
    }

    public void setIdAvaluadorSeleccionado(Long idAvaluadorSeleccionado) {
        this.idAvaluadorSeleccionado = idAvaluadorSeleccionado;
    }

    public boolean isBanderaEsBusquedaAvaluos() {
        return banderaEsBusquedaAvaluos;
    }

    public void setBanderaEsBusquedaAvaluos(boolean banderaEsBusquedaAvaluos) {
        this.banderaEsBusquedaAvaluos = banderaEsBusquedaAvaluos;
    }

    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#init");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.listaAvaluos = new ArrayList<Avaluo>();

        // // TODO:: rodrigo.hernandez :: Este avalúo está quemado
        // this.avaluoSeleccionado = this.getAvaluosService()
        // .obtenerAvaluoPorIdConAtributos(172L);
        this.avaluoSeleccionado = new Avaluo();

        this.avaluoMovimiento = new AvaluoMovimiento();

        this.cargarListaInterventores();
        /*
         * Se procede a seleccionar el primer interventor apenas se cargue la pantalla
         */
        this.idInterventorSeleccionado = this.interventores.get(0).getId();

        /*
         * Se cargan los avaluadores asociados al interventer seleccionado
         */
        this.cargarListaAvaluadores();

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#iniciarDatos");
    }

    /**
     * Método para cargar la lista de interventores que se selecciona para la busqueda
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaInterventores() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#cargarListaInterventores");

        this.interventoresMenu = new ArrayList<SelectItem>();

        FiltroDatosConsultaProfesionalAvaluos filtroInteventor =
            new FiltroDatosConsultaProfesionalAvaluos();
        filtroInteventor
            .setTipoVinculacion(EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA
                .getCodigo());
        filtroInteventor.setTerritorialCodigoProfesional(this.usuario
            .getCodigoTerritorial());
        this.interventores = this.getAvaluosService()
            .buscarProfesionalesAvaluosPorFiltro(filtroInteventor);

        for (ProfesionalAvaluo pa : this.interventores) {
            this.interventoresMenu.add(new SelectItem(pa.getId(), pa
                .getNombreCompleto()));
        }

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#cargarListaInterventores");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar la lista de avaluadores
     *
     * @author rodrigo.hernandez
     */
    public void cargarListaAvaluadores() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#cargarListaAvaluadores");

        this.banderaEsBusquedaAvaluos = false;
        this.avaluadores = new ArrayList<ProfesionalAvaluo>();

        if (banderaEsAvaluadorExterno) {
            this.cargarListaAvaluadoresExternos();
        } else {
            this.cargarListaAvaluadoresNomina();
        }

        this.avaluadoresMenu = new ArrayList<SelectItem>();

        if (this.avaluadores.isEmpty()) {

            this.avaluadoresMenu.add(new SelectItem(null,
                "No se encontraron avaluadores"));
        }

        for (ProfesionalAvaluo pa : this.avaluadores) {
            this.avaluadoresMenu.add(new SelectItem(pa.getId(), pa
                .getNombreCompleto()));
        }

        if (!this.avaluadores.isEmpty()) {
            this.idAvaluadorSeleccionado = this.avaluadores.get(0).getId();
        }

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#cargarListaAvaluadores");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar lista de avaluadores por nomina asociados al interventor
     *
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaAvaluadoresNomina() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#cargarListaAvaluadoresNomina");

        this.avaluadores = this.getAvaluosService()
            .obtenerAvaluadoresPorInterventorId(
                this.idInterventorSeleccionado, false);

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#cargarListaAvaluadoresNomina");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cargar lista de avaluadores externos asociados al interventor
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaAvaluadoresExternos() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#cargarListaAvaluadoresExternos");

        this.avaluadores = this.getAvaluosService()
            .obtenerAvaluadoresPorInterventorId(
                this.idInterventorSeleccionado, true);

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#cargarListaAvaluadoresExternos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para buscar los avaluos asociados al avaluador seleccionado
     *
     * @author rodrigo.hernandez
     */
    public void buscarAvaluos() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#buscarAvaluos");

        this.banderaEsBusquedaAvaluos = true;
        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();
        filtro.setIdProfesional(this.idAvaluadorSeleccionado);

        this.listaAvaluos = this.getAvaluosService()
            .obtenerAvaluosConMovimientosPorFiltro(filtro);

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#buscarAvaluos");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para habilitar campo Descripcion otra justificacion en pagina
     * modificarPlazoEjecucionAvaluo
     *
     * @author rodrigo.hernandez
     */
    public void habilitarCampoOtraJustificacion() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#habilitarCampoOtraJustificacion");

        if (this.avaluoMovimiento.getJustificacion().equals(
            EAvaluoMovimientoJustifica.OTRA_JUSTIFICACION.getCodigo())) {
            this.banderaMostrarOtraJustificacion = true;
        } else {
            this.banderaMostrarOtraJustificacion = false;
        }

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#habilitarCampoOtraJustificacion");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para iniciar el objeto AvaluoMovimiento </br> Se llama desde los botones Ampliar
     * Plazo, Suspender Avalúo y Reactivar Avaluo de la pagina modificarPlazoEjecucionAvaluo.xhtml
     *
     * @author rodrigo.hernandez
     */
    public void iniciarAvaluoMovimiento() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#iniciarAvaluoMovimiento");

        this.avaluoMovimiento = new AvaluoMovimiento();
        this.iniciarDatosAvaluoMovimiento(this.avaluoMovimiento);

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#iniciarAvaluoMovimiento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatosAvaluoMovimiento(AvaluoMovimiento avaluoMov) {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#iniciarDatosAvaluoMovimiento");

        avaluoMov.setUsuarioLog(this.usuario.getLogin());
        avaluoMov.setUsuarioMovimientoId(this.usuario.getLogin());
        avaluoMov.setFecha(Calendar.getInstance().getTime());
        avaluoMov.setFechaLog(Calendar.getInstance().getTime());
        avaluoMov.setAvaluo(this.avaluoSeleccionado);
        avaluoMov.setDiasAmpliacion(0L);

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#iniciarDatosAvaluoMovimiento");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para setear los datos de un movimiento de tipo AMPLIACION</br></br> Se llama desde el
     * boton Aceptar de la pagina ingresarDiasAmpliacionAvaluo.xhtml
     *
     * @author rodrigo.hernandez
     */
    public void crearAvaluoMovimientoAmpliacion() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#crearAvaluoMovimientoAmpliacion");

        this.avaluoMovimiento
            .setTipoMovimiento(EMovimientoAvaluoTipo.AMPLIACION.getCodigo());

        /*
         * Se guarda el movimiento de AMPLIACION creado
         */
        this.getAvaluosService().guardarActualizarAvaluoMovimiento(
            this.avaluoMovimiento, this.usuario);

        /*
         * Se recarga la tabla de avaluos
         */
        this.buscarAvaluos();

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#crearAvaluoMovimientoAmpliacion");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para setear los datos de un movimiento de tipo SUSPENSION</br></br> Se llama desde el
     * boton Aceptar de la pagina registrarFechaSuspensionAvaluo.xhtml
     *
     *
     * @author rodrigo.hernandez
     */
    public void crearAvaluoMovimientoSuspension() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#crearAvaluoMovimientoSuspension");

        /*
         * Se crea un movimiento de Reactivacion asociado al de SUSPENSION. (Req. Especial 16)
         */
        AvaluoMovimiento movimientoReactivacion = new AvaluoMovimiento();
        this.iniciarDatosAvaluoMovimiento(movimientoReactivacion);
        movimientoReactivacion
            .setTipoMovimiento(EMovimientoAvaluoTipo.REACTIVACION
                .getCodigo());

        /*
         * Se guarda el movimiento de REACTIVACION creado
         */
        this.getAvaluosService().guardarActualizarAvaluoMovimiento(
            movimientoReactivacion, this.usuario);

        /*
         * Al movimiento actual se le asigna el tipo SUSPENSION
         */
        this.avaluoMovimiento
            .setTipoMovimiento(EMovimientoAvaluoTipo.SUSPENCION.getCodigo());

        /*
         * Se guarda el movimiento de SUSPENSION creado
         */
        this.getAvaluosService().guardarActualizarAvaluoMovimiento(
            this.avaluoMovimiento, this.usuario);

        /*
         * Se recarga la tabla de avaluos
         */
        this.buscarAvaluos();

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#crearAvaluoMovimientoSuspension");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para setear los datos de un movimiento de tipo REACTIVACION</br></br> Se llama desde
     * el boton Aceptar de la pagina registrarFechaReactivacionAvaluo.xhtml
     *
     *
     * @author rodrigo.hernandez
     */
    public void crearAvaluoMovimientoReactivacion() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#crearAvaluoMovimientoReactivacion");

        this.avaluoMovimiento
            .setTipoMovimiento(EMovimientoAvaluoTipo.REACTIVACION
                .getCodigo());

        /*
         * Se guarda el movimiento de REACTIVACIOn creado
         */
        this.getAvaluosService().guardarActualizarAvaluoMovimiento(
            this.avaluoMovimiento, this.usuario);

        /*
         * Se recarga la tabla de avaluos
         */
        this.buscarAvaluos();

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#crearAvaluoMovimientoReactivacion");
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para cerrar página principal asociada
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {
        LOGGER.debug("Inicio ModificacionPlazoEjecucionAvaluoMB#cerrar");

        UtilidadesWeb.removerManagedBeans();

        LOGGER.debug("Fin ModificacionPlazoEjecucionAvaluoMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

}
