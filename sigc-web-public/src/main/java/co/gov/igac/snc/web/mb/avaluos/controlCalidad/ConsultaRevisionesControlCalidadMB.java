/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.util.EAvaluoAsignacionProfActi;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * Managed bean del CU-SA-AC-108 Consultar informes de control de calidad realizados
 *
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-108
 */
@Component("consultaRevisionesControlCalidad")
@Scope("session")
public class ConsultaRevisionesControlCalidadMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 604549531386297112L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaInformeCorreccionesAlProtocoloMB.class);

    /**
     * Avalúo asociado a la consulta de controles de calidad
     */
    private Avaluo avaluo;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Control de calidad seleccionado para ver el informe de control de calidad
     */
    private ControlCalidadAvaluo controlCalidadSeleccionado;

    /**
     * Control de calidad seleccionado para ver el informe de control de calidad
     */
    private ControlCalidadAvaluoRev revisionSeleccionada;

    // ----------------------------Métodos SET y GET----------------------------
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public ControlCalidadAvaluo getControlCalidadSeleccionado() {
        return this.controlCalidadSeleccionado;
    }

    public void setControlCalidadSeleccionado(ControlCalidadAvaluo controlCalidadSeleccionado) {
        this.controlCalidadSeleccionado = controlCalidadSeleccionado;
    }

    public ControlCalidadAvaluoRev getRevisionSeleccionada() {
        return this.revisionSeleccionada;
    }

    public void setRevisionSeleccionada(ControlCalidadAvaluoRev revisionSeleccionada) {
        this.revisionSeleccionada = revisionSeleccionada;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaRevisionesControlCalidadMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
    }

    /**
     * Método para llamar este caso de uso desde otros Managed Beans. Se debe inicializar con el
     * metodo iniciarVariables().
     *
     * @author felipe.cadena
     *
     */
    public void cargarDesdeOtrosMB(Long idControlCalidad, Avaluo avaluo) {
        LOGGER.debug("En ConsultaRevisionesControlCalidadMB#cargarDesdeOtrosMB");
        this.avaluo = avaluo;
        this.controlCalidadSeleccionado = this.getAvaluosService().
            obtenerControlCalidadAvaluoPorId(idControlCalidad);
    }

    /**
     * Método para consultar el informe de la revisión seleccionada
     *
     * @author felipe.cadena
     */
    public void consultarInforme() {
        LOGGER.debug("Consultando informe de revision...");
        RealizaInformeControlCalidadMB informeControlCalidadMB =
            (RealizaInformeControlCalidadMB) UtilidadesWeb.getManagedBean(
                "realizaInformeControlCalidad");

        informeControlCalidadMB.llamarConsultarRevision(
            this.avaluo.getId(),
            this.revisionSeleccionada.getId(),
            this.controlCalidadSeleccionado.getId(),
            EAvaluoAsignacionProfActi.CC_SEDE_CENTRAL.getCodigo());
        LOGGER.debug("Informe consultado");
    }

}
