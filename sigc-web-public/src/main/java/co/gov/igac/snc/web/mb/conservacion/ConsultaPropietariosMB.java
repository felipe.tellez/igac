package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("consultaPropietarios")
@Scope("session")
public class ConsultaPropietariosMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4080047933059767210L;

    private Predio predio;
    private Persona persona;

    public Predio getPredio() {
        return this.predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        this.predio = new Predio();

        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");

        Long predioId = mb001.getSelectedPredioId();

        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioId != null) {
            if (!esPredioOrigen) {
                this.getPredioByPredioId(predioId);
            } else {
                this.predio = mb001.getSelectedPredio1();
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * se define esta método solo para mantener el estándar del tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * @modified pedro.garcia 21-03-2012
     * @param predioId
     */
    public void getPredioByPredioId(Long predioId) {
        this.predio = this.getConservacionService().obtenerPredioConPersonasPorPredioId(predioId);
    }
//--------------------------------------------------------------------------------------------------

    public String getTotalPropietarios() {
        if (this.predio != null && this.predio.getPersonaPredios() != null) {
            return Integer.valueOf(this.predio.getPersonaPredios().size()).toString();
        }
        return "0";
    }

    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;

        // Se elimina el managed bean de consulta bloqueo persona
        UtilidadesWeb.removerManagedBean("consultaBloqueoPersona");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link que se muestra cuando un propietario está bloqueado. Se usa para forzar la
     * inicialización del mb de consulta del bloqueo de una persona
     */
    public void cleanConsultaBloqueoPersonaMB() {
        UtilidadesWeb.removerManagedBean("consultaBloqueoPersona");
        ConsultaBloqueoPersonaMB mbConsultaBloqueo = (ConsultaBloqueoPersonaMB) UtilidadesWeb
            .getManagedBean("consultaBloqueoPersona");
        mbConsultaBloqueo.init();
    }
}
