package co.gov.igac.snc.web.util;

/**
 * Enumeracion para los posibles valores del los cargues desde cica
 *
 * @author felipe.cadena
 */
public enum ECargueCicaEstado {

    PROCESANDO("PROCESANDO"),
    ERROR("ERROR"),
    RECHAZADO("RECHAZADO"),
    CARGUE_EXITOSO("CARGUE EXITOSO"),
    //Estados del cargue geográfico de actualización-CICA
    EN_PROCESO("EN PROCESO"),
    SIN_VALIDAR("SIN VALIDAR"),
    VALIDADO("VALIDADO"),
    VALIDACION_EXITOSA("VALIDACION EXITOSA"),
    VALIDACION_FALLIDA("VALIDACION FALLIDA"),
    ERRORES("ERRORES"),
    //Estado cargue GDB
    CARGADA("CARGADA");

    private String codigo;

    private ECargueCicaEstado(String valor) {
        this.codigo = valor;
    }

    public String getCodigo() {
        return codigo;
    }

    public Boolean isCodigoIgual(String valor) {
        return this.codigo.equals(valor);
    }

}
