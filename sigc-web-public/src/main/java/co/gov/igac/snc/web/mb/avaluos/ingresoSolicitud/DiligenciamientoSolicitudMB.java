package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitudEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesDiligenciamientoSolicitudAvaluoComercial;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase para diligenciar Solicitud de Avaluo Comercial
 *
 * @author rodrigo.hernandez
 *
 */
@Component("diligenciamientoSolicitudAvaluo")
@Scope("session")
public class DiligenciamientoSolicitudMB extends SNCManagedBean {

    /**
     * serial auto-generado
     */
    private static final long serialVersionUID = 5417225239165088096L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DiligenciamientoSolicitudMB.class);

    private UsuarioDTO usuario;

    /* --- Objetos a usados en MB's--- */
    /**
     * Solicitud a crear o cargar
     */
    private Solicitud solicitud;

    /*
     * Listas de solicitantesSolicitud(Solicitantes y Contactos de Predio) asociada a la solicitud.
     * Usada en MB para CU-SA-AC-121, CU-SA-AC-109
     */
    /**
     * Lista que trae tanto solicitantes como contactos de predio
     */
    private List<SolicitanteSolicitud> listaSolicitantesSolicitud;

    /**
     * Lista que almacena los solicitantes asociados a la solicitud
     */
    private List<SolicitanteSolicitud> listaSolicitantes;

    /**
     * Lista que almacena los contactos de predios asociados a la solicitud
     */
    private List<SolicitanteSolicitud> listaContactosPredio;

    /**
     * Solicitante asociado a la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Lista de predios asociados a la solicitud. Usada en MB para CU-SA-AC-046
     */
    private List<SolicitudPredio> listaPredios;

    /**
     * Lista de documentos asociados a la solicitud. Usada en MB para CU-SA-AC-002
     */
    private List<SolicitudDocumentacion> listaDocumentosExistentes;

    /* ---------------------- Managed Beans de casos de usos asociados -------------------------------------------- */
    /**
     * MB para CU-SA-AC-121
     */
    @Autowired
    private DiligenciamientoInformacionSolicitanteMB diligenciamientoInfoSolicitanteMB;

    /**
     * MB para CU-SA-AC-136
     */
    @Autowired
    private DiligenciamientoSolicitudInformacionTipoTramiteMB diligenciamientoInfoTipoTramiteMB;

    /**
     * MB para CU-SA-AC-46
     */
    @Autowired
    private AdministracionPrediosAvaluosMB administracionPrediosAvaluosMB;

    /**
     * MB para CU-SA-AC-139
     */
    @Autowired
    private CarguePrediosDeAvaluoDesdeArchivoMB carguePrediosAvaluosMB;

    /**
     * MB para CU-SA-AC-109
     */
    @Autowired
    private AdministracionInfoContactoDePrediosMB administracionInfoContactoDePrediosMB;

    /**
     * MB para CU-SA-AC-002
     */
    @Autowired
    private AdministracionDocumentacionAvaluoComercialMB administracionDocumentacionTramiteAvaluoComercialMB;

    /* ---------------------- Datos para informacion de radicación seleccionada en el process --------------------- */
    /**
     * Variable para almacenar número de radicación
     */
    private String numeroRadicacion;

    /**
     * Variable para almacenar fecha de radicación
     */
    private String fechaRadicacion;

    /**
     * Variable para almacenar hora de radicación
     */
    private String horaRadicacion;
    /* ---------------------- Banderas para validar existencia de la informacion --------------------- */

    /**
     * Variable para indicar si existe solicitantes asociados a la solicitud
     */
    private boolean banderaExisteSolicitante;

    /**
     * Variable para indicar si existe el tramite asociado a la solicitud
     */
    private boolean banderaExisteTipoTramite;

    /**
     * Variable para indicar si existen predios asociados a la solicitud
     */
    private boolean banderaExistePredios;

    /**
     * Variable para indicar si existen contactos para asociar a los predios de una solicitud
     */
    private boolean banderaExisteContactosPredios;

    /**
     * Variable para indicar si existen documentos asociados a la solicitud
     */
    private boolean banderaExisteDocumentos;

    /**
     * Variable para indicar si existe pago asociado a la solicitud
     */
    private boolean banderaExisteFormaPago;

    /* ---------------------- Banderas para validar almacenamiento de informacion --------------------- */
    /**
     * Lista de banderas para indicar si la informacion de los paneles que componen la pantalla, ya
     * fue almacenada
     */
    private List<Boolean> listaBanderasInfoAlmacenada;

    /**
     * Variable para indicar si la información del Solicitante ya fue almacenada
     */
    private boolean banderaInformacionSolicitanteAlmacenada;

    /**
     * Variable para indicar si la información del Tipo Tramite ya fue almacenada
     */
    private boolean banderaInformacionTipoTramiteAlmacenada;

    /**
     * Variable para indicar si la información de los Predios ya fue almacenada
     */
    private boolean banderaInformacionPrediosAlmacenada;

    /**
     * Variable para indicar si la información de los Documentos ya fue almacenada
     */
    private boolean banderaInformacionDocumentosAlmacenada;

    /**
     * Variable para indicar si la información de la Forma de Pago ya fue almacenada
     */
    private boolean banderaInformacionFormaPagoAlmacenada;
    /* ------------------------------------------------------------------------------------------------------- */

    /**
     * Variable para indicar si se puede almacenar definitivamente la solicitud
     */
    private boolean banderaFinDiligenciamientoHabilitado;

    /**
     * Variable que indica si la solicitud fue almacenada definitivamente o no
     * </br> <b>True: </b>La solicitud fue almacenada definitivamente </br>
     * <b>False: </b>La solicitud fue almacenada temporalmente
     */
    private boolean banderaModoLecturaHabilitado;

    /**
     * Variable que indica si la informacion de la solicitud fue: </br>
     * <ol>
     * <li>Almacenada temporalmente</li>
     * <li>Almacenada definitivamente</li>
     * <li>No existe</li>
     * </ol>
     *
     */
    private int codigoTipoAlmacenamientoInfoSolicitud;

    /**
     * Formato para la hora
     */
    private final DateFormat formatoHora = new SimpleDateFormat("h:mm a");

    /**
     * Formato para la fecha
     */
    private final DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    /* ------------------------ Setters y Getters ---------------------------------- */
    public String getNumeroRadicacion() {
        return numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public String getFechaRadicacion() {
        return fechaRadicacion;
    }

    public void setFechaRadicacion(String fechaRadicacion) {
        this.fechaRadicacion = fechaRadicacion;
    }

    public String getHoraRadicacion() {
        return horaRadicacion;
    }

    public void setHoraRadicacion(String horaRadicacion) {
        this.horaRadicacion = horaRadicacion;
    }

    public boolean isBanderaInformacionSolicitanteAlmacenada() {
        return banderaInformacionSolicitanteAlmacenada;
    }

    public void setBanderaInformacionSolicitanteAlmacenada(
        boolean banderaInformacionSolicitanteAlmacenada) {
        this.banderaInformacionSolicitanteAlmacenada = banderaInformacionSolicitanteAlmacenada;
    }

    public boolean isBanderaInformacionTipoTramiteAlmacenada() {
        return banderaInformacionTipoTramiteAlmacenada;
    }

    public void setBanderaInformacionTipoTramiteAlmacenada(
        boolean banderaInformacionTipoTramiteAlmacenada) {
        this.banderaInformacionTipoTramiteAlmacenada = banderaInformacionTipoTramiteAlmacenada;
    }

    public boolean isBanderaInformacionPrediosAlmacenada() {
        return banderaInformacionPrediosAlmacenada;
    }

    public void setBanderaInformacionPrediosAlmacenada(
        boolean banderaInformacionPrediosAlmacenada) {
        this.banderaInformacionPrediosAlmacenada = banderaInformacionPrediosAlmacenada;
    }

    public boolean isBanderaInformacionDocumentosAlmacenada() {
        return banderaInformacionDocumentosAlmacenada;
    }

    public void setBanderaInformacionDocumentosAlmacenada(
        boolean banderaInformacionDocumentosAlmacenada) {
        this.banderaInformacionDocumentosAlmacenada = banderaInformacionDocumentosAlmacenada;
    }

    public boolean isBanderaInformacionFormaPagoAlmacenada() {
        return banderaInformacionFormaPagoAlmacenada;
    }

    public void setBanderaInformacionFormaPagoAlmacenada(
        boolean banderaInformacionFormaPagoAlmacenada) {
        this.banderaInformacionFormaPagoAlmacenada = banderaInformacionFormaPagoAlmacenada;
    }

    public boolean isBanderaFinDiligenciamientoHabilitado() {
        return banderaFinDiligenciamientoHabilitado;
    }

    public void setBanderaFinDiligenciamientoHabilitado(
        boolean banderaActivarFinalizarRadicacionSolicitud) {
        this.banderaFinDiligenciamientoHabilitado = banderaActivarFinalizarRadicacionSolicitud;
    }

    public boolean isBanderaModoLecturaHabilitado() {
        return banderaModoLecturaHabilitado;
    }

    public void setBanderaModoLecturaHabilitado(
        boolean banderaModoLecturaHabilitado) {
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
    }

    public boolean isBanderaExisteSolicitante() {
        return banderaExisteSolicitante;
    }

    public void setBanderaExisteSolicitante(boolean banderaExisteSolicitante) {
        this.banderaExisteSolicitante = banderaExisteSolicitante;
    }

    public boolean isBanderaExisteTipoTramite() {
        return banderaExisteTipoTramite;
    }

    public void setBanderaExisteTipoTramite(boolean banderaExisteTipoTramite) {
        this.banderaExisteTipoTramite = banderaExisteTipoTramite;
    }

    public boolean isBanderaExistePredios() {
        return banderaExistePredios;
    }

    public void setBanderaExistePredios(boolean banderaExistePredios) {
        this.banderaExistePredios = banderaExistePredios;
    }

    public boolean isBanderaExisteDocumentos() {
        return banderaExisteDocumentos;
    }

    public void setBanderaExisteDocumentos(boolean banderaExisteDocumentos) {
        this.banderaExisteDocumentos = banderaExisteDocumentos;
    }

    public boolean isBanderaExisteFormaPago() {
        return banderaExisteFormaPago;
    }

    public void setBanderaExisteFormaPago(boolean banderaExisteFormaPago) {
        this.banderaExisteFormaPago = banderaExisteFormaPago;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public List<SolicitanteSolicitud> getListaSolicitantesSolicitud() {
        return listaSolicitantesSolicitud;
    }

    public void setListaSolicitantesSolicitud(
        List<SolicitanteSolicitud> listaSolicitantesSolicitud) {
        this.listaSolicitantesSolicitud = listaSolicitantesSolicitud;
    }

    public int getCodigoTipoAlmacenamientoInfoSolicitud() {
        return codigoTipoAlmacenamientoInfoSolicitud;
    }

    public void setCodigoTipoAlmacenamientoInfoSolicitud(
        int codigoTipoAlmacenamientoInfoSolicitud) {
        this.codigoTipoAlmacenamientoInfoSolicitud = codigoTipoAlmacenamientoInfoSolicitud;
    }

    public List<SolicitudPredio> getListaPredios() {
        return listaPredios;
    }

    public void setListaPredios(List<SolicitudPredio> listaPredios) {
        this.listaPredios = listaPredios;
    }

    public SolicitanteSolicitud getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public List<SolicitanteSolicitud> getListaContactosPredio() {
        return listaContactosPredio;
    }

    public void setListaContactosPredio(
        List<SolicitanteSolicitud> listaContactosPredio) {
        this.listaContactosPredio = listaContactosPredio;
    }

    public List<SolicitanteSolicitud> getListaSolicitantes() {
        return listaSolicitantes;
    }

    public void setListaSolicitantes(
        List<SolicitanteSolicitud> listaSolicitantes) {
        this.listaSolicitantes = listaSolicitantes;
    }

    public boolean isBanderaExisteContactosPredios() {
        return banderaExisteContactosPredios;
    }

    public void setBanderaExisteContactosPredios(
        boolean banderaExisteContactosPredios) {
        this.banderaExisteContactosPredios = banderaExisteContactosPredios;
    }

    public List<Boolean> getListaBanderasInfoAlmacenada() {
        return listaBanderasInfoAlmacenada;
    }

    public void setListaBanderasInfoAlmacenada(
        List<Boolean> listaBanderasInfoAlmacenada) {
        this.listaBanderasInfoAlmacenada = listaBanderasInfoAlmacenada;
    }

    // -------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#init");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.iniciarDatosRadicacion();
        this.iniciarBanderas();
        this.iniciarObjetosMB();
        this.buscarSolicitud();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#iniciarDatos");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos de la radicación
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatosRadicacion() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#iniciarDatosRadicacion");

// FIXME::rodrigo.hernandez::04-09-2012::El dato está quemado
        // quemados, se debe tomar del objeto de negocio del process
        this.numeroRadicacion = "60402012ER00041221";

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#iniciarDatosRadicacion");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método que inicia las banderas usadas para visibilidad de paneles e indicacion del estado de
     * la informacion manejada (Almacenada o no almacenada)
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderas() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#iniciarBanderas");

        this.banderaFinDiligenciamientoHabilitado = false;
        this.banderaModoLecturaHabilitado = false;

        this.iniciarBanderasExisteInformacion();

        this.listaBanderasInfoAlmacenada = new ArrayList<Boolean>();
        for (int cont = 0; cont <=
            ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_FORMA_PAGO; cont++) {
            this.listaBanderasInfoAlmacenada.add(false);
        }

        this.modificarBanderasInformacionAlmacenadaSegunPanel(
            ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE,
            false);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#iniciarBanderas");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar los objetos que se envian a los MB asociados
     *
     * @author rodrigo.hernandez
     */
    private void iniciarObjetosMB() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#iniciarObjetosMB");

        this.solicitante = new SolicitanteSolicitud();
        this.listaSolicitantesSolicitud = new ArrayList<SolicitanteSolicitud>();
        this.listaPredios = new ArrayList<SolicitudPredio>();
        this.listaContactosPredio = new ArrayList<SolicitanteSolicitud>();
        this.listaSolicitantes = new ArrayList<SolicitanteSolicitud>();
        this.listaDocumentosExistentes = new ArrayList<SolicitudDocumentacion>();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#iniciarObjetosMB");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para determinar si se crea una solicitud o se carga
     *
     * @author rodrigo.hernandez
     */
    private void buscarSolicitud() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#buscarSolicitud");

        boolean banderaCargarSolicitud = false;

        // Se valida si existe o no la solicitud
        banderaCargarSolicitud = this.validarExistenciaSolicitud();

        this.determinarTipoInformacion();

        // Define el tipo de almacenamiento de la información de la solicitud
        this.banderaModoLecturaHabilitado = this
            .setModoDatos(this.codigoTipoAlmacenamientoInfoSolicitud);

        if (banderaCargarSolicitud) {
            this.cargarSolicitud();
        } else {
            this.iniciarSolicitud();
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#buscarSolicitud");
    }

    /* --------- Métodos para buscar, cargar y crear una solicitud --------- */
    /**
     * Método para validar si existe o no una solicitud
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true:</b> si existe la solicitud </br> <b>false:</b> si no existe la solicitud
     */
    private boolean validarExistenciaSolicitud() {
        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarExistenciaSolicitud");

        boolean result = false;

        this.solicitud = this.getAvaluosService().buscarSolicitudPorNumero(
            this.numeroRadicacion);

        // Se valida que exista la solicitud
        if (this.solicitud != null) {
            result = true;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarExistenciaSolicitud");

        return result;
    }

    /**
     * Método para crear una nueva solicitud
     *
     * @author rodrigo.hernandez
     */
    private void iniciarSolicitud() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#crearSolicitud");

        this.solicitud = new Solicitud();

        this.iniciarDatosDefaultSolicitud();

        /* Se crea en BD la solicitud */
        this.guardarSolicitud();

        this.enviarInformacionAPanelInfoSolicitante();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#crearSolicitud");
    }

    /**
     * Crea una solicitud con datos por default
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatosDefaultSolicitud() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#iniciarDatosDefaultSolicitud");

        Date fecha = Calendar.getInstance().getTime();

        this.fechaRadicacion = formatoFecha.format(fecha);
        this.horaRadicacion = formatoHora.format(fecha);

        this.solicitud.setNumero(this.numeroRadicacion);
        this.solicitud.setFecha(fecha);
        this.solicitud.setFechaLog(Calendar.getInstance().getTime());
        this.solicitud.setUsuarioLog(this.usuario.getNombreCompleto());
        this.solicitud.setTipo(ETramiteTipoTramite.SOLICITUD_DE_AVALUO
            .getCodigo());

        this.solicitud.setEstado(ESolicitudEstado.BORRADOR.getCodigo());
        this.solicitud.setFinalizadoJSF(false);
        this.solicitud.setFolios(0);
        this.solicitud.setAnexos(0);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#iniciarDatosDefaultSolicitud");

    }

    /**
     * Método que indica si la informacion de la solicitud fue: </br>
     * <ol>
     * <li>Almacenada temporalmente</li>
     * <li>Almacenada definitivamente</li>
     * <li>No existe</li>
     * </ol>
     *
     * @author rodrigo.hernandez
     */
    private void determinarTipoInformacion() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#determinarTipoInformacion");

        // Valida si la solicitud existe
        if (this.solicitud != null) {
            // Valida si el estado de la solicitud es Ingresada
            if (this.solicitud.getEstado().equals(
                ESolicitudEstado.INGRESADA.getCodigo())) {
                this.codigoTipoAlmacenamientoInfoSolicitud =
                    Constantes.AC_TIPO_INFORMACION_SOLICITUD_DEFINITIVA;
            } // Valida si el estado de la solicitud es Borrador
            else if (this.solicitud.getEstado().equals(
                ESolicitudEstado.BORRADOR.getCodigo())) {
                this.codigoTipoAlmacenamientoInfoSolicitud =
                    Constantes.AC_TIPO_INFORMACION_SOLICITUD_TEMPORAL;
            }
        } else {
            this.codigoTipoAlmacenamientoInfoSolicitud =
                Constantes.AC_TIPO_INFORMACION_SOLICITUD_NO_EXISTENTE;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#determinarTipoInformacion");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para cargar una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarSolicitud() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarSolicitud");

        this.horaRadicacion = formatoHora.format(this.solicitud.getFecha());
        this.fechaRadicacion = formatoFecha.format(this.solicitud.getFecha());

        this.cargarInformacionSolicitante();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarSolicitud");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para buscar la lista de solicitantes asociados a una solicitud
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true:</b> si existen solicitantes asociados a la solicitud
     * </br> <b>false:</b> si no existen solicitantes asociados a la solicitud </br>
     */
    private void buscarSolicitanteSolicitud() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#validarExistenciaInformacionSolicitante");

        // Busca la lista de solicitantes asociados a la solicitud
        this.listaSolicitantesSolicitud = this.getTramiteService()
            .obtenerSolicitantesSolicitud2(this.solicitud.getId());

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarExistenciaInformacionSolicitante");

    }

    // ----------------------------------------------------------------------------------------------------------
    /**
     * Método para buscar el solicitante y/o representante legal de una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void buscarInformacionSolicitantes() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#buscarInformacionSolicitantes");

        this.listaSolicitantes = new ArrayList<SolicitanteSolicitud>();

        // Se extraen los contactos de los predios asociados a la solicitud si
        // existen
        for (SolicitanteSolicitud solSol : this.listaSolicitantesSolicitud) {
            // Valida que el tipo de relacion del SolicitanteSolicitud sea
            // Contacto con el fin de identificar a los
            // posibles contactos de predios asociados a la solicitud
            if (!solSol.getRelacion().equals(
                ESolicitanteSolicitudRelac.CONTACTO.toString())) {
                this.listaSolicitantes.add(solSol);
            }
        }

        // Se valida que existan solicitantes asociados a la solicitud
        if (this.listaSolicitantes != null && this.listaSolicitantes.size() > 0) {
            this.banderaExisteSolicitante = true;
        } else {
            this.banderaExisteSolicitante = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#buscarInformacionSolicitantes");
    }

    /*
     * ----------- Métodos para buscar, cargar y crear informacion del solicitante de la solicitud
     */
    /**
     * Método para cargar la informacion del solicitante asociado a una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarInformacionSolicitante() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarInformacionSolicitante");

        // Carga todos los SolicitanteSolicitud asociados a la solicitud
        this.buscarSolicitanteSolicitud();

        // Filtra los SolicitanteSolicitud por la relacion PROPIETARIO y
        // REPRESENTANTE
        this.buscarInformacionSolicitantes();

        if (this.banderaExisteSolicitante) {

            this.enviarInformacionAPanelInfoSolicitante();

            // Se asigna la lista de solicitantesSolicitud a la solicitud
            this.solicitud.setSolicitanteSolicituds(this.listaSolicitantes);

            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE,
                true);
            this.setSolicitanteSolicutudSeleccionado();
            this.cargarInformacionTipoTramite();
        } else {
            this.listaSolicitantes = new ArrayList<SolicitanteSolicitud>();

            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE,
                false);
        }

        this.enviarInformacionAPanelInfoSolicitante();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarInformacionSolicitante");
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------- Métodos para informacion de tipo tramite
    // --------------------------------
    /**
     * Método para buscar datos relacionados con el tipo de trámite
     *
     * @author rodrigo.hernandez
     */
    private void buscarInformacionTipoTramite() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#buscarInformacionTipoTramite");

        /*
         * Se parte del hecho que una solicitud tiene informacion de solicitud si tiene uno de los
         * siguientes datos: MARCO JURIDICO, TRAMITE_NUMERO_RADICACION_REF y SUBSIDIO_IMPUGNACION
         *
         * Para tener en cuenta:
         *
         * El dato ORIGEN siempre sera diferente a null. Mirar metodo
         * DiligenciamientoSolicitudInformacionTipoTramiteMB
         *
         * El dato TIPO siempre se inicia en el metodo iniciarDatosDefaultSolicitud
         */
        if (this.solicitud.getMarcoJuridico() == null &&
             this.solicitud.getTramiteNumeroRadicacionRef() == null &&
             this.solicitud.getAsociadaSolicitudNumero() == null &&
             this.solicitud.getSubsidioImpugnacion() == null) {

            this.banderaExisteTipoTramite = false;

        } else {
            this.banderaExisteTipoTramite = true;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#buscarInformacionTipoTramite");
    }

    /**
     * Método para cargar Informacion de Tipo de Tramite
     *
     * @author rodrigo.hernandez
     */
    private void cargarInformacionTipoTramite() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarInformacionTipoTramite");

        this.buscarInformacionTipoTramite();

        if (this.banderaExisteTipoTramite) {

            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_TIPO_TRAMITE,
                true);

            this.cargarInformacionPredios();
        } else {
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_TIPO_TRAMITE,
                false);
        }

        this.enviarInformacionAPanelInfoTramite();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarInformacionTipoTramite");
    }

    // ---- Métodos para informacion de predios -----
    /**
     * Método para cargar los predios asociados a una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void buscarPredios() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#buscarPredios");

        this.listaPredios = this.getTramiteService()
            .buscarSolicitudesPredioBySolicitudId(this.solicitud.getId());

        if (listaPredios != null && !listaPredios.isEmpty()) {
            this.banderaExistePredios = true;
            this.solicitud.setSolicitudPredios(listaPredios);
        } else {
            this.banderaExistePredios = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#buscarPredios");
    }

    /**
     * Método para cargar la informacion de los Predios asociados a una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarInformacionPredios() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarInformacionPredios");

        this.buscarPredios();

        if (banderaExistePredios) {
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_PREDIOS,
                true);
        } else {
            this.listaPredios = new ArrayList<SolicitudPredio>();
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_PREDIOS,
                false);
        }

        this.cargarInformacionContactosPredios();
        this.cargarInformacionDocumentos();
        this.enviarInformacionAPanelInfoPredios();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarInformacionPredios");
    }

    // ---- Métodos para informacion de contactos de predios -----
    /**
     * Método para buscar los contactos asociados a los predios de una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void buscarInformacionContactosPredios() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#buscarInformacionContactosPredios");

        this.listaContactosPredio = new ArrayList<SolicitanteSolicitud>();

        // Se extraen los contactos de los predios asociados a la solicitud si
        // existen
        for (SolicitanteSolicitud solSol : this.listaSolicitantesSolicitud) {
            // Valida que el tipo de relacion del SolicitanteSolicitud sea
            // Contacto con el fin de identificar a los
            // posibles contactos de predios asociados a la solicitud
            if (solSol.getRelacion().equals(
                ESolicitanteSolicitudRelac.CONTACTO.toString())) {
                this.listaContactosPredio.add(solSol);
            }
        }

        if (this.listaContactosPredio.size() > 0) {
            this.banderaExisteContactosPredios = true;
        } else {
            this.banderaExisteContactosPredios = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#buscarInformacionContactosPredios");

    }

    /**
     *
     * Método para crear datos de contactos de predios asociados a una solicitud
     *
     * @author rodrigo.hernandez
     *
     */
    private void cargarInformacionContactosPredios() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarInformacionContactosPredios");

        this.buscarInformacionContactosPredios();

        if (this.banderaExisteContactosPredios) {
            this.banderaInformacionPrediosAlmacenada = true;

        } else {
            this.banderaInformacionPrediosAlmacenada = false;
            this.listaContactosPredio = new ArrayList<SolicitanteSolicitud>();
        }

        this.enviarInformacionAPanelInfoContactosPredios();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarInformacionContactosPredios");
    }

    // ---- Métodos para informacion de documentos -----
    /**
     * Método para buscar los documentos de una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void buscarInformacionDocumentos() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#buscarInformacionDocumentos");

        this.listaDocumentosExistentes = this.getAvaluosService()
            .obtenerSolicitudDocumentacionPorIdSolicitudAgrupadas(
                this.solicitud.getId());

        if (this.listaDocumentosExistentes.size() > 0) {
            this.banderaExisteDocumentos = true;
        } else {
            this.banderaExisteDocumentos = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#buscarInformacionDocumentos");

    }

    /**
     * Método para cargar la informacion de los Documentos asociados a una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarInformacionDocumentos() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarInformacionDocumentos");

        this.buscarInformacionDocumentos();

        if (this.banderaExisteDocumentos) {
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_DOCUMENTOS,
                true);

        } else {
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_DOCUMENTOS,
                false);
        }

        this.enviarInformacionAPanelInfoDocumentos();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarInformacionDocumentos");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para cargar la informacion del Pago asociado a una solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarInformacionFormaPago() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#cargarInformacionFormaPago");

        // TODO :: rodrigo.hernandez :: 03-09-2012 :: desarrollar funcionalidad
        // para cargar datos asociados a la forma de pago
        // CU-SA-AC-099
        LOGGER.debug("Fin DiligenciamientoSolicitudMB#cargarInformacionFormaPago");
    }

    /* ------------- Métodos para enviar informacion a MB asociados --------- */
    /**
     * Método para enviar datos requeridos por MB de CU-SA-AC-121 (Informacion Solicitante)
     *
     * @author rodrigo.hernandez
     */
    private void enviarInformacionAPanelInfoSolicitante() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoSolicitante");

        this.diligenciamientoInfoSolicitanteMB.cargarVariablesExternas(
            this.banderaModoLecturaHabilitado, this.solicitud,
            this.listaSolicitantes);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoSolicitante");
    }

    // -----------------------------------------------------------------------------------------------------------
    /**
     * Método para enviar datos requeridos por MB de CU-SA-AC-136 (Informacion Tipo Trámite)
     *
     * @author rodrigo.hernandez
     */
    public void enviarInformacionAPanelInfoTramite() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoTramite");

        // Se activa el panel de Informacion de tipo de tramite
        this.diligenciamientoInfoTipoTramiteMB.cargarVariablesExternas(
            this.solicitud, this.solicitante,
            this.banderaModoLecturaHabilitado);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoTramite");
    }

    // -----------------------------------------------------------------------------------------------------------
    /**
     * Método para enviar datos requeridos por MB de CU-SA-AC-46 (Informacion Predios)
     *
     * @author rodrigo.hernandez
     */
    public void enviarInformacionAPanelInfoPredios() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoPredios");

        this.administracionPrediosAvaluosMB.cargarVariablesExternas(
            this.solicitud, this.solicitante, this.listaPredios,
            this.banderaModoLecturaHabilitado, this.usuario);

        this.carguePrediosAvaluosMB.cargarVariablesExternas(this.solicitud,
            this.solicitante, this.usuario, this.listaPredios);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoPredios");
    }

    // -----------------------------------------------------------------------------------------------------------
    /**
     * Método para enviar datos requeridos por MB de CU-SA-AC-109 (Asociaciar y Desasociar contactos
     * de predios)
     *
     * @author rodrigo.hernandez
     */
    public void enviarInformacionAPanelInfoContactosPredios() {

        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoContactosPredios");

        this.administracionInfoContactoDePrediosMB.cargarVariablesExternas(
            this.banderaModoLecturaHabilitado, this.solicitante,
            this.solicitud, this.listaContactosPredio, this.usuario);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoContactosPredios");
    }

    // -----------------------------------------------------------------------------------------------------------
    /**
     * Método para enviar datos requeridos por MB de CU-SA-AC-002 (Informacion Documentos)
     *
     * @author rodrigo.hernandez
     */
    public void enviarInformacionAPanelInfoDocumentos() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoDocumentos");

        this.administracionDocumentacionTramiteAvaluoComercialMB
            .cargarVariablesExternas(this.solicitud,
                this.banderaModoLecturaHabilitado, this.solicitante,
                this.usuario, this.listaDocumentosExistentes);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoDocumentos");
    }

    // -----------------------------------------------------------------------------------------------------------
    /**
     * Método para enviar datos requeridos por MB de CU-SA-AC-99 (Informacion de Forma de Pago)
     *
     * @author rodrigo.hernandez
     */
    public void enviarInformacionAPanelInfoFormaPago() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoFormaPago");

        // TODO :: rodrigo.hernandez :: 03-09-2012 :: desarrollar funcionalidad
        // para enviar datos a Managed Bean de CU-SA-AC-099
        LOGGER.debug("Fin DiligenciamientoSolicitudMB#enviarInformacionAPanelInfoFormaPago");
    }

    // -----------------------------------------------------------------------------------------------------------
    /**
     *
     * Método para almacenar temporalmente la solicitud de Avaluo Comercial
     *
     * @author rodrigo.hernandez
     */
    private void guardarSolicitud() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#guardarSolicitud");

        this.solicitud = this.getAvaluosService()
            .guardarActualizarSolicitudAvaluoComercial(this.solicitud);

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#guardarSolicitud");
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Método para validar los datos de los paneles.</br> Llamado desde la página
     * <b>diligenciarSolicitud.xhtml</b> por el boton</br> <b>Guardar solicitud</b>
     *
     * @author rodrigo.hernandez
     */
    public void validarDatosPaneles() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#validarDatosPaneles");

        boolean banderaSonValidosDatosSolicitante;
        boolean banderaSonValidosDatosTipoTramite;
        boolean banderaSonValidosDatosPredios;

        // Se realiza la validacion de datos sobre el panel de información de
        // solicitante
        banderaSonValidosDatosSolicitante = this
            .validarDatosPanelInfoSolicitante();

        if (banderaSonValidosDatosSolicitante) {

            // Se realiza la validacion de datos sobre el panel de información
            // de tipo de tramite
            banderaSonValidosDatosTipoTramite = this
                .validarDatosPanelInfoTipoTramite();

            if (banderaSonValidosDatosTipoTramite) {

                banderaSonValidosDatosPredios = this
                    .validarDatosPanelInfoPredios();

                if (banderaSonValidosDatosPredios) {

                }

            }
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarDatosPaneles");
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Método para validar los datos del panel de Información del Solicitante
     *
     * @author rodrigo.hernandez
     */
    private boolean validarDatosPanelInfoSolicitante() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#validarDatosPanelInfoSolicitante");

        boolean result = true;
        String mensaje = "";

        /*
         * Se ejecutan las validaciones de datos en el MB para Diligenciar Info del Solicitante
         */
        if (this.diligenciamientoInfoSolicitanteMB.validarInfo()) {

            /*
             * Activa los paneles hasta panel de informacion de tipo de tramite
             */
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE,
                true);

            this.cargarDatosSolicitanteSolicitudsPostGuardarSolicitud();

            this.enviarInformacionAPanelInfoTramite();

            result = true;
            mensaje = "La información del solicitante fue guardada correctamente";
            this.addMensajeInfo(mensaje);

        } else {
            /* Se resetea la visibilidad de los paneles posteriores */
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE,
                false);

            result = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarDatosPanelInfoSolicitante");

        return result;
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Método para validar los datos del panel de Información del Tipo de Trámite
     *
     * @author rodrigo.hernandez
     */
    private boolean validarDatosPanelInfoTipoTramite() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#validarDatosPanelInfoTipoTramite");

        boolean result = true;
        String mensaje = "";

        /* Se valida que haya sido guardada la informacion del solicitante */
        if (this.banderaInformacionSolicitanteAlmacenada) {

            /* Se llama método para validar datos de panel Info Tramite */
            if (this.diligenciamientoInfoTipoTramiteMB.validarInfo()) {

                /* Se guarda la informcion del tipo de tramite */
                this.diligenciamientoInfoTipoTramiteMB.guardarInfoTramite();

                /* actualiza la solicitud con los datos de tipo tramite */
                this.guardarSolicitud();

                /* Se recarga la informacion del solicitante */
                this.cargarInformacionSolicitante();

                /* Se envia la informacion al panel de predios para iniciar datos del MB asociado */
                this.enviarInformacionAPanelInfoPredios();

                /* Activa los paneles hasta panel de informacion de predios */
                this.modificarBanderasInformacionAlmacenadaSegunPanel(
                    ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_TIPO_TRAMITE,
                    true);

                mensaje = "La información del tipo de trámite fue guardada correctamente";
                this.addMensajeInfo(mensaje);

            } else {
                /*
                 * No se agrega mensaje de error porque esos se agregan en el metodo
                 * DiligenciamientoInfoTipoTramiteMB.validarInfo()
                 */
                result = false;
                this.modificarBanderasInformacionAlmacenadaSegunPanel(
                    ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_TIPO_TRAMITE,
                    false);

            }

        } else {
            /*
             * Si no hay información de solicitante se ocultan los paneles diferentes al panel de
             * Info. Solicitante
             */
            result = false;
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE,
                false);

        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarDatosPanelInfoTipoTramite");

        return result;
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Método para validar los datos del panel de Información de predios
     *
     * @author rodrigo.hernandez
     */
    private boolean validarDatosPanelInfoPredios() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#validarDatosPanelInfoPredios");

        boolean result = true;
        String mensaje = "";

        // Se valida que haya sido guardada la informacion del tipo de
        // tramite
        if (this.banderaInformacionTipoTramiteAlmacenada) {

            /*
             * Como la información de los predios se modifica desde el MB del caso de uso
             * CU-SA-AC-46 Administracion Predios de Avaluos se valida si existen en BD predios
             * registrados.
             */
            this.buscarPredios();

            // Se valida que exista informacion de predios
            if (this.banderaExistePredios) {
                // Se llama método para validar datos de panel Info de Predios
                if (this.administracionPrediosAvaluosMB.validarInfo()) {
                    // Activa los paneles hasta panel de informacion de
                    // documentos
                    this.modificarBanderasInformacionAlmacenadaSegunPanel(
                        ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_PREDIOS,
                        true);

                    this.enviarInformacionAPanelInfoDocumentos();

                    mensaje = "La información de los predios fue guardada correctamente";
                    this.addMensajeInfo(mensaje);

                } else {
                    // Se resetea la visibilidad de los paneles posteriores
                    this.modificarBanderasInformacionAlmacenadaSegunPanel(
                        ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_PREDIOS,
                        false);
                }
            } else {

                this.enviarInformacionAPanelInfoPredios();

                mensaje = "No hay información de predios asociada a la solicitud";
                this.addMensajeError(mensaje);
                result = false;

                // Se resetea la visibilidad de los paneles posteriores
                this.modificarBanderasInformacionAlmacenadaSegunPanel(
                    ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_PREDIOS,
                    false);
            }
        } else {
            mensaje = "No hay información de trámite asociada a la solicitud";
            this.addMensajeError(mensaje);
            result = false;

            // Se resetea la visibilidad de los paneles posteriores
            this.modificarBanderasInformacionAlmacenadaSegunPanel(
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_TIPO_TRAMITE,
                false);
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarDatosPanelInfoPredios");

        return result;
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    /**
     * Método para seleccionar el solicitante asociado a la solicitud
     *
     * @author rodrigo.hernandez
     */
    private void setSolicitanteSolicutudSeleccionado() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#setSolicitanteSolicutudSeleccionado");

        if (this.solicitante == null) {
            this.solicitante = new SolicitanteSolicitud();
        }

        // Busca entre la lsita de solicitantesSolicitud de la solicitud al
        // solicitanteSolicitud con Relacion PROPIETARIO
        for (SolicitanteSolicitud solSol : this.solicitud
            .getSolicitanteSolicituds()) {
            if (solSol.getRelacion().equals(
                ESolicitanteSolicitudRelac.PROPIETARIO.toString())) {
                this.solicitante = solSol;
                break;
            }
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#setSolicitanteSolicutudSeleccionado");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para cargar los datos de solicitanteSolicituds relacionados con la solicitud después
     * de haber cargado la solicitud
     *
     * @author rodrigo.hernandez
     */
    private void cargarDatosSolicitanteSolicitudsPostGuardarSolicitud() {
        List<SolicitanteSolicitud> aux = null;

        /* Se guardan/actualizan los solicitantesSolicitud de la solicitud */
        aux = this.getAvaluosService().guardarActualizarSolicitanteSolicituds(
            this.solicitud.getSolicitanteSolicituds(),
            this.solicitud.getId());

        /*
         * Se actualiza la solicitud con la informacion de los solicitanteSolicituds
         */
        this.solicitud.setSolicitanteSolicituds(aux);

        /* Se define el solicitante */
        this.setSolicitanteSolicutudSeleccionado();
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para almacenar definitivamente la solicitud de Avaluo Comercial.</br> Llamado desde
     * página <b>diligenciarSolicitud.xhtml
     * </b></br> por botón <b>Finalizar</b>
     *
     * @author rodrigo.hernandez
     */
    public void finalizarSolicitud() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#finalizarSolicitud");

        this.validarBanderasInformacionAlmacenada();

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#finalizarSolicitud");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para validar si ya toda la informacion de la solicitud fue ingresada para almacenarla
     * definitivamente
     *
     * @author rodrigo.hernandez
     */
    private void validarBanderasInformacionAlmacenada() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#validarBanderasInformacion");

        if (this.banderaInformacionSolicitanteAlmacenada &&
             this.banderaInformacionTipoTramiteAlmacenada &&
             this.banderaInformacionPrediosAlmacenada &&
             this.banderaInformacionDocumentosAlmacenada &&
             this.banderaInformacionFormaPagoAlmacenada) {
            this.banderaFinDiligenciamientoHabilitado = true;
        } else {
            this.banderaFinDiligenciamientoHabilitado = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#validarBanderasInformacion");
    }

    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar</br> Llamado
     * desde página <b>diligenciarSolicitud.xhtml </b> por botón <b>Cerrar</b>
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {

        LOGGER.debug("iniciando DiligenciamientoSolicitudMB#cerrar");

        UtilidadesWeb.removerManagedBean("adminInfoSolicitante");
        UtilidadesWeb.removerManagedBean("diligenciamientoInfoSolicitante");
        UtilidadesWeb.removerManagedBean("diligenciamientoInfoTipoTramite");
        UtilidadesWeb.removerManagedBean("adminPrediosAvaluos");
        UtilidadesWeb.removerManagedBean("adminInfoContactoPredios");
        UtilidadesWeb.removerManagedBean("diligenciamientoSolicitudAvaluo");
        UtilidadesWeb.removerManagedBean("carguePrediosAvaluos");
        UtilidadesWeb.removerManagedBean("general");
        // TODO:rodrigo.hernandez::03-09-2012::agregar cuando el MB este listo,
        // la linea para cerrar el MB de Forma de pago

        LOGGER.debug("finalizando DiligenciamientoSolicitudMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método que indica si los datos se visualizaran en modo lectura o modo edicion según tipo de
     * almacenamiento de informacion de la solicitud
     *
     * @author rodrigo.hernandez
     *
     * @return <b>True</b> si es modo lectura</br> <b>False</b> si es modo edicion</br>
     *
     *
     */
    private boolean setModoDatos(int tipoAlmacenamientoInfo) {
        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#setModoDatos");

        boolean result = true;

        // Si el tipo de almacenamiento de la informacion es definitivo se
        // activa el modo lectura
        if (tipoAlmacenamientoInfo != Constantes.AC_TIPO_INFORMACION_SOLICITUD_DEFINITIVA) {
            result = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudMB#setModoDatos");

        return result;
    }

    /**
     * Método para habilitar/deshabilitar en cadena la visibilidad de los paneles
     *
     * @author rodrigo.hernandez
     *
     * @param codigoPanel - Id del Panel
     * @param esHabilitacionPaneles - Indica si se van a habilitar o deshabilitar la visibilidad de
     * los paneles
     */
    private void modificarBanderasInformacionAlmacenadaSegunPanel(
        int codigoPanel, boolean esHabilitacionPaneles) {

        LOGGER.debug(
            "iniciando DiligenciamientoSolicitudMB#modificarBanderasInformacionAlmacenadaSegunPanel");

        // Se valida si la modificacion se realiza para habilitar los paneles
        if (esHabilitacionPaneles) {
            for (int cont = codigoPanel; cont >=
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE;
                cont--) {
                this.listaBanderasInfoAlmacenada.set(cont, true);
            }

            // Se valida si la modificacion se realiza para deshabilitar los
            // paneles
        } else {
            for (int cont = codigoPanel; cont <=
                ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_FORMA_PAGO;
                cont++) {
                this.listaBanderasInfoAlmacenada.set(cont, false);
            }
        }

        this.banderaInformacionSolicitanteAlmacenada = this.listaBanderasInfoAlmacenada
            .get(ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_SOLICITANTE);

        this.banderaInformacionTipoTramiteAlmacenada = this.listaBanderasInfoAlmacenada
            .get(ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_TIPO_TRAMITE);

        this.banderaInformacionPrediosAlmacenada = this.listaBanderasInfoAlmacenada
            .get(ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_PREDIOS);

        this.banderaInformacionDocumentosAlmacenada = this.listaBanderasInfoAlmacenada
            .get(ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_DOCUMENTOS);

        this.banderaInformacionFormaPagoAlmacenada = this.listaBanderasInfoAlmacenada
            .get(ConstantesDiligenciamientoSolicitudAvaluoComercial.CODIGO_PANEL_INFO_FORMA_PAGO);

        LOGGER.debug(
            "finalizando DiligenciamientoSolicitudMB#modificarBanderasInformacionAlmacenadaSegunPanel");

    }

    /**
     * Método para iniciar las banderas que determinan si existe o no información de los diferentes
     * paneles
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderasExisteInformacion() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudMB#iniciarBanderasExisteInformacion");

        this.banderaExisteSolicitante = false;
        this.banderaExisteTipoTramite = false;
        this.banderaExisteContactosPredios = false;
        this.banderaExistePredios = false;
        this.banderaExisteDocumentos = false;
        this.banderaExisteFormaPago = false;

        LOGGER.debug("Finalizando DiligenciamientoSolicitudMB#iniciarBanderasExisteInformacion");

    }

}
