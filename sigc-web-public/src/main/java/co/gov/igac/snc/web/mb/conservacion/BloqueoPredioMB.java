package co.gov.igac.snc.web.mb.conservacion;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.TmpBloqueoMasivo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EArchivoAnexoFormato;
import co.gov.igac.snc.persistence.util.EDocumentoArchivoAnexoEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEntidadEstado;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoBloqueo;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteEstadoBloqueo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPrediosBloqueo;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace de MB para manejar la consulta y bloqueo de predios
 *
 * @author juan.agudelo
 *
 */
@Component("bloqueoPredio")
@Scope("session")
public class BloqueoPredioMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(BloqueoPredioMB.class);

    private static final String SEPARATOR_PARAMETER = ";;";

    /*
     * interfaces de servicio
     */
    @Autowired
    private GeneralMB generalService;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;

    /**
     * Orden apra la lista de Municipios
     */
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;

    /**
     * Codigo del departamento
     */
    private String departamentoCodigo;

    /**
     * Codigo del municipio
     */
    private String municipioCodigo;

    /**
     * datos de la busqueda
     */
    private Departamento departamento;
    private Municipio municipio;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;

    private ArrayList<SelectItem> deptosEntidadItemList;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosBuscadorItemList;

    /**
     * Listas de items para el combo de tipo de bloqueo
     */
    private List<SelectItem> tiposBloqueoItemList;

    /**
     * valor seleccionado en el combo tipo de bloqueo
     */
    private String selectedTipoBloqueo;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamento;
    private String selectedMunicipio;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamentoCod;
    private String selectedMunicipioCod;

    /**
     * Determina si la opción de restitución de tierras fue seleccionada
     */
    private boolean selectedRestitucionTierras;

    /**
     * Codigo de sector en predio
     */
    private String sectorCodigo;

    /**
     * Documento archivo anexo soporte del bloqueo masivo
     */
    private DocumentoArchivoAnexo documentoArchivoAnexo;

    /**
     * Tipo de avaluo en predio
     */
    private String tipoAvaluo;

    /**
     * Predio temporal para ser removido de la lista de predios a desbloquear
     */
    private Predio selectedPredioTemp;

    /**
     * variable donde se cargan los resultados paginados de la consulta de predios bloqueados
     */
    private LazyDataModel<PredioBloqueo> lazyAssignedBloqueos;

    /**
     * lista de PredioBloqueo paginados
     */
    private List<PredioBloqueo> rows;

    /**
     * filtro de datos para la consulta de predios bloqueados
     */
    private FiltroDatosConsultaPrediosBloqueo filtroCPredioB;

    /**
     * filtro de datos para la consulta de predios bloqueados por rango
     */
    private FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB;

    /**
     * Bandera de visualización de la tabla de resultados
     */
    private boolean banderaBuscar;

    /**
     * Almacena el valor del radio button para el tipo de bloqueo a realizar
     */
    private int tipoBloqueo;

    /**
     * Visualización bloqueo individual
     */
    private boolean tipoBloqueo0;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteBloqueo;

    /**
     * Mensaje del boton que permite cargar archivos
     */
    private String mensajeCargaArchivo;

    /**
     * Visualización bloqueo por rangos
     */
    private boolean tipoBloqueo1;

    /**
     * Visualización bloqueo masivo
     */
    private boolean tipoBloqueo2;

    /**
     * Mensaje de salida del bloqueo de persona
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * indican si los combos están ordenados por nombre (si no, lo están por código)
     */
    private boolean municipiosOrderedByNombre;
    private boolean departamentosOrderedByNombre;

    /**
     * Condición de propiedad para el predio a bloquear
     */
    private String condicionPropiedad;

    /**
     * Código del predio a bloquear
     */
    private String predioB;

    /**
     * Manzana o vereda del predio a bloquear
     */
    private String manzanaVereda;

    /**
     * Código del sector del prédio a bloquear
     */
    private String sectorCodigoB;

    /**
     * Fecha inicial del bloqueo de predios por rango
     */
    private Date fechaInicial;

    /**
     * Fecha final del bloqueo de predios por rango
     */
    private Date fechaFinal;

    /**
     * Fecha de envio del bloqueo
     */
    private Date fechaEnvio;

    /**
     * Numero de radicado del bloqueo
     */
    private String numeroRadicado;

    /**
     * Fecha de recibido el bloqueo
     */
    private Date fechaRecibido;

    /**
     * Motivo del bloqueo de el/los predios
     */
    private String motivoBloqueo;

    /**
     * Almacena el tipo de archivo valido dependiendo de la opcion de carga
     */
    private String tiposDeArchivoValidos;

    /**
     * Visualizacion del boton de carga de archivos
     */
    private boolean tipoBloqueoCargar;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Lista que contiene los predios bloqueados o el rango de predios bloqueados
     */
    private List<PredioBloqueo> prediosBloqueados;

    /**
     * PredioBloqueo que delimitan el rango de inserción
     */
    private PredioBloqueo predioBloqueo;

    /**
     * Predio final del rango de bloqueo
     */
    private Predio predioFinal;

    /**
     * Generador aleatorio para nombres de archivos
     */
    private Random generadorNumeroAleatorioNombreArchivo;

    /**
     * Ruta del archivo cargado
     */
    private String rutaMostrar;

    /**
     * Archivo resultado cargado
     */
    private File archivoResultado;
    private File archivoMasivoResultado;

    /**
     * Banderas de visualización de los botones guardar y generar informe
     */
    private boolean banderaBotonGuardar;
    private boolean banderaBotonGenerarInforme;
    private boolean banderaValidar;
    private boolean laBandera;

    /**
     * Descripción del documento de soporte cargado
     */
    private String descripcionArchivo;

    /**
     * Control de carga de archivos de soporte
     */
    private int aSoporte;
    private int aBMasa;
    private long aSoporteSize;

    /**
     * Lista de predios bloqueo cargados desde un archivo de bloqueo masivo
     */
    private ArrayList<PredioBloqueo> bloqueoMasivo;
    private long aBloqueoSize;

    /**
     * Lista que contiene los predios seleccionados para el bloqueo
     */
    private List<Predio> prediosSeleccionados;

    /**
     * Objeto que contiene los datos del reporte de bloqueo másivo de predios
     */
    private ReporteDTO reporteBloqueoMasivoPredios;

    /**
     * Archivo del reporte de diligencia de notificación personal
     */
    private File file;

    /**
     * Nombre del archivo de soporte del bloqueo masivo
     */
    private String nombreDocumentoBloqueoMasivo;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Documento temporal a visualizar
     */
    private Documento documentoTemporalAVisualizar;

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    private StreamedContent fileTempDownload;

    /**
     * Documento temporal para la previsualización del archivo de bloqueo masivo
     */
    private Documento documentoTemporalBloqueoMasivo;

    /**
     * Variable para asociarel id de la {
     *
     * @Entidad} que ordena el bloqueo del {@link Predio}
     */
    private Long idEntidadBloqueo;

    /**
     * Lista de tipo {@link Entidad} con las entidades que se encuentran activas
     */
    private List<SelectItem> entidades;

    /**
     * Archivo tempora lque contiene la lista de errores encontrados en la validación de los predios
     * a bloquear.
     */
    private DefaultStreamedContent exportFile;

    /**
     * Varible que indica que se encontraron errores en en la validación de los predios a bloquear.
     */
    private Boolean erroresPredios = false;

    /**
     * Varible que indica que muestra el boton de bloquear predios
     */
    private Boolean bloquearPredios = true;

    /**
     * Varible que contiene los tramites a bloquear por predio
     */
    private List<Tramite> tramites = null;

    private Workbook wb = null;

    // ------------- methods ------------------------------------
    public EOrden getOrdenDepartamentos() {
        return this.ordenDepartamentos;
    }

    public boolean isLaBandera() {
        return this.laBandera;
    }

    public void setLaBandera(boolean laBandera) {
        this.laBandera = laBandera;
    }

    public String getNombreDocumentoBloqueoMasivo() {
        return this.nombreDocumentoBloqueoMasivo;
    }

    public void setNombreDocumentoBloqueoMasivo(
        String nombreDocumentoBloqueoMasivo) {
        this.nombreDocumentoBloqueoMasivo = nombreDocumentoBloqueoMasivo;
    }

    public boolean isBanderaValidar() {
        return this.banderaValidar;
    }

    public Documento getDocumentoTemporalAVisualizar() {
        return documentoTemporalAVisualizar;
    }

    public void setDocumentoTemporalAVisualizar(
        Documento documentoTemporalAVisualizar) {
        this.documentoTemporalAVisualizar = documentoTemporalAVisualizar;
    }

    public void setBanderaValidar(boolean banderaValidar) {
        this.banderaValidar = banderaValidar;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public StreamedContent getFileTempDownload() {
        controladorDescargaArchivos();
        return fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return mensajeSalidaConfirmationDialog;
    }

    public List<Predio> getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(List<Predio> prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public Long getIdEntidadBloqueo() {
        return idEntidadBloqueo;
    }

    public void setIdEntidadBloqueo(Long idEntidadBloqueo) {
        this.idEntidadBloqueo = idEntidadBloqueo;
    }

    public List<SelectItem> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<SelectItem> entidades) {
        this.entidades = entidades;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public FiltroDatosConsultaPrediosBloqueo getFiltroFinalCPredioB() {
        return filtroFinalCPredioB;
    }

    public void setFiltroFinalCPredioB(
        FiltroDatosConsultaPrediosBloqueo filtroFinalCPredioB) {
        this.filtroFinalCPredioB = filtroFinalCPredioB;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }

    public Documento getDocumentoTemporalBloqueoMasivo() {
        return documentoTemporalBloqueoMasivo;
    }

    public void setDocumentoTemporalBloqueoMasivo(
        Documento documentoTemporalBloqueoMasivo) {
        this.documentoTemporalBloqueoMasivo = documentoTemporalBloqueoMasivo;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(
        boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public boolean isBanderaBotonGenerarInforme() {
        return banderaBotonGenerarInforme;
    }

    public void setBanderaBotonGenerarInforme(boolean banderaBotonGenerarInforme) {
        this.banderaBotonGenerarInforme = banderaBotonGenerarInforme;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public Random getGeneradorNumeroAleatorioNombreArchivo() {
        return generadorNumeroAleatorioNombreArchivo;
    }

    public void setGeneradorNumeroAleatorioNombreArchivo(
        Random generadorNumeroAleatorioNombreArchivo) {
        this.generadorNumeroAleatorioNombreArchivo = generadorNumeroAleatorioNombreArchivo;
    }

    public Predio getPredioFinal() {
        return predioFinal;
    }

    public void setPredioFinal(Predio predioFinal) {
        this.predioFinal = predioFinal;
    }

    public PredioBloqueo getPredioBloqueo() {
        return predioBloqueo;
    }

    public void setPredioBloqueo(PredioBloqueo predioBloqueo) {
        this.predioBloqueo = predioBloqueo;
    }

    public List<PredioBloqueo> getPrediosBloqueados() {
        return prediosBloqueados;
    }

    public void setPrediosBloqueados(List<PredioBloqueo> prediosBloqueados) {
        this.prediosBloqueados = prediosBloqueados;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isTipoBloqueoCargar() {
        return tipoBloqueoCargar;
    }

    public void setTipoBloqueoCargar(boolean tipoBloqueoCargar) {
        this.tipoBloqueoCargar = tipoBloqueoCargar;
    }

    public String getTiposDeArchivoValidos() {
        return tiposDeArchivoValidos;
    }

    public void setTiposDeArchivoValidos(String tiposDeArchivoValidos) {
        this.tiposDeArchivoValidos = tiposDeArchivoValidos;
    }

    public Predio getSelectedPredioTemp() {
        return selectedPredioTemp;
    }

    public void setSelectedPredioTemp(Predio selectedPredioTemp) {
        this.selectedPredioTemp = selectedPredioTemp;
    }

    public String getMensajeCargaArchivo() {
        return this.mensajeCargaArchivo;
    }

    public void setMensajeCargaArchivo(String mensajeCargaArchivo) {
        this.mensajeCargaArchivo = mensajeCargaArchivo;
    }

    public String getMotivoBloqueo() {
        return motivoBloqueo;
    }

    public void setMotivoBloqueo(String motivoBloqueo) {
        this.motivoBloqueo = motivoBloqueo;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Date getFechaRecibido() {
        return fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    public boolean isTipoBloqueo0() {
        return this.tipoBloqueo0;
    }

    public void setTipoBloqueo0(boolean tipoBloqueo0) {
        this.tipoBloqueo0 = tipoBloqueo0;
    }

    public boolean isTipoBloqueo1() {
        return this.tipoBloqueo1;
    }

    public void setTipoBloqueo1(boolean tipoBloqueo1) {
        this.tipoBloqueo1 = tipoBloqueo1;
    }

    public boolean isTipoBloqueo2() {
        return tipoBloqueo2;
    }

    public void setTipoBloqueo2(boolean tipoBloqueo2) {
        this.tipoBloqueo2 = tipoBloqueo2;
    }

    public String getSectorCodigoB() {
        return sectorCodigoB;
    }

    public void setSectorCodigoB(String sectorCodigoB) {
        this.sectorCodigoB = sectorCodigoB;
    }

    public String getCondicionPropiedad() {
        return condicionPropiedad;
    }

    public void setCondicionPropiedad(String condicionPropiedad) {
        this.condicionPropiedad = condicionPropiedad;
    }

    public String getPredioB() {
        return predioB;
    }

    public void setPredioB(String predioB) {
        this.predioB = predioB;
    }

    public String getManzanaVereda() {
        return manzanaVereda;
    }

    public void setManzanaVereda(String manzanaVereda) {
        this.manzanaVereda = manzanaVereda;
    }

    public int getTipoBloqueo() {
        return tipoBloqueo;
    }

    public void setTipoBloqueo(int tipoBloqueo) {
        this.tipoBloqueo = tipoBloqueo;
    }

    public File getArchivoResultado() {
        return archivoResultado;
    }

    public void setArchivoResultado(File archivoResultado) {
        this.archivoResultado = archivoResultado;
    }

    public File getArchivoMasivoResultado() {
        return this.archivoMasivoResultado;
    }

    public void setArchivoMasivoResultado(File archivoMasivoResultado) {
        this.archivoMasivoResultado = archivoMasivoResultado;
    }

    public FiltroDatosConsultaPrediosBloqueo getFiltroCPredioB() {
        return this.filtroCPredioB;
    }

    public void setFiltroCPredioB(
        FiltroDatosConsultaPrediosBloqueo filtroCPredioB) {
        this.filtroCPredioB = filtroCPredioB;
    }

    public LazyDataModel<PredioBloqueo> getLazyAssignedBloqueos() {
        return this.lazyAssignedBloqueos;
    }

    public void setLazyAssignedBloqueos(
        LazyDataModel<PredioBloqueo> lazyAssignedBloqueos) {
        this.lazyAssignedBloqueos = lazyAssignedBloqueos;
    }

    public List<PredioBloqueo> getRows() {
        return this.rows;
    }

    public void setRows(List<PredioBloqueo> rows) {
        this.rows = rows;
    }

    public EOrden getOrdenDepartamentosDocumento() {
        return this.ordenDepartamentosDocumento;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public String getSectorCodigo() {
        return sectorCodigo;
    }

    public void setSectorCodigo(String sectorCodigo) {
        this.sectorCodigo = sectorCodigo;
    }

    public String getTipoAvaluo() {
        return tipoAvaluo;
    }

    public void setTipoAvaluo(String tipoAvaluo) {
        this.tipoAvaluo = tipoAvaluo;
    }

    public void setOrdenDepartamentosDocumento(
        EOrden ordenDepartamentosDocumento) {
        this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
    }

    public boolean isBanderaBuscar() {
        return banderaBuscar;
    }

    public void setBanderaBuscar(boolean banderaBuscar) {
        this.banderaBuscar = banderaBuscar;
    }

    public EOrden getOrdenMunicipios() {
        return ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public EOrden getOrdenMunicipiosDocumento() {
        return ordenMunicipiosDocumento;
    }

    public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
        this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public Documento getDocumentoSoporteBloqueo() {
        return documentoSoporteBloqueo;
    }

    public void setDocumentoSoporteBloqueo(Documento documentoSoporteBloqueo) {
        this.documentoSoporteBloqueo = documentoSoporteBloqueo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public ReporteDTO getReporteBloqueoMasivoPredios() {
        return reporteBloqueoMasivoPredios;
    }

    public void setReporteBloqueoMasivoPredios(
        ReporteDTO reporteBloqueoMasivoPredios) {
        this.reporteBloqueoMasivoPredios = reporteBloqueoMasivoPredios;
    }

    public String getSelectedTipoBloqueo() {
        return selectedTipoBloqueo;
    }

    public void setSelectedTipoBloqueo(String selectedTipoBloqueo) {
        this.selectedTipoBloqueo = selectedTipoBloqueo;
    }

    public List<SelectItem> getMunicipiosBuscadorItemList() {
        return municipiosBuscadorItemList;
    }

    public void setMunicipiosBuscadorItemList(List<SelectItem> municipiosBuscadorItemList) {
        this.municipiosBuscadorItemList = municipiosBuscadorItemList;
    }

    public Boolean getErroresPredios() {
        return erroresPredios;
    }

    public void setErroresPredios(Boolean erroresPredios) {
        this.erroresPredios = erroresPredios;
    }

    public Boolean getBloquearPredios() {
        return bloquearPredios;
    }

    public void setBloquearPredios(Boolean bloquearPredios) {
        this.bloquearPredios = bloquearPredios;
    }

    // ----------------------------------------------------------------------
    public List<SelectItem> getTiposBloqueoItemList() {
        return tiposBloqueoItemList;
    }

    public void setTiposBloqueoItemList(List<SelectItem> tiposBloqueoItemList) {
        this.tiposBloqueoItemList = tiposBloqueoItemList;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(
        ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    // ----------------------------------------------------------------------
    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    // --------------------------------------------------------------------------------
    public ArrayList<SelectItem> getDeptosEntidadItemList() {
        return this.deptosEntidadItemList;
    }

    public void setDeptosEntidadItemList(final ArrayList<SelectItem> deptosEntidadItemList) {
        this.deptosEntidadItemList = deptosEntidadItemList;
    }

    public String getSelectedDepartamentoCod() {
        return this.selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String codigo) {

        this.selectedDepartamentoCod = codigo;
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento depto : this.departamentos) {
                if (codigo.equals(depto.getCodigo())) {
                    dptoSeleccionado = depto;
                    break;
                }
            }
        }
        this.departamento = dptoSeleccionado;
        this.selectedMunicipioCod = null;
    }

    public String getSelectedDepartamento() {
        return selectedDepartamento;
    }

    public void setSelectedDepartamento(String selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public String getSelectedMunicipio() {
        return selectedMunicipio;
    }

    public void setSelectedMunicipio(String selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public boolean isSelectedRestitucionTierras() {
        return selectedRestitucionTierras;
    }

    public void setSelectedRestitucionTierras(boolean selectedRestitucionTierras) {
        this.selectedRestitucionTierras = selectedRestitucionTierras;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public DefaultStreamedContent getExportFile() {

        try {
            String excelFileName = "reporte.csv";
            FileOutputStream fos = new FileOutputStream(excelFileName);
            wb.write(fos);
            fos.flush();
            fos.close();
            InputStream stream = new BufferedInputStream(new FileInputStream(excelFileName));
            exportFile = new DefaultStreamedContent(stream, "application/xls", excelFileName);
        } catch (IOException eq) {
        }
        return exportFile;
    }

    public void setExportFile(DefaultStreamedContent exportFile) {
        this.exportFile = exportFile;
    }

    // ---------------------------------------------------------------------
    public String getSelectedMunicipioCod() {
        return this.selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String codigo) {
        this.selectedMunicipioCod = codigo;

        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.municipio = municipioSelected;
    }

    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("BloqueoPredioMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.tipoBloqueoCargar = true;
        this.banderaBuscar = false;
        this.tipoBloqueo = 0;
        this.tipoBloqueo0 = true;
        this.tipoBloqueo1 = false;
        this.tipoBloqueo2 = false;
        this.banderaBotonGenerarInforme = false;
        this.banderaBotonGuardar = false;
        this.banderaValidar = false;
        this.rutaMostrar = null;
        this.mensajeCargaArchivo = "Cargar documento";
        this.prediosBloqueados = new ArrayList<PredioBloqueo>();
        this.predioBloqueo = new PredioBloqueo();
        this.predioFinal = new Predio();
        this.aSoporte = 0;
        this.aSoporteSize = 0;
        this.aBloqueoSize = 0;
        this.aBMasa = 0;
        this.laBandera = false;
        this.fechaRecibido = generalService.getFechaActual();

        this.ordenDepartamentos = EOrden.NOMBRE;
        this.departamentosOrderedByNombre = true;
        this.ordenMunicipios = EOrden.NOMBRE;
        this.municipiosOrderedByNombre = true;
        this.selectedDepartamentoCod = null;
        this.selectedMunicipioCod = null;
        this.erroresPredios = false;
        this.bloquearPredios = true;

        // Inicialización de departamento
        for (String sTemp : this.usuario.getRoles()) {
            if (sTemp.equals(ERol.SUBDIRECTOR_CATASTRO.toString())) {
                this.departamentos = (ArrayList<Departamento>) this
                    .getGeneralesService()
                    .buscarDepartamentosConCatastroCentralizadoPorCodigoPais(
                        Constantes.COLOMBIA);
                break;
            } else {

                this.departamentos = (ArrayList<Departamento>) this
                    .getGeneralesService()
                    .getDepartamentoByCodigoEstructuraOrganizacional(
                        this.usuario
                            .getCodigoEstructuraOrganizacional());

                break;
            }
        }

        updateMunicipiosItemList();

        this.filtroCPredioB = new FiltroDatosConsultaPrediosBloqueo();
        this.filtroFinalCPredioB = new FiltroDatosConsultaPrediosBloqueo();

        this.mensajeSalidaConfirmationDialog = "La marcación" +
             " se realizó exitosamente." + " Desea continuar" +
             " en el módulo de marcación de predios?";

        this.tiposBloqueoItemList = new ArrayList<SelectItem>();

        List<Dominio> dominio = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.PREDIO_TIPO_BLOQUEO);

        for (Dominio dom : dominio) {
            this.tiposBloqueoItemList.add(new SelectItem(dom.getCodigo(), dom.getValor()));
        }

        this.tiposBloqueoItemList.add(new SelectItem(null, DEFAULT_COMBOS));

        this.cargarDepartamentos();

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        this.cargarDepartamentosEntidad();

    }
//--------------------------------------------------------------------------------------------------

    public void poblar() {

        int count;
        count = this.getConservacionService().contarPrediosBloqueo(
            this.filtroCPredioB);

        if (count > 0) {
            this.lazyAssignedBloqueos.setRowCount(count);
        } else {
            this.lazyAssignedBloqueos.setRowCount(0);
        }
    }
//--------------------------------------------------------------------------------------------------

    private void populatePredioBloqueoLazyly(List<PredioBloqueo> answer,
        int first, int pageSize) {
        Entidad entidad = null;

        rows = new ArrayList<PredioBloqueo>();

        int size;
        rows = this.getConservacionService().buscarPrediosBloqueo(
            filtroCPredioB, first, pageSize);

        if (rows != null) {
            size = rows.size();
            for (int i = 0; i < size; i++) {
                if (rows.get(i).getEntidadId() != null) {
                    entidad = this.getConservacionService().findEntidadById(rows.get(i).
                        getEntidadId());
                    rows.get(i).setNombreEntidad(entidad.getNombre());
                }
                answer.add(rows.get(i));
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de departamentos
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.updateDepartamentosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @author pedro.garcia
     */
    private void updateDepartamentosItemList() {
        
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
                .add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (usuario != null && usuario.getDescripcionEstructuraOrganizacional().contains(Constantes.PREFIJO_HABILITADOS)) {
            for (Departamento departamento1 : this.departamentos) {
                if(departamento1.getCodigo()!=null && departamento1.getCodigo().equals("66")){
                    this.departamentosItemList.add(new SelectItem(departamento1
                            .getCodigo(), departamento1.getNombre()));
                    return;
                }
            }
        
        } else {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                Collections.sort(this.departamentos);
            } else {
                Collections.sort(this.departamentos,
                        Departamento.getComparatorNombre());
            }

            for (Departamento departamento1 : this.departamentos) {
                if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                    this.departamentosItemList.add(new SelectItem(departamento1
                            .getCodigo(), departamento1.getCodigo() + "-"
                            + departamento1.getNombre()));
                } else {
                    this.departamentosItemList.add(new SelectItem(departamento1
                            .getCodigo(), departamento1.getNombre()));
                }
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de municipios
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenMunicipios() {

        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.updateMunicipiosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * actualiza la lista de municipios según el departamento seleccionado
     *
     * @author pedro.garcia
     * @modified juan.agudelo
     */
    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem(null, this.DEFAULT_COMBOS));

        this.municipios = (ArrayList<Municipio>) this
                .getGeneralesService().findMunicipiosbyCodigoEstructuraOrganizacional(usuario.getCodigoEstructuraOrganizacional());
        for (Municipio municipio1 : this.municipios) {
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                this.municipiosItemList.add(new SelectItem(municipio1
                        .getCodigo(), municipio1.getCodigo3Digitos() + "-"
                        + municipio1.getNombre()));
            } else {
                this.municipiosItemList.add(new SelectItem(municipio1
                        .getCodigo(), municipio1.getNombre()));
            }
        }
    }

    private void cargarDepartamentosEntidad() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = generalService.getDepartamentos(Constantes.COLOMBIA);

        this.cargarDeptosEntidadItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService().getCacheMunicipiosByDepartamento(
                dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getCodigo().substring(2, m.getCodigo().length()) + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener del cambio en el combo de departamentos
     */
    public void onChangeDepartamentos() {
        this.municipio = null;
        this.updateMunicipiosItemList();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton Buscar
     *
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public void buscar() {

        if (this.filtroCPredioB != null &&
             (this.filtroCPredioB.getDepartamento() != null ||
             this.filtroCPredioB.getMunicipio() != null ||
             (this.filtroCPredioB.getDepartamentoCodigo() != null && !this.filtroCPredioB
            .getDepartamentoCodigo().isEmpty()) ||
             (this.filtroCPredioB.getMunicipioCodigo() != null && !this.filtroCPredioB
            .getMunicipioCodigo().isEmpty()) ||
             (this.filtroCPredioB.getTipoAvaluo() != null && !this.filtroCPredioB
            .getTipoAvaluo().isEmpty()) || (this.filtroCPredioB
                .getSectorCodigo()) != null &&
             !this.filtroCPredioB.getSectorCodigo().isEmpty()) ||
             (this.filtroCPredioB.getComunaCodigo() != null && !this.filtroCPredioB
            .getComunaCodigo().isEmpty()) ||
             (this.filtroCPredioB.getBarrioCodigo() != null && !this.filtroCPredioB
            .getBarrioCodigo().isEmpty()) ||
             (this.filtroCPredioB.getManzanaVeredaCodigo() != null && !this.filtroCPredioB
            .getManzanaVeredaCodigo().isEmpty()) ||
             (this.filtroCPredioB.getPredio() != null && !this.filtroCPredioB
            .getPredio().isEmpty()) ||
             (this.filtroCPredioB.getCondicionPropiedad() != null && !this.filtroCPredioB
            .getCondicionPropiedad().isEmpty()) ||
             (this.filtroCPredioB.getEdificio() != null && !this.filtroCPredioB
            .getEdificio().isEmpty()) ||
             (this.filtroCPredioB.getPiso() != null && !this.filtroCPredioB
            .getPiso().isEmpty()) ||
             (this.filtroCPredioB.getUnidad() != null && !this.filtroCPredioB
            .getUnidad().isEmpty())) {

            this.banderaBuscar = true;

            if (this.departamento != null) {
                this.departamento.setCodigo(this.selectedDepartamentoCod);
            }

            if (this.municipio != null) {
                this.municipio.setCodigo(this.selectedMunicipioCod);
            }

            this.lazyAssignedBloqueos = new LazyDataModel<PredioBloqueo>() {

                @Override
                public List<PredioBloqueo> load(int first, int pageSize,
                    String sortField, SortOrder sortOrder,
                    Map<String, String> filters) {
                    List<PredioBloqueo> answer = new ArrayList<PredioBloqueo>();
                    populatePredioBloqueoLazyly(answer, first, pageSize);

                    return answer;
                }
            };
            poblar();
            this.banderaBuscar = true;
        } else {
            String mensaje = "Para realizar la busqueda se necesita por lo menos un parametro." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de bloqueo
     */
    public void bloquearV() {

        if (tipoBloqueo == 0) {
            this.tipoBloqueo0 = true;
            this.tipoBloqueo1 = false;
            this.tipoBloqueo2 = false;
            this.mensajeCargaArchivo = "Cargar documento";
            this.banderaValidar = false;
            this.laBandera = false;

            if (this.filtroFinalCPredioB != null) {
                this.filtroFinalCPredioB = new FiltroDatosConsultaPrediosBloqueo();
            }

        } else if (tipoBloqueo == 1) {
            this.tipoBloqueo0 = false;
            this.tipoBloqueo1 = true;
            this.tipoBloqueo2 = false;
            this.mensajeCargaArchivo = "Cargar documento";
            this.banderaValidar = false;
            this.laBandera = false;

            if (this.filtroFinalCPredioB == null) {
                this.filtroFinalCPredioB = new FiltroDatosConsultaPrediosBloqueo();
            }

            if (this.filtroCPredioB.getDepartamentoCodigo() != null &&
                 !this.filtroCPredioB.getDepartamentoCodigo().isEmpty()) {
                this.filtroFinalCPredioB
                    .setDepartamentoCodigo(this.filtroCPredioB
                        .getDepartamentoCodigo());
            }

            if (this.filtroCPredioB.getMunicipioCodigo() != null &&
                 !this.filtroCPredioB.getMunicipioCodigo().isEmpty()) {
                this.filtroFinalCPredioB.setMunicipioCodigo(this.filtroCPredioB
                    .getMunicipioCodigo());
            }

        } else if (tipoBloqueo == 2) {
            this.tipoBloqueo0 = false;
            this.tipoBloqueo1 = false;
            this.tipoBloqueo2 = true;
            this.mensajeCargaArchivo = "Cargar archivo";
            this.banderaValidar = true;
            this.laBandera = false;

            if (this.filtroFinalCPredioB != null) {
                this.filtroFinalCPredioB = new FiltroDatosConsultaPrediosBloqueo();
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo que escucha la seleccion de cambio de pestaña
     */
    public void cambioTab(TabChangeEvent eventoCambioPes) {

        if (eventoCambioPes.getTab().getId().equals("bloquearPrediosTab")) {
            this.tipoBloqueoCargar = false;
            this.tipoAvaluo = "";
        } else {
            this.tipoBloqueoCargar = true;
            this.tipoAvaluo = "";
            this.sectorCodigo = "";
        }

        this.selectedDepartamentoCod = null;
        this.selectedMunicipioCod = null;
        this.lazyAssignedBloqueos = null;

        if (this.filtroCPredioB != null) {
            this.filtroCPredioB.setDepartamentoCodigo(null);
            this.filtroCPredioB.setMunicipioCodigo(null);
        }
        this.filtroCPredioB = new FiltroDatosConsultaPrediosBloqueo();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo para guardar los datos de predio bloqueo ejecutado sobre el boton guardar
     */
    public void bloquearPB() {

        RequestContext context = RequestContext.getCurrentInstance();
        this.prediosBloqueados = new ArrayList<PredioBloqueo>();

        this.predioBloqueo = new PredioBloqueo();
        PredioBloqueo predioBloqueoTemporal;
        ArrayList<PredioBloqueo> prediosBloqueadosTemporal = new ArrayList<PredioBloqueo>();

        Municipio m = new Municipio();
        m.setCodigo(this.selectedMunicipio);

        Departamento d = new Departamento();
        d.setCodigo(this.selectedDepartamento);

        Entidad entidad;
        Boolean actividad;

        if (this.tipoBloqueo == 0 && this.aSoporte == 1) {
            if (this.prediosSeleccionados.size() == 1) {

                this.predioBloqueo.setPredio(this.prediosSeleccionados.get(0));
                this.predioBloqueo.setFechaInicioBloqueo(this.fechaInicial);
                this.predioBloqueo.setFechaTerminaBloqueo(this.fechaFinal);

                this.predioBloqueo.setFechaEnvio(this.fechaEnvio);
                this.predioBloqueo.setNumeroRadicacion(this.numeroRadicado);

                this.predioBloqueo.setFechaRecibido(this.fechaRecibido);
                this.predioBloqueo.setMotivoBloqueo(this.motivoBloqueo);
                this.predioBloqueo.setDocumentoSoporteBloqueo(this.documentoSoporteBloqueo);

                this.predioBloqueo.setMunicipio(m);
                this.predioBloqueo.setDepartamento(d);

                if (this.selectedRestitucionTierras == true) {
                    this.predioBloqueo.setRestitucionTierras("SI");
                } else {
                    this.predioBloqueo.setRestitucionTierras("NO");
                }

                if (this.idEntidadBloqueo != null) {
                    this.predioBloqueo.setEntidadId(this.idEntidadBloqueo);
                }

                this.predioBloqueo.setTipoBloqueo(this.selectedTipoBloqueo);

                this.prediosBloqueados.add(this.predioBloqueo);

                if (this.predioBloqueo.getFechaTerminaBloqueo() != null) {
                    this.predioBloqueo.setFechaDesbloqueo(this.predioBloqueo.
                        getFechaTerminaBloqueo());
                }

                try {

                    ResultadoBloqueoMasivo resultado = this
                        .getConservacionService().bloquearMasivamentePredios(
                            this.usuario, (ArrayList) this.prediosBloqueados);

                    if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                         !resultado.getBloqueoMasivoPrediosExitoso()
                            .isEmpty()) {

                        int contadorErrores = 0;
                        for (PredioBloqueo pbTemp : resultado
                            .getBloqueoMasivoPrediosExitoso()) {

                            if (pbTemp.getDocumentoSoporteBloqueo() != null) {

                                if (pbTemp.getDocumentoSoporteBloqueo() != null) {
                                    if (pbTemp.getDocumentoSoporteBloqueo()
                                        .getIdRepositorioDocumentos() == null) {
                                        contadorErrores++;
                                        break;
                                    }
                                }
                            }
                        }

                        if (resultado.getErroresProcesoMasivo() != null &&
                             !resultado.getErroresProcesoMasivo()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio identificado con número predial ");

                            for (ErrorProcesoMasivo epm : resultado
                                .getErroresProcesoMasivo()) {
                                mensaje.append(epm.getTextoFuente()).append(" ");
                            }

                            mensaje.append(" se encuentrá cancelado y no puede ser marcado.");

                            this.addMensajeWarn(mensaje.toString());

                        }

                        if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                             !resultado.getBloqueoMasivoPrediosExitoso()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio  ");

                            if (contadorErrores == 0) {
                                mensaje.append(" se marcó exitosamente.");
                                this.addMensajeInfo(mensaje.toString());
                            } else if (contadorErrores > 0) {
                                mensaje.append(" se marcó exitosamente.");
                                mensaje.append(
                                    " pero se presentaron errores en el servicio de Alfresco");
                                mensaje.append(" y el documento de soporte no pudo ser almacenado!");
                                this.addMensajeWarn(mensaje.toString());
                            }

                            if (this.selectedRestitucionTierras &&
                                 this.getConservacionService().busquedaPredioBloqueoByTipoBloqueo(
                                    predioBloqueo.getPredio().getNumeroPredial(),
                                    EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo()) == null) {
                                predioBloqueoTemporal = predioBloqueo.clone();
                                predioBloqueoTemporal.setId(null);
                                predioBloqueoTemporal.setTipoBloqueo(
                                    EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo());
                                prediosBloqueadosTemporal.add(predioBloqueoTemporal);
                                this.getConservacionService().actualizarPrediosBloqueo(this.usuario,
                                    prediosBloqueadosTemporal);
                            }

                            entidad = this.getConservacionService().findEntidadById(
                                this.idEntidadBloqueo);

                            if (this.selectedTipoBloqueo.equals(EPredioBloqueoTipoBloqueo.SUSPENDER.
                                getCodigo())) {

                                this.tramites = this.getTramiteService().
                                    buscarTramitesTiposTramitesByNumeroPredial(predioBloqueo.
                                        getPredio().getNumeroPredial());

                                if (this.tramites != null && !this.tramites.isEmpty()) {

                                    for (Tramite tram : this.tramites) {
                                        tram.setEstadoBloqueo(ETramiteEstadoBloqueo.SUSPENDIDO.
                                            getCodigo());
                                        this.getTramiteService().guardarActualizarTramite2(tram);
                                    }

                                    actividad = validarSubProcesoValidacion(this.predioBloqueo,
                                        entidad.getNombre(), entidad.getCorreo());

                                    if (actividad == false) {
                                        bloqueoProcesos(this.predioBloqueo, entidad.getNombre(),
                                            entidad.getCorreo());
                                    }
                                }

                            }
                        }

                        this.banderaBotonGuardar = false;
                        this.banderaBotonGenerarInforme = true;

                    } else if (resultado.getErroresProcesoMasivo() != null &&
                         !resultado.getErroresProcesoMasivo()
                            .isEmpty()) {

                        StringBuilder mensaje = new StringBuilder();
                        mensaje.append("El predio identificado con número predial ");

                        for (ErrorProcesoMasivo epm : resultado
                            .getErroresProcesoMasivo()) {
                            mensaje.append(epm.getTextoFuente()).append(" ");
                        }

                        mensaje.append(" se encuentra cancelado y no puede ser marcado.");

                        this.addMensajeInfo(mensaje.toString());
                        context.addCallbackParam("error", "error");

                    } else {
                        List<String> prediosCancelados = new ArrayList<String>();
                        for (PredioBloqueo predBloq : this.prediosBloqueados) {
                            if (predBloq != null &&
                                 predBloq.getPredio() != null &&
                                 EPredioEstado.CANCELADO.getCodigo().equals(predBloq.getPredio().
                                    getEstado())) {
                                prediosCancelados.add(predBloq.getPredio().getNumeroPredial());
                            }
                        }

                        if (!prediosCancelados.isEmpty()) {
                            if (prediosCancelados.size() == 1) {
                                this.addMensajeError("El predio identificado con número predial " +
                                    prediosCancelados.get(0) +
                                    " se encuentra cancelado y no puede ser marcado.");
                            } else {
                                String prediosCanceladosConcat = new String("");
                                for (String s : prediosCancelados) {
                                    prediosCanceladosConcat = prediosCanceladosConcat + s + ", ";
                                }
                                this.addMensajeError(
                                    "Los predios identificados con los números prediales " +
                                    prediosCanceladosConcat +
                                    "se encuentran cancelados y no pueden ser marcados.");
                            }
                        } else {
                            String mensaje = "El predio seleccionado no arrojó ningún resultado";
                            this.addMensajeError(mensaje);
                        }
                        context.addCallbackParam("error", "error");
                    }

                } catch (Exception e) {
                    String mensaje = "El predio seleccionado no existe o no pudo ser marcado!";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }
            } else if (this.prediosSeleccionados.size() < 1) {

                String mensaje = "No existe ningún predio con el número predial ingresado" +
                     " en el departamento y municipio seleccionados." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            } else if (this.prediosSeleccionados.size() > 1) {
                String mensaje = "Existén multiples predios con el número predial ingresado" +
                     " en el departamento y municipio seleccionados." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else if (tipoBloqueo == 0 && this.aSoporte == 0) {
            String mensaje = "No se adjuntó un documento de soporte para la marcación";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 1 && this.aSoporte == 1) {

            PredioBloqueo pbTemp;
            Documento docTemp;
            this.prediosBloqueados = new ArrayList<PredioBloqueo>();

            for (Predio p : this.prediosSeleccionados) {

                pbTemp = new PredioBloqueo();

                docTemp = this.documentoSoporteBloqueo;
                docTemp.setPredioId(p.getId());

                pbTemp.setFechaInicioBloqueo(this.fechaInicial);
                pbTemp.setFechaTerminaBloqueo(this.fechaFinal);
                pbTemp.setFechaRecibido(this.fechaRecibido);
                pbTemp.setMotivoBloqueo(this.motivoBloqueo);
                pbTemp.setFechaEnvio(this.fechaEnvio);
                pbTemp.setNumeroRadicacion(this.numeroRadicado);
                pbTemp.setDocumentoSoporteBloqueo(docTemp);
                pbTemp.setPredio(p);
                pbTemp.setMunicipio(m);
                pbTemp.setDepartamento(d);

                if (this.selectedRestitucionTierras == true) {
                    pbTemp.setRestitucionTierras("SI");
                } else {
                    pbTemp.setRestitucionTierras("NO");
                }

                if (this.idEntidadBloqueo != null) {
                    pbTemp.setEntidadId(this.idEntidadBloqueo);
                }

                pbTemp.setTipoBloqueo(this.selectedTipoBloqueo);

                this.prediosBloqueados.add(pbTemp);
            }

            try {
                ResultadoBloqueoMasivo resultado = this
                    .getConservacionService().bloquearMasivamentePredios(
                        this.usuario, (ArrayList) this.prediosBloqueados);

                if (!resultado.getBloqueoMasivoPrediosExitoso().isEmpty() &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     resultado.getBloqueoMasivoPrediosExitoso().size() > 0) {

                    if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                         !resultado.getBloqueoMasivoPrediosExitoso()
                            .isEmpty()) {

                        int contadorErrores = 0;
                        for (PredioBloqueo pbTmp : resultado
                            .getBloqueoMasivoPrediosExitoso()) {

                            if (pbTmp.getDocumentoSoporteBloqueo() != null) {

                                if (pbTmp.getDocumentoSoporteBloqueo() != null) {
                                    if (pbTmp.getDocumentoSoporteBloqueo()
                                        .getIdRepositorioDocumentos() == null) {
                                        contadorErrores++;
                                        break;
                                    }
                                }
                            }
                        }

                        if (resultado.getErroresProcesoMasivo() != null &&
                             !resultado.getErroresProcesoMasivo()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El predio(s) identificado con número predial ");

                            for (ErrorProcesoMasivo epm : resultado
                                .getErroresProcesoMasivo()) {
                                mensaje.append(epm.getTextoFuente()).append(" ");
                            }

                            mensaje.append(" se encuentrá cancelado y no puede ser marcado.");

                            this.addMensajeWarn(mensaje.toString());
                        }

                        if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                             !resultado.getBloqueoMasivoPrediosExitoso()
                                .isEmpty()) {

                            StringBuilder mensaje = new StringBuilder();
                            mensaje.append("El rango de predios");

                            if (this.selectedTipoBloqueo.equals(EPredioBloqueoTipoBloqueo.SUSPENDER.
                                getCodigo())) {
                                entidad = this.getConservacionService().findEntidadById(
                                    this.idEntidadBloqueo);

                                for (PredioBloqueo pb : resultado.getBloqueoMasivoPrediosExitoso()) {

                                    this.tramites = this.getTramiteService().
                                        buscarTramitesTiposTramitesByNumeroPredial(pb.getPredio().
                                            getNumeroPredial());

                                    if (this.selectedRestitucionTierras == true && this.
                                        getConservacionService().busquedaPredioBloqueoByTipoBloqueo(
                                            pb.getPredio().getNumeroPredial(),
                                            EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo()) == null) {
                                        predioBloqueoTemporal = pb.clone();
                                        predioBloqueoTemporal.setId(null);
                                        prediosBloqueadosTemporal = new ArrayList<PredioBloqueo>();
                                        predioBloqueoTemporal.setTipoBloqueo(
                                            EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo());
                                        prediosBloqueadosTemporal.add(predioBloqueoTemporal);
                                        this.getConservacionService().actualizarPrediosBloqueo(
                                            this.usuario, prediosBloqueadosTemporal);
                                    }

                                    if (this.tramites != null && !this.tramites.isEmpty()) {

                                        for (Tramite tram : this.tramites) {
                                            tram.setEstadoBloqueo(ETramiteEstadoBloqueo.SUSPENDIDO.
                                                getCodigo());
                                            this.getTramiteService().guardarActualizarTramite2(tram);
                                        }

                                        actividad = validarSubProcesoValidacion(pb, entidad.
                                            getNombre(), entidad.getCorreo());

                                        if (actividad == false) {
                                            bloqueoProcesos(pb, entidad.getNombre(), entidad.
                                                getCorreo());
                                        }
                                    }
                                }
                            }

                            if (contadorErrores == 0) {

                                mensaje.append(" se marcó exitosamente.");
                                this.addMensajeInfo(mensaje.toString());

                            } else if (contadorErrores > 0) {
                                mensaje.append(" se marcó exitosamente.");
                                mensaje.append(
                                    " pero se presentaron errores en el servicio de Alfresco");
                                mensaje.append(" y el documento de soporte no pudo ser almacenado!");
                                this.addMensajeWarn(mensaje.toString());
                            }
                        }

                        this.banderaBotonGuardar = false;
                        this.banderaBotonGenerarInforme = true;
                    }
                } else if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     resultado.getBloqueoMasivoPrediosExitoso().size() == 0) {
                    String mensaje = "El rango de predios seleccionados no arrojó ningún resultado";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                } else {
                    String mensaje =
                        "Ocurrio un error marcando el rango de predios y no se realizaron marcaciones.";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }
            } catch (Exception e) {
                String mensaje =
                    "El rango de predios selecionados no es válido o no pudo ser marcado!";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else if (this.tipoBloqueo == 1 && this.aSoporte == 0) {
            String mensaje = "No se adjuntó un documento de soporte para la marcación";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para guardar los datos de predio bloqueo ejecutado sobre el botón guardar
     */
    public void bloquearMPB() {
        RequestContext context = RequestContext.getCurrentInstance();
        Entidad entidad;
        Boolean actividad;
        PredioBloqueo predioBloqueoTemporal;
        ArrayList<PredioBloqueo> prediosBloqueadosTemporal = new ArrayList<PredioBloqueo>();

        if (this.tipoBloqueo == 2 && this.aSoporte == 1 && this.aBMasa == 1) {

            List<DocumentoArchivoAnexo> daas = null;

            try {

                for (PredioBloqueo pb : this.bloqueoMasivo) {

                    pb.setDocumentoSoporteBloqueo(this.documentoSoporteBloqueo);

                    if (pb.getDocumentoSoporteBloqueo()
                        .getDocumentoArchivoAnexos() == null ||
                         (pb.getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos() == null && !pb
                            .getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos().isEmpty())) {

                        daas = new ArrayList<DocumentoArchivoAnexo>();

                        daas.add(this.documentoArchivoAnexo);
                        pb.getDocumentoSoporteBloqueo()
                            .setDocumentoArchivoAnexos(daas);
                    }
                }

                ResultadoBloqueoMasivo resultado = this
                    .getConservacionService().bloquearMasivamentePredios(
                        this.usuario, this.bloqueoMasivo);

                if (resultado != null &&
                     (resultado.getBloqueoMasivoPrediosExitoso() != null && !resultado.
                    getBloqueoMasivoPrediosExitoso().isEmpty() ||
                     resultado.getErroresProcesoMasivo() != null && !resultado.
                    getErroresProcesoMasivo().isEmpty())) {
                    generarReporteDePrediosBloqueados(resultado);
                }

                if (resultado != null) {

                    int contadorErrores = 0;
                    for (PredioBloqueo pbTmp : resultado
                        .getBloqueoMasivoPrediosExitoso()) {

                        if (pbTmp.getDocumentoSoporteBloqueo() != null) {

                            if (pbTmp.getDocumentoSoporteBloqueo() != null) {
                                if (pbTmp.getDocumentoSoporteBloqueo()
                                    .getIdRepositorioDocumentos() == null) {
                                    contadorErrores++;
                                    break;
                                }
                            }
                        }
                    }

                    if (contadorErrores > 0) {
                        String mensaje = "La marcación masiva de predios " +
                             " presentó errores en el servicio de Alfresco" +
                             " y el documento de soporte no pudo ser almacenado!";
                        this.addMensajeWarn(mensaje);
                    }

                }

                if (resultado != null &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     resultado.getErroresProcesoMasivo() != null &&
                     !resultado.getErroresProcesoMasivo().isEmpty()) {
                    String mensaje = "La marcación masiva se realizó pero se presentaron errores";
                    this.addMensajeInfo(mensaje);
                } else if (resultado != null &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty() &&
                     (resultado.getErroresProcesoMasivo() == null || (resultado
                    .getErroresProcesoMasivo() != null && resultado
                        .getErroresProcesoMasivo().isEmpty()))) {
                    String mensaje = "La marcación masiva se realizó exitosamente";
                    this.addMensajeInfo(mensaje);
                } else if (resultado != null &&
                     resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     resultado.getBloqueoMasivoPrediosExitoso().isEmpty()) {
                    String mensaje = "El bloqueo masivo no arrojó ningún resultado";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }

                //Suspención de los predios que se bloquearon
                if (resultado != null && resultado.getBloqueoMasivoPrediosExitoso() != null &&
                     !resultado.getBloqueoMasivoPrediosExitoso()
                        .isEmpty()) {

                    for (PredioBloqueo pb : resultado.getBloqueoMasivoPrediosExitoso()) {

                        entidad = this.getConservacionService().findEntidadById(pb.getEntidadId());

                        if (pb.getTipoBloqueo().equals(EPredioBloqueoTipoBloqueo.SUSPENDER.
                            getCodigo())) {

                            if ("SI".equals(pb.getRestitucionTierras()) && this.
                                getConservacionService().busquedaPredioBloqueoByTipoBloqueo(pb.
                                    getPredio().getNumeroPredial(),
                                    EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo()) == null) {
                                prediosBloqueadosTemporal = new ArrayList<PredioBloqueo>();
                                predioBloqueoTemporal = pb.clone();
                                predioBloqueoTemporal.setId(null);
                                predioBloqueoTemporal.setTipoBloqueo(
                                    EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo());
                                prediosBloqueadosTemporal.add(predioBloqueoTemporal);
                                this.getConservacionService().actualizarPrediosBloqueo(this.usuario,
                                    prediosBloqueadosTemporal);
                            }

                            this.tramites = this.getTramiteService().
                                buscarTramitesTiposTramitesByNumeroPredial(pb.getPredio().
                                    getNumeroPredial());

                            if (this.tramites != null && !this.tramites.isEmpty()) {

                                for (Tramite tram : this.tramites) {
                                    tram.setEstadoBloqueo(ETramiteEstadoBloqueo.SUSPENDIDO.
                                        getCodigo());
                                    this.getTramiteService().guardarActualizarTramite2(tram);
                                }

                                actividad = validarSubProcesoValidacion(pb, entidad.getNombre(),
                                    entidad.getCorreo());

                                if (actividad == false) {
                                    bloqueoProcesos(pb, entidad.getNombre(), entidad.getCorreo());
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                String mensaje =
                    "La marcación masiva no es válido o no pudo realizar las marcaciones!";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }

        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 1 && this.aSoporteSize == 0) {
            String mensaje =
                "El documento soporte de la marcación, se encuentra en blanco, verifique";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 1) {
            String mensaje =
                "No se ha realizado la carga del documento soporte de la marcación masiva, verifique";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 1 &&
             this.aBMasa == 0 && this.aBloqueoSize == 0) {
            String mensaje = "El archivo con predios a marcár se encuentra vacío, verifique";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 1 &&
             this.aBMasa == 0) {
            String mensaje;
            if (this.nombreDocumentoBloqueoMasivo == null ||
                 (this.nombreDocumentoBloqueoMasivo != null && this.nombreDocumentoBloqueoMasivo
                    .isEmpty())) {
                mensaje = "No se ha realizado la carga del archivo con predios a marcár, verifique";
            } else {
                mensaje = "El archivo que adjunto no contiene ningúna marcación valida." +
                     " Por favor corrija los datos e intente nuevamente.";
            }
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 0) {
            String mensaje =
                "No se adjuntó un documento de soporte, ni un archivo de marcación masiva." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    /**
     * Método encargado de generar el reporte de predios bloqueados masivamente
     *
     * @author javier.aponte
     *
     */
    public void generarReporteDePrediosBloqueados(
        ResultadoBloqueoMasivo resultado) {

        List<TmpBloqueoMasivo> idsBloqueos = new ArrayList<TmpBloqueoMasivo>();
        TmpBloqueoMasivo bloqueoMasivo;

        // Generar secuencia de sesion de tmp_bloqueo_masivo
        BigDecimal sessionId = this.getConservacionService()
            .generarSecuenciaTmpBloqueoMasivo();

        if (resultado.getBloqueoMasivoPrediosExitoso() != null &&
             !resultado.getBloqueoMasivoPrediosExitoso().isEmpty()) {
            for (PredioBloqueo bloqueoMasivoExitoso : resultado
                .getBloqueoMasivoPrediosExitoso()) {
                bloqueoMasivo = new TmpBloqueoMasivo();
                bloqueoMasivo.setSessionId(sessionId);
                bloqueoMasivo.setBloqueado(ESiNo.SI.getCodigo());
                bloqueoMasivo.setFechaLog(new Date());
                bloqueoMasivo.setIdentificador(bloqueoMasivoExitoso.getPredio()
                    .getNumeroPredial());

                idsBloqueos.add(bloqueoMasivo);
            }
        }

        if (resultado.getErroresProcesoMasivo() != null &&
             !resultado.getErroresProcesoMasivo().isEmpty()) {
            String[] textoFuente;
            for (ErrorProcesoMasivo bloqueoMasivoError : resultado
                .getErroresProcesoMasivo()) {
                bloqueoMasivo = new TmpBloqueoMasivo();
                bloqueoMasivo.setSessionId(sessionId);
                bloqueoMasivo.setBloqueado(ESiNo.NO.getCodigo());
                bloqueoMasivo.setFechaLog(new Date());
                if (bloqueoMasivoError.getTextoFuente() != null) {
                    textoFuente = bloqueoMasivoError.getTextoFuente().split(
                        SEPARATOR_PARAMETER);
                    // guarda el número predial
                    bloqueoMasivo.setIdentificador(textoFuente[0]);
                    idsBloqueos.add(bloqueoMasivo);
                }
            }
        }

        // Guarda los ids de las ofertas inmobiliarias seleccionadas
        idsBloqueos = this.getConservacionService()
            .guardarActualizarIdsBloqueoMasivo(idsBloqueos);

        if (idsBloqueos != null && !idsBloqueos.isEmpty()) {
            try {
                this.generarReporteBloqueoMasivoPredios(idsBloqueos.get(0)
                    .getSessionId(), resultado);
            } catch (Exception e) {
                LOGGER.error("Ocurrió un error en el método generarReporteBloqueoMasivoPredios");
            } finally {
                // Borra los ids de las ofertas inmobiliarias seleccionadas
                this.getConservacionService().borrarIdsBloqueoMasivo(
                    idsBloqueos);
            }
        }
    }

    /**
     * Método para generar el reporte de bloqueo de predios masivos
     *
     * @author javier.aponte
     * @throws Exception
     */
    // --------------------------------------------------------------------------------------------------
    public void generarReporteBloqueoMasivoPredios(BigDecimal sessionId,
        ResultadoBloqueoMasivo resultado) throws Exception {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.BLOQUEO_MASIVO_PREDIOS;

        Map<String, String> parameters = new HashMap<String, String>();
        try {
            if (resultado != null &&
                 resultado.getBloqueoMasivoPrediosExitoso() != null &&
                 !resultado.getBloqueoMasivoPrediosExitoso().isEmpty()) {

                SimpleDateFormat formatoFecha = new SimpleDateFormat(
                    Constantes.FORMATO_FECHA);
                String fechaAux = null;
                if (resultado.getBloqueoMasivoPrediosExitoso().get(0)
                    .getFechaInicioBloqueo() != null) {
                    fechaAux = formatoFecha.format(resultado
                        .getBloqueoMasivoPrediosExitoso().get(0)
                        .getFechaInicioBloqueo());
                }
                parameters.put("FECHA_INICIO_BLOQUEO", fechaAux);
                fechaAux = null;
                if (resultado.getBloqueoMasivoPrediosExitoso().get(0)
                    .getFechaTerminaBloqueo() != null) {
                    fechaAux = formatoFecha.format(resultado
                        .getBloqueoMasivoPrediosExitoso().get(0)
                        .getFechaTerminaBloqueo());
                }
                parameters.put("FECHA_TERMINA_BLOQUEO", fechaAux);
                parameters.put("MOTIVO_BLOQUEO", resultado
                    .getBloqueoMasivoPrediosExitoso().get(0)
                    .getMotivoBloqueo());
            }
            parameters.put("SESSION_ID", String.valueOf(sessionId));

            this.reporteBloqueoMasivoPredios = reportsService.generarReporte(parameters,
                enumeracionReporte, this.usuario);

            if (this.reporteBloqueoMasivoPredios != null) {

                PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
                    .getManagedBean("previsualizacionDocumento");

                previsualizacionDocMB.setRutaArchivoTemporal(this.reporteBloqueoMasivoPredios.
                    getUrlWebReporte());
            } else {
                this.addMensajeError(Constantes.MENSAJE_ERROR_REPORTE);
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar al reporte de marcación masiva de predios");
            throw new Exception(
                "Ocurrió un error al generar el reporte de marcación " +
                 "masiva de predios");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de soporte a cargar
     */
    private void nuevoDocumento() {

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento
            .setId(ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PREDIO.getId());

        this.documentoSoporteBloqueo.setFechaDocumento(new Date());
        this.documentoSoporteBloqueo.setEstado(EDocumentoEstado.RECIBIDO
            .getCodigo());
        this.documentoSoporteBloqueo.setBloqueado(ESiNo.NO.getCodigo());

        this.documentoSoporteBloqueo.setTipoDocumento(tipoDocumento);
        this.documentoSoporteBloqueo.setUsuarioCreador(this.usuario.getLogin());
        this.documentoSoporteBloqueo.setUsuarioLog(this.usuario.getLogin());
        this.documentoSoporteBloqueo.setFechaLog(new Date());

    }

//--------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                eventoCarga.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            nuevoDocumento();

            this.documentoSoporteBloqueo.setArchivo(eventoCarga.getFile()
                .getFileName());

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;
            this.aSoporteSize = eventoCarga.getFile().getSize();

            String mensaje = "El documento se cargó adecuadamente.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            LOGGER.error("ERROR: " + e);
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * metodo para recueprar la dirección de la imagen
     */
    public String getUrlBase() {
        return FacesContext.getCurrentInstance().getExternalContext()
            .getRealPath("/");
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivo de bloqueo en masa
     */
    public void archivoVM(FileUploadEvent uploadEventMasivo) {

        FileOutputStream fileOutputStream = null;
        this.documentoTemporalBloqueoMasivo = new Documento();
        this.nombreDocumentoBloqueoMasivo = uploadEventMasivo.getFile().getFileName();
        this.erroresPredios = false;
        this.bloquearPredios = true;

        this.documentoTemporalBloqueoMasivo
            .setArchivo(this.nombreDocumentoBloqueoMasivo);
        try {
            this.archivoMasivoResultado = new File(
                FileUtils.getTempDirectory().getAbsolutePath(),
                this.nombreDocumentoBloqueoMasivo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoMasivoResultado);

            byte[] buffer = new byte[Math.round(uploadEventMasivo.getFile().getSize())];

            int bulk;
            InputStream inputStream = uploadEventMasivo.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;
            this.aBloqueoSize = uploadEventMasivo.getFile().getSize();

            this.lecturaArchivoMasivo(uploadEventMasivo);

        } catch (IOException e) {
            this.LOGGER.error("ERROR: " + e);
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }
    }

    private boolean esNumeroRadicacionValido(final String numeroRadicacion, final UsuarioDTO usuario) {
        boolean retval = true;
        if (usuario == null || numeroRadicacion == null || numeroRadicacion.trim().isEmpty()) {
            retval = false;
        } else {
            retval = this.getConservacionService().validarNumeroRadicacion(numeroRadicacion.trim(),
                usuario);
        }
        if (usuario != null && usuario.getDescripcionEstructuraOrganizacional() != null &&
            (usuario.getDescripcionEstructuraOrganizacional().contains(Constantes.PREFIJO_DELEGADOS)
                || usuario.getDescripcionEstructuraOrganizacional().contains(Constantes.PREFIJO_HABILITADOS))
                ) {
            retval = true;
        }
        return retval;
    }

    /**
     * Método de lectura del archivo de bloqueo masivo
     */
    @SuppressWarnings("rawtypes")
    private void lecturaArchivoMasivo(FileUploadEvent eventoMasivo) {

        String sCadena;
        String separador;
        this.bloqueoMasivo = new ArrayList<PredioBloqueo>();
        Predio predioTemp;
        PredioBloqueo pb;
        Entidad entidad = null;
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaTemp;
        Object part[] = null;
        String mensaje = "";
        Date fechatemporal1;
        Date fechatemporal2;
        int contadorErrores = 0;
        int posEmpiezanFechas = 0;
        int c = 0;
        Row fila = null;
        Cell celda = null;
        wb = new HSSFWorkbook();
        HSSFCellStyle styleHeader = (HSSFCellStyle) wb.createCellStyle();
        HSSFFont fontHeader = (HSSFFont) wb.createFont();
        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHeader.setFont(fontHeader);
        Sheet sheetReporte = wb.createSheet("sheet");
        int columnaReporte = 0;
        int filaReporte = 0;
        boolean validarDptoMuni = false;
        Pattern p = Pattern.compile("[0-9]*ER[0-9]*");
        Matcher m = null;
        Municipio mun = null;
        Departamento dep = null;
        int rango = 0;

        try {

            if (eventoMasivo.getFile().getContentType()
                .equals(EArchivoAnexoFormato.ARCHIVO_PLANO.getValor()) ||
                 eventoMasivo
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_PLANO
                            .getExtension())) {

                BufferedReader bf = new BufferedReader(new FileReader(
                    this.archivoMasivoResultado));
                separador = Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO;
                contadorErrores = 0;
                String partes[];

                while ((sCadena = bf.readLine()) != null) {

                    partes = sCadena.split(separador);
                    posEmpiezanFechas = 0;
                    fila = sheetReporte.createRow((short) filaReporte);

                    if (partes.length > 13 || partes.length < 12) {
                        celda = fila.createCell(columnaReporte);
                        mensaje = "El predio " + partes[0] +
                            " no tiene el número de columnas requeridas, verifique";
                        celda.setCellValue(mensaje);
                        contadorErrores++;
                    } else {
                        if (partes.length == 12) {
                            rango = 11;
                        } else {
                            rango = 10;
                        }

                        for (int x = 0; x <= rango; x++) {
                            mensaje = "";
                            if (partes[x].isEmpty()) {
                                switch (x) {
                                    case 0:
                                        mensaje =
                                            "Un predio no tiene registrado un número predial, verifique";
                                        break;
                                    case 1:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrado un tipo de marcación valido, verifique";
                                        break;
                                    case 2:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrado un departamento valido, verifique";
                                        break;
                                    case 3:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrado un municipio valido, verifique";
                                        break;
                                    case 4:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrado una entidad ordena la marcación valido, verifique";
                                        break;
                                    case 5:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrado un valor de restitución de tierras valido, verifique";
                                        break;
                                    case 6:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrado un motivo de marcación valido, verifique";
                                        break;
                                    case 7:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrada una fecha inicial valido, verifique";
                                        break;
                                    case 8:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrada una fecha envió valido, verifique";
                                        break;
                                    case 9:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene registrada una fecha recibido valido, verifique";
                                        break;
                                    case 10:
                                        mensaje = "El predio " + partes[0] +
                                            " no tiene un formato número de radicación valido, verifique";
                                        break;
                                    default:
                                        mensaje = "";
                                        break;
                                }

                                if (!"".equals(mensaje)) {
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }
                            }
                            if (!partes[x].isEmpty() && (x == 7 || x == 8 || x == 9 || x == 11)) {

                                try {
                                    if (!partes[x].isEmpty()) {
                                        formatoFecha.parse(partes[x]);
                                    }
                                } catch (Exception e) {
                                    partes[x] = "ERROR";
                                }
                            }

                            posEmpiezanFechas++;

                        }

                        if (!partes[0].isEmpty() && partes[0].length() != 30) {
                            mensaje = "El predio " + partes[0] +
                                " el número predial no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else if (!partes[0].isEmpty() && partes[0].length() == 30) {

                            if (this.getTramiteService().buscaPredio(partes[0]) == null) {
                                mensaje = "El predio " + partes[0] +
                                    ", no existe en la base catastral,  Verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (!partes[1].isEmpty() && !(EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo().
                            toString().equals(partes[1])) && !(EPredioBloqueoTipoBloqueo.SUSPENDER.
                            getCodigo().equals(partes[1]))) {
                            mensaje = "El predio " + partes[0] +
                                " el tipo de marcación no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[2].isEmpty() && (partes[2].length()) != 2) {
                            mensaje = "Para el predio " + partes[0] +
                                ", el código de departamento no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else if (!this.getConservacionService().validaExistenciaEntidad(null,
                            EEntidadEstado.ACTIVO.getEstado(), partes[2], null)) {
                            mensaje = "Para el predio " + partes[0] +
                                ", el departamento no tiene  entidades que puedan realizar la marcación de predios, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[3].isEmpty() && partes[3].length() != 3) {

                            mensaje = "Para el predio " + partes[0] +
                                " el código de municipio no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;

                        } else if (!partes[2].isEmpty() && (partes[2].length()) == 2) {

                            if (this.getGeneralesService().validaMunicipioDepartamento(partes[2],
                                partes[2].toString().trim().concat(partes[3].toString().trim())) ==
                                false) {
                                mensaje = "Para el predio " + partes[0] +
                                    " el código de municipio no corresponde al departamento asociado”";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            } else {
                                String codMun = (partes[3].toString().trim().length() == 3) ?
                                    partes[2] + partes[3] : partes[3];
                                if (this.getConservacionService().validaExistenciaEntidad(null,
                                    EEntidadEstado.ACTIVO.getEstado(), partes[2], codMun) == false) {
                                    mensaje = " Para el predio " + partes[0] +
                                        ", el municipio no tiene entidades que puedan realizar la marcación de predios, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                } else if (!partes[4].isEmpty() &&
                                     !this.getConservacionService().validaExistenciaEntidad(
                                        partes[4], EEntidadEstado.ACTIVO.getEstado(), partes[2],
                                        codMun)) {
                                    mensaje = " La entidad registrada para el predio " + partes[0] +
                                        ", no está parametrizada para realizar la marcación de predios, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }
                            }
                        }

                        if (!partes[5].isEmpty() && !"SI".equals(partes[5]) && !"NO".equals(
                            partes[5])) {
                            mensaje = "El predio " + partes[0] +
                                " el dato restitución de tierras no tiene un formato válido, tiene registrado un valor diferente a SI o NO, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[7].isEmpty() && !partes[7].equals("ERROR") && !partes[11].
                            isEmpty() && !"ERROR".equals(partes[11])) {

                            fechatemporal1 = formatoFecha.parse(partes[7]);
                            fechatemporal2 = formatoFecha.parse(partes[11]);

                            if (fechatemporal2.before(fechatemporal1)) {
                                mensaje = "Para el predio identificado con número predial " +
                                     partes[0].toString().trim().trim() +
                                     " la fecha final no puede ser menor a la fecha inicial" +
                                     " por lo que este predio no se incluirá para ser marcada.";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (!partes[7].isEmpty() && "ERROR".equals(partes[7])) {
                            mensaje = "El predio " + partes[0] +
                                " tiene registrada una fecha inicial sin un formato valido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (partes.length == 12 && !partes[11].isEmpty() && "ERROR".equals(
                            partes[11])) {
                            mensaje = "El predio " + partes[0] +
                                " tiene registrada una fecha final sin un formato valido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[8].isEmpty() && !"ERROR".equals(partes[8]) && !partes[9].
                            isEmpty() && !"ERROR".equals(partes[9])) {

                            fechatemporal1 = formatoFecha.parse(partes[8]);
                            fechatemporal2 = formatoFecha.parse(partes[9]);

                            if (fechatemporal2.before(fechatemporal1)) {
                                mensaje = "En el predio " + partes[0] +
                                    
                                    " La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (!partes[8].isEmpty() && "ERROR".equals(partes[8])) {
                            mensaje = "El predio " + partes[0] +
                                " tiene registrada una fecha de envió sin un formato valido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[9].isEmpty() && !"ERROR".equals(partes[9])) {

                            fechatemporal1 = formatoFecha.parse(partes[9]);
                            fechatemporal2 = new Date();

                            if (fechatemporal1.after(fechatemporal2)) {
                                mensaje = "En el predio " + partes[0] +
                                    
                                    " La fecha de recibido no puede ser mayor a la fecha actual, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (!partes[9].isEmpty() && "ERROR".equals(partes[9])) {
                            mensaje = "El predio " + partes[0] +
                                " tiene registrada una fecha recibido sin un formato valido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        /*if (!esNumeroRadicacionValido(partes[10], usuario)) {
                            mensaje = "El predio " + partes[0] +
                                " no tiene un formato de número de radicación válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else if (!partes[10].isEmpty() && !(partes[10].isEmpty()) && (partes[10].
                            length() == 16 || partes[10].length() == 18)) {

                            m = p.matcher(partes[10]);
                            if (!m.find()) {
                                mensaje = "El predio " + partes[0] +
                                    " tiene  un  formato de radicado que no corresponde a una Externa Recibida, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }*/

                        validarDptoMuni = false;
                        if (!partes[0].isEmpty()) {
                            validarDptoMuni = validarDepartamentoMunicipio(partes[0]);
                        }

                        if (validarDptoMuni == false) {
                            mensaje = "Usted está intentando marcár un predio que se" +
                                 " encuentra fuera de su jurisdicción. " +
                                 " El predio " +
                                 partes[0] +
                                 " no se marcára.";

                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            contadorErrores++;
                        }

                        String numeroPredial = partes[0];

                        Predio predio = this.getTramiteService().buscaPredio(numeroPredial);

                        boolean cancelado = this.getConservacionService().
                            esPredioCanceladoPorPredioIdONumeroPredial(predio.getId(), numeroPredial);

                        if (cancelado) {

                            mensaje = "Usted está intentando márcar un predio que se" +
                                 " encuentra cancelado. " +
                                 " El predio " +
                                 partes[0] +
                                 " no se marcára.";

                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            contadorErrores++;
                        }

                        if (contadorErrores == 0) {

                            pb = new PredioBloqueo();
                            predioTemp = new Predio();

                            predioTemp.setNumeroPredial(partes[0]);
                            pb.setPredio(predioTemp);

                            entidad = this.validarEntidadBloqueo(partes[0], partes[4], partes[2].
                                trim().concat(partes[3].trim()));
                            pb.setEntidadId(entidad.getId());

                            pb.setMotivoBloqueo(partes[6]);
                            pb.setFechaInicioBloqueo(formatoFecha.parse(partes[7]));
                            pb.setFechaRecibido(formatoFecha.parse(partes[9]));
                            pb.setNumeroRadicacion(partes[10]);

                            pb.setFechaEnvio(formatoFecha.parse(partes[8]));
                            pb.setRestitucionTierras(partes[5]);
                            pb.setTipoBloqueo(partes[1]);

                            dep = new Departamento();
                            dep.setCodigo(partes[2].trim());
                            mun = new Municipio();
                            mun.setCodigo(partes[2].trim().concat(partes[3].trim()));

                            pb.setDepartamento(dep);
                            pb.setMunicipio(mun);

                            if (partes.length == 12 && !partes[11].isEmpty() && !"ERROR".equals(
                                partes[11])) {
                                pb.setFechaTerminaBloqueo(formatoFecha.parse(partes[11]));
                            }

                            nuevoDocumentoAnexo();
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_PLANO
                                    .getCodigo());
                            this.documentoArchivoAnexo.setArchivo(eventoMasivo
                                .getFile().getFileName());

                            this.bloqueoMasivo.add(pb);
                        }

                    }//If principal

                    if (contadorErrores != 0 && this.erroresPredios == false) {
                        this.erroresPredios = true;
                        filaReporte++;
                    } else if (contadorErrores != 0) {
                        filaReporte++;
                    }

                    contadorErrores = 0;
                    columnaReporte = 0;

                }// While

                if (this.erroresPredios == false) {
                    mensaje = "El documento de marcaciónn masiva se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                    this.aBMasa = 1;
                    this.bloquearPredios = false;
                } else {
                    mensaje =
                        "El formato del documento soporte de marcación no es válido, verifique.";
                    this.addMensajeError(mensaje);
                    this.aBMasa = 0;
                    this.bloquearPredios = true;
                }

            } else if (eventoMasivo.getFile().getContentType()
                .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                 eventoMasivo
                    .getFile()
                    .getContentType()
                    .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                        .getValor()) ||
                 eventoMasivo
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                            .getExtension()) ||
                 eventoMasivo
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                            .getExtension())) {

                FileInputStream fis = new FileInputStream(
                    this.archivoMasivoResultado);
                XSSFWorkbook workbook = new XSSFWorkbook(fis);
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rowsX = sheet.rowIterator();

                while (rowsX.hasNext()) {
                    XSSFRow row = (XSSFRow) rowsX.next();

                    c = 0;
                    part = new Object[12];
                    fila = sheetReporte.createRow((short) filaReporte);
                    posEmpiezanFechas = 0;

                    for (short colIndex = 0; colIndex <= 11; colIndex++) {

                        XSSFCell cell = row.getCell(colIndex);

                        if (posEmpiezanFechas != 7 && posEmpiezanFechas != 8 && posEmpiezanFechas !=
                            9 && posEmpiezanFechas != 11 && cell != null && cell.getCellType() !=
                            XSSFCell.CELL_TYPE_BLANK) {
                            try {
                                part[c] = cell.getStringCellValue();
                            } catch (Exception ex) {

                                part[c] = null;
                            }
                        } else {
                            try {

                                if ((cell != null && posEmpiezanFechas != 11) || (cell != null &&
                                    posEmpiezanFechas == 11 && cell.getCellType() !=
                                    XSSFCell.CELL_TYPE_BLANK)) {

                                    if (cell.getCellType() == 0) {
                                        part[c] = cell.getDateCellValue();
                                    } else {
                                        part[c] = cell;
                                        fechaTemp = formatoFecha.parse(part[c].toString().trim());
                                        part[c] = fechaTemp;
                                    }
                                }
                            } catch (Exception e) {

                                if (posEmpiezanFechas == 11) {
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrada una fecha final valida, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }
                                part[c] = null;
                            }
                        }

                        if ((part[c] == null || part[c].toString().trim().isEmpty()) &&
                            posEmpiezanFechas != 11) {
                            switch (c) {
                                case 0:
                                    mensaje =
                                        "Un predio no tiene registrado un número predial, verifique";
                                    break;
                                case 1:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrado un tipo de marcación valido, verifique";
                                    break;
                                case 2:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrado un departamento valido, verifique";
                                    break;
                                case 3:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrado un municipio valido, verifique";
                                    break;
                                case 4:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrado una entidad ordena la marcación valido, verifique";
                                    break;
                                case 5:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrado un valor de restitución de tierras valido, verifique";
                                    break;
                                case 6:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrado un motivo de marcación valido, verifique";
                                    break;
                                case 7:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrada una fecha inicial valido, verifique";
                                    break;
                                case 8:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrada una fecha envió valido, verifique";
                                    break;
                                case 9:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene registrada una fecha recibido valido, verifique";
                                    break;
                                case 10:
                                    mensaje = "El predio " + part[0].toString().trim() +
                                        " no tiene un formato número de radicación valido, verifique";
                                    break;
                                default:
                                    mensaje = "";
                                    break;
                            }

                            if (!"".equals(mensaje)) {
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }
                        posEmpiezanFechas++;
                        c++;
                    }

                    if (part[0] != null && part[0].toString().trim().length() != 30) {
                        mensaje = "El predio " + part[0].toString().trim() +
                            " el número predial no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    } else if (part[0] != null && !(part[0].toString().trim().isEmpty()) && part[0].
                        toString().trim().length() == 30) {

                        if (this.getTramiteService().buscaPredio(part[0].toString().trim()) == null) {
                            mensaje = "El predio " + part[0].toString().trim() +
                                ", no existe en la base catastral,  Verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[1] != null && !(EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo().
                        toString().equals(part[1].toString().trim())) &&
                        !(EPredioBloqueoTipoBloqueo.SUSPENDER.getCodigo().equals(part[1].toString().
                            trim()))) {
                        mensaje = "El predio " + part[0].toString().trim() +
                            " el tipo de marcación no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    }

                    if (part[2] != null && (part[2].toString().trim().length()) != 2) {
                        mensaje = "Para el predio " + part[0].toString().trim() +
                            ", el código de departamento no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    } else if (part[2] != null) {
                        if (this.getConservacionService().validaExistenciaEntidad(null,
                            EEntidadEstado.ACTIVO.getEstado(), part[2].toString().trim(), null) ==
                            false) {
                            mensaje = "Para el predio " + part[0].toString().trim() +
                                ", el departamento no tiene  entidades que puedan realizar la marcación de predios, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[3] != null && part[3].toString().trim().length() != 3) {

                        mensaje = "Para el predio " + part[0].toString().trim() +
                            " el código de municipio no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;

                    } else if (part[2] != null && (part[2].toString().trim().length()) == 2) {

                        if (this.getGeneralesService().validaMunicipioDepartamento(part[2].
                            toString().trim(), part[2].toString().trim().concat(part[3].toString().
                                trim())) == false) {
                            mensaje = "Para el predio " + part[0].toString().trim() +
                                " el código de municipio no corresponde al departamento asociado”";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else {
                            String codMun = (part[3].toString().trim().length() == 3) ? part[2].
                                toString() + part[3].toString() : part[3].toString();
                            if (!this.getConservacionService().validaExistenciaEntidad(part[4].
                                toString().trim(), EEntidadEstado.ACTIVO.getEstado(), part[2].
                                toString().trim(), codMun)) {
                                mensaje = " Para el predio " + part[0].toString().trim() +
                                    ", el municipio no tiene entidades que puedan realizar la marcación de predios, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            } else if (part[4] != null &&
                                 !this.getConservacionService().validaExistenciaEntidad(part[4].
                                    toString().trim(), EEntidadEstado.ACTIVO.getEstado(), part[2].
                                    toString().trim(), codMun)) {
                                mensaje = " La entidad registrada para el predio " + part[0].
                                    toString().trim() +
                                    ", no está parametrizada para realizar la marcación de predios, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }
                    }

                    if (part[5] != null && !"SI".equals(part[5].toString().trim()) && !"NO".equals(
                        part[5].toString().trim())) {
                        mensaje = "El predio " + part[0].toString().trim() +
                            " el dato restitución de tierras no tiene un formato válido, tiene registrado un valor diferente a SI o NO, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    }

                    if (part[7] != null && part[11] != null) {

                        fechatemporal1 = (Date) part[7];
                        fechatemporal2 = (Date) part[11];

                        if (fechatemporal2.before(fechatemporal1)) {
                            mensaje = "Para el predio identificado con número predial " +
                                 part[0].toString().trim() +
                                 " la fecha final no puede ser menor a la fecha inicial" +
                                 " por lo que este predio no se incluirá para ser marcado.";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[9] != null) {

                        fechatemporal1 = (Date) part[9];
                        fechatemporal2 = new Date();

                        if (fechatemporal1.after(fechatemporal2)) {
                            mensaje = "En el predio " + part[0].toString().trim() +
                                
                                " La fecha de recibido no puede ser mayor a la fecha actual, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[8] != null && part[9] != null) {

                        fechatemporal1 = (Date) part[8];
                        fechatemporal2 = (Date) part[9];

                        if (fechatemporal2.before(fechatemporal1)) {
                            mensaje = "En el predio " + part[0].toString().trim() +
                                
                                " La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[10] != null && !esNumeroRadicacionValido(part[10].toString(), usuario)) {
                        mensaje = "El predio " +
                             part[0].toString().trim() +
                             " no tiene un formato de número de radicación válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    }

                    validarDptoMuni = false;
                    if (part[0] != null) {
                        validarDptoMuni = validarDepartamentoMunicipio(part[0].toString().trim());
                    }

                    if (validarDptoMuni == false) {

                        mensaje = "Usted está intentando marcár un predio que se" +
                             " encuentra fuera de su jurisdicción. " +
                             " El predio " +
                             part[0].toString().trim() +
                             " no se marcára.";

                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        contadorErrores++;
                    }

                    String numeroPredial = part[0].toString();

                    Predio predio = this.getTramiteService().buscaPredio(numeroPredial);

                    if (predio != null) {
                        boolean cancelado = this.getConservacionService().
                            esPredioCanceladoPorPredioIdONumeroPredial(predio.getId(), numeroPredial);
                        if (cancelado) {

                            mensaje = "Usted está intentando márcar un predio que se" +
                                 " encuentra cancelado. " +
                                 " El predio " +
                                 part[0] +
                                 " no se bloqueará.";

                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            contadorErrores++;
                        }
                    } else {
                        mensaje = "El predio con número predial " + numeroPredial +
                            " no se encuentra en el sistema";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        contadorErrores++;
                    }
                    if (contadorErrores == 0) {

                        c = 0;
                        pb = new PredioBloqueo();
                        predioTemp = new Predio();

                        predioTemp.setNumeroPredial(part[0].toString().trim());
                        pb.setPredio(predioTemp);

                        // Validación de la entidad ingresada
                        entidad = this.validarEntidadBloqueo(part[0].toString().trim(), part[4].
                            toString().trim(), part[2].toString().trim().concat(part[3].toString().
                                trim()));
                        pb.setEntidadId(entidad.getId());

                        pb.setMotivoBloqueo(part[6].toString().trim());
                        pb.setFechaInicioBloqueo((Date) part[7]);
                        pb.setFechaRecibido((Date) part[9]);
                        pb.setNumeroRadicacion(part[10].toString().trim());

                        pb.setFechaEnvio((Date) part[8]);
                        pb.setRestitucionTierras(part[5].toString().trim());
                        pb.setTipoBloqueo(part[1].toString().trim());

                        dep = new Departamento();
                        dep.setCodigo(part[2].toString().trim());
                        mun = new Municipio();
                        mun.setCodigo(part[2].toString().trim().concat(part[3].toString().trim()));

                        pb.setDepartamento(dep);
                        pb.setMunicipio(mun);

                        if (part[11] != null && !"".equals(part[11].toString().trim())) {
                            pb.setFechaTerminaBloqueo((Date) part[11]);
                        }

                        nuevoDocumentoAnexo();
                        this.documentoArchivoAnexo
                            .setFormato(eventoMasivo.getFile()
                                .getContentType());
                        if (eventoMasivo
                            .getFile()
                            .getContentType()
                            .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                .getValor()) ||
                             eventoMasivo
                                .getFile()
                                .getFileName()
                                .endsWith(
                                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                        .getExtension())) {
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                    .getCodigo());
                        } else {
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                                    .getCodigo());
                        }
                        this.documentoArchivoAnexo.setArchivo(eventoMasivo
                            .getFile().getFileName());

                        this.bloqueoMasivo.add(pb);

                    }

                    if (contadorErrores != 0 && this.erroresPredios == false) {
                        this.erroresPredios = true;
                        filaReporte++;
                    } else if (contadorErrores != 0) {
                        filaReporte++;
                    }

                    contadorErrores = 0;
                    columnaReporte = 0;

                }//While

                if (this.erroresPredios == false) {
                    mensaje = "El documento de marcación masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                    this.aBMasa = 1;
                    this.bloquearPredios = false;
                } else {
                    mensaje =
                        "El formato del documento soporte de marcación no es válido, verifique.";
                    this.addMensajeInfo(mensaje);
                    this.aBMasa = 0;
                    this.bloquearPredios = true;
                }
            }// Else if

            String excelFileName = "reporte.csv";
            FileOutputStream fos = new FileOutputStream(excelFileName);
            wb.write(fos);
            fos.flush();
            fos.close();
            InputStream stream = new BufferedInputStream(new FileInputStream(
                excelFileName));
            exportFile = new DefaultStreamedContent(stream, "application/xls",
                excelFileName);

        } catch (Exception e) {
            this.LOGGER.error(e.getMessage(), e);
            this.aBMasa = 0;
            this.nombreDocumentoBloqueoMasivo = null;
            mensaje = "Ocurrio un error al leer el archivo!";
            this.addMensajeError(mensaje);
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado al encontrar un error en el formato de fecha en un archivo de bloqueo masivo
     */
    @SuppressWarnings("unused")
    private void errorFormatoFecha(String numeroPredial, String fechaConError) {

        this.aBMasa = 0;
        this.nombreDocumentoBloqueoMasivo = null;
        String mensaje = "Para el predio identificado con número predial " +
             numeroPredial + " la fecha ingresada '" + fechaConError +
             "' no contienen el formato dd/MM/yyyy";
        this.addMensajeError(mensaje);

    }

    private boolean validarDepartamentoMunicipio(String numeroPredial) {

        boolean departamentoCoincidente = false;
        boolean municipioCoincidente = false;

        if (this.departamentos != null && !this.departamentos.isEmpty()) {
            for (Departamento dTemp : this.departamentos) {

                if (dTemp.getCodigo().equals(numeroPredial.substring(0, 2))) {
                    departamentoCoincidente = true;
                    break;
                }
            }
        } else {
            departamentoCoincidente = true;
        }

        if (this.municipios != null && !this.municipios.isEmpty()) {
            for (Municipio mTemp : this.municipios) {

                if (mTemp.getCodigo3Digitos().equals(
                    numeroPredial.substring(2, 5))) {
                    municipioCoincidente = true;
                    break;
                }
            }
        } else {
            municipioCoincidente = true;
        }

        if (departamentoCoincidente == true && municipioCoincidente == true) {
            return true;
        } else {
            return false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de bloqueo masivo a cargar
     */
    private void nuevoDocumentoAnexo() {

        TipoDocumento tipoDocumento = new TipoDocumento();
        this.documentoArchivoAnexo = new DocumentoArchivoAnexo();

        tipoDocumento
            .setId(ETipoDocumentoId.DOC_BLOQUEO_MASIVO_PERSONA.getId());

        this.documentoArchivoAnexo
            .setDescripcion(ETipoDocumentoId.DOC_BLOQUEO_MASIVO_PREDIO
                .toString());
        this.documentoArchivoAnexo.setDocumento(this.documentoSoporteBloqueo);
        this.documentoArchivoAnexo
            .setEstado(EDocumentoArchivoAnexoEstado.CARGADO.getCodigo());
        this.documentoArchivoAnexo.setFechaLog(new Date());
        this.documentoArchivoAnexo.setUsuarioLog(this.usuario.getLogin());
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Action del botón "validar". Metodo utilizado para validar los datos de la forma de datos del
     * bloqueo
     */
    public void validar() {

        boolean validacionFiltroRangoPredios;
        Date fechaActualSinMills = DateUtils.round(
            generalService.getFechaActual(), Calendar.DAY_OF_MONTH);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(fechaActualSinMills);
        gc.add(Calendar.DAY_OF_YEAR, -1);
        fechaActualSinMills = gc.getTime();
        this.laBandera = true;

        if (this.tipoBloqueo0 == true) {

            this.validarTipoBloqueo0();

        } else if (this.tipoBloqueo1 == true) {

            validacionFiltroRangoPredios = validacionFiltroRangoPredios();

            if (validacionFiltroRangoPredios == true) {
                this.prediosSeleccionados = this
                    .getConservacionService()
                    .buscarRangoPrediosValidosPorFiltrosDatosConsultaPrediosBloqueo(
                        this.filtroCPredioB, this.filtroFinalCPredioB);

                if (this.prediosSeleccionados != null &&
                     !this.prediosSeleccionados.isEmpty()) {

                    if (this.prediosSeleccionados.isEmpty()) {
                        this.banderaValidar = false;
                        this.laBandera = false;
                        String mensaje =
                            "No existe ningún predio con los números prediales ingresados" +
                             " en el departamento y municipio seleccionados." +
                             " Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                    } else {
                        this.banderaValidar = true;
                        this.laBandera = true;
                    }
                } else {
                    String mensaje =
                        "No existe ningún predio con los números prediales ingresados" +
                         " en el departamento y municipio seleccionados." +
                         " Por favor corrija los datos e intente nuevamente.";
                    this.addMensajeError(mensaje);
                    this.banderaValidar = false;
                    this.laBandera = false;
                }
            } else {
                this.banderaValidar = false;
                this.laBandera = false;
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida los datos para el tipo de bloqueo 0
     */
    private boolean validarTipoBloqueo0() {

        boolean departamentoCoincidente = false;
        boolean municipioCoincidente = false;
        Date fechaSistema = new Date();

        if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
             this.filtroCPredioB.getDepartamentoCodigo().isEmpty() ||
             this.filtroCPredioB.getMunicipioCodigo() == null ||
             this.filtroCPredioB.getMunicipioCodigo().isEmpty() ||
             this.filtroCPredioB.getTipoAvaluo() == null ||
             this.filtroCPredioB.getTipoAvaluo().isEmpty() ||
             this.filtroCPredioB.getSectorCodigo() == null ||
             this.filtroCPredioB.getSectorCodigo().isEmpty() ||
             this.filtroCPredioB.getBarrioCodigo() == null ||
             this.filtroCPredioB.getBarrioCodigo().isEmpty() ||
             this.filtroCPredioB.getManzanaVeredaCodigo() == null ||
             this.filtroCPredioB.getManzanaVeredaCodigo().isEmpty() ||
             this.filtroCPredioB.getPredio() == null ||
             this.filtroCPredioB.getPredio().isEmpty() ||
             this.filtroCPredioB.getCondicionPropiedad() == null ||
             this.filtroCPredioB.getCondicionPropiedad().isEmpty() ||
             this.filtroCPredioB.getEdificio() == null ||
             this.filtroCPredioB.getEdificio().isEmpty() ||
             this.filtroCPredioB.getPiso() == null ||
             this.filtroCPredioB.getPiso().isEmpty() ||
             this.filtroCPredioB.getUnidad() == null ||
             this.filtroCPredioB.getUnidad().isEmpty()) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Para marcár un predio debe incluir todos los campos" +
                 " del número predial. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
            return false;
        }

        //Validar número de radicación
        if (!esNumeroRadicacionValido(numeroRadicado, usuario)) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "El número de radicado no se encontró en el sistema. Por favor revise.";
            this.addMensajeError(mensaje);
        }

        for (Departamento dTemp : this.departamentos) {

            if (dTemp.getCodigo().equals(
                this.filtroCPredioB.getDepartamentoCodigo())) {
                departamentoCoincidente = true;
                break;
            }
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Usted está intentando marcár un predio que corresponde" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
            return false;
        }

        for (Municipio mTemp : this.municipios) {

            if (mTemp.getCodigo3Digitos().equals(
                this.filtroCPredioB.getMunicipioCodigo())) {
                municipioCoincidente = true;
                break;
            }
        }

        if (municipioCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Usted está intentando marcár un predio que corresponde" +
                 " a un municipio fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
            return false;
        }

        if (this.fechaFinal != null &&
             this.fechaFinal.before(this.fechaInicial)) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "La fecha final no puede ser menor a la fecha inicial." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.fechaEnvio != null &&
             fechaSistema.before(this.fechaEnvio)) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Fecha envío no puede ser mayor a la fecha actual, verifique.";
            this.addMensajeError(mensaje);
            return false;
        }
        if (this.fechaRecibido != null &&
             fechaSistema.before(this.fechaRecibido)) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "La fecha de recibido no puede ser mayor a la fecha actual, verifique.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.fechaRecibido != null && this.fechaEnvio != null &&
             this.fechaRecibido.before(this.fechaEnvio)) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje =
                "La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (this.laBandera == true) {
            if (this.tipoBloqueo0 == true) {
                this.prediosSeleccionados = this
                    .getConservacionService()
                    .buscarPredioValidoPorFiltroDatosConsultaPrediosBloqueo(
                        this.filtroCPredioB);

                if (this.prediosSeleccionados != null) {

                    if (this.prediosSeleccionados.size() == 0) {
                        this.banderaValidar = false;
                        this.laBandera = false;
                        String mensaje = "No existe ningún predio con el número predial ingresado" +
                             " en el departamento y municipio seleccionados." +
                             " Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        return false;
                    } else if (this.prediosSeleccionados.size() > 1) {
                        this.banderaValidar = false;
                        this.laBandera = false;
                        String mensaje =
                            "Existen múltiples predios con el número predial ingresado" +
                             " en el departamento y municipio seleccionados." +
                             " Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        return false;
                    }

                }
            }
        }
        return true;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida los datos para el bloqueo masivo
     */
    private boolean validarTipoBloqueoMasivo(PredioBloqueo predioBloqueoTemp) {

        if (predioBloqueoTemp.getFechaTerminaBloqueo() != null &&
             predioBloqueoTemp.getFechaTerminaBloqueo().before(
                predioBloqueoTemp.getFechaInicioBloqueo())) {
            this.banderaValidar = false;
            this.laBandera = false;
            String mensaje = "Para el predio identificado con número predial" +
                 predioBloqueoTemp.getPredio().getNumeroPredial() +
                 " la fecha final no puede ser menor a la fecha inicial" +
                 " por lo que este predio no se incluirá para ser marcado.";
            this.addMensajeError(mensaje);
            return false;
        }

        return true;
    }

    /**
     * Metodo utilizado para validar sí un predio tiene tramites asociados a un proceso dentro del
     * subproceso de Validacion Tipo BloqueoMasivo TBM
     */
    private boolean validarSubProcesoValidacion(PredioBloqueo predioBloqueoTemp,
        String nombreEntidad, String correoEntidad) {

        Actividad act;
        Map filtros;
        Boolean resultado = false;
        List<Actividad> actividades;
        Locale colombia = new Locale("ES", "es_CO");
        Object parametrosContenidoEmail[] = new Object[9];
        Parametro correoRestitucionTierras;
        Plantilla plantillaTierras;
        Plantilla plantillaentidad;
        String contenidoRestitucion = "";
        String contenidoEntidad = "";

        plantillaTierras = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_RESTITUCION_TIERRAS_BLOQUEO_RESOLUCION.getCodigo());

        plantillaentidad = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_ENTIDAD_BLOQUEO_RESOLUCION.getCodigo());

        if (null != plantillaTierras) {
            contenidoRestitucion = plantillaTierras.getHtml();
            if (null == contenidoRestitucion) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de restitución de tierras");
            }
        }

        if (null != plantillaentidad) {
            contenidoEntidad = plantillaentidad.getHtml();
            if (null == contenidoEntidad) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        for (Tramite tram : this.tramites) {

            if (!tram.getEstado().equals(ETramiteEstado.ARCHIVADO.getCodigo()) && !tram.getEstado().
                equals(ETramiteEstado.CANCELADO.getCodigo()) &&
                 !tram.getEstado().equals(ETramiteEstado.RECHAZADO.toString()) &
                 !tram.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.toString())) {

                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION, tram.
                    getNumeroRadicacion());
                filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, String.valueOf(tram.
                    getId()));
                actividades = this.getProcesosService().consultarListaActividades(filtros);

                if (actividades != null && !actividades.isEmpty()) {

                    act = actividades.get(0);
                    if (!act.getEstado().equals(EEstadoActividad.SUSPENDIDA.toString())) {

                        if (act.getNombre().equals(
                            ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {

                            if (tram.getResultadoDocumento() != null && !tram.
                                getResultadoDocumento().getNumeroDocumento().isEmpty()) {

                                correoRestitucionTierras = this.getGeneralesService().
                                    getCacheParametroPorNombre(
                                        EParametro.CORREO_RESTITUCION_TIERRAS.toString());

                                parametrosContenidoEmail[0] = nombreEntidad;
                                parametrosContenidoEmail[1] = predioBloqueoTemp.getPredio().
                                    getNumeroPredial();
                                parametrosContenidoEmail[2] = tramites.get(0).getMunicipio().
                                    getNombre();
                                parametrosContenidoEmail[3] = tramites.get(0).getDepartamento().
                                    getNombre();
                                parametrosContenidoEmail[4] = tram.getTipoTramiteCadenaCompleto();
                                parametrosContenidoEmail[5] = tram.getNumeroRadicacion();
                                parametrosContenidoEmail[6] = tram.getSolicitud().getSolicitante().
                                    getNombreCompleto();
                                parametrosContenidoEmail[7] = tram.getResultadoDocumento().
                                    getNumeroDocumento();
                                parametrosContenidoEmail[8] = tram.getResultadoDocumento().
                                    getFechaDocumento();

                                contenidoRestitucion = new MessageFormat(contenidoRestitucion,
                                    colombia).format(parametrosContenidoEmail);
                                contenidoEntidad = new MessageFormat(contenidoEntidad, colombia).
                                    format(parametrosContenidoEmail);

                                enviarCorreo(correoRestitucionTierras.getValorCaracter(),
                                    contenidoRestitucion);
                                enviarCorreo(correoEntidad, contenidoEntidad);

                                resultado = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return resultado;

    }

    /**
     * Metodo utilizado para identificar los tramites que esten asociados a un predio y suspender
     * los respectivos procesos de los tramites dentro de los subprocesos de Validacion, ejecución,
     * asignación, depuración
     */
    private boolean bloqueoProcesos(PredioBloqueo predioBloqueoTemp,
        String nombreEntidad, String correoEntidad) {

        Actividad act;
        Map filtros;
        Boolean resultado = false;
        List<Actividad> actividades;
        Locale colombia = new Locale("ES", "es_CO");
        Object parametrosContenidoEmail[] = new Object[5];
        Parametro correoRestitucionTierras;
        Parametro nombreRestitucionTierras;

        String cuerpoCorreo = "";

        String contenidoRestitucion = "";
        String contenidoEntidad = "";
        String contenidoPieDetalleCorreo = "";
        String contenidoDetalleCorreo = "";
        String contenidoPlantilla;

        Plantilla plantillaentidad;
        Plantilla pieDetalleCorreo;
        Plantilla detalleCorreo;

        Calendar cal = Calendar.getInstance();

        if (predioBloqueoTemp.getFechaTerminaBloqueo() != null) {
            cal.setTime(predioBloqueoTemp.getFechaTerminaBloqueo());
        } else {
            cal.setTime(predioBloqueoTemp.getFechaInicioBloqueo());
            cal.add(Calendar.DATE, Constantes.ANIO_SUSPENSION);
        }

        plantillaentidad = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_ENTIDAD_BLOQUEO_SUSPENDER_TRAMITE.getCodigo());

        pieDetalleCorreo = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_PIE_BLOQUEO_SUSPENDER_TRAMITE.getCodigo());

        detalleCorreo = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_DETALLE_BLOQUEO_SUSPENDER_TRAMITE.getCodigo());

        if (null != plantillaentidad) {
            contenidoEntidad = plantillaentidad.getHtml();
            contenidoRestitucion = plantillaentidad.getHtml();

            if (null == contenidoEntidad) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        if (null != pieDetalleCorreo) {
            contenidoPieDetalleCorreo = pieDetalleCorreo.getHtml();
            if (null == contenidoPieDetalleCorreo) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        if (null != detalleCorreo) {
            contenidoDetalleCorreo = detalleCorreo.getHtml();
            if (null == contenidoDetalleCorreo) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        correoRestitucionTierras = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.CORREO_RESTITUCION_TIERRAS.toString());
        nombreRestitucionTierras = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.NOMBRE_RESTITUCION_TIERRAS.toString());

        for (Tramite tram : this.tramites) {
            if (!tram.getEstado().equals(ETramiteEstado.ARCHIVADO.getCodigo()) &&
                 !tram.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo()) &&
                 !tram.getEstado().equals(ETramiteEstado.RECHAZADO.toString()) &&
                 !tram.getEstado().equals(ETramiteEstado.FINALIZADO_APROBADO.toString())) {

                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION, tram.
                    getNumeroRadicacion());
                filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD, String.valueOf(tram.
                    getId()));
                actividades = this.getProcesosService().consultarListaActividades(filtros);

                if (actividades != null && !actividades.isEmpty()) {

                    act = actividades.get(0);

                    if (!act.getEstado().equals(
                        EEstadoActividad.SUSPENDIDA.toString())) {

                        if ( // SUBPROCESO DE VALIDACION   
                            act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA) || //SUBPROCESO DE EJECUCIÓN 
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_AUTO_PRUEBA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_REVISAR_AUTO_PRUEBA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_APROBAR_COMISION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA_COMISIONAR) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_ALISTAR_INFORMACION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_CARGAR_AL_SNC) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_SOLICITAR_PRUEBA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_APROBAR_SOLICITUD_PRUEBA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE) || //SUBPROCESO DE ASIGNACIÓN    
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_DEVUELTO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO) || //SUBPROCESO DE DEPURACION
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_TERRENO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_DIGITALIZADOR) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_TOPOGRAFO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_EJECUTOR) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_POR_REALIZAR) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_EJECUCION_APROBAR_COMISION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION_EJECUTOR) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REVISAR_PREPARAR_INFO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REGISTRAR_VISITA_TERRENO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REGISTRAR_VISITA_TERRENO) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_INFORME_VISITA) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_EXP_AREA_IDENTIFICAR_PUNTOS_TOPOGRAFICOS) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR) || //SUBPROCESO DEPURACION                           
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA) || //SUBPROCESO CALIDAD DE DEPURACION                           
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) ||
                             act.getNombre().equals(
                                ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {

                            if (tram.getResultadoDocumento() == null) {

                                parametrosContenidoEmail[0] = tram.getTipoTramiteCadenaCompleto();
                                parametrosContenidoEmail[1] = tram.getNumeroRadicacion();
                                parametrosContenidoEmail[2] = tram.getSolicitud().getSolicitante().
                                    getNombreCompleto();

                                contenidoPlantilla = contenidoDetalleCorreo;
                                contenidoPlantilla =
                                    new MessageFormat(contenidoPlantilla, colombia).format(
                                        parametrosContenidoEmail);
                                cuerpoCorreo = cuerpoCorreo + contenidoPlantilla;

                                //Eliminar Replicas
                                if (act.getNombre().equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                                     act.getNombre().equals(
                                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                                     act.getNombre().equals(
                                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {

                                    this.getGeneralesService().
                                        borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                                            tram.getId(),
                                            ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());
                                    this.getGeneralesService().
                                        borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                                            tram.getId(), ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.
                                            getId());

                                }

                                resultado = true;
                                this.getProcesosService().suspenderActividad(
                                    act.getId(), cal, null);
                            }
                        }
                    }

                }
            }
        }

        if (resultado) {

            parametrosContenidoEmail[0] = nombreEntidad;
            parametrosContenidoEmail[1] = predioBloqueoTemp.getPredio().getNumeroPredial();
            parametrosContenidoEmail[2] = tramites.get(0).getMunicipio().getNombre();
            parametrosContenidoEmail[3] = tramites.get(0).getDepartamento().getNombre();

            contenidoEntidad = new MessageFormat(contenidoEntidad, colombia).format(
                parametrosContenidoEmail);
            enviarCorreo(correoEntidad, contenidoEntidad + cuerpoCorreo + contenidoPieDetalleCorreo);

            parametrosContenidoEmail[0] = nombreRestitucionTierras.getValorCaracter();
            contenidoRestitucion = new MessageFormat(contenidoRestitucion, colombia).format(
                parametrosContenidoEmail);
            enviarCorreo(correoRestitucionTierras.getValorCaracter(), (contenidoRestitucion +
                cuerpoCorreo + contenidoPieDetalleCorreo));

        }

        return resultado;

    }

// --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado validar los datos basicos de la busqueda por rangos
     */
    private boolean validacionFiltroRangoPredios() {

        boolean departamentoCoincidente = false;
        boolean municipioCoincidente = false;

        String mensaje = "Los números prediales tanto del número predial inicial como del número" +
             " predial final deben contener información en los campos hasta manzana / vereda." +
             " Por favor corrija los datos e intente nuevamente.";

        if (this.filtroCPredioB == null || this.filtroFinalCPredioB == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
             this.filtroFinalCPredioB.getDepartamentoCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getMunicipioCodigo() == null ||
             this.filtroFinalCPredioB.getMunicipioCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getTipoAvaluo() == null ||
             this.filtroFinalCPredioB.getTipoAvaluo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getSectorCodigo() == null ||
             this.filtroFinalCPredioB.getSectorCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getComunaCodigo() == null ||
             this.filtroFinalCPredioB.getComunaCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getBarrioCodigo() == null ||
             this.filtroFinalCPredioB.getBarrioCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        } else if (this.filtroCPredioB.getManzanaVeredaCodigo() == null ||
             this.filtroFinalCPredioB.getManzanaVeredaCodigo() == null) {
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getDepartamentoCodigo() != null && !this.filtroCPredioB
            .getDepartamentoCodigo().isEmpty()) ||
             (this.filtroFinalCPredioB.getDepartamentoCodigo() != null && !this.filtroFinalCPredioB
            .getDepartamentoCodigo().isEmpty())) {

            mensaje = "El número predial inicial y el número" +
                 " predial final deben pertenecer al mismo departamento." +
                 " Por favor corrija los datos e intente nuevamente.";

            if (this.filtroCPredioB.getDepartamentoCodigo() != null &&
                 !this.filtroCPredioB.getDepartamentoCodigo().isEmpty()) {
                if (this.filtroFinalCPredioB.getDepartamentoCodigo() == null ||
                     !this.filtroCPredioB.getDepartamentoCodigo().equals(
                        this.filtroFinalCPredioB
                            .getDepartamentoCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            } else if (this.filtroFinalCPredioB.getDepartamentoCodigo() != null &&
                 !this.filtroFinalCPredioB.getDepartamentoCodigo()
                    .isEmpty()) {
                if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
                     !this.filtroCPredioB.getDepartamentoCodigo().equals(
                        this.filtroFinalCPredioB
                            .getDepartamentoCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }
            }
        }

        if ((this.filtroCPredioB.getMunicipioCodigo() != null && !this.filtroCPredioB
            .getMunicipioCodigo().isEmpty()) ||
             (this.filtroFinalCPredioB.getMunicipioCodigo() != null && !this.filtroFinalCPredioB
            .getMunicipioCodigo().isEmpty())) {

            mensaje = "El número predial inicial y el número" +
                 " predial final deben pertenecer al mismo municipio." +
                 " Por favor corrija los datos e intente nuevamente.";

            if (this.filtroCPredioB.getMunicipioCodigo() != null &&
                 !this.filtroCPredioB.getMunicipioCodigo().isEmpty()) {
                if (this.filtroFinalCPredioB.getMunicipioCodigo() == null ||
                     !this.filtroCPredioB.getMunicipioCodigo().equals(
                        this.filtroFinalCPredioB.getMunicipioCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            } else if (this.filtroFinalCPredioB.getMunicipioCodigo() != null &&
                 !this.filtroFinalCPredioB.getMunicipioCodigo().isEmpty()) {
                if (this.filtroCPredioB.getMunicipioCodigo() == null ||
                     !this.filtroCPredioB.getMunicipioCodigo().equals(
                        this.filtroFinalCPredioB.getMunicipioCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            }
        }

        if ((this.filtroCPredioB.getTipoAvaluo() != null && !this.filtroCPredioB
            .getTipoAvaluo().isEmpty()) ||
             (this.filtroFinalCPredioB.getTipoAvaluo() != null && !this.filtroFinalCPredioB
            .getTipoAvaluo().isEmpty())) {

            mensaje = "El número predial inicial y el número" +
                 " predial final deben pertenecer a la misma zona." +
                 " Por favor corrija los datos e intente nuevamente.";

            if (this.filtroCPredioB.getTipoAvaluo() != null &&
                 !this.filtroCPredioB.getTipoAvaluo().isEmpty()) {
                if (this.filtroFinalCPredioB.getTipoAvaluo() == null ||
                     !this.filtroCPredioB.getTipoAvaluo().equals(
                        this.filtroFinalCPredioB.getTipoAvaluo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            } else if (this.filtroFinalCPredioB.getTipoAvaluo() != null &&
                 !this.filtroFinalCPredioB.getTipoAvaluo().isEmpty()) {
                if (this.filtroCPredioB.getTipoAvaluo() == null ||
                     !this.filtroCPredioB.getTipoAvaluo().equals(
                        this.filtroFinalCPredioB.getMunicipioCodigo())) {
                    this.addMensajeError(
                        "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                        mensaje);
                    return false;
                }

            }
        }

        if (this.tipoBloqueo1 == true) {

            if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
                 this.filtroCPredioB.getDepartamentoCodigo().isEmpty() ||
                 this.filtroCPredioB.getMunicipioCodigo() == null ||
                 this.filtroCPredioB.getMunicipioCodigo().isEmpty() ||
                 this.filtroCPredioB.getTipoAvaluo() == null ||
                 this.filtroCPredioB.getTipoAvaluo().isEmpty() ||
                 this.filtroCPredioB.getSectorCodigo() == null ||
                 this.filtroCPredioB.getSectorCodigo().isEmpty() ||
                 this.filtroCPredioB.getBarrioCodigo() == null ||
                 this.filtroCPredioB.getBarrioCodigo().isEmpty() ||
                 this.filtroCPredioB.getManzanaVeredaCodigo() == null ||
                 this.filtroCPredioB.getManzanaVeredaCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getDepartamentoCodigo() == null ||
                 this.filtroFinalCPredioB.getDepartamentoCodigo()
                    .isEmpty() ||
                 this.filtroFinalCPredioB.getMunicipioCodigo() == null ||
                 this.filtroFinalCPredioB.getMunicipioCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getTipoAvaluo() == null ||
                 this.filtroFinalCPredioB.getTipoAvaluo().isEmpty() ||
                 this.filtroFinalCPredioB.getSectorCodigo() == null ||
                 this.filtroFinalCPredioB.getSectorCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getBarrioCodigo() == null ||
                 this.filtroFinalCPredioB.getBarrioCodigo().isEmpty() ||
                 this.filtroFinalCPredioB.getManzanaVeredaCodigo() == null ||
                 this.filtroFinalCPredioB.getManzanaVeredaCodigo().isEmpty()) {
                this.banderaValidar = false;
                this.laBandera = false;
                mensaje = "Para marcár un rango de predios debe incluir los campos" +
                     " hasta manzana/vereda de los números prediales. " +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(
                    "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                    mensaje);
            }
        }

        if (this.filtroCPredioB.getDepartamentoCodigo() == null ||
             (this.filtroCPredioB.getDepartamentoCodigo() != null && this.filtroCPredioB
            .getDepartamentoCodigo().isEmpty()) ||
             this.filtroFinalCPredioB.getDepartamentoCodigo() == null ||
             (this.filtroFinalCPredioB.getDepartamentoCodigo() != null && this.filtroFinalCPredioB
            .getDepartamentoCodigo().isEmpty())) {

            mensaje = "El campo departamento debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getDepartamentoCodigo().equals(
            this.filtroFinalCPredioB.getDepartamentoCodigo())) {

            mensaje = "El campo departamento debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        for (Departamento dTemp : this.departamentos) {

            if (dTemp.getCodigo().equals(
                this.filtroCPredioB.getDepartamentoCodigo())) {
                departamentoCoincidente = true;
                break;
            }
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando marcár un rango de predios que corresponden" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
        }

        if (!this.filtroCPredioB.getMunicipioCodigo().equals(
            this.filtroFinalCPredioB.getMunicipioCodigo())) {

            mensaje = "El campo municipio debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        for (Municipio mTemp : this.municipios) {

            if (mTemp.getCodigo3Digitos().equals(
                this.filtroCPredioB.getMunicipioCodigo())) {
                municipioCoincidente = true;
                break;
            }
        }

        if (municipioCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando marcár un  rango de predios que corresponden" +
                 " a un municipio fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando marcár un rango de predios que corresponden" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
        }

        if (departamentoCoincidente == false) {
            this.banderaValidar = false;
            this.laBandera = false;
            mensaje = "Usted esta intentando marcár un rango de predios que corresponden" +
                 " a un departamento fuera de su jurisdicción. " +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosBnumeroPredialLabel",
                mensaje);
        }

        if (!this.filtroCPredioB.getTipoAvaluo().equals(
            this.filtroFinalCPredioB.getTipoAvaluo())) {

            mensaje = "El campo zona debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getSectorCodigo().equals(
            this.filtroFinalCPredioB.getSectorCodigo())) {

            mensaje = "El campo sector debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getComunaCodigo().equals(
            this.filtroFinalCPredioB.getComunaCodigo())) {

            mensaje = "El campo comuna debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getBarrioCodigo().equals(
            this.filtroFinalCPredioB.getBarrioCodigo())) {

            mensaje = "El campo barrio debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if (!this.filtroCPredioB.getMunicipioCodigo().equals(
            this.filtroFinalCPredioB.getMunicipioCodigo())) {

            mensaje = "El campo municipio debe coincidir" +
                 " tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        // comentado hasta definir si la manzana vereda es requerida o no
        /*
         * if (this.filtroCPredioB.getManzanaCodigo() == null ||
         * this.filtroFinalCPredioB.getManzanaCodigo() == null ||
         * (this.filtroCPredioB.getManzanaCodigo() != null && this.filtroCPredioB
         * .getManzanaCodigo().isEmpty()) || (this.filtroFinalCPredioB.getManzanaCodigo() != null &&
         * this.filtroFinalCPredioB .getManzanaCodigo().isEmpty())) {
         *
         * mensaje = "El campo manzana / vereda debe encontrarse" + " presente tanto en el número
         * predial inicial como en el número predial final." + " Por favor corrija los datos e
         * intente nuevamente."; this.addMensajeError(
         * "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial", mensaje); return false; }
         */
        if ((this.filtroCPredioB.getPredio() == null && this.filtroFinalCPredioB
            .getPredio() != null) ||
             (this.filtroCPredioB.getPredio() != null && this.filtroFinalCPredioB
            .getPredio() == null) ||
             (this.filtroCPredioB.getPredio() != null &&
             !this.filtroCPredioB.getPredio().isEmpty() && (this.filtroFinalCPredioB
            .getPredio() == null || (this.filtroFinalCPredioB
                .getPredio() != null && this.filtroFinalCPredioB
                .getPredio().isEmpty()))) ||
             (this.filtroFinalCPredioB.getPredio() != null &&
             !this.filtroFinalCPredioB.getPredio().isEmpty() && (this.filtroCPredioB
            .getPredio() == null || (this.filtroCPredioB
                .getPredio() != null && this.filtroCPredioB.getPredio()
                .isEmpty())))) {

            mensaje = "Si se ingresa el campo terreno este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getCondicionPropiedad() == null && this.filtroFinalCPredioB
            .getCondicionPropiedad() != null) ||
             (this.filtroCPredioB.getCondicionPropiedad() != null && this.filtroFinalCPredioB
            .getCondicionPropiedad() == null) ||
             (this.filtroCPredioB.getCondicionPropiedad() != null &&
             !this.filtroCPredioB.getCondicionPropiedad()
                .isEmpty() && (this.filtroFinalCPredioB
                .getCondicionPropiedad() == null || (this.filtroFinalCPredioB
                .getCondicionPropiedad() != null && this.filtroFinalCPredioB
                .getCondicionPropiedad().isEmpty()))) ||
             (this.filtroFinalCPredioB.getCondicionPropiedad() != null &&
             !this.filtroFinalCPredioB.getCondicionPropiedad()
                .isEmpty() && (this.filtroCPredioB
                .getCondicionPropiedad() == null || (this.filtroCPredioB
                .getCondicionPropiedad() != null && this.filtroCPredioB
                .getCondicionPropiedad().isEmpty())))) {
            mensaje = "Si se ingresa el campo condición de propiedad este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getEdificio() == null && this.filtroFinalCPredioB
            .getEdificio() != null) ||
             (this.filtroCPredioB.getEdificio() != null && this.filtroFinalCPredioB
            .getEdificio() == null) ||
             (this.filtroCPredioB.getEdificio() != null &&
             !this.filtroCPredioB.getEdificio().isEmpty() && (this.filtroFinalCPredioB
            .getEdificio() == null || (this.filtroFinalCPredioB
                .getEdificio() != null && this.filtroFinalCPredioB
                .getEdificio().isEmpty()))) ||
             (this.filtroFinalCPredioB.getEdificio() != null &&
             !this.filtroFinalCPredioB.getEdificio().isEmpty() && (this.filtroCPredioB
            .getEdificio() == null || (this.filtroCPredioB
                .getEdificio() != null && this.filtroCPredioB
                .getEdificio().isEmpty())))) {
            mensaje = "Si se ingresa el campo número del edificio este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getPiso() == null && this.filtroFinalCPredioB
            .getPiso() != null) ||
             (this.filtroCPredioB.getPiso() != null && this.filtroFinalCPredioB
            .getPiso() == null) ||
             (this.filtroCPredioB.getPiso() != null &&
             !this.filtroCPredioB.getPiso().isEmpty() && (this.filtroFinalCPredioB
            .getPiso() == null || (this.filtroFinalCPredioB
                .getPiso() != null && this.filtroFinalCPredioB
                .getPiso().isEmpty()))) ||
             (this.filtroFinalCPredioB.getPiso() != null &&
             !this.filtroFinalCPredioB.getPiso().isEmpty() && (this.filtroCPredioB
            .getPiso() == null || (this.filtroCPredioB.getPiso() != null && this.filtroCPredioB
            .getPiso().isEmpty())))) {
            mensaje = "Si se ingresa el campo número de piso este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        if ((this.filtroCPredioB.getUnidad() == null && this.filtroFinalCPredioB
            .getUnidad() != null) ||
             (this.filtroCPredioB.getUnidad() != null && this.filtroFinalCPredioB
            .getUnidad() == null) ||
             (this.filtroCPredioB.getUnidad() != null &&
             !this.filtroCPredioB.getUnidad().isEmpty() && (this.filtroFinalCPredioB
            .getUnidad() == null || (this.filtroFinalCPredioB
                .getUnidad() != null && this.filtroFinalCPredioB
                .getUnidad().isEmpty()))) ||
             (this.filtroFinalCPredioB.getUnidad() != null &&
             !this.filtroFinalCPredioB.getUnidad().isEmpty() && (this.filtroCPredioB
            .getUnidad() == null || (this.filtroCPredioB
                .getUnidad() != null && this.filtroCPredioB.getUnidad()
                .isEmpty())))) {
            mensaje = "Si se ingresa el campo número de unidad este debe encontrarse" +
                 " presente tanto en el número predial inicial como en el número predial final." +
                 " Por favor corrija los datos e intente nuevamente.";
            this.addMensajeError(
                "bloquearPredioTabView:bloqueoPrediosRdepartamentoNumPredial",
                mensaje);
            return false;
        }

        return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado al cerrar las ventanas modales
     */
    public void cerrarEsc(org.primefaces.event.CloseEvent event) {
        this.aBMasa = 0;
        this.aSoporte = 0;
        this.motivoBloqueo = "";
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar y bloquear otra predio
     */
    public void terminarYBloquearOtroP() {
        init();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar
     */
    public String terminar() {
        try {
            UtilidadesWeb.removerManagedBean("bloqueoPredio");
            return "index";
        } catch (Exception e) {
            this.addMensajeError("Error al cerrar la sesión");
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosArchivoBloqueo() {
        this.documentoSoporteBloqueo = new Documento();
        this.aSoporte = 0;
        this.motivoBloqueo = "";
        this.selectedTipoBloqueo = null;
        this.selectedRestitucionTierras = false;
        this.selectedDepartamento = "";
        this.selectedMunicipio = "";
        this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosArchivoBloqueoMasivo() {
        this.documentoSoporteBloqueo = new Documento();
        this.aSoporte = 0;
        this.motivoBloqueo = "";
        this.nombreDocumentoBloqueoMasivo = null;
        this.erroresPredios = false;
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado para cerrar cancelar trámites
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("bloqueoPredio");
        return "index";
    }

    // -------------------------------------------------------------------------
    /**
     * Método ejecutado para eliminar un predio de la lista de predios a bloquear
     *
     * @author juan.agudelo
     */
    public void removerPrediosABloquear() {

        this.prediosSeleccionados.remove(this.selectedPredioTemp);
        if (this.prediosSeleccionados.isEmpty()) {
            this.bloquearV();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización Se
     * adiciono un atributo tipoMimeDocumento para la clase de previsualización de documento y este
     * debe ser el que se consuma
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoTemporalAVisualizar
                .getArchivo());
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de finalizar los datos de documento temporal al cerrar la previsualización
     */
    public void cerrarDocumentoTemporalPrev() {
        // Si se queire hacer algo aca debe hacerse
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de la descarga de un archivo temporal de office
     */
    public void controladorDescargaArchivos() {

        if (this.documentoTemporalAVisualizar != null &&
             this.documentoTemporalAVisualizar.getArchivo() != null &&
             !this.documentoTemporalAVisualizar.getArchivo().isEmpty()) {

            File fileTemp = new File(FileUtils.getTempDirectory().getAbsolutePath(),
                this.documentoTemporalAVisualizar.getArchivo());
            InputStream stream;

            try {

                stream = new FileInputStream(fileTemp);
                cargarDocumentoTemporalPrev();
                this.fileTempDownload = new DefaultStreamedContent(stream,
                    this.tipoMimeDocTemporal,
                    this.documentoTemporalAVisualizar.getArchivo());

            } catch (FileNotFoundException e) {
                String mensaje = "Ocurrió un error al cargar al archivo." +
                     " Por favor intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }
    }

    // ------------------------------------------------------------------------
    /**
     * Método que se ejcuta sobre la accion del botón aceptar en la previsualización de documento de
     * soporte de bloqueo
     */
    public void cerrarVistaPreliminarDocumento() {
        // Si se desea hacer algo al aceptar debe hacerse aca
    }

    /**
     * Método que realiza la validación de entidades según su nombre, sin importar mayúsculas o
     * mínusculas.
     *
     * @author david.cifuentes
     */
    private Entidad validarEntidadBloqueo(String numeroPredial, String nombreEntidad,
        String codigoMunicipioEntidad) {

        Entidad entidad = null;
        try {

            if (nombreEntidad != null && !nombreEntidad.isEmpty()) {

                List<Entidad> entidadesBloqueo = this.getConservacionService()
                    .buscarEntidadesPorNombreYEstado(nombreEntidad, EEntidadEstado.ACTIVO.
                        getEstado());

                if (entidadesBloqueo != null && !entidadesBloqueo.isEmpty()) {
                    if (entidadesBloqueo.size() == 1) {
                        return entidadesBloqueo.get(0);
                    } else {
                        for (Entidad e : entidadesBloqueo) {
                            if (e.getMunicipio() != null &&
                                 e.getMunicipio().getCodigo().equals(codigoMunicipioEntidad)) {
                                return e;
                            }
                        }
                        return entidadesBloqueo.get(0);
                    }
                } else {
                    entidadesBloqueo = this.getConservacionService()
                        .buscarEntidadesPorNombreYEstado(nombreEntidad,
                            EEntidadEstado.INACTIVO.getEstado());
                    if (entidadesBloqueo != null && !entidadesBloqueo.isEmpty() &&
                         entidadesBloqueo.size() == 1) {
                        this.addMensajeError("La entidad '" + nombreEntidad +
                             "' se encuentra inactiva. El predio " +
                             numeroPredial + " no se marcara.");
                    } else {
                        this.addMensajeError("La entidad '" +
                             nombreEntidad +
                            
                            "' no se encuentra registrada, por favor verifique la información. El predio " +
                             numeroPredial + " no se marcara.");
                    }
                }
                return entidad;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error consultando las entidades");
        }
        return entidad;
    }

    /**
     * Metodo para actualizar los municipios
     *
     * @author dumar.penuela
     */
    public void actualizarMunicipiosBuscardorListener() {
        if (this.municipiosDeptos != null) {
            this.municipiosBuscadorItemList = this.municipiosDeptos.get(this.selectedDepartamento);
        }
        this.entidades = new ArrayList<SelectItem>();
    }

    /**
     * Metodo para cargar los departamentos
     *
     * @author dumar.penuela
     */
    public void cargarDepartamentos() {
        List<Municipio> municipiosList;
        List<SelectItem> municipiosItemListTemp;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

         List<Departamento> deptosList = (ArrayList<Departamento>) this .getGeneralesService()
         .getDepartamentoByCodigoEstructuraOrganizacional( this.usuario
										.getCodigoEstructuraOrganizacional()); 
        /*List<Departamento> deptosList = generalService.getDepartamentos(Constantes.COLOMBIA);
        this.cargarDepartamentosItemList(deptosList);*/

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService().
                obteneMunicipiosDepartamentoSinCatastroDescentralizado(
                    dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getCodigo() + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);
        }
    }

    /**
     * Metodo para constuir la lista de departamentos
     *
     * @author dumar.penuela
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {
        
        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        this.departamentosItemList.add(new SelectItem(null, DEFAULT_COMBOS));
        for (Departamento dapto : departamentosList) {
            if (!dapto.getCodigo().equals(Constantes.BOGOTA_CODIGO)) {
                this.departamentosItemList.add(new SelectItem(dapto.getCodigo(), dapto.getCodigo() +
                    "-" +
                     dapto.getNombre()));
            }
        }
    }

    public void cargarDeptosEntidadItemList(List<Departamento> departamentosList) {

        this.deptosEntidadItemList = new ArrayList<SelectItem>();

        if (!this.deptosEntidadItemList.isEmpty()) {
            this.deptosEntidadItemList.clear();
        }
        this.deptosEntidadItemList.add(new SelectItem(null, DEFAULT_COMBOS));
        for (final Departamento dapto : departamentosList) {
            this.deptosEntidadItemList.add(new SelectItem(dapto.getCodigo(),
                dapto.getCodigo() + "-" +
                 dapto.getNombre()));
        }
    }

    public void cargarEntidades() {
        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        List<Entidad> entidadesActivas = this.getConservacionService()
            .buscarEntidades(null, EEntidadEstado.ACTIVO.getEstado(),
                selectedDepartamento, selectedMunicipio);

        if (entidadesActivas != null && !entidadesActivas.isEmpty()) {
            for (Entidad e : entidadesActivas) {
                this.entidades.add(new SelectItem(e.getId(), e.getNombre()));
            }
        } else {
            Municipio d = this.getGeneralesService().getCacheMunicipioByCodigo(
                selectedMunicipio);
            this.addMensajeError("El municipio " +
                 d.getNombre() +
                 ", no tiene asociadas entidades que puedan realizar la marcación, verifique");
        }
    }

    /**
     * Método que envia un correo a la entidad que ordena el bloqueo y a la oficina de restitució de
     * tierras
     *
     * @author dumar.penuela
     * @param emailEntidad
     * @param contenido
     */
    public void enviarCorreo(String emailEntidad, String contenido) {
        try {

            this.getGeneralesService().enviarCorreo(
                emailEntidad,
                ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_BLOQUEO_PREDIO,
                contenido, null, null);
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico ");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeWarn("No se pudo enviar el informando la marcación del predio");
        }
    }

}
