package co.gov.igac.snc.web.controller.conservacion;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.vo.DetalleVisorVO;
import co.gov.igac.snc.vo.ErrorVO;
import co.gov.igac.snc.vo.EstadisticasRegistroVO;
import co.gov.igac.snc.vo.ListDetallesVisorVO;
import co.gov.igac.snc.vo.RespuestaVO;
import co.gov.igac.snc.web.controller.BaseController;

/**
 *
 * @author franz.gamba
 */
@Controller
@RequestMapping(value = "/consulta/**")
public class ConsultaController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 7231068459155081890L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneralController.class);

    /**
     * Muestra los detales de un Predio para el Visor WEB
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/predio/{codigo}")
    public String mostrarDatosVisor(ModelMap modelMap,
        @PathVariable String codigo) {

        if (codigo.length() < 17) {
            ErrorVO error = new ErrorVO("No es un código válido");
            modelMap.addAttribute("respuesta", error);
            return null;
        } else {

            List<Predio> predios = new ArrayList<Predio>();
            try {
                predios = this.getConservacionService().getPrediosPorCodigo(
                    codigo);
                if (predios != null) {
                    List<DetalleVisorVO> detalles = new ArrayList<DetalleVisorVO>();

                    for (Predio p : predios) {
                        DetalleVisorVO d = new DetalleVisorVO(p);
                        detalles.add(d);
                    }

                    ListDetallesVisorVO listaDetalles = new ListDetallesVisorVO();

                    listaDetalles.setListaDetalles(detalles);

                    modelMap.addAttribute("Detalles", listaDetalles);
                    return null;
                } else {
                    ErrorVO error = new ErrorVO(
                        "No hay resultados para este código");
                    modelMap.addAttribute("respuesta", error);
                    return null;
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                ErrorVO error = new ErrorVO("Error en el servidor");
                modelMap.addAttribute("respuesta", error);
                ;
                return null;
            }
        }
    }

    /**
     * Muestra los detales de un Predio para el Visor WEB segun su matricula inmobiliaria
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/predio/municipio/{municipioCod}/matricula/{matriculaInm}")
    public String mostrarDatosPredioPorMatricula(ModelMap modelMap,
        @PathVariable String municipioCod, @PathVariable String matriculaInm) {

        List<Predio> predios = new ArrayList<Predio>();
        try {
            predios = this.getConservacionService()
                .buscarPrediosPorMunicipioYMatricula(municipioCod,
                    matriculaInm);
            if (predios != null) {
                List<DetalleVisorVO> detalles = new ArrayList<DetalleVisorVO>();

                for (Predio p : predios) {
                    DetalleVisorVO d = new DetalleVisorVO(p);
                    detalles.add(d);
                }

                ListDetallesVisorVO listaDetalles = new ListDetallesVisorVO();

                listaDetalles.setListaDetalles(detalles);

                modelMap.addAttribute("Detalles", listaDetalles);
                return null;
            } else {
                ErrorVO error = new ErrorVO(
                    "No hay resultados para ese codigo de " +
                     "municipio y/o matricula inmobiliaria");
                modelMap.addAttribute("respuesta", error);
                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            ErrorVO error = new ErrorVO("Error en el servidor");
            modelMap.addAttribute("respuesta", error);
            return null;
        }

    }

    /**
     * Muestra los detales de un Predio para el Visor WEB segun su direccion
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/predio/municipio/{municipioCod}/direccion/{direccion}")
    public String mostrarDatosPredioPorDireccion(ModelMap modelMap,
        @PathVariable String municipioCod, @PathVariable String direccion) {

        List<Predio> predios = new ArrayList<Predio>();
        try {

            String decodedDir = URLDecoder.decode(direccion, "UTF-8");
            predios = this.getConservacionService()
                .buscarPrediosPorMunicipioYDireccion(municipioCod,
                    direccion);
            if (predios != null) {
                List<DetalleVisorVO> detalles = new ArrayList<DetalleVisorVO>();

                for (Predio p : predios) {
                    DetalleVisorVO d = new DetalleVisorVO(p);
                    detalles.add(d);
                }

                ListDetallesVisorVO listaDetalles = new ListDetallesVisorVO();

                listaDetalles.setListaDetalles(detalles);

                modelMap.addAttribute("Detalles", listaDetalles);
                return null;
            } else {
                ErrorVO error = new ErrorVO(
                    "No hay resultados para ese codigo de " +
                     "municipio y/o dirección");
                modelMap.addAttribute("respuesta", error);
                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            ErrorVO error = new ErrorVO("Error en el servidor");
            modelMap.addAttribute("respuesta", error);
            return null;
        }

    }

    @RequestMapping(value = "/predio/fachada/{codigo}")
    public String mostrarFachadaPredioPorId(ModelMap modelMap,
        @PathVariable String codigo) {

        Predio predio = this.getConservacionService().getPredioByNumeroPredial(codigo);
        if (predio != null) {
            try {
                List<String> fotos = this.getConservacionService()
                    .buscarFotosPredioPorComponenteDeConstruccion(
                        predio.getId(), null);
                if (fotos == null || fotos.isEmpty()) {
                    ErrorVO error = new ErrorVO("No se encontraron imágenes");
                    modelMap.addAttribute("respuesta", error);
                } else {
                    RespuestaVO respuesta = new RespuestaVO("Done", fotos);
                    modelMap.addAttribute("respuesta", respuesta);
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                ErrorVO error = new ErrorVO("Error en el servidor");
                modelMap.addAttribute("respuesta", error);
            }
        } else {
            ErrorVO error = new ErrorVO("Predio no encontrado");
            modelMap.addAttribute("respuesta", error);
        }
        return null;
    }
//--------------------------------------------------------------------------------------------------

    @RequestMapping(value = "/predio/numeros/{cod}")
    public String mostrarDetallesPrediosSeleccion(ModelMap modelMap,
        @RequestParam("codigos") String codigos) {

        RespuestaVO respuesta;
        ListDetallesVisorVO listaDetalles = new ListDetallesVisorVO();
        listaDetalles.setListaDetalles(new ArrayList<DetalleVisorVO>());

        try {
            listaDetalles = this.getConservacionService().
                buscarPrediosPorNumerosPredialesVisor(codigos);
            if (listaDetalles == null) {
                respuesta = new RespuestaVO(
                    "El valor de la selección no existe");
                modelMap.addAttribute("Respuesta", respuesta);
                return null;
            } else {
                //aca se hace la traduccion del PREDIO_DESTINO
                List<Dominio> dominio = this.getGeneralesService().getCacheDominioPorNombre(
                    EDominio.PREDIO_DESTINO);
                for (DetalleVisorVO det : listaDetalles.getListaDetalles()) {
                    for (Dominio dom : dominio) {
                        if (det.getDestinoValor().equals(dom.getCodigo())) {
                            det.setDestinoValor(dom.getValor());
                        }
                    }
                }
                modelMap.addAttribute("Detalles", listaDetalles);

                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            ErrorVO error = new ErrorVO("Error en el servidor");
            modelMap.addAttribute("respuesta", error);
            return null;
        }

    }

    /**
     *
     * @param modelMap
     * @param codigos
     * @return
     *
     * Modificado lorena.salamanca :: Cambio URL del servicio, anterior:
     * "/predio/estadisticas/{cod}"
     *
     */
    @RequestMapping(value = "/predio/estadisticas/concentracionDePropiedad/{cod}")
    public String consultarEstadisticasPredio(ModelMap modelMap,
        @RequestParam("depto") String depto,
        @RequestParam("municipio") String municipio,
        @RequestParam("tipo") String tipo) {

        List<EstadisticasRegistroVO> listaPropiedades = new ArrayList<EstadisticasRegistroVO>();

        try {

            listaPropiedades = this.getConservacionService()
                .obtenerEstadisticasConcentracionPropiedad(depto,
                    municipio, tipo);
            if (listaPropiedades != null) {

                RespuestaVO respuesta = new RespuestaVO("Done",
                    listaPropiedades);
                modelMap.addAttribute("respuesta", respuesta);
            } else {
                ErrorVO error = new ErrorVO(
                    "No existen valores para los criterios de búsqueda");
                modelMap.addAttribute("respuesta", error);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            ErrorVO error = new ErrorVO("Error en el servidor");
            modelMap.addAttribute("respuesta", error);
            return null;
        }

        return null;
    }

}
