package co.gov.igac.snc.web.jobs;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.snc.web.listener.IContextListener;

/**
 * Job para eliminación de archivos temporales
 *
 * @author juan.mendez
 *
 */
public class CleanTempDirJob extends QuartzJobBean {

    private static final Logger LOGGER = Logger.getLogger(CleanTempDirJob.class);

    private IContextListener context;


    /*
     * (non-Javadoc) @see
     * org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     */
    @Override
    protected void executeInternal(JobExecutionContext ctx)
        throws JobExecutionException {
        try {
            FileUtils.cleanDirectory(new File(FileUtils.getTempDirectory().getAbsolutePath()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        try {
            IDocumentosService service = DocumentalServiceFactory.getService();
            service.limpiarWorkspacePreview();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    public void setContext(IContextListener context) {
        this.context = context;
    }

    public IContextListener getContext() {
        return context;
    }

}
