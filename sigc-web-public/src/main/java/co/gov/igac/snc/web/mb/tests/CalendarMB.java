/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.tests;

import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author pagm
 */
@Component("calendar")
@Scope("session")
public class CalendarMB implements Serializable {

    private Date fecha;

    private Date currentDate;

    private boolean isSomething;

    /**
     * solo para probar si incluyendo un MB con la anotación @Autowired hace que de inmediato este
     * se instancie
     */
    @Autowired
    private ConsultaPredioMB consultaPredioMB;

    public CalendarMB() {

    }

    @PostConstruct
    public void init() {
        this.setCurrentDate(new Date());

        this.isSomething = true;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getCurrentDate() {
        return this.currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public void validarFecha() {

    }

    public boolean getIsSomething() {
        return this.isSomething;
    }

    public void setIsSomething(boolean isSomething) {
        this.isSomething = isSomething;
    }

}
