/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.util.List;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoAsignacionProfesional;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Managed bean del CU-SA-AC-112 Consultar órdenes de práctica por radicación
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-112
 */
@Component("consultarOrdenesPracticaRad")
@Scope("session")
public class ConsultaOrdenPracticaPorRadicacionMB extends SNCManagedBean {

    /**
     * Serial ID generado
     */
    private static final long serialVersionUID = 297583064013776641L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaOrdenPracticaPorRadicacionMB.class);

    /**
     * contiene la lista de asignaciones encontradas para la solicitud. El objeto
     * {@link AvaluoAsignacionProfesional} es el que contiene la información de la orden práctica el
     * avalúo relacionado
     */
    private List<AvaluoAsignacionProfesional> asignacionesSolicitud;

    /**
     * contiene el avalúo seleccionado para visualización de información predial, deptos y munis
     */
    @SuppressWarnings("unused")
    private Avaluo avaluoSeleccionado;

    /**
     * Contiene el número de radicación de la solicitud que se está consultando
     */
    private String radicacion;

    /**
     * Orden de práctica seleccionada
     */
    private OrdenPractica ordenPracticaSeleccionada;

    // --------------------------------Reportes---------------------------------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte de la orden de practica
     */
    private ReporteDTO ordenPracticaReporte;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public List<AvaluoAsignacionProfesional> getAsignacionesSolicitud() {
        return this.asignacionesSolicitud;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public String getRadicacion() {
        return this.radicacion;
    }

    public void setOrdenPracticaSeleccionada(
        OrdenPractica ordenPracticaSeleccionada) {
        this.ordenPracticaSeleccionada = ordenPracticaSeleccionada;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getOrdenPracticaReporte() {
        return this.ordenPracticaReporte;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    // ---------------------------------Métodos---------------------------------
    // ------------------------------------------------------------------------
    /**
     * Método que se llama desde otros MB para consultar las ordenes de practica asociadas a un
     * número de radicado
     *
     * @author christian.rodriguez
     * @param numeroRadicado cadena de texto con el numero de radicación
     */
    public void cargarDesdeOtrosMB(String numeroRadicado) {
        if (numeroRadicado != null) {
            this.radicacion = numeroRadicado;
            this.consultarOrdenesDePracticaAsociadasAlRadicado();
        }
    }

    // ------------------------------------------------------------------------
    /**
     * Método que llama la consulta que trae las {@link AvaluoAsignacionProfesional} asociadas a la
     * {@link Solicitud} con el número de radicación seleccionado
     *
     * @author christian.rodriguez
     */
    private void consultarOrdenesDePracticaAsociadasAlRadicado() {
        if (this.radicacion != null && !this.radicacion.isEmpty()) {
            this.asignacionesSolicitud = this
                .getAvaluosService()
                .consultarAvaluoAsignacionProfesionalPorSolicitudConOrdenPractica(
                    this.radicacion);
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se ejecuta para cargar el documento asociado a la orden de práctica y
     * visualizarlo en el visualizador de memorandos
     *
     * @author christian.rodriguez
     */
    public void cargarDocumentoOrdenPractica() {
        if (this.ordenPracticaSeleccionada != null) {

            Documento documentoOrdenPractica = this.getGeneralesService()
                .buscarDocumentoPorId(
                    this.ordenPracticaSeleccionada.getDocumentoId());

            this.ordenPracticaReporte = this.reportsService
                .consultarReporteDeGestorDocumental(
                    documentoOrdenPractica.getIdRepositorioDocumentos());

            if (this.ordenPracticaReporte == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "La orden prática asociada no pudo ser cargado.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        }
    }

}
