/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.ofertasInmobiliarias.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.ProcesoDeOfertasInmobiliarias;
import co.gov.igac.snc.apiprocesos.osmi.ofertasInmobiliarias.objetosNegocio.OfertaInmobiliaria;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.persistence.entity.avaluos.ComisionOferta;
import co.gov.igac.snc.persistence.entity.avaluos.DetalleCapturaOferta;
import co.gov.igac.snc.persistence.entity.avaluos.RegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EEstadoRegionCapturaOferta;
import co.gov.igac.snc.persistence.util.EOfertaComisionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para el cu CU-TV-0F-003 Proyectar Orden Comisión Ofertas
 *
 * @author christian.rodriguez
 * @cu CU-TV-0F-003
 * @version 2.0
 */
@Component("proyeccionOrdenComisionOfertas")
@Scope("session")
public class ProyeccionOrdenComisionOfertasMB extends OrdenComisionOfertasMB {

    /**
     * auto generated serial
     */
    private static final long serialVersionUID = 5656168997679537655L;

    /**
     * Contiene las regiones pendientes de comisión que se muestran en la tabla de areas pendientes
     * de comision
     */
    private List<RegionCapturaOferta> regionesPendientesComision;

    /**
     * Contiene las regiones pendientes de comisión seleccionadas de la tabla de areas pendientes de
     * comision
     */
    private RegionCapturaOferta[] selectedRegionesPendientesComision;

    private DetalleCapturaOferta[] selectedAreasPendienteComision;

    // ---- banderas
    /**
     * Determina si se debe mostrar o no el panel de comisiones existentes
     */
    public boolean banderaActivarPanelComisiones;

    // -------------------- VARIABLES USADAS PARA INTEGRACION CON VISOR
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String manzanaCodigo;

    // ---------------- methods
    // ------------------------------------------------------------------
    public RegionCapturaOferta[] getSelectedRegionesPendientesComision() {
        return this.selectedRegionesPendientesComision;
    }

    public void setSelectedRegionesPendientesComision(
        RegionCapturaOferta[] selectedRegionesPendientesComision) {
        this.selectedRegionesPendientesComision = selectedRegionesPendientesComision;
    }

    public List<RegionCapturaOferta> getRegionesPendientesComision() {
        return this.regionesPendientesComision;
    }

    public void setRegionesPendientesComision(List<RegionCapturaOferta> regionesPendientesComision) {
        this.regionesPendientesComision = regionesPendientesComision;
    }

    public ComisionOferta[] getSelectedComisionesPorAprobar() {
        return this.selectedComisionesPorAprobar;
    }

    public void setSelectedComisionesPorAprobar(ComisionOferta[] selectedComisionesPorAprobar) {
        this.selectedComisionesPorAprobar = selectedComisionesPorAprobar;
    }

    public DetalleCapturaOferta[] getSelectedAreasPendienteComision() {
        return selectedAreasPendienteComision;
    }

    public void setSelectedAreasPendienteComision(
        DetalleCapturaOferta[] selectedAreasPendienteComision) {
        this.selectedAreasPendienteComision = selectedAreasPendienteComision;
    }

    public String getApplicationClientName() {
        return this.applicationClientName;
    }

    public String getModuleLayer() {
        return this.moduleLayer;
    }

    public String getModuleTools() {
        return this.moduleTools;
    }

    public String getManzanaCodigo() {
        return this.manzanaCodigo;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on ProyeccionOrdenComisionOfertasMB init");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        this.extraerCodigoEstructuraOrganizacional();

        this.inicializarVariablesVisorGIS();

        this.estadoInicialComisiones = EOfertaComisionEstado.ENVIO_APROBACION.getCodigo();

        this.almacenarIdsActividadesArbol();

        this.cargarListaComisiones();

        this.banderaEsCreacion = true;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see OrdenComisionOfertasMB#reinicializarComisionesSeleccionadas()
     * @author christian.rodriguez
     */
    @Override
    protected void reinicializarComisionesSeleccionadas() {
        this.setSelectedComisionesPorAprobar(new ComisionOferta[]{});
        super.reinicializarComisionesSeleccionadas();
    }

    /**
     * @see OrdenComisionOfertasMB#guardarComisionOferta()
     * @author christian.rodriguez
     */
    @Override
    public void guardarComisionOferta() {
        super.guardarComisionOferta();

        this.selectedRegionesPendientesComision = new RegionCapturaOferta[]{};
    }

//--------------------------------------------------------------------------------------------------
    /**
     * @see OrdenComisionOfertasMB#cargarListaComisiones()
     * @author christian.rodriguez
     */
    @Override
    public void cargarListaComisiones() {
        super.cargarListaComisiones();

        List<String> estados = new ArrayList<String>();
        estados.add(EEstadoRegionCapturaOferta.ASIGNADA.getEstado());
        estados.add(EEstadoRegionCapturaOferta.CREADA.getEstado());
        estados.add(EEstadoRegionCapturaOferta.EN_COMISION.getEstado());

        this.regionesPendientesComision = this.getAvaluosService()
            .buscarRegionesCapturaOfertaPorEstadosYIds(this.idRegiones, estados);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.manzanaCodigo = "";
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Crea una comisión de ofertas cons los datos ignresados por el usuario y los valores por
     * defecto apra la comisión
     *
     * @author christian.rodriguez
     * @modified pedro.garcia 27-06-2012 no se usa el AreaCapturaOferta para obtener el departamento
     * y municipio
     */
    public void crearComisionOferta() {

        String numeroComision;

        this.banderaEsCreacion = true;

        this.reinicializarComisionesSeleccionadas();

        numeroComision = String.valueOf(this.getGeneralesService()
            .generarNumeracion(ENumeraciones.NUMERACION_TYA,
                this.estructuraOrganizacionalCodUsuario, "", "", 0)[0]);

        this.selectedComisionPorAprobar.setNumero(numeroComision);
        this.selectedComisionPorAprobar.setFechaLog(new Date(System.currentTimeMillis()));
        this.selectedComisionPorAprobar
            .setEstado(EOfertaComisionEstado.ENVIO_APROBACION.toString());
        this.selectedComisionPorAprobar.setRegionCapturaOfertas(Arrays
            .asList(this.selectedRegionesPendientesComision));
        this.selectedComisionPorAprobar
            .setEstructuraOrganizacionalCod(this.estructuraOrganizacionalUsuario);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Adiciona un recolector de la comisión
     *
     * @author christian.rodriguez
     */
    public void asignarAComisionSeleccionada() {

        this.actualizarRegionesComision();

        this.selectedComisionPorAprobar.getRegionCapturaOfertas().addAll(
            Arrays.asList(this.selectedRegionesPendientesComision));

        this.banderaEsCreacion = false;

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Determina si se debe mostrar o no el panel de comisiones existentes
     *
     * @author christian.rodriguez
     * @return verdadero si se ha seleccionado un área para comisioanr, falso si no
     */
    public boolean isBanderaActivarPanelComisiones() {
        if (this.selectedRegionesPendientesComision != null &&
             this.selectedRegionesPendientesComision.length > 0) {
            return true;
        }
        return false;
    }

    /**
     * Método ejecutado cuando se selecciona un área de la tabla de áreas pendientes de comisionar,
     * se encarga de concatenar la manzana o vereda a la cadena que va a enviarse como parametro al
     * visor GIS
     *
     * @author christian.rodriguez
     * @param event evento de selección de la tabla de áreas pendientes de comisionar
     */
    public void agregarManzanaVisorGIS(SelectEvent event) {

        RegionCapturaOferta region = (RegionCapturaOferta) event.getObject();
        String manzana;

        for (DetalleCapturaOferta detalle : region.getDetalleCapturaOfertas()) {
            manzana = detalle.getManzanaVeredaCodigo();

            if (!this.manzanaCodigo.isEmpty()) {
                this.manzanaCodigo = this.manzanaCodigo.concat(",");
            }
            this.manzanaCodigo = this.manzanaCodigo.concat(manzana);
        }

    }

    /**
     * Método ejecutado cuando se deselecciona un área de la tabla de áreas pendientes de
     * comisionar, se encarga de eliminar la manzana o vereda de la cadena que va a enviarse como
     * parametro al visor GIS
     *
     * @author christian.rodriguez
     * @param event evento de deselección de la tabla de áreas pendientes de comisionar
     */
    public void eliminarManzanaVisorGIS(UnselectEvent event) {

        RegionCapturaOferta region = (RegionCapturaOferta) event.getObject();

        String manzana;

        for (DetalleCapturaOferta detalle : region.getDetalleCapturaOfertas()) {
            manzana = detalle.getManzanaVeredaCodigo();

            this.manzanaCodigo = this.manzanaCodigo.replace(manzana, "");
            this.manzanaCodigo = this.manzanaCodigo.replace(",,", ",");

            if (this.manzanaCodigo.charAt(this.manzanaCodigo.length() - 1) == ',') {
                this.manzanaCodigo = this.manzanaCodigo.substring(0,
                    this.manzanaCodigo.length() - 1);
            }
            if (this.manzanaCodigo.charAt(0) == ',') {
                this.manzanaCodigo = this.manzanaCodigo.substring(1, this.manzanaCodigo.length());
            }
        }

    }

    /**
     * Limpia las manzanas seleccionadas para que no sean pintadas con el visor
     *
     * @author christian.rodriguez
     */
    public void eliminarManzanasSeleccionadas() {
        this.manzanaCodigo = "";
        this.selectedAreasPendienteComision = new DetalleCapturaOferta[]{};
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Método ejecutado cuando se oprime el botón de enviar a aprobación. Se enecargar de cambiar el
     * estado de las comisiones seleccioandas y actualizarlas en la base de datos
     *
     * Tambien ejecuta los métodos de process.
     *
     * @author christian.rodriguez
     * @modified pedro.garcia
     */
    public void enviarComisionesAprobacion() {

        if (this.getSelectedComisionesPorAprobar() != null) {

            for (ComisionOferta comision : this.getSelectedComisionesPorAprobar()) {
                comision.setEstado(EOfertaComisionEstado.POR_APROBAR.getCodigo());
            }

            if (!this.forwardProcess()) {
                this.addMensajeError("Ocurrió un error avanzando alguna de las actividades del " +
                    "proceso");
            }

            this.cargarListaComisiones();
        }

    }

    // --------------------------------------------------------------
    /**
     * Valida que todas las regiones seleccionadas estén en estado ASIGNADA
     *
     * @author christian.rodriguez
     */
    public void validarRegiones() {

        if (this.selectedRegionesPendientesComision != null &&
             this.selectedRegionesPendientesComision.length != 0) {

            boolean regionesValidas = true;

            for (RegionCapturaOferta rco : this.selectedRegionesPendientesComision) {
                if (!rco.getEstado().equals(EEstadoRegionCapturaOferta.ASIGNADA.getEstado())) {
                    regionesValidas = false;
                }
            }

            if (!regionesValidas) {
                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "Todas las regiones a comisionar deben estar en estado " +
                     EEstadoRegionCapturaOferta.ASIGNADA.getEstado();
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            String mensaje = "Debe seleccionar al menos una región para comisionar";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }
    }

    // ------------- Metodos para integración con process -------------
    private boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     * @return false si no hubo ningún error avanzando las actividades
     */
    private boolean forwardProcess() {

        LOGGER.debug("Moviendo el proceso para las regiones asociadas a la comisión");

        boolean ok = true;
        ArrayList<RegionCapturaOferta> regionesToForward, tempRegionesToForward;
        List<UsuarioDTO> listaUsuarios;
        OfertaInmobiliaria ofertaBPM;
        String idActividad, transition;

        if (!this.validateProcess()) {
            return false;
        }

        if (!this.doDatabaseStatesUpdate()) {
            return false;
        }

        //D: buscar las regiones que estén asociadas con cada comisión que se va a aprobar para
        //   tener los datos de los procesos que se deben avanzar
        regionesToForward = new ArrayList<RegionCapturaOferta>();
        for (ComisionOferta comision : this.getSelectedComisionesPorAprobar()) {
            tempRegionesToForward = (ArrayList<RegionCapturaOferta>) this.getAvaluosService().
                obtenerRegionesCapturaOfertaDeComisionNF(comision.getId());

            if (tempRegionesToForward != null && !tempRegionesToForward.isEmpty()) {
                regionesToForward.addAll(tempRegionesToForward);
            }
        }

        transition = ProcesoDeOfertasInmobiliarias.ACT_ASIGNACION_APROBAR_AJUSTAR_ORDEN_COMISION;
        listaUsuarios = new ArrayList<UsuarioDTO>();

        //D: si el usuario en un investigador de marcado, la comisión la aprueba el director de la
        //   territorial; si es el coordinador GIT avaluos, la aprueba el subdirector de catastro
        String[] rolesUsuario = this.loggedInUser.getRoles();
        ArrayList<UsuarioDTO> usuariosRolDestino = null;

        for (String rol : rolesUsuario) {
            //N: no se deben comparar con equals porque por alguna razón ponene en mayúsculas los
            //  roles del usuario al crearlo en el inicio de sesión
            if (rol.compareToIgnoreCase(ERol.INVESTIGADOR_MERCADO.toString()) == 0) {
                usuariosRolDestino = (ArrayList<UsuarioDTO>) this.getTramiteService().
                    buscarFuncionariosPorRolYTerritorial(
                        this.loggedInUser.getDescripcionTerritorial(), ERol.DIRECTOR_TERRITORIAL);
            } else if (rol.compareToIgnoreCase(ERol.COORDINADOR_GIT_AVALUOS.toString()) == 0) {
                usuariosRolDestino = (ArrayList<UsuarioDTO>) this.getTramiteService().
                    buscarFuncionariosPorRolYTerritorial(
                        this.loggedInUser.getDescripcionTerritorial(), ERol.SUBDIRECTOR_CATASTRO);
            }
        }

        if (usuariosRolDestino == null || usuariosRolDestino.isEmpty()) {
            this.addMensajeError("No se encontró un usuario destino de la actividad 'Aprobar " +
                "orden de comisión'");
            return false;
        }

        listaUsuarios.add(usuariosRolDestino.get(0));

        for (RegionCapturaOferta rco : regionesToForward) {
            ofertaBPM = new OfertaInmobiliaria();
            ofertaBPM.setUsuarios(listaUsuarios);
            ofertaBPM.setTransicion(transition);
            ofertaBPM.setTerritorial(this.estructuraOrganizacionalUsuario);
            idActividad = this.mapaActividadRegion.get(rco.getId());

            if (idActividad != null) {
                try {
                    this.getProcesosService().avanzarActividad(idActividad, ofertaBPM);
                } catch (Exception ex) {
                    LOGGER.error("Error moviendo actividad con id " + idActividad + " a la " +
                        "actividad " + transition + " : " + ex.getMessage());
                    ok = false;
                    break;
                }
            }
        }

        return ok;
    }

// end of class
}
