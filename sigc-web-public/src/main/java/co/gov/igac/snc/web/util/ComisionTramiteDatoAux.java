/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util;

/**
 * clase auxiliar para poder armar una tabla con checks en las columnas representando los valores
 * para los campos 'validado' y 'modificado' de la clase ComisionTramiteDato
 *
 * @author pedro.garcia
 */
public class ComisionTramiteDatoAux {

    private String dato;
    private boolean modificado;
    private boolean verificado;

    //-----------   methods
    public String getDato() {
        return this.dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public boolean isModificado() {
        return this.modificado;
    }

    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    public boolean isVerificado() {
        return this.verificado;
    }

    public void setVerificado(boolean verificado) {
        this.verificado = verificado;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * constructor
     *
     * @param datoP
     */
    public ComisionTramiteDatoAux(String datoP) {
        this.dato = datoP;
        this.modificado = false;
        this.verificado = false;
    }

//end of class
}
