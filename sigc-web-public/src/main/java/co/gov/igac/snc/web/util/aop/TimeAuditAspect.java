package co.gov.igac.snc.web.util.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Aspecto para la medición del tiempo de ejecución de métodos
 *
 * @author juan.mendez
 *
 */
public class TimeAuditAspect {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(TimeAuditAspect.class);

    /**
     * Aspecto para la medición del tiempo de ejecución de métodos
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    public Object timeAudit(ProceedingJoinPoint pjp) throws Throwable {
        String targetName = pjp.getSignature().getDeclaringTypeName();
        String methodName = pjp.getSignature().getName();

        String targetMethod = targetName + "." + methodName;
        long startTime = System.currentTimeMillis();
        //LOGGER.debug("Execution - Invoking " + targetMethod);
        try {
            return pjp.proceed();
        } finally {
            // log.debug("Exiting " + target);
            long totalTime = System.currentTimeMillis() - startTime;
            LOGGER.debug("Execution - End Method:" + targetMethod + " Time:" + totalTime + "ms");
        }
    }

}
