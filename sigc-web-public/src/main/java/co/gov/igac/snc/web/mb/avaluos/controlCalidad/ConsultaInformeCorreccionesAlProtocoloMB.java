/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.Serializable;
import javax.annotation.PostConstruct;

/**
 *
 * Managed bean del CU-SA-AC-021 Consultar Informe de Correcciones al Protocolo
 *
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-021
 */
@Component("consultaInformeCorrecccionesProtocolo")
@Scope("session")
public class ConsultaInformeCorreccionesAlProtocoloMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 604549531386297112L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaInformeCorreccionesAlProtocoloMB.class);
    /**
     * Avalúo asociado a la consulta de controles de calidad
     */
    private Avaluo avaluo;
    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Control de calidad seleccionado para ver el informe de control de calidad
     */
    private ControlCalidadAvaluo controlCalidadSeleccionado;

    /**
     * Lista de controles de calidad asociados al avalúo
     */
    private List<ControlCalidadAvaluo> listaControlesCalidad;

    // ----------------------------Métodos SET y GET----------------------------
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public ControlCalidadAvaluo getControlCalidadSeleccionado() {
        return controlCalidadSeleccionado;
    }

    public void setControlCalidadSeleccionado(ControlCalidadAvaluo controlCalidadSeleccionado) {
        this.controlCalidadSeleccionado = controlCalidadSeleccionado;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public List<ControlCalidadAvaluo> getListaControlesCalidad() {
        return listaControlesCalidad;
    }

    public void setListaControlesCalidad(List<ControlCalidadAvaluo> listaControlesCalidad) {
        this.listaControlesCalidad = listaControlesCalidad;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaInformeCorreccionesAlProtocoloMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.avaluo = new Avaluo();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }
    }

    /**
     * Método para llamar este caso de uso desde otros CU se debe inicializar el avaluo.id para
     * cargar los datos del avalúo
     *
     * @author felipe.cadena
     */
    public void cargarDesdeOtrosMB() {

        this.avaluo = this.getAvaluosService().obtenerAvaluoPorIdConAtributos(this.avaluo.getId());
        this.buscarControlesCalidad();
    }

    /**
     * Método para consultar los controles de calidad asociados al avaluo actual
     *
     * @author felipe.cadena
     */
    public void buscarControlesCalidad() {
        LOGGER.debug("Buscando controles de calidad ...");
        this.listaControlesCalidad = this.getAvaluosService().
            consultarControlCalidadPorAvaluoId(this.avaluo.getId());
        LOGGER.debug("Busqueda terminada");
    }

    /**
     * Método para llamar el caso de uso CU-SA-AC-108
     *
     * @author felipe.cadena
     */
    public void verInformeControlCalidad() {
        LOGGER.debug(
            "Llamar CU-SA-AC-108-Consultar Informes de Control de Calidad realizados o en proceso");
//TODO :: felipe.cadena::integrar CU-SA-AC-108-Consultar Informes de Control de Calidad realizados o en proceso

        ConsultaRevisionesControlCalidadMB revisionesControlCalidadMB =
            (ConsultaRevisionesControlCalidadMB) UtilidadesWeb.getManagedBean(
                "consultaRevisionesControlCalidad");
        revisionesControlCalidadMB.cargarDesdeOtrosMB(
            this.controlCalidadSeleccionado.getId(),
            this.avaluo);

        LOGGER.debug("Busqueda terminada");
    }
}
