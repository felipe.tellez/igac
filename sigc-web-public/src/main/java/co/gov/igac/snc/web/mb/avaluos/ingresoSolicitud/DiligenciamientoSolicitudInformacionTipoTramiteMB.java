package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.persistence.util.ESolicitudOrigenPriPubMix;
import co.gov.igac.snc.persistence.util.ETramiteMarcoJuridico;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed Bean para caso de uso CU-SA-AC-136
 *
 * @cu CU-SA-AC-136 Diligenciar Solicitud – Información Tipo Trámite a Solicitar
 *
 * @author rodrigo.hernandez
 *
 */
@Component("diligenciamientoInfoTipoTramite")
@Scope("session")
public class DiligenciamientoSolicitudInformacionTipoTramiteMB extends
    SNCManagedBean implements Serializable {

    /**
     * Serial generado por default
     */
    private static final long serialVersionUID = -8446289905338303081L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(DiligenciamientoSolicitudInformacionTipoTramiteMB.class);

    // ---------- Managed Beans ----------------------------------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private GeneralMB generalMB;

    // ---------- Variables que se cargan con informacion de Managed Bean de
    // Diligenciar Solicitud ---------------------
    /**
     * Solicitante de la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Solicitud en curso
     */
    private Solicitud solicitud;

    /**
     * Solicitud en curso
     */
    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    /**
     * Variable que indica si el almacenamiento de la información de la solicitud se ha hecho de
     * forma definitiva o temporal
     */
    private boolean banderaModoLecturaHabilitado;

    // -----------------------------------------------------------------------
    /**
     * Lista para almacenar valores de los tipos de solicitud
     */
    private List<SelectItem> listaTipoSolicitudSelectItem;
    /**
     * Lista para almacenar valores de Sec. Radicados asociados a una solicitud de avaluo
     */
    private List<SelectItem> listaSecRadicadosSolicitudAvaluo;
    /**
     * Lista para almacenar valores de Sec. Radicados asociados a una solicitud de cotizacion
     */
    private List<SelectItem> listaSecRadicadosSolicitudCotizacion;

    /**
     * Variable para indicar si la solicitud de revisión es en subsidio de impugnación
     */
    private boolean banderaTipoTramiteSolicitudAvaluoSeleccionado;

    /**
     * Variable para indicar si la solicitud de revisión es en subsidio de impugnación
     */
    private boolean banderaTipoTramiteSolicitudCotizacionSeleccionado;

    /**
     * Variable para indicar si la solicitud de revisión es en subsidio de impugnación
     */
    private boolean banderaTipoTramiteSolicitudRevisionSeleccionado;

    /**
     * Variable para indicar si la solicitud de revisión es en subsidio de impugnación
     */
    private boolean banderaTipoTramiteSolicitudImpugnacionSeleccionado;

    /**
     * Variable para indicar si se debe mostrar el dato de tipo solicitud en pantalla
     */
    private boolean banderaMostrarTipoSolicitud;

    /*
     * Otros
     */
    /**
     * Sec Radicado seleccionado para Solicitud de Revision o Solicitud de Impugnacion
     */
    private String secRadicadoSeleccionado;

    /**
     * Indica si la solicitud tiene subsidio de impugnacion
     */
    private String conSubsidioDeImpugnacion;

    /**
     * Avaluo asociado al sec radicado seleccionado para Solicitud de impugnacion y Solicitud de
     * revision
     */
    private Avaluo avaluo;

    // ----------------------------------------------------------------------
    // --------- Setters y Getters
    // --------------------------------------------------
    public List<SelectItem> getListaTipoSolicitudSelectItem() {
        return listaTipoSolicitudSelectItem;
    }

    public void setListaTipoSolicitudSelectItem(
        List<SelectItem> listaTipoSolicitudSelectItem) {
        this.listaTipoSolicitudSelectItem = listaTipoSolicitudSelectItem;
    }

    public boolean isBanderaTipoTramiteSolicitudAvaluoSeleccionado() {
        return banderaTipoTramiteSolicitudAvaluoSeleccionado;
    }

    public void setBanderaTipoTramiteSolicitudAvaluoSeleccionado(
        boolean banderaTipoTramiteSolicitudAvaluoSeleccionado) {
        this.banderaTipoTramiteSolicitudAvaluoSeleccionado =
            banderaTipoTramiteSolicitudAvaluoSeleccionado;
    }

    public boolean isBanderaTipoTramiteSolicitudCotizacionSeleccionado() {
        return banderaTipoTramiteSolicitudCotizacionSeleccionado;
    }

    public void setBanderaTipoTramiteSolicitudCotizacionSeleccionado(
        boolean banderaTipoTramiteSolicitudCotizacionSeleccionado) {
        this.banderaTipoTramiteSolicitudCotizacionSeleccionado =
            banderaTipoTramiteSolicitudCotizacionSeleccionado;
    }

    public boolean isBanderaTipoTramiteSolicitudRevisionSeleccionado() {
        return banderaTipoTramiteSolicitudRevisionSeleccionado;
    }

    public void setBanderaTipoTramiteSolicitudRevisionSeleccionado(
        boolean banderaTipoTramiteSolicitudRevisionSeleccionado) {
        this.banderaTipoTramiteSolicitudRevisionSeleccionado =
            banderaTipoTramiteSolicitudRevisionSeleccionado;
    }

    public boolean isBanderaTipoTramiteSolicitudImpugnacionSeleccionado() {
        return banderaTipoTramiteSolicitudImpugnacionSeleccionado;
    }

    public void setBanderaTipoTramiteSolicitudImpugnacionSeleccionado(
        boolean banderaTipoTramiteSolicitudImpugnacionSeleccionado) {
        this.banderaTipoTramiteSolicitudImpugnacionSeleccionado =
            banderaTipoTramiteSolicitudImpugnacionSeleccionado;
    }

    public boolean isBanderaMostrarTipoSolicitud() {
        return banderaMostrarTipoSolicitud;
    }

    public void setBanderaMostrarTipoSolicitud(
        boolean banderaMostrarTipoSolicitud) {
        this.banderaMostrarTipoSolicitud = banderaMostrarTipoSolicitud;
    }

    public List<SelectItem> getListaSecRadicadosSolicitudAvaluo() {
        return listaSecRadicadosSolicitudAvaluo;
    }

    public void setListaSecRadicadosSolicitudAvaluo(
        List<SelectItem> listaSecRadicadosSolicitudAvaluo) {
        this.listaSecRadicadosSolicitudAvaluo = listaSecRadicadosSolicitudAvaluo;
    }

    public List<SelectItem> getListaSecRadicadosSolicitudCotizacion() {
        return listaSecRadicadosSolicitudCotizacion;
    }

    public void setListaSecRadicadosSolicitudCotizacion(
        List<SelectItem> listaSecRadicadosSolicitudCotizacion) {
        this.listaSecRadicadosSolicitudCotizacion = listaSecRadicadosSolicitudCotizacion;
    }

    public SolicitanteSolicitud getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public boolean isBanderaModoLecturaHabilitado() {
        return banderaModoLecturaHabilitado;
    }

    public void setBanderaModoLecturaHabilitado(
        boolean banderaModoLecturaHabilitado) {
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;
    }

    public Avaluo getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public String getSecRadicadoSeleccionado() {
        return secRadicadoSeleccionado;
    }

    public void setSecRadicadoSeleccionado(String secRadicadoSeleccionado) {
        this.secRadicadoSeleccionado = secRadicadoSeleccionado;
    }

    public String getConSubsidioDeImpugnacion() {
        return conSubsidioDeImpugnacion;
    }

    public void setConSubsidioDeImpugnacion(String conSubsidioDeImpugnacion) {
        this.conSubsidioDeImpugnacion = conSubsidioDeImpugnacion;
    }

    // -------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#init");
        LOGGER.debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#init");
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método para cargar lista de tipos de solicitud segun tipo de solicitante
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaTiposSolicitudSegunSolicitante() {
        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#cargarListaTiposSolicitudSegunSolicitante");

        if (this.solicitante.getTipoSolicitante().equals(
            ESolicitanteTipoSolicitant.FISCALIA.getCodigo())) {
            this.listaTipoSolicitudSelectItem = this.generalMB
                .getAvaluosComercialesTipoSolicitudPorFiscalia();
        } else if (this.solicitante.getTipoSolicitante().equals(
            ESolicitanteTipoSolicitant.JUZGADO.getCodigo())) {
            this.listaTipoSolicitudSelectItem = this.generalMB
                .getAvaluosComercialesTipoSolicitudPorJuzgado();
        } else if (this.solicitante.getTipoSolicitante().equals(
            ESolicitanteTipoSolicitant.PROCURADURIA.getCodigo())) {
            this.listaTipoSolicitudSelectItem = this.generalMB
                .getAvaluosComercialesTipoSolicitudPorProcuraduria();
        } else if (this.solicitante.getTipoSolicitante().equals(
            ESolicitanteTipoSolicitant.PRIVADA.getCodigo()) ||
             this.solicitante.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.PUBLICA.getCodigo()) ||
             this.solicitante.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.MIXTA.getCodigo())) {
            this.listaTipoSolicitudSelectItem = this.generalMB
                .getAvaluosComercialesTipoSolicitudPorPublicaPrivadaMixta();

            // Se define el valor por default
            this.solicitud.setOrigen(ESolicitudOrigenPriPubMix.AVALUO_NORMAL
                .getCodigo());
        }
        LOGGER.debug(
            "Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#cargarListaTiposSolicitudSegunSolicitante");
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método para activar los componentes segun el tipo de tramite seleccionado Llamado desde la
     * página
     * <b>diligenciarSolicitudInformacionTipoTramite.xhtml</b></br> por selectOneMenu para <b>Tipo
     * de trámite</b>
     *
     * @author rodrigo.hernandez
     */
    public void actualizarSegunTipoTramiteSeleccionado() {
        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#actualizarSegunTipoTramiteSeleccionado");

        this.activarDesactivarBanderasTipoTramite(this.solicitud.getTipo());

        LOGGER.debug(
            "Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#actualizarSegunTipoTramiteSeleccionado");
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método para validar si se muestra el dato de entrada Tipo de Solicitud
     *
     * @author rodrigo.hernandez
     */
    private void validarVisibilidadTipoSolicitud() {
        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#validarVisibilidadTipoSolicitud");

        if (this.solicitante.getTipoSolicitante().equals(
            ESolicitanteTipoSolicitant.PRIVADA.getCodigo()) ||
             this.solicitante.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.PUBLICA.getCodigo()) ||
             this.solicitante.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.MIXTA.getCodigo())) {
            this.banderaMostrarTipoSolicitud = false;
        } else {
            this.banderaMostrarTipoSolicitud = true;
        }

        LOGGER.debug(
            "Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#validarVisibilidadTipoSolicitud");
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método para modificar los valores de las banderas asociadas al tipo de tramite seleccionado
     *
     * @param caller
     * @param valorBandera
     */
    private void activarDesactivarBanderasTipoTramite(String caller) {

        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#activarDesactivarBanderasTipoTramite");

        if (caller != null && !caller.isEmpty()) {

            // Caso para cuando el tipo de tramite es Solicitud de Cotización
            if (caller.equals(ETramiteTipoTramite.SOLICITUD_DE_COTIZACION
                .getCodigo())) {
                this.banderaTipoTramiteSolicitudAvaluoSeleccionado = false;
                this.banderaTipoTramiteSolicitudCotizacionSeleccionado = true;
                this.banderaTipoTramiteSolicitudImpugnacionSeleccionado = false;
                this.banderaTipoTramiteSolicitudRevisionSeleccionado = false;
            } // Caso para cuando el tipo de tramite es Solicitud de Avaluo
            else if (caller.equals(ETramiteTipoTramite.SOLICITUD_DE_AVALUO
                .getCodigo())) {
                this.banderaTipoTramiteSolicitudAvaluoSeleccionado = true;
                this.banderaTipoTramiteSolicitudCotizacionSeleccionado = false;
                this.banderaTipoTramiteSolicitudImpugnacionSeleccionado = false;
                this.banderaTipoTramiteSolicitudRevisionSeleccionado = false;

                cargarListaSecRadicadosSolicitudCotizacionParaAsignarSolicitudAvaluo();
            } // Caso para cuando el tipo de tramite es Solicitud de
            // Impugnación
            else if (caller.equals(ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION
                .getCodigo())) {
                this.banderaTipoTramiteSolicitudAvaluoSeleccionado = false;
                this.banderaTipoTramiteSolicitudCotizacionSeleccionado = false;
                this.banderaTipoTramiteSolicitudImpugnacionSeleccionado = true;
                this.banderaTipoTramiteSolicitudRevisionSeleccionado = false;

                this.cargarListaSecRadicadosSolicitudAvaluoParaRevisarImpugnar();
            } // Caso para cuando el tipo de tramite es Solicitud de Revisión
            else if (caller.equals(ETramiteTipoTramite.SOLICITUD_DE_REVISION
                .getCodigo())) {
                this.banderaTipoTramiteSolicitudAvaluoSeleccionado = false;
                this.banderaTipoTramiteSolicitudCotizacionSeleccionado = false;
                this.banderaTipoTramiteSolicitudImpugnacionSeleccionado = false;
                this.banderaTipoTramiteSolicitudRevisionSeleccionado = true;

                this.cargarListaSecRadicadosSolicitudAvaluoParaRevisarImpugnar();
            }
        } // Caso para resetear todas las banderas asociadas al tipo de
        // tramite
        else {
            this.banderaTipoTramiteSolicitudAvaluoSeleccionado = false;
            this.banderaTipoTramiteSolicitudCotizacionSeleccionado = false;
            this.banderaTipoTramiteSolicitudImpugnacionSeleccionado = false;
            this.banderaTipoTramiteSolicitudRevisionSeleccionado = false;

        }

        LOGGER.debug(
            "Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#activarDesactivarBanderasTipoTramite");
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método para cerrar página
     *
     * @author rodrigo.hernandez
     *
     */
    public String cerrar() {

        LOGGER.debug("Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#cerrar");

        UtilidadesWeb.removerManagedBean("diligenciamientoInfoTipoTramite");

        LOGGER.debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Metodo que se llama desde DiligenciarSolicitudMB para enviar datos de Solicitante y Tipo de
     * Almacenamiento de la informacion de la solicitud
     *
     * @author rodrigo.hernandez
     *
     * @param solicitud - Solicitud a diligenciar
     * @param solicitante - Solicitante asociado a la solicitud
     * @param banderaModoLecturaHabilitado - bandera que indica el modo de lectura de los datos</br>
     * <b>true: </b>Modo lectura </br> <b>false: </b>Modo edición
     * </br> </br>
     */
    public void cargarVariablesExternas(Solicitud solicitud,
        SolicitanteSolicitud solicitante,
        boolean banderaModoLecturaHabilitado) {

        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#cargarVariablesExternas");

        this.solicitud = solicitud;
        this.solicitante = solicitante;
        this.banderaModoLecturaHabilitado = banderaModoLecturaHabilitado;

        if (this.solicitud.getSubsidioImpugnacion() == null ||
             this.solicitud.getSubsidioImpugnacion().isEmpty()) {
            this.conSubsidioDeImpugnacion = ESiNo.NO.getCodigo();
        } else {
            this.conSubsidioDeImpugnacion = this.solicitud.getSubsidioImpugnacion();
        }

        this.secRadicadoSeleccionado = this.solicitud.getTramiteNumeroRadicacionRef();

        this.actualizarSegunTipoTramiteSeleccionado();
        this.validarVisibilidadTipoSolicitud();
        this.cargarListaTiposSolicitudSegunSolicitante();

        LOGGER.
            debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#cargarVariablesExternas");

    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Metodo para cargar la lista de Sec. Radicados asociados a solicitudes de avaluo de un
     * solicitante
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaSecRadicadosSolicitudAvaluoParaRevisarImpugnar() {
        this.listaSecRadicadosSolicitudAvaluo = new ArrayList<SelectItem>();

        LOGGER.debug("Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#cargarListaSecRadicadosSolicitudAvaluoParaRevisarImpugnar");

        List<String> listaSecRadicados = this
            .getAvaluosService()
            .consultarSecRadicadosSolicitudAvaluoPorSolicitanteParaRevisionImpugnacion(
                this.solicitante.getSolicitante().getId(),
                this.banderaTipoTramiteSolicitudRevisionSeleccionado);

        for (String secRadicado : listaSecRadicados) {
            this.listaSecRadicadosSolicitudAvaluo.add(new SelectItem(
                secRadicado, secRadicado));
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#cargarListaSecRadicadosSolicitudAvaluoParaRevisarImpugnar");
    }

    /**
     * Metodo para cargar la lista de Sec. Radicados asociados a solicitudes de avaluo de un
     * solicitante
     *
     * @author rodrigo.hernandez
     */
    private void cargarListaSecRadicadosSolicitudCotizacionParaAsignarSolicitudAvaluo() {
        this.listaSecRadicadosSolicitudCotizacion = new ArrayList<SelectItem>();

        LOGGER.debug("Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#cargarListaSecRadicadosSolicitudCotizacionParaAsignarSolicitudAvaluo");

        List<String> listaSecRadicados = this
            .getAvaluosService()
            .consultarSecRadicadosCotizacionPorSolicitanteParaAsociarSolicitudAvaluo(
                solicitante.getSolicitante().getId());

        for (String secRadicado : listaSecRadicados) {
            this.listaSecRadicadosSolicitudCotizacion.add(new SelectItem(
                secRadicado, secRadicado));
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#cargarListaSecRadicadosSolicitudCotizacionParaAsignarSolicitudAvaluo");
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que se llama cuando se hace click en el boton Aceptar
     *
     * @author rodrigo.hernandez
     */
    public void guardarInfoTramite() {
        LOGGER.debug(
            "Iniciando DiligenciamientoSolicitudInformacionTipoTramiteMB#guardarInfoTramite");

        String mensaje = "";

        if (!this.validarDatosAsociadosATipoTramiteSeleccionado()) {
            // Se reinician los valores seleccionados
            this.solicitud.setTipo(null);
            this.solicitud.setOrigen(null);
            this.solicitud.setMarcoJuridico(null);
            this.solicitud.setTramiteNumeroRadicacionRef(null);
            this.solicitud.setSubsidioImpugnacion(null);
        } else {

            /*
             * Caso para cuando el tipo de tramite es Solicitud de Impugnación o Solicitud de
             * Revision
             */
            if (this.solicitud.getTipo().equals(
                ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION.getCodigo()) ||
                 this.solicitud.getTipo().equals(
                    ETramiteTipoTramite.SOLICITUD_DE_REVISION
                        .getCodigo())) {

                this.obtenerAvaluoPorSecRadicado();

                this.solicitud
                    .setSubsidioImpugnacion(this.conSubsidioDeImpugnacion);

                this.solicitud
                    .setTramiteNumeroRadicacionRef(this.secRadicadoSeleccionado);

                this.solicitud.setAsociadaSolicitudNumero(this.avaluo
                    .getTramite().getSolicitud().getNumero());

            }

            mensaje = "Se ha ingresado correctamente la información";
            this.addMensajeInfo(mensaje);
        }

        LOGGER.debug(
            "Finalizando DiligenciamientoSolicitudInformacionTipoTramiteMB#guardarInfoTramite");
    }

    /**
     * Método que valida los campos relacionados al tipo de tramite seleccionado
     *
     * @author rodrigo.hernandez
     */
    private boolean validarDatosAsociadosATipoTramiteSeleccionado() {
        boolean result = true;
        String mensaje = "";

        LOGGER.debug("Iniciando DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#validarDatosAsociadosATipoTramiteSeleccionado");

        if (this.solicitud.getTipo() != null &&
             !this.solicitud.getTipo().isEmpty()) {

            // Caso para cuando el tipo de tramite es Solicitud de Avaluo
            if (this.solicitud.getTipo().equals(
                ETramiteTipoTramite.SOLICITUD_DE_AVALUO.getCodigo())) {

                if (!this.validarTipoSolicitudSeleccionada()) {
                    result = false;
                }

                if (this.solicitud.getMarcoJuridico() != null) {
                    if (this.solicitud.getMarcoJuridico().equals(
                        ETramiteMarcoJuridico.SIN_SELECCIONAR.getValor())) {
                        mensaje =
                            "Debe seleccionar el marco jurídico para la solicitud de Avalúo, intente nuevamente";
                        this.addMensajeError(mensaje);
                        result = false;
                    }
                } else {
                    mensaje =
                        "Debe seleccionar el marco jurídico para la solicitud de Avalúo, intente nuevamente";
                    this.addMensajeError(mensaje);
                    result = false;
                }

            } // Caso para cuando el tipo de tramite es Solicitud de
            // Impugnación
            else if (this.solicitud.getTipo().equals(
                ETramiteTipoTramite.SOLICITUD_DE_IMPUGNACION.getCodigo())) {

                if (!this.validarTipoSolicitudSeleccionada()) {
                    result = false;
                }

                if (this.solicitud.getTramiteNumeroRadicacionRef() == null ||
                     this.solicitud.getTramiteNumeroRadicacionRef()
                        .isEmpty()) {
                    mensaje =
                        "Debe ingresar  el dato de entrada Número radicación del avalúo a impugnar o revisar " +
                         "para la solicitud de Impugnación, intente nuevamente";
                    this.addMensajeError(mensaje);
                    result = false;
                }

            } // Caso para cuando el tipo de tramite es Solicitud de Revisión
            else if (this.solicitud.getTipo().equals(
                ETramiteTipoTramite.SOLICITUD_DE_REVISION.getCodigo())) {

                if (!this.validarTipoSolicitudSeleccionada()) {
                    result = false;
                }

                if (this.secRadicadoSeleccionado == null ||
                     this.secRadicadoSeleccionado.isEmpty()) {
                    mensaje =
                        "Debe ingresar  el dato de entrada Número radicación del avalúo a impugnar o revisar " +
                         "para la solicitud de Revisión, solo se hace revisiones " +
                         "para Avalúos realizados por el IGAC, intente nuevamente";
                    this.addMensajeError(mensaje);
                    result = false;
                }
            }
        } else {
            mensaje = "Debe escoger el tipo de solicitud";
            this.addMensajeError(mensaje);
            result = false;
        }

        LOGGER.debug(mensaje.isEmpty() ? "No hubo errores" : mensaje);

        LOGGER.debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#validarDatosAsociadosATipoTramiteSeleccionado");
        return result;
    }

    /**
     * Método que valida el campo de tipo de solicitud seleccionado
     *
     * @author rodrigo.hernandez
     * @return <b>true: </b>si el tipo de solicitud es valido<br/>
     * <b>false: </b>si el tipo de solicitud <b>NO</b> es valido<br/>
     */
    private boolean validarTipoSolicitudSeleccionada() {
        boolean result = true;
        String mensaje = "";

        LOGGER.debug("Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#validarTipoSolicitudSeleccionada");

        if (this.solicitud.getOrigen().isEmpty()) {
            mensaje = "Debe ingresar  el dato de entrada Tipo solicitud,  intente nuevamente";
            this.addMensajeError(mensaje);
            result = false;
        }

        LOGGER.debug("Fin DiligenciamientoSolicitudInformacionTipoTramiteMB" +
             "#validarTipoSolicitudSeleccionada");

        return result;
    }

    /**
     * Método que valida que no existan datos sin ingresar
     *
     * @author rodrigo.hernandez
     *
     * @return <b>true: </b>los datos requeridos fueron ingresados<br/>
     * <b>false: </b>hay datos pendientes por ingresar<br/>
     */
    public boolean validarInfo() {
        boolean result = true;

        result = this.validarDatosAsociadosATipoTramiteSeleccionado();

        return result;
    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método para consultar un avaluo asociado a un sec radicado escogido para Solicitud de
     * Revision o Solicitud de Impugnacion
     *
     * @author rodrigo.hernandez
     */
    private void obtenerAvaluoPorSecRadicado() {
        LOGGER.debug(
            "Inicio DiligenciamientoSolicitudInformacionTipoTramiteMB#obtenerAvaluoPorSecRadicado");

        this.avaluo = this.getAvaluosService().buscarAvaluoPorSecRadicado(
            this.secRadicadoSeleccionado);

        LOGGER.debug(
            "Fin DiligenciamientoSolicitudInformacionTipoTramiteMB#obtenerAvaluoPorSecRadicado");
    }

}
