package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.primefaces.context.RequestContext;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.comun.anotaciones.Implement;

/**
 * Converter para reemplazar un UsuarioDTO.
 *
 * @author david.cifuentes
 */
public class UsuarioConverter implements Converter {

    @Implement
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic,
        String objectAsString) {

        UsuarioDTO answer = new UsuarioDTO();

        try {
            String[] objectAttributes;
            objectAttributes = objectAsString.split(",");

            if (objectAttributes != null && objectAttributes.length > 10) {
                answer.setTipoIdentificacion(this.split(objectAttributes[1]));
                answer.setPrimerNombre(this.split(objectAttributes[2]));
                answer.setSegundoNombre(this.split(objectAttributes[3]));
                answer.setPrimerApellido(this.split(objectAttributes[4]));
                answer.setSegundoApellido(this.split(objectAttributes[5]));
                answer.setLogin(this.split(objectAttributes[6]));
                answer.setEmail(this.split(objectAttributes[7]));
                answer.setCodigoUOC(this.split(objectAttributes[10]));
                answer.setCodigoTerritorial(this.split(objectAttributes[11]));
            }
        } catch (Exception e) {
            return null;
        }
        return answer;
    }

    @Implement
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        try {
            if (o != null) {
                UsuarioDTO objDT = (UsuarioDTO) o;
                String answer = objDT.toString();
                return answer;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Método que realiza un split de un {@link String} y retorna el segundo termino de la cadena
     * parámetro
     *
     * @param object
     * @return
     */
    public String split(String object) {

        try {
            if (object != null) {
                String[] parts = object.split("=");
                if (parts.length > 0 && "<null>".equals(parts[1])) {
                    return null;
                } else {
                    return parts[1];
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
