package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.util.FiltroDatosConsultaSolicitante;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.util.AdministracionInformacionSolicitanteMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el caso de uso 054 de avalúos, sirve para buscar las entidades (de tipo
 * {@link Solicitante}) que tienen contrato intergbernamental según el filtro especificado por el
 * usuario
 *
 * @author christian.rodriguez
 *
 */
@Component("consultaInformacionEntidades")
@Scope("session")
public class ConsultaInformacionEntidadesMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 3192560069126536848L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaInformacionEntidadesMB.class);

    /**
     * Contiene los solicitantes encontrados en la busqueda
     */
    private List<Solicitante> solicitantesEncontrados;

    /**
     * MB para manejar informacion de entidades
     */
    private AdministracionInformacionSolicitanteMB administracionInformacionSolicitanteMB;

    private FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que indica si el tipo de persona es natural en la busqueda del solicitante
     */
    private boolean banderaEsPersonaNatural;

    private boolean verPanelResultados;
    // ----------------------------Métodos SET y GET----------------------------

    public FiltroDatosConsultaSolicitante getFiltroDatosConsultaSolicitante() {
        return filtroDatosConsultaSolicitante;
    }

    public void setFiltroDatosConsultaSolicitante(
        FiltroDatosConsultaSolicitante filtroDatosConsultaSolicitante) {
        this.filtroDatosConsultaSolicitante = filtroDatosConsultaSolicitante;
    }

    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    public void setBanderaEsPersonaNatural(boolean banderaEsPersonaNatural) {
        this.banderaEsPersonaNatural = banderaEsPersonaNatural;
    }

    public boolean isVerPanelResultados() {
        return verPanelResultados;
    }

    public void setVerPanelResultados(boolean verPanelResultados) {
        this.verPanelResultados = verPanelResultados;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on ConsultaInformacionEntidadesMB init");

        this.solicitantesEncontrados = new ArrayList<Solicitante>();

        this.filtroDatosConsultaSolicitante = new FiltroDatosConsultaSolicitante();
        this.filtroDatosConsultaSolicitante
            .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());

        this.banderaEsPersonaNatural = true;
        this.verPanelResultados = false;

        this.administracionInformacionSolicitanteMB =
            (AdministracionInformacionSolicitanteMB) UtilidadesWeb
                .getManagedBean("adminInfoSolicitante");
        this.administracionInformacionSolicitanteMB.cargarDesdeCU54(this.solicitantesEncontrados);
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia el tipo de persona. Activa la bandera de si es
     * persona natural y cambia el tipo de documento por defecto
     *
     * @author lorena.salamanca
     * @modified christian.rodriguez
     */
    public void onChangeTipoPersona() {

        if (this.filtroDatosConsultaSolicitante.getTipoPersona().equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {

            this.banderaEsPersonaNatural = true;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());

        } else {
            this.banderaEsPersonaNatural = false;
            this.filtroDatosConsultaSolicitante
                .setTipoIdentificacion(EPersonaTipoIdentificacion.NIT.getCodigo());
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener del botón de buscar, llama al método que busca los solicitantes de acuerdo al filtro
     * especificado por el usuario
     *
     * @author christian.rodriguez
     */
    public void buscar() {

        if (this.filtroDatosConsultaSolicitante != null) {

            this.solicitantesEncontrados = this.getTramiteService()
                .buscarSolicitantesPorFiltroConContratoInteradministrativo(
                    this.filtroDatosConsultaSolicitante);

            this.administracionInformacionSolicitanteMB
                .cargarDesdeCU54(this.solicitantesEncontrados);
        }
    }

    // --------------------------------------------------------------------------
    public String cerrar() {

        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;

    }

}
