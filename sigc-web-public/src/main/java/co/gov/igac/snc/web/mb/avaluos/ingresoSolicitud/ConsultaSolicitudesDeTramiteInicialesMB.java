package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Component("consultarSolicitudesDeTramiteIniciales")
@Scope("session")
public class ConsultaSolicitudesDeTramiteInicialesMB extends SNCManagedBean implements
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3192560069126536848L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaSolicitudesDeTramiteInicialesMB.class);

    private boolean renderPanelGridRadicacionesAsociadas;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @PostConstruct
    public void init() {
        LOGGER.debug("ConsultarSolicitudesDeTramiteInicialesMB#init");
        this.renderPanelGridRadicacionesAsociadas = false;

    }

    public void mostrarPanelRadicacionesAsociadas() {
        if (this.renderPanelGridRadicacionesAsociadas == false) {
            this.renderPanelGridRadicacionesAsociadas = true;
        } else if (this.renderPanelGridRadicacionesAsociadas == true) {
            this.renderPanelGridRadicacionesAsociadas = false;
        }
    }

    public boolean isRenderPanelGridRadicacionesAsociadas() {
        return renderPanelGridRadicacionesAsociadas;
    }

    public void setRenderPanelGridRadicacionesAsociadas(boolean renderPanelGridRadicacionesAsociadas) {
        this.renderPanelGridRadicacionesAsociadas = renderPanelGridRadicacionesAsociadas;
    }

    public String cerrar() {

        UtilidadesWeb.removerManagedBean("consultarSolicitudesDeTramiteIniciales");
        this.tareasPendientesMB.init();
        this.init();
        return "index";
    }

}
