package co.gov.igac.snc.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * DTO que contiene información asociada a documentos generados en el sistema, documentos subidos
 * por el usuario o reportes generados por la aplicación. Contiene un objeto File que es un File del
 * documento Contiene una urlWebReporte que es la url por donde se puede visualizar el documento
 * Contiene un StreamedContent que es un objeto para poder usar el componente FileDownload de prime
 * que sirve para descargar el documento a la máquina local Contiene una rutaCargarReporteAAlfresco
 * que es una ruta de la carpeta temporal local donde se encuentra el archivo para poder enviar como
 * parametro a los métodos que guardan los documentos en Alfresco Contiene un tipo Mime del reporte
 * que es el tipo mime del documento Contiene un nombre del reporte que es el nombre del documento
 *
 * @author javier.aponte
 *
 */
public class ReporteDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6522859901337210216L;

    /**
     * File asociado al documento, este file corresponde al archivo que se encuentra en la carpeta
     * temporal local
     */
    private File archivoReporte;
    /**
     * Es la url por donde se puede visualizar el documento, se usa en los visores de documentos
     */
    private String urlWebReporte;
    /**
     * Es un objeto para poder usar el componente FileDownload de prime que sirve para que el
     * usuario puedad descargar el documento a la máquina local
     */
    private StreamedContent streamedContentReporte;

    /**
     * Es una ruta de la carpeta temporal local donde se encuentra el archivo para poder enviar como
     * párametro a los métodos que guardan los documentos en Alfresco
     */
    private String rutaCargarReporteAAlfresco;

    /**
     * Tipo mime del documento
     */
    private String tipoMimeReporte;
    /**
     * Nombre del documeto
     */
    private String nombreReporte;

    //------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     */
    public ReporteDTO() {

    }
    //------------------------------------------------------------------------------------------------------

    /*
     * Getters y Setters
     */

    public File getArchivoReporte() {
        return archivoReporte;
    }

    public void setArchivoReporte(File archivoReporte) {
        this.archivoReporte = archivoReporte;
    }

    public String getUrlWebReporte() {
        return this.urlWebReporte;
    }

    public void setUrlWebReporte(String urlWebReporte) {
        this.urlWebReporte = urlWebReporte;
    }

    public StreamedContent getStreamedContentReporte() {

        if (this.archivoReporte != null) {
            InputStream stream;

            try {
                stream = new FileInputStream(this.archivoReporte);
                this.streamedContentReporte = new DefaultStreamedContent(stream,
                    this.tipoMimeReporte, this.nombreReporte);
            } catch (FileNotFoundException e) {
            }
        }

        return streamedContentReporte;
    }

    public void setStreamedContentReporte(StreamedContent streamedContentReporte) {
        this.streamedContentReporte = streamedContentReporte;
    }

    public String getRutaCargarReporteAAlfresco() {
        return this.rutaCargarReporteAAlfresco;
    }

    public void setRutaCargarReporteAAlfresco(String rutaCargarReporteAAlfresco) {
        this.rutaCargarReporteAAlfresco = rutaCargarReporteAAlfresco;
    }

    public String getTipoMimeReporte() {
        return tipoMimeReporte;
    }

    public void setTipoMimeReporte(String tipoMimeReporte) {
        this.tipoMimeReporte = tipoMimeReporte;
    }

    public String getNombreReporte() {
        return nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

}
