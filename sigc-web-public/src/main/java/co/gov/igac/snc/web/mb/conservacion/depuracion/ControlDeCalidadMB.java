/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracionObservacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.tramite.gestion.RecuperacionTramitesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.EActividadControlCalidad;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso Realizar control de calidad
 *
 * @author juanfelipe.garcia
 * @CU CU-NP-CO-206
 */
@Component("controlCalidad")
@Scope("session")
public class ControlDeCalidadMB extends ProcessFlowManager {

    private static final Logger LOGGER =
        LoggerFactory.getLogger(ControlDeCalidadMB.class);

    //------------------ services   ------
    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO usuario;

    /**
     * Tramite relacionado a la actividad actual
     */
    private Tramite tramiteSeleccionado;

    /**
     * Entidad de depuración asociada al trámite.
     */
    private TramiteDepuracion tramiteDepuracion;

    /**
     * Nombre de las variables necesarias para llamar al editor geográfico
     */
    private String parametroEditorGeografico;

    /**
     * Bandera para habilitar el avance del proceso
     */
    private boolean banderaAvanzarProceso;

    /**
     * variable que almacena las inconsistencias geográficas
     */
    private String inconsistenciasGeograficas;

    /**
     * variable que almacena las inconsistencias geográficas de la revisión
     */
    private String inconsistenciasGeograficasRevision;

    /**
     * variable que almacena las inconsistencias geográficas
     */
    private String tareasRealizarDepuracion;

    /**
     * Predios relacionados al tramite actual
     */
    private List<Predio> prediosTramite;

    /**
     * variable con el nombre de la actividad a mostrar en la pantalla
     */
    private String nombreActividad;

    /**
     * variable con el nombre del usuario de la actividad
     */
    private String nombreActividadUsuario;

    /**
     * Solicitud seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * observaciones
     */
    private List<TramiteDepuracionObservacion> observacionesRealizadas;

    //private ObservacionesDepuracionDTO observacionActual;
    private String aprueba;

    private boolean activarObservaciones;

    private String observacionesNuevas;

    private boolean habilitarBotonGuardar;

    private boolean habilitarActualizarInconsistencias;

    private boolean habilitarAvanzarProceso;

    /**
     * Lista de {@link TramiteInconsistencia} con las inconsistencias modificadas del
     * {@link Tramite}
     */
    private List<TramiteInconsistencia> inconsistenciasTramite;

    /**
     * Variable {@link TramiteInconsistencia} que almacena la inconsistencia a remover de la lista
     * de inconsistencias del {@link TramiteDepuracion}
     */
    private TramiteInconsistencia inconsistenciaAEliminar;

    /**
     * Variable {@link String} para cargar las razones del porque se ha enviado el trámite a
     * depuración geográfica.
     */
    private String razonezDepuracionGeografica;

    /**
     * Variable que almacena el último {@link TramiteDepuracion} registrado para el tramite
     * seleccionado.
     */
    private TramiteDepuracion ultimoTramiteDepuracion;

    /**
     * Lista de {@link TramiteInconsistencia} con las inconsistencias iniciales del {@link Tramite}
     */
    private List<TramiteInconsistencia> inconsistenciasTramiteIniciales;

    /**
     * Lista de {@link TramiteInconsistencia} de la revisión una vez que se ha enviado el
     * {@link Tramite} al editor geográfico.
     */
    private List<TramiteInconsistencia> inconsistenciasTramiteDeLaRevision;

    //---------------------Getters and Setters---------------------//
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Tramite getTramiteSeleccionado() {
        return tramiteSeleccionado;
    }

    public void setTramiteSeleccionado(Tramite tramiteSeleccionado) {
        this.tramiteSeleccionado = tramiteSeleccionado;
    }

    public TramiteDepuracion getTramiteDepuracion() {
        return tramiteDepuracion;
    }

    public void setTramiteDepuracion(TramiteDepuracion tramiteDepuracion) {
        this.tramiteDepuracion = tramiteDepuracion;
    }

    public String getParametroEditorGeografico() {
        return parametroEditorGeografico;
    }

    public void setParametroEditorGeografico(String parametroEditorGeografico) {
        this.parametroEditorGeografico = parametroEditorGeografico;
    }

    public boolean isBanderaAvanzarProceso() {
        return banderaAvanzarProceso;
    }

    public void setBanderaAvanzarProceso(boolean banderaAvanzarProceso) {
        this.banderaAvanzarProceso = banderaAvanzarProceso;
    }

    public String getInconsistenciasGeograficas() {
        return inconsistenciasGeograficas;
    }

    public void setInconsistenciasGeograficas(String inconsistenciasGeograficas) {
        this.inconsistenciasGeograficas = inconsistenciasGeograficas;
    }

    public String getTareasRealizarDepuracion() {
        return tareasRealizarDepuracion;
    }

    public void setTareasRealizarDepuracion(String tareasRealizarDepuracion) {
        this.tareasRealizarDepuracion = tareasRealizarDepuracion;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public List<Predio> getPrediosTramite() {
        return prediosTramite;
    }

    public void setPrediosTramite(List<Predio> prediosTramite) {
        this.prediosTramite = prediosTramite;
    }

    public Solicitud getSolicitudSeleccionada() {
        return this.solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;

        if (this.solicitudSeleccionada != null && this.solicitudSeleccionada.getId() != null) {
            this.solicitudSeleccionada = this.getTramiteService()
                .buscarSolicitudFetchTramitesBySolicitudId(this.solicitudSeleccionada.getId());
        }
    }

    public List<TramiteDepuracionObservacion> getObservacionesRealizadas() {
        return observacionesRealizadas;
    }

    public void setObservacionesRealizadas(
        List<TramiteDepuracionObservacion> observacionesRealizadas) {
        this.observacionesRealizadas = observacionesRealizadas;
    }

    public String getAprueba() {
        return aprueba;
    }

    public void setAprueba(String aprueba) {
        this.aprueba = aprueba;
    }

    public boolean isActivarObservaciones() {
        return activarObservaciones;
    }

    public void setActivarObservaciones(boolean activarObservaciones) {
        this.activarObservaciones = activarObservaciones;
    }

    public String getObservacionesNuevas() {
        return observacionesNuevas;
    }

    public void setObservacionesNuevas(String observacionesNuevas) {
        this.observacionesNuevas = observacionesNuevas;
    }

    public boolean isHabilitarBotonGuardar() {
        return habilitarBotonGuardar;
    }

    public void setHabilitarBotonGuardar(boolean habilitarBotonGuardar) {
        this.habilitarBotonGuardar = habilitarBotonGuardar;
    }

    public boolean isHabilitarActualizarInconsistencias() {
        return habilitarActualizarInconsistencias;
    }

    public void setHabilitarActualizarInconsistencias(boolean habilitarActualizarInconsistencias) {
        this.habilitarActualizarInconsistencias = habilitarActualizarInconsistencias;
    }

    public boolean isHabilitarAvanzarProceso() {
        return habilitarAvanzarProceso;
    }

    public void setHabilitarAvanzarProceso(boolean habilitarAvanzarProceso) {
        this.habilitarAvanzarProceso = habilitarAvanzarProceso;
    }

    public String getInconsistenciasGeograficasRevision() {
        return inconsistenciasGeograficasRevision;
    }

    public void setInconsistenciasGeograficasRevision(String inconsistenciasGeograficasRevision) {
        this.inconsistenciasGeograficasRevision = inconsistenciasGeograficasRevision;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramite() {
        return inconsistenciasTramite;
    }

    public void setInconsistenciasTramite(List<TramiteInconsistencia> inconsistenciasTramite) {
        this.inconsistenciasTramite = inconsistenciasTramite;
    }

    public TramiteInconsistencia getInconsistenciaAEliminar() {
        return inconsistenciaAEliminar;
    }

    public void setInconsistenciaAEliminar(TramiteInconsistencia inconsistenciaAEliminar) {
        this.inconsistenciaAEliminar = inconsistenciaAEliminar;
    }

    public String getRazonezDepuracionGeografica() {
        return razonezDepuracionGeografica;
    }

    public void setRazonezDepuracionGeografica(String razonezDepuracionGeografica) {
        this.razonezDepuracionGeografica = razonezDepuracionGeografica;
    }

    public TramiteDepuracion getUltimoTramiteDepuracion() {
        return ultimoTramiteDepuracion;
    }

    public void setUltimoTramiteDepuracion(TramiteDepuracion ultimoTramiteDepuracion) {
        this.ultimoTramiteDepuracion = ultimoTramiteDepuracion;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramiteIniciales() {
        return inconsistenciasTramiteIniciales;
    }

    public void setInconsistenciasTramiteIniciales(
        List<TramiteInconsistencia> inconsistenciasTramiteIniciales) {
        this.inconsistenciasTramiteIniciales = inconsistenciasTramiteIniciales;
    }

    public List<TramiteInconsistencia> getInconsistenciasTramiteDeLaRevision() {
        return inconsistenciasTramiteDeLaRevision;
    }

    public void setInconsistenciasTramiteDeLaRevision(
        List<TramiteInconsistencia> inconsistenciasTramiteDeLaRevision) {
        this.inconsistenciasTramiteDeLaRevision = inconsistenciasTramiteDeLaRevision;
    }

    public String getNombreActividadUsuario() {
        return nombreActividadUsuario;
    }

    public void setNombreActividadUsuario(String nombreActividadUsuario) {
        this.nombreActividadUsuario = nombreActividadUsuario;
    }

    @PostConstruct
    public void init() {

        LOGGER.debug("en ControlDeCalidadMB#init ");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.tramiteSeleccionado = new Tramite();
        this.banderaAvanzarProceso = false;
        this.activarObservaciones = false;
        this.habilitarBotonGuardar = false;
        this.habilitarActualizarInconsistencias = false;
        this.habilitarAvanzarProceso = false;

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        Long idtramiteActual = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();

        //obtener datos de la actividad
        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            idtramiteActual);

        this.tramiteSeleccionado.setId(idtramiteActual);

        this.cargarDatosTramite();

        this.cargarDatosDepuracionDelTramite();

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método para cargar los datos relacionados al tramite que se deben mostrar.
     */
    public void cargarDatosTramite() {
        LOGGER.debug("Cargando datos tramite...");

        this.tramiteSeleccionado = this.getTramiteService().buscarTramitePorTramiteIdAsignacion(
            this.tramiteSeleccionado.getId());

        this.setPrediosTramite(this.tramiteSeleccionado.getPredios());

        this.solicitudSeleccionada = this.tramiteSeleccionado.getSolicitud();

        this.parametroEditorGeografico = this.usuario.getLogin() + "," + this.tramiteSeleccionado.
            getId() + "," + UtilidadesWeb.obtenerNombreConstante(ProcesoDeConservacion.class,
                this.currentProcessActivity.getNombre());
        LOGGER.debug("Fin carga datos...");
    }

    /**
     * Método que realiza el cargue las inconsistencias geográficas y razones de depuración
     * existentes. El cargue de las inconsistencias se realiza a partir del {@link Tramite}.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public void cargarDatosDepuracionDelTramite() {

        this.observacionesRealizadas = new ArrayList<TramiteDepuracionObservacion>();
        this.razonezDepuracionGeografica = "";

        // Cargar razones de depuración y las inconsistencias geográficas.
        if (this.tramiteSeleccionado.getTramiteDepuracions() != null &&
             !this.tramiteSeleccionado.getTramiteDepuracions().isEmpty()) {

            this.ultimoTramiteDepuracion = this.getTramiteService().
                buscarUltimoTramiteDepuracionPorIdTramite(this.tramiteSeleccionado.getId());
            // Cargar razones de depuración.
            this.razonezDepuracionGeografica = this.ultimoTramiteDepuracion
                .getRazonesSolicitudDepuracion();
            // Cargar observaciones
            this.observacionesRealizadas = this.ultimoTramiteDepuracion
                .getTramiteDepuracionObservacions();
        }

        //cargar nombre y actividad del usuario depuración
        if (this.ultimoTramiteDepuracion.getNombreDigitalizador() != null) {
            this.nombreActividad = EActividadControlCalidad.DIGITALIZADOR.getNombre();
            this.nombreActividadUsuario = this.ultimoTramiteDepuracion.getNombreDigitalizador();
        } else if (this.ultimoTramiteDepuracion.getNombreEjecutor() != null) {
            this.nombreActividad = EActividadControlCalidad.EJECUTOR.getNombre();
            this.nombreActividadUsuario = this.ultimoTramiteDepuracion.getNombreEjecutor();
        } else if (this.ultimoTramiteDepuracion.getNombreTopografo() != null) {
            this.nombreActividad = EActividadControlCalidad.TOPOGRAFO.getNombre();
            this.nombreActividadUsuario = this.ultimoTramiteDepuracion.getNombreTopografo();
        }

        // Set de las inconsistencias geográficas del trámite.
        this.inconsistenciasTramite = new ArrayList<TramiteInconsistencia>();
        for (TramiteInconsistencia ti : this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.tramiteSeleccionado.getId())) {
            if (!ti.getDepurada().equals("CC")) {
                this.inconsistenciasTramite.add(ti);
            }
        }

        List<TramiteInconsistencia> tis = this.getTramiteService().
            buscarTramiteInconsistenciaPorTramiteId(this.tramiteSeleccionado.getId());
        if (tis != null &&
             !tis.isEmpty()) {

            this.inconsistenciasTramiteIniciales = tis;

        }

        // TODO :: juanFelipe.garcia :: Verificar el cargué de esta variable una
        // vez se ha regresado del editor.
        this.inconsistenciasTramiteDeLaRevision = new ArrayList<TramiteInconsistencia>();
        LOGGER.debug("Fin carga datos...");
    }

    // ------------------------------------------------ //
    /**
     * Método que realiza la consulta de las inconsistencias de la revisión, luego de terminar la
     * edición geográfica
     *
     * @author juanfelipe.garcia
     */
    public void cargarInconsistenciasDeLaRevision() {
        this.inconsistenciasTramiteDeLaRevision = new ArrayList<TramiteInconsistencia>();
        try {
            List<TramiteInconsistencia> tiList = this.getTramiteService().
                buscarTramiteInconsistenciaPorTramiteId(this.tramiteSeleccionado.getId());
            for (TramiteInconsistencia ti : tiList) {
                if (ti.getDepurada().equals("CC")) {
                    this.inconsistenciasTramiteDeLaRevision.add(ti);
                }
            }
        } catch (Exception e) {
            this.addMensajeError(
                "Ocurrió un error al cargar las inconsistencias de la revisión, por favor intente nuevamente");
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que realiza el set de las variables boolenas usadas para la activación y
     * deshabilitación de campos y botones en la pantalla de control de calidad.
     *
     * @author juanfelipe.garcia
     */
    public void apruebaControlListener() {

        if (this.aprueba.equals("NO")) {
            this.activarObservaciones = true;
            this.habilitarBotonGuardar = true;
            this.habilitarActualizarInconsistencias = false;
            this.habilitarAvanzarProceso = true;
        } else {
            //Se valida que la copia geografica
            this.habilitarAvanzarProceso = false;
            GeneralMB generalMB = (GeneralMB) UtilidadesWeb.getManagedBean("general");
            if (!generalMB.validarExistenciaCopiaFinalEdicion(this.tramiteSeleccionado)) {
                return;
            }
            this.observacionesNuevas = "";
            this.activarObservaciones = false;
            this.habilitarBotonGuardar = false;
            this.habilitarActualizarInconsistencias = true;

        }
    }

    // ------------------------------------------------ //
    /**
     * Método que guarda las observaciones nuevas ingresadas en la pantalla de control de calidad de
     * depuración.
     *
     * @author juanfelipe.garcia
     */
    public void guardarObservaciones() {
        if (this.observacionesNuevas == null || this.observacionesNuevas.isEmpty()) {
            this.addMensajeError(
                "Debe ingresar observaciones para explicar porque no aprueba el control de calidad");
            return;
        }

        try {
            TramiteDepuracionObservacion tdo = new TramiteDepuracionObservacion();
            tdo.setApruebaControlCalidad(aprueba);
            tdo.setFecha(new Date());
            tdo.setFechaLog(new Date());
            long num = this.observacionesRealizadas.size() + 1;
            tdo.setNumeroRevision(num);
            tdo.setObservacion(observacionesNuevas);
            tdo.setUsuarioLog(this.usuario.getLogin());
            tdo.setTramiteDepuracion(this.ultimoTramiteDepuracion);

            this.observacionesRealizadas.add(tdo);
            this.ultimoTramiteDepuracion.setTramiteDepuracionObservacions(observacionesRealizadas);

            this.ultimoTramiteDepuracion = this.getTramiteService().actualizarTramiteDepuracion(
                this.ultimoTramiteDepuracion);

            this.ultimoTramiteDepuracion = this.getTramiteService().
                buscarUltimoTramiteDepuracionPorIdTramite(this.tramiteSeleccionado.getId());
            this.observacionesRealizadas = this.ultimoTramiteDepuracion.
                getTramiteDepuracionObservacions();
            this.observacionesNuevas = "";
            this.addMensajeInfo("Se guardaron las observaciones satisfactoriamente");

        } catch (Exception e) {
            LOGGER.error(
                "Error al almacenar las observaciones del trámite: ControlDeCalidadMB#guardarObservaciones",
                e);
            this.addMensajeError("Error al actualizar las observaciones del trámite");
        }

    }

    private void forwardThisProcess() {
        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();
        }
        if (this.aprueba.equals(ESiNo.NO.getCodigo())) {
            super.forwardProcess();
        }
        //Se a movido el tramite a la actividad correspondiente de forma  exitosa.
    }

    @Implement
    @Override
    public boolean validateProcess() {
        return true;
    }

    @Implement
    @Override
    public void setupProcessMessage() {
        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        List<UsuarioDTO> usuariosActividad;
        SolicitudCatastral solicitudCatastral;
        UsuarioDTO usuarioDestinoActividad;

        solicitudCatastral = new SolicitudCatastral();
        usuarioDestinoActividad = new UsuarioDTO();

        usuariosActividad = new ArrayList<UsuarioDTO>();

        messageDTO = new ActivityMessageDTO();
        messageDTO.setUsuarioActual(this.usuario);
        messageDTO.setActivityId(this.currentProcessActivity.getId());

        if (this.ultimoTramiteDepuracion.getEjecutor() != null &&
             !this.ultimoTramiteDepuracion.getEjecutor().isEmpty()) {

        }

        if (this.aprueba.equals(ESiNo.NO.getCodigo())) {
            if (this.ultimoTramiteDepuracion.getEjecutor() != null &&
                 !this.ultimoTramiteDepuracion.getEjecutor().isEmpty()) {
                processTransition =
                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR;//Realizar visita a terreno y verificar información espacial
                usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                    this.ultimoTramiteDepuracion.getEjecutor());
            } else if (this.ultimoTramiteDepuracion.getTopografo() != null &&
                 !this.ultimoTramiteDepuracion.getTopografo().isEmpty()) {
                processTransition =
                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_TOPOGRAFIA;//Ajustar información espacial por topografía
                usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                    this.ultimoTramiteDepuracion.getTopografo());
            } else if (this.ultimoTramiteDepuracion.getDigitalizador() != null &&
                 !this.ultimoTramiteDepuracion.getDigitalizador().isEmpty()) {
                processTransition =
                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION;//Ajustar información espacial por ejecutor
                usuarioDestinoActividad = this.getGeneralesService().getCacheUsuario(
                    this.ultimoTramiteDepuracion.getDigitalizador());
            }

            observaciones = "";
            usuariosActividad.add(usuarioDestinoActividad);
        } else if (this.aprueba.equals(ESiNo.SI.getCodigo())) {

            //this.getConservacionService().aplicarCambiosDepuracion(this.tramiteSeleccionado.getId(), this.usuario);
            //Se modifica la forma de enviar los tramites a aplicar cambios de depuracion
            Actividad actTramite = this.tareasPendientesMB.buscarActividadPorIdTramiteSeleccionadas(
                this.tramiteSeleccionado.getId());
            if (actTramite == null) {
                actTramite = this.tareasPendientesMB.buscarActividadPorIdTramite(
                    this.tramiteSeleccionado.getId());
            }

            //felipe.cadena::#18380::06-09-2016::Se asigna el estado GDB solo para los tramites que llegaron desde informacion alfanumerica a depuracion
            if (this.tramiteSeleccionado.isTramiteGeografico() &&
                actTramite.getObservacion().equals(
                    ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA.toString())) {
                this.tramiteSeleccionado.setEstadoGdb(
                    Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
                this.getTramiteService().actualizarTramite2(this.tramiteSeleccionado, this.usuario);
            }

            this.tramiteSeleccionado.setActividadActualTramite(actTramite);
            RecuperacionTramitesMB recuperarTamitesMB = (RecuperacionTramitesMB) UtilidadesWeb.
                getManagedBean("recuperarTramites");
            recuperarTamitesMB.aplicarCambiosDepuracion(this.tramiteSeleccionado, this.usuario, 0);

        }
        messageDTO.setComment(observaciones);
        //D: según alejandro.sanchez la transición que importa es la que se define en la
        //   solicitud catastral; sin embargo, por si acaso se hace también en el messageDTO
        messageDTO.setTransition(processTransition);
        solicitudCatastral.setTransicion(processTransition);
        solicitudCatastral.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
        solicitudCatastral.setUsuarios(usuariosActividad);
        messageDTO.setSolicitudCatastral(solicitudCatastral);

        Calendar cal = Calendar.getInstance();
        cal.setTime(this.tramiteSeleccionado.getFechaRadicacion());
        messageDTO.getSolicitudCatastral().setNumeroRadicacion(this.tramiteSeleccionado.
            getNumeroRadicacion());
        messageDTO.getSolicitudCatastral().setFechaRadicacion(cal);
        messageDTO.getSolicitudCatastral().setTipoTramite(this.tramiteSeleccionado.
            getTipoTramiteCadenaCompleto());

        this.setMessage(messageDTO);
    }

    @Implement
    @Override
    public void doDatabaseStatesUpdate() {
        //this.getTramiteService().actualizarTramite(this.tramiteSeleccionado, this.usuario);
    }

    // ------------------------------------------------ //
    /**
     * Método que remueve la inconsistencia seleccionada de la lista de
     * {@link TramiteInconsistencia} cargada con las inconsistencias del {@link TramiteDepuracion}.
     *
     * @author david.cifuentes
     */
    public void eliminarInconsistencia() {
        if (this.inconsistenciaAEliminar != null) {
            this.inconsistenciasTramite.remove(this.inconsistenciaAEliminar);
            this.inconsistenciasTramiteIniciales.remove(this.inconsistenciaAEliminar);
            this.getTramiteService().eliminarTramiteInconsistencia(this.inconsistenciaAEliminar);
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que guarda las inconsistencias actualizadas y las asocia al trámtie depuracion. nuevas
     * ingresadas en la pantalla de control de calidad de depuración.
     *
     * @author david.cifuentes
     */
    public void guardarInconsistencias() {
        try {
            // Set de las inconsistencias
            this.tramiteSeleccionado
                .setTramiteInconsistencias(this.inconsistenciasTramite);

            // Actualización del trámite con sus respectivas inconsistencias.
            this.getTramiteService().actualizarTramite(this.tramiteSeleccionado);

            // Consulta del trámite actualizado.
            this.tramiteSeleccionado = this.getTramiteService()
                .findTramitePruebasByTramiteId(this.tramiteSeleccionado.getId());

            // Se actualizan nuevamente las inconsistencias del trámite
            this.inconsistenciasTramiteIniciales = new ArrayList<TramiteInconsistencia>();
            for (TramiteInconsistencia ti : this.tramiteSeleccionado
                .getTramiteInconsistencias()) {
                this.inconsistenciasTramiteIniciales
                    .add((TramiteInconsistencia) ti.clone());
            }

            this.addMensajeInfo("Se guardaron las inconsistencias satisfactoriamente");
            this.aprueba = ESiNo.SI.getCodigo();
            this.apruebaControlListener();
            this.habilitarAvanzarProceso = true;

            // Set de las inconsistencias geográficas del trámite.
            this.inconsistenciasTramite = new ArrayList<TramiteInconsistencia>();
            for (TramiteInconsistencia ti : this.getTramiteService()
                .buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(
                    this.tramiteSeleccionado.getId())) {
                if (!ti.getDepurada().equals("CC")) {
                    this.inconsistenciasTramite.add(ti);
                }
            }

            List<TramiteInconsistencia> tis = this.getTramiteService()
                .buscarTramiteInconsistenciaPorTramiteId(
                    this.tramiteSeleccionado.getId());
            if (tis != null && !tis.isEmpty()) {

                this.inconsistenciasTramiteIniciales = tis;
            }

        } catch (Exception e) {
            LOGGER.error(
                "Error al almacenar las inconsistencias del trámite: ActualizacionInconsistenciaGeograficasMB#guardarInconsistencias",
                e);
            this.addMensajeError("Error al actualizar las inconsistencias del trámite");
        }
    }

    // ------------------------------------------------ //
    /**
     * Método para retornar al arbol de tareas, action del botón "cerrar" que forza el init del MB.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public String cerrar() {
        TareasPendientesMB tareasPendientesMB = (TareasPendientesMB) UtilidadesWeb
            .getManagedBean("tareasPendientes");
        tareasPendientesMB.cerrar();
        return ConstantesNavegacionWeb.INDEX;
    }

    // ------------------------------------------------ //
    /**
     * Listener ejecutado en el cambio de tab en la página de control de calidad
     *
     * @author david.cifuentes
     * @param event
     */
    public void onTabChangeControlCalidad(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (tabId.equals("tabActualizarInconsistencias")) {
            this.inconsistenciasTramite = new ArrayList<TramiteInconsistencia>();
            if (this.inconsistenciasTramiteIniciales != null) {
                this.inconsistenciasTramite
                    .addAll(this.inconsistenciasTramiteIniciales);
            }
        }
    }

    // ------------------------------------------------ //
    /**
     * Método que avanza el proceso al momento de actualizar las inconsistencias geográficas de los
     * predios asociados a un trámite.
     *
     * @author david.cifuentes
     * @return
     */
    /*
     * @modified by leidy.gonzalez :: #11962 :: 06-04-2015 Se elimina validaciòn, si aprueba es
     * igual a si, debido a que no es un campo que se almacene en Base de Datos y cuando va a ser
     * avanzado el tramite inmediatamente no encuentra este valor.
     */
    public String avanzarProcesoControlCalidad() {
        try {

            List<ProductoCatastralJob> jobs = this.getConservacionService().
                obtenerJobsGeograficosPorTramiteTipoEstado(this.tramiteSeleccionado.getId(),
                    ProductoCatastralJob.SIG_JOB_FINALIZAR_EDICION.toString(), null);

            ProductoCatastralJob jobVigente;

            if (jobs != null) {
                if (!jobs.isEmpty()) {
                    if (jobs.size() == 1) {
                        jobVigente = jobs.get(0);
                    } else {
                        jobVigente = jobs.get(0);
                        for (ProductoCatastralJob productoCatastralJob : jobs) {
                            if (productoCatastralJob.getFechaLog().after(jobVigente.getFechaLog())) {
                                jobVigente = productoCatastralJob;
                            }
                        }
                    }

                    if (jobVigente.getEstado().equals(ProductoCatastralJob.AGS_ESPERANDO.toString()) ||
                         jobVigente.getEstado().equals(ProductoCatastralJob.AGS_EN_EJECUCION.
                            toString()) ||
                         jobVigente.getEstado().
                            equals(ProductoCatastralJob.AGS_TERMINADO.toString()) ||
                         jobVigente.getEstado().equals(ProductoCatastralJob.SNC_EN_EJECUCION.
                            toString())) {
                        this.addMensajeError(
                            "El trámite ya esta en proceso de aplicar cambios en estado : " +
                            jobVigente.getEstado());
                        return "";
                    }

                    if (jobVigente.getEstado().equals(ProductoCatastralJob.AGS_ERROR.toString()) ||
                         jobVigente.getEstado().equals(ProductoCatastralJob.SNC_ERROR.toString())) {
                        this.addMensajeError(
                            "El trámite presento un error en la aplicacion de cambios: " +
                            jobVigente.getEstado());
                        return "";
                    }
                }
            }

            this.forwardThisProcess();

            UtilidadesWeb.removerManagedBeans();
            this.tareasPendientesMB.init();
            return ConstantesNavegacionWeb.INDEX;

        } catch (Exception e) {
            LOGGER.error(
                "Error al avanzar el proceso en ActualizacionInconsistenciaGeograficasMB#avanzarProcesoActualizarInconsistencias",
                e);
            this.addMensajeError("No fue posible avanzar el proceso para el trámite seleccionado.");
            return null;
        }
    }

    //end of class
}
