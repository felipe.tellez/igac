package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.snc.dao.util.EProcedimientoAlmacenadoFuncion;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredioTerreno;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizTorre;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioDireccion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;
import co.gov.igac.snc.persistence.util.EFichaMatrizEstado;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioEstado;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoInscripcion;
import co.gov.igac.snc.persistence.util.IProyeccionObject;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.conservacion.RectificacionComplementacionMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ESeccionEjecucion;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.Map;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author fabio.navarrete
 *
 */
/*
 * @modified pedro.garcia Todo lo que tiene que ver con trámites de quinta (nuevo y omitido)
 */
@SuppressWarnings("deprecation")
@Component("entradaProyeccion")
@Scope("session")
public class EntradaProyeccionMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 2184496880277340270L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EntradaProyeccionMB.class);

    //private PPredio predioSeleccionado;
    private PPredio predioResultanteSeleccionado;
    private List<SelectItem> condicionesPropiedadFiltradas;
    private List<SelectItem> condicionesPropiedadDesenglobeManzana;
    private PPredio predioNuevo;
    private List<PPredio> prediosResultantes;
    private List<PPredio> prediosResultantesCancelacionMasiva;
    private Predio predioPrincipalEnglobe;
    private List<PPredio> prediosResultantesSeleccionados;

    /**
     * Variable que contiene los predios resultante para los trámites de tercera o rectificación
     * másivos
     */
    private List<PPredio> prediosResultantesTerceraRectificacionMasiva;

    @Autowired
    private ProyectarConservacionMB proyectarConservacionMB;

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private CombosDeptosMunisMB combosDeptosMunis;

    private boolean mantieneNumeroPredial;
    private boolean adicionarPredioHabilitado;

    private List<SelectItem> zonasSelectItems;

    private List<SelectItem> sectoresSelectItems;

    private List<SelectItem> barriosSelectItems;

    private List<SelectItem> manzanasSelectItems;

    private List<SelectItem> comunasSelectItems;

    private ArrayList<SelectItem> terrenoItemList;

    private EOrden ordenTerreno;

    /**
     * lista de items del combo de selección de número de edificio o torre
     */
    private List<SelectItem> noEdificioSelectItems;

    private List<SelectItem> pisoSelectItems;

    private List<SelectItem> unidadSelectItems;

    /**
     * lista de items del combo de selección de número de terreno
     */
    private List<SelectItem> terrenoSelectItems;

    private boolean selectAllResultantes;

    private boolean selectAllResultantesManzanas;

    private String condicionPropiedadSeleccionada;

    /*
     * variables que guardan datos del PPredio
     */
    // Atributos temporales mientras se separan los campos en la base de datos,
    // los dos se almacenan en barrio y cuentan con la modificación en el setter
    private String comuna;
    private String barrio;

    private String zona;
    private String sector;
    private String manzana;
    private String edificioTorre;
    private String terreno;

    /**
     * Se utiliza para determinar si se van a crear manzanas en el desenglobe
     */
    private boolean creaManzanas;

    //-------- banderas ----------
    /**
     * Bandera que se activa sobre la selección de una manzana valida en la ventana datos predio
     */
    private boolean banderaManzanaSeleccionada;

    /**
     * bandera para indicar si el campo 'terreno' debe ser digitado
     */
    private boolean mustEnterDataTerreno;

    /**
     * indica si el edificio o torre va a ser seleccionado de un combo
     */
    private boolean mustSelectEdificioTorrePredioNuevo;

    /**
     * indica si el piso va a ser seleccionado de un combo
     */
    private boolean mustSelectPisoPredioNuevo;

    /**
     * indica si se debe ingresar un predio nuevo cuando el trámite es de quinta omitido
     */
    private boolean ingresarPredioNuevoOmitido;

    /**
     * indica si ya se seleccionó una condición de propiedad
     */
    private boolean condPropWasSelected;

    /**
     * indica si el predio que se va a usar para la proyección en un trámite de quinta omitido es el
     * que se encontró como omitido (cancelado). Se pone en true cuando se buscan los omitidos y se
     * encuentra alguno con el número predial asociado al trámite
     */
    private boolean usePredioOmitido;

    /**
     * Define si se debe activar el botón de aceptar la creación de predio nuevo en trámite de
     * quinta. Por defecto está en true, pero si ocurre alguna de las condiciones para mostrar error
     * en la pantalla, se pone en false;
     */
    private boolean okToAddAcceptPredioNuevoQuinta;

    /**
     * datos del combo de codición de propiedad
     */
    private ArrayList<SelectItem> condicionPropiedadSelectItems;

    /**
     * guarda el Predio que se encontró en la búsqueda de predios omitidos para que pueda ser usado
     * como Predio del trámite que se actualiza antes de generar la proyección de éste (trámite)
     * para obtener el PPredio sobre el que se trabaja
     */
    private Predio predioOmitidoEncontrado;

    private UsuarioDTO loggedInUser;

    @Autowired
    private IContextListener contextoWeb;

    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String manzanaCodigo;

    /**
     * Variable usada para indicar si dentro de los predios resultantes seleccionados se encuentra
     * seleccionada la ficha matriz.
     */
    private boolean selectedPredioFichaMatrizBool;

    /**
     * Variable usada para determinar si la proyección se realizó satisfactoriamente o si se cerro
     * la ventana de proyección del predio antes de culminarse la proyeción.
     */
    private boolean proyeccionValidaBool;

    /**
     * Usada para deshabilitar la selección de condición de propiedad cuando en un englobe se tiene
     * un predio condición 0 y uno condición 5 (Mejora)
     */
    private boolean limitadoACondicion0;

    /**
     * Bandera usada para limitar las acciones cuando la condicion de propiedad es 0
     */
    private boolean condicion0selected;

    /**
     * Bandera usada para limitar las acciones cuando la condicion de propiedad es 5
     */
    private boolean condicion5selected;

    /**
     * Modelo seleccionado para asignarlo a los predios seleccionados
     */
    private Long modeloSeleccionado;

    /**
     * Listado de predios que pueden llegar a ser predio principal
     */
    private List<Predio> prediosEnglobe;

    /**
     * Cantidad de manzanas que se van a generar
     */
    private Integer cantidadPrediosAGenerar;

    /**
     * Lista para utilizar cuando se desengloban manzanas y se generan varios predios a la vez
     */
    private List<PPredio> prediosNuevosAGenerar;

    /**
     * Manzanas para asociar al los predios.
     */
    private List<SelectItem> manzanasDisponibles;

    /**
     * Mostrar la cantidad de predios a generar
     */
    private boolean verCantidadPredios;

    /**
     * Manzana original del desenglobe.
     */
    private String nManzanaInicial;

    /**
     * Muestra la opcion de mantener el numero predial
     */
    private boolean mostrarMantieneNumeroPredial;

    /**
     * Lista de manzanas para el desenglobe de manzanas
     */
    private List<PManzanaVereda> manzanasDesenglobe;

    /**
     * Determina si se habilita la opcion de retirar manzanas.
     */
    private boolean retirarManzana;

    /**
     * Determina si ya se puede avanzar el tramite en un desenglobe de manzanas
     */
    private boolean envioDesenglobeManzanas;

    /**
     * Cantidad de manzanas que se van a generar
     */
    private Integer manzanasAGenerar;

    //desenglobes por etapas
    private int nPrediosSeleccionados;
    private boolean borrarDatoMasivo;

    private String destinoEtapas;
    private String tipoPredioEtapas;

    private String circuloRegistralEtapas;
    private int numeroRegistroInicial;
    private int numeroRegistrofinal;
    private int incrementeoRegistro;

    private boolean banderaIncremento;

    private PPredioDireccion direccionEtapas;

    /**
     * Variable que almacena el número predial para filtrar en la tabla
     * pPrediosTerceraRectificacionMasivos
     */
    private String numeroPredialBusquedaTerceraRectificacionMasiva;

    /**
     * Bandera para determinar si ya se realizo la cancelacion de los predios en la proyeccion.
     */
    private boolean banderaCancelacionMasivaAprobado;

    /**
     * banderas activacion de botones cancelacion masiva
     */
    private boolean banderaExistenInconsistencias;
    private boolean banderaEnviarDepuracion;
    private boolean banderaEnviarGeografica;
    private boolean banderaCoordinador;
    private boolean banderaRetirarPrediosBase;
    private boolean banderaRetirarPrediosResultantes;
    private boolean banderaActivaCancelar;
    /**
     * Predios seleccionados de la seccion predios a cancelar.
     */
    List<PPredio> prediosCancelarSeleccionados;
    /**
     * Predio seleccionado de la seccion predios a cancelar.
     */
    PPredio predioCancelarSeleccionado;

    /**
     * atributos asociados a desenglobes virtuales
     */
    private String nuevaFichaMatriz;
    private String tipoFichaMatriz;
    private String numeroFichaMatriz;
    private SelectItem numeroFichaMatrizSel;
    private String englobeVirtual;

    private boolean banderaEnglobeVirtualAprobado;

    private PFichaMatrizPredioTerreno predioARetirar;

    private List<SelectItem> tiposFicha;

    private List<SelectItem> numerosFichaDisponibles;

    private boolean banderaHabilitaGuardar;

    private boolean quintaNuevoCond5;

//-------------------      methods    -----------------------------------------------------
    public boolean isQuintaNuevoCond5() {
        return quintaNuevoCond5;
    }

    public void setQuintaNuevoCond5(boolean quintaNuevoCond5) {
        this.quintaNuevoCond5 = quintaNuevoCond5;
    }

    public boolean isCondicion5selected() {
        return condicion5selected;
    }

    public void setCondicion5selected(boolean condicion5selected) {
        this.condicion5selected = condicion5selected;
    }

    public ArrayList<SelectItem> getTerrenoItemList() {
        return terrenoItemList;
    }

    public void setTerrenoItemList(ArrayList<SelectItem> terrenoItemList) {
        this.terrenoItemList = terrenoItemList;
    }

    public EOrden getOrdenTerreno() {
        return ordenTerreno;
    }

    public void setOrdenTerreno(EOrden ordenTerreno) {
        this.ordenTerreno = ordenTerreno;
    }

    public int getNumeroRegistrofinal() {
        return numeroRegistrofinal;
    }

    public void setNumeroRegistrofinal(int numeroRegistrofinal) {
        this.numeroRegistrofinal = numeroRegistrofinal;
    }

    public boolean isBanderaHabilitaGuardar() {
        return banderaHabilitaGuardar;
    }

    public void setBanderaHabilitaGuardar(boolean banderaHabilitaGuardar) {
        this.banderaHabilitaGuardar = banderaHabilitaGuardar;
    }

    public PPredioDireccion getDireccionEtapas() {
        return direccionEtapas;
    }

    public void setDireccionEtapas(PPredioDireccion direccionEtapas) {
        this.direccionEtapas = direccionEtapas;
    }

    public boolean isBanderaEnviarGeografica() {
        return banderaEnviarGeografica;
    }

    public List<SelectItem> getTiposFicha() {
        return tiposFicha;
    }

    public void setTiposFicha(List<SelectItem> tiposFicha) {
        this.tiposFicha = tiposFicha;
    }

    public SelectItem getNumeroFichaMatrizSel() {
        return numeroFichaMatrizSel;
    }

    public void setNumeroFichaMatrizSel(SelectItem numeroFichaMatrizSel) {
        this.numeroFichaMatrizSel = numeroFichaMatrizSel;
    }

    public List<SelectItem> getNumerosFichaDisponibles() {
        return numerosFichaDisponibles;
    }

    public void setNumerosFichaDisponibles(List<SelectItem> numerosFichaDisponibles) {
        this.numerosFichaDisponibles = numerosFichaDisponibles;
    }

    public PFichaMatrizPredioTerreno getPredioARetirar() {
        return predioARetirar;
    }

    public void setPredioARetirar(PFichaMatrizPredioTerreno predioARetirar) {
        this.predioARetirar = predioARetirar;
    }

    public String getNuevaFichaMatriz() {
        return nuevaFichaMatriz;
    }

    public void setNuevaFichaMatriz(String nuevaFichaMatriz) {
        this.nuevaFichaMatriz = nuevaFichaMatriz;
    }

    public String getTipoFichaMatriz() {
        return tipoFichaMatriz;
    }

    public void setTipoFichaMatriz(String tipoFichaMatriz) {
        this.tipoFichaMatriz = tipoFichaMatriz;
    }

    public String getNumeroFichaMatriz() {
        return numeroFichaMatriz;
    }

    public void setNumeroFichaMatriz(String numeroFichaMatriz) {
        this.numeroFichaMatriz = numeroFichaMatriz;
    }

    public String getEnglobeVirtual() {
        return englobeVirtual;
    }

    public void setEnglobeVirtual(String englobeVirtual) {
        this.englobeVirtual = englobeVirtual;
    }

    public boolean isBanderaEnglobeVirtualAprobado() {
        return banderaEnglobeVirtualAprobado;
    }

    public void setBanderaEnglobeVirtualAprobado(boolean banderaEnglobeVirtualAprobado) {
        this.banderaEnglobeVirtualAprobado = banderaEnglobeVirtualAprobado;
    }

    public void setBanderaEnviarGeografica(boolean banderaEnviarGeografica) {
        this.banderaEnviarGeografica = banderaEnviarGeografica;
    }

    public boolean isBanderaExistenInconsistencias() {
        return banderaExistenInconsistencias;
    }

    public void setBanderaExistenInconsistencias(boolean banderaExistenInconsistencias) {
        this.banderaExistenInconsistencias = banderaExistenInconsistencias;
    }

    public boolean isBanderaEnviarDepuracion() {
        return banderaEnviarDepuracion;
    }

    public void setBanderaEnviarDepuracion(boolean banderaEnviarDepuracion) {
        this.banderaEnviarDepuracion = banderaEnviarDepuracion;
    }

    public boolean isBanderaCoordinador() {
        return banderaCoordinador;
    }

    public void setBanderaCoordinador(boolean banderaCoordinador) {
        this.banderaCoordinador = banderaCoordinador;
    }

    public List<PPredio> getPrediosCancelarSeleccionados() {
        return prediosCancelarSeleccionados;
    }

    public void setPrediosCancelarSeleccionados(List<PPredio> prediosCancelarSeleccionados) {
        this.prediosCancelarSeleccionados = prediosCancelarSeleccionados;
    }

    public PPredio getPredioCancelarSeleccionado() {
        return predioCancelarSeleccionado;
    }

    public void setPredioCancelarSeleccionado(PPredio predioCancelarSeleccionado) {
        this.predioCancelarSeleccionado = predioCancelarSeleccionado;
    }

    public boolean isBanderaCancelacionMasivaAprobado() {
        return banderaCancelacionMasivaAprobado;
    }

    public void setBanderaCancelacionMasivaAprobado(boolean banderaCancelacionMasivaAprobado) {
        this.banderaCancelacionMasivaAprobado = banderaCancelacionMasivaAprobado;
    }

    public String getCirculoRegistralEtapas() {
        return circuloRegistralEtapas;
    }

    public void setCirculoRegistralEtapas(String circuloRegistralEtapas) {
        this.circuloRegistralEtapas = circuloRegistralEtapas;
    }

    public int getNumeroRegistroInicial() {
        return numeroRegistroInicial;
    }

    public void setNumeroRegistroInicial(int numeroRegistroInicial) {
        this.numeroRegistroInicial = numeroRegistroInicial;
    }

    public int getIncrementeoRegistro() {
        return incrementeoRegistro;
    }

    public void setIncrementeoRegistro(int incrementeoRegistro) {
        this.incrementeoRegistro = incrementeoRegistro;
    }

    public String getTipoPredioEtapas() {
        return tipoPredioEtapas;
    }

    public void setTipoPredioEtapas(String tipoPredioEtapas) {
        this.tipoPredioEtapas = tipoPredioEtapas;
    }

    public boolean isBorrarDatoMasivo() {
        return borrarDatoMasivo;
    }

    public void setBorrarDatoMasivo(boolean borrarDatoMasivo) {
        this.borrarDatoMasivo = borrarDatoMasivo;
    }

    public String getDestinoEtapas() {
        return destinoEtapas;
    }

    public void setDestinoEtapas(String destinoEtapas) {
        this.destinoEtapas = destinoEtapas;
    }

    public int getnPrediosSeleccionados() {
        return nPrediosSeleccionados;
    }

    public void setnPrediosSeleccionados(int nPrediosSeleccionados) {
        this.nPrediosSeleccionados = nPrediosSeleccionados;
    }

    public List<Predio> getPrediosEnglobe() {
        return prediosEnglobe;
    }

    public void setPrediosEnglobe(List<Predio> prediosEnglobe) {
        this.prediosEnglobe = prediosEnglobe;
    }

    public String getnManzanaInicial() {
        return nManzanaInicial;
    }

    public void setnManzanaInicial(String nManzanaInicial) {
        this.nManzanaInicial = nManzanaInicial;
    }

    public boolean isMostrarMantieneNumeroPredial() {
        return mostrarMantieneNumeroPredial;
    }

    public void setMostrarMantieneNumeroPredial(boolean mostrarMantieneNumeroPredial) {
        this.mostrarMantieneNumeroPredial = mostrarMantieneNumeroPredial;
    }

    public List<SelectItem> getCondicionesPropiedadDesenglobeManzana() {
        return condicionesPropiedadDesenglobeManzana;
    }

    public void setCondicionesPropiedadDesenglobeManzana(
        List<SelectItem> condicionesPropiedadDesenglobeManzana) {
        this.condicionesPropiedadDesenglobeManzana = condicionesPropiedadDesenglobeManzana;
    }

    public boolean isVerCantidadPredios() {
        return verCantidadPredios;
    }

    public void setVerCantidadPredios(boolean verCantidadPredios) {
        this.verCantidadPredios = verCantidadPredios;
    }

    public boolean isSelectAllResultantesManzanas() {
        return selectAllResultantesManzanas;
    }

    public void setSelectAllResultantesManzanas(boolean selectAllResultantesManzanas) {
        this.selectAllResultantesManzanas = selectAllResultantesManzanas;
    }

    public List<SelectItem> getManzanasDisponibles() {
        return manzanasDisponibles;
    }

    public void setManzanasDisponibles(List<SelectItem> manzanasDisponibles) {
        this.manzanasDisponibles = manzanasDisponibles;
    }

    public Integer getCantidadPrediosAGenerar() {
        return cantidadPrediosAGenerar;
    }

    public void setCantidadPrediosAGenerar(Integer cantidadPrediosAGenerar) {
        this.cantidadPrediosAGenerar = cantidadPrediosAGenerar;
    }

    public List<PPredio> getPrediosNuevosAGenerar() {
        return prediosNuevosAGenerar;
    }

    public void setPrediosNuevosAGenerar(List<PPredio> prediosNuevosAGenerar) {
        this.prediosNuevosAGenerar = prediosNuevosAGenerar;
    }

    public Integer getManzanasAGenerar() {
        return manzanasAGenerar;
    }

    public void setManzanasAGenerar(Integer manzanasAGenerar) {
        this.manzanasAGenerar = manzanasAGenerar;
    }

    public boolean isEnvioDesenglobeManzanas() {
        return envioDesenglobeManzanas;
    }

    public void setEnvioDesenglobeManzanas(boolean envioDesenglobeManzanas) {
        this.envioDesenglobeManzanas = envioDesenglobeManzanas;
    }

    public boolean isRetirarManzana() {
        return retirarManzana;
    }

    public void setRetirarManzana(boolean retirarManzana) {
        this.retirarManzana = retirarManzana;
    }

    public List<PManzanaVereda> getManzanasDesenglobe() {
        return manzanasDesenglobe;
    }

    public void setManzanasDesenglobe(List<PManzanaVereda> manzanasDesenglobe) {
        this.manzanasDesenglobe = manzanasDesenglobe;
    }

    public boolean isCreaManzanas() {
        return creaManzanas;
    }

    public void setCreaManzanas(boolean creaManzanas) {
        this.creaManzanas = creaManzanas;
    }

    public boolean isLimitadoACondicion0() {
        return limitadoACondicion0;
    }

    public Long getModeloSeleccionado() {
        return modeloSeleccionado;
    }

    public void setModeloSeleccionado(Long modeloSeleccionado) {
        this.modeloSeleccionado = modeloSeleccionado;
    }

    public void setLimitadoACondicion0(boolean limitadoACondicion0) {
        this.limitadoACondicion0 = limitadoACondicion0;
    }

    public boolean isOkToAddAcceptPredioNuevoQuinta() {
        return this.okToAddAcceptPredioNuevoQuinta;
    }

    public void setOkToAddAcceptPredioNuevoQuinta(boolean okToAddAcceptPredioNuevoQuinta) {
        this.okToAddAcceptPredioNuevoQuinta = okToAddAcceptPredioNuevoQuinta;
    }

    public CombosDeptosMunisMB getCombosDeptosMunis() {
        return this.combosDeptosMunis;
    }

    public void setCombosDeptosMunis(CombosDeptosMunisMB combosDeptosMunis) {
        this.combosDeptosMunis = combosDeptosMunis;
    }

    public boolean isCondPropWasSelected() {
        return this.condPropWasSelected;
    }

    public void setCondPropWasSelected(boolean wasSelected) {
        this.condPropWasSelected = wasSelected;
    }

    public boolean isIngresarPredioNuevoOmitido() {
        return this.ingresarPredioNuevoOmitido;
    }

    public void setIngresarPredioNuevoOmitido(boolean ingresarPredioNuevoOmitido) {
        this.ingresarPredioNuevoOmitido = ingresarPredioNuevoOmitido;
    }

    public boolean isBanderaActivaCancelar() {
        return banderaActivaCancelar;
    }

    public void setBanderaActivaCancelar(boolean banderaActivaCancelar) {
        this.banderaActivaCancelar = banderaActivaCancelar;
    }

    public String getEdificioTorre() {
        return this.edificioTorre;
    }

    /**
     * @author pedro.garcia
     * @param edificioTorre
     */
    public void setEdificioTorre(String edificioTorre) {
        this.edificioTorre = edificioTorre;

        if (!this.edificioTorre.equals(Constantes.VALOR_DUMMY_COMBOS)) {
            this.predioNuevo.setNumeroEdificioTorre(edificioTorre);
        }
    }

    public boolean isMustSelectEdificioTorrePredioNuevo() {
        return this.mustSelectEdificioTorrePredioNuevo;
    }

    public void setMustSelectEdificioTorrePredioNuevo(boolean mustSelectEdificioTorrePredioNuevo) {
        this.mustSelectEdificioTorrePredioNuevo = mustSelectEdificioTorrePredioNuevo;
    }

    public boolean isMustSelectPisoPredioNuevo() {
        return this.mustSelectPisoPredioNuevo;
    }

    public void setMustSelectPisoPredioNuevo(boolean mustSelectPisoPredioNuevo) {
        this.mustSelectPisoPredioNuevo = mustSelectPisoPredioNuevo;
    }

    public ArrayList<SelectItem> getCondicionPropiedadSelectItems() {
        return this.condicionPropiedadSelectItems;
    }

    public void setCondicionPropiedadSelectItems(ArrayList<SelectItem> condicionPropiedadSelectItems) {
        this.condicionPropiedadSelectItems = condicionPropiedadSelectItems;
    }

    public boolean isMustEnterDataTerreno() {
        return this.mustEnterDataTerreno;
    }

    public void setMustEnterDataTerreno(boolean mustEnterDataTerreno) {
        this.mustEnterDataTerreno = mustEnterDataTerreno;
    }

    public boolean isSelectedPredioFichaMatrizBool() {
        return selectedPredioFichaMatrizBool;
    }

    public void setSelectedPredioFichaMatrizBool(
        boolean selectedPredioFichaMatrizBool) {
        this.selectedPredioFichaMatrizBool = selectedPredioFichaMatrizBool;
    }

    public PPredio getPredioSeleccionado() {
        return this.proyectarConservacionMB.getPredioSeleccionado();
    }

    public String getManzanaCodigo() {
        return manzanaCodigo;
    }

    public void setManzanaCodigo(String manzanaCodigo) {
        this.manzanaCodigo = manzanaCodigo;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public boolean isVerModelosQuintaMasivo() {
        if (this.getTramite().isQuintaMasivo() && this.prediosResultantesSeleccionados != null) {
            for (PPredio pp : this.prediosResultantesSeleccionados) {
                if (pp.isPredioOriginalTramite()) {
                    return true;
                }
            }
        }
        return false;
    }

    public Tramite getTramite() {
        return this.proyectarConservacionMB.getTramite();
    }

    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
        this.combosDeptosMunis.setSelectedZonaCod(this.zona);
        if(this.predioNuevo != null) {
            this.predioNuevo.setZona(this.zona);
        }
        this.sector = null;
        this.comuna = null;
        this.barrio = null;

    }

    public String getSector() {
        return this.sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
        this.combosDeptosMunis.setSelectedSectorCod(this.sector);
        if(this.predioNuevo != null) {
            this.predioNuevo.setSector(this.sector);
        }

        this.comuna = null;
        this.barrio = null;
    }

    public List<SelectItem> getCondicionesPropiedadFiltradas() {
        return condicionesPropiedadFiltradas;
    }

    public String getComuna() {
        return comuna;
    }

    /*
     * @modified pedro.garcia 11-07-2012 todo el método estaba paila
     */
    public void setComuna(String comuna) {

        String comunaReal = "";
        StringBuilder codigoBarrioReal;

        this.comuna = comuna;
        this.combosDeptosMunis.setSelectedComunaCod(this.comuna);

        if (comuna.length() == 2) {
            comunaReal = comuna;
        } else if (comuna.length() == 11) {
            comunaReal = comuna.substring(9, 11);
        }
        if(this.predioNuevo != null) {
            this.predioNuevo.setComuna(comunaReal);
            codigoBarrioReal = new StringBuilder(this.predioNuevo.getBarrioCodigo());
            codigoBarrioReal.replace(0, 2, comunaReal);
            this.predioNuevo.setBarrioCodigo(codigoBarrioReal.toString());
        }


        this.barrio = null;

    }

    public String getBarrio() {
        return barrio;
    }

    /**
     * el bario puede llegar de longitud 2 -> el código propiamante del barrio según la nueva
     * estructura del código predial 4 -> el código de barrio que incluye al de comuna 13 -> el
     * número predial hasta el segmento del barrio
     *
     * @param barrio
     */
    /*
     * @modified pedro.garcia 26-06-2012 cambio de uso de StringBuffer a StringBuilder 11-07-2012 el
     * método todo estaba paila
     */
    public void setBarrio(String barrio) {

        String barrioReal = "";

        this.barrio = barrio;
        this.combosDeptosMunis.setSelectedBarrioCod(this.barrio);

        if (barrio.length() == 2) {
            if (this.comuna != null && this.comuna.length() >= 2) {
                barrioReal = this.comuna.substring(this.comuna.length() - 2, this.comuna.length()) +
                    barrio;
            } else {
                barrioReal = "00" + barrio;
            }
        } else if (barrio.length() == 4) {
            barrioReal = barrio;
        } else if (barrio.length() == 13) {
            barrioReal = barrio.substring(9, 13);
        }

        this.predioNuevo.setBarrio(barrioReal.substring(
            barrioReal.length() - 2, barrioReal.length()));
        this.predioNuevo.setBarrioCodigo(barrioReal);

    }

    public boolean isBanderaIncremento() {
        return banderaIncremento;
    }

    public void setBanderaIncremento(boolean banderaIncremento) {
        this.banderaIncremento = banderaIncremento;
    }

    public String getManzana() {
        return this.manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
        if (!this.manzana.equals(Constantes.VALOR_DUMMY_COMBOS)) {
            this.predioNuevo.setManzanaVereda(this.manzana);
            this.banderaManzanaSeleccionada = true;
        } else {
            this.banderaManzanaSeleccionada = false;
        }
    }
//--------------------------------------------------------

    public String getTerreno() {
        return this.terreno;
    }

    public void setTerreno(String terreno) {
        this.predioNuevo.setTerreno(terreno);
        this.terreno = terreno;
    }
//--------------------------------------------------------

    public void setCondicionesPropiedadFiltradas(List<SelectItem> condicionesPropiedadFiltradas) {
        this.condicionesPropiedadFiltradas = condicionesPropiedadFiltradas;
    }

    public PPredio getPredioNuevo() {
        if (this.predioNuevo != null && this.predioNuevo.getNumeroPredial().length() >= 17) {
            this.manzanaCodigo = this.predioNuevo.getNumeroPredial().substring(0, 17);
        }
        return this.predioNuevo;
    }

    public void setPredioNuevo(PPredio predioNuevo) {
        this.predioNuevo = predioNuevo;
    }

    public List<PPredio> getPrediosResultantesCancelacionMasiva() {
        return prediosResultantesCancelacionMasiva;
    }

    public void setPrediosResultantesCancelacionMasiva(
        List<PPredio> prediosResultantesCancelacionMasiva) {
        this.prediosResultantesCancelacionMasiva = prediosResultantesCancelacionMasiva;
    }

    public List<PPredio> getPrediosResultantes() {
        return this.prediosResultantes;
    }

    public void setPrediosResultantes(List<PPredio> prediosResultantes) {
        this.prediosResultantes = prediosResultantes;
    }

    public List<PPredio> getPrediosResultantesTerceraRectificacionMasiva() {
        return prediosResultantesTerceraRectificacionMasiva;
    }

    public void setPrediosResultantesTerceraRectificacionMasiva(
        List<PPredio> prediosResultantesTerceraRectificacionMasiva) {
        this.prediosResultantesTerceraRectificacionMasiva =
            prediosResultantesTerceraRectificacionMasiva;
    }

    public boolean isMantieneNumeroPredial() {
        return this.mantieneNumeroPredial;
    }

    public void setMantieneNumeroPredial(boolean conservaNumeroPredial) {
        this.mantieneNumeroPredial = conservaNumeroPredial;
    }

    public Predio getPredioPrincipalEnglobe() {
        return this.predioPrincipalEnglobe;
    }

    public boolean isBanderaManzanaSeleccionada() {
        return this.banderaManzanaSeleccionada;
    }

    public void setBanderaManzanaSeleccionada(boolean banderaManzanaSeleccionada) {
        this.banderaManzanaSeleccionada = banderaManzanaSeleccionada;
    }

    public void setPredioPrincipalEnglobe(Predio predioPrincipalEnglobe) {
        this.predioPrincipalEnglobe = predioPrincipalEnglobe;
    }

    public boolean isAdicionarPredioHabilitado() {
        return this.adicionarPredioHabilitado;
    }

    public void setAdicionarPredioHabilitado(boolean adicionarPredioHabilitado) {
        this.adicionarPredioHabilitado = adicionarPredioHabilitado;
    }

    public List<SelectItem> getZonasSelectItems() {
        return this.zonasSelectItems;
    }

    public void setZonasSelectItems(List<SelectItem> zonasSelectItems) {
        this.zonasSelectItems = zonasSelectItems;
    }

    public List<SelectItem> getSectoresSelectItems() {
        return this.sectoresSelectItems;
    }

    public void setSectoresSelectItems(List<SelectItem> sectoresSelectItems) {
        this.sectoresSelectItems = sectoresSelectItems;
    }

    public List<SelectItem> getBarriosSelectItems() {
        return this.barriosSelectItems;
    }

    public void setBarriosSelectItems(List<SelectItem> barriosSelectItems) {
        this.barriosSelectItems = barriosSelectItems;
    }

    public List<SelectItem> getManzanasSelectItems() {
        return this.manzanasSelectItems;
    }

    public void setManzanasSelectItems(List<SelectItem> manzanasSelectItems) {
        this.manzanasSelectItems = manzanasSelectItems;
    }

    public List<SelectItem> getComunasSelectItems() {
        return this.comunasSelectItems;
    }

    public void setComunasSelectItems(List<SelectItem> comunasSelectItems) {
        this.comunasSelectItems = comunasSelectItems;
    }

    public List<SelectItem> getNoEdificioSelectItems() {
        return this.noEdificioSelectItems;
    }

    public void setNoEdificioSelectItems(List<SelectItem> noEdificioSelectItems) {
        this.noEdificioSelectItems = noEdificioSelectItems;
    }

    public List<SelectItem> getPisoSelectItems() {
        return this.pisoSelectItems;
    }

    public void setPisoSelectItems(List<SelectItem> pisoSelectItems) {
        this.pisoSelectItems = pisoSelectItems;
    }

    public List<SelectItem> getUnidadSelectItems() {
        return this.unidadSelectItems;
    }

    public void setUnidadSelectItems(List<SelectItem> unidadSelectItems) {
        this.unidadSelectItems = unidadSelectItems;
    }

    public List<SelectItem> getTerrenoSelectItems() {
        return this.terrenoSelectItems;
    }

    public void setTerrenoSelectItems(List<SelectItem> terrenoSelectItems) {
        this.terrenoSelectItems = terrenoSelectItems;
    }

    public PPredio getPredioResultanteSeleccionado() {
        return predioResultanteSeleccionado;
    }

    public void setPredioResultanteSeleccionado(PPredio predioResultanteSeleccionado) {
        this.predioResultanteSeleccionado = predioResultanteSeleccionado;
    }

    public boolean isSelectAllResultantes() {
        return this.selectAllResultantes;
    }

    public void setSelectAllResultantes(boolean selectAllResultantes) {
        this.selectAllResultantes = selectAllResultantes;
    }

    public List<PPredio> getPrediosResultantesSeleccionados() {
        return this.prediosResultantesSeleccionados;
    }

    public void setPrediosResultantesSeleccionados(List<PPredio> prediosResultantesSeleccionados) {
        this.prediosResultantesSeleccionados = prediosResultantesSeleccionados;
    }

    public String getCondicionPropiedadSeleccionada() {
        return this.condicionPropiedadSeleccionada;
    }

    public void setCondicionPropiedadSeleccionada(String condicionPropiedadSeleccionada) {
        this.condicionPropiedadSeleccionada = condicionPropiedadSeleccionada;
    }

    public boolean isCondicion0selected() {
        return this.condicion0selected;
    }

    public void setCondicion0selected(boolean condicion0selected) {
        this.condicion0selected = condicion0selected;
    }

    public String getNumeroPredialBusquedaTerceraRectificacionMasiva() {
        return numeroPredialBusquedaTerceraRectificacionMasiva;
    }

    public void setNumeroPredialBusquedaTerceraRectificacionMasiva(
        String numeroPredialBusquedaTerceraRectificacionMasiva) {
        this.numeroPredialBusquedaTerceraRectificacionMasiva =
            numeroPredialBusquedaTerceraRectificacionMasiva;
    }

    public boolean isBanderaRetirarPrediosBase() {
        return banderaRetirarPrediosBase;
    }

    public void setBanderaRetirarPrediosBase(boolean banderaRetirarPrediosBase) {
        this.banderaRetirarPrediosBase = banderaRetirarPrediosBase;
    }

    public boolean isBanderaRetirarPrediosResultantes() {
        return banderaRetirarPrediosResultantes;
    }

    public void setBanderaRetirarPrediosResultantes(boolean banderaRetirarPrediosResultantes) {
        this.banderaRetirarPrediosResultantes = banderaRetirarPrediosResultantes;
    }

    //--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        //felipe.cadena:: DESENGLOBE_ETAPAS::para tramites de desenglobe por etapas no se continua si existen predios en otros tramites
        if (this.getTramite().isDesenglobeEtapas() && this.proyectarConservacionMB.
            isPredioEnTramite()) {
            return;
        }

        this.initializeSelectItems();

        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();

        this.prediosResultantes = new ArrayList<PPredio>();
        this.prediosResultantesSeleccionados = new ArrayList<PPredio>();
        this.prediosCancelarSeleccionados = new ArrayList<PPredio>();
        this.banderaRetirarPrediosBase = false;
        this.banderaRetirarPrediosResultantes = false;
        this.banderaCoordinador = true;
        this.banderaEnviarGeografica = true;
        this.banderaIncremento = false;
        this.banderaActivaCancelar= false;
        this.quintaNuevoCond5 = false;

        if (this.getTramite().isViaGubernativa()) {
            //@modified by leidy.gonzalez:: #51998:: 20/03/2018:: ajuste para asignar funcionario ejecutor 
            //a tramites de via gubernativa
            this.asignaUsuarioEjecutor();
        }

        if (this.getTramite().isCancelacionMasiva()) {
            this.inicializarPrediosCancelacionMasiva();
        }

        if (this.getTramite().isSegundaEnglobe()) {
            this.prediosResultantes = this.proyectarConservacionMB.getpPrediosEnglobe();
            if (this.prediosResultantes != null && this.prediosResultantes.size() > 0) {
                this.prediosResultantesSeleccionados.add(this.prediosResultantes.get(0));
                this.prediosResultantes.get(0).setSelected(true);
                for (Predio p : this.getTramite().getPredios()) {
                    if (p.getId().equals(this.prediosResultantes.get(0).getId())) {
                        this.mantieneNumeroPredial = true;
                        break;
                    }
                }
            }
            boolean hayMejora = false;
            boolean hayCondicion0 = false;
            // Setea por defecto el predio que al radicar fue elegido como principal
            for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
                if (tpe.getEnglobePrincipal() != null && tpe.getEnglobePrincipal().equals(ESiNo.SI.
                    getCodigo())) {
                    predioPrincipalEnglobe = tpe.getPredio();
                    break;
                }
            }
            for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
                if ("5".equals(tpe.getPredio().getCondicionPropiedad())) {
                    hayMejora = true;
                }
                if ("0".equals(tpe.getPredio().getCondicionPropiedad())) {
                    hayCondicion0 = true;
                }
            }
            if (hayMejora && hayCondicion0) {
                this.limitadoACondicion0 = true;
                this.condicionPropiedadSeleccionada = "0";
            }
        }
        if (this.getTramite().isSegundaDesenglobe()) {
            this.prediosResultantes = this.proyectarConservacionMB.getpPrediosDesenglobe();

            if (this.prediosResultantes != null && !this.prediosResultantes.isEmpty()) {
                this.condicionPropiedadSeleccionada = this.prediosResultantes
                    .get(0).getCondicionPropiedad();
                for (PPredio p : this.prediosResultantes) {
                    if (p.getId().equals(this.getTramite().getPredio().getId())) {
                        this.mantieneNumeroPredial = true;
                        break;
                    }
                }
                int len = this.prediosResultantes.size();
                if (len == 1) {
                    this.prediosResultantes.get(0).setUltimoDesenglobe(true);

                } else {
                    if (len > 0) {
                        if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
                            PPredio ultimo = this.obtenerUltimoPredioPorUnidad(
                                this.prediosResultantes);
                            int idx = this.prediosResultantes.indexOf(ultimo);
                            this.prediosResultantes.get(idx).setUltimoDesenglobe(true);
                        } else {
                            PPredio ultimo = this.obtenerUltimoPredioPorTerreno(
                                this.prediosResultantes);
                            int idx = this.prediosResultantes.indexOf(ultimo);
                            this.prediosResultantes.get(idx).setUltimoDesenglobe(true);
                        }
                    }
                }
            }
        }
        if (this.getTramite().isQuinta() || this.getTramite().isModificacionInscripcionCatastral()) {
            if (this.proyectarConservacionMB.getPredioSeleccionado() != null) {
                this.prediosResultantes.add(this.proyectarConservacionMB.getPredioSeleccionado());
                this.prediosResultantesSeleccionados.add(this.proyectarConservacionMB.
                    getPredioSeleccionado());
            }

            //lorena.salamanca :: 24-08-2015 :: Quinta Masivo.        
            if (this.getTramite().isQuintaMasivo()) {
                this.prediosResultantes.clear();
                this.prediosResultantes = this.getConservacionService().
                    findPPrediosCompletosActivosByTramiteId(this.getTramite().getId());
                this.prediosResultantesSeleccionados.clear();
            }

            if (this.getTramite().isQuintaOmitidoNuevo()) {
                if (!this.prediosResultantes.isEmpty()) {
                    String condicionPropiedad = this.prediosResultantes.get(0).getNumeroPredial().
                        substring(21, 22);
                    if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
                        this.proyectarConservacionMB.setBanderaQuintaOmitidoMejora(true);
                        this.proyectarConservacionMB.cargarFechasQuintaMejora();
                    }
                }
            }

            if (this.getTramite().isQuintaNuevo()) {

                if (!this.prediosResultantes.isEmpty()) {
                    String condicionPropiedad = this.prediosResultantes.get(0).getNumeroPredial().substring(21, 22);
                    if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
                        this.quintaNuevoCond5 = true;
                        this.setQuintaNuevoCond5(true);
                    }else{
                        this.quintaNuevoCond5 = false;
                    }
                }

            }
        }

        //Genera la proyeccion cuando se trata de desenglobes por etapas
        if (this.getTramite().isDesenglobeEtapas()) {
            if (this.prediosResultantes.isEmpty()) {
                this.condicionPropiedadSeleccionada = this.getTramite().getPredio().
                    getCondicionPropiedad();
                this.aceptarEnglobeDesenglobeMantiene();
                this.proyectarConservacionMB.init();
                this.proyectarConservacionMB.consultarFichaMatriz();
            }
        }

        //Para los trámites de tercera másivos y rectificación matriz javier.aponte refs#13168
        if (this.getTramite().isTerceraMasiva() || this.getTramite().isRectificacionMatriz()) {
            this.prediosResultantes = this.proyectarConservacionMB.
                getpPrediosTerceraRectificacionMasiva();
            this.prediosResultantesTerceraRectificacionMasiva = new ArrayList<PPredio>();
            if (this.prediosResultantes != null) {
                for (PPredio pp : this.prediosResultantes) {
                    if (pp.isPredioOriginalTramite()) {
                        //Se obtienen el numero predial antiguo para los predio originales
                        Predio predioOriginal = this.getConservacionService().obtenerPredioPorId(pp.
                            getId());
                        if (predioOriginal != null) {
                            pp.setNumeroPredialOriginal(predioOriginal.getNumeroPredial());
                        }
                    }

                }

                this.prediosResultantesTerceraRectificacionMasiva.addAll(this.prediosResultantes);
            }
            this.numeroPredialBusquedaTerceraRectificacionMasiva = this.getTramite().getPredio().
                getNumeroPredial().substring(0, 22);
        }

        this.selectAllResultantes = false;

        this.armarCondicionesPropiedadFiltradas();

        // Coeficientes de copropiedad de los predios resultantes creados a
        // partir de la ficha matriz.
        if (this.getTramite().isSegundaDesenglobe() ||
            this.getTramite().isSegundaEnglobe() ||
            this.getTramite().isQuintaMasivo()) {
            if (this.prediosResultantes != null &&
                !this.prediosResultantes.isEmpty()) {

                this.prediosResultantes = this.proyectarConservacionMB
                    .cargarCoeficientesCopropiedadPrediosFichaMatriz(this.prediosResultantes);

                this.prediosResultantesTerceraRectificacionMasiva = new ArrayList<PPredio>();
                this.prediosResultantesTerceraRectificacionMasiva.addAll(this.prediosResultantes);

                this.proyectarConservacionMB.consultarFichaMatriz();
            }
        }

        // Activar o desactivar los botones 'Adicionar' y 'Retirar' según su
        // mutación.
        this.revisarActivacionAdicionarRetirar();

        // Debido a que existe más de un predio resultante, se debe hacer null
        // el predio seleccionado cada vez que se entre a éste init, con el fin
        // de no tener un predio seleccionado por debajo y para poder activar
        // las secciones a partir de la selección de la tabla.
        if (this.getTramite().isSegundaDesenglobe() || this.getTramite().isTerceraMasiva() ||
            this.getTramite().isRectificacionMatriz()) {
            this.prediosResultantesSeleccionados = new ArrayList<PPredio>();
            this.proyectarConservacionMB.setPredioSeleccionado(null);
        }

        if (this.getTramite().isQuintaOmitidoNuevo()) {
            this.ingresarPredioNuevoOmitido = true;
        }
        this.condicion0selected = false;

        //Se retiran los predios que no son mejoras para cuando el englobe
        //es de mejoras y predios de terreno
        this.prediosEnglobe = new ArrayList<Predio>();
        List<Predio> prediosTramite = this.getTramite().getPredios();
        if (this.proyectarConservacionMB.isBanderaMejorasTerrenoEnglobe()) {
            for (Predio predio : prediosTramite) {
                if (!predio.isMejora()) {
                    this.prediosEnglobe.add(predio);
                }
            }
        } else {
            this.prediosEnglobe = this.getTramite().getPredios();
        }
        this.inicializaDatosDesenglobeManzanas();

        if (this.getTramite().isEnglobeVirtual()) {
            this.inicializaEnglobeVirtual();
        }

    }

    /**
     *
     * Asigna funcionario ejecutor al tramite seleccionado
     *
     * @author leidy.gonzalez
     */
    private void asignaUsuarioEjecutor() {

        this.getTramite().setTramitePruebas(null);
        this.getTramite().setFuncionarioEjecutor(this.loggedInUser.getLogin());
        this.getTramite().setNombreFuncionarioEjecutor(this.loggedInUser
            .getNombreCompleto());
        this.getTramiteService().actualizarTramite(
            this.getTramite(), this.loggedInUser);
    }

    /**
     * Inicializa los datos necesarios para la cancelacion masiva
     *
     * @author felipe.cadena
     */
    public void inicializarPrediosCancelacionMasiva() {

        this.banderaCoordinador = false;
        this.prediosResultantesCancelacionMasiva = new ArrayList<PPredio>();
        this.prediosResultantes.clear();

        List<PPredio> predioProyectados = this.getConservacionService().
            buscarPPrediosCompletosPorTramiteId(this.getTramite().getId());

        for (PPredio pp : predioProyectados) {
            if (pp.isPredioOriginalTramite()) {
                this.prediosResultantes.add(pp);
            } else {
                this.prediosResultantesCancelacionMasiva.add(pp);
            }
        }

        if (!this.prediosResultantesCancelacionMasiva.isEmpty()) {
            this.banderaCancelacionMasivaAprobado = true;
            this.banderaRetirarPrediosBase = false;
            this.banderaEnviarGeografica = true;
            if (this.getTramite().getEstadoGdb() == null) {
                this.banderaCoordinador = true;
            }
        }

        if (!this.prediosResultantes.isEmpty()) {

            PFichaMatriz fichaMatriz = this.getConservacionService()
                .buscarPFichaMatrizPorPredioId(this.getTramite()
                    .getPredio().getId());

            List<PFichaMatrizPredio> prediosFicha = this.getConservacionService().
                buscarPFichaMatrizPredioPorIdFichaYEstadoPredio("", fichaMatriz.getId());

            this.proyectarConservacionMB.setFichaMatrizSeleccionada(fichaMatriz);

            for (PFichaMatrizPredio pfmp : prediosFicha) {
                for (PPredio pp : this.prediosResultantes) {
                    if (pp.getNumeroPredial().equals(pfmp.getNumeroPredial())) {
                        pp.setCoeficienteCopropiedadFM(pfmp.getCoeficiente());
                    }
                }
            }
        }
    }

    /**
     * Realiza la preparacion los predios asociados a la cancelacion masiva
     *
     * @author felipe.cadena
     */
    public void setupCancelarMasivo() {

        Object[] result = this.getTramiteService().
            generarProyeccionCancelacionBasica(this.getTramite().getId(), this.loggedInUser.
                getLogin());

        String[] errors = UtilidadesWeb.extraerMensajeSP(result);

        this.getTramite().setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
        this.getTramiteService().guardarActualizarTramite2(this.getTramite());

        if (errors[0].isEmpty()) {
            this.inicializarPrediosCancelacionMasiva();
        } else {
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(errors[0]));
        }

    }

    /**
     * Realiza la cancelacion de todos los predios asociados a la cancelacion masiva
     *
     * @author felipe.cadena
     */
    public void cancelarMasivo() {

        this.banderaCancelacionMasivaAprobado = true;
        this.banderaEnviarGeografica = true;

        for (PPredio pp : this.prediosResultantes) {
            pp.setEstado(ETramiteEstado.CANCELADO.getCodigo());
        }

        this.proyectarConservacionMB.getFichaMatrizSeleccionada().setEstado(
            EFichaMatrizEstado.CANCELADO.getCodigo());

        List<PFichaMatrizPredio> prediosFicha = this.getConservacionService().
            buscarPFichaMatrizPredioPorIdFichaYEstadoPredio(null, this.proyectarConservacionMB.
                getFichaMatrizSeleccionada().getId());

        for (PFichaMatrizPredio pfmp : prediosFicha) {
            pfmp.setEstado(EPredioEstado.CANCELADO.getCodigo());
            pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
        }

        this.getConservacionService().guardarPrediosFichaMatriz(prediosFicha);

        List<PPredioDireccion> direccionesFicha = null;
        for (PPredio pp : this.prediosResultantes) {
            if (pp.isEsPredioFichaMatriz()) {
                direccionesFicha = pp.getPPredioDireccions();
                break;
            }
        }

        if (direccionesFicha != null) {

            for (PPredio pp : this.prediosResultantesCancelacionMasiva) {
                for (PPredioDireccion ppd : direccionesFicha) {
                    ppd.setId(null);
                    ppd.setPPredio(pp);
                    ppd.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                }
                direccionesFicha = this.getConservacionService().actualizarPPredioDirecciones(
                    direccionesFicha);
                pp.setPPredioDireccions(direccionesFicha);
            }
            this.getConservacionService().
                guardarPPredios(this.prediosResultantesCancelacionMasiva);

            this.proyectarConservacionMB.actualizarListaDeDirecciones();

        }

        this.proyectarConservacionMB.getFichaMatrizSeleccionada().setEstado(EPredioEstado.CANCELADO.
            getCodigo());
        this.proyectarConservacionMB.getFichaMatrizSeleccionada().
            setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());

        for (PFichaMatrizTorre pft : this.proyectarConservacionMB.getFichaMatrizSeleccionada().
            getPFichaMatrizTorres()) {
            pft.setEstado(EPredioEstado.CANCELADO.getCodigo());
            pft.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
        }

        for (PFichaMatrizPredio pfp : this.proyectarConservacionMB.getFichaMatrizSeleccionada().
            getPFichaMatrizPredios()) {
            pfp.setEstado(EPredioEstado.CANCELADO.getCodigo());
            pfp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
        }

        for (PFichaMatrizModelo pfm : this.proyectarConservacionMB.getFichaMatrizSeleccionada().
            getPFichaMatrizModelos()) {
            pfm.setEstado(EPredioEstado.CANCELADO.getCodigo());
            pfm.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
        }

        this.getConservacionService().actualizarFichaMatriz(this.proyectarConservacionMB.
            getFichaMatrizSeleccionada());

    }

    /**
     * Reversa las operaciones realizadas en el tramite de cancelacion masiva
     *
     * @author felipe.cadena
     */
    public void reversarCancelacionMasiva() {

        boolean result = this.getTramiteService().
            reversarProyeccion(this.getTramite());

        if (result) {
            this.inicializarPrediosCancelacionMasiva();
        } else {
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(
                EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION.toString()));
        }

        this.eliminarReplicasGeograficas();

        if (this.getTramite().getTramitePredioEnglobes() == null ||
            this.getTramite().getTramitePredioEnglobes().isEmpty()) {

            List<TramitePredioEnglobe> prediosAsociadosFM = new ArrayList<TramitePredioEnglobe>();

            List<Predio> prediosFM = this.getConservacionService().buscarPrediosPorRaizNumPredial(
                this.getTramite().getPredio().getNumeroPredial().substring(0, 22));
            for (Predio predio : prediosFM) {

                if (predio.getId().equals(this.getTramite().getPredio().getId())) {
                    continue;
                }
                //No se agregan los predios de mejoras o los que estan cancelados.
                if (predio.isMejora() || EPredioEstado.CANCELADO.getCodigo().equals(predio.
                    getEstado())) {
                    continue;
                }
                TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                tpe.setPredio(predio);
                tpe.setTramite(this.getTramite());
                tpe.setFechaLog(new Date());
                tpe.setUsuarioLog(this.loggedInUser.getLogin());
                prediosAsociadosFM.add(tpe);
            }

            this.getTramite().setTramitePredioEnglobes(prediosAsociadosFM);

        }

        //Se reproyecta el tramite completo
        Object mensajes[] = this.getTramiteService().generarProyeccion(this.getTramite());

        String[] errors = UtilidadesWeb.extraerMensajeSP(mensajes);

        if (errors[0].isEmpty()) {
            this.inicializarPrediosCancelacionMasiva();
        } else {
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(errors[0]));
        }

        this.banderaCancelacionMasivaAprobado = false;
    }

    /**
     * Método para actualizar la ediciones de los predios seleccionados ya que solo se puede
     * modificar los datos relacionados al predio inicial del trámite
     *
     * @author felipe.cadena
     */
    public void actualizarEdicionDatos() {

        RectificacionComplementacionMB rc = (RectificacionComplementacionMB) UtilidadesWeb.
            getManagedBean("rc");
        ProyectarConservacionMB pc = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");

//        if (this.prediosSeleccionados.length == 0) {
//            rc.setHabilitarTabRectificacion(true);
//            return;
//        }
//        if (this.prediosSeleccionados.length > 1) {
//            //deshabilita pestañas
//            rc.setHabilitarTabRectificacion(false);
//
//        } else if (this.prediosSeleccionados[0].getId() == this.predioInicial.getId()) {
//            // habilita los campos para la rectificación
//            pc.setPredioSeleccionado(this.predioProyectadoTramite);
//            pc.setAvaluo();////version 1.1.4 - felipe.cadena_7286
//            rc.init();
//            rc.setHabilitarTabRectificacion(true);
//        } else {
//            
//            if(this.tramite.isRectificacionMatriz()){
//                rc.setHabilitarTabRectificacion(false);
//            }else{
//                rc.setHabilitarTabRectificacion(true);
//            }
//            
//            PPredio predioProyectado = null;
//            try {
//                predioProyectado = this.getConservacionService().
//                        obtenerPPredioCompletoByIdyTramite(this.prediosSeleccionados[0].getId(), this.tramite.getId());
//
//                if (predioProyectado != null) {
//                    rc.init();
//                    pc.setPredioSeleccionado(predioProyectado);
//                    pc.setAvaluo();//version 1.1.4 - felipe.cadena_7286
//                    rc.setHabilitarTabRectificacion(true);
//                    if(!this.tramite.isRectificacionMatriz()){
//                        rc.deshabilitarTodo();
//                    }
//                    
//                }
//            } catch (Exception ex) {
//                LOGGER.debug("Predio seleccionado no proyectado");
//                this.addMensajeInfo("Predio seleccionado no proyectado, No se puede mostrar información de proyección");
//            }
//
//        }
    }

    /**
     * inicializa las variables necesarias para el desenglobe de manzanas
     *
     * @author felipe.cadena
     */
    private void inicializaDatosDesenglobeManzanas() {

        if (this.proyectarConservacionMB.getTramite().getManzanasNuevas() == null) {
            this.creaManzanas = false;
        } else {
            this.creaManzanas = this.proyectarConservacionMB.getTramite().getManzanasNuevas() > 0;
        }

        this.retirarManzana = false;
        this.manzanasAGenerar = 0;
        this.mostrarMantieneNumeroPredial = false;
        this.envioDesenglobeManzanas = true;

        if (this.creaManzanas) {
            this.manzanasDesenglobe = this.getConservacionService().
                obtenerManzanasProyectadasPorTramite(this.getTramite().getId());
            this.nManzanaInicial = this.getTramite().getPredio().getManzanaCodigo();

        } else {
            this.manzanasDesenglobe = new ArrayList<PManzanaVereda>();
        }

        this.armarCondicionesPropiedadDesenglobeManzana();
    }

    /**
     * Obtiene el ultimo predio adicionado por el valor de la unidad
     *
     * @author felipe.cadena
     * @param predios
     * @return
     */
    private PPredio obtenerUltimoPredioPorUnidad(List<PPredio> predios) {

        PPredio resultado = new PPredio();
        int len = predios.size();
        if (len > 1) {
            int Nunidad = UtilidadesWeb.obtenerEntero(predios.get(0).getUnidad());
            resultado = predios.get(0);

            for (PPredio pp : this.prediosResultantes) {
                int NunidadAux = UtilidadesWeb.obtenerEntero(pp.getUnidad());
                if (NunidadAux > Nunidad) {
                    Nunidad = NunidadAux;
                    resultado = pp;
                }
            }
        }

        return resultado;
    }

    /**
     * Obtiene el ultimo predio adicionado por el valor de la unidad
     *
     * @author felipe.cadena
     * @param predios
     * @return
     */
    private PPredio obtenerUltimoPredioPorTerreno(List<PPredio> predios) {

        PPredio resultado = new PPredio();
        int len = predios.size();
        if (len > 1) {
            int Nterreno = UtilidadesWeb.obtenerEntero(predios.get(0).getTerreno());
            resultado = predios.get(0);
            for (PPredio pp : this.prediosResultantes) {
                int NterrenoAux = UtilidadesWeb.obtenerEntero(pp.getTerreno());
                if (NterrenoAux > Nterreno) {
                    Nterreno = NterrenoAux;
                    resultado = pp;
                }
            }
        }

        return resultado;
    }
//--------------------------------------------------------------------------------------------------

    private void initializeSelectItems() {
        this.zonasSelectItems = new ArrayList<SelectItem>();
        this.sectoresSelectItems = new ArrayList<SelectItem>();
        this.barriosSelectItems = new ArrayList<SelectItem>();
        this.manzanasSelectItems = new ArrayList<SelectItem>();
        this.comunasSelectItems = new ArrayList<SelectItem>();
        this.noEdificioSelectItems = new ArrayList<SelectItem>();
        this.pisoSelectItems = new ArrayList<SelectItem>();
        this.unidadSelectItems = new ArrayList<SelectItem>();
        this.terrenoSelectItems = new ArrayList<SelectItem>();
    }

    public void armarCondicionesPropiedadFiltradas() {
        this.condicionesPropiedadFiltradas = new ArrayList<SelectItem>();
        if (this.getTramite().isSegundaEnglobe()) {
            this.armarCondicionesPropiedadEnglobe();
        } else if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {

            SelectItem si = new SelectItem(EPredioCondicionPropiedad.CP_5.getCodigo(),
                EPredioCondicionPropiedad.CP_5.getCodigo() + " - " + EPredioCondicionPropiedad.CP_5.
                getValor());
            this.condicionesPropiedadFiltradas.add(si);

        } else {
            for (SelectItem si : this.generalMB.getPredioCondicionPropiedadCV()) {

                if (this.getTramite().isSegundaDesenglobe()) {
                    if (Constantes.CONDICIONES_PROPIEDAD_DESENGLOBE.contains(si
                        .getValue())) {
                        this.condicionesPropiedadFiltradas.add(si);
                    }
                }
            }
        }
    }

    private void armarCondicionesPropiedadEnglobe() {
        List<String> condsPropiedad = new ArrayList<String>();
        for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
            if (!condsPropiedad.contains(tpe.getPredio()
                .getCondicionPropiedad())) {
                condsPropiedad.add(tpe.getPredio().getCondicionPropiedad());
            }
        }

        for (SelectItem si : this.generalMB.getPredioCondicionPropiedadCV()) {

            if (condsPropiedad.contains(si.getValue())) {
                if (this.proyectarConservacionMB.isBanderaMejorasTerrenoEnglobe() &&
                    si.getValue().equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
                    continue;
                }
                this.condicionesPropiedadFiltradas.add(si);
            }
        }
    }

    public void setupNuevoPredio(Predio predioData) {
        this.predioNuevo = new PPredio();
        this.predioNuevo.setDepartamento(this.getTramite().getPredio()
            .getDepartamento());
        this.predioNuevo.setMunicipio(this.getTramite().getPredio().getMunicipio());
        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();
        this.predioNuevo.setZona(predioData.getZona());
        this.predioNuevo.setSector(predioData.getSector());
        this.predioNuevo.setComuna(predioData.getComuna());
        this.predioNuevo.setBarrio(predioData.getBarrio());
        this.predioNuevo.setManzanaVereda(predioData.getManzanaVereda());
        this.predioNuevo.setCondicionPropiedadNumeroPredial(predioData
            .getCondicionPropiedadNumeroPredial());
        this.predioNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

        if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
            this.predioNuevo.setTerreno(predioData.getTerreno());
        }

        if (this.getTramite().isSegundaDesenglobe() &&
            !this.mantieneNumeroPredial) {
            // Al crear un predio en una mutación de segunda desenglobe para
            // condición cero en la que no se mantiene el número de terreno,
            // el tipo de predio para dicho predio sea vacio, y de ésta
            // manera se deba seleccionar obligatoriamente en la pestaña de
            // gestionar ubicación del predio.
            this.predioNuevo.setTipo(Constantes.CONSTANTE_CADENA_VACIA_DB);
        } else {
            this.predioNuevo.setTipo(Constantes.TIPO_PREDIO_DEFECTO);
        }

        this.predioNuevo.setZonaUnidadOrganica(this.predioNuevo.getZona() != null ?
            (this.predioNuevo.getZona().equals("00") ? "00" : "01") : null);

        // Si se mantiene el número predial se debe hacer el set de la matrícula
        // inmobiliaria, pero únicamente para el predio que mantiene el número
        // predial, el resto deberan tener su propia matricula.
        if (this.mantieneNumeroPredial &&
            (this.prediosResultantes == null || this.prediosResultantes
                .isEmpty())) {
            this.predioNuevo.setCirculoRegistral(predioData
                .getCirculoRegistral());
            this.predioNuevo.setNumeroRegistro(predioData.getNumeroRegistro());
        }
        this.setupDefaultDataPredioNuevo();

        if (this.getTramite().isSegundaDesenglobe()) {
            this.predioNuevo.setTipoAvaluo(predioData.getTipoAvaluo());
            this.predioNuevo.setTipoCatastro(predioData.getTipoCatastro());
        }
        this.proyeccionValidaBool = false;
    }

    /**
     * Adecúa los componentes del número predial referentes a propiedad horizontal y condominios en
     * ceros
     *
     * @author fabio.navarrete
     */
    private void setNoPHCeros() {
        this.predioNuevo.setNumeroEdificioTorre("00");
        this.predioNuevo.setPisoNumeroPredial("00");
        this.predioNuevo.setNumeroUnidad("0000");
    }

    public void aceptarDatosEnglobeDesenglobe() {
        StringBuilder numPredialAux = new StringBuilder(this.getTramite()
            .getPredio().getNumeroPredial());
        if (this.mantieneNumeroPredial) {
            numPredialAux.replace(21, 22,
                this.getPredioSeleccionado().getCondicionPropiedad());
            numPredialAux.replace(22, -1, "0");
        }
        this.getPredioSeleccionado().setNumeroPredial(numPredialAux.toString());
    }

    public void crearPredioOmitido() {

    }

    /**
     * Método que realiza la lógica necesaria para la cancelación del predio.
     *
     * @author fabio.navarrete
     */
    public void cancelarPredio() {
        this.getPredioSeleccionado()
            .setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                .getCodigo());

        this.getPredioSeleccionado().setEstado(EPredioEstado.CANCELADO.getCodigo());
        this.getPredioSeleccionado().setUsuarioLog(this.loggedInUser
            .getLogin());
        this.getPredioSeleccionado().setFechaLog(new Date());

        this.proyectarConservacionMB.setPredioSeleccionado(this.getConservacionService()
            .guardarActualizarPPredio(this.getPredioSeleccionado()));

    }

    public void onMunicipioChange() {
        this.combosDeptosMunis.setOrdenZonas(EOrden.CODIGO);
        this.combosDeptosMunis.updateZonasItemList();
    }

    /*
     * @modified by juanfelipe.garcia :: 24-08-2013 :: #5405 adición de estadoGdb
     */
    public void aceptarPredioEnglobeProyectar() {
        try {
            for (TramitePredioEnglobe tpe : this.getTramite()
                .getTramitePredioEnglobes()) {
                tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                if (this.predioPrincipalEnglobe.getId().equals(tpe.getPredio().getId())) {
                    tpe.setEnglobePrincipal(ESiNo.SI.getCodigo());
//					if (mantieneNumeroPredial && tpe.getPredio().isMejora()){
//						tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
//						this.addMensajeError("Una mejora no puede mantenerse como principal"); 
//						return;
//					}
                    tpe.getPredio().setCondicionPropiedad(condicionPropiedadSeleccionada);
                }
            }
            this.getTramite().setEstadoGdb(Constantes.ACT_MODIFICAR_INFORMACION_GEOGRAFICA);
            this.getTramiteService().actualizarTramite(this.getTramite());
            Object[] mensajes = this.getTramiteService()
                .generarProyeccionActualizarCondicionPropiedad(
                    this.getTramite(), this.condicionPropiedadSeleccionada);
            if (hayError(mensajes)) {
                return;
            }

            // this.getTramiteService().generarProyeccion(this.tramite);
            this.proyectarConservacionMB.cargarPrediosEnglobe();
            this.prediosResultantes = this.proyectarConservacionMB.getpPrediosEnglobe();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Fallo en la proyección.");

        }
    }

    /**
     * Método que elimina el predio seleccionado y si es necesario elimina también la proyección del
     * trámite asociada
     *
     * @author fabio.navarrete
     */
    public void eliminarPredioSeleccionado() {
        // Si es englobe
        if (this.getTramite().isSegunda() && this.getTramite().isSegundaEnglobe()) {
            this.getTramiteService().eliminarProyeccion(this.getTramite());
        }
    }

    /**
     * Método encargado de la acción del botón aceptar para un englobe cuando no mantiene el número
     * predial
     *
     * @author fabio.navarrete
     */
    /*
     * @modified by juanfelipe.garcia :: 02-07-2014 :: se adiciono lógica para asociar los
     * propietarios de los predios vigentes al nuevo predio.
     */
    private void aceptarEnglobeNoMantiene() {
        for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
            tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
        }
        Object mensajes[] = this.getTramiteService().generarProyeccion(this.getTramite());
        if (this.hayError(mensajes)) {
            return;
        }
        /* si el sp crea el predio nuevo entonces hay que actualizarlo ojo que pasa cuando le dan
         * cancelar? */
        List<PPredio> predios = getConservacionService().buscarPprediosResultantesPorTramiteId(this.
            getTramite().getId());
        this.predioNuevo = new PPredio();

        for (PPredio p : predios) {
            if (p.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                try {
                    // Se hace busqueda porque el listado anterior no trae los predio con todas las colecciones
                    predioNuevo = getConservacionService().obtenerPPredioCompletoById(p.getId());
                } catch (Exception e) {
                    this.addMensajeError(e);
                }
                break;
            }
        }
        this.predioNuevo.setDepartamento(this.getTramite().getDepartamento());
        this.predioNuevo.setMunicipio(this.getTramite().getMunicipio());
        this.predioNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
            .getCodigo());

        this.predioNuevo
            .setCondicionPropiedad(this.condicionPropiedadSeleccionada);
        this.predioNuevo.setEstado(EPredioEstado.ACTIVO.getCodigo());
        //this.predioNuevo.setTipoAvaluo(" ");
        this.predioNuevo.setTipo(" ");

        //felipe.cadena::tipo_catastro::#8242
        String tipoCatastro = " ";
        if (this.prediosEnglobe != null) {
            if (!this.prediosEnglobe.isEmpty()) {
                tipoCatastro = this.prediosEnglobe.get(0).getTipoCatastro();
            }
        }

        this.predioNuevo.setDestino(" ");
        this.predioNuevo.setTipoCatastro(tipoCatastro);
        this.predioNuevo.setAreaConstruccion(0.0);
        this.predioNuevo.setAreaTerreno(0.0);

        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

        this.generarZonasSelectItems();
        this.generarSectoresSelectItems();
        this.generarComunasSelectItems();
        this.generarBarriosSelectItems();
        this.generarManzanasSelectItems();

        this.predioNuevo.setZonaUnidadOrganica(this.predioNuevo.getZona() != null ?
            (this.predioNuevo.getZona().equals("00") ? "00" : "01") : null);

        if (this.proyectarConservacionMB.isBanderaSoloMejorasEnglobe()) {
            Predio p = this.getPrediosEnglobe().get(0);
            String nTerreno = p.getTerreno();
            String nUnidad = this.getConservacionService().generarNumeroUnidadPorPiso(p.
                getNumeroPredial().substring(0, 26), true);
            this.predioNuevo.setTerreno(nTerreno);
            this.predioNuevo.setNumeroUnidad(nUnidad);
        }
        if (this.proyectarConservacionMB.isBanderaMejorasTerrenoEnglobe()) {

            String numeroBase = this.predioNuevo.getNumeroPredial().substring(0, 7);
            String sector = this.obtenerMaxItem(this.agruparAreasPorSector(this.prediosEnglobe));
            numeroBase += sector;
            String comuna = this.obtenerMaxItem(this.agruparAreasPorComuna(this.prediosEnglobe,
                numeroBase));
            numeroBase += comuna;
            String barrio = this.obtenerMaxItem(this.agruparAreasPorBarrio(this.prediosEnglobe,
                numeroBase));
            numeroBase += barrio;
            String manzana = this.obtenerMaxItem(this.agruparAreasPorManzana(this.prediosEnglobe,
                numeroBase));

            this.predioNuevo.setSector(sector);
            this.predioNuevo.setComuna(comuna);
            this.predioNuevo.setBarrio(barrio);
            this.predioNuevo.setManzanaVereda(manzana);
        }

        //asociar los propietarios de los predios originales a el nuevo predio como propietarios nuevos
        //no se puede como vigentes porque el predio nuevo no esta en firme
        try {
            for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {

                PPredio pPredioBase = this.getConservacionService()
                    .obtenerPPredioCompletoById(tpe.getPredio().getId());

                for (PPersonaPredio ppp : pPredioBase.getPPersonaPredios()) {
                    ppp.setId(null);
                    ppp.setPPredio(this.predioNuevo);
                    ppp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

                    PPersonaPredio personaPredio = this.getConservacionService().
                        guardarPPersonaPredio2(this.loggedInUser, ppp);
                }

            }
        } catch (Exception e) {
            LOGGER.error("Error en EntradaProyeccionMB#aceptarEnglobeNoMantiene: " +
                e.getMessage(), e);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Invocado con false desde setupNuevoPredio (Boton Adicionar predio en desenglobes) Invocado
     * con true desde aceptarEnglobeDesenglobeNoMantiene
     *
     * @param generarProyeccion
     * @modified by leidy.gonzalez 02/10/2014 Se elimina validación para desenglobes en la que se
     * verifica que la proyección del predio tenga una fecha de inscripción catastral para
     * asociarsela y tambien que no la asocie en caso contrario, para que esta sea nula.
     */
    private void aceptarDesenglobeNoMantiene(boolean generarProyeccion) {

        if (generarProyeccion) {
            if (this.condicionPropiedadSeleccionada
                .equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
                this.getTramite().setTipoInscripcion(ETramiteTipoInscripcion.PH
                    .getCodigo());
            }

            if (this.condicionPropiedadSeleccionada
                .equals(EPredioCondicionPropiedad.CP_8
                    .getCodigo())) {
                this.getTramite()
                    .setTipoInscripcion(ETramiteTipoInscripcion.CONDOMINIO
                        .getCodigo());
            }

            this.getTramite().setMantieneNumeroPredial(ESiNo.NO.getCodigo());
            Object mensajes[] = this.getTramiteService().generarProyeccion(this.getTramite());
            if (this.hayError(mensajes)) {
                return;
            }

            // Las unidades de construcción deben entrar como canceladas
            if (this.getTramite().getPredio().getUnidadConstruccions() != null) {
                try {
                    PPredio predioSetUnidades = this.getConservacionService()
                        .obtenerPPredioCompletoById(
                            this.proyectarConservacionMB.getTramite()
                                .getPredio().getId());
                    if (predioSetUnidades != null && predioSetUnidades.getPUnidadConstruccions() !=
                        null) {
                        for (PUnidadConstruccion pu : predioSetUnidades.getPUnidadConstruccions()) {
                            if (!this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {

                                // @modified david.cifuentes :: CU-NP-CO-238 :: Las construcciones 
                                // se proyectarán como canceladas, y se activarán o inscribiran al momento
                                // de ser asociadas:: 24/06/2015
                                if (!this.getTramite().isDesenglobeEtapas()) {
                                    pu.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
                                        getCodigo());
                                } else {
                                    pu.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.
                                        getCodigo());
                                }
                            } else {
                                pu.
                                    setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.
                                        getCodigo());
                            }

                        }
                        this.getConservacionService().actualizarPPredio(predioSetUnidades);
                    }
                } catch (Exception e) {
                    LOGGER.error("Error en EntradaProyeccionMB#aceptarDesenglobeNoMantiene: " +
                        e.getMessage(), e);
                }
            }
        }
        this.setupNuevoPredio(this.getTramite().getPredio());
        this.setNoPHCeros();
        this.predioNuevo
            .setCondicionPropiedad(this.condicionPropiedadSeleccionada);

        this.generarNumeroPredial();
    }
//--------------------------------------------------------------------------------------------------

    private void generarComunasSelectItems() {
        this.comunasSelectItems = new ArrayList<SelectItem>();
        // Si es Englobe
        if (this.getTramite().isSegunda() && this.getTramite().isSegundaEnglobe()) {
            this.loadSelectItemsEnglobe("Comuna", this.comunasSelectItems);
        }
    }
//--------------------------------------------------------------------------------------------------

    private void generarManzanasSelectItems() {
        this.manzanasSelectItems = new ArrayList<SelectItem>();
        // Si es Englobe
        if (this.getTramite().isSegunda() && this.getTramite().isSegundaEnglobe()) {
            this.loadSelectItemsEnglobe("ManzanaVereda",
                this.manzanasSelectItems);
        }
    }
//--------------------------------------------------------------------------------------------------

    private void generarBarriosSelectItems() {
        this.barriosSelectItems = new ArrayList<SelectItem>();
        // Si es Englobe
        if (this.getTramite().isSegunda() && this.getTramite().isSegundaEnglobe()) {
            this.loadSelectItemsEnglobe("Barrio", this.barriosSelectItems);
        }

    }
//--------------------------------------------------------------------------------------------------

    private void generarSectoresSelectItems() {
        this.sectoresSelectItems = new ArrayList<SelectItem>();
        // Si es Englobe
        if (this.getTramite().isSegunda() && this.getTramite().isSegundaEnglobe()) {
            this.loadSelectItemsEnglobe("Sector", this.sectoresSelectItems);
        }
    }
//--------------------------------------------------------------------------------------------------

    private void generarZonasSelectItems() {
        this.zonasSelectItems = new ArrayList<SelectItem>();
        // Si es Englobe
        if (this.getTramite().isSegunda() && this.getTramite().isSegundaEnglobe()) {
            this.loadSelectItemsEnglobe("Zona", this.zonasSelectItems);
        }
    }
//--------------------------------------------------------------------------------------------------

    private void loadSelectItemsEnglobe(String getterDescriptor,
        List<SelectItem> selectItems) {
        List<String> values = new ArrayList<String>();
        for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
            try {
                Method method = Predio.class.getDeclaredMethod("get" +
                    getterDescriptor);
                String value = (String) method.invoke(tpe.getPredio(),
                    new Object[0]);
                if (!values.contains(value)) {
                    values.add(value);
                }
            } catch (SecurityException e) {
                LOGGER.error("Error en EntradaProyeccionMB#loadSelectItemsEnglobe: " +
                    e.getMessage(), e);
            } catch (NoSuchMethodException e) {
                LOGGER.error("Error en EntradaProyeccionMB#loadSelectItemsEnglobe: " +
                    e.getMessage(), e);
            } catch (IllegalArgumentException e) {
                LOGGER.error("Error en EntradaProyeccionMB#loadSelectItemsEnglobe: " +
                    e.getMessage(), e);
            } catch (IllegalAccessException e) {
                LOGGER.error("Error en EntradaProyeccionMB#loadSelectItemsEnglobe: " +
                    e.getMessage(), e);
            } catch (InvocationTargetException e) {
                LOGGER.error("Error en EntradaProyeccionMB#loadSelectItemsEnglobe: " +
                    e.getMessage(), e);
            }
        }
        for (String str : values) {
            selectItems.add(new SelectItem(str));
        }
    }

//--------------------------------------------------------------------------------------------------
    public void generarNumeroPredial() {
        String condProp = this.predioNuevo.getCondicionPropiedad();
        String numPredial = this.getConservacionService()
            .buscarUltimoNumeroPredialDisponible(
                this.predioNuevo.getNumeroPredial());
        if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
            String nTerreno = this.predioNuevo.getTerreno();
            String numPaux = numPredial.substring(0, 17) + nTerreno + numPredial.substring(21);
            numPredial = numPaux;
            String nUnidad = this.getConservacionService().generarNumeroUnidadPorPiso(numPredial.
                substring(0, 26), true);
            numPaux = numPredial.substring(0, 26) + nUnidad;
            numPredial = numPaux;

        }

        this.predioNuevo.setNumeroPredial(numPredial);
        this.predioNuevo.setCondicionPropiedadNumeroPredial(condProp);
        this.predioNuevo.generarComponentesAPartirDeNumeroPredial();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" en la ventana de datosPredio.xhtml
     */
    /*
     * @documented pedro.garcia @modified pedro.garcia + orden + uso de enumeraciones en lugar de
     * valores quemados
     */
    public void aceptarDatosPredio() {

        List<PPersonaPredio> pPersonasPredioTemp;

        try {

            inicializarCamposVacios();

            this.predioNuevo.setFechaLog(new Date());
            this.predioNuevo.setUsuarioLog(this.loggedInUser.getLogin());
            this.predioNuevo.setTramite(this.getTramite());

            //Validaciones necesarias a raiz de los predios fiscales
            if (this.proyectarConservacionMB.isBanderaPredioFiscal()) {
                if (this.predioNuevo.getTerreno().length() != 4 && this.predioNuevo.getUnidad().
                    length() != 4) {
                    this.addMensajeError("El terreno debe ser de 4 digitos");
                    return;
                }

                try {
                    Integer.parseInt(this.predioNuevo.getTerreno());
                    Integer.parseInt(this.predioNuevo.getUnidad());
                } catch (NumberFormatException e) {
                    this.addMensajeError("Digite nuevamente el número del terreno");
                    return;
                }

                if (Integer.parseInt(this.predioNuevo.getTerreno()) < 0) {
                    this.addMensajeError("Digite nuevamente el número del terreno");
                    return;
                }

                PPredio ptemp = this.getConservacionService().getPPredioByNumeroPredial(
                    this.predioNuevo.getNumeroPredial());
                Predio prtemp = this.getConservacionService().getPredioByNumeroPredial(
                    this.predioNuevo.getNumeroPredial());

                if (ptemp != null || prtemp != null) {

                    if (prtemp != null) {
                        this.addMensajeError("Éste número predial ya exíste o está proyectado");
                        return;
                    }

                    if (!ptemp.getTramite().getId().equals(this.getTramite().getId()) ||
                        !this.getTramite().isSegundaEnglobe()) {
                        this.addMensajeError("Éste número predial ya exíste o está proyectado");
                        return;
                    }
                }
            }

            if (this.getTramite().isSegundaDesenglobe()) {
                if (this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_9.getCodigo()) ||
                    this.predioNuevo.getCondicionPropiedad().equals(
                        EPredioCondicionPropiedad.CP_8.getCodigo())) {
                    PFichaMatriz fichaMatriz = this.getConservacionService()
                        .guardarPPredioYFichaMatriz(this.predioNuevo,
                            this.loggedInUser);

                    this.predioNuevo = fichaMatriz.getPPredio();
                    if (this.predioNuevo.getPFichaMatrizs() == null) {
                        this.predioNuevo
                            .setPFichaMatrizs(new ArrayList<PFichaMatriz>());
                    }
                    this.predioNuevo.getPFichaMatrizs().add(fichaMatriz);

                    // Asociar Unidades de construcción
                    this.asociarUnidadesDeConstruccionANuevoPredio(this.getTramite().getPredio());

                    this.proyectarConservacionMB
                        .setPredioSeleccionado(this.predioNuevo);
                } else {
                    this.predioNuevo = this.getConservacionService()
                        .guardarActualizarPPredio(this.predioNuevo);
                }

                List<PPersonaPredio> pps = new ArrayList<PPersonaPredio>();

                PPredio ppAux = this.getConservacionService().obtenerPPredioCompletoById(
                    this.proyectarConservacionMB.getTramite().getPredio().getId());
               if(ppAux != null) {
                   pPersonasPredioTemp = ppAux.getPPersonaPredios();

                   // Consultar los personasPredio del predio base, y únicamente
                   // asociar los iniciales, puesto que al mantener el número
                   // predial pueden adicionarse propietarios al predio sin ser los
                   // iniciales.
                   pPersonasPredioTemp = buscarPPersonasPrediosBase(pPersonasPredioTemp);

                   // Esto solo aplica para desenglobe, y se trae el ppredio cuyo id es el id del predio del tramite
                   for (PPersonaPredio ppAnterior : pPersonasPredioTemp) {
                       PPersonaPredio pp = new PPersonaPredio();
                       pp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                       pp.setFechaInscripcionCatastral(null);
                       pp.setFechaLog(new Date());
                       pp.setParticipacion(ppAnterior.getParticipacion());

                       PPersona per = new PPersona();
                       per.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                       per.setDigitoVerificacion(ppAnterior.getPPersona().getDigitoVerificacion());
                       per.setFechaLog(new Date());
                       per.setNumeroIdentificacion(ppAnterior.getPPersona().getNumeroIdentificacion());
                       per.setPrimerApellido(ppAnterior.getPPersona().getPrimerApellido());
                       per.setSegundoApellido(ppAnterior.getPPersona().getSegundoApellido());
                       per.setPrimerNombre(ppAnterior.getPPersona().getPrimerNombre());
                       per.setSegundoNombre(ppAnterior.getPPersona().getSegundoNombre());
                       per.setRazonSocial(ppAnterior.getPPersona().getRazonSocial());
                       per.setSigla(ppAnterior.getPPersona().getSigla());
                       per.setTipoIdentificacion(ppAnterior.getPPersona().getTipoIdentificacion());
                       per.setTipoPersona(ppAnterior.getPPersona().getTipoPersona());
                       per.setUsuarioLog(this.loggedInUser.getLogin());
                       /* List<PPersonaPredio> ppp=new ArrayList<PPersonaPredio>(); ppp.add(pp);
                        * per.setPPersonaPredios(ppp); */

                       pp.setPPersona(per);

                       pp.setPPredio(this.predioNuevo);
                       pp.setTipo(ppAnterior.getTipo());
                       pp.setUsuarioLog(this.loggedInUser.getLogin());

                       pps.add(pp);
                   }
                   predioNuevo.setPPersonaPredios(pps);
               }

                // Asociar las direcciones al nuevo predio.
                this.asociarDireccionesAPredioNuevo();

                // Asociar las zonas homogeneas al nuevo predio
                this.asociarZonasHomogeneas();

                this.predioNuevo = this.getConservacionService()
                    .guardarActualizarPPredio(this.predioNuevo);

            } else {
                if (this.getTramite().isModificacionInscripcionCatastral()) {
//					this.predioNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                    this.predioNuevo.setTipoCatastro(this.proyectarConservacionMB.getTramite().
                        getPredio().getTipoCatastro());
                    this.asociarPPersonaPredioBase();
                }

                this.predioNuevo = this.getConservacionService()
                    .guardarActualizarPPredio(this.predioNuevo);
            }

            this.proyectarConservacionMB.setPredioSeleccionado(this.predioNuevo);

            for (PPredio pp : this.prediosResultantes) {
                pp.setUltimoDesenglobe(false);
            }
            this.predioNuevo.setUltimoDesenglobe(true);

            if (!this.prediosResultantes.contains(this.predioNuevo)) {
                this.prediosResultantes.add(this.predioNuevo);
            } else {
                int idx = this.prediosResultantes.indexOf(this.predioNuevo);
                this.prediosResultantes.remove(this.predioNuevo);
                this.prediosResultantes.set(idx, this.predioNuevo);
            }

            // Consultar si se debe crear una ficha matriz.
            consultarCreacionFichaMatriz();
            this.proyeccionValidaBool = true;
        } catch (Exception e) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }
    }

    // ---------------------------------------------------------------- //
    /**
     * Método usado para asociar las {@link PPersonaPredio} del {@link Predio} al nuevo
     * {@link PPredio}
     *
     * @author david.cifuentes
     */
    public void asociarPPersonaPredioBase() throws Exception {

        List<PPersonaPredio> pPersonasPredioTemp;

        PPredio aux = this
            .getConservacionService()
            .obtenerPPredioCompletoById(
                this.proyectarConservacionMB.getTramite().getPredio()
                    .getId());

        pPersonasPredioTemp = aux.getPPersonaPredios();

        if (pPersonasPredioTemp != null) {
            pPersonasPredioTemp = buscarPPersonasPrediosBase(pPersonasPredioTemp);

            for (PPersonaPredio ppAnterior : pPersonasPredioTemp) {
                ppAnterior.setPPredio(this.predioNuevo);
            }
            this.predioNuevo.setPPersonaPredios(pPersonasPredioTemp);
        }
    }

    // ---------------------------------------------------------------- //
    /**
     * Método que a partir de una lista de pPersonasPredio descarta los que no venian con el predio
     * inicialmente, es decir los adicionados en la proyección, este caso sucede particularmente
     * cuando se mantiene el número predial, pues no se pueden distinguir los que vienen de los
     * adicionados en la tabla PPersonaPredio.
     *
     * @author david.cifuentes
     */
    public List<PPersonaPredio> buscarPPersonasPrediosBase(
        List<PPersonaPredio> pPersonaPredio) {
        List<PersonaPredio> personasPredioBase = this.getConservacionService()
            .buscarPersonaPredioPorPredioId(
                this.proyectarConservacionMB.getTramite().getPredio()
                    .getId());

        List<PPersonaPredio> auxRemove = new ArrayList<PPersonaPredio>();
        if (personasPredioBase != null && !personasPredioBase.isEmpty()) {
            for (PersonaPredio pp : personasPredioBase) {
                for (PPersonaPredio ppp : pPersonaPredio) {
                    if (ppp.getId().longValue() != pp.getId().longValue()) {
                        auxRemove.add(ppp);
                    }
                }
            }
        }
        if (auxRemove != null && !auxRemove.isEmpty()) {
            for (PPersonaPredio pp : auxRemove) {
                pPersonaPredio.remove(pp);
            }
        }
        return pPersonaPredio;
    }

    // ---------------------------------------------------------------- //
    /**
     * Método que realiza el setup de las construcciones al predio nuevo.
     *
     * @author david.cifuentes
     */
    public void asociarUnidadesDeConstruccionANuevoPredio(Predio predioData) {

        List<PUnidadConstruccion> pUnidadConstruccions = this.getConservacionService().
            buscarUnidadesDeConstruccionPorPPredioId(predioData.getId(), false);
        if (pUnidadConstruccions != null && !pUnidadConstruccions.isEmpty()) {
            for (PUnidadConstruccion puc : pUnidadConstruccions) {
                puc.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                puc.setFechaLog(new Date(System.currentTimeMillis()));
                puc.setUsuarioLog(this.proyectarConservacionMB.getUsuario().getLogin());
                puc.setPPredio(this.predioNuevo);
                puc.setId(null);
            }
            this.predioNuevo.setPUnidadConstruccions(pUnidadConstruccions);
        }
    }

    // ---------------------------------------------------------------- //
    /**
     * Método que asocia las direcciones del predio del trámite al nuevo predio.
     *
     * @author david.cifuentes
     */
    public void asociarDireccionesAPredioNuevo() {

        PPredio aux = this.getConservacionService().obtenerPPredioPorIdTraerDirecciones(this.
            getTramite().getPredio().getId());
        if (aux != null && aux.getPPredioDireccions() != null && !aux.getPPredioDireccions().
            isEmpty()) {
            List<PPredioDireccion> direcciones = new ArrayList<PPredioDireccion>();
            for (PPredioDireccion pd : aux.getPPredioDireccions()) {
                pd.setUsuarioLog(this.loggedInUser.getLogin());
                pd.setPPredio(this.predioNuevo);
                pd.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
                pd.setId(null);
                if (pd.getDireccion() != null &&
                    !pd.getDireccion().trim().isEmpty() &&
                    !direcciones.contains(pd)) {
                    direcciones.add(pd);
                }
            }
            this.predioNuevo.setPPredioDireccions(direcciones);
        }
    }

    // ------------------------------------------------------ //
    /**
     * Método que consulta las zonas homogeneas del predio a desenglobar y se las asocia al predio
     * nuevo
     *
     * @author david.cifuentes
     */
    public void asociarZonasHomogeneas() {
        List<PPredioZona> pPredioZonas = this.getConservacionService()
            .buscarZonasHomogeneasPorPPredioId(
                this.getTramite().getPredio().getId());
        List<PPredioZona> pPredioZonasAAsociar = new ArrayList<PPredioZona>();
        if (pPredioZonas != null) {
            for (PPredioZona ppz : pPredioZonas) {
                ppz.setPPredio(this.predioNuevo);
                ppz.setUsuarioLog(this.loggedInUser.getLogin());
                ppz.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                    .getCodigo());
                ppz.setId(null);
                ppz.setFechaLog(new Date(System.currentTimeMillis()));
                pPredioZonasAAsociar.add(ppz);
            }
            this.predioNuevo.setPPredioZonas(pPredioZonasAAsociar);
        }
    }

//-------------------------------------------------------------------------
    /**
     * Método ejecutado para inicializar los campos vacios, los cuales debido al manejo en cascada
     * de los combos de selección para predio nuevo fue necesario inicializarlos con valor por
     * defecto Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE
     *
     * @deprecated chambonada de Gabriel
     */
    @Deprecated
    private void inicializarCamposVacios() {

        if (this.predioNuevo.getZona().equals(
            Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)) {
            this.predioNuevo.setZona(null);
            this.predioNuevo.setZonaUnidadOrganica(null);
        }

        if (this.predioNuevo.getSector().equals(
            Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)) {
            this.predioNuevo.setSector(null);
        }

        if (this.predioNuevo.getComuna().equals(
            Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)) {
            this.predioNuevo.setComuna(null);
        }

        if (this.predioNuevo.getBarrio().equals(
            Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)) {
            this.predioNuevo.setBarrio(null);
        }

        if (this.predioNuevo.getManzanaVereda().equals(
            "000" + Constantes.SEPARADOR_CADENAS_DOCUMENTO_NOMBRE)) {
            this.predioNuevo.setManzanaVereda(null);
        }

    }

    public void seleccionarTodosResultantes() {

        RectificacionComplementacionMB rc = (RectificacionComplementacionMB) UtilidadesWeb.
            getManagedBean("rc");
        ProyectarConservacionMB pc = (ProyectarConservacionMB) UtilidadesWeb.getManagedBean(
            "proyectarConservacion");
        for (PPredio pPredio : this.prediosResultantes) {
            pPredio.setSelected(this.selectAllResultantes);
        }

        this.actualizarPredioSeleccionado();
    }

    /**
     * Selecciona todas las manzanas
     *
     * @author felipe.cadena
     */
    public void seleccionarTodosManzanas() {

        for (PManzanaVereda manzana : this.manzanasDesenglobe) {
            manzana.setSelected(this.selectAllResultantesManzanas);
        }
        this.actualizarSeleccionManzanas();
    }

    /**
     * Selecciona todas las manzanas
     *
     * @author felipe.cadena
     */
    public void actualizarSeleccionManzanas() {

        this.retirarManzana = false;
        for (PManzanaVereda manzana : this.manzanasDesenglobe) {
            if (manzana.isSelected()) {
                this.retirarManzana = true;
                break;
            }
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * listener de la selección en la tabla de predios. Función usada para actualizar y armar la
     * lista prediosResultantesSeleccionados
     */
    public void updateSelectedPrediosResultantes() {

        this.actualizarPredioSeleccionado();
        if (this.proyectarConservacionMB.getPredioSeleccionado() != null &&
            this.proyectarConservacionMB.isValidacion() &&
            !this.getProyectarConservacionMB().getPredioSeleccionado().isEsPredioFichaMatriz()) {
            this.proyectarConservacionMB.setTab(1);
        } else {
            this.proyectarConservacionMB.setTab(0);
        }
    }

    /**
     * Listener para cuando se slecciona un predio nuevo
     *
     * @author felipe.cadena
     * @param ev
     */
    public void nuevoPredioSeleccionado(ValueChangeEvent ev) {

        for (PPredio pPredio : this.prediosResultantes) {
            pPredio.setSelected(false);
        }

    }

    /**
     * Listener para cuando se selecciona un predio original del tramite
     *
     * @author felipe.cadena
     * @param ev
     */
    public void originalPredioSeleccionado(ValueChangeEvent ev) {

        if (this.prediosResultantesCancelacionMasiva != null) {
            for (PPredio pPredio : this.prediosResultantesCancelacionMasiva) {
                pPredio.setSelected(false);
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * obtiene los datos del PPredio seleccionado de la tabla de predios. Si se trata de un predio
     * que fue encontrado como omitido en un trámite de quinta, primero genera la proyección del
     * trámite para que se llenen las tablas correspondientes a un PPredio que 'nace' del predio
     * omitido seleccionado
     */
    private void actualizarPredioSeleccionado() {
        this.prediosResultantesSeleccionados = new ArrayList<PPredio>();

        //retira predios nuevos anteriores seleccionados
        if (this.getTramite().isCancelacionMasiva()) {
            for (int i = 0; i < this.prediosResultantesSeleccionados.size(); i++) {
                PPredio pp = this.prediosResultantesSeleccionados.get(i);
                if (!pp.isPredioOriginalTramite()) {
                    this.prediosResultantesSeleccionados.remove(pp);
                }
            }
        }

        for (PPredio pPredio : this.prediosResultantes) {
            if (pPredio.isSelected()) {

                if (this.getTramite().isQuintaNuevo() && pPredio.isMejora()){
                    this.quintaNuevoCond5 = true;
                    this.setQuintaNuevoCond5(true);
                }else{
                    this.quintaNuevoCond5 = false;
                }

                this.banderaRetirarPrediosBase = true;
                this.banderaRetirarPrediosResultantes = false;

                this.prediosCancelarSeleccionados.add(pPredio);

                String condicionPropiedad = pPredio.getCondicionPropiedad();

                if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
                    this.getTramite().setTipoInscripcion(ETramiteTipoInscripcion.CONDOMINIO.
                        getCodigo());
                } else if (condicionPropiedad.equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {

                    this.getTramite().setTipoInscripcion(ETramiteTipoInscripcion.PH.getCodigo());
                }

                if (this.usePredioOmitido && this.predioOmitidoEncontrado != null) {
                    Tramite tramiteToUpdate = null;
                    try {
                        tramiteToUpdate = this.getTramite();
                        tramiteToUpdate.setPredio(this.predioOmitidoEncontrado);
                        this.getTramiteService().actualizarTramite(tramiteToUpdate,
                            this.loggedInUser);
                        if (!this.getTramite().isQuintaOmitido()) {
                            // Se agrega condicion anterior porque no se ve la necesidad de reproyectar en quinta omitido. 
                            // Queda la duda para los otros casos
                            Object mensajes[] = this.getTramiteService().generarProyeccion(
                                tramiteToUpdate.getId());
                            if (this.hayError(mensajes)) {
                                return;
                            }
                        }
                    } catch (Exception e) {
                        String idTramite = (tramiteToUpdate == null || tramiteToUpdate.getId() ==
                            null) ? "null" :
                                tramiteToUpdate.getId().toString();
                        LOGGER.error(
                            "ocurrió un error actualizando o generando la proyección del " +
                            "trámite con id " + idTramite + ":" + e.getMessage(), e);
                    }
                }
                if (pPredio.isEsPredioFichaMatriz()) {
                    this.proyectarConservacionMB.setMostrarFichaMatrizRectificacion(true);
                    PFichaMatriz pfm = this.getConservacionService()
                        .buscarPFichaMatrizPorPredioId(pPredio.getId());
                    this.proyectarConservacionMB.setFichaMatrizSeleccionada(pfm);
                } else {
                    this.proyectarConservacionMB.setMostrarFichaMatrizRectificacion(false);
                }

                try {
                    boolean esUltimo = pPredio.isUltimoDesenglobe();
                    pPredio = this.getConservacionService()
                        .obtenerPPredioCompletoById(pPredio.getId());
                    pPredio.setUltimoDesenglobe(esUltimo);
                    this.prediosResultantesSeleccionados.add(pPredio);
                } catch (Exception e) {
                    LOGGER.error("ocurrió un error obteniendo el PPredio completo con id " +
                        pPredio.getId() + e.getMessage(), e);
                }
            } else {
                this.prediosCancelarSeleccionados.remove(pPredio);
            }
        }
        if (this.getTramite().isCancelacionMasiva()) {

            for (PPredio pp : this.prediosResultantesCancelacionMasiva) {
                if (pp.isSelected()) {

                    this.prediosResultantesSeleccionados.clear();

                    for (PPredio ppOriginal : this.prediosResultantes) {
                        ppOriginal.setSelected(false);
                    }

                    this.banderaRetirarPrediosBase = false;
                    this.banderaRetirarPrediosResultantes = true;
                    this.prediosResultantesSeleccionados.add(pp);
                }
            }
        }

        if (this.prediosResultantesSeleccionados.size() == 1) {

            //felipe.cadena:: DESENGLOBE_ETAPAS ::Se inicializan los campos para solo habilitar los requeridos dependiendo el tramite
            if (this.getTramite().isDesenglobeEtapas() && !this.getTramite().isEnglobeVirtual()) {
                RectificacionComplementacionMB rc = (RectificacionComplementacionMB) UtilidadesWeb.
                    getManagedBean("rc");
                if (this.prediosResultantesSeleccionados
                    .get(0).isPredioOriginalTramite()) {
                    rc.inicializarCamposParaEtapas();
                } else {
                    rc.habilitarTodo();
                }
            } else if (this.getTramite().isCancelacionMasiva()) {
                RectificacionComplementacionMB rc = (RectificacionComplementacionMB) UtilidadesWeb.
                    getManagedBean("rc");
                if (this.prediosResultantesSeleccionados.get(0).isPredioOriginalTramite()) {
                    rc.deshabilitarTodo();
                } else {
                    rc.habilitarTodo();
                }
                //leidy.gonzalez:: ENGLOBE_VIRTUAL ::Se inicializan los campos para solo habilitar los requeridos dependiendo el tramite
            } else if (this.getTramite().isEnglobeVirtual()) {
                RectificacionComplementacionMB rc = (RectificacionComplementacionMB) UtilidadesWeb.
                    getManagedBean("rc");
                if (this.prediosResultantesSeleccionados
                    .get(0).isPredioOriginalTramite()) {
                    rc.inicializarCamposEnglobeVirtual();
                } else {
                    rc.habilitarTodo();
                }
            }

            this.proyectarConservacionMB.setPredioSeleccionado(this.prediosResultantesSeleccionados.
                get(0));

            if (this.getTramite().isSegundaDesenglobe() && !this.getTramite().isDesenglobeEtapas()) {
                this.proyectarConservacionMB.cargarConstruccionesAsociadasPredioOrigen();
            }

            if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
                int idx = this.prediosResultantes.indexOf(this.proyectarConservacionMB.
                    getPredioSeleccionado());
                PPredio ppredioSeleccionado = this.prediosResultantes.get(idx);
                if (ppredioSeleccionado.isSelected()) {

                    PPredio pp = this.getConservacionService().obtenerPPredioDatosAvaluo(
                        this.proyectarConservacionMB.getTramite().getPredio().getId());

                    if (pp.getPUnidadConstruccions() != null) {
                        List<UnidadConstruccion> UCVigententes = new ArrayList<UnidadConstruccion>();
                        for (PUnidadConstruccion puc : pp.getPUnidadesConstruccionConvencional()) {
                            if (puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.
                                getCodigo())) {
                                UCVigententes.add(this.generalMB.
                                    copiarPUnidadConstruccionAUnidadConstruccion(puc));
                            }
                        }
                        this.proyectarConservacionMB.setUnidadesConsVigentesDesenglobeMejora(
                            UCVigententes);

                        List<UnidadConstruccion> UCVigententesNC =
                            new ArrayList<UnidadConstruccion>();
                        for (PUnidadConstruccion puc : pp.getPUnidadesConstruccionNoConvencional()) {
                            if (puc.getCancelaInscribe().equals(EProyeccionCancelaInscribe.CANCELA.
                                getCodigo())) {
                                UCVigententesNC.add(this.generalMB.
                                    copiarPUnidadConstruccionAUnidadConstruccion(puc));
                            }
                        }
                        this.proyectarConservacionMB.
                            setUnidadesConsNoConvencionalesVigentesDesenglobeMejora(UCVigententesNC);
                    }
                }
            }

            this.proyectarConservacionMB.setAvaluo();
        } else {
            this.proyectarConservacionMB.setPredioSeleccionado(null);
        }

        // Activar o desactivar los botones 'Adicionar' y 'Retirar' según su
        // mutación.
        this.revisarActivacionAdicionarRetirar();

        JustificacionProyeccionMB jpMB = (JustificacionProyeccionMB) UtilidadesWeb.getManagedBean(
            "justificacionProyeccion");
        jpMB.setJustificacionSeleccionada(null);
        jpMB.init();

        boolean noSelect = true;
        if (this.getTramite().isCancelacionMasiva()) {
            for (PPredio pp : this.prediosResultantesCancelacionMasiva) {
                if (pp.isSelected()) {
                    noSelect = false;
                }
            }
        }

        if (noSelect) {
            this.banderaRetirarPrediosResultantes = false;
        }

        noSelect = true;
        for (PPredio pp : this.prediosResultantes) {
            if (pp.isSelected()) {
                noSelect = false;
            }
        }

        if (noSelect) {
            this.banderaRetirarPrediosBase = false;
        }

    }

    // ------------------------------------------------------------------------ //
    /**
     * Método que realiza las validaciones para activar y desactivar los botones de retirar y
     * adicionar predio para mutación de segunda desenglobe en los que se ha seleccionado la
     * condición de propiedad 8 o 9.
     *
     * @author david.cifuentes
     */
    public boolean getSelectedFichaMatrizBool() {

        // Si se seleccionó un único predio
        if (this.prediosResultantesSeleccionados.size() == 1 &&
            this.prediosResultantes.size() == 1) {
            // El único predio que puede retirarse es el predio ficha matriz.
            if (this.prediosResultantesSeleccionados.get(0) != null &&
                this.prediosResultantesSeleccionados.get(0)
                    .isEsPredioFichaMatriz()) {
                return true;
            }
        } // Si se seleccionaron todos los predios
        else if (this.prediosResultantesSeleccionados.size() == this.prediosResultantes
            .size()) {
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------------ //
    /**
     * Método que activa o desactiva los botones de adicionar y retirar predio en la mutación de
     * segunda desenglobe seteando en la variable selectedPredioFichaMatrizBool en 'false' cuando la
     * condición de propiedad elegida es 8 o 9, y según la selección de predios. Para todas las
     * otras mutaciones la variable es 'true'.
     *
     * @author david.cifuentes
     */
    public void revisarActivacionAdicionarRetirar() {
        if (this.getTramite().isSegundaDesenglobe()) {

            if (this.condicionPropiedadSeleccionada != null && (this.condicionPropiedadSeleccionada.
                equals("8") ||
                this.condicionPropiedadSeleccionada.equals("9"))) {
                this.selectedPredioFichaMatrizBool = this
                    .getSelectedFichaMatrizBool();

                // Set del coeficiente de copropiedad al predio seleccionado
                if (this.prediosResultantesSeleccionados != null &&
                    this.prediosResultantesSeleccionados.size() == 1) {
                    this.prediosResultantesSeleccionados = this.proyectarConservacionMB
                        .cargarCoeficientesCopropiedadPrediosFichaMatriz(
                            this.prediosResultantesSeleccionados);

                    this.proyectarConservacionMB.getPredioSeleccionado()
                        .setCoeficienteCopropiedadFM(
                            this.prediosResultantesSeleccionados.get(0)
                                .getCoeficienteCopropiedadFM());
                }
            } else {
                this.selectedPredioFichaMatrizBool = true;
            }
        } else {
            this.selectedPredioFichaMatrizBool = true;
        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "Retirar predios" en la tabla de predios resultantes
     */
    /*
     * @documented pedro.garcia @modified pedro.garcia 02-08-2012 adicionada condición para trámites
     * de quinta
     */
    public void retirarPrediosSeleccionados() {

        this.quintaNuevoCond5 = false;

        if (this.prediosResultantesSeleccionados == null || this.prediosResultantesSeleccionados.
            isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.toString());
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.getMensajeTecnico());
            return;
        }

        if (this.getTramite().isSegundaEnglobe() || this.getTramite().isSegundaDesenglobe()) {

            //felipe.cadena:: Eliminar predios de los desenglobes por etapas
            if (this.getTramite().isDesenglobeEtapas()) {

                if (!this.validarRetirarPrediosEtapas(this.prediosResultantesSeleccionados,
                    this.prediosResultantes)) {
                    return;
                }
                for (PPredio pp : this.prediosResultantesSeleccionados) {

                    if (!pp.isPredioOriginalTramite()) {
                        this.getConservacionService().reversarProyeccionPredio(this.getTramite().
                            getId(), pp.getId());
                        PFichaMatrizPredio pfmp = this.getConservacionService().
                            obtenerPFichaMatrizPredioPorNumeroPredial(pp.getNumeroPredial());
                        PFichaMatriz pfm = this.getConservacionService().
                            buscarFichaPorNumeroPredialProyectada(pfmp.getNumeroPredial().substring(
                                0, 21));
                        pfm.getPFichaMatrizPredios().remove(pfmp);
                        pfm = this.getConservacionService().actualizarFichaMatriz(pfm);
                        pfmp.setPFichaMatriz(pfm);
                        this.getConservacionService().eliminarPFichaMatrizPredio(pfmp);

                    } else {
                        pp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                        pp.setEstado(EPredioEstado.CANCELADO.getCodigo());
                        pp = this.getConservacionService().actualizarPPredio(pp);
                        PFichaMatrizPredio pfmp = this.getConservacionService().
                            obtenerPFichaMatrizPredioPorNumeroPredial(pp.getNumeroPredial());
                        pfmp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                        PFichaMatrizPredio[] pfmps = {pfmp};
                        this.getConservacionService().actualizarPFichaMatrizPredios(pfmps);
                    }

                    this.prediosResultantes.remove(pp);
                }
                return;
            }

            // Se usa para eliminar los predios cuando es un desenglobe de manazanas
            if (this.creaManzanas) {

                this.proyectarConservacionMB.setPredioSeleccionado(null);

                if (this.prediosResultantesSeleccionados.size() == this.prediosResultantes.size()) {
                    for (PPredio pp : this.prediosResultantesSeleccionados) {
                        this.prediosResultantes.clear();

                        if (!pp.getId().equals(this.getTramite().getPredio().getId())) {
                            this.getConservacionService().reversarProyeccionPredio(
                                this.getTramite().getId(), pp.getId());
                        } else {
                            pp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                            pp = this.getConservacionService().actualizarPPredio(pp);
                        }

                        this.getTramite().setMantieneNumeroPredial(ESiNo.NO.getCodigo());
                        this.getTramiteService().guardarActualizarTramite2(this.getTramite());
                    }
                    return;
                }

                for (PPredio pp : this.prediosResultantesSeleccionados) {
                    this.prediosResultantes.remove(pp);
                }

                boolean eliminarOk = this.validarEliminacionPredios(
                    this.prediosResultantesSeleccionados);

                if (eliminarOk) {
                    for (PPredio pp : this.prediosResultantesSeleccionados) {

                        if (pp.isEsPredioFichaMatriz()) {
                            List<PPredio> unidaesPHC = this.obtenerPrediosFM(pp.getNumeroPredial().
                                substring(0, 22));
                            //se eliminan todas las unidades de la ficha matriz.
                            for (PPredio pphc : unidaesPHC) {
                                this.prediosResultantes.remove(pphc);
                                this.getConservacionService().reversarProyeccionPredio(this.
                                    getTramite().getId(), pphc.getId());

                            }

                        }

                        if (!pp.getId().equals(this.getTramite().getPredio().getId())) {
                            this.getConservacionService().reversarProyeccionPredio(
                                this.getTramite().getId(), pp.getId());
                        } else {
                            pp.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                            pp = this.getConservacionService().actualizarPPredio(pp);
                            this.getTramite().setMantieneNumeroPredial(ESiNo.NO.getCodigo());
                            this.getTramiteService().guardarActualizarTramite2(this.getTramite());
                        }

                        if (this.getTramite().getPredio().getNumeroPredial().equals(pp.
                            getNumeroPredial())) {
                            this.getTramite().setMantieneNumeroPredial(ESiNo.NO.getCodigo());
                            this.getTramiteService().guardarActualizarTramite2(this.getTramite());
                        }
                    }
                } else {
                    for (PPredio pp : this.prediosResultantesSeleccionados) {
                        this.prediosResultantes.add(pp);
                    }

                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError(
                        "Solo se puede retirar los últimos predios o todos los predios desenglobados");
                }
                return;

            }

            if (this.prediosResultantesSeleccionados.size() == this.prediosResultantes.size()) {
                this.getTramiteService().eliminarProyeccion(this.getTramite());
                this.proyectarConservacionMB.init();
                this.init();
                return;
            }

            if (this.prediosResultantesSeleccionados.size() > 1) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "Solo se puede retirar el último predio o todos los predios desenglobados");
                return;
            }

            if (!this.prediosResultantesSeleccionados.get(0).isUltimoDesenglobe() &&
                !this.prediosResultantesSeleccionados.get(0).getCancelaInscribe().equals("M")) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError(
                    "Solo se puede retirar el último predio o todos los predios desenglobados");
                return;
            }

            List<IProyeccionObject> pPrediosProyeccionSeleccionados =
                new ArrayList<IProyeccionObject>();

            for (IProyeccionObject objProy : this.prediosResultantesSeleccionados) {
                pPrediosProyeccionSeleccionados.add(objProy);
            }

            this.getConservacionService().eliminarProyecciones(
                this.loggedInUser, pPrediosProyeccionSeleccionados);

            for (PPredio pPredio : this.prediosResultantesSeleccionados) {
                this.prediosResultantes.remove(pPredio);
            }
            this.actualizarPredioSeleccionado();

            int len = this.prediosResultantes.size();
            if (len == 1) {
                this.prediosResultantes.get(0).setUltimoDesenglobe(true);

            } else {
                if (len > 0) {
                    if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
                        PPredio ultimo = this.obtenerUltimoPredioPorUnidad(this.prediosResultantes);
                        int idx = this.prediosResultantes.indexOf(ultimo);
                        this.prediosResultantes.get(idx).setUltimoDesenglobe(true);
                    } else {
                        PPredio ultimo = this.obtenerUltimoPredioPorTerreno(this.prediosResultantes);
                        int idx = this.prediosResultantes.indexOf(ultimo);
                        this.prediosResultantes.get(idx).setUltimoDesenglobe(true);
                    }
                }
            }
        } //D: si el trámite es de quinta, solo debería haber un predio resultante, y por lo tanto uno
        //   seleccionado. Este predio debería estar como 'I' en la tabla PPredio.
        //   Se debe borrar el PPredio
        else if (this.getTramite().isQuinta() || this.getTramite().
            isModificacionInscripcionCatastral()) {

            this.getConservacionService().eliminarProyeccion(
                this.loggedInUser, this.prediosResultantes.get(0));
            this.prediosResultantes.clear();
            this.proyectarConservacionMB.setPredioSeleccionado(null);
        }
    }

    /**
     * Función que determina cuál es la condición predominante entre las ingresadas como parámetro
     *
     * @param condicionesPropiedad
     * @return
     * @author fabio.navarrete
     */
    private String condicionPropiedadPredominante(List<String> condicionesPropiedad) {
        List<String> condicionesPropiedadNoRepetidas = new ArrayList<String>();
        for (String str : condicionesPropiedad) {
            if (!condicionesPropiedadNoRepetidas.contains(str)) {
                condicionesPropiedadNoRepetidas.add(str);
            }
        }

        String condicionPredominante = condicionesPropiedadNoRepetidas.get(0);

        for (int i = 1; i < condicionesPropiedadNoRepetidas.size(); i++) {
            String str = condicionesPropiedadNoRepetidas.get(i);
            condicionPredominante = this.condicionPropiedadPredominante(
                condicionPredominante, str);
            if (condicionPredominante == null || condicionPredominante.equals("?")) {
                break;
            }
        }

        return condicionPredominante;
    }

    private String condicionPropiedadPredominante(String cond1, String cond2) {
        int idx1 = cond1.equals("0") ? 0 : Integer.valueOf(cond1) - 1;
        int idx2 = cond2.equals("0") ? 0 : Integer.valueOf(cond2) - 1;
        return Constantes.MATRIZ_CONDICION_PROPIEDAD_PREDOMINANTE[idx1][idx2];
    }

    /**
     * Método que recibe el action del botón Aceptar decidiendo finalmente qué método invocar según
     * el tipo y subtipo de trámite
     *
     * @author fabio.navarrete
     */
    public void aceptarEnglobeDesenglobeNoMantiene() {
        this.proyeccionValidaBool = false;

        if (this.getTramite().isSegundaEnglobe()) {
            this.aceptarEnglobeNoMantiene();
        }
        if (this.getTramite().isSegundaDesenglobe()) {
            this.aceptarDesenglobeNoMantiene(true);
        }
    }

    /**
     * Método que se ejecuta en el botón aceptar en la pantalla inicial de la proyección cuando se
     * selecciona la opción de mantener número predial
     *
     * @author fabio.navarrete
     */
    public void aceptarEnglobeDesenglobeMantiene() {
        if (this.getTramite().isSegundaDesenglobe()) {
            if (this.condicionPropiedadSeleccionada
                .equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
                this.getTramite().setTipoInscripcion(ETramiteTipoInscripcion.PH
                    .getCodigo());
            }

            if (this.condicionPropiedadSeleccionada
                .equals(EPredioCondicionPropiedad.CP_8
                    .getCodigo())) {
                this.getTramite()
                    .setTipoInscripcion(ETramiteTipoInscripcion.CONDOMINIO
                        .getCodigo());
            }

            this.getTramite().setMantieneNumeroPredial(ESiNo.SI.getCodigo());
            this.getTramiteService().actualizarTramite(this.getTramite());

            if (this.condicionPropiedadSeleccionada.equals(EPredioCondicionPropiedad.CP_9.
                getCodigo()) ||
                this.condicionPropiedadSeleccionada.equals(EPredioCondicionPropiedad.CP_8.
                    getCodigo())) {
                this.getTramiteService()
                    .generarProyeccionActualizarCondicionPropiedadYFichaMatriz(
                        this.getTramite(), condicionPropiedadSeleccionada,
                        this.loggedInUser);
            } else {
                Object mensajes[] = this.getTramiteService()
                    .generarProyeccionActualizarCondicionPropiedad(
                        this.getTramite(),
                        this.condicionPropiedadSeleccionada);
                if (this.hayError(mensajes)) {
                    return;
                }
                if (this.proyectarConservacionMB.isBanderaDesenglobeMejora()) {
                    PPredio pp = this.getConservacionService().
                        obtenerPPredioDatosAvaluo(this.proyectarConservacionMB.getTramite().
                            getPredio().getId());
                    for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                        puc.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                    }
                    this.getConservacionService().actualizarPPredio(pp);
                }
            }

            if (this.getTramite().isSegundaDesenglobe()) {
                Long idPredio = this.getTramite().getPredio().getId();
                PPredio pp = this.getConservacionService().obtenerPPredioDatosAvaluo(idPredio);
                // @modified david.cifuentes :: CU-NP-CO-238 :: Las construcciones 
                // se proyectarán como canceladas, y se activarán o inscribiran al momento
                // de ser asociadas:: 24/06/2015
                if (pp != null && pp.getPUnidadConstruccions() != null) {
                    for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                        if (!this.getTramite().isDesenglobeEtapas()) {
                            puc.setCancelaInscribe(EProyeccionCancelaInscribe.CANCELA.getCodigo());
                        } else {
                            pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                        }
                    }
                    pp = this.getConservacionService().actualizarPPredio(pp);
                }
            }
            this.proyectarConservacionMB.cargarPrediosDesenglobe();
            this.prediosResultantes = this.proyectarConservacionMB.getpPrediosDesenglobe();
        }
    }

    /**
     * Invocado desde el boton Adicionar predio
     */
    public void setupNuevoPredio() {
        aceptarDesenglobeNoMantiene(false);
    }

    public void onNumeroEdificioChange() {
        this.pisoSelectItems = new ArrayList<SelectItem>();
        this.unidadSelectItems = new ArrayList<SelectItem>();
        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();
        for (String str : this.getConservacionService().buscarPisosEdificio(
            this.predioNuevo.getNumeroPredial())) {
            this.pisoSelectItems.add(new SelectItem(str, str));
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón 'Modificar predio' o 'Nuevo' en la página entradaProyeccion.xhtml
     */
    /*
     * El método quedó nombrado así por chambonada de juan.agudelo, pero nada tiene que ver con
     * trámites de quinta (aunque en su cuerpo pregunte si se trata de uno) porque para eso se hizo
     * un método aparte
     *
     * @documented pedro.garcia
     */
    public void setupNuevoPredioQuinta() {

        this.predioNuevo = new PPredio();
        if (this.getTramite().isQuinta()) {
            this.predioNuevo.setNumeroPredial(this.getTramite().getNumeroPredial());
            this.predioNuevo.setDepartamento(this.getTramite().getDepartamento());
            this.predioNuevo.setMunicipio(this.getTramite().getMunicipio());
        } else {
            this.predioNuevo.setNumeroPredial(this.getTramite().getPredio().getNumeroPredial());
            this.predioNuevo.setDepartamento(this.getTramite().getPredio().getDepartamento());
            this.predioNuevo.setMunicipio(this.getTramite().getPredio().getMunicipio());
        }
        this.predioNuevo.generarComponentesAPartirDeNumeroPredial();
        this.setupDefaultDataPredioNuevo();
        this.predioNuevo.setZonaUnidadOrganica(this.predioNuevo.getZona() != null ?
            (this.predioNuevo.getZona().equals("00") ? "00" : "01") : null);
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Listener del botón "nuevo" en entradaProyeccion.xhtml. Se hizo un listener aparte porque el
     * que estaba no era exclusivo para trámites de quinta Crea un nuevo predio para una mutación de
     * quinta.
     *
     * @author pedro.garcia
     */
    public void setupNuevoPredioTramiteQuinta() {
        this.limpiarCamposNuevoPredioQuinta();
        this.banderaActivaCancelar = false;
        this.predioNuevo = new PPredio();
        this.okToAddAcceptPredioNuevoQuinta = true;
        

        if (this.ingresarPredioNuevoOmitido) {
            this.getTramite().setOmitidoNuevo(ESiNo.SI.getCodigo());
        } else {
            this.getTramite().setOmitidoNuevo(ESiNo.NO.getCodigo());
        }
        this.condicionPropiedadSelectItems = armarCondicionPropiedadPredioQuintaCombo();

//            if (!this.getTramite().isQuintaOmitido()) {
        //D: esta línea es la que sirve para nuevo, pero no para omitido
        this.predioNuevo.setNumeroPredial(this.getTramite().getNumeroPredial());
//        this.predioNuevo.setNumeroPredial("");
//                this.zona = null;
//            }

        this.predioNuevo.setDepartamento(this.getTramite().getDepartamento());
        this.predioNuevo.setMunicipio(this.getTramite().getMunicipio());

        if (!this.getTramite().isQuintaOmitido()) {
            this.predioNuevo.generarComponentesAPartirDeNumeroPredial();
        }

        this.setupDefaultDataPredioNuevo();

        //D: para que la condición de propiedad no salga predeterminada en 0
        if (this.getTramite().isQuinta()) {
            this.predioNuevo.setCondicionPropiedad(null);
        }

    }

//--------------------------------------------------------------------------------------------------
    private void setupDefaultDataPredioNuevo() {
        if (!this.getTramite().isQuintaOmitido()) {
            this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

            if (this.getTramite().isSegundaDesenglobe() &&
                !this.mantieneNumeroPredial) {
                // Al crear un predio en una mutación de segunda desenglobe para
                // condición cero en la que no se mantiene el número de terreno,
                // el tipo de predio para dicho predio sea vacio, y de ésta
                // manera se deba seleccionar obligatoriamente en la pestaña de
                // gestionar ubicación del predio.
                this.predioNuevo.setTipo(Constantes.CONSTANTE_CADENA_VACIA_DB);
            } else {
                this.predioNuevo.setTipo(Constantes.TIPO_PREDIO_DEFECTO);
            }
        }
        this.predioNuevo.setAreaConstruccion(0.0);
        this.predioNuevo.setAreaTerreno(0.0);
        this.predioNuevo.setEstado(EPredioEstado.ACTIVO.getCodigo());
        this.predioNuevo.setTipoAvaluo(" ");
        this.predioNuevo.setDestino(" ");

        this.predioNuevo.setZonaUnidadOrganica(this.predioNuevo.getZona() != null ?
            (this.predioNuevo.getZona().equals("00") ? "00" : "01") : null);

        this.combosDeptosMunis.setSelectedDepartamentoCod(this.predioNuevo
            .getDepartamento().getCodigo());
        this.combosDeptosMunis.onChangeDepartamentosListener();
        this.combosDeptosMunis.setSelectedMunicipioCod(this.predioNuevo
            .getMunicipio().getCodigo());
        this.combosDeptosMunis.updateZonasItemList();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor en el combo de selección de condición de propiedad. Consulta los
     * valores de los combos que dependen del tipo de condición de propiedad seleccionada. Solo
     * entra aquí cuando es un trámite de quinta (solo en esos se pinta como combo)
     *
     * Aplica las condiciones definidas en el doc de cu, que definen el valor del campo "terreno"
     * dependiendo de la condición de propiedad, en el caso de que es un trámite de quinta nuevo
     * (los nuevos que se crean porque no existe el omitido tienen otras condiciones)
     *
     * @author pedro.garcia
     */
    public void cambioCondicionPropiedadPredioNuevoQuintaListener() {

        this.okToAddAcceptPredioNuevoQuinta = true;
        this.condicion0selected = false;
        this.condicion5selected = false;

        if (this.predioNuevo.getCondicionPropiedad() != null &&
            !this.predioNuevo.getCondicionPropiedad().equals("")) {
            this.condPropWasSelected = true;
        } else {
            this.condPropWasSelected = false;
        }

        this.predioNuevo.
            setCondicionPropiedadNumeroPredial(this.predioNuevo.getCondicionPropiedad());

        if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_0.getCodigo())) {
            this.condicion0selected = true;
            this.predioNuevo.setNumeroUnidad("0000");
        } //D: condición propiedad eq 2
        else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_2.getCodigo())) {
            this.mustEnterDataTerreno = true;
        } else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_5.getCodigo())) {

            this.condicion5selected = true;
            armarComboTerrenoPorManzana();
        } else {
            //D: revisar las condiciones para el número de terreno
            revisarArmadoComboTerreno();
        }

        //D: condiciones para número de edificio o torre y número de piso
        //D: condición propiedad eq 0, 5, 2
        if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_0.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_2.getCodigo())) {

            this.predioNuevo.setNumeroEdificioTorre("00");
            this.predioNuevo.setPiso("00");
            this.setEdificioTorre("00");
            this.mustSelectEdificioTorrePredioNuevo = false;
            this.mustSelectPisoPredioNuevo = false;

        }

        //D: las condiciones que se dan al cambiar de condición de propiedad
        //   solo aplican para los trámites de quinta nuevo
        if (this.getTramite().isQuintaOmitido() || this.getTramite().isQuintaOmitidoNuevo()) {
            return;
        }

        //D: condición propiedad == 3 o == 4. En este momento (a 06-07-2012) no se admiten
        if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_3.getCodigo()) ||
             this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_4.getCodigo())) {
            this.addMensajeError("Condición de propiedad no aceptada en este momento.");
            this.okToAddAcceptPredioNuevoQuinta = false;
            return;
        } //D: condición propiedad eq 0
        else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_0.getCodigo())) {

            //D: al llamar a este método se genera el número de terreno a partir del llamado al
            //   procedimiento almacenado GENERAR_NUMERO_PREDIO
            this.generarNumeroPredialAPartirDeManzana();
            this.condicion0selected = true;
            this.predioNuevo.setNumeroUnidad("0000");
        }

        //D: condiciones para número de edificio o torre y número de piso
        //D: condición propiedad eq 0, 5, 2
        if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_0.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_2.getCodigo())) {

            this.predioNuevo.setNumeroEdificioTorre("00");
            this.predioNuevo.setPiso("00");
            this.setEdificioTorre("00");
            this.mustSelectEdificioTorrePredioNuevo = false;
            this.mustSelectPisoPredioNuevo = false;

        }

        //D: condiciones para número de unidad
        //   D: condición propiedad eq 0, 2, 8
        if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_0.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_2.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_8.getCodigo())) {
            this.predioNuevo.setNumeroUnidad("0000");
        }

        //D: obligar a que el número predial resultante se refresque con la condición de propiedad
        this.predioNuevo.setCondicionPropiedadNumeroPredial(
            this.predioNuevo.getCondicionPropiedad());

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor en el combo de selección de número de terreno. Solo entra aquí
     * cuando es un trámite de quinta (solo en esos se pinta como combo)
     *
     * Aplica las condiciones definidas en el doc de cu, que definen el valor del campo "# de
     * edificio" dependiendo de la condición de propiedad y del número de terreno seleccionado
     *
     * @author pedro.garcia
     */
    public void cambioTerrenoPredioNuevoQuintaListener() {

        this.okToAddAcceptPredioNuevoQuinta = true;

        //D: estas condiciones solo aplican para un predio nuevo de quinta, no omitido
        if (!this.getTramite().isQuintaOmitidoNuevo() || !this.getTramite().isQuintaOmitido()) {
            //D: condición propiedad eq 6, 7, 8, 9
            //OJO: la 7 está pendiente de decidir de forma definitiva
            if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_6.getCodigo()) ||
                this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_7.getCodigo()) ||
                this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_8.getCodigo()) ||
                this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_9.getCodigo())) {

                this.mustSelectEdificioTorrePredioNuevo = true;
                this.noEdificioSelectItems = this.getEdificioTorreOptionsNuevoQuinta();

                if (this.noEdificioSelectItems == null || this.noEdificioSelectItems.isEmpty()) {
                    this.addMensajeError("No existen edificios o torres asociados al terreno. " +
                        "Por favor revise datos");
                    this.predioNuevo.setEdificio("");
                    this.okToAddAcceptPredioNuevoQuinta = false;
                } else {
                    this.noEdificioSelectItems.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
                }
            }

            //D: condición para número de unidad
            if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo())) {

                this.definirNumeroUnidadPredioNuevoQuinta();
            }

            this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

            //D: para obligar a que se seleccione un edificio
            if (this.mustSelectEdificioTorrePredioNuevo) {
                this.edificioTorre = "";
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor en el combo de selección de número de edificio o torre. Solo
     * entra aquí cuando es un trámite de quinta (solo en esos se pinta como combo)
     *
     * Aplica las condiciones definidas en el doc de cu, que definen el valor del campo "# de piso"
     * dependiendo de la condición de propiedad y del número de terreno seleccionado
     *
     * @author pedro.garcia
     */
    public void cambioEdificioTorrePredioNuevoQuintaListener() {

        this.okToAddAcceptPredioNuevoQuinta = true;

        //D: la condición solo aplica para predio nuevo, no omitido
        if (!this.getTramite().isQuintaOmitido()) {
            //D: condición propiedad eq 6, 7, 8, 9
            //OJO: la 7 está pendiente de decidir de forma definitiva
            if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_6.getCodigo()) ||
                this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_7.getCodigo()) ||
                this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_8.getCodigo()) ||
                this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_9.getCodigo())) {

                this.mustSelectPisoPredioNuevo = true;
                this.pisoSelectItems = this.getPisoOptionsNuevoQuinta();

                if (this.pisoSelectItems == null || this.pisoSelectItems.isEmpty()) {
                    this.addMensajeError("No existen pisos asociados al edificio o torre. " +
                        "Por favor revise datos");
                    this.predioNuevo.setPiso("");
                    this.okToAddAcceptPredioNuevoQuinta = false;
                } else {
                    this.pisoSelectItems.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
                }
                //this.predioNuevo.setPiso("");

            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del cambio de valor en el combo o input de número de piso. Solo entra aquí cuando es
     * un trámite de quinta (solo en esos se pinta como combo)
     *
     * Aplica las condiciones definidas en el doc de cu, que definen el valor del campo "# de
     * unidad" dependiendo de la condición de propiedad y del número de terreno seleccionado
     *
     * @author pedro.garcia
     */
    public void cambioPisoPredioNuevoQuintaListener() {

        //D: la condición solo aplica para predio nuevo, no omitido
        if (!this.getTramite().isQuintaOmitido()) {
            if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_6.getCodigo())) {

                this.definirNumeroUnidadPredioNuevoQuinta();
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma los valores para el combo de selección de "terreno" cuando se está creando un predio
     * nuevo en un trámite de quinta.
     *
     * PRE: ya se ha seleccionado el número de manzana PRE: el parámetro no es nulo ni vacío
     *
     * @author pedro.garcia
     * @param condicionesPropiedad lista de condiciones de propiedad
     * @return
     */
    private ArrayList<SelectItem> getTerrenoOptionsPredioNuevoQuinta(
        ArrayList<String> condicionesPropiedad) {

        ArrayList<SelectItem> answer;
        ArrayList<String> respBusqueda;

        answer = null;
        try {
            //D: NO se usa el campo "manzana" del predio nuevo porque haría la búsqueda sobre
            // todos los predios de la tabla. Se usa el atributo 'manzana' de este MB que tiene el
            // número predial hasta la manzana
            respBusqueda = (ArrayList<String>) this.getConservacionService().
                obtenerNumsTerrenoPorCondPropiedadEnManzana(this.manzana, condicionesPropiedad);
            if (respBusqueda != null) {
                answer = new ArrayList<SelectItem>();
                for (String ct : respBusqueda) {
                    answer.add(new SelectItem(ct, ct));
                }
            } else if (null == respBusqueda || respBusqueda.isEmpty()) {
                this.addMensajeError(
                    "No se obtuvieron números de terreno para la condición de propiedad seleccionada");
            }
        } catch (Exception ex) {
            LOGGER.error("Error obteniendo los números de terreno por condición de propiedad:" +
                ex.getMessage(), ex);
        }

        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * llama al procedimiento almacenado que calcula el siguiente número de predio a partir del
     * número de manzana (número predial hasta el segmento de manzana). Se puede enviar un número
     * predial completo (30 caracteres) porque el sp toma el segmento encesario
     *
     * @author pedro.garcia
     */
    public void generarNumeroPredialAPartirDeManzana() {
        String condProp = this.predioNuevo.getCondicionPropiedad();
        String numPredial;

        //N: el método invocado genera el núm. predial tomando el segmento de manzana
        numPredial = this.getConservacionService().buscarUltimoNumeroPredialDisponible(
            this.predioNuevo.getNumeroPredial());
        this.predioNuevo.setNumeroPredial(numPredial);
        this.predioNuevo.setCondicionPropiedadNumeroPredial(condProp);
        this.predioNuevo.generarComponentesAPartirDeNumeroPredial();
        this.terreno = this.predioNuevo.getPredio();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma los valores para el combo de selección de "numero edificio o torre" cuando se está
     * creando un predio nuevo en un trámite de quinta.
     *
     * PRE: ya se ha seleccionado el número de terreno
     *
     * @author pedro.garcia
     * @return
     */
    private List<SelectItem> getEdificioTorreOptionsNuevoQuinta() {

        ArrayList<SelectItem> answer;
        ArrayList<String> respBusqueda;

        answer = null;
        try {
            respBusqueda = (ArrayList<String>) this.getConservacionService().
                obtenerNumsEdificioTorrePorNumeroTerreno(this.predioNuevo.getNumeroPredial());
            if (respBusqueda != null) {
                answer = new ArrayList<SelectItem>();
                for (String ct : respBusqueda) {
                    answer.add(new SelectItem(ct, ct));
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error obteniendo los números de edificio o torre por número de terreno:" +
                ex.getMessage(), ex);
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * arma los valores para el combo de selección de "numero de piso" cuando se está creando un
     * predio nuevo en un trámite de quinta.
     *
     * PRE: ya se ha seleccionado el número de terreno
     *
     * @author pedro.garcia
     * @return
     */
    private List<SelectItem> getPisoOptionsNuevoQuinta() {

        ArrayList<SelectItem> answer;
        ArrayList<String> respBusqueda;

        answer = null;
        try {
            respBusqueda = (ArrayList<String>) this.getConservacionService().
                obtenerNumsPisoPorNumeroEdificioTorre(this.predioNuevo.getNumeroPredial().substring(
                    0, 24));
            if (respBusqueda != null) {
                answer = new ArrayList<SelectItem>();
                for (String ct : respBusqueda) {
                    answer.add(new SelectItem(ct, ct));
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error obteniendo los números de piso por número de edificio o torre:" +
                ex.getMessage(), ex);
        }
        return answer;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * resetea los valores que se usan en la página datosPredioNuevoQuinta.xhtml para el predio
     * nuevo que no se toman del objeto PPredio directamente
     *
     * @author pedro.garcia
     */
    public void resetSelectionsCombosPredioNuevoQuinta() {

        this.zona = "";
        this.sector = "";
        this.comuna = "";
        this.barrio = "";
        this.manzana = "";
        this.edificioTorre = "";
        this.combosDeptosMunis.resetAll();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" en la ventana donde se dan los datos del número predial de un
     * predio nuevo de quinta (datosPredioNuevoQuinta.xhtml)
     *
     * 1. Valida las reglas para el número predial resultante El número predial completo no debe
     * existir en la base de datos ni como activo ni como cancelado. Si el número predial es de
     * condición de propiedad 6, debe tener un número diferente de cero en edificio o torre; Si el
     * número predial es de condición de propiedad 6, debe tener un número diferente de cero en
     * piso; Si el número predial es de condición de propiedad 6, 5, debe tener un número diferente
     * de cero en unidad en PH o mejora
     *
     * 2. se inserta el nuevo PPredio en la base de datos y se adiciona a la lista que muestra los
     * predios resultantes
     *
     * @author pedro.garcia
     *
     * @modified javier.aponte
     *
     * Se agrega validación que verifica que el predio no tenga otra mejora en curso, a la cual no
     * se la hayan aplicado cambios, es decir, verifica que en la tabla p_predio no exista otro
     * predio con el mismo número predial
     */
    public void aceptarDatosPredioNuevoQuinta() {

        boolean existePredio, error = false;
        String mensajeError = "";
        String[] estadosPredio;

        //D: 1.
        if (this.predioNuevo.getCondicionPropiedad() == null){
            mensajeError = "Condición de propiedad: " +
                    "Error de validación: se necesita un valor";
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
        
        if (this.predioNuevo.getCondicionPropiedad() != null 
            && this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_5.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_6.getCodigo())) {

            if (this.predioNuevo.getNumeroUnidad().equals("0000")) {
                mensajeError = "Si la condición de propiedad es 5 o 6 el número de unidad debe " +
                    "ser diferente de 0000";
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

            //valida que no exista en la tabla p_predio otro predio con el mismo
            //número predial que se va a asignar a este predio nuevo
            try {
                PPredio predioTemporal = this.getConservacionService()
                    .getPPredioByNumeroPredial(this.predioNuevo.getNumeroPredial());
                if (predioTemporal != null) {
                    mensajeError =
                        "Ya existe otro predio con este número predial al cual no se le" +
                        " han aplicado cambios, por favor cambie el número de unidad por un valor" +
                        " siguiente";
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
                }
            } catch (Exception e) {
                mensajeError = "Ocurrio un error al hacer una consulta sql";
                LOGGER.debug("Error al consultar en la tabla p_predio");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

            if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_6.getCodigo())) {
                if (this.predioNuevo.getNumeroEdificioTorre().equals("00")) {
                    mensajeError = "Si la condición de propiedad es 6 el número de edificio debe " +
                        "ser diferente de 00";
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }
                if (this.predioNuevo.getPiso().equals("00")) {
                    mensajeError = "Si la condición de propiedad es 6 el número de piso debe ser " +
                        "diferente de 00";
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }
            }
        }

        estadosPredio = new String[]{
            EPredioEstado.ACTIVO.getCodigo(), EPredioEstado.CANCELADO.getCodigo()};

        existePredio = this.getConservacionService().existePredio(
            this.predioNuevo.getNumeroPredial(), estadosPredio);
        if (existePredio) {
            mensajeError = "Ya existe un predio con el número predial resultante";
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        // 2.
        //D: OJO: como es un predio nuevo, debe tener el campo cancelaInscribe en 'I'
        this.predioNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

        this.predioNuevo.setFechaLog(new Date());
        this.predioNuevo.setUsuarioLog(this.loggedInUser.getLogin());
        this.predioNuevo.setTramite(this.getTramite());

        //felipe.cadena::tipo_catastro::#8242
        String tipoCatastro = " ";

        List<Predio> prediosManzana = this.getTramiteService().buscaPrediosPorManzana(manzana);
        if (prediosManzana != null) {
            if (!prediosManzana.isEmpty()) {
                tipoCatastro = prediosManzana.get(0).getTipoCatastro();
            }
        }

        this.predioNuevo.setTipoCatastro(tipoCatastro);
        try {
            this.predioNuevo = this.getConservacionService().guardarActualizarPPredio(
                this.predioNuevo);
        } catch (Exception ex) {
            LOGGER.error("error guardando PPredio nuevo: " + ex.getMessage(), ex);
            mensajeError = "No se pudo guardar el nuevo predio en la base de datos";
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        this.proyectarConservacionMB.setPredioSeleccionado(this.predioNuevo);
        this.prediosResultantes.clear();
        this.prediosResultantes.add(this.predioNuevo);

        if (error) {
            //D: para hacer que no se cierre la ventana de datos del predio nuevo
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError(mensajeError);
            return;

        } else if (this.ingresarPredioNuevoOmitido) {
            this.getTramite().setOmitidoNuevo(ESiNo.SI.getCodigo());
            this.getTramiteService().actualizarTramite(this.getTramite());
        }

        if(this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo())
                && this.getTramite().isQuintaNuevo()) {

            this.quintaNuevoCond5 = true;
            this.setQuintaNuevoCond5(true);

        }else{
            this.quintaNuevoCond5 = false;
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Buscar predio omitido" en la página entradaProyeccion. Hace la búsqueda de
     * predios con el número predial que llega en el trámite y que estén en estado "cancelado"
     *
     * @author pedro.garcia
     */
    public void buscarPredioOmitido() {

        ArrayList<Predio> predios;
        String[] estadosPredios;
        PPredio predioResultante;
        Predio predioTemp;

        estadosPredios = new String[]{EPredioEstado.CANCELADO.getCodigo()};
        predios = (ArrayList<Predio>) this.getConservacionService().
            buscarPrediosPorNumPredialYEstadosNF(
                this.getTramite().getNumeroPredial(), estadosPredios);

        PPredio predioProyectado = this.getConservacionService().
            obtenerPPredioPorNumeroPredialConTramite(this.getTramite().getNumeroPredial());

        if (predioProyectado != null) {
            this.addMensajeInfo("Este predio ya se encuentra en proyección asociado al tramite: " +
                predioProyectado.getTramite().getNumeroRadicacion());
            this.ingresarPredioNuevoOmitido = true;
            return;
        }

        if (predios != null && !predios.isEmpty()) {

            predioTemp = predios.get(0);
            this.getTramite().setPredio(predioTemp);
            this.getTramiteService().actualizarTramite(this.getTramite());

            Object mensajes[] = this.getTramiteService().
                generarProyeccion(this.getTramite().getId());
            if (this.hayError(mensajes)) {
                return;
            }

            predioResultante = new PPredio();

            //N: esto se puede hacer porque las tablas de la BD se cuadraron para que el id de los
            //   Predio coincida con el de los PPredio
            predioResultante.setId(predioTemp.getId());
            predioResultante.setNumeroPredial(predioTemp.getNumeroPredial());
            predioResultante.setDepartamento(predioTemp.getDepartamento());
            predioResultante.setMunicipio(predioTemp.getMunicipio());
            predioResultante.setTipo(predioTemp.getTipo());
            predioResultante.generarComponentesAPartirDeNumeroPredial();
            predioResultante.setEstado(EPredioEstado.ACTIVO.getCodigo());
            predioResultante.setDestino(predioTemp.getDestino());
            predioResultante.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
            predioResultante.setUsuarioLog(this.proyectarConservacionMB.getUsuario().getLogin());
            predioResultante.setAreaConstruccion(predioTemp.getAreaConstruccion());
            predioResultante.setTipoCatastro(predioTemp.getTipoCatastro());
            predioResultante.setAreaTerreno(predioTemp.getAreaTerreno());
            predioResultante.setFechaLog(new Date());
            predioResultante.setTramite(this.getTramite());
            predioResultante.setZonaUnidadOrganica(predioResultante.getZona() != null ?
                (predioResultante.getZona().equals("00") ? "00" : "01") : null);
            predioResultante.setCirculoRegistral(predioTemp.getCirculoRegistral());
            predioResultante.setNumeroRegistro(predioTemp.getNumeroRegistro());

            PPredio pPredioActualizado = this.getConservacionService().actualizarPPredio(
                predioResultante);
            Predio predioOrigen = this.getConservacionService()
                .obtenerPredioConPersonasPorPredioId(predioTemp.getId());
            this.getTramite().setPredio(predioOrigen);

            this.getConservacionService().actualizarPPredio(predioResultante);
            this.getTramite().setPredio(predioTemp);
            this.getTramiteService().actualizarTramite(this.getTramite());

            this.prediosResultantes.clear();
            this.prediosResultantes.add(predioResultante);
            this.usePredioOmitido = true;
            this.predioOmitidoEncontrado = predioTemp;

            //D: para que se pueda diferenciar (en el MB de proyección) si el predio con el que
            //  se trabaja es uno que se encontró como omitido
            this.proyectarConservacionMB.setTramiteQuintaOmitidoPredioExistente(true);

            try {
                if (pPredioActualizado != null) {
                    this.proyectarConservacionMB.setPredioSeleccionado(this
                        .getConservacionService()
                        .obtenerPPredioCompletoById(
                            pPredioActualizado.getId()));
                    this.proyectarConservacionMB.setPredioOrigen(predioOrigen);
                    this.proyectarConservacionMB.init();
                }
            } catch (Exception e) {
                LOGGER.error("Error al consultar el predio base", e);
            }
        } else {
            this.addMensajeInfo(
                "No se encontraron predios cancelados con el número predial del trámite");
            this.ingresarPredioNuevoOmitido = true;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * asigna el número de unidad al predio nuevo de quinta que se está armando dependiendo de la
     * condición de propiedad y de el terreno (cuando ésta es 5) o del piso (si esta es 6) Se debe
     * invocar en el cambio de condición de propiedad, en el de terreno si cond. prop eq 5, y en el
     * de piso si cond. prop. eq 6
     *
     * @author pedro.garcia
     */
    private void definirNumeroUnidadPredioNuevoQuinta() {

        String numeroUnidad = null;

        this.okToAddAcceptPredioNuevoQuinta = true;

        if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_5.getCodigo())) {

            if (this.predioNuevo.getTerreno() != null && !this.predioNuevo.getTerreno().isEmpty()) {
                //OJO: el sp invocado devuelve en 0s el número de edificio y el número de piso, pero
                //     aquí solo se tiene en cuenta el número de unidad
                numeroUnidad = this.getConservacionService().generarNumeroUnidadPorTerreno(
                    this.predioNuevo.getNumeroPredial().substring(0, 22));
                if (numeroUnidad == null) {
                    this.addMensajeError(
                        "No se pudo generar el número de unidad usando el número " +
                        "de terreno " + this.predioNuevo.getTerreno() + ".\nPor favor revise datos");
                    this.okToAddAcceptPredioNuevoQuinta = false;
                    return;
                }
            } else {
                this.addMensajeError(
                    "El número de terreno debe tener un valor para generar el número de unidad");
                this.okToAddAcceptPredioNuevoQuinta = false;
                return;
            }
        } else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_6.getCodigo())) {

            if (this.predioNuevo.getPiso() != null && !this.predioNuevo.getPiso().isEmpty()) {
                numeroUnidad = this.getConservacionService().generarNumeroUnidadPorPiso(
                    this.predioNuevo.getNumeroPredial().substring(0, 26), true);
                if (numeroUnidad == null) {
                    this.addMensajeError(
                        "No se pudo generar el número de unidad usando el número " +
                        "de piso " + this.predioNuevo.getPiso() + ".\nPor favor revise datos");
                    this.okToAddAcceptPredioNuevoQuinta = false;
                    return;
                }
            } else {
                this.addMensajeError(
                    "El número de piso debe tener un valor para generar el número de unidad");
                this.okToAddAcceptPredioNuevoQuinta = false;
                return;
            }

        } else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_7.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_9.getCodigo())) {

            if (this.predioNuevo.getPiso() != null && !this.predioNuevo.getPiso().isEmpty()) {
                numeroUnidad = this.getConservacionService().generarNumeroUnidadPorPiso(
                    this.predioNuevo.getNumeroPredial().substring(0, 26), false);
                if (numeroUnidad == null) {
                    this.addMensajeError(
                        "No se pudo generar el número de unidad usando el número " +
                        "de piso " + this.predioNuevo.getPiso() + ".\nPor favor revise datos");
                    this.okToAddAcceptPredioNuevoQuinta = false;
                    return;
                }
            } else {
                this.addMensajeError(
                    "El número de piso debe tener un valor para generar el número de unidad");
                this.okToAddAcceptPredioNuevoQuinta = false;
                return;
            }

        }

        this.predioNuevo.setNumeroUnidad(numeroUnidad);
        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Llamado al método que realiza los cambios en las tablas la proyección.
     */
    public void validarGuardar() {

        boolean resultadoValidacion = this.validarSeccion();
        proyectarConservacionMB.getValidezSeccion().put(ESeccionEjecucion.PROYECCION_TRAMITE,
            resultadoValidacion);

    }

//TODO :: fredy.wilches :: documentar método :: pedro.garcia
    public boolean validarSeccion() {
        boolean valido = true;
// TODO :: fredy.wilches :: Validaciones de la seccion Entrada Proyeccion
        return valido;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Listener del cambio en el combo de manzanas
     *
     * @author pedro.garcia
     */
    public void onChangeManzanaListener() {

        this.manzanaCodigo = this.predioNuevo.getNumeroPredial().substring(0, 17);
        this.condicion5selected = false;

        if (null != this.predioNuevo.getCondicionPropiedad()) {

            //D: condición propiedad == 3 o == 4. En este momento (a 06-07-2012) no se admiten
            if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_3.getCodigo()) ||
                 this.predioNuevo.getCondicionPropiedad().equals(
                    EPredioCondicionPropiedad.CP_4.getCodigo())) {
                this.addMensajeError("Condición de propiedad no aceptada en este momento.");
                this.okToAddAcceptPredioNuevoQuinta = false;
                return;
            } //D: condición propiedad eq 0
            else if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_0.getCodigo())) {

                //D: al llamar a este método se genera el número de terreno a partir del llamado al
                //   procedimiento almacenado GENERAR_NUMERO_PREDIO
                this.generarNumeroPredialAPartirDeManzana();

            } //D: condición propiedad eq 2
            else if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_2.getCodigo())) {
                this.mustEnterDataTerreno = true;
            } else if (this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_5.getCodigo())) {

                this.condicion5selected = true;
                armarComboTerrenoPorManzana();
            } else {
                //D: revisar las condiciones para el número de terreno
                revisarArmadoComboTerreno();
            }

        }
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Según la condición de propiedad, arma el combo de valores para el campo terreno.
     *
     * @author pedro.garcia
     */
    private void revisarArmadoComboTerreno() {

        ArrayList<String> condProp;

        condProp = new ArrayList<String>();
        this.okToAddAcceptPredioNuevoQuinta = true;

        //D: condición propiedad eq 5
        if (this.predioNuevo.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.
            getCodigo())) {

            condProp.add(EPredioCondicionPropiedad.CP_0.getCodigo());
            condProp.add(EPredioCondicionPropiedad.CP_2.getCodigo());
        } //D: condición propiedad eq 6
        else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_6.getCodigo())) {

            condProp.add(EPredioCondicionPropiedad.CP_8.getCodigo());
            condProp.add(EPredioCondicionPropiedad.CP_9.getCodigo());
        } //D: condición propiedad eq 7, 8, 9
        //OJO: la 7 está pendiente de decidir de forma definitiva
        else if (this.predioNuevo.getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_7.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_8.getCodigo()) ||
            this.predioNuevo.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_9.getCodigo())) {

            condProp.add(this.predioNuevo.getCondicionPropiedad());
        }

        this.terrenoSelectItems = this.getTerrenoOptionsPredioNuevoQuinta(condProp);
        if (this.terrenoSelectItems == null || this.terrenoSelectItems.isEmpty()) {
            this.addMensajeError("No existen terrenos asociados a la manzana. " +
                "Por favor revise datos");
            this.okToAddAcceptPredioNuevoQuinta = false;
        } else {
            this.terrenoSelectItems.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
        }

        //D: para obligar a que se seleccione un terreno
        this.setTerreno(null);

    }

    /**
     * Según la condición de propiedad, arma el combo de valores para el campo terreno.
     *
     * @author leidy.gonzalez
     */
    private void armarComboTerrenoPorManzana() {

        String predioHastaManzana = null;

        predioHastaManzana = this.predioNuevo.getNumeroPredial().substring(0, 17);

        if (predioHastaManzana != null && !predioHastaManzana.isEmpty()) {
            this.terrenoSelectItems = this.obtenerOpcionesTerrenoPredioNuevoQuinta(
                predioHastaManzana);
        }

        if (this.terrenoSelectItems == null || this.terrenoSelectItems.isEmpty()) {
            this.addMensajeError("No existen terrenos asociados a la manzana. " +
                "Por favor revise datos");
            this.okToAddAcceptPredioNuevoQuinta = false;
        } else {
            this.terrenoSelectItems.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
        }

        //D: para obligar a que se seleccione un terreno
        this.setTerreno(null);

    }

    /**
     * arma los valores para el combo de selección de "terreno" cuando se está creando un predio
     * nuevo en un trámite de quinta.
     *
     * PRE: ya se ha seleccionado el número de manzana
     *
     * @author leidy.gonzalez
     * @param predioHastaManzana numero predial hasta la manzana
     * @return
     */
    private ArrayList<SelectItem> obtenerOpcionesTerrenoPredioNuevoQuinta(String predioHastaManzana) {

        ArrayList<SelectItem> answer;
        ArrayList<String> respBusqueda;

        answer = null;
        try {
            //D: NO se usa el campo "manzana" del predio nuevo porque haría la búsqueda sobre
            // todos los predios de la tabla. Se usa el atributo 'manzana' de este MB que tiene el
            // número predial hasta la manzana
            respBusqueda = (ArrayList<String>) this.getConservacionService().
                obtenerNumerosTerrenoPorPredioHastaManzana(predioHastaManzana);

            if (respBusqueda != null) {
                answer = new ArrayList<SelectItem>();
                for (String ct : respBusqueda) {
                    answer.add(new SelectItem(ct, ct));
                }
            } else if (null == respBusqueda || respBusqueda.isEmpty()) {
                this.addMensajeError(
                    "No se obtuvieron números de terreno para el predio seleccionado hasta la manzana");
            }
        } catch (Exception ex) {
            LOGGER.error("Error obteniendo los números de terreno por manzana" +
                ex.getMessage(), ex);
        }

        return answer;

    }

    /**
     * Metodo que revisa los mensajes devueltos por el SP que genera resolucion, y si existen los
     * agrega al contexto de JSF para presentarlos al usuario
     *
     * @author incognito att. Pedro
     * @param mensajes
     * @return
     */
    public boolean hayError(Object mensajes[]) {
        if (mensajes != null) {
            int numeroErrores = 0;
            if (mensajes.length > 0) {
                for (Object o : mensajes) {
                    List l = (List) o;
                    if (l.size() > 0) {
                        String mensaje = "";

                        for (Object obj : l) {
                            Object[] obje = (Object[]) obj;
                            if (!((Object[]) obje)[0].equals("0")) {
                                numeroErrores++;
                                mensaje += (((Object[]) obje)[0] != null) ? ((Object[]) obje)[0].
                                    toString() : "";
                                mensaje += (((Object[]) obje)[1] != null) ? ((Object[]) obje)[1].
                                    toString() : "";
                                mensaje += (((Object[]) obje)[2] != null) ? ((Object[]) obje)[2].
                                    toString() : "";
                                mensaje += (((Object[]) obje)[3] != null) ? ((Object[]) obje)[3].
                                    toString() : "";
                            }
                        }
                        if (numeroErrores > 0 && !mensaje.contains(
                            "No se encontraron predios a proyectar.")) {
                            this.addMensajeError(mensaje);
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
                return false;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que dependiento de la condición de propiedad del predio creado, realiza un llamado a
     * la inicialización de la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void consultarCreacionFichaMatriz() {

        if (this.getTramite().isSegunda() && this.predioNuevo != null) {
            if (this.predioNuevo.isPHCondominio()) {

                // Crear ficha matriz
                this.proyectarConservacionMB.setPredioSeleccionado(this.predioNuevo);
                this.proyectarConservacionMB.inicializarFichaMatriz();
            }
        }
    }

    /**
     * Metodo que elimina la proyeccion si es cancelado el proceso de creacion de un predio.
     */
    public void eliminarProyeccion() {
        this.getTramiteService().eliminarProyeccion(getTramite());
        this.proyeccionValidaBool = true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Arma la lista de valores para el dato 'condición de propiedad' para predios de quinta nuevo y
     * omitido nuevo, ya que son un casso especial en el que no se deben incluir todos los valores
     * del dominio
     *
     * @author pedro.garcia
     * @return
     */
    private ArrayList<SelectItem> armarCondicionPropiedadPredioQuintaCombo() {

        ArrayList<SelectItem> answer;

        answer = new ArrayList<SelectItem>();
        answer.add(0, new SelectItem(null, this.DEFAULT_COMBOS));
        answer.add(new SelectItem(EPredioCondicionPropiedad.CP_0.getCodigo(),
            EPredioCondicionPropiedad.CP_0.getCodigo() + "-" + EPredioCondicionPropiedad.CP_0.
            getValor()));
        answer.add(new SelectItem(EPredioCondicionPropiedad.CP_5.getCodigo(),
            EPredioCondicionPropiedad.CP_5.getCodigo() + "-" + EPredioCondicionPropiedad.CP_5.
            getValor()));

        return answer;
    }

    // ------------------------------------------------------ //
    /**
     * Método que realiza un llamado al método que guarda el ppredio y actualiza los nombres de los
     * predios asociados a la ficha matriz.
     *
     * @author david.cifuentes
     */
    public void guardarActualizarNombresPPredioFichaMatriz() {

        // Guardar la proyección del predio asociado a la ficha matriz.
        this.proyectarConservacionMB.guardarPPredioProyeccion();

//		if (this.prediosResultantes != null) {
//
//			for (PPredio pp : this.prediosResultantes) {
//				pp.setNombre(this.proyectarConservacionMB
//						.getNombrePredioFichaMatriz());
//			}
//			// Actualizar los predios resultantes con los cambios hechos en el
//			// nombre de la ficha matriz.
//			this.getConservacionService().guardarPPredios(
//					this.prediosResultantes);
//		}
    }

    // ------------------------------------------------------ //
    /**
     * Método que hace un llamado al método eliminarProyeccion() cuando se cierra la ventana
     * datosPredio
     *
     * @author david.cifuentes
     */
    public void closeProyeccion(org.primefaces.event.CloseEvent event) {
        // No se ejecuta si es desenglobe de manzanas
        if (this.creaManzanas) {
            return;
        }

        //felipe.cadena::#12756::05-06-2015::Solo se elimina toda la proyeccion cuando no se trata de tramites de desenglobe
        if (!this.proyeccionValidaBool && !this.getTramite().isSegundaDesenglobe()) {
            this.eliminarProyeccion();
        }

    }

    /**
     * Método que hace un llamado al método eliminarProyeccion() cuando se cierra la ventana
     * datosPredio
     *
     * @author david.cifuentes
     */
    public void cancelarProyeccion() {
        // No se ejecuta si es desenglobe de manzanas
        if (this.creaManzanas) {
            return;
        }

        //felipe.cadena::#12756::05-06-2015::Solo se elimina toda la proyeccion cuando no se trata de tramites de desenglobe
        if (!this.proyeccionValidaBool && !this.getTramite().isSegundaDesenglobe()) {
            this.eliminarProyeccion();
        }

    }

    /**
     * Método para asignar el modelo seleccionado a los predios seleccionados
     */
    public void asignarModelo() {
        if (this.modeloSeleccionado == null || this.modeloSeleccionado == 0) {
            this.addMensajeError("Debe seleccionar un modelo");
            return;
        }
        if (this.prediosResultantesSeleccionados == null || this.prediosResultantesSeleccionados.
            isEmpty()) {
            this.addMensajeError("Debe seleccionar al menos un predio");
            return;
        }

        PPredio predioInicialmenteSeleccionado = this.proyectarConservacionMB.
            getPredioSeleccionado();
        PFichaMatrizModelo pfm = this.proyectarConservacionMB.
            getModeloUnidadDeConstruccionSeleccionado();

        PFichaMatrizModelo fmmAsignar = null;

        for (PFichaMatrizModelo fmm : this.proyectarConservacionMB.getFichaMatrizSeleccionada().
            getPFichaMatrizModelos()) {
            if (fmm.getId().equals(this.modeloSeleccionado)) {
                fmmAsignar = fmm;
                break;
            }
        }

        for (PPredio predioSeleccionado : this.prediosResultantesSeleccionados) {
            this.proyectarConservacionMB.setModeloUnidadDeConstruccionSeleccionado(fmmAsignar);
            this.proyectarConservacionMB.setPredioSeleccionado(predioSeleccionado);
            this.proyectarConservacionMB.asociarUnidadesDelModelo();
        }

        this.proyectarConservacionMB.setPredioSeleccionado(predioInicialmenteSeleccionado);
        this.proyectarConservacionMB.setModeloUnidadDeConstruccionSeleccionado(pfm);

    }

    /**
     * Método para realizar el cargue de un modelo de construcción seleccionado y visualizar el
     * detalle de sus unidades de construcción.
     *
     * @author david.cifuentes
     */
    public void cargarModeloDeConstruccion() {
        if (this.modeloSeleccionado != null) {
            PFichaMatrizModelo modeloUnidadDeConstruccionSeleccionado = this
                .getConservacionService().cargarModeloDeConstruccion(
                    this.modeloSeleccionado);
            if (modeloUnidadDeConstruccionSeleccionado != null) {
                this.proyectarConservacionMB
                    .setModeloUnidadDeConstruccionSeleccionado(
                        modeloUnidadDeConstruccionSeleccionado);
                this.proyectarConservacionMB.cargarModeloDeConstruccion();
                this.proyectarConservacionMB.setEditModeloBool(false);
            }
        }
    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Se selecciona por defecto el predio de menor unidad o el elegido como principal, para
     * englobes de solo mejoras.
     *
     * @author felipe.cadena
     */
    public void inicializarPredioAMantener() {

        List<TramitePredioEnglobe> prediosEnglobeTemp = this.proyectarConservacionMB.getTramite().
            getTramitePredioEnglobes();
        if (this.proyectarConservacionMB.isBanderaSoloMejorasEnglobe()) {
            for (TramitePredioEnglobe tpe : prediosEnglobeTemp) {
                if (tpe.getEnglobePrincipal().equals(ESiNo.SI.getCodigo())) {
                    this.predioPrincipalEnglobe = tpe.getPredio();
                    return;
                }
            }
            // si ninguno de los predios fue seleccionado como principal
            if (this.predioPrincipalEnglobe == null) {
                this.predioPrincipalEnglobe =
                    this.proyectarConservacionMB.getTramite().getPredios().get(0);
            }
        } else if (this.proyectarConservacionMB.isBanderaMejorasTerrenoEnglobe()) {
            for (TramitePredioEnglobe tpe : prediosEnglobeTemp) {
                if (tpe.getEnglobePrincipal().equals(ESiNo.SI.getCodigo()) && !tpe.getPredio().
                    isMejora()) {
                    this.predioPrincipalEnglobe = tpe.getPredio();
                    return;
                }
            }
            // si ninguno de los predios fue seleccionado como principal
            if (this.predioPrincipalEnglobe == null) {
                this.predioPrincipalEnglobe = this.prediosEnglobe.get(0);
            }
        }
    }

    /**
     * Retorna un HashMap con los valores de las areas por sector, agrupando los predios del
     * parametro de entrada
     *
     * @param predios
     * @author felipe.cadena
     * @return
     */
    public HashMap<String, Double> agruparAreasPorSector(List<Predio> predios) {

        HashMap<String, Double> areas = new HashMap<String, Double>();

        for (Predio predio : predios) {

            String sectorTemp = predio.getNumeroPredial().substring(7, 9);
            Double area = predio.getAreaTerreno();
            if (areas.containsKey(sectorTemp)) {
                area += areas.get(sectorTemp);
            }
            areas.put(sectorTemp, area);
        }

        return areas;
    }

    /**
     * Retorna un HashMap con los valores de las areas por comuna, agrupando los predios del
     * parametro de entrada
     *
     * @param predios
     * @author felipe.cadena
     * @return
     */
    public HashMap<String, Double> agruparAreasPorComuna(List<Predio> predios, String numeroBase) {

        HashMap<String, Double> areas = new HashMap<String, Double>();

        for (Predio predio : predios) {
            String numeroBasePredio = predio.getNumeroPredial().substring(0, 9);
            String comuna = predio.getNumeroPredial().substring(9, 11);
            Double area = predio.getAreaTerreno();
            if (areas.containsKey(comuna)) {
                area += areas.get(comuna);
            }
            if (numeroBase.equals(numeroBasePredio)) {
                areas.put(comuna, area);
            }

        }

        return areas;
    }

    /**
     * Retorna un HashMap con los valores de las areas por barrio, agrupando los predios del
     * parametro de entrada
     *
     * @param predios
     * @author felipe.cadena
     * @return
     */
    public HashMap<String, Double> agruparAreasPorBarrio(List<Predio> predios, String numeroBase) {

        HashMap<String, Double> areas = new HashMap<String, Double>();

        for (Predio predio : predios) {

            String numeroBasePredio = predio.getNumeroPredial().substring(0, 11);
            String barrioTemp = predio.getNumeroPredial().substring(11, 13);
            Double area = predio.getAreaTerreno();
            if (areas.containsKey(barrioTemp)) {
                area += areas.get(barrioTemp);
            }

            if (numeroBase.equals(numeroBasePredio)) {
                areas.put(barrioTemp, area);
            }

        }

        return areas;
    }

    /**
     * Retorna un HashMap con los valores de las areas por manzana, agrupando los predios del
     * parametro de entrada
     *
     * @param predios
     * @author felipe.cadena
     * @return
     */
    public HashMap<String, Double> agruparAreasPorManzana(List<Predio> predios, String numeroBase) {

        HashMap<String, Double> areas = new HashMap<String, Double>();

        for (Predio predio : predios) {

            String numeroBasePredio = predio.getNumeroPredial().substring(0, 13);
            String manzanaTemp = predio.getNumeroPredial().substring(13, 17);
            Double area = predio.getAreaTerreno();
            if (areas.containsKey(manzanaTemp)) {
                area += areas.get(manzanaTemp);
            }

            if (numeroBase.equals(numeroBasePredio)) {
                areas.put(manzanaTemp, area);
            }

        }

        return areas;
    }

    /**
     * Retorna el maximo item por area de los enviados como parametro
     *
     * @author felipe.cadena
     * @param items
     * @return - String perteneciaente al item de mayor area (sector, comuna, barrio, manzana)
     */
    public String obtenerMaxItem(HashMap<String, Double> items) {

        Set<String> keys = items.keySet();
        Double maxArea = 0.0;
        String maxItem = "";

        for (String string : keys) {
            Double aux = items.get(string);
            if (aux > maxArea) {
                maxArea = aux;
                maxItem = string;
            }
        }

        return maxItem;
    }

    /**
     * Genera las manzanas para el desenglobe de manzanas
     *
     * @author felipe.cadena
     *
     */
    public void generarManzanas() {

        String numeroManzana;
        Integer nManzanas;
        String numeroBarrio;
        String strNumeroManzana;
        int intNumeroManzana;

        if (this.manzanasDesenglobe != null) {
            nManzanas = this.manzanasDesenglobe.size();
        } else {
            nManzanas = 0;
            this.manzanasDesenglobe = new LinkedList<PManzanaVereda>();
        }

        if (this.manzanasAGenerar == 0) {
            this.addMensajeError("La cantidad de manzanas debe ser diferente de cero");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        if (nManzanas + this.manzanasAGenerar > this.getTramite().getManzanasNuevas()) {
            this.
                addMensajeError("El total de manzanas es mayor a la cantidad de manzanas a generar");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        numeroBarrio = this.getTramite().getPredio().getNumeroPredial().substring(0, 13);
        numeroManzana = this.getConservacionService().generarNumeroManzana(numeroBarrio);

        intNumeroManzana = Integer.valueOf(numeroManzana.substring(13, 17));

        Barrio b = new Barrio();
        b.setCodigo(numeroBarrio);
        for (int i = 0; i < this.manzanasAGenerar; i++) {

            strNumeroManzana = String.valueOf(intNumeroManzana);

            while (strNumeroManzana.length() < 4) {
                strNumeroManzana = "0" + strNumeroManzana;
            }
            PManzanaVereda pmv = new PManzanaVereda();

            pmv.setBarrio(b);
            pmv.setCodigo(numeroBarrio + strNumeroManzana);
            pmv.setSelected(false);
            pmv.setTramite(this.getTramite());
            pmv.setUsuarioLog(this.getTramite().getFuncionarioEjecutor());
            pmv.setFechaLog(new Date());
            pmv.setNombre("Manzana " + strNumeroManzana);
            pmv.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

            //pmv = this.getConservacionService().guardarActualizarPManzanaVereda(pmv);
            this.manzanasDesenglobe.add(pmv);

            intNumeroManzana++;

        }

        this.getConservacionService().guardarActualizarMultiplePManzanaVereda(
            this.manzanasDesenglobe);
        this.getTramiteService().actualizarTramite(this.getTramite());
    }

    /**
     * Validar si se puede eliminar una lista de predios sin que existan predios predios mas nuevos
     * persistentes
     *
     * @author felipe.cadena
     * @return
     */
    private boolean validarEliminacionPredios(List<PPredio> predios) {

        boolean eliminaOk = true;

        for (PPredio ppEliminar : predios) {

            if (ppEliminar.getCondicionPropiedad().
                equals(EPredioCondicionPropiedad.CP_0.getCodigo()) || ppEliminar.
                isEsPredioFichaMatriz()) {
                for (PPredio ppActual : this.prediosResultantes) {
                    int codigoBorrar = Integer.valueOf(ppEliminar.getPredio());
                    int codigoExiste = Integer.valueOf(ppActual.getPredio());
                    if (codigoBorrar < codigoExiste) {
                        eliminaOk = false;
                        break;
                    }
                }
            } else {
                eliminaOk = this.validarEliminarUnidad(ppEliminar);
            }
        }
        return eliminaOk;
    }

    /**
     * Carga las posibles condiciones de propiedad de los predios para un desenglobe de manzanas.
     *
     * @author felipe.cadena
     */
    public void armarCondicionesPropiedadDesenglobeManzana() {

        Parametro cpParam = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.CP_DESENGLOBE_MANZANAS.toString());
        this.condicionesPropiedadDesenglobeManzana = new LinkedList<SelectItem>();
        String[] cps = null;
        if (cpParam.getValorCaracter() != null) {
            cps = cpParam.getValorCaracter().split(",");
        }

        if (this.generalMB.getPredioCondicionPropiedadCV() != null && cps != null) {
            for (SelectItem si : this.generalMB.getPredioCondicionPropiedadCV()) {
                for (int i = 0; i < cps.length; i++) {
                    String cp = cps[i];
                    if (si.getValue().equals(cp)) {
                        this.condicionesPropiedadDesenglobeManzana.add(si);
                    }
                }
            }
        }
    }

    /**
     * Elimina las manzanas para el desenglobe de manzanas
     *
     * @author felipe.cadena
     *
     */
    public void retirarManzanas() {

        List<PManzanaVereda> pmvs = new LinkedList<PManzanaVereda>();
        boolean eliminaOk = true;

        for (PManzanaVereda pmv : this.manzanasDesenglobe) {
            if (pmv.isSelected()) {
                if (!this.obtenerPrediosManzana(pmv.getManzanaCodigo()).isEmpty()) {
                    this.addMensajeError("La manzana " + pmv.getManzanaCodigo() +
                        " tiene predios asociados, no puede ser eliminada.");
                } else {
                    pmvs.add(pmv);
                }
            }
        }

        for (PManzanaVereda pmv : pmvs) {

            this.manzanasDesenglobe.remove(pmv);
        }

        // se valida que solo se eliminen las ultimas manzanas
        for (PManzanaVereda pmv : pmvs) {
            for (PManzanaVereda pmvDes : this.manzanasDesenglobe) {
                int codigoBorrar = Integer.valueOf(pmv.getManzanaCodigo());
                int codigoExiste = Integer.valueOf(pmvDes.getManzanaCodigo());
                if (codigoBorrar < codigoExiste) {
                    eliminaOk = false;
                    break;

                }
            }
        }

        if (eliminaOk) {
            // se eliminan las manzanas
            for (PManzanaVereda pmv : pmvs) {
                this.getConservacionService().eliminarPManzanaVereda(pmv);
            }
            this.retirarManzana = false;
        } else {
            //Se restauran las manzanas a la lista 
            for (PManzanaVereda pmv : pmvs) {
                this.manzanasDesenglobe.add(pmv);
            }
            this.addMensajeError("Solo se pueden eliminar las ultimas manzanas generadas");
        }
    }

    /**
     * Metodo que se ejecuta cuando se cambia la decision de crear manzanas aca se borra toda la
     * informacion que se ha ingesado acerca del tramite y se restaura la proyeccion.
     *
     *
     * @author felipe.cadena
     */
    public void cambiarCrearManzanas() {

        LOGGER.debug("Iniciando Cambio decision de crear manzanas . . . . .");

        for (PManzanaVereda pmv : this.manzanasDesenglobe) {
            this.getConservacionService().eliminarPManzanaVereda(pmv);
        }

        //Se elimina la proyeccion del tramite para crear una nueva desde cero
        this.getTramiteService().eliminarProyeccion(this.getTramite());

        this.getTramite().setManzanasNuevas(0);
        if (this.creaManzanas) {
            this.getTramite().setMantieneNumeroPredial(ESiNo.NO.getCodigo());
            //Se actualiza la informacion del tramite
            this.getTramiteService().guardarActualizarTramite2(this.getTramite());
            //Se genera de nuevo la proyeccion
            this.getTramiteService().generarProyeccion(this.getTramite().getId());
        } else {
            this.getTramite().setMantieneNumeroPredial(ESiNo.SI.getCodigo());
        }
        //Se actualiza la informacion del tramite
        this.getTramiteService().guardarActualizarTramite2(this.getTramite());

        //Se recarga la informacion del tramite
        this.proyectarConservacionMB.init();
        if (this.creaManzanas) {
            this.getTramite().setManzanasNuevas(1);
            this.getTramite().setTipoInscripcion("");
        }
        this.init();
    }

    /**
     * Se ejecuta cuando voy a generar predios
     *
     * @author felipe.cadena
     */
    public void setupGeneracionNuevosPredios() {

        this.condicionPropiedadSeleccionada = EPredioCondicionPropiedad.CP_0.getCodigo();
        this.mostrarMantieneNumeroPredial = false;
        this.mantieneNumeroPredial = false;
        this.verCantidadPredios = true;
        this.cantidadPrediosAGenerar = 0;

        this.prediosNuevosAGenerar = new LinkedList<PPredio>();
        this.predioNuevo = new PPredio();
        this.predioNuevo.setTramite(this.getTramite());
        this.predioNuevo.setDepartamento(this.getTramite().getPredio().getDepartamento());
        this.predioNuevo.setMunicipio(this.getTramite().getPredio().getMunicipio());
        this.predioNuevo.setTipoAvaluo(this.getTramite().getPredio().getTipoAvaluo());
        this.predioNuevo.setSectorCodigo(this.getTramite().getPredio().getSectorCodigo());
        this.predioNuevo.setBarrioCodigo(this.getTramite().getPredio().getBarrioCodigo());

        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

        if (this.getTramite().isSegundaDesenglobe() &&
            !this.mantieneNumeroPredial) {
            // Al crear un predio en una mutación de segunda desenglobe para
            // condición cero en la que no se mantiene el número de terreno,
            // el tipo de predio para dicho predio sea vacio, y de ésta
            // manera se deba seleccionar obligatoriamente en la pestaña de
            // gestionar ubicación del predio.
            this.predioNuevo.setTipo(Constantes.CONSTANTE_CADENA_VACIA_DB);
        } else {
            this.predioNuevo.setTipo(Constantes.TIPO_PREDIO_DEFECTO);
        }

        this.predioNuevo.setAreaConstruccion(0.0);
        this.predioNuevo.setAreaTerreno(0.0);
        this.predioNuevo.setEstado(EPredioEstado.ACTIVO.getCodigo());
        this.predioNuevo.setDestino(" ");
        this.predioNuevo.setTipoCatastro(" ");
        this.predioNuevo.setZonaUnidadOrganica(this.predioNuevo.getZona() != null ?
            (this.predioNuevo.getZona().equals("00") ? "00" : "01") : null);

        this.manzanasDisponibles = new LinkedList<SelectItem>();

        SelectItem si = new SelectItem();

        si.setLabel("Seleccionar...");
        si.setValue(null);
        this.manzanasDisponibles.add(si);

        SelectItem si2 = new SelectItem();
        si2.setLabel(this.nManzanaInicial);
        si2.setValue(this.nManzanaInicial);
        this.manzanasDisponibles.add(si2);

        for (PManzanaVereda pmv : this.manzanasDesenglobe) {
            si = new SelectItem();

            si.setLabel(pmv.getManzanaCodigo());
            si.setValue(pmv.getManzanaCodigo());
            this.manzanasDisponibles.add(si);
        }
    }

    /**
     * Genera los grupos de predios en el desenglobe de manazanas
     *
     * @author felipe.cadena
     */
    public void generarPrediosManzaneo() {

        if (null == this.predioNuevo.getManzanaCodigo() || this.predioNuevo.getManzanaCodigo().
            equals("0000")) {
            this.addMensajeError("Debe seleccionar una manzana");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;

        }

        if (this.cantidadPrediosAGenerar == 0) {
            this.addMensajeError("La cantidad de predios a generar debe ser mayor de cero");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;

        }

        this.prediosNuevosAGenerar = new LinkedList<PPredio>();

        String tipoCatastro = this.getTramite().getPredio().getTipoCatastro();

        String nTerreno = this.getConservacionService().buscarUltimoNumeroPredialDisponible(
            this.predioNuevo.getNumeroPredial().substring(0, 17));
        int intTerreno = Integer.valueOf(nTerreno.substring(17, 21));

        if (!this.condicionPropiedadSeleccionada.equals(EPredioCondicionPropiedad.CP_0.getCodigo())) {
            // Crea ficha matriz
            this.predioNuevo.setTerreno(String.valueOf(intTerreno));

            if (this.mantieneNumeroPredial) {
                this.predioNuevo.setTerreno(this.getTramite().getPredio().getPredio());
                this.getTramite().setMantieneNumeroPredial(ESiNo.SI.getCodigo());
                this.getTramiteService().guardarActualizarTramite2(this.getTramite());
            } else {
                this.predioNuevo.setTerreno(String.valueOf(intTerreno));
            }

            this.predioNuevo.setUsuarioLog(this.proyectarConservacionMB.getUsuario().getLogin());
            this.predioNuevo.setFechaLog(new Date());
            this.predioNuevo.setCondicionPropiedad(this.condicionPropiedadSeleccionada);
            this.predioNuevo.generarNumeroPredialAPartirDeComponentes();
            this.predioNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

            PFichaMatriz fichaMatriz = this.getConservacionService()
                .guardarPPredioYFichaMatriz(this.predioNuevo,
                    this.loggedInUser);

            this.predioNuevo = fichaMatriz.getPPredio();
            if (this.predioNuevo.getPFichaMatrizs() == null) {
                this.predioNuevo
                    .setPFichaMatrizs(new ArrayList<PFichaMatriz>());
            }
            this.predioNuevo.getPFichaMatrizs().add(fichaMatriz);

            this.prediosNuevosAGenerar.add(this.predioNuevo);

        } else {

            for (int i = 0; i < this.cantidadPrediosAGenerar; i++) {

                PPredio pp = new PPredio();
                pp.setTramite(this.getTramite());
                pp.setDepartamento(this.getTramite().getPredio().getDepartamento());
                pp.setMunicipio(this.getTramite().getPredio().getMunicipio());
                pp.setTipoAvaluo(this.getTramite().getPredio().getTipoAvaluo());
                pp.setSectorCodigo(this.getTramite().getPredio().getSectorCodigo());
                pp.setBarrioCodigo(this.getTramite().getPredio().getBarrioCodigo());
                pp.setTipo(this.predioNuevo.getTipo());
                pp.setManzanaCodigo(this.predioNuevo.getManzanaCodigo());
                pp.setCondicionPropiedad(this.condicionPropiedadSeleccionada);
                pp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());

                pp.setAreaConstruccion(0.0);
                pp.setAreaTerreno(0.0);
                pp.setEstado(EPredioEstado.ACTIVO.getCodigo());
                pp.setDestino(" ");
                pp.setTipoCatastro(tipoCatastro);
                pp.setZonaUnidadOrganica(this.predioNuevo.getZona() != null ? (this.predioNuevo.
                    getZona().equals("00") ? "00" : "01") : null);
                pp.generarNumeroPredialAPartirDeComponentes();

                if (i == 0 && this.mantieneNumeroPredial) {
                    pp.setTerreno(this.getTramite().getPredio().getPredio());
                    try {
                        pp = this.getConservacionService().obtenerPPredioCompletoById(this.
                            getTramite().getPredio().getId());
                    } catch (Exception ex) {
                        LOGGER.error(
                            "Error al consultar el predio base cuando se mantiene el numero predial",
                            ex);
                    }
                    pp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.getCodigo());
                    pp.setEstado(EPredioEstado.ACTIVO.getCodigo());
                    this.getTramite().setMantieneNumeroPredial(ESiNo.SI.getCodigo());
                    this.getTramiteService().guardarActualizarTramite2(this.getTramite());
                } else {
                    pp.setTerreno(String.valueOf(intTerreno));
                    if (this.condicionPropiedadSeleccionada.equals(EPredioCondicionPropiedad.CP_0.
                        getCodigo())) {
                        intTerreno++;
                    }
                }

                pp.setUsuarioLog(this.proyectarConservacionMB.getUsuario().getLogin());
                pp.setFechaLog(new Date());

                PPredio ppp = this.getConservacionService().guardarActualizarPPredio(pp);
                pp.setId(ppp.getId());
                this.prediosNuevosAGenerar.add(pp);
            }

        }

        this.prediosResultantes.addAll(this.prediosNuevosAGenerar);

        this.proyeccionValidaBool = true;

    }

    /**
     * Genera el numero predial del predio seleccionado
     *
     * @author felipe.cadena
     */
    public void updateNumeroPredial() {

        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

        if (this.mantieneNumeroPredial) {
            this.predioNuevo.setTerreno(this.getTramite().getPredio().getTerreno());
        } else {
            String nTerreno = this.getConservacionService().buscarUltimoNumeroPredialDisponible(
                this.predioNuevo.getNumeroPredial().substring(0, 17));
            int intTerreno = Integer.valueOf(nTerreno.substring(17, 21));
            this.predioNuevo.setTerreno(String.valueOf(intTerreno));
        }

        this.predioNuevo.generarNumeroPredialAPartirDeComponentes();

        if (this.getTramite().getMantieneNumeroPredial().equals(ESiNo.NO.getCodigo()) &&
            this.predioNuevo.getManzanaCodigo().equals(this.nManzanaInicial)) {
            this.mostrarMantieneNumeroPredial = true;
        } else {
            this.mostrarMantieneNumeroPredial = false;
        }
    }

    /**
     * Se actualiza el estado del campo de texto
     *
     * @author felipe.cadena
     */
    public void updateVerCantidadPredios() {
        this.verCantidadPredios = this.condicionPropiedadSeleccionada.equals(
            EPredioCondicionPropiedad.CP_0.getCodigo());
    }

    /**
     * Retorna los predios de una ficha matriz basado en el numeo predial enviado
     *
     *
     * @param numeroPredial
     * @return
     */
    private List<PPredio> obtenerPrediosFM(String numeroPredial) {

        List<PPredio> resultado = new LinkedList<PPredio>();

        for (PPredio pPredio : this.prediosResultantes) {

            String nPredial = pPredio.getNumeroPredial().substring(0, 22);

            if (nPredial.equals(numeroPredial)) {
                resultado.add(pPredio);
            }

        }
        return resultado;

    }

    /**
     * Determina si una unidad se puede eliminar o no, se deben retirar temporal mente los predios
     * de la lista de predios generados
     *
     * @param unidadesPHC
     * @return
     */
    private boolean validarEliminarUnidad(PPredio unidadesPHC) {

        boolean eliminaOk = true;

        if (unidadesPHC == null) {
            return true;
        }
        List<PPredio> unidadesPHCActuales = this.obtenerPrediosFM(unidadesPHC.getNumeroPredial().
            substring(0, 22));

        for (PPredio pPredio : unidadesPHCActuales) {

            int codigoEliminar = Integer.valueOf(unidadesPHC.getNumeroPredial().substring(22, 30));
            int codigoActual = Integer.valueOf(pPredio.getNumeroPredial().substring(22, 30));

            if (codigoEliminar < codigoActual) {
                return false;
            }

        }

        return eliminaOk;

    }

    /**
     * Validar si se puede eliminar una lista de predios sin que existan predios predios mas nuevos
     * persistentes
     *
     * @author felipe.cadena
     * @return
     */
    /* private boolean validarEliminacionPredios(List<PPredio> predios){
     *
     * boolean eliminaOk = true;
     *
     * for (PPredio ppEliminar : predios) {
     *
     * if (ppEliminar.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0.getCodigo()) ||
     * ppEliminar.isEsPredioFichaMatriz()) { for (PPredio ppActual : this.prediosResultantes) { int
     * codigoBorrar = Integer.valueOf(ppEliminar.getPredio()); int codigoExiste =
     * Integer.valueOf(ppActual.getPredio()); if (codigoBorrar < codigoExiste) { eliminaOk = false;
     * break; } } } else { eliminaOk = this.validarEliminarUnidad(ppEliminar); } } return eliminaOk;
     * } */
    /**
     * Retorna los predio asociados a una manzana
     *
     * @author felipe.cadena
     * @param nManzana
     * @return
     */
    public List<PPredio> obtenerPrediosManzana(String nManzana) {

        List<PPredio> resultado = new ArrayList<PPredio>();

        for (PPredio pPredio : this.prediosResultantes) {
            if (pPredio.getManzanaCodigo() != null && pPredio.getManzanaCodigo().equals(nManzana)) {
                resultado.add(pPredio);
            }
        }
        return resultado;
    }

    /**
     * Determina si todas las manzanas generadas tienen por lo menos un predio creado
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validarPrediosManzanas() {

        if (this.manzanasDesenglobe == null || this.manzanasDesenglobe.isEmpty()) {
            this.addMensajeError("Se deben generar  las manzanas");
            return false;
        }

        for (PManzanaVereda pmv : this.manzanasDesenglobe) {

            int nPredios = this.obtenerPrediosManzana(pmv.getManzanaCodigo()).size();
            if (nPredios == 0) {
                this.addMensajeError("Existe la manzana " + pmv.getManzanaCodigo() +
                    "  que no tiene predios asociados");
                return false;
            }
        }
        return true;
    }

    /**
     * Carga las posibles condiciones de propiedad de los predios para un desenglobe de manzanas.
     *
     * @author felipe.cadena
     */
    /* public void armarCondicionesPropiedadDesenglobeManzana() {
     *
     * Parametro cpParam =
     * this.getGeneralesService().getCacheParametroPorNombre(EParametro.CP_DESENGLOBE_MANZANAS.toString());
     * this.condicionesPropiedadDesenglobeManzana = new LinkedList<SelectItem>(); String[] cps =
     * cpParam.getValorCaracter().split(",");
     *
     * for (SelectItem si : this.generalMB.getPredioCondicionPropiedadCV()) {
     *
     * for (int i = 0; i < cps.length; i++) { String cp = cps[i]; if (cp.equals(si.getValue())) {
     * this.condicionesPropiedadDesenglobeManzana.add(si); } }
     *
     * }
     * } */
    /**
     * Realiza las actualizaciones cuando se selecciona o no el desenglobe de manzanas
     *
     * @author felipe.cadena
     *
     */
    public void listenerEnglobeManzanas() {

        this.cambiarCrearManzanas();
        if (!this.creaManzanas) {
            this.envioDesenglobeManzanas = false;
        }
        this.prediosResultantes = new ArrayList<PPredio>();
        this.proyectarConservacionMB.setPredioSeleccionado(null);
    }

    /**
     * Se ejecuta cuando no se confirma el cambio de seleccion en el cambio de creacion de manzanas
     *
     * @author felipe.cadena
     *
     */
    public void listenerNoConfirmaCreaManzana() {

        this.creaManzanas = !this.creaManzanas;

    }

    /**
     * Listener para cuando se cambia la cantidad de manzanas a crear
     *
     * @author felipe.cadena
     */
    public void listenerCantidadManzanas() {

        int manzanasCreadas;
        int manzanasOriginales;

        manzanasOriginales = this.getTramiteService().buscarTramitePorId(this.getTramite().getId()).
            getManzanasNuevas();

        if (this.getTramite().getManzanasNuevas() < 1) {
            this.addMensajeError("La cantidad de manzanas no puede ser cero.");
            this.getTramite().setManzanasNuevas(manzanasOriginales);
            return;
        }
        if (this.manzanasDesenglobe == null) {
            manzanasCreadas = 0;
        } else {
            manzanasCreadas = this.manzanasDesenglobe.size();
        }

        if (manzanasCreadas > this.getTramite().getManzanasNuevas()) {
            this.addMensajeError(
                "La cantidad de manzanas generadas es mayor a la cantidad registrada.");
            this.getTramite().setManzanasNuevas(manzanasOriginales);
            return;
        }

        this.getTramiteService().actualizarTramite(this.getTramite());

    }

    /**
     * Se valida si los predios selecionados para eliminar cumplen las condiciones necesarias.
     *
     * @author felipe.cadena
     * @return
     */
    private boolean validarRetirarPrediosEtapas(List<PPredio> prediosSelecionados,
        List<PPredio> prediosProyectados) {

        //crear modelos de comparacion
        TreeMap<String, TreeMap<String, List<PPredio>>> modeloEliminar;
        TreeMap<String, TreeMap<String, List<PPredio>>> modeloBase;

        for (PPredio pp : prediosSelecionados) {
            if (pp.isEsPredioFichaMatriz()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_003.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_003.getMensajeTecnico());
                return false;
            }
        }

        // En segundas etapas para condominios se pueden eliminar predios
        // intermedios, la única reestricción es que quede al menos un predio
        // (original o nuevo)
        if (this.getTramite().isTipoInscripcionCondominio()) {
            if (prediosProyectados.size() - 1 > prediosSelecionados.size()) {
                return true;
            } else {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_016.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_016.getMensajeTecnico());
                return false;
            }
        } else {
            modeloEliminar = this.crearModelo(prediosSelecionados);
            modeloBase = this.crearModelo(prediosProyectados);
            return this.validarEliminar(modeloBase, modeloEliminar);
        }
    }

    /**
     * Organiza una lista de predios unidades PH en una estructura de torres, pisos, y unidades
     * ordenadas y con jerarquia
     *
     * @author felipe.cadena
     * @return
     */
    private TreeMap<String, TreeMap<String, List<PPredio>>> crearModelo(List<PPredio> predios) {

        TreeMap<String, TreeMap<String, List<PPredio>>> modelo =
            new TreeMap<String, TreeMap<String, List<PPredio>>>();

        for (PPredio pp : predios) {

            if (modelo.containsKey(pp.getEdificio())) {
                if (modelo.get(pp.getEdificio()).containsKey(pp.getPiso())) {
                    modelo.get(pp.getEdificio()).get(pp.getPiso()).add(pp);
                } else {
                    List<PPredio> prediosPiso = new ArrayList<PPredio>();
                    prediosPiso.add(pp);
                    modelo.get(pp.getEdificio()).put(pp.getPiso(), prediosPiso);
                }
            } else {
                TreeMap<String, List<PPredio>> modeloEdificio = new TreeMap<String, List<PPredio>>();
                List<PPredio> prediosPiso = new ArrayList<PPredio>();
                prediosPiso.add(pp);
                modeloEdificio.put(pp.getPiso(), prediosPiso);
                modelo.put(pp.getEdificio(), modeloEdificio);
            }

        }

        return modelo;
    }

    /**
     * Valida si se pueden eliminar los predios seleccionados por el usuario
     *
     * @author felipe.cadena
     * @param modeloBase
     * @param modeloEliminar
     * @return
     */
    private boolean validarEliminar(TreeMap<String, TreeMap<String, List<PPredio>>> modeloBase,
        TreeMap<String, TreeMap<String, List<PPredio>>> modeloEliminar) {

        List<List<TreeMap<String, List<PPredio>>>> edificiosValidar =
            new ArrayList<List<TreeMap<String, List<PPredio>>>>();

        for (String edificio : modeloEliminar.keySet()) {

            for (String edificioBase : modeloBase.keySet()) {
                if (edificioBase.equals(edificio)) {
                    if (this.numeroUnidadesPorEdificio(modeloEliminar.get(edificio)) !=
                        this.numeroUnidadesPorEdificio(modeloBase.get(edificioBase))) {

                        List<TreeMap<String, List<PPredio>>> duplaEd =
                            new ArrayList<TreeMap<String, List<PPredio>>>();
                        duplaEd.add(modeloEliminar.get(edificio));
                        duplaEd.add(modeloBase.get(edificioBase));
                        edificiosValidar.add(duplaEd);
                    }
                }
            }

        }

        if (!edificiosValidar.isEmpty()) {
            for (List<TreeMap<String, List<PPredio>>> duplaEdificio : edificiosValidar) {
                if (!this.validarEdificio(duplaEdificio.get(0), duplaEdificio.get(1))) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }

    }

    /**
     * Valida si los predios de un edificio se pueden eliminar, el edificio base y el edificio a
     * eliminar
     *
     * @author felipe.cadena
     *
     * @param edificioEliminar
     * @param edificioBase
     * @return
     */
    private boolean validarEdificio(TreeMap<String, List<PPredio>> edificioEliminar,
        TreeMap<String, List<PPredio>> edificioBase) {

        TreeMap<String, List<PPredio>> pisosEliminar = new TreeMap<String, List<PPredio>>();

        for (String piso : edificioEliminar.keySet()) {
            for (String pisoBase : edificioBase.keySet()) {

                if (pisoBase.equals(piso)) {
                    if (edificioBase.get(pisoBase).size() == edificioEliminar.get(piso).size()) {
                        pisosEliminar.put(piso, edificioEliminar.get(piso));
                    } else {
                        if (!validarPiso(edificioBase.get(pisoBase), edificioEliminar.get(piso))) {
                            return false;
                        }
                    }
                }
            }
        }

        while (!pisosEliminar.isEmpty()) {
            if (!pisosEliminar.pollLastEntry().getKey().
                equals(edificioBase.pollLastEntry().getKey())) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_002.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_002.getMensajeTecnico());
                return false;
            }
        }

        return true;
    }

    /**
     * Validas si los predios seleccionados para eliminar de un piso estan en orden
     *
     *
     * @author felipe.cadena
     * @param pisoBase
     * @param pisoEliminar
     * @return
     */
    private boolean validarPiso(List<PPredio> pisoBase, List<PPredio> pisoEliminar) {

        while (!pisoEliminar.isEmpty()) {
            PPredio maxUnidad;
            maxUnidad = pisoEliminar.get(0);
            if (pisoEliminar.size() > 1) {
                for (PPredio pPredio : pisoEliminar) {
                    if (Integer.valueOf(maxUnidad.getUnidad()) < Integer.
                        valueOf(pPredio.getUnidad())) {
                        maxUnidad = pPredio;
                    }
                }
            }

            PPredio maxUnidadBase;
            maxUnidadBase = pisoBase.get(0);
            if (pisoBase.size() > 1) {
                for (PPredio pPredio : pisoBase) {
                    if (Integer.valueOf(maxUnidadBase.getUnidad()) < Integer.valueOf(pPredio.
                        getUnidad())) {
                        maxUnidadBase = pPredio;
                    }
                }
            }

            if (!maxUnidadBase.getUnidad().equals(maxUnidad.getUnidad())) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_001.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_002.getMensajeTecnico());
                return false;
            }

            pisoBase.remove(maxUnidadBase);
            pisoEliminar.remove(maxUnidad);
        }
        return true;
    }

    /**
     * Retorna el numero de unidades que tiene un edificion en una estructura de pisos
     *
     * @author felipe.cadena
     * @param pisos
     * @return
     */
    private int numeroUnidadesPorEdificio(TreeMap<String, List<PPredio>> pisos) {

        int nUnidades = 0;

        for (String key : pisos.keySet()) {
            nUnidades += pisos.get(key).size();
        }

        return nUnidades;
    }

    /**
     * Inicializa los datos requeridos para la asignacion de destino economico de forma masiva,en
     * los tramites de desenglobe por etapas.
     *
     * Se invoca desde el boton id=destinoBtn, de la pagina entradaProyeccion.xhtml
     *
     * @author felipe.cadena
     */
    public void initDestinoMasivo() {

        if (this.prediosResultantesSeleccionados == null || this.prediosResultantesSeleccionados.
            isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.toString());
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //Validaciones
        //leidy.gonzalez :: 42003 :: 23/10/2017:: Para los trÃ¡mites de englobe virtual se realiza 
        //esta validaciÃ³n, ya que para todos los predios, a excepciÃ³n de una ficha matriz nueva,
        //se les puede cambiar la destinaciÃ³n econÃ³mica.
        if (this.getTramite().isEnglobeVirtual()) {

            if (!this.validarPrediosSeleccionadosEnglobeVirtual()) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_063.toString());
                LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_063.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        }

        //javier.aponte :: Para los trámites de tercera masivos se realiza esta validación, ya que para todos
        //los predios, a excepción de la ficha matriz, se les puede cambiar la destinación económica. :: 27/07/2015
        if (this.getTramite().isTerceraMasiva()) {

            if (!this.validarPrediosSeleccionadosTerceraMasiva()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        } else {

            if (this.validarPrediosSeleccionados() == null) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

            if (!this.validarPrediosSeleccionados()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_005.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_005.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        }
        this.destinoEtapas = null;
        this.borrarDatoMasivo = false;
        this.nPrediosSeleccionados = this.prediosResultantesSeleccionados.size();
    }

    /**
     * Inicializa los datos requeridos para la asignacion de destino economico de forma masiva,en
     * los tramites de desenglobe por etapas.
     *
     * Se invoca desde el boton id=tipoPredioBtn, de la pagina entradaProyeccion.xhtml
     *
     * @author felipe.cadena
     */
    public void initTipoPredioMasivo() {

        if (this.prediosResultantesSeleccionados == null || this.prediosResultantesSeleccionados.
            isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.toString());
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //Validaciones
        //leidy.gonzalez :: 42003 :: 23/10/2017:: Para los trÃ¡mites de englobe virtual se realiza 
        //esta validaciÃ³n, ya que para todos los predios, a excepciÃ³n de una ficha matriz nueva,
        //se les puede cambiar la destinaciÃ³n econÃ³mica.
        if (this.getTramite().isEnglobeVirtual()) {

            if (!this.validarPrediosSeleccionadosEnglobeVirtual()) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_064.toString());
                LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_064.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        }
        //javier.aponte :: Para los trámites de tercera masivos se realiza esta validación, ya que para todos
        //los predios, a excepción de la ficha matriz, se les puede cambiar la destinación económica. :: 27/07/2015
        if (this.getTramite().isTerceraMasiva()) {

            if (!this.validarPrediosSeleccionadosTerceraMasiva()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        } else {

            if (this.validarPrediosSeleccionados() == null) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_006.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_006.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
            if (!this.validarPrediosSeleccionados()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_007.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_007.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        }

        this.borrarDatoMasivo = false;
        this.nPrediosSeleccionados = this.prediosResultantesSeleccionados.size();
    }

    /**
     * Inicializa los datos requeridos para la asignacion de destino economico de forma masiva,en
     * los tramites de desenglobe por etapas.
     *
     * Se invoca desde el boton id=tipoPredioBtn, de la pagina entradaProyeccion.xhtml
     *
     * @author felipe.cadena
     */
    public void initMatriculaMasivo() {

        if (this.prediosResultantesSeleccionados == null || this.prediosResultantesSeleccionados.
            isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.toString());
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //Validaciones
        //javier.aponte :: Para los trámites de tercera masivos se realiza esta validación, ya que para todos
        //los predios, a excepción de la ficha matriz, se les puede cambiar la destinación económica. :: 27/07/2015
        if (this.getTramite().isTerceraMasiva()) {

            if (!this.validarPrediosSeleccionadosTerceraMasiva()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_004.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        } else {

            if (this.validarPrediosSeleccionados() == null) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_008.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_008.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
            if (!this.validarPrediosSeleccionados()) {
                this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_009.toString());
                LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_009.getMensajeTecnico());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        }

        this.banderaIncremento = false;
        if (this.prediosResultantesSeleccionados.size() == 1) {
            this.banderaIncremento = true;

        }

        this.borrarDatoMasivo = false;
        this.numeroRegistroInicial = 0;
        this.numeroRegistrofinal = 0;
        this.incrementeoRegistro = 1;
        this.borrarDatoMasivo = false;
        this.nPrediosSeleccionados = this.prediosResultantesSeleccionados.size();
    }

    /**
     * Inicializa los datos requeridos para la asignacion de destino economico de forma masiva,en
     * los tramites de desenglobe por etapas.
     *
     * Se invoca desde el boton id=direccionBtn, de la pagina entradaProyeccion.xhtml
     *
     * @author felipe.cadena
     */
    public void initDireccionMasivo() {

        if (this.prediosResultantesSeleccionados == null || this.prediosResultantesSeleccionados.
            isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.toString());
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_015.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //Validaciones
        if (this.prediosResultantesSeleccionados.size() > 1) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_013.toString());
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_013.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //@modified by leidy.gonzalez:: #42687::01/11/2017
        //Se ajusta que desde la pantalla de entrada de proyeccion cada vez que se modifique la direccion
        //de un predio, se visualice la direccion modificada y no la del predio de ficha matriz
        if (this.prediosResultantesSeleccionados.get(0).isEsPredioFichaMatriz()) {

            this.direccionEtapas = this.getConservacionService().
                obtenerDireccionPrincipalFichaMatrizId(this.prediosResultantesSeleccionados.get(0).
                    getId());

        } else {

            List<PPredioDireccion> direccionesProy = this.getConservacionService().
                obtenerPpredioDirecionesPorPredioId(this.prediosResultantesSeleccionados.get(0).
                    getId());

            if (direccionesProy != null && !direccionesProy.isEmpty()) {
                for (PPredioDireccion pPredioDireccion : direccionesProy) {

                    if (ESiNo.SI.getCodigo().equals(pPredioDireccion.getPrincipal())) {
                        this.direccionEtapas = pPredioDireccion;
                    }
                }

            } else {
                this.direccionEtapas = this.getConservacionService().
                    obtenerDireccionPrincipalFM(this.prediosResultantesSeleccionados.get(0).
                        getNumeroPredial().substring(0, 21));
            }

        }

        if (this.direccionEtapas == null) {
            this.direccionEtapas = new PPredioDireccion();
        }
        if (this.getTramite().isEnglobeVirtual()) {
            this.direccionEtapas.setSelectedPrincipal(true);
        }

        this.borrarDatoMasivo = false;
        this.proyectarConservacionMB.setDireccionNormalizada("");
        this.proyectarConservacionMB.setNormalizadaBool(false);
        this.nPrediosSeleccionados = this.prediosResultantesSeleccionados.size();
    }

    /**
     * Método para asignar el destino economico de forma masiva, en los tramites de desenglobe por
     * etapas y de tercera masivos
     *
     * Se invoca desde el boton id=aceptarDestinoBtn, de la pagina destinoEconomicoEtapas.xhtml
     *
     * @author felipe.cadena
     * @modified by javier.aponte
     */
    //javier.aponte :: Se agrega llamado a método para actualizar los predios en el caso que el trámite
    //sea de tercera masivo :: 27/07/2015
    public void asignarDestinoMasivo() {

        for (PPredio pp : this.prediosResultantesSeleccionados) {
            if (this.borrarDatoMasivo) {
                pp.setDestino(" ");
            } else {
                pp.setDestino(this.destinoEtapas);
            }
        }

        if (this.getTramite().isTerceraMasiva()) {
            this.actualizarPrediosTerceraRectificacionMasivos();
        } else {
            this.actualizarPrediosEtapas();
        }
    }

    /**
     * Método para asignar el destino economico de forma masiva, en los tramites de desenglobe por
     * etapas y tercera masivos
     *
     * Se invoca desde el boton id=aceptarTipoPredioBtn, de la pagina tipoPredioEtapas.xhtml
     *
     * @author felipe.cadena
     * @modified by javier.aponte
     */
    //javier.aponte :: Se agrega llamado a método para actualizar los predios en el caso que el trámite
    //sea de tercera masivo :: 27/07/2015
    public void asignarTipoPredioMasivo() {

        for (PPredio pp : this.prediosResultantesSeleccionados) {
            if (this.borrarDatoMasivo) {
                pp.setTipo(" ");
            } else {
                pp.setTipo(this.tipoPredioEtapas);
            }
        }

        if (this.getTramite().isTerceraMasiva()) {
            this.actualizarPrediosTerceraRectificacionMasivos();
        } else {
            this.actualizarPrediosEtapas();
        }
    }

    /**
     * Método para asignar matricula de forma masiva, en los tramites de desenglobe por etapas y de
     * tercera masivos
     *
     * Se invoca desde el boton id=aceptarMatriculaBtn, de la pagina matriculaEtapas.xhtml
     *
     * @author felipe.cadena
     * @modified by javier.aponte
     */
    //javier.aponte :: Se agrega llamado a método para actualizar los predios en el caso que el trámite
    //sea de tercera masivo :: 27/07/2015
    public void asignarMatriculaMasivo() {

        List<String> registrosStr = new ArrayList<String>();
        if (this.borrarDatoMasivo) {
            for (PPredio pp : this.prediosResultantesSeleccionados) {
                pp.setCirculoRegistral(null);
                pp.setNumeroRegistro(" ");
            }
        } else {

            //Las validaciones no aplican para cuando se borra la justificacion
            if (this.getTramite().isEnglobeVirtual()) {

                if (this.incrementeoRegistro <= 0) {
                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_065.toString());
                    LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_065.getMensajeTecnico());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }

                if (this.numeroRegistroInicial <= 0) {
                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_066.toString());
                    LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_066.getMensajeTecnico());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }

                List<String> registros = new ArrayList<String>();
                int reg = 0;
                String registro = new String();

                for (int i = 0; i < this.nPrediosSeleccionados; i++) {
                    reg = this.numeroRegistroInicial + i * this.incrementeoRegistro;
                }

                registro = String.valueOf(reg);
                registros.add(registro);

                //Se verifica que el registro no exista en otro predio del municipio
                List<Predio> matriculasExistentes = this.getConservacionService().
                    validarExistenciaMatriculasPorMunicipio(registros,
                        this.getTramite().getPredio().getMunicipio().getCodigo());

                if (matriculasExistentes != null && !matriculasExistentes.isEmpty()) {
                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_069.toString());
                    LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_069.getMensajeTecnico());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }

            } else {

                if (this.incrementeoRegistro <= 0 || this.numeroRegistroInicial <= 0) {
                    this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_012.toString());
                    LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_012.getMensajeTecnico());
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    return;
                }
            }

            //generar numeros de regsitro
            int[] registros = new int[this.nPrediosSeleccionados];

            for (int i = 0; i < this.nPrediosSeleccionados; i++) {
                registros[i] = this.numeroRegistroInicial + i * this.incrementeoRegistro;
            }

            //validar numeros de registro
            for (int i : registros) {

                if (this.validarExistenciaRegistro(String.valueOf(i))) {
                    registrosStr.add(String.valueOf(i));
                } else {
                    return;
                }
            }

            //Se agregan los registros a los predios seleccionados
            int k = 0;
            for (PPredio pp : this.prediosResultantesSeleccionados) {
                CirculoRegistral cr = new CirculoRegistral();
                cr.setCodigo(this.circuloRegistralEtapas);
                pp.setCirculoRegistral(cr);
                pp.setNumeroRegistro(registrosStr.get(k));
                k++;
            }
        }
        if (this.getTramite().isTerceraMasiva()) {
            this.actualizarPrediosTerceraRectificacionMasivos();
        } else {
            this.actualizarPrediosEtapas();
        }
    }

    /**
     * Método para asignar el destino economico de forma masiva, en los tramites de desenglobe por
     * etapas y de tercera masivos.
     *
     * Se invoca desde el boton id=aceptarDireccionBtn, de la pagina tipoPredioEtapas.xhtml
     *
     * @author felipe.cadena
     * @modified by javier.aponte
     */
    //javier.aponte :: Se agrega llamado a método para actualizar los predios en el caso que el trámite
    //sea de tercera masivo :: 27/07/2015
    public void asignarDireccionMasivo() {

        for (PPredio pp : this.prediosResultantesSeleccionados) {

            if (this.proyectarConservacionMB.getNormalizadaBool()) {
                this.direccionEtapas.setDireccion(this.proyectarConservacionMB.
                    getDireccionNormalizada());
            }

            PPredioDireccion dirPrincipialInicial = pp.getPredioDireccionPrincipal();

            if (dirPrincipialInicial != null) {
                dirPrincipialInicial.setDireccion(this.direccionEtapas.getDireccion());
                dirPrincipialInicial.setCodigoPostal(this.direccionEtapas.getCodigoPostal());
                dirPrincipialInicial.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA.
                    getCodigo());
            } else {
                this.direccionEtapas.setPPredio(pp);
                this.direccionEtapas.setId(null);
                if (this.getTramite().isEnglobeVirtual()) {
                    this.direccionEtapas.setPrincipal(ESiNo.SI.getCodigo());
                    this.direccionEtapas.setUsuarioLog(this.loggedInUser.getLogin());
                    this.direccionEtapas.setFechaLog(new Date(System.currentTimeMillis()));
                }
                this.direccionEtapas = this.getConservacionService().guardarPPredioDireccion(
                    this.direccionEtapas);
                pp.getPPredioDireccions().add(this.direccionEtapas);

            }

        }
        if (this.getTramite().isTerceraMasiva() || this.getTramite().isRectificacionMatriz()) {
            this.actualizarPrediosTerceraRectificacionMasivos();
        } else {
            this.actualizarPrediosEtapas();
        }
    }

    /**
     * Metodo para validar si existe un Predio con el numero de registro dado
     *
     * @author felipe.cadena
     * @param registro
     * @return
     */
    public boolean validarExistenciaRegistro(String registro) {

        List<PPredio> prediosProyectados;
        List<Predio> prediosEnFirme;

        prediosProyectados = this.getConservacionService().buscarPPredioPorMatricula(
            this.circuloRegistralEtapas, registro);

        if (prediosProyectados != null && !prediosProyectados.isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_010.getMensajeUsuario(registro,
                prediosProyectados.get(0).getNumeroPredial()));
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_010.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return false;
        }

        prediosEnFirme = this.getConservacionService().buscarPredioPorMatricula(
            this.circuloRegistralEtapas, registro);

        if (prediosEnFirme != null && !prediosEnFirme.isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.DESENGLOBE_ETAPAS_011.getMensajeUsuario(registro,
                prediosProyectados.get(0).getNumeroPredial()));
            LOGGER.debug(ECodigoErrorVista.DESENGLOBE_ETAPAS_011.getMensajeTecnico());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return false;
        }

        return true;

    }

    /**
     * Actualiza los predios cuando se modifican los datos del mismos
     *
     * @author felipe.cadena
     *
     */
    private void actualizarPrediosEtapas() {

        this.prediosResultantesSeleccionados = this.getConservacionService().
            guardarPPredios(this.prediosResultantesSeleccionados);
        if (this.getTramite().isEnglobeVirtual()) {
            this.cargarPrediosEnglobeVirtual();
        } else {
            this.proyectarConservacionMB.cargarPrediosDesenglobe();
        }

        this.prediosResultantes = this.proyectarConservacionMB.getpPrediosDesenglobe();
        this.prediosResultantesSeleccionados = new ArrayList<PPredio>();
    }

    /**
     * Actualiza los predios cuando se modifican los datos del mismos para los trámites de tercera
     * masivos
     *
     * @author javier.aponte
     *
     */
    private void actualizarPrediosTerceraRectificacionMasivos() {

        this.prediosResultantesSeleccionados = this.getConservacionService().
            guardarPPredios(this.prediosResultantesSeleccionados);
        this.proyectarConservacionMB.cargarPrediosTerceraRectificacionMasiva();
        this.prediosResultantes = this.proyectarConservacionMB.
            getpPrediosTerceraRectificacionMasiva();
        this.prediosResultantesSeleccionados = new ArrayList<PPredio>();
    }

    /**
     * Valida si los predios seleccionados se pueden modificar
     *
     * @author felipe.cadena
     * @return
     */
    private Boolean validarPrediosSeleccionados() {

        for (PPredio pp : this.prediosResultantesSeleccionados) {

            //@modified by leidy.gonzalez:: #41206:: Se valida que para los tramites de englobe virtual
            //cuando se crea una nueva ficha matriz, se permita modificar la matricula inmobiliaria.
            if (this.getTramite().isEnglobeVirtual()) {

                if (pp.isEsPredioFichaMatriz()) {
                    if (ESiNo.SI.getCodigo().
                        equals(this.getTramite().obtenerInfoCampoAdicional(
                            EInfoAdicionalCampo.NUEVA_FICHA_MATRIZ_EV))) {
                        return true;
                    } else {
                        return null;
                    }
                }
            }

            if (pp.isEsPredioFichaMatriz()) {
                return null;
            }
            //@modified by leidy.gonzalez:: #42673:: Se valida que para los predios que ya  
            //existÃ­an en las fichas matrices asociados al englobe virtual,se permita modificar el destino economico
            if (pp.isPredioOriginalTramite() && !this.getTramite().isEnglobeVirtual()) {
                return false;
            }

        }
        return true;
    }

    /**
     * Valida si los predios seleccionados de un trámite de tercera masiva se pueden modificar
     *
     * @author javier.aponte
     * @return
     */
    private boolean validarPrediosSeleccionadosTerceraMasiva() {

        for (PPredio pp : this.prediosResultantesSeleccionados) {

            if (pp.isEsPredioFichaMatriz()) {
                return false;
            }

        }
        return true;
    }

    /**
     * action del botón "generar" en la ventana en la que se adiciona una dirección para el predio.
     * Metodo que normaliza el valor de una dirección.
     *
     * @author david.cifuentes
     */
    public void normalizarDireccion() {
        this.proyectarConservacionMB.setDireccionNormalizada(this.getConservacionService()
            .normalizarDireccion(this.direccionEtapas.getDireccion()));

        if (this.getTramite().isDesenglobeEtapas()) {
            if (this.proyectarConservacionMB.getDireccionNormalizada() == null ||
                this.proyectarConservacionMB.getDireccionNormalizada().isEmpty()) {
                this.proyectarConservacionMB.setBanderaAceptarDireccionNormalizada(true);

            } else {
                this.proyectarConservacionMB.setBanderaAceptarDireccionNormalizada(false);
            }
        }
    }

    /**
     * Método encargado de filtrar los pPrediosReltantesTerceraRectificacionMasiva dependiendo de lo
     * que el usuario ingrese en el campo de búsqueda.
     *
     * @author javier.aponte
     */
    public void filtrarPPrediosResultantes() {
        List<PPredio> answer = new ArrayList<PPredio>();

        for (PPredio predioTemp : this.prediosResultantesTerceraRectificacionMasiva) {
            if (predioTemp.getNumeroPredial().contains(
                this.numeroPredialBusquedaTerceraRectificacionMasiva)) {
                answer.add(predioTemp);
            }
        }

        this.prediosResultantes = answer;

    }

    /**
     * Elimina los predios seleccionados para retirar de la cancelacion, no se pueden retirar
     * predios del PH o COndominio original
     *
     *
     * @author felipe.cadena
     */
    public void retirarPrediosCancelacionMasiva() {

        //Se utiliza la entidad deprecada para actualizar la asociacion al tramite que es del tipo deprecado.
        List<TramiteDetallePredio> prediosTramite;
        List<TramiteInconsistencia> inconsistenciasAEliminar =
            new ArrayList<TramiteInconsistencia>();

        //Se valida que no se retiren predio de PH 
        for (PPredio pp : this.prediosCancelarSeleccionados) {
            if (!pp.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0.getCodigo())) {

                this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_007.getMensajeUsuario());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_007.getMensajeTecnico());
                return;

            }
        }

        List<TramiteInconsistencia> inconsistenciasTotales = this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.getTramite().getId());

        prediosTramite = this.getTramiteService().getTramiteDetallePredioByTramiteId(this.
            getTramite().getId());
        for (PPredio pp : this.prediosCancelarSeleccionados) {

            //reversa la proyeccion del predio
            this.getConservacionService().reversarProyeccionPredio(this.getTramite().getId(), pp.
                getId());
            //eliminar tramite detalle predio
            for (TramiteDetallePredio tdt : prediosTramite) {
                if (tdt.getPredio().getId().equals(pp.getId())) {
                    this.getTramiteService().eliminarTramitePredioEnglobe(tdt);
                }
            }

            //eliminar de la lista principal
            TramitePredioEnglobe tpeARetirar = null;
            for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
                if (tpe.getPredio().getId().equals(pp.getId())) {
                    tpeARetirar = tpe;
                }
            }

            if (tpeARetirar != null) {
                this.getTramite().getTramitePredioEnglobes().remove(tpeARetirar);
            }

            //Prepara inconsistencias que deben ser eliminadas
            for (TramiteInconsistencia ti : inconsistenciasTotales) {
                if (ti.getNumeroPredial().equals(pp.getNumeroPredial())) {
                    inconsistenciasAEliminar.add(ti);
                }
            }

            //Se eliminan las inconsistencias a los predios eliminados
            this.getTramiteService().eliminarTramiteInconsistencia(inconsistenciasAEliminar);
        }

        this.inicializarPrediosCancelacionMasiva();
        this.banderaEnviarGeografica = true;

    }

    /**
     * Elimina los predios seleccionados para retirar de la cancelacion, no se pueden retirar
     * predios del PH o Condominio original
     *
     * Se invoca desde el html
     *
     * @author felipe.cadena
     */
    public void retirarPrediosEnglobeVirtual() {

        //Se utiliza la entidad deprecada para actualizar la asociacion al tramite que es del tipo deprecado.
        List<TramiteDetallePredio> prediosTramite;
        List<PFichaMatrizPredioTerreno> prediosAdicionalesEliminar =
            new ArrayList<PFichaMatrizPredioTerreno>();

        List<TramiteInconsistencia> inconsistenciasTotales = this.getTramiteService().
            buscarTramiteInconsistenciaFiltradasParaWebPorTramiteId(this.getTramite().getId());

        prediosTramite = this.getTramiteService().getTramiteDetallePredioByTramiteId(this.
            getTramite().getId());

        String npBase = this.predioARetirar.getNumeroPredialOrigen().substring(0, 21);

        if (!this.predioARetirar.getPredio().getCondicionPropiedad().equals(
            EPredioCondicionPropiedad.CP_5.getCodigo())) {
            this.retirarPrediosEnglobeVirtualGeneral(this.predioARetirar, prediosTramite,
                inconsistenciasTotales);

            for (PFichaMatrizPredioTerreno pfmt : this.getTramite().getFichaMatrizPredioTerrenos()) {

                if (pfmt.getNumeroPredialOrigen().substring(0, 21).equals(npBase) &&
                    pfmt.getPredio().getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.
                        getCodigo())) {
                    prediosAdicionalesEliminar.add(pfmt);
                }
            }
        } else {
            for (PFichaMatrizPredioTerreno pfmt : this.getTramite().getFichaMatrizPredioTerrenos()) {

                if (pfmt.getNumeroPredialOrigen().substring(0, 21).equals(npBase)) {
                    prediosAdicionalesEliminar.add(pfmt);
                }
            }
        }
        //Se eliminan los predios adicionales
        for (PFichaMatrizPredioTerreno pfmt : prediosAdicionalesEliminar) {
            this.retirarPrediosEnglobeVirtualGeneral(pfmt, prediosTramite, inconsistenciasTotales);
        }
        this.getTramiteService().guardarActualizarTramite2(this.getTramite());

    }

    /**
     * Elimina los predios seleccionados para retirar de la cancelacion, no se pueden retirar
     * predios del PH o Condominio original
     *
     *
     * @author felipe.cadena
     */
    public void retirarPrediosEnglobeVirtualGeneral(PFichaMatrizPredioTerreno predioAEliminar,
        List<TramiteDetallePredio> prediosTramite,
        List<TramiteInconsistencia> inconsistenciasTotales) {

        List<TramiteInconsistencia> inconsistenciasAEliminar =
            new ArrayList<TramiteInconsistencia>();

        //eliminar tramite detalle predio
        for (TramiteDetallePredio tdt : prediosTramite) {
            if (tdt.getPredio().getId().equals(predioAEliminar.getPredio().getId())) {
                this.getTramiteService().eliminarTramitePredioEnglobe(tdt);
            }
        }

        //eliminar de la lista principal
        TramitePredioEnglobe tpeARetirar = null;
        for (TramitePredioEnglobe tpe : this.getTramite().getTramitePredioEnglobes()) {
            if (tpe.getPredio().getId().equals(predioAEliminar.getPredio().getId())) {
                tpeARetirar = tpe;
            }
        }

        if (tpeARetirar != null) {
            this.getTramite().getTramitePredioEnglobes().remove(tpeARetirar);
        }

        //Prepara inconsistencias que deben ser eliminadas
        for (TramiteInconsistencia ti : inconsistenciasTotales) {
            if (ti.getNumeroPredial().equals(predioAEliminar.getPredio().getNumeroPredial())) {
                inconsistenciasAEliminar.add(ti);
            }
        }

        //Eliminar de lista de anexos al tramite
        this.getTramite().
            getFichaMatrizPredioTerrenos().remove(predioAEliminar);
        this.getTramiteService().eliminarPFichaMatrizPT(predioAEliminar);

        //Se eliminan las inconsistencias a los predios eliminados
        this.getTramiteService().eliminarTramiteInconsistencia(inconsistenciasAEliminar);

    }

    /**
     * Adiciona los predios seleccionados al tramite de cancelacion masiva
     *
     *
     * @author felipe.cadena
     */
    public void adicionarPrediosCancelacionMasiva() {

        this.banderaEnviarGeografica = true;
        this.banderaCoordinador = false;
        ConsultaPredioMB consultaPredioMB = (ConsultaPredioMB) UtilidadesWeb.getManagedBean(
            "consultaPredio");
        consultaPredioMB.setListenerMethodIdentifier(2);
        List<Predio> prediosSel = consultaPredioMB.getPrediosSeleccionados2();

        if (prediosSel.isEmpty()) {
            this.addMensajeWarn("No existen predios seleccionados");
            return;
        }

        //determina predios condicion cero
        if (!this.determinarPrediosCondicionCero(prediosSel)) {
            return;
        }

        //determina mismo municipio
        if (!RectificacionAreaMB.determinarMunicipio(
            prediosSel, this.getTramite().getPredio().getMunicipio().getCodigo(), this)) {
            return;
        }

        //determina predios bloqueados
        if (this.determinarPrediosBloqueados(prediosSel)) {
            return;
        }

        //determina predios existentes
        prediosSel = this.determinarPrediosExistentes(prediosSel);
        if (prediosSel.isEmpty()) {
            return;
        }

        //determina cancelados
        if (this.determinarPrediosCancelados(prediosSel)) {
            return;
        }

        //determina tramites asociados
        if (this.determinarTramitesAsociados(prediosSel)) {
            return;
        }

        //determina colindancia
        if (!this.determinarColindancia(prediosSel)) {
            return;
        }

        for (Predio predio : prediosSel) {
            this.determinarInconsistenciasGeograficas(predio);
            this.agregarPredioTramite(predio);
        }

        //se regenera la proyeccion 
        this.getTramiteService().generarProyeccion(this.getTramite());

        //se recargan los proyectados proyectados
        this.inicializarPrediosCancelacionMasiva();

    }

    /**
     * Realiza las acciones pertinentes cuando se acepta el englobe virtual
     *
     * @author felipe.cadena
     */
    public void aprobarEnglobeVirtual() {

        //invocar procedimiento BD aceptar 
        Object[] result = this.getTramiteService().
            aceptarEnglobeVirtual(this.getTramite().getId(), this.loggedInUser.getLogin());

        String[] errors = UtilidadesWeb.extraerMensajeSP(result);

        if (errors[0].isEmpty()) {

            Tramite tAux = this.getTramiteService().buscarTramiteCompletoConResolucion(this.
                getTramite().getId());
            this.getTramite().setTramiteInfoAdicionals(tAux.getTramiteInfoAdicionals());
            this.getTramite().setFichaMatrizPredioTerrenos(tAux.getFichaMatrizPredioTerrenos());

            this.inicializaEnglobeVirtual();
            this.banderaEnglobeVirtualAprobado = true;
            this.cargarConstruccionesEnglobeVirtual();
        } else {
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(errors[0]));
        }

    }

    /**
     * Consulta los predios asociados al englobe virtual.
     *
     * @author felipe.cadena
     */
    public void cargarPrediosEnglobeVirtual() {
        List<PPredio> predios = this.getConservacionService().buscarPPrediosCompletosPorTramiteId(
            this.getTramite().getId());
        this.prediosResultantes.clear();
        Long idFichaMatriz = null;
        for (PPredio predio : predios) {
            if (!predio.getEstado().equals(EPredioEstado.CANCELADO.getCodigo())) {
                this.prediosResultantes.add(predio);
                if (predio.isEsPredioFichaMatriz()) {
                    idFichaMatriz = predio.getId();
                }
            }
        }

        //Se actualiza la ficha matriz
        PFichaMatriz fichaMatrizSeleccionada = this.getConservacionService()
            .buscarPFichaMatrizPorPredioId(
                idFichaMatriz);

        this.proyectarConservacionMB.setFichaMatrizSeleccionada(fichaMatrizSeleccionada);
        this.prediosResultantes = this.proyectarConservacionMB
            .cargarCoeficientesCopropiedadPrediosFichaMatriz(this.prediosResultantes);

        this.prediosResultantesTerceraRectificacionMasiva = this.prediosResultantes;

        //actualizar numero prediales originales
        this.prediosResultantes = this.asociarNumerosPredialesOriginales(this.prediosResultantes);
    }

    /**
     * Asocia los numero prediales originales cuando se renumera el predio
     *
     * @author felipe.cadena
     *
     * @param predios
     * @return
     */
    public List<PPredio> asociarNumerosPredialesOriginales(List<PPredio> predios) {

        if (predios == null || predios.isEmpty()) {
            return predios;
        }

        List<Long> idsPredio = new ArrayList<Long>();
        for (PPredio pp : predios) {
            idsPredio.add(pp.getId());
        }

        List<Predio> prediosOriginales = this.getConservacionService().consultarPrediosPorPredioIds(
            idsPredio);

        for (Predio predio : prediosOriginales) {
            for (PPredio pp : predios) {

                if (pp.getId().equals(predio.getId()) &&
                    !pp.getNumeroPredial().equals(predio.getNumeroPredial())) {
                    pp.setNumeroPredialOriginal(predio.getNumeroPredial());
                }
            }
        }

        return predios;
    }

    /**
     * Consulta y modifica los valores de C/I de las cosntrucciones de los predios asociados al
     * englobe virtual.
     *
     * @author leidy.gonzalez
     */
    public void cargarConstruccionesEnglobeVirtual() {

        List<PUnidadConstruccion> unidadConstruccionOriginalesEnglobeVirtual = this.
            getConservacionService()
            .buscarUnidConstOriginalesCanceladasPorTramite(this.getTramite().getId());

        //Se carga Unidades Convencionales Comunes(FM), condicion 0 y 5
        if (unidadConstruccionOriginalesEnglobeVirtual != null &&
            !unidadConstruccionOriginalesEnglobeVirtual.isEmpty()) {

            for (PUnidadConstruccion pUnidadConstTemp : unidadConstruccionOriginalesEnglobeVirtual) {

                pUnidadConstTemp.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                    .getCodigo());

            }

            this.getConservacionService()
                .guardarListaPUnidadConstruccion(unidadConstruccionOriginalesEnglobeVirtual);

        }

    }

    /**
     * Adiciona los predios seleccionados al tramite de modificacion de PH
     *
     *
     * @author felipe.cadena
     */
    public void adicionarPrediosEnglobeVirtual() {

        if (this.getTramite().getFichaMatrizPredioTerrenos() == null) {
            this.getTramite().setFichaMatrizPredioTerrenos(
                new ArrayList<PFichaMatrizPredioTerreno>());
        }

        this.banderaEnviarGeografica = true;
        this.banderaCoordinador = false;
        ConsultaPredioMB consultaPredioMB = (ConsultaPredioMB) UtilidadesWeb.getManagedBean(
            "consultaPredio");
        consultaPredioMB.setListenerMethodIdentifier(2);
        List<Predio> prediosSel = consultaPredioMB.getPrediosSeleccionados2();

        if (prediosSel.isEmpty()) {
            this.addMensajeWarn("No existen predios seleccionados");
            return;
        }

        //determina misma manzana       
        if (!this.determinarPrediosMismaManzana(prediosSel)) {
            return;
        }

        //determina predios bloqueados
        if (this.determinarPrediosBloqueados(prediosSel)) {
            return;
        }

        //determina predios existentes
        prediosSel = this.determinarPrediosExistentes(prediosSel);
        if (prediosSel.isEmpty()) {
            return;
        }

        //determina cancelados
        if (this.determinarPrediosCancelados(prediosSel)) {
            return;
        }

        //determina tramites asociados
        if (this.determinarTramitesAsociados(prediosSel)) {
            return;
        }

        //determina predios condicion cero
        if (!this.determinarPrediosFMCondicionCero(prediosSel)) {
            return;
        }

        //determina colindancia
        //se consideran los predios que ya estan asociados
        List<Predio> prediosAgregadosPreviamente = new ArrayList<Predio>();
        if (this.getTramite().getFichaMatrizPredioTerrenos() != null) {
            for (PFichaMatrizPredioTerreno pfmpt : this.getTramite().getFichaMatrizPredioTerrenos()) {
                prediosAgregadosPreviamente.add(pfmpt.getPredio());
            }
        }

        //determina inconsistencias
        for (Predio predio : prediosSel) {

            this.determinarInconsistenciasGeograficas(predio);
        }

        if (this.banderaExistenInconsistencias) {
            return;
        }

        prediosAgregadosPreviamente.addAll(prediosSel);
        if (!this.determinarColindancia(prediosAgregadosPreviamente)) {
            return;
        }

        for (Predio predio : prediosSel) {

            this.agregarPredioTramite(predio);
            predio = this.getConservacionService().buscarPredioCompletoPorPredioId(predio.getId());
            PFichaMatrizPredioTerreno fmpt = new PFichaMatrizPredioTerreno();
            fmpt.setPredio(predio);
            fmpt.setTramite(this.getTramite());
            fmpt.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE.getCodigo());
            fmpt.setEstado(EFichaMatrizEstado.ACTIVO.getCodigo());
            fmpt.setUsuarioLog(this.loggedInUser.getLogin());
            fmpt.setFechaLog(new Date());

            //campos de la ficha original   
            fmpt.setNumeroPredialOrigen(predio.getNumeroPredial());
            fmpt.setDireccionPrincipalOrigen(predio.getDireccionPrincipal());
            if (predio.getCirculoRegistral() != null && predio.getNumeroRegistro() != null) {
                fmpt.setMatriculaOrigen(predio.getCirculoRegistral().getCodigo() + "-" + predio.
                    getNumeroRegistro());
            }
            if (predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0.getCodigo()) ||
                predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_5.getCodigo())) {
                fmpt.setAreaTotalTerrenoOrigen(predio.getAreaTerreno());
                fmpt.setAreaConstruidaComunOrigen(0d);
                fmpt.setAreaConstruidaPrivadaOrigen(predio.getAreaConstruccion());
            } else {
                FichaMatriz fichaPredio = this.getConservacionService().
                    buscarFichaMatrizPorPredioId(predio.getId());

                fmpt.setAreaTotalTerrenoOrigen(fichaPredio.getAreaTotalTerrenoComun() + fichaPredio.
                    getAreaTotalTerrenoPrivada());
                fmpt.setAreaConstruidaComunOrigen(fichaPredio.getAreaTotalConstruidaComun());
                fmpt.setAreaConstruidaPrivadaOrigen(fichaPredio.getAreaTotalConstruidaPrivada());
            }

            this.getTramite().getFichaMatrizPredioTerrenos().add(fmpt);

        }

        prediosSel.clear();
        consultaPredioMB.getPrediosSeleccionados2().clear();
        consultaPredioMB.getPrediosSeleccionados().clear();
        this.getTramiteService().guardarActualizarTramite2(this.getTramite());
        this.getProyectarConservacionMB().setTramite(this.getTramiteService()
            .buscarTramitePorTramiteIdProyeccion(
                this.getTramite().getId()));
    }

    /**
     * Metodo para determinar si los predios son colindantes indirectos con el predio relacionado al
     * tramite.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarColindancia(List<Predio> prediosSel) {

        prediosSel.add(this.getTramite().getPredio());
        List<Predio> prediosNoColindantes = this.getConservacionService().
            determinarColindancia(prediosSel, this.getTramite().getPredio(), this.loggedInUser, true);

        prediosSel.remove(this.getTramite().getPredio());
        if (prediosNoColindantes.isEmpty()) {

            return true;
        }
        StringBuilder mensaje = new StringBuilder("");
        for (Predio predio : prediosNoColindantes) {
            mensaje.append(predio.getNumeroPredial()).append(", ");
        }

        this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_002.getMensajeUsuario(mensaje.
            toString()));
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("error", "error");
        LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_002.getMensajeTecnico(mensaje.toString()));

        return false;
    }

    /**
     * Metodo para determinar si los predios tienen otros tramites asociados.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarTramitesAsociados(List<Predio> prediosSel) {

        boolean tienenTramites = false;
        for (Predio predio : prediosSel) {
            //Determinar si el predio tiene tramites asociados
            List<Tramite> tramites = (ArrayList<Tramite>) this.getTramiteService().
                buscarTramitesPendientesDePredio(predio.getId());
            if (tramites != null && tramites.size() > 0) {
                if (tramites.size() == 1) {
                    if (tramites.get(0).getId().equals(this.getTramite().getId())) {
                        continue;
                    }
                }
                StringBuilder tramiteRads = new StringBuilder("");
                tienenTramites = true;
                for (Tramite tramite : tramites) {
                    tramiteRads.append(tramite.getNumeroRadicacion()).append(", ");
                }
                this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_003.getMensajeUsuario(
                    predio.getNumeroPredial(), tramiteRads.toString()));
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_003.getMensajeTecnico(predio.
                    getNumeroPredial(), tramiteRads.toString()));
            }
        }
        return tienenTramites;
    }

    /**
     * Metodo para determinar si los predios estan en estado cancelado.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarPrediosCancelados(List<Predio> prediosSel) {

        boolean cancelados = false;
        for (Predio predio : prediosSel) {
            if (predio.getEstado() != null && predio.getEstado().equals(ETramiteEstado.CANCELADO.
                getCodigo())) {
                cancelados = true;
                this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_004.getMensajeUsuario(
                    predio.getNumeroPredial()));
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_004.getMensajeTecnico());
            }
        }
        return cancelados;
    }

    /**
     * Metodo para determinar si los predios estan en estado cancelado.
     *
     * @author felipe.cadena
     * @return
     */
    public List<Predio> determinarPrediosExistentes(List<Predio> prediosSel) {

        for (int i = 0; i < prediosSel.size(); i++) {
            for (PPredio pp : this.prediosResultantes) {
                if (pp.getId().equals(prediosSel.get(i).getId())) {
                    prediosSel.remove(prediosSel.get(i));
                    this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_005.getMensajeUsuario(
                        pp.getNumeroPredial()));
                    this.addErrorCallback();
                    LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_005.getMensajeTecnico());
                    break;
                }
            }
            if (this.getTramite().isEnglobeVirtual()) { //validacion adicional para tramites de englobe virtual
                for (PFichaMatrizPredioTerreno pfmpt : this.getTramite().
                    getFichaMatrizPredioTerrenos()) {
                    if (pfmpt.getPredio().getId().equals(prediosSel.get(i).getId())) {
                        prediosSel.remove(prediosSel.get(i));
                        this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_005.
                            getMensajeUsuario(
                                pfmpt.getPredio().getNumeroPredial()));
                        this.addErrorCallback();
                        LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_005.getMensajeTecnico());
                        break;
                    }
                }

            }
        }
        return prediosSel;
    }

    /**
     * Metodo para determinar si los predios son todos condicion cero.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarPrediosCondicionCero(List<Predio> prediosSel) {

        for (Predio predio : prediosSel) {
            if (predio.getCondicionPropiedad() != null && !predio.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_0.getCodigo())) {
                this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_001.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_001.getMensajeTecnico());
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo para determinar si los predios tienen algun tipo de bloqueo.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarPrediosBloqueados(List<Predio> prediosSel) {

        boolean bloqueado = false;
        for (Predio predio : prediosSel) {
            List<PredioBloqueo> bloqueos = this.getConservacionService().obtenerBloqueosPredio(
                predio.getId(), null);
            //TODO::felipe.cadena::pendiente validacion de estado bloqueo
            if (bloqueos != null && !bloqueos.isEmpty()) {
                for (PredioBloqueo pb : bloqueos) {
                    if (pb.getFechaDesbloqueo() == null || pb.getFechaDesbloqueo().after(new Date())) {
                        this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_006.
                            getMensajeUsuario(predio.getNumeroPredial()));
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        LOGGER.error(ECodigoErrorVista.CANCELACION_MASIVA_006.getMensajeTecnico(
                            predio.getNumeroPredial()));
                        bloqueado = true;
                        break;
                    }
                }
            }
        }
        return bloqueado;
    }

    /**
     * Metodo para determinar si uno de los predios seleccionados tiene inconsistencias geográficas.
     *
     * @author felipe.cadena
     * @return
     */
    public void determinarInconsistenciasGeograficas(Predio predioSel) {

        List<TramiteInconsistencia> inconsistencias = null;
        String mensaje = "";

        try {
            //Se validan la unidades del condominio
            if (this.getTramite().isEnglobeVirtual() && predioSel.getCondicionPropiedad().equals(
                EPredioCondicionPropiedad.CP_8.getCodigo())) {
                FichaMatriz fm = this.getConservacionService().getFichaMatrizByNumeroPredialPredio(
                    predioSel.getNumeroPredial());

                String nPrediales = "";

                for (FichaMatrizPredio fmp : fm.getFichaMatrizPredios()) {
                    nPrediales += fmp.getNumeroPredial() + ",";
                }

                nPrediales += predioSel.getNumeroPredial();

                inconsistencias = this.getGeneralesService().determinarInconsistenciasGeograficas(
                    nPrediales, this.getTramite(), this.loggedInUser);

            } else {
                inconsistencias = this.getGeneralesService().determinarInconsistenciasGeograficas(
                    predioSel.getNumeroPredial(), this.getTramite(), this.loggedInUser);
            }

            if (inconsistencias == null || inconsistencias.isEmpty()) {
                this.banderaExistenInconsistencias = false;
                return;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);

        }

        Map<String, List<String>> icps = new HashMap<String, List<String>>();
        for (TramiteInconsistencia ti : inconsistencias) {
            if (icps.get(ti.getNumeroPredial()) == null) {
                List icp = new ArrayList<String>();
                icp.add(ti.getInconsistencia());
                icps.put(ti.getNumeroPredial(), icp);
            } else {
                icps.get(ti.getNumeroPredial()).add(ti.getInconsistencia());
            }
        }

        for (String nPredial : icps.keySet()) {
            mensaje = "El predio No " + nPredial + ", tiene las siguientes inconsistencias: ";
            for (String ti : icps.get(nPredial)) {
                mensaje += ti + ", ";

            }
            this.addMensajeError(mensaje.substring(0, mensaje.lastIndexOf(",")) + ".");
        }

        this.getTramiteService().guardarActualizarTramiteInconsistencia(inconsistencias);

        this.banderaExistenInconsistencias = true;
        this.banderaEnviarDepuracion = true;
        this.banderaEnviarGeografica = false;
        this.banderaCoordinador = false;

    }

    /**
     * Metodo para determinar si los predios son todos coresponden a una ficha matriz.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarPrediosFMCondicionCero(List<Predio> prediosSel) {

        for (Predio predio : prediosSel) {
            if (predio.isEsPredioEnPH()) {
                FichaMatriz fm = this.getConservacionService().
                    buscarFichaMatrizPorPredioIdSinAsociaciones(predio.getId());

                if (fm == null) {
                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_001.toString());
                    this.addErrorCallback();
                    LOGGER.error(ECodigoErrorVista.MODIFICACION_PH_001.getMensajeTecnico());
                    return false;

                }
            } else if (predio.isMejora()) {
                String numeroBase = predio.getNumeroPredial().substring(0, 21);
                boolean existePredioBase = false;
                for (Predio pAux : prediosSel) {
                    if (pAux.getNumeroPredial().startsWith(numeroBase + "0")) {
                        existePredioBase = true;
                        break;
                    }
                }

                if (!existePredioBase) {
                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_002.toString());
                    this.addErrorCallback();
                    LOGGER.error(ECodigoErrorVista.MODIFICACION_PH_002.getMensajeTecnico());
                    return false;

                }
            } else if (predio.getCondicionPropiedad().equals(EPredioCondicionPropiedad.CP_0.
                getCodigo())) {
                List<Predio> mejoras = this.getConservacionService().buscarMejorasPorNumeroPredial(
                    predio.getNumeroPredial());

                if (!mejoras.isEmpty()) {
                    for (Predio mejora : mejoras) {
                        boolean existeMejora = false;
                        for (Predio predioSel : prediosSel) {
                            if (predioSel.getId().equals(mejora.getId())) {
                                existeMejora = true;
                                break;
                            }
                        }
                        if (!existeMejora) {
                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_004.
                                getMensajeUsuario(predio.getNumeroPredial()));
                            this.addErrorCallback();
                            LOGGER.error(ECodigoErrorVista.MODIFICACION_PH_004.getMensajeTecnico());
                            return false;
                        }
                    }
                }

            } else {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_002.toString());
                this.addErrorCallback();
                LOGGER.error(ECodigoErrorVista.MODIFICACION_PH_002.getMensajeTecnico());
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo para determinar si los predios son todos coresponden a una ficha matriz.
     *
     * @author felipe.cadena
     * @return
     */
    public boolean determinarPrediosMismaManzana(List<Predio> prediosSel) {

        String codigoManzana = this.getTramite().getPredio().getNumeroPredial().substring(0, 17);
        for (Predio predio : prediosSel) {

            if (!predio.getNumeroPredial().startsWith(codigoManzana)) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_003.toString());
                this.addErrorCallback();
                LOGGER.error(ECodigoErrorVista.MODIFICACION_PH_003.getMensajeTecnico());
                return false;
            }
        }
        return true;
    }

    /**
     * Agrega el predio al tramite actual para su posterior proyeccion
     *
     * @author felipe.cadena
     *
     * @param predio
     */
    public void agregarPredioTramite(Predio predio) {

        //Se utiliza la entidad deprecada para actualizar la asociacion al tramite que es del tipo deprecado.
        TramitePredioEnglobe tdp = new TramitePredioEnglobe();
        tdp.setPredio(predio);
        tdp.setEnglobePrincipal(ESiNo.NO.getCodigo());
        tdp.setTramite(this.getTramite());
        tdp.setUsuarioLog(this.loggedInUser.getLogin());
        tdp.setFechaLog(new Date());
        tdp = this.getTramiteService().guardarYactualizarTramitePredioEnglobe(tdp);
        if (tdp == null) {
            this.addMensajeError("Error al guardar en la Base de datos");
            return;
        }
        tdp.setPredio(predio);
        this.getTramite().getTramitePredioEnglobes().add(tdp);

    }

    /**
     * elimina las replicas geograficas asociadas al tramite de cancelacion masiva
     *
     * @author felipe.cadena
     */
    public void eliminarReplicasGeograficas() {

        Documento replicaFinal = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.getTramite().getId(), ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());
        if (replicaFinal != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(replicaFinal);
        }
        Documento replicaInicial = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.getTramite().getId(), ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());
        if (replicaInicial != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(replicaInicial);
        }
        Documento replicaConsulta = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.getTramite().getId(), ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());
        if (replicaConsulta != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(replicaConsulta);
        }
        //Elimina imagenes del comparativo
        Documento imagenInicial = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.getTramite().getId(),
                ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_ANTES_DEL_TRAMITE.getId());
        if (imagenInicial != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(imagenInicial);
        }
        Documento imagenFinal = this.getGeneralesService().
            buscarDocumentoPorTramiteIdyTipoDocumento(
                this.getTramite().getId(),
                ETipoDocumento.IMAGEN_GEOGRAFICA_DEL_PREDIO_DESPUES_DEL_TRAMITE.getId());
        if (imagenFinal != null) {
            this.getGeneralesService().borrarDocumentoEnBDyAlfresco(imagenFinal);
        }

    }

    /**
     * Metodo para inicializar datos asociados a tramites de moodificacion de PH
     *
     *
     * @author felipe.cadena
     */
    public void inicializaEnglobeVirtual() {

        this.cargarPrediosEnglobeVirtual();

        if (ESiNo.SI.getCodigo().equals(this.getTramite().obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.ENGLOBE_VIRTUAL))) {
            this.englobeVirtual = ESiNo.SI.getCodigo();
        } else {
            this.englobeVirtual = ESiNo.NO.getCodigo();
        }
        if (ESiNo.SI.getCodigo().equals(this.getTramite().obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.NUEVA_FICHA_MATRIZ_EV))) {
            this.nuevaFichaMatriz = ESiNo.SI.getCodigo();
        } else {
            this.nuevaFichaMatriz = ESiNo.NO.getCodigo();
        }

        if (ESiNo.SI.getCodigo().equals(this.getTramite().obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.ENGLOBE_VIRTUAL_APROBADO))) {
            this.banderaEnglobeVirtualAprobado = true;
        } else {
            this.banderaEnglobeVirtualAprobado = false;
        }

        this.numeroFichaMatriz = this.getTramite().obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV);
        this.numeroFichaMatrizSel = new SelectItem(this.numeroFichaMatriz, UtilidadesWeb.
            formatNumeroPredial(this.numeroFichaMatriz));
        this.tipoFichaMatriz = this.getTramite().obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.TIPO_FICHA_MATRIZ_EV);

        //inicializa los tipos de ficha
        this.tiposFicha = new ArrayList<SelectItem>();

        SelectItem item = new SelectItem(EPredioCondicionPropiedad.CP_9.getCodigo(),
            EPredioCondicionPropiedad.CP_9.getCodigo() + " - " + EPredioCondicionPropiedad.CP_9.
            getValor());
        this.tiposFicha.add(item);
        item = new SelectItem(EPredioCondicionPropiedad.CP_8.getCodigo(),
            EPredioCondicionPropiedad.CP_8.getCodigo() + " - " + EPredioCondicionPropiedad.CP_8.
            getValor());
        this.tiposFicha.add(item);
        item = new SelectItem(EPredioCondicionPropiedad.CP_1.getCodigo(),
            EPredioCondicionPropiedad.CP_1.getCodigo() + " - " + EPredioCondicionPropiedad.CP_1.
            getValor());
        this.tiposFicha.add(item);

    }

    /**
     * Realiza las acciones cuando no se acepta la accion de englobe virtual
     *
     * @author felipe.cadena
     *
     */
    public void listenerEnglobeVirtualNo() {

        if (this.englobeVirtual.equals(ESiNo.SI.getCodigo())) {
            this.englobeVirtual = ESiNo.NO.getCodigo();
        } else {
            this.englobeVirtual = ESiNo.SI.getCodigo();
        }
    }

    /**
     * Realiza las actualizaciones cuando se selecciona o no el desenglobe de manzanas
     *
     * @author felipe.cadena
     *
     */
    public void listenerEnglobeVirtual() {

        //Se elimina la proyeccion del tramite para crear una nueva desde cero
        this.getTramiteService().eliminarProyeccion(this.getTramite());

        if (this.englobeVirtual.equals(ESiNo.SI.getCodigo())) {
            this.getTramite().asociarInfoCampoAdicional(EInfoAdicionalCampo.ENGLOBE_VIRTUAL,
                ESiNo.SI.getCodigo());
            //Se actualiza la informacion del tramite
            Tramite t = this.getTramiteService().guardarActualizarTramite2(this.getTramite());
            this.getTramite().setTramiteInfoAdicionals(t.getTramiteInfoAdicionals());
            //Se genera de nuevo la proyeccion
            this.getTramiteService().generarProyeccion(this.getTramite().getId());
        } else {
            this.getTramiteService().eliminarTramiteInfoMultiple(this.getTramite().
                getTramiteInfoAdicionals());
            this.getTramite().getTramiteInfoAdicionals().clear();

            this.eliminarReplicasGeograficas();
            this.getTramiteService().eliminarTramiteDetallePredioMultiple(Utilidades.
                convertirTPEtoTDPMultiple(this.getTramite().getTramitePredioEnglobes()));

            List<TramitePredioEnglobe> prediosTemp = new ArrayList<TramitePredioEnglobe>();

            for (TramitePredioEnglobe tramitePredioEnglobe : this.getTramite().
                getTramitePredioEnglobes()) {
                prediosTemp.add(tramitePredioEnglobe);
            }
            this.getTramite().getTramitePredioEnglobes().clear();

            List<TramitePredioEnglobe> prediosAsociadosFM = new ArrayList<TramitePredioEnglobe>();

            List<Predio> prediosFM = this.getConservacionService().buscarPrediosPorRaizNumPredial(
                this.getTramite().getPredio().getNumeroPredial().substring(0, 22));
            for (Predio predio : prediosFM) {

                if (predio.getId().equals(this.getTramite().getPredio().getId())) {
                    continue;
                }
                //No se agregan los predios de mejoras o los que estan cancelados.
                if (predio.isMejora() || EPredioEstado.CANCELADO.getCodigo().equals(predio.
                    getEstado())) {
                    continue;
                }
                TramitePredioEnglobe tpe = new TramitePredioEnglobe();
                tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
                tpe.setPredio(predio);
                tpe.setTramite(this.getTramite());
                tpe.setFechaLog(new Date());
                tpe.setUsuarioLog(this.loggedInUser.getLogin());
                prediosAsociadosFM.add(tpe);
            }

            this.getTramiteService().guardarYActualizarTramiteDetallePredioMultiple(Utilidades.
                convertirTPEtoTDPMultiple(prediosAsociadosFM));

            this.getProyectarConservacionMB().setTramite(this.getTramiteService()
                .buscarTramitePorTramiteIdProyeccion(
                    this.getTramite().getId()));

            this.getConservacionService().eliminarFichaMatrizPredioTerrenoMul(this.getTramite().
                getFichaMatrizPredioTerrenos());
            this.getTramite().getFichaMatrizPredioTerrenos().clear();
            this.getTramiteService().guardarActualizarTramite2(this.getTramite());

        }

        //Se recarga la informacion del tramite
        this.proyectarConservacionMB.init();
        this.init();

        this.proyectarConservacionMB.setPredioSeleccionado(null);
        this.proyectarConservacionMB.setTab(0);
    }

    /**
     * Prepara las fichas disponibles para el tramite de englobe virtual
     *
     * @author felipe.cadena
     */
    public void inicializarSeleccionFicha() {

        this.numerosFichaDisponibles = new ArrayList<SelectItem>();
        if (this.nuevaFichaMatriz == null) {
            this.nuevaFichaMatriz = ESiNo.NO.getCodigo();
        }

        if (this.nuevaFichaMatriz.equals(ESiNo.NO.getCodigo())) {
            //ficha radicada
            SelectItem si = new SelectItem(this.getTramite().getPredio().getNumeroPredial(),
                this.getTramite().getPredio().getFormattedNumeroPredialCompleto());
            this.numerosFichaDisponibles.add(si);

            for (PFichaMatrizPredioTerreno pfmpt : this.getTramite().getFichaMatrizPredioTerrenos()) {

                if (pfmpt.getPredio().isEsPredioEnPH()) {

                    si = new SelectItem(pfmpt.getPredio().getNumeroPredial(),
                        pfmpt.getPredio().getFormattedNumeroPredialCompleto());
                    this.numerosFichaDisponibles.add(si);
                }

            }
        } else {
            this.tipoFichaMatriz = null;
        }

    }

    /**
     * Prepara las fichas disponibles para el tramite de englobe virtual
     *
     * @author felipe.cadena
     */
    public void generarNumeroNuevaFicha() {

        if (this.validarTipoFicha(this.tipoFichaMatriz)) {

            //invocar procedimiento BD generar numero 
            Object[] result = this.getTramiteService().generarNumeroEnglobeVirtual(
                this.getTramite().getId(),
                this.tipoFichaMatriz,
                this.loggedInUser.getLogin());

            if (result[0] != null) {
                String nuevoNumero = (String) result[0];
                this.numeroFichaMatrizSel = new SelectItem(nuevoNumero, UtilidadesWeb.
                    formatNumeroPredial(nuevoNumero));
            } else {
                this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(
                    (String) result[1]));
            }

        } else {
            this.numeroFichaMatrizSel = null;
            this.numeroFichaMatriz = null;
        }

    }

    /**
     * Se cancela la generacion del numero predial
     *
     * @author felipe.cadena
     */
    public void cancelarNumeroFichaMatriz() {
        this.numeroFichaMatrizSel = null;
        this.numeroFichaMatriz = null;
        this.nuevaFichaMatriz = ESiNo.NO.getCodigo();
    }

    /**
     * Reversa las operaciones realizadas en el tramite de englobe virtual
     *
     * @author felipe.cadena
     */
    public void reversarEnglobeVirtual() {

        //para garantizar que no se eliminen los predios cargados previamente
        for (PFichaMatrizPredioTerreno fmpt : this.getTramite().getFichaMatrizPredioTerrenos()) {
            if (!fmpt.getNumeroPredialOrigen().equals(this.getTramite().getPredio().
                getNumeroPredial())) {
                fmpt.setFichaMatriz(null);
                this.getConservacionService().actualizarListaPFichaMatrizPredioTerreno(fmpt);
            }
        }

        boolean result = this.getTramiteService().
            reversarProyeccion(this.getTramite());

        if (result) {
            this.inicializaEnglobeVirtual();
        } else {
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(
                EProcedimientoAlmacenadoFuncion.REVERSAR_PROYECCION.toString()));
            return;
        }

        this.eliminarReplicasGeograficas();

        this.getTramiteService().eliminarTramiteDetallePredioMultiple(Utilidades.
            convertirTPEtoTDPMultiple(this.getTramite().getTramitePredioEnglobes()));

        List<TramitePredioEnglobe> prediosTemp = new ArrayList<TramitePredioEnglobe>();

        for (TramitePredioEnglobe tramitePredioEnglobe : this.getTramite().
            getTramitePredioEnglobes()) {
            prediosTemp.add(tramitePredioEnglobe);
        }
        this.getTramite().getTramitePredioEnglobes().clear();

        List<TramitePredioEnglobe> prediosAsociadosFM = new ArrayList<TramitePredioEnglobe>();

        List<Predio> prediosFM = this.getConservacionService().buscarPrediosPorRaizNumPredial(
            this.getTramite().getPredio().getNumeroPredial().substring(0, 22));
        for (Predio predio : prediosFM) {

            if (predio.getId().equals(this.getTramite().getPredio().getId())) {
                continue;
            }
            //No se agregan los predios de mejoras o los que estan cancelados.
            if (predio.isMejora() || EPredioEstado.CANCELADO.getCodigo().equals(predio.getEstado())) {
                continue;
            }
            TramitePredioEnglobe tpe = new TramitePredioEnglobe();
            tpe.setEnglobePrincipal(ESiNo.NO.getCodigo());
            tpe.setPredio(predio);
            tpe.setTramite(this.getTramite());
            tpe.setFechaLog(new Date());
            tpe.setUsuarioLog(this.loggedInUser.getLogin());
            prediosAsociadosFM.add(tpe);
        }

        this.getTramiteService().guardarYActualizarTramiteDetallePredioMultiple(Utilidades.
            convertirTPEtoTDPMultiple(prediosAsociadosFM));

        this.getProyectarConservacionMB().setTramite(this.getTramiteService()
            .buscarTramitePorTramiteIdProyeccion(
                this.getTramite().getId()));

        //Se reproyecta el tramite completo
        Object mensajes[] = this.getTramiteService().generarProyeccion(this.getTramite());

        String[] errors = UtilidadesWeb.extraerMensajeSP(mensajes);

        if (!errors[0].isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(errors[0]));
        }

        this.getTramite().asociarInfoCampoAdicional(EInfoAdicionalCampo.ENGLOBE_VIRTUAL_APROBADO,
            ESiNo.NO.getCodigo());
        this.getTramite().
            asociarInfoCampoAdicional(EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV, null);
        this.getTramite().asociarInfoCampoAdicional(EInfoAdicionalCampo.NUEVA_FICHA_MATRIZ_EV, null);

        //Se restauran los predios asociado previamente, sin proyectar
        for (TramitePredioEnglobe temp : prediosTemp) {
            for (TramitePredioEnglobe tpe : prediosAsociadosFM) {
                if (!tpe.getPredio().getId().equals(tpe.getPredio().getId())) {
                    this.getTramite().getTramitePredioEnglobes().add(temp);
                }
            }

        }
        this.getTramiteService().guardarActualizarTramite2(this.getTramite());

        this.inicializaEnglobeVirtual();

        this.banderaEnglobeVirtualAprobado = false;
    }

    /**
     * Valida si el tipo de ficha es consistente con las fichas asociadas
     *
     *
     * @author felipe.cadena
     * @return
     */
    private boolean validarTipoFicha(String tipoSeleccionado) {

        boolean resultado = true;
        boolean con = false;
        boolean ph = false;
        boolean mix = false;

        if (this.getTramite().getPredio().getCondicionPropiedad().
            equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
            ph = true;
        }
        if (this.getTramite().getPredio().getCondicionPropiedad().
            equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
            con = true;
        }

        if (this.getTramite().getPredio().getCondicionPropiedad().
            equals(EPredioCondicionPropiedad.CP_1.getCodigo()) ||
            (ph && con)) {
            mix = true;
        }

        if (!mix) {
            for (PFichaMatrizPredioTerreno pfmpt : this.getTramite().getFichaMatrizPredioTerrenos()) {
                if (!EPredioCondicionPropiedad.CP_0.getCodigo().equals(pfmpt.getPredio().
                    getCondicionPropiedad()) &&
                    !EPredioCondicionPropiedad.CP_5.getCodigo().equals(pfmpt.getPredio().
                        getCondicionPropiedad())) {

                    if (pfmpt.getPredio().getCondicionPropiedad().
                        equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
                        ph = true;
                    }
                    if (pfmpt.getPredio().getCondicionPropiedad().
                        equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
                        con = true;
                    }

                    if (pfmpt.getPredio().getCondicionPropiedad().
                        equals(EPredioCondicionPropiedad.CP_1.getCodigo()) ||
                        (ph && con)) {
                        mix = true;
                        break;
                    }

                }
            }
        }

        if (mix || (ph && con)) {
            if (!tipoSeleccionado.equals(EPredioCondicionPropiedad.CP_1.getCodigo())) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_009.toString());
                LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_009.getMensajeTecnico());
                this.addErrorCallback();
                return false;
            } else {
                return true;
            }
        }
        if (ph) {
            if (!tipoSeleccionado.equals(EPredioCondicionPropiedad.CP_9.getCodigo())) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_009.toString());
                LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_009.getMensajeTecnico());
                this.addErrorCallback();
                return false;
            } else {
                return true;
            }
        }
        if (con) {
            if (!tipoSeleccionado.equals(EPredioCondicionPropiedad.CP_8.getCodigo())) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_009.toString());
                LOGGER.debug(ECodigoErrorVista.MODIFICACION_PH_009.getMensajeTecnico());
                this.addErrorCallback();
                return false;
            } else {
                return true;
            }
        }

        return resultado;
    }

    /**
     * Guarda los datos seleccionados de la ficha
     *
     *
     * @author felipe.cadena
     * @return
     */
    public void guardarNumeroFichaMatriz() {

        if (this.numeroFichaMatrizSel == null) {
            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_077.getMensajeUsuario());
            this.addErrorCallback();
        } else {
            String tipoSeleccionado = this.numeroFichaMatrizSel.getValue().toString().substring(21,
                22);
            if (this.validarTipoFicha(tipoSeleccionado)) {
                this.getTramite().asociarInfoCampoAdicional(
                    EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV, numeroFichaMatriz);
                this.getTramite().asociarInfoCampoAdicional(
                    EInfoAdicionalCampo.NUEVA_FICHA_MATRIZ_EV, nuevaFichaMatriz);

                Tramite t = this.getTramiteService().guardarActualizarTramite2(this.getTramite());
                this.getTramite().setTramiteInfoAdicionals(t.getTramiteInfoAdicionals());
            }
        }

    }

    /**
     * Valida si los predios seleccionados de un trÃ¡mite de englobe virtual se pueden modificar
     *
     * @author leidy.gonzalez
     * @return
     */
    private boolean validarPrediosSeleccionadosEnglobeVirtual() {

        for (PPredio pp : this.prediosResultantesSeleccionados) {

            if (pp.isEsPredioFichaMatriz() &&
                ESiNo.SI.getCodigo().
                    equals(this.getTramite().obtenerInfoCampoAdicional(
                        EInfoAdicionalCampo.NUEVA_FICHA_MATRIZ_EV))) {
                return false;
            }

        }
        return true;
    }

    /**
     * MÃ©todo que carga la registro final de la matricula Inmobiliaria de acuerdo a la registro
     * inicial seleccionada.
     *
     * @author leidy.gonzalez
     */
    public void calcularRegFinal() {

        this.banderaHabilitaGuardar = false;

        if (numeroRegistroInicial > 0) {
            this.numeroRegistrofinal = (this.numeroRegistroInicial + ((this.nPrediosSeleccionados -
                1) * this.incrementeoRegistro));
            this.banderaHabilitaGuardar = true;
        } else {
            this.banderaHabilitaGuardar = false;
            this.addMensajeError("El numero de registroinicial debe tener un valor mayor a 0 ");
            return;
        }

    }
    
    /**
     * MÃ©todo que se carga al cerrar la ventana de registro de datos
     * para la creación del predio nuevo.
     *
     * @author leidy.gonzalez
     */
    public void limpiarCamposNuevoPredioQuinta() {
        //this.setZona(new String(""));
        this.zona = "";
        //this.setSector(new String(""));
        this.sector = "";
        //this.setComuna(new String(""));
        this.comuna = "";
        this.barrio = "";
        this.manzana = "";
        this.edificioTorre = "";
        this.condicionPropiedadSeleccionada = "";
        this.initializeSelectItems();
        this.combosDeptosMunis.resetAll();
        this.terreno = "";

        this.banderaActivaCancelar=true;


        this.predioNuevo = new PPredio();
        this.okToAddAcceptPredioNuevoQuinta = true;


        if (this.ingresarPredioNuevoOmitido) {
            this.getTramite().setOmitidoNuevo(ESiNo.SI.getCodigo());
        } else {
            this.getTramite().setOmitidoNuevo(ESiNo.NO.getCodigo());
        }
        this.condicionPropiedadSelectItems = armarCondicionPropiedadPredioQuintaCombo();

//            if (!this.getTramite().isQuintaOmitido()) {
        //D: esta línea es la que sirve para nuevo, pero no para omitido
        this.predioNuevo.setNumeroPredial(this.getTramite().getNumeroPredial());
//        this.predioNuevo.setNumeroPredial("");
//                this.zona = null;
//            }

        this.predioNuevo.setDepartamento(this.getTramite().getDepartamento());
        this.predioNuevo.setMunicipio(this.getTramite().getMunicipio());

        if (!this.getTramite().isQuintaOmitido()) {
            this.predioNuevo.generarComponentesAPartirDeNumeroPredial();
        }

        this.setupDefaultDataPredioNuevo();

        //D: para que la condición de propiedad no salga predeterminada en 0
        if (this.getTramite().isQuinta()) {
            this.predioNuevo.setCondicionPropiedad(null);
        }

    }
    
    public void limpiarSector(){
        this.sector = "";
        this.combosDeptosMunis.onChangeZonaListener();
       
    }
    
    

    //end of class
}
