package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.AdministracionDocumentacionAvaluoComercialMB;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.DiligenciamientoSolicitudInformacionTipoTramiteMB;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.AdministracionInformacionSolicitanteMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso CU-SA-AC-151 Consultar información de una solicitud
 *
 * @cu CU-SA-AC-151
 *
 * @author felipe.cadena
 */
@Component("consultaInformacionSolicitud")
@Scope("session")
public class ConsultaInformacionSolicitudMB extends SNCManagedBean implements Serializable {

    /**
     * serial generado
     */
    private static final long serialVersionUID = 4357839476367666644L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaInformacionSolicitudMB.class);
    /**
     * Solicitud asociada a la consulta
     */
    private Solicitud solicitud;
    /**
     * Avaluo relacionado al sec_radicado
     */
    private Avaluo avaluo;
    /**
     * Tramite relacionado al avaluo
     */
    private Tramite tramite;
    /**
     * Número de radicación asociado a la solicitud
     */
    private String numeroRadicacion;
    /**
     * Número de radicación asociado a la solicitud
     */
    private String numeroSecRadicado;
    /**
     * Predio seleccionado
     */
    private Predio predioSeleccionado;
    /**
     * Predio seleccionado
     */
    private String numeroPredio;
    /*
     * Listas de solicitantesSolicitud(Solicitantes y Contactos de Predio) asociada a la solicitud.
     * Usada en MB para CU-SA-AC-121, CU-SA-AC-109
     */
    /**
     * Lista que trae tanto solicitantes como contactos de predio
     */
    private List<SolicitanteSolicitud> listaSolicitantesSolicitud;
    /**
     * Lista que almacena los solicitantes asociados a la solicitud
     */
    private List<SolicitanteSolicitud> listaSolicitantes;
    /**
     * Lista que almacena los contactos de predios asociados a la solicitud
     */
    private List<SolicitanteSolicitud> listaContactosPredio;
    /**
     * Lista de avaluos trelacionados a la solicitud
     */
    private List<Avaluo> listaAvaluos;
    /**
     * Solicitante asociado a la solicitud
     */
    private SolicitanteSolicitud solicitante;
    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;
    /**
     * Bandera para determinar si se muestra informacion para todo la solicitud, o para un
     * sec_radicado particular.
     */
    private boolean filtroSecRadicado;
    /**
     * Lista de predios asociados a la solicitud. Usada en MB para CU-SA-AC-046
     */
    private List<SolicitudPredio> listaPredios;
    private List<SolicitudDocumentacion> listaDocumentosExistentes;

    /* ---------------------- Managed Beans de casos de usos asociados -------------------------------------------- */
    /**
     * MB para informacion de solicitantes
     */
    @Autowired
    private AdministracionInformacionSolicitanteMB adminInformacionSolicitanteMB;
    /**
     * MB para CU-SA-AC-002
     */
    @Autowired
    private AdministracionDocumentacionAvaluoComercialMB administracionDocumentacionTramiteAvaluoComercialMB;
    /**
     * MB para CU-SA-AC-136
     */
    @Autowired
    private DiligenciamientoSolicitudInformacionTipoTramiteMB diligenciamientoInfoTipoTramiteMB;

    // --------getters-setters----------------
    public String getNumeroPredio() {
        return this.numeroPredio;
    }

    public void setNumeroPredio(String numeroPredio) {
        this.numeroPredio = numeroPredio;
    }

    public Predio getPredioSeleccionado() {
        return this.predioSeleccionado;
    }

    public void setPredioSeleccionado(Predio predioSeleccionado) {
        this.predioSeleccionado = predioSeleccionado;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<Avaluo> getListaAvaluos() {
        return this.listaAvaluos;
    }

    public void setListaAvaluos(List<Avaluo> listaAvaluos) {
        this.listaAvaluos = listaAvaluos;
    }

    public Tramite getTramite() {
        return this.tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getNumeroSecRadicado() {
        return this.numeroSecRadicado;
    }

    public void setNumeroSecRadicado(String numeroSecRadicado) {
        this.numeroSecRadicado = numeroSecRadicado;
    }

    public boolean isFiltroSecRadicado() {
        return this.filtroSecRadicado;
    }

    public void setFiltroSecRadicado(boolean filtroSecRadicado) {
        this.filtroSecRadicado = filtroSecRadicado;
    }

    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public void setAvaluo(Avaluo avaluo) {
        this.avaluo = avaluo;
    }

    public String getNumeroRadicacion() {
        return this.numeroRadicacion;
    }

    public void setNumeroRadicacion(String numeroRadicacion) {
        this.numeroRadicacion = numeroRadicacion;
    }

    public List<SolicitanteSolicitud> getListaSolicitantesSolicitud() {
        return this.listaSolicitantesSolicitud;
    }

    public void setListaSolicitantesSolicitud(List<SolicitanteSolicitud> listaSolicitantesSolicitud) {
        this.listaSolicitantesSolicitud = listaSolicitantesSolicitud;
    }

    public List<SolicitanteSolicitud> getListaSolicitantes() {
        return this.listaSolicitantes;
    }

    public void setListaSolicitantes(List<SolicitanteSolicitud> listaSolicitantes) {
        this.listaSolicitantes = listaSolicitantes;
    }

    public List<SolicitanteSolicitud> getListaContactosPredio() {
        return this.listaContactosPredio;
    }

    public void setListaContactosPredio(List<SolicitanteSolicitud> listaContactosPredio) {
        this.listaContactosPredio = listaContactosPredio;
    }

    public SolicitanteSolicitud getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public List<SolicitudPredio> getListaPredios() {
        return this.listaPredios;
    }

    public void setListaPredios(List<SolicitudPredio> listaPredios) {
        this.listaPredios = listaPredios;
    }

    public List<SolicitudDocumentacion> getListaDocumentosExistentes() {
        return this.listaDocumentosExistentes;
    }

    public void setListaDocumentosExistentes(List<SolicitudDocumentacion> listaDocumentosExistentes) {
        this.listaDocumentosExistentes = listaDocumentosExistentes;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on getListaDocumentosExistentes init");
        this.iniciarVariables();

    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {
        //this.numeroRadicacion = "60402012ER00041214";
        //this.numeroSecRadicado = "60402012ER00041214-0";
        this.usuario = MenuMB.getMenu().getUsuarioDto();

    }

    /**
     * Método para iniciar la consulta de la solictud,se puede llamar desde otras clases para cargar
     * las consultas de una solicitud, pero se debe asignar un número de solicitud en el atributo
     * this.numeroRadicacion
     *
     * @author felipe.cadena
     *
     */
    public void consultarSolicitud() {
        LOGGER.debug("Consulta solicitud");
        this.filtroSecRadicado = false;
        this.cargarInfoConsulta();
    }

    /**
     * Método para iniciar consulta del sec_radicado, se puede llamar desde otras clases para cargar
     * las consultas del sec radicado, pero se debe asignar un número de sec radicado en el atributo
     * this.numeroSecRadicado
     *
     *
     * @author felipe.cadena
     */
    public void consultarSecRadicado() {
        LOGGER.debug("Consulta secRadicado");
        this.filtroSecRadicado = true;
        LOGGER.debug("Consulta secRadicado " + this.numeroSecRadicado);
        this.cargarInfoConsulta();

    }

    /**
     * Método para cargar la información relacionada a la solicitud o al sec_radicado.
     *
     * @author felipe.cadena
     *
     */
    public void cargarInfoConsulta() {

        //datos de prueba
        //fin datos de prueba
        if (this.filtroSecRadicado) {
            this.avaluo = this.getAvaluosService().
                buscarAvaluoPorSecRadicado(this.numeroSecRadicado);
            LOGGER.debug("Avaluo " + this.avaluo.getId());
            this.tramite = this.avaluo.getTramite();
            this.solicitud = this.tramite.getSolicitud();

            LOGGER.debug("Tramite " + this.tramite.getId());
        } else {
            LOGGER.debug("Solicitud >> " + this.numeroRadicacion);
            this.solicitud = this.getAvaluosService().
                buscarSolicitudPorNumero(this.numeroRadicacion);

        }

        // this.solicitante = this.getTramiteService().buscar
        this.cargarInfoSolicitantes();
        this.cargarInfoTramite();
        this.cargarInfoPredios();
        this.cargarInfoSecRadicados();
        this.cargarInfoDocumentos();
        this.cargarInfoPago();

    }

    /**
     * Método para cargar la información del tramite
     *
     * @author felipe.cadena
     */
    public void cargarInfoTramite() {
        LOGGER.debug("cargando info tramite...");

        this.diligenciamientoInfoTipoTramiteMB.cargarVariablesExternas(
            this.solicitud, this.solicitante,
            true);

        LOGGER.debug("Información cargada");
    }

    /**
     * Método para cargar la información de los solicitantes asociados a la solicitud
     *
     * @author felipe.cadena
     */
    public void cargarInfoSolicitantes() {
        LOGGER.debug("cargando info solicitantes...");

        this.listaSolicitantes = new ArrayList<SolicitanteSolicitud>();

        // Busca la lista de solicitantes asociados a la solicitud
        this.listaSolicitantesSolicitud = this.getTramiteService().obtenerSolicitantesSolicitud2(
            this.solicitud.getId());
        // Se extraen los contactos de los predios asociados a la solicitud si
        // existen
        for (SolicitanteSolicitud solSol : this.listaSolicitantesSolicitud) {
            if (!solSol.getRelacion().equals(ESolicitanteSolicitudRelac.CONTACTO.toString())) {
                this.listaSolicitantes.add(solSol);
            }
            if (solSol.getRelacion().equals(ESolicitanteSolicitudRelac.PROPIETARIO.toString())) {
                this.solicitante = solSol;
            }
        }

        this.adminInformacionSolicitanteMB.cargarDesdeOtrosMBConSolicitanteSolicituds(
            this.listaSolicitantes);

        LOGGER.debug("Información cargada");
    }

    /**
     * Método para cargar la información de los predios asociados a la solicitud o al sec_radicado
     *
     * @author felipe.cadena
     */
    public void cargarInfoPredios() {
        LOGGER.debug("cargando info predios...");

        List<SolicitudPredio> listaInicialPredios = this.getTramiteService().
            buscarSolicitudesPredioBySolicitudId(this.solicitud.getId());

        this.listaPredios = new ArrayList<SolicitudPredio>();
        if (this.filtroSecRadicado) {
            List<AvaluoPredio> prediosAvaluo = this.getAvaluosService().
                buscarAvaluoPredioPorIdAvaluo(this.avaluo.getId());
//TODO :: felipe.cadena::modificar para incluir los predios que no estan relacionados a la solicitud y si al avaluo
            LOGGER.debug("Predios Lista " + prediosAvaluo.size());
            for (SolicitudPredio predio : listaInicialPredios) {
                if (predio.getAvaluoPredioId() != null) {
                    for (AvaluoPredio avaluoPredio : prediosAvaluo) {
                        if (predio.getAvaluoPredioId().equals(avaluoPredio.getId())) {
                            this.listaPredios.add(predio);
                        }
                    }
                }
            }

            LOGGER.debug("Lista predios sec_radicado " + this.listaPredios.size());
        } else {

            this.listaPredios = listaInicialPredios;
            LOGGER.debug("Lista predios solicitud " + this.listaPredios.size());
        }
        LOGGER.debug("Información cargada");
    }

    /**
     * Método para cargar los sec_radicados relacionados a la solicitud
     *
     * @author felipe.cadena
     */
    public void cargarInfoSecRadicados() {
        LOGGER.debug("cargando info secRadicado...");

        if (filtroSecRadicado) {
            this.listaAvaluos = new ArrayList<Avaluo>();
            this.listaAvaluos.add(avaluo);
        } else {
            this.listaAvaluos = this.getAvaluosService().consultarAvaluosPorSolicitudId(solicitud.
                getId());
        }

        LOGGER.debug("Información cargada");
    }

    /**
     * Método para cargar los documentos relacionados a la solicitud
     *
     * @author felipe.cadena
     */
    public void cargarInfoDocumentos() {
        LOGGER.debug("cargando info documentos...");

        if (this.filtroSecRadicado) {
            this.listaDocumentosExistentes = this.getAvaluosService().
                obtenerSolicitudDocumentacionPorSecRadicadoAgrupadas(this.solicitud.getId(),
                    this.tramite.getId());

        } else {
            this.listaDocumentosExistentes = this.getAvaluosService().
                obtenerSolicitudDocumentacionPorIdSolicitudAgrupadas(this.solicitud.getId());

        }

        this.administracionDocumentacionTramiteAvaluoComercialMB.cargarVariablesExternas(solicitud,
            true, solicitante, usuario, listaDocumentosExistentes);

        LOGGER.debug("Información cargada");
    }

    /**
     * Método para cargar la información del pago
     *
     * @author felipe.cadena
     */
    public void cargarInfoPago() {
        LOGGER.debug("cargando info pago...");

        LOGGER.debug("Información cargada");
    }

    /**
     * Método para cargar la información del predio a consultar
     *
     * @author felipe.cadena
     *
     */
    public void inicializarConsultaPredioMB() {
        LOGGER.debug("cargando info predio...");

        this.predioSeleccionado = this.getConservacionService().
            buscarPredioPorNumeroPredialParaAvaluoComercial(this.numeroPredio);

        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        mb001.cleanAllMB();
        mb001.setSelectedPredio1(this.predioSeleccionado);
        //LOGGER.debug("predio " + predioSeleccionado.getBarrio() + " " + predioSeleccionado.getMunicipio());

//REVIEW :: felipe.cadena :: esto debió preguntarlo antes del llamado a setSelectedPredio1. Al llamar a ese método este llamado se hace innecesario :: pedro.garcia
        if (this.predioSeleccionado != null) {
            mb001.setSelectedPredioId(this.predioSeleccionado.getId());
        }

        LOGGER.debug("Información cargada");
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando ConsultaInformacionSolicitudMB#cerrar");

        UtilidadesWeb.removerManagedBean("consultaInformacionSolicitud");
        UtilidadesWeb.removerManagedBean("diligenciamientoInfoTipoTramite");
        UtilidadesWeb.removerManagedBean("administracionDocumentacionAvaluoComercial");
        UtilidadesWeb.removerManagedBean("adminInfoSolicitante");
        UtilidadesWeb.removerManagedBean("general");

        LOGGER.debug("finalizando ConsultaInformacionSolicitudMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
