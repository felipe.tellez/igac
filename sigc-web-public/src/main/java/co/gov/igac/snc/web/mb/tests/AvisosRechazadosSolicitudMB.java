package co.gov.igac.snc.web.mb.tests;

import co.gov.igac.snc.persistence.entity.tramite.AvisoRegistroRechazo;
import co.gov.igac.snc.web.util.SNCManagedBean;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB temporal para probar la parte de mostrar avisos rechazados
 *
 * @author pagm
 *
 * OJO: para que funcione ahora habría que cambiar los nombres de las variables por los que se usan
 * en tablaTramitesRechazadosAvisoSolicitud.xhtml, y en la página que incluya esa tabla usar un
 * f:param co el nombre de este bean
 */
@Component("avisosRechazadosSolicitud")
@Scope("session")
public class AvisosRechazadosSolicitudMB extends SNCManagedBean {

    private List<AvisoRegistroRechazo> rechazados;

    /**
     * Folio rechazado que tiene un link para mostrar el detalle de todos los predios asociados
     */
    private AvisoRegistroRechazo selectedAvisoRechazado;

    @PostConstruct
    public void init() {

        String numeroSolicitud = "769457567";
        this.rechazados = this.getTramiteService().
            obtenerInfoAvisosRechazadosPorSolicitud(numeroSolicitud);

    }
//--------------------------------------------------------------------------------------------------    

    public AvisoRegistroRechazo getSelectedAvisoRechazado() {
        return this.selectedAvisoRechazado;
    }

    public void setSelectedAvisoRechazado(
        AvisoRegistroRechazo selectedAvisoRechazado) {
        this.selectedAvisoRechazado = selectedAvisoRechazado;
    }

    public List<AvisoRegistroRechazo> getRechazados() {
        return this.rechazados;
    }

    public void setRechazados(List<AvisoRegistroRechazo> rechazados) {
        this.rechazados = rechazados;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del commandLink que se pinta cuando hay varios predios de un aviso rechazado
     */
    public void mostrarMultiplesPrediosAvisoRechazado() {

    }
}
