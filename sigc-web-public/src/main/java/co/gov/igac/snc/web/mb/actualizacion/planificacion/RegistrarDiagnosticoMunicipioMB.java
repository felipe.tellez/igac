package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IActualizacion;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.persistence.entity.vistas.VJurisdiccionProducto;
import co.gov.igac.snc.persistence.util.EActualizacionDocumentacionZona;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * SNCManagedBean para registrar el diagnóstico de un municipio en actualización.
 *
 * @author jamir.avila.
 */
@Component("registrarDiagnosticoMunicipio")
@Scope("session")
public class RegistrarDiagnosticoMunicipioMB extends SNCManagedBean implements Serializable {

    /**
     * Identificador de versión por omisión
     */
    private static final long serialVersionUID = 1L;
    /**
     * Servicio de bitácora
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistrarDiagnosticoMunicipioMB.class);

    /** Código del departamento */
    private String departamentoCodigo;
    /** Código del municipio */
    private String municipioCodigo;
    /** Vigencia de la actualización */
    private java.util.Date vigencia;
    /** Identificador del responsable del diagnóstico */
    private String responsableDiagnosticoId;

    /** Orden para la lista de departamentos */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    /** Orden para la lista de municipios */
    private EOrden ordenMunicipios = EOrden.CODIGO;

    /** Departamentos de Colombia */
    List<SelectItem> departamentosColombia;
    /** Muncipios pertenecientes al departamento seleccionado */
    List<SelectItem> municipiosColombia;
    /** Responsables de diagnóstico por Territorial (Departamento/Municipio) */
    private List<SelectItem> responsablesDiagnostico;

    /** zona para la cual se va a realizar la actualizacion */
    private String zonaActualizacion;
    private List<SelectItem> zonasAct;
    /**
     * /**
     * Usuario autenticado en el sistema.
     */
    private UsuarioDTO usuarioDTO;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /** id de la actualización creada */
    private Long actualizacionId;

    /** Banderas para el ordenamiento de combos */
    private boolean departamentosOrderedByNombre;
    private boolean municipiosOrderedByNombre;

    /* ----------------------------------------------------------------- */
    /**
     * Constructor por omisión.
     */
    public RegistrarDiagnosticoMunicipioMB() {
        LOGGER.debug("Inicio del constructor por omisión");
        vigencia = new java.util.Date();

        usuarioDTO = MenuMB.getMenu().getUsuarioDto();

        LOGGER.debug("fin del constructor por omisión");
    }

    @PostConstruct
    public void init() {
        cargarZonasDeActualizacion();
        cargarDepartamentosColombia();
        cargarMunicipiosDepartamento();
        cargarResponsablesDiagnostico();
    }

    /**
     * Carga el selectItem de las zonas a actualizar
     *
     * @author franz.gamba
     */
    public void cargarZonasDeActualizacion() {
        this.zonasAct = new ArrayList<SelectItem>();
        this.zonasAct.add(new SelectItem(null, "Seleccionar..."));
        for (EActualizacionDocumentacionZona actZona : EActualizacionDocumentacionZona.values()) {
            this.zonasAct.add(new SelectItem(actZona.getDescripcion(), actZona.getDescripcion()));
        }
    }

    /**
     * LLeva a cabo el registro del diagnóstico del municipio según lo definido en el caso de uso
     * CU-NP-FA-101.
     *
     * @author jamir.avila
     */
    public void cmdOrdenarRegistroDiagnosticoMunicipio() {
        LOGGER.debug("inicio de registro de diagnóstico de municipio.");

        LOGGER.debug("Departamento: " + departamentoCodigo + ", Municipio: " + municipioCodigo);
        LOGGER.debug("Vigencia: " + vigencia);
        LOGGER.debug("Responsable del diagnóstico: " + responsableDiagnosticoId);

        try {
            String responsableDiagnosticoNombre = null;
            if (null != responsableDiagnosticoId && responsablesDiagnostico.size() > 0) {
                for (SelectItem selectItem : responsablesDiagnostico) {
                    if (selectItem.getValue() == null) {
                        continue;
                    }
                    if (selectItem.getValue().equals(responsableDiagnosticoId)) {
                        responsableDiagnosticoNombre = selectItem.getLabel();
                        break;
                    }
                }
            }

            if (null == responsableDiagnosticoNombre) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Registrar diagnóstico",
                    "Imposible obtener el nombre del responsable"));
                LOGGER.error("Imposible obtener el nombre del responsable");
                //return null;
            }

            this.actualizacionId = this.getActualizacionService().
                ordenarRegistroDiagnosticoMunicipio(
                    departamentoCodigo, zonaActualizacion, municipioCodigo,
                    vigencia, responsableDiagnosticoNombre,
                    responsableDiagnosticoId, usuarioDTO);
        } catch (ExcepcionSNC e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "Registrar diagnóstico", e.getMensaje()));
            LOGGER.error(e.getMessage(), e);
        }

        LOGGER.debug("fin de registro de diagnóstico de municipio.");

        //return "/tareas/listaTareas.jsf";
    }

    /**
     * Método para cargar los departamentos de Colombia.
     */
    public void cargarDepartamentosColombia() {
        LOGGER.debug("cargarDepartamentosColombia");

        List<Departamento> departamentos = //this.getGeneralesService().getCacheDepartamentosPorPais("140");
            (ArrayList<Departamento>) this
                .getGeneralesService()
                .getDepartamentoByCodigoEstructuraOrganizacional(
                    this.usuarioDTO
                        .getCodigoEstructuraOrganizacional());
        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(departamentos);
        } else {
            Collections.sort(departamentos, Departamento.getComparatorNombre());
        }

        departamentosColombia = new LinkedList<SelectItem>();
        departamentosColombia.add(new SelectItem("", "Seleccionar..."));

        for (Departamento departamento : departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                departamentosColombia.add(new SelectItem(departamento.getCodigo(), departamento.
                    getCodigo() + " - " + departamento.getNombre()));
            } else {
                departamentosColombia.add(new SelectItem(departamento.getCodigo(), departamento.
                    getNombre()));
            }
        }
    }

    /**
     * Método que maneja el cambio de un departamento y la correspondiente actualización de los
     * municipios que pertenecen al departamento.
     */
    public void onCambioDepartamento() {
        LOGGER.debug("onCambioDepartamento...");
        if (null != departamentoCodigo && !departamentoCodigo.equals("")) {
            cargarMunicipiosDepartamento();
            cargarResponsablesDiagnostico();

        } else {
            municipiosColombia = new LinkedList<SelectItem>();
        }
    }

    /**
     * Método que maneja una nueva selección de un municipio y la correspondiente actualización de
     * los responsables de diagnóstico.
     */
    public void onCambioMunicipio() {

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * action del commandlink para ordenar la lista de departamentos
     *
     * @author pedro.garcia
     */
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.cargarDepartamentosColombia();
    }
    // --------------------------------------------------------------------------------------------------

    /**
     * action del commandlink para ordenar la lista de departamentos (adaptado del de departamentos)
     *
     * @author franz.gamba
     */
    public void cambiarOrdenMunicipios() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.cargarMunicipiosDepartamento();
    }

    /**
     * Método para cargar los municipios que corresponden al departamento seleccionado.
     */
    private void cargarMunicipiosDepartamento() {
        municipiosColombia = new LinkedList<SelectItem>();
        //municipiosColombia.add(new SelectItem(null, "Seleccionar..."));

        if (null != departamentoCodigo && !departamentoCodigo.equals("")) {
            List<Municipio> municipios = this.getGeneralesService().
                getCacheMunicipiosPorDepartamento(getDepartamentoCodigo());
            if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                Collections.sort(municipios);
            } else {
                Collections.sort(municipios,
                    Municipio.getComparatorNombre());
            }
            municipiosColombia.add(new SelectItem(null, "Seleccionar..."));
            for (Municipio municipioColombia : municipios) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    municipiosColombia.add(new SelectItem(municipioColombia.getCodigo(),
                        municipioColombia.getCodigo3Digitos() + "-" + municipioColombia.getNombre()));
                } else {
                    municipiosColombia.add(new SelectItem(municipioColombia.getCodigo(),
                        municipioColombia.getNombre()));
                }
            }
        }
    }

    /**
     * Método que carga los responsables de conservación de la territorial que administra el
     * departamento - municipio seleccionados.
     */
    private void cargarResponsablesDiagnostico() {
        LOGGER.debug(
            "inicio de obtención de los responsables de diagnóstico para la correspondiente dirección territorial");

        responsablesDiagnostico = new LinkedList<SelectItem>();
        responsablesDiagnostico.add(new SelectItem(null, "Seleccionar"));

        if (null != departamentoCodigo && !departamentoCodigo.equals("") //&& null != municipioCodigo && !municipioCodigo.equals("")
            ) {
            List<UsuarioDTO> responsablesActualizacion = new LinkedList<UsuarioDTO>();
            //VJurisdiccionProducto jurisdiccionProducto = this.getActualizacionService().recuperarJurisdiccionProductoPorCodigoMunicipio(municipioCodigo);
            //if (null != jurisdiccionProducto) {
            //String nombreTerritorial = jurisdiccionProducto.getNombreTerritorial();
            responsablesActualizacion = this.getActualizacionService()
                .recuperarResponsablesDeActualizacion(
                    this.usuarioDTO.getDescripcionTerritorial());
            //}

            if (null != responsablesActualizacion && responsablesActualizacion.size() > 0) {

                for (UsuarioDTO responsableActualizacion : responsablesActualizacion) {
                    SelectItem si = new SelectItem(responsableActualizacion.getLogin(),
                        responsableActualizacion.getNombreCompleto());
                    responsablesDiagnostico.add(si);
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "Actualización",
                    "No se encontraron responsables de diagnóstico para el municipio seleccionado."));
            }
        }

        LOGGER.debug(
            "fin de obtención de los responsables de diagnóstico para la correspondiente dirección territorial");
    }

    public List<SelectItem> getMunicipiosColombia() {
        return municipiosColombia;
    }

    public List<SelectItem> getDepartamentosColombia() {
        return departamentosColombia;
    }

    public List<SelectItem> getResponsablesDiagnostico() {
        return responsablesDiagnostico;
    }

    public String getDepartamentoCodigo() {
        return departamentoCodigo;
    }

    public void setDepartamentoCodigo(String departamentoCodigo) {
        this.departamentoCodigo = departamentoCodigo;
    }

    public String getMunicipioCodigo() {
        return municipioCodigo;
    }

    public void setMunicipioCodigo(String municipioCodigo) {
        this.municipioCodigo = municipioCodigo;
    }

    public java.util.Date getVigencia() {
        return vigencia;
    }

    public void setVigencia(java.util.Date vigencia) {
        this.vigencia = vigencia;
    }

    public String getResponsableDiagnosticoId() {
        return responsableDiagnosticoId;
    }

    public void setResponsableDiagnosticoId(String responsableDiagnosticoId) {
        this.responsableDiagnosticoId = responsableDiagnosticoId;
    }

    public String getZonaActualizacion() {
        return zonaActualizacion;
    }

    public void setZonaActualizacion(String zonaActualizacion) {
        this.zonaActualizacion = zonaActualizacion;
    }

    public List<SelectItem> getZonasAct() {
        return zonasAct;
    }

    public void setZonasAct(List<SelectItem> zonasAct) {
        this.zonasAct = zonasAct;
    }

    public Long getActualizacionId() {
        return actualizacionId;
    }

    public void setActualizacionId(Long actualizacionId) {
        this.actualizacionId = actualizacionId;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    //---------------------------------------------------------------------------------------------
    /**
     * Método que cierra la pantalla
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("registrarDiagnosticoMunicipio");
        this.tareasPendientesMB.init();
        return "index";
    }
}
