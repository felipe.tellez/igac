/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumentacion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.RealizaInformeControlCalidadMB;
import co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud.AdministracionDocumentacionAvaluoComercialMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del CU-SA-AC-155 Revisar completitud de documentos
 *
 * @author felipe.cadena
 * @cu CU-SA-AC-155
 */
@Component("revisarCompletitudDocumentos")
@Scope("session")
public class RevisarCompletitudDocumentosMB extends SNCManagedBean implements Serializable {

    /**
     * Serial generado
     */
    private static final long serialVersionUID = 6277989195123675118L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(RevisarCompletitudDocumentosMB.class);
    /**
     * Solicitud relacionada a los documentos
     */
    private Solicitud solicitud;

    /**
     * Ultimo documento radicado para la solicitud
     */
    private Documento documentoDefinitivo;
    /**
     * MB relacionado al CU-002 que muestra los documentos relacionados a la solicitud
     */
    @Autowired
    private AdministracionDocumentacionAvaluoComercialMB administracionDocumentacionAvaluoComercialMB;

    /**
     * Usuario en sesion
     */
    private UsuarioDTO usuario;

    // --------getters-setters----------------
    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Documento getDocumentoDefinitivo() {
        return this.documentoDefinitivo;
    }

    public void setDocumentoDefinitivo(Documento documentoDefinitivo) {
        this.documentoDefinitivo = documentoDefinitivo;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    // -------Metodos-----------
    @PostConstruct
    public void init() {
        LOGGER.debug("on RevisarCompletitudDocumentosMB init");
        this.iniciarVariables();
    }

    /**
     * Método para inicializar variables
     *
     * @author felipe.cadena
     */
    public void iniciarVariables() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        if (this.usuario.getCodigoTerritorial() == null) {
            this.usuario.setCodigoTerritorial(
                Constantes.CODIGO_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL);
        }

        this.cargarDocumentos("60402012ER00041219");

    }

    /**
     * Método para cargar la documentación relacionada a la solicitud
     */
    public void cargarDocumentos(String numeroSolicitud) {

        LOGGER.debug("Cargando documentos ... ");
        this.solicitud = this.getAvaluosService().buscarSolicitudPorNumero(numeroSolicitud);
        List<SolicitudDocumentacion> documentosExistentes = this.getAvaluosService()
            .obtenerSolicitudDocumentacionPorIdSolicitudAgrupadas(
                this.solicitud.getId());

        this.administracionDocumentacionAvaluoComercialMB.
            cargarVariablesExternas(solicitud, true, solicitud.getSolicitanteSolicituds().get(0),
                usuario, documentosExistentes);

        LOGGER.debug("Documentos cargados ");

    }

    /**
     *
     * Método para buscar el ultimo documento radicado relacionado a la solicitud actual.
     *
     * @author felipe.cadena
     *
     */
    public void buscarRadicacionDefinitiva() {
        LOGGER.debug("Buscando radicacion ... ");
        this.documentoDefinitivo = this.getAvaluosService().
            obtenerDocumentoParaRadicacionDefinitiva(this.solicitud.getId());

        LOGGER.debug("Fin busqueda");
    }

    /**
     * Método pra integrar el CU 122 Solicitar documentos faltantes
     */
    public void solicitarDocumentosFaltantes() {

        LOGGER.debug("Solicitar documentos faltantes solicitud No: " + this.solicitud.getNumero());

//TODO :: felipe.cadena :: integrar el caso de uso 122 Solicitar documentos faltantes
        LOGGER.debug("Solicitud realizada ");
    }

    /**
     * Método para enviar el tramite al process y generar la radicación definitiva.
     */
    public void iniciarTramite() {
        LOGGER.debug("Iniciando tramite ... ");

        this.solicitud.setNumeroRadicacionDefinitiva(this.documentoDefinitivo.getNumeroRadicacion());
        this.solicitud.setFechaRadicacionDefinitiva(this.documentoDefinitivo.getFechaRadicacion());

        LOGGER.debug("Radicacion definitiva  " + this.solicitud.getNumeroRadicacionDefinitiva());

//TODO :: felipe.cadena::definir integración con process para continuar el tramite
        LOGGER.debug("Tramite iniciado ... ");
    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {

        LOGGER.debug("iniciando RevisarCompletitudDocumentosMB#cerrar");

        UtilidadesWeb.removerManagedBean("revisarCompletitudDocumentos");

        ConsultaInformacionSolicitudMB cisMB = (ConsultaInformacionSolicitudMB) UtilidadesWeb
            .getManagedBean("consultaInformacionSolicitud");
        cisMB.cerrar();

        LOGGER.debug("finalizando RevisarCompletitudDocumentosMB#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }
}
