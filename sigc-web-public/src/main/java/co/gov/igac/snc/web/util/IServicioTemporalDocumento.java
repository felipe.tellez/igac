package co.gov.igac.snc.web.util;

/**
 *
 * @author juan.agudelo
 *
 */
public interface IServicioTemporalDocumento {

    /**
     * Método que retorna el nombre temporal de almacenamiento de un documento de soporte
     *
     * Estandar de nombre de documento generado ddMMyyyy_IDTramite_tipoDocumentoId_codigoN_IDDB.ext
     * Digitos 8_10_5_3_10_.3-4 Ej fotografía fachada 22082011_0000001435_00012_FA1_0000002312.jpg
     * Ej documento de soporte 22082011_0000001436_00009_000_0000002313.pdf Ej documento de soporte
     * 23082011_0000001437_00010_000_0000002314.docx
     *
     * @author juan.agudelo
     * @param idTemporal id temporal del objeto al que se encuentra asociado el documento puede ser
     * el id de Solicitud, Trámite o Predio
     * @param tipoDocId id temporal del tipo de documento
     * @param codN codigo de identificación de unidades de construcción y solo aplica para
     * fotografías de UC
     * @return
     * @return
     */
    public String nombreInicialDocumento(String idTemporal, String tipoDocId,
        String codN, String extension);

}
