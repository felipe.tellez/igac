/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.listener;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * Clase listener para el cierre de la sesion del usuario, solo se activa cuando el cierre de sesion
 * se hace de manera explicita por el usuario
 *
 *
 * @author felipe.cadena
 */
public class SNCLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private IContextListener contexto;
    private static Log LOGGER = LogFactory.getLog(SessionCounterListener.class);
    private String logoutUrl;

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    /**
     * Este es el metodo que se ejecuta cada que un usuario sale del sistema no se ejecuta cuando el
     * cierre de sesion es por timeout, para eso se utiliza el SessionCounterListener
     *
     * @param request
     * @param response
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
        Authentication authentication) throws IOException, ServletException {

        response.sendRedirect(request.getContextPath() + this.logoutUrl);
    }

}
