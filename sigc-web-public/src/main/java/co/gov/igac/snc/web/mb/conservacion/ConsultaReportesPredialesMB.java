/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.CirculoRegistral;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.RepConfigParametroReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepControlReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucion;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionDetalle;
import co.gov.igac.snc.persistence.entity.conservacion.RepReporteEjecucionUsuario;
import co.gov.igac.snc.persistence.entity.conservacion.RepTerritorialReporte;
import co.gov.igac.snc.persistence.entity.conservacion.RepUsuarioRolReporte;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EParametroConfigReportes;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ERepReporteCategoria;
import co.gov.igac.snc.persistence.util.ERepReporteEjecucionEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.FiltroGenerarReportes;
import co.gov.igac.snc.util.NumeroPredialPartes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ESeccionReportesPrediales;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import net.sf.jasperreports.components.barbecue.BarcodeProviders;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.multipdf.Splitter;

/**
 * Clase que hace las veces de managed bean para ConsultaPredio
 * 
 * @author javier.aponte 
 * @modified by leidy.gonzalez
 */

@Component("consultaReportesPrediales")
@Scope("session")
public class ConsultaReportesPredialesMB extends SNCManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaReportesPredialesMB.class);

	/**
	 * CONSTANTES USADAS PARA OBTENER ID DE REPORTES DE ESTADO
	 */
	public static final Long  REPORTE_ESTADO_32 = 32L;
	
	public static final Long  REPORTE_ESTADO_33 = 33L;
	
	public static final Long  REPORTE_ESTADO_34 = 34L;
	
	public static final Long  REPORTE_ESTADO_35 = 35L;

	/** Interfaces de servicio */
	
    /**
     * contexto usado para las cosas de documentos
     */
	@Autowired
	private IContextListener contextoWeb;
	
	@Autowired
	private GeneralMB generalMB;

	/**
	 * Objeto usado como contenedor de los datos suministrados para la consulta
	 */
	private FiltroGenerarReportes datosConsultaPredio;
	
	/**
	 * variable que almacena los datos del usuario en sesión
	 */
	private UsuarioDTO usuario;
	
	/**
	 * Código del municipio seleccionado
	 */
	private String selectedMunicipioCodigo;
	
	private List<CirculoRegistral> circuloRegistralV;
	private ArrayList<SelectItem> circuloRegistralItemSelect;
	
	/**
	 * Lista con los reportes asociados a un usuario Finalizados
	 */
	private List<RepReporteEjecucion> reportesDeUsuario;
	
	/**
	 * Lista con los reportes asociados a un reporte generado con filtros avanzados
	 */
	private List<RepReporteEjecucionDetalle> reportesDetalle;
	
	/**
	 * Reporte seleccionado para ver el reporte
	 */
	private RepControlReporte reporteControlSeleccionado;
	
	/**
	 * Reporte a ser visualizado con las caracteristicas ingresadas por el usuario  
	 */
	private RepReporteEjecucionDetalle repReporteEjecucionDetalle;
	
	/**
	 * Reporte seleccionado para ser ejecutado y generar el reporte  
	 */
	private RepReporteEjecucion repReporteEjecucion;
	
	/**
	 * Reporte seleccionado para ver el reporte  
	 */
	private RepReporte repReporte;
	
	/**
	 * Lista con los tipos de reportes a generar 
	 */
	private List<RepReporte> tiposReportes;
	
	/**
    * Utilidad para la creación del reporte
    */
    private ReportesUtil reportsService = ReportesUtil.getInstance();
	
	/**
	 * Dto que contiene los datos del reporte predial generado
	 */
	private ReporteDTO reportePredial;
	
	private Municipio municipio;
	
	private Departamento departamento;
	
	/**
	 * Variable encargada de almacenar los reportes particionados asociados a un reporte
	 */
	private List<Object> reportesParticionados;
	
	/**
	 * Lista que contiene los reportes por categoria
	 */
	private List<SelectItem> reportePorCategoria;
	
	/**
	 * Lista que contiene los tipos de reportes por categoria
	 */
	private List<SelectItem> tipoReportePorCategoria;
	
	/**
	 * Lista que contiene los tipos de reportes por categoria
	 */
	private List<SelectItem> tipoReportePorTipo;
	
	/**
	 * Lista que contiene los tipos de predios 
	 */
	private List<SelectItem> tipoPredios;
	
	/**
	 * Lista que contiene los tipos de predios 
	 */
	private List<SelectItem> destinoPredios;
	
	/**
	 * Lista que contiene los modo de adquisicion de predios 
	 */
	private List<SelectItem> modoAdquisicionPredios;
	
	/**
	 * Lista que contiene la zona fisica de los predios 
	 */
	private List<SelectItem> zonaFisicaPredios;
	
	/**
	 * Lista que contiene la zona economica de los predios 
	 */
	private List<SelectItem> zonaEconomicaPredios;
	
	/**
	 * Lista que contiene las unidades de construccion de predios 
	 */
	private List<SelectItem> unidadConstruccionPredios;
	
	/**
	 * Lista que contiene las destino de construccion de predios 
	 */
	private List<SelectItem> tipoTituloPredio;
	
	/**
	 * Lista que contiene las tipificaciones de predios 
	 */
	private List<SelectItem> tipificacionPredios;
	
	/**
	 * Lista que contiene las tipificaciones de predios 
	 */
	private List<SelectItem> tipoIdentificacion;
	
	/**
	 * Lista que contiene las tipificaciones de predios 
	 */
	private List<SelectItem> tipoCirculoRegistral;
	
	/**
	 * listas con los valores de los combos de selección para la búsqueda
	 */

	private List<SelectItem> departamentosItemList;
	private List<SelectItem> municipiosItemList;
	
	/**
	 * Variable que almacena la territorial seleccionada.
	 */
	private String territorialSeleccionadaCodigo;
	
	 /**
     * Variable para saber si se activa cuando la matricula a seleccionar es antigua
     */
	private boolean matriculaAntigua;
	
	 /**
     * Variable para saber si se activa el circulo registral
     */
	private boolean circuloRegistralMatricula;
	
	/**
     * Variable para saber si se activa la tipificación cuando el destino de la construcción es residencial
     */
	private boolean destinoConstruccionResidencial;
	
	/**
     * Variable para saber si se activa el boton imprimir de acuerdo al usuario
     */
	private boolean imprimirUsuario;
        
        /**
         * Variable que determina si el tipo de reporte es por matricula inmobiliaria
         */
        private boolean defaultComboMatricula; 
	
	/**
	 * Id reporte a consultar(Consecutivo)
	 */
	private String idReporteConsultar;
	
	/**
	 * Id reporte a consultar(Año)
	 */
	private String idReporteConsultarAño;
	
	/**
	 * Orden para la lista de Departamentos
	 */
	private EOrden ordenDepartamentos = EOrden.CODIGO;
	private EOrden ordenDepartamentosDocumento = EOrden.CODIGO;

	/**
	 * Orden apra la lista de Municipios
	 */
	private EOrden ordenMunicipios = EOrden.CODIGO;
	private EOrden ordenMunicipiosDocumento = EOrden.CODIGO;
	
	/**
	 * indican si los combos están ordenados por nombre (si no, lo están por
	 * código)
	 */
	private boolean municipiosOrderedByNombre;
	private boolean departamentosOrderedByNombre;
	
	private List<Municipio> municipiosList;
	
	private List<Departamento> deptosList;
	
	/**
	 * valores seleccionados de los combos de departamento y municipio
	 */
	private String selectedDepartamentoCod;
	private String selectedMunicipioCod;
	
	/**
	 * filtro de datos para la consulta de predio
	 */
	private FiltroDatosConsultaPredio filtroCPredio;
	
	/**
	 * filtro de datos para la consulta de predio
	 */
	private FiltroDatosConsultaPredio filtroFinalCPredio;

	/**
	 * Lista que contiene los predios seleccionados para el bloqueo
	 */
	private List<Predio> prediosSeleccionados;
	
	private String numeroPredial;
	
	private String numeroPredialFinal;
	
	/**
	 * Variable de tipo streamed content que contiene el archivo con el .zip con la exportacion de laz manzanas
	 */
	transient private StreamedContent archivosADescargar;
	
	/**
	 * Tipo MIME del documento temporal para su visualización
	 */
	private String tipoMimeDocTemporal;
	
	/**
	 * constantePrediales
	 */
	private String constantePrediales;
	
	/**
	 * Territorial seleccionada
	 */
	private EstructuraOrganizacional territorialSeleccionada;
	
	/**
	 * UOC seleccionada
	 */
	private EstructuraOrganizacional unidadOperativaSeleccionada;
	
	/**
	 * Lista de Territoriales del usuario autenticado
	 */
	private List<SelectItem> listaTerritoriales;
	
	/**
     * determina si esta habilitada la seleccion de la territorial
     */
    private boolean banderaSeleccionTerritorial;
    
    /**
     * Codigo de la Territorial
     */
    private String codigoTerritorial;
    
    /**
     * UOC seleccionada
     */
    private String uOCseleccionada;
	
    /**
     * Lista de las UOC de la territorial
     */
    private List<EstructuraOrganizacional> uocs;
    
    /**
     * Variable item list con los valores de las UOC de la territorial
     */
    private List<SelectItem> UOCItemList;
    
    /**
     * determina si esta habilitada la seleccion de uocs
     */
    private boolean banderaSeleccionUOC;
    
    /**
     * Determina si se habilita radio button fecha corte 31 de Diciembre
     */
    private boolean habilitaFechaCorte31;
    
    /**
     * Determina si se habilita combo tipo de Identificacion
     */
    private boolean habilitaTipoIdentificacion;
    
    /**
     * Determina si el predio tiene o no autoestimacion
     */
    private String autoestimacion;
    
    /**
     * Determina si se habilita o no la opcion de Generar Reporte
     */
    private boolean banderaGenerarReporte;
    
    /**
     * Determina si se habilita o no la opciones Ver reporte
     */
    private boolean banderaVerReporte;
    
    /**
     * Determina si se habilita o no la opciones Exportar
     */
    private boolean banderaExportarReporte;
    
    /**
     * Determina si se habilita o no la opcion de Refrescar
     */
    private boolean banderaRefrescar;
    
    /**
     * Determina si se habilita o no la opciones Anio Actual
     */
    private boolean banderaAnioActual;;
    
    /**
     * Determina el consecutivo a generar en el reporte
     */
    private String numeracionReportePredial;
    
    private String rutaReporteGenerado;
    
    /**
     * Determina los reportes seleccionados por el usuario
     */
    private RepReporteEjecucion[] reportesSeleccionados;
    
    /**
     * Determina los reportes seleccionados por el usuario
     */
    private Documento documentoReporte;
    
    /**
     * Determina los atributos a visualizar en la consulta avanzada
     * dependiendo del tipo de Informe
     */
    private RepConfigParametroReporte configuracionAvanzada;
    
    /**
     * Determina los atributos a visualizar en la consulta avanzada
     * dependiendo del tipo de Informe
     */
    private List<RepConfigParametroReporte> configuracionesAvanzadas;
    
    /**
     * Determina los atributos a visualizar en la consulta avanzada
     * dependiendo del tipo de Informe
     */
    private HashMap<ESeccionReportesPrediales, Boolean> validezSeccion;
    
    /**
     * Determina los atributos a visualizar en la consulta avanzada
     * dependiendo del tipo de Informe
     */
    private boolean tabs[] = new boolean[6];
    
    /**
     * Determina el tamaño del los reportes
     * generados a visualizar en la tabla
     */
    private String tamanioReporte;
    
    /**
     * Nombre de la UOC seleccionada
     */
    private String uOCseleccionadaNombre;
    
    /**
     * Determina si un reporte ha sido generado
     * de acuerdo a los fistros ingresados
     */
    private boolean banderaReporteGenerado;
    
    /**
	 * Variable que almacena la parametrización del usuario la cual es usada
	 * para armar los menus de consulta
	 */
	private List<RepUsuarioRolReporte> parametrizacionUsuario;
	
	/**
	 * Objeto que contiene las partes del número predial inicial cuando la consulta
	 * es por rango
	 */
	private NumeroPredialPartes numeroPredialPartesInicial;
	
	/**
	 * Objeto que contiene las partes del número predial final cuando la consulta
	 * es por rango
	 */
	private NumeroPredialPartes numeroPredialPartesFinal;
	
	/**
	 * Objeto que contiene las partes del número predial
	 */
	private NumeroPredialPartes numeroPredialPartes;
	
	private String urlDocumento;
	
	private boolean banderaVisualizarReporteOComprimir;
	
	private boolean banderaFitrosAvanzados;
	
	/**
	 * Variable usada para habilitar o deshabilitar campos de consulta según el
	 * tipo de reporte seleccionado
	 */
	private boolean deshabilitarCamposConsultaBool;
	
	/**
	 * Variables para habilitar la busqueda y generación de reportes según la
	 * parametrización del usuario.
	 */
	private boolean permisoBuscarReporteBool;
	
	/**
	 * Variable para setear el Documento del reporte seleccionado para su
	 * visualización
	 */
	private String rutaPrevisualizacionReporte;
	
	/**
	 * Variables para habilitar la busqueda avanzada
	 * dependiendo del tipo de reporte seleccionado.
	 */
	private boolean banVerFiltrosAvanzados;
	
	/**
	 * Variable usada para almacenar los Usuarios asociados a los reportes.
	 */
	private RepReporteEjecucionUsuario repReporteEjecucionUsuario;
	
	/**
	 * Variable usada para habilitar la asociacion de un reporte a un usuario
	 */
	private boolean habilitaAsociarReporteUsuario;
	
	/**
	 * Variable usada para almacenar los usuarios asociados a un reporte
	 */
	private List<RepReporteEjecucionUsuario> reportesEjecucionUsuarios;
	
	/**
	 * Variable que almacena el reporte seleccionado a visualizar
	 */
	private RepReporteEjecucion reporteVisualizacionSeleccionado;
    
    /**
     * Variable item list con los valores de los años en los que se puede generar el reporte
     */
    private List<SelectItem> anoReporteItemList;
    
    /**
     * Variable que contiene el Dominio en BD de los años en los que se puede generar el reporte
     */
    List<Dominio> dominiosAnoReporte;
    
    /**
     * Variable usada para habilitar campos dependiendo el tipo de reporte
     */
    private boolean habilitaCampos;
    
	//----------Init--------------------------
	//--------------------------------------------------------------------------------------------------
	@PostConstruct
	public void init() {

		LOGGER.debug("entra al init de ConsultaReportesPredialesMB");
		
		this.numeroPredialPartes = new NumeroPredialPartes();
		this.numeroPredialPartesInicial = new NumeroPredialPartes();
		this.numeroPredialPartesFinal = new NumeroPredialPartes();
		
		this.documentoReporte = new Documento();
		
		this.reportesSeleccionados= new RepReporteEjecucion[]{};
		
		this.constantePrediales = Constantes.CODIGO_REPORTES_PREDIALES;
		
		this.banderaVisualizarReporteOComprimir = false;
		
		this.banderaFitrosAvanzados = false;
		
		this.banderaVerReporte = false;
		
		this.banderaExportarReporte = false;
		
		this.banderaReporteGenerado = false;
		
		this.habilitaTipoIdentificacion = false;
		
		this.banderaAnioActual= false;
		
		this.banVerFiltrosAvanzados = false;
		
		this.habilitaAsociarReporteUsuario = false;

		this.filtroCPredio = new FiltroDatosConsultaPredio();

		this.filtroFinalCPredio = new FiltroDatosConsultaPredio();

		this.tipoPredios = new ArrayList<SelectItem>();

		this.modoAdquisicionPredios = new ArrayList<SelectItem>();

		this.destinoPredios = new ArrayList<SelectItem>();

		this.zonaFisicaPredios = new ArrayList<SelectItem>();

		this.zonaEconomicaPredios = new ArrayList<SelectItem>();

		this.unidadConstruccionPredios = new ArrayList<SelectItem>();

		this.tipoTituloPredio = new ArrayList<SelectItem>();

		this.tipificacionPredios = new ArrayList<SelectItem>();

		this.tipoIdentificacion = new ArrayList<SelectItem>();

		this.tipoCirculoRegistral = new ArrayList<SelectItem>();

		this.tipoReportePorTipo = new ArrayList<SelectItem>();
		
		this.tipoReportePorCategoria = new ArrayList<SelectItem>();
		this.tipoReportePorCategoria.add(new SelectItem("", this.DEFAULT_COMBOS));

		this.reportePorCategoria = new ArrayList<SelectItem>();

		this.usuario = MenuMB.getMenu().getUsuarioDto();

		this.datosConsultaPredio = new FiltroGenerarReportes();

		this.repReporte = new RepReporte();

		this.repReporteEjecucion = new RepReporteEjecucion();

		this.repReporteEjecucionDetalle = new RepReporteEjecucionDetalle();
		
		this.repReporte = new RepReporte();

		this.repReporteEjecucion = new RepReporteEjecucion();
		
		this.repReporteEjecucionUsuario = new RepReporteEjecucionUsuario();
		
		this.reporteVisualizacionSeleccionado = new RepReporteEjecucion();
		
		this.reportesDeUsuario = new  ArrayList<RepReporteEjecucion>();

		this.ordenDepartamentos = EOrden.NOMBRE;
		this.departamentosOrderedByNombre = true;
		this.ordenMunicipios = EOrden.NOMBRE;
		this.municipiosOrderedByNombre = true;

		this.circuloRegistralV = new ArrayList<CirculoRegistral>();
		this.circuloRegistralItemSelect = new ArrayList<SelectItem>();
        
        this.anoReporteItemList = new ArrayList<SelectItem>();
        
        this.dominiosAnoReporte = new ArrayList<Dominio>();
		
		this.habilitaFechaCorte31 = false;

		// Consulta de parametrización del usuario
		this.parametrizacionUsuario = this.getConservacionService()
									.buscarListaRepUsuarioRolReportePorUsuarioYCategoria(
											this.usuario,ERepReporteCategoria.REPORTES_PREDIALES.getCategoria());

		this.listaTerritoriales = new ArrayList<SelectItem>();
		this.UOCItemList = new ArrayList<SelectItem>();
		this.departamentosItemList = new ArrayList<SelectItem>();
		this.municipiosItemList = new ArrayList<SelectItem>();
		this.municipiosList = new ArrayList<Municipio>();
		this.deptosList = new ArrayList<Departamento>();
		
		this.configuracionesAvanzadas = new ArrayList<RepConfigParametroReporte>();
		this.configuracionAvanzada = new RepConfigParametroReporte();

		// se asigna por defecto la territorial del usuario
		this.territorialSeleccionadaCodigo = this.usuario
				.getCodigoTerritorial();
        
        this.cargarAnoReporte();

		this.setSeUsanDeptosFijos(false);
		
		//Se cargan las territoriales asociadas al usuario.
		this.cargarTerritoriales();
	        
	    //Se cargan las UOCs asociadas al usuario.
	    this.cargarUOCs();

		this.circuloRegistralMatricula = true;
		this.destinoConstruccionResidencial = false;

		this.archivosADescargar = new DefaultStreamedContent();
		
		//imprimirUsuario = true;
		this.banderaGenerarReporte = false;

		for (String rolUsuario : this.usuario.getRoles()) {
			if (rolUsuario.equals("PROFESIONAL_ABOGADO")) {
				imprimirUsuario = false;
			}
		}


		this.banderaExportarReporte = false;
		this.territorialSeleccionadaCodigo = null;

		this.autoestimacion = ESiNo.NO.getCodigo();
		
	}
	
	//--------------------- Methods--------------------------------
	/**
	 * Método que realiza la inicialización de los menus en pantalla
	 * 
	 * @author leidy.gonzalez
	 */
	public void cargarTiposDeReporte(){
		
		if(this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()){
			
			for(RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario){
				
				// Tipos de reporte
				if(repUsuarioRol.getRepReporte() != null){
				
					SelectItem tipoReporte = new SelectItem(
							repUsuarioRol.getRepReporte().getId(),
							repUsuarioRol.getRepReporte().getNombre());

					if(!this.tipoReportePorCategoria.contains(tipoReporte)){
						this.tipoReportePorCategoria.add(tipoReporte);
					}
				}
				
			}
		}
	}
    
    //--------------------- Methods--------------------------------
	
	/**
	 * Retorna los valores para un combo box (selectOneMenu) con los registros
	 * del dominio AÑO_REPORTE
	 * @author leidy.gonzalez
	 * @return
	 */
	public List<SelectItem> cargarAnoReporte() {
        
        this.anoReporteItemList = new ArrayList<SelectItem>();
        
		this.dominiosAnoReporte = this.getGeneralesService()
				.getCacheDominioPorNombre(EDominio.ANIO_REPORTE);
        
        for (Dominio dominio : this.dominiosAnoReporte) {
			this.anoReporteItemList.add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
		}
		anoReporteItemList.add(0, new SelectItem(null,"Seleccionar..."));
        
		return anoReporteItemList;
	}
	
	
	/**
     * Método encargado de verificar la fecha de corte
     * en la que se genero el reporte
     * @author leidy.gonzalez
     */
	public void verificarAnoReporte(){
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String anioActual = String.valueOf(year);
		
		if(this.datosConsultaPredio.getTipoDeFechaCorte() != null 
                && !this.datosConsultaPredio.getTipoDeFechaCorte().trim().isEmpty()){
			
			 if ("2".equals(this.datosConsultaPredio.getTipoDeFechaCorte())
					|| "3".equals(this.datosConsultaPredio.getTipoDeFechaCorte())) {
                
                if (this.dominiosAnoReporte != null) {
                    
                    this.anoReporteItemList = new ArrayList<SelectItem>();

                    for (Dominio dominio : this.dominiosAnoReporte) {

                        if (dominio.getCodigo() != null
                                && !anioActual.equals(dominio.getCodigo())) {

                            this.anoReporteItemList.add(new SelectItem(dominio.getCodigo(),
                                    dominio.getValor()));
                            this.banderaAnioActual = false;
                        }
                    }
                }

				 if ("3".equals(this.datosConsultaPredio.getTipoDeFechaCorte())){
					 this.anoReporteItemList.add(new SelectItem(anioActual, anioActual));
				 }
                 anoReporteItemList.add(0, new SelectItem(null,"Seleccionar..."));
				 this.datosConsultaPredio.setAnoReporte("Seleccionar...");
            } 
            else{
				 this.anoReporteItemList = new ArrayList<SelectItem>();
				 this.anoReporteItemList.add(new SelectItem(anioActual, anioActual));
				 this.datosConsultaPredio.setAnoReporte(anioActual);
                 this.banderaAnioActual = true;
            }
			
		}
		
	}
	
	/**
     * Método encargado de verificar Si se precarga el
     * tipod e fecha de corte,de acuerdo al año seleccionado
     * @author leidy.gonzalez
     */
	public void consultarTipoFechaCorte(){
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String añoActual = String.valueOf(year);
		
		if(this.datosConsultaPredio.getAnoReporte() != null && añoActual.equals(this.datosConsultaPredio.getAnoReporte())){
			this.habilitaFechaCorte31 = true;
			
		}else{
			this.habilitaFechaCorte31 = false;
		}
		
	}
	
	/**
     * Método encargado de verificar Si se precarga el
     * tipo de identificacion,de acuerdo al tipo persona seleccionado
     * @author leidy.gonzalez
     */
	public void consultarTipoIdentificacion(){
		
		if(this.datosConsultaPredio.getTipoPersona() != null && !this.datosConsultaPredio.getTipoPersona().isEmpty()){
			this.habilitaTipoIdentificacion = true;
			
			if(EPersonaTipoPersona.JURIDICA.getCodigo().equals(this.datosConsultaPredio.getTipoPersona())){
				
				this.tipoIdentificacion = this.generalMB.getPersonaTipoIdentificacionCV();
				
				if(this.tipoIdentificacion != null && !this.tipoIdentificacion.isEmpty()){
					for (SelectItem tipoIde : this.tipoIdentificacion) {
						
						if(EPersonaTipoIdentificacion.NIT.getCodigo().equals(tipoIde.getValue())){
							
							this.tipoIdentificacion = new ArrayList<SelectItem>();
							this.tipoIdentificacion.add(tipoIde);
						}
					}
				}
				
			}else if(EPersonaTipoPersona.NATURAL.getCodigo().equals(this.datosConsultaPredio.getTipoPersona())){
				
				this.tipoIdentificacion = this.generalMB.getPersonaTipoIdentificacionNoNitCV();
			}else{
				this.tipoIdentificacion = this.generalMB.getPersonaTipoIdentificacionV();
			}
			
		}else{
			this.habilitaTipoIdentificacion = false;
		}
		
	}
	
	/**
     * Método encargado de buscar una lista de categorias de reporte
     * @author leidy.gonzalez
     */
    public void buscarReportePorCategoria(){
    	
    	if(this.datosConsultaPredio.getCodigoTipoReporte()!= null){
    		this.tipoReportePorTipo = this.generalMB.getTipoReporte(this.datosConsultaPredio.getCodigoTipoReporte());
    		
    		this.cambioTipoReporte();
    		
    		if(this.datosConsultaPredio.getCodigoTipoReporte() != null
                    && !this.datosConsultaPredio.getCodigoTipoReporte().isEmpty()
                    && this.datosConsultaPredio.getCodigoTipoReporte().equals("LISTADO DE PREDIOS DEL ESTADO")){
    			
    			//PARA ESTE TIPO DE REPORTE SE VISUALIZAN SOLAMENTE LOS 
    			//Tipo predio: Nacional, Municipal, Departamental, Baldíos y Ejidos  
    			this.tipoPredios = this.generalMB.getPredioTipoEstado();
    		}else{
                
                this.tipoPredios = this.generalMB.getPredioTipoCV();
            }
    	}
        
        if (this.datosConsultaPredio.getCodigoTipoReporte() != null) {
            if (this.datosConsultaPredio.getCodigoTipoReporte().equals("LISTADO DE PREDIOS QUE SON MEJORAS")) {
                habilitaCampos = false;
            } else {
                habilitaCampos = true;
            }
         
            if (this.datosConsultaPredio.getCodigoTipoReporte().equals("LISTADO DE PREDIOS POR MATRICULA INMOBILIARIA")) {
                this.defaultComboMatricula = true;
            } else {
                this.defaultComboMatricula = false;
            }
        }
    }
    
    /**
     * Método encargado de buscar una lista de tipos de predios
     * @author leidy.gonzalez
     */
	public void buscarTipoPredios() {
		for (String tipoPredio : this.datosConsultaPredio.getTipoPredioId()) {
			if (tipoPredio != null) {
				this.tipoPredios = this.generalMB.getPredioTipoCV();
			}
		}
	}
    
    /**
     * Método encargado de buscar una lista de tipos de reporte por categoria
     * @author leidy.gonzalez
     */
    public void buscarTipoReportePorCategoria(){
    	
    	//D: cargar la variable que define qué tabs se muestran según el tipo de informe
		this.configuracionesAvanzadas = this.getConservacionService()
						.consultaConfiguracionReportePredial(this.datosConsultaPredio.getCodigoTipoInforme());
				
		//Inicia Valides de Secciones para Consulta Avanzada
		this.inicializarValidezSecciones();
		
		//Valida Autocomplementacion de Tipo Predio, para tipo de Informe del Estado
		if(!this.configuracionesAvanzadas.isEmpty()){
			
			this.banVerFiltrosAvanzados = true;
			
			for (RepConfigParametroReporte confAvanzada : this.configuracionesAvanzadas) {
				
				if(confAvanzada.getNombreParametro() !=null 
						&& confAvanzada.getNombreParametro().equals(EParametroConfigReportes.TIPO_PREDIO.getCodigo())
						&& confAvanzada.getHabilitado() != null && confAvanzada.getRequerido() != null 
						&& confAvanzada.getHabilitado().equals(ESiNo.SI.getCodigo())
						&& confAvanzada.getRequerido().equals(ESiNo.NO.getCodigo())
						&& (confAvanzada.getReporteId() != null 
								&& (REPORTE_ESTADO_32.equals(confAvanzada.getReporteId()) 
										|| REPORTE_ESTADO_33.equals(confAvanzada.getReporteId())
										|| REPORTE_ESTADO_34.equals(confAvanzada.getReporteId())
										|| REPORTE_ESTADO_35.equals(confAvanzada.getReporteId())))){
					
					
					List<Dominio> dominio = this.getGeneralesService().getCacheDominioPorNombre(EDominio.PREDIO_TIPO);
					
					if (dominio != null){
						//this.datosConsultaPredio.setTipoPredioId(dominio);
						//this.repReporteEjecucionDetalle.setPredioTipo(this.datosConsultaPredio.getTipoPredioId());
					}
					
				}
				
				this.configuracionAvanzada=confAvanzada;
			}
		}else{
			this.banVerFiltrosAvanzados = false;
		}
		
		// Inicialización de menús que dependen del tipo de informe
		this.listaTerritoriales.clear();
		this.UOCItemList.clear();
		this.departamentosItemList.clear();
		this.municipiosItemList.clear();

		this.cargarTerritoriales();
    	
    }
    
    /**
     * Método encargado de buscar una lista de modos de adquisicion de los predios
     * @author leidy.gonzalez
     */
    public void buscarModoAdquisionPredios(){
    	
    	this.modoAdquisicionPredios = this.generalMB.getDerechoPropiedadModoAdqCV();
    	
    }
    
    /**
     * Método encargado de buscar una lista de destinos de los predios
     * @author leidy.gonzalez
     */
    public void buscarDestinoPredios(){
    	
    	this.destinoPredios = this.generalMB.getPredioDestinoCV();
    	
    }
    
    /**
     * Método encargado de buscar una lista de zonas fisicas de los predios
     * @author leidy.gonzalez
     */
    public void buscarZonaFisicaPredios(){
    	
    	this.zonaFisicaPredios = this.generalMB.getZonaFisica();
    	
    }
    
    /**
     * Método encargado de buscar una lista de zonas economicas de los predios
     * @author leidy.gonzalez
     */
    public void buscarZonaEconomicaPredios(){
    	
    	this.zonaEconomicaPredios = this.generalMB.getZonaEconomica();
    	
    }
    
    /**
     * Método encargado de buscar una lista de unidades de construccion de los predios
     * @author leidy.gonzalez
     */
    public void buscarUnidadConstruccionPredios(){
    	
    	this.unidadConstruccionPredios = this.generalMB.getUnidadConstruccionCV();
    	
    }
    
    /**
     * Método encargado de buscar una lista de tipos de titulo de los predios
     * @author leidy.gonzalez
     */
    public void buscarTipoTitulo(){
    	
    	this.tipoTituloPredio = this.generalMB.getTipoTitulo();
    	
    }
    
    /**
     * Método encargado de buscar una lista de tipificaciones de los predios
     * @author leidy.gonzalez
     */
    public void buscarTipificacionPredios(){
    	
    	this.tipificacionPredios = this.generalMB.getUnidadConstruccionTipificacion();
    	
    }
    
    /**
     * Método encargado de buscar una lista de tipo de identificacion
     * del propietarios
     * @author leidy.gonzalez
     */
    public void buscarTipoIdentificacion(){
    	
    	this.tipoIdentificacion = this.generalMB.getPersonaTipoIdentificacionV();
    	
    }
    
    /**
     * Método encargado de buscar una lista de tipo de identificacion
     * del propietarios
     * @author leidy.gonzalez
     */
    
    public void buscarCirculoRegistral(){
        
        this.tipoCirculoRegistral = new ArrayList<SelectItem>();
        List<CirculoRegistral> cr = new ArrayList<CirculoRegistral>();
        
        if(this.getCombosDeptosMunisMB().getSelectedMunicipioCod() != null 
                && !this.getCombosDeptosMunisMB().getSelectedMunicipioCod().isEmpty()){
            
            if (!this.defaultComboMatricula) {
                this.tipoCirculoRegistral.add(new SelectItem(DEFAULT_COMBOS));
            }
            
            //Obtener Circulos registrales por Municipio
            cr = this.getGeneralesService().
                    getCacheCirculosRegistralesPorMunicipio
                    (this.getCombosDeptosMunisMB().getSelectedMunicipioCod());
		
            if(!cr.isEmpty()){
                for (CirculoRegistral crTemp : cr) {
                    this.tipoCirculoRegistral.
                            add(new SelectItem(crTemp.getCodigo(),
                            crTemp.getCodigo() +  " -\r"+crTemp.getNombre()));
                }
            }
            
         }else if(this.selectedDepartamentoCod != null && !this.selectedDepartamentoCod.isEmpty()){
             
            if (!this.defaultComboMatricula) {
                this.tipoCirculoRegistral.add(new SelectItem(DEFAULT_COMBOS));
            }
             
             //Obtener Municipios por Departamento
             cr = this.getGeneralesService().
                     getCacheCirculosRegistralesPorDepartamento(this.selectedDepartamentoCod);
             
             if(!cr.isEmpty()){
                for (CirculoRegistral crTemp : cr) {
                    this.tipoCirculoRegistral.add(new SelectItem(crTemp.getCodigo(),
                            crTemp.getCodigo() +  " -\r"+crTemp.getNombre()));
                }
             }  
        }
    }
    
    /**
 	 * Método que se ejecuta cuando se selecciona o se desselecciona
 	 * un valor de la matricula inmobiliaria antigua
 	 * @author leidy.gonzalez
 	 */
 	public void seleccionMatriculaAntigua(){
 		if(this.matriculaAntigua){
 			this.matriculaAntigua = false;
 			this.circuloRegistralMatricula = true;
 		}else{
 	 		this.circuloRegistralMatricula = false;	
 		}
 	}
 	
 	/**
     * Metodo para consultar el reporte
     * @author leidy.gonzalez
     * 
     */
	public void consultarReporte() {
            
            int contadorRegistrosMultiples = 0;
            
		//Consulta Reporte por Codigo Generado
		if(this.idReporteConsultar != null && !this.idReporteConsultar.isEmpty() 
				&& this.idReporteConsultarAño != null && !this.idReporteConsultarAño.isEmpty()){
			
			
			this.numeracionReportePredial = this.constantePrediales+"-"+this.idReporteConsultar+"-"+this.idReporteConsultarAño;
			
			this.reportesDeUsuario = this.getConservacionService().buscarReportesPorCodigo(this.numeracionReportePredial);
            
            //Se revisan los reportes permisos que tiene el usuario para consultar o generar Reporte
            this.revisarPermisoGenerarReporte();
			
			if(this.reportesDeUsuario == null || this.reportesDeUsuario.isEmpty()){
				this.addMensajeWarn("No se encontraron reportes que cumplan con estos filtros de búsqueda.");
                                this.banderaExportarReporte = false;
				return;
			}else{
				
				this.banderaRefrescar = true;
				
				for (RepReporteEjecucion reporteEjecucionGenerados : this.reportesDeUsuario) {
					this.buscarSubscripcionPorUsuarioIdReporte(reporteEjecucionGenerados);
				}
			}
			
		}else{
			//Recarga de Departamento Y Municipio
			this.seleccionDepartamentoYMunicipio();
			
		        //Valida Campos Obligatorios
                    if (!this.validaCamposObligatorios()) {
                        this.addErrorCallback();
                        return;
                    }
			
            this.datosConsultaPredio.setNumeroPredial(this.numeroPredial);
            this.datosConsultaPredio.setNumeroPredialFinal(this.numeroPredialFinal);
            
            List<String> tiposAdquisicion = new ArrayList<String>();

            if (datosConsultaPredio.getModoAdquisicionId() != null
                    && !datosConsultaPredio.getModoAdquisicionId().isEmpty()) {

                for (String modoAdquisicion : this.datosConsultaPredio
                        .getModoAdquisicionId()) {

                    if (this.datosConsultaPredio.getModoAdquisicionId().get(contadorRegistrosMultiples)
                            .equals("A")) {
                        modoAdquisicion = Constantes.MODO_ADQUISICION_TRADICION;
                        tiposAdquisicion.add(modoAdquisicion);
                    }

                    if (this.datosConsultaPredio.getModoAdquisicionId().get(contadorRegistrosMultiples)
                            .equals("B")) {
                        modoAdquisicion = Constantes.MODO_ADQUISICION_OCUPACION;
                        tiposAdquisicion.add(modoAdquisicion);
                    }

                    if (this.datosConsultaPredio.getModoAdquisicionId().get(contadorRegistrosMultiples)
                            .equals("C")) {
                        modoAdquisicion = Constantes.MODO_ADQUISICION_SUCESION;
                        tiposAdquisicion.add(modoAdquisicion);
                    }

                    if (this.datosConsultaPredio.getModoAdquisicionId().get(contadorRegistrosMultiples)
                            .equals("D")) {
                        modoAdquisicion = Constantes.MODO_ADQUISICION_ACCESION;
                        tiposAdquisicion.add(modoAdquisicion);
                    }

                    if (this.datosConsultaPredio.getModoAdquisicionId().get(contadorRegistrosMultiples)
                            .equals("E")) {
                        modoAdquisicion = Constantes.MODO_ADQUISICION_PRESCRIPCION;
                        tiposAdquisicion.add(modoAdquisicion);
                    }
                    contadorRegistrosMultiples++;
                }
                this.datosConsultaPredio.setModoAdquisicionId(tiposAdquisicion);
            }

			//Realiza consulta por filtros
			this.buscarReportePorFiltros();
            
            //Se revisan los reportes permisos que tiene el usuario para consultar o generar Reporte
            this.revisarPermisoGenerarReporte();
			
			if (this.reportesDeUsuario == null || this.reportesDeUsuario.isEmpty()){
				
				this.addMensajeInfo("No se encontraron  resultados para esta búsqueda.");
                                this.banderaExportarReporte = false;
				return;
			}else{
				
				
				if (this.reportesDeUsuario != null){
					for (RepReporteEjecucion reporteEjecucion : this.reportesDeUsuario) {
						
						this.tamanioReporte = reporteEjecucion.getValorTamano() + "  "+reporteEjecucion.getUnidadTamano();
						this.buscarSubscripcionPorUsuarioIdReporte(reporteEjecucion);
					}
					this.banderaRefrescar = true;
					this.banderaVerReporte = true;
				}
				
			}
		}

		this.banderaExportarReporte = false;
		
	}
	
	/**
     * Metodo para validar si el dato
     * ingresado es un numero 
     * @author leidy.gonzalez
     * 
     */
	 public static boolean isNumeric(String str) {
	        return (str.matches("[+-]?\\d*(\\.\\d+)?") && str.equals("")==false);
	 }
	
	/**
     * Metodo para validar si el predio
     * ingresado se encuentra bloqueado
     * @author leidy.gonzalez
     * 
     */
	public void validaPredioBloqueo(){
		
		if(this.numeroPredial != null && !this.numeroPredial.isEmpty()){
			
			//Verifica si El predio esta Bloqueado
			PredioBloqueo predioBloqueo = this.getConservacionService().getPredioBloqueo(this.numeroPredial);
			
			if(predioBloqueo != null){
				this.datosConsultaPredio.setPredioBloqueado(ESiNo.NO.getCodigo());
			}
		}
	}
	
	/**
     * Metodo para validar campos obligatorios
     * @author leidy.gonzalez
     * 
     */
    public boolean validaCamposObligatorios() {
        
        int numeroRegistroMin;
        int numeroRegistroMax;
        
        if (this.selectedDepartamentoCod == null || this.selectedDepartamentoCod.isEmpty()) {
            this.addMensajeError("Se debe seleccionar el departamento");
            return false;
        }

        // Valida que se seleccione el formato a consultar el reporte
        if (this.datosConsultaPredio.getFormato() == null
            || this.datosConsultaPredio.getFormato().isEmpty()) {

            this.addMensajeError("Se debe seleccionar el formato");
            return false;
        }

        // Valida que se seleccione el reporte a consultar
        if (this.datosConsultaPredio.getCodigoTipoReporte() == null) {

            this.addMensajeError("Se debe seleccionar el reporte a generar");
            return false;
        }
        // Valida que se seleccione el tipo de reporte a consultar
        if (this.datosConsultaPredio.getCodigoTipoInforme() == null) {

            this.addMensajeError("Se debe seleccionar el tipo de reporte");
            return false;
        }

        if (this.datosConsultaPredio.getAnoReporte() == null
            || this.datosConsultaPredio.getAnoReporte().isEmpty()) {

            this.addMensajeError("Se debe seleccionar el año del reporte");
            return false;
        }

        if (this.datosConsultaPredio.getTipoDeFechaCorte() == null
            || this.datosConsultaPredio.getTipoDeFechaCorte().isEmpty()) {

            this.addMensajeError("Se debe seleccionar la fecha de corte");
            return false;
        }

        //Valida Matricula Inicial y Final
        if ((this.datosConsultaPredio.getNumeroRegistro() != null
            && !this.datosConsultaPredio.getNumeroRegistro().isEmpty())
            && (this.datosConsultaPredio.getNumeroRegistroFinal() == null
            && this.datosConsultaPredio.getNumeroRegistroFinal().isEmpty())) {

            this.addMensajeError("Si ingresa la Matricula Inicial, debe ingresar la Matricula Final");
            return false;
        }
        
        //Valida Numerico de campos de rangos
        try {

            //Valida que sea un numero los datos de Rango de Area y Avaluo
            if (this.datosConsultaPredio.getAreaTerrenoMin() != null) {

                this.datosConsultaPredio.getAreaTerrenoMin().doubleValue();
            }

            if (this.datosConsultaPredio.getAreaTerrenoMax() != null) {

                this.datosConsultaPredio.getAreaTerrenoMax().doubleValue();
            }

            if (this.datosConsultaPredio.getAreaConstruidaMin() != null) {

                this.datosConsultaPredio.getAreaConstruidaMin().doubleValue();
            }

            if (this.datosConsultaPredio.getAreaConstruidaMax() != null) {

                this.datosConsultaPredio.getAreaConstruidaMax().doubleValue();
            }

            if (this.datosConsultaPredio.getAvaluoDesde() != null) {

                this.datosConsultaPredio.getAvaluoDesde().longValue();
            }

            if (this.datosConsultaPredio.getAvaluoHasta() != null) {

                this.datosConsultaPredio.getAvaluoHasta().longValue();
            }

        } catch (Exception e) {

            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("El campo ingresado debe de ser Nùmerico.");
        }

        //Valida Reportes Tipo Manzana
        if (this.datosConsultaPredio.getCodigoTipoReporte() != null && !this.datosConsultaPredio.getCodigoTipoReporte().isEmpty()
                && "LISTADO DE PREDIOS A NIVEL DE MANZANA".equals(this.datosConsultaPredio.getCodigoTipoReporte())) {

            if (this.numeroPredialPartesInicial != null) {

                if (this.numeroPredialPartesInicial.getManzanaVeredaCodigo() != null
                        && !this.numeroPredialPartesInicial.getManzanaVeredaCodigo().isEmpty()
                        && this.numeroPredialPartesFinal.getManzanaVeredaCodigo() != null
                        && !this.numeroPredialPartesFinal.getManzanaVeredaCodigo().isEmpty()) {

                    this.numeroPredial = this.numeroPredialPartesInicial.toString();
                } else {
                    this.addMensajeError("Para los tipos de Reporte: " + this.datosConsultaPredio.getCodigoTipoReporte()
                            + " se debe ingresar los nùmero prediales hasta la manzana");
                    return false;
                }
            }
        }

        //Ingresa numero predial
        if (numeroPredialPartesInicial.getUnidad() != null
                && !numeroPredialPartesInicial.getUnidad().isEmpty()) {

            this.assembleNumeroPredialFromSegments();
            this.validaPredioBloqueo();
        }

        /*@modified by hector.arias::#77543::Debido a que no se actualizaba 
        el número predial inicial y el número predial final cuando el usuario 
        lo cambiaba se carga de nuevo para garantizar la consulta del reporte 
        con los cambios en los datos de entrada del usuario.*/
        this.datosConsultaPredio.setNumeroPredial(UtilidadesWeb.completarNumeroPredialConDigito(this.numeroPredialPartesInicial, 0));
        this.datosConsultaPredio.setNumeroPredialFinal(UtilidadesWeb.completarNumeroPredialConDigito(this.numeroPredialPartesFinal, 9));
        
        //Valida datos ingresados a nivel de predio, áreas y avalúos
        if (this.datosConsultaPredio.getNumeroPredial() != null && !this.datosConsultaPredio.getNumeroPredial().isEmpty()
                && this.datosConsultaPredio.getNumeroPredialFinal() != null && !this.datosConsultaPredio.getNumeroPredialFinal().isEmpty()) {
            
            if (this.datosConsultaPredio.getNumeroPredial()
                    .compareTo(this.datosConsultaPredio.getNumeroPredialFinal()) > 0) {
                this.addMensajeWarn(ECodigoErrorVista.REPORTES_PRE_001.toString());
                return false;
            }
            
        }

        if (this.datosConsultaPredio.getAreaConstruidaMin() != null
                && this.datosConsultaPredio.getAreaConstruidaMax() != null) {
            
            if (this.datosConsultaPredio.getAreaConstruidaMin()
                    .compareTo(this.datosConsultaPredio.getAreaConstruidaMax()) > 0) {
                this.addMensajeWarn(ECodigoErrorVista.REPORTES_PRE_002.toString());
                return false;
            }
            
        }

        if (this.datosConsultaPredio.getAreaTerrenoMin() != null
                && this.datosConsultaPredio.getAreaTerrenoMax() != null) {
            
            if (this.datosConsultaPredio.getAreaTerrenoMin()
                    .compareTo(this.datosConsultaPredio.getAreaTerrenoMax()) > 0) {
                this.addMensajeWarn(ECodigoErrorVista.REPORTES_PRE_003.toString());
                return false;
            }
            
        }

        if (this.datosConsultaPredio.getAvaluoDesde() != null
                && this.datosConsultaPredio.getAvaluoHasta() != null) {
            
            if (this.datosConsultaPredio.getAvaluoDesde()
                    .compareTo(this.datosConsultaPredio.getAvaluoHasta()) > 0) {
                this.addMensajeWarn(ECodigoErrorVista.REPORTES_PRE_004.toString());
                return false;
            }
            
        }

        if (this.datosConsultaPredio.getNumeroRegistro() != null && !this.datosConsultaPredio.getNumeroRegistro().isEmpty()
                && this.datosConsultaPredio.getNumeroRegistroFinal() != null && !this.datosConsultaPredio.getNumeroRegistroFinal().isEmpty()) {
            
            numeroRegistroMin = Integer.valueOf(this.datosConsultaPredio.getNumeroRegistro());
            numeroRegistroMax = Integer.valueOf(this.datosConsultaPredio.getNumeroRegistroFinal());
            
            if (numeroRegistroMin > numeroRegistroMax) {
                this.addMensajeWarn(ECodigoErrorVista.REPORTES_PRE_005.toString());
                return false;
            }
            
        }

        
        return true;
    }
	
	/**
     * Metodo para consulta si el reporte
     * ha sido generado anteriormente
     * de acuerdo a los filtros ingresados
     * @author leidy.gonzalez
     * 
     */
	public void buscarReportePorFiltros(){
            
            int tamAuxiliar;
            final int matriculaTam = 20;
            Vector<String> valorAuxiliar;
            StringBuilder nuevoNumeroMatricula;
            StringBuilder nuevoNumeroMatriculaHasta;
            StringBuilder parteFija = new StringBuilder();
            nuevoNumeroMatricula = new StringBuilder();
            nuevoNumeroMatricula.append(parteFija);
            nuevoNumeroMatriculaHasta = new StringBuilder();
            nuevoNumeroMatriculaHasta.append(parteFija);
		
		// Valida que el reporte ya exista con los filtros enviados
		if ((this.idReporteConsultar.isEmpty() && this.idReporteConsultarAño
				.isEmpty())
				&& this.datosConsultaPredio.getCodigoTipoInforme() != null
				&& this.datosConsultaPredio.getFormato() != null
				&& this.datosConsultaPredio.getTipoDeFechaCorte() != null
				&& this.datosConsultaPredio.getAnoReporte() != null
				&& this.selectedDepartamentoCod != null
				&& (this.codigoTerritorial != null || this.uOCseleccionada != null)) {

			// Realiza Consulta Basica por Filtros
			this.datosConsultaPredio.setNumeroPredialPartesInicial(this.numeroPredialPartesInicial);
			this.datosConsultaPredio.setNumeroPredialPartesFinal(this.numeroPredialPartesFinal);
			this.datosConsultaPredio.setNumeroPredial(UtilidadesWeb.completarNumeroPredialConDigito(this.numeroPredialPartesInicial, 0));
			this.datosConsultaPredio.setNumeroPredialFinal(UtilidadesWeb.completarNumeroPredialConDigito(this.numeroPredialPartesFinal, 9));

                    // Realiza formato de la matricula inmobiliaria para BD
                    if (this.datosConsultaPredio.getNumeroRegistro() != null && !this.datosConsultaPredio.getNumeroRegistro().isEmpty()
                            || this.datosConsultaPredio.getNumeroRegistroFinal() != null && !this.datosConsultaPredio.getNumeroRegistroFinal().isEmpty()) {
                        tamAuxiliar = matriculaTam
                                - this.datosConsultaPredio.getNumeroRegistro().length();
                        valorAuxiliar = new Vector<String>();
                        if (tamAuxiliar > 0) {
                            valorAuxiliar.setSize(tamAuxiliar);
                            Collections.fill(valorAuxiliar, "0");
                            nuevoNumeroMatricula.append(StringUtils.join(
                                    valorAuxiliar, "").concat(
                                            this.datosConsultaPredio.getNumeroRegistro()));
                            this.datosConsultaPredio.setNumeroRegistro(nuevoNumeroMatricula.toString());
                        }
                        tamAuxiliar = matriculaTam
                                - this.datosConsultaPredio.getNumeroRegistroFinal().length();
                        valorAuxiliar = new Vector<String>();
                        if (tamAuxiliar > 0) {
                            valorAuxiliar.setSize(tamAuxiliar);
                            Collections.fill(valorAuxiliar, "0");
                            nuevoNumeroMatriculaHasta.append(StringUtils.join(
                                    valorAuxiliar, "").concat(
                                            this.datosConsultaPredio.getNumeroRegistroFinal()));
                            this.datosConsultaPredio.setNumeroRegistroFinal(nuevoNumeroMatriculaHasta.toString());
                        }

                    }
      

			this.reportesDeUsuario = this.getConservacionService()
						.buscarReportePorFiltrosBasicos(
								this.datosConsultaPredio,
								this.selectedDepartamentoCod,
								this.selectedMunicipioCod,
								this.codigoTerritorial, this.uOCseleccionada);


		}
		
	}
	
	/**
     * Metodo para consulta si el reporte
     * ha sido generado anteriormente
     * de acuerdo a los filtros ingresados
     * @author leidy.gonzalez
     * 
     */
	public void contarCantidadPrediosPorFiltro(){
		
		Parametro cantidadMaxPredios = this.getGeneralesService().getCacheParametroPorNombre(EParametro.MAX_CANTIDAD_PREDIOS_REPORTE_DIURNO.toString());
		int cantidadPredios = 0;
		
//		if (this.datosConsultaPredio.getAreaTerrenoMin() != null ||
//				this.datosConsultaPredio.getAreaTerrenoMax() != null ||
//				this.datosConsultaPredio.getAreaConstruidaMin() != null ||
//				this.datosConsultaPredio.getAreaConstruidaMax() != null) {
//			
//			this.repReporteEjecucion.setEjecucionHorario(Constantes.NOCTURNO);
//			this.addMensajeWarn("De acuerdo a los filtros ingresados, el reporte se genera en horario Nocturno");
//		}
		
		//fecha_corte IN ('2','3')
		if((this.numeroPredial != null && !this.numeroPredial.isEmpty()) &&
				this.numeroPredialFinal != null && !this.numeroPredialFinal.isEmpty()
				&& this.datosConsultaPredio.getTipoDeFechaCorte().equals(1)){

			//Se consulta Cantidad de Predios
			cantidadPredios = this.getConservacionService().contarPrediosPorRangoNumeroPredial(this.numeroPredial,this.numeroPredialFinal);
			
			if(cantidadPredios > cantidadMaxPredios.getValorNumero()){
				this.repReporteEjecucion.setEjecucionHorario(Constantes.NOCTURNO);
				this.addMensajeWarn("De acuerdo a los filtros ingresados, el reporte se genera en horario Nocturno");
			}
			
		}
		
		if((this.numeroPredial != null && !this.numeroPredial.isEmpty()) &&
				this.numeroPredialFinal != null && !this.numeroPredialFinal.isEmpty()
				&& (this.datosConsultaPredio.getTipoDeFechaCorte().equals(2)
						|| this.datosConsultaPredio.getTipoDeFechaCorte().equals(3))){

			//Se consulta Cantidad de Predios
			cantidadPredios = this.getConservacionService().contarPrediosPorRangoNumeroPredialHistorico(this.numeroPredial,this.numeroPredialFinal);
			
			if(cantidadPredios > cantidadMaxPredios.getValorNumero()){
				this.repReporteEjecucion.setEjecucionHorario(Constantes.NOCTURNO);
				this.addMensajeWarn("De acuerdo a los filtros ingresados, el reporte se genera en horario Nocturno");
			}
			
		}
		//Consulta por Filtros Avanzados a Tabla: Predio
		if(this.selectedMunicipioCod != null && !this.selectedMunicipioCod.isEmpty()
				&& this.banderaFitrosAvanzados){
			
			this.filtroCPredio.setMunicipioId(this.selectedMunicipioCod);
			cantidadPredios = this.getConservacionService().contarPredios(this.filtroCPredio);
			
			if(cantidadPredios > cantidadMaxPredios.getValorNumero()){
				this.repReporteEjecucion.setEjecucionHorario(Constantes.NOCTURNO);
				this.addMensajeWarn("De acuerdo a los filtros ingresados, el reporte se genera en horario Nocturno");
			}
		}
		
		this.repReporteEjecucion=this.getConservacionService().actualizarRepReporteEjecucion(
				this.repReporteEjecucion);
	}
	
	/**
     * Metodo para validar que se ingrese
     * el predio hasta la manzana, cuando
     * el tipo de reporte es por manzana
     * @author leidy.gonzalez
     * 
     */
	public void validaPredioTipoRepManzana(){
		
		if(this.datosConsultaPredio.getCodigoTipoReporte() != null && !this.datosConsultaPredio.getCodigoTipoReporte().isEmpty()
				&& "LISTADO DE PREDIOS A NIVEL DE MANZANA".equals(this.datosConsultaPredio.getCodigoTipoReporte())){
			
			if(this.numeroPredialPartesInicial != null){
				
				if(this.numeroPredialPartesInicial.getManzanaVeredaCodigo() != null
						&& !this.numeroPredialPartesInicial.getManzanaVeredaCodigo().isEmpty()){
					
					this.numeroPredial = this.numeroPredialPartesInicial.toString();
				}else{
					this.addMensajeError("Para los tipos de Reporte: "+ this.datosConsultaPredio.getCodigoTipoReporte()+
							" se debe ingresar el nùmero predial inicial hasta la manzana");
					return;
				}
			}
		}
		
	}
	
	 /**
     * Metodo para generar el reporte
     * @author leidy.gonzalez
     * 
     */
	public void verificaReporteSeGenero() {
		//Inicializa Tabla Reportes Genrados
		this.reportesDeUsuario = new ArrayList<RepReporteEjecucion>();
				
		//Valida Campos Obligatorios
		if(!this.validaCamposObligatorios()){
            this.addErrorCallback();
            return;
        }
				
		//Valida que el reporte ya haya sido generado
		this.buscarReportePorFiltros();
		
		if(this.reportesDeUsuario != null && !this.reportesDeUsuario.isEmpty()){
			this.banderaReporteGenerado = true;
		}else{
			this.generarReporte();
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
		}
	}
 	
    /**
     * Metodo para generar el reporte
     * @author leidy.gonzalez
     * 
     */
	public void generarReporte() {
		
		if(this.reportesDeUsuario == null || this.reportesDeUsuario.isEmpty()){
			this.reportesDeUsuario = new ArrayList<RepReporteEjecucion>();
		}
		//Lena Tabla REP_REPORTE_EJECUCION
		this.repReporteEjecucion= actualizarReporteEjecucion();
		
		//Lena Tabla REP_REPORTE_EJECUCION_DETALLE
		this.repReporteEjecucionDetalle= actualizarReporteEjecucionDetalle(this.repReporteEjecucion);
		
		this.contarCantidadPrediosPorFiltro();
		
			// LLAMAR AL PROCEDIMIENTO ALMACENADO
			if(this.repReporteEjecucion != null){
				
				this.getConservacionService().generarReportePredialDatosBasicos(
						this.repReporteEjecucion);
				
				//Se asocia reporte a Usuario
				this.actualizarReporteEjecucionUsuario(this.repReporteEjecucion);

				this.reportesDeUsuario.add(this.repReporteEjecucion);
				
				this.addMensajeInfo("El código de reporte asignado es:  " + this.numeracionReportePredial);
				this.banderaRefrescar = true;
			}
			
	}
	
	/**
     * Metodo para generar la numeracion del reporte
     * @author leidy.gonzalez
     * 
     */
	public void generarNumeracionReporte(){
		
		
		Object[] numeracionReportePredialObj = this.getGeneralesService().generarNumeracion(
				ENumeraciones.NUMERACION_T_A_CONS, this.codigoTerritorial, "","", 0);
		
		this.numeracionReportePredial = (String) numeracionReportePredialObj[1];
		
		String cadena_a = this.numeracionReportePredial.substring(0,13);
        //Borramos los primeros 14 caracteres.
        String cadena_b = this.numeracionReportePredial.substring(14);
        
        this.numeracionReportePredial = cadena_a+cadena_b;
	}
	
	/**
     * Metodo para limpiar los campos
     * @author leidy.gonzalez
     * 
     */
	public void limpiarCampos(){
		
		
		this.selectedDepartamentoCod = null;
		
		this.selectedMunicipioCod = null;
		
		this.datosConsultaPredio.setFormato(null);
		
		this.datosConsultaPredio.setCodigoTipoReporte(null);
    	
    	this.datosConsultaPredio.setCodigoTipoInforme(null);
    	
    	this.numeroPredial = null;
    	
    	this.numeroPredialFinal = null;
    	
    	this.idReporteConsultar = null;
    	
    	this.idReporteConsultarAño = null;
    	
    	this.datosConsultaPredio.setCirculoRegistral(null);
    	
    	this.datosConsultaPredio.setNumeroRegistro("");
    	
    	this.datosConsultaPredio.setLibro("");
    	
    	this.datosConsultaPredio.setTomo("");
    	
    	this.datosConsultaPredio.setPagina("");
    	
    	this.datosConsultaPredio.setNumero("");
    	
    	this.datosConsultaPredio.setAnoReporte("");
    	
    	this.datosConsultaPredio.setTipoDeFechaCorte("");
    	
    	this.datosConsultaPredio.setNombrePropietario("");
    	
    	this.datosConsultaPredio.setTipoIdentificacionPropietario(null);
    	
    	this.datosConsultaPredio.setIdentificacionPropietario("");
    	
    	this.datosConsultaPredio.setDigitoVerificacion("");
    	
    	this.datosConsultaPredio.setAreaTerrenoMin(null);
    	
    	this.datosConsultaPredio.setAreaTerrenoMax(null);
    	
    	this.datosConsultaPredio.setAreaConstruidaMin(null);
    	
    	this.datosConsultaPredio.setAreaConstruidaMax(null);
    	
    	this.datosConsultaPredio.setAvaluoDesde(null);
    	
    	this.datosConsultaPredio.setAvaluoHasta(null);
    	
    	this.datosConsultaPredio.setTipoPredioId(null);
    	
    	this.datosConsultaPredio.setDestinoPredioId(null);
    	
    	this.datosConsultaPredio.setModoAdquisicionId(null);
    	
    	this.datosConsultaPredio.setZonaEconomicaId(null);
    	
    	this.datosConsultaPredio.setZonaFisicaId(null);
    	
    	this.datosConsultaPredio.setUsoConstruccionId(null);
    	
    	this.datosConsultaPredio.setTipoTitulo(null);
    	
    	
	}
	
	/**
     * Método para buscar los reportes Finalizados
     *
     * @author leidy.gonzalez
     */
    public void buscarReportesFinalizados() {
        LOGGER.debug("Consultando reportes finalizados...");
        //getUrlPrevisualizacion
        if(this.repReporteEjecucion != null && this.repReporteEjecucion.getRepReporte().getUrlPrevisualizacion() != null 
				&& !this.repReporteEjecucion.getRepReporte().getUrlPrevisualizacion().trim().isEmpty()
        		&& this.repReporteEjecucion.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO
    					.getCodigo())){
        	
        	this.reportePredial = this.reportsService.consultarReporteDeGestorDocumental(this.repReporteEjecucion.getRepReporte().getUrlPrevisualizacion());
        	
        	this.rutaReporteGenerado = this.reportePredial.getUrlWebReporte();
        			
        }

        LOGGER.debug("Tramites consultados");
    }
	
	 /**
     * Metodo para actualizar los datos del reporte
     * a ejecutarse
     * @author leidy.gonzalez
     * 
     */
	public RepReporteEjecucion actualizarReporteEjecucion() {
		
		this.repReporteEjecucion = new RepReporteEjecucion();

		// Actualizar reporte Ejecutado
		this.tiposReportes = this.getGeneralesService().obtenerNombreCategoriaReporte();
		
		this.repReporteEjecucion.setEjecucionHorario(Constantes.DIURNO);
		
		this.repReporteEjecucion.setNumeroEnvio(0);
		
		if(this.selectedDepartamentoCod != null && !this.selectedDepartamentoCod.isEmpty()){
			this.repReporteEjecucion.setDepartamentoCodigo(this.selectedDepartamentoCod);
		}
		
		this.selectedMunicipioCod= this.getCombosDeptosMunisMB().getSelectedMunicipioCod();
		
		if(this.selectedMunicipioCod != null && !this.selectedMunicipioCod.isEmpty()){
			this.repReporteEjecucion.setMunicipioCodigo(this.selectedMunicipioCod);
		}
		
		if(this.departamento != null && this.departamento.getNombre() != null && !this.departamento.getNombre().isEmpty()){
			this.repReporteEjecucion.setDepartamentoNombre(this.departamento.getNombre());
		}
		
		if(this.municipio != null && this.municipio.getNombre() != null && !this.municipio.getNombre().isEmpty()){
			this.repReporteEjecucion.setMunicipioNombre(this.municipio.getNombre());
		}
		
		this.repReporteEjecucion.setFechaInicio(new Date(System
				.currentTimeMillis()));
		if (this.tiposReportes != null) {
			for (RepReporte reporte : this.tiposReportes) {
				if (reporte.getId().equals(this.datosConsultaPredio.getCodigoTipoInforme())) {
					this.repReporteEjecucion.setRepReporte(reporte);
				}
			}
		}
		
		if(this.territorialSeleccionada != null){
			this.repReporteEjecucion.setTerritorial(this.territorialSeleccionada);
			
		}
		if(this.uOCseleccionada != null){
			this.unidadOperativaSeleccionada = this.buscarTerritorial(this.uOCseleccionada);
			this.repReporteEjecucion.setUOC(this.unidadOperativaSeleccionada);
		}
		
		//Se genera Secuencial del Codigo del Reporte
		this.generarNumeracionReporte();
		
		if(this.numeracionReportePredial != null && !this.numeracionReportePredial.isEmpty()){
			this.repReporteEjecucion.setCodigoReporte(this.numeracionReportePredial);
		}
		
		if(this.datosConsultaPredio.getFormato() != null){
			this.repReporteEjecucion.setFormato(this.datosConsultaPredio.getFormato());
		}
		
		if(this.datosConsultaPredio.getTipoDeFechaCorte() != null){
			this.repReporteEjecucion.setFechaCorte(this.datosConsultaPredio.getTipoDeFechaCorte());
		}
		
		if(this.datosConsultaPredio.getAnoReporte() != null){
			this.repReporteEjecucion.setVigencia(this.datosConsultaPredio.getAnoReporte());
		}
		
		this.repReporteEjecucion.setEstado(ERepReporteEjecucionEstado.INICIADO.getCodigo());
		this.repReporteEjecucion.setUsuario(this.usuario.getLogin());
		this.repReporteEjecucion.setUsuarioLog(this.usuario.getLogin());
		this.repReporteEjecucion.setFechaLog(new Date());
		this.repReporteEjecucion.setReporteVacio("I");

		this.repReporteEjecucion=this.getConservacionService().actualizarRepReporteEjecucion(
				this.repReporteEjecucion);

		return this.repReporteEjecucion;
	}
	
	/**
     * Metodo para actualizar los datos del reporte
     * con los detalles ingresados por el usuario
     * @author leidy.gonzalez
     * 
     */
	public RepReporteEjecucionDetalle actualizarReporteEjecucionDetalle(
			RepReporteEjecucion repReporteEjecucion) {
		
		this.repReporteEjecucionDetalle = new RepReporteEjecucionDetalle();

		// Actualizar reporte Ejecucion Detalle
		
		int contadorRegistrosMultiplesAnzada = 0;
        
        this.assembleNumeroPredialFromSegments();
		
		if(this.selectedDepartamentoCod != null && !this.selectedDepartamentoCod.isEmpty()){
			this.repReporteEjecucionDetalle.setDepartamentoCodigo(this.selectedDepartamentoCod);
			
		}
		
		if(this.selectedMunicipioCod != null && !this.selectedMunicipioCod.isEmpty()){
			this.repReporteEjecucionDetalle.setMunicipioCodigo(this.selectedMunicipioCod);
		}
		
		if (this.repReporteEjecucion != null) {

			this.repReporteEjecucionDetalle
					.setRepReporteEjecucion(this.repReporteEjecucion);

			if (repReporteEjecucion.getMunicipioCodigo() != null &&!repReporteEjecucion.getMunicipioCodigo().isEmpty()) {
				this.repReporteEjecucionDetalle
						.setMunicipioCodigo(repReporteEjecucion
								.getMunicipioCodigo());
			}

			if (repReporteEjecucion.getDepartamentoCodigo() != null && !repReporteEjecucion.getDepartamentoCodigo().isEmpty()) {

				this.repReporteEjecucionDetalle
						.setDepartamentoCodigo(repReporteEjecucion
								.getDepartamentoCodigo());
			}
		 }
		
			if(this.datosConsultaPredio.getTipoDeFechaCorte() != null){
				this.repReporteEjecucionDetalle.setFechaCorte(this.datosConsultaPredio.getTipoDeFechaCorte());
			}
		
			if(this.datosConsultaPredio.getAnoReporte() != null){
				this.repReporteEjecucionDetalle.setVigencia(this.datosConsultaPredio.getAnoReporte());
			}

			if (this.numeroPredial != null && !this.numeroPredial.isEmpty()) {
				this.repReporteEjecucionDetalle
						.setNumeroPredialDesde(this.numeroPredial);
			}
			if (this.numeroPredialFinal != null
					&& !this.numeroPredialFinal.isEmpty()) {
				this.repReporteEjecucionDetalle
						.setNumeroPredialHasta(this.numeroPredialFinal);
			}
			if (this.datosConsultaPredio.getNombrePropietario() != null) {
				this.repReporteEjecucionDetalle
						.setNombrePropietario(this.datosConsultaPredio
								.getNombrePropietario());
			}
			if (this.datosConsultaPredio.getTipoIdentificacionPropietario() != null) {
				this.repReporteEjecucionDetalle
						.setTipoIdentificacion(this.datosConsultaPredio
								.getTipoIdentificacionPropietario());
			}
			if (this.datosConsultaPredio.getIdentificacionPropietario() != null) {
				this.repReporteEjecucionDetalle
						.setNumeroIdentificacion(this.datosConsultaPredio
								.getIdentificacionPropietario());
			}

			if (this.datosConsultaPredio.getAreaTerrenoMin() != null) {
				this.repReporteEjecucionDetalle
						.setAreaTerrenoDesde(this.datosConsultaPredio
								.getAreaTerrenoMin());
			}
			if (this.datosConsultaPredio.getAreaTerrenoMax() != null) {
				this.repReporteEjecucionDetalle
						.setAreaTerrenoHasta(this.datosConsultaPredio
								.getAreaTerrenoMax());
			}
			if (this.datosConsultaPredio.getAreaConstruidaMin() != null) {
				this.repReporteEjecucionDetalle
						.setAreaConstruccionDesde(this.datosConsultaPredio
								.getAreaConstruidaMin());
			}
			if (this.datosConsultaPredio.getAreaConstruidaMax() != null) {
				this.repReporteEjecucionDetalle
						.setAreaConstruccionHasta(this.datosConsultaPredio
								.getAreaConstruidaMax());
			}
			if (this.datosConsultaPredio.getAvaluoDesde() != null) {
				this.repReporteEjecucionDetalle
						.setAvaluoDesde(this.datosConsultaPredio
								.getAvaluoDesde());
			}
			if (this.datosConsultaPredio.getAvaluoHasta() != null) {
				this.repReporteEjecucionDetalle
						.setAvaluoHasta(this.datosConsultaPredio
								.getAvaluoHasta());
			}
			
			//CRITERIOS ECONOMICOS
			if (this.datosConsultaPredio.getDestinoPredioId() != null) {
				for (String destinoPredio : this.datosConsultaPredio
						.getDestinoPredioId()) {
					
					contadorRegistrosMultiplesAnzada++;
					
					if(contadorRegistrosMultiplesAnzada > 1){
						this.repReporteEjecucionDetalle.setPredioDestino(this.repReporteEjecucionDetalle.getPredioDestino() + "," + "'"
								+ destinoPredio + "'");
						
					}else{
						this.repReporteEjecucionDetalle.setPredioDestino("'"
								+ destinoPredio + "'");
					}
				}
			}
			//reinicia contador
			contadorRegistrosMultiplesAnzada =0;
			
			if (this.datosConsultaPredio.getTipoPredioId() != null) {
				for (String tipoPredio : this.datosConsultaPredio
						.getTipoPredioId()) {
					
					contadorRegistrosMultiplesAnzada++;
					
					if(contadorRegistrosMultiplesAnzada > 1){
						
						this.repReporteEjecucionDetalle.setPredioTipo(this.repReporteEjecucionDetalle.getPredioTipo() + "," + "'"
								+ tipoPredio + "'");
						
					}else{
						this.repReporteEjecucionDetalle.setPredioTipo("'" + tipoPredio + "'");
					}
					
					
				}
			}
			
			//reinicia contador
			contadorRegistrosMultiplesAnzada =0;
			if (this.datosConsultaPredio.getUsoConstruccionId() != null) {
				for (String usoConstruccionPredio : this.datosConsultaPredio
						.getUsoConstruccionId()) {
					
					contadorRegistrosMultiplesAnzada++;
					
					if(contadorRegistrosMultiplesAnzada > 1){
						
						this.repReporteEjecucionDetalle.setConstruccionUso(this.repReporteEjecucionDetalle.getConstruccionUso() + ","  + "'"
								+ usoConstruccionPredio  + "'");
						
					}else{
						this.repReporteEjecucionDetalle.setConstruccionUso("'" + usoConstruccionPredio + "'");
					}
					
				}

			}
			
			//reinicia contador
			contadorRegistrosMultiplesAnzada =0;
			if (this.datosConsultaPredio.getZonaFisicaId() != null) {
				for (String zonaFisica : this.datosConsultaPredio
						.getZonaFisicaId()) {

					contadorRegistrosMultiplesAnzada++;
					
					if(contadorRegistrosMultiplesAnzada > 1){
						
						this.repReporteEjecucionDetalle.setZonaFisica(this.repReporteEjecucionDetalle.getZonaFisica() + ","  + "'"
								+ zonaFisica  + "'");
						
					}else{
						
						this.repReporteEjecucionDetalle.setZonaFisica("'" + zonaFisica  + "'");
					}

				}
			}
			
			//reinicia contador
			contadorRegistrosMultiplesAnzada =0;
			if (this.datosConsultaPredio.getZonaEconomicaId() != null) {
				for (String zonaEconomica : this.datosConsultaPredio
						.getZonaEconomicaId()) {

					contadorRegistrosMultiplesAnzada++;
					
					if(contadorRegistrosMultiplesAnzada > 1){
						
						this.repReporteEjecucionDetalle.setZonaGeoeconomica(this.repReporteEjecucionDetalle.getZonaGeoeconomica() + ","  + "'"
								+ zonaEconomica  + "'");
						
					}else{
						
						this.repReporteEjecucionDetalle.setZonaGeoeconomica("'" + zonaEconomica  + "'");
					}

				}
			}
			//JUSTIFICACION DE PROPIEDAD
			//reinicia contador
			contadorRegistrosMultiplesAnzada =0;
			if (this.datosConsultaPredio.getTipoTitulo() != null) {
				for (String tipoTitulo : this.datosConsultaPredio
						.getTipoTitulo()) {

					contadorRegistrosMultiplesAnzada++;
					
					if(contadorRegistrosMultiplesAnzada > 1){
						
						this.repReporteEjecucionDetalle.setTipoTitulo(this.repReporteEjecucionDetalle.getTipoTitulo() + ","  + "'"
								+ tipoTitulo  + "'");
						
					}else{
						
						this.repReporteEjecucionDetalle.setTipoTitulo("'" + tipoTitulo  + "'");
					}

				}
			}
			
			//reinicia contador
			contadorRegistrosMultiplesAnzada =0;
			if (this.datosConsultaPredio.getModoAdquisicionId() != null) {
				for (String modoAdquisicion : this.datosConsultaPredio
						.getModoAdquisicionId()) {

                                    if (modoAdquisicion.equals("A")) {
                                        modoAdquisicion = Constantes.MODO_ADQUISICION_TRADICION;
                                    }

                                    if (modoAdquisicion.equals("B")) {
                                        modoAdquisicion = Constantes.MODO_ADQUISICION_OCUPACION;
                                    }

                                    if (modoAdquisicion.equals("C")) {
                                        modoAdquisicion = Constantes.MODO_ADQUISICION_SUCESION;
                                    }

                                    if (modoAdquisicion.equals("D")) {
                                        modoAdquisicion = Constantes.MODO_ADQUISICION_PRESCRIPCION;
                                    }

                                    if (modoAdquisicion.equals("E")) {
                                        modoAdquisicion = Constantes.MODO_ADQUISICION_ACCESION;
                                    }
                               
					contadorRegistrosMultiplesAnzada++;
                                        
					if(contadorRegistrosMultiplesAnzada > 1){
						
						this.repReporteEjecucionDetalle.setModoAdquisicion(this.repReporteEjecucionDetalle.getModoAdquisicion() + "," + "'"
								+ modoAdquisicion  + "'");
						
					}else{
						this.repReporteEjecucionDetalle.setModoAdquisicion("'" + modoAdquisicion + "'");
					}
				}
			}
			
			//PREDIOS BLOQUEADOS
			//reinicia contador

			if (this.datosConsultaPredio.getPredioBloqueado() != null) {
				//this.repReporteEjecucionDetalle.setPredioBloqueado(this.datosConsultaPredio.getPredioBloqueado());
					
			}
			
			if (this.datosConsultaPredio.getFechaInicioBloqueo() != null) {
				this.repReporteEjecucionDetalle.setFechaInicioBloqueo(this.datosConsultaPredio.getFechaInicioBloqueo());
					
			}
			
			if (this.datosConsultaPredio.getFechaDesbloqueo() != null) {
				this.repReporteEjecucionDetalle.setFechaDesbloqueo(this.datosConsultaPredio.getFechaDesbloqueo());
					
			}
			
			if (this.datosConsultaPredio.getTipoBloqueo() != null) {
				//this.repReporteEjecucionDetalle.setBloqueo(this.datosConsultaPredio.getTipoBloqueo());
					
			}
			
			if (this.datosConsultaPredio.getFechaEnvio() != null) {
				//this.repReporteEjecucionDetalle.setFechaEnvio(this.datosConsultaPredio.getFechaEnvio());
					
			}
			
			if (this.datosConsultaPredio.getFechaRecibido() != null) {
				//this.repReporteEjecucionDetalle.setFechaRecibido(this.datosConsultaPredio.getFechaRecibido());
					
			}
			
			if (this.datosConsultaPredio.getNumeroRadicacionBloqueo() != null) {
				//this.repReporteEjecucionDetalle.setNumeroRadicacionBloqueo(this.datosConsultaPredio.getNumeroRadicacionBloqueo());
					
			}
			
			//MATRICULA
			if (this.datosConsultaPredio.getCirculoRegistral() != null) {

                            if (this.datosConsultaPredio.getCirculoRegistral().equals(DEFAULT_COMBOS)) {
                                this.repReporteEjecucionDetalle.
                                        setCirculoRegistral(null);

                            } else {
                                this.repReporteEjecucionDetalle
                                        .setCirculoRegistral(this.datosConsultaPredio
                                                .getCirculoRegistral());
                            }
			}
			
			if (this.datosConsultaPredio.getNumeroRegistro() != null) {
				this.repReporteEjecucionDetalle
						.setMatriculaInmobiliaria(this.datosConsultaPredio
								.getNumeroRegistro());
			}
			
			if (this.datosConsultaPredio.getNumeroRegistroFinal() != null) {
				this.repReporteEjecucionDetalle
						.setMatriculaInmobiliariaHasta(this.datosConsultaPredio
								.getNumeroRegistroFinal());
			}

                        if (this.datosConsultaPredio.getTipoPersona() != null) {
                            this.repReporteEjecucionDetalle
                                    .setTipoPersona(this.datosConsultaPredio.getTipoPersona());
                        }
                        
                        if (this.datosConsultaPredio.getDireccion() != null) {
                            this.repReporteEjecucionDetalle
                                    .setDireccion(this.datosConsultaPredio.getDireccion());
                        }

			this.repReporteEjecucionDetalle.setUsuarioLog(this.usuario.getLogin());
                        this.repReporteEjecucionDetalle.setFechaLog(new Date());


             //validar rangos de areas
		    if(this.repReporteEjecucionDetalle.getAreaTerrenoDesde()!=null && this.repReporteEjecucionDetalle.getAreaTerrenoHasta()!=null){
				if(this.repReporteEjecucionDetalle.getAreaTerrenoHasta().doubleValue() -
						this.repReporteEjecucionDetalle.getAreaTerrenoDesde().doubleValue() <= 0){
					this.repReporteEjecucionDetalle.setAreaTerrenoDesde(null);
					this.repReporteEjecucionDetalle.setAreaTerrenoHasta(null);

				}
			}

			if(this.repReporteEjecucionDetalle.getAreaConstruccionDesde()!=null && this.repReporteEjecucionDetalle.getAreaConstruccionHasta()!=null){
				if(this.repReporteEjecucionDetalle.getAreaConstruccionHasta().doubleValue() -
						this.repReporteEjecucionDetalle.getAreaConstruccionDesde().doubleValue() <= 0){
					this.repReporteEjecucionDetalle.setAreaConstruccionDesde(null);
					this.repReporteEjecucionDetalle.setAreaConstruccionHasta(null);

				}
			}
			if(this.repReporteEjecucionDetalle.getAvaluoDesde()!=null && this.repReporteEjecucionDetalle.getAvaluoHasta()!=null){
				if(this.repReporteEjecucionDetalle.getAvaluoHasta().doubleValue() -
						this.repReporteEjecucionDetalle.getAvaluoDesde().doubleValue() <= 0){
					this.repReporteEjecucionDetalle.setAvaluoHasta(null);
					this.repReporteEjecucionDetalle.setAvaluoDesde(null);

				}
			}

			this.repReporteEjecucionDetalle = this.getConservacionService()
					.actualizarRepReporteEjecucionDetalle(
							this.repReporteEjecucionDetalle);
			

		
		return this.repReporteEjecucionDetalle;
	}
	
	 /**
 	 * Funcion que permite descargar los archivos
 	 * 
 	 * @author leidy.gonzalez
 	 */
 	public void descargarArchivosaExportar(){
 		
		for (RepReporteEjecucion repTemp : this.reportesDeUsuario) {
			
			String rutaTempLocal = this.getGeneralesService()
					.descargarArchivoDeAlfrescoATemp(repTemp.getRutaReporteGenerado());

			File archivoDocumentosAsociadosATramite = new File(rutaTempLocal);

			if (!archivoDocumentosAsociadosATramite.exists()) {
				LOGGER.error("Error el archivo de origen no existe");
				this.addMensajeError("Ocurrió un error al descargar los archivos");
				return;
			}

			InputStream stream;

			try {
				stream = new FileInputStream(archivoDocumentosAsociadosATramite);
				
				FileNameMap fileNameMap = URLConnection.getFileNameMap();
				this.tipoMimeDocTemporal = fileNameMap
						.getContentTypeFor(rutaTempLocal);
				
				this.setArchivosADescargar(new DefaultStreamedContent(stream,
						this.tipoMimeDocTemporal, rutaTempLocal));
				LOGGER.debug("Entro cargar descargarArchivosaExportar stream:"
						+ stream.toString());
			} catch (FileNotFoundException e) {
				LOGGER.error("Archivo no encontrado, no se puede descargar los documentos asociados al trámite",e);
			}
		}
 	}
     
     
     public void onChangeDepartamentos() {
			this.municipio = null;
			this.cargarMunicipiosPorDepartamento();
	}
     
     
     public void cambiarOrdenDepartamentos() {

 		if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
 			this.ordenDepartamentos = EOrden.NOMBRE;
 			this.departamentosOrderedByNombre = true;
 		} else {
 			this.ordenDepartamentos = EOrden.CODIGO;
 			this.departamentosOrderedByNombre = false;
 		}

 		this.cargarDepartamentos();
 	}
     
     
  // ----------------------------------------------------- //
 	/**
 	 * Método que realiza el cargue de la lista de {@link Departamento} respecto a la parametrización del usuario
 	 * @author
 	 */
 	public void cargarDepartamentos(){
 		
 		this.departamentosItemList.clear();
 		
 		if(this.datosConsultaPredio.getCodigoTipoInforme() != null
 				&& this.territorialSeleccionadaCodigo != null 
 				&& this.parametrizacionUsuario != null 
 				&& !this.parametrizacionUsuario.isEmpty()){
 			
 			this.getCombosDeptosMunisMB().setDepartamentos(new ArrayList<Departamento>());
 			this.getCombosDeptosMunisMB().setDepartamentosItemList(new ArrayList<SelectItem>());
 			this.getCombosDeptosMunisMB().getDepartamentosItemList().add(new SelectItem(null, this.DEFAULT_COMBOS));
 			
 			for(RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario){
 				
 				if(this.datosConsultaPredio.getCodigoTipoInforme() != null
 						&& this.datosConsultaPredio.getCodigoTipoInforme() == 0L){
 					
 					this.inicializaCombosDepartamentoYMunicipio();
 				}
 				
 				if(repUsuarioRol.getRepReporte().getId().longValue() == this.datosConsultaPredio.getCodigoTipoInforme().longValue()
 						&& repUsuarioRol.getRepTerritorialReportes() != null){
 					
 					// Se filtran los RepTerritorialReporte de la territorial seleccionada
 					for(RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()){
 						
 						if(rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)){
 							
 							// Si el RepTerritorialReporte tiene el campo de código departamento 
 							// vacío quiere decir que se tiene permisos para todos los departamentos
 							// de esa territorial
 							if(rtr.getDepartamento() == null) {
 								
 								LOGGER.error("Se debe parametrizar el departamento para el reporte solicitado ");
 								this.addMensajeError("Se debe parametrizar el departamento para el reporte solicitado");
 								return;
 								
 							}else{
 								
     								this.deptosList = this.getGeneralesService()
 										.obtenerRepTerritorialReporte(this.territorialSeleccionadaCodigo, this.uOCseleccionada);
                                                                
 								if (this.deptosList != null && !this.deptosList.isEmpty()) {
 									
 									this.departamentosItemList.clear();
 									
 									for (Departamento departamento : this.deptosList) {
 										
 										if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
 											
											this.departamentosItemList.add(new SelectItem(departamento
													.getCodigo(), departamento.getCodigo() + "-"
													+ departamento.getNombre()));
                                                                                        
                                                                                    if (this.departamentosItemList != null) {
                                                                                        Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                                                                                            @Override
                                                                                            public int compare(SelectItem s1, SelectItem s2) {
                                                                                                return s1.getValue().toString().compareTo(s2.getValue().toString());
                                                                                            }
                                                                                        };
                                                                                        Collections.sort(this.departamentosItemList, comparator);
                                                                                    }

											this.getCombosDeptosMunisMB().getDepartamentosItemList().add(new SelectItem(departamento
													.getCodigo(), departamento.getCodigo() + "-"
													+ departamento.getNombre()));
											
										}else{
											this.departamentosItemList.add(new SelectItem(departamento
													.getCodigo(), departamento.getNombre()));
                                                                                        
                                                                                    if (this.departamentosItemList != null) {
                                                                                        Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                                                                                            @Override
                                                                                            public int compare(SelectItem s1, SelectItem s2) {
                                                                                                return s1.getValue().toString().compareTo(s2.getValue().toString());
                                                                                            }
                                                                                        };
                                                                                        Collections.sort(this.departamentosItemList, comparator);
                                                                                    }
											
											this.getCombosDeptosMunisMB().getDepartamentosItemList().add(new SelectItem(departamento
													.getCodigo(), departamento.getNombre()));
										}
 									}
                                                                       
                                                                    if (this.deptosList != null) {
                                                                        Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                                                                            @Override
                                                                            public int compare(SelectItem s1, SelectItem s2) {
                                                                                return s1.getValue().toString().compareTo(s2.getValue().toString());
                                                                            }
                                                                        };
                                                                        Collections.sort(this.deptosList);
                                                                    }

 									this.getCombosDeptosMunisMB().setDepartamentos((ArrayList<Departamento>)this.deptosList);
 				 					break;
 								}
 							}
 							
 						}
 					}
 				}
 			}
 		}else{
 			
 			this.inicializaCombosDepartamentoYMunicipio();
 		}
 	}
 	
 	/**
 	 * Método encargado de inicializar Datos de Combos
 	 * de Departamento, Municipio y Numeros Prediales
 	 * @author leidy.gonzalez
 	 */
 	public void inicializaCombosDepartamentoYMunicipio() {
 	
 		//Inicializa Combo departamento
			this.getCombosDeptosMunisMB().setDepartamentosItemList(new ArrayList<SelectItem>());
			this.getCombosDeptosMunisMB().getDepartamentosItemList().add(new SelectItem(null, this.DEFAULT_COMBOS));
			
			this.getCombosDeptosMunisMB().setMunicipiosItemList(new ArrayList<SelectItem>());
			this.getCombosDeptosMunisMB().getMunicipiosItemList().add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
			
			this.getCombosDeptosMunisMB().setDepartamentos(new ArrayList<Departamento>());
			
			this.selectedDepartamentoCod = null;
			this.getCombosDeptosMunisMB().setSelectedDepartamentoCod(null);
			
			
			//Inicializa combo Municipio
			
			this.getCombosDeptosMunisMB().setMunicipios(new ArrayList<Municipio>());
			this.getCombosDeptosMunisMB().setMunicipiosItemList(new ArrayList<SelectItem>());
			this.getCombosDeptosMunisMB().getMunicipiosItemList().add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
			
			this.selectedMunicipioCod = null;
			this.getCombosDeptosMunisMB().setSelectedMunicipioCod(null);
			
			//Inicializa Numero Predial Inicial y Final
			
			this.numeroPredialPartesInicial = new NumeroPredialPartes();
			this.numeroPredialPartesFinal = new NumeroPredialPartes();
 	}
     
     /**
 	 * Método encargado de cargar los departamentos filtrados por funcionarios en una lista de
 	 * SelecItem asociados a la territorial del usuario en sesion
 	 * @author leidy.gonzalez
 	 */
 	public void cargarDepartamentosPorFuncionario() {
 		
 		this.departamentosItemList.clear();

 		
		if (this.territorialSeleccionadaCodigo != null) {

			this.deptosList = this.getGeneralesService()
					.getCacheDepartamentosPorTerritorial(
							this.territorialSeleccionadaCodigo);
			
			if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
				Collections.sort(this.deptosList);
			} else {
				Collections.sort(this.deptosList,
						Departamento.getComparatorNombre());
			}

			for (Departamento departamentoTemp : deptosList) {
								
				this.departamento = this.getGeneralesService().getCacheDepartamentoByCodigo(departamentoTemp.getCodigo());
				
				if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
					this.departamentosItemList.add(new SelectItem(departamentoTemp
							.getCodigo(), departamentoTemp.getCodigo() + "-"
							+ departamentoTemp.getNombre()));
				} else {
					this.departamentosItemList.add(new SelectItem(departamentoTemp
							.getCodigo(), departamentoTemp.getNombre()));
				}
			}
                        
                    if (this.departamentosItemList != null) {
                        Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                            @Override
                            public int compare(SelectItem s1, SelectItem s2) {
                                return s1.getValue().toString().compareTo(s2.getValue().toString());
                            }
                        };
                        Collections.sort(this.departamentosItemList, comparator);
                    }
		}

 	}
 	
 	/**
	 * action del commandlink para ordenar la lista de municipios
	 * 
	 * @author pedro.garcia
	 */
	public void cambiarOrdenMunicipios() {

		if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
			this.ordenMunicipios = EOrden.NOMBRE;
			this.municipiosOrderedByNombre = true;
		} else {
			this.ordenMunicipios = EOrden.CODIGO;
			this.municipiosOrderedByNombre = false;
		}

		this.cargarMunicipiosPorDepartamento();
	}
	
	// ----------------------------------------------------- //
		/**
		 * Método que realiza la actualización de los municipios al seleccionar un
		 * departamento.
		 * 
		 * @author
		 */
		public void cargarMunicipios() {
			
			this.municipiosItemList.clear();
            
            this.selectedMunicipioCod = null;
				
	 		//Se limpia numero predial inicial y final
			this.numeroPredialPartesInicial = new NumeroPredialPartes();
			this.numeroPredialPartesFinal = new NumeroPredialPartes();
			
			this.selectedDepartamentoCod = this.getCombosDeptosMunisMB().getSelectedDepartamentoCod();
			
			if(this.selectedDepartamentoCod != null && !this.selectedDepartamentoCod.isEmpty()){
				
				//Se Inicializa combo de Municipios
				this.getCombosDeptosMunisMB().setMunicipios(new ArrayList<Municipio>());
				this.getCombosDeptosMunisMB().setMunicipiosItemList(new ArrayList<SelectItem>());
	 			this.getCombosDeptosMunisMB().getMunicipiosItemList().add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
				
				
				for(RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario){
					
					
					if(repUsuarioRol.getRepReporte().getId().longValue() == this.datosConsultaPredio.getCodigoTipoInforme().longValue()
							&& repUsuarioRol.getRepTerritorialReportes() != null){
						
						// Se filtran los RepTerritorialReporte de la territorial seleccionada
						for(RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()){
							
							if(rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)
									&& rtr.getDepartamento().getCodigo().equals(this.selectedDepartamentoCod)){
								
								
								List<Municipio> municipiosListTemp = this.getGeneralesService()
										.getCacheMunicipiosByDepartamento(this.selectedDepartamentoCod);
								
								// Si el RepTerritorialReporte tiene el campo de código municipio 
								// vacío quiere decir que se tiene permisos para todos los municipios
								// del departamento
								if(rtr.getMunicipio() == null) {
									
									if (municipiosListTemp != null && !municipiosListTemp.isEmpty()) {
										
										this.getCombosDeptosMunisMB().setMunicipios(new ArrayList<Municipio>());
										this.getCombosDeptosMunisMB().setMunicipios((ArrayList<Municipio>)this.municipiosList);
										
										this.municipiosItemList.clear();
										
										for (Municipio municipio : municipiosListTemp) {
											
											this.municipiosList.add(municipio);
											
											this.municipiosItemList.add(new SelectItem(municipio
													.getCodigo(), municipio.getNombre()));
											
											if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
												this.municipiosItemList.add(new SelectItem(municipio
														.getCodigo(), municipio.getCodigo3Digitos() + "-"
														+ municipio.getNombre()));
											} else {
												this.municipiosItemList.add(new SelectItem(municipio
														.getCodigo(), municipio.getNombre()));
											}
											
										}
										this.getCombosDeptosMunisMB().setMunicipios((ArrayList<Municipio>)this.municipiosList);
										
									}
									break;								
								}else{
									
									
									if (municipiosListTemp != null && !municipiosListTemp.isEmpty()) {
										
										this.municipiosItemList.clear();
										
										for (Municipio municipio : municipiosListTemp) {
											
											if(rtr.getMunicipio().getCodigo().equals(municipio.getCodigo())){
												
												this.municipiosList.add(municipio);
												
												if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
													
													this.municipiosItemList.add(new SelectItem(municipio
															.getCodigo(), municipio.getCodigo() + "-"
															+ municipio.getNombre()));
													
													this.getCombosDeptosMunisMB().getMunicipiosItemList().add(new SelectItem(municipio
															.getCodigo(), municipio.getCodigo() + "-"
															+ municipio.getNombre()));
													
												} else {
													
													this.municipiosItemList.add(new SelectItem(municipio
															.getCodigo(), municipio.getNombre()));
													
													this.getCombosDeptosMunisMB().getMunicipiosItemList().add(new SelectItem(municipio
															.getCodigo(), municipio.getNombre()));
												}
												
											}
											
										}
                                                                               
                                                                            if (this.municipiosList != null) {
                                                                                Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
                                                                                    @Override
                                                                                    public int compare(SelectItem s1, SelectItem s2) {
                                                                                        return s1.getValue().toString().compareTo(s2.getValue().toString());
                                                                                    }
                                                                                };
                                                                                Collections.sort(this.municipiosList);
                                                                            }
										this.getCombosDeptosMunisMB().setMunicipios((ArrayList<Municipio>)this.municipiosList);
										
									}
									
								}
							}
						}
					}
				}
			}else{
				//Se limpia combo municipios
				this.getCombosDeptosMunisMB().setMunicipios(new ArrayList<Municipio>());
				this.getCombosDeptosMunisMB().setMunicipiosItemList(new ArrayList<SelectItem>());
	 			this.getCombosDeptosMunisMB().getMunicipiosItemList().add(new SelectItem(null, this.DEFAULT_COMBOS_TODOS));
	 			
	 			this.selectedMunicipioCod = null;
				this.getCombosDeptosMunisMB().setSelectedMunicipioCod(null);
				
	 			//Se limpia numero predial inicial y final
				this.numeroPredialPartesInicial = new NumeroPredialPartes();
				this.numeroPredialPartesFinal = new NumeroPredialPartes();
			}
			
		}
 	
 	/**
 	 *Método encargado de cargar los municipios filtrados por departamento en una lista de
 	 * SelecItem asociados a la territorial del usuario en sesion
 	 * @author leidy.gonzalez
 	 */
 	public void cargarMunicipiosPorDepartamento() {
 		
 		this.municipiosItemList.clear();

		if (this.selectedDepartamentoCod != null
				&& this.selectedDepartamentoCod.compareTo("") != 0) {
			this.municipiosList = this.getGeneralesService()
					.getCacheMunicipiosByDepartamento(
							this.selectedDepartamentoCod);
			
			if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
				Collections.sort(this.municipiosList);
			} else {
				Collections.sort(this.municipiosList,
						Municipio.getComparatorNombre());
			}

			for (Municipio municipioTemp : this.municipiosList) {
						        
		        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
					this.municipiosItemList.add(new SelectItem(municipioTemp
							.getCodigo(), municipioTemp.getCodigo3Digitos() + "-"
							+ municipioTemp.getNombre()));
				} else {
					this.municipiosItemList.add(new SelectItem(municipioTemp
							.getCodigo(), municipioTemp.getNombre()));
				}
			}
		}
 	}
 	
 	/** --------------------------------------------------------------------- */
	/**
	 * Método que asocia un reporte a el usuario
	 * logeado
	 * 
	 * @author leidy.gonzalez
	 */
	 public void asociarReporteAUsuario(){
		
		this.repReporteEjecucionUsuario = new RepReporteEjecucionUsuario();
		
		if(this.reportesDeUsuario != null
				&& !this.reportesDeUsuario.isEmpty()){
			
			for (RepReporteEjecucion repReporteEjecucionAsociar : this.reportesDeUsuario) {
				
				//Se consulta si el usuario ya fue asociado
				this.reportesEjecucionUsuarios = this.getConservacionService().
						buscarSubscripcionPorUsuarioIdReporte(this.usuario.getLogin(),repReporteEjecucionAsociar.getId());
				
				if(reportesEjecucionUsuarios.isEmpty()){
					
					this.actualizarReporteEjecucionUsuario(repReporteEjecucionAsociar);
					
					if(this.repReporteEjecucionUsuario != null){
						
						this.addMensajeInfo("El reporte con codigo:"+ repReporteEjecucionAsociar.getCodigoReporte() +"  fue asociado al usuario: " + this.usuario.getLogin());
						this.habilitaAsociarReporteUsuario = false;
					}
					
				}else{
					this.addMensajeWarn("El usuario que ya está inscrito al reporte.");
				}
				
			}
			
		}
		
	}
	 
	/** --------------------------------------------------------------------- */
	/**
	 * Método que realiza el set de los valores seleccionados en los filtros,
	 * para generar el reporte asociado
	 * 
	 * @ref ConsultaReportesPredialesMB.actualizarReporteEjecucionUsuario -
	 *      Leidy Gonzalez
	 * @author leidy.gonzalez
	 */
	public RepReporteEjecucionUsuario actualizarReporteEjecucionUsuario(
			RepReporteEjecucion repReporteEjecucion) {

		if (repReporteEjecucion != null) {

			this.repReporteEjecucionUsuario.setUsuarioGenera(this.usuario.getLogin());
			this.repReporteEjecucionUsuario.setReporteEjecucionId(repReporteEjecucion.getId());
			this.repReporteEjecucionUsuario.setUsuarioLog(repReporteEjecucion.getUsuario());
			this.repReporteEjecucionUsuario.setFechaLog(new Date());

			this.repReporteEjecucionUsuario = this.getConservacionService()
					.actualizarRepReporteEjecucionUsuario(
							this.repReporteEjecucionUsuario);

			if (this.repReporteEjecucionUsuario == null) {
				this.addMensajeError("No se ha podido realizar su asociación al reporte. Por favor comuníquese con el administrador.");
				return null;
			}
		}

		return this.repReporteEjecucionUsuario;
	}
	
	/** --------------------------------------------------------------------- */
	/**
	 * Método que consulta si existe alguna subscripcion del reporte 
	 * consultado con el usuario logeado
	 * 
	 * @author leidy.gonzalez
	 */
	public void buscarSubscripcionPorUsuarioIdReporte(RepReporteEjecucion reporteEjecucionGenerados){
		
		//Se consulta si el usuario ya fue asociado
		this.reportesEjecucionUsuarios = this.getConservacionService().
				buscarSubscripcionPorUsuarioIdReporte(this.usuario.getLogin(),reporteEjecucionGenerados.getId());
		
		if(reportesEjecucionUsuarios.isEmpty()){
			
			this.habilitaAsociarReporteUsuario=true;
		}else{
			this.habilitaAsociarReporteUsuario=false;
		}
	}
 	
 	/**
	 * Permite la lectura de un numero predial en sus componentes
	 * 
	 * @author leidy.gonzalez
	 *
	 */
	public void assembleNumeroPredialFromSegments() {
        StringBuilder numeroPredialAssembled;
        numeroPredialAssembled = new StringBuilder();
        if (numeroPredialPartesInicial.getDepartamentoCodigo() == null
                || numeroPredialPartesInicial.getDepartamentoCodigo().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getDepartamentoCodigo());
        }
        if (numeroPredialPartesInicial.getMunicipioCodigo() == null
                || numeroPredialPartesInicial.getMunicipioCodigo().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getMunicipioCodigo());
        }
        if (numeroPredialPartesInicial.getTipoAvaluo() == null
                || numeroPredialPartesInicial.getTipoAvaluo().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getTipoAvaluo());
        }
        if (numeroPredialPartesInicial.getSectorCodigo() == null
                || numeroPredialPartesInicial.getSectorCodigo().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getSectorCodigo());
        }
        if (numeroPredialPartesInicial.getComunaCodigo() == null
                || numeroPredialPartesInicial.getComunaCodigo().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getComunaCodigo());
        }
        if (numeroPredialPartesInicial.getBarrioCodigo() == null
                || numeroPredialPartesInicial.getBarrioCodigo().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getBarrioCodigo());
        }
        if (numeroPredialPartesInicial.getManzanaVeredaCodigo() == null
                || numeroPredialPartesInicial.getManzanaVeredaCodigo().isEmpty()) {
            numeroPredialAssembled.append("0000");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getManzanaVeredaCodigo());
        }
        if (numeroPredialPartesInicial.getPredio() == null
                || numeroPredialPartesInicial.getPredio().isEmpty()) {
            numeroPredialAssembled.append("0000");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getPredio());
        }
        if (numeroPredialPartesInicial.getCondicionPropiedad() == null
                || numeroPredialPartesInicial.getCondicionPropiedad().isEmpty()) {
            numeroPredialAssembled.append("0");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getCondicionPropiedad());
        }
        if (numeroPredialPartesInicial.getEdificio() == null
                || numeroPredialPartesInicial.getEdificio().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getEdificio());
        }
        if (numeroPredialPartesInicial.getPiso() == null
                || numeroPredialPartesInicial.getPiso().isEmpty()) {
            numeroPredialAssembled.append("00");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getPiso());
        }
        if (numeroPredialPartesInicial.getUnidad() == null
                || numeroPredialPartesInicial.getUnidad().isEmpty()) {
            numeroPredialAssembled.append("0000");
        } else {
            numeroPredialAssembled.append(numeroPredialPartesInicial.getUnidad());
        }
        this.numeroPredial = numeroPredialAssembled.toString();
        this.datosConsultaPredio.setNumeroPredial(this.numeroPredial);
        
        
        StringBuilder numeroPredialFinalAssembled;
        numeroPredialFinalAssembled = new StringBuilder();
        if (numeroPredialPartesFinal.getDepartamentoCodigo() == null
                || numeroPredialPartesFinal.getDepartamentoCodigo().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getDepartamentoCodigo());
        }
        if (numeroPredialPartesFinal.getMunicipioCodigo() == null
                || numeroPredialPartesFinal.getMunicipioCodigo().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getMunicipioCodigo());
        }
        if (numeroPredialPartesFinal.getTipoAvaluo() == null
                || numeroPredialPartesFinal.getTipoAvaluo().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getTipoAvaluo());
        }
        if (numeroPredialPartesFinal.getSectorCodigo() == null
                || numeroPredialPartesFinal.getSectorCodigo().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getSectorCodigo());
        }
        if (numeroPredialPartesFinal.getComunaCodigo() == null
                || numeroPredialPartesFinal.getComunaCodigo().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getComunaCodigo());
        }
        if (numeroPredialPartesFinal.getBarrioCodigo() == null
                || numeroPredialPartesFinal.getBarrioCodigo().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getBarrioCodigo());
        }
        if (numeroPredialPartesFinal.getManzanaVeredaCodigo() == null
                || numeroPredialPartesFinal.getManzanaVeredaCodigo().isEmpty()) {
        	numeroPredialFinalAssembled.append("9999");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getManzanaVeredaCodigo());
        }
        if (numeroPredialPartesFinal.getPredio() == null
                || numeroPredialPartesFinal.getPredio().isEmpty()) {
        	numeroPredialFinalAssembled.append("9999");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getPredio());
        }
        if (numeroPredialPartesFinal.getCondicionPropiedad() == null
                || numeroPredialPartesFinal.getCondicionPropiedad().isEmpty()) {
        	numeroPredialFinalAssembled.append("9");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getCondicionPropiedad());
        }
        if (numeroPredialPartesFinal.getEdificio() == null
                || numeroPredialPartesFinal.getEdificio().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getEdificio());
        }
        if (numeroPredialPartesFinal.getPiso() == null
                || numeroPredialPartesFinal.getPiso().isEmpty()) {
        	numeroPredialFinalAssembled.append("99");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getPiso());
        }
        if (numeroPredialPartesFinal.getUnidad() == null
                || numeroPredialPartesFinal.getUnidad().isEmpty()) {
        	numeroPredialFinalAssembled.append("9999");
        } else {
        	numeroPredialFinalAssembled.append(numeroPredialPartesFinal.getUnidad());
        }
        this.numeroPredialFinal = numeroPredialFinalAssembled.toString();
        this.datosConsultaPredio.setNumeroPredialFinal(this.numeroPredialFinal);
    }
	
	/**
	 * Metodo que permite exportar el reporte que ya fue generado
	 * 
	 * @author leidy.gonzalez
	 */
	public void exportarReporte() {
		
		List<String> rutaDocumentosADescargar = new ArrayList<String>();

		if(this.reportesSeleccionados != null){
		
		 for(RepReporteEjecucion reporteTemp : this.reportesSeleccionados){
			
			this.repReporteEjecucion = reporteTemp;
			
			if(this.repReporteEjecucion != null && this.repReporteEjecucion.getRutaReporteGenerado() != null
	        		&& this.repReporteEjecucion.getEstado() != null
	        		&& this.repReporteEjecucion.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO
	    					.getCodigo())){
				
				this.verificarTamanioMaxReportes();
	        	
	        	this.reportePredial = this.reportsService.consultarReporteDeGestorDocumental(this.repReporteEjecucion.getRutaReporteGenerado());
	        	
	        	rutaDocumentosADescargar.add(this.reportePredial.getRutaCargarReporteAAlfresco());
	        	
	        	//Se consulta si el usuario ya fue asociado
				this.reportesEjecucionUsuarios = this.getConservacionService().
						buscarSubscripcionPorUsuarioIdReporte(this.usuario.getLogin(),reporteTemp.getId());
				
				if(reportesEjecucionUsuarios.isEmpty()){
					//Se asocia el reporte al usuario seleccionado
    				this.actualizarReporteEjecucionUsuario(reporteTemp);
				}else{
					this.addMensajeWarn("El usuario que ya está inscrito al reporte.");
				}
	        			
	        }else{
	        	LOGGER.error("El reporte con codigo:"+ this.repReporteEjecucion.getCodigoReporte()+ "no ha sido Finalizado");
				this.addMensajeWarn("El reporte con codigo:"+ this.repReporteEjecucion.getCodigoReporte()+ "no ha sido Finalizado");
				RequestContext context = RequestContext.getCurrentInstance();
				context.addCallbackParam("error", "error");
	        }
    		
    	}
		 //COMPRIMIR DOCUMENTOS
		if (!rutaDocumentosADescargar.isEmpty()) {

			String rutaZipDocumentos = this
					.getGeneralesService()
					.crearZipAPartirDeRutaDeDocumentos(rutaDocumentosADescargar);

			if (rutaZipDocumentos != null) {
				InputStream stream;
				File archivoDocumentosAsociadosATramite = new File(
						rutaZipDocumentos);
				if (!archivoDocumentosAsociadosATramite.exists()) {
					LOGGER.error("Error el archivo de origen no existe");
					this.addMensajeError("Ocurrió un error al descargar los archivos");
					return;
				}

				try {
                    DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd_HH_mm");
                    
					stream = new FileInputStream(
							archivoDocumentosAsociadosATramite);
                    
					this.archivosADescargar = new DefaultStreamedContent(
							stream, Constantes.TIPO_MIME_ZIP,
							"Reportes_Prediales_".concat(fechaHora.format(new Date())).trim()+".zip");
                    
					this.banderaReporteGenerado = true;
                    
				} catch (FileNotFoundException e) {
					LOGGER.error("Archivo no encontrado, no se puede descargar los documentos asociados al trámite",e);
				}
			}

		 }
		}else 
			
			/*if(this.reportesSeleccionados != null && this.reportesSeleccionados.length == 1){
			
			for(RepReporteEjecucion reporteTemp : this.reportesSeleccionados){
				
				this.repReporteEjecucion = reporteTemp;
				
				this.reportePredial = this.reportsService.consultarReporteDeGestorDocumental(this.repReporteEjecucion.getRutaReporteGenerado());
				
				this.urlDocumento = this.reportePredial.getUrlWebReporte();
				
				this.documentoReporte.setArchivo(this.urlDocumento);		
	        	
	        	rutaDocumentosADescargar.add(this.reportePredial.getRutaCargarReporteAAlfresco());
				
			}	
        	
        	
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			
			
		} */
			
			
			if(this.reportesSeleccionados == null && this.reportesSeleccionados.length == 0){
			
			LOGGER.error("Debe seleccionar reportes a exportar");
			this.addMensajeError("Debe seleccionar reportes a exportar");
			return;
		}
	}
	
	/**
	 * Metodo que permite refrescar la tabla de resultado de reportes
	 * que ya fueron generados
	 * 
	 * @author leidy.gonzalez
	 */
	public void refrescarReporte(){
		if (this.reportesSeleccionados != null) {
			
			this.buscarReportePorFiltros();
			
			for (RepReporteEjecucion reporteSeleccionado : this.reportesDeUsuario) {
				
				
				if(this.repReporteEjecucion.getEstado() != null &&
						(ERepReporteEjecucionEstado.EN_PROCESO.getCodigo().equals(this.repReporteEjecucion.getEstado())
							||ERepReporteEjecucionEstado.ERROR_ALMACENANDO.getCodigo().equals(this.repReporteEjecucion.getEstado())
							||ERepReporteEjecucionEstado.ERROR_GENERANDO.getCodigo().equals(this.repReporteEjecucion.getEstado()))
							||ERepReporteEjecucionEstado.TERMINADO.getCodigo().equals(this.repReporteEjecucion.getEstado())){
					
					this.banderaVerReporte = false;
				}
				
				
				if(this.repReporteEjecucion.getEstado() != null &&
						ERepReporteEjecucionEstado.FINALIZADO.getCodigo().equals(reporteSeleccionado.getEstado())){
					
					this.banderaVerReporte = true;
				}
				
			}
		}
		
	}
	
	/**
	 * Metodo que calcula el tamaño maximo
	 * de documentos a exportar
	 * 
	 * @author leidy.gonzalez
	 */
	public void verificarTamanioMaxReportes(){
		
		BigDecimal tamañoMaximo = new BigDecimal(2048);
		BigDecimal sumatoriaTamañoDocumentos = new BigDecimal(0);
		int resultado;
		
		for(RepReporteEjecucion reporteTemp : this.reportesSeleccionados){
						
			if(reporteTemp.getValorTamano() != null){
				
				sumatoriaTamañoDocumentos = sumatoriaTamañoDocumentos.add(reporteTemp.getValorTamano());
			}
		}

        resultado = tamañoMaximo.compareTo(sumatoriaTamañoDocumentos);
        
		if(resultado == -1 ){
			LOGGER.error("Ha superado el tamaño máximo de archivos a comprimir");
			this.addMensajeError("Ha superado el tamaño máximo de archivos a comprimir");
			RequestContext context = RequestContext.getCurrentInstance();
			context.addCallbackParam("error", "error");
			return;
		}
		
	}
	
	/**
	 * listener de selección en la tabla de comisiones Actualiza las banderas
	 * para determinar qué botones de acción se habilitan según las reglas
	 * definidas (ver la documentación de los métodos para determinar cuándo se
	 * activa qué botón)
	 *
	 */
	public void manejarActivacionDeBotones() {
		
		if(this.reportesSeleccionados != null || this.reportesSeleccionados.length != 0){
			for (RepReporteEjecucion repReporteEjecucionTemp : this.reportesSeleccionados) {
				
				if(repReporteEjecucionTemp.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO
						.getCodigo())){
					this.banderaExportarReporte = true;
				}else if (this.reportesDeUsuario == null || this.reportesDeUsuario.isEmpty()) {
					this.banderaExportarReporte = false;
				}
			}
			
		}else{
			this.banderaExportarReporte = false;
		}
		
	}
	
	/**
	 * listener de selección en la tabla de comisiones Actualiza las banderas
	 * para determinar qué botones de acción se habilitan según las reglas
	 * definidas (ver la documentación de los métodos para determinar cuándo se
	 * activa qué botón)
	 *
	 */
	public void manejarDesactivacionDeBotones() {
		
		if(this.reportesSeleccionados != null && this.reportesSeleccionados.length != 0){
			for (RepReporteEjecucion repReporteEjecucion : this.reportesSeleccionados) {
				
				if(repReporteEjecucion.getEstado().equals(ERepReporteEjecucionEstado.FINALIZADO
						.getCodigo())){
					this.banderaExportarReporte = true;
				}else{
					this.banderaExportarReporte = false;
				}
			}
			
		}else{
			this.banderaExportarReporte = false;
		}
		
	}
	
	/**
     * Listener para la seleccion de departamentos
     * 
     * @author leidy.gonzalez
     */
    public void onDeptoSelectedListener(){
        
        //combosDeptosMunisMB.onChangeDepartamentosListener();
    	this.cargarMunicipios();
        
        this.numeroPredialPartesInicial.setDepartamentoCodigo(this.getCombosDeptosMunisMB().getSelectedDepartamentoCod());
        this.numeroPredialPartesFinal.setDepartamentoCodigo(this.getCombosDeptosMunisMB().getSelectedDepartamentoCod());
        this.numeroPredialPartes.setDepartamentoCodigo(this.getCombosDeptosMunisMB().getSelectedDepartamentoCod());
        this.datosConsultaPredio.setDepartamentoId(this.getCombosDeptosMunisMB().getSelectedDepartamentoCod());
        
        this.buscarCirculoRegistral();
        
    }
    
    
    /**
     * Listener para la seleccion de departamentos
     * 
     * @author leidy.gonzalez
     */
    public void onMunSelectedListener(){
        
    	this.getCombosDeptosMunisMB().onChangeMunicipiosListener();
    	
    	if(this.getCombosDeptosMunisMB().getSelectedMunicipioCod() != null
                && !this.getCombosDeptosMunisMB().getSelectedMunicipioCod().isEmpty()){
    		
            this.numeroPredialPartesInicial.setMunicipioCodigo(this.getCombosDeptosMunisMB().getSelectedMunicipioCod().substring(2, 5));
            this.numeroPredialPartesFinal.setMunicipioCodigo(this.getCombosDeptosMunisMB().getSelectedMunicipioCod().substring(2, 5));
            this.numeroPredialPartes.setMunicipioCodigo(this.getCombosDeptosMunisMB().getSelectedMunicipioCod().substring(2, 5));
            this.datosConsultaPredio.setMunicipioId(this.getCombosDeptosMunisMB().getSelectedMunicipioCod().substring(2, 5));
                        
    	}else{
    		
            this.selectedMunicipioCod = null;
    		
    		this.numeroPredialPartesInicial.setMunicipioCodigo(null);
            this.numeroPredialPartesFinal.setMunicipioCodigo(null);
            this.numeroPredialPartes.setMunicipioCodigo(null);
    	}
        
        this.buscarCirculoRegistral();
        
    }
    
    /**
     * Listener para el cambio de valores en un filtro
     * 
     * @author leidy.gonzalez
     */
    public void cambiarValores(){
        
        if(this.uOCseleccionada != null &&
        		!this.uOCseleccionada.isEmpty()){
        	this.territorialSeleccionada.setCodigo(this.uOCseleccionada);
        }
        
    }
    /**
     * Listener para actualizar los departamentos disponibles
     * 
     * @author felipe.cadena
     */
    public void actualizarDeptos(){

        this.cargarDepartamentos();
        this.getCombosDeptosMunisMB().setSelectedDepartamentoCod(null);
        this.getCombosDeptosMunisMB().setSelectedMunicipio(null);
        this.onDeptoSelectedListener();
    }
    
    /**
	 * Método para saber el departamento y municipio seleccionados por el usuario
	 * @author leidy.gonzalez
	 */
	private void seleccionDepartamentoYMunicipio(){
		CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb.getManagedBean("combosDeptosMunis");
	
		this.selectedDepartamentoCod = combosDeptosMunisMB.getSelectedDepartamentoCod();
		
		this.departamento = combosDeptosMunisMB.getSelectedDepartamento();
		
		this.selectedMunicipioCod = combosDeptosMunisMB.getSelectedMunicipioCod();
		
		this.municipio = combosDeptosMunisMB.getSelectedMunicipio();
	}
	
	//-----------------------------Getters y Setters-----------------------
       
	public boolean isBanderaFitrosAvanzados() {
		return banderaFitrosAvanzados;
	}

	public void setBanderaFitrosAvanzados(boolean banderaFitrosAvanzados) {
		this.banderaFitrosAvanzados = banderaFitrosAvanzados;
	}

	public boolean isSeUsanDeptosFijos() {
		return imprimirUsuario;
	}

	public void setSeUsanDeptosFijos(boolean seUsanDeptosFijos) {
		this.imprimirUsuario = seUsanDeptosFijos;
	}

        public boolean isDefaultComboMatricula() {
            return defaultComboMatricula;
        }

        public void setDefaultComboMatricula(boolean defaultComboMatricula) {
            this.defaultComboMatricula = defaultComboMatricula;
        }
        
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public boolean isDestinoConstruccionResidencial() {
		return destinoConstruccionResidencial;
	}

	public void setDestinoConstruccionResidencial(
			boolean destinoConstruccionResidencial) {
		this.destinoConstruccionResidencial = destinoConstruccionResidencial;
	}

	public boolean isCirculoRegistralMatricula() {
		return circuloRegistralMatricula;
	}

	public void setCirculoRegistralMatricula(boolean circuloRegistralMatricula) {
		this.circuloRegistralMatricula = circuloRegistralMatricula;
	}

	public boolean isMatriculaAntigua() {
		return matriculaAntigua;
	}

	public void setMatriculaAntigua(boolean matriculaAntigua) {
		this.matriculaAntigua = matriculaAntigua;
	}

	public List<SelectItem> getTipoCirculoRegistral() {
		return tipoCirculoRegistral;
	}

	public void setTipoCirculoRegistral(List<SelectItem> tipoCirculoRegistral) {
		this.tipoCirculoRegistral = tipoCirculoRegistral;
	}

	public List<SelectItem> getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(List<SelectItem> tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public List<SelectItem> getTipificacionPredios() {
		return tipificacionPredios;
	}

	public void setTipificacionPredios(List<SelectItem> tipificacionPredios) {
		this.tipificacionPredios = tipificacionPredios;
	}

	public List<SelectItem> getUnidadConstruccionPredios() {
		return unidadConstruccionPredios;
	}

	public void setUnidadConstruccionPredios(
			List<SelectItem> unidadConstruccionPredios) {
		this.unidadConstruccionPredios = unidadConstruccionPredios;
	}

	public List<SelectItem> getZonaEconomicaPredios() {
		return zonaEconomicaPredios;
	}

	public void setZonaEconomicaPredios(List<SelectItem> zonaEconomicaPredios) {
		this.zonaEconomicaPredios = zonaEconomicaPredios;
	}

	public List<SelectItem> getZonaFisicaPredios() {
		return zonaFisicaPredios;
	}

	public void setZonaFisicaPredios(List<SelectItem> zonaFisicaPredios) {
		this.zonaFisicaPredios = zonaFisicaPredios;
	}

	public List<SelectItem> getDestinoPredios() {
		return destinoPredios;
	}

	public void setDestinoPredios(List<SelectItem> destinoPredios) {
		this.destinoPredios = destinoPredios;
	}

	public List<SelectItem> getModoAdquisicionPredios() {
		return modoAdquisicionPredios;
	}

	public void setModoAdquisicionPredios(List<SelectItem> modoAdquisicionPredios) {
		this.modoAdquisicionPredios = modoAdquisicionPredios;
	}

	public List<SelectItem> getTipoPredios() {
		return tipoPredios;
	}

	public void setTipoPredios(List<SelectItem> tipoPredios) {
		this.tipoPredios = tipoPredios;
	}

	public List<SelectItem> getReportePorCategoria() {
		return reportePorCategoria;
	}

	public void setReportePorCategoria(List<SelectItem> reportePorCategoria) {
		this.reportePorCategoria = reportePorCategoria;
	}

	public List<SelectItem> getTipoReportePorCategoria() {
		return tipoReportePorCategoria;
	}

	public void setTipoReportePorCategoria(List<SelectItem> tipoReportePorCategoria) {
		this.tipoReportePorCategoria = tipoReportePorCategoria;
	}

	public RepReporteEjecucionDetalle getRepReporteEjecucionDetalle() {
		return repReporteEjecucionDetalle;
	}

	public void setRepReporteEjecucionDetalle(
			RepReporteEjecucionDetalle repReporteEjecucionDetalle) {
		this.repReporteEjecucionDetalle = repReporteEjecucionDetalle;
	}

	public List<RepReporte> getTiposReportes() {
		return tiposReportes;
	}

	public void setTiposReportes(List<RepReporte> tiposReportes) {
		this.tiposReportes = tiposReportes;
	}

	public RepReporte getRepReporte() {
		return repReporte;
	}

	public void setRepReporte(RepReporte repReporte) {
		this.repReporte = repReporte;
	}

	public RepReporteEjecucion getRepReporteEjecucion() {
		return repReporteEjecucion;
	}

	public void setRepReporteEjecucion(RepReporteEjecucion repReporteEjecucion) {
		this.repReporteEjecucion = repReporteEjecucion;
	}

	public List<SelectItem> getDepartamentosItemList() {
		return departamentosItemList;
	}

	public List<SelectItem> getMunicipiosItemList() {
		return municipiosItemList;
	}

	public String getTerritorialSeleccionadaCodigo() {
		return territorialSeleccionadaCodigo;
	}

	public void setTerritorialSeleccionadaCodigo(
			String territorialSeleccionadaCodigo) {
		this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
	}

	public String getSelectedMunicipioCodigo() {
		return selectedMunicipioCodigo;
	}

	public void setSelectedMunicipioCodigo(String selectedMunicipioCodigo) {
		this.selectedMunicipioCodigo = selectedMunicipioCodigo;
	}

	public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
		this.departamentosItemList = departamentosItemList;
	}

	public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
		this.municipiosItemList = municipiosItemList;
	}

	public FiltroGenerarReportes getDatosConsultaPredio() {
		return this.datosConsultaPredio;
	}

	public void setDatosConsultaPredio(FiltroGenerarReportes datosConsultaPredio) {
		this.datosConsultaPredio = datosConsultaPredio;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public List<CirculoRegistral> getCirculoRegistralV(){
    	return this.circuloRegistralV;
	}
    
    public List<SelectItem> getCirculoRegistralItemSelect(){
    	
		return this.circuloRegistralItemSelect;
	}
	public void setCirculoRegistralV(List<CirculoRegistral> circuloRegistralV) {
		this.circuloRegistralV = circuloRegistralV;
	}

	public void setCirculoRegistralItemSelect(
			ArrayList<SelectItem> circuloRegistralItemSelect) {
		this.circuloRegistralItemSelect = circuloRegistralItemSelect;
	}

	public List<RepReporteEjecucion> getReportesDeUsuario() {
		return reportesDeUsuario;
	}

	public void setReportesDeUsuario(List<RepReporteEjecucion> reportesDeUsuario) {
		this.reportesDeUsuario = reportesDeUsuario;
	}

	public RepControlReporte getReporteControlSeleccionado() {
		return reporteControlSeleccionado;
	}

	public void setReporteControlSeleccionado(
			RepControlReporte reporteControlSeleccionado) {
		this.reporteControlSeleccionado = reporteControlSeleccionado;
	}

	public ReporteDTO getReportePredialDTO() {
		return reportePredial;
	}

	public void setReportePredialDTO(ReporteDTO reportePredialDTO) {
		this.reportePredial = reportePredialDTO;
	}

	public List<Object> getReportesParticionados() {
		return reportesParticionados;
	}

	public void setReportesParticionados(List<Object> reportesParticionados) {
		this.reportesParticionados = reportesParticionados;
	}

	public EOrden getOrdenDepartamentos() {
		return ordenDepartamentos;
	}

	public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
		this.ordenDepartamentos = ordenDepartamentos;
	}

	public EOrden getOrdenDepartamentosDocumento() {
		return ordenDepartamentosDocumento;
	}

	public void setOrdenDepartamentosDocumento(EOrden ordenDepartamentosDocumento) {
		this.ordenDepartamentosDocumento = ordenDepartamentosDocumento;
	}

	public EOrden getOrdenMunicipios() {
		return ordenMunicipios;
	}

	public void setOrdenMunicipios(EOrden ordenMunicipios) {
		this.ordenMunicipios = ordenMunicipios;
	}

	public EOrden getOrdenMunicipiosDocumento() {
		return ordenMunicipiosDocumento;
	}

	public void setOrdenMunicipiosDocumento(EOrden ordenMunicipiosDocumento) {
		this.ordenMunicipiosDocumento = ordenMunicipiosDocumento;
	}

	public boolean isMunicipiosOrderedByNombre() {
		return municipiosOrderedByNombre;
	}

	public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
		this.municipiosOrderedByNombre = municipiosOrderedByNombre;
	}

	public boolean isDepartamentosOrderedByNombre() {
		return departamentosOrderedByNombre;
	}

	public void setDepartamentosOrderedByNombre(boolean departamentosOrderedByNombre) {
		this.departamentosOrderedByNombre = departamentosOrderedByNombre;
	}

	public List<Municipio> getMunicipiosList() {
		return municipiosList;
	}

	public void setMunicipiosList(List<Municipio> municipiosList) {
		this.municipiosList = municipiosList;
	}

	public List<Departamento> getDeptosList() {
		return deptosList;
	}

	public void setDeptosList(List<Departamento> deptosList) {
		this.deptosList = deptosList;
	}

	public String getSelectedDepartamentoCod() {
		return selectedDepartamentoCod;
	}

	public void setSelectedDepartamentoCod(String selectedDepartamentoCod) {
		this.selectedDepartamentoCod = selectedDepartamentoCod;
		
		Departamento dptoSeleccionado = null;
		if (selectedDepartamentoCod != null && this.deptosList != null) {
			for (Departamento depto : this.deptosList) {
				if (selectedDepartamentoCod.equals(depto.getCodigo())) {
					dptoSeleccionado = depto;
					break;
				}
			}
		}
		this.departamento = dptoSeleccionado;
		this.selectedMunicipioCod = null;
	}

	public String getSelectedMunicipioCod() {
		return selectedMunicipioCod;
	}

	public void setSelectedMunicipioCod(String selectedMunicipioCod) {
		this.selectedMunicipioCod = selectedMunicipioCod;
		
		Municipio municipioSelected = null;
		if (selectedMunicipioCod != null && this.municipiosList != null) {
			for (Municipio municipioTemp : this.municipiosList) {
				if (selectedMunicipioCod.equals(municipioTemp.getCodigo())) {
					municipioSelected = municipioTemp;
					break;
				}
			}
		}
		this.municipio = municipioSelected;
	}

        public boolean isHabilitaCampos() {
            return habilitaCampos;
        }

        public void setHabilitaCampos(boolean habilitaCampos) {
            this.habilitaCampos = habilitaCampos;
        }

	// ----------------------------------------------------- //
	/**
	 * Método que se ejecuta al seleccionar una territorial para la carga de
	 * unidades operativas.
	 * 
	 * @author leidy.gonzalez
	 */
	public void cargarTerritoriales() {

		this.listaTerritoriales.clear();
		List<String> territorialesItemListTemp = new ArrayList<String>();

		if (this.parametrizacionUsuario != null
				&& !this.parametrizacionUsuario.isEmpty()) {

			for (RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario) {

				if (repUsuarioRol.getRepTerritorialReportes() != null) {
					for (RepTerritorialReporte rtr : repUsuarioRol
							.getRepTerritorialReportes()) {
						if (rtr.getTerritorial() != null) {
							SelectItem territorial = new SelectItem(rtr
									.getTerritorial().getCodigo(), rtr
									.getTerritorial().getNombre());

							if (!territorialesItemListTemp.contains(rtr
									.getTerritorial().getCodigo())) {
								this.listaTerritoriales.add(territorial);
								territorialesItemListTemp.add(rtr
										.getTerritorial().getCodigo());
							}

							// Se selecciona la territorial a la cual pertenece
							// el usuario
							if (rtr.getTerritorial()
									.getCodigo()
									.equals(this.usuario.getCodigoTerritorial())) {
								this.territorialSeleccionadaCodigo = this.usuario
										.getCodigoTerritorial();
							}
						}
					}
				}
			}
		}

		this.cargarUOC();
	}
		
	// ----------------------------------------------------- //
	/**
	 * Método que se ejecuta al seleccionar una territorial para la carga de
	 * unidades operativas.
	 * 
	 * @author leidy.gonzalez
	 */
	public void cargarUOC() {
		if (this.territorialSeleccionadaCodigo != null) {
			this.territorialSeleccionada = this.buscarTerritorial(this.territorialSeleccionadaCodigo);
			this.uOCseleccionada = null;

			if (this.territorialSeleccionada != null) {
				this.cargarListaUOC();
				this.cargarUOCSeleccionada();
				this.cargarDepartamentos();
			}
		} else {
			this.UOCItemList = null;
		}
	}
	
	// ----------------------------------------------------- //
	/**
	 * Método que retorna una territorial a partir del código que ingresa como
	 * parámetro para su búsqueda
	 * 
	 * @author leidy.gonzalez
	 * @param codigoTerritorial
	 * @return
	 */
	public EstructuraOrganizacional buscarTerritorial(String codigoTerritorial) {
		if (codigoTerritorial != null) {
			EstructuraOrganizacional estructuraOrganizacional, answer = null;
			for (EstructuraOrganizacional eo : this.getGeneralesService()
					.buscarTodasTerritoriales()) {
				if (eo.getCodigo().equals(codigoTerritorial)) {
					answer = eo;
					break;
				}
			}
			if (answer != null) {
				estructuraOrganizacional = this.getGeneralesService()
						.buscarEstructuraOrganizacionalPorCodigo(
								answer.getCodigo());
				if (estructuraOrganizacional != null) {
					return estructuraOrganizacional;
				} else {
					return answer;
				}
			}
		}
		return null;
	}
	
	
	/**
	 * Mètodo que realiza el cargue de unidades operativas de catastro cuando se
	 * ha seleccionado una territorial
	 * 
	 * @author
	 */
	public void cargarListaUOC() {
		
		if(this.parametrizacionUsuario != null && !this.parametrizacionUsuario.isEmpty()){
			
			this.UOCItemList.clear();
			List<String> unidadesOperativasCatastroItemListTemp = new ArrayList<String>();
			
			for(RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario){
				
				if(repUsuarioRol.getRepTerritorialReportes() != null){
					for(RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()){
						
						// UOC
						if(rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)){
							if(rtr.getUOC() != null) {
								SelectItem uoc = new SelectItem(rtr.getUOC().getCodigo(),
										rtr.getUOC().getNombre());
							
		
								if(!unidadesOperativasCatastroItemListTemp.contains(rtr.getUOC().getCodigo())){
									this.UOCItemList.add(uoc);
									unidadesOperativasCatastroItemListTemp.add(rtr.getUOC().getCodigo());
								}
							}else{
								this.uocs = this.getGeneralesService()
										.getCacheEstructuraOrganizacionalPorTipoYPadre(EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
												this.territorialSeleccionadaCodigo);
								
								this.UOCItemList.clear();
								
								for (EstructuraOrganizacional eo : this.uocs) {
									this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
								}
							}
						}
					}
				}
			}
		}
	}
	
	// ----------------------------------------------------- //
	/**
	 * Método que realiza el set por default de la UOC del usuario.
	 * 
	 * @author
	 */
	public void cargarUOCSeleccionada() {
		if (this.usuario.isUoc()) {
			this.uOCseleccionada = null;

			if (this.uocs != null) {   
				
				this.uOCseleccionada = this.usuario.getCodigoUOC();
				
				for (EstructuraOrganizacional eo : this.uocs) {
					if (eo.getCodigo().equals(
							this.uOCseleccionada)) {
						this.unidadOperativaSeleccionada = eo;
						break;
					}
				}
			}
		}
	}
	
	/**
     * Metodo para inicializar las UOC asociadas al usuario
     *
     * @author leidy.gonzalez
     */
    public void cargarUOCs() {

        this.codigoTerritorial = this.usuario.getCodigoTerritorial();

        if (this.usuario.getCodigoUOC() != null){
        	uocs = this.getGeneralesService().buscarUOCporCodigoTerritorial(this.codigoTerritorial);
        } else {
        	uocs = this.getGeneralesService().buscarUOCporTerritorialSinRolEnUOC(this.codigoTerritorial, ERol.LIDER_TECNICO);
        }
                
        this.UOCItemList = new ArrayList<SelectItem>();
        
        if(uocs != null && !uocs.isEmpty()){
        	for (EstructuraOrganizacional eo : uocs) {
                //Inicio lista de seleccion
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
                this.uOCseleccionadaNombre = eo.getNombre();
            }
        }else{
        	uocs = this.getGeneralesService().buscarUOCporCodigoTerritorial(this.codigoTerritorial);
        	
        	for (EstructuraOrganizacional eo : uocs) {
                //Inicio lista de seleccion
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        }
        
        if (!UOCItemList.isEmpty() && this.usuario.getCodigoUOC() == null){
            this.banderaSeleccionUOC = true;
        }

        this.uOCseleccionada = this.usuario.getCodigoUOC();

    }
	
	/**
     * Metodo para inicializar las Territoriales asociadas al usuario
     *
     * @author leidy.gonzalez
     */
    /*public void cargarTerritoriales() {

    	this.territorialSeleccionada = this.usuario.getDescripcionTerritorial();

        this.codigoTerritorial = this.usuario.getCodigoTerritorial();
        
        //Inicio lista de seleccion
        this.listaTerritoriales = new ArrayList<SelectItem>();
        this.listaTerritoriales.add(new SelectItem(this.territorialSeleccionada));
        
        //cargar roles para la territorial
        if (listaTerritoriales.size() > 1){
            this.banderaSeleccionTerritorial = true;
        }

        this.uOCseleccionada = this.usuario.getCodigoTerritorial();

    }*/
    
    
    /**
	 * Método que inicializa un HashMap de {@link ESeccionReportesPrediales} con las
     * secciones y su validación.
	 */
	public void inicializarValidezSecciones() {
		this.getTabs();
		this.validezSeccion = new HashMap<ESeccionReportesPrediales, Boolean>();
		
		for (ESeccionReportesPrediales ese : ESeccionReportesPrediales.values()) {

			this.validezSeccion.put(ese, false);
			if (ese.toString().equals(ESeccionReportesPrediales.MATRICULA.toString())
					&& !tabs[0]) {
				this.validezSeccion.put(ese, true);
			}
			if (ese.toString().equals(ESeccionReportesPrediales.RANGOS_DE_AREA.toString())
					&& !tabs[1]) {
				this.validezSeccion.put(ese, true);
			}
			
			if (ese.toString().equals(ESeccionReportesPrediales.VALOR_AVALUO.toString())
					&& !tabs[2]) {
				this.validezSeccion.put(ese, true);
			}
			
			if (ese.toString().equals(ESeccionReportesPrediales.CRITERIOS_ECONOMICOS.toString())
					&& !tabs[3]) {
				this.validezSeccion.put(ese, true);
			}
			
			if (ese.toString().equals(ESeccionReportesPrediales.JUSTFICACION_DE_PROPIEDAD.toString())
					&& !tabs[4]) {
				this.validezSeccion.put(ese, true);
			}
			
			if (ese.toString().equals(ESeccionReportesPrediales.PREDIOS_BLOQUEADOS.toString())
					&& !tabs[5]) {
				this.validezSeccion.put(ese, true);
			}
		}	
	}
	
	/**
	 * Método que realiza configuracion de panel a visualizar
     * dependiendo de reporte en la consulta avanzada
	 */
	public boolean[] getTabs() {
		
		if(!this.configuracionesAvanzadas.isEmpty()){
			
			this.banVerFiltrosAvanzados = true;
			
			for (RepConfigParametroReporte confAvanzada : this.configuracionesAvanzadas) {
				
				if(confAvanzada.getNombreParametro() !=null 
						&& confAvanzada.getNombreParametro().equals(EParametroConfigReportes.DEPARTAMENTO.getCodigo())
						&& confAvanzada.getRequerido() != null){
					
					//Matricula
					tabs[0] = confAvanzada.getRequerido().equals(ESiNo.SI.getCodigo());
					
					//Rangos de Àrea
					tabs[1] = confAvanzada.getRequerido().equals(ESiNo.SI.getCodigo());
					
					//Valor Avalúo
					tabs[2] = confAvanzada.getRequerido().equals(ESiNo.SI.getCodigo());
							
					//Criterios Econòmicos
					tabs[3] = confAvanzada.getRequerido().equals(ESiNo.SI.getCodigo());
					
					//Justificaciòn de Propiedad
					tabs[4] = confAvanzada.getRequerido().equals(ESiNo.SI.getCodigo());
					//Predios Bloqueados
					tabs[5] = confAvanzada.getRequerido().equals(ESiNo.SI.getCodigo());
					
					this.configuracionAvanzada=confAvanzada;
				}
				
			}
		}else{
			this.banVerFiltrosAvanzados = false;
		}
		
		return tabs;
	}
	
	/**
	 * Método que valioda que en el filtro del predio inicial
     * se haya agregado el numero predial al menos hasta el campo manzana
	 */
	public void validaReporteNivelManzana() {
		
		// Validar Predio Hasta Manzana
		if(
                EParametroConfigReportes.NUMERO_PREDIAL_MANZANA.getCodigo().equals(configuracionAvanzada.getNombreParametro())
        		&& configuracionAvanzada.getHabilitado() != null && configuracionAvanzada.getRequerido() != null
        		&& configuracionAvanzada.getHabilitado().equals(ESiNo.SI.getCodigo())
        		&& configuracionAvanzada.getRequerido().equals(ESiNo.SI.getCodigo())){
			
			if (this.numeroPredialPartesInicial != null
					&& (this.numeroPredialPartesInicial.getManzanaVeredaCodigo() == null 
							|| this.numeroPredialPartesInicial.getManzanaVeredaCodigo().isEmpty())) {
				
				this.addMensajeError("Para los tipos de Reporte: "+ this.datosConsultaPredio.getCodigoTipoReporte()+
						" se debe ingresar el nùmero predial hasta la manzana");
				
				return;
			}else{
				this.assembleNumeroPredialFromSegments();
			}
			
		}
		
	}
	
	/**
	 * Método que se ejecuta al seleccionar el tipo de reporte a buscar, éste
	 * principalmente recarga la lista de los menus que dependen del tipo de
	 * reporte
	 * 
	 * @author leidy.gonzalez
	 */
	public void cambioTipoReporte() {
		
		this.tipoReportePorCategoria = new ArrayList<SelectItem>();

		if (!this.tipoReportePorTipo.isEmpty()
				&& !this.parametrizacionUsuario.isEmpty()) {
			
			
			for (SelectItem itemReporte : this.tipoReportePorTipo) {
				if(itemReporte.getValue() != null) {
					
					for (RepUsuarioRolReporte urr : this.parametrizacionUsuario) {
						if (urr.getRepReporte() != null
								&& urr.getRepReporte().getId() != null
								&& itemReporte.getValue().equals(urr.getRepReporte().getId())) {
							this.repReporte = urr.getRepReporte();
							
							this.tipoReportePorCategoria.add(new SelectItem(itemReporte.getValue(),itemReporte.getLabel()));
							
						}
					}

				}
			}
			
			
		}
		Collections.sort(this.tipoReportePorCategoria, comparatorSI);
		this.tipoReportePorCategoria.add(0, new SelectItem("", this.DEFAULT_COMBOS));
		
	}
	
	/** --------------------------------------------------------------------- */
	/**
	 * Método que realiza la revisión de los permisos de generación del tipo de
	 * reporte seleccionado para habilitar el botón de generar reporte.
	 * 
	 * @author leidy.gonzalez
	 * 
	 * @return
	 */
	public void revisarPermisoGenerarReporte() {
		this.banderaGenerarReporte = false;
		int sinPermisoGenerar = 0;
		
		if(this.datosConsultaPredio.getCodigoTipoInforme() != null 
				&& this.territorialSeleccionadaCodigo != null 
				&& !this.territorialSeleccionadaCodigo.isEmpty()){
			
			for(RepUsuarioRolReporte repUsuarioRol : this.parametrizacionUsuario){
				
				if(repUsuarioRol.getRepReporte().getId().longValue() == this.datosConsultaPredio.getCodigoTipoInforme().longValue()
						&& repUsuarioRol.getRepTerritorialReportes() != null){
					
					for(RepTerritorialReporte rtr : repUsuarioRol.getRepTerritorialReportes()){
						
 						if(rtr.getTerritorial().getCodigo().equals(this.territorialSeleccionadaCodigo)){
							
							if(rtr.getDepartamento() != null){
								if(this.selectedDepartamentoCod != null 
									&& rtr.getDepartamento().getCodigo().equals(this.selectedDepartamentoCod) 
									&& !repUsuarioRol.isPermisoGenerar()){
									sinPermisoGenerar++;
									break;
								}
							}else{
								if(!repUsuarioRol.isPermisoGenerar()){
									sinPermisoGenerar++;
									break;
								}else{
									this.banderaGenerarReporte = true;
									return;
								}
							}
						}
					}
				}
			}
			
			if(sinPermisoGenerar > 0){
				this.banderaGenerarReporte = false;
			}else{
                this.banderaGenerarReporte = true;
            }
		}
	}
	
	
	// ------------------------------------------------------------- //
	/**
	 * Método que realiza el set de la ruta de la previsualización del documento
	 * {@link RepReporteEjecucion}
	 * 
	 * @author leidy.gonzalez
	 */
	public void cargarPrevisualizacionReporte() {

		this.rutaPrevisualizacionReporte = new String();

		if (this.reporteVisualizacionSeleccionado != null
				&& !this.reporteVisualizacionSeleccionado.getRutaReporteGenerado().isEmpty()) {

			// Consultar archivo de previsualización
			try {

				// Loading an existing PDF document
				String ruta = this.getConservacionService()
						.descargarOficioDeGestorDocumentalATempLocalMaquina(
								this.reporteVisualizacionSeleccionado
										.getRutaReporteGenerado());

				File file = new File(ruta);

				PDDocument documento = PDDocument.load(file);

				// Instantiating Splitter class
				Splitter splitter = new Splitter();

				// splitting the pages of a PDF document
				List<PDDocument> Pages = splitter.split(documento);

				// Creating an iterator
				Iterator<PDDocument> iterator = Pages.listIterator();

				// Saving each page as an individual document
				int i = 0;
				while (iterator.hasNext()) {
					if (i == 0) {

						PDDocument pd = iterator.next();

						pd.save(ruta);

						File filePrimeraPagina = new File(ruta);

						this.rutaPrevisualizacionReporte = this.getGeneralesService().
								publicarArchivoEnWeb(filePrimeraPagina.getPath());
						
						System.out.println("Documento primera pagina: "
								+ this.rutaPrevisualizacionReporte);
						
						break;

					} else {

						break;
					}

				}
				documento.close();

			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				this.addMensajeError("Ocurrió un error en la carga de la previsualización.");
			}
		}
	}
	
	// ---------------- Get and Set------------------------

	

	public List<Predio> getPrediosSeleccionados() {
		return prediosSeleccionados;
	}

	public void setPrediosSeleccionados(List<Predio> prediosSeleccionados) {
		this.prediosSeleccionados = prediosSeleccionados;
	}

	public ReportesUtil getReportsService() {
		return reportsService;
	}

	public void setReportsService(ReportesUtil reportsService) {
		this.reportsService = reportsService;
	}

	public boolean isImprimirUsuario() {
		return imprimirUsuario;
	}

	public void setImprimirUsuario(boolean imprimirUsuario) {
		this.imprimirUsuario = imprimirUsuario;
	}
	
	public ReporteDTO getReportePredial() {
		return reportePredial;
	}

	public void setReportePredial(ReporteDTO reportePredial) {
		this.reportePredial = reportePredial;
	}

	public String getNumeroPredial() {
		return numeroPredial;
	}

	public void setNumeroPredial(String numeroPredial) {
		this.numeroPredial = numeroPredial;
	}

	public String getNumeroPredialFinal() {
		return numeroPredialFinal;
	}

	public void setNumeroPredialFinal(String numeroPredialFinal) {
		this.numeroPredialFinal = numeroPredialFinal;
	}

	public StreamedContent getArchivosADescargar() {
		this.exportarReporte();
		return archivosADescargar;
	}

	public void setArchivosADescargar(StreamedContent archivosADescargar) {
		this.archivosADescargar = archivosADescargar;
	}

	public String getConstantePrediales() {
		return constantePrediales;
	}

	public void setConstantePrediales(String constantePrediales) {
		this.constantePrediales = constantePrediales;
	}

	public List<SelectItem> getTipoTituloPredio() {
		return tipoTituloPredio;
	}

	public void setTipoTituloPredio(List<SelectItem> tipoTituloPredio) {
		this.tipoTituloPredio = tipoTituloPredio;
	}

	public EstructuraOrganizacional getTerritorialSeleccionada() {
		return territorialSeleccionada;
	}

	public void setTerritorialSeleccionada(EstructuraOrganizacional territorialSeleccionada) {
		this.territorialSeleccionada = territorialSeleccionada;
	}

	public List<SelectItem> getListaTerritoriales() {
		return listaTerritoriales;
	}

	public void setListaTerritoriales(List<SelectItem> listaTerritoriales) {
		this.listaTerritoriales = listaTerritoriales;
	}

	public boolean isBanderaSeleccionTerritorial() {
		return banderaSeleccionTerritorial;
	}

	public void setBanderaSeleccionTerritorial(boolean banderaSeleccionTerritorial) {
		this.banderaSeleccionTerritorial = banderaSeleccionTerritorial;
	}

	public String getCodigoTerritorial() {
		return codigoTerritorial;
	}

	public void setCodigoTerritorial(String codigoTerritorial) {
		this.codigoTerritorial = codigoTerritorial;
	}

	public String getuOCseleccionada() {
		return uOCseleccionada;
	}

	public void setuOCseleccionada(String uOCseleccionada) {
		this.uOCseleccionada = uOCseleccionada;
	}

	public List<EstructuraOrganizacional> getUocs() {
		return uocs;
	}

	public void setUocs(List<EstructuraOrganizacional> uocs) {
		this.uocs = uocs;
	}

	public List<SelectItem> getUOCItemList() {
		return UOCItemList;
	}

	public void setUOCItemList(List<SelectItem> uOCItemList) {
		UOCItemList = uOCItemList;
	}

	public boolean isBanderaSeleccionUOC() {
		return banderaSeleccionUOC;
	}

	public void setBanderaSeleccionUOC(boolean banderaSeleccionUOC) {
		this.banderaSeleccionUOC = banderaSeleccionUOC;
	}

	public boolean isHabilitaFechaCorte31() {
		return habilitaFechaCorte31;
	}

	public void setHabilitaFechaCorte31(boolean habilitaFechaCorte31) {
		this.habilitaFechaCorte31 = habilitaFechaCorte31;
	}

	public String getAutoestimacion() {
		return autoestimacion;
	}

	public void setAutoestimacion(String autoestimacion) {
		this.autoestimacion = autoestimacion;
	}

	public boolean isBanderaGenerarReporte() {
		return banderaGenerarReporte;
	}

	public void setBanderaGenerarReporte(boolean banderaGenerarReporte) {
		this.banderaGenerarReporte = banderaGenerarReporte;
	}

	public boolean isBanderaRefrescar() {
		return banderaRefrescar;
	}

	public void setBanderaRefrescar(boolean banderaRefrescar) {
		this.banderaRefrescar = banderaRefrescar;
	}

	public String getIdReporteConsultar() {
		return idReporteConsultar;
	}

	public void setIdReporteConsultar(String idReporteConsultar) {
		this.idReporteConsultar = idReporteConsultar;
	}

	public String getIdReporteConsultarAño() {
		return idReporteConsultarAño;
	}

	public void setIdReporteConsultarAño(String idReporteConsultarAño) {
		this.idReporteConsultarAño = idReporteConsultarAño;
	}

	public String getNumeracionReportePredial() {
		return numeracionReportePredial;
	}

	public void setNumeracionReportePredial(String numeracionReportePredial) {
		this.numeracionReportePredial = numeracionReportePredial;
	}

	public String getRutaReporteGenerado() {
		return rutaReporteGenerado;
	}

	public void setRutaReporteGenerado(String rutaReporteGenerado) {
		this.rutaReporteGenerado = rutaReporteGenerado;
	}
	
	public RepReporteEjecucion[] getReportesSeleccionados() {
		return reportesSeleccionados;
	}

	public void setReportesSeleccionados(RepReporteEjecucion[] reportesSeleccionados) {
		
		this.reportesSeleccionados = reportesSeleccionados;
		
	}

	public Documento getDocumentoReporte() {
		return documentoReporte;
	}

	public void setDocumentoReporte(Documento documentoReporte) {
		this.documentoReporte = documentoReporte;
	}

	public HashMap<ESeccionReportesPrediales, Boolean> getValidezSeccion() {
		return validezSeccion;
	}

	public void setValidezSeccion(
			HashMap<ESeccionReportesPrediales, Boolean> validezSeccion) {
		this.validezSeccion = validezSeccion;
	}

	public RepConfigParametroReporte getConfiguracionAvanzada() {
		return configuracionAvanzada;
	}

	public void setConfiguracionAvanzada(RepConfigParametroReporte configuracionAvanzada) {
		this.configuracionAvanzada = configuracionAvanzada;
	}

	public void setTabs(boolean[] tabs) {
		this.tabs = tabs;
	}

	public String getTamanioReporte() {
		return tamanioReporte;
	}

	public void setTamanioReporte(String tamanioReporte) {
		this.tamanioReporte = tamanioReporte;
	}

	public List<RepReporteEjecucionDetalle> getReportesDetalle() {
		return reportesDetalle;
	}

	public void setReportesDetalle(List<RepReporteEjecucionDetalle> reportesDetalle) {
		this.reportesDetalle = reportesDetalle;
	}

	public boolean isBanderaVerReporte() {
		return banderaVerReporte;
	}

	public void setBanderaVerReporte(boolean banderaVerReporte) {
		this.banderaVerReporte = banderaVerReporte;
	}

	public boolean isBanderaExportarReporte() {
		return banderaExportarReporte;
	}

	public void setBanderaExportarReporte(boolean banderaExportarReporte) {
		this.banderaExportarReporte = banderaExportarReporte;
	}

	public String getuOCseleccionadaNombre() {
		return uOCseleccionadaNombre;
	}

	public void setuOCseleccionadaNombre(String uOCseleccionadaNombre) {
		this.uOCseleccionadaNombre = uOCseleccionadaNombre;
	}

	public boolean isBanderaReporteGenerado() {
		return banderaReporteGenerado;
	}

	public void setBanderaReporteGenerado(boolean banderaReporteGenerado) {
		this.banderaReporteGenerado = banderaReporteGenerado;
	}

	public NumeroPredialPartes getNumeroPredialPartesInicial() {
		return numeroPredialPartesInicial;
	}

	public void setNumeroPredialPartesInicial(
			NumeroPredialPartes numeroPredialPartesInicial) {
		this.numeroPredialPartesInicial = numeroPredialPartesInicial;
	}

	public NumeroPredialPartes getNumeroPredialPartesFinal() {
		return numeroPredialPartesFinal;
	}

	public void setNumeroPredialPartesFinal(
			NumeroPredialPartes numeroPredialPartesFinal) {
		this.numeroPredialPartesFinal = numeroPredialPartesFinal;
	}

	public NumeroPredialPartes getNumeroPredialPartes() {
		return numeroPredialPartes;
	}

	public void setNumeroPredialPartes(NumeroPredialPartes numeroPredialPartes) {
		this.numeroPredialPartes = numeroPredialPartes;
	}

	public boolean isHabilitaTipoIdentificacion() {
		return habilitaTipoIdentificacion;
	}

	public void setHabilitaTipoIdentificacion(boolean habilitaTipoIdentificacion) {
		this.habilitaTipoIdentificacion = habilitaTipoIdentificacion;
	}

	public boolean isBanderaAnioActual() {
		return banderaAnioActual;
	}

	public void setBanderaAnioActual(boolean banderaAnioActual) {
		this.banderaAnioActual = banderaAnioActual;
	}

	public String getUrlDocumento() {
		return urlDocumento;
	}

	public void setUrlDocumento(String urlDocumento) {
		this.urlDocumento = urlDocumento;
	}

	public boolean isBanderaVisualizarReporteOComprimir() {
		return banderaVisualizarReporteOComprimir;
	}

	public void setBanderaVisualizarReporteOComprimir(
			boolean banderaVisualizarReporteOComprimir) {
		this.banderaVisualizarReporteOComprimir = banderaVisualizarReporteOComprimir;
	}

	public FiltroDatosConsultaPredio getFiltroCPredio() {
		return filtroCPredio;
	}

	public void setFiltroCPredio(FiltroDatosConsultaPredio filtroCPredio) {
		this.filtroCPredio = filtroCPredio;
	}

	public FiltroDatosConsultaPredio getFiltroFinalCPredio() {
		return filtroFinalCPredio;
	}

	public void setFiltroFinalCPredio(FiltroDatosConsultaPredio filtroFinalCPredio) {
		this.filtroFinalCPredio = filtroFinalCPredio;
	}

	public boolean isDeshabilitarCamposConsultaBool() {
		return deshabilitarCamposConsultaBool;
	}

	public void setDeshabilitarCamposConsultaBool(
			boolean deshabilitarCamposConsultaBool) {
		this.deshabilitarCamposConsultaBool = deshabilitarCamposConsultaBool;
	}

	public boolean isPermisoBuscarReporteBool() {
		return permisoBuscarReporteBool;
	}

	public void setPermisoBuscarReporteBool(boolean permisoBuscarReporteBool) {
		this.permisoBuscarReporteBool = permisoBuscarReporteBool;
	}

	public String getRutaPrevisualizacionReporte() {
		return rutaPrevisualizacionReporte;
	}

	public void setRutaPrevisualizacionReporte(String rutaPrevisualizacionReporte) {
		this.rutaPrevisualizacionReporte = rutaPrevisualizacionReporte;
	}

	public List<SelectItem> getTipoReportePorTipo() {
		return tipoReportePorTipo;
	}

	public void setTipoReportePorTipo(List<SelectItem> tipoReportePorTipo) {
		this.tipoReportePorTipo = tipoReportePorTipo;
	}

	public List<RepConfigParametroReporte> getConfiguracionesAvanzadas() {
		return configuracionesAvanzadas;
	}

	public void setConfiguracionesAvanzadas(
			List<RepConfigParametroReporte> configuracionesAvanzadas) {
		this.configuracionesAvanzadas = configuracionesAvanzadas;
	}

	public List<RepUsuarioRolReporte> getParametrizacionUsuario() {
		return parametrizacionUsuario;
	}

	public void setParametrizacionUsuario(
			List<RepUsuarioRolReporte> parametrizacionUsuario) {
		this.parametrizacionUsuario = parametrizacionUsuario;
	}

	public EstructuraOrganizacional getUnidadOperativaSeleccionada() {
		return unidadOperativaSeleccionada;
	}

	public void setUnidadOperativaSeleccionada(
			EstructuraOrganizacional unidadOperativaSeleccionada) {
		this.unidadOperativaSeleccionada = unidadOperativaSeleccionada;
	}

	public boolean isBanVerFiltrosAvanzados() {
		return banVerFiltrosAvanzados;
	}

	public void setBanVerFiltrosAvanzados(boolean banVerFiltrosAvanzados) {
		this.banVerFiltrosAvanzados = banVerFiltrosAvanzados;
	}
	
	public RepReporteEjecucionUsuario getRepReporteEjecucionUsuario() {
		return repReporteEjecucionUsuario;
	}

	public void setRepReporteEjecucionUsuario(
			RepReporteEjecucionUsuario repReporteEjecucionUsuario) {
		this.repReporteEjecucionUsuario = repReporteEjecucionUsuario;
	}
	

	public boolean isHabilitaAsociarReporteUsuario() {
		return habilitaAsociarReporteUsuario;
	}

	public void setHabilitaAsociarReporteUsuario(
			boolean habilitaAsociarReporteUsuario) {
		this.habilitaAsociarReporteUsuario = habilitaAsociarReporteUsuario;
	}

	public List<RepReporteEjecucionUsuario> getReportesEjecucionUsuarios() {
		return reportesEjecucionUsuarios;
	}

	public void setReportesEjecucionUsuarios(
			List<RepReporteEjecucionUsuario> reportesEjecucionUsuarios) {
		this.reportesEjecucionUsuarios = reportesEjecucionUsuarios;
	}
	
	

	public RepReporteEjecucion getReporteVisualizacionSeleccionado() {
		return reporteVisualizacionSeleccionado;
	}

	public void setReporteVisualizacionSeleccionado(
			RepReporteEjecucion reporteVisualizacionSeleccionado) {
		this.reporteVisualizacionSeleccionado = reporteVisualizacionSeleccionado;
	}

    public List<SelectItem> getAnoReporteItemList() {
        return anoReporteItemList;
    }

    public void setAnoReporteItemList(List<SelectItem> anoReporteItemList) {
        this.anoReporteItemList = anoReporteItemList;
    }
    
    

	/**
     * Retorna la instancia del MB CombosDeptosMunisMB
     * @return 
     */
    private CombosDeptosMunisMB getCombosDeptosMunisMB(){
         return (CombosDeptosMunisMB) UtilidadesWeb.getManagedBean("combosDeptosMunis");
    }


	Comparator<SelectItem> comparatorSI = new Comparator<SelectItem>() {
		@Override
		public int compare(SelectItem s1, SelectItem s2) {
			// the items must be compared based on their value (assuming String or Integer value here)
			return s1.getLabel().compareTo(s2.getLabel());
		}
	};	
}