package co.gov.igac.snc.ldap;

import java.util.Collection;

import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetails;

/**
 * Clase para extraer del LDAP los datos del usuario autenticado.
 *
 * @author juan.mendez
 *
 */
public class SNCUserDetails implements UserDetails {

    private static final Logger LOGGER = LoggerFactory.getLogger(SNCUserDetails.class);
    /**
     *
     */
    private static final long serialVersionUID = -9046836436051069376L;

    private LdapUserDetails ldapUserDetails;

    /**
     *
     * @param ldapUserDetails
     */
    public SNCUserDetails(Attributes attribs, LdapUserDetails userDetails) {
        // this.ldapUserDetails = ldapUserDetails;
        // ////////////////////////////////////////////

        ldapUserDetails = userDetails;
        // ////////////////////////////////////////////
    }

    /*
     * public String getDn() { return this.dn; }
     */
    /**
     *
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.ldapUserDetails.getAuthorities();
    }

    @Override
    public String getPassword() {
        return this.ldapUserDetails.getPassword();
    }

    @Override
    public String getUsername() {
        return this.ldapUserDetails.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.ldapUserDetails.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.ldapUserDetails.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.ldapUserDetails.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return this.ldapUserDetails.isEnabled();
    }

}
