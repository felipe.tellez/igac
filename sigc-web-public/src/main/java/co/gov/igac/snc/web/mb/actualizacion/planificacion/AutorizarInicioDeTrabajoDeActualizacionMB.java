package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionContrato;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EOrdenActualizacionDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @description Managed bean para la autoriazación del inicio del proceso de Formación o
 * Actualización oficialmente, registrando memorandos y comunicaciones requeridos.
 *
 * @author franz.gamba
 *
 * @documented david.cifuentes
 *
 */
@Component("autorizarTrabajoActualizacion")
@Scope("session")
public class AutorizarInicioDeTrabajoDeActualizacionMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AutorizarInicioDeTrabajoDeActualizacionMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS------------- */
    /** ---------------------------------- */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /**
     * Actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    /**
     * Usuario logueado.
     */
    private UsuarioDTO usuario;

    /**
     * Nombre del departamento al que pertenece el municipio en actualización.
     */
    private String departamentoNombre;

    /**
     * Nombre del municipio que va a ser actualizado.
     */
    private String municipioNombre;

    /**
     * Nombre del tipo de proyecto: Misional o Convenio.
     */
    private String tipoProyectoNombre;

    /**
     * Nombre la zona: urbana, rural, urbana/rural
     */
    private String zonaNombre;

    /**
     * Número de convenio.
     */
    private String convenioNumero;

    /**
     * Lista de convenios asociados con la resolución en curso.
     */
    private List<Convenio> convenios;

    /**
     * Actualización sobre la que se está trabajando.
     */
    private Actualizacion actualizacion;

    /**
     * Lista para almacenar los documentos requeridos.
     */
    private List<ActualizacionDocumento> documentosInicio;

    /**
     * Variable para almacenar el nombre del alcalde.
     */
    private String nombreAlcalde;

    /**
     * Lista de los contratistas a desplegar en el reporte.
     */
    private List<ActualizacionContrato> contratos;

    /**
     * Nuevo contratista seleccionado.
     */
    private ActualizacionContrato contratoSeleccionado;

    /**
     * Lista de selectItems de actividades segun el dominio 138
     */
    private List<SelectItem> actividadesList;

    /**
     * Bandera para la generacion de cada documento.
     */
    private boolean comunicacionFlag;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.currentActivity = this.tareasPendientesMB
            .getInstanciaSeleccionada();

        Long idActualizacion = this.tareasPendientesMB
            .getInstanciaSeleccionada().getIdObjetoNegocio();
        // long idActualizacion = 250;

        this.actualizacion = this.getActualizacionService()
            .recuperarActualizacionPorId(idActualizacion);

        this.departamentoNombre = actualizacion.getDepartamento().getNombre();
        this.municipioNombre = actualizacion.getMunicipio().getNombre();
        this.convenios = this.getActualizacionService()
            .recuperarConveniosPorIdActualizacion(actualizacion.getId());
        if (convenios != null && convenios.size() > 0) {

            // TODO franz.gamba :: comprobar que el convenio es el primero en la
            // lista de convenios
            // de no serlo, cuadrar para que trabaje con el convenio que es.
            this.tipoProyectoNombre = this.convenios.get(0).getTipoProyecto();
            this.zonaNombre = this.convenios.get(0).getZona();
            this.convenioNumero = this.convenios.get(0).getNumero();
        }
        this.documentosInicio = new ArrayList<ActualizacionDocumento>();
        crearDocumentosDeInicio();
        if (this.actualizacion.getActualizacionContratos() == null) {
            this.contratos = new ArrayList<ActualizacionContrato>();
        } else {
            this.contratos = this.actualizacion.getActualizacionContratos();
        }
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getTipoProyectoNombre() {
        return tipoProyectoNombre;
    }

    public void setTipoProyectoNombre(String tipoProyectoNombre) {
        this.tipoProyectoNombre = tipoProyectoNombre;
    }

    public String getZonaNombre() {
        return zonaNombre;
    }

    public void setZonaNombre(String zonaNombre) {
        this.zonaNombre = zonaNombre;
    }

    public String getConvenioNumero() {
        return convenioNumero;
    }

    public void setConvenioNumero(String convenioNumero) {
        this.convenioNumero = convenioNumero;
    }

    public List<Convenio> getConvenios() {
        return convenios;
    }

    public void setConvenios(List<Convenio> convenios) {
        this.convenios = convenios;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public String getNombreAlcalde() {
        return nombreAlcalde;
    }

    public void setNombreAlcalde(String nombreAlcalde) {
        this.nombreAlcalde = nombreAlcalde;
    }

    public List<ActualizacionContrato> getContratos() {
        return contratos;
    }

    public void setContratos(List<ActualizacionContrato> contratos) {
        this.contratos = contratos;
    }

    public ActualizacionContrato getContratoSeleccionado() {
        return contratoSeleccionado;
    }

    public void setContratoSeleccionado(
        ActualizacionContrato contratoSeleccionado) {
        this.contratoSeleccionado = contratoSeleccionado;
    }

    public List<SelectItem> getActividadesList() {
        return actividadesList;
    }

    public void setActividadesList(List<SelectItem> actividadesList) {
        this.actividadesList = actividadesList;
    }

    public List<ActualizacionDocumento> getDocumentosInicio() {
        return documentosInicio;
    }

    public void setDocumentosInicio(
        List<ActualizacionDocumento> documentosInicio) {
        this.documentosInicio = documentosInicio;
    }

    public boolean isComunicacionFlag() {
        return comunicacionFlag;
    }

    public void setComunicacionFlag(boolean comunicacionFlag) {
        this.comunicacionFlag = comunicacionFlag;
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método para eliminar contrato ingresado.
     *
     * @author franz.gamba
     */
    public void eliminarContrato() {
        this.contratos.remove(this.contratoSeleccionado);
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para adicionar un nuevo contrato.
     *
     * @author franz.gamba
     */
    public void adicionarContrato() {
        this.contratoSeleccionado = new ActualizacionContrato();
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para crear los documentos de resolucion al alcalde y autorizacion de contratacion para
     * el respectivo proceso de Actualización.
     *
     * @author franz.gamba
     */
    public void crearDocumentosDeInicio() {

        Documento resolucionAlcalde = new Documento();
        Documento autorizacionContrato = new Documento();

        TipoDocumento tipodoc = new TipoDocumento();
        // TODO::franz.gamba:: validar con el analista el tipo de documento.

        tipodoc = this.getGeneralesService().buscarTipoDocumentoPorId(
            ETipoDocumento.OTRO.getId());

        String nombre = "Comunicacion de resolución al alcalde.";

        resolucionAlcalde = this.inicializaDocumento(resolucionAlcalde, nombre,
            tipodoc);

        nombre = "Autorización de contratación para el proceso de actualización.";

        autorizacionContrato = this.inicializaDocumento(autorizacionContrato,
            nombre, tipodoc);

        /** Primera actualizacion */
        ActualizacionDocumento actualizacionDoc1 = this
            .inicializaActualizacionDocumento(resolucionAlcalde,
                EOrdenActualizacionDocumento.PRIMERO);

        ActualizacionDocumento actualizacionDoc2 = this
            .inicializaActualizacionDocumento(autorizacionContrato,
                EOrdenActualizacionDocumento.SEGUNDO);

        this.documentosInicio.add(actualizacionDoc1);
        this.documentosInicio.add(actualizacionDoc2);
    }

    /** --------------------------------------------------------------------- */
    /**
     * Metodo que inicializa un documento con los datos requeridos para su creacion en la base de
     * datos.
     *
     * @author franz.gamba
     */
    public Documento inicializaDocumento(Documento doc, String nombre,
        TipoDocumento tipodoc) {
        doc.setTipoDocumento(tipodoc);
        doc.setEstado(EDocumentoEstado.CARGADO.toString());
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setFechaLog(new Date());
        doc.setArchivo(nombre);
        doc.setBloqueado("NO");
        doc.setUsuarioLog(this.usuario.getLogin());
        doc = this.getGeneralesService().guardarDocumento(doc);//

        return doc;

    }

    /** --------------------------------------------------------------------- */
    /**
     * Metodo que inicializa un ActualizacionDocumento con los datos requeridos para su creacion en
     * la base de datos.
     *
     * @author franz.gamba
     */
    public ActualizacionDocumento inicializaActualizacionDocumento(
        Documento doc, EOrdenActualizacionDocumento orden) {

        ActualizacionDocumento actualizacionDoc = new ActualizacionDocumento();
        actualizacionDoc.setActualizacion(this.actualizacion);
        actualizacionDoc.setDocumento(doc);
        actualizacionDoc.setFechaLog(new Date());
        actualizacionDoc.setUsuarioLog(this.usuario.getLogin());
        actualizacionDoc.setOpcionDocumentoActualizacion(orden.getValor());
        this.getGeneralesService().guardarActualizacionDocumento(
            actualizacionDoc);
        return actualizacionDoc;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que avanza el proceso de actualización.
     *
     * @author franz.gamba
     *
     * @return
     */
    public String moverProcesodeActualizacion() {
        try {

            for (ActualizacionContrato ac : this.contratos) {
                ac.setActualizacion(this.actualizacion);
                ac.setUsuarioLog(this.usuario.getLogin());
                ac.setFechaLog(new Date());
            }

            this.getActualizacionService()
                .guardarYActualizarListActualizacionContrato(this.contratos);

            this.actualizacion.setActualizacionContratos(this.contratos);

            this.getActualizacionService().guardarYActualizarActualizacion(
                this.actualizacion);

            this.getActualizacionService().ordenarActualizacionAutorizada(
                this.currentActivity.getId(), this.actualizacion,
                this.usuario.getDescripcionTerritorial(), this.usuario);

            UtilidadesWeb.removerManagedBean("autorizarTrabajoActualizacion");
            this.tareasPendientesMB.init();
            return "/tareas/listaTareas.jsf";
        } catch (Exception e) {
            this.addMensajeError("Ha ocurrido un error al intentar mover el proceso.");
            LOGGER.error("Ha ocurrido un error al intentar mover el proceso");
            return null;
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que hace las validaciones para guardar un contratísta adecuadamente.
     *
     * @author david.cifuentes
     *
     * @return
     */
    public boolean validateContratista() {
        if (this.contratoSeleccionado != null) {
            if (this.contratoSeleccionado.getValorMensual() == null ||
                 this.contratoSeleccionado.getValorMensual() < 0D) {
                this.addMensajeError("Por favor ingrese un valor mensual mayor a cero.");
                return false;
            }
            if (this.contratoSeleccionado.getValorTotal() == null ||
                 this.contratoSeleccionado.getValorMensual() < 0D) {
                this.addMensajeError("Por favor ingrese un valor total mayor a cero.");
                return false;
            }
        }
        return true;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que almacena un contratísta.
     *
     * @author david.cifuentes
     */
    public void guardarContratista() {
        try {
            if (validateContratista()) {
                this.contratoSeleccionado.setFechaLog(new Date(System
                    .currentTimeMillis()));
                this.contratoSeleccionado
                    .setUsuarioLog(this.usuario.getLogin());
                this.contratoSeleccionado.setActualizacion(this.actualizacion);

                this.contratoSeleccionado = this.getActualizacionService()
                    .guardarYActualizarActualizacionContrato(
                        this.contratoSeleccionado);
                ActualizacionContrato aux = null;
                for (ActualizacionContrato ce : this.contratos) {
                    if (this.contratoSeleccionado.getId().longValue() == ce
                        .getId().longValue()) {
                        aux = ce;
                    }
                }
                if (aux != null) {
                    this.contratos.remove(aux);
                }
                this.contratos.add(this.contratoSeleccionado);

                this.addMensajeInfo("Se guardó la entidad satisfactoriamente");
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
            }
        } catch (Exception e) {
            LOGGER.error("Error en guardarEntidad");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que cierra la pantalla.
     *
     * @author franz.gamba
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("autorizarTrabajoActualizacion");
        this.tareasPendientesMB.init();
        return "index";
    }
}
