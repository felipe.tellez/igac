package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para ver los tramites asociados a un tramite principal
 *
 *
 * Se manejan los siguientes estados para los trámites: POR_APROBAR: cuando
 *
 * @author franz.gamba
 */
/*
 * @modified pedro.garcia 03-09-2013 documentación, cambio de nombres de variables (para que
 * parezcan lo que son), eliminación de código innecesario, orden, etc
 */
@Component("tramsNuevosAp")
@Scope("session")
public class AprobarTramitesNuevosMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -4674942725173023821L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AprobarTramitesNuevosMB.class);

    @Autowired
    private TareasPendientesMB tareaPendiente;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    // ---- variables ----
    private Tramite[] selectedTramites;

    /**
     * lista de trámites correspondiente a los trámites que están en la actividad (según el proceso)
     * y los trámites asociados a estos
     */
    private List<Tramite> tramitesNuevos;

    private List<Long> idObjetosNeg = new ArrayList<Long>();

    private List<Actividad> actividades = new ArrayList<Actividad>();

    private UsuarioDTO usuario;

    /**
     * lista que se usa para mostrar en la interfaz de usuario los trámites que finalmente quedaron
     * como aprobados al mover el proceso
     */
    private List<Tramite> tramitesAprobados;

    private List<Tramite> tramitesCancelados;

    private List<Tramite> tramitesPadreMovidos;

    private ReporteDTO comunicacion;

    private List<TramiteDocumento> listaComunicados;

    /**
     * Variable de tipo streamed content que contiene los documentos asociados al trámite
     */
    private StreamedContent documentosAsociadosATramite;

    // --------- methods -------------------
    public TareasPendientesMB getTareaPendiente() {
        return this.tareaPendiente;
    }

    public void setTareaPendiente(TareasPendientesMB tareaPendiente) {
        this.tareaPendiente = tareaPendiente;
    }

    public List<Tramite> getTramitesNuevos() {
        return this.tramitesNuevos;
    }

    public void setTramitesNuevos(List<Tramite> tramitesNuevos) {
        this.tramitesNuevos = tramitesNuevos;
    }

    public List<Long> getIdObjetosNeg() {
        return this.idObjetosNeg;
    }

    public void setIdObjetosNeg(List<Long> idObjetosNeg) {
        this.idObjetosNeg = idObjetosNeg;
    }

    public List<Actividad> getActividades() {
        return this.actividades;
    }

    public void setActividades(List<Actividad> actividades) {
        this.actividades = actividades;
    }

    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Tramite[] getSelectedTramites() {
        return this.selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public List<Tramite> getTramitesAprobados() {
        return this.tramitesAprobados;
    }

    public void setTramitesAprobados(List<Tramite> tramitesAprobados) {
        this.tramitesAprobados = tramitesAprobados;
    }

    public List<Tramite> getTramitesCancelados() {
        return tramitesCancelados;
    }

    public void setTramitesCancelados(List<Tramite> tramitesCancelados) {
        this.tramitesCancelados = tramitesCancelados;
    }

    public boolean isMostrarTablaCancelados() {
        if (this.tramitesCancelados == null || this.tramitesCancelados.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public ReporteDTO getComunicacion() {
        return comunicacion;
    }

    public void setComunicacion(ReporteDTO comunicacion) {
        this.comunicacion = comunicacion;
    }

    public List<TramiteDocumento> getListaComunicados() {
        return listaComunicados;
    }

    public void setListaComunicados(List<TramiteDocumento> listaComunicados) {
        this.listaComunicados = listaComunicados;
    }

    public StreamedContent getDocumentosAsociadosATramite() {
        return documentosAsociadosATramite;
    }

    public void setDocumentosAsociadosATramite(StreamedContent documentosAsociadosATramite) {
        this.documentosAsociadosATramite = documentosAsociadosATramite;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("en el init de AprobarTramitesNuevosMB ");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        List<Tramite> trams;
        List<Long> tramitesProvenientesDepuracion = new ArrayList<Long>();

        this.actividades = this.tareaPendiente.getListaInstanciasActividadesSeleccionadas();
        if (this.actividades != null) {

            for (Actividad a : this.actividades) {
                if (a.getObservacion() != null &&
                     (a.getObservacion()
                        .equals(ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_POR_REALIZAR
                            .toString()) || a
                        .getObservacion()
                        .equals(ProcesoDeConservacion.ACT_DEPURACION_REGISTRAR_VISITA_TERRENO
                            .toString()))) {
                    tramitesProvenientesDepuracion.add(a.getIdObjetoNegocio());
                }
                this.idObjetosNeg.add(a.getIdObjetoNegocio());
            }
            if (this.idObjetosNeg != null && !this.idObjetosNeg.isEmpty()) {
                trams = this.getTramiteService()
                    .obtenerTramitesParaAprobarPorTramiteIds(this.idObjetosNeg);

                if (trams != null) {
                    for (Tramite t : trams) {
                        if (t.getTramite() == null) {
                            if (tramitesProvenientesDepuracion.contains(t.getId())) {
                                t.setTramiteProvenienteDepuracion("Depuración.");
                            }
                            this.tramitesNuevos.add(t);
                        }
                    }
                    this.poblarPadres(this.tramitesNuevos);
                }
            }
        }
        // Verificación del estado en depuración para identificar que trámites
        // pasaron por depuración.
        // this.verificarTramitesConRegistroTramiteDepuracion();

        // Verificación de los trámites a los cuales se les asociaron trámites y
        // son provenientes de actividades en depuración.
        this.verificarTramitesProvenientesDepuracion();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo para encontrar la ID de la actividad de un tramite Padre
     *
     * @author franz.gamba
     */
    private String encontrarIdDeActividadPorIdTramite(Long tramiteId) {

        String idActividad;

        for (Actividad a : this.actividades) {
            if (a.getIdObjetoNegocio() == tramiteId) {
                idActividad = a.getId();
                return idActividad;
            }
        }
        return null;
    }

//--------------------------------------------------------------------------------------------------
    public AprobarTramitesNuevosMB() {
        this.tramitesNuevos = new ArrayList<Tramite>();
    }

//--------------------------------------------------------------------------------------------------
    //Método que asigna cada Tramite hijo a su correspondiente padre
    /**
     * Llena una lista de trámites correspondiente a los trámites que están en la actividad (según
     * el proceso) y los trámites asociados a estos.
     *
     * @author franz.gamba
     *
     * @param padres lista de trámites que están en la actividad actual del proceso
     */
    /*
     * @documented pedro.garcia
     */
    private void poblarPadres(List<Tramite> padres) {

        // franz.chamba, qué chimba tu capacidad para nombrar variables y para documentar!
        //List<Tramite> listaOrdenada = new ArrayList<Tramite>();
        List<Tramite> listaTramites = new ArrayList<Tramite>();
        List<Tramite> tList;

        for (Tramite padre : padres) {
            listaTramites.add(padre);
            tList = this.getTramiteService().
                consultarTramitesAsociadosATramiteId(padre.getId());
            if (tList != null) {
                for (Tramite tram : tList) {
                    listaTramites.add(tram);
                }
            }
        }

        this.tramitesNuevos = listaTramites;

    }

    public void borrarHijosTramite() {

        List<Tramite> listaTramites = new ArrayList<Tramite>();
        List<Tramite> tList;

        for (Tramite padre : this.tramitesPadreMovidos) {
            tList = this.getTramiteService().consultarTramitesAsociadosATramiteId(padre.getId());

            if (tList != null) {

                for (Tramite tram : tList) {
                    listaTramites.add(tram);
                }
            }

            this.tramitesNuevos.removeAll(listaTramites);
            this.tramitesNuevos.remove(padre);

        }

        this.tramitesPadreMovidos = new ArrayList<Tramite>();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que realiza las decisiones tomadas sobre los trámites. Mueve los procesos de los
     * trámites.
     *
     * @author franz.gamba
     */
    /*
     * @modified juanfelipe.garcia :: 05-09-2013 :: adición de lista para almacenar los trámites
     * cancelados
     */
    public void aplicarSolicitudActividades() {

        this.tramitesAprobados = new LinkedList<Tramite>();
        this.tramitesCancelados = new LinkedList<Tramite>();
        this.tramitesCancelados = new LinkedList<Tramite>();
        this.tramitesPadreMovidos = new LinkedList<Tramite>();

        List<String> idsTramitesHijosAprobados;
        List<Tramite> tramitesHijosAprobadosTemp;
        String activityId;
        SolicitudCatastral solicitudCatastral;
        List<UsuarioDTO> usuarios;

        if (hayTramitesSeleccionado()) {

            for (Tramite tramitePadre : this.tramitesNuevos) {

                //N: según esto, solo mueve los trámites padre
                if (tramitePadre.getTramite() == null && tramitePadre.isSeleccionAprobar()) {

                    idsTramitesHijosAprobados = new LinkedList<String>();
                    tramitesHijosAprobadosTemp = new LinkedList<Tramite>();

                    if (tramitePadre.getEstado().equals(ETramiteEstado.APROBADO.getCodigo())) {
                        this.tramitesAprobados.add(tramitePadre);
                    }

                    //D: se revisan los trámites que son hijos
                    for (Tramite tramiteHijo : this.tramitesNuevos) {

                        //D: si fue aprobado y no tiene número de radicación, se le asigna uno, se le
                        // asigna un funcionario y se actualiza
                        if (tramiteHijo.getTramite() != null && tramiteHijo.getTramite().getId().
                            equals(tramitePadre.getId()) &&
                            tramiteHijo.getEstado().equals(ETramiteEstado.APROBADO.getCodigo())) {

                            if (tramiteHijo.getNumeroRadicacion() == null) {
                                Object[] resultado;

                                resultado = this.getGeneralesService().generarNumeracion(
                                    ENumeraciones.NUMERACION_RADICACION_CATASTRAL, "0",
                                    tramiteHijo.getDepartamento().getCodigo(), tramiteHijo.
                                    getMunicipio().getCodigo(), 0);

                                if (resultado != null && resultado.length > 0) {
                                    tramiteHijo.setNumeroRadicacion(resultado[0].toString());
                                    tramiteHijo.setFechaRadicacion(new Date());
                                    tramiteHijo.setFuncionarioEjecutor(tramitePadre.
                                        getFuncionarioEjecutor());
                                    tramiteHijo.setNombreFuncionarioEjecutor(tramitePadre.
                                        getNombreFuncionarioEjecutor());
                                    tramiteHijo.setFuncionarioRadicador(this.usuario.getLogin());
                                    tramiteHijo.setNombreFuncionarioRadicador(this.usuario.
                                        getNombreCompleto());
                                    tramiteHijo = this.getTramiteService().
                                        guardarActualizarTramite2(tramiteHijo);
                                    this.tramitesAprobados.add(tramiteHijo);
                                } else {
                                    this.addMensajeError(
                                        "Ocurrió un error al generar los numeros de radicación de los nuevos trámites. Inténtelo de nuevo más tarde.");

                                }
                            } else {
                                this.tramitesAprobados.add(tramiteHijo);
                            }

                            tramitesHijosAprobadosTemp.add(tramiteHijo);

                        }
                    }

                    int j = 1;
                    for (int i = 0; j <= tramitesHijosAprobadosTemp.size() && i <
                        tramitesHijosAprobadosTemp.size(); i++) {
                        if (tramitesHijosAprobadosTemp.get(i).getOrdenEjecucion() == j) {
                            idsTramitesHijosAprobados.add(tramitesHijosAprobadosTemp.get(i).getId().
                                toString());
                            j++;
                            i = -1;
                        }
                    }

                    //D: mover el trámite padre
                    // Creacion de la solicitud catastral
                    activityId = this.encontrarIdDeActividadPorIdTramite(tramitePadre.getId());
                    this.tramitesPadreMovidos.add(tramitePadre);

                    solicitudCatastral = new SolicitudCatastral();
                    usuarios = new LinkedList<UsuarioDTO>();
                    usuarios.add(this.getGeneralesService().getCacheUsuario(tramitePadre.
                        getFuncionarioEjecutor()));
                    solicitudCatastral.setUsuarios(usuarios);

                    if (tramitePadre.getEstado().equals(ETramiteEstado.RECIBIDO.getCodigo())) {

                        if (tramitePadre.isCuarta() || tramitePadre.isRevisionAvaluo() ||
                            tramitePadre.isTramiteDeAutoavaluo()) {
                            solicitudCatastral.setTransicion(
                                ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO);
                        } else {
                            solicitudCatastral.setTransicion(
                                ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA);
                        }
                    } else if (tramitePadre.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo())) {
                        solicitudCatastral.setTransicion(
                            ProcesoDeConservacion.ACT_EJECUCION_TERMINAR_EJECUCION_TRAMITE);
                        this.tramitesCancelados.add(tramitePadre);
                    } else if (!tramitePadre.getEstado().equals(ETramiteEstado.RECIBIDO.getCodigo()) &&
                        !tramitePadre.getEstado().equals(ETramiteEstado.CANCELADO.getCodigo())) {
                        this.addMensajeError(
                            "No se aceptaron o rechazaron actividades sobre el trámite principal. Por favor revise la información e intente de nuevo.");

                    }
                    if (!idsTramitesHijosAprobados.isEmpty()) {
                        solicitudCatastral.setTramitesAsociados(idsTramitesHijosAprobados);
                    }

                    this.getProcesosService().avanzarActividad(this.usuario, activityId,
                        solicitudCatastral);

                    this.generarComunicaciones();

                }
            }
        } else {
            this.addMensajeError("Debe seleccionar por lo menos un trámite.");

        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que hace la aprobación sobre los trámites seleccionados.
     *
     * Si el trámite
     *
     * @author franz.gamba
     *
     *
     */
    /*
     * @modified felipe.cadena :: 13-01-2016 :: Se agrega logica para reclamar en el gestor de
     * procesos los tramites aprobados o sus padres
     */
    public void aprobarActividadSolicitada() {

        if (hayTramitesSeleccionado()) {

            for (Tramite t : this.tramitesNuevos) {

                //felipe.cadena::#15723::07-01-2016::Se reclama la actividad asociada al tramite, solo la primera vez
                //solo se reclaman los tramites que ya tienen radicacion, los que no tienen se debe reclamar su tramite padre
                Tramite tramiteAReclamar = t;
                if (t.getNumeroRadicacion() == null || t.getNumeroRadicacion().isEmpty()) {
                    for (Tramite tPadre : this.tramitesNuevos) {
                        if (tPadre.getTramite() != null && tPadre.getTramite().getId().equals(t.
                            getId())) {
                            tramiteAReclamar = tPadre;
                            break;
                        }
                    }
                }
                if (!tramiteAReclamar.isReclamado()) {

                    Actividad activity = this.tareaPendiente.buscarActividadPorIdTramite(
                        tramiteAReclamar.getId());
                    tramiteAReclamar.setReclamado(true);

                    if (activity != null) {
                        try {
                            this.getProcesosService().reclamarActividad(activity.getId(),
                                this.usuario);
                        } catch (ExcepcionSNC e) {

                            if (!e.getMensaje().contains("previamente")) {
                                this.addMensajeWarn(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.
                                    toString());
                                LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.
                                    getMensajeTecnico() + tramiteAReclamar.getId() + e, e);
                            }

                        }
                    }
                }

                if (t.isSeleccionAprobar()) {

                    String nuevoEstado = "";

                    if (t.getEstado().equals(ETramiteEstado.POR_APROBAR.getCodigo()) ||
                         t.getEstado().equals(ETramiteEstado.NO_APROBADO.getCodigo())) {

                        t.setProcesoInstanciaId(t.getTramite().getProcesoInstanciaId());
                        nuevoEstado = ETramiteEstado.APROBADO.getCodigo();

                    } else if (t.getEstado().equals(ETramiteEstado.POR_CANCELAR.getCodigo()) ||
                         (t.getEstado().equals(ETramiteEstado.RECIBIDO.getCodigo()) && t.
                        isPorCancelar())) {

                        nuevoEstado = ETramiteEstado.CANCELADO.getCodigo();

                    }

                    if (!nuevoEstado.isEmpty()) {
                        this.guardarTramiteEstado(t, nuevoEstado);
                    }

                }
            }
            this.addMensajeInfo("Trámites aprobados satisfactoriamente.");
        } else {
            this.addMensajeError("Debe seleccionar por lo menos un trámite.");
        }

    }

    /**
     * Método encargado de guardar el registro en la tabla trámite estado y lo asocia al trámite,
     * finalmente actualiza el trámite con el nuevo estado
     *
     * @param Tramite tramite al que se quiere guardar el registro en la tabla trámite estado
     * @param Código del trámite estado
     * @author javier.aponte
     */
    private void guardarTramiteEstado(Tramite t, String codigoTramiteEstado) {

        /*
         * Se consulta el trámite estado vigente para establecer el mismo motivo al nuevo estado que
         * se desea crear, en este caso funicona así porque el coordinador está aprobando o
         * rechazando el nuevo estado del trámite a partir del motivo que le sugirieron
         */
        TramiteEstado teTemp = this.getTramiteService().buscarTramiteEstadoVigentePorTramiteId(t.
            getId());

        if (teTemp == null && t.getTramite() != null) {
            teTemp = this.getTramiteService().buscarTramiteEstadoVigentePorTramiteId(t.getTramite().
                getId());
        }

        TramiteEstado tramEstado = new TramiteEstado();
        tramEstado.setEstado(codigoTramiteEstado);
        tramEstado.setFechaInicio(new Date());
        tramEstado.setFechaLog(new Date());

        if (teTemp != null) {
            tramEstado.setMotivo(teTemp.getMotivo());
        }

        tramEstado.setTramite(t);
        tramEstado.setUsuarioLog(this.usuario.getLogin());

        tramEstado = this.getTramiteService().actualizarTramiteEstado(
            tramEstado, this.usuario);

        try {
            t.setTramiteEstado(tramEstado);
        } catch (Exception e) {
            LOGGER.error("error actualizando el trámite-estado a " + codigoTramiteEstado +
                 " en AprobarTramitesNuevosMB#guardarTramiteEstado:" + e, e);
        }

        t.setUsuarioLog(this.usuario.getLogin());
        t.setFechaLog(new Date());
        this.getTramiteService().guardarActualizarTramite2(t);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo que rechaza los tramites seleccionados
     *
     * @author franz.gamba
     */
    /*
     * modified by javier.aponte Se hace una factorización del código, se crea método para guardar
     * el nuevo estado del trámite, y se llama desde diferentes partes de este método. 10-05-2016
     */
    public void rechazarActividadSolicitada() {

        if (hayTramitesSeleccionado()) {

            for (Tramite t : this.tramitesNuevos) {

                if (t.isSeleccionAprobar()) {

                    if (ETramiteEstado.POR_APROBAR.getCodigo().equals(t.getEstado()) ||
                        ETramiteEstado.APROBADO.getCodigo().equals(t.getEstado())) {

                        this.guardarTramiteEstado(t, ETramiteEstado.NO_APROBADO.getCodigo());

                    } else if (ETramiteEstado.POR_CANCELAR.getCodigo().equals(t.getEstado())) {

                        this.guardarTramiteEstado(t, ETramiteEstado.RECIBIDO.getCodigo());

                    } else if (ETramiteEstado.CANCELADO.getCodigo().equals(t.getEstado())) {

                        this.guardarTramiteEstado(t, ETramiteEstado.POR_CANCELAR.getCodigo());

                    } else if (ETramiteEstado.NO_APROBADO.getCodigo().equals(t.getEstado())) {

                        t.setProcesoInstanciaId(t.getTramite().getProcesoInstanciaId());
                        this.guardarTramiteEstado(t, ETramiteEstado.APROBADO.getCodigo());

                    } else if (ETramiteEstado.RECIBIDO.getCodigo().equals(t.getEstado()) &&
                         t.getTramite() == null) {
                        this.addMensajeInfo(
                            "No se puede rechazar un trámite original que se mantenga.");
                    }
                }
            }
            this.addMensajeInfo("Actividades rechazadas satisfactoriamente.");
        } else {
            this.addMensajeError("Debe seleccionar por lo menos un trámite.");
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método para el onchange del selection de Tramite Padre
     */
    public void onChangeSelectionPadre(AjaxBehaviorEvent event) {

        for (Tramite t : this.tramitesNuevos) {
            if (t.getTramite() == null && t.isSeleccionAprobar()) {
                for (int i = this.tramitesNuevos.indexOf(t) + 1; i < this.tramitesNuevos
                    .size(); i++) {
                    if (this.tramitesNuevos.get(i).getTramite() != null &&
                         this.tramitesNuevos.get(i).getTramite().getId()
                            .equals(t.getId())) {
                        this.tramitesNuevos.get(i).setSeleccionAprobar(true);
                    }
                }
            }
        }

    }

    // ------------------------------------------------------------------------------------------
    /**
     * Método para el onchange del selection de Tramite Hijo
     */
    public void onChangeDesSelectionHijo() {

        for (Tramite t : this.tramitesNuevos) {
            if (t.getTramite() != null && !t.isSeleccionAprobar()) {
                for (int i = 0; i < this.tramitesNuevos.indexOf(t); i++) {
                    if (this.tramitesNuevos.get(i).getTramite() == null &&
                         this.tramitesNuevos.get(i).getId()
                            .equals(t.getTramite().getId())) {
                        this.tramitesNuevos.get(i).setSeleccionAprobar(false);
                    }
                }
            }
        }

    }

    // ------------------------------------------------------------------------------------------
    /**
     * Valida si existe por lo menos un trámite seleccionado
     *
     * @return
     */
    private boolean hayTramitesSeleccionado() {
        boolean seleccionado = false;
        for (Tramite t : this.tramitesNuevos) {
            if (t.isSeleccionAprobar()) {
                seleccionado = true;
            }
        }
        return seleccionado;
    }

    // --------------------------------------------------------------------------------------------    
    /**
     * Método que toma la lista de trámites cancelados, nuevos Aprobados y genera las
     * comunicaciones: -Documento por cada tramite padre -Correo electrónico notificando la
     * existencia de documentos soporte de la cancelación y /o aprobación trámites, al responsable
     * de conservación
     *
     * @author juanfelipe.garcia
     * @throws Exception
     */
    public void generarComunicaciones() {
        String mensaje;
        this.listaComunicados = new ArrayList<TramiteDocumento>();
        HashMap<Long, Tramite> auxTramite = new HashMap<Long, Tramite>();
        try {
            //obtener los tramites padres
            for (Tramite t : this.tramitesNuevos) {
                if (t.isSeleccionAprobar()) {
                    if (t.getTramite() == null) {
                        auxTramite.put(t.getId(), t);
                    } else {
                        auxTramite.put(t.getTramite().getId(), t.getTramite());
                    }
                }
            }

            //Generar documento Comunicación de  cancelación y/o creación de trámites, por cada tramite padre.
            for (Map.Entry t : auxTramite.entrySet()) {
                this.guardarDocumento((Tramite) t.getValue());

            }

            //envio de correo al responsable de conservacion con documentos adjuntos
            this.enviarCorreoNotificacion();

        } catch (Exception ex) {
            mensaje = "Ocurrio un error inesperado al generar el reporte." +
                 " Por favor intente nuevamente";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------    
    /**
     * Método que genera el comunicado individual, "Comunicación de cancelación y/o creación de
     * trámites"
     *
     * @author juanfelipe.garcia
     * @throws Exception
     */
    private void generarComunicado(Long idTram) throws Exception {
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.GENERAR_COMUNICACIONES;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", idTram.toString());

        this.comunicacion = reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);
    }

//-------------------------------------------------------------------------------------------------- 
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en el gestor
     * documental y en la base de datos mediante el llamado al método guardar documento de la
     * interfaz de ITramite. Posteriormente le asocia el documento al trámite documento creado al
     * principio.
     *
     * @author juanfelipe.garcia
     *
     * @throws Exception
     */
    private void guardarDocumento(Tramite tramite) throws Exception {
        TramiteDocumento tramiteDocumento = new TramiteDocumento();
        Documento documento = new Documento();

        tramiteDocumento.setTramite(tramite);
        tramiteDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        tramiteDocumento.setUsuarioLog(this.usuario.getLogin());

        try {

            this.generarComunicado(tramite.getId());

            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.COMUNICACION_DE_CANCELACION_CREACION_TRAMITE
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.comunicacion != null) {
                documento.setArchivo(this.comunicacion.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(tramite.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());

            // Subo el archivo a el gestor documental y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.usuario, documento);

            if (documento != null &&
                 documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos()
                    .trim().isEmpty()) {
                tramiteDocumento.setDocumento(documento);
                tramiteDocumento.setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                tramiteDocumento.setfecha(new Date(System.currentTimeMillis()));

                tramiteDocumento = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumento);
                this.listaComunicados.add(tramiteDocumento);

            }

        } catch (Exception e) {
            throw new Exception("Error al guardar el oficio cancelacion en alfresco: " + e.
                getMessage());
        }

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método que envía el correo de notificación tramites aprobados/cancelados al responsable de
     * conservación
     *
     * @author juanfelipe.garcia
     */
    public void enviarCorreoNotificacion() {

        List<String> destinatariosList = new ArrayList<String>();

        try {
            List<UsuarioDTO> responsablesConservacion = (List<UsuarioDTO>) this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);

            if (responsablesConservacion != null &&
                 !responsablesConservacion.isEmpty() &&
                 responsablesConservacion.get(0) != null) {
                destinatariosList.add(responsablesConservacion.get(0).getEmail());
            }

            List<String> destinatarios = destinatariosList;

            Object[] datosCorreo = new Object[1];
            DateFormat df = DateFormat.getDateInstance();
            String datos = "";
            if (!this.tramitesCancelados.isEmpty()) {
                datos = datos.concat("Trámites cancelados");
                datos = datos.concat("<table border=\"1\">\n" +
                    "<tr>\n" +
                    "<th>No. radicación</th>\n" +
                    "<th>Fecha radicación</th>\n" +
                    "</tr>");
                for (Tramite tra : this.tramitesCancelados) {
                    String s = df.format(tra.getFechaRadicacion());
                    datos = datos.concat("<tr>\n" +
                        "<td>" + tra.getNumeroRadicacion() + "</td>\n" +
                        "<td>" + s + "</td>\n" +
                        "</tr>");
                }
                datos = datos.concat("</table><br />");
            }

            if (!this.tramitesAprobados.isEmpty()) {
                datos = datos.concat("<table border=\"1\">\n" +
                    "<tr>\n" +
                    "<th>No. radicación</th>\n" +
                    "<th>Fecha radicación</th>\n" +
                    "</tr>");
                for (Tramite tra : this.tramitesAprobados) {
                    String s = df.format(tra.getFechaRadicacion());
                    datos = datos.concat("<tr>\n" +
                        "<td>" + tra.getNumeroRadicacion() + "</td>\n" +
                        "<td>" + s + "</td>\n" +
                        "</tr>");
                }
                datos = datos.concat("</table><br />");
            }

            datosCorreo[0] = datos;//Lista de tramites Tramites 

//REVIEW :: juanfelipe.garcia :: ¿debería revisar el valor de la variable para dar alguna notificación? :: pedro.garcia            
            boolean notificacionExitosa = this.getGeneralesService()
                .enviarCorreoElectronicoConCuerpo(
                    EPlantilla.MENSAJE_COMUNICACION_CANCELACION_CREACION_TRAMITE,
                    destinatarios, null, null, datosCorreo, null,
                    null);

        } catch (Exception e) {
            LOGGER.error("Error al enviar el comunicado: " + e.getMessage(), e);
        }

    }

    // ---------------------------------------------------- //
    /**
     * Método que realiza el cargue del estado en depuración, para los trámites que vienen de dicho
     * proceso, verificando si existe o no un registro de {@link TramiteDocumento} para dichos
     * trámites.
     *
     * @author david.cifuentes
     */
    public void verificarTramitesConRegistroTramiteDepuracion() {
        if (this.tramitesNuevos != null && !this.tramitesNuevos.isEmpty()) {
            List<Long> tramiteIds = new ArrayList<Long>();
            for (Tramite t : this.tramitesNuevos) {
                tramiteIds.add(t.getId());
            }

            List<Long> tramiteIdsEnDepuracion = this.getTramiteService()
                .verificarTramitesConRegistroTramiteDepuracion(tramiteIds);

            if (tramiteIdsEnDepuracion != null &&
                 !tramiteIdsEnDepuracion.isEmpty()) {
                for (Tramite t : this.tramitesNuevos) {
                    if (tramiteIdsEnDepuracion.contains(t.getId())) {
                        t.setTramiteProvenienteDepuracion("Depuración");
                    }
                }
            }
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que verifica si el trámite es proveniente de depuración. Esto se hace revisando la
     * actividades de los trámites, y verificando si en las observaciones de las actividades está
     * asociado el nombre de la actividad previa a aprobar nuevos trámites.
     *
     * @author david.cifuentes
     */
    public void verificarTramitesProvenientesDepuracion() {
        if (this.tramitesNuevos != null && !this.tramitesNuevos.isEmpty()) {
            List<Long> tramiteIds = new ArrayList<Long>();
            for (Tramite t : this.tramitesNuevos) {
                tramiteIds.add(t.getId());
            }

            List<Long> tramiteIdsEnDepuracion = this.getTramiteService()
                .verificarTramitesConRegistroTramiteDepuracion(tramiteIds);

            if (tramiteIdsEnDepuracion != null &&
                 !tramiteIdsEnDepuracion.isEmpty()) {
                for (Tramite t : this.tramitesNuevos) {
                    if (tramiteIdsEnDepuracion.contains(t.getId())) {
                        t.setTramiteProvenienteDepuracion("Depuración");
                    }
                }
            }
        }
    }

    // ------------------------------------------------------------------------------------------
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("tramsNuevosAp");
        this.tareaPendiente.init();
        return "index";
    }
}
