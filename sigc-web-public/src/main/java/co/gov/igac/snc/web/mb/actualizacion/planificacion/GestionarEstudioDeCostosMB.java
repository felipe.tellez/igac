package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.objetosNegocio.ActualizacionCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EgresoConceptoGastoParaConvenioDTO;
import co.gov.igac.snc.web.util.ItemDeEstudioDeCostosDTO;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * SNCManagedBean para Registrar programas de trabajo, actividades, montos y cupos presupuestales en
 * el subproceso de planificacion
 *
 * @author franz.gamba
 *
 */
@Component("estudioDeCostosAct")
@Scope("session")
public class GestionarEstudioDeCostosMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionarEstudioDeCostosMB.class);

// -------------------------------------------------------------------------------------------------------
    /*
     * Variables
     */
    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    private UsuarioDTO usuario;

    /** Ruta del formato */
    private String rutaLocal = null;

    /** Nombre del departamento al que pertenece el municipio en actualización. */
    private String departamentoNombre;
    /** Nombre del municipio que va a ser actualizado. */
    private String municipioNombre;
    /** Nombre del tipo de proyecto: Misional o Convenio. */
    private String tipoProyectoNombre;
    /** Nombre la zona: urbana, rural, urbana/rural */
    private String zonaNombre;
    /** Número de convenio. */
    private String convenioNumero;
    /** lista de convenios asociados con la resolución en curso */
    private List<Convenio> convenios;

    private Actualizacion actualizacion;

    private FileInputStream libro;

    private HSSFWorkbook workbook;

    private HSSFSheet sheet;

    private List<Dominio> registros;

    private List<ItemDeEstudioDeCostosDTO> itemsEstudioCosto =
        new ArrayList<ItemDeEstudioDeCostosDTO>();
    /*
     * Banderas para mostrar los paneles segun se requiera
     */
    private boolean formatosFlag;
    private boolean estudioCostosFlag;

    private double porcentajeIgac;

    private StreamedContent archivoMostrar;

    private boolean registrarFormatos = false;
    /*
     * Para la pantalla de aprobar el estudio de costos
     */
    private List<EgresoConceptoGastoParaConvenioDTO> egresos;

    /** Elaboración de la resolución. */
    private Resolucion resolucion;

    /** Lista de objetos para los parametros del correo */
    private Object[] datosCorreo;
    private List<UsuarioDTO> destinatario;

    /** Format de los mensajes */
    private MessageFormat messageFormat;

    /** String que contiene el texto del correo */
    private String contenido;
    /** String para almacenar los comenatrios adicionales realizados por el Director Territorial */
    private String comentariosAdicionales = new String("");

    private String correoDestinatario;

    private boolean existeFormatoCostos;
    // -------------------------------------------------------------------------------------------------------

    /*
     * Métodos
     */
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getTipoProyectoNombre() {
        return tipoProyectoNombre;
    }

    public void setTipoProyectoNombre(String tipoProyectoNombre) {
        this.tipoProyectoNombre = tipoProyectoNombre;
    }

    public String getZonaNombre() {
        return zonaNombre;
    }

    public void setZonaNombre(String zonaNombre) {
        this.zonaNombre = zonaNombre;
    }

    public String getConvenioNumero() {
        return convenioNumero;
    }

    public void setConvenioNumero(String convenioNumero) {
        this.convenioNumero = convenioNumero;
    }

    public FileInputStream getLibro() {
        return libro;
    }

    public void setLibro(FileInputStream libro) {
        this.libro = libro;
    }

    public HSSFWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(HSSFWorkbook workbook) {
        this.workbook = workbook;
    }

//	public static String getRutaarchivo() {
//		return rutaArchivo;
//	}

    /*
     * Almacena los registros de la pantalla de formatos
     */
    public List<Dominio> getRegistros() {
        return registros;
    }

    public void setRegistros(List<Dominio> registros) {
        this.registros = registros;
    }

    /*
     * Lista con items para el estudio de costos
     */
    public List<ItemDeEstudioDeCostosDTO> getItemsEstudioCosto() {
        return itemsEstudioCosto;
    }

    public void setItemsEstudioCosto(
        List<ItemDeEstudioDeCostosDTO> itemsEstudioCosto) {
        this.itemsEstudioCosto = itemsEstudioCosto;
    }

    /*
     * Hoja de excel para traer la información
     */
    public HSSFSheet getSheet() {
        return sheet;
    }

    public void setSheet(HSSFSheet sheet) {
        this.sheet = sheet;
    }

    public boolean isFormatosFlag() {
        return formatosFlag;
    }

    public void setFormatosFlag(boolean formatosFlag) {
        this.formatosFlag = formatosFlag;
    }

    public boolean isEstudioCostosFlag() {
        return estudioCostosFlag;
    }

    public void setEstudioCostosFlag(boolean estudioCostosFlag) {
        this.estudioCostosFlag = estudioCostosFlag;
    }

    public double getPorcentajeIgac() {
        return porcentajeIgac;
    }

    public void setPorcentajeIgac(double porcentajeIgac) {
        this.porcentajeIgac = porcentajeIgac;
    }

    public StreamedContent getArchivoMostrar() {

        this.archivoMostrar = new DefaultStreamedContent(this.libro,
            "application/vnd.ms-excel",
            Constantes.FORMATOS_ESTUDIO_DE_COSTOS);

        return archivoMostrar;
    }

    public void setArchivoMostrar(StreamedContent archivoMostrar) {
        this.archivoMostrar = archivoMostrar;
    }

    public boolean isRegistrarFormatos() {
        return registrarFormatos;
    }

    public void setRegistrarFormatos(boolean isRegistrarFormatos) {
        this.registrarFormatos = isRegistrarFormatos;
    }

    public List<EgresoConceptoGastoParaConvenioDTO> getEgresos() {
        return egresos;
    }

    public void setEgresos(List<EgresoConceptoGastoParaConvenioDTO> egresos) {
        this.egresos = egresos;
    }

    public Resolucion getResolucion() {
        return resolucion;
    }

    public void setResolucion(Resolucion resolucion) {
        this.resolucion = resolucion;
    }

    public Object[] getDatosCorreo() {
        return datosCorreo;
    }

    public void setDatosCorreo(Object[] datosCorreo) {
        this.datosCorreo = datosCorreo;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public MessageFormat getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(MessageFormat messageFormat) {
        this.messageFormat = messageFormat;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getComentariosAdicionales() {
        return comentariosAdicionales;
    }

    public void setComentariosAdicionales(String comentariosAdicionales) {
        this.comentariosAdicionales = comentariosAdicionales;
    }

    public List<Convenio> getConvenios() {
        return convenios;
    }

    public void setConvenios(List<Convenio> convenios) {
        this.convenios = convenios;
    }

    public String getCorreoDestinatario() {
        return correoDestinatario;
    }

    public void setCorreoDestinatario(String correoDestinatario) {
        this.correoDestinatario = correoDestinatario;
    }

    public boolean isExisteFormatoCostos() {
        return existeFormatoCostos;
    }

    public void setExisteFormatoCostos(boolean existeFormatoCostos) {
        this.existeFormatoCostos = existeFormatoCostos;
    }

//-------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        Long idActualizacion = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();
        //Long idActualizacion = 250L;
        this.actualizacion = this.getActualizacionService().
            recuperarActualizacionPorId(idActualizacion);

        this.departamentoNombre = actualizacion.getDepartamento().getNombre();
        this.municipioNombre = actualizacion.getMunicipio().getNombre();
        this.convenios = getActualizacionService().
            recuperarConveniosPorIdActualizacion(actualizacion.getId());
        if (this.convenios != null && this.convenios.size() > 0) {

//TODO :: franz.gamba :: comprobar que el convenio es el priemro en la lista de convenios
            // de no serlo, cuadrar para que trabaje con el convenio que es.
            this.tipoProyectoNombre = this.convenios.get(0).getTipoProyecto();
            this.zonaNombre = this.convenios.get(0).getZona();
            this.convenioNumero = this.convenios.get(0).getNumero();
        }

        this.registros = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ACTUALIZACION_FORMATOS);
        this.formatosFlag = false;
        this.estudioCostosFlag = false;

        try {

            // @modified by juanfelipe.garcia :: 25-11-2013 :: comentariado por cambio al gestor documental (FTP), ajustar este método
            //                                                 con la logica de la nueva versión
            this.rutaLocal = null;/* this.getGeneralesService().buscarFormatoEnAlfresco(
                    Constantes.FORMATOS_ESTUDIO_DE_COSTOS_FTP); */

            File file = new File(this.rutaLocal);
            this.libro = new FileInputStream(file);
            this.workbook = new HSSFWorkbook(this.libro);
            this.sheet = this.workbook.getSheet(Constantes.ESTUDIO_DE_COSTOS);
            this.cargarEstudioDeCostos();
            this.calcularEgresos();

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error durante la carga del archivo");
        }
        if (this.sheet != null) {
            this.porcentajeIgac = this.sheet.getRow(9).getCell(1)
                .getNumericCellValue();
            this.existeFormatoCostos = true;
        } else {
            this.addMensajeWarn(
                "El archivo: Formatos calculo costos para Convenios2 2010, no se encuentra en el sistema");
            this.existeFormatoCostos = false;
        }

    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Metodo que desplega la informacion del estudio de costos a partir del archivo Excel
     *
     * @throws IOException
     *
     */
    public void cargarEstudioDeCostos() throws IOException {
        for (int i = 12; i < 51; i++) {
            HSSFRow row = this.sheet.getRow(i);
            this.itemsEstudioCosto.add(new ItemDeEstudioDeCostosDTO(row
                .getCell(1).getStringCellValue(), row.getCell(2)
                .getStringCellValue(), Double.toString(row.getCell(3)
                    .getNumericCellValue()), Double.toString(row.getCell(4)
                    .getNumericCellValue()), Double.toString(row.getCell(5)
                    .getNumericCellValue()), Double.toString(row.getCell(6)
                    .getNumericCellValue()), Double.toString(row.getCell(7)
                    .getNumericCellValue()), Double.toString(row.getCell(8)
                    .getNumericCellValue()), Double.toString(row.getCell(9)
                    .getNumericCellValue()), Double.toString(row.getCell(10)
                    .getNumericCellValue()), Double.toString(row.getCell(11)
                    .getNumericCellValue()), Double.toString(row.getCell(12)
                    .getNumericCellValue()), Double.toString(row.getCell(13)
                    .getNumericCellValue()), Double.toString(row.getCell(14)
                    .getNumericCellValue()), Double.toString(row.getCell(15)
                    .getNumericCellValue()), Double.toString(row.getCell(17)
                    .getNumericCellValue()), Double.toString(row.getCell(19)
                    .getNumericCellValue())));
        }
        this.libro.close();
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que recarga la informacion de la tabla a partir del archivo excel
     *
     */
    public void recargarTabla() {
        try {
            this.libro = new FileInputStream(rutaLocal);
            this.workbook = new HSSFWorkbook(libro);
            this.sheet = workbook.getSheet("F12000-10-10.V5");
            this.itemsEstudioCosto = new ArrayList<ItemDeEstudioDeCostosDTO>();
            this.cargarEstudioDeCostos();
            this.libro.close();

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error durante la carga del archivo");
            e.printStackTrace();
        }
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que calcula los egresos a partir de la hoja de excel con el estudio de costos
     *
     */
    public void calcularEgresos() {
        this.egresos = new ArrayList<EgresoConceptoGastoParaConvenioDTO>();

        this.egresos.add(new EgresoConceptoGastoParaConvenioDTO("",
            "Egresos programados", 0.0));
        Double contratacionManoObra = Double.parseDouble(this.itemsEstudioCosto
            .get(8).getTotal()) +
             Double.parseDouble(this.itemsEstudioCosto.get(14).getTotal()) +
             Double.parseDouble(this.itemsEstudioCosto.get(15).getTotal());
        this.egresos
            .add(new EgresoConceptoGastoParaConvenioDTO("1",
                "Contratación mano de obra + IVA asumido",
                contratacionManoObra));

        Double viaticos = Double.parseDouble(this.itemsEstudioCosto.get(9)
            .getTotal()) +
             Double.parseDouble(this.itemsEstudioCosto.get(16).getTotal());
        this.egresos.add(new EgresoConceptoGastoParaConvenioDTO("2",
            "Viáticos y gastos de comisión", viaticos));

        Double adquisicionDeBienes = Double.parseDouble(this.itemsEstudioCosto
            .get(4).getTotal()) +
             Double.parseDouble(this.itemsEstudioCosto.get(5).getTotal()) +
             Double.parseDouble(this.itemsEstudioCosto.get(11).getTotal());
        this.egresos.add(new EgresoConceptoGastoParaConvenioDTO("3",
            "Adquisición de bienes, insumos y servicios",
            adquisicionDeBienes));

        Double mantenimiento = Double.parseDouble(this.itemsEstudioCosto
            .get(17).getTotal());
        this.egresos.add(new EgresoConceptoGastoParaConvenioDTO("4",
            "Mantenimiento", mantenimiento));

        this.egresos.get(0).setTotal(
            contratacionManoObra + viaticos + adquisicionDeBienes +
             mantenimiento);
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que avanza el preoceso despues del registro de formatos
     *
     */
    public void enviarRegistroFormatos() {
        try {
            String idActividad = new String("250");

            List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
            listaUsuarios.add(this.usuario);
            String transicion = ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_ESTUDIO_COSTOS_PROG;
            ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
            actualizacionCatastral.setIdentificador(this.actualizacion.getId());
            actualizacionCatastral.setIdDepartamento(
                this.actualizacion.getDepartamento().getCodigo());
            actualizacionCatastral.setIdMunicipio(this.actualizacion.getMunicipio().getCodigo());
            actualizacionCatastral.setResultado(
                "El registro de formatos para el estudio de costos se envía a revisión");
            actualizacionCatastral.setUsuarios(listaUsuarios);
            actualizacionCatastral.setTransicion(transicion);
            actualizacionCatastral = this.getProcesosService().avanzarActividad(
                idActividad, actualizacionCatastral);

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al mover el proceso");
            e.printStackTrace();
        }
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que avanza el proceso despues de la revision del estudio de costos
     *
     */
    public void avanzarVerificacionDelEstudioDeCostos() {
        try {
            //Este idActividad debe venir del objeto actividad que vendra del arbol
            String idActividad = new String("250");

            List<UsuarioDTO> listaUsuarios = new ArrayList<UsuarioDTO>();
            listaUsuarios.add(this.usuario);
            String transicion = ProcesoDeActualizacion.ACT_PLANIFICACION_INGRESAR_CONVENIO;
            ActualizacionCatastral actualizacionCatastral = new ActualizacionCatastral();
            actualizacionCatastral.setIdentificador(this.actualizacion.getId());
            actualizacionCatastral.setIdDepartamento(
                this.actualizacion.getDepartamento().getCodigo());
            actualizacionCatastral.setIdMunicipio(this.actualizacion.getMunicipio().getCodigo());
            actualizacionCatastral
                .setResultado("El registro de formatos ya revisado procede al ingreso del convenio");
            actualizacionCatastral.setUsuarios(listaUsuarios);
            actualizacionCatastral.setTransicion(transicion);
            actualizacionCatastral = this.getProcesosService().avanzarActividad(
                idActividad, actualizacionCatastral);

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al mover el proceso");
            e.printStackTrace();
        }
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que inicializa los parametros del correo
     *
     * @modified pedro.garcia 13-06-2012 uso de constante para extensión de email
     */
    public void obtenerDatosCorreo() {
        Calendar hoy = Calendar.getInstance();
        Date fecha = new java.util.Date(hoy.getTimeInMillis());

        this.datosCorreo = new Object[7];
        this.datosCorreo[0] = this.usuario.getLogin() +
            ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC;
        String codigoTerritorialUOC = (this.usuario.isUoc()) ? this.usuario.getCodigoUOC() :
            this.usuario.getCodigoTerritorial();

        this.destinatario = this.getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(
                codigoTerritorialUOC,
                ERol.RESPONSABLE_ACTUALIZACION);
        if (destinatario != null) {
            this.correoDestinatario = destinatario.get(0).getLogin() + "@" +
                 Constantes.DOMINIO_IGAC;
        } else {
            this.addMensajeError(
                "No existen usuarios con el rol Responsable Actualización en esta territorial.");
        }
        this.datosCorreo[1] = this.correoDestinatario;
        this.datosCorreo[2] = fecha;
        this.datosCorreo[3] = this.tipoProyectoNombre;
        this.datosCorreo[4] = this.actualizacion.getMunicipio().getNombre();
        this.datosCorreo[5] = this.comentariosAdicionales;
        this.datosCorreo[6] = this.usuario.getNombreCompleto();
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que inicializa los parametros del correo
     *
     */
    public void revisarEstudioDeCostos() {
        this.obtenerDatosCorreo();

        Plantilla plantilla = this.getGeneralesService()
            .recuperarPlantillaPorCodigo("PLANTILLA_MENSAJE_REVISION_ESTUDIO_DE_COSTOS");
        if (null != plantilla) {
            contenido = plantilla.getHtml();
            if (null == contenido) {
                super.addMensajeError("Imposible recuperar la plantilla: " +
                     "PLANTILLA_MENSAJE_REVISION_ESTUDIO_DE_COSTOS");
            } else {

                Locale colombia = new Locale("ES", "es_CO");
                this.messageFormat = new MessageFormat(this.contenido, colombia);

                this.contenido = this.messageFormat.format(this.datosCorreo);
            }
        }

    }
    // -------------------------------------------------------------------------------------------------------

    /**
     * Método que envia a revisión el estudio de costos que llega al directorTerritorial
     */
    public String enviarARevisarEstudioDeCostos() {

        this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(),
            this.actualizacion,
            this.getGeneralesService().getCacheUsuario(
                this.actualizacion.getResponsableDiagnostico()),
            ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_ESTUDIO_COSTOS,
            this.usuario.getDescripcionTerritorial());

        UtilidadesWeb.removerManagedBean("estudioDeCostosAct");
        this.tareasPendientesMB.init();
        return "index";
    }
    // -------------------------------------------------------------------------------------------------------

    /**
     * Método que Envia el correo electrónico
     *
     */
    public void enviarCorreoRevision() {
        String[] adjuntos = new String[0];
        String[] titulos = new String[0];

        this.obtenerDatosCorreo();

        Plantilla plantilla = this.getGeneralesService()
            .recuperarPlantillaPorCodigo("MENSAJE_PARA_ENVIO_REVISION_ESTUDIO_DE_COSTOS");
        if (null != plantilla) {
            contenido = plantilla.getHtml();
            if (null == contenido) {
                super.addMensajeError("Imposible recuperar la plantilla: " +
                     "MENSAJE_PARA_ENVIO_REVISION_ESTUDIO_DE_COSTOS");
            } else {

                Locale colombia = new Locale("ES", "es_CO");
                this.messageFormat = new MessageFormat(this.contenido, colombia);

                this.contenido = this.messageFormat.format(this.datosCorreo);
            }
        }

        this.getGeneralesService().enviarCorreo(this.correoDestinatario,
            "Solicitud de Revisión para el Estudio de costos",
            this.contenido, adjuntos, titulos);

        this.getActualizacionService().ordenarRevisionEstudioDeCostos(
            this.currentActivity.getId(), this.actualizacion, this.destinatario);
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que avanza el proceso
     */
    public String enviarEstudioDeCostos() {
        List<UsuarioDTO> directorTerritorial = this.getGeneralesService().
            obtenerFuncionarioTerritorialYRol(this.usuario.getCodigoTerritorial(),
                ERol.DIRECTOR_TERRITORIAL);
        if (directorTerritorial != null) {
            this.getActualizacionService().ordenarVerificarEstudioDeCostos(
                this.currentActivity.getId(), this.actualizacion, directorTerritorial);
        } else {
            this.addMensajeError("No existe un usuario Director Territorial para esta territorial.");
        }

        UtilidadesWeb.removerManagedBean("estudioDeCostosAct");
        this.tareasPendientesMB.init();
        return "index";
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que aprueba el estudio de costos y o envia al registro de convenios
     */
    public String aprobarEstudioDeCostos() {
        List<UsuarioDTO> profesionalAbogado = this.getGeneralesService().
            obtenerFuncionarioTerritorialYRol(this.usuario.getCodigoTerritorial(), ERol.ABOGADO);

        if (profesionalAbogado != null) {
            this.getActualizacionService().ordenarIngresarConvenios(
                this.currentActivity.getId(), this.actualizacion, profesionalAbogado);
        } else {
            this.addMensajeError("No existe un usuario Profesional Abogado para esta territorial.");
        }

        UtilidadesWeb.removerManagedBean("estudioDeCostosAct");
        this.tareasPendientesMB.init();
        return "index";
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que aprueba los montos y cupos presupuestales del proceso de actualizacion
     */
    public String aprobarMontosYCupos() {

        String territorial = this.getGeneralesService().
            getEstructuraOrgSuperiorPorMunicipioCod(this.actualizacion.getMunicipio().getCodigo());
        boolean procesoFlag = false;

        //TODO::franz.gamba:: reemplazar el valor de la transicion por el que se encuentra en la enumeracion
        procesoFlag = this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion,
            ERol.ABOGADO, "ProyectarResolucionOrdenandoEjecucion",
            territorial);
        if (procesoFlag) {
            UtilidadesWeb.removerManagedBean("estudioDeCostosAct");
            this.tareasPendientesMB.init();
            return "index";
        } else {
            this.addMensajeError("Ha ocurrido un error al mover el proceso.");
            return null;
        }
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que cierra la pantalla
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("estudioDeCostosAct");
        this.tareasPendientesMB.init();
        return "index";
    }

}
