package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumento;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EOrdenActualizacionDocumento;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 *
 * @author franz.gamba
 *
 */
@Component("registrarDocumentosDeTramiteMunicipal")
@Scope("session")
public class RegistrarDocumentosDeTramiteMunicipalMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -2501424680686186849L;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    // ---------------------------------------------------------------------------------------------------------------------
    /*
     * Variables
     */
    private UsuarioDTO usuario;

    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionarEstudioDeCostosMB.class);

    /** Nombre del departamento al que pertenece el municipio en actualización. */
    private String departamentoNombre;
    /** Nombre del municipio que va a ser actualizado. */
    private String municipioNombre;
    /** Nombre del tipo de proyecto: Misional o Convenio. */
    private String tipoProyectoNombre;
    /** Nombre la zona: urbana, rural, urbana/rural */
    private String zonaNombre;
    /** Número de convenio. */
    private String convenioNumero;
    /** lista de convenios asociados con la resolución en curso */
    private List<Convenio> convenios;
    private Actualizacion actualizacion;

    /** Lista para almacenar los documentos requeridos */
    private List<ActualizacionDocumento> documentosInicio;

    /** Banderas para las pantallas de generacion de documentos */
    private boolean adicionarSolicitudFlag = false;
    private boolean relacionPrediosIncoderFlag = false;
    private boolean prediosYZonasFlag = false;
    private boolean resguardosIndigenasFlag = false;
    private boolean parquesNaturalesFlag = false;
    private boolean relacionPrediosAlcaldiaFlag = false;

    /** Datos requeridos en los reportes a generar */
    private String tratamiento;
    private String nombreRepresentante;
    private String cargoRepresentante;
    private String nombreDeLaEntidad;
    private String direccionEntidad;

    private String nombreDirector;
    private String direccion;

    private String nombreAlcalde;

    /** Listado de tratamientos para los destinatarios en el SelectItem */
    private List<SelectItem> tratamientoDestinatario;

    /** Documento seleccionado en la lista de documentos de inicio */
    private ActualizacionDocumento documentoSeleccionado;

    // ---------------------------------------------------------------------------------------------------------------------
    /*
     * Métodos
     */
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getTipoProyectoNombre() {
        return tipoProyectoNombre;
    }

    public void setTipoProyectoNombre(String tipoProyectoNombre) {
        this.tipoProyectoNombre = tipoProyectoNombre;
    }

    public String getZonaNombre() {
        return zonaNombre;
    }

    public void setZonaNombre(String zonaNombre) {
        this.zonaNombre = zonaNombre;
    }

    public String getConvenioNumero() {
        return convenioNumero;
    }

    public void setConvenioNumero(String convenioNumero) {
        this.convenioNumero = convenioNumero;
    }

    public List<Convenio> getConvenios() {
        return convenios;
    }

    public void setConvenios(List<Convenio> convenios) {
        this.convenios = convenios;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<ActualizacionDocumento> getDocumentosInicio() {
        return documentosInicio;
    }

    public void setDocumentosInicio(
        List<ActualizacionDocumento> documentosInicio) {
        this.documentosInicio = documentosInicio;
    }

    public boolean isAdicionarSolicitudFlag() {
        return adicionarSolicitudFlag;
    }

    public void setAdicionarSolicitudFlag(boolean adicionarSolicitudFlag) {
        this.adicionarSolicitudFlag = adicionarSolicitudFlag;
    }

    public boolean isRelacionPrediosIncoderFlag() {
        return relacionPrediosIncoderFlag;
    }

    public void setRelacionPrediosIncoderFlag(boolean relacionPrediosIncoderFlag) {
        this.relacionPrediosIncoderFlag = relacionPrediosIncoderFlag;
    }

    public boolean isPrediosYZonasFlag() {
        return prediosYZonasFlag;
    }

    public void setPrediosYZonasFlag(boolean prediosYZonasFlag) {
        this.prediosYZonasFlag = prediosYZonasFlag;
    }

    public boolean isResguardosIndigenasFlag() {
        return resguardosIndigenasFlag;
    }

    public void setResguardosIndigenasFlag(boolean resguardosIndigenasFlag) {
        this.resguardosIndigenasFlag = resguardosIndigenasFlag;
    }

    public boolean isParquesNaturalesFlag() {
        return parquesNaturalesFlag;
    }

    public void setParquesNaturalesFlag(boolean parquesNaturalesFlag) {
        this.parquesNaturalesFlag = parquesNaturalesFlag;
    }

    public boolean isRelacionPrediosAlcaldiaFlag() {
        return relacionPrediosAlcaldiaFlag;
    }

    public void setRelacionPrediosAlcaldiaFlag(
        boolean relacionPrediosAlcaldiaFlag) {
        this.relacionPrediosAlcaldiaFlag = relacionPrediosAlcaldiaFlag;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getNombreRepresentante() {
        return nombreRepresentante;
    }

    public void setNombreRepresentante(String nombreRepresentante) {
        this.nombreRepresentante = nombreRepresentante;
    }

    public String getCargoRepresentante() {
        return cargoRepresentante;
    }

    public void setCargoRepresentante(String cargoRepresentante) {
        this.cargoRepresentante = cargoRepresentante;
    }

    public String getNombreDeLaEntidad() {
        return nombreDeLaEntidad;
    }

    public void setNombreDeLaEntidad(String nombreDeLaEntidad) {
        this.nombreDeLaEntidad = nombreDeLaEntidad;
    }

    public String getDireccionEntidad() {
        return direccionEntidad;
    }

    public void setDireccionEntidad(String direccionEntidad) {
        this.direccionEntidad = direccionEntidad;
    }

    public String getNombreDirector() {
        return nombreDirector;
    }

    public void setNombreDirector(String nombreDirector) {
        this.nombreDirector = nombreDirector;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombreAlcalde() {
        return nombreAlcalde;
    }

    public void setNombreAlcalde(String nombreAlcalde) {
        this.nombreAlcalde = nombreAlcalde;
    }

    public List<SelectItem> getTratamientoDestinatario() {
        return tratamientoDestinatario;
    }

    public void setTratamientoDestinatario(
        List<SelectItem> tratamientoDestinatario) {
        this.tratamientoDestinatario = tratamientoDestinatario;
    }

    public ActualizacionDocumento getDocumentoSeleccionado() {
        return documentoSeleccionado;
    }

    public void setDocumentoSeleccionado(
        ActualizacionDocumento documentoSeleccionado) {
        this.documentoSeleccionado = documentoSeleccionado;
    }

    // ---------------------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.currentActivity = this.tareasPendientesMB
            .getInstanciaSeleccionada();

        Long idActualizacion = this.tareasPendientesMB
            .getInstanciaSeleccionada().getIdObjetoNegocio();

        //long idActualizacion = 250;
        this.actualizacion = this.getActualizacionService()
            .recuperarActualizacionPorId(idActualizacion);

        this.departamentoNombre = actualizacion.getDepartamento().getNombre();
        this.municipioNombre = actualizacion.getMunicipio().getNombre();
        this.convenios = this.getActualizacionService()
            .recuperarConveniosPorIdActualizacion(actualizacion.getId());
        if (convenios != null && convenios.size() > 0) {

            // TODO franz.gamba :: comprobar que el convenio es el primero en la
            // lista de convenios
            // de no serlo, cuadrar para que trabaje con el convenio que es.
            this.tipoProyectoNombre = this.convenios.get(0).getTipoProyecto();
            this.zonaNombre = this.convenios.get(0).getZona();
            this.convenioNumero = this.convenios.get(0).getNumero();
        }
        this.documentosInicio = new ArrayList<ActualizacionDocumento>();
        this.documentoSeleccionado = new ActualizacionDocumento();
        crearDocumentosDeInicio();
        inicializaDestinatarios();
    }

    // ----------------------------------------------------------------------------------------------------------------
    /**
     * Método para crear los documentos de inicio para el respectivo proceso de Actualización
     */
    public void crearDocumentosDeInicio() {
        Documento relacionPrediosIncoder = new Documento();
        Documento prediosYZonasDesplazamiento = new Documento();
        Documento resguardosIndigenas = new Documento();
        Documento parquesNaturales = new Documento();
        Documento relacionPrediosAlcaldia = new Documento();

        TipoDocumento tipodoc = new TipoDocumento();
        // TODO::franz.gamba:: validar con el analista el tipo de documento
        tipodoc = this.getGeneralesService()
            .buscarTipoDocumentoPorId(ETipoDocumento.OTRO.getId());
        /** Creacion de los Documentos de inicio */
        String nombre = "Solicitud a INCODER: Relación de predios";
        relacionPrediosIncoder = this.inicializaDocumento(
            relacionPrediosIncoder, nombre, tipodoc);

        nombre = "Solicitud a INCODER: Predios y zonas en situación de desplazamiento";
        prediosYZonasDesplazamiento = this.inicializaDocumento(
            prediosYZonasDesplazamiento, nombre, tipodoc);

        nombre = "Solicitud a INCODER: Resguardos indígenas";
        resguardosIndigenas = this.inicializaDocumento(resguardosIndigenas,
            nombre, tipodoc);

        nombre = "Solicitud a MINAMBIENTE: Parques naturales nacionales";
        parquesNaturales = this.inicializaDocumento(parquesNaturales, nombre,
            tipodoc);

        nombre = "Solicitud de relación de predios: Alcaldía municipal";
        relacionPrediosAlcaldia = this.inicializaDocumento(
            relacionPrediosAlcaldia, nombre, tipodoc);
        /** Asociacion con el proceso de actualizacion */
        ActualizacionDocumento actualizaciondoc1 = this
            .inicializaActualizacionDocumento(relacionPrediosIncoder,
                EOrdenActualizacionDocumento.PRIMERO);
        ActualizacionDocumento actualizaciondoc2 = this
            .inicializaActualizacionDocumento(prediosYZonasDesplazamiento,
                EOrdenActualizacionDocumento.SEGUNDO);
        ActualizacionDocumento actualizaciondoc3 = this
            .inicializaActualizacionDocumento(resguardosIndigenas,
                EOrdenActualizacionDocumento.TERCERO);
        ActualizacionDocumento actualizaciondoc4 = this
            .inicializaActualizacionDocumento(parquesNaturales,
                EOrdenActualizacionDocumento.CUARTO);
        ActualizacionDocumento actualizaciondoc5 = this
            .inicializaActualizacionDocumento(relacionPrediosAlcaldia,
                EOrdenActualizacionDocumento.QUINTO);

        /** Asignacion a la lista de los documentos de inicio */
        this.documentosInicio.add(actualizaciondoc1);
        this.documentosInicio.add(actualizaciondoc2);
        this.documentosInicio.add(actualizaciondoc3);
        this.documentosInicio.add(actualizaciondoc4);
        this.documentosInicio.add(actualizaciondoc5);

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Metodo que inicializa un documento con los datos requeridos para su creacion en la base de
     * datos
     */
    public Documento inicializaDocumento(Documento doc, String nombre,
        TipoDocumento tipodoc) {
        doc.setTipoDocumento(tipodoc);
        doc.setEstado(EDocumentoEstado.CARGADO.toString());
        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setFechaLog(new Date());
        doc.setArchivo(nombre);
        doc.setBloqueado("NO");
        doc.setUsuarioLog(this.usuario.getLogin());
        doc = this.getGeneralesService().guardarDocumento(doc);//

        return doc;

    }

    // --------------------------------------------------------------------------------------------
    /**
     * Metodo que inicializa un ActualizacionDocumento con los datos requeridos para su creacion en
     * la base de datos
     */
    public ActualizacionDocumento inicializaActualizacionDocumento(
        Documento doc, EOrdenActualizacionDocumento orden) {

        ActualizacionDocumento actualizacionDoc = new ActualizacionDocumento();
        actualizacionDoc.setActualizacion(this.actualizacion);
        actualizacionDoc.setDocumento(doc);
        actualizacionDoc.setFechaLog(new Date());
        actualizacionDoc.setUsuarioLog(this.usuario.getLogin());
        actualizacionDoc.setOpcionDocumentoActualizacion(orden.getValor());
        this.getGeneralesService().guardarActualizacionDocumento(actualizacionDoc);
        return actualizacionDoc;
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Metodo que inicializa la lista de tratamientos a destinatarios
     */
    public void inicializaDestinatarios() {
        this.tratamientoDestinatario = new ArrayList<SelectItem>();
        for (String s : Constantes.TRATAMIENTOS_DESTINATARIOS) {
            this.tratamientoDestinatario.add(new SelectItem(s, s));
        }
    }

    // --------------------------------------------------------------------------------------------
    public void seleccionarDocumentoAgenerar() {
        if (this.documentoSeleccionado.getOpcionDocumentoActualizacion()
            .equals("1")) {
            this.adicionarSolicitudFlag = false;
            this.relacionPrediosIncoderFlag = true;
            this.prediosYZonasFlag = false;
            this.resguardosIndigenasFlag = false;
            this.parquesNaturalesFlag = false;
            this.relacionPrediosAlcaldiaFlag = false;
        }
        if (this.documentoSeleccionado.getOpcionDocumentoActualizacion()
            .equals("2")) {
            this.adicionarSolicitudFlag = false;
            this.relacionPrediosIncoderFlag = false;
            this.prediosYZonasFlag = true;
            this.resguardosIndigenasFlag = false;
            this.parquesNaturalesFlag = false;
            this.relacionPrediosAlcaldiaFlag = false;
        }
        if (this.documentoSeleccionado.getOpcionDocumentoActualizacion()
            .equals("3")) {
            this.adicionarSolicitudFlag = false;
            this.relacionPrediosIncoderFlag = false;
            this.prediosYZonasFlag = false;
            this.resguardosIndigenasFlag = true;
            this.parquesNaturalesFlag = false;
            this.relacionPrediosAlcaldiaFlag = false;
        }
        if (this.documentoSeleccionado.getOpcionDocumentoActualizacion()
            .equals("4")) {
            this.adicionarSolicitudFlag = false;
            this.relacionPrediosIncoderFlag = false;
            this.prediosYZonasFlag = false;
            this.resguardosIndigenasFlag = false;
            this.parquesNaturalesFlag = true;
            this.relacionPrediosAlcaldiaFlag = false;
        }
        if (this.documentoSeleccionado.getOpcionDocumentoActualizacion()
            .equals("5")) {
            this.adicionarSolicitudFlag = false;
            this.relacionPrediosIncoderFlag = false;
            this.prediosYZonasFlag = false;
            this.resguardosIndigenasFlag = false;
            this.parquesNaturalesFlag = false;
            this.relacionPrediosAlcaldiaFlag = true;
        }

    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que finaliza el registro de documentos
     */
    public String finalizarRegistroDeDocumentosTramiteMunicipal() {
        //TODO: actualizar el objeto actualizacion con las modificaciones que se realizan en
        // el caso de uso

        this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion,
            ERol.DIRECTOR_TERRITORIAL,
            ProcesoDeActualizacion.ACT_PLANIFICACION_REGISTRAR_LUGAR_TRABAJO_SUMINISTRO_DOTACION,
            this.usuario.getDescripcionTerritorial());

        UtilidadesWeb.removerManagedBean("registrarDocumentosDeTramiteMunicipal");
        this.tareasPendientesMB.init();
        return "index";
    }
    //---------------------------------------------------------------------------------------------

    /**
     * Método que cierra la pantalla
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("registrarDocumentosDeTramiteMunicipal");
        this.tareasPendientesMB.init();
        return "index";
    }
}
