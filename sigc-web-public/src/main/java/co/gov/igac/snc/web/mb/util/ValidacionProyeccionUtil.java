package co.gov.igac.snc.web.mb.util;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.FichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizModelo;
import co.gov.igac.snc.persistence.entity.conservacion.PFmModeloConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import co.gov.igac.snc.persistence.entity.conservacion.PManzanaVereda;
import co.gov.igac.snc.persistence.entity.conservacion.PPersona;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteRectificacion;
import co.gov.igac.snc.persistence.util.EMutacionClase;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.controller.AbstractLocator;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilidad para realizar validaciones sobre los datos de una proyección, dentro de ellos
 * identificar los valores Cancela/Inscribe que se encuentran incompletos o corruptos y generan
 * problemas al consolidar los cambios del {@link Tramite} y verificar la existencia de información
 * en las tablas de proyección.
 *
 * @see CC-NP-CO-071 Realizar asignación de Cancela/Inscribe
 * @see CC-NP-CO-103 Realizar ajuste para proyección de trámite
 *
 * @author david.cifuentes
 * @version 1.0
 */
public class ValidacionProyeccionUtil extends AbstractLocator {

    private static final long serialVersionUID = -7294890330465997108L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ValidacionProyeccionUtil.class);

    /** ------------------------------------------------------ */
    /** ------------ INSTANCIA DE LA CLASE ------------------- */
    /** ------------------------------------------------------ */
    private static ValidacionProyeccionUtil validacionProyeccion;

    // Estados cancela/inscribe
    private static final String CANCELA = EProyeccionCancelaInscribe.CANCELA.getCodigo();
    private static final String INSCRIBE = EProyeccionCancelaInscribe.INSCRIBE.getCodigo();
    private static final String MODIFICA = EProyeccionCancelaInscribe.MODIFICA.getCodigo();

    // Mensajes de validación por tabla que deben tener registros obigatorios
    private static final String VALIDACION_P_PREDIO = "predios";
    private static final String VALIDACION_P_PERSONA_PREDIO = "propietarios";
    private static final String VALIDACION_P_PERSONA = "personas propietarias";
    private static final String VALIDACION_P_PERSONA_PREDIO_PROPIEDAD =
        "justificaciones de propiedad";
    private static final String VALIDACION_P_PREDIO_ZONA = "zonas";
    private static final String VALIDACION_P_PREDIO_AVALUO_CATASTRAL = "inscripciones y decretos";
    private static final String VALIDACION_P_PREDIO_DIRECCION = "direcciones";
    private static final String VALIDACION_P_PREDIO_SERVIDUMBRE = "servidumbres";
    private static final String VALIDACION_P_FICHA_MATRIZ = "datos de la ficha matriz";
    private static final String VALIDACION_P_FICHA_MATRIZ_PREDIO = "predios de la ficha matriz";
    private static final String VALIDACION_P_FICHA_MATRIZ_TORRE = "torres del PH";
    private static final String VALIDACION_P_MANZANA_VEREDA = "manzanas";
    private static final String VALIDACION_P_REFERENCIA_CARTOGRAFICA = "referencias cartográficas";
    private static final String VALIDACION_P_PREDIO_FECHA_INSCRIPCION =
        "fecha de inscripción catastral";

    private static final boolean PERSISTENCIA = true;

    private ValidacionProyeccionUtil() {

    }

    public static ValidacionProyeccionUtil getInstance() {
        if (validacionProyeccion == null) {
            validacionProyeccion = new ValidacionProyeccionUtil();
        }
        return validacionProyeccion;
    }

    /** ------------------------------------------------------ */
    /** -------------------- VARIABLES ---------------------- */
    /** ------------------------------------------------------ */
    private Tramite tramite;
    private PPredio predio;
    private UsuarioDTO usuario;
    private List<PPredio> prediosDesenglobe;
    private PFichaMatriz fichaMatriz;

    private List<String[]> cambiosTramite;
    private List<String> mensajeValidacion;

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public PPredio getPredio() {
        return predio;
    }

    public void setPredio(PPredio predio) {
        this.predio = predio;
    }

    public List<PPredio> getPrediosDesenglobe() {
        return prediosDesenglobe;
    }

    public void setPrediosDesenglobe(List<PPredio> prediosDesenglobe) {
        this.prediosDesenglobe = prediosDesenglobe;
    }

    public List<String[]> getCambiosTramite() {
        return cambiosTramite;
    }

    public void setCambiosTramite(List<String[]> cambiosTramite) {
        this.cambiosTramite = cambiosTramite;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    /** ------------------------------------------------------ */
    /** ----------------- M É T O D O S --------------------- */
    /** ------------------------------------------------------ */
    public PFichaMatriz getFichaMatriz() {
        return fichaMatriz;
    }

    public void setFichaMatriz(PFichaMatriz fichaMatriz) {
        this.fichaMatriz = fichaMatriz;
    }

    public List<String> getMensajeValidacion() {
        return mensajeValidacion;
    }

    public void setMensajeValidacion(List<String> mensajeValidacion) {
        this.mensajeValidacion = mensajeValidacion;
    }

    /**
     * Método principal para la validación de Cancela/Inscribe, éste controla el llamado a los
     * métodos de validación para cada una de las mutaciones.
     *
     * @param tramite
     * @param usuario
     */
    public void validarCancelaInscribeProyeccion(Tramite tramite, UsuarioDTO usuario) {

        LOGGER.debug("Inicio validación Cancela/Inscribe");
        try {
            this.cambiosTramite = new ArrayList<String[]>();

            tramite = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(tramite.getId());

            this.setTramite(tramite);
            this.setUsuario(usuario);
            this.consultarPredio();

            EMutacionClase claseMutacion = tramite.getEMutacionClase();
            if (claseMutacion != null) {
                switch (claseMutacion) {
                    case PRIMERA:
                        this.validarInscripcionesYDecretos();
                        this.validarPropietarios();
                        break;
                    case SEGUNDA:
                        this.consultarPrediosDesenglobe();
                        this.validarConstruccionesAsociadasMutacionSegunda();
                        this.validarInscripcionesYDecretosMutacionSegunda();
                        this.validarPropietariosMutacionSegunda();
                        this.consultarFichaMatrizDesenglobe();
                        this.validarFichaMatrizMutacionSegundaDesenglobe();
                        break;
                    case TERCERA:
                        this.validarConstruccionesCI();
                        this.validarConstruccionesAsociadas();
                        this.validarFotosConstruccionesCI();
                        break;
                    case CUARTA:
                        break;
                    case QUINTA:
                        break;
                    default:
                        break;
                }
            }

            this.logCambiosTramite();
        } catch (Exception e) {
            LOGGER.error("Error durante la validación de Cancela/Inscribe");
        }
        LOGGER.debug("Fin validación Cancela/Inscribe");
    }

    /**
     * Método que realiza la consulta de {@link PPredio} asociado al {@link Tramite} para mutaciones
     * diferentes a desenglobes
     */
    private void consultarPredio() {
        try {
            this.predio = this.getConservacionService().buscarPPredioPorTramite(this.tramite);
        } catch (Exception e) {
            LOGGER.error("Error consultando el predio" + e.getMessage());
        }
    }

    /**
     * Método que realiza la consulta del listado de {@link PPredio} asociado al {@link Tramite}
     * para mutaciones de desenglobes
     */
    private void consultarPrediosDesenglobe() {
        try {
            if (tramite.isSegundaDesenglobe()) {
                // Predio base
                this.predio = this.getConservacionService()
                    .obtenerPPredioCompletoById(this.tramite.getPredio().getId());

                // Predios desenglobe
                this.prediosDesenglobe = this.getConservacionService()
                    .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                        this.tramite.getId());
            }
        } catch (Exception e) {
            LOGGER.error("Error consultando el predio" + e.getMessage());
        }
    }

    /**
     * Método que realiza la consulta de la {@link PFichaMatriz} asociado al {@link PPredio} del
     * {@link Tramite} para mutaciones de desenglobe
     */
    private void consultarFichaMatrizDesenglobe() {
        try {
            if (tramite.isSegundaDesenglobe()) {

                // Consulta ficha matriz
                if (this.predio != null && this.predio.isEsPredioFichaMatriz()) {
                    this.setFichaMatriz(this.getConservacionService().buscarPFichaMatrizPorPredioId(
                        this.predio.getId()));
                } else {
                    if (this.prediosDesenglobe != null) {
                        Long idPredioFM = null;
                        for (PPredio pp : this.prediosDesenglobe) {
                            if (pp.isEsPredioFichaMatriz()) {
                                idPredioFM = pp.getId();
                                break;
                            }
                        }
                        this.setFichaMatriz(this.getConservacionService().
                            buscarPFichaMatrizPorPredioId(idPredioFM));
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error consultando el predio" + e.getMessage());
        }
    }

    /**
     * Método que realiza una revisión de las {@link PUnidadConstruccion} de un {@link PPredio} y
     * sus componentes asociados {@link PUnidadConstruccionComp} y {@link PFoto}
     */
    private void validarConstruccionesCI() {
        if (this.predio.getPUnidadConstruccions() != null) {

            List<PUnidadConstruccion> pUnidadConstruccionsActualizar =
                new ArrayList<PUnidadConstruccion>();
            List<PFoto> pFotosActualizar = new ArrayList<PFoto>();

            for (PUnidadConstruccion pu : this.predio.getPUnidadConstruccions()) {
                // Si una unidad se ha cancelado, sus componentes asociados se deben cancelar.
                if (CANCELA.equals(pu.getCancelaInscribe())) {
                    for (PUnidadConstruccionComp puc : pu.getPUnidadConstruccionComps()) {
                        if (!CANCELA.equals(puc.getCancelaInscribe())) {
                            this.registroCambios(puc.getClass().getSimpleName(), puc.getId(), puc.
                                getCancelaInscribe(), CANCELA);
                            puc.setCancelaInscribe(CANCELA);
                            pUnidadConstruccionsActualizar.add(pu);
                        }
                    }
                } // Si una unidad se ha inscrito, sus componentes asociados se deben inscribir.
                else if (INSCRIBE.equals(pu.getCancelaInscribe())) {
                    for (PUnidadConstruccionComp puc : pu.getPUnidadConstruccionComps()) {
                        if (!INSCRIBE.equals(puc.getCancelaInscribe())) {
                            this.registroCambios(puc.getClass().getSimpleName(), puc.getId(), puc.
                                getCancelaInscribe(), INSCRIBE);
                            puc.setCancelaInscribe(INSCRIBE);
                            pUnidadConstruccionsActualizar.add(pu);
                        }
                    }
                }
            }

            if (PERSISTENCIA && !pUnidadConstruccionsActualizar.isEmpty()) {
                this.getConservacionService().guardarListaPUnidadConstruccion(
                    pUnidadConstruccionsActualizar);
            }

            if (PERSISTENCIA && !pFotosActualizar.isEmpty()) {
                this.getConservacionService().guardarPFotos(pFotosActualizar);
            }
        }
    }

    /**
     * Método que realiza una revisión de las {@link PFoto} de una {@link PUnidadConstruccion}
     * asociada a un {@link PPredio} de tal manera que coincidan los valores cancela inscribe,
     * tomando como prioridad el valor cancela inscribe de la {@link PUnidadConstruccion}
     */
    private void validarFotosConstruccionesCI() {

        if (this.predio.getPUnidadConstruccions() != null) {

            List<PFoto> pFotosUnidadConstruccionsActualizar = new ArrayList<PFoto>();

            for (PUnidadConstruccion puc : this.predio.getPUnidadConstruccions()) {

                List<PFoto> pFotosUnidadConstruccion = this.getConservacionService()
                    .buscarPFotosUnidadConstruccion(puc.getId(), false);

                if (pFotosUnidadConstruccion != null) {
                    for (PFoto pf : pFotosUnidadConstruccion) {

                        if ((puc.getCancelaInscribe() != null && !puc.getCancelaInscribe().equals(
                            pf.getCancelaInscribe())) ||
                             (puc.getCancelaInscribe() == null && pf.getCancelaInscribe() != null)) {

                            this.registroCambios(pf.getClass().getSimpleName(), pf.getId(), pf.
                                getCancelaInscribe(), puc.getCancelaInscribe());
                            pf.setCancelaInscribe(puc.getCancelaInscribe());
                            pFotosUnidadConstruccionsActualizar.add(pf);
                        }
                    }
                }
            }

            if (PERSISTENCIA && !pFotosUnidadConstruccionsActualizar.isEmpty()) {
                this.getConservacionService().actualizarPFotos(pFotosUnidadConstruccionsActualizar);
            }
        }
    }

    /**
     * Método que realiza la validación de las construcciones asociadas para una mutación que no sea
     * de segunda
     */
    private void validarConstruccionesAsociadas() {
        List<PUnidadConstruccion> pUCAsociadas = new ArrayList<PUnidadConstruccion>();
        if (this.predio.getPUnidadConstruccions() != null) {
            for (PUnidadConstruccion pu : this.predio.getPUnidadConstruccions()) {
                // Construcciones asociadas
                if (pu.getProvieneUnidad() != null &&
                     pu.getProvienePredio() != null &&
                     INSCRIBE.equals(pu.getCancelaInscribe())) {
                    // Al asociar la construcción, se inscribio una nueva
                    // construcción y la construcción base debe estar cancelada
                    PUnidadConstruccion pUCBase = this.buscarConstruccionBase(this.predio.
                        getPUnidadConstruccions(), pu.getProvieneUnidad(), pu.getProvienePredio());
                    if (pUCBase != null) {
                        pUCAsociadas.add(pUCBase);
                    }
                }
            }
            if (!pUCAsociadas.isEmpty()) {

                List<PUnidadConstruccion> pUnidadConstruccionsActualizar =
                    new ArrayList<PUnidadConstruccion>();
                List<PUnidadConstruccionComp> pUnidadConstruccionsCompActualizar =
                    new ArrayList<PUnidadConstruccionComp>();

                // Lista de construcciones iniciales que fueron asociadas
                for (PUnidadConstruccion pu : pUCAsociadas) {
                    if (!CANCELA.equals(pu.getCancelaInscribe())) {
                        this.registroCambios(pu.getClass().getName(), pu.getId(), pu.
                            getCancelaInscribe(), CANCELA);
                        pu.setCancelaInscribe(CANCELA);
                        pUnidadConstruccionsActualizar.add(pu);

                        if (pu.getPUnidadConstruccionComps() != null && !pu.
                            getPUnidadConstruccionComps().isEmpty()) {
                            for (PUnidadConstruccionComp puc : pu.getPUnidadConstruccionComps()) {
                                if (!CANCELA.equals(puc.getCancelaInscribe())) {
                                    this.registroCambios(puc.getClass().getName(), puc.getId(), puc.
                                        getCancelaInscribe(), CANCELA);
                                    puc.setCancelaInscribe(CANCELA);
                                    pUnidadConstruccionsCompActualizar.add(puc);
                                }
                            }
                        }
                    }
                }

                if (PERSISTENCIA) {
                    if (!pUnidadConstruccionsActualizar.isEmpty()) {
                        this.getConservacionService().guardarListaPUnidadConstruccion(
                            pUnidadConstruccionsActualizar);
                    }
                    if (!pUnidadConstruccionsCompActualizar.isEmpty()) {
                        this.getConservacionService().actualizarPUnidadConstruccionCompRegistros(
                            pUnidadConstruccionsCompActualizar);
                    }
                }
            }
        }
    }

    /**
     * Método que busca en una lista de {@link PUnidadConstruccion} una construcción asociada a una
     * unidad inicial y a un predio inicial que fue asociada en la proyección
     *
     * @param pUnidadConstruccions
     * @param provieneUnidad
     * @param provienePredio
     * @return
     */
    private PUnidadConstruccion buscarConstruccionBase(
        List<PUnidadConstruccion> pUnidadConstruccions,
        String provieneUnidad, Predio provienePredio) {
        for (PUnidadConstruccion pu : pUnidadConstruccions) {
            if (pu.getPPredio().getId().longValue() == provienePredio.getId().longValue() &&
                 pu.getUnidad().equals(provieneUnidad)) {
                return pu;
            }
        }
        return null;
    }

    /**
     * Método que realiza la validación de las construcciones asociadas para una mutación de segunda
     */
    private void validarConstruccionesAsociadasMutacionSegunda() {

        List<PUnidadConstruccion> pUCAsociadas = new ArrayList<PUnidadConstruccion>();

        // Identificar construcciones asociadas de los predios desenglobados
        if (this.prediosDesenglobe != null) {
            for (PPredio p : this.prediosDesenglobe) {
                if (p.getPUnidadConstruccions() != null && !p.getPUnidadConstruccions().isEmpty()) {
                    for (PUnidadConstruccion pu : p.getPUnidadConstruccions()) {
                        // Construcciones asociadas
                        if (pu.getProvieneUnidad() != null &&
                             pu.getProvienePredio() != null &&
                             INSCRIBE.equals(pu.getCancelaInscribe())) {

                            PUnidadConstruccion pUCBase = this.buscarConstruccionBase(this.predio.
                                getPUnidadConstruccions(), pu.getProvieneUnidad(), pu.
                                getProvienePredio());
                            if (pUCBase != null) {
                                pUCAsociadas.add(pUCBase);
                            }
                        }
                    }
                }
            }

            if (!pUCAsociadas.isEmpty()) {
                List<PUnidadConstruccion> pUnidadConstruccionsActualizar =
                    new ArrayList<PUnidadConstruccion>();
                List<PUnidadConstruccionComp> pUnidadConstruccionsCompActualizar =
                    new ArrayList<PUnidadConstruccionComp>();

                if (PERSISTENCIA) {
                    if (!pUnidadConstruccionsActualizar.isEmpty()) {
                        this.getConservacionService().guardarListaPUnidadConstruccion(
                            pUnidadConstruccionsActualizar);
                    }
                    if (!pUnidadConstruccionsCompActualizar.isEmpty()) {
                        this.getConservacionService().actualizarPUnidadConstruccionCompRegistros(
                            pUnidadConstruccionsCompActualizar);
                    }
                }
            }
        }
    }

    /**
     * Método que realiza las validaciones sobre {@link PPredioAvaluoCatastral}, principalmente
     * valida que no se inscriban avaluos para vigencias existentes, esto es para {@link Tramite}
     * que no sea de desenglobe
     */
    private void validarInscripcionesYDecretos() {

        Map<String, PredioAvaluoCatastral> vigenciasExistentes =
            new HashMap<String, PredioAvaluoCatastral>();

        // Avaluos proyectados
        List<PPredioAvaluoCatastral> pPredioAvaluoCatastrals = this
            .getConservacionService()
            .obtenerListaPPredioAvaluoCatastralPorIdPPredio(this.predio.getId());

        // Avaluos predio base
        Predio predioAux = this.getConservacionService().getPredioFetchAvaluosByNumeroPredial(
            this.predio.getNumeroPredial());

        if (predioAux != null) {
            if (predioAux.getPredioAvaluoCatastrals() != null && !predioAux.
                getPredioAvaluoCatastrals().isEmpty()) {
                for (PredioAvaluoCatastral pac : predioAux.getPredioAvaluoCatastrals()) {
                    Calendar vigenciaPAC = Calendar.getInstance();
                    vigenciaPAC.setTime(pac.getVigencia());
                    vigenciasExistentes.put("" + vigenciaPAC.get(Calendar.YEAR), pac);
                }
            }
        }

        if (pPredioAvaluoCatastrals != null) {

            List<PPredioAvaluoCatastral> predioAvaluoCatastralActualizar =
                new ArrayList<PPredioAvaluoCatastral>();

            for (PPredioAvaluoCatastral ppac : pPredioAvaluoCatastrals) {
                Calendar vigenciaPAC = Calendar.getInstance();
                vigenciaPAC.setTime(ppac.getVigencia());
                String vigencia = "" + vigenciaPAC.get(Calendar.YEAR);
                if (vigenciasExistentes.get(vigencia) != null) {

                    if (ppac.getCancelaInscribe() == null) {
                        // Verificar que no se han actualizado datos, en caso que si, se coloca M
                        PredioAvaluoCatastral pacConsolidado = vigenciasExistentes.get(vigencia);
                        if ((pacConsolidado.getAutoavaluo() != null && !pacConsolidado.
                            getAutoavaluo().equals(ppac.getAutoavaluo())) ||
                             (pacConsolidado.getJustificacionAvaluo() != null && !pacConsolidado.
                            getJustificacionAvaluo().equals(ppac.getJustificacionAvaluo())) ||
                             (pacConsolidado.getNotaAvaluo() != null && !pacConsolidado.
                            getNotaAvaluo().equals(ppac.getNotaAvaluo())) ||
                             (pacConsolidado.getValorConstruccionComun() != null &&
                            !(pacConsolidado.getValorConstruccionComun().doubleValue() == ppac.
                            getValorConstruccionComun().doubleValue())) ||
                             (pacConsolidado.getValorTerreno() != null && !(pacConsolidado.
                            getValorTerreno().doubleValue() == ppac.getValorTerreno().doubleValue())) ||
                             (pacConsolidado.getValorTerrenoComun() != null && !(pacConsolidado.
                            getValorTerrenoComun().doubleValue() == ppac.getValorTerrenoComun().
                                doubleValue())) ||
                             (pacConsolidado.getValorTerrenoPrivado() != null && !(pacConsolidado.
                            getValorTerrenoPrivado().doubleValue() == ppac.getValorTerrenoPrivado().
                                doubleValue())) ||
                             (pacConsolidado.getValorTotalAvaluoCatastral() != null &&
                            !(pacConsolidado.getValorTotalAvaluoCatastral().doubleValue() == ppac.
                            getValorTotalAvaluoCatastral().doubleValue())) ||
                             (pacConsolidado.getValorTotalAvaluoVigencia() != null &&
                            !(pacConsolidado.getValorTotalAvaluoVigencia().doubleValue() == ppac.
                            getValorTotalAvaluoVigencia().doubleValue())) ||
                             (pacConsolidado.getValorTotalConsNconvencional() != null &&
                            !(pacConsolidado.getValorTotalConsNconvencional().doubleValue() == ppac.
                            getValorTotalConsNconvencional().doubleValue())) ||
                             (pacConsolidado.getValorTotalConstruccion() != null &&
                            !(pacConsolidado.getValorTotalConstruccion().doubleValue() == ppac.
                            getValorTotalConstruccion().doubleValue())) ||
                             (pacConsolidado.getValorTotalConstruccionPrivada() != null &&
                            !(pacConsolidado.getValorTotalConstruccionPrivada().doubleValue() ==
                            ppac.getValorTotalConstruccionPrivada().doubleValue())) ||
                             (pacConsolidado.getVigencia() != null && !pacConsolidado.getVigencia().
                            equals(ppac.getVigencia()))) {
                            this.registroCambios(ppac.getClass().getSimpleName(), ppac.getId(),
                                ppac.getCancelaInscribe(), MODIFICA);
                            ppac.setCancelaInscribe(MODIFICA);
                            predioAvaluoCatastralActualizar.add(ppac);
                        }
                    } else if (!MODIFICA.equals(ppac.getCancelaInscribe())) {

                        //Si el cancela inscribe está en null verificar que no se hicieron cambios
                        this.registroCambios(ppac.getClass().getSimpleName(), ppac.getId(), ppac.
                            getCancelaInscribe(), MODIFICA);
                        ppac.setCancelaInscribe(MODIFICA);
                        predioAvaluoCatastralActualizar.add(ppac);
                    }
                } else if (ppac.getCancelaInscribe() == null) {
                    this.registroCambios(ppac.getClass().getSimpleName(), ppac.getId(), ppac.
                        getCancelaInscribe(), INSCRIBE);
                    ppac.setCancelaInscribe(INSCRIBE);
                    predioAvaluoCatastralActualizar.add(ppac);
                }
            }

            if (PERSISTENCIA && !predioAvaluoCatastralActualizar.isEmpty()) {
                for (PPredioAvaluoCatastral pac : predioAvaluoCatastralActualizar) {
                    this.getConservacionService().actualizarPPredioAvaluoCatastral(pac);
                }
            }
        }
    }

    /**
     * Método que realiza las validaciones sobre {@link PPredioAvaluoCatastral}, principalmente
     * valida que no se inscriban avaluos para vigencias existentes, esto es para todos los predios
     * asociados {@link Tramite} cuando es un desenglobe
     */
    private void validarInscripcionesYDecretosMutacionSegunda() {

        if (this.prediosDesenglobe != null) {
            for (PPredio p : this.prediosDesenglobe) {

                Map<String, PredioAvaluoCatastral> vigenciasExistentes =
                    new HashMap<String, PredioAvaluoCatastral>();

                // Avaluos proyectados
                List<PPredioAvaluoCatastral> pPredioAvaluoCatastrals = this
                    .getConservacionService()
                    .obtenerListaPPredioAvaluoCatastralPorIdPPredio(p.getId());

                // Avaluos predio base
                Predio predioAux = this.getConservacionService().
                    getPredioFetchAvaluosByNumeroPredial(p.getNumeroPredial());

                if (predioAux != null) {
                    if (predioAux.getPredioAvaluoCatastrals() != null && !predioAux.
                        getPredioAvaluoCatastrals().isEmpty()) {
                        for (PredioAvaluoCatastral pac : predioAux.getPredioAvaluoCatastrals()) {
                            Calendar vigenciaPAC = Calendar.getInstance();
                            vigenciaPAC.setTime(pac.getVigencia());
                            vigenciasExistentes.put("" + vigenciaPAC.get(Calendar.YEAR), pac);
                        }
                    }
                }

                if (pPredioAvaluoCatastrals != null) {

                    List<PPredioAvaluoCatastral> predioAvaluoCatastralActualizar =
                        new ArrayList<PPredioAvaluoCatastral>();

                    for (PPredioAvaluoCatastral ppac : pPredioAvaluoCatastrals) {
                        Calendar vigenciaPAC = Calendar.getInstance();
                        vigenciaPAC.setTime(ppac.getVigencia());
                        String vigencia = "" + vigenciaPAC.get(Calendar.YEAR);
                        if (vigenciasExistentes.get(vigencia) != null) {
                            if (ppac.getCancelaInscribe() == null) {
                                // Verificar que no se han actualizado datos, en caso que si, se coloca M
                                PredioAvaluoCatastral pacConsolidado = vigenciasExistentes.get(
                                    vigencia);
                                if ((pacConsolidado.getAutoavaluo() != null && !pacConsolidado.
                                    getAutoavaluo().equals(ppac.getAutoavaluo())) ||
                                     (pacConsolidado.getJustificacionAvaluo() != null &&
                                    !pacConsolidado.getJustificacionAvaluo().equals(ppac.
                                        getJustificacionAvaluo())) ||
                                     (pacConsolidado.getNotaAvaluo() != null && !pacConsolidado.
                                    getNotaAvaluo().equals(ppac.getNotaAvaluo())) ||
                                     (pacConsolidado.getValorConstruccionComun() != null &&
                                    !(pacConsolidado.getValorConstruccionComun().doubleValue() ==
                                    ppac.getValorConstruccionComun().doubleValue())) ||
                                     (pacConsolidado.getValorTerreno() != null && !(pacConsolidado.
                                    getValorTerreno().doubleValue() == ppac.getValorTerreno().
                                        doubleValue())) ||
                                     (pacConsolidado.getValorTerrenoComun() != null &&
                                    !(pacConsolidado.getValorTerrenoComun().doubleValue() == ppac.
                                    getValorTerrenoComun().doubleValue())) ||
                                     (pacConsolidado.getValorTerrenoPrivado() != null &&
                                    !(pacConsolidado.getValorTerrenoPrivado().doubleValue() == ppac.
                                    getValorTerrenoPrivado().doubleValue())) ||
                                     (pacConsolidado.getValorTotalAvaluoCatastral() != null &&
                                    !(pacConsolidado.getValorTotalAvaluoCatastral().doubleValue() ==
                                    ppac.getValorTotalAvaluoCatastral().doubleValue())) ||
                                     (pacConsolidado.getValorTotalAvaluoVigencia() != null &&
                                    !(pacConsolidado.getValorTotalAvaluoVigencia().doubleValue() ==
                                    ppac.getValorTotalAvaluoVigencia().doubleValue())) ||
                                     (pacConsolidado.getValorTotalConsNconvencional() != null &&
                                    !(pacConsolidado.getValorTotalConsNconvencional().doubleValue() ==
                                    ppac.getValorTotalConsNconvencional().doubleValue())) ||
                                     (pacConsolidado.getValorTotalConstruccion() != null &&
                                    !(pacConsolidado.getValorTotalConstruccion().doubleValue() ==
                                    ppac.getValorTotalConstruccion().doubleValue())) ||
                                     (pacConsolidado.getValorTotalConstruccionPrivada() != null &&
                                    !(pacConsolidado.getValorTotalConstruccionPrivada().
                                        doubleValue() == ppac.getValorTotalConstruccionPrivada().
                                        doubleValue())) ||
                                     (pacConsolidado.getVigencia() != null && !pacConsolidado.
                                    getVigencia().equals(ppac.getVigencia()))) {
                                    this.registroCambios(ppac.getClass().getSimpleName(), ppac.
                                        getId(), ppac.getCancelaInscribe(), MODIFICA);
                                    ppac.setCancelaInscribe(MODIFICA);
                                    predioAvaluoCatastralActualizar.add(ppac);
                                }
                            } else if (!MODIFICA.equals(ppac.getCancelaInscribe())) {
                                this.registroCambios(ppac.getClass().getSimpleName(), ppac.getId(),
                                    ppac.getCancelaInscribe(), MODIFICA);
                                ppac.setCancelaInscribe(MODIFICA);
                                predioAvaluoCatastralActualizar.add(ppac);
                            }
                        } else if (ppac.getCancelaInscribe() == null) {
                            PredioAvaluoCatastral pac = this.getConservacionService().
                                getPredioAvaluoCatastralById(ppac.getId());
                            if (pac == null) {
                                this.registroCambios(ppac.getClass().getSimpleName(), ppac.getId(),
                                    ppac.getCancelaInscribe(), INSCRIBE);
                                ppac.setCancelaInscribe(INSCRIBE);
                                predioAvaluoCatastralActualizar.add(ppac);
                            }
                        }
                    }

                    if (PERSISTENCIA && !predioAvaluoCatastralActualizar.isEmpty()) {
                        for (PPredioAvaluoCatastral pac : predioAvaluoCatastralActualizar) {
                            this.getConservacionService().actualizarPPredioAvaluoCatastral(pac);
                        }
                    }
                }
            }
        }
    }

    /**
     * Valida la situación de la situación de la {@link PFichaMatriz} en el {@link Tramite} de
     * desenglobe
     */
    private void validarFichaMatrizMutacionSegundaDesenglobe() {
        if (!this.tramite.isEnglobeVirtual()) {//Validacion no aplica paa englobe virtual
            if (this.fichaMatriz != null) {

                // Validar la existencia de la ficha matriz
                FichaMatriz fichaMatrizEnFirme = this.getConservacionService().
                    buscarFichaMatrizPorPredioId(this.fichaMatriz.getPPredio().getId());


                if (this.fichaMatriz.getCancelaInscribe() == null ||
                     MODIFICA.equals(this.fichaMatriz.getCancelaInscribe()) ||
                     CANCELA.equals(this.fichaMatriz.getCancelaInscribe())) {

                    if (PERSISTENCIA && fichaMatrizEnFirme == null) {
                        this.fichaMatriz.setCancelaInscribe(INSCRIBE);
                        this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);
                    }
                } else if (INSCRIBE.equals(this.fichaMatriz.getCancelaInscribe())) {
                    if (PERSISTENCIA && fichaMatrizEnFirme != null) {
                        this.fichaMatriz.setCancelaInscribe(MODIFICA);
                        this.getConservacionService().actualizarFichaMatriz(this.fichaMatriz);
                    }
                }
            }
        }
    }

    /**
     * Valida la situación de los propietarios basados en los {@link PPersonaPredio} asociados al
     * {@link PPredio}
     */
    private void validarPropietarios() {

        if (this.predio.getPPersonaPredios() != null) {

            List<PPersona> personasActualizar = new ArrayList<PPersona>();

            for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
                if (INSCRIBE.equals(pp.getCancelaInscribe())) {
                    if (pp.getPPersona() != null) {
                        if (MODIFICA.equals(pp.getPPersona().getCancelaInscribe())) {

                            // Validar la existencia de la persona
                            List<Persona> personas = this.getConservacionService().
                                buscarPersonaPorNumeroYTipoIdentificacion(pp.getPPersona().
                                    getTipoIdentificacion(), pp.getPPersona().
                                        getNumeroIdentificacion());

                            if (personas != null && !personas.isEmpty()) {
                                boolean existeId = false;
                                for (Persona p : personas) {
                                    if (p.getId().longValue() == pp.getPPersona().getId().
                                        longValue()) {
                                        existeId = true;
                                    }
                                }
                                if (!existeId) {
                                    this.
                                        registroCambios(pp.getPPersona().getClass().getSimpleName(),
                                            pp.getPPersona().getId(), pp.getPPersona().
                                            getCancelaInscribe(), INSCRIBE);
                                    pp.getPPersona().setCancelaInscribe(INSCRIBE);
                                    personasActualizar.add(pp.getPPersona());
                                }
                            } else {
                                this.registroCambios(pp.getPPersona().getClass().getSimpleName(),
                                    pp.getPPersona().getId(), pp.getPPersona().getCancelaInscribe(),
                                    INSCRIBE);
                                pp.getPPersona().setCancelaInscribe(INSCRIBE);
                                personasActualizar.add(pp.getPPersona());
                            }
                        }
                    } else {
                        this.registroCambios(pp.getPPersona().getClass().getSimpleName(), pp.
                            getPPersona().getId(), pp.getPPersona().getCancelaInscribe(), INSCRIBE);
                        pp.getPPersona().setCancelaInscribe(INSCRIBE);
                        personasActualizar.add(pp.getPPersona());
                    }
                }
            }

            if (PERSISTENCIA && !personasActualizar.isEmpty()) {
                for (PPersona persona : personasActualizar) {
                    this.getConservacionService().actualizarPPersona(persona);
                }
            }
        }
    }

    /**
     * Valida la situación de los propietarios basados en los {@link PPersonaPredio} asociados al
     * todos los predios del {@link Tramite} para mutaciones de segunda desenglobe
     */
    private void validarPropietariosMutacionSegunda() {
        if (this.prediosDesenglobe != null) {

            List<PPersona> personasActualizar = new ArrayList<PPersona>();

            for (PPredio predio : this.prediosDesenglobe) {
                if (predio.getPPersonaPredios() != null) {
                    for (PPersonaPredio pp : this.predio.getPPersonaPredios()) {
                        if (INSCRIBE.equals(pp.getCancelaInscribe())) {
                            if (pp.getPPersona() != null) {
                                if (MODIFICA.equals(pp.getPPersona().getCancelaInscribe())) {

                                    // Validar la existencia de la persona
                                    List<Persona> personas = this.getConservacionService().
                                        buscarPersonaPorNumeroYTipoIdentificacion(pp.getPPersona().
                                            getTipoIdentificacion(), pp.getPPersona().
                                                getNumeroIdentificacion());

                                    if (personas != null && !personas.isEmpty()) {
                                        boolean existeId = false;
                                        for (Persona p : personas) {
                                            if (p.getId().longValue() == pp.getPPersona().getId().
                                                longValue()) {
                                                existeId = true;
                                            }
                                        }
                                        if (!existeId) {
                                            this.registroCambios(pp.getPPersona().getClass().
                                                getSimpleName(), pp.getPPersona().getId(), pp.
                                                getPPersona().getCancelaInscribe(), INSCRIBE);
                                            pp.getPPersona().setCancelaInscribe(INSCRIBE);
                                            personasActualizar.add(pp.getPPersona());
                                        }
                                    } else {
                                        this.registroCambios(pp.getPPersona().getClass().
                                            getSimpleName(), pp.getPPersona().getId(), pp.
                                            getPPersona().getCancelaInscribe(), INSCRIBE);
                                        pp.getPPersona().setCancelaInscribe(INSCRIBE);
                                        personasActualizar.add(pp.getPPersona());
                                    }
                                }
                            } else {
                                this.registroCambios(pp.getPPersona().getClass().getSimpleName(),
                                    pp.getPPersona().getId(), pp.getPPersona().getCancelaInscribe(),
                                    INSCRIBE);
                                pp.getPPersona().setCancelaInscribe(INSCRIBE);
                                personasActualizar.add(pp.getPPersona());
                            }
                        }
                    }
                }
            }

            if (PERSISTENCIA && !personasActualizar.isEmpty()) {
                for (PPersona persona : personasActualizar) {
                    this.getConservacionService().actualizarPPersona(persona);
                }
            }
        }
    }

    /**
     * Método principal para la verificación de existencia de datos en las tablas de proyección,
     * éste controla el llamado a los métodos de verificación para cada una de las mutaciones.
     *
     * @param tramite
     * @param usuario
     * @return
     */
    public boolean verificarDatosProyeccion(Tramite tramite) {

        LOGGER.debug("Inicio verificación datos de la proyección");
        boolean datosProyeccionValidos = true;
        this.mensajeValidacion = new ArrayList<String>();

        try {

            tramite = this.getTramiteService().buscarTramitePorTramiteIdProyeccion(tramite.getId());
            this.setTramite(tramite);
            this.consultarPredio();
            this.consultarPrediosDesenglobe();

            ETramiteTipoTramite tipoTramite = tramite.getETipoTramite();
            EMutacionClase claseMutacion = tramite.getEMutacionClase();

            // Verificación de datos básicos, cómun a todos los trámites
            datosProyeccionValidos = this.verificacionDatosBasicos();

            if (datosProyeccionValidos) {
                if (tipoTramite != null) {
                    switch (tipoTramite) {
                        case AUTOESTIMACION:
                            break;
                        case CANCELACION_DE_PREDIO:
                            break;
                        case COMPLEMENTACION:
                            if (this.verificacionDatosFotos() ||
                                 this.verificacionDatosFMConstruccionComponente(false) ||
                                 this.verificarDatosModeloConstruccion(false)) {
                                datosProyeccionValidos = true;
                            } else {
                                datosProyeccionValidos = false;
                            }
                            break;
                        case MODIFICACION_INSCRIPCION_CATASTRAL:
                            break;
                        case MUTACION:
                            if (claseMutacion != null) {
                                switch (claseMutacion) {
                                    case PRIMERA:
                                        break;
                                    case SEGUNDA:
                                        if (this.verificarDatosFichaMatriz() ||
                                             this.verificacionDatosFotos() ||
                                             this.verificacionDatosFMConstruccionComponente(true) ||
                                             this.verificarDatosModeloConstruccion(true) ||
                                             this.verificarDatosDesenglobeManzana()) {
                                            datosProyeccionValidos = true;
                                        } else {
                                            datosProyeccionValidos = false;
                                        }
                                        break;
                                    case TERCERA:
                                        break;
                                    case CUARTA:
                                        break;
                                    case QUINTA:
                                        if (this.verificarServidumbres() ||
                                             this.verificacionDatosFotos() ||
                                             this.verificacionDatosReferenciaCartografica() ||
                                             this.verificacionDatosFMConstruccionComponente(false) ||
                                             this.verificarDatosModeloConstruccion(false)) {
                                            datosProyeccionValidos = true;
                                        } else {
                                            datosProyeccionValidos = false;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        case RECTIFICACION:
                            if (this.verificacionDatosFotos() ||
                                 this.verificacionDatosFMConstruccionComponente(false) ||
                                 this.verificarDatosModeloConstruccion(false)) {
                                datosProyeccionValidos = true;
                            } else {
                                datosProyeccionValidos = false;
                            }
                            break;
                        case REVISION_DE_AVALUO:
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error durante la verificación de datos de la proyección");
        }
        LOGGER.debug("Fin verificación datos de la proyección");
        return datosProyeccionValidos;
    }

    /**
     * Método encargado de validar la existencia de información para las tablas: P_PREDIO,
     * P_PERSONA, P_PERSONA_PREDIO, P_PERSONA_PREDIO_PROPIEDAD, P_PREDIO_ZONA,
     * P_PREDIO_AVALUO_CATASTRAL, P_UNIDAD_CONSTRUCCION_COMP, P_UNIDAD_CONSTRUCCION,
     * P_PREDIO_DIRECCION.
     *
     * @return
     */
    private boolean verificacionDatosBasicos() {

        LOGGER.info("|| -------------------------------------------------- ||");
        LOGGER.info("|| ---------- Log validación datos básicos ---------- ||");
        LOGGER.info("|| -------------------------------------------------- ||");
        boolean datosProyeccionValidos = true;
        List<PPredio> prediosValidar = new ArrayList<PPredio>();

        try {

            if (this.tramite.isSegundaDesenglobe()) {
                prediosValidar.addAll(this.prediosDesenglobe);
            } else {
                prediosValidar.add(this.predio);
            }

            // P_PREDIO
            if (prediosValidar == null || prediosValidar.isEmpty()) {
                LOGGER.info("No se encontraron registros para la tabla P_PREDIO ");
                if (!this.mensajeValidacion.contains(VALIDACION_P_PREDIO)) {
                    this.mensajeValidacion.add(VALIDACION_P_PREDIO);
                }
                return false;
            } else {
                for (PPredio pp : prediosValidar) {

                    // P_PREDIO
                    if (pp.getFechaInscripcionCatastral() == null) {
                        LOGGER.info(
                            "No se encontró la fecha de inscripción catastral para los registros de la tabla P_PREDIO ");
                        if (!this.mensajeValidacion.contains(VALIDACION_P_PREDIO_FECHA_INSCRIPCION)) {
                            this.mensajeValidacion.add(MessageFormat.format(
                                Constantes.MENSAJE_ERROR_PREDIO, pp.getNumeroPredial(),
                                VALIDACION_P_PREDIO_FECHA_INSCRIPCION));
                        }
                        return false;
                    }
                    if (!pp.isEsPredioFichaMatriz()) {
                        // P_PERSONA_PREDIO
                        if (pp.getPPersonaPredios() == null || pp.getPPersonaPredios().isEmpty()) {
                            LOGGER.info(
                                "No se encontraron registros para la tabla P_PERSONA_PREDIO ");
                            if (!this.mensajeValidacion.contains(VALIDACION_P_PERSONA_PREDIO)) {
                                this.mensajeValidacion.add(MessageFormat.format(
                                    Constantes.MENSAJE_ERROR_PREDIO, pp.getNumeroPredial(),
                                    VALIDACION_P_PERSONA_PREDIO));
                            }
                            return false;
                        } else {
                            for (PPersonaPredio ppp : pp.getPPersonaPredios()) {
                                if (INSCRIBE.equals(ppp.getCancelaInscribe())) {
                                    // P_PERSONA
                                    if (ppp.getPPersona() == null || ppp.getPPersona().getId() ==
                                        null) {
                                        LOGGER.info(
                                            "No se encontraron registros para la tabla P_PERSONA ");
                                        if (!this.mensajeValidacion.contains(VALIDACION_P_PERSONA)) {
                                            this.mensajeValidacion.add(MessageFormat.format(
                                                Constantes.MENSAJE_ERROR_PREDIO, pp.
                                                    getNumeroPredial(), VALIDACION_P_PERSONA));
                                        }
                                        return false;
                                    }
                                    // P_PERSONA_PREDIO_PROPIEDAD
                                    if (ppp.getPPersonaPredioPropiedads() == null || ppp.
                                        getPPersonaPredioPropiedads().isEmpty()) {
                                        LOGGER.info(
                                            "No se encontraron registros para la tabla P_PERSONA_PREDIO_PROPIEDAD ");
                                        if (!this.mensajeValidacion.contains(
                                            VALIDACION_P_PERSONA_PREDIO_PROPIEDAD)) {
                                            this.mensajeValidacion.add(MessageFormat.format(
                                                Constantes.MENSAJE_ERROR_PREDIO, pp.
                                                    getNumeroPredial(),
                                                VALIDACION_P_PERSONA_PREDIO_PROPIEDAD));
                                        }
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                     // P_PREDIO_ZONA
                    //@modified by vsocarras: se retira la validacion ddonde no aplique
                    //@modified by jonathan.chacon :: Se adiciona validacion para predios fiscales
                    //@modified by leidy.gonzalez:: 17660 :: 20/06/2016 :: Se valida que la los tramites
                    // de tercera no verifique los datos de zonas proyectadas.	
                    if ((!this.tramite.isCuarta() && !this.tramite.isModificacionInscripcionCatastral()  
                    	&& !this.tramite.isRevisionAvaluo()	&& !this.tramite.isEsComplementacion()
                    	&& !this.tramite.isTercera() && !this.tramite.isPrimera() && 
                        !this.tramite.isEnglobeVirtual() &&  !this.tramite.isRectificacion() &&
                         !pp.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.
                            getCodigo()) )|| (this.tramite.isRectificacionArea()
                    		|| this.tramite.isRectificacionZona() || this.tramite.isRectificacionZonaMatriz())) {
                    	if (!EPredioCondicionPropiedad.CP_5.getCodigo().equals(pp.
                            getCondicionPropiedad()) && (pp.getPPredioZonas() == null || pp.
                            getPPredioZonas().isEmpty())) {
                            LOGGER.info("No se encontraron registros para la tabla P_PREDIO_ZONA ");
                            if (!this.mensajeValidacion.contains(VALIDACION_P_PREDIO_ZONA)) {
                                this.mensajeValidacion.add(MessageFormat.format(
                                    Constantes.MENSAJE_ERROR_PREDIO, pp.getNumeroPredial(),
                                    VALIDACION_P_PREDIO_ZONA));
                            }
                            return false;
                        }
                    }
                    // P_PREDIO_AVALUO_CATASTRAL
                    if (!this.tramite.isEnglobeVirtual()) {
                        if (pp.getPPredioAvaluoCatastrals() == null || pp.
                            getPPredioAvaluoCatastrals().isEmpty()) {
                            LOGGER.info(
                                "No se encontraron registros para la tabla P_PREDIO_AVALUO_CATASTRAL ");
                            if (!this.mensajeValidacion.contains(
                                VALIDACION_P_PREDIO_AVALUO_CATASTRAL)) {
                                this.mensajeValidacion.add(MessageFormat.format(
                                    Constantes.MENSAJE_ERROR_PREDIO, pp.getNumeroPredial(),
                                    VALIDACION_P_PREDIO_AVALUO_CATASTRAL));
                            }
                            return false;
                        }
                    }
                    // P_PREDIO_DIRECCION
                    if (pp.getPPredioDireccions() == null || pp.getPPredioDireccions().isEmpty()) {
                        LOGGER.info("No se encontraron registros para la tabla P_PREDIO_DIRECCION ");
                        if (!this.mensajeValidacion.contains(VALIDACION_P_PREDIO_DIRECCION)) {
                            this.mensajeValidacion.add(MessageFormat.format(
                                Constantes.MENSAJE_ERROR_PREDIO, pp.getNumeroPredial(),
                                VALIDACION_P_PREDIO_DIRECCION));
                        }
                        return false;
                    }
                    // P_UNIDAD_CONSTRUCCION
                    if (pp.getPUnidadConstruccions() == null || pp.getPUnidadConstruccions().
                        isEmpty()) {
                        // Condición que se debe verificar pero no es obligotaria
                        LOGGER.info(
                            "No se encontraron registros para la tabla P_UNIDAD_CONSTRUCCION - Datos no obligatorios");
                    } else {
                        for (PUnidadConstruccion puc : pp.getPUnidadConstruccions()) {
                            // P_UNIDAD_CONSTRUCCION_COMP
                            if (puc.getPUnidadConstruccionComps() == null || puc.
                                getPUnidadConstruccionComps().isEmpty()) {
                                // Condición que se debe verificar pero no es obligotaria
                                LOGGER.info(
                                    "No se encontraron registros para la tabla P_UNIDAD_CONSTRUCCION_COMP - Datos no obligatorios");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error verificando los datos básicos del trámite" + e.getMessage());
        }
        LOGGER.info("|| -------------------------------------------------- ||");
        return datosProyeccionValidos;
    }

    /**
     * Método que realiza la verificación de datos en la tabla P_PREDIO_SERVIDUMBRE para un predio
     *
     * @return
     */
    private boolean verificarServidumbres() {
        try {
            // P_PREDIO_SERVIDUMBRE
            if (this.predio.getPPredioServidumbre() == null || this.predio.getPPredioServidumbre().
                isEmpty()) {
                LOGGER.info("No se encontraron registros para la tabla P_PREDIO_SERVIDUMBRE ");
                if (!this.mensajeValidacion.contains(VALIDACION_P_PREDIO_SERVIDUMBRE)) {
                    this.mensajeValidacion.add(VALIDACION_P_PREDIO_SERVIDUMBRE);
                }
                return false;
            }
        } catch (Exception e) {
            LOGGER.error("Error verificando los datos de servidumbre" + e.getMessage());
        }
        return true;
    }

    /**
     * Método que realiza la verificación de datos sobre las tablas P_FICHA_MATRIZ,
     * P_FICHA_MATRIZ_MODELO, P_FICHA_MATRIZ_PREDIO, P_FICHA_MATRIZ_TORRE
     *
     * @return
     */
    private boolean verificarDatosFichaMatriz() {
        try {
            if (this.tramite.isTipoInscripcionCondominio() || this.tramite.isTipoInscripcionPH()) {

                // P_FICHA_MATRIZ
                PFichaMatriz pFichaMatriz = this.getConservacionService().
                    buscarPFichaMatrizPorPredioId(this.predio.getId());
                if (pFichaMatriz == null) {
                    LOGGER.info("No se encontraron registros para la tabla P_FICHA_MATRIZ");
                    if (!this.mensajeValidacion.contains(VALIDACION_P_FICHA_MATRIZ)) {
                        this.mensajeValidacion.add(VALIDACION_P_FICHA_MATRIZ);
                    }
                    return false;
                } else {
                    // P_FICHA_MATRIZ_MODELO
                    if (pFichaMatriz.getPFichaMatrizModelos() == null || pFichaMatriz.
                        getPFichaMatrizModelos().isEmpty()) {
                        // Condición que se debe verificar pero no es obligotaria
                        LOGGER.info(
                            "No se encontraron registros para la tabla P_FICHA_MATRIZ_MODELO - Datos no obligatorios");
                    }
                    // P_FICHA_MATRIZ_PREDIO
                    if (pFichaMatriz.getPFichaMatrizPredios() == null || pFichaMatriz.
                        getPFichaMatrizPredios().isEmpty()) {
                        LOGGER.info(
                            "No se encontraron registros para la tabla P_FICHA_MATRIZ_PREDIO");
                        if (!this.mensajeValidacion.contains(VALIDACION_P_FICHA_MATRIZ_PREDIO)) {
                            this.mensajeValidacion.add(VALIDACION_P_FICHA_MATRIZ_PREDIO);
                        }
                        return false;
                    }
                    if (this.tramite.isTipoInscripcionPH()) {
                        // P_FICHA_MATRIZ_TORRE 
                        if (pFichaMatriz.getPFichaMatrizTorres() == null || pFichaMatriz.
                            getPFichaMatrizTorres().isEmpty()) {
                            LOGGER.info(
                                "No se encontraron registros para la tabla P_FICHA_MATRIZ_TORRE");
                            if (!this.mensajeValidacion.contains(VALIDACION_P_FICHA_MATRIZ_TORRE)) {
                                this.mensajeValidacion.add(VALIDACION_P_FICHA_MATRIZ_TORRE);
                            }
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error verificando los datos de ficha matriz" + e.getMessage());
        }
        return true;
    }

    /**
     * Método que realiza la verificación de datos sobre las P_FOTOS asociadas a un predio
     *
     * @return
     */
    private boolean verificacionDatosFotos() {
        try {
            if (this.tramite.isSegundaDesenglobe()) {
                for (PPredio pp : this.prediosDesenglobe) {
                    if (pp.getPFotos() == null || pp.getPFotos().isEmpty()) {
                        // Condición que se debe verificar pero no es obligotaria
                        LOGGER.info(
                            "No se encontraron registros para la tabla P_FOTOS - Datos no obligatorios");
                    }
                }
            } else {
                if (this.predio.getPFotos() == null || this.predio.getPFotos().isEmpty()) {
                    // Condición que se debe verificar pero no es obligotaria
                    LOGGER.info(
                        "No se encontraron registros para la tabla P_FOTOS - Datos no obligatorios");
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error verificando los datos de fotos " + e.getMessage());
        }
        return true;
    }

    /**
     * Método que realiza la verificación de datos sobre las P_MANZANA_VEREDA asociadas a un predio
     * cuando el trámite es un desenglobe de manzanas
     *
     * @return
     */
    private boolean verificarDatosDesenglobeManzana() {
        try {
            if (this.tramite.isSegundaDesenglobe() &&
                 this.tramite.getManzanasNuevas() != null &&
                 this.tramite.getManzanasNuevas() > 0) {
                List<PManzanaVereda> manzanasProyectadas = this.getConservacionService().
                    obtenerManzanasProyectadasPorTramite(this.tramite.getId());
                if (manzanasProyectadas == null || manzanasProyectadas.isEmpty()) {
                    // P_MANZANA_VEREDA 
                    LOGGER.info("No se encontraron registros para la tabla P_MANZANA_VEREDA");
                    if (!this.mensajeValidacion.contains(VALIDACION_P_MANZANA_VEREDA)) {
                        this.mensajeValidacion.add(VALIDACION_P_MANZANA_VEREDA);
                    }
                    return false;
                }
            }
        } catch (Exception e) {
            LOGGER.error(
                "Error verificando los datos de las manzana_veredas para el trámite de desenglobe de manzanas " +
                e.getMessage());
        }
        return true;
    }

    /**
     * Método encargado de realizar la verificación de datos para los componentes de las
     * construcciones de un modelo asociados a la tabla P_FM_CONSTRUCCION_COMPONENTE
     *
     * @param validacionObligatoria
     *
     * @return
     */
    private boolean verificacionDatosFMConstruccionComponente(boolean validacionObligatoria) {

        PFichaMatriz pFichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
            this.predio.getId());

        if (pFichaMatriz == null) {
            if (validacionObligatoria) {
                // P_FICHA_MATRIZ
                LOGGER.info(
                    "No se encontraron registros para la tabla P_FICHA_MATRIZ - Datos obligatorios");
                if (!this.mensajeValidacion.contains(VALIDACION_P_FICHA_MATRIZ)) {
                    this.mensajeValidacion.add(VALIDACION_P_FICHA_MATRIZ);
                }
                return false;
            } else {
                // Condición que se debe verificar pero no es obligotaria
                LOGGER.info(
                    "No se encontraron registros para la tabla P_FICHA_MATRIZ - Datos no obligatorios");
            }
        } else {
            if (pFichaMatriz.getPFichaMatrizModelos() != null && !pFichaMatriz.
                getPFichaMatrizModelos().isEmpty()) {
                for (PFichaMatrizModelo pfmm : pFichaMatriz.getPFichaMatrizModelos()) {
                    if (pfmm.getPFmModeloConstruccions() != null && !pfmm.
                        getPFmModeloConstruccions().isEmpty()) {
                        for (PFmModeloConstruccion pfmmc : pfmm.getPFmModeloConstruccions()) {
                            if (pfmmc.getPFmConstruccionComponentes() != null && pfmmc.
                                getPFmConstruccionComponentes().isEmpty()) {
                                return true;
                            } else {
                                // Condición que se debe verificar pero no es obligotaria
                                LOGGER.info(
                                    "No se encontraron registros para la tabla P_FM_CONSTRUCCION_COMPONENTE - Datos no obligatorios");
                            }
                        }
                    } else {
                        // Condición que se debe verificar pero no es obligotaria
                        LOGGER.info(
                            "No se encontraron registros para la tabla P_FM_MODELO_CONSTRUCCION - Datos no obligatorios");
                    }
                }
            } else {
                // Condición que se debe verificar pero no es obligotaria
                LOGGER.info(
                    "No se encontraron registros para la tabla P_FICHA_MATRIZ_MODELO - Datos no obligatorios");
            }
        }
        return true;
    }

    /**
     * Método encargado de realizar la verificación de datos para las construcciones de un modelo
     * asociados a la tabla P_FM_MODELO_CONSTRUCCION
     *
     * @param validacionObligatoria
     *
     * @return
     */
    private boolean verificarDatosModeloConstruccion(boolean validacionObligatoria) {

        PFichaMatriz pFichaMatriz = this.getConservacionService().buscarPFichaMatrizPorPredioId(
            this.predio.getId());

        if (pFichaMatriz == null) {
            if (validacionObligatoria) {
                // P_FICHA_MATRIZ
                LOGGER.info(
                    "No se encontraron registros para la tabla P_FICHA_MATRIZ - Datos obligatorios");
                if (!this.mensajeValidacion.contains(VALIDACION_P_FICHA_MATRIZ)) {
                    this.mensajeValidacion.add(VALIDACION_P_FICHA_MATRIZ);
                }
                return false;
            } else {
                // Condición que se debe verificar pero no es obligotaria
                LOGGER.info(
                    "No se encontraron registros para la tabla P_FICHA_MATRIZ - Datos no obligatorios");
            }
        } else {
            if (pFichaMatriz.getPFichaMatrizModelos() != null && !pFichaMatriz.
                getPFichaMatrizModelos().isEmpty()) {
                if (pFichaMatriz.getPFichaMatrizModelos() != null && !pFichaMatriz.
                    getPFichaMatrizModelos().isEmpty()) {
                    for (PFichaMatrizModelo pfmm : pFichaMatriz.getPFichaMatrizModelos()) {
                        if (pfmm.getPFmModeloConstruccions() != null && !pfmm.
                            getPFmModeloConstruccions().isEmpty()) {
                            return true;
                        } else {
                            // Condición que se debe verificar pero no es obligotaria
                            LOGGER.info(
                                "No se encontraron registros para la tabla P_FM_MODELO_CONSTRUCCION - Datos no obligatorios");
                        }
                    }
                } else {
                    // Condición que se debe verificar pero no es obligotaria
                    LOGGER.info(
                        "No se encontraron registros para la tabla P_FICHA_MATRIZ_MODELO - Datos no obligatorios");
                }
            }
        }
        return true;
    }

    /**
     * Método encargado de realizar la verificación de datos para las referencias cartográficas de
     * un predio
     *
     * @return
     */
    private boolean verificacionDatosReferenciaCartografica() {
        try {
            if (this.predio.getPReferenciaCartograficas() == null || this.predio.
                getPReferenciaCartograficas().isEmpty()) {
                // P_REFERENCIA_CARTOGRAFICA
                LOGGER.info(
                    "No se encontraron registros para la tabla P_REFERENCIA_CARTOGRAFICA - Datos obligatorios");
                if (!this.mensajeValidacion.contains(VALIDACION_P_REFERENCIA_CARTOGRAFICA)) {
                    this.mensajeValidacion.add(VALIDACION_P_REFERENCIA_CARTOGRAFICA);
                }
                return false;
            }
        } catch (Exception e) {
            LOGGER.error("Error verificando los datos de referencias cartográficas " + e.
                getMessage());
        }
        return true;
    }

    /** ------------------------------------------------------ */
    /** ------------ L O G D E C A M B I O S ------------- */
    /** ------------------------------------------------------ */
    private void registroCambios(String nameClass, Long idObject, String cancelaInscribeOld,
        String cancelaInscribeNew) {
        String[] cambio = new String[]{nameClass, idObject.toString(), cancelaInscribeOld,
            cancelaInscribeNew};
        cambiosTramite.add(cambio);
    }

    private void logCambiosTramite() {
        LOGGER.info("|| ------------------------------------ ||");
        LOGGER.info("|| --- Validación Cancela/Inscribe ---- ||");
        LOGGER.info("|| ---------- Log de cambios ---------- ||");
        int i = 0;
        for (String[] cambio : this.cambiosTramite) {
            LOGGER.info(++i + ". " +
                 " " + ((cambio[0] != null) ? cambio[0] : " ") +
                 " " + ((cambio[1] != null) ? cambio[1] : " ") +
                 " " + ((cambio[2] != null) ? cambio[2] : " ") +
                 " " + ((cambio[3] != null) ? cambio[3] : " "));
        }
        LOGGER.info(" Cambios totales: " + i);
        LOGGER.info("|| ------- Fin log de cambios --------- ||");
        LOGGER.info("|| ------------------------------------ ||");
    }
}
