package co.gov.igac.snc.web.mb.conservacion.ejecucion;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Funcionario;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramitePrueba;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EEntidadGrupoTrabajoPruebas;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.components.IAvanzarProcesoConservacion;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;

import com.lowagie.text.pdf.PdfReader;

/**
 * Managed bean encargado de gestionar las ejecuciones de trámites en terreno. Puede ser llamado
 * desde el proceso o desde otras pantallas.
 *
 * @author fabio.navarrete
 * @modified juan.agudelo
 * @modified david.cifuentes
 *
 */
@Component("envioComunicacionAutoPruebas")
@Scope("session")
public class EnvioComunicacionAutoPruebasMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -8074261877623275753L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(EnvioComunicacionAutoPruebasMB.class);

    // -------- SERVICIOS -------- //
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private GeneralMB generalMB;

    // -------- VARIABLES -------- //
    private UsuarioDTO usuario;

    /**
     * Trámite seleccionado
     */
    private Tramite tramite;

    /**
     * Actividad seleccionada desde el arbol
     */
    private Actividad actividad;

    /**
     * Variable que une los afectados y los solicitantes del trámite.
     */
    private List<Solicitante> solicitantesAfectados;

    /**
     * Variable usada para almacenar el solicitante seleccionado de la tabla.
     */
    private Solicitante solicitanteAfectadoSeleccionado;

    /**
     * Lista de entidades o grupos de trabajo a realizar pruebas
     */
    private List<TramitePruebaEntidad> entidadesOGruposARealizarPruebas;

    /**
     * Entidad o grupo de trabajo seleccionada
     */
    private TramitePruebaEntidad entidadOGrupoSeleccionado;

    /**
     * Bandera que determina la visualización de los datos del afectado solo si este se ha buscado
     * primero en el sistema
     */
    private boolean banderaBusquedaAfectadoRealizada;

    /**
     * Variable usada para ativar los botones de edición o eliminación de un afectado.
     */
    private boolean solicitanteSelectBool;

    /**
     * Variable usada para distinguir si un afectado es nuevo o esta siendo modificado
     */
    private boolean editModeBool;

    /**
     * Variable booleana qu indica si para este trámite ya se generó la primer comunicación auto de
     * pruebas.
     */
    private boolean radicadoPrimeraVezBool;

    /**
     * Bandera que inidica si se debe mostrar el botón de generar documento de auto de pruebas
     */
    private boolean banderaGenerarDocumentoAutoDePruebas;

    /**
     * Bandera que inidica si se puede o no modificar el auto de pruebas
     */
    private boolean banderaModificarAutoDePruebas;

    /**
     * Variable booleana usada para seleccionar el repote a generar.
     */
    public boolean isSolicitanteReportBool;

    /**
     * Variable booleana para habilitar el botón de radicar en la pantalla en la que se visualiza el
     * documento de la comunicación.
     */
    private boolean radicadoBool;

    /**
     * Variable que almacena el número de radicado de una comunicación.
     */
    private String numeroRadicado;

    /**
     * Variable que indica si el reporte se generó de forma correcta.
     */
    private boolean reporteGeneradoOK;

    /**
     * Objeto Documento que se guarda en la base de datos
     */
    private Documento documentoComunicacionAutoDePruebas;

    /**
     * Lista que contiene los TramiteDocumento del trámite
     */
    private ArrayList<TramiteDocumento> tramiteDocumentosDelTramite;

    /**
     * Lista que contiene los TramiteDocumento del trámite de tipo
     * OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS
     */
    private ArrayList<TramiteDocumento> tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados;

    /**
     * Lista que contiene los TramiteDocumento del trámite de tipo
     * MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC
     */
    private ArrayList<TramiteDocumento> tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac;

    /**
     * Lista que contiene los TramiteDocumento del trámite de tipo
     * OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA
     */
    private ArrayList<TramiteDocumento> tramiteDocumentosSolicitudAutoPruebasAEntidadExterna;

    /**
     * indica si queda pendiente solo uno de los solicitantes para el registro de comunicación de
     * notificación
     */
    private boolean lastSolicitanteForRegistroComunicNotif;

    /**
     * Variable que indica si ya fueron registrados todos los solicitantes, o afectados y todas las
     * entidades externas o grupos internos
     */
    private boolean todosRegistrados;

    /**
     * Datos geográficos del aviso
     */
    private Pais paisSolicitante;
    private Departamento departamentoSolicitante;
    private Municipio municipioSolicitante;

    /**
     * Listas de select Items de los paises
     */
    private List<SelectItem> paisesSolicitante;
    private List<SelectItem> municipiosSolicitante;
    private List<SelectItem> departamentosSolicitante;

    /**
     * Listas geográficas de los paises
     */
    private List<Pais> paissSolicitante;
    private List<Departamento> dptosSolicitante;
    private List<Municipio> munisSolicitante;

    /**
     * Orden para la listas de solicitante
     */
    private EOrden ordenDepartamentosSolicitante = EOrden.CODIGO;
    private EOrden ordenMunicipiosSolicitante = EOrden.CODIGO;
    private EOrden ordenPaisesSolicitante = EOrden.CODIGO;

    /**
     * Valores para las relaciones del solicitante.
     */
    private List<SelectItem> relacionesSolicitante;

    /**
     * Solicitante del trámite seleccionado
     */
    private boolean solicitanteTramite;

    private String entidadGrupoInternoPrueba;

    private EstructuraOrganizacional territorialSeleccionada;

    /**
     * Lista de selección de Estructuras Organizacionales asociadas.
     */
    private List<EstructuraOrganizacional> unidadesOperativasCatastroObj;
    private List<EstructuraOrganizacional> subdireccionesOficinasObj;
    private List<EstructuraOrganizacional> gruposTrabajoObj;
    private List<SelectItem> unidadesOperativasCatastro;
    private List<SelectItem> subdireccionesOficinas;
    private List<SelectItem> gruposTrabajo;
    private List<SelectItem> nombresFuncionarios;

    private String territorialSeleccionadaCodigo;

    private String funcionarioSeleccionado;
    private String loginFuncionarioSeleccionado;
    private String eMailFuncionarioSeleccionado;

    private EstructuraOrganizacional subdireccionOficinaSeleccionada;
    private String subdireccionOficinaSeleccionadaCodigo;

    private String grupoInternoTrabajoSeleccionadoCodigo;
    private EstructuraOrganizacional grupoInternoTrabajoSeleccionado;

    private String unidadOperativaSeleccionadaCodigo;
    private EstructuraOrganizacional unidadOperativaSeleccionada;

    /**
     * lista de usuarios con el rol de responsables de conservación
     */
    private List<UsuarioDTO> directoresTerritoriales;

    /**
     * Variable donde se almacena la territorial del usuario autenticado en el sistema
     */
    private EstructuraOrganizacional territorialUsuario;

    /**
     * Lista de reponsables de cierta UOC seleccionada.
     */
    private List<UsuarioDTO> responsableUOC;

    /**
     * Director territorial.
     */
    private UsuarioDTO directorTerritorial;

    /**
     * Variable que almacena al funcionario ejecutor que tenia el trámite.
     */
    private UsuarioDTO funcionarioEjecutor;

    /**
     * Variable booleana que habilita el botón de avanzar proceso, una vez se ha radicado la
     * comunicación del auto de pruebas, para todos los solicitantes, afectados, entidades externas
     * y grupos internos.
     */
    private boolean habilitarAvanzarProcesoBool;

    /**
     * Variable booleana que habilita un mensaje para que se adicionen entidades externas o grupos
     * internos por si todavia no se han adicionado.
     */
    private boolean habilitarMensajeAdicionarEntidades;

    /**
     * Variables booleanas para controlar la habilitación de los botones de edición de entidades y
     * afectados.
     */
    private boolean deshabilitarSolicitante;
    private boolean deshabilitarEntidades;

    /**
     * Variable item list con los valores del tipo de solicitante
     */
    private List<SelectItem> tipoSolicitanteItemList;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteDocumentoAutoDePruebas;

    /**
     * Objeto que contiene los datos del reporte de notificación por edicto
     */
    private ReporteDTO reporteComunicacionAutoDePruebas;

    /**
     * Tipo de documento de comunicado de auto de pruebas
     */
    private Long tipoComunicacionAutoDePruebas;

    /**
     * Determina si el solicitante seleccionado para edicion es un propietario
     */
    private boolean banderaPropietario;

    // ================================================================= //
    // ------------------------- INIT ---------------------------------- //
    // ================================================================= //
    @PostConstruct
    public void init() {

        this.actividad = tareasPendientesMB.getInstanciaSeleccionada();
        long tramiteId = this.actividad.getIdObjetoNegocio();

        this.tramite = this.getTramiteService().findTramitePruebasByTramiteIdNuevaTransaccion(
            tramiteId);

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // ======== Consulta de solicitantes y afectados ====== //
        this.solicitantesAfectados = (ArrayList<Solicitante>) this
            .getTramiteService().obtenerSolicitsParaEnvioComunicNotif(
                this.tramite.getId(),
                this.tramite.getSolicitud().getId(), 4);

        // ======== Consulta de entidades y grupos de trabajo ======= //
        this.entidadesOGruposARealizarPruebas = this.getTramiteService()
            .getTramitePruebaEntidadsByTramiteId(this.tramite.getId());
        if (this.entidadesOGruposARealizarPruebas == null) {
            this.entidadesOGruposARealizarPruebas = new ArrayList<TramitePruebaEntidad>();
        }

        // ========= Consulta de los directores territoriales ======= //
        this.directorTerritorial = null;
        if (this.directoresTerritoriales == null) {
            this.directoresTerritoriales = (List<UsuarioDTO>) this
                .getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionTerritorial(),
                    ERol.DIRECTOR_TERRITORIAL);
        }
        if (this.directoresTerritoriales != null &&
             !this.directoresTerritoriales.isEmpty() &&
             this.directoresTerritoriales.get(0) != null) {
            this.directorTerritorial = this.directoresTerritoriales.get(0);
        }

        // ========= Consulta de la territorial del usuario en el sistema ======= //
        this.territorialUsuario = findTerritorial(this.usuario.getCodigoTerritorial());

        // =========== Consulta de los documentos del trámite ========== //
        // Se verifica si ya se había hecho la comunicación y está pendiente el
        // registro de la misma:
        // para ello se buscan TramiteDocumento de ese tipo para este trámite.
        this.buscarTramiteDocumentosDeTramite();

        consultarAutoDePruebasYEstado();
        /* Inhabilitado por Dumar this.radicadoPrimeraVezBool = (this.tramiteDocumentosDelTramite ==
         * null || this.tramiteDocumentosDelTramite.isEmpty())? false : true;
         */

        // D: se marcan los solicitantes a quienes ya se les hizo registro de la
        // comunicación. Sólo hay necesidad de hacerlo si hay pendientes. Si no, pone false
        // por defecto.
        if (this.radicadoPrimeraVezBool) {
            this.marcarSolicitantesConRegistroComunic();
        }

        this.reporteGeneradoOK = false;

        // Revisar el estado del botón avanzar proceso
        revisarEstadoAvanzarProceso();

        this.deshabilitarSolicitante = true;
        this.deshabilitarEntidades = true;

        this.tipoSolicitanteItemList = new ArrayList<SelectItem>();
        for (Dominio dominio : this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITANTE_TIPO_SOLICITANT)) {
            this.tipoSolicitanteItemList.
                add(new SelectItem(dominio.getCodigo(), dominio.getValor()));
        }

        this.banderaModificarAutoDePruebas = true;
        this.banderaPropietario = false;
        this.radicadoBool = true; //FIX::EJE::52110::Se realiza ajuste del branch 52110 al branch de development.
    }

    // ------------ GETTERS Y SETTERS ----------------- //
    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public boolean isSolicitanteReportBool() {
        return isSolicitanteReportBool;
    }

    public void setSolicitanteReportBool(boolean isSolicitanteReportBool) {
        this.isSolicitanteReportBool = isSolicitanteReportBool;
    }

    public boolean isBanderaPropietario() {
        return banderaPropietario;
    }

    public void setBanderaPropietario(boolean banderaPropietario) {
        this.banderaPropietario = banderaPropietario;
    }

    public boolean isRadicadoPrimeraVezBool() {
        return this.radicadoPrimeraVezBool;
    }

    public void setRadicadoPrimeraVezBool(boolean radicadoPrimeraVezBool) {
        this.radicadoPrimeraVezBool = radicadoPrimeraVezBool;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public List<Solicitante> getSolicitantesAfectados() {
        return solicitantesAfectados;
    }

    public boolean isEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public String getSubdireccionOficinaSeleccionadaCodigo() {
        return subdireccionOficinaSeleccionadaCodigo;
    }

    public void setSubdireccionOficinaSeleccionadaCodigo(
        String subdireccionOficinaSeleccionadaCodigo) {
        this.subdireccionOficinaSeleccionadaCodigo = subdireccionOficinaSeleccionadaCodigo;
    }

    public boolean isHabilitarMensajeAdicionarEntidades() {
        return habilitarMensajeAdicionarEntidades;
    }

    public void setHabilitarMensajeAdicionarEntidades(
        boolean habilitarMensajeAdicionarEntidades) {
        this.habilitarMensajeAdicionarEntidades = habilitarMensajeAdicionarEntidades;
    }

    public boolean isHabilitarAvanzarProcesoBool() {
        return habilitarAvanzarProcesoBool;
    }

    public void setHabilitarAvanzarProcesoBool(boolean habilitarAvanzarProcesoBool) {
        this.habilitarAvanzarProcesoBool = habilitarAvanzarProcesoBool;
    }

    public boolean isLastSolicitanteForRegistroComunicNotif() {
        return lastSolicitanteForRegistroComunicNotif;
    }

    public void setLastSolicitanteForRegistroComunicNotif(
        boolean lastSolicitanteForRegistroComunicNotif) {
        this.lastSolicitanteForRegistroComunicNotif = lastSolicitanteForRegistroComunicNotif;
    }

    public boolean isTodosRegistrados() {
        return todosRegistrados;
    }

    public void setTodosRegistrados(boolean todosRegistrados) {
        this.todosRegistrados = todosRegistrados;
    }

    public boolean isRadicadoBool() {
        return radicadoBool;
    }

    public void setRadicadoBool(boolean radicadoBool) {
        this.radicadoBool = radicadoBool;
    }

    public boolean isBanderaBusquedaAfectadoRealizada() {
        return banderaBusquedaAfectadoRealizada;
    }

    public void setBanderaBusquedaAfectadoRealizada(
        boolean banderaBusquedaAfectadoRealizada) {
        this.banderaBusquedaAfectadoRealizada = banderaBusquedaAfectadoRealizada;
    }

    public boolean isBanderaModificarAutoDePruebas() {
        return banderaModificarAutoDePruebas;
    }

    public void setBanderaModificarAutoDePruebas(
        boolean banderaModificarAutoDePruebas) {
        this.banderaModificarAutoDePruebas = banderaModificarAutoDePruebas;
    }

    public boolean isSolicitanteSelectBool() {
        return solicitanteSelectBool;
    }

    public void setSolicitanteSelectBool(boolean solicitanteSelectBool) {
        this.solicitanteSelectBool = solicitanteSelectBool;
    }

    public String getEntidadGrupoInternoPrueba() {
        return entidadGrupoInternoPrueba;
    }

    public void setEntidadGrupoInternoPrueba(String entidadGrupoInternoPrueba) {
        this.entidadGrupoInternoPrueba = entidadGrupoInternoPrueba;
    }

    public List<SelectItem> getUnidadesOperativasCatastro() {
        return unidadesOperativasCatastro;
    }

    public void setUnidadesOperativasCatastro(
        List<SelectItem> unidadesOperativasCatastro) {
        this.unidadesOperativasCatastro = unidadesOperativasCatastro;
    }

    public List<SelectItem> getSubdireccionesOficinas() {
        return subdireccionesOficinas;
    }

    public void setSubdireccionesOficinas(
        List<SelectItem> subdireccionesOficinas) {
        this.subdireccionesOficinas = subdireccionesOficinas;
    }

    public List<SelectItem> getGruposTrabajo() {
        return gruposTrabajo;
    }

    public void setGruposTrabajo(List<SelectItem> gruposTrabajo) {
        this.gruposTrabajo = gruposTrabajo;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public List<SelectItem> getNombresFuncionarios() {
        return nombresFuncionarios;
    }

    public void setNombresFuncionarios(List<SelectItem> nombresFuncionarios) {
        this.nombresFuncionarios = nombresFuncionarios;
    }

    public Documento getDocumentoComunicacionAutoDePruebas() {
        return documentoComunicacionAutoDePruebas;
    }

    public void setDocumentoComunicacionAutoDePruebas(
        Documento documentoComunicacionAutoDePruebas) {
        this.documentoComunicacionAutoDePruebas = documentoComunicacionAutoDePruebas;
    }

    public boolean isDeshabilitarEntidades() {
        return deshabilitarEntidades;
    }

    public void setDeshabilitarEntidades(boolean deshabilitarEntidades) {
        this.deshabilitarEntidades = deshabilitarEntidades;
    }

    public boolean isDeshabilitarSolicitante() {
        return deshabilitarSolicitante;
    }

    public void setDeshabilitarSolicitante(boolean deshabilitarSolicitante) {
        this.deshabilitarSolicitante = deshabilitarSolicitante;
    }

    public EstructuraOrganizacional getTerritorialSeleccionada() {
        return territorialSeleccionada;
    }

    public String getFuncionarioSeleccionado() {
        return funcionarioSeleccionado;
    }

    public void setFuncionarioSeleccionado(String funcionarioSeleccionado) {
        this.funcionarioSeleccionado = funcionarioSeleccionado;
    }

    public String getLoginFuncionarioSeleccionado() {
        return loginFuncionarioSeleccionado;
    }

    public void setLoginFuncionarioSeleccionado(String loginFuncionarioSeleccionado) {
        this.loginFuncionarioSeleccionado = loginFuncionarioSeleccionado;
    }

    public String geteMailFuncionarioSeleccionado() {
        return eMailFuncionarioSeleccionado;
    }

    public void seteMailFuncionarioSeleccionado(
        String eMailFuncionarioSeleccionado) {
        this.eMailFuncionarioSeleccionado = eMailFuncionarioSeleccionado;
    }

    public String getGrupoInternoTrabajoSeleccionadoCodigo() {
        return grupoInternoTrabajoSeleccionadoCodigo;
    }

    public void setGrupoInternoTrabajoSeleccionadoCodigo(
        String grupoInternoTrabajoSeleccionadoCodigo) {
        this.grupoInternoTrabajoSeleccionadoCodigo = grupoInternoTrabajoSeleccionadoCodigo;
    }

    public EstructuraOrganizacional getSubdireccionOficinaSeleccionada() {
        return subdireccionOficinaSeleccionada;
    }

    public EstructuraOrganizacional getGrupoInternoTrabajoSeleccionado() {
        return grupoInternoTrabajoSeleccionado;
    }

    public String getUnidadOperativaSeleccionadaCodigo() {
        return unidadOperativaSeleccionadaCodigo;
    }

    public void setUnidadOperativaSeleccionadaCodigo(
        String unidadOperativaSeleccionadaCodigo) {
        this.unidadOperativaSeleccionadaCodigo = unidadOperativaSeleccionadaCodigo;
    }

    public Solicitante getSolicitanteAfectadoSeleccionado() {
        return solicitanteAfectadoSeleccionado;
    }

    public void setSolicitanteAfectadoSeleccionado(
        Solicitante solicitanteAfectadoSeleccionado) {
        this.solicitanteAfectadoSeleccionado = solicitanteAfectadoSeleccionado;
    }

    public EstructuraOrganizacional getUnidadOperativaSeleccionada() {
        return unidadOperativaSeleccionada;
    }

    public boolean isBanderaGenerarDocumentoAutoDePruebas() {
        return banderaGenerarDocumentoAutoDePruebas;
    }

    public void setBanderaGenerarDocumentoAutoDePruebas(
        boolean banderaGenerarDocumentoAutoDePruebas) {
        this.banderaGenerarDocumentoAutoDePruebas = banderaGenerarDocumentoAutoDePruebas;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<TramitePruebaEntidad> getEntidadesOGruposARealizarPruebas() {
        return entidadesOGruposARealizarPruebas;
    }

    public void setEntidadesOGruposARealizarPruebas(
        List<TramitePruebaEntidad> entidadesOGruposARealizarPruebas) {
        this.entidadesOGruposARealizarPruebas = entidadesOGruposARealizarPruebas;
    }

    public TramitePruebaEntidad getEntidadOGrupoSeleccionado() {
        return entidadOGrupoSeleccionado;
    }

    public void setEntidadOGrupoSeleccionado(
        TramitePruebaEntidad entidadOGrupoSeleccionado) {
        this.entidadOGrupoSeleccionado = entidadOGrupoSeleccionado;
    }

    public void setPaisesSolicitante(List<SelectItem> paisesSolicitante) {
        this.paisesSolicitante = paisesSolicitante;
    }

    public void setMunicipiosSolicitante(List<SelectItem> municipiosSolicitante) {
        this.municipiosSolicitante = municipiosSolicitante;
    }

    public void setDepartamentosSolicitante(
        List<SelectItem> departamentosSolicitante) {
        this.departamentosSolicitante = departamentosSolicitante;
    }

    public Pais getPaisSolicitante() {
        return paisSolicitante;
    }

    public void setPaisSolicitante(Pais paisSolicitante) {
        this.paisSolicitante = paisSolicitante;
    }

    public Departamento getDepartamentoSolicitante() {
        return departamentoSolicitante;
    }

    public void setDepartamentoSolicitante(Departamento departamentoSolicitante) {
        this.departamentoSolicitante = departamentoSolicitante;
    }

    public Municipio getMunicipioSolicitante() {
        return municipioSolicitante;
    }

    public void setMunicipioSolicitante(Municipio municipioSolicitante) {
        this.municipioSolicitante = municipioSolicitante;
    }

    public List<SelectItem> getPaisesSolicitante() {
        return paisesSolicitante;
    }

    public ArrayList<TramiteDocumento> getTramiteDocumentosDelTramite() {
        return tramiteDocumentosDelTramite;
    }

    public void setTramiteDocumentosDelTramite(
        ArrayList<TramiteDocumento> tramiteDocumentosDelTramite) {
        this.tramiteDocumentosDelTramite = tramiteDocumentosDelTramite;
    }

    public List<UsuarioDTO> getDirectoresTerritoriales() {
        return directoresTerritoriales;
    }

    public void setDirectoresTerritoriales(List<UsuarioDTO> directoresTerritoriales) {
        this.directoresTerritoriales = directoresTerritoriales;
    }

    public boolean isReporteGeneradoOK() {
        return reporteGeneradoOK;
    }

    public void setReporteGeneradoOK(boolean reporteGeneradoOK) {
        this.reporteGeneradoOK = reporteGeneradoOK;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return directorTerritorial;
    }

    public void setDirectorTerritorial(UsuarioDTO directorTerritorial) {
        this.directorTerritorial = directorTerritorial;
    }

    public void setSolicitantesAfectados(List<Solicitante> solicitantesAfectados) {
        this.solicitantesAfectados = solicitantesAfectados;
    }

    public void setGrupoInternoTrabajoSeleccionado(
        EstructuraOrganizacional grupoInternoTrabajoSeleccionado) {
        this.grupoInternoTrabajoSeleccionado = grupoInternoTrabajoSeleccionado;
    }

    public List<SelectItem> getDepartamentosSolicitante() {
        return departamentosSolicitante;
    }

    public void onChangeOrdenDepartamentosSolicitante() {
        this.updateDepartamentosSolicitante();
    }

    public List<SelectItem> getMunicipiosSolicitante() {
        return this.municipiosSolicitante;
    }

    public void onChangeMunicipiosSolicitante() {
    }

    public String getOrdenPaisesSolicitante() {
        return ordenPaisesSolicitante.toString();
    }

    public String getOrdenDepartamentosSolicitante() {
        return ordenDepartamentosSolicitante.toString();
    }

    public String getOrdenMunicipiosSolicitante() {
        return ordenMunicipiosSolicitante.toString();
    }

    /**
     * Determina si los países están siendo ordenados por NOMBRE
     */
    public boolean isOrdenPaisesNombreSolicitante() {
        return this.ordenPaisesSolicitante.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los departamentos están siendo ordenados por NOMBRE
     */
    public boolean isOrdenDepartamentosNombreSolicitante() {
        return this.ordenDepartamentosSolicitante.equals(EOrden.NOMBRE);
    }

    /**
     * Determina si los municipios están siendo ordenados por NOMBRE
     */
    public boolean isOrdenMunicipiosNombreSolicitante() {
        return this.ordenMunicipiosSolicitante.equals(EOrden.NOMBRE);
    }

    public List<SelectItem> getRelacionesSolicitante() {
        return this.relacionesSolicitante;
    }

    public List<SelectItem> getTipoSolicitanteItemList() {
        return tipoSolicitanteItemList;
    }

    public void setTipoSolicitanteItemList(List<SelectItem> tipoSolicitanteItemList) {
        this.tipoSolicitanteItemList = tipoSolicitanteItemList;
    }

    public ReporteDTO getReporteDocumentoAutoDePruebas() {
        return reporteDocumentoAutoDePruebas;
    }

    public void setReporteDocumentoAutoDePruebas(
        ReporteDTO reporteDocumentoAutoDePruebas) {
        this.reporteDocumentoAutoDePruebas = reporteDocumentoAutoDePruebas;
    }

    public ReporteDTO getReporteComunicacionAutoDePruebas() {
        return reporteComunicacionAutoDePruebas;
    }

    public void setReporteComunicacionAutoDePruebas(
        ReporteDTO reporteComunicacionAutoDePruebas) {
        this.reporteComunicacionAutoDePruebas = reporteComunicacionAutoDePruebas;
    }

// ----------------------- MÉTODOS -------------------------------- //
    /**
     * Método que consulta el auto de pruebas y revisa si ya se consolido el auto de pruebas, es
     * decir, si ya se radicó la primer comunicación a un solicitante, afectado, entidad externa o
     * grupo interno de trabajo. Si sí, el auto de pruebas no debe permitir modificación, ni tampoco
     * se debe permitir edición de los afectados.
     *
     * @author david.cifuentes
     */
    public void consultarAutoDePruebasYEstado() {

        // Consultar si el documento auto de pruebas
        // existe y ya tiene asignada la cantidad de folios.
        // - Si no se tiene, es porque aún se permite su modificación.
        // - Si se encuentra y tiene un valor la cantidad de folios,
        // es porque se consolidó y ya no se permite su modificación.
        if (tramite.getTramitePruebas() != null &&
             tramite.getTramitePruebas().getAutoPruebasDocumento() != null &&
             tramite.getTramitePruebas().getAutoPruebasDocumento()
                .getCantidadFolios() != null &&
             tramite.getTramitePruebas().getAutoPruebasDocumento()
                .getCantidadFolios().intValue() > 0) {
            // Se realizó la primer comunicación, el
            // auto de pruebas está consolidado.
            this.radicadoPrimeraVezBool = true;
        } else {
            // No se ha realizado la primer comunicación,
            // el auto de pruebas permite ser modificado.
            this.radicadoPrimeraVezBool = false;
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método revisa el estado de avanzar el proceso, habilita el botón avanzar proceso, si y sólo
     * si se comunicó a todos los solicitantes, afectados, entidades externas y grupos internos de
     * trabajo.
     *
     * @author david.cifuentes
     */
    public void revisarEstadoAvanzarProceso() {
        this.habilitarAvanzarProcesoBool = false;
        this.habilitarMensajeAdicionarEntidades = false;
        int totalSolicitantesYEntidades = 0, totalDocumentos = 0;
        // Calcular total de solicitantes y afectados
        if (this.solicitantesAfectados != null &&
             this.solicitantesAfectados.size() > 0) {
            totalSolicitantesYEntidades = totalSolicitantesYEntidades +
                 this.solicitantesAfectados.size();
        }
        // Calcular total de entidades externas y grupos internos de trabajo
        if (this.entidadesOGruposARealizarPruebas != null &&
             this.entidadesOGruposARealizarPruebas.size() > 0) {
            totalSolicitantesYEntidades = totalSolicitantesYEntidades +
                 this.entidadesOGruposARealizarPruebas.size();
        } else {
            this.habilitarMensajeAdicionarEntidades = true;
            return;
        }

        // Calcular total de documentos asociados al trámite que sean del tipo
        // COMUNICACION_AUTO_DE_PRUEBAS
        if (tramiteDocumentosDelTramite != null &&
             tramiteDocumentosDelTramite.size() > 0) {
            totalDocumentos = tramiteDocumentosDelTramite.size();
        }

        if (totalSolicitantesYEntidades > totalDocumentos) {
            LOGGER.debug("Faltan " +
                 (totalSolicitantesYEntidades - totalDocumentos) +
                 " por comunicar.");
        } else if (totalSolicitantesYEntidades < totalDocumentos) {
            LOGGER.debug("Hay un excedente en los archivos de las comunicaciones.");
            //TODO::javier.aponte::arreglar la consulta de trámites documentos::27-06-2013::javier.aponte
            this.habilitarAvanzarProcesoBool = true;
        } else {
            this.addMensajeInfo(
                "Se ha realizado satisfactoriamente la comunicación auto de pruebas para la totalidad los solicitantes, afectados, entidades externas y grupos internos de trabajo. Por favor avance el proceso.");
            LOGGER.debug("Se completaron las comunicaciones, puede avanzar el proceso.");
            this.habilitarAvanzarProcesoBool = true;
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que verifica que no se haya radicado por primera vez la comunicación para algunos de
     * los solicitantes o afectados.
     *
     * @author david.cifuentes
     */
    public boolean validateRadicadoPrimerVez() {

        if (this.radicadoPrimeraVezBool) {
            this.addMensajeWarn(
                "No se pueden realizar edición sobre los afectados pues ya se radicó la comunicación para al menos uno de ellos.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return radicadoPrimeraVezBool;
        }
        return radicadoPrimeraVezBool;
    }

    // ---------------------------------------------------- //
    /**
     * Método que valida si un solicitante o afectado se puede eliminar.
     *
     * @author david.cifuentes
     */
    public void validateEliminarSolicitanteOAfectado() {

        if (validateRadicadoPrimerVez()) {
            return;
        }

        if (this.solicitanteAfectadoSeleccionado == null) {
            this.addMensajeWarn("Por favor seleccione un solicitante o afectado a eliminar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
    }
    // ---------------------------------------------------- //

    /**
     * Método que valida si una entidad o grupo de trabajo se puede eliminar.
     *
     * @author david.cifuentes
     */
    public void validateEliminarEntidadOGrupo() {

        if (validateRadicadoPrimerVez()) {
            return;
        }

        if (this.entidadOGrupoSeleccionado == null) {
            this.addMensajeWarn(
                "Por favor seleccione una entidad externa o grupo de trabajo interno a eliminar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
    }

    // ---------------------------------------------------- //
    /**
     * Método que inicializa los valores para adicionar un nuevo afectado.
     *
     * @author david.cifuentes
     */
    public void setupNuevoAfectado() {

        // Verificar que no se haya radicado por primera vez la comunicación
        if (validateRadicadoPrimerVez()) {
            return;
        }

        this.solicitanteAfectadoSeleccionado = new Solicitante();
        this.solicitanteAfectadoSeleccionado.setTipoIdentificacion(
            EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
        this.solicitanteAfectadoSeleccionado.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());

        this.solicitanteAfectadoSeleccionado.setFechaLog(new Date());
        this.solicitanteAfectadoSeleccionado.setUsuarioLog(this.usuario.getLogin());

        // Departamento
        if (this.tramite.getPredio() != null &&
             this.tramite.getPredio().getDepartamento() != null) {
            this.solicitanteAfectadoSeleccionado.setDireccionDepartamento(this.tramite.getPredio().
                getDepartamento());
        } else {
            this.solicitanteAfectadoSeleccionado.setDireccionDepartamento(this.tramite.
                getDepartamento());
        }
        // Municipio
        if (this.tramite.getPredio() != null &&
             this.tramite.getPredio().getMunicipio() != null) {
            this.solicitanteAfectadoSeleccionado.setDireccionMunicipio(this.tramite.getPredio().
                getMunicipio());
        } else {
            this.solicitanteAfectadoSeleccionado.setDireccionMunicipio(this.tramite.getMunicipio());
        }
        // País
        if (this.tramite.getPredio() != null &&
             this.tramite.getPredio().getDepartamento() != null &&
             this.tramite.getPredio().getDepartamento().getPais() != null) {

            this.solicitanteAfectadoSeleccionado.setDireccionPais(this.tramite.getPredio().
                getDepartamento().getPais());
        } else {
            this.solicitanteAfectadoSeleccionado.setDireccionPais(this.tramite.getDepartamento().
                getPais());
        }

        this.solicitanteAfectadoSeleccionado.setRelacion(ESolicitanteSolicitudRelac.AFECTADO.
            toString());
        this.banderaBusquedaAfectadoRealizada = false;

        if (this.entidadGrupoInternoPrueba != null && this.entidadGrupoInternoPrueba.equals(
            EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA.getNombre())) {

            Pais defaultPais = new Pais();
            defaultPais.setCodigo(Constantes.COLOMBIA);
            this.paisSolicitante = defaultPais;
            this.departamentoSolicitante = null;
            this.municipioSolicitante = null;

            this.updatePaisesSolicitante();
            this.updateDepartamentosSolicitante();
            this.updateMunicipiosSolicitante();

            this.generarRelacionesSolicitante();
        }

    }

    // ---------------------------------------------------- //
    /**
     * Método encargado de eliminar un afectado de la lista de solicitantes y afectados a comunicar
     * la notificación de resolución.
     *
     * @author david.cifuentes
     */
    public void eliminarSolicitanteAfectado() {

        try {
            if (validateAfectado()) {

                for (SolicitanteTramite st : this.tramite
                    .getSolicitanteTramites()) {
                    if (st.getSolicitante()
                        .getId()
                        .compareTo(solicitanteAfectadoSeleccionado.getId()) == 0) {
                        this.getTramiteService().eliminarSolicitanteTramite(st);
                        this.tramite.getSolicitanteTramites().remove(st);
                        this.solicitantesAfectados.remove(solicitanteAfectadoSeleccionado);
                        this.addMensajeInfo("Se eliminó satisfactoriamente el afectado");
                        this.deshabilitarSolicitante = true;
                        solicitanteAfectadoSeleccionado = null;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Error al eliminar el afectado seleccionado.");
            LOGGER.error(e.getMessage(), e);
        }
    }

    // ----------------------------------------------------//
    /**
     * Método encargado de verificar que se vaya a eliminar un afectado, más no uno de los
     * solicitantes que vienen del trámite.
     *
     * @return false si es un solicitante y true si es un afectado.
     *
     * @author david.cifuentes
     */
    public boolean validateAfectado() {
        boolean esAfectado = false;
        if (this.solicitanteAfectadoSeleccionado != null) {
            if (this.tramite.getSolicitanteTramites() != null &&
                 this.tramite.getSolicitanteTramites().size() > 0) {
                for (SolicitanteTramite st : this.tramite
                    .getSolicitanteTramites()) {

                    if (st.getSolicitante().getId().longValue() ==
                        this.solicitanteAfectadoSeleccionado
                            .getId().longValue()) {

                        // Se valida que sea un afectado.
                        if (st.getRelacion() != null &&
                             st.getRelacion().equals(
                                ESolicitanteSolicitudRelac.AFECTADO.toString())) {
                            esAfectado = true;
                        }
                    }
                }
                // Verificar si el solicitante seleccionado no es un afectado.
                if (!esAfectado) {
                    this.addMensajeError(
                        "No se puede eliminar un solicitante, unicamente es  posible eliminar afectados.");
                    esAfectado = false;
                }
            } else {
                this.addMensajeError(
                    "No se puede eliminar un solicitante, unicamente es posible eliminar afectados");
                esAfectado = false;
            }
        }
        return esAfectado;
    }

    //--------------------------------------------------------//
    /**
     * Método que prepara al afectado para su edición.
     *
     * @author david.cifuentes
     */
    public void setupSolicitanteOAfectadoEdicion() {

        // Verificar que no se haya radicado por primera vez la comunicación
        if (validateRadicadoPrimerVez()) {
            return;
        }
        // Verificar que se haya seleccionado un afectado
        if (this.solicitanteAfectadoSeleccionado == null) {
            this.addMensajeWarn("Por favor seleccione un afectado a modificar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        boolean esAfectado = false;

        if (this.tramite.getSolicitanteTramites() != null &&
             this.tramite.getSolicitanteTramites().size() > 0) {
            for (SolicitanteTramite st : this.tramite
                .getSolicitanteTramites()) {

                if (st.getSolicitante().getId().longValue() == this.solicitanteAfectadoSeleccionado
                    .getId().longValue()) {

                    // Se valida que la información que se intenta modificar sea
                    // la de un afectado.
                    if (st.getRelacion() != null &&
                         st.getRelacion().equals(
                            ESolicitanteSolicitudRelac.AFECTADO.toString())) {
                        esAfectado = true;
                        break;
                    }
                }
            }
            // Verificar si el solicitante seleccionado no es un afectado.
            if (!esAfectado) {
                this.addMensajeWarn(
                    "Sólo se puede editar la información para los afectados, seleccione un afectado e intente de nuevo.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.editModeBool = true;
                return;
            } else {
                editModeBool = false;
            }
        }
        this.banderaBusquedaAfectadoRealizada = true;
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de buscar un afectado a partir de los datos de documento
     *
     * @author david.cifuentes
     * @modified felipe.cadena - Se agrega a autogeneración del digito de verificación
     */
    public void buscarAfectado() {
        LOGGER.debug("Entrando a buscar afectado");

        Solicitante sol = this.getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
                solicitanteAfectadoSeleccionado.getNumeroIdentificacion(),
                solicitanteAfectadoSeleccionado.getTipoIdentificacion());
        if (sol != null) {
            this.solicitanteAfectadoSeleccionado = sol;
            this.editModeBool = true;
        } else {
            LOGGER.debug("buscarAfectado Return null");
            String tipoIdentif = this.solicitanteAfectadoSeleccionado
                .getTipoIdentificacion();
            String numDoc = this.solicitanteAfectadoSeleccionado
                .getNumeroIdentificacion();
            String tipoPersona = this.solicitanteAfectadoSeleccionado.getTipoPersona();
            setDefaultDataAfectado();
            this.solicitanteAfectadoSeleccionado.setTipoIdentificacion(tipoIdentif);
            this.solicitanteAfectadoSeleccionado.setNumeroIdentificacion(numDoc);
            this.solicitanteAfectadoSeleccionado.setTipoPersona(tipoPersona);
            this.editModeBool = false;
            //se calcula al digito de verificación
            if (this.solicitanteAfectadoSeleccionado.getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                String dv = String.valueOf(this.getGeneralesService().calcularDigitoVerificacion(
                    this.solicitanteAfectadoSeleccionado.getNumeroIdentificacion()));
                this.solicitanteAfectadoSeleccionado.setDigitoVerificacion(dv);
            }

        }
    }

    //--------------------------------------------------------//
    /**
     * Método que quema los datos de fechas departamento tipo de identificación y demás requeridos
     * para inicializar el afectado.
     *
     * @author david.cifuentes
     */
    private void setDefaultDataAfectado() {
        Solicitante sol = new Solicitante();
        sol.setTipoIdentificacion("CC");
        // Departamento
        if (this.tramite.getPredio() != null &&
             this.tramite.getPredio().getDepartamento() != null) {
            sol.setDireccionDepartamento(this.tramite.getPredio()
                .getDepartamento());
        } else {
            sol.setDireccionDepartamento(this.tramite.getDepartamento());
        }
        // Municipio
        if (this.tramite.getPredio() != null &&
             this.tramite.getPredio().getMunicipio() != null) {
            sol.setDireccionMunicipio(this.tramite.getPredio().getMunicipio());
        } else {
            sol.setDireccionMunicipio(this.tramite.getMunicipio());
        }
        // País
        if (this.tramite.getPredio() != null &&
             this.tramite.getPredio().getDepartamento() != null &&
             this.tramite.getPredio().getDepartamento().getPais() != null) {

            sol.setDireccionPais(this.tramite.getPredio().getDepartamento()
                .getPais());
        } else {
            sol.setDireccionPais(this.tramite.getDepartamento().getPais());
        }
        sol.setFechaLog(new Date());
        sol.setUsuarioLog(this.usuario.getLogin());
        this.solicitanteAfectadoSeleccionado = sol;
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de guardar/actualizar el afectado actual en la tabla solicitantes o
     * afectados, y crear el solicitanteTramite para asociar el afectado al trámite.
     *
     * @author david.cifuentes
     */
    public void guardarAfectado() {

        //se valida el codigo postal   
        RequestContext context = RequestContext.getCurrentInstance();
        if (this.solicitanteAfectadoSeleccionado.getCodigoPostal() != null &&
             !this.solicitanteAfectadoSeleccionado.getCodigoPostal().isEmpty() &&
             !this.dptosSolicitante.isEmpty()) {

            if (this.solicitanteAfectadoSeleccionado.getDireccion() == null ||
                this.solicitanteAfectadoSeleccionado.getDireccion().isEmpty()) {
                this.addMensajeError("Dirección: Error de validación: se necesita un valor.");
                context.addCallbackParam("error", "error");
                return;
            }

            if (!UtilidadesWeb.validarSoloDigitos(this.solicitanteAfectadoSeleccionado.
                getCodigoPostal())) {
                this.addMensajeError("El Código postal debe contener solo números");
                context.addCallbackParam("error", "error");
                return;
            }

            if (this.solicitanteAfectadoSeleccionado.getCodigoPostal().length() < 6) {
                this.addMensajeError("El Código postal debe tener por lo menos 6 digitos");
                context.addCallbackParam("error", "error");
                return;
            }
            String initCodigoPostal = this.solicitanteAfectadoSeleccionado.getCodigoPostal().
                substring(0, 2);

            if (!this.departamentoSolicitante.getCodigo().equals(initCodigoPostal)) {
                this.addMensajeError(
                    "Código postal no corresponde con el código del departamento seleccionado");
                context.addCallbackParam("error", "error");
                return;
            }
        }

        // N: El solicitante siempre acepta ser notificado por correo
        // electrónico, según indicaciones de los analistas
        this.solicitanteAfectadoSeleccionado.setNotificacionEmailBoolean(true);

        // Se guarda el solicitante
        boolean yaAdicionado = false;
        for (Solicitante sol : this.solicitantesAfectados) {
            if (sol.getNumeroIdentificacion() != null &&
                 sol.getTipoIdentificacion() != null &&
                 sol.getTipoIdentificacion().equals(
                    this.solicitanteAfectadoSeleccionado
                        .getTipoIdentificacion()) &&
                 sol.getNumeroIdentificacion().equals(
                    this.solicitanteAfectadoSeleccionado
                        .getNumeroIdentificacion())) {
                yaAdicionado = true;
                break;
            }
        }

        this.solicitanteAfectadoSeleccionado
            .setRelacion(ESolicitanteSolicitudRelac.AFECTADO.toString());

        Solicitante auxSolicitante = this.getTramiteService()
            .guardarActualizarSolicitante(
                this.solicitanteAfectadoSeleccionado);

        if (auxSolicitante != null) {

            this.addMensajeInfo("El afectado fue " +
                 (yaAdicionado ? "actualizado" : "adicionado") +
                 " correctamente");

            auxSolicitante.setDireccionPais(solicitanteAfectadoSeleccionado
                .getDireccionPais());
            auxSolicitante
                .setDireccionDepartamento(solicitanteAfectadoSeleccionado
                    .getDireccionDepartamento());
            auxSolicitante
                .setDireccionMunicipio(solicitanteAfectadoSeleccionado
                    .getDireccionMunicipio());
            auxSolicitante.setRelacion(solicitanteAfectadoSeleccionado
                .getRelacion());
            auxSolicitante.setNotificacionEmail(solicitanteAfectadoSeleccionado
                .getNotificacionEmail());
            this.solicitanteAfectadoSeleccionado = auxSolicitante;

            // Se remueve de la lista el afectado desactualizado y se reemplaza
            // por el actualizado.
            if (yaAdicionado) {
                Solicitante solAux = null;
                for (Solicitante s : this.solicitantesAfectados) {
                    if (s.getId().longValue() == this.solicitanteAfectadoSeleccionado
                        .getId()) {
                        solAux = s;
                        break;
                    }
                }
                if (solAux != null) {
                    this.solicitantesAfectados.remove(solAux);
                }
            }
            // Se adiciona a nuestra lista de solicitantes y afectados.
            this.solicitantesAfectados
                .add(this.solicitanteAfectadoSeleccionado);

            // Se crea el solicitanteTramite para almacenar el afectado.
            // La manera para identificar el afectado, es que no
            // tiene un valor para tipoSolicitante y en relacion tiene el
            // valor 'AFECTADO'
            SolicitanteTramite solTram = new SolicitanteTramite();

            if (yaAdicionado) {
                // Recupero mi solicitanteTramite para ser actualizado
                for (SolicitanteTramite st : this.tramite
                    .getSolicitanteTramites()) {
                    if (st.getSolicitante().getId().longValue() ==
                        this.solicitanteAfectadoSeleccionado
                            .getId().longValue()) {
                        solTram = st;
                        break;
                    }
                }
            } else {
                // Es un nuevo afectado, se adicionan los valores a este.
                solTram.setUsuarioLog(this.usuario.getLogin());
                solTram.setFechaLog(new Date(System.currentTimeMillis()));
                solTram.setTramite(this.tramite);
            }

            solTram.setSolicitante(this.solicitanteAfectadoSeleccionado);

            int i;
            for (i = 0; i < this.tramite.getSolicitanteTramites().size(); i++) {
                if (this.tramite.getSolicitanteTramites().get(i)
                    .getSolicitante().getId()
                    .compareTo(solicitanteAfectadoSeleccionado.getId()) == 0) {
                    this.tramite
                        .getSolicitanteTramites()
                        .get(i)
                        .setSolicitante(
                            this.solicitanteAfectadoSeleccionado);
                    this.tramite
                        .getSolicitanteTramites()
                        .get(i)
                        .incorporarDatosSolicitante(
                            this.solicitanteAfectadoSeleccionado);
                    break;
                } else {
                    // Modificación del afectado
                    if (this.tramite.getSolicitanteTramites().get(i)
                        .getSolicitante().getId().longValue() == solicitanteAfectadoSeleccionado
                            .getId().longValue()) {

                        this.tramite
                            .getSolicitanteTramites()
                            .get(i)
                            .setSolicitante(
                                this.solicitanteAfectadoSeleccionado);
                        this.tramite
                            .getSolicitanteTramites()
                            .get(i)
                            .incorporarDatosSolicitante(
                                this.solicitanteAfectadoSeleccionado);
                        break;
                    }
                }
            }

            solTram.incorporarDatosSolicitante(auxSolicitante);
            solTram.setTipoSolicitante("");
            solTram.setRelacion(ESolicitanteSolicitudRelac.AFECTADO.toString());

            // Guardar TramiteSolicitante
            solTram = this.getTramiteService()
                .guardarActualizarSolicitanteTramite(solTram);

            if (solTram != null && solTram.getId() != null) {
                this.tramite.getSolicitanteTramites().add(solTram);
            }
            // Se reinician las variables para desactivar los botones de edición
            // en la pantalla.
            this.solicitanteAfectadoSeleccionado = null;
            this.solicitanteSelectBool = false;

        } else {
            context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al almacenar el afectado");
        }
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de generar el reporte de auto de pruebas
     *
     * @author javier.aponte
     */
    /*
     * @modified by leidy.gonzalez :: #16589 :: 28/03/2016 :: Se realiza validacion para que
     * visualice en el reporte el abogado que se encuetra activo.
     */
    public void generarDocumentoAutoDePruebas() {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.AUTO_DE_PRUEBAS;

        Map<String, String> parameters = new HashMap<String, String>();

        parameters.put("TRAMITE_ID", "" + this.tramite.getId());
        parameters.put("ROL_USUARIO", ERol.ABOGADO.getRol());

        String estructuraOrg = this.getGeneralesService().
            getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                this.tramite.getPredio().getMunicipio().getCodigo());
        List<UsuarioDTO> abogados = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
            estructuraOrg, ERol.ABOGADO);
        UsuarioDTO abogado = new UsuarioDTO();

        for (UsuarioDTO abogadoTemp : abogados) {

            //Se valida que el abogado este disponible
            //para asignacion al reporte
            if (abogadoTemp.getFechaExpiracion().after(new Date()) ||
                 ELDAPEstadoUsuario.ACTIVO.codeExist(abogadoTemp.getEstado())) {

                abogado = abogadoTemp;
            }

        }

        if (abogado != null && abogado.getNombreCompleto() != null) {
            FirmaUsuario firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(abogado.
                getNombreCompleto());

            if (firma != null && abogado.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {
                String documentoFirma = this.getGeneralesService().
                    descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                        getIdRepositorioDocumentos());
                parameters.put("FIRMA_USUARIO", documentoFirma);
            }
            this.reporteDocumentoAutoDePruebas = reportsService.generarReporte(parameters,
                enumeracionReporte, abogado);

        }
    }
    //--------------------------------------------------------//

    /**
     * Método ejecutado sobre el boton guardar en modificar auto de pruebas
     */
    public void guardarSolicitudPruebas() {
        if (validarDatosAprobarPruebas()) {
            try {
                TramitePrueba tramitePrueba;
                tramitePrueba = this.tramite.getTramitePruebas();
                tramitePrueba.setTramite(this.tramite);

                tramitePrueba = this.getTramiteService()
                    .guardarActualizarTramitePruebas(tramitePrueba, this.usuario,
                        null);
                this.tramite.setTramitePruebas(tramitePrueba);
                this.addMensajeInfo("Se guardó el auto de pruebas exitosamente!");
                this.banderaGenerarDocumentoAutoDePruebas = true;

            } catch (Exception e) {
                String mensaje =
                    "Ocurrió un error al guardar la solicitud de pruebas, por favor intente nuevamente";
                this.addMensajeError(mensaje);
            }
        }
    }

    //--------------------------------------------------------//
    /**
     * Método que valida los datos para guardar una desición de aporbación de una solicitud de
     * pruebas
     *
     * @return
     */
    private boolean validarDatosAprobarPruebas() {

        if (this.tramite.getTramitePruebas() != null &&
            this.tramite.getTramitePruebas().getAprobadas() != null &&
            this.tramite.getTramitePruebas().getAprobadas()
                .equals(ESiNo.SI.getCodigo())) {
            if (this.tramite
                .getTramitePruebas()
                .getFechaInicio()
                .before(this.tramite.getTramitePruebas()
                    .getFechaFin())) {
                return true;
            } else {
                String mensaje = "La fecha de fin de pruebas no puede ser anterior a la fecha " +
                     " de inicio de pruebas." +
                     " Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                return false;
            }
        }
        return true;
    }

    //--------------------------------------------------------//
    /**
     * Determina si la entidad o grupo de trabajo seleccionada es Territorial Area de Conservación
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isTerritorialAreaConservacion() {
        if (this.entidadGrupoInternoPrueba != null) {
            return this.entidadGrupoInternoPrueba
                .equals(EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
                    .getNombre());
        }
        return false;
    }

    // ------------------------------------------------------ //
    /**
     * Determina si la entidad o grupo de trabajo seleccionada es Grupo Inrterno de Trabajo (GIT)
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isGrupoInternoTrabajo() {
        if (this.entidadGrupoInternoPrueba != null) {
            return this.entidadGrupoInternoPrueba
                .equals(EEntidadGrupoTrabajoPruebas.GRUPO_DE_TRABAJO_INTERNO.getNombre());
        }
        return false;
    }

    // ------------------------------------------------------ //
    /**
     * Determina si la entidad o grupo de trabajo seleccionada es Entidad Externa
     *
     * @return
     * @author fabio.navarrete
     */
    public boolean isEntidadExterna() {
        if (this.entidadGrupoInternoPrueba != null) {
            return this.entidadGrupoInternoPrueba
                .equals(EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA
                    .getNombre());
        }
        return false;
    }

    // ------------------------------------------------------ //
    /**
     * Método que verifica si el tramite prueba entidad ya fue registrado.
     */
    private boolean isTramitePruebaEntidadRegistrado(
        TramitePruebaEntidad tramitePruebaEntidad) {
        if (this.tramite.getTramitePruebas() != null) {

            for (TramitePruebaEntidad tpe : this.entidadesOGruposARealizarPruebas) {
                if (ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION.getCodigo().equals(
                    tramitePruebaEntidad.getTipoSolicitante())) {

                    if (ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION.getCodigo().
                        equals(tpe.getTipoSolicitante()) &&
                         (tpe.getEstructuraOrganizacional() != null && tramitePruebaEntidad.
                        getEstructuraOrganizacional() != null &&
                         tpe.getEstructuraOrganizacional().getCodigo().equals(tramitePruebaEntidad.
                            getEstructuraOrganizacional().getCodigo()))) {
                        return true;
                    }
                }
                if (ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO.getCodigo().equals(
                    tramitePruebaEntidad.getTipoSolicitante())) {

                    if (ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO.getCodigo().equals(tpe.
                        getTipoSolicitante()) &&
                         (tpe.getEstructuraOrganizacional() != null && tramitePruebaEntidad.
                        getEstructuraOrganizacional() != null &&
                         tpe.getEstructuraOrganizacional().getCodigo().equals(tramitePruebaEntidad.
                            getEstructuraOrganizacional().getCodigo()))) {
                        return true;
                    }
                } else if (tpe.getSolicitante() != null && tramitePruebaEntidad.getSolicitante() !=
                    null &&
                     tpe.getSolicitante().getId().equals(tramitePruebaEntidad.getSolicitante().
                        getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    // ------------------------------------------------------ //
    /**
     * Método que guarda una territorial area de conservación.
     *
     * @author david.cifuentes
     * @modified by javier.aponte
     */
    /*
     * @modified by juanfelipe.garcia :: 10-10-2013 :: ajuste para vía gubernativa (5743)
     */
 /*
     * @modified by javier.aponte :: 21-05-2014 :: Se cambió todo el método porque la lógica que
     * estaba antes guardaba el funcionario ejecutor en la tabla solicitante y no en la tabla
     * trámite prueba entidad.
     */
    public void guardarTerritorialAreaConservacion() {

        Municipio municipio;
        Departamento departamento;
        Pais pais;

        try {

            if (this.funcionarioEjecutor != null) {

                if (this.funcionarioEjecutor.getTipoIdentificacion() == null) {
                    String tipoIdentificacion = EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.
                        getCodigo();
                    this.funcionarioEjecutor.setTipoIdentificacion(tipoIdentificacion);
                }

            } else if (this.tramite.getSolicitud().isViaGubernativa()) {

                /*
                 * REVISAR CON JUAN FELIPE
                 */
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("No se encontró un funcionario ejecutor.");
            }

            if (this.territorialSeleccionada == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("No se encontró una territorial seleccionada");

            }

            // PAÍS, DEPARTAMENTO Y MUNICIPIO
            // Capturar datos del departamento, municipio y pais de la territorial seleccionada
            municipio = this.territorialSeleccionada.getMunicipio();
            departamento = this.territorialSeleccionada.getMunicipio().getDepartamento();
            pais = this.territorialSeleccionada.getMunicipio().getDepartamento().getPais();

            // Se crea la entidad asociada a las pruebas
            TramitePruebaEntidad tramitePruebaEntidad = new TramitePruebaEntidad();

            tramitePruebaEntidad.setPrimerNombre(this.funcionarioEjecutor.getPrimerNombre());
            tramitePruebaEntidad.setSegundoNombre(this.funcionarioEjecutor.getSegundoNombre());
            tramitePruebaEntidad.setPrimerApellido(this.funcionarioEjecutor.getPrimerApellido());
            tramitePruebaEntidad.setSegundoApellido(this.funcionarioEjecutor.getSegundoApellido());
            tramitePruebaEntidad.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
            tramitePruebaEntidad.setTipoIdentificacion(this.funcionarioEjecutor.
                getTipoIdentificacion());
            tramitePruebaEntidad.setNumeroIdentificacion(this.funcionarioEjecutor.
                getIdentificacion());
            tramitePruebaEntidad.setCorreoElectronico(this.funcionarioEjecutor.getEmail());

            tramitePruebaEntidad.setDireccionMunicipio(municipio);
            tramitePruebaEntidad.setDireccionDepartamento(departamento);
            tramitePruebaEntidad.setDireccionPais(pais);
            tramitePruebaEntidad.setDireccion(this.territorialSeleccionada.getDireccion());
            tramitePruebaEntidad.setTelefonoPrincipal(this.territorialSeleccionada.getTelefono());

            tramitePruebaEntidad.setTramitePrueba(this.tramite.getTramitePruebas());
            tramitePruebaEntidad.setUsuarioLog(this.usuario.getLogin());
            tramitePruebaEntidad.setFechaLog(new Date());

            tramitePruebaEntidad.setEstructuraOrganizacional(this.territorialSeleccionada);

            tramitePruebaEntidad.setTipoSolicitante(
                ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION.getCodigo());

            // Establecer datos del departamento, municipio y país
            tramitePruebaEntidad.setDireccionMunicipio(municipio);
            tramitePruebaEntidad.setDireccionDepartamento(departamento);
            tramitePruebaEntidad.setDireccionPais(pais);

            // Se verifica que la entidad externa no esté registrada en las
            // entidades a notificar
            if (!this.isTramitePruebaEntidadRegistrado(tramitePruebaEntidad)) {

                tramitePruebaEntidad = this.getTramiteService().
                    guardarActualizarTramitePruebaEntidad(tramitePruebaEntidad);

                if (tramitePruebaEntidad == null) {
                    this.addMensajeError("No fue posible adicionar la entidad o grupo");
                } else {
                    this.addMensajeInfo("La entidad o grupo fue almacenada correctamente");
                    // Consultar las entidades y grupos de trabajo actualizados
                    this.entidadesOGruposARealizarPruebas = this.getTramiteService().
                        getTramitePruebaEntidadsByTramiteId(this.tramite.getId());
                }
            } else {
                this.addMensajeWarn("La territorial área conservación ya se encuentra registrada.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al guardar el área de conservación de la territorial.");
        }
    }

    //--------------------------------------------------------//
    /**
     * Método que guarda un grupo interno de trabajo
     *
     * @author david.cifuentes
     */
    /*
     * @modified by javier.aponte :: 30-05-2014 :: Se cambió todo el método porque la lógica que
     * estaba antes guardaba el solicitante del trámite en la tabla trámite prueba entidad y no los
     * datos de la territorial ni del funcionario ejecutor
     */
    public void guardarGrupoInternoTrabajo() {

        Departamento departamento;
        Municipio municipio;
        Pais pais;

        Funcionario funcionarioResponsable;
        UsuarioDTO usuarioResponsable = null;

        // Se crea la entidad asociada a las pruebas
        TramitePruebaEntidad tramitePruebaEntidad = new TramitePruebaEntidad();

        // Hay una territorial seleccionada
        try {
            if (this.territorialSeleccionada != null) {

                //Si fue seleccionada la dirección general
                if (this.territorialSeleccionada.isDireccionGeneral()) {
                    if (this.subdireccionOficinaSeleccionada != null) {
                        if (this.grupoInternoTrabajoSeleccionado != null) {

                            funcionarioResponsable = this.grupoInternoTrabajoSeleccionado.
                                getFuncionario();

                            tramitePruebaEntidad.
                                setPrimerNombre(funcionarioResponsable.getNombres());
                            tramitePruebaEntidad.setPrimerApellido(funcionarioResponsable.
                                getApellidos());
                            tramitePruebaEntidad.setTipoPersona(EPersonaTipoPersona.NATURAL.
                                getCodigo());
                            tramitePruebaEntidad.setTipoIdentificacion(funcionarioResponsable.
                                getTipo());
                            tramitePruebaEntidad.setNumeroIdentificacion(String.valueOf(
                                funcionarioResponsable.getIdentificacion()));
                            tramitePruebaEntidad.setCorreoElectronico(funcionarioResponsable.
                                getCorreoElectronico());

                        } else {
                            this.addMensajeWarn("Por favor seleccione un grupo interno de trabajo.");
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.addCallbackParam("error", "error");
                            return;
                        }
                    } else {
                        this.addMensajeWarn(
                            "Por favor seleccione una subdirección u oficina de trabajo");
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    }
                    // No fue seleccionada la dirección general
                } else {

                    usuarioResponsable = this.getGeneralesService().getCacheUsuario(
                        this.loginFuncionarioSeleccionado);

                    if (usuarioResponsable != null) {
                        usuarioResponsable.setTipoIdentificacion(
                            EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());

                        tramitePruebaEntidad.setPrimerNombre(usuarioResponsable.getPrimerNombre());
                        tramitePruebaEntidad.setSegundoNombre(usuarioResponsable.getSegundoNombre());
                        tramitePruebaEntidad.setPrimerApellido(usuarioResponsable.
                            getPrimerApellido());
                        tramitePruebaEntidad.setSegundoApellido(usuarioResponsable.
                            getSegundoApellido());
                        tramitePruebaEntidad.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
                        tramitePruebaEntidad.setTipoIdentificacion(usuarioResponsable.
                            getTipoIdentificacion());
                        tramitePruebaEntidad.setNumeroIdentificacion(usuarioResponsable.
                            getIdentificacion());
                        tramitePruebaEntidad.setCorreoElectronico(usuarioResponsable.getEmail());
                    } else {
                        this.addMensajeWarn(
                            "Ocurrió un error al consultar el usuario responsable, por favor comuníquese con el administrador");
                        RequestContext context = RequestContext.getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    }

                }

                municipio = this.territorialSeleccionada.getMunicipio();
                departamento = this.territorialSeleccionada.getMunicipio().getDepartamento();
                pais = this.territorialSeleccionada.getMunicipio().getDepartamento().getPais();

                tramitePruebaEntidad.setDireccionMunicipio(municipio);
                tramitePruebaEntidad.setDireccionDepartamento(departamento);
                tramitePruebaEntidad.setDireccionPais(pais);
                tramitePruebaEntidad.setDireccion(this.territorialSeleccionada.getDireccion());
                tramitePruebaEntidad.setTelefonoPrincipal((this.territorialSeleccionada.
                    getTelefono().length() > 15) ? this.territorialSeleccionada.getTelefono().
                            substring(0, 15) : this.territorialSeleccionada.getTelefono());

                tramitePruebaEntidad.setTramitePrueba(this.tramite.getTramitePruebas());
                tramitePruebaEntidad.setUsuarioLog(this.usuario.getLogin());
                tramitePruebaEntidad.setFechaLog(new Date());

                tramitePruebaEntidad.setEstructuraOrganizacional(this.territorialSeleccionada);

                tramitePruebaEntidad.setTipoSolicitante(
                    ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO.getCodigo());

                // Establecer datos del departamento, municipio y país
                tramitePruebaEntidad.setDireccionMunicipio(municipio);
                tramitePruebaEntidad.setDireccionDepartamento(departamento);
                tramitePruebaEntidad.setDireccionPais(pais);

                // Se verifica que la entidad externa no esté registrada en
                // las entidades a notificar
                if (!this.isTramitePruebaEntidadRegistrado(tramitePruebaEntidad)) {
                    tramitePruebaEntidad = this.getTramiteService().
                        guardarActualizarTramitePruebaEntidad(tramitePruebaEntidad);

                    tramite.getTramitePruebas().getTramitePruebaEntidads().add(tramitePruebaEntidad);

                    // Consultar las entidades y grupos de trabajo
                    // actualizados
                    this.entidadesOGruposARealizarPruebas = this.getTramiteService().
                        getTramitePruebaEntidadsByTramiteId(this.tramite.getId());
                    this.addMensajeInfo("La entidad o grupo fue almacenada correctamente");
                } else {
                    this.addMensajeWarn(
                        "El usuario, entidad o grupo seleccionado ya ha sido registrado.");
                }

                // No se ha seleccionado una territorial
            } else {
                this.addMensajeWarn("Por favor seleccione una territorial");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al guardar el grupo interno de trabajo.");
        }
    }

    // -------------------------------------------------- //
    /**
     * Método que guarda una entidad externa
     *
     * @author david.cifuentes
     */
    public void guardarEntidadExterna() {
        Solicitante solicitante = null;
        RequestContext context = RequestContext.getCurrentInstance();

        try {

            if (validarSolicitante()) {

                this.solicitanteAfectadoSeleccionado.setDireccionPais(this.paisSolicitante);
                this.solicitanteAfectadoSeleccionado.setDireccionDepartamento(
                    this.departamentoSolicitante);
                this.solicitanteAfectadoSeleccionado.
                    setDireccionMunicipio(this.municipioSolicitante);
                this.solicitanteAfectadoSeleccionado.setTipoSolicitante(
                    ESolicitanteTipoSolicitant.ENTIDAD_EXTERNA.getCodigo());

                solicitante = this.getTramiteService().guardarActualizarSolicitante(
                    this.solicitanteAfectadoSeleccionado);
            }

            if (solicitante != null) {

                // Se crea la nueva entidad externa asociada al trámite
                TramitePruebaEntidad tramitePruebaEntidad = new TramitePruebaEntidad();

                tramitePruebaEntidad.incorporarDatosSolicitante(solicitante);
                tramitePruebaEntidad.setTramitePrueba(this.tramite.getTramitePruebas());
                tramitePruebaEntidad.setSolicitante(solicitante);
                tramitePruebaEntidad.setUsuarioLog(this.usuario.getLogin());
                tramitePruebaEntidad.setFechaLog(new Date());
                tramitePruebaEntidad.setTipoSolicitante(ESolicitanteTipoSolicitant.ENTIDAD_EXTERNA.
                    getCodigo());

                if (this.isTramitePruebaEntidadRegistrado(tramitePruebaEntidad)) {
                    tramitePruebaEntidad = this.entidadOGrupoSeleccionado;
                }

                tramitePruebaEntidad.setDireccionPais(this.paisSolicitante);
                tramitePruebaEntidad.incorporarDatosSolicitante(solicitante);
                tramitePruebaEntidad.setSolicitante(solicitante);
                tramitePruebaEntidad.setDireccionDepartamento(this.departamentoSolicitante);
                tramitePruebaEntidad.setDireccionMunicipio(this.municipioSolicitante);

                tramitePruebaEntidad.setDireccionPais(this.paisSolicitante);
                tramitePruebaEntidad.setDireccionDepartamento(this.departamentoSolicitante);
                tramitePruebaEntidad.setDireccionMunicipio(this.municipioSolicitante);

                // Se verifica que la entidad externa no esté registrada en las
                // entidades a notificar
                tramitePruebaEntidad = this.getTramiteService()
                    .guardarActualizarTramitePruebaEntidad(tramitePruebaEntidad);

                this.tramite.getTramitePruebas().getTramitePruebaEntidads().
                    add(tramitePruebaEntidad);

                // Consultar las entidades y grupos de trabajo
                // actualizados
                this.entidadesOGruposARealizarPruebas = this
                    .getTramiteService()
                    .getTramitePruebaEntidadsByTramiteId(this.tramite.getId());

                this.addMensajeInfo("La entidad o grupo fue almacenada correctamente");
                this.entidadOGrupoSeleccionado = null;
                this.solicitanteAfectadoSeleccionado = null;

            } else {
                context.addCallbackParam("error", "error");
                this.addMensajeError("Error al guardar la entidad externa");
            }
        } catch (Exception e) {
            e.printStackTrace();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al guardar la entidad externa.");
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método que guarda entidades o grupos de trabajo
     */
    public void guardarEntidadGrupo() {

        // Se trata del área de conservación de una territorial:
        if (this.isTerritorialAreaConservacion()) {
            guardarTerritorialAreaConservacion();
        } // Se trata de un GIT
        else if (this.isGrupoInternoTrabajo()) {
            guardarGrupoInternoTrabajo();
        } else if (this.isEntidadExterna()) {
            guardarEntidadExterna();
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método que inicializa las variables para adicionar una nueva entidad o grupo de trabajo.
     */
    public void setupNuevaEntidadOGrupo() {
        // Verificar que no se haya radicado por primera vez la comunicación
        if (validateRadicadoPrimerVez()) {
            return;
        }

        this.responsableUOC = null;
        this.banderaBusquedaAfectadoRealizada = false;
        this.entidadGrupoInternoPrueba = null;
        this.resetCombosSelects();
    }

    // ------------------------------------------------------- //
    /**
     * Método que inicializa las variables de los combos.
     */
    public void resetCombosSelects() {
        this.funcionarioSeleccionado = "";
        this.loginFuncionarioSeleccionado = "";
        this.eMailFuncionarioSeleccionado = "";
        this.territorialSeleccionada = null;
        this.territorialSeleccionadaCodigo = null;
        this.subdireccionOficinaSeleccionada = null;
        this.subdireccionOficinaSeleccionadaCodigo = null;
        this.grupoInternoTrabajoSeleccionado = null;
        this.grupoInternoTrabajoSeleccionadoCodigo = null;
        this.unidadOperativaSeleccionada = null;
        this.unidadOperativaSeleccionadaCodigo = null;
    }

    // ------------------------------------------------------- //
    /**
     * Método que genera el reporte de comunicación auto de pruebas, para solicitantes o afectados y
     * para entidades o grupos. Hay dos opciones para generarlo: - Si no se ha radicado ningún
     * solicitante se genera sin número de radicación. - Si se está radicando o ya se radicó, se
     * radica la comunicación y se genera el reporte con el número de radicación.
     *
     * @author david.cifuentes
     */
    public void generarReporteComunicacionAutoDePruebas() {

        boolean answer = false;
        boolean comunicacionRadicadaBool = false;
        boolean selectedSolicitanteOEntidad = true;

        if (this.solicitanteSelectBool) {
            // Verificar que el solicitante o afectado seleccionado no sea nulo
            if (this.solicitanteAfectadoSeleccionado == null) {
                this.addMensajeWarn("Por favor seleccione antes un solicitante o afectado.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                selectedSolicitanteOEntidad = false;
                return;
            }
        } else {
            // Verificar que la entidad externa o grupo de trabajo interno seleccionado no sea nulo
            if (this.entidadOGrupoSeleccionado == null) {
                this.addMensajeWarn("Por favor seleccione antes una entidad o grupo de trabajo.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                selectedSolicitanteOEntidad = false;
                return;
            }
        }

        if (selectedSolicitanteOEntidad) {
            // Si el documento ya se radico anteriormente, se debe cargar el
            // radicado, y no generar uno nuevo.
            comunicacionRadicadaBool = comunicacionRadicadaParaEsteSolicitante();

            if (comunicacionRadicadaBool) {
                // SE ENCONTRÓ LA COMUNICACIÓN Y FUE BUSCADA EN ALFRESCO
                LOGGER.debug("Url público del archivo:" + this.reporteComunicacionAutoDePruebas.
                    getUrlWebReporte());
                this.reporteGeneradoOK = true;
            } else {

                // NO SE ENCONTRÓ LA COMUNICACIÓN, HAY QUE GENERAR EL REPORTE
                try {

                    // Seleccionar nombre del reporte
                    EReporteServiceSNC enumeracionReporte = reporteAGenerar();

                    if (enumeracionReporte != null) {
                        // Reemplazar parametros para el reporte.
                        Map<String, String> parameters = replaceReportsParameters();

                        // Generar el reporte
                        this.reporteComunicacionAutoDePruebas = reportsService.generarReporte(
                            parameters,
                            enumeracionReporte, this.usuario);

                        if (this.reporteComunicacionAutoDePruebas != null) {
                            answer = true;
                            this.radicadoBool = false;
                        } else {
                            this.addMensajeError(
                                "Error al generar la comunicación de auto de pruebas.");
                            RequestContext context = RequestContext.getCurrentInstance();
                            context.addCallbackParam("error", "error");
                            answer = false;
                            return;
                        }
                    }
                } catch (Exception ex) {
                    this.addMensajeError("Error al generar la comunicación de auto de pruebas.");
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");

                    LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
                    throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(
                        LOGGER, ex);
                }
                this.reporteGeneradoOK = answer;
            }
        }

    }

    // ------------------------------------------------------- //
    /**
     * Método que devuelve el nombre del reporte a utilizar dependiendo de donde o para quien se
     * quiera generar el reporte.
     *
     * @author david.cifuentes
     */
    public EReporteServiceSNC reporteAGenerar() {
        EReporteServiceSNC enumeracionReporte = null;

        try {
            if (this.isSolicitanteReportBool) {
                enumeracionReporte =
                    EReporteServiceSNC.COMUNICACION_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_O_AFECTADOS;
                this.tipoComunicacionAutoDePruebas =
                    ETipoDocumento.OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS.
                        getId();
            } else if (this.entidadOGrupoSeleccionado != null &&
                 this.entidadOGrupoSeleccionado.getTipoSolicitante() != null &&
                 (this.entidadOGrupoSeleccionado
                    .getTipoSolicitante()
                    .equals(ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO
                        .getCodigo()) || this.entidadOGrupoSeleccionado
                    .getTipoSolicitante()
                    .equals(ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION
                        .getCodigo()))) {
                enumeracionReporte =
                    EReporteServiceSNC.COMUNICACION_AUTO_DE_PRUEBAS_A_GRUPO_INTERNO_DE_TRABAJO;
                this.tipoComunicacionAutoDePruebas =
                    ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.getId();
            } else if (this.entidadOGrupoSeleccionado != null &&
                 this.entidadOGrupoSeleccionado.getTipoSolicitante() != null &&
                 this.entidadOGrupoSeleccionado.getTipoSolicitante()
                    .equals(ESolicitanteTipoSolicitant.ENTIDAD_EXTERNA
                        .getCodigo())) {

                enumeracionReporte =
                    EReporteServiceSNC.COMUNICACION_AUTO_DE_PRUEBAS_A_ENTIDAD_EXTERNA;
                this.tipoComunicacionAutoDePruebas =
                    ETipoDocumento.OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA.getId();
            } else {
                enumeracionReporte =
                    EReporteServiceSNC.COMUNICACION_AUTO_DE_PRUEBAS_A_ENTIDAD_EXTERNA;
                this.tipoComunicacionAutoDePruebas =
                    ETipoDocumento.OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA.getId();
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
        }
        return enumeracionReporte;
    }

    // ------------------------------------------------------- //
    /**
     * Método que reemplaza los parametros para el reporte.
     *
     * @author david.cifuentes
     * @modified by javier.aponte
     */
    /*
     * @modified by leidy.gonzalez :: #14054:: 20/08/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public Map<String, String> replaceReportsParameters() {
        Map<String, String> parameters = new HashMap<String, String>();
        try {

            if (this.isSolicitanteReportBool) {
                // Parametro del id del trámite.
                parameters.put("TRAMITE_ID", String.valueOf(this.tramite.getId()));

                // Parametro afectado seleccionado.
                if (this.solicitanteAfectadoSeleccionado != null) {
                    if (this.solicitanteAfectadoSeleccionado.getId() != null) {
                        parameters.put("SOLICITANTE_ID",
                            String.valueOf(this.solicitanteAfectadoSeleccionado.getId()));
                    }
                }
            } else {
                //Id del trámite prueba entidad
                parameters.put("TRAMITE_PRUEBA_ENTIDAD_ID", String.valueOf(
                    this.entidadOGrupoSeleccionado.getId()));

                if (this.territorialUsuario != null) {
                    //Dirección de la territorial donde se encuentra el usuario autenticado en el sistema
                    parameters.put("DIR_TERRITORIAL", this.territorialUsuario.getDireccion());
                    //Teléfono de la territorial donde se encuentra el usuario autenticado en el sistema
                    parameters.put("TEL_TERRITORIAL", this.territorialUsuario.getTelefono());
                }
            }

            // Parametro número de radicado.
            if (this.numeroRadicado != null &&
                 !this.numeroRadicado.trim().isEmpty()) {
                parameters.put("NUMERO_RADICADO", this.numeroRadicado);
            }

            // Parametro director territorial o responsable de conservación
            //lorena.salamanca :: 2015-11-20 :: Quien debe firmar la comunicacion es el director de la territorial
            //			UsuarioDTO responsableConservacion = null;
            //
            //	        responsableConservacion = this.getGeneralesService().obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(this.tramite, this.usuario);
            //			
            //			if (responsableConservacion != null
            //					&& responsableConservacion.getNombreCompleto() != null) {
            //				parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
            //						responsableConservacion.getNombreCompleto());
            //				
            //				firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
            //						responsableConservacion.getNombreCompleto());
            //
            //				if (firma != null && responsableConservacion.getNombreCompleto().equals(firma.getNombreFuncionarioFirma())) {
            //					
            //					documentoFirma= this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview
            //							(firma.getDocumentoId().getIdRepositorioDocumentos());
            //
            //					parameters.put("FIRMA_USUARIO", documentoFirma);
            //				}
            //			}
            String estructuraOrg = this.getGeneralesService().
                getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    this.tramite.getPredio().getMunicipio().getCodigo());
            List<UsuarioDTO> directores = this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(estructuraOrg, ERol.DIRECTOR_TERRITORIAL);
            UsuarioDTO director = directores.get(0);

            if (director != null && director.getNombreCompleto() != null) {
                FirmaUsuario firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                    director.getNombreCompleto());

                if (firma != null && director.getNombreCompleto().equals(firma.
                    getNombreFuncionarioFirma())) {
                    String documentoFirma = this.getGeneralesService().
                        descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                            getIdRepositorioDocumentos());
                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            return null;
        }

        return parameters;
    }

    // ------------------------------------------------------- //
    /**
     * Método encargado de consolidar el auto de pruebas de tal manera, que no se pueda modificar.
     * Tambien calcula la cantidad de folios del auto de pruebas y la guarda en el documento
     * asociado.
     *
     * @author david.cifuentes
     */
    public void consolidarAutoDePruebas() {
        // Guarda el documento de auto de pruebas en alfresco
        // y allí calcula la cantidad de folios del mismo.
        this.guardarDocumentoAutoDePruebas();

        // Se marca como consolidado haciendo el set de la variable 
        // que indica que ya se radicó por primera vez.
        this.radicadoPrimeraVezBool = true;
    }

    // ------------------------------------------------------- //
    /**
     * Método que crea un documento. Luego guarda el documento en Alfresco y en la base de datos
     * mediante el llamado al método guardar documento de la interfaz de ITramite. Finalmente como
     * el documento es un auto de pruebas le asocia el documento a trámite prueba y actualiza el
     * trámite
     *
     * @author javier.aponte
     */
    public void guardarDocumentoAutoDePruebas() {

        Documento documento = new Documento();
        int cantidadFoliosAutoDePruebas = 0;

        if (this.reporteDocumentoAutoDePruebas == null) {
            this.generarDocumentoAutoDePruebas();
        }

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            if (this.tramite.getSolicitud().isViaGubernativa()) {
                tipoDocumento = this.getGeneralesService().buscarTipoDocumentoPorId(
                    ETipoDocumento.AUTO_DE_PRUEBAS_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId());
            } else {
                tipoDocumento = this.getGeneralesService().buscarTipoDocumentoPorId(
                    ETipoDocumento.AUTO_DE_PRUEBAS_REVISION_AVALUO_AUTOESTIMACION_Y_REPOSICION.
                        getId());
            }

            documento.setTipoDocumento(tipoDocumento);
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(this.tramite.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setFechaDocumento(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documento.setDescripcion("Documento de auto de pruebas generado por el sistema");

            if (this.reporteDocumentoAutoDePruebas != null) {
                documento.setArchivo(this.reporteDocumentoAutoDePruebas.
                    getRutaCargarReporteAAlfresco());
                // Calcular la cantidad de folios del auto de pruebas.
                PdfReader pdfDocument = new PdfReader(new FileInputStream(new File(
                    this.reporteDocumentoAutoDePruebas.getRutaCargarReporteAAlfresco())));
                cantidadFoliosAutoDePruebas = pdfDocument.getNumberOfPages();
            }

            documento.setCantidadFolios(cantidadFoliosAutoDePruebas);

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.usuario, documento);

            //Le asocia el auto de pruebas al trámite prueba asociada al trámite 
            //y actualiza el trámite
            this.tramite.getTramitePruebas().setAutoPruebasDocumento(documento);

            this.getTramiteService().guardarActualizarTramitePruebas(this.tramite.
                getTramitePruebas(), this.usuario, null);

        } catch (Exception e) {
            LOGGER.error("Error al guardar el auto de pruebas en alfresco", e);
        }
    }

    // ------------------------------------------------------- //
    /**
     * Método que verifica si ya se radico la comunicación para el solicitante o afectado
     * seleccionado. De ser así la consulta en los TramiteDocumentos del trámite, si no returna
     * false y procede a la generación del reporte de la comunicación.
     *
     * @author david.cifuentes
     *
     * @modified by juanfelipe.garcia adicion de validacion, td.getIdPersonaNotificacion(), cuanso
     * es null
     */
    public boolean comunicacionRadicadaParaEsteSolicitante() {

        boolean comunicacionRadicadaBool = false;
        boolean auxSelectedSolicitanteOEntidad = false;
        TramiteDocumento tramiteDoc = null;

        try {
            if (this.solicitanteSelectBool &&
                 this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados != null) {
                for (TramiteDocumento td
                    : this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados) {

                    if (this.solicitanteAfectadoSeleccionado != null) {
                        if (td.getIdPersonaNotificacion() != null && td.getIdPersonaNotificacion().
                            equals(this.solicitanteAfectadoSeleccionado.getNumeroIdentificacion())) {
                            if (td.getTipoIdPersonaNotificacion() != null && td.
                                getIdPersonaNotificacion().equals(
                                    this.solicitanteAfectadoSeleccionado.getTipoIdentificacion())) {
                                auxSelectedSolicitanteOEntidad = true;
                                tramiteDoc = td;
                                break;
                            }
                        }
                    }

                }
            } else if (this.entidadOGrupoSeleccionado != null) {
                List<TramiteDocumento> auxTramiteDoc = new ArrayList<TramiteDocumento>();
                if (this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna != null ||
                    !this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna.isEmpty()) {
                    auxTramiteDoc
                        .addAll(this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna);
                }
                if (this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac != null) {
                    auxTramiteDoc
                        .addAll(this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac);

                }
                for (TramiteDocumento td : auxTramiteDoc) {
                    if (this.entidadOGrupoSeleccionado != null) {

                        if (td.getDocumento() != null &&
                             td.getDocumento().getTipoDocumento() != null &&
                             !td.getDocumento().getTipoDocumento().equals(
                                ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.
                                    getId())) {

                            if (td.getIdPersonaNotificacion() != null && td.
                                getIdPersonaNotificacion().equals(this.entidadOGrupoSeleccionado.
                                    getNumeroIdentificacion())) {
                                if (td.getTipoIdPersonaNotificacion() != null && td.
                                    getIdPersonaNotificacion().equals(
                                        this.entidadOGrupoSeleccionado.getTipoIdentificacion())) {
                                    auxSelectedSolicitanteOEntidad = true;
                                    tramiteDoc = td;
                                    break;
                                } else {
                                    this.addMensajeWarn(
                                        "No se encontró el tipo de documento de la persona notificada, por favor comúniquese con el administrador");
                                    return false;
                                }
                            }

                        }

                    }
                }
            }
            if (auxSelectedSolicitanteOEntidad) {

                comunicacionRadicadaBool = true;
                // ---------------------------------------//
                // Cargar el reporte encontrado.
                // ---------------------------------------//

                if (tramiteDoc.getDocumento() == null ||
                     tramiteDoc.getIdRepositorioDocumentos() == null ||
                     tramiteDoc.getIdRepositorioDocumentos().trim()
                        .isEmpty()) {

                    // No debería entrar acá.
                    this.addMensajeError("No se encontró el documento de la comunicación");
                    comunicacionRadicadaBool = false;

                } else {
                    this.reporteComunicacionAutoDePruebas = this.reportsService
                        .consultarReporteDeGestorDocumental(tramiteDoc
                            .getIdRepositorioDocumentos());
                }
            } else {
                comunicacionRadicadaBool = false;
            }

        } catch (Exception e) {
            LOGGER.error("Error al recuperar la comunicación auto de pruebas de Alfresco." + e.
                getMessage(), e);
            if (this.reporteComunicacionAutoDePruebas == null) {
                this.addMensajeError(
                    "Error al recuperar la comunicación de auto de pruebas guardada.");
            }
        }
        return comunicacionRadicadaBool;
    }

    // ----------------------------------------------------- //
    /**
     * se marcan los solicitantes a quienes ya se les hizo registro de la comunicación de
     * notificación. El criterio es: se marca el solicitante X si existe un TramiteDocumento del
     * tipo Comunicación de notificación para este trámite, para el solicitante X, cuya fecha de
     * notificación esté en null
     *
     * Se aprovecha para dar valor a la bandera que indica si solo queda un solicitante pendiente de
     * registro de comunicación
     */
    private void marcarSolicitantesConRegistroComunic() {

        int hmSinRegistro = 0;
        int hmConRegistro;
        boolean docRegistrado;
        for (Solicitante solicitante : this.solicitantesAfectados) {
            solicitante.setComunicacionDeNotificacionRegistrada(false);
            docRegistrado = false;
            for (TramiteDocumento td : this.tramiteDocumentosDelTramite) {
                if (td.getIdPersonaNotificacion().equals(
                    solicitante.getNumeroIdentificacion())) {
                    if (td.getFechaNotificacion() != null) {
                        docRegistrado = true;
                        break;
                    }
                }
            }
            if (!docRegistrado) {
                hmSinRegistro++;
            } else {
                solicitante.setComunicacionDeNotificacionRegistrada(true);
            }
        }
        this.lastSolicitanteForRegistroComunicNotif = (hmSinRegistro <= 1) ? true : false;

        hmConRegistro = this.solicitantesAfectados.size() - hmSinRegistro;
        this.todosRegistrados =
            (hmConRegistro == this.solicitantesAfectados.size()) ? true : false;
    }

    // ----------------------------------------------------- //
    /**
     * Método que guarda el documento con la comunicación auto de pruebas en el servidor documental,
     * e inserta el respectivo registro en la tabla Documento
     */
    private boolean guardarDocumentoComunicAutoDePruebas() {

        boolean answer = true;
        Documento tempDocumento;
        TipoDocumento tipoDoc;
        Long tipoDocumentoId;
        Date fechaLogYRadicacion;

        tipoDocumentoId = this.tipoComunicacionAutoDePruebas;

        try {
            tipoDoc = this.getGeneralesService().buscarTipoDocumentoPorId(
                tipoDocumentoId);
        } catch (Exception ex) {
            LOGGER.error("error buscando TipoDocumento con id " +
                 tipoDocumentoId + " : " + ex.getMessage(), ex);
            return false;
        }

        tempDocumento = new Documento();
        fechaLogYRadicacion = new Date(System.currentTimeMillis());

        // D: se arma el objeto Documento con todos los atributos posibles...
        // D: el método que guarda el documento en el gestor documental recibe
        // el nombre del archivo sin la ruta
        tempDocumento.setArchivo(this.reporteComunicacionAutoDePruebas.
            getRutaCargarReporteAAlfresco());
        tempDocumento.setTramiteId(this.tramite.getId());
        tempDocumento.setTipoDocumento(tipoDoc);
        tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
        tempDocumento.setUsuarioCreador(this.usuario.getLogin());
        tempDocumento.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        tempDocumento.setFechaRadicacion(fechaLogYRadicacion);

        tempDocumento.setFechaLog(fechaLogYRadicacion);
        tempDocumento.setNumeroRadicacion(this.numeroRadicado);
        tempDocumento.setUsuarioLog(this.usuario.getLogin());
        tempDocumento
            .setDescripcion("Documento de comunicación auto de pruebas generado por el sistema");

        // Asociación de la identificación del solicitante o la entidad al documento.
        if (this.solicitanteSelectBool) {
            // Verificar que el solicitante o afectado seleccionado no sea nulo
            if (this.solicitanteAfectadoSeleccionado != null) {
                tempDocumento
                    .setObservaciones(this.solicitanteAfectadoSeleccionado
                        .getNumeroIdentificacion());
            }
        } else {
            // Verificar que la entidad externa o grupo de trabajo interno
            // seleccionado no sea nulo
            if (this.entidadOGrupoSeleccionado != null) {
                tempDocumento.setObservaciones(this.entidadOGrupoSeleccionado
                    .getNumeroIdentificacion());
            }
        }

        try {
            // D: se guarda el documento
            this.documentoComunicacionAutoDePruebas = this.getTramiteService()
                .guardarDocumento(this.usuario, tempDocumento);
        } catch (Exception ex) {
            LOGGER.error("error guardando Documento : " + ex.getMessage(), ex);
            return false;
        }

        return answer;
    }

    // ----------------------------------------------------- //
    /**
     * Guarda registros en la tabla Tramite_Documento por cada uno de los solicitantes a los que se
     * les envía la comunicación.
     *
     * PRE: se ha ejecutado el método guardarDocumentoComunicAutoDePruebas
     *
     * Deja en null la fecha de notificación (y el método de notificación) porque ese criterio (que
     * sea null) se utiliza para saber si ya se hizo el registro de la comunicación de notificación
     *
     * @return
     */
    private boolean guardarTramiteDocumentosComunicAutoDePruebas() {

        boolean answer = true;
        TramiteDocumento tramDoc, tempTramDoc;

        if (this.documentoComunicacionAutoDePruebas == null) {
            return false;
        }

        tramDoc = new TramiteDocumento();
        tramDoc.setTramite(this.tramite);
        tramDoc.setDocumento(this.documentoComunicacionAutoDePruebas);
        tramDoc.setIdRepositorioDocumentos(this.documentoComunicacionAutoDePruebas
            .getIdRepositorioDocumentos());
        tramDoc.setfecha(new Date(System.currentTimeMillis()));
        tramDoc.setFechaLog(new Date(System.currentTimeMillis()));
        tramDoc.setUsuarioLog(this.usuario.getLogin());

        // Asociación de valores del solicitante o la entidad
        if (this.solicitanteSelectBool) {
            // Verificar que el solicitante o afectado seleccionado no sea nulo
            if (this.solicitanteAfectadoSeleccionado != null) {

                tramDoc.setPersonaNotificacion(this.solicitanteAfectadoSeleccionado
                    .getNombreCompleto());
                tramDoc.setIdPersonaNotificacion(this.solicitanteAfectadoSeleccionado.
                    getNumeroIdentificacion());
                tramDoc.setTipoIdPersonaNotificacion(this.solicitanteAfectadoSeleccionado.
                    getTipoIdentificacion());
                tramDoc.setObservacionNotificacion(this.solicitanteAfectadoSeleccionado.
                    getNumeroIdentificacion());
            }
        } else {
            // Verificar que la entidad externa o grupo de trabajo interno
            // seleccionado no sea nulo
            if (this.entidadOGrupoSeleccionado != null) {
                tramDoc.setPersonaNotificacion(this.entidadOGrupoSeleccionado.getNombreCompleto());
                tramDoc.setIdPersonaNotificacion(this.entidadOGrupoSeleccionado.
                    getNumeroIdentificacion());
                tramDoc.setTipoIdPersonaNotificacion(this.entidadOGrupoSeleccionado.
                    getTipoIdentificacion());
                tramDoc.setObservacionNotificacion(this.entidadOGrupoSeleccionado.
                    getNumeroIdentificacion());
            }
        }

        try {
            tempTramDoc = this.getTramiteService().actualizarTramiteDocumento(
                tramDoc);
            if (tempTramDoc != null) {
                if (this.tramite.getTramiteDocumentos() == null) {
                    this.tramite.setTramiteDocumentos(new ArrayList<TramiteDocumento>());
                }
                this.tramite.getTramiteDocumentos().add(tempTramDoc);
                if (this.tramiteDocumentosDelTramite == null) {
                    this.tramiteDocumentosDelTramite = new ArrayList<TramiteDocumento>();
                }
                this.tramiteDocumentosDelTramite.add(tempTramDoc);
            } else {
                LOGGER.warn("No fue posible crear el trámite documento.");
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("Error guardando un TramiteDocumento en " +
                 "envioComunicacionAutoPruebas#guardarTramiteDocumentosComunicAutoDePruebas: " +
                 ex.getMessage(), ex);
            return false;
        }

        return answer;
    }

    // ----------------------------------------------------- //
    /**
     * Método que envia un correo con el reporte de la comunicación del auto de pruebas al
     * solicitante seleccionado.
     */
    private boolean enviarCorreo() {

        boolean answer = true;

        // Verificar que el solicitante/afectado o la entidad/grupo si tenga un
        // correo asociado
        if (this.solicitanteSelectBool) {
            // Verificar que el solicitante o afectado seleccionado no sea nulo
            if (this.solicitanteAfectadoSeleccionado != null) {
                if (this.solicitanteAfectadoSeleccionado.getCorreoElectronico() == null ||
                     this.solicitanteAfectadoSeleccionado
                        .getCorreoElectronico().trim().isEmpty()) {
                    this.addMensajeWarn(
                        "No fue posible enviar el correo para el solicitante o afectado seleccionado pues éste no tiene un correo electrónico asociado.");
                    return answer;
                }
            }
        } else {
            // Verificar que la entidad externa o grupo de trabajo interno
            // seleccionado no sea nulo
            if (this.entidadOGrupoSeleccionado != null) {
                if (this.entidadOGrupoSeleccionado.getCorreoElectronico() == null ||
                     this.entidadOGrupoSeleccionado
                        .getCorreoElectronico().trim().isEmpty()) {
                    this.addMensajeWarn(
                        "No fue posible enviar el correo para la entidad externa o grupo interno de trabajo pues éste no tiene un correo electrónico asociado.");
                    return answer;
                }
            }
        }

        String asunto =
            ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_AUTO_DE_PRUEBAS;
        String contenido =
            ConstantesComunicacionesCorreoElectronico.CONTENIDO_COMUNICACION_DE_AUTO_DE_PRUEBAS;

        // Reemplazar parametros
        // ------ Número del trámite ----- //
        contenido = contenido.replaceFirst("\\Q{p\\E}", this.tramite.getNumeroRadicacion());
        // ------ Responsable de conservación quien firma ----- //		
        contenido = contenido.replaceFirst("\\Q{p\\E}", this.usuario.getNombreCompleto());

        String[] adjuntos;
        String[] titulos;

        // Adjuntar auto de prueas
        if (this.reporteDocumentoAutoDePruebas == null) {
            this.generarDocumentoAutoDePruebas();
        }

        // Se adjunta la comunicación auto de pruebas y el auto de pruebas.
        adjuntos = new String[]{
            this.reporteComunicacionAutoDePruebas.getRutaCargarReporteAAlfresco(),
            this.reporteDocumentoAutoDePruebas.getRutaCargarReporteAAlfresco()};

        titulos = new String[]{
            ConstantesComunicacionesCorreoElectronico.ARCHIVO_COMUNICACION_AUTO_DE_PRUEBAS,
            ConstantesComunicacionesCorreoElectronico.ARCHIVO_AUTO_DE_PRUEBAS};

        try {

            if (this.solicitanteSelectBool) {
                // Verificar que el solicitante o afectado seleccionado no sea
                // nulo
                if (this.solicitanteAfectadoSeleccionado != null) {
                    if (this.solicitanteAfectadoSeleccionado
                        .getCorreoElectronico() != null &&
                         !this.solicitanteAfectadoSeleccionado
                            .getCorreoElectronico().trim().isEmpty()) {

                        this.getGeneralesService()
                            .enviarCorreo(
                                this.solicitanteAfectadoSeleccionado
                                    .getCorreoElectronico(),
                                asunto, contenido, adjuntos, titulos);
                    }
                }
            } else {
                // Verificar que la entidad externa o grupo de trabajo interno
                // seleccionado no sea nulo
                if (this.entidadOGrupoSeleccionado != null) {
                    if (this.entidadOGrupoSeleccionado.getCorreoElectronico() != null &&
                         !this.entidadOGrupoSeleccionado
                            .getCorreoElectronico().trim().isEmpty()) {

                        this.getGeneralesService()
                            .enviarCorreo(
                                this.entidadOGrupoSeleccionado
                                    .getCorreoElectronico(),
                                asunto, contenido, adjuntos, titulos);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico en " +
                 "envioComunicacionAutoPruebas#enviarCorreo: " +
                 e.getMessage());
            answer = false;
        }
        return answer;
    }

    // ----------------------------------------------------- //
    /**
     * Método que se encarga de radicar la comunicación a solicitantes afectados, del solicitante,
     * afectado, entidad ezterna o grupo interno seleccionado.
     *
     * Action del botón "radicar" de la pantalla con el doc de comunicación auto de pruebas
     *
     * 1. obtiene número de radicado del documento 2. vuelve a generar el reporte con en número de
     * radicado 3. guarda el documento en el gestor documental e inserta el respectivo registro en
     * BD 4. crear los registros en la tabla Tramite_Documento 5. enviar correo al solicitante
     *
     * @author david.cifuentes
     */
    public void radicarComunicacion() {

        int exceptionIdentifier = 0;

        if (this.entidadOGrupoSeleccionado == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("No se encontró un funcionario ejecutor.");
        } else {
            if (ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION.getCodigo().equals(
                this.entidadOGrupoSeleccionado.getTipoSolicitante())) {
                if (this.entidadOGrupoSeleccionado.getEstructuraOrganizacional() == null) {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError(
                        "No hay una territorial de área de conservación asociada, por favor comuniquese con el administrador");
                }

            }
        }

        try {
            // 1. Generación del número de Radicación en correspondencia
            // (sticker de radicación)

            if (this.isSolicitanteReportBool) {

                this.numeroRadicado = this.obtenerNumeroRadicadoSolicitantesYAfectados();

            } else if (this.entidadOGrupoSeleccionado != null &&
                 this.entidadOGrupoSeleccionado.getTipoSolicitante() != null &&
                 (this.entidadOGrupoSeleccionado
                    .getTipoSolicitante()
                    .equals(ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO
                        .getCodigo()) || this.entidadOGrupoSeleccionado
                    .getTipoSolicitante()
                    .equals(ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION
                        .getCodigo()))) {

                this.numeroRadicado = this.obtenerNumeroRadicadoOficinaInterna();

            } else if (this.entidadOGrupoSeleccionado != null &&
                 this.entidadOGrupoSeleccionado.getTipoSolicitante() != null &&
                 this.entidadOGrupoSeleccionado.getTipoSolicitante()
                    .equals(ESolicitanteTipoSolicitant.ENTIDAD_EXTERNA
                        .getCodigo())) {

                this.numeroRadicado = this.obtenerNumeroRadicadoEntidadExterna();

            } else {

                this.numeroRadicado = this.obtenerNumeroRadicadoEntidadExterna();

            }

            if (this.numeroRadicado == null || this.numeroRadicado.equals("")) {
                exceptionIdentifier = 1;
                throw new Exception("No se pudo generar el número de radicación");
            }

            // 2. Generación del reporte con el número de radicación
            this.generarReporteComunicacionAutoDePruebas();
            if (!this.reporteGeneradoOK) {
                exceptionIdentifier = 2;
                throw new Exception(
                    "No se pudo generar el reporte de comunicación del auto de pruebas");
            }

            //D: 3.
            if (!this.guardarDocumentoComunicAutoDePruebas()) {
                exceptionIdentifier = 3;
                throw new Exception(
                    "No se pudo guardar el documento del reporte de comunicación del auto de pruebas");
            }

            //D: 4.
            if (!this.guardarTramiteDocumentosComunicAutoDePruebas()) {
                exceptionIdentifier = 1;
                throw new Exception(
                    "Ocurrió algún error al guardar registros en la tabla Tramite_Documento");
            }

            // D: 5.
            if (!this.enviarCorreo()) {
                this.addMensajeWarn(
                    "No se pudo enviar el correo electrónico. Por favor seleccione una opción de envio diferente al momento de registrar la comunicación de notificación.");
                // Se debe permitir de completar el proceso de radicación, pero
                // se debe informar al usuario que no se pudo enviar el correo.
            }

            this.radicadoBool = true;
            this.banderaModificarAutoDePruebas = false;
            this.addMensajeInfo("Se radicó la comunicación del auto de pruebas satisfactoriamente.");

        } catch (Exception ex) {
            switch (exceptionIdentifier) {
                case (1): {
                    throw SNCWebServiceExceptions.EXCEPCION_0001.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                case (2): {
                    throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                case (3): {
                    throw SNCWebServiceExceptions.EXCEPCION_0003.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                default: {
                    break;
                }
            }
        }

        // D: hay que volver a buscar los TramiteDocumento para refrescar el
        // atributo de registro de los notificantes
        this.buscarTramiteDocumentosDeTramite();

        consultarAutoDePruebasYEstado();

        // Despues de radicar el comportamiento de activación de los botones es:
        this.radicadoBool = true;

        // Reinicio el valor del número de radicado
        this.numeroRadicado = null;
        // Deseleccionar el solicitante
        this.solicitanteSelectBool = false;

        // Una vez radicada la primer comunicación, se consolida
        // el auto de pruebas y este no se podrá modificar.
        if (!this.radicadoPrimeraVezBool) {
            consolidarAutoDePruebas();
        }

        // Validar avanzar proceso
        revisarEstadoAvanzarProceso();
    }

    /**
     * Método para obtener el número de radicado de correspondencia para entidad externa
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     */
    private String obtenerNumeroRadicadoEntidadExterna() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA.
            getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Comunicar auto de interesado a entidad externa"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    /**
     * Método para obtener el número de radicado de correspondencia para oficina interna de IGAC
     *
     * @author javier.aponte
     *
     */
    private String obtenerNumeroRadicadoOficinaInterna() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("IE"); // tipo correspondencia
        parametros.add(String.valueOf(
            ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Comunicar auto de interesado a oficina interna"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(this.entidadOGrupoSeleccionado.getEstructuraOrganizacional().getCodigo()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.entidadOGrupoSeleccionado.getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.entidadOGrupoSeleccionado.getDireccion()); // Direccion destino
        parametros.add(this.entidadOGrupoSeleccionado.getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.entidadOGrupoSeleccionado.getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);
        //ultimo paso: llamar procedimiento recibirRadicado (Servicio de correspondencia para crear tramites)
        this.getTramiteService().recibirRadicado(numeroRadicado, this.usuario.getLogin());

        return numeroRadicado;
    }

    /**
     * Método para obtener el número de radicado de correspondencia para solicitantes y afectados
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     *
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     *
     */
    private String obtenerNumeroRadicadoSolicitantesYAfectados() {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.usuario.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.usuario.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(
            ETipoDocumento.OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Comunicar auto de interesado a solicitantes y afectados"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).getDireccion()); // Direccion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(this.tramite.getSolicitud().getSolicitanteSolicituds().get(0).
            getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    // ----------------------------------------------------- //
    /**
     * Método que busca los documentos del trámite del tipo
     * OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS,
     * MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC y
     * OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA
     *
     * @author javier.aponte
     */
    private void buscarTramiteDocumentosDeTramite() {

        this.tramiteDocumentosDelTramite = new ArrayList<TramiteDocumento>();
        this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados =
            (ArrayList<TramiteDocumento>) this
                .getTramiteService()
                .obtenerTramiteDocumentosDeTramitePorTipoDoc(
                    this.tramite.getId(),
                    ETipoDocumento.OFICIO_SOLICITUD_AUTO_DE_PRUEBAS_A_SOLICITANTES_Y_AFECTADOS.
                        getId());
        if (this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados != null &&
            !this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados.isEmpty()) {
            this.tramiteDocumentosDelTramite.addAll(
                this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados);
        }

        this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac =
            (ArrayList<TramiteDocumento>) this
                .getTramiteService()
                .obtenerTramiteDocumentosDeTramitePorTipoDoc(
                    this.tramite.getId(),
                    ETipoDocumento.MEMORANDO_SOLICITUD_DE_PRUEBAS_A_OFICINA_INTERNA_IGAC.getId());
        if (this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac != null &&
            !this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac.isEmpty()) {
            this.tramiteDocumentosDelTramite.addAll(
                this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac);
        }

        this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna =
            (ArrayList<TramiteDocumento>) this
                .getTramiteService()
                .obtenerTramiteDocumentosDeTramitePorTipoDoc(
                    this.tramite.getId(),
                    ETipoDocumento.OFICIO_SOLICITUD_DE_PRUEBAS_A_ENTIDAD_EXTERNA.getId());
        if (this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna != null &&
            !this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna.isEmpty()) {
            this.tramiteDocumentosDelTramite.addAll(
                this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna);
        }

    }

    /**
     * Método que elimina una entidad o grupo de trabajo asociada al tramitePruebas.
     */
    public void eliminarEntidadOGrupo() {

        try {

            for (TramitePruebaEntidad tpe : this.entidadesOGruposARealizarPruebas) {
                if (tpe.getId()
                    .compareTo(entidadOGrupoSeleccionado.getId()) == 0) {

                    this.getTramiteService().eliminarTramitePruebaEntidad(
                        this.entidadOGrupoSeleccionado);

                    this.entidadesOGruposARealizarPruebas
                        .remove(this.entidadOGrupoSeleccionado);

                    this.addMensajeInfo(
                        "Se eliminó satisfactoriamente la entidad o grupo de trabajo seleccionada.");
                    this.entidadOGrupoSeleccionado = null;
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al eliminar la entidad o grupo de trabajo seleccionada." + e.
                getMessage(), e);
        }

    }

    // ----------------------------------------------------- //
    /**
     * Método que completa los datos del solicitante con los datos del funcionario.
     *
     * @author david.cifuentes
     */
    public Solicitante completarSolicitanteConFuncionario(
        Funcionario funcionario, Solicitante solicitante) {

        solicitante.setRazonSocial(funcionario.getNombres());
        // this.grupoInternoTrabajoSeleccionado.getNombre());
        solicitante.setDireccion(funcionario.getEstructuraOrganizacional().getDireccion());
        solicitante.setTelefonoPrincipal(funcionario.getEstructuraOrganizacional().getTelefono());
        return solicitante;
    }

    /**
     * Método que se ejecuta cuando se selecciona del menú de grupos un grupo interno de trabajo.
     */
    public void refreshPostSelectGrupoTrabajo() {
        this.grupoInternoTrabajoSeleccionado = null;
        for (EstructuraOrganizacional eo : this.gruposTrabajoObj) {
            if (eo.getCodigo().equals(
                this.grupoInternoTrabajoSeleccionadoCodigo)) {
                this.grupoInternoTrabajoSeleccionado = eo;
                break;
            }
        }

        this.funcionarioSeleccionado = "";
        this.loginFuncionarioSeleccionado = "";
        this.loginFuncionarioSeleccionado = "";
        this.eMailFuncionarioSeleccionado = "";

        if (grupoInternoTrabajoSeleccionado != null) {
            Funcionario funcionario = this.grupoInternoTrabajoSeleccionado
                .getFuncionario();
            if (funcionario != null) {
                this.funcionarioSeleccionado = funcionario.getNombres() + " " + funcionario.
                    getApellidos();
                this.eMailFuncionarioSeleccionado = funcionario.getCorreoElectronico();
            } else {
                this.addMensajeError(
                    "Existen inconsistencias con la información de los responsables del grupo interno de trabajo.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta cuando se selecciona una territorial del menú de las territoriales.
     */
    public void refreshPostSelectTerritorial() {
        this.funcionarioSeleccionado = "";
        this.loginFuncionarioSeleccionado = "";
        this.eMailFuncionarioSeleccionado = "";
        this.grupoInternoTrabajoSeleccionado = null;
        this.grupoInternoTrabajoSeleccionadoCodigo = null;
        this.subdireccionOficinaSeleccionada = null;
        this.subdireccionOficinaSeleccionadaCodigo = null;

        this.territorialSeleccionada = findTerritorial(territorialSeleccionadaCodigo);
        if (territorialSeleccionada != null) {
            if (territorialSeleccionada.isDireccionGeneral()) {
                this.subdireccionesOficinasObj = this.getGeneralesService()
                    .getCacheSubdireccionesYOficinas(
                        Constantes.DIRECCION_GENERAL_CODIGO);
                this.setupSubdireccionesOficinas();
            } else {

                this.unidadesOperativasCatastroObj = this
                    .getGeneralesService()
                    .getCacheEstructuraOrganizacionalPorTipoYPadre(
                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                        this.territorialSeleccionadaCodigo);
                this.setupUnidadesOperativasCtastro();

                this.subdireccionesOficinasObj = this.getGeneralesService().
                    getCacheEstructuraOrganizacionalPorTipoYPadre(
                        EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL,
                        this.territorialSeleccionadaCodigo);
                this.setupSubdireccionesOficinas();
            }
        } else {
            this.addMensajeError(
                "Existen inconsistencias con la información de usuarios para ésta territorial.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
        this.setNombreYCorreoEncargado();
    }

    // ----------------------------------------------------- //
    /**
     * Método que hace un set de la UOC del usuario actual si la tiene, y carga el menú de las UOC.
     */
    private void setupUnidadesOperativasCtastro() {

        this.unidadesOperativasCatastro = new ArrayList<SelectItem>();
        this.unidadesOperativasCatastro.add(new SelectItem("", this.DEFAULT_COMBOS));

        for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
            this.unidadesOperativasCatastro.add(new SelectItem(eo.getCodigo(),
                eo.getNombre()));
            LOGGER.info("" + eo.getCodigo() + ", " + eo.getNombre());
        }

        // Predefinir la UOC a la del usuario actual si la tiene:
        if (this.usuario.isUoc()) {
            LOGGER.info("Predefinir UOC igual a la del usuario actual");
            this.unidadOperativaSeleccionadaCodigo = this.usuario
                .getCodigoUOC();
            this.refreshPostSelectUnidadesOperativasCatastro();
        }
        if (this.unidadesOperativasCatastro.isEmpty()) {
            this.addMensajeWarn(
                "Existen inconsistencias con la información registrada para Unidad operativa.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

    }

    // ----------------------------------------------------- //
    /**
     * Método que carga el menú de las oficinas de las subdirecciones.
     */
    private void setupSubdireccionesOficinas() {
        this.subdireccionesOficinas = new ArrayList<SelectItem>();
        this.subdireccionesOficinas
            .add(new SelectItem("", this.DEFAULT_COMBOS));
        for (EstructuraOrganizacional eo : this.subdireccionesOficinasObj) {
            this.subdireccionesOficinas.add(new SelectItem(eo.getCodigo(), eo
                .getNombre()));
        }
        if (this.subdireccionesOficinas.isEmpty()) {
            this.addMensajeWarn(
                "Existen inconsistencias con la información registrada para las oficinas de las subdirecciones.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna una territorial a partir del código que ingresa como parámetro para su
     * búsqueda
     *
     * @param codigoTerritorial
     * @return
     */
    public EstructuraOrganizacional findTerritorial(String codigoTerritorial) {
        EstructuraOrganizacional estructuraOrganizacional, answer = null;
        for (EstructuraOrganizacional eo : this.getGeneralesService()
            .getCacheTerritorialesConDireccionGeneral()) {
            if (eo.getCodigo().equals(codigoTerritorial)) {
                answer = eo;
                break;
            }
        }
        if (answer != null) {
            estructuraOrganizacional = this
                .getGeneralesService()
                .buscarEstructuraOrganizacionalPorCodigo(answer.getCodigo());
            if (estructuraOrganizacional != null) {
                return estructuraOrganizacional;
            } else {
                return answer;
            }
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta cuando se selecciona una entidad, grupo de trabajo o área de
     * conservación que va a realizar una prueba.
     */
    public void refreshPostSelectReceptor() {
        LOGGER.debug("inicio");
        resetCombosSelects();

        this.nombresFuncionarios = new ArrayList<SelectItem>();
        if (this.entidadGrupoInternoPrueba != null) {
            if (this.entidadGrupoInternoPrueba
                .equals(EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
                    .getNombre())) {

                // Se ha seleccionado Territorial área conservación de la lista
                // entidadGrupoInternoPrueba,
                // Predefinir la territorial a la del usuario actual:
                this.territorialSeleccionadaCodigo = this.usuario
                    .getCodigoTerritorial();
                this.refreshPostSelectTerritorial();

                if (isTerritorialAreaConservacion()) {
                    if (this.funcionarioEjecutor != null) {
                        // Asignar a nombre y correo los del usuario actual
                        this.nombresFuncionarios.add(new SelectItem(
                            this.funcionarioEjecutor.getLogin(),
                            this.funcionarioEjecutor.getNombreCompleto()));
                        if (this.funcionarioEjecutor.getNombreCompleto() != null &&
                             !this.funcionarioEjecutor
                                .getNombreCompleto().trim().isEmpty()) {
                            this.funcionarioSeleccionado = this.funcionarioEjecutor.
                                getNombreCompleto();
                            this.loginFuncionarioSeleccionado = this.funcionarioEjecutor.getLogin();
                        } else {
                            this.funcionarioSeleccionado = this.funcionarioEjecutor.getLogin();
                            this.loginFuncionarioSeleccionado = this.funcionarioEjecutor.getLogin();
                        }
                        this.funcionarioSeleccionado = this.funcionarioEjecutor.getLogin();
                        this.loginFuncionarioSeleccionado = this.funcionarioEjecutor.getLogin();

                        this.eMailFuncionarioSeleccionado = this.funcionarioEjecutor
                            .getLogin() + "@" + Constantes.DOMINIO_IGAC;
                    } else {
                        // Asignar a nombre y correo los del usuario actual
                        this.nombresFuncionarios.add(new SelectItem(
                            this.usuario.getLogin(), this.usuario
                            .getNombreCompleto()));
                        this.funcionarioSeleccionado = this.usuario.getLogin();
                        this.loginFuncionarioSeleccionado = this.funcionarioEjecutor.getLogin();
                        this.eMailFuncionarioSeleccionado = this.usuario
                            .getLogin() + "@" + Constantes.DOMINIO_IGAC;
                    }
                } else {
                    // Asignar a nombre y correo los del usuario actual
                    this.nombresFuncionarios.add(new SelectItem(this.usuario
                        .getLogin(), this.usuario.getNombreCompleto()));
                    this.funcionarioSeleccionado = this.usuario.getLogin();
                    this.loginFuncionarioSeleccionado = this.funcionarioEjecutor.getLogin();
                    this.eMailFuncionarioSeleccionado = this.usuario.getLogin() +
                         "@" + Constantes.DOMINIO_IGAC;
                }
            } else if (this.entidadGrupoInternoPrueba
                .equals(EEntidadGrupoTrabajoPruebas.GRUPO_DE_TRABAJO_INTERNO.getNombre())) {
                //
                // Se ha seleccionado Territorial área conservación de la lista
                // entidadGrupoInternoPrueba,
                // Predefinir la territorial a la del usuario actual:
                //
                this.territorialSeleccionadaCodigo = this.usuario
                    .getCodigoTerritorial();
                this.refreshPostSelectTerritorial();
            } else if (this.entidadGrupoInternoPrueba
                .equals(EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA
                    .getNombre())) {
                this.setupNuevoAfectado();
            }
        }

        LOGGER.debug("fin");
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta cuando se selecciona una de las oficinas de las subdirecciones.
     */
    public void refreshPostSelectSubdireccionesOficinas() {
        this.funcionarioSeleccionado = "";
        this.loginFuncionarioSeleccionado = "";
        this.eMailFuncionarioSeleccionado = "";
        this.grupoInternoTrabajoSeleccionado = null;
        this.grupoInternoTrabajoSeleccionadoCodigo = null;

        this.gruposTrabajoObj = this.getGeneralesService()
            .getCacheGruposTrabajo(
                this.subdireccionOficinaSeleccionadaCodigo);
        this.setupGruposTrabajo();

        this.subdireccionOficinaSeleccionada = null;
        for (EstructuraOrganizacional eo : this.subdireccionesOficinasObj) {
            if (eo.getCodigo().equals(
                this.subdireccionOficinaSeleccionadaCodigo)) {
                this.subdireccionOficinaSeleccionada = eo;
                break;
            }
        }
        setNombreYCorreoEncargado();
    }

    // ----------------------------------------------------- //
    /**
     * Método que se ejecuta al seleccionar una UOC.
     */
    public void refreshPostSelectUnidadesOperativasCatastro() {
        this.funcionarioSeleccionado = "";
        this.loginFuncionarioSeleccionado = "";
        this.eMailFuncionarioSeleccionado = "";
        this.grupoInternoTrabajoSeleccionado = null;
        this.grupoInternoTrabajoSeleccionadoCodigo = null;

        this.unidadOperativaSeleccionada = null;
        for (EstructuraOrganizacional eo : this.unidadesOperativasCatastroObj) {
            if (eo.getCodigo().equals(this.unidadOperativaSeleccionadaCodigo)) {
                this.unidadOperativaSeleccionada = eo;
                break;
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que setea el nombre y el correo del funcionario encargado.
     *
     * @author franz.gamba
     */
    public void setNombreYCorreoEncargado() {
        if (this.isTerritorialAreaConservacion()) {
            this.funcionarioEjecutor = null;
            // Buscar el funcionario ejecutor que venía con el trámite
            if (this.tramite.getFuncionarioEjecutor() != null &&
                 !this.tramite.getFuncionarioEjecutor().trim().isEmpty()) {
                this.funcionarioEjecutor = this.getGeneralesService()
                    .getCacheUsuario(tramite.getFuncionarioEjecutor());

            } else {
                if (this.tramite.isViaGubernativa()) {
                    this.tramite.setFuncionarioEjecutor(this.usuario.getLogin());
                    this.getTramiteService().actualizarTramite(this.tramite);
                    this.funcionarioEjecutor = this.usuario;
                } else {

                    List<UsuarioDTO> aux = this.getTramiteService()
                        .buscarFuncionariosPorRolYTerritorial(
                            this.usuario.getDescripcionEstructuraOrganizacional(),
                            ERol.EJECUTOR_TRAMITE);

                    if (aux != null && !aux.isEmpty()) {
                        this.funcionarioEjecutor = aux.get(0);
                        this.addMensajeWarn(
                            "No se encontró un funcionario ejecutor asociado previamente al trámite, " +
                            "por lo que fue asignado un ejecutor de la territorial.");
                    }
                }
            }
            if (this.unidadOperativaSeleccionada == null) {
                if (directoresTerritoriales.size() != 0) {
                    this.nombresFuncionarios = new ArrayList<SelectItem>();
                    this.nombresFuncionarios
                        .add(new SelectItem(directoresTerritoriales.get(0),
                            directoresTerritoriales.get(0)
                                .getNombreCompleto()));
                }
            } else {
                this.directoresTerritoriales = null;
                this.responsableUOC = this.getGeneralesService()
                    .obtenerFuncionarioTerritorialYRol(
                        this.territorialSeleccionadaCodigo,
                        ERol.RESPONSABLE_UNIDAD_OPERATIVA);
                if (responsableUOC.size() != 0) {
                    this.nombresFuncionarios = new ArrayList<SelectItem>();
                    this.nombresFuncionarios
                        .add(new SelectItem(responsableUOC.get(0),
                            responsableUOC.get(0).getNombreCompleto()));
                }
            }
        } else if (this.isGrupoInternoTrabajo()) {
            List<UsuarioDTO> funcionarios = null;
            if (this.subdireccionOficinaSeleccionadaCodigo != null) {
                LOGGER.debug("código de la UOC seleccionada: " +
                     subdireccionOficinaSeleccionadaCodigo);

                funcionarios = this.getGeneralesService()
                    .obtenerFuncionarioTerritorialYRol(
                        subdireccionOficinaSeleccionadaCodigo,
                        ERol.RESPONSABLE_UNIDAD_OPERATIVA);
                if (funcionarios != null && !funcionarios.isEmpty()) {
                    this.responsableUOC = funcionarios;
                } else {

                    funcionarios = this.getGeneralesService()
                        .obtenerFuncionarioTerritorialYRol(
                            this.territorialSeleccionadaCodigo,
                            ERol.DIRECTOR_TERRITORIAL);
                    if (funcionarios != null && !funcionarios.isEmpty()) {
                        this.addMensajeWarn(
                            "No se encontró un responsable para la unidad operativa, por lo que se asigna al director de la territorial");
                    } else {
                        this.addMensajeError(
                            "No se encontró información registrada ni para el responsable de la unidad operativa ni para el director de la territorial.");
                    }
                }
            } else if (this.territorialSeleccionadaCodigo != null) {
                LOGGER.debug("nombre de la territorial seleccionada: " +
                     this.territorialSeleccionadaCodigo);
                funcionarios = this.getGeneralesService()
                    .obtenerFuncionarioTerritorialYRol(
                        this.territorialSeleccionadaCodigo,
                        ERol.DIRECTOR_TERRITORIAL);
            }

            if (null != funcionarios && !funcionarios.isEmpty()) {
                UsuarioDTO dt = funcionarios.get(0);
                this.funcionarioSeleccionado = dt.getNombreCompleto();
                this.loginFuncionarioSeleccionado = dt.getLogin();
                this.eMailFuncionarioSeleccionado = dt.getLogin() + "@" +
                     Constantes.DOMINIO_IGAC;
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que carga el menú de grupos de trabajo
     */
    private void setupGruposTrabajo() {
        this.gruposTrabajo = new ArrayList<SelectItem>();
        this.gruposTrabajo.add(new SelectItem("", this.DEFAULT_COMBOS));
        for (EstructuraOrganizacional eo : this.gruposTrabajoObj) {
            this.gruposTrabajo.add(new SelectItem(eo.getCodigo(), eo
                .getNombre()));
        }
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de buscar un solicitante a partir de los datos de documento.
     *
     * @modified felipe.cadena - Se agrega a autogeneración del digito de verificación
     *
     */
    public void buscarSolicitante() {
        LOGGER.debug("Entrando a buscar solicitante");

        Solicitante sol = this
            .getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
                solicitanteAfectadoSeleccionado
                    .getNumeroIdentificacion(),
                solicitanteAfectadoSeleccionado.getTipoIdentificacion());
        if (sol != null) {
            this.paisSolicitante = sol.getDireccionPais();
            this.updateDepartamentosSolicitante();
            this.departamentoSolicitante = sol.getDireccionDepartamento();
            this.updateMunicipiosSolicitante();
            this.municipioSolicitante = sol.getDireccionMunicipio();
            this.solicitanteAfectadoSeleccionado = sol;

        } else {
            LOGGER.debug("buscarSolicitante Return null");
            String tipoIdentif = this.solicitanteAfectadoSeleccionado
                .getTipoIdentificacion();
            String numDoc = this.solicitanteAfectadoSeleccionado
                .getNumeroIdentificacion();
            String tipoPersona = this.solicitanteAfectadoSeleccionado
                .getTipoPersona();
            setDefaultDataSolicitante();
            this.solicitanteAfectadoSeleccionado
                .setTipoIdentificacion(tipoIdentif);
            this.solicitanteAfectadoSeleccionado
                .setNumeroIdentificacion(numDoc);
            this.solicitanteAfectadoSeleccionado.setTipoPersona(tipoPersona);

            //se calcula al digito de verificación
            if (this.solicitanteAfectadoSeleccionado.getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                String dv = String.valueOf(this.getGeneralesService().calcularDigitoVerificacion(
                    this.solicitanteAfectadoSeleccionado.getNumeroIdentificacion()));
                this.solicitanteAfectadoSeleccionado.setDigitoVerificacion(dv);
            }

        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que quema los datos de fechas departamento tipo de identificación y demás requeridos
     * para iniciar los solicitantes requeridos.
     */
    private void setDefaultDataSolicitante() {
        Solicitante sol = new Solicitante();
        sol.setTipoIdentificacion("CC");
        Pais defaultPais = new Pais();
        defaultPais.setCodigo(Constantes.COLOMBIA);
        this.paisSolicitante = defaultPais;
        this.departamentoSolicitante = new Departamento();
        this.municipioSolicitante = new Municipio();
        sol.setDireccionPais(defaultPais);
        sol.setDireccionDepartamento(new Departamento());
        sol.setDireccionMunicipio(new Municipio());
        sol.setFechaLog(new Date());
        sol.setUsuarioLog(this.usuario.getLogin());
        sol.setRelacion(Constantes.RELACION_DEFECTO);
        this.solicitanteAfectadoSeleccionado = sol;
    }

    // ----------------------------------------------------- //
    /**
     * Método que actualiza los municipios en la adicion de un solicitante.
     */
    public void updateMunicipiosSolicitante() {
        municipiosSolicitante = new ArrayList<SelectItem>();
        munisSolicitante = new ArrayList<Municipio>();
        municipiosSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));
        if (this.solicitanteAfectadoSeleccionado != null &&
             this.departamentoSolicitante != null &&
             this.departamentoSolicitante.getCodigo() != null) {
            munisSolicitante = this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(
                    this.departamentoSolicitante.getCodigo());
            if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
                Collections.sort(munisSolicitante);
            } else {
                Collections.sort(munisSolicitante,
                    Municipio.getComparatorNombre());
            }
            for (Municipio municipio : munisSolicitante) {
                if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
                    municipiosSolicitante.add(new SelectItem(municipio
                        .getCodigo(), municipio.getCodigo3Digitos() + "-" +
                         municipio.getNombre()));
                } else {
                    municipiosSolicitante.add(new SelectItem(municipio
                        .getCodigo(), municipio.getNombre()));
                }
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que actualiza los departamentos en la adicion de un solicitante.
     */
    public void updateDepartamentosSolicitante() {
        departamentosSolicitante = new ArrayList<SelectItem>();
        departamentosSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));

        this.dptosSolicitante = this.getGeneralesService()
            .getCacheDepartamentosPorPais(this.paisSolicitante.getCodigo());
        if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
            Collections.sort(this.dptosSolicitante);
        } else {
            Collections.sort(this.dptosSolicitante,
                Departamento.getComparatorNombre());
        }
        for (Departamento departamento : this.dptosSolicitante) {
            if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
                departamentosSolicitante.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                departamentosSolicitante.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que hace el set del pais seleccionado en la lista de selectItems de paises.
     */
    public void setSelectedPaisSolicitante(String codigo) {
        Pais paisSeleccionado = null;
        if (codigo != null) {
            for (Pais pais : paissSolicitante) {
                if (codigo.equals(pais.getCodigo())) {
                    paisSeleccionado = pais;
                    break;
                }
            }
        }
        if (this.solicitanteAfectadoSeleccionado != null) {
            this.paisSolicitante = paisSeleccionado;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que consulta el pais del solicitante.
     */
    public String getSelectedPaisSolicitante() {
        if (this.solicitanteAfectadoSeleccionado != null &&
             this.paisSolicitante != null) {
            return this.paisSolicitante.getCodigo();
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que cambia el orden de los paises.
     */
    public void onChangeOrdenPaisesSolicitante() {
        this.updatePaisesSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que actualiza los paises del solicitante y reinicia el departamento y el municipìo.
     */
    public void onChangePaisesSolicitante() {
        this.municipioSolicitante = null;
        this.departamentoSolicitante = null;
        this.updateDepartamentosSolicitante();
        this.updateMunicipiosSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que cambia el orden de los paises.
     */
    public void cambiarOrdenPaisesSolicitante() {
        if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
            this.ordenPaisesSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenPaisesSolicitante = EOrden.CODIGO;
        }
        this.updatePaisesSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que arma la lista de selectItem de los paises.
     */
    public void updatePaisesSolicitante() {
        paisesSolicitante = new ArrayList<SelectItem>();
        paisesSolicitante.add(new SelectItem("", this.DEFAULT_COMBOS));

        paissSolicitante = this.getGeneralesService().getCachePaises();
        if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
            Collections.sort(paissSolicitante);
        } else {
            Collections.sort(paissSolicitante, Pais.getComparatorNombre());
        }
        for (Pais pais : paissSolicitante) {
            if (this.ordenPaisesSolicitante.equals(EOrden.CODIGO)) {
                paisesSolicitante.add(new SelectItem(pais.getCodigo(), pais
                    .getCodigo() + "-" + pais.getNombre()));
            } else {
                paisesSolicitante.add(new SelectItem(pais.getCodigo(), pais
                    .getNombre()));
            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que hace el set del departamento seleccionado, de la lista de selectItem de
     * departamentos.
     */
    public void setSelectedDepartamentoSolicitante(String codigo) {
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.dptosSolicitante != null) {
            for (Departamento departamento : dptosSolicitante) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        if (this.solicitanteAfectadoSeleccionado != null) {
            this.departamentoSolicitante = dptoSeleccionado;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que consulta el departamento del solicitante.
     */
    public String getSelectedDepartamentoSolicitante() {
        if (this.solicitanteAfectadoSeleccionado != null &&
             this.departamentoSolicitante != null) {
            return this.departamentoSolicitante.getCodigo();
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que actualiza los municipios del solicitante en base al departamento seleccionado.
     */
    public void onChangeDepartamentosSolicitante() {
        this.municipioSolicitante = null;
        this.updateMunicipiosSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que cambia el orden de los departamentos.
     */
    public void cambiarOrdenDepartamentosSolicitante() {
        if (this.ordenDepartamentosSolicitante.equals(EOrden.CODIGO)) {
            this.ordenDepartamentosSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenDepartamentosSolicitante = EOrden.CODIGO;
        }
        this.updateDepartamentosSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza el set del municipio seleccionado.
     */
    public void setSelectedMunicipioSolicitante(String codigo) {
        Municipio municipioSelected = null;
        if (codigo != null && this.munisSolicitante != null) {
            for (Municipio municipio : this.munisSolicitante) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        } else {
            this.municipioSolicitante = null;
        }
        if (this.solicitanteAfectadoSeleccionado != null) {
            this.municipioSolicitante = municipioSelected;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que consulta el municipio seleccionado.
     */
    public String getSelectedMunicipioSolicitante() {
        if (this.solicitanteAfectadoSeleccionado != null &&
             this.municipioSolicitante != null) {
            return this.municipioSolicitante.getCodigo();
        }
        return null;
    }

    // ----------------------------------------------------- //
    /**
     * Método que actualiza la lista de los municipios.
     */
    public void onChangeOrdenMunicipiosSolicitante() {
        this.updateMunicipiosSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que cambia el orden de los municipios.
     */
    public void cambiarOrdenMunicipiosSolicitante() {
        if (this.ordenMunicipiosSolicitante.equals(EOrden.CODIGO)) {
            this.ordenMunicipiosSolicitante = EOrden.NOMBRE;
        } else {
            this.ordenMunicipiosSolicitante = EOrden.CODIGO;
        }
        this.updateMunicipiosSolicitante();
    }

    // ----------------------------------------------------- //
    /**
     * Método que genera las relaciones del solicitante..
     */
    public void generarRelacionesSolicitante() {
        List<SelectItem> values = generalMB.getSolicitanteSolicitudRelacV();
        List<SelectItem> auxValues = new ArrayList<SelectItem>();

        for (SelectItem si : values) {
            if (!si.getValue().equals(
                ESolicitanteSolicitudRelac.AFECTADO.toString()) &&
                 !si.getValue().equals(
                    ESolicitanteSolicitudRelac.ENTE.toString())) {
                if (!this.solicitanteTramite) {
                    if (this.tramite.getSolicitud().isSolicitudAvisos()) {
                        if (!si.getValue().equals(
                            ESolicitanteSolicitudRelac.REPRESENTANTE_LEGAL
                                .toString()) &&
                             !si.getValue().equals(
                                ESolicitanteSolicitudRelac.APODERADO
                                    .toString())) {
                            auxValues.add(si);
                        }
                    } else {
                        auxValues.add(si);
                    }
                } else {
                    auxValues.add(si);
                }
            }
            values = auxValues;
        }
        this.relacionesSolicitante = values;
    }

    // ----------------------------------------------------- //
    /**
     * Método que realiza la validación lógica de campos para solicitante
     */
    private boolean validarSolicitante() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.solicitanteAfectadoSeleccionado.getNotificacionEmail() != null &&
             this.solicitanteAfectadoSeleccionado.getNotificacionEmail().
                equals(ESiNo.SI.getCodigo()) &&
             (this.solicitanteAfectadoSeleccionado.getCorreoElectronico() == null ||
            this.solicitanteAfectadoSeleccionado
                .getCorreoElectronico().isEmpty())) {

            String mensaje = "La opción notificación por email fue seleccionada pero" +
                 " no se diligenció una cuenta de correo electrónico." +
                 " Por favor complete los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
            return false;
        }

        if (this.solicitanteAfectadoSeleccionado.getTelefonoPrincipal() != null &&
             !this.solicitanteAfectadoSeleccionado.getTelefonoPrincipal().isEmpty() &&
             this.solicitanteAfectadoSeleccionado.getDireccion() != null &&
             !this.solicitanteAfectadoSeleccionado.getDireccion().isEmpty()) {

            if (this.paisSolicitante == null) {

                String mensaje = "Pais: error de validación: valor requerido.";
                this.addMensajeError("solicitanteSolicituddatosSolicitanteForm:paisSolicitante",
                    mensaje);
                context.addCallbackParam("error", "error");
                return false;

            }

            if (this.paisSolicitante.getCodigo().equals(Constantes.COLOMBIA)) {

                if (this.departamentoSolicitante == null) {
                    String mensaje = "Departamento: error de validación: valor requerido.";
                    this.addMensajeError(
                        "solicitanteSolicituddatosSolicitanteForm:departamentoSolicitante", mensaje);
                    context.addCallbackParam("error", "error");
                    return false;
                }

                if (this.municipiosSolicitante == null) {
                    String mensaje = "Municipio: error de validación: valor requerido.";
                    this.addMensajeError(
                        "solicitanteSolicituddatosSolicitanteForm:municipioSolicitante", mensaje);
                    context.addCallbackParam("error", "error");
                    return false;
                }

            }
        }

        //se valida el codigo postal   
        if (this.solicitanteAfectadoSeleccionado.getCodigoPostal() != null &&
             !this.solicitanteAfectadoSeleccionado.getCodigoPostal().isEmpty() &&
             !this.dptosSolicitante.isEmpty()) {

            if (this.solicitanteAfectadoSeleccionado.getDireccion() == null ||
                this.solicitanteAfectadoSeleccionado.getDireccion().isEmpty()) {
                this.addMensajeError("Dirección: Error de validación: se necesita un valor.");
                context.addCallbackParam("error", "error");
                return false;
            }

            if (!UtilidadesWeb.validarSoloDigitos(this.solicitanteAfectadoSeleccionado.
                getCodigoPostal())) {
                this.addMensajeError("El Código postal debe contener solo números");
                context.addCallbackParam("error", "error");
                return false;
            }

            if (this.solicitanteAfectadoSeleccionado.getCodigoPostal().length() < 6) {
                this.addMensajeError("El Código postal debe tener por lo menos 6 digitos");
                context.addCallbackParam("error", "error");
                return false;
            }
            String initCodigoPostal = this.solicitanteAfectadoSeleccionado.getCodigoPostal().
                substring(0, 2);

            if (!this.departamentoSolicitante.getCodigo().equals(initCodigoPostal)) {
                this.addMensajeError(
                    "Código postal no corresponde con el código del departamento seleccionado");
                context.addCallbackParam("error", "error");
                return false;
            }
        }
        return true;
    }

    // ----------------------------------------------------- //
    /**
     * Método que carga los valores de la entidad o grupo seleccionada, para su edición.
     *
     * @author david.cifuentes
     */
    public void setupEntidadOGrupoEdicion() {

        // Verificar que no se haya radicado por primera vez la comunicación
        if (validateRadicadoPrimerVez()) {
            return;
        }
        // Verificar que se haya seleccionado un afectado
        if (this.entidadOGrupoSeleccionado == null) {
            this.addMensajeWarn(
                "Por favor seleccione una entidad externa o grupo interno de trabajo a modificar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        resetCombosSelects();

        // Set de los campos de los combos manejados
        if (entidadOGrupoSeleccionado.getTipoSolicitante() != null) {
            if (entidadOGrupoSeleccionado.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION
                    .getCodigo())) {
                setupTerritorialAreaConservacion();

            } else if (entidadOGrupoSeleccionado.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO
                    .getCodigo())) {
                setupGrupoDeTrabajoInterno();

            } else if (entidadOGrupoSeleccionado.getTipoSolicitante().equals(
                ESolicitanteTipoSolicitant.ENTIDAD_EXTERNA
                    .getCodigo())) {
                setupEntidadExterna();

            }
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que carga los valores de la entidad o grupo seleccionado que es una territorial área
     * de conservación
     *
     * @author david.cifuentes
     */
    public void setupTerritorialAreaConservacion() {
        this.entidadGrupoInternoPrueba =
            EEntidadGrupoTrabajoPruebas.TERRITORIAL_AREA_DE_CONSERVACION
                .getNombre();

        refreshPostSelectReceptor();
    }

    // ----------------------------------------------------- //
    /**
     * Método que carga los valores de la entidad o grupo seleccionado que es un grupo de trabajo
     * interno
     *
     * @author david.cifuentes
     */
    public void setupGrupoDeTrabajoInterno() {

        this.entidadGrupoInternoPrueba = EEntidadGrupoTrabajoPruebas.GRUPO_DE_TRABAJO_INTERNO
            .getNombre();
        refreshPostSelectReceptor();
        territorialSeleccionadaCodigo = this.entidadOGrupoSeleccionado
            .getEstructuraOrganizacional().getCodigo();
        refreshPostSelectTerritorial();
    }

    // ----------------------------------------------------- //
    /**
     * Método que carga los valores de la entidad o grupo seleccionado que es una entidad externa
     *
     * @author david.cifuentes
     */
    public void setupEntidadExterna() {
        this.entidadGrupoInternoPrueba = EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA
            .getNombre();

        refreshPostSelectReceptor();

        this.solicitanteAfectadoSeleccionado = this
            .getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
                this.entidadOGrupoSeleccionado
                    .getNumeroIdentificacion(),
                this.entidadOGrupoSeleccionado.getTipoIdentificacion());
        this.banderaBusquedaAfectadoRealizada = true;

        if (this.entidadOGrupoSeleccionado.getDireccionPais() != null) {
            this.paisSolicitante = this.entidadOGrupoSeleccionado
                .getDireccionPais();
        }
        this.updateDepartamentosSolicitante();
        if (this.entidadOGrupoSeleccionado.getDireccionDepartamento() != null) {
            this.departamentoSolicitante = this.entidadOGrupoSeleccionado
                .getDireccionDepartamento();
        }
        this.updateMunicipiosSolicitante();
        if (this.entidadOGrupoSeleccionado.getDireccionMunicipio() != null) {
            this.municipioSolicitante = this.entidadOGrupoSeleccionado
                .getDireccionMunicipio();
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que carga la comunicación radicada, para un solicitante, afectado, entidad externa o
     * grupo interno de trabajo
     *
     * @author david.cifuentes
     */
    public void cargarComunicacionAutoDePruebasView() {

        TramiteDocumento tramiteDoc;
        String idDocEnGestorDocumental;

        tramiteDoc = this.buscarDocumentoRadicado();
        //this.radicadoBool = true;
        if (tramiteDoc == null) {
            this.addMensajeWarn(
                "No se encontró el documento de comunicación auto de pruebas, por favor verifique que ya se haya generado y radicado");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
        idDocEnGestorDocumental = tramiteDoc.getIdRepositorioDocumentos();

        this.reporteComunicacionAutoDePruebas = this.reportsService.
            consultarReporteDeGestorDocumental(idDocEnGestorDocumental);

        if (this.reporteComunicacionAutoDePruebas != null) {

            reporteGeneradoOK = true;
        } else {
            this.addMensajeWarn("No se encontró la comunicación auto de pruebas radicada.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            reporteGeneradoOK = false;
            return;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que retorna un tramiteDocumento de la lista de tramiteDocumentos del trámite.
     *
     * @return el índice del TramiteDocumento buscado
     */
    /*
     * @modified :: leidy.gonzalez :: 11/03/15 :: #5046 :: Se valida que el reporte a visualizar sea
     * aquel de la entidad y/o grupo que se selecciono.
     */
    private TramiteDocumento buscarDocumentoRadicado() {

        TramiteDocumento tramiteDocumentoARegistrar = null;
        int count = 0;

        if (this.tramite.getTramiteDocumentos() != null) {

            // Se verifica si se está buscando la comunicación de un
            // solicitante, afectado, o de una entidad o grupo de trabajo
            if (this.solicitanteSelectBool) {
                for (TramiteDocumento td
                    : this.tramiteDocumentosSolicitudAutoPruebasASolicitantesYAfectados) {
                    if (this.solicitanteAfectadoSeleccionado != null &&
                         this.solicitanteAfectadoSeleccionado
                            .getNumeroIdentificacion() != null &&
                         td.getIdPersonaNotificacion() != null &&
                         td.getIdPersonaNotificacion().equals(
                            this.solicitanteAfectadoSeleccionado
                                .getNumeroIdentificacion())) {
                        tramiteDocumentoARegistrar = td;
                        count++;
                        break;

                    }
                }
            } else {
                List<TramiteDocumento> auxTramiteDoc = new ArrayList<TramiteDocumento>();
                if (this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna != null &&
                     !this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna.isEmpty()) {

                    for (TramiteDocumento tramiteDocumentoEntidadExterna
                        : this.tramiteDocumentosSolicitudAutoPruebasAEntidadExterna) {

                        if (tramiteDocumentoEntidadExterna.getIdPersonaNotificacion().equals(
                            this.entidadOGrupoSeleccionado.getNumeroIdentificacion()) &&
                            tramiteDocumentoEntidadExterna.getTipoIdPersonaNotificacion().equals(
                                this.entidadOGrupoSeleccionado.getTipoIdentificacion())) {

                            StringBuffer sb = new StringBuffer();
                            sb.append("");
                            if (this.entidadOGrupoSeleccionado.getPrimerNombre() != null) {
                                sb.append(this.entidadOGrupoSeleccionado.getPrimerNombre()).append(
                                    " ");
                            }
                            if (this.entidadOGrupoSeleccionado.getSegundoNombre() != null) {
                                sb.append(this.entidadOGrupoSeleccionado.getSegundoNombre()).append(
                                    " ");
                            }
                            if (this.entidadOGrupoSeleccionado.getPrimerApellido() != null) {
                                sb.append(this.entidadOGrupoSeleccionado.getPrimerApellido()).
                                    append(" ");
                            }
                            if (this.entidadOGrupoSeleccionado.getSegundoApellido() != null) {
                                sb.append(this.entidadOGrupoSeleccionado.getSegundoApellido()).
                                    append(" ");
                            }

                            if (this.entidadOGrupoSeleccionado.getTipoIdentificacion().equals(
                                EPersonaTipoIdentificacion.NIT.getCodigo()) &&
                                tramiteDocumentoEntidadExterna.getPersonaNotificacion().trim().
                                    equalsIgnoreCase(
                                        this.entidadOGrupoSeleccionado.getRazonSocial().trim())) {

                                auxTramiteDoc.add(tramiteDocumentoEntidadExterna);

                            } else if (tramiteDocumentoEntidadExterna.getPersonaNotificacion().
                                equals(sb.toString())) {

                                auxTramiteDoc.add(tramiteDocumentoEntidadExterna);
                            }
                        }
                    }
                }
                if (this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac != null &&
                    !this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac.isEmpty()) {

                    for (TramiteDocumento tramiteDocumentoOficinaInterna
                        : this.tramiteDocumentosSolicitudAutoPruebasAOficinaInternaIgac) {

                        if (tramiteDocumentoOficinaInterna.getIdPersonaNotificacion().equals(
                            this.entidadOGrupoSeleccionado.getNumeroIdentificacion()) &&
                            tramiteDocumentoOficinaInterna.getTipoIdPersonaNotificacion().trim().
                                equals(this.entidadOGrupoSeleccionado.getTipoIdentificacion().trim())) {

                            auxTramiteDoc.add(tramiteDocumentoOficinaInterna);
                        }
                    }
                }
                for (TramiteDocumento td : auxTramiteDoc) {
                    if (this.entidadOGrupoSeleccionado != null &&
                         this.entidadOGrupoSeleccionado
                            .getNumeroIdentificacion() != null &&
                         td.getIdPersonaNotificacion() != null &&
                         td.getIdPersonaNotificacion().equals(
                            this.entidadOGrupoSeleccionado
                                .getNumeroIdentificacion())) {
                        tramiteDocumentoARegistrar = td;
                        count++;
                        break;
                    }
                }
            }
        }

        if (count == 1) {
            return tramiteDocumentoARegistrar;
        } else {
            return null;
        }
    }

    // ----------------------------------------------------- //
    /**
     * Método que revisa el estado de la comunicación auto de pruebas para el solicitante o afectado
     * seleccionado, y habilita o deshabilita el botón de radicar.
     *
     * @author david.cifuentes
     */
    public void revisarEstadoComunicacionAutoDePruebas() {
        if (this.solicitanteSelectBool) {
            // =========================================================== //
            // Se está evaluando a un solicitante o afectado seleccionado. //
            // =========================================================== //
            if (this.solicitanteAfectadoSeleccionado != null) {
                // 1. Verificar si ya se radicó la primer comunicación
                // (Se sabe que se radicó cuando hay un tramite documento con el
                // tipo COMUNICACION_AUTO_DE_PRUEBAS)
                if (!this.radicadoPrimeraVezBool) {
                    // No se ha radicado la primer vez
                    this.radicadoBool = false;
                } else {
                    // Ya se radicó la primer vez.
                    // 2. Revisar si para el solicitante actual ya se hizo la
                    // radicación
                    radicadoBool = false;
                    // Revisar si ya se radicó, consultar documento.
                    if (this.tramite.getTramiteDocumentos() != null &&
                         this.tramite.getTramiteDocumentos().size() > 0 &&
                         !this.tramite.getTramiteDocumentos().isEmpty()) {
                        for (TramiteDocumento td : this.tramite
                            .getTramiteDocumentos()) {
                            if (td.getPersonaNotificacion() != null &&
                                 td.getPersonaNotificacion().equals(
                                    this.solicitanteAfectadoSeleccionado
                                        .getNombreCompleto())) {
                                this.radicadoBool = true;
                                break;
                            }
                        }
                    }
                }

                // Deshabilitar los botones de edición del afectado si es un
                // solicitante
                if (this.solicitanteAfectadoSeleccionado.getRelacion() != null &&
                     this.solicitanteAfectadoSeleccionado.getRelacion()
                        .equals(ESolicitanteSolicitudRelac.AFECTADO
                            .toString())) {
                    this.deshabilitarSolicitante = false;
                } else {
                    this.deshabilitarSolicitante = true;
                }
            } else {
                this.deshabilitarSolicitante = false;
            }
        } else {
            // =========================================================== //
            // Se está evaluando a una entidad externa o grupo interno.    //
            // =========================================================== //
            if (this.entidadOGrupoSeleccionado != null) {
                // 1. Verificar si ya se radicó la primer comunicación
                // (Se sabe que se radicó cuando hay un tramite documento con el
                // tipo COMUNICACION_AUTO_DE_PRUEBAS)
                if (!this.radicadoPrimeraVezBool) {
                    // No se ha radicado la primer vez
                    this.radicadoBool = false;
                } else {
                    // Ya se radicó la primer vez.
                    // 2. Revisar si para el solicitante actual ya se hizo la
                    // radicación
                    radicadoBool = false;
                    // Revisar si ya se radicó, consultar documento.
                    if (this.tramite.getTramiteDocumentos() != null &&
                         this.tramite.getTramiteDocumentos().size() > 0 &&
                         !this.tramite.getTramiteDocumentos().isEmpty()) {
                        for (TramiteDocumento td : this.tramite
                            .getTramiteDocumentos()) {
                            if (td.getPersonaNotificacion() != null &&
                                 td.getPersonaNotificacion().equals(
                                    this.entidadOGrupoSeleccionado
                                        .getNombreCompleto())) {
                                this.radicadoBool = true;
                                break;
                            }
                        }
                    }
                }
            }
            this.deshabilitarEntidades = false;
        }
    }

    // -------------------------------------------- //
    // ------------------ BPM --------------------- //
    // -------------------------------------------- //
    /**
     * Mueve el proceso a la transicion segun las entidades encargadas a las pruebas.
     *
     * @modified :: david.cifuentes :: 02/10/12 :: Se modificó el método de tal manera que realizara
     * un llamado al método de la interfaz IAvanzarProcesoConservacion#avanzarProceso que avanza el
     * proceso según el ambiente en que se encuentre.
     *
     * @return
     */
    public String avanzarProceso() {

        try {
            IAvanzarProcesoConservacion avanzarProcesoConservacionService = this.
                getAvanzarProcesoConservacion();
            avanzarProcesoConservacionService.avanzarProcesoComunicarAutoInteresado(this.usuario,
                this.actividad, this.tramite,
                this.entidadesOGruposARealizarPruebas);
        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            this.addMensajeError("Error al avanzar el proceso.");
            return "";
        }

        UtilidadesWeb.removerManagedBean("ejecucion");
        this.tareasPendientesMB.init();
        return "index";
    }

    //--------------------------------------------------------//
    /**
     * Método ejecutado al dar click en el botón cerrar, redirije al arbol de procesos.
     */
    public String cerrar() {
        tareasPendientesMB.init();
        return "index";
    }

    //--------------------------------------------------------//
    // ---------------------- EVENTOS ----------------------- //
    //--------------------------------------------------------//
    public void solicitanteAfectadoUnselect(UnselectEvent ev) {
        this.solicitanteSelectBool = false;
        this.solicitanteAfectadoSeleccionado = null;
    }

    // ----------------------------------------------------- //
    public void solicitanteAfectadoSelect(SelectEvent ev) {
        this.solicitanteAfectadoSeleccionado = (Solicitante) ev.getObject();
        this.solicitanteSelectBool = true;
        // Evaluar el estado de la comunicación de la notificación de resolución
        // para éste solicitante o afectado.
        revisarEstadoComunicacionAutoDePruebas();
    }

    // ----------------------------------------------------- //	
    public void entidadOGrupoUnselect(UnselectEvent ev) {
        this.solicitanteSelectBool = false;
        this.entidadOGrupoSeleccionado = null;
    }

    // ----------------------------------------------------- //
    public void entidadOGrupoSelect(SelectEvent ev) {
        this.entidadOGrupoSeleccionado = (TramitePruebaEntidad) ev.getObject();
        this.solicitanteSelectBool = false;
        // Evaluar el estado de la comunicación de la notificación de
        //resolución para ésta entidad o grupo interno.
        revisarEstadoComunicacionAutoDePruebas();
    }

    public boolean isActivarGuardarYAdjuntarAdicionarEntidadGrupoTrabajo() {
        boolean answer = false;

        if (this.entidadGrupoInternoPrueba != null && this.entidadGrupoInternoPrueba
            .equals(EEntidadGrupoTrabajoPruebas.ENTIDAD_EXTERNA.getNombre())) {
            answer = !this.banderaBusquedaAfectadoRealizada;
        }

        return answer;
    }
}
