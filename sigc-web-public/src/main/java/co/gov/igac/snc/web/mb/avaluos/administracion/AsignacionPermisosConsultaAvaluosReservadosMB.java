/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPermiso;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean del cu Asignar Permisos para Consultar Avalúos Reservados
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-016
 */
@Component("asignarPermisosConsultaAvaluosReservados")
@Scope("session")
public class AsignacionPermisosConsultaAvaluosReservadosMB extends
    SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AsignacionPermisosConsultaAvaluosReservadosMB.class);

    /**
     * {@link FiltroDatosConsultaAvaluo} filtro usado para la consulta
     */
    private FiltroDatosConsultaAvaluo filtroAvaluos;

    /**
     * Lista con los {@link Avaluo} reservados encontrados en la busqueda
     */
    private List<Avaluo> avaluosReservados;

    private ArrayList<SelectItem> listaDepartamentos;
    private ArrayList<SelectItem> listaMunicipios;

    /**
     * Usuario del sístema
     */
    private UsuarioDTO usuario;

    /**
     * Avaluo seleccionado para la aplicación de permisos
     */
    private Avaluo avaluoSeleccionado;

    /**
     * Lista con los avaluadores disponibles para asignar permisos
     */
    private ArrayList<SelectItem> listaAvaluadores;

    /**
     * Código territorial seleccionada
     */
    private String territorialSeleccionada;

    /**
     * Contiene los avaluadores seleccionados para asignarles permisos
     */
    private ProfesionalAvaluo[] avaluadoresSeleccionados;

    /**
     * Contiene los permisos seleccionados para desasignación
     */
    private AvaluoPermiso[] permisosSeleccionados;

    /**
     * Lista con los profesionales de avaluos asociados a la {@link #territorialSeleccionada}
     */
    private List<ProfesionalAvaluo> listaAvaluadoresTerritorial;

    /**
     * Número de días de permiso para que los {@link #avaluadoresSeleccionados} puedan consultar el
     * {@link #avaluoSeleccionado}
     */
    private Long numeroDiasPermiso;

    // --------------------------------Banderas---------------------------------
    /**
     * Bandera que determina si persona natural o no
     */
    private boolean banderaEsPersonaNatural;

    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaAvaluo getFiltroAvaluos() {
        return filtroAvaluos;
    }

    public List<Avaluo> getAvaluosReservados() {
        return this.avaluosReservados;
    }

    public ArrayList<SelectItem> getListaDepartamentos() {
        return this.listaDepartamentos;
    }

    public ArrayList<SelectItem> getListaMunicipios() {
        return this.listaMunicipios;
    }

    public Avaluo getAvaluoSeleccionado() {
        return this.avaluoSeleccionado;
    }

    public void setAvaluoSeleccionado(Avaluo avaluoSeleccionado) {
        this.avaluoSeleccionado = avaluoSeleccionado;
    }

    public ArrayList<SelectItem> getListaAvaluadores() {
        return this.listaAvaluadores;
    }

    public void setTerritorialSeleccionada(String territorialSeleccionada) {
        this.territorialSeleccionada = territorialSeleccionada;
    }

    public String getTerritorialSeleccionada() {
        return this.territorialSeleccionada;
    }

    public ProfesionalAvaluo[] getAvaluadoresSeleccionados() {
        return this.avaluadoresSeleccionados;
    }

    public void setAvaluadoresSeleccionados(
        ProfesionalAvaluo[] avaluadoresSeleccionados) {
        this.avaluadoresSeleccionados = avaluadoresSeleccionados;
    }

    public AvaluoPermiso[] getPermisosSeleccionados() {
        return this.permisosSeleccionados;
    }

    public void setPermisosSeleccionados(AvaluoPermiso[] permisosSeleccionados) {
        this.permisosSeleccionados = permisosSeleccionados;
    }

    public List<ProfesionalAvaluo> getListaAvaluadoresTerritorial() {
        return this.listaAvaluadoresTerritorial;
    }

    public Long getNumeroDiasPermiso() {
        return this.numeroDiasPermiso;
    }

    public void setNumeroDiasPermiso(Long numeroDiasPermiso) {
        this.numeroDiasPermiso = numeroDiasPermiso;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    public boolean isBanderaEsPersonaNatural() {
        return this.banderaEsPersonaNatural;
    }

    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init de ConsultaAvaluosAprobadosMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.filtroAvaluos = new FiltroDatosConsultaAvaluo();
        this.filtroAvaluos.setReservado(ESiNo.SI.getCodigo());
        this.filtroAvaluos.setAprobado(ESiNo.SI.getCodigo());

        this.cargarListaDepartamentos();
        this.cargarListaMunicipios();
        this.cargarAvaluadores();
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se activa cuando se cambia el tipo de empresa. Activa la bandera de tipo de
     * empresa dependiendo de si es natural o juridica
     *
     * @modified christian.rodriguez
     */
    public void onChangeTipoPersona() {

        if (this.filtroAvaluos.getTipoEmpresa().equals(
            EPersonaTipoPersona.NATURAL.getCodigo())) {
            this.banderaEsPersonaNatural = true;
        } else {
            this.banderaEsPersonaNatural = false;
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lsita de departamentos según la territorial del usuario activo
     *
     * @author christian.rodriguez
     */
    private void cargarListaDepartamentos() {

        this.listaDepartamentos = new ArrayList<SelectItem>();
        this.listaDepartamentos.add(new SelectItem(null, this.DEFAULT_COMBOS));

        List<Departamento> departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.usuario.getCodigoTerritorial());

        for (Departamento departamento : departamentos) {
            this.listaDepartamentos.add(new SelectItem(
                departamento.getCodigo(), departamento.getNombre()));
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener/Método que genera la lista de municipios según el departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void cargarListaMunicipios() {

        this.listaMunicipios = new ArrayList<SelectItem>();
        this.listaMunicipios.add(new SelectItem(null, this.DEFAULT_COMBOS));

        List<Municipio> municipios = (ArrayList<Municipio>) this
            .getGeneralesService().getCacheMunicipiosByDepartamento(
                this.filtroAvaluos.getDepartamento());

        for (Municipio municipio : municipios) {
            this.listaMunicipios.add(new SelectItem(municipio.getCodigo(),
                municipio.getNombre()));
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que realiza la busqueda de los avaluos candelados dependiendo del
     * {@link FiltroDatosConsultaAvaluo} ingreasdo
     *
     * @author christian.rodriguez
     */
    public void buscar() {
        if (this.filtroAvaluos != null) {

            this.avaluosReservados = this.getAvaluosService()
                .buscarAvaluosPorFiltro(this.filtroAvaluos);
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se ejecuta cuando se abre el dialogo para asignar o desasignar los permisos, se
     * encarga de reinicializar las variables del dialogo
     *
     * @author christian.rodriguez
     */
    public void cargarAsignarDesasignarPermisos() {
        if (this.avaluoSeleccionado != null) {
            this.avaluadoresSeleccionados = new ProfesionalAvaluo[]{};
            this.numeroDiasPermiso = 0L;
            this.territorialSeleccionada = null;
            this.listaAvaluadoresTerritorial = new ArrayList<ProfesionalAvaluo>();
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se ejecuta cuando se desasignan los permisos a los avaluadores seleccionados
     *
     * @author christian.rodriguez
     */
    public void desasignarPermisos() {

        boolean errorValidación = false;

        if (this.permisosSeleccionados.length < 1) {
            this.addMensajeError(
                "Debe seleccionar al menos un profesional de avalúos para realizar la desasignación de permisos.");
            errorValidación = true;
        }
        if (this.avaluoSeleccionado == null) {
            this.addMensajeError(
                "Debe seleccionar un avalúo para realizar la asignación de permisos.");
            errorValidación = true;
        }

        if (!errorValidación) {

            for (AvaluoPermiso permiso : this.permisosSeleccionados) {

                boolean borradoSatisfactorio = this.getAvaluosService()
                    .borrarAvaluoPermisoPorId(permiso.getId());

                if (borradoSatisfactorio) {
                    this.addMensajeInfo("Permisos desasignados satisfactoriamente al profesional " +
                         permiso.getProfesionalAvaluos()
                            .getNombreCompleto() +
                         " sobre el avalúo " +
                         this.avaluoSeleccionado.getSecRadicado() + ".");
                }

            }

        }

        if (errorValidación) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

        this.buscar();
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se ejecuta cuando se asignan los permisos a los avaluadores seleccionados,
     * valida que se hayan seleccionado avaluadores, que se haya seleccionado avaluador y que se
     * haya ingresado el número de dias del permiso
     *
     * @author christian.rodriguez
     */
    public void asignarPermisos() {

        boolean errorValidación = false;

        if (this.avaluadoresSeleccionados.length < 1) {
            this.addMensajeError(
                "Debe seleccionar al menos un profesional de avalúos para realizar la asignación de permisos.");
            errorValidación = true;
        }
        if (this.avaluoSeleccionado == null) {
            this.addMensajeError(
                "Debe seleccionar un avalúo para realizar la asignación de permisos.");
            errorValidación = true;
        }
        if (this.numeroDiasPermiso < 1) {
            this.addMensajeError("Debe indicar el número de días de permiso.");
            errorValidación = true;
        }

        if (!errorValidación) {

            for (ProfesionalAvaluo profesional : this.avaluadoresSeleccionados) {

                if (!this.profesionalTieenePermisos(profesional,
                    this.avaluoSeleccionado)) {

                    AvaluoPermiso permiso = new AvaluoPermiso();
                    permiso.setProfesionalAvaluos(profesional);
                    permiso.setAvaluo(this.avaluoSeleccionado);
                    permiso.setDias(this.numeroDiasPermiso);
                    permiso.setFechaDesde(new Date());

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    cal.add(Calendar.DATE, this.numeroDiasPermiso.intValue());
                    cal.getTime();

                    permiso.setFechaHasta(cal.getTime());

                    permiso = this.getAvaluosService()
                        .guardarActualizarAvaluoPermiso(permiso,
                            this.usuario);

                    if (permiso != null) {
                        this.addMensajeInfo(
                            "Permisos asignados satisfactoriamente al profesional " +
                             profesional.getNombreCompleto() + ".");
                    }

                } else {
                    this.addMensajeError("El pofesional " +
                         profesional.getNombreCompleto() +
                         " tiene permisos activos sobre el avalúo.");
                    errorValidación = true;
                }
            }

        }

        if (errorValidación) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

        this.buscar();
    }

    // ----------------------------------------------------------------------
    /**
     * método que valida que el profesional no tenga permisos activos para el avaluo especificado
     *
     * @author christian.rodriguez
     * @param profesional profesional al que se le quiere revisar si tiene permisos
     * @param avaluo avaluo sobre el cual se quieren consultar los permisos
     * @return true si el usuario tiene permisos activos sobre el avaluo, false en otro caso
     */
    private boolean profesionalTieenePermisos(ProfesionalAvaluo profesional,
        Avaluo avaluo) {

        List<AvaluoPermiso> permisos = avaluo.getAvaluoPermisos();
        for (AvaluoPermiso avaluoPermiso : permisos) {

            if (avaluoPermiso.getProfesionalAvaluos().getId() != profesional
                .getId()) {
                continue;
            }

            if (avaluoPermiso.getFechaDesde().before(new Date()) &&
                 avaluoPermiso.getFechaHasta().after(new Date())) {
                return true;
            }
        }
        return false;
    }

    // ----------------------------------------------------------------------
    /**
     * Método que carga todos los {@link ProfesionalAvaluo} dentro de un combobox
     *
     * @author christian.rodriguez
     */
    private void cargarAvaluadores() {
        this.listaAvaluadores = new ArrayList<SelectItem>();

        List<ProfesionalAvaluo> avaluadores = this.getAvaluosService()
            .buscarProfesionalesAvaluosPorFiltro(
                new FiltroDatosConsultaProfesionalAvaluos());

        listaAvaluadores.add(new SelectItem(null, "Seleccionar..."));
        for (ProfesionalAvaluo profesional : avaluadores) {
            listaAvaluadores.add(new SelectItem(profesional.getId(),
                profesional.getNombreCompleto()));
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que carga todos los {@link ProfesionalAvaluo} de la {@link #territorialSeleccionada}
     *
     * @author christian.rodriguez
     */
    public void cargarAvaluadoresTerritorial() {

        this.listaAvaluadoresTerritorial = this.getAvaluosService()
            .buscarAvaluadoresPorTerritorial(this.territorialSeleccionada);
    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

}
