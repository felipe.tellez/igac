/**
 * 7 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.plot.MultiplePiePlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.TableOrder;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ETema;
import co.gov.igac.snc.util.DivisionAdministrativaDTO;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.util.MunicipioManzanaDTO;
import co.gov.igac.snc.util.TotalManzanaDTO;
import co.gov.igac.snc.util.TotalPredialDTO;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed Bean usado en la Consulta Gerencial a nivel de predio
 *
 * @author fredy.wilches
 * @modified
 *
 * @version 1.0
 * @cu CU-NP-CO-190
 */
@Component("consultaGerencialPredial")
@Scope("session")
public class ConsultaGerencialPredialMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = -5172998811646733085L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaGerencialPredialMB.class);

    private List<SelectItem> territorialesItemList;
    private List<SelectItem> uocItemList;
    private List<SelectItem> departamentosItemList;
    private List<SelectItem> municipiosItemList;
    private List<SelectItem> temasItemList;
    private List<SelectItem> zonasList;
    private String temaSeleccionado;

    //private boolean seUsaTerritorial;
    private List<DivisionAdministrativaDTO> divisiones;
    private List<MunicipioManzanaDTO> manzanas;
    private DivisionAdministrativaDTO divisionSeleccionada;

    //private StreamedContent tortas;  
    /**
     * Acceso al arbol
     *
     * @author fredy.wilches
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    private TotalPredialDTO totalUrbano;
    private TotalPredialDTO totalRural;
    private TotalPredialDTO totalCorregimiento;

    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaGerencialPredialMB");
        this.territorialesItemList = new ArrayList<SelectItem>();
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList = new ArrayList<SelectItem>();
        this.temasItemList = new ArrayList<SelectItem>();
        this.zonasList = new ArrayList<SelectItem>();
        this.zonasList.add(new SelectItem("U", "Urbano"));
        this.zonasList.add(new SelectItem("R", "Rural"));
        this.zonasList.add(new SelectItem("C", "Corregimiento"));
        this.temaSeleccionado = ETema.TEMA1.toString();
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        Calendar c = Calendar.getInstance();
        this.datosConsultaPredio.setAno((long) c.get(Calendar.YEAR));

        List<EstructuraOrganizacional> eoList = this.getGeneralesService().getCacheTerritoriales();
        this.setTerritorialesItemList(eoList);

        this.onChangeTerritorial();

        for (ETema t : ETema.values()) {
            this.temasItemList.add(new SelectItem(t.name(), t.getDescripcion()));
        }

        //cargarPrediosDummy();
        this.buscar();
        this.divisionSeleccionada = this.divisiones.get(0);

    }

    /* public void cargarPrediosDummy(){ this.divisiones = new
     * ArrayList<DivisionAdministrativaDTO>();
     *
     * DivisionAdministrativaDTO div = new DivisionAdministrativaDTO("Cundinamarca");
     *
     * TotalPredialDTO dto=new TotalPredialDTO(2012, "Urbano", new BigDecimal(12000), new
     * BigDecimal(10500), new BigDecimal(23233.65), new BigDecimal(2323.65), new BigDecimal(12.65));
     * div.getDatos().add(dto);
     *
     * dto=new TotalPredialDTO(2012, "Rural", new BigDecimal(3000), new BigDecimal(10550), new
     * BigDecimal(23233.65), new BigDecimal(2323.65), new BigDecimal(89.65));
     * div.getDatos().add(dto);
     *
     * dto=new TotalPredialDTO(2012, "Corregimientos", new BigDecimal(6000), new BigDecimal(10500),
     * new BigDecimal(23233.65), new BigDecimal(2323.65), new BigDecimal(23.65));
     * div.getDatos().add(dto);
     *
     * divisiones.add(div);
     *
     * div = new DivisionAdministrativaDTO("Meta");
     *
     * dto=new TotalPredialDTO(2012, "Urbano", new BigDecimal(2000), new BigDecimal(10500), new
     * BigDecimal(23233.65), new BigDecimal(2323.65), new BigDecimal(58.65));
     * div.getDatos().add(dto); dto=new TotalPredialDTO(2012, "Rural", new BigDecimal(500), new
     * BigDecimal(10500), new BigDecimal(23233.65), new BigDecimal(2323.65), new BigDecimal(47.65));
     * div.getDatos().add(dto); dto=new TotalPredialDTO(2012, "Corregimientos", new BigDecimal(200),
     * new BigDecimal(23233), new BigDecimal(23233.65), new BigDecimal(2323.65), new
     * BigDecimal(69.65)); div.getDatos().add(dto);
     *
     * divisiones.add(div);
	} */
    public List<SelectItem> getTerritorialesItemList() {
        return territorialesItemList;
    }

    public List<SelectItem> getUocItemList() {
        return this.uocItemList;
    }

    public void setUocItemList(List<SelectItem> uocItemList) {
        this.uocItemList = uocItemList;
    }

    public List<MunicipioManzanaDTO> getManzanas() {
        return manzanas;
    }

    public void setManzanas(List<MunicipioManzanaDTO> manzanas) {
        this.manzanas = manzanas;
    }

    public TotalPredialDTO getTotalUrbano() {
        return totalUrbano;
    }

    public void setTotalUrbano(TotalPredialDTO totalUrbano) {
        this.totalUrbano = totalUrbano;
    }

    public TotalPredialDTO getTotalRural() {
        return totalRural;
    }

    public void setTotalRural(TotalPredialDTO totalRural) {
        this.totalRural = totalRural;
    }

    public TotalPredialDTO getTotalCorregimiento() {
        return totalCorregimiento;
    }

    public void setTotalCorregimiento(TotalPredialDTO totalCorregimiento) {
        this.totalCorregimiento = totalCorregimiento;
    }

    /* public StreamedContent getTortas() { return tortas; }
     *
     * public void setTortas(StreamedContent tortas) { this.tortas = tortas;
	} */
    public String getTemaSeleccionado() {
        return temaSeleccionado;
    }

    public void setTemaSeleccionado(String temaSeleccionado) {
        this.temaSeleccionado = temaSeleccionado;
    }

    public void setTerritorialesItemList(ArrayList<SelectItem> territorialesItemList) {
        this.territorialesItemList = territorialesItemList;
    }

    public List<SelectItem> getZonasList() {
        return zonasList;
    }

    public void setZonasList(List<SelectItem> zonasList) {
        this.zonasList = zonasList;
    }

    public List<SelectItem> getDepartamentosItemList() {

        if (this.isSeUsaTerritorial()) {
            // Deben estar cargados desde el cambio de territorial y uoc
            return departamentosItemList;
        } else {

            List<Departamento> d = this.getGeneralesService().getCacheDepartamentos();

            if (d != null && !d.isEmpty()) {
                this.setDepartamentosItemList(d);
            }

        }

        return departamentosItemList;
    }

    public void setDepartamentosItemList(ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosConsultaPredio) {
        this.datosConsultaPredio = datosConsultaPredio;
    }

    public boolean isSeUsaTerritorial() {
        return false;
        /* if (this.temaSeleccionado.equals(ETema.TEMA2.toString())){ return true; } return false; */
    }

    public void rowSelected(SelectEvent event) {
        System.out.println(event.getObject());
    }

    public DivisionAdministrativaDTO getDivisionSeleccionada() {
        return divisionSeleccionada;
    }

    public void setDivisionSeleccionada(
        DivisionAdministrativaDTO divisionSeleccionada) {
        this.divisionSeleccionada = divisionSeleccionada;
    }

    public List<SelectItem> getTemasItemList() {
        return temasItemList;
    }

    public void setTemasItemList(List<SelectItem> temasItemList) {
        this.temasItemList = temasItemList;
    }

    public List<DivisionAdministrativaDTO> getDivisiones() {
        return divisiones;
    }

    public void setDivisiones(List<DivisionAdministrativaDTO> divisiones) {
        this.divisiones = divisiones;
    }

    public void setTerritorialesItemList(List<EstructuraOrganizacional> items) {
        LOGGER.debug("entra al que retorna la lista de T");

        for (EstructuraOrganizacional territorial : items) {
            this.territorialesItemList.add(new SelectItem(territorial
                .getCodigo(), territorial.getNombre()));
        }
    }

    public void onChangeTerritorial() {
        // Carga UOC

        /* List<Departamento> d =
         * this.getGeneralesService().getCacheDepartamentosPorTerritorial(this.datosConsultaPredio.getTerritorialId());
         *
         * if (d != null && !d.isEmpty()) { this.setDepartamentosItemList(d);
         * this.datosConsultaPredio.setDepartamentoId(d.get(0).getCodigo());
         * this.onChangeDepartamento();
		} */
        this.uocItemList = new ArrayList<SelectItem>();
        if (this.datosConsultaPredio.getTerritorialId() != null && !this.datosConsultaPredio.
            getTerritorialId().equals("")) {
            List<EstructuraOrganizacional> uoc = this.getGeneralesService().
                getCacheEstructuraOrganizacionalPorTipoYPadre(
                    EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL, this.datosConsultaPredio.
                        getTerritorialId());
            for (EstructuraOrganizacional eo : uoc) {
                uocItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
            if (uocItemList.size() > 0) {
                this.departamentosItemList = new ArrayList<SelectItem>();
                this.datosConsultaPredio.setUoc(uocItemList.get(0).getValue().toString());
                this.onChangeUoc();
            }
        }
    }

    public void onChangeUoc() {
        //Carga departamentos de la UOC

        List<Departamento> d = this.getGeneralesService().getCacheDepartamentosByCodigoUOC(
            this.datosConsultaPredio.getUoc());

        if (d != null && !d.isEmpty()) {
            this.setDepartamentosItemList(d);
            this.datosConsultaPredio.setDepartamentoId(d.get(0).getCodigo());
            this.onChangeDepartamento();
        }
    }

    public void onChangeDepartamento() {

        List<Municipio> listM = this.getGeneralesService().getCacheMunicipiosPorDepartamento(
            this.datosConsultaPredio.getDepartamentoId());
        this.setMunicipiosItemList(listM);
        if (this.datosConsultaPredio.getDepartamentoId() != null) {
            Municipio m = this.getGeneralesService().getCapitalDepartamentoByCodigoDepartamento(
                this.datosConsultaPredio.getDepartamentoId());
            this.datosConsultaPredio.setMunicipioId(m.getCodigo());
        }

    }

    /**
     * @param departamentosItemList the departamentosItemList to set
     */
    public void setDepartamentosItemList(List<Departamento> departamentosList) {
        LOGGER.debug("entra al que retorna la lista de D");

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                departamento.getNombre()));
        }

        //D: para hacer que no quede seleccionado un municipio cuando se cambia de departamento y
        //   no se selecciona un nuevo municipio
        //this.datosConsultaPredio.setMunicipioId(null);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @return the municipiosItemList
     */
    public List<SelectItem> getMunicipiosItemList() {

        /* List<Municipio> municipiosList;
         *
         * if (this.datosConsultaPredio.getDepartamentoId() != null &&
         * this.datosConsultaPredio.getDepartamentoId().compareTo("") != 0) { municipiosList =
         * this.getGeneralesService().getCacheMunicipiosByDepartamento(
         * this.datosConsultaPredio.getDepartamentoId()); } else { municipiosList = new
         * ArrayList<Municipio>(); } this.setMunicipiosItemList(municipiosList);
         */
        return this.municipiosItemList;

    }

    /**
     * @param municipiosItemList the municipiosItemList to set
     */
    public void setMunicipiosItemList(List<Municipio> municipiosList) {

        // D: para evitar que queden los elementos de búsquedas anteriores
        if (!this.municipiosItemList.isEmpty()) {
            this.municipiosItemList.clear();
        }

        for (Municipio municipio : municipiosList) {
            this.municipiosItemList.add(new SelectItem(municipio.getCodigo(),
                municipio.getNombre()));
        }

    }

    public PieChartModel getModeloTortaPredios() {
        if (this.divisionSeleccionada != null) {
            PieChartModel pieModel = new PieChartModel();
            if (this.totalRural != null) {
                pieModel.set("Rural", this.totalRural.getTotalPredios());
            }
            if (this.totalUrbano != null) {
                pieModel.set("Urbano", this.totalUrbano.getTotalPredios());
            }
            if (this.totalCorregimiento != null) {
                pieModel.set("Corregimiento", this.totalCorregimiento.getTotalPredios());
            }
            return pieModel;
        } else {
            return null;
        }
    }

    public PieChartModel getModeloTortaPropietarios() {
        if (this.divisionSeleccionada != null) {
            PieChartModel pieModel = new PieChartModel();
            if (this.totalRural != null) {
                pieModel.set("Rural", this.totalRural.getTotalPropietarios());
            }
            if (this.totalUrbano != null) {
                pieModel.set("Urbano", this.totalUrbano.getTotalPropietarios());
            }
            if (this.totalCorregimiento != null) {
                pieModel.set("Corregimiento", this.totalCorregimiento.getTotalPropietarios());
            }
            return pieModel;
        } else {
            return null;
        }
    }

    public PieChartModel getModeloTortaAreaTerreno() {
        if (this.divisionSeleccionada != null) {
            PieChartModel pieModel = new PieChartModel();
            if (this.totalRural != null) {
                pieModel.set("Rural", this.totalRural.getAreaTerreno().intValue());
            }
            if (this.totalUrbano != null) {
                pieModel.set("Urbano", this.totalUrbano.getAreaTerreno().intValue());
            }
            if (this.totalCorregimiento != null) {
                pieModel.set("Corregimiento", this.totalCorregimiento.getAreaTerreno().intValue());
            }
            return pieModel;
        } else {
            return null;
        }
    }

    public PieChartModel getModeloTortaAreaConstruccion() {
        if (this.divisionSeleccionada != null) {
            PieChartModel pieModel = new PieChartModel();
            if (this.totalRural != null) {
                pieModel.set("Rural", this.totalRural.getAreaConstruccion());
            }
            if (this.totalUrbano != null) {
                pieModel.set("Urbano", this.totalUrbano.getAreaConstruccion());
            }
            if (this.totalCorregimiento != null) {
                pieModel.set("Corregimiento", this.totalCorregimiento.getAreaConstruccion());
            }
            return pieModel;
        } else {
            return null;
        }
    }

    public PieChartModel getModeloTortaAvaluos() {
        if (this.divisionSeleccionada != null) {
            PieChartModel pieModel = new PieChartModel();
            if (this.totalRural != null) {
                pieModel.set("Rural", this.totalRural.getTotalAvaluos());
            }
            if (this.totalUrbano != null) {
                pieModel.set("Urbano", this.totalUrbano.getTotalAvaluos());
            }
            if (this.totalCorregimiento != null) {
                pieModel.set("Corregimiento", this.totalCorregimiento.getTotalAvaluos());
            }
            return pieModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasPredios() {
        if (this.divisionSeleccionada != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();
            ChartSeries datos = new ChartSeries();
            BigDecimal total = new BigDecimal(0);
            if (this.totalRural != null) {
                datos.set("Rural", this.totalRural.getTotalPredios());
                total = total.add(this.totalRural.getTotalPredios());
                datos.setLabel("" + this.totalRural.getAno());
            }
            if (this.totalUrbano != null) {
                datos.set("Urbano", this.totalUrbano.getTotalPredios());
                total = total.add(this.totalUrbano.getTotalPredios());
                datos.setLabel("" + this.totalUrbano.getAno());
            }
            if (this.totalCorregimiento != null) {
                datos.set("Corregimiento", this.totalCorregimiento.getTotalPredios());
                total = total.add(this.totalCorregimiento.getTotalPredios());
                datos.setLabel("" + this.totalCorregimiento.getAno());
            }
            datos.set("Total", total);
            categoryModel.addSeries(datos);
            return categoryModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasPropietarios() {
        if (this.divisionSeleccionada != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();
            ChartSeries datos = new ChartSeries();
            BigDecimal total = new BigDecimal(0);
            if (this.totalRural != null) {
                datos.set("Rural", this.totalRural.getTotalPropietarios());
                total = total.add(this.totalRural.getTotalPropietarios());
                datos.setLabel("" + this.totalRural.getAno());
            }
            if (this.totalUrbano != null) {
                datos.set("Urbano", this.totalUrbano.getTotalPropietarios());
                total = total.add(this.totalUrbano.getTotalPropietarios());
                datos.setLabel("" + this.totalUrbano.getAno());
            }
            if (this.totalCorregimiento != null) {
                datos.set("Corregimiento", this.totalCorregimiento.getTotalPropietarios());
                total = total.add(this.totalCorregimiento.getTotalPropietarios());
                datos.setLabel("" + this.totalCorregimiento.getAno());
            }
            datos.set("Total", total);
            categoryModel.addSeries(datos);
            return categoryModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasAvaluos() {
        if (this.divisionSeleccionada != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();
            ChartSeries datos = new ChartSeries();
            BigDecimal total = new BigDecimal(0);
            if (this.totalRural != null) {
                datos.set("Rural", this.totalRural.getTotalAvaluos());
                total = total.add(this.totalRural.getTotalAvaluos());
                datos.setLabel("" + this.totalRural.getAno());
            }
            if (this.totalUrbano != null) {
                datos.set("Urbano", this.totalUrbano.getTotalAvaluos());
                total = total.add(this.totalUrbano.getTotalAvaluos());
                datos.setLabel("" + this.totalUrbano.getAno());
            }
            if (this.totalCorregimiento != null) {
                datos.set("Corregimiento", this.totalCorregimiento.getTotalAvaluos());
                total = total.add(this.totalCorregimiento.getTotalAvaluos());
                datos.setLabel("" + this.totalCorregimiento.getAno());
            }
            datos.set("Total", total);
            categoryModel.addSeries(datos);
            return categoryModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasAreaTerreno() {
        if (this.divisionSeleccionada != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();
            ChartSeries datos = new ChartSeries();
            BigDecimal total = new BigDecimal(0);
            if (this.totalRural != null) {
                datos.set("Rural", this.totalRural.getAreaTerreno());
                total = total.add(this.totalRural.getAreaTerreno());
                datos.setLabel("" + this.totalRural.getAno());

            }

            if (this.totalUrbano != null) {
                datos.set("Urbano", this.totalUrbano.getAreaTerreno());
                total = total.add(this.totalUrbano.getAreaTerreno());
                datos.setLabel("" + this.totalUrbano.getAno());

            }

            if (this.totalCorregimiento != null) {
                datos.set("Corregimiento", this.totalCorregimiento.getAreaTerreno());
                total = total.add(this.totalCorregimiento.getAreaTerreno());
                datos.setLabel("" + this.totalCorregimiento.getAno());

            }
            datos.set("Total", total);
            categoryModel.addSeries(datos);
            return categoryModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasAreaConstruccion() {
        if (this.divisionSeleccionada != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();
            ChartSeries datos = new ChartSeries();
            BigDecimal total = new BigDecimal(0);
            if (this.totalRural != null) {

                datos.set("Rural", this.totalRural.getAreaConstruccion());
                total = total.add(this.totalRural.getAreaConstruccion());
                datos.setLabel("" + this.totalRural.getAno());
            }

            if (this.totalUrbano != null) {

                datos.set("Urbano", this.totalUrbano.getAreaConstruccion());
                total = total.add(this.totalUrbano.getAreaConstruccion());
                datos.setLabel("" + this.totalUrbano.getAno());
            }

            if (this.totalCorregimiento != null) {

                datos.set("Corregimiento", this.totalCorregimiento.getAreaConstruccion());
                total = total.add(this.totalCorregimiento.getAreaConstruccion());
                datos.setLabel("" + this.totalCorregimiento.getAno());
            }
            datos.set("Total", total);
            categoryModel.addSeries(datos);
            return categoryModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasCondicion() {
        if (this.manzanas != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();

            ChartSeries urbana = new ChartSeries();
            ChartSeries rural = new ChartSeries();
            ChartSeries corregimientos = new ChartSeries();

            BigDecimal total = new BigDecimal(0);

            for (TotalManzanaDTO d : this.manzanas.get(0).getDatos()) {
                if (d.getZona().equals("R")) {
                    rural.setLabel("Rural " + d.getAno());
                    rural.set("No reglamentado en PH", d.getPrediosCondicion0());
                    rural.set("Predios condición 1", d.getPrediosCondicion1());
                    rural.set("No ley 14", d.getPrediosCondicion2());
                    rural.set("Uso público diferente a vías", d.getPrediosCondicion3());
                    rural.set("Vías", d.getPrediosCondicion4());
                    rural.set("Mejoras en terreno ajeno No PH", d.getPrediosCondicion5());
                    rural.set("Mejoras en terreno ajeno PH", d.getPrediosCondicion6());
                    rural.set("Cementerios", d.getPrediosCondicion7());
                    rural.set("Condominios", d.getPrediosCondicion8());
                    rural.set("PH", d.getPrediosCondicion9());
                    categoryModel.addSeries(rural);
                }
                if (d.getZona().equals("U")) {
                    urbana.setLabel("Urbano " + d.getAno());
                    urbana.set("No reglamentado en PH", d.getPrediosCondicion0());
                    urbana.set("Predios condición 1", d.getPrediosCondicion1());
                    urbana.set("No ley 14", d.getPrediosCondicion2());
                    urbana.set("Uso público diferente a vías", d.getPrediosCondicion3());
                    urbana.set("Vías", d.getPrediosCondicion4());
                    urbana.set("Mejoras en terreno ajeno No PH", d.getPrediosCondicion5());
                    urbana.set("Mejoras en terreno ajeno PH", d.getPrediosCondicion6());
                    urbana.set("Cementerios", d.getPrediosCondicion7());
                    urbana.set("Condominios", d.getPrediosCondicion8());
                    urbana.set("PH", d.getPrediosCondicion9());
                    categoryModel.addSeries(urbana);
                }

                if (d.getZona().equals("C")) {
                    corregimientos.setLabel("Corregimiento " + d.getAno());
                    corregimientos.set("No reglamentado en PH", d.getPrediosCondicion0());
                    corregimientos.set("Predios condición 1", d.getPrediosCondicion1());
                    corregimientos.set("No ley 14", d.getPrediosCondicion2());
                    corregimientos.set("Uso público diferente a vías", d.getPrediosCondicion3());
                    corregimientos.set("Vías", d.getPrediosCondicion4());
                    corregimientos.set("Mejoras en terreno ajeno No PH", d.getPrediosCondicion5());
                    corregimientos.set("Mejoras en terreno ajeno PH", d.getPrediosCondicion6());
                    corregimientos.set("Cementerios", d.getPrediosCondicion7());
                    corregimientos.set("Condominios", d.getPrediosCondicion8());
                    corregimientos.set("PH", d.getPrediosCondicion9());
                    categoryModel.addSeries(corregimientos);
                }
            }
            return categoryModel;
        } else {
            return null;
        }
    }

    public CartesianChartModel getModeloBarrasManzanas() {
        if (this.manzanas != null) {
            CartesianChartModel categoryModel = new CartesianChartModel();

            ChartSeries datos = new ChartSeries();

            BigDecimal total = new BigDecimal(0);

            for (TotalManzanaDTO d : this.manzanas.get(0).getDatos()) {
                if (d.getZona().equals("R")) {
                    datos.setLabel("" + d.getAno());
                    datos.set("Rural (Veredas)", d.getCantidadManzanasVeredas());
                    total = total.add(d.getCantidadManzanasVeredas());
                }
                if (d.getZona().equals("U")) {
                    datos.setLabel("" + d.getAno());
                    datos.set("Urbano (Manzanas)", d.getCantidadManzanasVeredas());
                    total = total.add(d.getCantidadManzanasVeredas());
                }

                if (d.getZona().equals("C")) {
                    datos.setLabel("" + d.getAno());
                    datos.set("Corregimientos (Manzanas)", d.getCantidadManzanasVeredas());
                    total = total.add(d.getCantidadManzanasVeredas());
                }
            }
            datos.set("Total", total);
            categoryModel.addSeries(datos);
            return categoryModel;
        } else {
            return null;
        }
    }

    public void adicionarTotal() {

        this.totalCorregimiento = null;
        this.totalRural = null;
        this.totalUrbano = null;

        DivisionAdministrativaDTO totales = new DivisionAdministrativaDTO("TOTAL");
        for (DivisionAdministrativaDTO dto : this.divisiones) {
            for (TotalPredialDTO d : dto.getDatos()) {
                TotalPredialDTO total = totales.getDato(d.getZona());
                boolean existe = true;
                if (total == null) {
                    existe = false;
                    total = new TotalPredialDTO(d.getAno(), d.getZona(), d.getTotalPredios(), d.
                        getTotalPropietarios(), d.getAreaTerreno(), d.getAreaConstruccion(), d.
                        getTotalAvaluos());
                    if (d.getZona().equals("R")) {
                        this.totalRural = total;
                    }
                    if (d.getZona().equals("U")) {
                        this.totalUrbano = total;
                    }
                    if (d.getZona().equals("C")) {
                        this.totalCorregimiento = total;
                    }
                } else {

                    total.setTotalPredios(total.getTotalPredios().add(d.getTotalPredios()));
                    total.setTotalAvaluos(total.getTotalAvaluos().add(d.getTotalAvaluos()));
                    total.setTotalPropietarios(total.getTotalPropietarios().add(d.
                        getTotalPropietarios()));

                    total.setAreaConstruccion(total.getAreaConstruccion().add(d.
                        getAreaConstruccion()));
                    total.setAreaTerreno(total.getAreaTerreno().add(d.getAreaTerreno()));

                }
                if (!existe) {
                    totales.getDatos().add(total);
                }
            }
        }
        this.divisiones.add(totales);
    }

    public String buscar() {

        this.divisiones = null;
        this.manzanas = null;

        if (this.temaSeleccionado.equals(ETema.TEMA1.toString())) {
            this.divisiones = this.getConservacionService().consultaEstadisticaGerencialPredio(
                this.datosConsultaPredio.getAno(), this.datosConsultaPredio.getDepartamentoId(),
                this.datosConsultaPredio.getMunicipioId(), this.datosConsultaPredio.getZona());
            this.adicionarTotal();
        }
        if (this.temaSeleccionado.equals(ETema.TEMA2.toString())) {
            if (this.datosConsultaPredio.getMunicipioId() != null) {
                this.manzanas = this.getConservacionService().consultaEstadisticaGerencialManzana(
                    this.datosConsultaPredio.getAno(), this.datosConsultaPredio.getMunicipioId(),
                    this.datosConsultaPredio.getZona());
            } else {
                this.addMensajeError(
                    "Debe seleccionar un municipio para realizar la consulta de Total de Manzanas");
            }
        }

        /* if (this.divisiones!=null){
         *
         *
         * int alto=this.divisiones.size()*20;
         *
         * CategoryDataset categorydataset = createDataset(); JFreeChart jfreechart =
         * createChart(categorydataset); ChartPanel chartPanel = new ChartPanel(jfreechart, true,
         * true, true, false, true); chartPanel.setPreferredSize(new java.awt.Dimension(800, alto));
         *
         * try{ File f = new File("imagen"+(int)Math.random()+".png");
         *
         * ChartUtilities.saveChartAsPNG(f, jfreechart, 800, alto); tortas = new
         * DefaultStreamedContent(new FileInputStream(f), "image/png"); f.delete();
         *
         * }catch(IOException e){ LOGGER.error(e.getMessage()); }
		} */
        return null;
    }

    public String getTipoDivision() {
        if (this.datosConsultaPredio.getDepartamentoId() == null) {
            return "Departamento";
        } else {
            return "Municipio";
        }
    }

    private CategoryDataset createDataset() {
        DefaultCategoryDataset defaultcategorydataset = new DefaultCategoryDataset();
        for (DivisionAdministrativaDTO dto : this.divisiones) {
            for (TotalPredialDTO d : dto.getDatos()) {
                defaultcategorydataset.addValue(d.getTotalPredios(), d.getZona(), dto.
                    getDivisionAdministrativa());
            }
        }
        return defaultcategorydataset;
    }

    private JFreeChart createChart(CategoryDataset categorydataset) {
        JFreeChart jfreechart = ChartFactory.createMultiplePieChart3D("Total predial",
            categorydataset, TableOrder.BY_COLUMN, true, true, false);

        final MultiplePiePlot plot = (MultiplePiePlot) jfreechart.getPlot();
        plot.setBackgroundPaint(Color.white);
        plot.setOutlineStroke(new BasicStroke(1.0f));
        final JFreeChart subchart = plot.getPieChart();
        final PiePlot p = (PiePlot) subchart.getPlot();
        p.setBackgroundPaint(null);
        p.setOutlineStroke(null);
        p.setLabelGenerator(new PieSectionLabelGenerator() {

            @Override
            public String generateSectionLabel(PieDataset arg0, Comparable arg1) {
                double cien = 0;
                for (int i = 0; i < arg0.getItemCount(); i++) {
                    if (arg0.getValue(i) != null) {
                        cien += arg0.getValue(i).doubleValue();
                    } else {
                        System.out.println(i);
                    }
                }
                double n = arg0.getValue(arg1).doubleValue() * 100 / cien;

                return "" + (Math.round(n * 100) / 100.0) + " %";
            }

            @Override
            public AttributedString generateAttributedSectionLabel(PieDataset arg0,
                Comparable arg1) {
                // TODO Auto-generated method stub
                return null;
            }
        });
        p.setMaximumLabelWidth(0.35);
        p.setLabelFont(new Font("SansSerif", Font.PLAIN, 9));
        p.setInteriorGap(0.30);

        /* jfreechart.setBackgroundPaint(new Color(216, 255, 216)); MultiplePiePlot multiplepieplot =
         * (MultiplePiePlot)jfreechart.getPlot(); PiePlot pieplot =
         * (PiePlot)multiplepieplot.getPieChart().getPlot();
         * pieplot.setMaximumLabelWidth(0.17999999999999999D); pieplot.setLabelFont(new
         * Font("SansSerif", 0, 9));
         *
         *
         * pieplot.setBackgroundPaint(null); pieplot.setOutlineStroke(null);
         * /*pieplot.setLabelGenerator(new StandardPieItemLabelGenerator( "{0} ({2})",
         * NumberFormat.getNumberInstance(), NumberFormat.getPercentInstance() ));
         * pieplot.setMaximumLabelWidth(0.35); pieplot.setLabelFont(new Font("SansSerif",
         * Font.PLAIN, 9)); pieplot.setInteriorGap(0.30);
         *
         *
         *
         * //pieplot.setSectionPaint("Row 1", Color.cyan); */
        return jfreechart;
    }

    public String cerrar() {
        this.tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean("consultaGerencialPredial");
        return "index";
    }
//end of class    

}
