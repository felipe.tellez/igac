/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tramite.radicacion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.MotivoEstadoTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMotivoEstadoTramite;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * Managed bean para el envío de trámites al responsable de conservación
 *
 * @author pedro.garcia
 */
@Component("envioAAsignacion")
@Scope("session")
public class EnvioAAsignacionMB extends SNCManagedBean {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvioAAsignacionMB.class);

    @Autowired
    private GeneralMB generalMBService;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * usuario loggeado
     */
    private UsuarioDTO usuario;

    /**
     * trámites que se muestran en la tabla (que están por ser enviados a 'Asignación'
     */
    private LazyDataModel<Tramite> tramites;

    private HashMap<Long, String> actividadesTramites;

    /**
     * trámite del que se van a ver los detalles
     */
    private Tramite tramiteParaVerDetalles;

    /**
     * Trámites seleccionados en la tabla en la GUI
     */
    private Tramite[] selectedTramites;

    /**
     * Motivo de devolución que se asignará al trámite estado
     */
    private String motivoDevolucion;

    /**
     * Tramites id entregados por el arbol de procesos
     */
    private List<Long> tramiteIds;

    /**
     * Objeto que contiene los datos del reporte de constancia de radicación
     */
    private ReporteDTO reporteConstanciaRadicacion;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    // ------- methods --------------------------------------------------------
    public UsuarioDTO getUsuario() {
        return this.usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public LazyDataModel<Tramite> getTramites() {
        return this.tramites;
    }

    public void setTramites(LazyDataModel<Tramite> currentTramites) {
        this.tramites = currentTramites;
    }

    public Tramite getTramiteParaVerDetalles() {
        return this.tramiteParaVerDetalles;
    }

    public void setTramiteParaVerDetalles(Tramite tramiteParaVerDetalles) {
        this.tramiteParaVerDetalles = tramiteParaVerDetalles;
    }

    public Tramite[] getSelectedTramites() {
        return selectedTramites;
    }

    public void setSelectedTramites(Tramite[] selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    public String getMotivoDevolucion() {
        return motivoDevolucion;
    }

    public void setMotivoDevolucion(String motivoDevolucion) {
        this.motivoDevolucion = motivoDevolucion;
    }

    public HashMap<Long, String> getActividadTramite() {
        return actividadesTramites;
    }

    public void setActividadTramite(HashMap<Long, String> actividadesTramites) {
        this.actividadesTramites = actividadesTramites;
    }

    // --------------------------------------------------------------------------------------------------
    @SuppressWarnings("serial")
    public EnvioAAsignacionMB() {

        this.tramites = new LazyDataModel<Tramite>() {

            @Override
            public List<Tramite> load(int first, int pageSize,
                String sortField, SortOrder sortOrder,
                Map<String, String> filters) {
                List<Tramite> answer = new ArrayList<Tramite>();
                // tramites.setRowCount(tramiteIds.size());
                populateTramitesLazyly(answer, first, pageSize, sortField,
                    sortOrder.toString(), filters);

                actividadesTramites = new HashMap<Long, String>();
                List<Actividad> listaInstanciasActividadesSeleccionadas = tareasPendientesMB
                    .getListaInstanciasActividadesSeleccionadas();
                if (listaInstanciasActividadesSeleccionadas != null) {
                    for (Actividad act : listaInstanciasActividadesSeleccionadas) {
                        for (Tramite tram : answer) {
                            if (tram.getId() == act.getIdObjetoNegocio()) {
                                actividadesTramites.put(tram.getId(),
                                    act.getId());
                            }
                        }
                    }
                }

                return answer;
            }
        };

    }

    @PostConstruct
    public void init() {
        LOGGER.info("|| ***************** Envio a asignación *************** ||");

//		String idTerritorial = MenuMB.getMenu().getUsuarioDto()
//				.getCodigoEstructuraOrganizacional();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        // this.registerCurrentManagedBean(this, "envioAAsignacion");

        loadTramitesFromTree();
        tramites.setRowCount(tramiteIds.size());
    }

//--------------------------------------------------------------------------------------------------
    public String enviarRevisarDigitalizacion() {

        // No se usa, suprimido el boton por decision funcional. El mismo debe
        // hacer control de calidad
        List<MotivoEstadoTramite> motivosAux = generalMBService
            .getMotivoEstadoTramites();
        MotivoEstadoTramite motivoEstadoTramite = new MotivoEstadoTramite();

        for (MotivoEstadoTramite met : motivosAux) {
            if (met.getId() == EMotivoEstadoTramite.POR_ESCANEO.getId()) {
                motivoEstadoTramite = met;
                break;
            }
        }

        for (Tramite t : this.selectedTramites) {
            TramiteEstado te = new TramiteEstado();
            te.setMotivo(motivoEstadoTramite.getMotivo());
            te.setFechaInicio(new Date());
            te.setFechaLog(new Date());
            te.setTramite(t);
            te.setObservaciones(this.motivoDevolucion);
            te.setEstado(ETramiteEstado.DEVUELTO_RADICACION.getCodigo());
            te.setUsuarioLog(usuario.getLogin());
            t.getTramiteEstados().add(te);
        }

        this.getTramiteService().guardarActualizarTramites(Arrays.asList(this.selectedTramites));

        for (Tramite t : this.selectedTramites) {
            String activityId = actividadesTramites.get(t.getId());
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            solicitudCatastral.setRol(ERol.CONTROL_DIGITALIZACION
                .getDistinguishedName());
            solicitudCatastral.setTerritorial(this.usuario
                .getDescripcionEstructuraOrganizacional());
            solicitudCatastral
                .setTransicion(ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO);

            getProcesosService().avanzarActividad(this.usuario, activityId,
                solicitudCatastral);
        }

        this.tareasPendientesMB.init();
        this.init();
        return "index";

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón 'Enviar al responsable'
     *
     * @author alguien que no documenta
     * @return
     */
    /*
     * @modified by juanfelipe.garcia :: 30-10-2013 :: para tramites de vía gobernativa, se
     * actualiza FuncionarioEjecutor y NombreFuncionarioEjecutor @modified pedro.garcia 18-11-2013 +
     * Se atrapa el error en el punto exacto d la ejecución para poder informar con certeza qué
     * falló. + Se optimiza la búsqueda de usuarios de la actividad (solo se hace una vez) + Se
     * refactoriza código repetido
     *
     * @modified fredy.wilches 27-12-2013 + No se puede optimizar sino se tiene en cuenta que se
     * pueden mover tramites de diferentes territoriales + Si es una UOC y se va a enviar al
     * director territorial, se buscan usuarios de ese rol en la territorial
     */
    @SuppressWarnings("unused")
    public String enviarResponsableConservacion() {
        try {
            LOGGER.debug("enviarResponsableConservacion ...");
            LOGGER.error(
                "enviarResponsableConservacion ...*************************************************************");
            LOGGER.error("enviarResponsableConservacion ...");
            String estructuraOrg, mensaje;
            boolean cr;

            if (this.selectedTramites != null && this.selectedTramites.length > 0) {
                cr = generarConstanciasRadicacion();
                if (!cr) {
                    LOGGER.error("Hubo un error generando las constancias de radicación");
                }
            } else {
                this.addMensajeError("Debe seleccionar al menos un trámite.");
                return null;
            }

            Hashtable<String, List<UsuarioDTO>> listaResponsablesConservacion =
                new Hashtable<String, List<UsuarioDTO>>();
            Hashtable<String, List<UsuarioDTO>> listaDirectoresTerritorial =
                new Hashtable<String, List<UsuarioDTO>>();

            //List<UsuarioDTO> listaResponsablesConservacion, listaDirectoresTerritorial;
            //listaResponsablesConservacion = null;
            //listaDirectoresTerritorial = null;
            mensaje = "";
            LOGGER.error("Paso 1 " + this.selectedTramites);
            for (Tramite t : this.selectedTramites) {
                String activityId = this.actividadesTramites.get(t.getId());
                SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
                LOGGER.error("Paso 2 " + t.getSolicitud().isViaGubernativa());
                if (t.getSolicitud().isViaGubernativa()) {

                    //OJO: algunas veces el municipio está en el predio del trámite y otras en el trámite
                    estructuraOrg = this.getGeneralesService().
                        getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                            t.getPredio().getMunicipio().getCodigo());

                    if (estructuraOrg == null) {
                        this.addMensajeError(
                            "No fué posible determinar la estructura organizacional destino para el municipio : " +
                             t.getPredio().getMunicipio().getCodigo());
                    }

                    List<UsuarioDTO> listausuarios = null;

                    if (t.getTipoTramite().equals(ETramiteTipoTramite.RECURSO_DE_REPOSICION.
                        getCodigo()) ||
                         t.getTipoTramite().equals(
                            ETramiteTipoTramite.RECURSO_REPOSICION_EN_SUBSIDIO_APELACION.getCodigo())) {

                        if (estructuraOrg != null) {
                            if (listaResponsablesConservacion.get(estructuraOrg) == null) {
                                listaResponsablesConservacion.put(estructuraOrg, this.
                                    getTramiteService().
                                    buscarFuncionariosPorRolYTerritorial(
                                        estructuraOrg, ERol.RESPONSABLE_CONSERVACION));
                            }
                        }

                        solicitudCatastral.setUsuarios(listaResponsablesConservacion.get(
                            estructuraOrg));
                    } else {

                        if (estructuraOrg != null) {
                            if (listaDirectoresTerritorial.get(estructuraOrg) == null) {
                                listaDirectoresTerritorial.put(estructuraOrg, this.
                                    getTramiteService().
                                    buscarFuncionariosPorRolYTerritorial(
                                        (this.usuario.isUoc() ? this.usuario.
                                        getDescripcionTerritorial() : estructuraOrg),
                                        ERol.DIRECTOR_TERRITORIAL));
                            }
                        }
                        if (listaDirectoresTerritorial != null) {
                            solicitudCatastral.setUsuarios(listaDirectoresTerritorial.get(
                                estructuraOrg));
                        }
                    }

                    //D: se hace esto para vía gubernativa
                    if (listausuarios != null && !listausuarios.isEmpty()) {
                        t.setFuncionarioEjecutor(listausuarios.get(0).getLogin());
                        t.setNombreFuncionarioEjecutor(listausuarios.get(0).getLogin());
                        this.getTramiteService().actualizarTramite(t, this.usuario);
                    }

                    solicitudCatastral
                        .setTransicion(ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO);
                } else {
                    LOGGER.error("Paso 3 " + t.isQuinta());
                    if (!t.isQuinta()) {
                        estructuraOrg = this.getGeneralesService().
                            getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                t.getPredio().getMunicipio().getCodigo());
                    } else {
                        estructuraOrg = this.getGeneralesService()
                            .getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                                t.getMunicipio().getCodigo());
                    }

                    LOGGER.error("Paso 4 " + estructuraOrg);
                    if (estructuraOrg == null) {
                        this.addMensajeError(
                            "No fué posible determinar la estructura organizacional destino para el municipio : " +
                             t.getPredio().getMunicipio().getCodigo());
                    }

                    LOGGER.error("Paso 5 " + t.getDocumentacionCompleta());
                    //N: transición para cuando no se vía gubernativa
                    if (t.getDocumentacionCompleta().equals(ESiNo.NO.toString())) {

                        //felipe.cadena::12-09-2016::#19450::Se envian las actividades a los coordinadores y los responsables
                        if (listaResponsablesConservacion.get(estructuraOrg) == null) {

                            listaResponsablesConservacion.put(estructuraOrg, this.
                                getTramiteService().
                                buscarFuncionariosPorRolYTerritorial(
                                    estructuraOrg, ERol.RESPONSABLE_CONSERVACION));
                        }

                        solicitudCatastral.setUsuarios(listaResponsablesConservacion.get(
                            estructuraOrg));

                        solicitudCatastral
                            .setTransicion(
                                ProcesoDeConservacion.ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE);
                    } else {

                        //felipe.cadena::12-09-2016::#19450::Se envian las actividades a los coordinadores y los responsables
                        if (listaResponsablesConservacion.get(estructuraOrg) == null) {

                            listaResponsablesConservacion.put(estructuraOrg, this.
                                getTramiteService().
                                buscarFuncionariosPorRolYTerritorial(
                                    estructuraOrg, ERol.RESPONSABLE_CONSERVACION));

                        }

                        solicitudCatastral.setUsuarios(listaResponsablesConservacion.get(
                            estructuraOrg));
                        solicitudCatastral.getUsuarios().addAll(this.getTramiteService().
                            buscarFuncionariosPorRolYTerritorial(
                                estructuraOrg, ERol.COORDINADOR));

                        solicitudCatastral
                            .setTransicion(
                                ProcesoDeConservacion.ACT_ASIGNACION_DETERMINAR_PROCEDENCIA_TRAMITE);
                    }

                } // fin de: no es vía gubernativa

                if (solicitudCatastral.getUsuarios() == null) {
                    this.addMensajeError(
                        "El proceso no tiene usuario destino válido, revise la información e intente de nuevo");
                    return null;
                } else {

                    //D: avanzar la actividad del trámite
                    mensaje = "";
                    try {
                        LOGGER.error("Paso 5a " + this.usuario + " " + activityId + " " +
                            solicitudCatastral);
                        SolicitudCatastral scAnswer =
                            this.getProcesosService().avanzarActividad(this.usuario, activityId,
                                solicitudCatastral);
                        LOGGER.error("Paso 5b " + scAnswer);
                        if (scAnswer == null) {
                            mensaje = "Ocurrió un error en el BPM al avanzar la actividad del " +
                                "trámite con id " + t.getId().toString();
                            this.addMensajeError(mensaje);
                        }
                    } catch (Exception e) {
                        mensaje = e.getMessage();
                        e.printStackTrace();
                        LOGGER.error(e.getMessage());
                        break;
                    }

                    /*
                     * Se elimina la generación de la ficha predial digital y de la carta catastral
                     * urbana en la radicación del predio por control de calidad //Se genera ficha
                     * predial digital LOGGER.error("Paso 6 "); try { boolean fpd =
                     * this.getConservacionService(). generarActualizarFichaPredialDigital(t,
                     * this.usuario, true); if(!fpd) { mensaje = "Ocurrió un error al guardar o
                     * actualizar la ficha " + "predial digital del trámite con id " +
                     * t.getId().toString(); LOGGER.error("Error en EnvioAAsignacion " + mensaje);
                     * break; } }catch(ExcepcionSNC e){ mensaje = "El servidor de productos no se
                     * encuentra disponible, por favor comuníquese con el Administrador del
                     * Sistema"; LOGGER.error("Error en EnvioAAsignacion " + mensaje); break; }
                     *
                     * LOGGER.error("Paso 7 "); //Se genera la carta catastral urbana boolean ccu =
                     * this.getConservacionService(). generarActualizarCartaCatastralUrbana(t,
                     * this.usuario,true); if(!ccu) { LOGGER.error("Error en EnvioAAsignacion al
                     * guardar o actualizar la carta " + "catastral urbana trámite con id " +
                     * t.getId().toString()); break; }
                     */
                }
            }

            if (mensaje.isEmpty()) {
                this.tareasPendientesMB.init();
                return cerrar();
            } else {
                this.addMensajeError(mensaje);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            return null;
        }
    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Método para generar el reporte de constancia de radicación
     *
     * @author javier.aponte
     */
    private void generarReporteConstanciaRadicacion(String numeroSolicitud) {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.CONSTANCIA_RADICACION;

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_SOLICITUD", numeroSolicitud);

        LOGGER.debug("Inicia ejecución servicio de reportes...");

        this.reporteConstanciaRadicacion = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.usuario);

        if (this.reporteConstanciaRadicacion == null) {
            this.addMensajeError(Constantes.MENSAJE_ERROR_REPORTE);
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un trámite documento. Luego guarda el documento en la base de
     * datos y en Alfresco. Posteriormente asocia el documento a un trámite documento que también lo
     * guarda en la base de datos.
     *
     * @author javier.aponte
     * @throws Exception
     */
    public void guardarReporteConstanciaRadicacion(Solicitud solicitudTemporal) {

        SolicitudDocumento solicitudDocumento = null;
        Documento documento = null;

        solicitudDocumento = new SolicitudDocumento();
        documento = new Documento();

        solicitudDocumento.setSolicitud(solicitudTemporal);
        solicitudDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        solicitudDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.CONSTANCIA_DE_RADICACION
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (this.reporteConstanciaRadicacion != null) {
                documento.setArchivo(this.reporteConstanciaRadicacion.
                    getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());

            DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
                .crearArgumentoCargarSolicitudAvaluoComercial(solicitudTemporal
                    .getNumero(), solicitudTemporal.getFecha(),
                    ETipoDocumento.CONSTANCIA_DE_RADICACION.getNombre(),
                    this.reporteConstanciaRadicacion
                        .getRutaCargarReporteAAlfresco());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getGeneralesService()
                .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                    datosGestorDocumental, documento);

            if (documento != null && documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                solicitudDocumento.setSoporteDocumento(documento);
                solicitudDocumento.
                    setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                solicitudDocumento.setFecha(new Date(System.currentTimeMillis()));

                solicitudDocumento = this.getTramiteService().guardarActualizarSolicitudDocumento(
                    solicitudDocumento);
            }

        } catch (Exception e) {
            LOGGER.debug("Error al guardar la constancia de radicación en alfresco:" +
                " para la solicitud con número de radicación: " + solicitudTemporal.getNumero() + e.
                getMessage());
        }
    }

//--------------------------------------------------------------------------------------------------
    public void loadTramitesFromTree() {

        tramiteIds = new ArrayList<Long>();
        List<Actividad> listaInstanciasActividadesSeleccionadas = tareasPendientesMB
            .getListaInstanciasActividadesSeleccionadas();
        if (tareasPendientesMB.getActividadesSeleccionadas() != null &&
             listaInstanciasActividadesSeleccionadas != null) {
            for (Actividad act : listaInstanciasActividadesSeleccionadas) {
                tramiteIds.add(act.getIdObjetoNegocio());
            }
        }
    }

    public String cerrar() {
        this.tareasPendientesMB.init();
        return "index";
    }

    // --------------------------------------------------------------------------------------------------
    private void populateTramitesLazyly(List<Tramite> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<Tramite> rows;

        rows = this.getTramiteService().obtenerTramitesPorIds(tramiteIds,
            sortField, sortOrder, filters, first, pageSize);

        for (int i = 0; rows != null && i < rows.size(); i++) {
            answer.add(rows.get(i));
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Revisa si ya se generó la constancia de radicación para las solicitudes de los trámites
     * seleccionados, y si no, la genera. </br>
     * Se genera una constancia por {@link Solicitud} (una para todos los trámites de una solicitud)
     *
     * PRE: los trámites vienen con su {@link Solicitud}
     *
     * @author pedro.garcia
     */
    private boolean generarConstanciasRadicacion() {

        boolean answer = false;
        SolicitudDocumento constanciaDeRadicacion;
        ArrayList<Long> idsSolicitudes;
        Long idSolicitud;

        idsSolicitudes = new ArrayList<Long>();
        for (Tramite tramite : this.selectedTramites) {

            idSolicitud = tramite.getSolicitud().getId();
            if (idsSolicitudes.contains(idSolicitud)) {
                continue;
            }

            idsSolicitudes.add(idSolicitud);
            constanciaDeRadicacion =
                this.getGeneralesService().buscarDocumentoPorSolicitudIdAndTipoDocumentoId(
                    idSolicitud, ETipoDocumento.CONSTANCIA_DE_RADICACION.getId());

            //D: si no existe la constancia de radicación para esta Solicitud, se genera y se guarda
            if (constanciaDeRadicacion == null) {

                //Genera y guarda la constancia de radicación
                this.generarReporteConstanciaRadicacion(tramite.getSolicitud().getNumero());
                //Si se generó satisfactoriamente la constancia de radicación, entonces se guarda en alfresco
                if (this.reporteConstanciaRadicacion != null) {
                    this.guardarReporteConstanciaRadicacion(tramite.getSolicitud());
                    //refs #7733:  En la actividad revisar solicitudes se debe llamar al servicio creado por CORDIS de “Recibir”
                    this.getTramiteService().recibirRadicado(tramite.getSolicitud().getNumero(),
                        this.usuario.getLogin());
                }
            }
        }

        answer = true;

        return answer;
    }

// end of class
}
