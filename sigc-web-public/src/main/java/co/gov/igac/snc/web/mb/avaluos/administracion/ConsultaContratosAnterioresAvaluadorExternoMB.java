package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.vistas.VProfesionalAvaluosContrato;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el caso de uso 055 de avalúos, sirve para mostrar los contratos anteriores de un
 * avaluador externo {@link ProfesionalAvaluo}
 *
 * @author christian.rodriguez
 *
 */
@Component("consultaContratosAnterioresAvaluador")
@Scope("session")
public class ConsultaContratosAnterioresAvaluadorExternoMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaContratosAnterioresAvaluadorExternoMB.class);

    /**
     * Managed bean del caso de uso 083
     */
    private ConsultaPagosParafiscalesAvaluadorExternoMB consultaPagosParafiscalesMB;

    /**
     * ManagedBean del caso de uso 82
     */
    private AdminPagosContratoAvaluadorExternoMB adminPagosContratosMB;

    /**
     * Avaluador sobre el cual se está haciendo la consulta
     */
    private VProfesionalAvaluosContrato avaluador;

    /**
     * Contratos no activos del avaluador
     */
    private List<ProfesionalAvaluosContrato> contratos;

    /**
     * Contrato seleccionado para detalles
     */
    public ProfesionalAvaluosContrato contratoSeleccionado;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public VProfesionalAvaluosContrato getAvaluador() {
        return this.avaluador;
    }

    public List<ProfesionalAvaluosContrato> getContratos() {
        return this.contratos;
    }

    public void setContratoSeleccionado(ProfesionalAvaluosContrato contratoSeleccionado) {
        this.contratoSeleccionado = contratoSeleccionado;
    }

    // ---------------------------------Métodos---------------------------------
    // --------------------------------------------------------------------------
    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado desde el caso de uso 035
     *
     * @author christian.rodriguez
     * @param avaluador {@link ProfesionalAvaluosContrato }
     */
    public void cargarDesdeCU035(VProfesionalAvaluosContrato avaluador) {

        FiltroDatosConsultaProfesionalAvaluos filtro = new FiltroDatosConsultaProfesionalAvaluos();
        filtro.setNumeroIdentificacion(avaluador.getNumeroIdentificacion());
        List<VProfesionalAvaluosContrato> avaluadores = this.getAvaluosService()
            .buscarAvaluadoresPorFiltro(filtro);

        this.contratos = this.getAvaluosService()
            .obtenerProfesionalAvaluosContratoAnterioresPorIdProfesional(
                avaluador.getProfesionalAvaluosId());

        if (avaluadores != null) {
            this.avaluador = avaluadores.get(0);
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que carga el caso de uso 083 de consulta de pagos parafiscales asociados al contrato y
     * avaluador seleccionados
     *
     * @author christian.rodriguez
     */
    public void cargarPagosParafiscales() {

        if (this.contratoSeleccionado != null && this.avaluador != null) {

            this.consultaPagosParafiscalesMB =
                (ConsultaPagosParafiscalesAvaluadorExternoMB) UtilidadesWeb
                    .getManagedBean("consultaPagosParafiscales");
            this.consultaPagosParafiscalesMB.cargarDesdeOtrosMB(this.contratoSeleccionado,
                this.avaluador.getProfesionalAvaluosId());

        }
    }

    // --------------------------------------------------------------------------
    /**
     * Listener que carga el caso de uso 82 para adminsitrar los pagos del contrato
     *
     * @author christian.rodriguez
     */
    public void cargarAdminPagosContrato() {

        if (this.contratoSeleccionado != null && this.avaluador != null) {

            this.adminPagosContratosMB = (AdminPagosContratoAvaluadorExternoMB) UtilidadesWeb
                .getManagedBean("adminPagosContratoAvaluador");
            this.adminPagosContratosMB.cargarDesdeOtrosMB(
                this.contratoSeleccionado,
                this.avaluador.getProfesionalAvaluosId(), true);

        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeWarn("Debe seleccionar un contrato y un avaluador.");
        }
    }

}
