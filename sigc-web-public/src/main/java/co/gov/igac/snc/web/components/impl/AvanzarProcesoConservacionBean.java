package co.gov.igac.snc.web.components.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.fachadas.IGenerales;
import co.gov.igac.snc.fachadas.IProcesos;
import co.gov.igac.snc.fachadas.ITramite;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramitePruebaEntidad;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.persistence.util.ETramiteClasificacion;
import co.gov.igac.snc.web.components.IAvanzarProcesoConservacion;
import co.gov.igac.snc.web.util.ActivityMessageDTO;

/**
 * Clase usada para avanzar el proceso en conservación en ambiente de pruebas.
 *
 * @author david.cifuentes
 *
 */
public class AvanzarProcesoConservacionBean implements
    IAvanzarProcesoConservacion {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AvanzarProcesoConservacionBean.class);

    /**
     * Interfaces locales de servicios
     */
    private ITramite tramiteService = null;
    private IGenerales generalesService = null;
    private IProcesos procesoService = null;

    /**
     * Default Constructor
     */
    public AvanzarProcesoConservacionBean() {
    }

    // ---------------------------------------------------- //
    /**
     * Método que avanza el proceso que se encuentra en la transición ComunicarAutoInteresado.
     *
     * @see IAvanzarProcesoConservacion#avanzarProceso(UsuarioDTO, Actividad, Tramite,
     * List<TramitePruebaEntidad>)
     *
     * @author david.cifuentes
     */
    @Override
    public void avanzarProcesoComunicarAutoInteresado(UsuarioDTO usuario,
        Actividad actividad, Tramite tramite,
        List<TramitePruebaEntidad> entidadesOGruposARealizarPruebas)
        throws ExcepcionSNC {

        try {

            boolean territorialAreaConservacionFlag = false;
            boolean grupoInternoTrabajoFlag = false;
            boolean entidadExternaFlag = false;
            UsuarioDTO usuarioEjecutor;
            List<UsuarioDTO> usuariosEjecutores = new ArrayList<UsuarioDTO>();
            List<UsuarioDTO> usuarioDirectorTerritorial = new ArrayList<UsuarioDTO>();
            List<UsuarioDTO> usuarioResponsableConservacion = new ArrayList<UsuarioDTO>();
            String transicion1 = null;
            String transicion2 = null;

            // VALIDACIÓN PARA DEFINIR LAS TRANSICIONES
            for (TramitePruebaEntidad tpe : entidadesOGruposARealizarPruebas) {
                if (tpe.getTipoSolicitante().equals(
                    ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION.getCodigo())) {
                    territorialAreaConservacionFlag = true;
                }
                if (tpe.getTipoSolicitante().equals(
                    ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO.getCodigo())) {
                    grupoInternoTrabajoFlag = true;
                }
                if (tpe.getTipoSolicitante().equals(ESolicitanteTipoSolicitant.ENTIDAD_EXTERNA.
                    getCodigo())) {
                    entidadExternaFlag = true;
                }
            }

            // ASIGNACIÓN DEL USUARIO
            SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
            // USUARIO EJECUTOR
            if (tramite.getFuncionarioEjecutor() != null) {
                usuarioEjecutor = this.generalesService.getCacheUsuario(tramite.
                    getFuncionarioEjecutor());
            } else {
                usuarioEjecutor = this.generalesService.
                    obtenerResponsableConservacionODirectorTerritorialAsociadoATramite(tramite,
                        usuario);
            }
            usuariosEjecutores.add(usuarioEjecutor);

            // RESPONSABLE DE CONSERVACIÓN O DIRECTOR TERRITORIAL
            if (tramite.getMunicipio() != null &&
                 tramite.getMunicipio().getCodigo() != null) {
                String estructuraOrg = generalesService
                    .getDescripcionEstructuraOrganizacionalPorMunicipioCod(tramite
                        .getMunicipio().getCodigo());

                usuarioDirectorTerritorial = tramiteService
                    .buscarFuncionariosPorRolYTerritorial(estructuraOrg,
                        ERol.DIRECTOR_TERRITORIAL);

                usuarioResponsableConservacion = tramiteService
                    .buscarFuncionariosPorRolYTerritorial(estructuraOrg,
                        ERol.RESPONSABLE_CONSERVACION);
            }

            // TRANSICIONES
            if (territorialAreaConservacionFlag) {
                // Cuando hay territorial de area de conservacion.
                transicion1 = ProcesoDeConservacion.ACT_EJECUCION_COMISIONAR;

                // Se debe reclasificar el trámite
                tramite.setClasificacion(ETramiteClasificacion.TERRENO.toString());
                tramiteService.guardarActualizarTramite(tramite);

                if (grupoInternoTrabajoFlag || entidadExternaFlag) {
                    // Cuando hay un GIT o una entidad externa
                    transicion2 = ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA;
                }
            } else {
                transicion1 = ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA;
            }

            if (transicion2 != null && !transicion2.trim().isEmpty()) {

                // Existen dos posibles transiciones que van a usuarios
                // diferentes
                List<ActividadUsuarios> actividadesUsuarios = new ArrayList<ActividadUsuarios>();

                // TRANSICIÓN 1
                actividadesUsuarios.add(new ActividadUsuarios(transicion1,
                    usuarioResponsableConservacion));

                // TRANSICIÓN 2
                actividadesUsuarios.add(new ActividadUsuarios(transicion2,
                    usuariosEjecutores));

                // Adjuntar actividades usuarios a la solicitud catastral
                solicitudCatastral.setActividadesUsuarios(actividadesUsuarios);

            } else {
                // Únicamente hay una transición
                solicitudCatastral.setTransicion(transicion1);
                if (transicion1
                    .equals(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA)) {
                    solicitudCatastral.setUsuarios(usuariosEjecutores);
                } else {
                    solicitudCatastral
                        .setUsuarios(usuarioResponsableConservacion);
                }
            }

            procesoService.avanzarActividad(usuario, actividad.getId(),
                solicitudCatastral);

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            throw new ExcepcionSNC(
                "AvanzarProcesoConservacionBean#avanzarProceso",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
    }

    // ---------------------------------------------------- //
    /**
     * @see IAvanzarProcesoConservacion#avanzarProcesoDepurarInformacionGeografica(UsuarioDTO,
     * Actividad)
     * @author javier.aponte
     */
    @Override
    public void avanzarProcesoDepurarInformacionGeografica(UsuarioDTO usuarioActual,
        UsuarioDTO usuarioDestino,
        Actividad actividad) throws ExcepcionSNC {

        try {

            ActivityMessageDTO messageDTO;
            String observaciones = "", processTransition = "";
            List<UsuarioDTO> usuariosActividad;
            SolicitudCatastral solicitudCatastral;
            UsuarioDTO usuarioDestinoActividad;

            solicitudCatastral = new SolicitudCatastral();
            usuarioDestinoActividad = usuarioDestino;

            usuariosActividad = new ArrayList<UsuarioDTO>();
            messageDTO = new ActivityMessageDTO();

            messageDTO.setUsuarioActual(usuarioActual);

            messageDTO.setActivityId(actividad.getId());

            messageDTO.setComment(observaciones);

            processTransition = ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL;
            observaciones = actividad.getNombre();

            // D: según alejandro.sanchez la transición que importa es la que se
            // define en la solicitud catastral; sin embargo, por si acaso se
            // hace también en el messageDTO
            messageDTO.setTransition(processTransition);
            solicitudCatastral.setTransicion(processTransition);
            solicitudCatastral.setObservaciones(observaciones);

            usuarioDestinoActividad.copiarEstructuraOrganizacional(usuarioDestino);
            usuarioDestinoActividad.setLogin(usuarioDestino.getLogin());
            usuariosActividad.add(usuarioDestinoActividad);

            solicitudCatastral.setUsuarios(usuariosActividad);
            solicitudCatastral.setRol(ERol.RESPONSABLE_SIG.getRol());
            messageDTO.setSolicitudCatastral(solicitudCatastral);

            procesoService.avanzarActividad(usuarioActual, actividad.getId(),
                solicitudCatastral);

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            throw new ExcepcionSNC(
                "AvanzarProcesoConservacionBean#avanzarProcesoEstableceProcedenciaSolicitud",
                ESeveridadExcepcionSNC.ERROR, e.getMessage(), null);
        }
    }

    // ---------------------------------------------------- //
    /**
     * Métodos utilizados para la inyección manual de los servicios adicionales
     */
    @Override
    public void setTramiteService(ITramite tramiteService) {
        this.tramiteService = tramiteService;
    }

    @Override
    public void setGeneralesService(IGenerales generalesService) {
        this.generalesService = generalesService;
    }

    @Override
    public void setProcesoService(IProcesos procesoService) {
        this.procesoService = procesoService;
    }

}
