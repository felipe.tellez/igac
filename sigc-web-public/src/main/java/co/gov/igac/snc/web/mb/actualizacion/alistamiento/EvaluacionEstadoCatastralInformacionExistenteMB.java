package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.colecciones.Pair;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionDocumentacion;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EActualizacionDocumentacionCategoria;
import co.gov.igac.snc.persistence.util.EActualizacionDocumentacionZona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author
 *
 *
 */
@Component("evaluarEstadoCatastInfoExist")
@Scope("session")
public class EvaluacionEstadoCatastralInformacionExistenteMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    private UsuarioDTO usuario;

    private Actualizacion actualizacion;

    /*
     * Variables que almacenan los datos de referencia del documento a subir cuando el medio
     * seleccionado es analogo
     */
    private String tipoReferenciaText;
    private String referenciaText;

    // Define la zona correspondiente para la documentacion
    private String zonaDocumentacion;

    private boolean alistarInformacion;
    private boolean alistarDocumentacionGeneral;
    private boolean alistarDocumentacionDeReconocimiento;

    /*
     * Variables para los saldos de conservacion
     */
    private List<Actividad> saldosActividades;
    private List<Tramite> saldosTramites;
    private CartesianChartModel graficaBarras;
    private Integer valorMaxbarras;
    private PieChartModel graficaTorta;
    private List<Pair<String, Integer>> tablaTorta;

    /*
     * Variables para georeferenciacion y topografia
     */
    private List<ActualizacionDocumentacion> georrefYTopoDocs;
    private ActualizacionDocumentacion selectedDocumentacion;

    /*
     * Variables para Documentacion general
     */
    private List<ActualizacionDocumentacion> documentacionGeneral;

    /*
     * Lista para reconocimiento predial
     */
    private List<ActualizacionDocumentacion> reconocimientoPredial;

    // -------------------------------------------------------------------------------------------------------
    /**
     * Métodos de acceso a datos
     */
    public String getTipoReferenciaText() {
        return tipoReferenciaText;
    }

    public void setTipoReferenciaText(String tipoReferenciaText) {
        this.tipoReferenciaText = tipoReferenciaText;
    }

    public String getReferenciaText() {
        return referenciaText;
    }

    public void setReferenciaText(String referenciaText) {
        this.referenciaText = referenciaText;
    }

    public boolean isAlistarInformacion() {
        return alistarInformacion;
    }

    public void setAlistarInformacion(boolean alistarInformacion) {
        this.alistarInformacion = alistarInformacion;
    }

    public boolean isAlistarDocumentacionGeneral() {
        return alistarDocumentacionGeneral;
    }

    public void setAlistarDocumentacionGeneral(
        boolean alistarDocumentacionGeneral) {
        this.alistarDocumentacionGeneral = alistarDocumentacionGeneral;
    }

    public boolean isAlistarDocumentacionDeReconocimiento() {
        return alistarDocumentacionDeReconocimiento;
    }

    public void setAlistarDocumentacionDeReconocimiento(
        boolean alistarDocumentacionDeReconocimiento) {
        this.alistarDocumentacionDeReconocimiento = alistarDocumentacionDeReconocimiento;
    }

    public Actividad getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Actividad currentActivity) {
        this.currentActivity = currentActivity;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<ActualizacionDocumentacion> getGeorrefYTopoDocs() {
        return georrefYTopoDocs;
    }

    public void setGeorrefYTopoDocs(
        List<ActualizacionDocumentacion> georrefYTopoDocs) {
        this.georrefYTopoDocs = georrefYTopoDocs;
    }

    public String getZonaDocumentacion() {
        return zonaDocumentacion;
    }

    public void setZonaDocumentacion(String zonaDocumentacion) {
        this.zonaDocumentacion = zonaDocumentacion;
    }

    public ActualizacionDocumentacion getSelectedDocumentacion() {
        return selectedDocumentacion;
    }

    public void setSelectedDocumentacion(
        ActualizacionDocumentacion selectedDocumentacion) {
        this.selectedDocumentacion = selectedDocumentacion;
    }

    public List<ActualizacionDocumentacion> getDocumentacionGeneral() {
        return documentacionGeneral;
    }

    public void setDocumentacionGeneral(
        List<ActualizacionDocumentacion> documentacionGeneral) {
        this.documentacionGeneral = documentacionGeneral;
    }

    public List<ActualizacionDocumentacion> getReconocimientoPredial() {
        return reconocimientoPredial;
    }

    public void setReconocimientoPredial(
        List<ActualizacionDocumentacion> reconocimientoPredial) {
        this.reconocimientoPredial = reconocimientoPredial;
    }

    public List<Actividad> getSaldosActividades() {
        return saldosActividades;
    }

    public void setSaldosActividades(List<Actividad> saldosActividades) {
        this.saldosActividades = saldosActividades;
    }

    public List<Tramite> getSaldosTramites() {
        return saldosTramites;
    }

    public void setSaldosTramites(List<Tramite> saldosTramites) {
        this.saldosTramites = saldosTramites;
    }

    public CartesianChartModel getGraficaBarras() {
        return graficaBarras;
    }

    public Integer getValorMaxbarras() {
        return valorMaxbarras;
    }

    public void setValorMaxbarras(Integer valorMaxbarras) {
        this.valorMaxbarras = valorMaxbarras;
    }

    public PieChartModel getGraficaTorta() {
        return graficaTorta;
    }

    public void setGraficaTorta(PieChartModel graficaTorta) {
        this.graficaTorta = graficaTorta;
    }

    public List<Pair<String, Integer>> getTablaTorta() {
        return tablaTorta;
    }

    public void setTablaTorta(List<Pair<String, Integer>> tablaTorta) {
        this.tablaTorta = tablaTorta;
    }

    // -------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.currentActivity =
            this.tareasPendientesMB.getInstanciaSeleccionada();

//		Long idActualizacion =
//				this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();
        Long idActualizacion = 580L;
        this.actualizacion = this.getActualizacionService()
            .recuperarActualizacionPorId(idActualizacion);
        this.determinarActividadAplicacion();
        this.determinarZonaDocumentacion();
        this.inicializarSaldosDeConservacion();
        this.inicializaGeorreferenciacionYTopografia();
        this.inicializaDocumentacionGeneral();
        this.inicializaReconocimientoPredial();
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Método que determina desde que actividad se llama a la pantalla
     */
    public void determinarActividadAplicacion() {
//		if(this.currentActivity.getNombre().equals("AlistarInformacionParaProcesosCatastrales")){
//			this.alistarInformacion = true;
//			this.alistarDocumentacionGeneral = false;
//			this.alistarDocumentacionDeReconocimiento = false;
//		}else if(this.currentActivity.getNombre().equals("AlistarInformacionGeneral")){
        this.alistarInformacion = true;
        this.alistarDocumentacionGeneral = true;
        this.alistarDocumentacionDeReconocimiento = true;
//		}
    }
    // -------------------------------------------------------------------------------------------------------

    /**
     * Metodo que inicializa la informacion par mostrar los saldos que vienen de conservacion
     */
    public void inicializarSaldosDeConservacion() {

//		this.saldosActividades = this.getProcesosService()
//				.consultarListaActividades(this.getGeneralesService().
//						getDescripcionEstructuraOrganizacionalPorMunicipioCod(
//								this.actualizacion.getMunicipio().getCodigo()),
//						ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES);
        this.saldosActividades = this.getProcesosService()
            .consultarListaActividades(this.getGeneralesService().
                getDescripcionEstructuraOrganizacionalPorMunicipioCod(
                    "08001"),
                ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES);
        this.graficaBarras = new CartesianChartModel();

        ChartSeries sectores = new ChartSeries();

        if (this.saldosActividades != null && this.saldosActividades.size() > 0) {

            List<Long> idObjetosNegocio = new LinkedList<Long>();
            for (Actividad a : this.saldosActividades) {
                idObjetosNegocio.add(a.getIdObjetoNegocio());
            }
            this.saldosTramites = this.getTramiteService()
                .obtenerSaldosDeConservacion(idObjetosNegocio);
        } else {
            this.addMensajeError("No existen saldos de conservación");
            this.saldosTramites = new LinkedList<Tramite>();
        }

        /** Se ingresan los valores de la tabla */
        List<Integer> valorSectores = new LinkedList<Integer>();
        List<Integer> conteoSectores = new LinkedList<Integer>();

        for (Tramite tram : this.saldosTramites) {
            Integer s = Integer.parseInt(tram.getPredio().getNumeroPredial()
                .substring(7, 9));
            if (!valorSectores.contains(s)) {
                valorSectores.add(s);
                conteoSectores.add(1);
            } else {
                int indice = valorSectores.indexOf(s);
                Integer conteo = conteoSectores.get(indice);
                conteo++;
                conteoSectores.set(indice, conteo);
            }
        }
        sectores.setLabel("Sectores");
        for (int i = 0; i < valorSectores.size(); i++) {
            sectores.set("" + valorSectores.get(i), conteoSectores.get(i));
        }
        this.valorMaxbarras = this.saldosTramites.size();
        this.graficaBarras.addSeries(sectores);
        /**  */

        this.generarPieChart();

    }

    // -------------------------------------------------------------------------------------------------------
    /*
     * Metodo que crea la informacion necesaria para el diagrama de torta
     */
    public void generarPieChart() {

        this.graficaTorta = new PieChartModel();
        this.tablaTorta = new LinkedList<Pair<String, Integer>>();

        List<String> tipo = new LinkedList<String>();
        List<Integer> conteo = new LinkedList<Integer>();

        for (Tramite t : this.saldosTramites) {

            if (!tipo.contains(t.getTipoTramiteCadenaCompleto())) {
                tipo.add(t.getTipoTramiteCadenaCompleto());
                conteo.add(1);
            } else {
                int indice = tipo.indexOf(t.getTipoTramiteCadenaCompleto());
                Integer count = conteo.get(indice);
                count++;
                conteo.set(indice, count);
            }

        }

        for (int i = 0; i < tipo.size(); i++) {
            this.tablaTorta.add(
                new Pair<String, Integer>(tipo.get(i), conteo.get(i)));
            this.graficaTorta.set(tipo.get(i), conteo.get(i));
        }

    }
    // -------------------------------------------------------------------------------------------------------

    /*
     * Evalua la zona a la cual correspondera la documentacion
     */
    public void determinarZonaDocumentacion() {

        boolean urbano = false;
        boolean rural = false;

        for (Convenio conv : this.actualizacion.getConvenios()) {
            if (conv.getZona().equals(
                EActualizacionDocumentacionZona.RURAL.getDescripcion())) {
                rural = true;
            }
            if (conv.getZona().equals(
                EActualizacionDocumentacionZona.URBANO.getDescripcion())) {
                urbano = true;
            }
        }

        if (urbano && !rural) {
            this.zonaDocumentacion = EActualizacionDocumentacionZona.URBANO
                .getDescripcion();
        }
        if (!urbano && rural) {
            this.zonaDocumentacion = EActualizacionDocumentacionZona.RURAL
                .getDescripcion();
        }
        if (urbano && rural) {
            this.zonaDocumentacion = EActualizacionDocumentacionZona.URBANO_RURAL
                .getDescripcion();
        }
    }

    // -------------------------------------------------------------------------------------------------------
    public void inicializaDocumentacionGeneral() {

        this.documentacionGeneral = new LinkedList<ActualizacionDocumentacion>();

        ActualizacionDocumentacion copiaCartoPOT = new ActualizacionDocumentacion();
        copiaCartoPOT = this.inicializarDocumentacion(copiaCartoPOT,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.COPIA_CARTOGRAFIA_POT.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion copiaAcuerdpPOT = new ActualizacionDocumentacion();
        copiaAcuerdpPOT = this.inicializarDocumentacion(copiaAcuerdpPOT,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.COPIA_ACUERDO_POT.getId(), ESiNo.SI.getCodigo());

        ActualizacionDocumentacion copiaPlanesParciales = new ActualizacionDocumentacion();
        copiaPlanesParciales = this.inicializarDocumentacion(
            copiaPlanesParciales,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.COPIA_PLANES_PARCIALES.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion acuerdoNomenclatura = new ActualizacionDocumentacion();
        acuerdoNomenclatura = this.inicializarDocumentacion(
            acuerdoNomenclatura,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.ACUERDO_DE_NOMENCLATURA_VIAL.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion planoConjuntoCabCor = new ActualizacionDocumentacion();
        planoConjuntoCabCor = this.inicializarDocumentacion(
            planoConjuntoCabCor,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.PLANO_CONJUNTO_CABECERAS_Y_CORREGIMIENTOS
                .getId(), ESiNo.SI.getCodigo());

        ActualizacionDocumentacion acuerdoBarrios = new ActualizacionDocumentacion();
        acuerdoBarrios = this
            .inicializarDocumentacion(acuerdoBarrios,
                EActualizacionDocumentacionCategoria.GENERAL
                    .getDescripcion(),
                ETipoDocumento.ACUERDO_DE_BARRIOS.getId(), ESiNo.NO
                .getCodigo());

        ActualizacionDocumentacion acuerdoPerimetros = new ActualizacionDocumentacion();
        acuerdoPerimetros = this.inicializarDocumentacion(acuerdoPerimetros,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.ACUERDO_DE_PERIMETROS.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion acuerdoVeredas = new ActualizacionDocumentacion();
        acuerdoVeredas = this
            .inicializarDocumentacion(acuerdoVeredas,
                EActualizacionDocumentacionCategoria.GENERAL
                    .getDescripcion(),
                ETipoDocumento.ACUERDO_DE_VEREDAS.getId(), ESiNo.NO
                .getCodigo());

        ActualizacionDocumentacion comunas = new ActualizacionDocumentacion();
        comunas = this.inicializarDocumentacion(comunas,
            EActualizacionDocumentacionCategoria.GENERAL.getDescripcion(),
            ETipoDocumento.COMUNAS.getId(), ESiNo.NO.getCodigo());

        this.documentacionGeneral.add(copiaCartoPOT);
        this.documentacionGeneral.add(copiaAcuerdpPOT);
        this.documentacionGeneral.add(copiaPlanesParciales);
        this.documentacionGeneral.add(acuerdoNomenclatura);
        this.documentacionGeneral.add(planoConjuntoCabCor);
        this.documentacionGeneral.add(acuerdoBarrios);
        this.documentacionGeneral.add(acuerdoPerimetros);
        this.documentacionGeneral.add(acuerdoVeredas);
        this.documentacionGeneral.add(comunas);

    }

    // -------------------------------------------------------------------------------------------------------
    public void inicializaReconocimientoPredial() {

        this.reconocimientoPredial = new LinkedList<ActualizacionDocumentacion>();

        ActualizacionDocumentacion informeEvolucion = new ActualizacionDocumentacion();
        informeEvolucion = this.inicializarDocumentacion(informeEvolucion,
            EActualizacionDocumentacionCategoria.RECONOCIMIENTO_PREDIAL
                .getDescripcion(),
            ETipoDocumento.INFORME_EVOLUCION_MANZANAS_CATASTRALES.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion cartasCatastAnalogas = new ActualizacionDocumentacion();
        cartasCatastAnalogas = this.inicializarDocumentacion(
            cartasCatastAnalogas,
            EActualizacionDocumentacionCategoria.RECONOCIMIENTO_PREDIAL
                .getDescripcion(),
            ETipoDocumento.CARTAS_CATASTRALES_ANALOGAS.getId(), ESiNo.SI
            .getCodigo());

        ActualizacionDocumentacion stickersProceso = new ActualizacionDocumentacion();
        stickersProceso = this.inicializarDocumentacion(stickersProceso,
            EActualizacionDocumentacionCategoria.RECONOCIMIENTO_PREDIAL
                .getDescripcion(), ETipoDocumento.STICKERS_DEL_PROCESO
                .getId(), ESiNo.SI.getCodigo());

        ActualizacionDocumentacion escriturasFoliosBD = new ActualizacionDocumentacion();
        escriturasFoliosBD = this.inicializarDocumentacion(escriturasFoliosBD,
            EActualizacionDocumentacionCategoria.RECONOCIMIENTO_PREDIAL
                .getDescripcion(),
            ETipoDocumento.ESCRITURAS_FOLIOS_BASES_DE_DATOS.getId(),
            ESiNo.SI.getCodigo());

        this.reconocimientoPredial.add(informeEvolucion);
        this.reconocimientoPredial.add(cartasCatastAnalogas);
        this.reconocimientoPredial.add(stickersProceso);
        this.reconocimientoPredial.add(escriturasFoliosBD);
    }

    // -------------------------------------------------------------------------------------------------------
    public void inicializaGeorreferenciacionYTopografia() {

        this.georrefYTopoDocs = new LinkedList<ActualizacionDocumentacion>();

        ActualizacionDocumentacion limiteMunicipalOf = new ActualizacionDocumentacion();
        limiteMunicipalOf = this.inicializarDocumentacion(limiteMunicipalOf,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(),
            ETipoDocumento.LIMITE_MUNICIPAL_OFICIAL.getId(), ESiNo.SI
            .getCodigo());

        ActualizacionDocumentacion insumosCarto = new ActualizacionDocumentacion();
        insumosCarto = this.inicializarDocumentacion(insumosCarto,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(), ETipoDocumento.INSUMOS_CARTOGRAFICOS
                .getId(), ESiNo.SI.getCodigo());

        ActualizacionDocumentacion insumosAgro = new ActualizacionDocumentacion();
        insumosAgro = this.inicializarDocumentacion(insumosAgro,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(), ETipoDocumento.INSUMOS_AGROLOGICOS
                .getId(), ESiNo.SI.getCodigo());

        ActualizacionDocumentacion puntosGeodesicos = new ActualizacionDocumentacion();
        puntosGeodesicos = this.inicializarDocumentacion(puntosGeodesicos,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(),
            ETipoDocumento.PUNTOS_GEODESICOS_DEL_DEPARTAMENTO.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion areasHomogeneas = new ActualizacionDocumentacion();
        areasHomogeneas = this.inicializarDocumentacion(areasHomogeneas,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(),
            ETipoDocumento.AREAS_HOMOGENEAS_DE_TIERRA.getId(), ESiNo.SI
            .getCodigo());

        ActualizacionDocumentacion estudioSuelos = new ActualizacionDocumentacion();
        estudioSuelos = this.inicializarDocumentacion(estudioSuelos,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(),
            ETipoDocumento.ESTUDIO_DETALLADO_DE_SUELOS.getId(), ESiNo.NO
            .getCodigo());

        ActualizacionDocumentacion ortofotos = new ActualizacionDocumentacion();
        ortofotos = this.inicializarDocumentacion(ortofotos,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(), ETipoDocumento.ORTOFOTOS.getId(),
            ESiNo.NO.getCodigo());

        ActualizacionDocumentacion otrasImagenes = new ActualizacionDocumentacion();
        otrasImagenes = this.inicializarDocumentacion(otrasImagenes,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(), ETipoDocumento.OTRAS_IMAGENES
                .getId(), ESiNo.NO.getCodigo());

        ActualizacionDocumentacion fotoAereasIndices = new ActualizacionDocumentacion();
        fotoAereasIndices = this.inicializarDocumentacion(fotoAereasIndices,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(),
            ETipoDocumento.FOTOGRAFIAS_AEREAS_CON_INDICES_DE_VUELO.getId(),
            ESiNo.SI.getCodigo());

        ActualizacionDocumentacion inventarioEquiposCalibracion = new ActualizacionDocumentacion();
        inventarioEquiposCalibracion = this.inicializarDocumentacion(
            inventarioEquiposCalibracion,
            EActualizacionDocumentacionCategoria.GEOREFERENCIADO
                .getDescripcion(),
            ETipoDocumento.INVENTARIO_EQUIPOS_Y_VERIFICACION_CALIBRACION
                .getId(), ESiNo.NO.getCodigo());

        this.georrefYTopoDocs.add(limiteMunicipalOf);
        this.georrefYTopoDocs.add(insumosCarto);
        this.georrefYTopoDocs.add(insumosAgro);
        this.georrefYTopoDocs.add(puntosGeodesicos);
        this.georrefYTopoDocs.add(areasHomogeneas);
        this.georrefYTopoDocs.add(estudioSuelos);
        this.georrefYTopoDocs.add(ortofotos);
        this.georrefYTopoDocs.add(otrasImagenes);
        this.georrefYTopoDocs.add(fotoAereasIndices);
        this.georrefYTopoDocs.add(inventarioEquiposCalibracion);
    }

    // -------------------------------------------------------------------------------------------------------
    /**
     * Genera la informacion suficiente para poder mostrar la documentacion en las tablas, sin
     * llenar los atributos que se llenan en cada tabla.
     *
     * @param documentacion
     * @param categoria
     * @param formato
     * @param tipoDocumentoId
     * @return
     */
    public ActualizacionDocumentacion inicializarDocumentacion(
        ActualizacionDocumentacion documentacion, String categoria,
        Long tipoDocumentoId, String requerido) {

        documentacion.setActualizacion(this.actualizacion);
        documentacion.setCategoria(categoria);
        documentacion.setFechaLog(new Date());
        documentacion.setRequerido(requerido);
        documentacion.setTipoDocumento(this.getGeneralesService()
            .buscarTipoDocumentoPorId(tipoDocumentoId));
        documentacion.setUrbanoRural(this.zonaDocumentacion);
        documentacion.setUsuarioLog(this.usuario.getLogin());

        return documentacion;
    }

    // -------------------------------------------------------------------------------------------------------
    public String finalizarRegistroDocumentacionGeorefYTopo() {

        for (ActualizacionDocumentacion ad : this.georrefYTopoDocs) {
            this.getActualizacionService()
                .guardarYActualizarActualizacionDocumentacion(ad);
        }

        this.getActualizacionService().guardarYActualizarActualizacion(
            this.actualizacion);

        // TODO franz.gamba :: aca se implementa la transicion en el proceso
        UtilidadesWeb.removerManagedBean("evaluarEstadoCatastInfoExist");
        this.tareasPendientesMB.init();
        return "index";
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Método que cierra la pantalla
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("evaluarEstadoCatastInfoExist");
        this.tareasPendientesMB.init();
        return "index";
    }

}
