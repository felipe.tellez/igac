package co.gov.igac.snc.web.mb.conservacion;

/**
 * @author david.cifuentes
 */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEntidadEstado;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.HashMap;
import java.util.Map;
import co.gov.igac.snc.fachadas.IGenerales;

@Component("gestionEntidadBloqueo")
@Scope("session")
public class GestionEntidadBloqueoMB extends SNCManagedBean {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionEntidadBloqueoMB.class);

    // ------ Servicios ------//
    private IGenerales generalesService = null;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    // ------ Variables ------//
    /**
     * Variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Variable que almacena la {@link Entidad} seleccionada
     */
    private Entidad entidadSeleccionada;

    /**
     * Entidad para ser utilizada en la modificación
     */
    private Entidad entidadEdicion;

    /**
     * Variable usada para la consulta por nombre de entidades
     */
    private String nombreEntidad;

    /**
     * Variable usada para la consulta por estados a entidades
     */
    private String estadoEntidad;

    /**
     * Lista de las entidades filtradas mediante los criterios de búsqueda
     */
    private List<Entidad> entidadesBloqueo;

    /**
     * Variable booleana para saber si se está editando una entidad o si se está adicionando una
     * nueva
     */
    private boolean editModeBool;

    /**
     * Lista de items de los estados de una entidad.
     */
    private List<SelectItem> estadosEntidad;

    /**
     * Variable item list con los valores de los departamentos disponibles
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Variable con el codigo del departamento seleccionado
     */
    private String departamentoSeleccionado;

    /**
     * Variable con el codigo del departamento usado en la busqueda
     */
    private String departamento;

    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosItemList;

    /**
     * Variable item list con los valores de los municipios disponibles para el buscador
     */
    private List<SelectItem> municipiosBuscadorItemList;

    /**
     * Variable con el codigo del municipio seleccionado
     */
    private String municipioSeleccionado;

    /**
     * Variable con el codigo del municipio usado en la busqueda
     */
    private String municipio;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    @Autowired
    private GeneralMB generalMB;
    // ------------------ GETTERS Y SETTERS -------------------- //

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Entidad getEntidadSeleccionada() {
        return entidadSeleccionada;
    }

    public void setEntidadSeleccionada(Entidad entidadSeleccionada) {
        this.entidadSeleccionada = entidadSeleccionada;
    }

    public String getNombreEntidad() {
        return nombreEntidad;
    }

    public void setNombreEntidad(String nombreEntidad) {
        this.nombreEntidad = nombreEntidad;
    }

    public String getEstadoEntidad() {
        return estadoEntidad;
    }

    public void setEstadoEntidad(String estadoEntidad) {
        this.estadoEntidad = estadoEntidad;
    }

    public List<Entidad> getEntidadesBloqueo() {
        return entidadesBloqueo;
    }

    public void setEntidadesBloqueo(List<Entidad> entidadesBloqueo) {
        this.entidadesBloqueo = entidadesBloqueo;
    }

    public boolean isEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public List<SelectItem> getEstadosEntidad() {
        return estadosEntidad;
    }

    public void setEstadosEntidad(List<SelectItem> estadosEntidad) {
        this.estadosEntidad = estadosEntidad;
    }

    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public List<SelectItem> getMunicipiosBuscadorItemList() {
        return municipiosBuscadorItemList;
    }

    public void setMunicipiosBuscadorItemList(List<SelectItem> municipiosBuscadorItemList) {
        this.municipiosBuscadorItemList = municipiosBuscadorItemList;
    }

    public String getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    public void setDepartamentoSeleccionado(String departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    public String getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(String municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDepartamentoSeleccionadoNombre() {

        for (SelectItem si : this.departamentosItemList) {
            if (si.getValue().equals(this.departamentoSeleccionado)) {
                return si.getLabel();
            }
        }

        return "";
    }

    /**
     * @return the entidadEdicion
     */
    public Entidad getEntidadEdicion() {
        return entidadEdicion;
    }

    /**
     * @param entidadEdicion the entidadEdicion to set
     */
    public void setEntidadEdicion(final Entidad entidadEdicion) {
        this.entidadEdicion = entidadEdicion;
    }

    // ------------------ M É T O D O S -------------------- //
    @PostConstruct
    public void init() {
        // usuario
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        // Cargar estados existentes
        this.estadosEntidad = new ArrayList<SelectItem>();
        this.estadosEntidad.add(new SelectItem(null, DEFAULT_COMBOS));

        for (Dominio dominio : this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.ENTIDAD_ESTADO)) {
            this.estadosEntidad.add(new SelectItem(dominio.getCodigo(),
                dominio.getCodigo() + " - " + dominio.getValor()));

        }

        this.entidadSeleccionada = new Entidad();
        entidadEdicion = new Entidad();
        actualizarEstadoNuevaEntidad();
        this.entidadesBloqueo = new ArrayList<Entidad>();

        this.cargarDepartamentos();
    }

    /**
     * Método que realiza el cargue de la entidad seleccionada
     *
     * @author david.cifuentes
     */
    public void cargarEntidad() {

        if (!this.editModeBool) {
            reiniciarVariables();
            buscarEntidades();
        } else {
            this.departamentoSeleccionado = this.entidadSeleccionada.getDepartamento().getCodigo();
            this.municipioSeleccionado = this.entidadSeleccionada.getMunicipio().getCodigo();
            actualizarMunicipiosListener();
            copiarEntidadEdicion(entidadSeleccionada, entidadEdicion);
        }
    }

    /**
     * Método que realiza la eliminación de la entidad seleccionada.
     *
     * @author david.cifuentes
     */
    public void eliminarEntidad() {
        // Verificar las validaciones para la eliminación de la entidad
        if (!this.validarAsociacionEntidadBloqueo()) {
            this.getConservacionService().eliminarEntidad(this.entidadSeleccionada);
            this.reiniciarVariables();
            this.buscarEntidades();
            this.addMensajeInfo("Se eliminó la entidad satisfactoriamente.");
        }
    }

    /**
     * Método que verifica si una entidad se encuentra o no asociada al bloqueo de un predio o una
     * persona
     *
     * @author david.cifuentes
     */
    public boolean validarAsociacionEntidadBloqueo() {

        // No se permite eliminar o modificar entidades que estén asociadas al bloqueo de un
        // predio o persona
        boolean validate = this.getConservacionService().existeAsociacionEntidadBloqueo(
            this.entidadSeleccionada);

        if (validate) {
            String msg = new String(
                "Esta entidad se encuentra asociada al bloqueo de un predio o persona y no puede ser ");
            if (this.editModeBool) {
                msg = msg + "modificada";
            } else {
                msg = msg + "eliminada";
            }
            this.addMensajeError(msg);
        }
        if (this.usuario.getCodigoEstructuraOrganizacional() != null &&
             this.entidadSeleccionada.getOrganizacion() != null &&
             !this.usuario.getCodigoEstructuraOrganizacional().equals(this.entidadSeleccionada.
                getOrganizacion().getCodigo())) {
            EstructuraOrganizacional eo = this.getGeneralesService().
                buscarEstructuraOrganizacionalPorCodigo(this.entidadSeleccionada.getOrganizacion().
                    getCodigo());
            String msg = new String("La entidad fue registrada por " + eo.getNombre() +
                " por lo tanto no puede ser ");
            if (this.editModeBool) {
                msg = msg + "modificada";
            } else {
                msg = msg + "eliminada";
            }
            this.addMensajeError(msg);
            return true;
        }
        return validate;
    }

    /**
     * Método que guarda una entidad nueva, o actualiza una existente.
     *
     * @author david.cifuentes
     */
    public void guardarEntidad() {
        try {
            // Validar permisos de actualización de la entidad
            if (this.editModeBool &&
                 this.usuario.getCodigoEstructuraOrganizacional() != null &&
                 this.entidadSeleccionada.getOrganizacion() != null &&
                 !this.usuario.getCodigoEstructuraOrganizacional().equals(this.entidadSeleccionada.
                    getOrganizacion().getCodigo())) {
                EstructuraOrganizacional eo = this.getGeneralesService().
                    buscarEstructuraOrganizacionalPorCodigo(this.entidadSeleccionada.
                        getOrganizacion().getCodigo());
                this.addMensajeError("La entidad fue registrada por " + eo.getNombre() +
                    " por lo tanto no puede ser modificada. ");
                copiarEntidadEdicion(entidadEdicion, entidadSeleccionada);
                return;
            }
            // Verificar existencia de la entidad
            Entidad entidadExiste = this.getConservacionService().buscarEntidad(
                this.entidadSeleccionada.getNombre().toUpperCase(),
                this.departamentoSeleccionado, this.municipioSeleccionado, this.entidadSeleccionada.
                    getCorreo().toUpperCase());

            // Verificar existencia del correo ya este asociado a otra entidad
            Entidad entidadExisteCorreo = this.getConservacionService().buscarEntidad(null,
                this.departamentoSeleccionado, this.municipioSeleccionado, entidadSeleccionada.
                    getCorreo());

            if (!this.editModeBool) {
                if (entidadExiste != null) {
                    this.addMensajeError(
                        "El nombre de la entidad ya se encuentra registrado, por favor verifique e intente nuevamente.");
                    this.buscarEntidades();
                    return;
                }

                if (entidadExisteCorreo != null) {
                    this.addMensajeError(
                        "Existe una entidad registrada para el departamento, municipio y correo electrónico que se está registrando. Verifique");
                    this.buscarEntidades();
                    return;
                }

            } else if (entidadExiste != null && entidadExiste.getId().longValue() !=
                this.entidadSeleccionada.getId().longValue()) {
                this.addMensajeError(
                    "El nombre de la entidad ya se encuentra registrado, por favor verifique e intente nuevamente.");
                this.buscarEntidades();
                return;
            } else if (entidadExisteCorreo != null && entidadExisteCorreo.getId().longValue() !=
                this.entidadSeleccionada.getId().longValue()) {
                this.addMensajeError(
                    "Existe una entidad registrada para el departamento, municipio y correo electrónico que se está registrando. Verifique");
                this.buscarEntidades();
                return;
            }

            if (this.editModeBool && this.validarAsociacionEntidadBloqueo()) {
                this.buscarEntidades();
                return;
            } else {
                this.entidadSeleccionada.setFechaRegistro(new Date(System.currentTimeMillis()));
            }

            this.entidadSeleccionada.setFechaLog(new Date(System.currentTimeMillis()));
            this.entidadSeleccionada.setUsuarioLog(this.usuario.getLogin());

            Municipio m = new Municipio();
            m.setCodigo(this.municipioSeleccionado);
            this.entidadSeleccionada.setMunicipio(m);

            Departamento d = new Departamento();
            d.setCodigo(this.departamentoSeleccionado);
            this.entidadSeleccionada.setDepartamento(d);

            EstructuraOrganizacional estructuraOrganizacional = this.getGeneralesService().
                buscarEstructuraOrganizacionalPorCodigo(this.usuario.
                    getCodigoEstructuraOrganizacional());
            this.entidadSeleccionada.setOrganizacion(estructuraOrganizacional);

            convertirMayusculasDatosEntidad();

            this.entidadSeleccionada = this.getConservacionService().guardarEntidad(
                this.entidadSeleccionada);

            if (this.entidadSeleccionada != null) {
                if (this.editModeBool) {
                    this.addMensajeInfo("Se actualizó la entidad satisfactoriamente.");
                } else {
                    this.addMensajeInfo("Se registró la entidad satisfactoriamente.");
                }
            } else {
                this.addMensajeError("Error al guardar la entidad. Por favor intente nuevamente.");
            }
            this.reiniciarVariables();
            this.buscarEntidades();

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error guardando la entidad");
        }
        buscarEntidades();
    }

    private void convertirMayusculasDatosEntidad() {
        if (this.entidadSeleccionada.getCorreo() != null) {
            this.entidadSeleccionada.setCorreo(this.entidadSeleccionada.getCorreo().toUpperCase());
            this.entidadSeleccionada.setDireccion(this.entidadSeleccionada.getDireccion().
                toUpperCase());
            this.entidadSeleccionada.setNombre(this.entidadSeleccionada.getNombre().toUpperCase());
        }
    }

    private void reiniciarVariables() {
        this.estadoEntidad = null;
        this.entidadesBloqueo = null;
        this.entidadSeleccionada = null;
        this.departamentoSeleccionado = null;
        this.municipioSeleccionado = null;
        this.entidadesBloqueo = new ArrayList<Entidad>();
        this.entidadSeleccionada = new Entidad();
        this.entidadEdicion = new Entidad();
        this.entidadSeleccionada.setEstado(EEntidadEstado.ACTIVO.getEstado());
    }

    /**
     * Método que realiza la búsqueda de entidades según su nombre y su estado, sin importar
     * mayúsculas o mínusculas.
     *
     * @author david.cifuentes
     */
    public void buscarEntidades() {
        try {
            actualizarDeptoMunicipioBusqueda();
            if (this.departamento != null && !this.departamento.isEmpty() &&
                 this.municipio != null && !this.municipio.isEmpty()) {
                this.entidadesBloqueo = this.getConservacionService()
                    .buscarEntidades(this.nombreEntidad,
                        this.estadoEntidad, this.departamento, this.municipio);

                if (this.entidadesBloqueo == null ||
                     this.entidadesBloqueo.isEmpty()) {
                    this.addMensajeWarn("No se encontraron resultados de búsqueda.");
                    this.entidadSeleccionada = new Entidad();
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error consultando las entidades");
        }
    }

    private void actualizarDeptoMunicipioBusqueda() {
        if (this.entidadSeleccionada != null && this.entidadSeleccionada.getDepartamento() != null &&
             this.entidadSeleccionada.getMunicipio() != null) {
            if (this.departamento != null && !this.departamento.equals(
                entidadSeleccionada.getDepartamento().getCodigo())) {
                this.departamento = this.entidadSeleccionada.getDepartamento().getCodigo();
            }
            if (this.municipio != null && !this.municipio.equals(this.entidadSeleccionada.
                getMunicipio().getCodigo())) {
                this.municipio = this.entidadSeleccionada.getMunicipio().getCodigo();
            }
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que retorna al arbol de tareas.
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("gestionEntidadBloqueo");
        this.tareasPendientesMB.init();
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Metodo para cargar los departamentos
     *
     * @author dumar.penuela
     */
    public void cargarDepartamentos() {

        List<Departamento> deptosList;
        List<Municipio> municipiosList;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = generalMB.getDepartamentos(Constantes.COLOMBIA);

        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService().getCacheMunicipiosByDepartamento(
                dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                    m.getCodigo().substring(2, m.getCodigo().length()) + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    /**
     * Metodo para constuir la lista de departamentos
     *
     * @author dumar.penuela
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento dpto : departamentosList) {
            this.departamentosItemList.add(new SelectItem(dpto.getCodigo(), dpto.getCodigo() + "-" +
                 dpto.getNombre()));
        }
    }

    /**
     * Metodo para actualizar los municipios del buscador
     *
     * @author dumar.penuela
     */
    public void actualizarMunicipiosBuscardorListener() {

        if (this.municipiosDeptos != null) {

            this.municipiosBuscadorItemList = this.municipiosDeptos.get(this.departamento);
        }

    }

    /**
     * Metodo para actualizar los municipios en la edición de la entidad
     *
     * @author dumar.penuela
     */
    public void actualizarMunicipiosListener() {

        if (this.municipiosDeptos != null) {

            this.municipiosItemList = this.municipiosDeptos.get(this.departamentoSeleccionado);
        }

    }

    private void copiarEntidadEdicion(final Entidad origen, final Entidad destino) {
        destino.setCorreo(origen.getCorreo());
        destino.setDepartamento(origen.getDepartamento());
        destino.setDireccion(origen.getDireccion());
        destino.setEstado(origen.getEstado());
        destino.setFechaLog(origen.getFechaLog());
        destino.setFechaRegistro(origen.getFechaRegistro());
        destino.setId(origen.getId());
        destino.setMunicipio(origen.getMunicipio());
        destino.setNombre(origen.getNombre());
        destino.setOrganizacion(origen.getOrganizacion());
        destino.setUsuarioLog(origen.getUsuarioLog());
    }

    private void actualizarEstadoNuevaEntidad() {
        for (SelectItem item : estadosEntidad) {
            if (item != null && item.getValue() != null && item.getValue().equals("A")) {
                entidadSeleccionada.setEstado(item.getValue().toString());
            }
        }
    }

    // Fin de la clase
}
