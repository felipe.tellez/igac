package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.InformacionBasicaAgro;
import co.gov.igac.snc.persistence.entity.actualizacion.InformacionBasicaCarto;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para registrar la información correspondiente al diagnóstico de un municipio.
 *
 * @author jamir.avila
 */
@Component("registrarInformacion")
@Scope("session")
public class RegistrarInformacionDiagnosticoMunicipioMB extends SNCManagedBean
    implements Serializable {

    /** Servicios Generales */
    /**
     * Identificador de versión por omisión.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Servicio de bitácora
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistrarInformacionDiagnosticoMunicipioMB.class);

    /* ------------------------------------------------------------------- */
    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    /**
     * Identificador único de la actualización.
     */
    private long idActualizacion;

    private UsuarioDTO usuario;

    /** Nombre del departamento en el que está el municipio en actualización. */
    private String departamentoNombre;
    /** Nombre del municipio en actualización. */
    private String municipioNombre;

    /** Información disponible seleccionada de cartografía. */
    private List<InformacionDisponible> cartografias;

    /** Paginado de la tabla de cartografías seleccionadas. */
    private LazyDataModel<InformacionDisponible> cartografiasLazyModel;

    /** Información disponible seleccionada de agrología. */
    private List<InformacionDisponible> agrologias;

    /** Paginador de la tabla de agrologías seleccionadas. */
    private LazyDataModel<InformacionDisponible> agrologiasLazyModel;

    /** Información disponible de cartografía. */
    private List<InformacionDisponible> cartografiasDisponibles;

    /** Información disponible de agrología. */
    private List<InformacionDisponible> agrologiasDisponibles;

    /** Paginador de la tabla de cartografìas disponibles. */
    private LazyDataModel<InformacionDisponible> cartografiasDisponiblesLazyModel;

    /** Paginador de la tabla de agrologias disponibles. */
    private LazyDataModel<InformacionDisponible> agrologiasDisponiblesLazyModel;

    /* ------------------------------------------------------------------- */
    /** Cartografías seleccionadas por el usuario. */
    private InformacionDisponible cartografiaSeleccionada;
    /** Agrologías seleccionadas por el usuario. */
    private InformacionDisponible agrologiaSeleccionada;

    /** Cartografías disponibles seleccionadas por el usuario. */
    private InformacionDisponible[] nuevasCartografiasSeleccionadas;
    /** Agrologías disponibles seleccionadas por el usuario. */
    private InformacionDisponible[] nuevasAgrologiasSeleccionadas;

    /** Private objeto actualización sobre el que se registra la información del municipio */
    private Actualizacion actualizacion;

    /* ------------------------------------------------------------------- */
    /** Nombre de la tabla que se debe actualizar. */
    private enum TablaModel {
        CARTOGRAFIAS_LAZY_MODEL, AGROLOGIAS_LAZY_MODEL, CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL,
        AGROLOGIAS_DISPONIBLES_LAZY_MODEL
    };

    /**
     * Implementación del objeto LazyDataModel para los datatable.
     *
     * @author jamir.avila
     */
    private class InformacionLazyDataModel extends LazyDataModel<InformacionDisponible> {

        /** Identificador de versión por omisión. */
        private static final long serialVersionUID = 1L;
        private TablaModel tablaModel;

        /** Constructor por omisión. */
        public InformacionLazyDataModel(TablaModel tablaModel) {
            this.tablaModel = tablaModel;
        }

        @Override
        public List<InformacionDisponible> load(int first, int pageSize,
            String sortField, SortOrder sortOrder, Map<String, String> filters) {
            List<InformacionDisponible> resultados =
                poblarTabla(first, pageSize, tablaModel);
            return resultados;
        }

        /**
         * Se agregó este método por migración a PrimeFaces 3.2
         *
         * @version 2.0
         */
        @Override
        public Object getRowKey(InformacionDisponible informacionDisponible) {
            return informacionDisponible.getInformacionAsociada();
        }

    }

    /* ------------------------------------------------------------------- */
    /**
     * Constructor por omisión.
     */
    public RegistrarInformacionDiagnosticoMunicipioMB() {
        cartografias = new LinkedList<InformacionDisponible>();
        agrologias = new LinkedList<InformacionDisponible>();

        cartografiasLazyModel = new InformacionLazyDataModel(TablaModel.CARTOGRAFIAS_LAZY_MODEL);
        agrologiasLazyModel = new InformacionLazyDataModel(TablaModel.AGROLOGIAS_LAZY_MODEL);
        cartografiasDisponiblesLazyModel = new InformacionLazyDataModel(
            TablaModel.CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL);
        agrologiasDisponiblesLazyModel = new InformacionLazyDataModel(
            TablaModel.AGROLOGIAS_DISPONIBLES_LAZY_MODEL);
    }

    /**
     * Método de inicializaci{on una vez construido el objeto.
     */
    @PostConstruct
    public void init() {
        LOGGER.debug("Método PostConstruct invocado.");
        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }
        this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        //TODO JAMIR: identificador de actualización quemado. 09/11/2011
        //Long idActualizacion = 250L;
        this.idActualizacion = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();

        actualizacion = this.getActualizacionService().recuperarActualizacionPorId(idActualizacion);
        if (null == actualizacion) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "Registrar información",
                "Imposible obtener el objeto actualización de id: " +
                 idActualizacion));
            LOGGER.error("Imposible obtener el objeto actualización de id: " +
                 idActualizacion);
        } else {
            Departamento departamento = actualizacion.getDepartamento();
            if (null != departamento) {
                departamentoNombre = departamento.getNombre();
            } else {
                LOGGER.error("Departamento de la actualización: " + idActualizacion +
                    " no encontrado!!!");
            }
            Municipio municipio = actualizacion.getMunicipio();
            if (null != municipio) {
                municipioNombre = municipio.getNombre();
            } else {
                LOGGER.error("Municipio de la actualización: " + idActualizacion +
                    " no encontrado!!!");
            }
        }

        cartografiasDisponibles = new LinkedList<InformacionDisponible>();
        List<Dominio> tempCartografias = null;
        try {
            tempCartografias = this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.INFORMACION_BASICA_CAR_TIPO);
            for (Dominio dominio : tempCartografias) {
                cartografiasDisponibles.add(new InformacionDisponible(dominio));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "Registrar información",
                "Imposible obtener el dominio " +
                 EDominio.INFORMACION_BASICA_CAR_TIPO));
            LOGGER.error("Imposible obtener el dominio " +
                 EDominio.INFORMACION_BASICA_CAR_TIPO, e);
        }

        agrologiasDisponibles = new LinkedList<InformacionDisponible>();
        List<Dominio> tempAgrologias = null;
        try {
            tempAgrologias = this.getGeneralesService()
                .getCacheDominioPorNombre(EDominio.INFORMACION_BASICA_AGR_TIPO);
            for (Dominio dominio : tempAgrologias) {
                agrologiasDisponibles.add(new InformacionDisponible(dominio));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "Registrar información",
                "Imposible obtener el dominio " +
                 EDominio.INFORMACION_BASICA_AGR_TIPO));
            LOGGER.error("Imposible obtener el dominio " +
                 EDominio.INFORMACION_BASICA_AGR_TIPO, e);
        }

        for (TablaModel nombreVariable : TablaModel.values()) {
            poblar(nombreVariable);
        }
    }

    /**
     * Este método se encarga de poblar la tabla indicada.
     *
     * @param first primer registro del que se debe recuperar información.
     * @param pageSize número de registros a recuperar.
     * @param tablaModel indica la tabla a llenar: CARTOGRAFIAS_LAZY_MODEL, AGROLOGIAS_LAZY_MODEL,
     * CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL, AGROLOGIAS_DISPONIBLES_LAZY_MODEL
     * @return una lista de objetos InformacionDisponible recuperados.
     */
    private List<InformacionDisponible> poblarTabla(int first, int pageSize, TablaModel tablaModel) {
        LOGGER.debug("inicio de poblar(" + first + ", " + pageSize + ")");
        List<InformacionDisponible> resultados = new LinkedList<InformacionDisponible>();

        List<InformacionDisponible> datos = null;
        switch (tablaModel) {
            case CARTOGRAFIAS_LAZY_MODEL: datos = cartografias;
                break;
            case AGROLOGIAS_LAZY_MODEL: datos = agrologias;
                break;
            case CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL: datos = cartografiasDisponibles;
                break;
            case AGROLOGIAS_DISPONIBLES_LAZY_MODEL: datos = agrologiasDisponibles;
                break;
        }

        for (int i = first, LIMITE = first + pageSize;
            i < LIMITE && i < datos.size(); ++i) {
            resultados.add(datos.get(i));
        }

        return resultados;
    }

    /**
     * Método para poblar una tabla de datos de PrimeFaces.
     *
     * @param tablaModel indica la tabla a llenar: CARTOGRAFIAS_LAZY_MODEL, AGROLOGIAS_LAZY_MODEL,
     * CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL, AGROLOGIAS_DISPONIBLES_LAZY_MODEL
     */
    private void poblar(TablaModel tablaModel) {
        LOGGER.debug("poblar(" + tablaModel.name() + ")");

        List<InformacionDisponible> datos = null;
        LazyDataModel<InformacionDisponible> lazyDataModel = null;
        switch (tablaModel) {
            case CARTOGRAFIAS_LAZY_MODEL:
                datos = cartografias;
                lazyDataModel = cartografiasLazyModel;
                break;
            case AGROLOGIAS_LAZY_MODEL:
                datos = agrologias;
                lazyDataModel = agrologiasLazyModel;
                break;
            case CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL:
                datos = cartografiasDisponibles;
                lazyDataModel = cartografiasDisponiblesLazyModel;
                break;
            case AGROLOGIAS_DISPONIBLES_LAZY_MODEL:
                datos = agrologiasDisponibles;
                lazyDataModel = agrologiasDisponiblesLazyModel;
                break;
        }
        lazyDataModel.setRowCount(datos.size());
    }

    /**
     * Esta operación permite vincular la información cartográfica seleccionada con el
     * correspondiente proceso de actualización.
     *
     * @param ae evento JSF.
     */
    public void cmdVincularCartografias(ActionEvent ae) {
        LOGGER.debug("inicio de vincular cartografías");
        if (nuevasCartografiasSeleccionadas != null) {
            cartografias.addAll(Arrays.asList(nuevasCartografiasSeleccionadas));
            cartografiasDisponibles.removeAll(cartografias);
        }
        poblar(TablaModel.CARTOGRAFIAS_LAZY_MODEL);
        poblar(TablaModel.CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL);
        LOGGER.debug("fin de vincular cartografías");
    }

    /**
     * Esta operación permite vincular la información agrológica seleccionada con el correspondiente
     * proceso de agrología.
     *
     * @param ae evento JSF.
     */
    public void cmdVincularAgrologias(ActionEvent ae) {
        LOGGER.debug("inicio de vincular agrologías");
        if (nuevasAgrologiasSeleccionadas != null) {
            agrologias.addAll(Arrays.asList(nuevasAgrologiasSeleccionadas));
            agrologiasDisponibles.removeAll(agrologias);
        }
        poblar(TablaModel.AGROLOGIAS_LAZY_MODEL);
        poblar(TablaModel.AGROLOGIAS_DISPONIBLES_LAZY_MODEL);
        LOGGER.debug("fin de vincular agrologías");
    }

    /**
     * Esta operación se usa para borrrar el elemento cartográfico seleccionado.
     *
     * @param ae información de la acción.
     */
    public void cmdBorrarCartografia() {
        LOGGER.debug("inicio de borrar el elemento cartográfico seleccionado");
        if (null != cartografiaSeleccionada) {
            LOGGER.debug("borrando... " + cartografiaSeleccionada.getInformacionAsociada());
            cartografias.remove(cartografiaSeleccionada);
            cartografiasDisponibles.add(cartografiaSeleccionada);
        }
        poblar(TablaModel.CARTOGRAFIAS_LAZY_MODEL);
        poblar(TablaModel.CARTOGRAFIAS_DISPONIBLES_LAZY_MODEL);
        LOGGER.debug("fin de borrar el elemento cartográfico seleccionado");
    }

    /**
     * Esta operación se usa para borrrar el elemento agrológico seleccionado.
     *
     * @param ae información de la acción.
     */
    public void cmdBorrarAgrologia() {
        LOGGER.debug("inicio de borrar elemento agrológico seleccionado: ");
        if (null != agrologiaSeleccionada) {
            LOGGER.debug("borrando... " + agrologiaSeleccionada.getInformacionAsociada());
            agrologias.remove(agrologiaSeleccionada);
            agrologiasDisponibles.add(agrologiaSeleccionada);
        }
        poblar(TablaModel.AGROLOGIAS_LAZY_MODEL);
        poblar(TablaModel.AGROLOGIAS_DISPONIBLES_LAZY_MODEL);
        LOGGER.debug("inicio de borrar elemento agrológico seleccionado: ");
    }

    /**
     * Operación que registra las operaciones en la base de datos.
     *
     * @return una cadena con la página que se debe invocar.
     */
    public String cmdAceptar() {
        LOGGER.debug("Inicio de IMPLEMENTAR aceptar");

        String siguientePagina = null;
        UsuarioDTO usuarioDTO = MenuMB.getMenu().getUsuarioDto();

        try {
            List<InformacionBasicaCarto> informacionesCartograficas =
                new LinkedList<InformacionBasicaCarto>();
            for (InformacionDisponible info : cartografias) {
                InformacionBasicaCarto basicaCarto = new InformacionBasicaCarto();
                basicaCarto.setDescripcion(info.getDescripcion());
                basicaCarto.setInformacion(info.getInformacionAsociada());
                basicaCarto.setTipo(info.getDescripcion());
                basicaCarto.setFechaLog(new java.util.Date());
                basicaCarto.setUsuarioLog(usuarioDTO.getLogin());
                basicaCarto.setActualizacion(actualizacion);
                informacionesCartograficas.add(basicaCarto);
            }
            List<InformacionBasicaAgro> informacionesAgrologicas =
                new LinkedList<InformacionBasicaAgro>();
            for (InformacionDisponible info : cartografias) {
                InformacionBasicaAgro basicaAgro = new InformacionBasicaAgro();
                basicaAgro.setDescripcion(info.getDescripcion());
                basicaAgro.setInformacion(info.getInformacionAsociada());
                basicaAgro.setTipo(info.getDescripcion());
                basicaAgro.setFechaLog(new java.util.Date());
                basicaAgro.setUsuarioLog(usuarioDTO.getLogin());
                basicaAgro.setActualizacion(actualizacion);
                informacionesAgrologicas.add(basicaAgro);
            }

            this.getActualizacionService().registrarInformacionDiagnosticoMunicipio(actualizacion,
                informacionesCartograficas, informacionesAgrologicas, usuarioDTO);

            this.actualizacion = this.getActualizacionService().guardarYActualizarActualizacion(
                this.actualizacion);
            this.getActualizacionService().ordenarGestionarEstudioDeCostos(
                this.currentActivity.getId(), this.actualizacion, this.usuario);

            siguientePagina = "/tareas/listaTareas.jsf";
            LOGGER.debug("Fin de IMPLEMENTAR aceptar");
            UtilidadesWeb.removerManagedBean("registrarInformacion");
            this.tareasPendientesMB.init();
            return siguientePagina;
        } catch (Exception e) {
            this.addMensajeError("Registrar Información", "Imposible registrar la información");
            LOGGER.error("JAAM:Actualización", e);
            return null;
        }
    }

    /**
     * Operación que permite cancelar el registro de información de diagnóstico.
     *
     * @return
     */
    public String cmdCancelar() {
        LOGGER.debug("comando cancelar");
        return "/tareas/listaTareas.jsf";
    }

    /* ------------------------------------------------------------------- */
    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public List<InformacionDisponible> getCartografias() {
        return cartografias;
    }

    public void setCartografias(List<InformacionDisponible> cartografias) {
        this.cartografias = cartografias;
    }

    public InformacionDisponible getCartografiaSeleccionada() {
        return cartografiaSeleccionada;
    }

    public void setCartografiaSeleccionada(
        InformacionDisponible cartografiaSeleccionada) {
        this.cartografiaSeleccionada = cartografiaSeleccionada;
    }

    public InformacionDisponible getAgrologiaSeleccionada() {
        return agrologiaSeleccionada;
    }

    public void setAgrologiaSeleccionada(
        InformacionDisponible agrologiaSeleccionada) {
        this.agrologiaSeleccionada = agrologiaSeleccionada;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<InformacionDisponible> getAgrologias() {
        return agrologias;
    }

    public void setAgrologias(List<InformacionDisponible> agrologias) {
        this.agrologias = agrologias;
    }

    public List<InformacionDisponible> getCartografiasDisponibles() {
        return cartografiasDisponibles;
    }

    public void setCartografiasDisponibles(
        List<InformacionDisponible> cartografiasDisponibles) {
        this.cartografiasDisponibles = cartografiasDisponibles;
    }

    public LazyDataModel<InformacionDisponible> getCartografiasLazyModel() {
        return cartografiasLazyModel;
    }

    public void setCartografiasLazyModel(
        LazyDataModel<InformacionDisponible> cartografiasLazyModel) {
        this.cartografiasLazyModel = cartografiasLazyModel;
    }

    public List<InformacionDisponible> getAgrologiasDisponibles() {
        return agrologiasDisponibles;
    }

    public LazyDataModel<InformacionDisponible> getCartografiasDisponiblesLazyModel() {
        return cartografiasDisponiblesLazyModel;
    }

    public LazyDataModel<InformacionDisponible> getAgrologiasLazyModel() {
        return agrologiasLazyModel;
    }

    public void setAgrologiasLazyModel(
        LazyDataModel<InformacionDisponible> agrologiasLazyModel) {
        this.agrologiasLazyModel = agrologiasLazyModel;
    }

    public void setCartografiasDisponiblesLazyModel(
        LazyDataModel<InformacionDisponible> cartografiasDisponiblesLazyModel) {
        this.cartografiasDisponiblesLazyModel = cartografiasDisponiblesLazyModel;
    }

    public LazyDataModel<InformacionDisponible> getAgrologiasDisponiblesLazyModel() {
        return agrologiasDisponiblesLazyModel;
    }

    public void setAgrologiasDisponiblesLazyModel(
        LazyDataModel<InformacionDisponible> agrologiasDisponiblesLazyModel) {
        this.agrologiasDisponiblesLazyModel = agrologiasDisponiblesLazyModel;
    }

    public void setAgrologiasDisponibles(
        List<InformacionDisponible> agrologiasDisponibles) {
        this.agrologiasDisponibles = agrologiasDisponibles;
    }

    public InformacionDisponible[] getNuevasCartografiasSeleccionadas() {
        return nuevasCartografiasSeleccionadas;
    }

    public void setNuevasCartografiasSeleccionadas(
        InformacionDisponible[] nuevasCartografiasSeleccionadas) {
        this.nuevasCartografiasSeleccionadas = nuevasCartografiasSeleccionadas;
    }

    public InformacionDisponible[] getNuevasAgrologiasSeleccionadas() {
        return nuevasAgrologiasSeleccionadas;
    }

    public void setNuevasAgrologiasSeleccionadas(
        InformacionDisponible[] nuevasAgrologiasSeleccionadas) {
        this.nuevasAgrologiasSeleccionadas = nuevasAgrologiasSeleccionadas;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

}
