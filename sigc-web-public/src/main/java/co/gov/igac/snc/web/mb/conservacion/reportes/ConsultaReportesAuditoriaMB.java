package co.gov.igac.snc.web.mb.conservacion.reportes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReportesAuditoriaDinamicosSNC;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author javier.barajas
 *
 * MB Reportes Inconsistencias
 */
@Component("consultaReportesAuditoria")
@Scope("session")
public class ConsultaReportesAuditoriaMB extends SNCManagedBean {

    private static final long serialVersionUID = -2964745655243791827L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaReportesAuditoriaMB.class);

    @Autowired
    private IContextListener contextoWeb;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    /**
     * Archivo del reporte dinamico
     */
    private File archivoReporteDinamico;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Url del reporte dinamico
     */
    private String urlArchivoReporte;

    /**
     * Lista de items de los reportes
     */
    private ArrayList<SelectItem> listaReportesAuditoriaItemSelect;

    /**
     * Select Item de la seleccion del reporte
     */
    //private SelectItem selectReporte;
    /**
     * Select Item de la seleccion del reporte
     */
    private String selectReporte;

    /**
     * Si hay reporte permite mostrar
     */
    private boolean renderedReporte;

    /**
     * Usuario de la aplicacion
     */
    private UsuarioDTO usuario;

    /**
     * Atributos para el reporte de memorando de comisión
     */
    private StreamedContent reporteDinamico;

    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();
        this.renderedReporte = false;
        LOGGER.debug("++++++++++++entro ConsultaReportesAuditoriaMB");
    }

    // **************************************************************************************************************************
    // Getters y Setters
    // **************************************************************************************************************************
    public String getSelectReporte() {
        return selectReporte;
    }

    public void setSelectReporte(String selectReporte) {
        this.selectReporte = selectReporte;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public File getArchivoReporteDinamico() {
        return archivoReporteDinamico;
    }

    public void setArchivoReporteDinamico(File archivoReporteDinamico) {
        this.archivoReporteDinamico = archivoReporteDinamico;
    }

    public String getUrlArchivoReporte() {
        return urlArchivoReporte;
    }

    public void setUrlArchivoReporte(String urlArchivoReporte) {
        this.urlArchivoReporte = urlArchivoReporte;
    }

    public ArrayList<SelectItem> getListaReportesAuditoriaItemSelect() {
        return listaReportesAuditoriaItemSelect;
    }

    public void setListaReportesAuditoriaItemSelect(
        ArrayList<SelectItem> listaReportesAuditoriaItemSelect) {
        this.listaReportesAuditoriaItemSelect = listaReportesAuditoriaItemSelect;
    }

    public boolean isRenderedReporte() {
        return renderedReporte;
    }

    public void setRenderedReporte(boolean renderedReporte) {
        this.renderedReporte = renderedReporte;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public StreamedContent getReporteDinamico() {
        InputStream stream;

        try {
            stream = new FileInputStream(this.archivoReporteDinamico);
            this.reporteDinamico = new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_PDF, "Reporte.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte " + e.getMessage());
        }
        return reporteDinamico;
    }

    public void setReporteDinamico(StreamedContent reporteDinamico) {
        this.reporteDinamico = reporteDinamico;
    }

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        UtilidadesWeb.removerManagedBean("consultaReportesAuditoria");
        this.tareasPendientesMB.init();
        //this.init();
        return "index";
    }

    // **************************************************************************************************************************
    // Funciones
    // **************************************************************************************************************************
    /**
     * Metodo para generar el reporte
     *
     */
    public void generarReporte() {

        LOGGER.debug("++++++++++++entro URl reporte:" + this.selectReporte);
        try {
            Map<String, String> parametros = new HashMap<String, String>();
            //parametros.put("DEPARTAMENTO_CODIGO", "08");
            //parametros.put("MUNICIPIO_CODIGO","08001");
            this.archivoReporteDinamico = this.getGeneralesService().
                obtenerReporteDinamico(parametros, Long.parseLong(this.selectReporte), usuario);

            if (this.archivoReporteDinamico != null || this.archivoReporteDinamico.length() < 2000) {
                LOGGER.debug("Se genero el reporte en:" + this.archivoReporteDinamico.getPath());
                this.urlArchivoReporte = this.getGeneralesService().publicarArchivoEnWeb(
                    archivoReporteDinamico.getPath());
                this.renderedReporte = true;
                LOGGER.debug("++++++++++++URl reporte:" + this.archivoReporteDinamico.getPath());
            } else {
                LOGGER.debug("No pudo generar el reporte:");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "No pudo generar el Reporte",
                    "No pudo generar el Reporte"));
            }

        } catch (Exception e) {
            LOGGER.debug("No pudo generar el reporte:");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                FacesMessage.SEVERITY_ERROR, "No pudo generar el Reporte",
                "No pudo generar el Reporte"));

        }

    }

    /**
     * Accion cuando selecciona un reporte
     *
     */
    public void seleccionaReporte() {
        LOGGER.debug("+++++++++++++++++++++++ reporte Selccionado" + this.selectReporte);

    }

}
