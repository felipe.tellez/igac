package co.gov.igac.snc.web.mb.conservacion;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.Persona;
import co.gov.igac.snc.persistence.entity.conservacion.PersonaBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.TmpBloqueoMasivo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.DocumentoArchivoAnexo;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.ErrorProcesoMasivo;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.util.EArchivoAnexoFormato;
import co.gov.igac.snc.persistence.util.EDocumentoArchivoAnexoEstado;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EEntidadEstado;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoBloqueo;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETipoDocumentoId;
import co.gov.igac.snc.persistence.util.ETramiteEstado;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.FiltroDatosConsultaPersonasBloqueo;
import co.gov.igac.snc.util.ResultadoBloqueoMasivo;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace de MB para manejar la consulta y bloqueo de personas
 *
 * @author juan.agudelo
 *
 */
@Component("bloqueoPersona")
@Scope("session")
public class BloqueoPersonaMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -16589435516476452L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(BloqueoPersonaMB.class);

    private static final String SEPARATOR_PARAMETER = ";";

    /*
     * Interfaces de servicio
     */
    @Autowired
    private GeneralMB generalService;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * filtro de datos para la consulta de personas bloqueadas
     */
    private FiltroDatosConsultaPersonasBloqueo filtroCPersonaB;

    /**
     * Bandera de visualización de la tabla de resultados
     */
    private boolean banderaBuscar;

    /**
     * Almacena el valor del radio button para el tipo de bloqueo a realizar
     */
    private int tipoBloqueo;

    /**
     * Visualización bloqueo individual
     */
    private boolean tipoBloqueo0;

    /**
     * Mensaje de salida del bloqueo de persona
     */
    private String mensajeSalidaConfirmationDialog;

    /**
     * Mensaje del boton que permite cargar archivos
     */
    private String mensajeCargaArchivo;

    /**
     * Visualización bloqueo masivo
     */
    private boolean tipoBloqueo2;

    /**
     * Documento de soporte de desbloqueo
     */
    private Documento documentoSoporteBloqueo;

    /**
     * Documento archivo anexo soporte del bloqueo masivo
     */
    private DocumentoArchivoAnexo documentoArchivoAnexo;

    /**
     * Fecha inicial del bloqueo de predios por rango
     */
    private Date fechaInicial;

    /**
     * Fecha final del bloqueo de predios por rango
     */
    private Date fechaFinal;

    /**
     * Fecha de recibido el bloqueo
     */
    private Date fechaRecibido;

    /**
     * Motivo del bloqueo de el/los predios
     */
    private String motivoBloqueo;

    /**
     * Almacena el tipo de archivo valido dependiendo de la opcion de carga
     */
    private String tiposDeArchivoValidos;

    /**
     * Visualizacion del boton de carga de archivos
     */
    private boolean tipoBloqueoCargar;

    /**
     * Usuario que accede al sistema
     */
    private UsuarioDTO usuario;

    /**
     * Lista que contiene las personas bloqueadas o el rango de personas bloqueadas
     */
    private List<PersonaBloqueo> personasBloqueadas;

    /**
     * PersonaBloqueo para la inserción
     */
    private PersonaBloqueo personaBloqueo;

    /**
     * Generador aleatorio para nombres de archivos
     */
    private Random generadorNumeroAleatorioNombreArchivo;

    /**
     * Ruta del archivo cargado
     */
    private String rutaMostrar;

    /**
     * Archivo resultado cargado
     */
    private File archivoResultado;
    private File archivoMasivoResultado;

    /**
     * Banderas de visualización de los botones guardar y generar informe
     */
    private boolean banderaBotonGuardar;
    private boolean banderaBotonGenerarInforme;
    private boolean banderaValidar;
    private boolean laBandera;

    /**
     * Descripción del documento de soporte cargado
     */
    private String descripcionArchivo;

    /**
     * Control de carga de archivos de soporte
     */
    private int aSoporte;
    private int aBMasa;

    /**
     * Número de identificació para la busqueda de personas bloqueadas
     */
    private String numeroIdentificacion;

    /**
     * Tipo de identificación para la busqueda de personas bloqueadas
     */
    private String tipoIdentificacion;

    /**
     * Tipo de identificación de una persona
     */
    private List<SelectItem> personaTiposIdentificacion;

    /**
     * Bandera de visualización de tipos de persona para el bloqueo individual
     */
    private boolean tipoPersonaN;
    private boolean tipoPersonaJ;

    /**
     * Tipo de persona para el bloqueo individual
     */
    private int tipoPersona;

    /**
     * Lista de personas bloqueo cargados desde un archivo de bloqueo masivo
     */
    private ArrayList<PersonaBloqueo> bloqueoMasivo;

    /**
     * Objeto que contiene los datos del reporte de bloqueo másivo de personas
     */
    private ReporteDTO reporteBloqueoMasivoPersonas;

    /**
     * Nombre del archivo de soporte del bloqueo masivo
     */
    private String nombreDocumentoBloqueoMasivo;

    /**
     * Documento temporal para la previsualización del archivo de bloqueo masivo
     */
    private Documento documentoTemporalBloqueoMasivo;

    /**
     * Archivo temporal de descarga de documento para la previsualización de documentos de office
     */
    private StreamedContent fileTempDownload;

    /**
     * Documento temporal a visualizar
     */
    private Documento documentoTemporalAVisualizar;

    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    /**
     * Lista de personas coincidentes en la búsqueda.
     */
    private List<Persona> personasCoincidentes;

    /**
     * Persona seleccionada de una lista de personas coincidentes.
     */
    private Persona personaSeleccionada;

    /**
     * Variable para asociarel id de la {
     *
     * @Entidad} que ordena el bloqueo del {@link Persona}
     */
    private Long idEntidadBloqueo;

    /**
     * Lista de tipo {@link Entidad} con las entidades que se encuentran activas
     */
    private List<SelectItem> entidades;

    /**
     * Archivo tempora lque contiene la lista de errores encontrados en la validación de las
     * personas a bloquear.
     */
    private DefaultStreamedContent exportFile;

    /**
     * Fecha de envio del bloqueo
     */
    private Date fechaEnvio;

    /**
     * Numero de radicado del bloqueo
     */
    private String numeroRadicado;

    /**
     * Determina si la opción de restitución de tierras fue seleccionada
     */
    private boolean selectedRestitucionTierras;

    /**
     * valores seleccionados de los combos de departamento y municipio
     */
    private String selectedDepartamento;
    private String selectedMunicipio;

    /**
     * Se almacenan los clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosBuscadorItemList;

    /**
     * Listas de items para el combo de tipo de bloqueo
     */
    private List<SelectItem> tiposBloqueoItemList;

    /**
     * valor seleccionado en el combo tipo de bloqueo
     */
    private String selectedTipoBloqueo;

    /**
     * Listas de items para los combos de Departamento y Municipio
     */
    private List<SelectItem> departamentosItemList;

    /**
     * listas de Departamento y Municipio
     */
    private ArrayList<Departamento> departamentos;

    /**
     * Orden para la lista de Departamentos
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;

    /**
     * Varible que indica que se encontraron errores en en la validación de los personas a bloquear
     */
    private Boolean erroresPersonas = false;

    /**
     * Varible que indica que se mostrar el boton de bloquear personas
     */
    private Boolean bloquearPersonas = true;

    /**
     * Varible que contiene los tramites a bloquear por persona
     */
    private List<Tramite> tramites = null;

    private Workbook wb = new HSSFWorkbook();

    // ------------- methods ------------------------------------
    public Documento getDocumentoTemporalBloqueoMasivo() {
        return documentoTemporalBloqueoMasivo;
    }

    public void setDocumentoTemporalBloqueoMasivo(
        Documento documentoTemporalBloqueoMasivo) {
        this.documentoTemporalBloqueoMasivo = documentoTemporalBloqueoMasivo;
    }

    public boolean isBanderaValidar() {
        return banderaValidar;
    }

    public Documento getDocumentoTemporalAVisualizar() {
        return documentoTemporalAVisualizar;
    }

    public void setDocumentoTemporalAVisualizar(
        Documento documentoTemporalAVisualizar) {
        this.documentoTemporalAVisualizar = documentoTemporalAVisualizar;
    }

    public StreamedContent getFileTempDownload() {
        controladorDescargaArchivos();
        return fileTempDownload;
    }

    public void setFileTempDownload(StreamedContent fileTempDownload) {
        this.fileTempDownload = fileTempDownload;
    }

    public Long getIdEntidadBloqueo() {
        return idEntidadBloqueo;
    }

    public void setIdEntidadBloqueo(Long idEntidadBloqueo) {
        this.idEntidadBloqueo = idEntidadBloqueo;
    }

    public List<SelectItem> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<SelectItem> entidades) {
        this.entidades = entidades;
    }

    public List<Persona> getPersonasCoincidentes() {
        return personasCoincidentes;
    }

    public void setPersonasCoincidentes(List<Persona> personasCoincidentes) {
        this.personasCoincidentes = personasCoincidentes;
    }

    public Persona getPersonaSeleccionada() {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(Persona personaSeleccionada) {
        this.personaSeleccionada = personaSeleccionada;
    }

    public boolean isLaBandera() {
        return laBandera;
    }

    public String getNombreDocumentoBloqueoMasivo() {
        return nombreDocumentoBloqueoMasivo;
    }

    public void setNombreDocumentoBloqueoMasivo(
        String nombreDocumentoBloqueoMasivo) {
        this.nombreDocumentoBloqueoMasivo = nombreDocumentoBloqueoMasivo;
    }

    public void setLaBandera(boolean laBandera) {
        this.laBandera = laBandera;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Documento getDocumentoSoporteBloqueo() {
        return documentoSoporteBloqueo;
    }

    public void setDocumentoSoporteBloqueo(Documento documentoSoporteBloqueo) {
        this.documentoSoporteBloqueo = documentoSoporteBloqueo;
    }

    public boolean isTipoPersonaN() {
        return tipoPersonaN;
    }

    public void setTipoPersonaN(boolean tipoPersonaN) {
        this.tipoPersonaN = tipoPersonaN;
    }

    public boolean isTipoPersonaJ() {
        return tipoPersonaJ;
    }

    public void setTipoPersonaJ(boolean tipoPersonaJ) {
        this.tipoPersonaJ = tipoPersonaJ;
    }

    public List<SelectItem> getPersonaTiposIdentificacion() {
        return personaTiposIdentificacion;
    }

    public void setPersonaTiposIdentificacion(
        List<SelectItem> personaTiposIdentificacion) {
        this.personaTiposIdentificacion = personaTiposIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public FiltroDatosConsultaPersonasBloqueo getFiltroCPersonaB() {
        return filtroCPersonaB;
    }

    public void setFiltroCPersonaB(
        FiltroDatosConsultaPersonasBloqueo filtroCPersonaB) {
        this.filtroCPersonaB = filtroCPersonaB;
    }

    public void setBanderaValidar(boolean banderaValidar) {
        this.banderaValidar = banderaValidar;
    }

    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }

    public boolean isBanderaBotonGuardar() {
        return banderaBotonGuardar;
    }

    public void setBanderaBotonGuardar(boolean banderaBotonGuardar) {
        this.banderaBotonGuardar = banderaBotonGuardar;
    }

    public boolean isBanderaBotonGenerarInforme() {
        return banderaBotonGenerarInforme;
    }

    public void setBanderaBotonGenerarInforme(boolean banderaBotonGenerarInforme) {
        this.banderaBotonGenerarInforme = banderaBotonGenerarInforme;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }

    public void setRutaMostrar(String rutaMostrar) {
        this.rutaMostrar = rutaMostrar;
    }

    public Random getGeneradorNumeroAleatorioNombreArchivo() {
        return generadorNumeroAleatorioNombreArchivo;
    }

    public void setGeneradorNumeroAleatorioNombreArchivo(
        Random generadorNumeroAleatorioNombreArchivo) {
        this.generadorNumeroAleatorioNombreArchivo = generadorNumeroAleatorioNombreArchivo;
    }

    public PersonaBloqueo getPersonaBloqueo() {
        return personaBloqueo;
    }

    public void setPersonaBloqueo(PersonaBloqueo personaBloqueo) {
        this.personaBloqueo = personaBloqueo;
    }

    public List<PersonaBloqueo> getPersonasBloqueadas() {
        return personasBloqueadas;
    }

    public void setPersonasBloqueadas(List<PersonaBloqueo> personasBloqueadas) {
        this.personasBloqueadas = personasBloqueadas;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public boolean isTipoBloqueoCargar() {
        return tipoBloqueoCargar;
    }

    public void setTipoBloqueoCargar(boolean tipoBloqueoCargar) {
        this.tipoBloqueoCargar = tipoBloqueoCargar;
    }

    public String getTiposDeArchivoValidos() {
        return tiposDeArchivoValidos;
    }

    public void setTiposDeArchivoValidos(String tiposDeArchivoValidos) {
        this.tiposDeArchivoValidos = tiposDeArchivoValidos;
    }

    public String getMensajeCargaArchivo() {
        return mensajeCargaArchivo;
    }

    public void setMensajeCargaArchivo(String mensajeCargaArchivo) {
        this.mensajeCargaArchivo = mensajeCargaArchivo;
    }

    public String getMotivoBloqueo() {
        return motivoBloqueo;
    }

    public void setMotivoBloqueo(String motivoBloqueo) {
        this.motivoBloqueo = motivoBloqueo;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Date getFechaRecibido() {
        return fechaRecibido;
    }

    public void setFechaRecibido(Date fechaRecibido) {
        this.fechaRecibido = fechaRecibido;
    }

    public String getMensajeSalidaConfirmationDialog() {
        return mensajeSalidaConfirmationDialog;
    }

    public void setMensajeSalidaConfirmationDialog(
        String mensajeSalidaConfirmationDialog) {
        this.mensajeSalidaConfirmationDialog = mensajeSalidaConfirmationDialog;
    }

    public boolean isTipoBloqueo0() {
        return tipoBloqueo0;
    }

    public void setTipoBloqueo0(boolean tipoBloqueo0) {
        this.tipoBloqueo0 = tipoBloqueo0;
    }

    public boolean isTipoBloqueo2() {
        return tipoBloqueo2;
    }

    public void setTipoBloqueo2(boolean tipoBloqueo2) {
        this.tipoBloqueo2 = tipoBloqueo2;
    }

    public int getTipoBloqueo() {
        return tipoBloqueo;
    }

    public void setTipoBloqueo(int tipoBloqueo) {
        this.tipoBloqueo = tipoBloqueo;
    }

    public boolean isBanderaBuscar() {
        return banderaBuscar;
    }

    public void setBanderaBuscar(boolean banderaBuscar) {
        this.banderaBuscar = banderaBuscar;
    }

    public ReporteDTO getReporteBloqueoMasivoPersonas() {
        return reporteBloqueoMasivoPersonas;
    }

    public void setReporteBloqueoMasivoPersonas(
        ReporteDTO reporteBloqueoMasivoPersonas) {
        this.reporteBloqueoMasivoPersonas = reporteBloqueoMasivoPersonas;
    }

    public String getSelectedTipoBloqueo() {
        return selectedTipoBloqueo;
    }

    public void setSelectedTipoBloqueo(String selectedTipoBloqueo) {
        this.selectedTipoBloqueo = selectedTipoBloqueo;
    }

    public List<SelectItem> getMunicipiosBuscadorItemList() {
        return municipiosBuscadorItemList;
    }

    public void setMunicipiosBuscadorItemList(
        List<SelectItem> municipiosBuscadorItemList) {
        this.municipiosBuscadorItemList = municipiosBuscadorItemList;
    }

    public List<SelectItem> getTiposBloqueoItemList() {
        return tiposBloqueoItemList;
    }

    public void setTiposBloqueoItemList(List<SelectItem> tiposBloqueoItemList) {
        this.tiposBloqueoItemList = tiposBloqueoItemList;
    }

    public String getSelectedDepartamento() {
        return selectedDepartamento;
    }

    public void setSelectedDepartamento(String selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public String getSelectedMunicipio() {
        return selectedMunicipio;
    }

    public void setSelectedMunicipio(String selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public boolean isSelectedRestitucionTierras() {
        return selectedRestitucionTierras;
    }

    public void setSelectedRestitucionTierras(boolean selectedRestitucionTierras) {
        this.selectedRestitucionTierras = selectedRestitucionTierras;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public List<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }
        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public Boolean getErroresPersonas() {
        return erroresPersonas;
    }

    public void setErroresPersonas(Boolean erroresPersonas) {
        this.erroresPersonas = erroresPersonas;
    }

    public Boolean getBloquearPersonas() {
        return bloquearPersonas;
    }

    public void setBloquearPersonas(Boolean bloquearPersonas) {
        this.bloquearPersonas = bloquearPersonas;
    }

    public DefaultStreamedContent getExportFile() {

        try {

            String excelFileName = "reporte.csv";
            FileOutputStream fos = new FileOutputStream(excelFileName);
            wb.write(fos);
            fos.flush();
            fos.close();
            InputStream stream = new BufferedInputStream(new FileInputStream(
                excelFileName));
            exportFile = new DefaultStreamedContent(stream, "application/xls",
                excelFileName);

        } catch (IOException eq) {
            LOGGER.error(eq.getMessage(), eq);
        }

        return exportFile;

    }

    public void setExportFile(DefaultStreamedContent exportFile) {
        this.exportFile = exportFile;
    }

    // ----------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("BloqueoPredioMB#init");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.tipoBloqueoCargar = true;
        this.banderaBuscar = false;
        this.tipoBloqueo0 = true;
        this.tipoBloqueo2 = false;
        this.tipoBloqueo = 0;
        this.banderaBotonGenerarInforme = false;
        this.banderaBotonGuardar = false;
        this.banderaValidar = false;
        this.rutaMostrar = null;
        this.mensajeCargaArchivo = "Cargar documento";
        this.personasBloqueadas = new ArrayList<PersonaBloqueo>();
        this.personaBloqueo = new PersonaBloqueo();
        this.aSoporte = 0;
        this.aBMasa = 0;
        this.tipoPersonaN = true;
        this.tipoPersonaJ = false;
        this.laBandera = false;

        this.personaTiposIdentificacion = new ArrayList<SelectItem>();
        this.personaTiposIdentificacion = this.generalService
            .getPersonaTipoIdentificacionV();
        this.fechaRecibido = generalService.getFechaActual();

        Persona persona = new Persona();
        this.personaBloqueo.setPersona(persona);
        this.personaBloqueo.getPersona().setTipoIdentificacion(
            EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());

        this.mensajeSalidaConfirmationDialog = "La marcación" +
             " se realizó exitosamente." + " Desea continuar" +
             " en el módulo de marcar personas?";

        this.tiposBloqueoItemList = new ArrayList<SelectItem>();

        List<Dominio> dominio = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_TIPO_BLOQUEO);

        this.tiposBloqueoItemList.add(new SelectItem(null, DEFAULT_COMBOS));

        for (Dominio dom : dominio) {
            this.tiposBloqueoItemList.add(new SelectItem(dom.getCodigo(), dom
                .getValor()));

        }

        this.erroresPersonas = false;
        this.bloquearPersonas = true;

        this.cargarDepartamentos();

        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el boton Buscar
     */
    public void buscar() {
        this.banderaBuscar = true;
        if (this.tipoIdentificacion != null &&
             !this.tipoIdentificacion.trim().isEmpty() &&
             this.numeroIdentificacion != null &&
             !this.numeroIdentificacion.trim().isEmpty()) {

            this.filtroCPersonaB = new FiltroDatosConsultaPersonasBloqueo(
                this.tipoIdentificacion, this.numeroIdentificacion);

            this.personasBloqueadas = this.getConservacionService()
                .buscarPersonasBloqueo(this.filtroCPersonaB);
            LOGGER.debug("poblo " + banderaBuscar);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de bloqueo
     */
    public void bloquearV() {

        if (tipoBloqueo == 0) {
            this.tipoBloqueo0 = true;
            this.tipoBloqueo2 = false;
            this.mensajeCargaArchivo = "Cargar documento";
            this.laBandera = false;
            this.banderaValidar = false;
        } else if (tipoBloqueo == 2) {
            this.tipoBloqueo0 = false;
            this.tipoBloqueo2 = true;
            this.mensajeCargaArchivo = "Cargar archivo";
            this.banderaValidar = true;
            this.laBandera = false;
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Accion ejecutada sobre el radio button asociado al tipo de persona
     */
    public void bloquearTipoPersona() {

        if (this.personaBloqueo.getPersona() == null) {
            this.personaBloqueo = new PersonaBloqueo();
            this.personaBloqueo.setPersona(new Persona());
        }
        if (this.tipoPersona == 0) {
            this.personaBloqueo.getPersona().setTipoIdentificacion(
                EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo());
            this.tipoPersonaN = true;
            this.tipoPersonaJ = false;
            this.banderaValidar = false;
            this.numeroIdentificacion = "";
        } else if (tipoPersona == 1) {
            this.tipoPersonaN = false;
            this.tipoPersonaJ = true;
            this.banderaValidar = false;
            this.fechaFinal = null;
            this.fechaInicial = null;
            this.personaBloqueo.getPersona().setTipoIdentificacion(
                EPersonaTipoIdentificacion.NIT.getCodigo());
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo que escucha la seleccion de cambio de pestaña
     */
    public void cambioTab(TabChangeEvent eventoCambioPes) {

        if (eventoCambioPes.getTab().getId().equals("bloquearPersonasTab")) {
            this.tipoBloqueoCargar = false;
        } else {
            this.tipoBloqueoCargar = true;
            this.tipoPersona = 0;
        }
        this.personaBloqueo.setPersona(null);
        this.bloquearTipoPersona();
        this.tipoIdentificacion = "";
        this.numeroIdentificacion = "";
        this.fechaFinal = null;
        this.fechaInicial = null;
        this.fechaEnvio = null;
        this.numeroRadicado = "";
        this.personasBloqueadas = new ArrayList<PersonaBloqueo>();
        this.bloquearV();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para guardar los datos de predio bloqueo ejecutado sobre el boton guardar
     */
    public void guardarPB() {

        RequestContext context = RequestContext.getCurrentInstance();
        this.asociarPersonasABloquear();
        Entidad entidad = null;
        Boolean actividad = false;

        if (tipoBloqueo == 0 && this.aSoporte == 1) {

            try {
                List<PersonaBloqueo> resultado = this.getConservacionService()
                    .actualizarPersonasBloqueo(usuario, personasBloqueadas);

                if (resultado.size() != 0) {

                    int contadorErrores = 0;
                    for (PersonaBloqueo pbTemp : resultado) {

                        if (pbTemp.getDocumentoSoporteBloqueo() != null) {

                            if (pbTemp.getDocumentoSoporteBloqueo() != null) {
                                if (pbTemp.getDocumentoSoporteBloqueo()
                                    .getIdRepositorioDocumentos() == null) {
                                    contadorErrores++;
                                    break;
                                }
                            }
                        }
                    }

                    if (contadorErrores == 0) {
                        String mensaje = "La persona seleccionada se marcó exitosamente";
                        this.addMensajeInfo(mensaje);
                    } else if (contadorErrores > 0) {
                        String mensaje = "La persona seleccionada se marcó exitosamente," +
                             " pero se presentaron errores en el servicio de Alfresco" +
                             " y el documento de soporte no pudo ser almacenado";
                        this.addMensajeWarn(mensaje);
                    }

                    if (this.selectedRestitucionTierras == true &&
                         this.selectedTipoBloqueo
                            .equals(EPredioBloqueoTipoBloqueo.SUSPENDER
                                .getCodigo())) {

                        this.tramites = this
                            .getTramiteService()
                            .buscarTramitesTiposTramitesByNumeroIdentificacion(
                                personaBloqueo.getPersona()
                                    .getNumeroIdentificacion(),
                                personaBloqueo.getPersona()
                                    .getTipoIdentificacion());

                        if (this.tramites != null && !this.tramites.isEmpty()) {

                            entidad = this.getConservacionService()
                                .findEntidadById(this.idEntidadBloqueo);
                            actividad = validarSubProcesoValidacion(
                                entidad.getNombre(), entidad.getCorreo());

                            if (actividad == false) {
                                bloqueoProcesos(personaBloqueo,
                                    entidad.getNombre(),
                                    entidad.getCorreo());
                            }
                        }
                    }

                    this.banderaBotonGuardar = false;
                    this.banderaBotonGenerarInforme = true;

                } else if (resultado.size() == 0) {
                    String mensaje =
                        "Los datos de la persona, ingresados no arrojaron ningún resultado";
                    this.addMensajeError(mensaje);
                    context.addCallbackParam("error", "error");
                }

            } catch (Exception e) {
                e.getMessage();
                String mensaje = "La persona seleccionada no existe o no pudo ser marcada";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        } else if (this.tipoBloqueo == 0 && this.aSoporte == 0) {
            String mensaje = "No se adjuntó un documento soporte para la marcación";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    /**
     * Método que realiza una búsqueda de las personas a bloquear basandose en la persona que se
     * tiene seleccionada, con el fin de bloquar todos los registros asociados a la misma persona.
     */
    public void asociarPersonasABloquear() {
        RequestContext context = RequestContext.getCurrentInstance();

        try {
            List<Persona> personasABloquear = this.getConservacionService()
                .buscarPersonaPorNumeroYTipoIdentificacion(
                    this.personaBloqueo.getPersona()
                        .getTipoIdentificacion(),
                    this.personaBloqueo.getPersona()
                        .getNumeroIdentificacion());

            Municipio m = new Municipio();
            m.setCodigo(this.selectedMunicipio);

            Departamento d = new Departamento();
            d.setCodigo(this.selectedDepartamento);

            // Set de los campos del bloqueo.
            this.personaBloqueo.setFechaInicioBloqueo(this.fechaInicial);
            this.personaBloqueo.setFechaTerminaBloqueo(this.fechaFinal);
            this.personaBloqueo.setFechaRecibido(this.fechaRecibido);
            this.personaBloqueo.setMotivoBloqueo(this.motivoBloqueo);
            this.personaBloqueo
                .setDocumentoSoporteBloqueo(this.documentoSoporteBloqueo);

            this.personaBloqueo.setFechaEnvio(this.fechaEnvio);
            this.personaBloqueo.setNumeroRadicacion(this.numeroRadicado);
            this.personaBloqueo.setMunicipio(m);
            this.personaBloqueo.setDepartamento(d);
            this.personaBloqueo.setTipoBloqueo(this.selectedTipoBloqueo);

            if (this.selectedRestitucionTierras == true) {
                this.personaBloqueo.setRestitucionTierras("SI");
            } else {
                this.personaBloqueo.setRestitucionTierras("NO");
            }

            if (this.idEntidadBloqueo != null) {
                this.personaBloqueo.setEntidadId(this.idEntidadBloqueo);
            }

            // Se clona la personaBloqueo y se adiciona a cada una la persona
            // asociada, que tiene los mismos registros reelevantes pero un
            // diferente id.
            if (personasABloquear != null && !personasABloquear.isEmpty()) {
                for (Persona p : personasABloquear) {
                    PersonaBloqueo pb = (PersonaBloqueo) this.personaBloqueo
                        .clone();
                    pb.setPersona(p);
                    this.personasBloqueadas.add(pb);
                    if (this.selectedRestitucionTierras == true && this.personaBloqueo.
                        getTipoBloqueo().equals(EPredioBloqueoTipoBloqueo.SUSPENDER.getCodigo())) {
                        List<PersonaBloqueo> bloqueos = this.getConservacionService().
                            obtenerPersonaBloqueosPorPersonaId(p.getId());
                        int contAlertas = 0;
                        for (final PersonaBloqueo pbloqueo : bloqueos) {
                            if (!(pbloqueo.getTipoBloqueo() != null &&
                                pbloqueo.getTipoBloqueo().equals(EPredioBloqueoTipoBloqueo.ALERTAR.
                                    getCodigo()) &&
                                 pbloqueo.getRestitucionTierras() != null && pbloqueo.
                                getRestitucionTierras().equals(ESiNo.SI.getCodigo()))) {
                                PersonaBloqueo pb2 = (PersonaBloqueo) pb.clone();
                                pb2.setTipoBloqueo(EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo());
                                this.personasBloqueadas.add(pb2);
                                contAlertas++;
                            }
                        }
                        if (contAlertas <= 0) {
                            PersonaBloqueo pb2 = (PersonaBloqueo) pb.clone();
                            pb2.setTipoBloqueo(EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo());
                            this.personasBloqueadas.add(pb2);
                        }

                    }

                }
            }

            //Si es restitucion de tierras, se debe buscar si hay un PersonaBloqueo 
            //en alertar y restitucion de tierras en si, si no lo hay, crear un nuevo personabloqueo e
            //temporal y agregar a personas a bloquear
        } catch (Exception e) {
            context.addCallbackParam("error", "error");
            LOGGER.error("Error al asociar las personas que se quieren marcár.");
            e.getMessage();
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de soporte a cargar
     */
    @SuppressWarnings("deprecation")
    private void nuevoDocumento() {

        TipoDocumento tipoDocumento = new TipoDocumento();

        tipoDocumento.setId(ETipoDocumentoId.DOC_SOPORTE_BLOQUEO_PERSONA
            .getId());

        this.documentoSoporteBloqueo.setFechaDocumento(new Date());
        this.documentoSoporteBloqueo.setEstado(EDocumentoEstado.RECIBIDO
            .getCodigo());
        this.documentoSoporteBloqueo.setBloqueado(ESiNo.NO.getCodigo());

        this.documentoSoporteBloqueo.setTipoDocumento(tipoDocumento);
        this.documentoSoporteBloqueo.setUsuarioCreador(this.usuario.getLogin());
        this.documentoSoporteBloqueo.setUsuarioLog(this.usuario.getLogin());
        this.documentoSoporteBloqueo.setFechaLog(new Date());

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo que recupera los datos del documento de bloqueo masivo a cargar
     */
    @SuppressWarnings("deprecation")
    private void nuevoDocumentoAnexo() {

        TipoDocumento tipoDocumento = new TipoDocumento();
        this.documentoArchivoAnexo = new DocumentoArchivoAnexo();

        tipoDocumento
            .setId(ETipoDocumentoId.DOC_BLOQUEO_MASIVO_PERSONA.getId());

        this.documentoArchivoAnexo
            .setDescripcion(ETipoDocumentoId.DOC_BLOQUEO_MASIVO_PERSONA
                .toString());
        this.documentoArchivoAnexo.setDocumento(this.documentoSoporteBloqueo);
        this.documentoArchivoAnexo
            .setEstado(EDocumentoArchivoAnexoEstado.CARGADO.getCodigo());
        this.documentoArchivoAnexo.setFechaLog(new Date());
        this.documentoArchivoAnexo.setUsuarioLog(this.usuario.getLogin());

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivos
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        FileOutputStream fileOutputStream = null;

        try {
            this.archivoResultado = new File(FileUtils.getTempDirectory()
                .getAbsolutePath(), eventoCarga.getFile().getFileName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoResultado);

            byte[] buffer = new byte[Math
                .round(eventoCarga.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            nuevoDocumento();
            this.documentoSoporteBloqueo.setArchivo(eventoCarga.getFile()
                .getFileName());

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;

            String mensaje = "El documento se cargó adecuadamente.";
            this.addMensajeInfo(mensaje);

        } catch (IOException e) {
            LOGGER.error("ERROR: " + e);
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo ejecutado sobre el boton cerrar
     */
    public void cerrarGestionArchivo() {

        this.aBMasa = 0;
        this.aSoporte = 0;
        this.numeroIdentificacion = "";
        this.personaBloqueo = new PersonaBloqueo();
        this.fechaFinal = null;
        this.fechaInicial = null;
        this.tipoPersona = 0;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado al cerrar las ventanas modales
     */
    public void cerrarEsc(org.primefaces.event.CloseEvent event) {

        this.aBMasa = 0;
        this.aSoporte = 0;
        this.numeroIdentificacion = "";
        this.personaBloqueo = new PersonaBloqueo();
        this.fechaFinal = null;
        this.fechaInicial = null;
        this.fechaRecibido = null;
        this.tipoPersona = 0;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo utilizado para validar los datos de la forma
     */
    public void validar() {

        List<Persona> personaEncontrada = null;

        personaEncontrada = this.getConservacionService()
            .buscarPersonaPorNombrePartesTipoDocNumDocTemp(
                this.personaBloqueo.getPersona());

        this.laBandera = true;
        this.banderaValidar = true;

        if (personaEncontrada != null && personaEncontrada.size() < 1) {
            this.laBandera = false;
            this.banderaValidar = false;
            String mensaje = "No existen personas coincidentes con los parámetros ingresados." +
                 " Por favor corrija los datos e intente nuevamente ";
            this.addMensajeError(mensaje);
        }

        if (this.fechaFinal != null &&
             this.fechaFinal.before(this.fechaInicial)) {
            this.banderaValidar = false;
            String mensaje = "La fecha final no puede ser anterior a la fecha inicial!";
            this.addMensajeError(mensaje);
            this.laBandera = false;
        }

        if (this.fechaEnvio != null && this.fechaEnvio.after(new Date())) {
            this.banderaValidar = false;
            String mensaje = "Fecha envío no puede ser mayor a la fecha actual, verifique!";
            this.addMensajeError(mensaje);
            this.laBandera = false;
        }

        if (this.fechaRecibido != null && this.fechaRecibido.after(new Date())) {
            this.banderaValidar = false;
            String mensaje = "La fecha de recibido no puede ser mayor a la fecha actual, verifique!";
            this.addMensajeError(mensaje);
            this.laBandera = false;
        }

        if (this.fechaRecibido != null && this.fechaEnvio != null &&
             this.fechaEnvio.after(this.fechaRecibido)) {
            this.banderaValidar = false;
            String mensaje =
                "La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique!";
            this.addMensajeError(mensaje);
            this.laBandera = false;
        }
        
        if (usuario != null && !usuario.getDescripcionEstructuraOrganizacional().contains(Constantes.PREFIJO_HABILITADOS)) {
            if (this.numeroRadicado != null
                    && !this.getConservacionService().validarNumeroRadicacion(
                            this.numeroRadicado, this.usuario)) {
                this.banderaValidar = false;
                String mensaje = "El Número de radicación  no es válido, verifique!";
                this.addMensajeError(mensaje);
                this.laBandera = false;
            }
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * metodo validador de carga de archivo de bloqueo en masa
     */
    public void archivoVM(FileUploadEvent eventoMasa) {

        FileOutputStream fileOutputStream = null;
        this.documentoTemporalBloqueoMasivo = new Documento();
        this.nombreDocumentoBloqueoMasivo = eventoMasa.getFile().getFileName();

        this.documentoTemporalBloqueoMasivo
            .setArchivo(this.nombreDocumentoBloqueoMasivo);

        this.erroresPersonas = false;

        try {
            this.archivoMasivoResultado = new File(FileUtils.getTempDirectory()
                .getAbsolutePath(), this.nombreDocumentoBloqueoMasivo);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        try {
            fileOutputStream = new FileOutputStream(this.archivoMasivoResultado);

            byte[] buffer = new byte[Math.round(eventoMasa.getFile().getSize())];

            int bulk;
            InputStream inputStream = eventoMasa.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            inputStream.close();

            this.banderaBotonGuardar = true;
            this.aSoporte = 1;

            this.lecturaArchivoMasivo(eventoMasa);

        } catch (IOException e) {
            LOGGER.error("ERROR: " + e);
            String mensaje = "Los archivos seleccionados no pudieron ser cargados";
            this.addMensajeError(mensaje);
        }
    }

    /**
     * Método de lectura del archivo de bloqueo masivo
     *
     * @modified by leidy.gonzalez 25/07/2014 Se elimina validación de longitud debido a que cuando
     * el documento no encuentra personas, la variable partes quedaria nula y siempre enviara la
     * excepción. Se valida el formato para la juridica.
     */
    @SuppressWarnings("rawtypes")
    private void lecturaArchivoMasivo(FileUploadEvent eventoMasa) {

        String sCadena;
        String separador = Constantes.CADENA_SEPARADOR_ARCHIVO_TEXTO_PLANO;
        this.bloqueoMasivo = new ArrayList<PersonaBloqueo>();
        Persona personaTemp;
        Entidad entidad = null;
        PersonaBloqueo pb;
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaTemp;
        boolean formatoIncorrecto;
        Object part[] = null;
        String mensaje = "";
        Date fechatemporal1;
        Date fechatemporal2;
        Row fila = null;
        Cell celda = null;
        wb = new HSSFWorkbook();
        HSSFCellStyle styleHeader = (HSSFCellStyle) wb.createCellStyle();
        HSSFFont fontHeader = (HSSFFont) wb.createFont();
        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHeader.setFont(fontHeader);
        Sheet sheetReporte = wb.createSheet("sheet");
        int columnaReporte = 0;
        int filaReporte = 0;
        Pattern p = Pattern.compile("[0-9]*ER[0-9]*");
        Matcher m = null;
        Municipio mun = null;
        Departamento dep = null;
        int posEmpiezanFechas = 0;
        int contadorErrores = 0;
        int rango = 0;

        try {

            if (eventoMasa.getFile().getContentType()
                .equals(EArchivoAnexoFormato.ARCHIVO_PLANO.getValor()) ||
                 eventoMasa
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_PLANO
                            .getExtension())) {

                String partes[];
                BufferedReader bf = new BufferedReader(new FileReader(
                    this.archivoMasivoResultado));

                while ((sCadena = bf.readLine()) != null) {
                    partes = sCadena.split(separador);
                    fila = sheetReporte.createRow((short) filaReporte);

                    if (partes.length > 18 || partes.length < 17) {
                        celda = fila.createCell(columnaReporte);
                        mensaje = "El documento " +
                             partes[1] +
                             " no tiene el número de columnas requeridas, verifique";
                        celda.setCellValue(mensaje);
                        contadorErrores++;

                    } else {

                        if (partes.length == 18) {
                            rango = 17;
                        } else {
                            rango = 16;
                        }

                        for (int x = 0; x <= rango; x++) {

                            if (partes[x].isEmpty()) {
                                switch (x) {
                                    case 0:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrado un tipo de documento valido, verifique";
                                        break;
                                    case 1:
                                        mensaje =
                                            "Un documento no se encuentra registrado, verifique";
                                        break;
                                    case 2:
                                        mensaje = "El documento " +
                                             partes[1] +
                                             " no tiene registrado un DV valido, verifique";
                                        break;
                                    case 8:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrado un departamento valido, verifique";
                                        break;
                                    case 9:
                                        mensaje = "El documento " +
                                             partes[1] +
                                             " no tiene registrado un municipio valido, verifique";
                                        break;
                                    case 10:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrado una entidad ordena la marcación valido, verifique";
                                        break;
                                    case 11:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrado un valor de restitución de tierras valido, verifique";
                                        break;
                                    case 12:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrado un motivo de marcación valido, verifique";
                                        break;
                                    case 13:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrada una fecha inicial valida, verifique";
                                        break;
                                    case 14:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrada una fecha envió valida, verifique";
                                        break;
                                    case 15:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene registrada una fecha recibido valida, verifique";
                                        break;
                                    case 16:
                                        mensaje = "El documento " +
                                             partes[1] +
                                            
                                            " no tiene un formato número de radicación valido, verifique";
                                        break;
                                    default:
                                        mensaje = "";
                                        break;
                                }

                                if (!"".equals(mensaje)) {
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }
                            }

                            if (!partes[x].isEmpty() &&
                                 (x == 13 || x == 14 || x == 15 || x == 17)) {

                                try {
                                    if (!partes[x].isEmpty()) {
                                        formatoFecha.parse(partes[x]);
                                    }
                                } catch (Exception e) {
                                    partes[x] = "ERROR";
                                }
                            }
                        }// FOR

                        if (!partes[7].isEmpty() &&
                             !(EPredioBloqueoTipoBloqueo.ALERTAR
                                .getCodigo().equals(partes[7])) &&
                             !(EPredioBloqueoTipoBloqueo.SUSPENDER
                                .getCodigo().equals(partes[7]))) {
                            mensaje = "El documento " +
                                 partes[1] +
                                 " el tipo de marcación no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[8].isEmpty() && (partes[8].length()) != 2) {
                            mensaje = "Para el documento " +
                                 partes[1] +
                                 ", el código de departamento no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else if (!partes[8].isEmpty() &&
                             (partes[8].length()) == 2) {
                            if (this.getConservacionService()
                                .validaExistenciaEntidad(null,
                                    EEntidadEstado.ACTIVO.getEstado(),
                                    partes[8], null) == false) {
                                mensaje = "Para el documento " +
                                     partes[1] +
                                    
                                    ", el departamento no tiene entidades que puedan realizar la marcación de predios, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (!partes[9].isEmpty() && partes[9].length() != 3) {

                            mensaje = "Para el documento " +
                                 partes[1] +
                                 " el código de municipio no tiene un formato válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;

                        } else if (!partes[8].isEmpty() &&
                             (partes[8].length()) == 2) {

                            if (this.getGeneralesService()
                                .validaMunicipioDepartamento(partes[8].toString().trim(), partes[8].
                                    toString().trim().concat(partes[9].toString().trim())) == false) {
                                mensaje = "Para el documento " +
                                     partes[1] +
                                    
                                    " el código de municipio no corresponde al departamento asociado";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            } else {
                                String codMun = (partes[9].toString().trim().length() == 3) ?
                                    partes[8] + partes[9] : partes[9];
                                if (this.getConservacionService()
                                    .validaExistenciaEntidad(
                                        null,
                                        EEntidadEstado.ACTIVO
                                            .getEstado(),
                                        partes[8], codMun) == false) {
                                    mensaje = " Para el documento " +
                                         partes[1] +
                                        
                                        ", el municipio no tiene entidades que puedan realizar la marcación de predios, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                } else if (!partes[10].isEmpty() &&
                                     this.getConservacionService()
                                        .validaExistenciaEntidad(
                                            partes[10],
                                            EEntidadEstado.ACTIVO
                                                .getEstado(),
                                            partes[8], codMun) == false) {
                                    mensaje = " La entidad registrada para el documento " +
                                         partes[1] +
                                        
                                        ", no está parametrizada para realizar la marcación de predios, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }
                            }
                        }

                        if (!partes[11].isEmpty() && !"SI".equals(partes[11]) &&
                             !"NO".equals(partes[11])) {
                            mensaje = "El documento " +
                                 partes[1] +
                                
                                " el dato restitución de tierras no tiene un formato válido, tiene registrado un valor diferente a SI o NO, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }

                        if (!partes[14].isEmpty() &&
                             !"ERROR".equals(partes[14])) {

                            fechatemporal1 = formatoFecha.parse(partes[14]);
                            fechatemporal2 = new Date();

                            if (fechatemporal1.after(fechatemporal2)) {
                                mensaje = "El documento " +
                                     partes[1] +
                                    
                                    " La fecha de envio no puede ser mayor a la fecha actual, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (!partes[14].isEmpty() &&
                             !"ERROR".equals(partes[14]) &&
                             !partes[15].isEmpty() &&
                             !"ERROR".equals(partes[15])) {

                            fechatemporal1 = formatoFecha.parse(partes[14]);
                            fechatemporal2 = formatoFecha.parse(partes[15]);

                            if (fechatemporal2.before(fechatemporal1)) {
                                mensaje = "El documento " +
                                     partes[1] +
                                    
                                    " La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                       /* if (partes[16] != null && !(partes[16].toString().isEmpty()) &&
                             !this.getConservacionService().validarNumeroRadicacion(partes[16].
                                toString().trim(), this.usuario)) {
                            mensaje = "El documento " +
                                 partes[0] +
                                 " no tiene un formato de número de radicación válido, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else if (!(partes[16].isEmpty()) &&
                             (partes[16].length() == 16 || partes[16]
                            .length() == 18)) {

                            m = p.matcher(partes[16]);
                            if (!m.find()) {
                                mensaje = "El documento " +
                                     partes[1] +
                                    
                                    " tiene  un  formato de radicado que no corresponde a una Externa Recibida, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }*/

                        formatoIncorrecto = validarEstructuraArchivoBloqueoMasivo(partes);

                        if (formatoIncorrecto == false) {

                            mensaje = "El tipo de documento registrado para el documento " +
                                 partes[1] +
                                 ", no está parametrizado en el sistema, verifique";

                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            contadorErrores++;
                        }

                        if (partes[0] != null && !partes[0].isEmpty() && partes[1] != null &&
                            partes[1].isEmpty()) {
                            if (!existePersonaEnBD(partes)) {
                                mensaje = "La persona identificada con documento " + partes[1] +
                                    " No se encuentra registrada en el sistema";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }

                        if (contadorErrores == 0) {

                            pb = new PersonaBloqueo();
                            personaTemp = new Persona();

                            personaTemp.setTipoIdentificacion(partes[0]);
                            personaTemp.setNumeroIdentificacion(partes[1]);

                            if (partes[0]
                                .equalsIgnoreCase(Constantes.TIPO_DOCUMENTO_NIT)) {
                                personaTemp.setRazonSocial(partes[3]);
                                personaTemp.setDigitoVerificacion(partes[2]);

                            } else {
                                if (partes[3]
                                    .equals(Constantes.SEPARADOR_VACIO_DOCUMENTO_BLOQUEO)) {
                                    personaTemp.setPrimerApellido(partes[3]);
                                }

                                if (!partes[4]
                                    .equals(Constantes.SEPARADOR_VACIO_DOCUMENTO_BLOQUEO)) {
                                    personaTemp.setSegundoApellido(partes[4]);
                                }
                                personaTemp.setPrimerNombre(partes[5]);
                                if (!partes[6]
                                    .equals(Constantes.SEPARADOR_VACIO_DOCUMENTO_BLOQUEO)) {
                                    personaTemp.setSegundoNombre(partes[6]);
                                }
                            }

                            if ("-".equals(partes[4])) {
                                personaTemp.setSegundoNombre(null);
                            }
                            if ("-".equals(partes[6])) {
                                personaTemp.setSegundoApellido(null);
                            }
                            if ("-".equals(partes[2])) {
                                personaTemp.setDigitoVerificacion(null);
                            }

                            pb.setPersona(personaTemp);

                            pb.setTipoBloqueo(partes[7]);

                            dep = new Departamento();
                            dep.setCodigo(partes[8]);
                            mun = new Municipio();
                            mun.setCodigo(partes[8].trim().concat(partes[9].trim()));

                            pb.setRestitucionTierras(partes[11]);

                            // Validación de la entidad ingresada
                            entidad = this.validarEntidadBloqueo(partes[1],
                                partes[10]);

                            pb.setEntidadId(entidad.getId());

                            pb.setMotivoBloqueo(partes[12]);

                            pb.setFechaInicioBloqueo(formatoFecha
                                .parse(partes[13]));
                            pb.setFechaEnvio(formatoFecha.parse(partes[14]));
                            pb.setFechaRecibido(formatoFecha.parse(partes[15]));

                            pb.setNumeroRadicacion(partes[16]);

                            if (partes.length > 17) {
                                pb.setFechaTerminaBloqueo(formatoFecha
                                    .parse(partes[17]));
                            }

                            pb.setDepartamento(dep);
                            pb.setMunicipio(mun);

                            nuevoDocumentoAnexo();
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_PLANO
                                    .getCodigo());
                            this.documentoArchivoAnexo.setArchivo(eventoMasa
                                .getFile().getFileName());

                            this.bloqueoMasivo.add(pb);
                        }

                    }

                    if (contadorErrores != 0 && this.erroresPersonas == false) {
                        this.erroresPersonas = true;
                        filaReporte++;
                    } else if (contadorErrores != 0) {
                        filaReporte++;
                    }

                    contadorErrores = 0;
                    columnaReporte = 0;
                }

                if (this.erroresPersonas == false) {
                    mensaje = "El documento de marcación masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                    this.aBMasa = 1;
                    bloquearPersonas = true;
                } else {
                    mensaje =
                        "El formato del documento soporte de marcación no es válido, verifique.";
                    this.addMensajeError(mensaje);
                    this.aBMasa = 0;
                    bloquearPersonas = false;
                }

            } else if (eventoMasa.getFile().getContentType()
                .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS.getValor()) ||
                 eventoMasa
                    .getFile()
                    .getContentType()
                    .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                        .getValor()) ||
                 eventoMasa
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                            .getExtension()) ||
                 eventoMasa
                    .getFile()
                    .getFileName()
                    .endsWith(
                        EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                            .getExtension())) {

                FileInputStream fis = new FileInputStream(
                    this.archivoMasivoResultado);
                XSSFWorkbook workbook = new XSSFWorkbook(fis);
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rowsX = sheet.rowIterator();

                contadorErrores = 0;

                while (rowsX.hasNext()) {
                    XSSFRow row = (XSSFRow) rowsX.next();
                    int c = 0;
                    part = new Object[18];

                    fila = sheetReporte.createRow((short) filaReporte);

                    posEmpiezanFechas = 0;

                    for (short colIndex = 0; colIndex <= 17; colIndex++) {

                        XSSFCell cell = row.getCell(colIndex);

                        if (posEmpiezanFechas != 13 &&
                             posEmpiezanFechas != 14 &&
                             posEmpiezanFechas != 15 &&
                             posEmpiezanFechas != 17 &&
                             cell != null &&
                             cell.getCellType() != XSSFCell.CELL_TYPE_BLANK) {
                            try {
                                part[c] = cell.getStringCellValue();
                                if (part[c] != null) {
                                    part[c] = part[c].toString().trim();
                                }
                            } catch (Exception ex) {

                                part[c] = null;
                                switch (c) {
                                    case 3:
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                            
                                            " el  primer apellido registrado/razón social no es valido, verifique";
                                        break;
                                    case 4:
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                            
                                            " el segundo apellido registrado no es valido, verifique";
                                        break;
                                    case 5:
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                             " el primer nombre registrado no es valido, verifique";
                                        break;
                                    case 6:
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                             " el segundo nombre registrado no es valido, verifique";
                                        break;
                                    default:
                                        mensaje = "";
                                        break;
                                }

                                if (!"".equals(mensaje)) {
                                    part[c] = "ERROR";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }
                            }
                        } else {
                            try {

                                if ((cell != null && posEmpiezanFechas != 17) ||
                                     (cell != null &&
                                     posEmpiezanFechas == 17 && cell
                                        .getCellType() != XSSFCell.CELL_TYPE_BLANK)) {

                                    if (cell.getCellType() == 0) {
                                        part[c] = cell.getDateCellValue();
                                    } else {
                                        part[c] = cell;
                                        fechaTemp = formatoFecha.parse(part[c]
                                            .toString());
                                        part[c] = fechaTemp;
                                    }
                                }

                            } catch (Exception e) {

                                if (posEmpiezanFechas == 17) {
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                         " no tiene registrada una fecha final valida, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }

                                if (posEmpiezanFechas == 7) {
                                    mensaje = "El documento " +
                                         part[0].toString() +
                                        
                                        " no tiene registrado un tipo de marcación valido, verifique";
                                    celda = fila.createCell(columnaReporte);
                                    celda.setCellValue(mensaje);
                                    columnaReporte++;
                                    contadorErrores++;
                                }

                                part[c] = null;
                            }
                        }

                        if ((part[c] == null || part[c].toString().isEmpty()) &&
                             posEmpiezanFechas != 17 &&
                             posEmpiezanFechas != 7) {
                            switch (c) {
                                case 0:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                        
                                        " no tiene registrado un tipo de documento valido, verifique";
                                    break;
                                case 1:
                                    mensaje = "Un documento no se encuentra registrado, verifique";
                                    break;
                                case 8:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                         " no tiene registrado un departamento valido, verifique";
                                    break;
                                case 9:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                         " no tiene registrado un municipio valido, verifique";
                                    break;
                                case 10:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                        
                                        " no tiene registrado una entidad ordena bloqueo valido, verifique";
                                    break;
                                case 11:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                        
                                        " no tiene registrado un valor de restitución de tierras valido, verifique";
                                    break;
                                case 12:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                        
                                        " no tiene registrado un motivo de marcación valido, verifique";
                                    break;
                                case 13:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                         " no tiene registrada una fecha inicial valida, verifique";
                                    break;
                                case 14:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                         " no tiene registrada una fecha envió valida, verifique";
                                    break;
                                case 15:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                         " no tiene registrada una fecha recibido valida, verifique";
                                    break;
                                case 16:
                                    mensaje = "El documento " +
                                         part[1].toString() +
                                        
                                        " no tiene un formato número de radicación valido, verifique";
                                    break;
                                default:
                                    mensaje = "";
                            }

                            if (part[0] != null &&
                                 !part[0].toString().isEmpty() &&
                                 (EPersonaTipoIdentificacion.NIT
                                    .toString().equals(part[0]
                                        .toString())) &&
                                 (posEmpiezanFechas == 3 ||
                                 posEmpiezanFechas == 4 ||
                                 posEmpiezanFechas == 5 || posEmpiezanFechas == 6)) {

                                if (part[posEmpiezanFechas] != null && !"ERROR".equals(
                                    part[posEmpiezanFechas].toString())) {

                                    if (posEmpiezanFechas == 3 &&
                                         (part[posEmpiezanFechas] == null || "-"
                                            .equals(part[posEmpiezanFechas]
                                                .toString()))) {
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                            
                                            " no tiene registrado una razón social  valida, verifique";
                                    }

                                    if (posEmpiezanFechas == 4 &&
                                         !(part[posEmpiezanFechas] == null) &&
                                         !"-".equals((part[posEmpiezanFechas]
                                            .toString()))) {
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                            
                                            " el campo segundo apellido debe estar vacio, verifique";
                                    }

                                    if (posEmpiezanFechas == 5 &&
                                         !(part[posEmpiezanFechas] == null) &&
                                         !("-".equals(part[posEmpiezanFechas]
                                            .toString()))) {
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                             " el campo primer nombre debe estar vacio, verifique";
                                    }

                                    if (posEmpiezanFechas == 6 &&
                                         !(part[posEmpiezanFechas] == null) &&
                                         !("-".equals(part[posEmpiezanFechas]
                                            .toString()))) {
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                             " el campo segundo nombre debe estar vacio, verifique";
                                    }
                                }
                            } else if (part[0] != null &&
                                 !part[0].toString().isEmpty() &&
                                 !(EPersonaTipoIdentificacion.NIT
                                    .toString().equals(part[0]
                                        .toString())) &&
                                 (posEmpiezanFechas == 3 || posEmpiezanFechas == 5)) {

                                if (!"ERROR".equals(part[posEmpiezanFechas]
                                    .toString())) {
                                    if (posEmpiezanFechas == 3 &&
                                         (part[posEmpiezanFechas] == null || "-"
                                            .equals(part[posEmpiezanFechas]
                                                .toString()))) {
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                            
                                            " no tiene registrado el primer apellido que sea valido, verifique";
                                    }

                                    if (posEmpiezanFechas == 5 &&
                                         part[posEmpiezanFechas] == null &&
                                         "-".equals(part[posEmpiezanFechas]
                                            .toString())) {
                                        mensaje = "El documento " +
                                             part[1].toString() +
                                            
                                            " no tiene registrado el primer nombre que sea valido, verifique";
                                    }
                                }
                            }

                            if (part[posEmpiezanFechas] != null &&
                                 !part[posEmpiezanFechas].toString()
                                    .isEmpty() &&
                                 "ERROR".equals(part[posEmpiezanFechas]
                                    .toString())) {
                                part[posEmpiezanFechas] = null;
                            }

                            if (!"".equals(mensaje)) {
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }
                        posEmpiezanFechas++;
                        c++;
                    }

                    if (part[7] != null &&
                         !(EPredioBloqueoTipoBloqueo.ALERTAR.getCodigo()
                            .equals(part[7].toString())) &&
                         !(EPredioBloqueoTipoBloqueo.SUSPENDER
                            .getCodigo().equals(part[7].toString()))) {
                        mensaje = "El documento " +
                             part[1].toString() +
                             " el tipo de maracación no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    }

                    if (part[8] != null && (part[8].toString().length()) != 2) {
                        mensaje = "Para el documento " +
                             part[1].toString() +
                             ", el código de departamento no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    } else if (part[8] != null &&
                         (part[8].toString().length()) == 2) {
                        if (this.getConservacionService()
                            .validaExistenciaEntidad(null,
                                EEntidadEstado.ACTIVO.getEstado(),
                                part[8].toString(), null) == false) {
                            mensaje = "Para el documento " +
                                 part[1].toString() +
                                
                                ", el departamento no tiene  entidades que puedan realizar la marcación de predios, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[9] != null && part[9].toString().length() != 3) {

                        mensaje = "Para el documento " +
                             part[1].toString() +
                             " el código de municipio no tiene un formato válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;

                    } else if (part[8] != null &&
                         (part[8].toString().length()) == 2) {

                        if (this.getGeneralesService()
                            .validaMunicipioDepartamento(part[8].toString().trim(), part[8].
                                toString().trim().concat(part[9].toString().trim())) == false) {
                            mensaje = "Para el documento " +
                                 part[1].toString() +
                                 " el código de municipio no corresponde al departamento asociado”";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        } else {
                            String codMun = (part[9].toString().trim().length() == 3) ? part[8].
                                toString() + part[9].toString() : part[9].toString();
                            if (this.getConservacionService()
                                .validaExistenciaEntidad(null,
                                    EEntidadEstado.ACTIVO.getEstado(),
                                    part[8].toString(),
                                    codMun) == false) {
                                mensaje = " Para el documento " +
                                     part[1].toString() +
                                    
                                    ", el municipio no tiene entidades que puedan realizar la marcación de predios, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            } else if (part[10] != null &&
                                 this.getConservacionService()
                                    .validaExistenciaEntidad(
                                        part[10].toString(),
                                        EEntidadEstado.ACTIVO
                                            .getEstado(),
                                        part[8].toString(),
                                        codMun) == false) {
                                mensaje = " La entidad registrada para el documento " +
                                     part[1].toString() +
                                    
                                    ", no está parametrizada para realizar la marcación de predios, verifique";
                                celda = fila.createCell(columnaReporte);
                                celda.setCellValue(mensaje);
                                columnaReporte++;
                                contadorErrores++;
                            }
                        }
                    }

                    if (part[11] != null && !"SI".equals(part[11].toString()) &&
                         !"NO".equals(part[11].toString())) {
                        mensaje = "El documento " +
                             part[1].toString() +
                            
                            " el dato restitución de tierras no tiene un formato válido, tiene registrado un valor diferente a SI o NO, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    }

                    if (part[14] != null) {

                        fechatemporal1 = (Date) part[14];
                        fechatemporal2 = new Date();

                        if (fechatemporal1.after(fechatemporal2)) {
                            mensaje = "El documento " +
                                 part[1].toString() +
                                
                                " La fecha de envio no puede ser mayor a la fecha actual, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[14] != null && part[15] != null) {

                        fechatemporal1 = (Date) part[14];
                        fechatemporal2 = (Date) part[15];

                        if (fechatemporal2.before(fechatemporal1)) {
                            mensaje = "El documento " +
                                 part[1].toString() +
                                
                                " La fecha de recibido debe ser mayor o igual a la fecha de envío, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (part[16] != null && !(part[16].toString().isEmpty()) &&
                         !this.getConservacionService().validarNumeroRadicacion(part[16].toString().
                            trim(), this.usuario)) {
                        mensaje = "El documento " +
                             part[0].toString() +
                             " no tiene un formato de número de radicación válido, verifique";
                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        columnaReporte++;
                        contadorErrores++;
                    } else if (part[16] != null &&
                         !(part[16].toString().isEmpty()) &&
                         (part[16].toString().length() == 16 || part[16]
                        .toString().length() == 18)) {

                        m = p.matcher(part[16].toString());
                        if (!m.find()) {
                            mensaje = "El documento " +
                                 part[1].toString() +
                                
                                " tiene  un  formato de radicado que no corresponde a una Externa Recibida, verifique";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    formatoIncorrecto = validarEstructuraArchivoBloqueoMasivo(part);

                    if (formatoIncorrecto == false) {
                        this.aBMasa = 0;

                        mensaje = "El tipo de documento registrado para el documento " +
                             part[1].toString() +
                             ", no está parametrizado en el sistema, verifique";

                        celda = fila.createCell(columnaReporte);
                        celda.setCellValue(mensaje);
                        contadorErrores++;
                    }

                    if (part[1] != null && part[0] != null) {
                        if (!existePersonaEnBD(part)) {
                            mensaje = "La persona identificada con documento " + part[1] +
                                " No se encuentra registrada en el sistema";
                            celda = fila.createCell(columnaReporte);
                            celda.setCellValue(mensaje);
                            columnaReporte++;
                            contadorErrores++;
                        }
                    }

                    if (contadorErrores == 0) {

                        pb = new PersonaBloqueo();
                        personaTemp = new Persona();

                        personaTemp.setTipoIdentificacion(part[0].toString().trim());
                        personaTemp.setNumeroIdentificacion(part[1].toString().trim());

                        if (part[0].toString().equalsIgnoreCase(
                            Constantes.TIPO_DOCUMENTO_NIT)) {
                            personaTemp.setRazonSocial(part[3].toString().trim());
                            if (part[2] != null) {
                                personaTemp.setDigitoVerificacion(part[2]
                                    .toString().trim());
                            }
                        } else {
                            if (part[3] != null &&
                                 !part[3]
                                    .toString()
                                    .equals(Constantes.SEPARADOR_VACIO_DOCUMENTO_BLOQUEO)) {
                                personaTemp.setPrimerApellido(part[3]
                                    .toString().trim());
                            }

                            if (part[4] != null &&
                                 !part[4]
                                    .toString()
                                    .equals(Constantes.SEPARADOR_VACIO_DOCUMENTO_BLOQUEO)) {
                                personaTemp.setSegundoApellido(part[4]
                                    .toString().trim());
                            }
                            personaTemp.setPrimerNombre(part[5].toString().trim());
                            if (part[6] != null &&
                                 !part[6]
                                    .toString()
                                    .equals(Constantes.SEPARADOR_VACIO_DOCUMENTO_BLOQUEO)) {
                                personaTemp
                                    .setSegundoNombre(part[6].toString().trim());
                            }
                        }

                        if ("-".equals(part[4])) {
                            personaTemp.setSegundoNombre(null);
                        }
                        if ("-".equals(part[6])) {
                            personaTemp.setSegundoApellido(null);
                        }
                        if ("-".equals(part[2])) {
                            personaTemp.setDigitoVerificacion(null);
                        }

                        pb.setPersona(personaTemp);

                        pb.setTipoBloqueo(part[7].toString().trim());

                        dep = new Departamento();
                        dep.setCodigo(part[8].toString().trim());
                        mun = new Municipio();
                        mun.setCodigo(part[8].toString().concat(part[9].toString().trim()));
                        pb.setRestitucionTierras(part[11].toString().trim());

                        // Validación de la entidad ingresada
                        entidad = this.validarEntidadBloqueo(
                            part[1].toString(), part[10].toString().trim());

                        pb.setEntidadId(entidad.getId());
                        pb.setMotivoBloqueo(part[12].toString().trim());
                        pb.setFechaInicioBloqueo((Date) part[13]);
                        pb.setFechaEnvio((Date) part[14]);
                        pb.setFechaRecibido((Date) part[15]);

                        pb.setNumeroRadicacion(part[16].toString().trim());

                        if (part[17] != null) {
                            pb.setFechaTerminaBloqueo((Date) part[17]);
                        }

                        pb.setDepartamento(dep);
                        pb.setMunicipio(mun);

                        nuevoDocumentoAnexo();
                        this.documentoArchivoAnexo.setFormato(eventoMasa
                            .getFile().getContentType());
                        if (eventoMasa
                            .getFile()
                            .getContentType()
                            .equals(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                .getValor()) ||
                             eventoMasa
                                .getFile()
                                .getFileName()
                                .endsWith(
                                    EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                        .getExtension())) {
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLS
                                    .getCodigo());
                        } else {
                            this.documentoArchivoAnexo
                                .setFormato(EArchivoAnexoFormato.ARCHIVO_EXCEL_XLSX
                                    .getCodigo());
                        }
                        this.documentoArchivoAnexo.setArchivo(eventoMasa
                            .getFile().getFileName());

                        this.bloqueoMasivo.add(pb);

                    }

                    if (contadorErrores != 0 && this.erroresPersonas == false) {
                        this.erroresPersonas = true;
                        filaReporte++;
                    } else if (contadorErrores != 0) {
                        filaReporte++;
                    }

                    contadorErrores = 0;
                    columnaReporte = 0;
                }

                if (this.erroresPersonas == false) {
                    mensaje = "El documento de marcación masivo se cargó adecuadamente.";
                    this.addMensajeInfo(mensaje);
                    this.aBMasa = 1;
                    bloquearPersonas = true;
                } else {
                    mensaje =
                        "Se requiere realizar algunos ajustes al documento de marcación masivo, por favor verifique el reporte.";
                    this.addMensajeWarn(mensaje);
                    this.aBMasa = 0;
                    bloquearPersonas = false;
                }
            }

            String excelFileName = "reporte.csv";
            FileOutputStream fos = new FileOutputStream(excelFileName);
            wb.write(fos);
            fos.flush();
            fos.close();
            InputStream stream = new BufferedInputStream(new FileInputStream(
                excelFileName));
            exportFile = new DefaultStreamedContent(stream, "application/xls",
                excelFileName);
            exportFile = new DefaultStreamedContent(stream, "application/xls",
                excelFileName);

        } catch (IOException e) {
            LOGGER.error("ERROR: " + e.getMessage());
            this.addMensajeError("El archivo no contiene la estructura adecuada!");
        } catch (Exception e) {
            this.addMensajeError("El archivo no contiene la estructura adecuada!");
            LOGGER.error("ERROR: " + e.getMessage());
        } catch (Throwable e) {
            this.addMensajeError("El archivo no contiene la estructura adecuada!");
            LOGGER.error("ERROR: " + e.getMessage());
        }

    }

    /**
     * Método que realiza la validación de entidades según su nombre, sin importar mayúsculas o
     * mínusculas.
     *
     * @author david.cifuentes
     */
    private Entidad validarEntidadBloqueo(String numeroDocumento,
        String nombreEntidad) {

        Entidad entidad = null;
        try {

            if (nombreEntidad != null && !nombreEntidad.isEmpty()) {

                List<Entidad> entidadesBloqueo = this.getConservacionService()
                    .buscarEntidadesPorNombreYEstado(nombreEntidad,
                        EEntidadEstado.ACTIVO.getEstado());

                if (entidadesBloqueo != null && !entidadesBloqueo.isEmpty() &&
                     entidadesBloqueo.size() == 1) {
                    return entidadesBloqueo.get(0);
                } else {
                    entidadesBloqueo = this.getConservacionService()
                        .buscarEntidadesPorNombreYEstado(nombreEntidad,
                            EEntidadEstado.INACTIVO.getEstado());
                    if (entidadesBloqueo != null && !entidadesBloqueo.isEmpty() &&
                         entidadesBloqueo.size() == 1) {
                        this.addMensajeError("La entidad '" +
                             nombreEntidad +
                             "' se encuentra inactiva. La persona con identificación " +
                             numeroDocumento + " no se marcará.");
                    } else {
                        this.addMensajeError("La entidad '" +
                             nombreEntidad +
                            
                            "' no se encuentra registrada, por favor verifique la información. La persona con identificación " +
                             numeroDocumento + " no se marcará.");
                    }
                }
                return entidad;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            LOGGER.debug("Error consultando las entidades");
        }
        return entidad;
    }

    // -------------------------------------------------------------------------
    /**
     * Método que valida los datos ingresados para persona bloqueo masivo
     *
     * @modified by leidy.gonzalez 24/07/2014 Se valida que el campo primer nombre y primer apellido
     * sea obligatorio solo para personas naturales
     */
    private boolean validarTipoBloqueoMasivo(PersonaBloqueo personaBloqueoTemp) {

        if (personaBloqueoTemp.getMotivoBloqueo() == null ||
             (personaBloqueoTemp.getMotivoBloqueo() != null && personaBloqueoTemp
            .getMotivoBloqueo().isEmpty())) {
            String mensaje = "Para la persona identificada con número " +
                 personaBloqueoTemp.getPersona().getNumeroIdentificacion() +
                 " no se encontro un motivo de marcación" +
                 " por lo que esta persona no se incluirá para ser marcada.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (personaBloqueoTemp.getPersona().getTipoIdentificacion() != null &&
             personaBloqueoTemp
                .getPersona()
                .getTipoIdentificacion()
                .equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo()) &&
             personaBloqueoTemp.getPersona().getPrimerNombre() == null &&
             (personaBloqueoTemp.getPersona().getPrimerNombre() != null && personaBloqueoTemp
            .getPersona().getPrimerNombre().isEmpty())) {
            String mensaje = "Para la persona identificada con número " +
                 personaBloqueoTemp.getPersona().getNumeroIdentificacion() +
                 " no se encontro un nombre" +
                 " por lo que esta persona no se incluirá para ser marcada.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (personaBloqueoTemp.getPersona().getTipoIdentificacion() != null &&
             personaBloqueoTemp
                .getPersona()
                .getTipoIdentificacion()
                .equals(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                    .getCodigo()) &&
             personaBloqueoTemp.getPersona().getPrimerApellido() == null &&
             (personaBloqueoTemp.getPersona().getPrimerApellido() != null && personaBloqueoTemp
            .getPersona().getPrimerApellido().isEmpty())) {
            String mensaje = "Para la persona identificada con número " +
                 personaBloqueoTemp.getPersona().getNumeroIdentificacion() +
                 " no se encontro un apellido" +
                 " por lo que esta persona no se incluirá para ser marcada.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (personaBloqueoTemp.getFechaInicioBloqueo() == null) {
            String mensaje = "Para la persona identificada con número " +
                 personaBloqueoTemp.getPersona().getNumeroIdentificacion() +
                 " la fecha inicial es requerida y no se encontro el campo" +
                 " por lo que esta persona no se incluirá para ser marcada.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (personaBloqueoTemp.getFechaRecibido() == null) {
            String mensaje = "Para la persona identificada con número " +
                 personaBloqueoTemp.getPersona().getNumeroIdentificacion() +
                 " la fecha de recibido es requerida y no se encontro el campo" +
                 " por lo que esta persona no se incluirá para ser marcada.";
            this.addMensajeError(mensaje);
            return false;
        }

        if (personaBloqueoTemp.getFechaTerminaBloqueo() != null &&
             personaBloqueoTemp.getFechaTerminaBloqueo().before(
                personaBloqueoTemp.getFechaInicioBloqueo())) {
            String mensaje = "Para la persona identificada con número " +
                 personaBloqueoTemp.getPersona().getNumeroIdentificacion() +
                 " la fecha final no puede ser menor a la fecha inicial" +
                 " por lo que esta persona no se incluirá para ser marcada.";
            this.addMensajeError(mensaje);
            return false;
        }

        return true;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para validar la estructura basica del archivo de bloqueo masivo
     */
    private boolean validarEstructuraArchivoBloqueoMasivo(String[] partes) {

        if (partes[0] != null) {
            for (EPersonaTipoIdentificacion ti : EPersonaTipoIdentificacion
                .values()) {
                if (partes[0].equals(ti.getCodigo())) {
                    return true;
                }
            }
        }

        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para validar la estructura basica del archivo de bloqueo masivo
     */
    private boolean validarEstructuraArchivoBloqueoMasivo(Object[] partes) {

        if (partes[0] != null && !partes[0].toString().isEmpty()) {
            for (EPersonaTipoIdentificacion ti : EPersonaTipoIdentificacion
                .values()) {
                if (partes[0].toString().equals(ti.getCodigo())) {
                    return true;
                }
            }
        }

        return false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Metodo para guardar los datos de persona bloqueo ejecutado sobre el boton guardar
     */
    public void bloquearMPB() {

        RequestContext context = RequestContext.getCurrentInstance();
        List<DocumentoArchivoAnexo> daas = null;
        Entidad entidad;
        Boolean actividad;

        if (this.tipoBloqueo == 2 && this.aSoporte == 1 && this.aBMasa == 1) {

            try {

                for (PersonaBloqueo pb : this.bloqueoMasivo) {

                    pb.setDocumentoSoporteBloqueo(this.documentoSoporteBloqueo);

                    if (pb.getDocumentoSoporteBloqueo()
                        .getDocumentoArchivoAnexos() == null ||
                         (pb.getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos() == null && !pb
                            .getDocumentoSoporteBloqueo()
                            .getDocumentoArchivoAnexos().isEmpty())) {

                        daas = new ArrayList<DocumentoArchivoAnexo>();

                        daas.add(this.documentoArchivoAnexo);
                        pb.getDocumentoSoporteBloqueo()
                            .setDocumentoArchivoAnexos(daas);
                    }
                }

                ResultadoBloqueoMasivo resultado = this
                    .getConservacionService().bloquearMasivamentePersonas(
                        this.usuario, this.bloqueoMasivo);

                if (resultado != null &&
                     (resultado.getBloqueoMasivoPersonasExitoso() != null &&
                     !resultado.getBloqueoMasivoPersonasExitoso()
                        .isEmpty() || resultado
                        .getErroresProcesoMasivo() != null &&
                     !resultado.getErroresProcesoMasivo()
                        .isEmpty())) {
                    generarReporteDePersonasBloqueadas(resultado);
                }

                if (resultado != null &&
                     resultado.getBloqueoMasivoPersonasExitoso() != null &&
                     !resultado.getBloqueoMasivoPersonasExitoso()
                        .isEmpty() &&
                     resultado.getErroresProcesoMasivo() != null &&
                     !resultado.getErroresProcesoMasivo().isEmpty()) {
                    String mensaje = "La marcación masiva se realizó pero se presentaron errores";
                    this.addMensajeInfo(mensaje);
                } else if (resultado != null &&
                     resultado.getBloqueoMasivoPersonasExitoso() != null &&
                     !resultado.getBloqueoMasivoPersonasExitoso()
                        .isEmpty() &&
                     (resultado.getErroresProcesoMasivo() == null || (resultado
                    .getErroresProcesoMasivo() != null && resultado
                        .getErroresProcesoMasivo().isEmpty()))) {
                    String mensaje = "La marcación masiva se realizó exitosamente";
                    this.addMensajeInfo(mensaje);

                    // Suspención de los predios que se bloquearon
                    if (resultado.getBloqueoMasivoPersonasExitoso() != null &&
                         !resultado.getBloqueoMasivoPersonasExitoso()
                            .isEmpty()) {

                        for (PersonaBloqueo pb : resultado
                            .getBloqueoMasivoPersonasExitoso()) {

                            if ("SI".equals(pb.getRestitucionTierras()) &&
                                 pb.getTipoBloqueo().equals(
                                    EPredioBloqueoTipoBloqueo.SUSPENDER
                                        .getCodigo())) {

                                this.tramites = this
                                    .getTramiteService()
                                    .buscarTramitesTiposTramitesByNumeroIdentificacion(
                                        pb.getPersona()
                                            .getNumeroIdentificacion(),
                                        pb.getPersona()
                                            .getTipoIdentificacion());

                                if (this.tramites != null &&
                                     !this.tramites.isEmpty()) {

                                    entidad = this.getConservacionService()
                                        .findEntidadById(pb.getEntidadId());
                                    actividad = validarSubProcesoValidacion(
                                        entidad.getNombre(),
                                        entidad.getCorreo());

                                    if (actividad == false) {
                                        bloqueoProcesos(pb,
                                            entidad.getNombre(),
                                            entidad.getCorreo());
                                    }
                                }
                            }
                        }
                    }

                } else if (resultado != null &&
                     resultado.getBloqueoMasivoPersonasExitoso() != null &&
                     resultado.getBloqueoMasivoPersonasExitoso()
                        .isEmpty()) {
                    String mensaje = "No se pudo realiszar la marcación masiva para ninguna persona";
                    this.addMensajeError(mensaje);
                }
            } catch (Exception e) {
                String mensaje =
                    "El rango de personas selecionados no es valido o no pudo ser marcada!";
                this.addMensajeError(mensaje);
            }

        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 1) {
            String mensaje = "No se adjuntó un documento de soporte para la marcación";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 1 &&
             this.aBMasa == 0) {
            String mensaje;
            if (this.nombreDocumentoBloqueoMasivo == null ||
                 (this.nombreDocumentoBloqueoMasivo != null && this.nombreDocumentoBloqueoMasivo
                    .isEmpty())) {
                mensaje = "No se adjuntó un archivo de marcación masiva" +
                     " Por favor corrija los datos e intente nuevamente.";
            } else {
                mensaje = "El archivo que adjunto no contiene ningúna marcación valida." +
                     " Por favor corrija los datos e intente nuevamente.";
            }
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        } else if (this.tipoBloqueo == 2 && this.aSoporte == 0 &&
             this.aBMasa == 0) {
            String mensaje =
                "No se adjuntó un documento de soporte, ni un archivo de marcación masivo";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
        }

    }

    /**
     * Método encargado de generar el reporte de personas bloqueadas masivamente
     *
     * @author javier.aponte
     *
     */
    public void generarReporteDePersonasBloqueadas(
        ResultadoBloqueoMasivo resultado) {

        List<TmpBloqueoMasivo> idsBloqueos = new ArrayList<TmpBloqueoMasivo>();
        TmpBloqueoMasivo bloqueoMasivo;

        // Generar secuencia de sesion de tmp_bloqueo_masivo
        BigDecimal sessionId = this.getConservacionService()
            .generarSecuenciaTmpBloqueoMasivo();

        if (resultado.getBloqueoMasivoPersonasExitoso() != null &&
             !resultado.getBloqueoMasivoPersonasExitoso().isEmpty()) {
            for (PersonaBloqueo bloqueoMasivoExitoso : resultado
                .getBloqueoMasivoPersonasExitoso()) {
                bloqueoMasivo = new TmpBloqueoMasivo();
                bloqueoMasivo.setSessionId(sessionId);
                bloqueoMasivo.setBloqueado(ESiNo.SI.getCodigo());
                bloqueoMasivo.setIdentificador(bloqueoMasivoExitoso
                    .getPersona().getNumeroIdentificacion());
                bloqueoMasivo.setDescripcion(bloqueoMasivoExitoso.getPersona()
                    .getTipoIdentificacion());
                bloqueoMasivo.setFechaLog(new Date());
                idsBloqueos.add(bloqueoMasivo);
            }
        }

        if (resultado.getErroresProcesoMasivo() != null &&
             !resultado.getErroresProcesoMasivo().isEmpty()) {
            String[] textoFuente;
            for (ErrorProcesoMasivo bloqueoMasivoError : resultado
                .getErroresProcesoMasivo()) {
                bloqueoMasivo = new TmpBloqueoMasivo();
                bloqueoMasivo.setSessionId(sessionId);
                bloqueoMasivo.setBloqueado(ESiNo.NO.getCodigo());
                bloqueoMasivo.setFechaLog(new Date());
                if (bloqueoMasivoError.getTextoFuente() != null) {
                    textoFuente = bloqueoMasivoError.getTextoFuente().split(
                        SEPARATOR_PARAMETER);
                    // guarda en la descripción el tipo de identificación y
                    // nombre de la persona y en el identificador guarda el
                    // número de identificación de la persona
                    bloqueoMasivo.setDescripcion(textoFuente[0] +
                         SEPARATOR_PARAMETER + textoFuente[2]);
                    bloqueoMasivo.setIdentificador(textoFuente[1]);
                    idsBloqueos.add(bloqueoMasivo);
                }
            }
        }

        // Guarda los ids de las ofertas inmobiliarias seleccionadas
        idsBloqueos = this.getConservacionService()
            .guardarActualizarIdsBloqueoMasivo(idsBloqueos);

        if (idsBloqueos != null && !idsBloqueos.isEmpty()) {
            try {
                this.generarReporteBloqueoMasivoPersonas(idsBloqueos.get(0)
                    .getSessionId(), resultado);
            } catch (Exception e) {
                LOGGER.error("Ocurrió un error en el método generarReporteBloqueoMasivoPersonas");
            } finally {
                // Borra los ids de las ofertas inmobiliarias seleccionadas
                this.getConservacionService().borrarIdsBloqueoMasivo(
                    idsBloqueos);
            }
        }

    }

    /**
     * Método para generar el reporte de bloqueo de personas masivamente
     *
     * @author javier.aponte
     * @throws Exception
     */
    // --------------------------------------------------------------------------------------------------
    public void generarReporteBloqueoMasivoPersonas(BigDecimal sessionId,
        ResultadoBloqueoMasivo resultado) throws Exception {

        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.BLOQUEO_MASIVO_PERSONAS;

        Map<String, String> parameters = new HashMap<String, String>();

        try {
            if (resultado != null &&
                 resultado.getBloqueoMasivoPersonasExitoso() != null &&
                 !resultado.getBloqueoMasivoPersonasExitoso().isEmpty()) {

                SimpleDateFormat formatoFecha = new SimpleDateFormat(
                    Constantes.FORMATO_FECHA);
                String fechaAux = null;
                if (resultado.getBloqueoMasivoPersonasExitoso().get(0)
                    .getFechaInicioBloqueo() != null) {
                    fechaAux = formatoFecha.format(resultado
                        .getBloqueoMasivoPersonasExitoso().get(0)
                        .getFechaInicioBloqueo());
                }
                parameters.put("FECHA_INICIO_BLOQUEO", fechaAux);
                fechaAux = null;
                if (resultado.getBloqueoMasivoPersonasExitoso().get(0)
                    .getFechaTerminaBloqueo() != null) {
                    fechaAux = formatoFecha.format(resultado
                        .getBloqueoMasivoPersonasExitoso().get(0)
                        .getFechaTerminaBloqueo());
                }
                parameters.put("FECHA_TERMINA_BLOQUEO", fechaAux);
                parameters.put("MOTIVO_BLOQUEO", resultado
                    .getBloqueoMasivoPersonasExitoso().get(0)
                    .getMotivoBloqueo());
            }
            parameters.put("SESSION_ID", String.valueOf(sessionId));

            this.reporteBloqueoMasivoPersonas = reportsService.generarReporte(
                parameters, enumeracionReporte, this.usuario);

            if (this.reporteBloqueoMasivoPersonas != null) {

                PrevisualizacionDocMB previsualizacionDocMB = (PrevisualizacionDocMB) UtilidadesWeb
                    .getManagedBean("previsualizacionDocumento");

                previsualizacionDocMB
                    .setRutaArchivoTemporal(this.reporteBloqueoMasivoPersonas
                        .getUrlWebReporte());

            } else {
                this.addMensajeError(Constantes.MENSAJE_ERROR_REPORTE);
            }

        } catch (Exception e) {
            LOGGER.error("Ocurrió un error al generar al reporte de marcación masivo de personas");
            throw new Exception(
                "Ocurrió un error al generar el reporte de marcación " +
                 "masivo de personas");
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosArchivoBloqueo() {
        this.documentoSoporteBloqueo = new Documento();
        this.aSoporte = 0;
        this.motivoBloqueo = "";
        this.nombreDocumentoBloqueoMasivo = null;
        this.bloquearPersonas = false;
        this.selectedTipoBloqueo = null;
        this.selectedRestitucionTierras = false;
        this.selectedDepartamento = "";
        this.selectedMunicipio = "";
        this.municipiosBuscadorItemList = new ArrayList<SelectItem>();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * listener de inicialización de datos de desbloqueo
     */
    public void inicializarDatosArchivoBloqueoMasivo() {
        this.documentoSoporteBloqueo = new Documento();
        this.aSoporte = 0;
        this.motivoBloqueo = "";
        this.nombreDocumentoBloqueoMasivo = null;
        this.bloquearPersonas = false;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar y bloquear otra persona
     */
    public void terminarYBloquearOtraP() {
        init();
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar
     */
    public String terminar() {
        try {
            UtilidadesWeb.removerManagedBean("bloqueoPersona");
            return "index";
        } catch (Exception e) {
            this.addMensajeError("Error al cerrar la sesión");
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método para buscar una persona por número de identificación
     */
    public void buscarPersona() {

        if (this.personaBloqueo.getPersona() != null &&
             this.personaBloqueo.getPersona().getNumeroIdentificacion() != null &&
             this.personaBloqueo.getPersona().getTipoIdentificacion() != null) {

            List<Persona> pTemp = this.getConservacionService()
                .buscarPersonaPorNumeroYTipoIdentificacion(
                    this.personaBloqueo.getPersona()
                        .getTipoIdentificacion(),
                    this.personaBloqueo.getPersona()
                        .getNumeroIdentificacion());

            this.personasCoincidentes = new ArrayList<Persona>();
            this.personaSeleccionada = null;

            if (pTemp != null) {
                if (pTemp.size() == 1) {
                    this.personaBloqueo.setPersona(pTemp.get(0));
                    RequestContext context = RequestContext
                        .getCurrentInstance();
                    context.addCallbackParam("error", "error");
                } else {

                    this.banderaValidar = false;
                    this.personaBloqueo.getPersona().setPrimerNombre(null);
                    this.personaBloqueo.getPersona().setSegundoNombre(null);
                    this.personaBloqueo.getPersona().setPrimerApellido(null);
                    this.personaBloqueo.getPersona().setSegundoApellido(null);
                    this.personaBloqueo.getPersona().setRazonSocial(null);
                    this.personaBloqueo.getPersona()
                        .setDigitoVerificacion(null);
                    this.laBandera = false;

                    if (pTemp.size() < 1) {
                        String mensaje = "No existe ninguna persona con los datos ingresados." +
                             "Por favor corrija los datos e intente nuevamente.";
                        this.addMensajeError(mensaje);
                        RequestContext context = RequestContext
                            .getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    } else {
                        for (Persona p : pTemp) {
                            if (p != null) {
                                this.filtrarPersonasCoincidentes(p);
                            }
                        }
                    }
                }
            } else {
                String mensaje = "Ocurrio un error inesperado." +
                     "Por favor corrija los datos e intente nuevamente.";
                this.addMensajeError(mensaje);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }
        }
    }

    /**
     * Método que recorre la lista de personas coincidentes y si no se encuentra la persona enviada
     * como parámetro, la adiciona.
     */
    public void filtrarPersonasCoincidentes(Persona p) {
        boolean adicionar = true;
        List<Persona> nombres = new ArrayList<Persona>();
        List<Persona> nits = new ArrayList<Persona>();

        for (Persona auxP : this.personasCoincidentes) {
            if (EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo()
                .equals(auxP.getTipoIdentificacion())) {
                nombres.add(auxP);
            } else if (EPersonaTipoIdentificacion.NIT.getCodigo().equals(
                auxP.getTipoIdentificacion())) {
                nits.add(auxP);
            }
        }
        if (EPersonaTipoIdentificacion.CEDULA_CIUDADANIA.getCodigo().equals(
            p.getTipoIdentificacion())) {
            for (Persona pn : nombres) {
                if (p.getNombreCompleto() != null &&
                     p.getNombreCompleto().equals(pn.getNombreCompleto())) {
                    adicionar = false;
                }
            }
        } else if (EPersonaTipoIdentificacion.NIT.getCodigo().equals(
            p.getTipoIdentificacion())) {
            for (Persona prs : nits) {
                if (p.getRazonSocial() != null &&
                     p.getRazonSocial().equals(prs.getRazonSocial())) {
                    adicionar = false;
                }
            }
        } else {
            adicionar = false;
        }

        if (adicionar) {
            this.personasCoincidentes.add(p);
        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de la descarga de un archivo temporal de office
     */
    public void controladorDescargaArchivos() {

        if (this.documentoTemporalAVisualizar != null &&
             this.documentoTemporalAVisualizar.getArchivo() != null &&
             !this.documentoTemporalAVisualizar.getArchivo().isEmpty()) {

            File fileTemp = new File(FileUtils.getTempDirectory()
                .getAbsolutePath(),
                this.documentoTemporalAVisualizar.getArchivo());
            InputStream stream;

            try {

                stream = new FileInputStream(fileTemp);
                cargarDocumentoTemporalPrev();
                this.fileTempDownload = new DefaultStreamedContent(stream,
                    this.tipoMimeDocTemporal,
                    this.documentoTemporalAVisualizar.getArchivo());

            } catch (FileNotFoundException e) {
                String mensaje = "Ocurrió un error al cargar al archivo." +
                     " Por favor intente nuevamente.";
                this.addMensajeError(mensaje);
            }

        } else {
            String mensaje = "Ocurrió un error al acceder al archivo." +
                 " Por favor intente nuevamente.";
            this.addMensajeError(mensaje);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de inicializar los datos de documento temporal para la previsualización Se
     * adiciono un atributo tipoMimeDocumento para la clase de previsualización de documento y este
     * debe ser el que se consuma
     */
    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.documentoTemporalAVisualizar
                .getArchivo());
        return this.tipoMimeDocTemporal;
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método encargado de realizar el set de la persona seleccionada dentro de una lista de
     * personas que se encontraron con coincidencias en el número de identificación o nombres.
     */
    public void aceptarSeleccion() {
        if (this.personaSeleccionada != null &&
             this.personaSeleccionada.getTipoIdentificacion() != null) {
            this.personaBloqueo.setPersona(this.personaSeleccionada);
        }
    }

    /**
     * Metodo para actualizar los municipios
     *
     * @author dumar.penuela
     */
    public void actualizarMunicipiosBuscardorListener() {

        if (this.municipiosDeptos != null) {

            this.municipiosBuscadorItemList = this.municipiosDeptos
                .get(this.selectedDepartamento);
        }

    }

    /**
     * Metodo para cargar los departamentos
     *
     * @author dumar.penuela
     */
    public void cargarDepartamentos() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList;
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = generalService.getDepartamentos(Constantes.COLOMBIA);

        this.cargarDepartamentosItemList(deptosList);

        for (Departamento dpto : deptosList) {
            municipiosList = this.getGeneralesService()
                .getCacheMunicipiosByDepartamento(dpto.getCodigo());
            municipiosItemListTemp = new ArrayList<SelectItem>();

            for (Municipio m : municipiosList) {
                municipiosItemListTemp.add(new SelectItem(m.getCodigo(), m
                    .getCodigo() + "-" + m.getNombre()));
            }

            this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

        }
    }

    /**
     * Metodo para constuir la lista de departamentos
     *
     * @author dumar.penuela
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento
                .getCodigo(), departamento.getCodigo() + "-" +
                 departamento.getNombre()));
        }
    }

    public void cargarEntidades() {
        this.entidades = new ArrayList<SelectItem>();
        this.entidades.add(new SelectItem(null, DEFAULT_COMBOS));

        List<Entidad> entidadesActivas = this.getConservacionService()
            .buscarEntidades(null, EEntidadEstado.ACTIVO.getEstado(),
                selectedDepartamento, selectedMunicipio);

        if (entidadesActivas != null && !entidadesActivas.isEmpty()) {
            for (Entidad e : entidadesActivas) {
                this.entidades.add(new SelectItem(e.getId(), e.getNombre()));
            }
        } else {
            Municipio d = this.getGeneralesService().getCacheMunicipioByCodigo(
                selectedMunicipio);
            this.addMensajeError("El municipio " +
                 d.getNombre() +
                 ", no tiene asociadas entidades que puedan realizar la marcación, verifique");
        }
    }

    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList
            .add(new SelectItem(null, this.DEFAULT_COMBOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos,
                Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    /**
     * metodo utilizado para validar sí un predio tiene tramites asociados a un proceso dentro del
     * subproceso de Validacion Tipo BloqueoMasivo TBM
     */
    private boolean validarSubProcesoValidacion(String nombreEntidad,
        String correoEntidad) {

        Actividad act;
        Map filtros;
        Boolean resultado = false;
        List<Actividad> actividades;
        Locale colombia = new Locale("ES", "es_CO");
        Object parametrosContenidoEmail[] = new Object[9];

        Parametro correoRestitucionTierras;

        Plantilla plantillaTierras;
        Plantilla plantillaentidad;

        String contenidoRestitucion = "";
        String contenidoEntidad = "";

        plantillaTierras = this
            .getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_RESTITUCION_TIERRAS_BLOQUEO_RESOLUCION
                    .getCodigo());

        plantillaentidad = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_ENTIDAD_BLOQUEO_RESOLUCION
                    .getCodigo());

        if (null != plantillaTierras) {
            contenidoRestitucion = plantillaTierras.getHtml();
            if (null == contenidoRestitucion) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de restitución de tierras");
            }
        }

        if (null != plantillaentidad) {
            contenidoEntidad = plantillaentidad.getHtml();
            if (null == contenidoEntidad) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        for (Tramite tram : this.tramites) {

            if (!tram.getEstado().equals(ETramiteEstado.ARCHIVADO.getCodigo()) &&
                 !tram.getEstado().equals(
                    ETramiteEstado.CANCELADO.getCodigo()) &&
                 !tram.getEstado().equals(
                    ETramiteEstado.RECHAZADO.toString()) &
                 !tram.getEstado().equals(
                    ETramiteEstado.FINALIZADO_APROBADO.toString())) {

                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION,
                    tram.getNumeroRadicacion());
                filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD,
                    String.valueOf(tram.getId()));
                actividades = this.getProcesosService()
                    .consultarListaActividades(filtros);

                if (actividades != null && !actividades.isEmpty()) {

                    act = actividades.get(0);
                    if (!act.getEstado().equals(
                        EEstadoActividad.SUSPENDIDA.toString())) {

                        if (act.getNombre()
                            .equals(ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_VALIDACION_APLICAR_CAMBIOS) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO)) {

                            if (tram.getResultadoDocumento() != null &&
                                 !tram.getResultadoDocumento()
                                    .getNumeroDocumento().isEmpty()) {

                                correoRestitucionTierras = this
                                    .getGeneralesService()
                                    .getCacheParametroPorNombre(
                                        EParametro.CORREO_RESTITUCION_TIERRAS
                                            .toString());

                                parametrosContenidoEmail[0] = nombreEntidad;
                                parametrosContenidoEmail[1] = tramites.get(0)
                                    .getNumeroPredial();
                                parametrosContenidoEmail[2] = tramites.get(0)
                                    .getPredio().getMunicipio().getNombre();
                                parametrosContenidoEmail[3] = tramites.get(0)
                                    .getPredio().getDepartamento()
                                    .getNombre();
                                parametrosContenidoEmail[4] = tram
                                    .getTipoTramiteCadenaCompleto();
                                parametrosContenidoEmail[5] = tram
                                    .getNumeroRadicacion();
                                parametrosContenidoEmail[6] = tram
                                    .getSolicitud().getSolicitante()
                                    .getNombreCompleto();
                                parametrosContenidoEmail[7] = tram
                                    .getResultadoDocumento()
                                    .getNumeroDocumento();
                                parametrosContenidoEmail[8] = tram
                                    .getResultadoDocumento()
                                    .getFechaDocumento();

                                contenidoRestitucion = new MessageFormat(
                                    contenidoRestitucion, colombia)
                                    .format(parametrosContenidoEmail);
                                contenidoEntidad = new MessageFormat(
                                    contenidoEntidad, colombia)
                                    .format(parametrosContenidoEmail);

                                enviarCorreo(
                                    correoRestitucionTierras
                                        .getValorCaracter(),
                                    contenidoRestitucion);
                                enviarCorreo(correoEntidad, contenidoEntidad);

                                resultado = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return resultado;

    }

    /**
     * Metodo utilizado para identificar los tramites que esten asociados a un predio y suspender
     * los respectivos procesos de los tramites dentro de los subprocesos de Validacion, ejecución,
     * asignación, depuración
     */
    private boolean bloqueoProcesos(PersonaBloqueo personaBloqueo,
        String nombreEntidad, String correoEntidad) {

        Actividad act;
        Map filtros;
        Boolean resultado = false;
        List<Actividad> actividades;
        Locale colombia = new Locale("ES", "es_CO");
        Object parametrosContenidoEmail[] = new Object[5];
        Parametro correoRestitucionTierras;
        String cuerpoCorreo = "";

        String contenidoEntidad = "";
        String contenidoPieDetalleCorreo = "";
        String contenidoDetalleCorreo = "";
        String contenidoPlantilla;
        String contenidoRestitucion = "";
        Parametro nombreRestitucionTierras;

        Plantilla plantillaentidad;
        Plantilla pieDetalleCorreo;
        Plantilla detalleCorreo;

        Calendar cal = Calendar.getInstance();

        if (personaBloqueo.getFechaTerminaBloqueo() != null) {
            cal.setTime(personaBloqueo.getFechaTerminaBloqueo());
        } else {
            cal.setTime(personaBloqueo.getFechaInicioBloqueo());
            cal.add(Calendar.DATE, Constantes.ANIO_SUSPENSION);
        }

        plantillaentidad = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_ENTIDAD_BLOQUEO_SUSPENDER_TRAMITE
                    .getCodigo());

        pieDetalleCorreo = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(
                EPlantilla.MENSAJE_PIE_BLOQUEO_SUSPENDER_TRAMITE
                    .getCodigo());

        detalleCorreo = this.getGeneralesService().recuperarPlantillaPorCodigo(
            EPlantilla.MENSAJE_DETALLE_BLOQUEO_SUSPENDER_TRAMITE
                .getCodigo());

        if (null != plantillaentidad) {
            contenidoEntidad = plantillaentidad.getHtml();
            contenidoRestitucion = plantillaentidad.getHtml();
            if (null == contenidoEntidad) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        if (null != pieDetalleCorreo) {
            contenidoPieDetalleCorreo = pieDetalleCorreo.getHtml();
            if (null == contenidoPieDetalleCorreo) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        if (null != detalleCorreo) {
            contenidoDetalleCorreo = detalleCorreo.getHtml();
            if (null == contenidoDetalleCorreo) {
                super.addMensajeError(
                    "Imposible recuperar la plantilla del correo de la entidad que ordena la marcación");
            }
        }

        correoRestitucionTierras = this.getGeneralesService()
            .getCacheParametroPorNombre(
                EParametro.CORREO_RESTITUCION_TIERRAS.toString());
        nombreRestitucionTierras = this.getGeneralesService()
            .getCacheParametroPorNombre(
                EParametro.NOMBRE_RESTITUCION_TIERRAS.toString());

        for (Tramite tram : this.tramites) {

            if (!tram.getEstado().equals(ETramiteEstado.ARCHIVADO.getCodigo()) &&
                 !tram.getEstado().equals(
                    ETramiteEstado.CANCELADO.getCodigo()) &&
                 !tram.getEstado().equals(
                    ETramiteEstado.RECHAZADO.toString()) &
                 !tram.getEstado().equals(
                    ETramiteEstado.FINALIZADO_APROBADO.toString())) {

                filtros = new EnumMap<EParametrosConsultaActividades, String>(
                    EParametrosConsultaActividades.class);
                filtros.put(EParametrosConsultaActividades.NUMERO_RADICACION,
                    tram.getNumeroRadicacion());
                filtros.put(EParametrosConsultaActividades.ULTIMA_ACTIVIDAD,
                    String.valueOf(tram.getId()));
                actividades = this.getProcesosService()
                    .consultarListaActividades(filtros);

                if (actividades != null && !actividades.isEmpty()) {

                    act = actividades.get(0);

                    if (!act.getEstado().equals(
                        EEstadoActividad.SUSPENDIDA.toString())) {

                        if ( // SUBPROCESO DE VALIDACION
                            act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION_RESOLUCION) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_VALIDACION_GENERAR_RESOLUCION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_ALFANUMERICA) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_EJECUCION_MODIFICAR_INFO_GEOGRAFICA) || // SUBPROCESO DE EJECUCIÓN
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_AUTO_PRUEBA) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_REVISAR_AUTO_PRUEBA) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_EJECUCION_COMUNICAR_AUTO_INTERESADO) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_COMISION) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_PRUEBA_COMISIONAR) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_ALISTAR_INFORMACION) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_CARGAR_AL_SNC) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_NUEVOS_TRAMITES) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_EJECUCION_ELABORAR_INFORME_CONCEPTO) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_SOLICITAR_PRUEBA) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_SOLICITUD_PRUEBA) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_EJECUCION_ASOCIAR_DOC_AL_NUEVO_TRAMITE) || // SUBPROCESO DE ASIGNACIÓN
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_ASIGNACION_SOLICITAR_DOC_REQUERIDO_TRAMITE) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_DEVUELTO) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_ASIGNACION_REVISAR_TRAMITE_ASIGNADO) || // SUBPROCESO DE DEPURACION
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_ESTUDIAR_AJUSTE_ESPACIAL) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_TERRENO) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_DIGITALIZADOR) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_TOPOGRAFO) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_EJECUTOR) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_POR_REALIZAR) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_EJECUCION_APROBAR_COMISION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_DIGITALIZACION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION_EJECUTOR) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_REVISAR_PREPARAR_INFO) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_REGISTRAR_VISITA_TERRENO) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_REGISTRAR_VISITA_TERRENO) ||
                             act.getNombre()
                                .equals(ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_INFORME_VISITA) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_EXP_AREA_IDENTIFICAR_PUNTOS_TOPOGRAFICOS) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_AJUSTE_ESPACIAL_EJECUTOR) || // SUBPROCESO DEPURACION
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA) || // SUBPROCESO CALIDAD DE DEPURACION
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_EJECUCION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_DIGITALIZACION) ||
                             act.getNombre()
                                .equals(
                                    ProcesoDeConservacion.ACT_DEPURACION_REALIZAR_CONTROL_CALIDAD_TOPOGRAFIA)) {

                            if (tram.getResultadoDocumento() == null) {

                                parametrosContenidoEmail[0] = tram
                                    .getTipoTramiteCadenaCompleto();
                                parametrosContenidoEmail[1] = tram
                                    .getNumeroRadicacion();
                                parametrosContenidoEmail[2] = tram
                                    .getSolicitud().getSolicitante()
                                    .getNombreCompleto();

                                contenidoPlantilla = contenidoDetalleCorreo;
                                contenidoPlantilla = new MessageFormat(
                                    contenidoPlantilla, colombia)
                                    .format(parametrosContenidoEmail);
                                cuerpoCorreo = cuerpoCorreo +
                                     contenidoPlantilla;

                                // Eliminar Replicas
                                if (act.getNombre()
                                    .equals(
                                        ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION) ||
                                     act.getNombre()
                                        .equals(
                                            ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR) ||
                                     act.getNombre()
                                        .equals(
                                            ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {

                                    this.getGeneralesService()
                                        .borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                                            tram.getId(),
                                            ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL
                                                .getId());
                                    this.getGeneralesService()
                                        .borrarTramiteDocumentoYDocumentoPorTramiteIdYTipoDocumentoId(
                                            tram.getId(),
                                            ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL
                                                .getId());
                                }

                                resultado = true;
                                this.getProcesosService().suspenderActividad(
                                    act.getId(), cal, null);

                            }
                        }
                    }

                }
            }
        }

        if (resultado) {

            parametrosContenidoEmail[0] = nombreEntidad;
            parametrosContenidoEmail[1] = tramites.get(0).getPredio()
                .getNumeroPredial();
            parametrosContenidoEmail[2] = tramites.get(0).getPredio()
                .getMunicipio().getNombre();
            parametrosContenidoEmail[3] = tramites.get(0).getPredio()
                .getDepartamento().getNombre();

            contenidoEntidad = new MessageFormat(contenidoEntidad, colombia)
                .format(parametrosContenidoEmail);
            enviarCorreo(correoEntidad, contenidoEntidad + cuerpoCorreo +
                 contenidoPieDetalleCorreo);

            parametrosContenidoEmail[0] = nombreRestitucionTierras
                .getValorCaracter();
            contenidoRestitucion = new MessageFormat(contenidoRestitucion,
                colombia).format(parametrosContenidoEmail);
            enviarCorreo(correoRestitucionTierras.getValorCaracter(),
                contenidoRestitucion + cuerpoCorreo +
                 contenidoPieDetalleCorreo);

        }

        return resultado;

    }

    /**
     * Método que envia un correo a la entidad que ordena el bloqueo y a la oficina de restitució de
     * tierras
     *
     * @author dumar.penuela
     * @param emailEntidad
     * @param contenido
     */
    public void enviarCorreo(String emailEntidad, String contenido) {
        try {

            this.getGeneralesService()
                .enviarCorreo(
                    emailEntidad,
                    ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_BLOQUEO_PREDIO,
                    contenido, null, null);
        } catch (Exception e) {
            LOGGER.debug("Error al enviar el correo electrónico ");
            LOGGER.error(e.getMessage(), e);
            this.addMensajeWarn("No se pudo enviar el informando la marcación del predio");
        }
    }

    /**
     * Verifica la existencia de una persona a bloquear en la base de datos
     *
     * @param partes parte 0 y 1 del archivo de bloqueo masivo
     * @return
     */
    private boolean existePersonaEnBD(final String partes[]) {
        List<Persona> personas = this.getConservacionService().
            buscarPersonaPorNumeroYTipoIdentificacion(partes[0], partes[1]);
        if (personas == null || personas.isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean existePersonaEnBD(final Object partes[]) {
        List<Persona> personas = this.getConservacionService().
            buscarPersonaPorNumeroYTipoIdentificacion(partes[0].toString(), partes[1].toString());
        if (personas == null || personas.isEmpty()) {
            return false;
        }
        return true;
    }

    private void revisarPersonaBloqueo(PersonaBloqueo personaBloqueo) {

    }

}
