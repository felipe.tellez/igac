/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioAvaluoCatastral;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import co.gov.igac.snc.vo.HistoricoAvaluosVO;

/**
 * MB que maneja lp necesario para mostrar el contenido del tab "histórico del avalúo" en la
 * pantalla de detalles del predio.
 *
 * @author pedro.garcia
 */
/**
 * @modified by leidy.gonzalez::12528::26/05/2015 Se modifica por CU_187
 *
 */
@Component("consultaHistoricoAvaluoPredio")
@Scope("session")
public class ConsultaHistoricoAvaluoPredioMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 87495746477084261L;
    private static final Logger LOGGER = LoggerFactory.getLogger(
        ConsultaHistoricoAvaluoPredioMB.class);

    /**
     * variable en la que se consultan los datos del histórico del avalúo
     */
    private ArrayList<PredioAvaluoCatastral> historicoAvaluoPredio;

    /**
     * predio en cuestión
     */
    private Predio predioForHistoricoAvaluo;

    /**
     * url en la carpeta web temporal del archivo que se generó con la imagen del histórico del
     * avalúo
     */
    private String urlArchivoGeneradoImagenHistoricoAvaluo;

    /**
     * lista que visualiza el historico de avaluos realizados sobre un predio
     */
    private ArrayList<HistoricoAvaluosVO> historicoAvaluos;

    /**
     * Objeto que contiene los datos del reporte de resoluciones
     */
    private ReporteDTO reporteDocumentoWaterMark;

// ------------------ services ------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * historico de avaluo realizados sobre un predio
     */
    private HistoricoAvaluosVO historicoAvaluo;

    private Documento documentoResolucion;

//--------------------            methods   ------------------------
    public HistoricoAvaluosVO getHistoricoAvaluo() {
        return historicoAvaluo;
    }

    public void setHistoricoAvaluo(HistoricoAvaluosVO historicoAvaluo) {

        documentoResolucion = this.getGeneralesService().
            buscarDocumentoPorNumeroDocumentoAndTipoDocumento(historicoAvaluo.
                getDecretoNumeroOResolucion());

        if (documentoResolucion != null && documentoResolucion.getIdRepositorioDocumentos() != null) {

            this.reporteDocumentoWaterMark = this.reportsService.consultarReporteDeGestorDocumental(
                documentoResolucion.getIdRepositorioDocumentos());

        }
        this.historicoAvaluo = historicoAvaluo;
    }

    public ReporteDTO getReporteDocumentoWaterMark() {
        return reporteDocumentoWaterMark;
    }

    public void setReporteDocumentoWaterMark(
        ReporteDTO reporteDocumentoWaterMark) {
        this.reporteDocumentoWaterMark = reporteDocumentoWaterMark;
    }

    public ArrayList<HistoricoAvaluosVO> getHistoricoAvaluos() {
        return historicoAvaluos;
    }

    public void setHistoricoAvaluos(ArrayList<HistoricoAvaluosVO> historicoAvaluos) {
        this.historicoAvaluos = historicoAvaluos;
    }

    public String getUrlArchivoGeneradoImagenHistoricoAvaluo() {
        return this.urlArchivoGeneradoImagenHistoricoAvaluo;
    }

    public void setUrlArchivoGeneradoImagenHistoricoAvaluo(
        String urlArchivoGeneradoImagenHistoricoAvaluo) {
        this.urlArchivoGeneradoImagenHistoricoAvaluo = urlArchivoGeneradoImagenHistoricoAvaluo;
    }

    public Predio getPredioForHistoricoAvaluo() {
        return this.predioForHistoricoAvaluo;
    }

    public void setPredioForHistoricoAvaluo(Predio predioForHistoricoAvaluo) {
        this.predioForHistoricoAvaluo = predioForHistoricoAvaluo;
    }

    public ArrayList<PredioAvaluoCatastral> getHistoricoAvaluoPredio() {
        return this.historicoAvaluoPredio;
    }

    public void setHistoricoAvaluoPredio(ArrayList<PredioAvaluoCatastral> historicoAvaluoPredio) {
        this.historicoAvaluoPredio = historicoAvaluoPredio;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaHistoricoAvaluoPredioMB");

        //D: se obtiene la referencia al mb que maneja los detalles del predio
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");

        /**
         * C: ahora se hacen las consultas por id del predio N: ver método
         * ConsultaPredioMB#getSelectedPredioId para entender de dónde se obtiene el id
         */
        Long predioSeleccionadoId = mb001.getSelectedPredioId();

        if (mb001.getSelectedPredio1() != null) {
            this.predioForHistoricoAvaluo = mb001.getSelectedPredio1();
        } else if (predioSeleccionadoId != null) {
            this.predioForHistoricoAvaluo =
                this.getConservacionService().obtenerPredioPorId(predioSeleccionadoId);
        }
        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioSeleccionadoId != null) {
            this.historicoAvaluoPredio = new ArrayList<PredioAvaluoCatastral>();
            if (!esPredioOrigen) {
                this.obtenerPredioPorIdHistoricosAvaluo(predioSeleccionadoId);

                if (mb001.getSelectedPredio1() != null) {
                    this.predioForHistoricoAvaluo = mb001.getSelectedPredio1();
                } else {
                    this.predioForHistoricoAvaluo =
                        this.getConservacionService().obtenerPredioPorId(predioSeleccionadoId);
                }
            } else {
                this.predioForHistoricoAvaluo = mb001.getSelectedPredio1();
                this.historicoAvaluoPredio =
                    (ArrayList<PredioAvaluoCatastral>) predioForHistoricoAvaluo.
                        getPredioAvaluoCatastrals();
            }
            this.historicoAvaluos = this.getConservacionService().obtenerResolucionODecreto(
                predioSeleccionadoId);

        }

        this.armarChart();
    }

//--------------------------------------------------------------------------------------------------
    /**
     * se define este método solo para mantener el estándar de tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * También se hacen las consultas de los trámites pendientes y realizados
     *
     * @author pedro.garcia
     * @param predioId
     */
    private void obtenerPredioPorIdHistoricosAvaluo(Long predioId) {

        this.historicoAvaluoPredio = (ArrayList<PredioAvaluoCatastral>) this.
            getConservacionService().obtenerHistoricoAvaluoPredio(predioId);

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * arma el dataset y el jfreechart que se usan. Crea el archivo con la imagen para ser leído
     * desde la página modificado por leidy.gonzalez
     */
    private void armarChart() {

        JFreeChart chart;
        DefaultCategoryDataset defaultCategoryDataset;
        Double upperBound, lowerBound;
        TextTitle subtitle;
        CategoryPlot plot;
        LineAndShapeRenderer renderer;
        File chartFile;
        NumberAxis ejeY;

        Date tempDate;
        Calendar cal = Calendar.getInstance();
        Double[] avaluos;
        int posicion;
        Double avaluoTemp = 0.0;
        Double desviacionEstandar;

        IDocumentosService servicioDocumental;
        DocumentoVO documentoVo;
        String rutaArchivoGeneradoImagenHistoricoAvaluo;

        defaultCategoryDataset = new DefaultCategoryDataset();

        if (this.historicoAvaluoPredio == null || this.historicoAvaluoPredio.isEmpty()) {
            LOGGER.debug("No hay datos para armar el chart");
            return;
        }

        upperBound = 0D;
        lowerBound = 0D;
        avaluos = new Double[this.historicoAvaluoPredio.size()];
        posicion = 0;
        for (PredioAvaluoCatastral pac : this.historicoAvaluoPredio) {
            tempDate = pac.getVigencia();
            cal.setTime(tempDate);

            avaluoTemp = pac.getValorTotalAvaluoCatastral();

            upperBound = (avaluoTemp > upperBound) ? avaluoTemp : upperBound;

            //N: para hacer que el lowerbound no sea 0 siempre
            if (posicion == 0) {

                defaultCategoryDataset.addValue(0, "año", "0");
                defaultCategoryDataset.addValue(avaluoTemp, "año",
                    new Integer(cal.get(Calendar.YEAR)));
                lowerBound = avaluoTemp;
                upperBound = avaluoTemp * 1.1;
                avaluos[posicion] = avaluoTemp;
            } else {

                defaultCategoryDataset.addValue(avaluoTemp, "año",
                    new Integer(cal.get(Calendar.YEAR)));
                lowerBound = (avaluoTemp < lowerBound) ? avaluoTemp : lowerBound;
                avaluos[posicion] = avaluoTemp;
            }

            posicion++;
        }

        chart = ChartFactory.createLineChart("Comportamiento del avalúo del predio", "Año",
            "precio $", defaultCategoryDataset, PlotOrientation.VERTICAL, true, true, false);

        //D: adornar el mamarracho
        subtitle = new TextTitle("Valor del predio según el año");
        subtitle.setPosition(RectangleEdge.BOTTOM);
        subtitle.setHorizontalAlignment(HorizontalAlignment.RIGHT);
        chart.addSubtitle(subtitle);

        plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.white);

        renderer = (LineAndShapeRenderer) plot.getRenderer();
        renderer.setBaseShapesVisible(true);
        renderer.setDrawOutlines(true);

        //D: asignar loa valores límite para el eje Y
        ejeY = (NumberAxis) plot.getRangeAxis();
        // ... para que no quede la gráfica sobre los límites
        desviacionEstandar = UtilidadesWeb.calcularDesviacionEstandar(avaluos);
        upperBound = upperBound + desviacionEstandar;
        lowerBound = (lowerBound - desviacionEstandar > 0.0D) ?
            lowerBound - desviacionEstandar : 0.0D;
        ejeY.setRange(lowerBound, upperBound);

        //D: crear el archivo
        chartFile = new File(UtilidadesWeb.obtenerRutaTemporalArchivos(),
            "historicoAvaluo_" + System.currentTimeMillis() + ".png");
        LOGGER.debug("Archivo a crear " + chartFile.getAbsolutePath());

        try {
            ChartUtilities.saveChartAsPNG(chartFile, chart,
                defaultCategoryDataset.getColumnCount() * 100, 400);
            rutaArchivoGeneradoImagenHistoricoAvaluo = chartFile.getAbsolutePath();

            //N: hubo que subir el archivo generado a la carpeta web temporal porque si se hacía con
            // un DefaultStreamedContent usado como value del p:graphigImage, funcionaba bien la primera
            // vez, pero al cambiar de tab y volver, la imagen no se veía
            servicioDocumental = DocumentalServiceFactory.getService();
            documentoVo = servicioDocumental.cargarDocumentoWorkspacePreview(
                rutaArchivoGeneradoImagenHistoricoAvaluo);
            this.urlArchivoGeneradoImagenHistoricoAvaluo = documentoVo.getUrlPublico();
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }

        //this.chartAsStreamedContent = new DefaultStreamedContent(new FileInputStream(chartFile), "image/png", chartFile.getName());
    }

    /**
     * Método get que consulta la resolución en alfresco de las resoluciones asociadas a los
     * historicos de avaluo
     *
     * @author leidy.gonzalez
     */
    public void cargarUrlDocumentoConMarcaDeAgua() {

        if (this.historicoAvaluos != null) {
            for (HistoricoAvaluosVO historicoAvaluosVO : this.historicoAvaluos) {
                if (historicoAvaluosVO.isResolucion() == true) {

                    documentoResolucion = this.getGeneralesService().
                        buscarDocumentoPorNumeroDocumentoAndTipoDocumento(historicoAvaluosVO.
                            getDecretoNumeroOResolucion());

                    if (documentoResolucion != null && documentoResolucion.
                        getIdRepositorioDocumentos() != null) {

                        this.reporteDocumentoWaterMark = this.reportsService.
                            consultarReporteDeGestorDocumental(
                                documentoResolucion.getIdRepositorioDocumentos());

                    }
                }
            }

        }

    }
//end of class    

}
