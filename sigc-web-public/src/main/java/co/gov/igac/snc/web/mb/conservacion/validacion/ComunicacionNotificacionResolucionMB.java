/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.HorarioAtencionTerritorial;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.sigc.documental.impl.procesador.ProcesadorDocumentosImpl;
import co.gov.igac.sigc.documental.interfaces.IProcesadorDocumentos;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatriz;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteTramite;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDias;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EPersonaTipoIdentificacion;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteRelacionNoti;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitanteTipoSolicitant;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.web.util.procesos.ProcesosUtilidadesWeb;

/**
 * Managed bean para el caso de uso Comunicar notificación de resolución
 *
 * @author pedro.garcia
 *
 */
/*
 * @modified by leidy.gonzalez :: #14396 :: 29/09/2015 Se agrega campo persona que atendio.
 */
@Component("comunicacionNotificacionResolucion")
@Scope("session")
public class ComunicacionNotificacionResolucionMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -840799982185654970L;

    private static final Logger LOGGER =
        LoggerFactory.getLogger(ComunicacionNotificacionResolucionMB.class);

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * trámite sober el que se está trabajando
     */
    private Tramite currentTramite;

    /**
     * id del trámite seleccionado de la tabla
     */
    private long currentTramiteId;

    /**
     * lista con los solicitantes del trámite a quienes se debe enviar la comunicación de
     * notificación
     */
    private List<Solicitante> solicitantesEnvioComunicacionNotificacion;
    /**
     * Solicitantes seleccionados para notificacion
     */
    private Solicitante[] solicitantesSelecionados;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    /**
     * Estructura organizacional del usuario en sesión
     */
    private EstructuraOrganizacional estructuraOrganizacional;

    /**
     * lista que contiene los TramiteDocumento del trámite que sean del tipo comunicacion de
     * notificación de resolución.
     */
    private ArrayList<TramiteDocumento> currentTramiteTramiteDocumentosComNotReso;

    /**
     * Solicitante seleccionado de la tabla de solicitantes a quien se envía la comunicación de
     * notificación
     */
    private Solicitante solicitanteOAfectadoSeleccionado;

    /**
     * lista de usuarios con el rol de responsables de conservación
     */
    private List<UsuarioDTO> responsablesConservacion;

    //-----   BPM  --------
    private Actividad currentProcessActivity;

    /**
     * lista de usuarios destino de la actividad
     */
    private List<UsuarioDTO> usuariosRol;

    private String validateProcessErrorMessage;

    /**
     * Datos comunicación de notificación
     */
    private Date fechaHoy;

    private Date fechaComunicNotif;

    private String medioComunicNotif;

    private String observacionesComunicNotif;

    private String personaAtendio;

    //-----------   flags    ----------------
    /**
     * indica si el documento de comunicación de notificación fue radicado
     */
    private boolean docNotificacionRadicado;

    /**
     * indica si para este trámite ya se generó la comunicación de notificación y esta pendiente el
     * registro de la misma. Se sabe que sí si existe algún TramiteDocumento para este trámite con
     * fecha de notificación null y con un documento de tipo 'Comunicación de notificación asociado'
     */
    private boolean radicadoPrimeraVezBool;

    /**
     * indica si queda pendiente solo uno de los solicitantes para el registro de comunicación de
     * notificación
     */
    private boolean lastSolicitanteForRegistroComunicNotif;

    /**
     * indica si el reporte se generó de forma correcta. Hubo que usar este flag porque los métodos
     * valor de un action deben devolver void o String
     */
    private boolean reporteGeneradoOK;

    /**
     * Variable que indica si ya fueron registrados todos los solicitantes, o afectados
     */
    private boolean todosRegistrados;

    /**
     * indica si ese botón debe estar deshabilitado
     */
    private boolean disabledRegistrarComunicButton;

    //-------    reportes  ----------
    /**
     * número de radicación del documento de comunicación de la notificación
     */
    private String numeroRadicacionDocComNot;
    /**
     * Numeros masivos
     */
    private Map<Long, String> numerosRadicacionDocComNot;

    /**
     * Numeros masivos
     */
    private Map<Long, String> rutasocumentosRadicados;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de comunicación de la notificación
     */
    private ReporteDTO reporteComunicacionNotificacion;

    /**
     * objeto Documento que se guarda en la base de datos
     */
    private Documento documentoComunicacionNotificacion;
    /**
     * objeto Documento que se guarda en la base de datos
     */
    private Map<Long, Documento> documentosComunicacionNotificacion;

    /**
     * Variable usada para ativar los botones de edición o eliminación de un afectado.
     */
    private boolean solicitanteSelectBool;

    /**
     * Variable usada para distinguir si un afectado es nuevo o esta siendo modificado
     */
    private boolean editModeBool;

    /**
     * Determina si el solicitante seleccionado para edicion es un propietario
     */
    private boolean banderaPropietario;

    /**
     * Datos geográficos del aviso
     */
    private Pais paisSolicitante;
    private Departamento departamentoSolicitante;
    private Municipio municipioSolicitante;

    /**
     * Bandera que determina la visualización de los datos del afectado solo si este se ha buscado
     * primero en el sistema
     */
    private boolean banderaBusquedaAfectadoRealizada;

    /**
     * Solicitante del trámite seleccionado
     */
    private boolean solicitanteTramite;

    /**
     * Solicitud que se encuentra seleccionada
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Variable usada para saber si ya se radicó la comunicación de la notificación para un
     * solicitante seleccionado.
     */
    private boolean radicadoBool = false;

    /**
     * Variable usada para saber si ya se registró la comunicación de la notificación para un
     * solicitante especifico,.
     */
    private boolean registradoBool = false;

    /**
     * Variable usada para habilidar el botón de mover el proceso una vez registrada la última
     * comunicación.
     */
    private boolean mostrarBotonMover;

    /**
     * Variable item list con los valores del tipo de solicitante
     *
     * @return
     */
    private List<SelectItem> tipoSolicitanteItemList;

    /**
     * Variable usada para saber si se están seleccionando todos los solicitantes de la tabla
     */
    private boolean selectedAllBool;

    //--------------  methods   ---------------
    public boolean isDisabledRegistrarComunicButton() {
        return this.disabledRegistrarComunicButton;
    }

    public String getPersonaAtendio() {
        return personaAtendio;
    }

    public void setPersonaAtendio(String personaAtendio) {
        this.personaAtendio = personaAtendio;
    }

    public void setDisabledRegistrarComunicButton(boolean disabledRegistrarComunicButton) {
        this.disabledRegistrarComunicButton = disabledRegistrarComunicButton;
    }

    public boolean isBanderaPropietario() {
        return banderaPropietario;
    }

    public void setBanderaPropietario(boolean banderaPropietario) {
        this.banderaPropietario = banderaPropietario;
    }

    public Solicitante[] getSolicitantesSelecionados() {
        return solicitantesSelecionados;
    }

    public void setSolicitantesSelecionados(Solicitante[] solicitantesSelecionados) {
        this.solicitantesSelecionados = solicitantesSelecionados;
    }

    public ReporteDTO getReporteComunicacionNotificacion() {
        return reporteComunicacionNotificacion;
    }

    public void setReporteComunicacionNotificacion(
        ReporteDTO reporteComunicacionNotificacion) {
        this.reporteComunicacionNotificacion = reporteComunicacionNotificacion;
    }

    public boolean isLastSolicitanteForRegistroComunicNotif() {
        return this.lastSolicitanteForRegistroComunicNotif;
    }

    public void setLastSolicitanteForRegistroComunicNotif(
        boolean lastSolicitanteForRegistroComunicNotif) {
        this.lastSolicitanteForRegistroComunicNotif = lastSolicitanteForRegistroComunicNotif;
    }

    public boolean isMostrarBotonMover() {
        return mostrarBotonMover;
    }

    public void setMostrarBotonMover(boolean mostrarBotonMover) {
        this.mostrarBotonMover = mostrarBotonMover;
    }

    public boolean isSelectedAllBool() {
        return selectedAllBool;
    }

    public void setSelectedAllBool(boolean selectedAllBool) {
        this.selectedAllBool = selectedAllBool;
    }

    public Solicitante getSolicitanteOAfectadoSeleccionado() {
        return this.solicitanteOAfectadoSeleccionado;
    }

    public void setSolicitanteOAfectadoSeleccionado(Solicitante solicitanteOAfectadoSeleccionado) {
        this.solicitanteOAfectadoSeleccionado = solicitanteOAfectadoSeleccionado;
    }

    public boolean isRadicadoPrimeraVezBool() {
        return this.radicadoPrimeraVezBool;
    }

    public void setRadicadoPrimeraVezBool(boolean radicadoPrimeraVezBool) {
        this.radicadoPrimeraVezBool = radicadoPrimeraVezBool;
    }

    public String getMedioComunicNotif() {
        return this.medioComunicNotif;
    }

    public void setMedioComunicNotif(String medioComunicNotif) {
        this.medioComunicNotif = medioComunicNotif;
    }

    public List<Solicitante> getSolicitantesEnvioComunicacionNotificacion() {
        return this.solicitantesEnvioComunicacionNotificacion;
    }

    public void setSolicitantesEnvioComunicacionNotificacion(
        List<Solicitante> solicitantesEnvioComunicacionNotificacion) {
        this.solicitantesEnvioComunicacionNotificacion = solicitantesEnvioComunicacionNotificacion;
    }

    public Date getFechaComunicNotif() {
        return this.fechaComunicNotif;
    }

    public void setFechaComunicNotif(Date fechaComunicNotif) {
        this.fechaComunicNotif = fechaComunicNotif;
    }

    public String getObservacionesComunicNotif() {
        return this.observacionesComunicNotif;
    }

    public void setObservacionesComunicNotif(String observacionesComunicNotif) {
        this.observacionesComunicNotif = observacionesComunicNotif;
    }

    public boolean isRadicadoBool() {
        return radicadoBool;
    }

    public void setRadicadoBool(boolean radicadoBool) {
        this.radicadoBool = radicadoBool;
    }

    public boolean isRegistradoBool() {
        return registradoBool;
    }

    public void setRegistradoBool(boolean registradoBool) {
        this.registradoBool = registradoBool;
    }

    public Date getFechaHoy() {
        return this.fechaHoy;
    }

    public void setFechaHoy(Date fechaHoy) {
        this.fechaHoy = fechaHoy;
    }

    public boolean isDocNotificacionRadicado() {
        return this.docNotificacionRadicado;
    }

    public void setDocNotificacionRadicado(boolean docNotificacionRadicado) {
        this.docNotificacionRadicado = docNotificacionRadicado;
    }

    public Tramite getCurrentTramite() {
        return this.currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
    }

    public boolean isEditModeBool() {
        return editModeBool;
    }

    public void setEditModeBool(boolean editModeBool) {
        this.editModeBool = editModeBool;
    }

    public boolean isSolicitanteSelectBool() {
        return solicitanteSelectBool;
    }

    public void setSolicitanteSelectBool(boolean solicitanteSelectBool) {
        this.solicitanteSelectBool = solicitanteSelectBool;
    }

    public Pais getPaisSolicitante() {
        return paisSolicitante;
    }

    public void setPaisSolicitante(Pais paisSolicitante) {
        this.paisSolicitante = paisSolicitante;
    }

    public Departamento getDepartamentoSolicitante() {
        return departamentoSolicitante;
    }

    public void setDepartamentoSolicitante(Departamento departamentoSolicitante) {
        this.departamentoSolicitante = departamentoSolicitante;
    }

    public Municipio getMunicipioSolicitante() {
        return municipioSolicitante;
    }

    public void setMunicipioSolicitante(Municipio municipioSolicitante) {
        this.municipioSolicitante = municipioSolicitante;
    }

    public boolean isBanderaBusquedaAfectadoRealizada() {
        return banderaBusquedaAfectadoRealizada;
    }

    public void setBanderaBusquedaAfectadoRealizada(
        boolean banderaBusquedaAfectadoRealizada) {
        this.banderaBusquedaAfectadoRealizada = banderaBusquedaAfectadoRealizada;
    }

    public boolean isSolicitanteTramite() {
        return solicitanteTramite;
    }

    public void setSolicitanteTramite(boolean solicitanteTramite) {
        this.solicitanteTramite = solicitanteTramite;
    }

    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<SelectItem> getTipoSolicitanteItemList() {
        return tipoSolicitanteItemList;
    }

    public void setTipoSolicitanteItemList(List<SelectItem> tipoSolicitanteItemList) {
        this.tipoSolicitanteItemList = tipoSolicitanteItemList;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on ComunicacionNotificacionResolucionMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        this.estructuraOrganizacional = this
            .getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                this.currentUser.getCodigoEstructuraOrganizacional());

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "comunicacionNotificacionResolucion");

        //D: se obtienen el id del trámite objetivo a partir del árbol de tareas
        this.currentTramiteId =
            this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();

        //Obtener la actividad actual:
        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.currentTramiteId);

        //D: buscar los trámites
        this.currentTramite =
            this.getTramiteService().buscarTramiteSolicitudPorId(this.currentTramiteId);

        List<TramiteDocumento> documentosPruebas = this.getTramiteService()
            .obtenerTramiteDocumentosDePruebasPorTramiteId(
                this.currentTramite.getId());
        if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
            this.currentTramite.setDocumentosPruebas(documentosPruebas);
        }

        // Cargue de solicitantes y afectados
        cargarListaSolicitantesOAfectados();

        this.solicitudSeleccionada = this.getTramiteService()
            .buscarSolicitudConSolicitantesSolicitudPorId(
                this.currentTramite.getSolicitud().getId());

        // D: verificar si ya se había hecho la comunicación y está pendiente el registro de la misma:
        //   para ello se buscan TramiteDocumento de ese tipo para este trámite
        this.currentTramiteTramiteDocumentosComNotReso = (ArrayList<TramiteDocumento>) this.
            getTramiteService().obtenerTramiteDocumentosDeTramitePorTipoDoc(
                this.currentTramiteId, ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId());

        //Determina las notificaciones radicadas
        this.determinaSolicitantesNotificacion();

        this.radicadoPrimeraVezBool =
            (this.currentTramiteTramiteDocumentosComNotReso == null ||
            this.currentTramiteTramiteDocumentosComNotReso.isEmpty()) ? false : true;

        //D: se marcan los solicitantes a quienes ya se les hizo registro de la comunicación de notif
        //   Solo hay necesidad de hacerlo si hay pendientes. Si no, pone false por defecto
        if (this.radicadoPrimeraVezBool) {
            this.marcarSolicitantesConRegistroComunic();
            this.radicadoBool = true;
        }

        this.disabledRegistrarComunicButton = !this.radicadoPrimeraVezBool ||
             (this.radicadoPrimeraVezBool && this.todosRegistrados);

        this.disabledRegistrarComunicButton = !this.radicadoPrimeraVezBool ||
             (this.radicadoPrimeraVezBool && this.todosRegistrados);

        this.fechaHoy = new Date();
        this.reporteGeneradoOK = false;
        this.mostrarBotonMover = true;

        // D: mover el proceso cuando es el último (o único) por registrar
        // la comunicación
        if (this.lastSolicitanteForRegistroComunicNotif &&
             this.todosRegistrados) {
            mostrarBotonMover = false;
        }
        this.tipoSolicitanteItemList = new ArrayList<SelectItem>();
        for (Dominio dominio : this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.SOLICITANTE_TIPO_SOLICITANT)) {
            if (!ESolicitanteTipoSolicitant.GRUPO_INTERNO_DE_TRABAJO
                .getCodigo().equals(dominio.getCodigo()) &&
                 !ESolicitanteTipoSolicitant.TERRITORIAL_AREA_DE_CONSERVACION
                    .getCodigo().equals(dominio.getCodigo())) {
                this.tipoSolicitanteItemList.add(new SelectItem(dominio
                    .getCodigo(), dominio.getValor()));
            }
        }

    }

    //-------------------------------------------------------------------------------------------- //
    /**
     * Método que carga la lista de los solicitantes y afectados de un trámite basandose en el tipo
     * de trámite
     */
    /*
     * @modified by juanfelipe.garcia :: 22-11-2013 :: Si el solicitante no tiene direccion se le
     * asocia la del predio(5956)
     */
 /*
     * @modified by leidy.gonzalez :: 13-03-2015 :: :: 5956 :: Se modifica el solicitante para que
     * busque la direccion en los solicitantes que se esta visualizando en la pagina y se actualiza
     * la informacion del solicitante.
     */
    public void cargarListaSolicitantesOAfectados() {

        //D: la regla dice que si existen solicitantes en la solicitud y en el trámite debe enviarse
        //   a ambos, exceptuando el caso en que la solicitud es de tipo Aviso, en cuyo caso se le
        //   envía a los del trámite.
        //   La otra parte de la regla dice que solo se le envía a uno de los solicitantes.
        //   Ver documentación del método TramiteBean#selectSolicitanteParaEnvioComunicNotif
        if (this.currentTramite.getSolicitud().getTipo().
            equalsIgnoreCase(ESolicitudTipo.AVISOS.getCodigo())) {

            //D: cuando es de avisos traigo los de trámite
            this.solicitantesEnvioComunicacionNotificacion =
                (ArrayList<Solicitante>) this.getTramiteService().
                    obtenerSolicitsParaEnvioComunicNotif(this.currentTramiteId,
                        this.currentTramite.getSolicitud().getId(), 2);
        } else {
            // Se traen los solicitantes de la solicitud y los afectados
            this.solicitantesEnvioComunicacionNotificacion = (ArrayList<Solicitante>) this
                .getTramiteService().obtenerSolicitsParaEnvioComunicNotif(
                    this.currentTramiteId,
                    this.currentTramite.getSolicitud().getId(), 5);
            //Si el solicitante no tiene direccion se le asocia la del predio
            for (Solicitante ss : this.solicitantesEnvioComunicacionNotificacion) {

                if (ss.getDireccion() == null ||
                     ss.getDireccion().trim().isEmpty()) {
                    Predio p;
                    if (this.currentTramite.isQuinta()) {
                        p = this.getConservacionService().
                            obtenerPredioConDatosUbicacionPorNumeroPredial(this.currentTramite.
                                getNumeroPredial());
                    } else {
                        p = this.getConservacionService().
                            obtenerPredioConDatosUbicacionPorNumeroPredial(this.currentTramite.
                                getPredio().getNumeroPredial());
                    }
                    if (p != null) {
                        String direccionPredio = p.getDireccionPrincipal();
                        ss.setDireccion(direccionPredio);
                    }
                    ss = this.getTramiteService().guardarActualizarSolicitante(ss);

                }
            }
        }

        //felipe.cadena::07-07-2015::Se cargan los propietarios como afectados para la notificacion para tramites de rectificacion y tercera masivas
        //leidy.gonzalez::05/10/2017::Se adiciona cargue de propietarios para desenglobe
        if (this.currentTramite.isRectificacionMatriz() || this.currentTramite.isTerceraMasiva() ||
             (this.currentTramite.isDesenglobeEtapas() && !this.currentTramite.isEnglobeVirtual())) {
            List<Solicitante> afectadosPropietarios = this.getTramiteService().
                obtenerSolicitantesParaNotificacion(this.currentTramiteId);

            this.solicitantesEnvioComunicacionNotificacion.addAll(afectadosPropietarios);
        }
        //leidy.gonzalez::0/06/2018::#65337::Se adiciona cargue de propietarios 
        // de predios 0 y para englobe Virtual
        if (this.currentTramite.isEnglobeVirtual()) {
            List<Solicitante> propietariosEV = this.getTramiteService().
                obtenerSolicitantesParaNotificacionEV(this.currentTramiteId);

            this.solicitantesEnvioComunicacionNotificacion.addAll(propietariosEV);
        }

        if (this.solicitantesEnvioComunicacionNotificacion == null) {
            // D: no debería pasar
            this.addMensajeError("No hay solicitantes para este trámite.");
            this.solicitantesEnvioComunicacionNotificacion = new ArrayList<Solicitante>();
            return;
        } else {
            this.lastSolicitanteForRegistroComunicNotif =
                (this.solicitantesEnvioComunicacionNotificacion
                    .size() == 1) ? true : false;
        }
    }

    /**
     * action del botón "radicar" de la pantalla con el doc de comunicación notificación
     *
     * 1. obtiene número de radicado del doc 2. vuelve a generar el reporte con en número de
     * radicado 3. guarda el documento en el gestor documental e inserta el respectivo registro en
     * BD 4. crear los registros en la tabla Tramite_Documento 5. enviar correo al solicitante
     *
     * @modified felipe.cadena -- Se cambia la funcionalidad para radicaciones masivas de documentos
     */
    public void radicarComunicNotif() {

        this.numerosRadicacionDocComNot = new HashMap<Long, String>();
        this.documentosComunicacionNotificacion = new HashMap<Long, Documento>();
        int exceptionIdentifier = 0;
        boolean errorRadicando = false;

        try {
            // 1. Generación del número de Radicación en correspondencia
            // (sticker de radicación)

            String numeroRadicacion = null;
            //felipe.cadena::08-07-2015::Se generan los numeros para todos los solicitantes seleccionados
            for (Solicitante solicitante : this.solicitantesSelecionados) {
                numeroRadicacion = this.obtenerNumeroRadicado(solicitante);

                this.numerosRadicacionDocComNot.put(solicitante.getId(), numeroRadicacion);

                //Verificar si se pudo generar el número de radicación para el solicitante
                if (numeroRadicacion == null) {
                    this.addMensajeError(ECodigoErrorVista.NOTIFICACION_AVISO_009.toString() +
                        solicitante.getTipoIdentificacion() +
                         " " + solicitante.getNumeroIdentificacion());
                    LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_009.getMensajeTecnico() +
                        "Solicitante id: " + solicitante.getId());
                    errorRadicando = true;
                    continue;
                }
                this.numerosRadicacionDocComNot.put(solicitante.getId(), numeroRadicacion);
            }

            if (this.numerosRadicacionDocComNot == null || this.numerosRadicacionDocComNot.isEmpty()) {
                exceptionIdentifier = 1;
                throw new Exception("No se pudo generar el número de radicación");
            }

            // 2. Generación del reporte con el número de radicación
            this.generarReporteComunicacionNotificacion();
            if (!this.reporteGeneradoOK) {
                exceptionIdentifier = 2;
                throw new Exception(
                    "No se pudo generar el reporte de comunicación de la notificación");
            }

            //D: 3.
            for (Solicitante solicitante : this.solicitantesSelecionados) {

                //Verificar si se pudo generar el número de radicación para el solicitante, sino no hace nada
                if (this.numerosRadicacionDocComNot != null &&
                    this.numerosRadicacionDocComNot.get(solicitante.getId()) == null) {
                    continue;
                }

                if (!this.guardarDocumentoComunicNotif(solicitante)) {
                    exceptionIdentifier = 3;
                    throw new Exception(
                        "No se pudo guardar el documento del reporte de comunicación de la notificación");
                }
            }

            //D: 4.
            for (Solicitante solicitante : this.solicitantesSelecionados) {

                //Verificar si se pudo generar el número de radicación para el solicitante, sino no hace nada
                if (this.numerosRadicacionDocComNot != null &&
                    this.numerosRadicacionDocComNot.get(solicitante.getId()) == null) {
                    continue;
                }

                if (!this.guardarTramiteDocumentosComunicNotif(solicitante)) {
                    exceptionIdentifier = 1;
                    throw new Exception(
                        "Ocurrió algún error al guardar registros en la tabla Tramite_Documento");
                }
            }

            // D: 5.
            for (Solicitante solicitante : this.solicitantesSelecionados) {

                //Verificar si se pudo generar el número de radicación para el solicitante, sino no hace nada
                if (this.numerosRadicacionDocComNot != null &&
                    this.numerosRadicacionDocComNot.get(solicitante.getId()) == null) {
                    continue;
                }

                if (!this.enviarCorreo(solicitante)) {
                    this.addMensajeWarn(
                        "No se pudo enviar el correo electrónico. Por favor seleccione una opción de envio diferente al momento de registrar la comunicación de notificación.");
                    // Se debe permitir de completar el proceso de radicación, pero
                    // se debe informar al usuario que no se pudo enviar el correo.

                    exceptionIdentifier = 4;
                    // throw new Exception("Ocurrió un error al enviar el correo al solicitante o afectado");
                }
            }

            if (!errorRadicando) {
                this.docNotificacionRadicado = true;
                this.radicadoBool = true;
                this.addMensajeInfo("Se radicó la comunicación de notificación satisfactoriamente.");
                if (numeroRadicacion != null) {
                    cerrarRadicacionTramite(solicitudSeleccionada.getNumero(), numeroRadicacion,
                        currentUser.getLogin());
                }
            }
        } catch (Exception ex) {
            switch (exceptionIdentifier) {
                case (1): {
                    throw SNCWebServiceExceptions.EXCEPCION_0001.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                case (2): {
                    throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                case (3): {
                    throw SNCWebServiceExceptions.EXCEPCION_0003.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                case (4): {
                    //throw SNCWebServiceExceptions.EXCEPCION_0004.getExcepcion(LOGGER, ex, ex.getMessage());
                }
                default: {
                    break;
                }
            }
        }

        //D: hay que volver a buscar los TramiteDocumento para refrescar el atributo de registro de
        //   los notificantes
        this.currentTramiteTramiteDocumentosComNotReso = (ArrayList<TramiteDocumento>) this.
            getTramiteService().obtenerTramiteDocumentosDeTramitePorTipoDoc(
                this.currentTramiteId, ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId());

        this.radicadoPrimeraVezBool =
            (this.currentTramiteTramiteDocumentosComNotReso == null ||
            this.currentTramiteTramiteDocumentosComNotReso.isEmpty()) ? false : true;

        // Despues de radicar el comportamiento de activación de los botones es:
        this.radicadoBool = true;
        this.registradoBool = false;

        // Reinicio el valor del número de radicado
        this.numeroRadicacionDocComNot = null;

        // Reinicio el valor de los números de readicación masivos
        this.numerosRadicacionDocComNot = null;
    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     *
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     */
    private String obtenerNumeroRadicado(Solicitante solicitante) {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.currentUser.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.currentUser.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Comunicación de notificación"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(solicitante.getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(solicitante.getDireccion()); // Direccion destino
        parametros.add(solicitante.getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(solicitante.getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }

    //------------------------------------------------------------------------------------------//
    /**
     * + action del botón "generar comunicación notificación" + también se llama desde el método
     * radicarComunicNotif para que vuelva a generar el reporte ya con el número de radicado
     *
     * genera el reporte que contiene el el reporte de solicitud de documentos Hay dos opciones para
     * generarlo, dependiendo de si el documento ya fue radicado.
     *
     * En el caso en que ya se radicó primero radica el memorando y genera el reporte ya con el
     * número de radicación, en el caso contrario genera el reporte sin el número de radicado
     *
     * @author javier.aponte
     * @modified pedro.garcia
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarReporteComunicacionNotificacion() {
        FirmaUsuario firma = null;
        String documentoFirma = "";

        boolean comunicacionRadicadaBool = false;

        this.rutasocumentosRadicados = new HashMap<Long, String>();

        //Verificar si para alguno de los seleccionados ya se genero la notificacion
        if (this.solicitantesSelecionados == null || this.solicitantesSelecionados.length == 0) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_MASIVO_001.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.warn(ECodigoErrorVista.NOTIFICACION_MASIVO_001.getMensajeTecnico());
            return;
        }
        int numNotificados = 0;
        for (Solicitante solicitante : this.solicitantesSelecionados) {
            if (ESiNo.SI.getCodigo().equals(solicitante.getSolicitanteNotificado())) {
                numNotificados++;
            }
        }
        if (numNotificados > 0 && numNotificados < this.solicitantesSelecionados.length) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_MASIVO_002.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.warn(ECodigoErrorVista.NOTIFICACION_MASIVO_002.getMensajeTecnico());
            return;
        }

//		// Verificar que el solicitante o afectado seleccionado no sea nulo
//		if(this.solicitanteOAfectadoSeleccionado == null){
//			this.addMensajeWarn("Por favor seleccione antes un solicitante o afectado.");
//			RequestContext context = RequestContext.getCurrentInstance();
//			context.addCallbackParam("error", "error");
//			return;
//		}
//        notificacionesDeResolucionesTemp = (ArrayList<TramiteDocumento>)
//                this.getTramiteService().obtenerTramiteDocumentosDeTramitePorTipoDoc(
//                this.currentTramiteId, ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId());
        // Si el documento ya se radico anteriormente, se debe cargar el
        // radicado, y no generar uno nuevo.
//		if(notificacionesDeResolucionesTemp != null && !notificacionesDeResolucionesTemp.isEmpty()){
//			comunicacionRadicadaBool = comunicacionRadicadaParaEsteSolicitante();
//		}
        comunicacionRadicadaBool = comunicacionRadicadaParaEsteSolicitanteMasiva();
        if (comunicacionRadicadaBool) {
            // SE ENCONTRÓ LA COMUNICACIÓN Y FUE BUSCADA EN ALFRESCO
            this.radicadoBool = true;
            LOGGER.debug("Url público del archivo:" + this.reporteComunicacionNotificacion.
                getUrlWebReporte());
        } else {

            // No se encontró la comunicación.
            EReporteServiceSNC enumeracionReporte;
            Map<String, String> parameters;
            UsuarioDTO responsableConservacion = null;
            List<String> rutasMemorandos = new ArrayList<String>();

            //felipe.cadena::23-07-2015::Se solicita el repote adecuado dependientdo del tramite
            if (this.currentTramite.isRectificacionMatriz() ||
                 this.currentTramite.isTerceraMasiva() ||
                 this.currentTramite.isQuintaMasivo() ||
                 this.currentTramite.isDesenglobeEtapas()) {
                enumeracionReporte = EReporteServiceSNC.COMUNICACION_DE_NOTIFICACION_PERSONAL_MASIVA;
            } else {
                enumeracionReporte = EReporteServiceSNC.COMUNICACION_DE_NOTIFICACION_PERSONAL;
            }

            if (this.responsablesConservacion == null) {
                this.responsablesConservacion = (List<UsuarioDTO>) this
                    .getTramiteService()
                    .buscarFuncionariosPorRolYTerritorial(
                        this.currentUser
                            .getDescripcionEstructuraOrganizacional(),
                        ERol.RESPONSABLE_CONSERVACION);
            }

            if (this.responsablesConservacion != null &&
                 !this.responsablesConservacion.isEmpty() &&
                 this.responsablesConservacion.get(0) != null) {
                responsableConservacion = this.responsablesConservacion.get(0);

                firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                    responsableConservacion.getNombreCompleto());

                if (firma != null && responsableConservacion.getNombreCompleto().equals(firma.
                    getNombreFuncionarioFirma())) {

                    documentoFirma = this.getGeneralesService().
                        descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                            getIdRepositorioDocumentos());
                }
            }

            for (Solicitante solicitanteSeleccionado : this.solicitantesSelecionados) {

                //Verificar si se pudo generar el número de radicación para el solicitante, sino no hace nada
                if (this.numerosRadicacionDocComNot != null &&
                    this.numerosRadicacionDocComNot.get(solicitanteSeleccionado.getId()) == null) {
                    continue;
                }

                parameters = new HashMap<String, String>();

                if (!documentoFirma.isEmpty()) {
                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }

                // Parametro del trámite
                parameters.put("TRAMITE_ID", String.valueOf(this.currentTramite.getId()));

                // Parametro del solicitante
                parameters.put("SOLICITANTE_ID", String.valueOf(solicitanteSeleccionado.getId()));

                //SE VALIDA PARAMETRO DE HORARIO PARA CADA TERRITOTIAL
                //#16397 :: 31/03/2016
                validaHorarioTerritorial(parameters);

                if (responsableConservacion != null &&
                     responsableConservacion.getNombreCompleto() != null) {
                    parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                        responsableConservacion.getNombreCompleto());
                }

                if (this.numerosRadicacionDocComNot != null &&
                    this.numerosRadicacionDocComNot.get(solicitanteSeleccionado.getId()) != null) {
                    parameters.put("NUMERO_RADICADO",
                        this.numerosRadicacionDocComNot.get(solicitanteSeleccionado.getId()));
                } else {
                    parameters.put("NUMERO_RADICADO", "");
                }

                if (this.estructuraOrganizacional != null && this.estructuraOrganizacional.
                    getDireccion() != null) {
                    parameters.put("DIRECCION", estructuraOrganizacional.getDireccion());
                }

                ReporteDTO reporte = this.reportsService.generarReporte(
                    parameters, enumeracionReporte, this.currentUser);

                if (reporte != null) {
                    rutasMemorandos.add(reporte.getRutaCargarReporteAAlfresco());
                    this.rutasocumentosRadicados.put(solicitanteSeleccionado.getId(), reporte.
                        getRutaCargarReporteAAlfresco());
                } else {

                    Exception ex = new Exception(Constantes.MENSAJE_ERROR_REPORTE);

                    // Verificar desde donde se está invocando el método, si se
                    // invoca desde el botón de radicar, se debe lanzar la excepción al
                    // método radicar, pero si se invoca desde el botón de generar la
                    // comunicación,se muestra el mensaje de error.
                    if (this.numeroRadicacionDocComNot != null &&
                         !this.numeroRadicacionDocComNot.trim().isEmpty()) {
                        // Se está llamando con un numero de radicado, por lo
                        // quq se
                        // llamo desde el método radicar.
                        throw SNCWebServiceExceptions.EXCEPCION_0002
                            .getExcepcion(LOGGER, ex, enumeracionReporte.getNombreReporte());
                    } else {
                        // Se llamó desde el botón generar
                        this.addMensajeError("Error generando la comunicación de notificación.");
                        RequestContext context = RequestContext
                            .getCurrentInstance();
                        context.addCallbackParam("error", "error");
                        return;
                    }
                }
            }
            this.reporteGeneradoOK = true;
            this.radicadoBool = false;
            if (!rutasMemorandos.isEmpty()) {
                IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
                String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
                this.reporteComunicacionNotificacion =
                     this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
            }
            //---------------------------------------//
            //---------------------------------------//

        }
    }

    /*
     * @autor leidy.gonzalez Metodo que valida el horario ingresado en la territorial #16397 ::
     * 31/03/2016
     */
    public void validaHorarioTerritorial(Map<String, String> parameters) {

        try {

            String horarioParam = "";
            Object[] consultaProcedimientoHorarioAtencion = this.getConservacionService().
                consultarHorarioAtencionPorEstructuraOrganizacionalCodigo(currentUser.
                    getCodigoEstructuraOrganizacional());

            if (consultaProcedimientoHorarioAtencion != null) {

                horarioParam = consultaProcedimientoHorarioAtencion[0].toString();

                parameters.put("HORARIO", horarioParam + ".");

            }

        } catch (Exception e) {

            this.addMensajeError(ECodigoErrorVista.COMUNICA_NOTIFICACION_001.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(ECodigoErrorVista.COMUNICA_NOTIFICACION_001.getMensajeTecnico(), e);
            return;
        }

    }

    // ----------------------------------------------------- //
    /**
     * Método que verifica si ya se radico la comunicación para el solicitante o afectado
     * seleccionado. De ser así, la consulta en los TramiteDocumentos del trámite, si no returna
     * false y procede a la generación del reporte de la comunicación. Método que verifica si ya se
     * radico la comunicación para el solicitante o
     *
     * @author david.cifuentes
     */
    public boolean comunicacionRadicadaParaEsteSolicitante() {
        boolean comunicacionRadicadaBool = false;
        try {
            for (TramiteDocumento td : this.currentTramiteTramiteDocumentosComNotReso) {
                if (td.getIdPersonaNotificacion().contains(
                    this.solicitanteOAfectadoSeleccionado
                        .getNumeroIdentificacion())) {

                    comunicacionRadicadaBool = true;
                    // ---------------------------------------//
                    // Cargar el reporte encontrado.
                    // ---------------------------------------//

                    if (td.getDocumento() == null ||
                         td.getIdRepositorioDocumentos() == null ||
                         td.getIdRepositorioDocumentos()
                            .trim().isEmpty()) {

                        // No debería entrar acá.
                        this.addMensajeError("No se encontró el documento de la comunicación");
                        comunicacionRadicadaBool = false;

                    } else {

                        this.reporteComunicacionNotificacion = this.reportsService.
                            consultarReporteDeGestorDocumental(
                                td.getIdRepositorioDocumentos());

                        if (this.reporteComunicacionNotificacion == null) {
                            this.addMensajeError(
                                "Error al recuperar la comunicación de notificación guardada.");
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al recuperar la comunicación de notificación de Alfresco. " + e.
                getMessage(), e);
            if (this.reporteComunicacionNotificacion == null) {
                this.addMensajeError("Error al recuperar la comunicación de notificación guardada.");
            }
        }
        return comunicacionRadicadaBool;
    }

    /**
     * Método que verifica si ya se radico la comunicación para el solicitante o afectado
     * seleccionado. De ser así, la consulta en los TramiteDocumentos del trámite, si no returna
     * false y procede a la generación del reporte de la comunicación de manera masiva.
     *
     * @author felipe.cadena
     */
    public boolean comunicacionRadicadaParaEsteSolicitanteMasiva() {

        boolean comunicacionRadicadaBool = true;
        try {

            List<TramiteDocumento> documentosAGenerar = new ArrayList<TramiteDocumento>();
            for (Solicitante solicitanteSeleccionado : this.solicitantesSelecionados) {
                for (TramiteDocumento td : this.currentTramiteTramiteDocumentosComNotReso) {
                    if (td.getIdPersonaNotificacion().contains(
                        solicitanteSeleccionado
                            .getNumeroIdentificacion())) {

                        documentosAGenerar.add(td);
                        break;
                    }
                }
            }

            if (documentosAGenerar.size() != this.solicitantesSelecionados.length) {

                return false;
            }

            List<String> rutasMemorandos = new ArrayList<String>();

            for (TramiteDocumento td : documentosAGenerar) {

                String rutaTempLocal = this.getGeneralesService()
                    .descargarArchivoDeAlfrescoATemp(td.getIdRepositorioDocumentos());
                rutasMemorandos.add(rutaTempLocal);

            }

            if (!rutasMemorandos.isEmpty()) {
                IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
                String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
                this.reporteComunicacionNotificacion =
                     this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
            }

        } catch (Exception e) {
            LOGGER.error("Error al recuperar la comunicación de notificación de Alfresco. " + e.
                getMessage(), e);
            if (this.reporteComunicacionNotificacion == null) {
                this.addMensajeError("Error al recuperar la comunicación de notificación guardada.");
            }
        }
        return comunicacionRadicadaBool;
    }

    /**
     * Método que verifica la seleccion de un solicitante o afectado
     *
     * @author david.cifuentes
     */
    public void activarBotonAceptarOAvanzar() {

        // Verificar que el solicitante o afectado seleccionado no sea nulo
        if (this.solicitantesSelecionados == null || this.solicitantesSelecionados.length == 0) {
            this.addMensajeWarn("Por favor seleccione antes un solicitante o afectado.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        // Verificar que los seleccionados ya tengan radicacion de notificacion
        for (Solicitante solicitante : solicitantesSelecionados) {
            if (ESiNo.NO.getCodigo().equals(solicitante.getSolicitanteNotificado())) {
                this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_MASIVO_003.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.warn(ECodigoErrorVista.NOTIFICACION_MASIVO_003.getMensajeTecnico());
                return;
            }
        }

        //D: se marcan los solicitantes a quienes ya se les hizo registro de la comunicación de notif
        this.marcarSolicitantesConRegistroComunic();

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Guarda registros en la tabla Tramite_Documento por cada uno de los solicitantes a los que se
     * les envía la comunicación.
     *
     * PRE: se ha ejecutado el método guardarDocumentoComunicNotif
     *
     * Deja en null la fecha de notificación (y el método de notificación) porque ese criterio (que
     * sea null) se utiliza para saber si ya se hizo el registro de la comunicación de notificación
     *
     * @return
     */
    private boolean guardarTramiteDocumentosComunicNotif(Solicitante solicitante) {

        boolean answer = true;
        TramiteDocumento tramDoc, tempTramDoc;

        if (this.documentosComunicacionNotificacion.get(solicitante.getId()) == null) {
            return false;
        }

        tramDoc = new TramiteDocumento();
        tramDoc.setTramite(this.currentTramite);
        tramDoc.setDocumento(this.documentosComunicacionNotificacion.get(solicitante.getId()));
        tramDoc.setIdRepositorioDocumentos(this.documentosComunicacionNotificacion.get(solicitante.
            getId())
            .getIdRepositorioDocumentos());
        tramDoc.setfecha(new Date(System.currentTimeMillis()));
        tramDoc.setFechaLog(new Date(System.currentTimeMillis()));
        tramDoc.setUsuarioLog(this.currentUser.getLogin());

        // Asociación de valores del solicitante
        tramDoc.setPersonaNotificacion((solicitante.getNombreCompleto().length() > 98) ?
            solicitante.getNombreCompleto().substring(0, 98) : solicitante.getNombreCompleto());
        tramDoc.setIdPersonaNotificacion(solicitante.getNumeroIdentificacion());
        tramDoc.setTipoIdPersonaNotificacion(solicitante.getTipoIdentificacion());

        try {
            tempTramDoc = this.getTramiteService().actualizarTramiteDocumento(
                tramDoc);
            if (tempTramDoc != null) {
                solicitante.setSolicitanteNotificado(ESiNo.SI.getCodigo());
                this.currentTramite.getTramiteDocumentos().add(tempTramDoc);
            } else {
                LOGGER.error("No fue posible crear el trámite documento.");
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("Error guardando un TramiteDocumento en " +
                 "ComunicacionNotificacionResolucionMB#guardarTramiteDocumentosComunicNotif: " +
                 ex.getMessage(), ex);
            return false;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Guarda el documento con la comunicación de la notificación en el servidor documental, e
     * inserta el respectivo registro en la tabla Documento
     *
     * @return
     */
    private boolean guardarDocumentoComunicNotif(Solicitante solicitante) {

        boolean answer = true;
        Documento tempDocumento;
        TipoDocumento tipoDoc;
        Long tipoDocumentoId;
        Date fechaLogYRadicacion;

        tipoDocumentoId = ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId();
        try {
            tipoDoc =
                this.getGeneralesService().buscarTipoDocumentoPorId(tipoDocumentoId);
        } catch (Exception ex) {
            LOGGER.error("error buscando TipoDocumento con id " + tipoDocumentoId + " : " + ex.
                getMessage(), ex);
            return false;
        }

        tempDocumento = new Documento();
        fechaLogYRadicacion = new Date(System.currentTimeMillis());

        //D: se arma el objeto Documento con todos los atributos posibles...
        //D: el método que guarda el documento en el gestor documental recibe el nombre del archivo
        //   sin la ruta
        if (this.rutasocumentosRadicados.get(solicitante.getId()) != null) {
            tempDocumento.setArchivo(this.rutasocumentosRadicados.get(solicitante.getId()));
        }
        tempDocumento.setTramiteId(this.currentTramiteId);
        tempDocumento.setTipoDocumento(tipoDoc);
        tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
        tempDocumento.setUsuarioCreador(this.currentUser.getLogin());
        tempDocumento.setEstructuraOrganizacionalCod(this.currentUser.
            getCodigoEstructuraOrganizacional());
        tempDocumento.setFechaRadicacion(fechaLogYRadicacion);

//FIXME:: uncomment this when integrated with process
        //tempDocumento.setProcesoId(this.currentProcessActivity.getId());
        tempDocumento.setFechaLog(fechaLogYRadicacion);
        tempDocumento.setNumeroRadicacion(this.numerosRadicacionDocComNot.get(solicitante.getId()));
        tempDocumento.setUsuarioLog(this.currentUser.getLogin());
        tempDocumento.setDescripcion(
            "Documento de comunicación de notificación generado por el sistema");

        // Asociación de la identificación del solicitante al documento.
        tempDocumento.setObservaciones(solicitante.getNumeroIdentificacion());

        try {
            //D: se guarda el documento
            this.documentosComunicacionNotificacion.put(solicitante.getId(),
                this.getTramiteService().guardarDocumento(this.currentUser, tempDocumento));
        } catch (Exception ex) {
            LOGGER.error("error guardando Documento : " + ex.getMessage(), ex);
            return false;
        }

        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método que envia un correo con el reporte la comunicación de notificación de resolución al
     * solicitante seleccionado.
     *
     * @author pedro.garcia basado en lo hecho por david.cifuentes
     */
    private boolean enviarCorreo(Solicitante solicitante) {

        boolean answer = true;

        // Verificar que el solicitante o afectado si tenga un correo asociado
        if (solicitante.getCorreoElectronico() == null ||
            solicitante.getCorreoElectronico().trim().isEmpty()) {
            this.addMensajeWarn(
                "No fue posible enviar el correo para el solicitante o afectado seleccionado pues éste no tiene un correo electrónico asociado.");
            return answer;
        }

        String asunto =
            ConstantesComunicacionesCorreoElectronico.ASUNTO_COMUNICACION_DE_NOTIFICACION;
        String contenido =
            ConstantesComunicacionesCorreoElectronico.CONTENIDO_COMUNICACION_DE_NOTIFICACION;
        String[] adjuntos;
        String[] titulos;

        adjuntos = new String[]{this.rutasocumentosRadicados.get(solicitante.getId())};
        titulos = new String[]{
            ConstantesComunicacionesCorreoElectronico.ARCHIVO_COMUNICACION_DE_NOTIFICACION};

        if (solicitante.getCorreoElectronico() != null &&
             !solicitante.getCorreoElectronico().trim().isEmpty()) {
            try {
                this.getGeneralesService()
                    .enviarCorreo(
                        solicitante.getCorreoElectronico(),
                        asunto, contenido, adjuntos, titulos);
            } catch (Exception e) {
                LOGGER.debug("Error al enviar el correo electrónico en " +
                     "ComunicacionNotificacionResolucionMB#enviarCorreo: " +
                     e.getMessage());
                answer = false;
            }
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Aceptar" de la pantalla de registro de comunicación de la notificación
     */
    public void registrarComunicacionNotificacion() {

        for (Solicitante solicitanteSeleccionado : this.solicitantesSelecionados) {

            TramiteDocumento tramiteDocumentoARegistrar = null;

            mostrarBotonMover = true;

            // D: solo en el caso de que haya más de un solicitante puede entrar más
            // de una vez a este
            // método. En tal caso hay que validar si no se había registrado ya la
            // comunicación de
            // notificación para el usuario
            if (this.solicitantesEnvioComunicacionNotificacion.size() > 1) {
                if (solicitanteSeleccionado != null) {
                    if (solicitanteSeleccionado
                        .isComunicacionDeNotificacionRegistrada()) {
                        this.addMensajeInfo(
                            "Ya se había registrado la comunicación de notificación para el solicitante seleccionado");
                        return;
                    }
                }
            }

            tramiteDocumentoARegistrar = this.selectTramiteDocumentosItemToGet(
                solicitanteSeleccionado);
            // D: si no se encuentra el TramiteDocumento termina. 
            // Los mensajes de error se definen en el método invocado
            if (tramiteDocumentoARegistrar == null) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.
                    addMensajeError("No se encontró el documento de la notificación de resolución.");
                return;
            }

            if (this.currentTramiteTramiteDocumentosComNotReso.size() > 0) {

                tramiteDocumentoARegistrar
                    .setFechaNotificacion(this.fechaComunicNotif);
                tramiteDocumentoARegistrar
                    .setMetodoNotificacion(this.medioComunicNotif);
                tramiteDocumentoARegistrar
                    .setObservacionNotificacion(this.observacionesComunicNotif);
                tramiteDocumentoARegistrar.setPersonaAutorizacion(personaAtendio);

                tramiteDocumentoARegistrar = this.getTramiteService()
                    .actualizarTramiteDocumento(tramiteDocumentoARegistrar);

                // D: hay que volver a buscar los TramiteDocumento para refrescar el
                // atributo de registro de los notificantes
                this.currentTramiteTramiteDocumentosComNotReso = (ArrayList<TramiteDocumento>) this
                    .getTramiteService()
                    .obtenerTramiteDocumentosDeTramitePorTipoDoc(
                        this.currentTramiteId,
                        ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId());

                // D: se marcan los solicitantes a quienes ya se les hizo registro
                // de la comunicación de notif
                this.marcarSolicitantesConRegistroComunic();

                this.disabledRegistrarComunicButton = !this.radicadoPrimeraVezBool ||
                     (this.radicadoPrimeraVezBool && this.todosRegistrados);

                // D: mover el proceso cuando es el último (o único) por registrar
                // la comunicación
                if (this.lastSolicitanteForRegistroComunicNotif &&
                     this.todosRegistrados) {
                    mostrarBotonMover = false;
                    this.addMensajeInfo(
                        "Se completó el registro de las comunicaciones para todos los solicitantes y/o afectados, por favor proceda a avanzar el proceso.");
                } else {
                    this.addMensajeInfo("Se registró la comunicación de notificación.");
                }
            }
        }
    }
    //--------------------------------------------------------------------------------------------------

    /**
     * Método que avanza el proceso a la siguiente actividad
     *
     * @author david.cifuentes
     */
    public String moverProceso() {

        try {
            if (!this.forwardThisProcess()) {
                this.addMensajeError("No se puede avanzar el proceso por errores de validación: " +
                     this.validateProcessErrorMessage);
                return "";
            }
            //Utilidades.removerManagedBean("comunicacionNotificacionResolucion");
            this.tareasPendientesMB.init();

        } catch (Exception e) {
            LOGGER.error("Error al mover el proceso " + e.getMessage(), e);
        }
        return ConstantesNavegacionWeb.INDEX;
    }

    //---------------------------------------------------------------------------------------------------
    /**
     * se marcan los solicitantes a quienes ya se les hizo registro de la comunicación de
     * notificación. El criterio es: se marca el solicitante X si existe un TramiteDocumento del
     * tipo Comunicación de notificación para este trámite, para el solicitante X, cuya fecha de
     * notificación esté en null
     *
     * Se aprovecha para dar valor a la bandera que indica si solo queda un solicitante pendiente de
     * registro de comunicación
     */
    private void marcarSolicitantesConRegistroComunic() {

        int hmSinRegistro = 0;
        int hmConRegistro;
        boolean docRegistrado;
        for (Solicitante solicitante : this.solicitantesEnvioComunicacionNotificacion) {
            solicitante.setComunicacionDeNotificacionRegistrada(false);
            docRegistrado = false;
            for (TramiteDocumento td : this.currentTramiteTramiteDocumentosComNotReso) {
                if (td.getIdPersonaNotificacion().contains(
                    solicitante.getNumeroIdentificacion())) {
                    if (td.getFechaNotificacion() != null) {
                        docRegistrado = true;
                        break;
                    }
                }
            }
            if (!docRegistrado) {
                hmSinRegistro++;
            } else {
                solicitante.setComunicacionDeNotificacionRegistrada(true);
            }
        }
        this.lastSolicitanteForRegistroComunicNotif = (hmSinRegistro <= 1) ? true : false;

        hmConRegistro = this.solicitantesEnvioComunicacionNotificacion.size() - hmSinRegistro;
        this.todosRegistrados =
            (hmConRegistro == this.solicitantesEnvioComunicacionNotificacion.size()) ? true : false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Ver comunicación" en la pantalla de registro de comunicación de
     * notificación busca el documento en el gestor documental y lo carga en la variable de clase
     * para que se muestre en la página del documento
     */
    public void consultarDocumentoComunicacionNotificacion() {

        TramiteDocumento tramiteDoc;
        String idDocEnGestorDocumental;
        List<String> rutasMemorandos = new ArrayList<String>();

        for (Solicitante solicitante : this.solicitantesSelecionados) {

            tramiteDoc = this.selectTramiteDocumentosItemToGet(solicitante);
            if (tramiteDoc == null) {
                this.addMensajeError("No se encontró el documento de comunicación de notificación.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

            this.radicadoBool = true;

            idDocEnGestorDocumental = tramiteDoc.getIdRepositorioDocumentos();

            this.reporteComunicacionNotificacion = this.reportsService.
                consultarReporteDeGestorDocumental(idDocEnGestorDocumental);

            rutasMemorandos.
                add(this.reporteComunicacionNotificacion.getRutaCargarReporteAAlfresco());

            if (this.reporteComunicacionNotificacion == null) {
                this.addMensajeError("No se encontró el documento de comunicación de notificación.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                return;
            }

        }
        if (!rutasMemorandos.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
            this.reporteComunicacionNotificacion =
                 this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Método que retorna un tramiteDocumento de la lista de tramiteDocumentos del trámite el cúal
     * esté asociado a un solicitante.
     *
     * @return el índice del TramiteDocumento buscado
     */
    private TramiteDocumento selectTramiteDocumentosItemToGet(Solicitante solicitante) {

        TramiteDocumento tramiteDocumentoARegistrar = null;

        if (this.solicitantesEnvioComunicacionNotificacion.size() == 1) {
            //D: se supone en este caso que solo hay un TramiteDocumento
            if (this.currentTramiteTramiteDocumentosComNotReso.size() != 1) {
                this.addMensajeError(
                    "Hay una discrepancia entre el número de documentos de este tipo" +
                    " que hay y el que debería haber.");
                return null;
            } else {
                tramiteDocumentoARegistrar = this.currentTramiteTramiteDocumentosComNotReso.get(0);
            }
        } //D: se supone que hubo uno seleccionado de la lista de solicitantes
        else if (this.solicitantesEnvioComunicacionNotificacion.size() > 1) {
            //D: no debería pasar, pero en pruebas me pasó
            if (solicitante == null) {
                this.addMensajeError("Por favor seleccione un solicitante o afectado.");
                return null;
            }

            //D: en el guardado de TramiteDocumento (al menos para este caso de uso) se guardó el
            // número de identificación de la persona
            for (TramiteDocumento tempTD : this.currentTramiteTramiteDocumentosComNotReso) {
                if (tempTD.getIdPersonaNotificacion().
                    contains(solicitante.getNumeroIdentificacion())) {

                    // Capturar el tramite documento a modificar
                    tramiteDocumentoARegistrar = tempTD;
                    break;
                }
            }
        }

        return tramiteDocumentoARegistrar;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "cerrar" de la ventana principal
     */
    public String closeCUWindow() {

        this.tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean("comunicacionNotificacionResolucion");
        return ConstantesNavegacionWeb.INDEX;
    }
//--------------------------------------------------------------------------------------------------
    //----------------   BPM  ---------------------

    /**
     * se valida que para todos los solicitantes se haya hecho el registro de la comunicación, y que
     * haya un usuario con el rol requerido para ser destinatario de la actividad
     *
     * @author pedro.garcia
     */
    @Implement
    public boolean validateProcess() {

        boolean hayUsuario = false;

        //N: por alguna razón se debe usar el 2 (ver nombre del método)
        this.usuariosRol =
            this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.currentUser.getDescripcionEstructuraOrganizacional(),
                ERol.SECRETARIA_CONSERVACION);

        if (this.usuariosRol == null || this.usuariosRol.isEmpty()) {
            LOGGER.error("Error de LDAP: no se encuentra usuario con rol " +
                ERol.SECRETARIA_CONSERVACION.toString() +
                " para la territorial " + this.currentUser.getDescripcionEstructuraOrganizacional());
        } else {
            hayUsuario = true;
        }

        if (!this.todosRegistrados) {
            this.validateProcessErrorMessage =
                "No se ha registrado la comunicación de notificación " +
                "para todos los usuarios";
        }
        if (!hayUsuario) {
            this.validateProcessErrorMessage = "No se encontró un usuario con el rol " +
                ERol.SECRETARIA_CONSERVACION.toString() + " en la territorial";
        }
        boolean answer = hayUsuario && this.todosRegistrados;
        return answer;
    }
//-----------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";

        UsuarioDTO newUser;

        processTransition = ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_NOTIFICACION;
        newUser = this.usuariosRol.get(0);
        observaciones = "";
        messageDTO =
            ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                this.currentProcessActivity.getId(), processTransition, newUser, observaciones);

        messageDTO.setUsuarioActual(this.currentUser);
        this.setMessage(messageDTO);

    }
//-----------------------------------------------------------------------------------

    /**
     * there is nothing to do
     *
     * @author pedro.garcia
     */
    @Implement
    public void doDatabaseStatesUpdate() {

    }
//-------------------------------------------------------------------------------------

    private boolean forwardThisProcess() {

        if (this.validateProcess()) {
            this.setupProcessMessage();
            this.doDatabaseStatesUpdate();
            this.forwardProcess();
            return true;
        } else {
            return false;
        }
    }

    // ----------------------------------------------------------------------------- //
    // ----------------------------------------------------------------------------- //
    // ----------------------------------------------------------------------------- //
    /**
     * Método que verifica que no se haya radicado por primera vez la comunicación para algunos de
     * los solicitantes o afectados.
     *
     * @author david.cifuentes
     */
    public boolean validateRadicadoPrimerVez() {

        if (this.radicadoPrimeraVezBool) {
            this.addMensajeWarn(
                "No se pueden realizar edición sobre los afectados pues ya se radicó la comunicación para al menos uno de ellos.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return radicadoPrimeraVezBool;
        }
        return radicadoPrimeraVezBool;
    }

    //--------------------------------------------------------//
    /**
     * Método que verifica que no se haya radicado por primera vez y que haya un afectado
     * seleccionado
     *
     * @author david.cifuentes
     */
    public void validateAfectadoSeleccion() {

        if (this.radicadoPrimeraVezBool) {
            this.addMensajeWarn(
                "No se pueden realizar edición sobre los afectados pues ya se radicó la comunicación para al menos uno de ellos.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        // Verificar que el solicitante o afectado seleccionado no sea nulo
        if (this.solicitantesSelecionados == null && this.solicitantesSelecionados.length == 0) {
            this.addMensajeWarn("Por favor seleccione antes un afectado a eliminar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //validar que solo se seleccione solo un solicitante para eliminar
        if (this.solicitantesSelecionados.length > 1) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_MASIVO_005.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.debug(ECodigoErrorVista.NOTIFICACION_MASIVO_005.getMensajeTecnico());
            return;
        }

        this.solicitanteOAfectadoSeleccionado = this.solicitantesSelecionados[0];

        //validar que no sea un propietario
        if (ESolicitanteRelacionNoti.PROPIETARIO.getValor().equals(solicitanteOAfectadoSeleccionado.
            getRelacionNotificacion())) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_MASIVO_006.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.debug(ECodigoErrorVista.NOTIFICACION_MASIVO_006.getMensajeTecnico());
        }
    }

    //--------------------------------------------------------//
    /**
     * Método que inicializa un afectado.
     *
     * @author david.cifuentes
     */
    public void configurarNuevoAfectado() {

        // Verificar que no se haya radicado por primera vez la comunicación
        if (validateRadicadoPrimerVez()) {
            return;
        }

        this.solicitanteOAfectadoSeleccionado = new Solicitante();
        this.solicitanteOAfectadoSeleccionado
            .setTipoIdentificacion(EPersonaTipoIdentificacion.CEDULA_CIUDADANIA
                .getCodigo());
        this.solicitanteOAfectadoSeleccionado
            .setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());

        this.solicitanteOAfectadoSeleccionado
            .setDireccionDepartamento(new Departamento());
        this.solicitanteOAfectadoSeleccionado
            .setDireccionMunicipio(new Municipio());
        this.solicitanteOAfectadoSeleccionado.setFechaLog(new Date());
        this.solicitanteOAfectadoSeleccionado.setUsuarioLog(this.currentUser
            .getLogin());

        // Departamento
        if (this.currentTramite.getPredio() != null &&
             this.currentTramite.getPredio().getDepartamento() != null) {
            this.solicitanteOAfectadoSeleccionado
                .setDireccionDepartamento(this.currentTramite.getPredio()
                    .getDepartamento());
        } else {
            this.solicitanteOAfectadoSeleccionado
                .setDireccionDepartamento(this.currentTramite.getDepartamento());
        }
        // Municipio
        if (this.currentTramite.getPredio() != null &&
             this.currentTramite.getPredio().getMunicipio() != null) {
            this.solicitanteOAfectadoSeleccionado
                .setDireccionMunicipio(this.currentTramite.getPredio()
                    .getMunicipio());
        } else {
            this.solicitanteOAfectadoSeleccionado
                .setDireccionMunicipio(this.currentTramite.getMunicipio());
        }
        // País
        if (this.currentTramite.getPredio() != null &&
             this.currentTramite.getPredio().getDepartamento() != null &&
             this.currentTramite.getPredio().getDepartamento().getPais() != null) {

            this.solicitanteOAfectadoSeleccionado
                .setDireccionPais(this.currentTramite.getPredio()
                    .getDepartamento().getPais());
        } else {
            this.solicitanteOAfectadoSeleccionado
                .setDireccionPais(this.currentTramite.getDepartamento()
                    .getPais());
        }

        this.solicitanteOAfectadoSeleccionado
            .setRelacion(ESolicitanteSolicitudRelac.AFECTADO.toString());
        this.banderaBusquedaAfectadoRealizada = false;
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de buscar un afectado a partir de los datos de documento
     *
     * @author david.cifuentes
     */
    public void buscarAfectado() {
        LOGGER.debug("Entrando a buscar afectado");

        Solicitante sol = this.getTramiteService()
            .buscarSolicitantePorTipoIdentYNumDocAdjuntarRelacions(
                solicitanteOAfectadoSeleccionado.getNumeroIdentificacion(),
                solicitanteOAfectadoSeleccionado.getTipoIdentificacion());
        if (sol != null) {
            this.solicitanteOAfectadoSeleccionado = sol;
            this.editModeBool = true;
        } else {
            LOGGER.debug("buscarAfectado Return null");
            String tipoIdentif = this.solicitanteOAfectadoSeleccionado
                .getTipoIdentificacion();
            String numDoc = this.solicitanteOAfectadoSeleccionado
                .getNumeroIdentificacion();
            String tipoPersona = this.solicitanteOAfectadoSeleccionado.getTipoPersona();

            setDefaultDataAfectado();
            this.solicitanteOAfectadoSeleccionado.setTipoIdentificacion(tipoIdentif);
            this.solicitanteOAfectadoSeleccionado.setNumeroIdentificacion(numDoc);
            this.solicitanteOAfectadoSeleccionado.setTipoPersona(tipoPersona);

            //se calcula al digito de verificación
            if (this.solicitanteOAfectadoSeleccionado.getTipoIdentificacion().
                equals(EPersonaTipoIdentificacion.NIT.getCodigo())) {
                int dv = this.getGeneralesService().calcularDigitoVerificacion(
                    this.solicitanteOAfectadoSeleccionado.getNumeroIdentificacion());
                this.solicitanteOAfectadoSeleccionado.setDigitoVerificacion(String.valueOf(dv));
            }

            this.editModeBool = false;
        }
    }

    //--------------------------------------------------------//
    /**
     * Método que quema los datos de fechas departamento tipo de identificación y demás requeridos
     * para inicializar el afectado.
     *
     * @author david.cifuentes
     */
    private void setDefaultDataAfectado() {
        Solicitante sol = new Solicitante();
        sol.setTipoIdentificacion("CC");
        // Departamento
        if (this.currentTramite.getPredio() != null &&
             this.currentTramite.getPredio().getDepartamento() != null) {
            sol.setDireccionDepartamento(this.currentTramite.getPredio()
                .getDepartamento());
        } else {
            sol.setDireccionDepartamento(this.currentTramite.getDepartamento());
        }
        // Municipio
        if (this.currentTramite.getPredio() != null &&
             this.currentTramite.getPredio().getMunicipio() != null) {
            sol.setDireccionMunicipio(this.currentTramite.getPredio()
                .getMunicipio());
        } else {
            sol.setDireccionMunicipio(this.currentTramite.getMunicipio());
        }
        // País
        if (this.currentTramite.getPredio() != null &&
             this.currentTramite.getPredio().getDepartamento() != null &&
             this.currentTramite.getPredio().getDepartamento().getPais() != null) {

            sol.setDireccionPais(this.currentTramite.getPredio()
                .getDepartamento().getPais());
        } else {
            sol.setDireccionPais(this.currentTramite.getDepartamento()
                .getPais());
        }
        sol.setFechaLog(new Date());
        sol.setUsuarioLog(this.currentUser.getLogin());
        this.solicitanteOAfectadoSeleccionado = sol;
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de guardar/actualizar el afectado actual en la tabla solicitantes o
     * afectados, y crear el solicitanteTramite para asociar el afectado al trámite.
     *
     * @author david.cifuentes
     */
    public void guardarAfectado() {

        //N: El solicitante siempre acepta ser notificado por correo electrónico, según indicaciones
        //de los analistas
        this.solicitanteOAfectadoSeleccionado.setNotificacionEmailBoolean(true);

//		Se comenta porque la notificación por email NO es campo obligatorio :: 22-08-2012 :: javier.aponte 
//		if (validarAfectado()) {
        //se valida el codigo postal
        RequestContext context = RequestContext.getCurrentInstance();
        if (this.solicitanteOAfectadoSeleccionado.getCodigoPostal() != null &&
             !this.solicitanteOAfectadoSeleccionado.getCodigoPostal().isEmpty()) {

            if (this.solicitanteOAfectadoSeleccionado.getDireccion() == null ||
                this.solicitanteOAfectadoSeleccionado.getDireccion().isEmpty()) {
                this.addMensajeError("Dirección: Error de validación: se necesita un valor.");
                context.addCallbackParam("error", "error");
                return;
            }

            if (!UtilidadesWeb.validarSoloDigitos(this.solicitanteOAfectadoSeleccionado.
                getCodigoPostal())) {
                this.addMensajeError("El Código postal debe contener solo números");
                context.addCallbackParam("error", "error");
                return;
            }

            if (this.solicitanteOAfectadoSeleccionado.getCodigoPostal().length() < 6) {
                this.addMensajeError("El Código postal debe tener por lo menos 6 digitos");
                context.addCallbackParam("error", "error");
                return;
            }
            String initCodigoPostal = this.solicitanteOAfectadoSeleccionado.getCodigoPostal().
                substring(0, 2);

            if (!this.solicitanteOAfectadoSeleccionado.getDireccionDepartamento().getCodigo().
                equals(initCodigoPostal)) {
                this.addMensajeError(
                    "Código postal no corresponde con el código del departamento seleccionado");
                context.addCallbackParam("error", "error");
                return;
            }

        }

        // Se guarda el solicitante
        boolean yaAdicionado = false;
        for (Solicitante sol : this.solicitantesEnvioComunicacionNotificacion) {
            if (sol.getNumeroIdentificacion() != null &&
                 sol.getTipoIdentificacion() != null &&
                 sol.getTipoIdentificacion().equals(
                    this.solicitanteOAfectadoSeleccionado
                        .getTipoIdentificacion()) &&
                 sol.getNumeroIdentificacion().equals(
                    this.solicitanteOAfectadoSeleccionado
                        .getNumeroIdentificacion())) {
                yaAdicionado = true;
                break;
            }
        }

        Solicitante auxSolicitante = this.getTramiteService()
            .guardarActualizarSolicitante(this.solicitanteOAfectadoSeleccionado);

        if (auxSolicitante != null) {

            this.addMensajeInfo("El afectado fue " +
                 (yaAdicionado ? "actualizado" : "adicionado") +
                 " correctamente");

            auxSolicitante.setDireccionPais(solicitanteOAfectadoSeleccionado
                .getDireccionPais());
            auxSolicitante.setDireccionDepartamento(solicitanteOAfectadoSeleccionado
                .getDireccionDepartamento());
            auxSolicitante.setDireccionMunicipio(solicitanteOAfectadoSeleccionado
                .getDireccionMunicipio());
            auxSolicitante.setRelacion(solicitanteOAfectadoSeleccionado.getRelacion());
            auxSolicitante.setNotificacionEmail(solicitanteOAfectadoSeleccionado
                .getNotificacionEmail());
            this.solicitanteOAfectadoSeleccionado = auxSolicitante;

            // Se remueve de la lista el afectado desactualizado y se reemplaza por el actualizado.
            if (yaAdicionado) {
                Solicitante solAux = null;
                for (Solicitante s : this.solicitantesEnvioComunicacionNotificacion) {
                    if (s.getId().longValue() == this.solicitanteOAfectadoSeleccionado.getId()) {
                        solAux = s;
                        break;
                    }
                }
                if (solAux != null) {
                    this.solicitantesEnvioComunicacionNotificacion.remove(solAux);
                }
            }
            // Se adiciona a nuestra lista de solicitantes y afectados.
            this.solicitantesEnvioComunicacionNotificacion.
                add(this.solicitanteOAfectadoSeleccionado);

            // Se crea el solicitanteTramite para almacenar el afectado.
            // La manera para identificar el afectado, es que no
            // tiene un valor para tipoSolicitante y en relacion tiene el
            // valor 'AFECTADO'
            SolicitanteTramite solTram = new SolicitanteTramite();

            if (yaAdicionado) {
                // Recupero mi solicitanteTramite para ser actualizado
                for (SolicitanteTramite st : this.currentTramite
                    .getSolicitanteTramites()) {
                    if (st.getSolicitante().getId().longValue() ==
                        this.solicitanteOAfectadoSeleccionado.getId().longValue()) {
                        solTram = st;
                        break;
                    }
                }
            } else {
                // Es un nuevo afectado, se adicionan los valores a este.
                solTram.setUsuarioLog(this.currentUser.getLogin());
                solTram.setFechaLog(new Date(System.currentTimeMillis()));
                solTram.setTramite(this.currentTramite);
            }

            solTram.setSolicitante(this.solicitanteOAfectadoSeleccionado);

            int i;
            for (i = 0; i < this.currentTramite.getSolicitanteTramites()
                .size(); i++) {
                if (this.currentTramite
                    .getSolicitanteTramites()
                    .get(i)
                    .getSolicitante()
                    .getId()
                    .compareTo(solicitanteOAfectadoSeleccionado.getId()) == 0) {
                    this.currentTramite
                        .getSolicitanteTramites()
                        .get(i)
                        .setSolicitante(
                            this.solicitanteOAfectadoSeleccionado);
                    this.currentTramite
                        .getSolicitanteTramites()
                        .get(i)
                        .incorporarDatosSolicitante(
                            this.solicitanteOAfectadoSeleccionado);
                    break;
                } else {
                    // Modificación del afectado
                    if (this.currentTramite.getSolicitanteTramites().get(i)
                        .getSolicitante().getId().longValue() == solicitanteOAfectadoSeleccionado
                            .getId().longValue()) {

                        this.currentTramite
                            .getSolicitanteTramites()
                            .get(i)
                            .setSolicitante(
                                this.solicitanteOAfectadoSeleccionado);
                        this.currentTramite
                            .getSolicitanteTramites()
                            .get(i)
                            .incorporarDatosSolicitante(
                                this.solicitanteOAfectadoSeleccionado);
                        break;
                    }
                }
            }

            solTram.incorporarDatosSolicitante(auxSolicitante);
            solTram.setTipoSolicitante("");
            solTram.setRelacion(ESolicitanteSolicitudRelac.AFECTADO.toString());

            // Guardar TramiteSolicitante
            solTram = this.getTramiteService()
                .guardarActualizarSolicitanteTramite(solTram);

            if (solTram != null && solTram.getId() != null) {
                this.currentTramite.getSolicitanteTramites().add(solTram);
            }
            // Se reinician las variables para desactivar los botones de edición en la pantalla.
            this.solicitanteOAfectadoSeleccionado = null;
            this.solicitanteSelectBool = false;

        } else {
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error al almacenar el afectado");
        }
//		}
    }

    //--------------------------------------------------------//
    /**
     * Método que realiza la validación lógica de campos para el afectado
     *
     * @author david.cifuentes
     */
    private boolean validarAfectado() {

        RequestContext context = RequestContext.getCurrentInstance();

        if (this.solicitanteOAfectadoSeleccionado.getNotificacionEmail().equals(
            ESiNo.SI.getCodigo()) &&
             (this.solicitanteOAfectadoSeleccionado.getCorreoElectronico() == null ||
            this.solicitanteOAfectadoSeleccionado
                .getCorreoElectronico().isEmpty())) {

            String mensaje = "La notificación por email requiere que " +
                 " se diligencie una cuenta de correo electrónico." +
                 " Por favor complete los datos e intente nuevamente.";
            this.addMensajeError(mensaje);
            context.addCallbackParam("error", "error");
            return false;
        }
        return true;
    }

    //--------------------------------------------------------//
    /**
     * Método que prepara al afectado para su edición.
     *
     * @author david.cifuentes
     */
    public void setupSolicitanteOAfectadoEdicion() {

        // Verificar que el solicitante o afectado seleccionado no sea nulo
        if (this.solicitantesSelecionados == null && this.solicitantesSelecionados.length == 0) {
            this.addMensajeWarn("Por favor seleccione antes un afectado a modificar.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        //validar que solo se selccione un solicitante para modificar
        if (this.solicitantesSelecionados.length > 1) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_MASIVO_004.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.debug(ECodigoErrorVista.NOTIFICACION_MASIVO_004.getMensajeTecnico());
            return;
        }

        // Verificar que no se haya radicado por primera vez la comunicación
        if (validateRadicadoPrimerVez()) {
            return;
        }

        this.solicitanteOAfectadoSeleccionado = this.solicitantesSelecionados[0];
        boolean esAfectado = false;

        if (this.currentTramite.getSolicitanteTramites() != null &&
             this.currentTramite.getSolicitanteTramites().size() > 0) {
            for (SolicitanteTramite st : this.currentTramite
                .getSolicitanteTramites()) {

                if (st.getSolicitante().getId().longValue() == this.solicitanteOAfectadoSeleccionado
                    .getId().longValue()) {

                    // Se valida que la información que se intenta modificar sea
                    // la de un afectado.
                    if (st.getRelacion() != null &&
                         st.getRelacion().equals(
                            ESolicitanteSolicitudRelac.AFECTADO.toString())) {
                        esAfectado = true;
                        break;
                    }
                }
            }
            // Verificar si el solicitante seleccionado no es un afectado.
            if (!esAfectado &&
                 !ESolicitanteRelacionNoti.PROPIETARIO.getValor().equals(
                    solicitanteOAfectadoSeleccionado.getRelacionNotificacion())) {
                this.addMensajeWarn(
                    "Sólo se puede editar la información para los afectados, seleccione un afectado e intente de nuevo.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.editModeBool = true;
                return;
            } else {
                editModeBool = false;
            }
            this.banderaPropietario = ESolicitanteRelacionNoti.PROPIETARIO.getValor().equals(
                solicitanteOAfectadoSeleccionado.getRelacionNotificacion());
        }
        this.banderaBusquedaAfectadoRealizada = true;
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de eliminar un afectado de la lista de solicitantes y afectados a comunicar
     * la notificación de resolución.
     *
     * @author david.cifuentes
     */
    public void eliminarSolicitanteAfectado() {

        try {
            if (validateAfectado()) {

                for (SolicitanteTramite st : this.currentTramite
                    .getSolicitanteTramites()) {
                    if (st.getSolicitante()
                        .getId()
                        .compareTo(solicitanteOAfectadoSeleccionado.getId()) == 0) {
                        this.getTramiteService().eliminarSolicitanteTramite(st);
                        this.currentTramite.getSolicitanteTramites().remove(st);
                        this.solicitantesEnvioComunicacionNotificacion.remove(
                            solicitanteOAfectadoSeleccionado);
                        this.addMensajeInfo("Se eliminó satisfactoriamente el afectado");
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error al eliminar el afectado seleccionado. " + e.getMessage(), e);
        }
    }

    //--------------------------------------------------------//
    /**
     * Método encargado de verificar que se vaya a eliminar un afectado, más no uno de los
     * solicitantes que vienen del trámite.
     *
     * @return false si es un solicitante y true si es un afectado.
     *
     * @author david.cifuentes
     */
    public boolean validateAfectado() {
        boolean esAfectado = false;
        if (this.solicitanteOAfectadoSeleccionado != null) {
            if (this.currentTramite.getSolicitanteTramites() != null &&
                 this.currentTramite.getSolicitanteTramites().size() > 0) {
                for (SolicitanteTramite st : this.currentTramite
                    .getSolicitanteTramites()) {

                    if (st.getSolicitante().getId().longValue() ==
                        this.solicitanteOAfectadoSeleccionado
                            .getId().longValue()) {

                        // Se valida que sea un afectado.
                        if (st.getRelacion() != null &&
                             st.getRelacion().equals(
                                ESolicitanteSolicitudRelac.AFECTADO.toString())) {
                            esAfectado = true;
                        }
                    }
                }
                // Verificar si el solicitante seleccionado no es un afectado.
                if (!esAfectado) {
                    this.addMensajeError(
                        "No se puede eliminar un solicitante, unicamente es  posible eliminar afectados.");
                    esAfectado = false;
                }
            }
        }
        return esAfectado;
    }

    //--------------------------------------------------------//
    /**
     * Método que revisa el estado de envio o comunicación de la notificación de resolución para el
     * solicitante o afectado seleccionado.
     *
     * @author david.cifuentes
     */
    public void revisarEstadoComunicacionDeNotificacion() {
        if (this.solicitanteOAfectadoSeleccionado != null) {
            // 1. Verificar si ya se radicó la primer comunicación
            // (Se sabe que se radicó cuando hay un tramite documento con el
            // tipo COMUNICACION_DE_NOTIFICACION)
            if (!this.radicadoPrimeraVezBool) {
                // No se ha radicado la primer vez
                this.radicadoBool = false;
                this.registradoBool = true;
            } else {
                // Ya se radicó la primer vez.
                // 2. Revisar si para el solicitante actual ya se hizo la
                // radicación
                radicadoBool = false;
                // Revisar si ya se radicó, consultar documento.
                if (this.currentTramite.getTramiteDocumentos() != null &&
                     !this.currentTramite.getTramiteDocumentos()
                        .isEmpty()) {
                    for (TramiteDocumento td : this.currentTramite
                        .getTramiteDocumentos()) {
                        if (td.getPersonaNotificacion().equals(
                            this.solicitanteOAfectadoSeleccionado
                                .getNombreCompleto())) {
                            this.radicadoBool = true;
                            break;
                        }
                    }
                }
                if (radicadoBool) {
                    // Revisar si para el solicitante ya se hizo el registro.
                    if (this.solicitanteOAfectadoSeleccionado
                        .isComunicacionDeNotificacionRegistrada()) {
                        registradoBool = true;
                    } else {
                        registradoBool = false;
                    }
                } else {
                    // No se ha radicado, por lo tanto no se ha registrado
                    registradoBool = true;
                }

            }
        }
    }

    /**
     * Determina para cuales de los solicitantes ya fue generada la notificacion
     *
     * @author felipe.cadena
     */
    private void determinaSolicitantesNotificacion() {

        for (Solicitante solicitanteSeleccionado : this.solicitantesEnvioComunicacionNotificacion) {
            for (TramiteDocumento td : this.currentTramiteTramiteDocumentosComNotReso) {
                if (td.getIdPersonaNotificacion().contains(
                    solicitanteSeleccionado
                        .getNumeroIdentificacion())) {

                    solicitanteSeleccionado.setSolicitanteNotificado(ESiNo.SI.getCodigo());
                    break;
                }
            }

            if (!ESiNo.SI.getCodigo().equals(solicitanteSeleccionado.getSolicitanteNotificado())) {
                solicitanteSeleccionado.setSolicitanteNotificado(ESiNo.NO.getCodigo());
            }
        }
    }

    /**
     * Método usado para seleccionar o deseleccionar todos los solicitantes de la tabla sobre los
     * cuales se realizará alguna acción.
     *
     * @author david.cifuentes
     */
    public void seleccionarTodosSolicitantes() {

        for (Solicitante s : this.solicitantesEnvioComunicacionNotificacion) {
            s.setSelected(this.selectedAllBool);
        }

        if (this.selectedAllBool) {
            this.solicitantesSelecionados =
                new Solicitante[this.solicitantesEnvioComunicacionNotificacion.size()];

            for (int i = 0; i < solicitantesEnvioComunicacionNotificacion.size(); i++) {
                solicitantesSelecionados[i] = solicitantesEnvioComunicacionNotificacion.get(i);
            }
        } else {
            this.solicitanteOAfectadoSeleccionado = null;
            this.solicitantesSelecionados = null;
        }

        if (this.solicitantesEnvioComunicacionNotificacion != null &&
             this.solicitantesEnvioComunicacionNotificacion.size() == 1) {

            LOGGER.debug("comunicacionNotificacionResolucionMB#solicitanteSelectAll");
            this.solicitanteOAfectadoSeleccionado = this.solicitantesEnvioComunicacionNotificacion.
                get(0);

            // Evaluar el estado de la comunicación de la notificación de resolución
            // para éste solicitante o afectado.
            this.revisarEstadoComunicacionDeNotificacion();

            // Reinicio de variables para un nuevo registro de comunicación
            this.medioComunicNotif = "";
            this.fechaComunicNotif = null;
            this.personaAtendio = "";
            this.fechaHoy = null;
            this.observacionesComunicNotif = "";
        }
    }

    /**
     * Método usado en la selección o deselección de uno de los solicitantes
     *
     * @author david.cifuentes
     */
    public void updateSelectedSolicitantes() {
        this.seleccionarSolicitantesPorCheck();

        if (this.solicitantesSelecionados != null && this.solicitantesSelecionados.length > 0) {

            if (this.solicitantesEnvioComunicacionNotificacion != null &&
                 this.solicitantesEnvioComunicacionNotificacion.size() == 1) {

                LOGGER.debug("comunicacionNotificacionResolucionMB#solicitanteSelectAll");
                this.solicitanteOAfectadoSeleccionado =
                    this.solicitantesEnvioComunicacionNotificacion.get(0);

                // Evaluar el estado de la comunicación de la notificación de resolución
                // para éste solicitante o afectado.
                this.revisarEstadoComunicacionDeNotificacion();

                // Reinicio de variables para un nuevo registro de comunicación
                this.medioComunicNotif = "";
                this.fechaComunicNotif = null;
                this.personaAtendio = "";
                this.fechaHoy = null;
                this.observacionesComunicNotif = "";
            }

        } else {
            this.solicitanteSelectBool = false;
            this.solicitanteOAfectadoSeleccionado = null;
        }
    }

    /**
     * Método usado para realizar el cargue de los solicitantes seleccionados mediante el checkBox.
     *
     * @author david.cifuentes
     */
    public void seleccionarSolicitantesPorCheck() {

        List<Solicitante> auxListaSolicitantes = new ArrayList<Solicitante>();
        for (Solicitante s : this.solicitantesEnvioComunicacionNotificacion) {
            if (s.isSelected()) {
                auxListaSolicitantes.add(s);
            }
        }

        if (auxListaSolicitantes.size() > 0) {
            this.solicitantesSelecionados = new Solicitante[auxListaSolicitantes.size()];
            for (int i = 0; i < auxListaSolicitantes.size(); i++) {
                this.solicitantesSelecionados[i] = auxListaSolicitantes.get(i);
            }
        } else {
            this.solicitantesSelecionados = null;
        }
    }

    //18779.  Cerrar trámite en cordis 15/09/2016 wilmanjose.vega
    //TODO: Deberia crearse un servicio de radicación.
    private boolean cerrarRadicacionTramite(final String radicado,
        final String radicadoResponde, final String usuario) {
        boolean resultado = true;
        final Object[] resultadoCierre = this.getTramiteService()
            .finalizarRadicacion(radicado, radicadoResponde, usuario);
        if (resultadoCierre != null) {
            for (final Object objeto : resultadoCierre) {
                final List lista = (List) objeto;
                if (lista != null && !lista.isEmpty()) {
                    int numeroErrores = 0;
                    for (final Object obj : lista) {
                        final Object[] arrObjetos = (Object[]) obj;
                        //TODO: Eliminar la validación del código de error de oracle luego de que 
                        //el servicio de CORDIS solucione el error: http://redmine.igac.gov.co/issues/21662
                        if (!arrObjetos[0].equals("0") &&
                            !arrObjetos[1].toString().contains("0-ORA-0000")) {
                            numeroErrores++;
                            LOGGER.error("Error >>>" + arrObjetos[0]);
                        }
                    }
                    if (numeroErrores > 0) {
                        this.addMensajeError("Error al cerrar radicación");
                        resultado = false;
                    }
                }
            }
        }
        return resultado;
    }

    // end of class
}
