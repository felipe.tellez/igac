package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluosContrato;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalContratoPago;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * ManagedBean para el caso de uso 083 de avalúos, sirve para mostrar la información de los pagos
 * parafiscales de un avaluador ( externo {@link ProfesionalAvaluo}
 *
 * @author christian.rodriguez
 *
 */
@Component("consultaPagosParafiscales")
@Scope("session")
public class ConsultaPagosParafiscalesAvaluadorExternoMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ConsultaPagosParafiscalesAvaluadorExternoMB.class);

    /**
     * Contiene los pagos parafiscales que se van a mostrar en la tabla
     */
    private List<ProfesionalContratoPago> pagosParafiscales;

    /**
     * Contiene el contrato asociado al avalúador
     */
    private ProfesionalAvaluosContrato contratoAvaluador;

    /**
     * Contiene el avaluador seleccionado
     */
    private ProfesionalAvaluo avaluador;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public List<ProfesionalContratoPago> getPagosParafiscales() {
        return this.pagosParafiscales;
    }

    public ProfesionalAvaluosContrato getContratoAvaluador() {
        return this.contratoAvaluador;
    }

    public ProfesionalAvaluo getAvaluador() {
        return this.avaluador;
    }

    // ---------------------------------Métodos---------------------------------
    // --------------------------------------------------------------------------
    /**
     * Método que se debe usar para cargar los datos requeridos por este caso de uso cuando es
     * llamado por otros managedBeans
     *
     * @author christian.rodriguez
     * @param avaluador {@link ProfesionalAvaluosContrato }
     * @param idAvaluador id del avaluador {@link ProfesionalAvaluo} asociado al contrato
     */
    public void cargarDesdeOtrosMB(ProfesionalAvaluosContrato contrato, Long idAvaluador) {

        this.contratoAvaluador = this.getAvaluosService()
            .obtenerProfesionalAvaluoContratoPorIdConAtributos(contrato.getId());

        this.avaluador = this.getAvaluosService().obtenerProfesionalAvaluoPorIdConAtributos(
            idAvaluador);

        this.pagosParafiscales = this.getAvaluosService()
            .consultarPagosParafiscalesPorProfesionalAvaluoContratoId(
                this.contratoAvaluador.getId());

    }

}
