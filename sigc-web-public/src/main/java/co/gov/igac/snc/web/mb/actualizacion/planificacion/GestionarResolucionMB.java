package co.gov.igac.snc.web.mb.actualizacion.planificacion;

import java.io.FileInputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.apiprocesos.nucleoPredial.actualizacion.ProcesoDeActualizacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.Convenio;
import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.entity.vistas.VJurisdiccionProducto;
import co.gov.igac.snc.persistence.util.EActualizacionTipoProceso;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para la gestión de resoluciones de actualización.
 *
 * @author jamir.avila
 *
 */
@Component("gestionarResolucion")
@Scope("session")
public class GestionarResolucionMB extends co.gov.igac.snc.web.util.SNCManagedBean
    implements Serializable {

    /** Nombre de la plantila usada para generar la resolución. */
    private static final String BORRADOR_RESOLUCION_ACTUALIZACION_CATASTRAL =
        "BORRADOR_RESOLUCION_ACTUALIZACION_CATASTRAL";
    /**
     * Identificador de versión por omisión.
     */
    private static final long serialVersionUID = 1L;

    /** Servicios de actualización */
    /**
     * Servicio de bitácora
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(GestionarResolucionMB.class);

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    private UsuarioDTO usuario;

    /** Nombre del departamento al que pertenece el municipio en actualización. */
    private String departamentoNombre;
    /** Nombre del municipio que va a ser actualizado. */
    private String municipioNombre;
    /** Nombre del tipo de proyecto: Misional o Convenio. */
    private String tipoProyectoNombre;
    /** Nombre la zona: urbana, rural, urbana/rural */
    private String zonaNombre;
    /** Número de convenio. */
    private String convenioNumero;
    /** Nombre del Director Territorial. */
    private String nombreDirectorTerritorial;
    /** Elaboración de la resolución. */
    private Resolucion resolucion;
    /** Años posibles de la resolución */
    private List<SelectItem> anios;
    /** lista de convenios para primefaces */
    private List<SelectItem> convenioNumeros;
    /** lista de convenios asociados con la resolución en curso */
    private List<Convenio> convenios;
    /** Nombre de la territorial. */
    private String territorialNombre;

    /** Motivo por el cual fue reprobada la resolución. */
    private String motivoReprobacion;

    /** Objeto actualización */
    private Actualizacion actualizacion;

    /** Banderas para seleccionar cuales de las pestanias se van a ver y en que casos */
    private boolean proyectarResolucion;
    private boolean revisarResolucion;

    /* ---------------------------------------------------- */
    /**
     * Constructor por omisión.
     */
    public GestionarResolucionMB() {
        Calendar calendario = Calendar.getInstance();
        int anio = calendario.get(Calendar.YEAR);
        anios = new LinkedList<SelectItem>();
        for (int i = anio; i < anio + 10; ++i) {
            anios.add(new SelectItem(i, "" + i));
        }
        convenioNumeros = new LinkedList<SelectItem>();
        convenios = new LinkedList<Convenio>();
    }

    /**
     * Método de inicialización invocado automáticamente una vez construidad la instancia.
     */
    @PostConstruct
    public void init() {

        cargarEncabezado();
        cargarResolucion();
    }

    /**
     * Inicializa el encabezado del formulario: departamento, municipio, tipo de proyecto, zona,
     * número de convenio.
     */
    private void cargarEncabezado() {
        //	Long idActualizacion = 250L;
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        Long idActualizacion = this.tareasPendientesMB.getInstanciaSeleccionada().
            getIdObjetoNegocio();

        actualizacion = this.getActualizacionService()
            .recuperarActualizacionPorId(idActualizacion);
        if (null == actualizacion) {
            FacesContext.getCurrentInstance().addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Registrar información",
                    "Imposible obtener el objeto actualización de id: " +
                     idActualizacion));
            LOGGER.error("Imposible obtener el objeto actualización de id: " +
                 idActualizacion);
        } else {

            tipoProyectoNombre = actualizacion.getTipoProceso();
            // Obtener el nombre del departamento
            Departamento departamento = actualizacion.getDepartamento();
            if (null != departamento) {
                departamentoNombre = departamento.getNombre();
            } else {
                LOGGER.error("Departamento de la actualización: " +
                     idActualizacion + " no encontrado!!!");
            }
            // Obtener el nombre del municipio
            Municipio municipio = actualizacion.getMunicipio();
            if (null != municipio) {
                municipioNombre = municipio.getNombre();
                if (null != municipio.getCodigo()) {
                    // Obtener la territorial
                    VJurisdiccionProducto vJurisdiccionProducto = this.getActualizacionService()
                        .recuperarJurisdiccionProductoPorCodigoMunicipio(municipio
                            .getCodigo());
                    if (null != vJurisdiccionProducto) {
                        this.territorialNombre = vJurisdiccionProducto
                            .getNombreTerritorial();
                        // Obtener el nombre del director territorial
                        if (territorialNombre != null) {
                            UsuarioDTO dtDTO = this.getActualizacionService().
                                recuperarDirectorTerritorial(territorialNombre);
                            if (dtDTO != null) {
                                nombreDirectorTerritorial = dtDTO.getNombreCompleto();
                                LOGGER.debug("nombreDirectorTerritorial: " +
                                    nombreDirectorTerritorial);
                            } else {
                                final String MENSAJE_ERROR =
                                    "Imposible recuperar el nombre del Director Territorial";
                                super.addMensajeError(MENSAJE_ERROR);
                                LOGGER.error(MENSAJE_ERROR);
                            }
                        }
                    }
                }

            } else {
                LOGGER.error("Municipio de la actualización: " +
                     idActualizacion + " no encontrado!!!");
            }

            convenios = this.getActualizacionService()
                .recuperarConveniosPorIdActualizacion(idActualizacion);
            if (convenios != null) {

                for (Convenio convenio : convenios) {
                    SelectItem si = new SelectItem(convenio.getId(),
                        convenio.getNumero());
                    convenioNumeros.add(si);
                    if (null == zonaNombre) {
                        zonaNombre = convenio.getZona();
                        convenioNumero = convenio.getNumero();
                        tipoProyectoNombre = convenio.getTipoProyecto();
                    }
                }
            }
        }
    }

    /**
     * Carga la resolución, si existe.
     */
    private void cargarResolucion() {
        Plantilla plantilla = this.getGeneralesService().recuperarPlantillaPorCodigo(
            BORRADOR_RESOLUCION_ACTUALIZACION_CATASTRAL);
        if (null != plantilla) {
            String contenidoHTML = plantilla.getHtml();
            if (null == contenidoHTML) {
                super.addMensajeError("Imposible recuperar la plantilla: " +
                    BORRADOR_RESOLUCION_ACTUALIZACION_CATASTRAL);
            } else {
                resolucion = new Resolucion(
                    EActualizacionTipoProceso.ACTUALIZACION.getDescripcion(), this, contenidoHTML);
            }
        }
    }

    public void preliminar() {
        LOGGER.info("g e n e r a r P r e l i m i n a r");

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context
            .getExternalContext().getResponse();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "inline=filename=file.pdf");

        byte[] resultados = null;
        try {
            FileInputStream fis = new FileInputStream("c:\\temp\\pruebas.pdf");
            FileChannel canal = fis.getChannel();
            int tamano = (int) canal.size();
            resultados = new byte[tamano];
            ByteBuffer destino = ByteBuffer.wrap(resultados);
            canal.read(destino);
            canal.close();

            response.getOutputStream().write(resultados);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            context.responseComplete();

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Evento de cambio de tarjeta.
     *
     * @param event
     */
    public void onCambioTarjeta(TabChangeEvent event) {
        if (event.getTab().getId().equalsIgnoreCase("revisarResolucionAP")) {
            FacesMessage msg = new FacesMessage("Cambió la tarjeta",
                "Tarjeta activa: " + event.getTab().getId());
            FacesContext.getCurrentInstance().addMessage("msgsDlgAP", msg);
            FacesContext.getCurrentInstance().addMessage("msgsAP", msg);
        }

        /*
         * FacesContext.getCurrentInstance().addMessage(null, msg);
         */
        LOGGER.info("Cambio de tarjeta a: " + event.getTab().getId());
    }

    public void onCambioVigencia() {
        LOGGER.info("Cambio de vigencia.");
    }

    public void onCambioFecha() {
        LOGGER.info("Cambio de fecha");
    }

    public void onManejadorCambioFecha(DateSelectEvent event) {
        LOGGER.info("Se seleccionó otra fecha");
    }

    public void onCambioConvenio() {
        LOGGER.info("Cambio de convenio: " + this.convenioNumero);
        if (convenioNumero != null) {
            long idConvenio = Long.parseLong(convenioNumero);
            for (Convenio convenio : convenios) {
                if (idConvenio == convenio.getId()) {
                    zonaNombre = convenio.getZona();
                    tipoProyectoNombre = convenio.getTipoProyecto();
                }
            }
        }
    }

    /**
     * Muestra la versión preliminar de la resolución.
     *
     * @param ae evento JSF.
     */
    public void cmdVerPreliminar(ActionEvent ae) {
        LOGGER.info("IMPLEMENTAR operación ver preliminar." +
             ae.getComponent().getId());
    }

    /**
     * Esta operación, ejecutada por el Director Territorial, indica la aprobación de la resolución.
     *
     * @param ae evento de JSF.
     * @return
     */
    public String cmdAprobar() {
        LOGGER.info("IMPLEMENTAR operación de aprobación.");
        boolean procesoFlag = false;

        procesoFlag = this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion,
            ERol.DIRECTOR_TERRITORIAL,
            ProcesoDeActualizacion.ACT_PLANIFICACION_AUTORIZAR_INICIO_ACTUALIZACION,
            this.usuario.getDescripcionTerritorial());
        if (procesoFlag) {
            UtilidadesWeb.removerManagedBean("gestionarResolucion");
            this.tareasPendientesMB.init();
            return "/tareas/listaTareas.jsf";
        } else {
            this.addMensajeError("Ha ocurrido un error al mover el proceso.");
            return null;
        }
    }

    /**
     * Operación, ejecutada por el Director Territorial, que indica que la resolución fue reprobada.
     *
     * @return
     */
    public String cmdReprobar() {
        LOGGER.info("IMPLEMENTAR operación de reprobación.");

        this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion,
            ERol.ABOGADO,
            ProcesoDeActualizacion.ACT_PLANIFICACION_PROYECTAR_RESOLUCION_ORDENA_EJECUCION,
            this.usuario.getDescripcionTerritorial());

        UtilidadesWeb.removerManagedBean("gestionarResolucion");
        this.tareasPendientesMB.init();
        return "/tareas/listaTareas.jsf";
    }

    /**
     * Operación, ejecutada por el Director Territorial, que ordena la publicación en el diario
     * oficial.
     *
     * @return
     */
    public String cmdPublicar() {
        LOGGER.info("IMPLEMENTAR operación de publicación en el diario oficial");
        return "/tareas/listaTareas.jsf";
    }

    /**
     * Esta operación cancela la elaboración.
     *
     * @return
     */
    public String cmdCancelar() {
        return "/tareas/listaTareas.jsf";
    }

    /**
     * Esta operación se encarga de proyectar la resolución.
     *
     * @return la siguiente página a mostrar al usuario.
     */
    public String cmdProyectar() {
        LOGGER.info("IMPLEMENTAR operación proyectar");

        actualizacion.setAnio(resolucion.getAnio());
        actualizacion.setFecha(resolucion.getFecha());

        //actualizacionService.actualizarActualizacion(actualizacion);
        this.getActualizacionService().ordenarAvanzarASiguienteActividad(
            this.currentActivity.getId(), this.actualizacion,
            ERol.DIRECTOR_TERRITORIAL,
            ProcesoDeActualizacion.ACT_PLANIFICACION_REVISAR_RESOLUCION_ORDENA_EJECUCION,
            this.usuario.getDescripcionTerritorial());

        UtilidadesWeb.removerManagedBean("gestionarResolucion");
        this.tareasPendientesMB.init();
        return "/tareas/listaTareas.jsf";
    }

    /* ---------------------------------------------------- */
    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getTipoProyectoNombre() {
        return tipoProyectoNombre;
    }

    public void setTipoProyectoNombre(String tipoProyectoNombre) {
        this.tipoProyectoNombre = tipoProyectoNombre;
    }

    public String getZonaNombre() {
        return zonaNombre;
    }

    public void setZonaNombre(String zonaNombre) {
        this.zonaNombre = zonaNombre;
    }

    public String getConvenioNumero() {
        return convenioNumero;
    }

    public void setConvenioNumero(String convenioNumero) {
        this.convenioNumero = convenioNumero;
    }

    public Resolucion getResolucion() {
        return resolucion;
    }

    public void setResolucion(Resolucion resolucion) {
        this.resolucion = resolucion;
    }

    public List<SelectItem> getAnios() {
        return anios;
    }

    public void setAnios(List<SelectItem> anios) {
        this.anios = anios;
    }

    public List<SelectItem> getConvenioNumeros() {
        return convenioNumeros;
    }

    public void setConvenioNumeros(List<SelectItem> convenioNumeros) {
        this.convenioNumeros = convenioNumeros;
    }

    public String getMotivoReprobacion() {
        return motivoReprobacion;
    }

    public void setMotivoReprobacion(String motivoReprobacion) {
        this.motivoReprobacion = motivoReprobacion;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public String getTerritorialNombre() {
        return territorialNombre;
    }

    public void setTerritorialNombre(String territorialNombre) {
        this.territorialNombre = territorialNombre;
    }

    public String getNombreDirectorTerritorial() {
        return nombreDirectorTerritorial;
    }

    public void setNombreDirectorTerritorial(String nombreDirectorTerritorial) {
        this.nombreDirectorTerritorial = nombreDirectorTerritorial;
    }

    public boolean isProyectarResolucion() {
        return proyectarResolucion;
    }

    public void setProyectarResolucion(boolean proyectarResolucion) {
        this.proyectarResolucion = proyectarResolucion;
    }

    public boolean isRevisarResolucion() {
        return revisarResolucion;
    }

    public void setRevisarResolucion(boolean revisarResolucion) {
        this.revisarResolucion = revisarResolucion;
    }
}
