package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.persistence.entity.generales.Zona;
import co.gov.igac.snc.persistence.entity.conservacion.Entidad;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioBloqueo;
import co.gov.igac.snc.persistence.entity.conservacion.PredioInconsistencia;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoBloqueo;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoDesBloqueo;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.Calendar;

@Component("consultaDatosUbicacionPredio")
@Scope("session")
public class ConsultaDatosUbicacionPredioMB extends SNCManagedBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4569529581251360613L;
    private static final Logger LOGGER = LoggerFactory.getLogger(
        ConsultaDatosUbicacionPredioMB.class);

    private Predio predio;
    private List<PredioInconsistencia> predioInconsistencias;

    private String nombreZona;
    private String nombreSector;
    private String nombreComuna;
    private String nombreBarrio;
    private String nombreManzanaVereda;
    private String etiquetaEstado;
    private String etiquetaEstadoDesbloqueado;
    private List<PredioBloqueo> predioDesBloqueos;
    private PredioBloqueo predioseleccionado;

    /**
     * Variable que almacena si el predio tiene inconsistencias
     *
     * @author javier.aponte
     */
    private boolean predioTieneInconsistencias;

    /**
     * Bandera para saber si el predio es fiscal
     *
     * @author jonathan.chacon
     */
    private boolean banderaPredioFiscal;

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public Predio getPredio() {
        return this.predio;
    }

    public String getNombreZona() {
        return nombreZona;
    }

    public void setNombreZona(String nombreZona) {
        this.nombreZona = nombreZona;
    }

    public String getNombreSector() {
        return nombreSector;
    }

    public void setNombreSector(String nombreSector) {
        this.nombreSector = nombreSector;
    }

    public String getNombreComuna() {
        return nombreComuna;
    }

    public void setNombreComuna(String nombreComuna) {
        this.nombreComuna = nombreComuna;
    }

    public String getNombreBarrio() {
        return nombreBarrio;
    }

    public void setNombreBarrio(String nombreBarrio) {
        this.nombreBarrio = nombreBarrio;
    }

    public String getNombreManzanaVereda() {
        return nombreManzanaVereda;
    }

    public void setNombreManzanaVereda(String nombreManzanaVereda) {
        this.nombreManzanaVereda = nombreManzanaVereda;
    }

    public List<PredioInconsistencia> getPredioInconsistencias() {
        return predioInconsistencias;
    }

    public void setPredioInconsistencias(
        List<PredioInconsistencia> predioInconsistencias) {
        this.predioInconsistencias = predioInconsistencias;
    }

    public boolean isPredioTieneInconsistencias() {
        return predioTieneInconsistencias;
    }

    public void setPredioTieneInconsistencias(boolean predioTieneInconsistencias) {
        this.predioTieneInconsistencias = predioTieneInconsistencias;
    }

    public boolean isBanderaPredioFiscal() {
        return banderaPredioFiscal;
    }

    public void setBanderaPredioFiscal(boolean banderaPredioFiscal) {
        this.banderaPredioFiscal = banderaPredioFiscal;
    }

    public String getEtiquetaEstado() {
        return etiquetaEstado;
    }

    public void setEtiquetaEstado(String etiquetaEstado) {
        this.etiquetaEstado = etiquetaEstado;
    }

    public String getEtiquetaEstadoDesbloqueado() {
        return etiquetaEstadoDesbloqueado;
    }

    public void setEtiquetaEstadodesbloqueado(String etiquetaEstadoDesbloqueado) {
        this.etiquetaEstadoDesbloqueado = etiquetaEstadoDesbloqueado;
    }

    public List<PredioBloqueo> getPredioDesBloqueos() {
        return predioDesBloqueos;
    }

    public void setPredioDesBloqueos(List<PredioBloqueo> predioDesBloqueos) {
        this.predioDesBloqueos = predioDesBloqueos;
    }

    public PredioBloqueo getPredioseleccionado() {
        return predioseleccionado;
    }

    public void setPredioseleccionado(PredioBloqueo predioseleccionado) {
        this.predioseleccionado = predioseleccionado;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * se define esta método solo para mantener el estándar del tener un método que hace la consulta
     * específica de los datos que van en cada pestaña de detalles del predio
     *
     * @modified pedro.garcia 21-03-2012
     * @param predioId
     */
    private void getPredioById(Long predioId) {
        this.predio = this.getConservacionService().obtenerPredioConDatosUbicacionPorId(predioId);
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        LOGGER.debug("init ConsultaDatosUbicacionPredioMB");

        //D: se obtiene la referencia al mb que maneja los detalles del predio
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        PredioBloqueo predioBloqueo;
        PredioBloqueo predioDesbloqueo;
        Long predioSeleccionadoId = mb001.getSelectedPredioId();
        //juan.cruz: Se usa para validar si el predio que se está consultando es un predio Origen.
        boolean esPredioOrigen = mb001.isPredioOrigen();
        if (predioSeleccionadoId != null) {
            if (!esPredioOrigen) {
                this.getPredioById(predioSeleccionadoId);
            } else {
                this.predio = mb001.getSelectedPredio1();
            }
            if (this.predio != null) {
                String numeroPredial = this.predio.getNumeroPredial();
                Zona zona = this.getGeneralesService().getCacheZonaByCodigo(numeroPredial.substring(
                    0, 7));
                Sector sector = this.getGeneralesService().getCacheSectorByCodigo(numeroPredial.
                    substring(0, 9));
                Comuna comuna = this.getGeneralesService().getCacheComunaByCodigo(numeroPredial.
                    substring(0, 11));
                Barrio barrio = this.getGeneralesService().getCacheBarrioByCodigo(numeroPredial.
                    substring(0, 13));
                ManzanaVereda manzanaVereda = this.getGeneralesService().
                    getCacheManzanaVeredaByCodigo(numeroPredial.substring(0, 17));
                this.nombreZona = zona.getNombre();
                this.nombreSector = sector.getNombre();
                this.nombreComuna = comuna.getNombre();
                this.nombreBarrio = barrio.getNombre();
                this.nombreManzanaVereda = manzanaVereda.getNombre();

                this.consultarInconsistenciasGeograficas();

                if (this.predio.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.
                    getCodigo())) {
                    this.banderaPredioFiscal = true;
                }

                predioBloqueo = this.getConservacionService().busquedaPredioBloqueoReciente(
                    this.predio.getNumeroPredial());
                this.etiquetaEstado = "";

                if (predioBloqueo != null) {
                    if (predioBloqueo.getTipoBloqueo() != null && !predioBloqueo.getTipoBloqueo().
                        isEmpty()) {
                        this.etiquetaEstado = predioBloqueo.getTipoBloqueo();
                    } else if (predioBloqueo.getFechaDesbloqueo() == null) {
                        this.etiquetaEstado = "SI";
                    } else {
                        Calendar calFechaActual = Calendar.getInstance();
                        Calendar calFechaDesbloqueo = Calendar.getInstance();
                        calFechaDesbloqueo.setTime(predioBloqueo.getFechaDesbloqueo());
                        this.etiquetaEstado = calFechaActual.before(calFechaDesbloqueo) ? "SI" :
                            "NO";
                    }
                } else {
                    this.etiquetaEstado = "NO";
                }

                predioDesbloqueo = this.getConservacionService().busquedaPredioDesBloqueoReciente(
                    this.predio.getNumeroPredial());
                this.etiquetaEstadoDesbloqueado = "";
                if (predioDesbloqueo != null) {
                    if (predioDesbloqueo.getTipoDesbloqueo() != null && !predioDesbloqueo.
                        getTipoDesbloqueo().isEmpty()) {
                        this.etiquetaEstadoDesbloqueado = predioDesbloqueo.getTipoDesbloqueo();
                    } else {
                        this.etiquetaEstadoDesbloqueado = "SI";
                    }
                }
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    public void cleanNumPredialAnteriorMB() {
        UtilidadesWeb.removerManagedBean("consultarNumeroPredialAnterior");
        ConsultaNumeroPredialAnteriorMB mbNumPredialAnterior =
            (ConsultaNumeroPredialAnteriorMB) UtilidadesWeb.getManagedBean(
                "consultarNumeroPredialAnterior");
        mbNumPredialAnterior.init();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del link que se muestra cuando el predio está en estado Bloqueado
     */
    public void cleanBloqueoPredioMB() {
        UtilidadesWeb.removerManagedBean("consultaBloqueoPredio");
        ConsultaBloqueoPredioMB mbBloqueoPredio =
            (ConsultaBloqueoPredioMB) UtilidadesWeb.getManagedBean("consultaBloqueoPredio");
        mbBloqueoPredio.init();
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de consultar los desbloqueos realizados al predio
     *
     * @author dumar.penuela
     */
    public void cargaPrediosDesbloqueados() {

        Entidad entidad;
        int size;

        this.predioDesBloqueos = this.getConservacionService().getPredioDesBloqueosByNumeroPredial(
            this.predio.getNumeroPredial());

        if (this.predioDesBloqueos != null) {

            size = this.predioDesBloqueos.size();

            for (int i = 0; i < size; i++) {
                entidad = this.getConservacionService().findEntidadById(this.predioDesBloqueos.
                    get(i).getEntidadId());
                this.predioDesBloqueos.get(i).setNombreEntidad(entidad.getNombre());
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Método encargado de consultar las inconsistencias del predio
     *
     * @author javier.aponte
     */
    public void consultarInconsistenciasGeograficas() {
        //Consultar las inconsistencias del predio
        this.predioInconsistencias = this.getConservacionService().
            obtenerPrediosInconsistenciaPorIdPredio(this.predio.getId());

        //Si hay inconsistencias establecer la varibale en hayInconsistenciasGeograficas en true
        if (this.predioInconsistencias != null && !this.predioInconsistencias.isEmpty()) {
            this.predioTieneInconsistencias = true;
        } else {
            this.predioTieneInconsistencias = false;
        }
    }

    /**
     * Retorna el numero predial con formato
     *
     * @param numero
     * @return
     */
    public String getFormatNumeroPredial() {
        String formatedNumPredial = "";

        if (this.predio != null) {
            String numero = this.predio.getNumeroPredial();

            if (numero != null && !numero.isEmpty()) {
                // Departamento
                formatedNumPredial = numero.substring(0, 2).concat("-");
                // Municipio
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(2, 5)).concat("-");
                // Tipo Avalúo
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(5, 7)).concat("-");
                // Sector
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(7, 9)).concat("-");
                // Comuna
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(9, 11)).concat("-");
                // Barrio
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(11, 13)).concat("-");
                // Manzana
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(13, 17)).concat("-");
                // Predio
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(17, 21)).concat("-");
                // Condición Propiedad
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(21, 22)).concat("-");
                // Edificio
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(22, 24)).concat("-");
                // Piso
                formatedNumPredial = formatedNumPredial.concat(
                    numero.substring(24, 26)).concat("-");
                // Unidad
                formatedNumPredial = formatedNumPredial.concat(numero
                    .substring(26, 30));
            }
        }
        return formatedNumPredial;

    }

}
