package co.gov.igac.snc.web.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converter para formatear el numero predial
 *
 * @author felipe.cadena
 */
public class NumeroPredialConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumeroPredialConverter.class);

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorString) {

        if (valorString == null || (valorString.trim().length() == 0)) {
            return valorString;
        }
        try {
            return ((String) valorString);
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al convertir el valor " + valorString);
            return valorString;
        }
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorObjeto) {

        try {
            String valorString = ((String) valorObjeto);

            return UtilidadesWeb.formatNumeroPredial(valorString);
        } catch (Exception e) {

            LOGGER.error("Ocurrio un error al convertir el valor " + valorObjeto);
            return (String) valorObjeto;
        }
    }
}
