package co.gov.igac.snc.web.mb.conservacion.productos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.ProcesoDeProductosCatastrales;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el registro de entrega de producto
 *
 * @author javier.aponte
 */
@Component("registroEntregaProducto")
@Scope("session")
public class RegistroEntregaProductoMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -3865342860839390196L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistroEntregaProductoMB.class);

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    private long idSolicitudDeTrabajo;

    /**
     * Solicitud seleccionada en el árbol de tareas
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Lista de los documentos soportes de los productos catastrales solicitados
     */
    private List<Documento> documentosSoportesPC;

    /**
     * Lista que contiene los productos catastrales asociados a la solicitud
     */
    private List<ProductoCatastral> productosCatastralesAsociados;

    /**
     * variable que contiene el producto catastral seleccionado
     */
    private ProductoCatastral productoCatastralSeleccionado;

    /**
     * variable para indicar si se activa el botón de entregar producto
     */
    private boolean activarBotonEntregarProducto;

// -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init RegistroDatosProductosSolicitudMB");

        try {
            this.usuario = MenuMB.getMenu().getUsuarioDto();

            this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

            //D: se obtiene el id del trámite
            this.idSolicitudDeTrabajo = this.tareasPendientesMB.getInstanciaSeleccionada().
                getIdObjetoNegocio();

            this.solicitudSeleccionada = this.getTramiteService().
                buscarSolicitudConSolicitantesSolicitudPorId(this.idSolicitudDeTrabajo);

            this.documentosSoportesPC = this.getTramiteService().buscarDocumentacionPorSolicitudId(
                this.idSolicitudDeTrabajo);

            this.productosCatastralesAsociados = this.getGeneralesService().
                buscarProductosCatastralesPorSolicitudId(this.idSolicitudDeTrabajo);

            if (this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.
                isEmpty()) {
                this.productoCatastralSeleccionado = this.productosCatastralesAsociados.get(0);
            } else {
                LOGGER.error(
                    "Ocurrió un error al consultar los productos catastrales asociados a la solicitud con id : " +
                    this.idSolicitudDeTrabajo);
            }

            //Se reclama la actividad por el usuario actual
            this.reclamarActividad(this.solicitudSeleccionada);

            this.activarBotonEntregarProducto = true;
        } catch (Exception ex) {
            LOGGER.error(
                "Ocurrió un error en el método init de RegistroDatosProductosSolicitudMB: " + ex.
                    getMessage(), ex);
        }
    }
    //--------------------------------------------------------------------------------------------------

    @Override
    public boolean validateProcess() {
        return false;
    }

    @Override
    public void setupProcessMessage() {

    }

    @Override
    public void doDatabaseStatesUpdate() {

        //Establecer la fecha del sistema a la entidad producto catastral
        Date fechaActual = new Date();
        for (ProductoCatastral pc : this.productosCatastralesAsociados) {
            pc.setFechaEntrega(fechaActual);
            pc.setEntregado(ESiNo.NO.getCodigo());
            this.getGeneralesService().guardarActualizarProductoCatastralAsociadoASolicitud(pc);
        }

    }

    //--------------------------------------MÉTODOS-----------------------------------------
    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Documento> getDocumentosSoportesPC() {
        return documentosSoportesPC;
    }

    public void setDocumentosSoportesPC(List<Documento> documentosSoportesPC) {
        this.documentosSoportesPC = documentosSoportesPC;
    }

    public List<ProductoCatastral> getProductosCatastralesAsociados() {
        return productosCatastralesAsociados;
    }

    public void setProductosCatastralesAsociados(
        List<ProductoCatastral> productosCatastralesAsociados) {
        this.productosCatastralesAsociados = productosCatastralesAsociados;
    }

    public ProductoCatastral getProductoCatastralSeleccionado() {
        return productoCatastralSeleccionado;
    }

    public void setProductoCatastralSeleccionado(
        ProductoCatastral productoCatastralSeleccionado) {
        this.productoCatastralSeleccionado = productoCatastralSeleccionado;
    }

    public boolean isActivarBotonEntregarProducto() {
        return activarBotonEntregarProducto;
    }

    public void setActivarBotonEntregarProducto(boolean activarBotonEntregarProducto) {
        this.activarBotonEntregarProducto = activarBotonEntregarProducto;
    }

    /**
     * Método encargado de registrar la entrega del producto y avanzar el proceso a controlar tiempo
     * de espera
     *
     * @author javier.aponte
     */
    public String avanzarProcesoAControlarTiempoDeEspera() {

        this.activarBotonEntregarProducto = false;

        String mensaje = null;

        this.doDatabaseStatesUpdate();

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(this.usuario);

            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_CORRECCIONES_PRODUCTOS_ENTREGADOS,
                usuarios));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("Avanzando la actividad a entregar producto");

            this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);

        } catch (Exception e) {
            mensaje = "Ocurrió un error al avanzar el proceso";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
        }

        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }

    }

    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author felipe.cadena
     */
    private boolean reclamarActividad(Solicitud solicitudReclamar) {
        Actividad actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        List<Actividad> actividades;

        if (actividad == null) {
            actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (actividades != null && !actividades.isEmpty()) {
                for (Actividad actTemp : actividades) {
                    if (actTemp.getIdObjetoNegocio() == solicitudReclamar.getId()) {
                        actividad = actTemp;
                        break;
                    }
                }
            }
        }

        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.usuario.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                        solicitudReclamar.getNumero()));
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.usuario);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                    solicitudReclamar.getNumero()));
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("registroEntregaProducto");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

//end of class
}
