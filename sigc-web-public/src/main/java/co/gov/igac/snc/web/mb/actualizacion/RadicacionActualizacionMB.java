/*
 * Proyecto SNC 2015
 */
package co.gov.igac.snc.web.mb.actualizacion;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Pais;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.actualizacion.MunicipioActualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.PredioActualizacion;
import co.gov.igac.snc.persistence.entity.conservacion.AxMunicipio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EMunicipioActualizacionEstado;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.persistence.util.ESolicitudTipo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.conservacion.asignacion.AdministracionAsignacionUsuariosMB;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Clase para manejar las radicaciones masivas de los tramites de actualizacion del sistema nacional
 * catastral
 *
 * @author felipe.cadena
 */
@Component("radicacionActualizacion")
@Scope("session")
public class RadicacionActualizacionMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 7576908178244023347L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionAsignacionUsuariosMB.class);

    private static final String ERROR = "error";

    /**
     * Variable item list con los valores de los departamentos disponibles
     */
    private List<SelectItem> departamentosItemList;

    /**
     * Variabl con el codigo del departamento seleccionado
     */
    private String departamentoSeleccionado;
    /**
     * Variable item list con los valores de los municipios disponibles
     */
    private List<SelectItem> municipiosItemList;
    /**
     * Variabl con el codigo del departamento seleccionado
     */
    private String municipioSeleccionado;

    /**
     * Usuario autenticado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Se almacenan los municipios clasificados por departamentos
     */
    private Map<String, List<SelectItem>> municipiosDeptos;

    /**
     * Lista de procesos de actualizacion activos.
     */
    private List<MunicipioActualizacion> procesosActualizacion;

    /**
     * Lista de procesos de actualizacion activos.
     */
    private List<PredioActualizacion> tramitesActualizacion;

    /**
     * Proceso de actualizacion seleccionado
     */
    private MunicipioActualizacion procesoActualizacionSeleccionado;

    private String archivoAlfanumerico;
    private String archivoGeografico;

    /**
     * Banderas
     */
    private boolean banderaValidarInformacion;
    private boolean banderaRadicarTramites;
    private boolean banderaReporteInconsistencias;
    private boolean banderaDatosRadicacion;
    private boolean banderaMensajeEspera;
    private boolean banderaSolicitudNueva;
    private boolean banderaActualizarArchivos;
    private boolean banderaZipCargado;

    /**
     * Solicitud asociada al proceso de actualizacion
     */
    private Solicitud solicitudActualizacion;

    /**
     * Solicitante relacionado el proceso de actualizacion
     */
    private SolicitanteSolicitud solicitanteActualizacion;

    /**
     * reportes
     */
    private ReporteDTO reporteBase;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * Variable de tipo streamed content para la descarga de reporte de inconsistencias
     */
    private StreamedContent reporteInconsistencias;
    /**
     * Variable de tipo streamed content para la descarga de reporte de radicacion
     */
    private StreamedContent reporteRadicacion;

    /**
     * archivo con la informacion de cargue del proceso de actualizacion
     */
    private File zipDatosActualizacion;

    /**
     * Ruta local archivo de actualizacion
     */
    private String rutaZipDatosActualizacion;

    // --------------------GETTERS AND SETTERS--------------------//
    public List<SelectItem> getDepartamentosItemList() {
        return departamentosItemList;
    }

    public void setDepartamentosItemList(List<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public boolean isBanderaZipCargado() {
        return banderaZipCargado;
    }

    public void setBanderaZipCargado(boolean banderaZipCargado) {
        this.banderaZipCargado = banderaZipCargado;
    }

    public StreamedContent getReporteRadicacion() {

        this.generarReporteRadicaciones();
        return reporteRadicacion;
    }

    public void setReporteRadicacion(StreamedContent reporteRadicacion) {
        this.reporteRadicacion = reporteRadicacion;
    }

    public StreamedContent getReporteInconsistencias() {

        this.generarReporteInconsistencias();

        return reporteInconsistencias;
    }

    public void setReporteInconsistencias(StreamedContent reporteInconsistencias) {
        this.reporteInconsistencias = reporteInconsistencias;
    }

    public List<SelectItem> getMunicipiosItemList() {
        return municipiosItemList;
    }

    public void setMunicipiosItemList(List<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public String getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    public void setDepartamentoSeleccionado(String departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    public String getMunicipioSeleccionado() {
        return municipioSeleccionado;
    }

    public void setMunicipioSeleccionado(String municipioSeleccionado) {
        this.municipioSeleccionado = municipioSeleccionado;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Map<String, List<SelectItem>> getMunicipiosDeptos() {
        return municipiosDeptos;
    }

    public void setMunicipiosDeptos(Map<String, List<SelectItem>> municipiosDeptos) {
        this.municipiosDeptos = municipiosDeptos;
    }

    public List<MunicipioActualizacion> getProcesosActualizacion() {
        return procesosActualizacion;
    }

    public void setProcesosActualizacion(List<MunicipioActualizacion> procesosActualizacion) {
        this.procesosActualizacion = procesosActualizacion;
    }

    public MunicipioActualizacion getProcesoActualizacionSeleccionado() {
        return procesoActualizacionSeleccionado;
    }

    public void setProcesoActualizacionSeleccionado(
        MunicipioActualizacion procesoActualizacionSeleccionado) {
        this.procesoActualizacionSeleccionado = procesoActualizacionSeleccionado;
    }

    public String getArchivoAlfanumerico() {
        return archivoAlfanumerico;
    }

    public void setArchivoAlfanumerico(String archivoAlfanumerico) {
        this.archivoAlfanumerico = archivoAlfanumerico;
    }

    public String getArchivoGeografico() {
        return archivoGeografico;
    }

    public void setArchivoGeografico(String archivoGeografico) {
        this.archivoGeografico = archivoGeografico;
    }

    public boolean isBanderaValidarInformacion() {
        return banderaValidarInformacion;
    }

    public void setBanderaValidarInformacion(boolean banderaValidarInformacion) {
        this.banderaValidarInformacion = banderaValidarInformacion;
    }

    public boolean isBanderaRadicarTramites() {
        return banderaRadicarTramites;
    }

    public void setBanderaRadicarTramites(boolean banderaRadicarTramites) {
        this.banderaRadicarTramites = banderaRadicarTramites;
    }

    public boolean isBanderaMensajeEspera() {
        return banderaMensajeEspera;
    }

    public void setBanderaMensajeEspera(boolean banderaMensajeEspera) {
        this.banderaMensajeEspera = banderaMensajeEspera;
    }

    public boolean isBanderaReporteInconsistencias() {
        return banderaReporteInconsistencias;
    }

    public void setBanderaReporteInconsistencias(boolean banderaReporteInconsistencias) {
        this.banderaReporteInconsistencias = banderaReporteInconsistencias;
    }

    public boolean isBanderaDatosRadicacion() {
        return banderaDatosRadicacion;
    }

    public void setBanderaDatosRadicacion(boolean banderaDatosRadicacion) {
        this.banderaDatosRadicacion = banderaDatosRadicacion;
    }

    public boolean isBanderaSolicitudNueva() {
        return banderaSolicitudNueva;
    }

    public void setBanderaSolicitudNueva(boolean banderaSolicitudNueva) {
        this.banderaSolicitudNueva = banderaSolicitudNueva;
    }

    public List<PredioActualizacion> getTramitesActualizacion() {
        return tramitesActualizacion;
    }

    public void setTramitesActualizacion(List<PredioActualizacion> tramitesActualizacion) {
        this.tramitesActualizacion = tramitesActualizacion;
    }

    public Solicitud getSolicitudActualizacion() {
        return solicitudActualizacion;
    }

    public void setSolicitudActualizacion(Solicitud solicitudActualizacion) {
        this.solicitudActualizacion = solicitudActualizacion;
    }

    public SolicitanteSolicitud getSolicitanteActualizacion() {
        return solicitanteActualizacion;
    }

    public void setSolicitanteActualizacion(SolicitanteSolicitud solicitanteActualizacion) {
        this.solicitanteActualizacion = solicitanteActualizacion;
    }

    public ReporteDTO getReporteBase() {
        return reporteBase;
    }

    public void setReporteBase(ReporteDTO reporteBase) {
        this.reporteBase = reporteBase;
    }

    public String getDepartamentoSeleccionadoNombre() {

        for (SelectItem si : this.departamentosItemList) {
            if (si.getValue().equals(this.departamentoSeleccionado)) {
                return si.getLabel();
            }
        }

        return "";
    }

    public String getMunicipioSeleccionadoNombre() {

        for (SelectItem si : this.municipiosItemList) {
            if (si.getValue().equals(this.municipioSeleccionado)) {
                return si.getLabel();
            }
        }

        return "";
    }

    // --------------------------METODOS--------------------------//
    @PostConstruct
    public void init() {
        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.cargarDepartamentos();

        this.banderaValidarInformacion = false;
        this.banderaRadicarTramites = false;
        this.banderaReporteInconsistencias = false;
        this.banderaDatosRadicacion = false;
        this.banderaMensajeEspera = false;

    }

    /**
     * Metodo para cargar archivos relacionados a un nuevo proceso de actualizacion
     *
     * @author felipe.cadena
     *
     * @param eventoCarga
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        String rutaZip = eventoCarga.getFile().getFileName();

        try {
            String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();

            this.zipDatosActualizacion = new File(directorioTemporal, rutaZip);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(this.zipDatosActualizacion);

            byte[] buffer;
            buffer = new byte[(int) eventoCarga.getFile().getSize()];

            int bulk;
            InputStream inputStream = eventoCarga.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }
            fileOutputStream.close();
            inputStream.close();

            this.rutaZipDatosActualizacion = this.zipDatosActualizacion.getCanonicalPath();

            this.addMensajeInfo(ECodigoErrorVista.RADICACION_ACTUALIZACION_012.toString());

            this.banderaZipCargado = true;

        } catch (IOException e) {
            this.addMensajeInfo(ECodigoErrorVista.RADICACION_ACTUALIZACION_011.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_011.getMensajeTecnico(), e);

        }
    }

    /**
     * Metodo para guardar el archivo zip asociado a la actualizacion en el ftp
     *
     * @author felipe.cadena
     */
    public void cargarZipFTP() {

        MunicipioActualizacion nuevoProceso;

        this.banderaZipCargado = false;

        if (!this.banderaActualizarArchivos) {
            if (this.departamentoSeleccionado == null || this.municipioSeleccionado == null) {

                this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_013.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(ERROR, ERROR);
                LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_013.getMensajeTecnico());
                return;

            }

            nuevoProceso = new MunicipioActualizacion();
            Departamento depto = new Departamento();
            depto.setCodigo(this.departamentoSeleccionado);
            Municipio mun = new Municipio();
            mun.setCodigo(this.municipioSeleccionado);

            nuevoProceso.setActivo(ESiNo.SI.getCodigo());
            nuevoProceso.setEstado(EMunicipioActualizacionEstado.CARGUE.getCodigo());
            nuevoProceso.setDepartamento(depto);
            nuevoProceso.setMunicipio(mun);
            nuevoProceso.setFechaLog(new Date());
            nuevoProceso.setUsuarioLog(this.usuario.getLogin());

        } else {
            nuevoProceso = this.procesoActualizacionSeleccionado;
            this.banderaActualizarArchivos = false;
        }

        nuevoProceso = this.getActualizacionService().guardarArchivosActualizacion(nuevoProceso,
            this.usuario, this.rutaZipDatosActualizacion);

        if (nuevoProceso.getArchivoAlfanumerico() == null || nuevoProceso.getArchivoAlfanumerico().
            isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_014.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_014.getMensajeTecnico());
        }
        //Actualiza la lista de procesos activos
        this.buscar();
    }

    /**
     * Metodo para actualizar los archivos de carga asociados a un proceso existente
     */
    public void actualizarArchivos() {

        this.banderaActualizarArchivos = true;

    }

    /**
     * Metodo para constuir la lista de departamentos asociados a la territorial
     *
     * @author felipe.cadena
     * @param departamentosList
     */
    public void cargarDepartamentosItemList(List<Departamento> departamentosList) {

        this.departamentosItemList = new ArrayList<SelectItem>();

        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                departamento.getNombre()));
        }
    }

    /**
     * Metodo para cargar los departamentos asociados a la territorial.
     *
     * @author felipe.cadena
     */
    public void cargarDepartamentos() {
        List<Departamento> deptosList;
        List<Municipio> municipiosList = new LinkedList<Municipio>();
        this.municipiosDeptos = new HashMap<String, List<SelectItem>>();

        List<SelectItem> municipiosItemListTemp;

        deptosList = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
            this.usuario.getCodigoTerritorial());

        this.cargarDepartamentosItemList(deptosList);

        List<AxMunicipio> axMuns = this.getConservacionService().obtenerAxMunicipioEnActualizacion();

        if (axMuns != null && !axMuns.isEmpty()) {
            for (Departamento dpto : deptosList) {
                municipiosList = this.getGeneralesService().getCacheMunicipiosByDepartamento(
                    dpto.getCodigo());

                municipiosItemListTemp = new ArrayList<SelectItem>();

                for (Municipio m : municipiosList) {
                    for (AxMunicipio axmun : axMuns) {
                        if (axmun.getMunicipioCodigo().equals(m.getCodigo())) {
                            municipiosItemListTemp.add(new SelectItem(m.getCodigo(),
                                m.getNombre()));
                        }
                    }
                }

                this.municipiosDeptos.put(dpto.getCodigo(), municipiosItemListTemp);

            }
        }
    }

    /**
     * Metodo para actualizar los usuarios asociados al rol seleccionado
     *
     * @author felipe.cadena
     */
    public void actualizarMunicipiosListener() {

        if (this.municipiosDeptos != null) {

            this.municipiosItemList = this.municipiosDeptos.get(this.departamentoSeleccionado);
        }

    }

    /**
     * Metodo para buscar los procesos previos de actualizacion
     *
     *
     * @author felipe.cadena
     */
    public void buscar() {

        this.procesosActualizacion = new ArrayList<MunicipioActualizacion>();
        this.procesosActualizacion = this.getActualizacionService().
            obtenerMunicipioActualizacionPorMunicipioYEstado(municipioSeleccionado, null, true, null);

    }

    /**
     * Metodo que inicia el proceso de validacion de la informacion de actualizacion
     *
     * @author felipe.cadena
     */
    public void iniciarValidacion() {
        //this.getGeneralesService().validarExistenciaDocumentoGestorDocumental(null);

        //Se obtienen los procesos existentes
        this.buscar();

        if (this.procesoActualizacionSeleccionado == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_010.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_010.getMensajeTecnico());
            this.archivoAlfanumerico = "";
            this.archivoGeografico = "";
            return;
        }

        Object[] resultado = this.getTramiteService().validarActualizacion(
            this.procesoActualizacionSeleccionado.getId(), this.usuario.getLogin());

        if (resultado == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_008.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_008.getMensajeTecnico());
            this.archivoAlfanumerico = "";
            this.archivoGeografico = "";
            return;
        }

        //se actualiza la informacion de la pantalla
        this.buscar();

        this.addMensajeInfo("Se inicio el proceso de validación de manera exitosa");

    }

    /**
     * Metodo que inicia el proceso radicacion
     *
     * @author felipe.cadena
     */
    public void iniciarRadicacion() {

        if (this.municipioSeleccionado == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_010.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_010.getMensajeTecnico());
            this.archivoAlfanumerico = "";
            this.archivoGeografico = "";
            return;
        }

        this.banderaDatosRadicacion = true;

        this.banderaSolicitudNueva = true;
        this.solicitudActualizacion = new Solicitud();
        this.solicitudActualizacion.setFecha(new Date());
        this.solicitudActualizacion.setTipo(ESolicitudTipo.TRAMITE_ACTUALIZACION.getCodigo());
        this.solicitudActualizacion.setAsunto(Constantes.TEXTO_ASUNTO_SOLICITUD_ACT);
        this.solicitudActualizacion.setSistemaEnvio("01");
        this.solicitudActualizacion.setTipoDocumentoCorrespondencia("999");
        this.solicitudActualizacion.setEstado("1");
        this.solicitudActualizacion.setUsuarioLog(this.usuario.getLogin());
        this.solicitudActualizacion.setFechaLog(new Date());

        Solicitante s = this.getTramiteService().
            buscarSolicitantePorTipoYNumeroDeDocumento(
                Constantes.NUMERO_DOC_SOLICITANTE_ACTUALIZACION,
                Constantes.TIPO_DOC_SOLICITANTE_ACTUALIZACION);

        EstructuraOrganizacional eu = this.getGeneralesService().
            obtenerEstructuraOrganizacionalPorMunicipioCodigo(municipioSeleccionado);
        Municipio m = this.getGeneralesService().getCacheMunicipioByCodigo(
            this.municipioSeleccionado);
        Departamento d = this.getGeneralesService().getCacheDepartamentoByCodigo(
            this.departamentoSeleccionado);
        Pais p = new Pais("140", "COLOMBIA", null, null);

        this.solicitanteActualizacion = new SolicitanteSolicitud();
        this.solicitanteActualizacion.setTelefonoPrincipal(eu.getTelefono());
        this.solicitanteActualizacion.setDireccion(eu.getDireccion());
        this.solicitanteActualizacion.setDireccionDepartamento(d);
        this.solicitanteActualizacion.setDireccionMunicipio(m);
        this.solicitanteActualizacion.setDireccionPais(p);
        this.solicitanteActualizacion.setSolicitante(s);
        this.solicitanteActualizacion.setRelacion(ESolicitanteSolicitudRelac.INTERMEDIARIO.
            getValor());
        this.solicitanteActualizacion.setTipoIdentificacion(s.getTipoIdentificacion());
        this.solicitanteActualizacion.setNumeroIdentificacion(s.getNumeroIdentificacion());
        this.solicitanteActualizacion.setNotificacionEmail(ESiNo.SI.getCodigo());
        this.solicitanteActualizacion.setUsuarioLog(this.usuario.getLogin());
        this.solicitanteActualizacion.setFechaLog(new Date());
        this.solicitanteActualizacion.setNotificacionEmail(ESiNo.SI.getCodigo());
        this.solicitanteActualizacion.setRazonSocial(s.getRazonSocial());

    }

    /**
     * Inicia todo el proceso de radicacion de actualizacion
     *
     * @author felipe.cadena
     */
    public void guardarSolicitudActualizacion() {

        int validoRadicar = this.getTramiteService().
            validarRadicacionActualizacion(this.procesoActualizacionSeleccionado.getId());

        if (validoRadicar == 0) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_015.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_015.getMensajeTecnico());
            return;
        }

        this.banderaMensajeEspera = true;

        List<SolicitanteSolicitud> solicitantes = new ArrayList<SolicitanteSolicitud>();
        solicitantes.add(this.solicitanteActualizacion);
        this.solicitudActualizacion.setSolicitanteSolicituds(solicitantes);
        this.solicitudActualizacion.setNumero(this.obtenerNumeroSolicitud(
            this.solicitudActualizacion, this.usuario));

        this.solicitudActualizacion.setSolicitanteSolicituds(new ArrayList<SolicitanteSolicitud>());

        this.solicitudActualizacion = this.getTramiteService().guardaYActualizaSolicitud(
            this.solicitudActualizacion);

        this.solicitanteActualizacion.setSolicitud(this.solicitudActualizacion);
        solicitantes = new ArrayList<SolicitanteSolicitud>();
        solicitantes.add(this.solicitanteActualizacion);
        this.solicitudActualizacion.setSolicitanteSolicituds(solicitantes);
        this.solicitudActualizacion = this.getTramiteService().guardaYActualizaSolicitud(
            this.solicitudActualizacion);

        Object[] resultado = this.getTramiteService().
            radicarActualizacion(this.procesoActualizacionSeleccionado.getId(),
                this.solicitudActualizacion.getId(), this.usuario.getLogin());

        if (resultado == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_007.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_007.getMensajeTecnico());
            return;
        }

        //Se actualizan los datos luego de la radicacion
        this.buscar();

    }

    /**
     * Muestra el reporte de inconsistencias cuando la validacion falla
     *
     * @author felipe.cadena
     */
    public void mostrarDetalleRadicacion() {

        this.banderaSolicitudNueva = false;
        this.banderaMensajeEspera = false;
        this.banderaDatosRadicacion = true;
        this.solicitudActualizacion = this.getTramiteService().
            buscarSolicitudConSolicitantesSolicitudPorIdNoTramites(
                this.procesoActualizacionSeleccionado.getSolicitud().getId());
        this.solicitanteActualizacion = this.solicitudActualizacion.getSolicitanteSolicituds().
            get(0);

        if (this.tramitesActualizacion != null) {
            this.tramitesActualizacion.clear();
        }

        //TODO::Definir reporte de inconsistencias
    }

    /**
     * Muestra el reporte de inconsistencias cuando la validacion falla
     *
     * @author felipe.cadena
     */
    public void generarReporteInconsistencias() {

        List<Documento> docsReporte = this.getGeneralesService().
            buscarDocumentoPorNumeroDocumentoYTipo(
                this.procesoActualizacionSeleccionado.getId() + "-" +
                this.procesoActualizacionSeleccionado.getMunicipio().getCodigo(),
                ETipoDocumento.REPORTE_INCONSISTENCIAS_ACTUALIZACION.getId()
            );
        if (docsReporte != null && !docsReporte.isEmpty()) {

            Documento docReporte = docsReporte.get(0);

            ReporteDTO rdto;
            ReportesUtil rUtil = ReportesUtil.getInstance();
            rdto = rUtil.consultarReporteDeGestorDocumental(docReporte.getIdRepositorioDocumentos());
            this.reporteInconsistencias = rdto.getStreamedContentReporte();
        }

        if (this.reporteInconsistencias == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_002.toString());
            LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_002.getMensajeTecnico());
        }
    }

    /**
     * Muestra el reporte de tramites que fueron radicados en el proceso de actualizacion solo se
     * puede generar cuando se realizo de forma exitosa dicho proceso
     *
     * @author felipe.cadena
     */
    public void generarReporteRadicaciones() {

        ReporteDTO rdto;
        ReportesUtil rUtil = ReportesUtil.getInstance();

        List<Documento> docsReporte = this.getGeneralesService().
            buscarDocumentoPorNumeroDocumentoYTipo(
                this.procesoActualizacionSeleccionado.getId() + "-" +
                this.procesoActualizacionSeleccionado.getMunicipio().getCodigo(),
                ETipoDocumento.REPORTE_RADICACION_ACTUALIZACION.getId()
            );

        if (docsReporte != null && !docsReporte.isEmpty()) {
            Documento docReporte = docsReporte.get(0);
            rdto = rUtil.consultarReporteDeGestorDocumental(docReporte.getIdRepositorioDocumentos());
            this.reporteRadicacion = rdto.getStreamedContentReporte();
        }

        if (this.reporteRadicacion == null) {

            //javier.aponte :: Si el reporte no está se genera :: 11/08/2016
            EReporteServiceSNC enumeracionReporte =
                EReporteServiceSNC.CONSTANCIA_RADICACION_SOLICITUD_ACTUALIZACION;

            Map<String, String> parameters = new HashMap<String, String>();

            parameters.put("SOLICITUD_ID", String.valueOf(this.solicitudActualizacion.getId()));

            try {
                rdto = rUtil.generarReporte(parameters, enumeracionReporte, this.usuario);
                this.reporteRadicacion = rdto.getStreamedContentReporte();

                if (rdto != null) {
                    this.guardarReporteConstanciaRadicacion(this.solicitudActualizacion, rdto);
                }

            } catch (Exception e) {
                this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_003.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam(ERROR, ERROR);
                LOGGER.warn(ECodigoErrorVista.RADICACION_ACTUALIZACION_003.getMensajeTecnico());
            }

        }

    }

    /**
     * Muestra el reporte de inconsistencias cuando la validacion falla
     *
     * @author felipe.cadena
     */
    public void eliminarProcesoActualizacion() {

        //TODO::Invocar eliminacion de proceso de actualizacion
        Object[] resultado = this.getTramiteService().
            eliminarActualizacion(this.procesoActualizacionSeleccionado.getId(), this.usuario.
                getLogin());

        if (resultado == null) {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_009.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam(ERROR, ERROR);
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_009.getMensajeTecnico());
            this.archivoAlfanumerico = "";
            this.archivoGeografico = "";
            return;
        }
        this.buscar();
    }

    /**
     * Listener de la seleccion de la tabla de proceso
     *
     * @author felipe.cadena
     * @param event
     */
    public void seleccionarProcesoListener(SelectEvent event) {

        this.banderaRadicarTramites = false;
        this.banderaValidarInformacion = false;
        this.banderaReporteInconsistencias = false;
        this.banderaDatosRadicacion = false;

        if (this.municipioSeleccionado != null || this.departamentoSeleccionado != null) {
            this.banderaValidarInformacion = true;
        }

        if (this.procesoActualizacionSeleccionado.getEstado().
            equals(EMunicipioActualizacionEstado.VALIDACION_FALLIDA.getCodigo())) {
            this.banderaReporteInconsistencias = true;
        }

        if (this.procesoActualizacionSeleccionado.getEstado().
            equals(EMunicipioActualizacionEstado.VALIDACION_EXITOSA.getCodigo())) {
            this.banderaRadicarTramites = true;
        }

    }

    /**
     * Listener de la seleccion de la tabla de proceso
     *
     * @author felipe.cadena
     * @param event
     */
    public void seleccionarMunicipioListener() {

        if (this.municipioSeleccionado != null || this.departamentoSeleccionado != null) {
            this.banderaValidarInformacion = true;
        }

    }

    /**
     * Método encargado de terminar la sesión
     *
     * @author felipe.cadena
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("radicacionActualizacion");
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Genera un número de solicitud desde correspondencia.
     *
     * @param solicitud
     * @return
     */
    private String obtenerNumeroSolicitud(Solicitud solicitud, UsuarioDTO usuario) {

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Territorial=compania
        parametros.add(usuario.getLogin()); // Usuario
        parametros.add("ER"); // tipo correspondencia
        parametros.add(solicitud.getTipo()); // tipo contenido solicitud.getTipo()
        parametros.add(new BigDecimal(solicitud.getFolios()));
        parametros.add(new BigDecimal(solicitud.getAnexos()));
        parametros.add(solicitud.getAsunto()); // Asunto
        parametros.add(solicitud.getObservaciones()); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add(solicitud.getSistemaEnvio()); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("1"); // presentacion, 6 internet
        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(usuario.getCodigoEstructuraOrganizacional()); // Destino lugar
        parametros.add(""); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(""); // Direccion destino
        parametros.add(""); // Tipo identificacion destino
        parametros.add(""); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getTipoIdentificacion()); // Solicitante tipo documento
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getNumeroIdentificacion()); // Solicitante  identificacion
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getNombreCompleto()); //
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getTelefonoPrincipal()); //
        parametros.add(solicitud.getSolicitanteSolicituds().get(0).getCorreoElectronico()); //

        Object[] resultado = this.getGeneralesService().radicarEnCorrespondencia(parametros);

        if (resultado != null && resultado.length > 0 &&
            !((new BigDecimal(-1)).equals(resultado[0]))) {
            solicitud.setNumero(resultado[5].toString());
        } else {
            this.addMensajeError(ECodigoErrorVista.RADICACION_ACTUALIZACION_006.toString());
            LOGGER.error(ECodigoErrorVista.RADICACION_ACTUALIZACION_006.getMensajeTecnico());
        }
        return solicitud.getNumero();
    }

    /**
     * Muestra los tramites asociados al proceso de actualizacion
     *
     * @author felipe.cadena
     */
    public void mostrarTramites() {
        this.tramitesActualizacion = this.getActualizacionService()
            .obtenerPredioActualizacionPorSolicitudId(this.procesoActualizacionSeleccionado.
                getSolicitud().getId());
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que crea un documento y un solicitud documento. Luego guarda el documento en la base
     * de datos y en Alfresco. Posteriormente asocia el documento a un trámite documento que también
     * lo guarda en la base de datos.
     *
     * @author javier.aponte
     * @throws Exception
     */
    public void guardarReporteConstanciaRadicacion(Solicitud solicitudTemporal, ReporteDTO rdto) {

        SolicitudDocumento solicitudDocumento = null;
        Documento documento = null;

        solicitudDocumento = new SolicitudDocumento();
        documento = new Documento();

        solicitudDocumento.setSolicitud(solicitudTemporal);
        solicitudDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        solicitudDocumento.setUsuarioLog(this.usuario.getLogin());

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.REPORTE_RADICACION_ACTUALIZACION
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (rdto != null) {
                documento.setArchivo(rdto.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.usuario.getLogin());
            documento.setUsuarioLog(this.usuario.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setEstructuraOrganizacionalCod(this.usuario.
                getCodigoEstructuraOrganizacional());
            documento.setNumeroDocumento(this.procesoActualizacionSeleccionado.getId() + "-" +
                this.procesoActualizacionSeleccionado.getMunicipio().getCodigo());

            DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
                .crearArgumentoCargarSolicitudAvaluoComercial(solicitudTemporal
                    .getNumero(), solicitudTemporal.getFecha(),
                    ETipoDocumento.CONSTANCIA_DE_RADICACION.getNombre(),
                    rdto.getRutaCargarReporteAAlfresco());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getGeneralesService()
                .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                    datosGestorDocumental, documento);

            if (documento != null && documento.getIdRepositorioDocumentos() != null &&
                 !documento.getIdRepositorioDocumentos().trim().isEmpty()) {
                solicitudDocumento.setSoporteDocumento(documento);
                solicitudDocumento.
                    setIdRepositorioDocumentos(documento.getIdRepositorioDocumentos());
                solicitudDocumento.setFecha(new Date(System.currentTimeMillis()));

                solicitudDocumento = this.getTramiteService().guardarActualizarSolicitudDocumento(
                    solicitudDocumento);
            }

        } catch (Exception e) {
            LOGGER.error("Error al guardar la constancia de radicación en el gestor documental:" +
                " para la solicitud con número de radicación: " + solicitudTemporal.getNumero() + e.
                getMessage(), e);
        }
    }

}
