package co.gov.igac.snc.web.mb.conservacion.productos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.contenedores.ActividadUsuarios;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.ProcesoDeProductosCatastrales;
import co.gov.igac.snc.apiprocesos.nucleoPredial.productosCatrastrales.objetosNegocio.SolicitudProductoCatastral;
import co.gov.igac.snc.apiprocesos.procesos.comun.EParametrosConsultaActividades;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.apiprocesos.tareas.comun.EEstadoActividad;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.GrupoProducto;
import co.gov.igac.snc.persistence.entity.tramite.Producto;
import co.gov.igac.snc.persistence.entity.tramite.ProductoCatastral;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EProductoCatastralEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean para el ingreso de solicitud de productos para cuando se devuelve de la pantalla de
 * registrar o ajustar productos solicitados e informar catastral, revisar, imprimir o exportar
 *
 * @author javier.aponte
 */
@Component("ingresoSolicitudP")
@Scope("session")
public class IngresoSolicitudProductosMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -3865342860839390196L;

    private static final Logger LOGGER = LoggerFactory.getLogger(IngresoSolicitudProductosMB.class);

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    // **************** SERVICIOS ********************
    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * BPM
     */
    private Actividad currentProcessActivity;

    private long idSolicitudDeTrabajo;

    /**
     * Solicitud seleccionada en el árbol de tareas
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Lista de los documentos soportes de los productos catastrales solicitados
     */
    private List<Documento> documentosSoportesPC;

    /**
     * Lista que contiene los productos catastrales asociados a la solicitud
     */
    private List<ProductoCatastral> productosCatastralesAsociados;

    /**
     * Producto catastral seleccionado
     */
    private ProductoCatastral productoCatastralSeleccionado;

    /**
     * Producto seleccionado
     */
    private Producto productoSeleccionado;

    /**
     * Variable que contiene el grupo producto seleccionado
     */
    private GrupoProducto grupoProductoSeleccionado;

    /**
     * Lista que contiene los productos por grupo
     */
    private List<SelectItem> productoPorGrupoV;

    /**
     * Variable booleana que indica si se va a editar un producto catastral seleccionado en la tabla
     * de lista de productos asociados a la solicitud
     */
    private boolean editarProductoCatastral;

    //Banderas para saber de que parte del proceso fue devuelto
    //Bandera para saber si el trámite es nuevo o viene de registro de datos a productos solicitados,
    //o viene de registrar correcciones a productos entregados
    private boolean vieneDeRegistroDatosONuevo;

    private boolean vieneDeRevisarProductosGenerados;

    private boolean vieneDeRegistrarCorreccionesAProductosEntregados;

// -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init RegistroDatosProductosSolicitudMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        try {

            this.currentProcessActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

            //D: se obtiene el id del trámite
            this.idSolicitudDeTrabajo = this.tareasPendientesMB.getInstanciaSeleccionada().
                getIdObjetoNegocio();

            this.solicitudSeleccionada = this.getTramiteService().
                buscarSolicitudConSolicitantesSolicitudPorId(this.idSolicitudDeTrabajo);

            this.documentosSoportesPC = this.getTramiteService().buscarDocumentacionPorSolicitudId(
                this.idSolicitudDeTrabajo);

            this.productosCatastralesAsociados = this.getGeneralesService().
                buscarProductosCatastralesPorSolicitudId(this.idSolicitudDeTrabajo);

            //Inicializa banderas para identificar de que parte del proceso viene
            this.vieneDeRegistroDatosONuevo = true;

            this.vieneDeRevisarProductosGenerados = false;

            this.vieneDeRegistrarCorreccionesAProductosEntregados = false;

            this.establecerVariablesDeProcesos();

            //Se reclama la actividad
            this.reclamarActividad(this.solicitudSeleccionada);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError(e);
        }

    }

    /**
     * Método encargado de establecer las variables asociadas a procesos
     *
     * @author javier.aponte
     * @modified by leidy.gonzalez 22/09/2014 Se agregan nuevos filtros para realizar la consulta de
     * lista de actividades
     */
    public void establecerVariablesDeProcesos() {

        Map<EParametrosConsultaActividades, String> filtros =
            new EnumMap<EParametrosConsultaActividades, String>(EParametrosConsultaActividades.class);
        filtros.put(EParametrosConsultaActividades.ID_PRODUCTOS_CATASTRALES,
            this.solicitudSeleccionada.getId().toString());
        filtros.put(EParametrosConsultaActividades.NUMERO_SOLICITUD, this.solicitudSeleccionada.
            getNumero());
        filtros.put(EParametrosConsultaActividades.ESTADOS, EEstadoActividad.TAREAS_VIGENTES);
        filtros.put(EParametrosConsultaActividades.USUARIO_POTENCIAL, this.usuario.getLogin());

        List<Actividad> actividades = null;

        try {
            actividades = this.getProcesosService().consultarListaActividades(filtros);

            if (actividades != null && !actividades.isEmpty()) {

                if (actividades.size() > 1) {
                    if (ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REVISAR_PRODUCTOS_GENERADOS.
                        equals(actividades.get(actividades.size() - 2).getNombre())) {
                        this.vieneDeRevisarProductosGenerados = true;
                        this.vieneDeRegistroDatosONuevo = false;
                    } else if (ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_CORRECCIONES_PRODUCTOS_ENTREGADOS.
                        equals(actividades.get(actividades.size() - 2).getNombre())) {
                        this.vieneDeRegistrarCorreccionesAProductosEntregados = true;
                        this.vieneDeRegistroDatosONuevo = false;
                    }
                }

            }
        } catch (ExcepcionSNC ex) {
            LOGGER.error("Error consultando la lista de actividades por el número de solicitud: " +
                this.solicitudSeleccionada.getNumero(), ex);
        }

    }

    //--------------------------------------------------------------------------------------------------
    @Override
    public boolean validateProcess() {
        return false;
    }

    @Override
    public void setupProcessMessage() {

    }

    @Override
    public void doDatabaseStatesUpdate() {

        for (ProductoCatastral pc : this.productosCatastralesAsociados) {

            this.getGeneralesService().guardarActualizarProductoCatastralAsociadoASolicitud(pc);

        }

    }

    //--------------------------------------MÉTODOS-----------------------------------------
    public Solicitud getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public List<Documento> getDocumentosSoportesPC() {
        return documentosSoportesPC;
    }

    public void setDocumentosSoportesPC(List<Documento> documentosSoportesPC) {
        this.documentosSoportesPC = documentosSoportesPC;
    }

    public List<ProductoCatastral> getProductosCatastralesAsociados() {
        return productosCatastralesAsociados;
    }

    public void setProductosCatastralesAsociados(
        List<ProductoCatastral> productosCatastralesAsociados) {
        this.productosCatastralesAsociados = productosCatastralesAsociados;
    }

    public ProductoCatastral getProductoCatastralSeleccionado() {
        return productoCatastralSeleccionado;
    }

    public void setProductoCatastralSeleccionado(
        ProductoCatastral productoCatastralSeleccionado) {
        this.productoCatastralSeleccionado = productoCatastralSeleccionado;
    }

    public Producto getProductoSeleccionado() {
        return productoSeleccionado;
    }

    public void setProductoSeleccionado(Producto productoSeleccionado) {
        this.productoSeleccionado = productoSeleccionado;
    }

    public GrupoProducto getGrupoProductoSeleccionado() {
        return grupoProductoSeleccionado;
    }

    public void setGrupoProductoSeleccionado(GrupoProducto grupoProductoSeleccionado) {
        this.grupoProductoSeleccionado = grupoProductoSeleccionado;
    }

    public List<SelectItem> getProductoPorGrupoV() {
        return productoPorGrupoV;
    }

    public void setProductoPorGrupoV(List<SelectItem> productoPorGrupoV) {
        this.productoPorGrupoV = productoPorGrupoV;
    }

    public boolean isVieneDeRegistroDatosONuevo() {
        return vieneDeRegistroDatosONuevo;
    }

    public void setVieneDeRegistroDatosONuevo(boolean vieneDeRegistroDatosONuevo) {
        this.vieneDeRegistroDatosONuevo = vieneDeRegistroDatosONuevo;
    }

    public boolean isVieneDeRegistrarCorreccionesAProductosEntregados() {
        return vieneDeRegistrarCorreccionesAProductosEntregados;
    }

    public void setVieneDeRegistrarCorreccionesAProductosEntregados(
        boolean vieneDeRegistrarCorreccionesAProductosEntregados) {
        this.vieneDeRegistrarCorreccionesAProductosEntregados =
            vieneDeRegistrarCorreccionesAProductosEntregados;
    }

    public boolean isVieneDeRevisarProductosGenerados() {
        return vieneDeRevisarProductosGenerados;
    }

    public void setVieneDeRevisarProductosGenerados(
        boolean vieneDeRevisarProductosGenerados) {
        this.vieneDeRevisarProductosGenerados = vieneDeRevisarProductosGenerados;
    }

    public boolean isEditarProductoCatastral() {
        return editarProductoCatastral;
    }

    public void setEditarProductoCatastral(boolean editarProductoCatastral) {
        this.editarProductoCatastral = editarProductoCatastral;
    }

    /**
     * Método encargado de buscar una lista de productos por el id del grupo
     *
     * @author javier.aponte
     */
    public void buscarProductoPorGrupoId() {

        this.productoPorGrupoV = this.generalMB.getCodigoProductosV(this.grupoProductoSeleccionado.
            getId());

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de eliminar productos catastrales asociados
     * en la ventana de adicionar productos
     *
     * @author javier.aponte
     */
    public void eliminarProductosCatastralesAsociados() {

        this.productosCatastralesAsociados.remove(this.productoCatastralSeleccionado);

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de editar productos catastrales asociados en
     * la ventana de adicionar productos
     *
     * @author javier.aponte
     */
    public void editarProductosCatastralesAsociados() {

        this.productoSeleccionado = this.productoCatastralSeleccionado.getProducto();

        this.grupoProductoSeleccionado = this.productoSeleccionado.getGrupoProducto();

        this.productoPorGrupoV = this.generalMB.getCodigoProductosV(this.grupoProductoSeleccionado.
            getId());

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de aceptar en la pantalla de adicionar
     * producto
     *
     * @author javier.aponte
     */
    public void adicionarProductoCatastralAsociadoASolicitud() {

        if (!this.editarProductoCatastral) {
            boolean productoYaFueAsociado = false;

            if (this.productosCatastralesAsociados != null && !this.productosCatastralesAsociados.
                isEmpty()) {
                for (ProductoCatastral pcTemp : this.productosCatastralesAsociados) {
                    if (pcTemp.getProducto().getCodigo().equals(this.productoSeleccionado.
                        getCodigo())) {
                        pcTemp.setCantidad(pcTemp.getCantidad() +
                            this.productoCatastralSeleccionado.getCantidad());
                        productoYaFueAsociado = true;
                        break;
                    }
                }
            }

            if (!productoYaFueAsociado) {

                this.productoSeleccionado.setGrupoProducto(this.grupoProductoSeleccionado);

                this.productoCatastralSeleccionado.setProducto(this.productoSeleccionado);

                if (this.productosCatastralesAsociados == null) {
                    this.productosCatastralesAsociados = new ArrayList<ProductoCatastral>();
                }
                this.productosCatastralesAsociados.add(this.productoCatastralSeleccionado);
            }
        }

    }

    /**
     * Método que se ejecuta cuando se oprime el botón de adicionar producto catastral y que se
     * encarga de inicializar las variables necesarias para adicionar un producto catastral
     *
     * @author javier.aponte
     */
    public void inicializarProductoCatastralAsociadoASolicitud() {

        this.productoCatastralSeleccionado = new ProductoCatastral();

        this.productoCatastralSeleccionado.setUsuarioLog(this.usuario.getLogin());
        this.productoCatastralSeleccionado.setFechaLog(new Date());
        this.productoCatastralSeleccionado.setSolicitud(this.solicitudSeleccionada);
        this.productoCatastralSeleccionado.setDatosExtendidos(ESiNo.NO.getCodigo());
        this.productoCatastralSeleccionado.setDatosPropietarios(ESiNo.NO.getCodigo());
        this.productoCatastralSeleccionado.setEntregado(ESiNo.NO.getCodigo());
        this.productoCatastralSeleccionado.setEstado(EProductoCatastralEstado.SIN_EJECUTAR.
            getCodigo());

        this.productoSeleccionado = new Producto();
        this.grupoProductoSeleccionado = new GrupoProducto();
        this.productoPorGrupoV = new ArrayList<SelectItem>();

    }

    /**
     * Método encargado de buscar un producto por código
     *
     * @author javier.aponte
     */
    public void buscarProductoPorCodigo() {

        this.productoSeleccionado = this.getGeneralesService().obtenerProductoPorCodigo(
            this.productoSeleccionado.getCodigo());

    }

    /**
     * Método encargado de avanzar la actividad de solicitud de productos a registrar o ajustar
     * datos de solicitud de productos
     *
     * @author javier.aponte
     */
    public String avanzarProcesoARegistrarDatosDeProductos() {

        String mensaje = null;

        this.doDatabaseStatesUpdate();

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(this.usuario);
            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REGISTRAR_AJUSTAR_PRODUCTOS_TRAMITE_CATASTRAL,
                usuarios));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("Avanzando la actividad a registrar ajustar productos trámite catastral");

            this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);

        } catch (Exception e) {
            mensaje = "Ocurrió un error al avanzar el proceso";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
        }

        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return cerrarPaginaPrincipal();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }

    }

    /**
     * Método encargado de avanzar la actividad de solicitud de productos a revisar, imprimir o
     * exportar productos
     *
     * @author javier.aponte
     */
    public String avanzarProcesoARevisarProductosGenerados() {

        //Avanzar proceso
        String mensaje = null;

        this.doDatabaseStatesUpdate();

        //Avanzar el proceso
        try {
            SolicitudProductoCatastral spc = new SolicitudProductoCatastral();

            spc.setNumeroSolicitud(this.solicitudSeleccionada.getNumero());
            spc.setIdentificador(this.solicitudSeleccionada.getId());

            Calendar cal = Calendar.getInstance();
            cal.setTime(this.solicitudSeleccionada.getFecha());
            spc.setFechaSolicitud(cal);

            spc.setTipoSolicitud(this.solicitudSeleccionada.getTipo());

            UsuarioDTO controladorDeCalidad = this.obtenerControladorCalidadProductos();

            //Se valida que exista el controlador de calidad
            if (controladorDeCalidad == null) {
                this.addMensajeError(
                    "No existe usuario con el rol de control de calidad de productos");
                return null;
            }

            ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
            usuarios.add(controladorDeCalidad);

            List<ActividadUsuarios> transicionesUsuarios = new ArrayList<ActividadUsuarios>();
            transicionesUsuarios.add(new ActividadUsuarios(
                ProcesoDeProductosCatastrales.ACT_PRODUCTOS_REVISAR_PRODUCTOS_GENERADOS, usuarios));
            spc.setActividadesUsuarios(transicionesUsuarios);

            LOGGER.debug("Avanzando la actividad a registrar ajustar productos trámite catastral");

            this.getProcesosService().avanzarActividad(this.currentProcessActivity.getId(), spc);

        } catch (Exception e) {
            mensaje = "Ocurrió un error al avanzar el proceso";
            LOGGER.error(mensaje + " " + e.getMessage(), e);
        }

        if (mensaje == null) {
            this.tareasPendientesMB.init();
            return this.cerrarPaginaPrincipal();
        } else {
            this.addMensajeError(mensaje);
            return null;
        }

    }

    //--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrezcan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("ingresoSolicitudP");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método encargado de obtener el controlado de calidad de productos
     *
     * @author javier.aponte
     */
    private UsuarioDTO obtenerControladorCalidadProductos() {

        List<UsuarioDTO> controladoresCalidadProductos = new ArrayList<UsuarioDTO>();

        if (this.usuario.getDescripcionUOC() != null) {
            controladoresCalidadProductos = this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    usuario.getDescripcionUOC(),
                    ERol.CONTROL_CALIDAD_PRODUCTOS);
        }

        //si no existe Control calidad productos en la UOC se buscan en la territorial.
        if (controladoresCalidadProductos.isEmpty()) {
            controladoresCalidadProductos = this.getTramiteService().
                buscarFuncionariosPorRolYTerritorial(
                    usuario.getDescripcionTerritorial(),
                    ERol.CONTROL_CALIDAD_PRODUCTOS);
        }

        if (controladoresCalidadProductos != null && !controladoresCalidadProductos.isEmpty()) {
            return controladoresCalidadProductos.get(0);
        }

        return null;

    }

    /**
     * Metodo encargado de reclamar la actividad por parte del usuario autenticado
     *
     * @author felipe.cadena
     */
    private boolean reclamarActividad(Solicitud solicitudReclamar) {
        Actividad actividad = this.tareasPendientesMB.getInstanciaSeleccionada();

        List<Actividad> actividades;

        if (actividad == null) {
            actividades = this.tareasPendientesMB.getListaInstanciasActividadesSeleccionadas();
            if (actividades != null && !actividades.isEmpty()) {
                for (Actividad actTemp : actividades) {
                    if (actTemp.getIdObjetoNegocio() == solicitudReclamar.getId()) {
                        actividad = actTemp;
                        break;
                    }
                }
            }
        }

        if (actividad != null) {
            if (actividad.isEstaReclamada()) {
                if (this.usuario.getLogin().equals(actividad.getUsuarioEjecutor())) {
                    return true;
                } else {
                    this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                        solicitudReclamar.getNumero()));
                    return false;
                }
            }
            try {
                this.getProcesosService().reclamarActividad(actividad.getId(), this.usuario);
                return true;
            } catch (ExcepcionSNC e) {
                LOGGER.info(e.getMensaje(), e);
                this.addMensajeError(ECodigoErrorVista.ACT_RECLAMADA.getMensajeUsuario(
                    solicitudReclamar.getNumero()));
                return false;
            }
        } else {
            LOGGER.error(ECodigoErrorVista.RECLAMAR_ACTIVIDAD_001.getMensajeTecnico());
            return false;
        }
    }

//end of class
}
