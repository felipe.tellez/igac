/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.dao.util.ENumeraciones;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredioPropiedad;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.ModeloResolucion;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramitePredioEnglobe;

import co.gov.igac.snc.persistence.entity.tramite.TramiteTextoResolucion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.util.constantes.ConstantesResolucionesTramites;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.web.util.procesos.ProcesosUtilidadesWeb;

/**
 * Managed bean para el caso de Generar resoluciones IPER
 *
 * @author fredy.wilches
 */
@Component("generarResolucionIPER")
@Scope("session")
public class GenerarResolucionIPERMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = -2540852432560354900L;
    private static final SimpleDateFormat formatoFechaTituloResolucion = new SimpleDateFormat(
        Constantes.FORMATO_FECHA_TITULO_RESOLUCION);
    private static final Logger LOGGER = LoggerFactory.getLogger(AplicacionCambiosMB.class);

    private String mbComponentName = "generarResolucionIPER";

    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    @Autowired
    private MenuMB menuMB;

    /**
     * contexto usado para las cosas de documentos
     */
    @Autowired
    private IContextListener contextoDocs;

    /**
     * trámites sobre los que se está trabajando
     */
    private ArrayList<Tramite> currentTramites;

    /**
     * trámite seleccionado de la tabla
     */
    private Tramite selectedTramite;

    /**
     * trámite seleccionado en el link de número de resolución
     */
    private Tramite selectedTramiteResolucion;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    /**
     * URL del archivo con el documento de resolución
     */
    private String urlArchivoDocResolucion;

    /**
     * Archivo del reporte de resolución
     */
    private File archivo;

    /**
     * contenido del archivo con el documento de resolución. Es el que se pone como valor del
     * componente primefaces de descarga de archivos
     */
    private StreamedContent contenidoDocResolucion;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * lista de usuarios con el rol de coordinadores
     */
    private List<UsuarioDTO> coordinadores;

    //-----   BPM  --------
    private Actividad selectedTramiteActivity;

    /**
     * ids de los trámites que me devuelve el bpm
     */
    private long[] currentTramitesIds;

    private ReporteDTO reporteResolucion;

    //-------------     methods ----
    public Tramite getSelectedTramite() {
        return this.selectedTramite;
    }

    public void setSelectedTramite(Tramite selectedTramite) {
        this.selectedTramite = selectedTramite;
        List<TramiteDocumento> documentosPruebas = this.getTramiteService()
            .obtenerTramiteDocumentosDePruebasPorTramiteId(
                this.selectedTramite.getId());
        if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
            this.selectedTramite.setDocumentosPruebas(documentosPruebas);
        }
    }

    public Tramite getSelectedTramiteResolucion() {
        return selectedTramiteResolucion;
    }

    public void setSelectedTramiteResolucion(Tramite selectedTramiteResolucion) {
        this.selectedTramiteResolucion = selectedTramiteResolucion;
    }

    public ArrayList<Tramite> getCurrentTramites() {
        return this.currentTramites;
    }

    public void setCurrentTramites(ArrayList<Tramite> currentTramites) {
        this.currentTramites = currentTramites;
    }

    public ReporteDTO getReporteResolucion() {
        return reporteResolucion;
    }

    public void setReporteResolucion(ReporteDTO reporteResolucion) {
        this.reporteResolucion = reporteResolucion;
    }

    public StreamedContent getContenidoDocResolucion() {

        InputStream stream;

        try {
            stream = new FileInputStream(this.archivo);
            this.contenidoDocResolucion = new DefaultStreamedContent(stream,
                Constantes.TIPO_MIME_PDF, "Resolucion.pdf");
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado, no se puede imprimir el reporte " + e.getMessage());
        }

        return this.contenidoDocResolucion;
    }

    public void setContenidoDocResolucion(StreamedContent contenidoDocResolucion) {
        this.contenidoDocResolucion = contenidoDocResolucion;
    }

    public String getUrlArchivoDocResolucion() {
        return this.urlArchivoDocResolucion;
    }

    public void setUrlArchivoDocResolucion(String urlArchivoDocResolucion) {
        this.urlArchivoDocResolucion = urlArchivoDocResolucion;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on GenerarResolucionIPER#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "generarResolucionIPER");

        //D: se obtienen los ids de los trámites de trabajo        
        this.currentTramitesIds =
            this.tareasPendientesMB.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        List<Long> idsTramite = new ArrayList<Long>();
        for (Long l : currentTramitesIds) {
            idsTramite.add(l);
        }

        //D: se arma la lista de trámites
        this.currentTramites = (ArrayList<Tramite>) this.getTramiteService().
            obtenerTramitesPorIds(idsTramite);

        if (this.currentTramites != null && !this.currentTramites.isEmpty()) {
            for (Tramite tram : this.currentTramites) {
                List<TramiteDocumento> documentosPruebas = this.getTramiteService()
                    .obtenerTramiteDocumentosDePruebasPorTramiteId(
                        tram.getId());
                if (documentosPruebas != null && !documentosPruebas.isEmpty()) {
                    tram.setDocumentosPruebas(documentosPruebas);
                }

                /* if (tram.getResultadoDocumento()==null){ ReporteDTO
                 * rep=this.generarReporteResolucion(tram); Documento documento = new Documento();
                 * if(rep!=null){ documento.setArchivo(rep.getRutaCargarReporteAAlfresco());
                 * tram.setResultadoDocumento(documento); }
                 *
                 * } */
            }
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del link con el número del documento resultado para abrir la ventana donde se muestra
     */
    public void consultarDocResolucion() {

        this.reporteResolucion = this.reportsService.consultarReporteDeGestorDocumental(
            this.selectedTramiteResolucion.getResultadoDocumento().getIdRepositorioDocumentos());

    }

    public void avanzarProcesos() {
        int cuantos = 0;
        List<Tramite> avanzados = new ArrayList<Tramite>();
        for (Tramite t : this.currentTramites) {
            if (t.isSeleccionAprobar() && t.getResultadoDocumento() != null) {
                cuantos++;
                this.selectedTramite = t;
                selectedTramiteActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
                    this.selectedTramite.getId());
                this.setupProcessMessage();
                this.forwardProcess();
                avanzados.add(t);
            }
        }
        for (Tramite t : avanzados) {
            this.currentTramites.remove(this.currentTramites.indexOf(t));
        }
        if (cuantos == 0) {
            this.addMensajeError("Debe seleccionar al menos un trámite con Resolución");
        } else {
            this.addMensajeInfo("Proceso finalizado");
        }
    }
//--------------------------------------------------------------------------------------------------

    //----------- bpm
    /**
     * no se debe validar nada en cuanto al proceso
     *
     * @return
     */
    @Implement
    public boolean validateProcess() {
        return true;
    }
//--------------------------------------------------------------------------------------------------

    @Implement
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";
        UsuarioDTO usuarioDestinoActividad = new UsuarioDTO();
        List<UsuarioDTO> usuariosRol;

        /*
         * Como son mutaciones inicialmente de primera no requieren notificación
         */
        boolean requiereNotificacion = false;

        //Si requiere notificacion va a comunicar notificación resolución
        if (requiereNotificacion) {

            usuariosRol = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.currentUser.getDescripcionEstructuraOrganizacional(),
                ERol.SECRETARIA_CONSERVACION);

            processTransition =
                ProcesoDeConservacion.ACT_VALIDACION_COMUNICAR_NOTIFICACION_RESOLUCION;
            observaciones = "";

        } else {
            //Si no  requiere notificación va a radicar recurso

            usuariosRol = this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                this.currentUser.getDescripcionTerritorial(),
                ERol.FUNCIONARIO_RADICADOR);

            processTransition = ProcesoDeConservacion.ACT_VALIDACION_RADICAR_RECURSO;
            observaciones = "";

        }
        if (usuariosRol != null && !usuariosRol.isEmpty()) {

            usuarioDestinoActividad = usuariosRol.get(0);

        }

        messageDTO =
            ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                selectedTramiteActivity.getId(), processTransition, usuarioDestinoActividad,
                observaciones);

        messageDTO.setUsuarioActual(this.currentUser);
        this.setMessage(messageDTO);

    }
//--------------------------------------------------------------------------------------------------

    @Implement
    @Override
    public void doDatabaseStatesUpdate() {
        // Una vez se ejecuta el procedimiento que aplica los cambios en el
        // trámite, éste por debajo realiza todos las modificaciones en la base
        // de datos, por lo que all legar a este método ya se han realizado los
        // cambios respectivos, y por ello está vacio.
    }
//--------------------------------------------------------------------------------------------------

    /**
     * avanza el proceso para el trámite seleccionado de la tabla
     */
    private boolean avanzarProceso() {

        boolean answer = false;

        if (this.validateProcess()) {
            this.setupProcessMessage();

            try {
                this.doDatabaseStatesUpdate();
                this.forwardProcess();

                answer = true;
            } catch (Exception e) {
                LOGGER.error("error al mover proceso en AplicacionCambiosMB: " + e.getMessage(), e);
                this.addMensajeError("Ocurrió un error en procesos. Comuníquese con el " +
                    " administrador del sistema");
                throw SNCWebServiceExceptions.EXCEPCION_0008.getExcepcion(
                    LOGGER, e, this.selectedTramite.getId(), this.selectedTramite.
                    getNumeroRadicacion(),
                    this.selectedTramiteActivity);
            }
        }
        return answer;

    }

//--------------------------------------------------------------------------------------------------
    /**
     * action del botón "cerrar" Se debe forzar el init del MB porque de otro modo no se refrescan
     * los datos
     */
    public String cerrarPaginaPrincipal() {

        this.tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean(this.mbComponentName);
        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método que actualiza el ppredio
     *
     * @author javier.aponte
     */
    public void actualizarPPredio(Documento docResolucion) {

        Calendar calendario = Calendar.getInstance();
        try {

            if (this.selectedTramite.isSegundaEnglobe()) {

                List<PPredio> pPrediosEnglobe;
                List<Long> predioIds = new ArrayList<Long>();
                for (TramitePredioEnglobe tpe : this.selectedTramite
                    .getTramitePredioEnglobes()) {
                    predioIds.add(tpe.getPredio().getId());
                }
                pPrediosEnglobe = this.getConservacionService()
                    .obtenerPPrediosCompletosPorIds(predioIds);
                if (pPrediosEnglobe != null) {
                    for (PPredio ppredio : pPrediosEnglobe) {
                        ppredio.setAnioUltimaResolucion(Integer.valueOf(calendario.
                            get(Calendar.YEAR)).shortValue());
                        ppredio.setNumeroUltimaResolucion(docResolucion.getNumeroDocumento());
                    }
                    this.getConservacionService().guardarPPredios(pPrediosEnglobe);
                }

            } else if (this.selectedTramite.isSegundaDesenglobe()) {
                List<PPredio> pPrediosDesenglobe;
                pPrediosDesenglobe = this.getConservacionService()
                    .buscarPPrediosCompletosPorTramiteIdPHCondominio(
                        this.selectedTramite.getId());
                if (pPrediosDesenglobe != null) {
                    for (PPredio ppredio : pPrediosDesenglobe) {
                        ppredio.setAnioUltimaResolucion(Integer.valueOf(calendario.
                            get(Calendar.YEAR)).shortValue());
                        ppredio.setNumeroUltimaResolucion(docResolucion.getNumeroDocumento());
                    }
                    this.getConservacionService().guardarPPredios(pPrediosDesenglobe);
                }
            } else {
                PPredio predioSeleccionado;
                predioSeleccionado = this.getConservacionService()
                    .obtenerPPredioCompletoByIdTramite(this.selectedTramite.getId());
                if (predioSeleccionado != null) {
                    predioSeleccionado.setAnioUltimaResolucion(Integer.valueOf(calendario.get(
                        Calendar.YEAR)).shortValue());
                    predioSeleccionado.setNumeroUltimaResolucion(docResolucion.getNumeroDocumento());
                    this.getConservacionService().guardarActualizarPPredio(predioSeleccionado);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error al actualizar el predio", e);
        }
    }

    public void generarResoluciones() {
        for (Tramite t : this.currentTramites) {
            if (t.isSeleccionAprobar()) {
                this.generarResolucion(t);
            }
        }
    }

    /**
     * @modified by javier.aponte #7249 se genera la resolución sin marca de agua 11/03/2014
     *
     */
    public ReporteDTO generarReporteResolucion(Tramite currentTramite) {

        EReporteServiceSNC enumeracionReporte = this.getTramiteService().
            obtenerUrlReporteResoluciones(currentTramite.getId());

        if (enumeracionReporte != null) {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID", "" + currentTramite.getId());

            parameters.put("COPIA_USO_RESTRINGIDO", "false");

            return reportsService.generarReporte(
                parameters, enumeracionReporte, this.currentUser);
        } else {
            LOGGER.error("No se pudo obtener el url " +
                 "en el servidor de reportes asociada a la resolución ");
        }
        return null;
    }

    public void reemplazarParametrosTextoConsiderandoResuelve(Tramite tramite,
        ModeloResolucion modeloResolucion, PPredio predioSeleccionado, UsuarioDTO usuario) {

        String tipoTramite = tramite.getTipoTramite();

        Locale colombiaLocale = new Locale("ES", "es_CO");

        String estructuraOrganizacional = null;
        String responsableConservacion = null;

        Object parametrosTextoConsiderando[] = null;
        Object parametrosTextoResuelve[] = null;

        if (usuario.isTerritorial()) {
            estructuraOrganizacional = ConstantesResolucionesTramites.DIRECCION_TERRITORIAL +
                 usuario.getDescripcionEstructuraOrganizacional()
                    .toUpperCase();
            responsableConservacion =
                ConstantesResolucionesTramites.RESPONSABLE_DIRECCION_TERRITORIAL;
        }
        if (usuario.isUoc()) {
            estructuraOrganizacional = ConstantesResolucionesTramites.UNIDAD_OPERATIVA +
                 usuario.getDescripcionEstructuraOrganizacional()
                    .replaceAll("UOC_", "").toUpperCase();
            responsableConservacion = ConstantesResolucionesTramites.RESPONSABLE_UNIDAD_OPERATIVA;
        }

        StringBuilder solicitantes = new StringBuilder();
        StringBuilder identificaciones = new StringBuilder();
        StringBuilder condiciones = new StringBuilder();
        StringBuilder radicar = new StringBuilder();

        for (SolicitanteSolicitud solicitanteSolicitud : tramite
            .getSolicitud().getSolicitanteSolicituds()) {
            solicitantes
                .append(solicitanteSolicitud
                    .getNombreCompleto()
                    .substring(
                        0,
                        solicitanteSolicitud.getNombreCompleto()
                            .length() - 1).toUpperCase() +
                     ", ");
            identificaciones.append(solicitanteSolicitud
                .getTipoIdentificacion() +
                 ". No. " +
                 solicitanteSolicitud.getNumeroIdentificacion() + ", ");
            condiciones.append(solicitanteSolicitud.getRelacion().toUpperCase() +
                 ", ");
        }

        StringBuilder senorIdentificacion = new StringBuilder();
        if (tramite.getSolicitud().getSolicitanteSolicituds().size() > 1) {
            senorIdentificacion.append(ConstantesResolucionesTramites.SENIORES +
                 solicitantes +
                 ConstantesResolucionesTramites.IDENTIFICADOS +
                 identificaciones +
                 ConstantesResolucionesTramites.CONDICIONES + condiciones +
                 ConstantesResolucionesTramites.RESPECTIVAMENTE);
            radicar.append(ConstantesResolucionesTramites.RADICARON);
        }
        if (tramite.getSolicitud().getSolicitanteSolicituds().size() == 1) {
            senorIdentificacion.append(ConstantesResolucionesTramites.SENIOR +
                 solicitantes +
                 ConstantesResolucionesTramites.IDENTIFICADO +
                 identificaciones +
                 ConstantesResolucionesTramites.CONDICION + condiciones);
            radicar.append(ConstantesResolucionesTramites.RADICO);
        }

        radicar.append(" " + ConstantesResolucionesTramites.NUMERO_RADICADO +
             tramite.getNumeroRadicacion() + ", ");

        List<Dominio> tiposSolicitud = this.getGeneralesService().getCacheDominioPorNombre(
            EDominio.SOLICITUD_TIPO);
        String tipoSolicitudValor = null;
        for (Dominio tmp : tiposSolicitud) {
            if (tmp.getCodigo().equals(tramite.getSolicitud().getTipo())) {
                tipoSolicitudValor = tmp.getValor().toUpperCase();
                break;
            }
        }
        String documentosJustificativos = null;

        documentosJustificativos = this.formarDocumentosJustificativos(predioSeleccionado);

        if (tipoTramite.equals(ETramiteTipoTramite.MUTACION.getCodigo()) ||
             tipoTramite.equals(ETramiteTipoTramite.REVISION_DE_AVALUO.getCodigo())) {

            parametrosTextoConsiderando = new Object[8];
            parametrosTextoResuelve = new Object[5];

            parametrosTextoConsiderando[0] = estructuraOrganizacional;
            parametrosTextoConsiderando[1] = responsableConservacion;
            parametrosTextoConsiderando[2] = senorIdentificacion;
            parametrosTextoConsiderando[3] = (tramite.getPredio() == null) ? "" :
                 tramite.getPredio().getNumeroPredial();
            parametrosTextoConsiderando[4] = (tramite.getMunicipio() == null) ? "" :
                 tramite.getMunicipio().getNombre().toUpperCase();
            parametrosTextoConsiderando[5] = radicar;
            parametrosTextoConsiderando[6] = tipoSolicitudValor;
            parametrosTextoConsiderando[7] = documentosJustificativos;

            parametrosTextoResuelve[0] = responsableConservacion;
            parametrosTextoResuelve[1] = 2;
            parametrosTextoResuelve[2] = 3;
            parametrosTextoResuelve[3] = 4;
            parametrosTextoResuelve[4] = 5;

        }
        if (parametrosTextoConsiderando != null) {
            modeloResolucion.setTextoConsiderando(new MessageFormat(
                modeloResolucion.getTextoConsiderando(), colombiaLocale)
                .format(parametrosTextoConsiderando));
        }
        if (parametrosTextoResuelve != null) {
            modeloResolucion.setTextoResuelve(new MessageFormat(modeloResolucion
                .getTextoResuelve(), colombiaLocale)
                .format(parametrosTextoResuelve));
        }

    }

    private String formarDocumentosJustificativos(PPredio predioSeleccionado) {

        String answer = "";
        /*
         * if(this.pPersonaPredioPropiedad!= null && !this.pPersonaPredioPropiedad.isEmpty()){
         * this.pPersonaPredioPropiedad.clear(); } if(this.pPersonaPredioPropiedad == null){
         * this.pPersonaPredioPropiedad = new ArrayList<PPersonaPredioPropiedad>(); }
         */
        List<PPersonaPredioPropiedad> pPersonaPredioPropiedad =
            new ArrayList<PPersonaPredioPropiedad>();
        boolean agregarDocumentoJustificativo;

        StringBuilder documentosJustificativos = new StringBuilder();

        if (predioSeleccionado != null &&
             predioSeleccionado.getPPersonaPredios() != null) {
            for (PPersonaPredio persPred : predioSeleccionado
                .getPPersonaPredios()) {
                for (PPersonaPredioPropiedad ppp : persPred
                    .getPPersonaPredioPropiedads()) {
                    if (ppp.getCancelaInscribe() != null &&
                         (ppp.getCancelaInscribe().equals(
                            EProyeccionCancelaInscribe.INSCRIBE
                                .getCodigo()) || ppp
                            .getCancelaInscribe().equals(
                                EProyeccionCancelaInscribe.MODIFICA
                                    .getCodigo()))) {

                        agregarDocumentoJustificativo = true;
                        for (PPersonaPredioPropiedad pppTemp : pPersonaPredioPropiedad) {
                            if (pppTemp.getId() != ppp.getId() &&
                                pppTemp.getNumeroTitulo() != null && pppTemp.getFechaTitulo() !=
                                null &&
                                 pppTemp.getNumeroTitulo().equals(ppp.getNumeroTitulo()) &&
                                pppTemp.getFechaTitulo().equals(ppp.getFechaTitulo())) {
                                agregarDocumentoJustificativo = false;
                            }
                        }

                        if (agregarDocumentoJustificativo && ppp.getTipoTitulo() != null && ppp.
                            getNumeroTitulo() != null) {
                            documentosJustificativos.append(((ppp.getTipoTitulo() != null) ? ppp.
                                getTipoTitulo().toUpperCase() : "") +
                                 " NO. " +
                                 ((ppp.getNumeroTitulo() != null) ? ppp.getNumeroTitulo() : "") +
                                 " DE FECHA " +
                                 ((ppp.getFechaTitulo() != null) ? formatoFechaTituloResolucion.
                                format(ppp.getFechaTitulo()).toUpperCase() : "") +
                                 ", " +
                                 ConstantesResolucionesTramites.NOTARIA +
                                 " " +
                                 (ppp.getEntidadEmisora() == null ? "" : ppp
                                .getEntidadEmisora().toUpperCase()) +
                                 " DE " +
                                 (ppp.getMunicipio() == null ? "" : ppp.getMunicipio()
                                .getNombre().toUpperCase()) +
                                 ", " +
                                 (ppp.getDepartamento() == null ? "" : ppp
                                .getDepartamento().getNombre().toUpperCase()) + ", " +
                                 ConstantesResolucionesTramites.MATRICULA_INMOBILIARIA +
                                 ", " +
                                 (predioSeleccionado.getNumeroRegistro() != null ?
                                predioSeleccionado.getNumeroRegistro() : "") +
                                 ",<br>"
                            );
                            pPersonaPredioPropiedad.add(ppp);
                        }
                    }
                }
            }
        }

        if (documentosJustificativos.length() > 5) {
            answer = documentosJustificativos.delete(documentosJustificativos.length() - 5,
                documentosJustificativos.length()).toString();
        }

        return answer;
    }

    /**
     * @modified by javier.aponte #7249 se genera la resolución sin marca de agua 11/03/2014
     */
    public void guardarTramiteTextoResolucion(Tramite tramite,
        ModeloResolucion modeloResolucionSeleccionado, TramiteTextoResolucion tramiteTextoResolucion,
        UsuarioDTO usuario) {

        String txtConsiderando, txtResuelve;
        LOGGER.debug("Entra a Guardar Tramite Texto Resolucion");

        List<UsuarioDTO> responsablesConservacion = (List<UsuarioDTO>) this
            .getTramiteService().buscarFuncionariosPorRolYTerritorial(usuario
                .getDescripcionEstructuraOrganizacional(), ERol.RESPONSABLE_CONSERVACION);

        // Campos de USUARIO
        tramiteTextoResolucion.setFechaLog(new Date(System
            .currentTimeMillis()));
        tramiteTextoResolucion.setUsuarioLog(usuario.getLogin());

        tramiteTextoResolucion.setTitulo(modeloResolucionSeleccionado.getTextoTitulo());

        // Se debe hacer un replace de los estilos debido a que el editor los guarda de una manera y
        // al generar el reporte, jasper los traduce de otra, por tal motivo hay que reemplazarlos manualmente.
        txtConsiderando = reemplazarEstilosDelEditor(modeloResolucionSeleccionado
            .getTextoConsiderando());
        txtResuelve = reemplazarEstilosDelEditor(modeloResolucionSeleccionado
            .getTextoResuelve());

        modeloResolucionSeleccionado.setTextoConsiderando(txtConsiderando);
        modeloResolucionSeleccionado.setTextoResuelve(txtResuelve);
        tramiteTextoResolucion.setConsiderando(txtConsiderando);
        tramiteTextoResolucion.setResuelve(txtResuelve);

        tramiteTextoResolucion.setTramite(tramite);
        tramiteTextoResolucion.setModeloResolucion(modeloResolucionSeleccionado);

        try {
            // Guardar Texto Resolución
            tramiteTextoResolucion = this.getTramiteService()
                .guardarTramiteTextoResolucion(
                    tramiteTextoResolucion);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            this.addMensajeError(
                "Hubo un error al guardar el texto motivo, por favor intente nuevamente");
            return;
        }

        // ------ REPORTE ------//
        EReporteServiceSNC enumeracionReporte = this.getTramiteService()
            .obtenerUrlReporteResoluciones(tramite.getId());

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("TRAMITE_ID", "" + tramite.getId());
        parameters.put("COPIA_USO_RESTRINGIDO", "false");

        UsuarioDTO responsableConservacion = null;

        if (responsablesConservacion == null) {
            responsablesConservacion = (List<UsuarioDTO>) this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    usuario
                        .getDescripcionEstructuraOrganizacional(),
                    ERol.RESPONSABLE_CONSERVACION);
        }

        if (responsablesConservacion != null &&
             !responsablesConservacion.isEmpty() &&
             responsablesConservacion.get(0) != null) {
            responsableConservacion = responsablesConservacion
                .get(0);
        }

        //Siempre debe existir un responsable de conservación porque es quien firma el documento
        //en caso que haya no se debe generar el reporte
        ReporteDTO reporte = null;
        if (responsableConservacion != null &&
             responsableConservacion.getNombreCompleto() != null) {
            parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                responsableConservacion.getNombreCompleto());

            if (tramite.isTipoTramiteViaGubernativaModificado() || tramite.
                isTipoTramiteViaGubModSubsidioApelacion() ||
                 tramite.isRecursoApelacion() || tramite.isViaGubernativa()) {

                if (!tramite.getNumeroRadicacionReferencia().isEmpty()) {

                    Tramite tramiteReferencia = this.getTramiteService().
                        buscarTramitePorNumeroRadicacion(tramite.getNumeroRadicacionReferencia());

                    if (tramiteReferencia != null) {
                        parameters.put("NOMBRE_SUBREPORTE_RESUELVE", this.getTramiteService().
                            obtenerUrlSubreporteResolucionesResuelve(tramiteReferencia.getId()));
                    }
                }

            }

            reporte = reportsService.generarReporte(parameters,
                enumeracionReporte, usuario);
        }

        if (reporte != null) {
            // Mensaje satisfactorio
            this.addMensajeInfo("Se almacenó el texto motivo satisfactoriamente.");
        }

    }

    public String reemplazarEstilosDelEditor(String txt) {
        if (txt != null && !txt.trim().isEmpty()) {
            txt = txt.replaceAll("<strong>", "<b>");
            txt = txt.replaceAll("</strong>", "</b>");
            txt = txt.replaceAll("<em>", "<i>");
            txt = txt.replaceAll("</em>", "</i>");
        }
        return txt;
    }

    public void generarResolucion(Tramite currentTramite) {
        try {

            Object[] resultado = this.getGeneralesService().generarNumeracion(
                ENumeraciones.NUMERACION_RESOLUCION, "",
                currentTramite.getDepartamento().getCodigo(),
                currentTramite.getMunicipio().getCodigo(), 0);
            String numeroResolucion = resultado[1].toString();
            LOGGER.debug("Número de Resolución:" + numeroResolucion);
            Date fechaResolucion = new Date(System.currentTimeMillis());

            if (numeroResolucion != null) {

                // Genera el reporte con el número de resolución
                ReporteDTO reporte = this.generarReporteResolucion(currentTramite, numeroResolucion,
                    fechaResolucion);

                // Guarda el documento en Alfresco y en la base de datos y lo
                // asocia con un trámite documento
                currentTramite.setResultadoDocumento(this.guardarDocumentoResolucion(currentTramite,
                    numeroResolucion, fechaResolucion, reporte));

                // Avanza el proceso
                /* if (!this.forwardThisProcess()) { LOGGER.error("No se puede avanzar el proceso por
                 * errores de validación "); this.addMensajeError("Falla al avanzar el proceso por
                 * errores de validación"); } //this.resolucionGenerada = true; */
                this.
                    addMensajeInfo("Se generó la resolución " + numeroResolucion + " correctamente");
            } else {
                this.addMensajeError("Ocurrió un error al generar la resolución");
                LOGGER.debug("El número de Resolución no se generó bien, devolvió null");
                return;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("No fue posible generar la resolución.");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }
    }

    public ReporteDTO generarReporteResolucion(Tramite currentTramite, String numeroResolucion,
        Date fechaResolucion) {

        EReporteServiceSNC enumeracionReporte = this.getTramiteService().
            obtenerUrlReporteResoluciones(currentTramite.getId());

        if (enumeracionReporte != null) {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID", "" + currentTramite.getId());

            if (numeroResolucion != null) {
                parameters.put("COPIA_USO_RESTRINGIDO", "false");

                SolicitudCatastral objNegocio;

                UsuarioDTO coordinador = null;

                if (this.coordinadores == null) {
                    objNegocio = this.getProcesosService()
                        .obtenerObjetoNegocioConservacion(currentTramite.getProcesoInstanciaId(),
                            ProcesoDeConservacion.ACT_VALIDACION_REVISAR_PROYECCION);

                    if (objNegocio != null) {
                        this.coordinadores = (List<UsuarioDTO>) objNegocio.getUsuarios();
                    }
                }

                if (this.coordinadores != null && !this.coordinadores.isEmpty() &&
                     this.coordinadores.get(0) != null && this.coordinadores.get(0).getLogin() !=
                    null) {
                    coordinador = this.getGeneralesService().getCacheUsuario(this.coordinadores.get(
                        0).getLogin());
                }

                if (coordinador != null && coordinador.getNombreCompleto() != null) {
                    parameters.put("NOMBRE_REVISOR", coordinador.getNombreCompleto());
                } else {
                    parameters.put("NOMBRE_REVISOR", null);
                }

                parameters.put("NOMBRE_RESPONSABLE_CONSERVACION", this.currentUser.
                    getNombreCompleto());

                parameters.put("NUMERO_RESOLUCION", numeroResolucion);
                parameters.put("FECHA_RESOLUCION", Constantes.FORMAT_FECHA_RESOLUCION.format(
                    fechaResolucion));
                parameters.put("FECHA_RESOLUCION_CON_FORMATO", Constantes.FORMAT_FECHA_RESOLUCION_2.
                    format(fechaResolucion));

                if (currentTramite.isTipoTramiteViaGubernativaModificado() || currentTramite.
                    isTipoTramiteViaGubModSubsidioApelacion() ||
                     currentTramite.isRecursoApelacion() || currentTramite.isViaGubernativa()) {

                    parameters.put("NOMBRE_SUBREPORTE_RESUELVE", this.getTramiteService().
                        obtenerUrlSubreporteResolucionesResuelve(currentTramite.getId()));
                }

            } else {
                parameters.put("COPIA_USO_RESTRINGIDO", "true");
            }
            return reportsService.generarReporte(
                parameters, enumeracionReporte, this.currentUser);
        } else {
            LOGGER.error("No se pudo obtener el url " +
                 "en el servidor de reportes asociada a la resolución ");
        }
        return null;

    }

    public Documento guardarDocumentoResolucion(Tramite currentTramite, String numeroResolucion,
        Date fechaResolucion, ReporteDTO reporte) throws Exception {

        Documento documento = new Documento();

        try {
            TipoDocumento tipoDocumento = new TipoDocumento();
            tipoDocumento.setId(ETipoDocumento.RESOLUCION_DE_CONSERVACION
                .getId());

            documento.setTipoDocumento(tipoDocumento);
            if (reporte != null) {
                documento.setArchivo(reporte.getRutaCargarReporteAAlfresco());
            }
            documento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documento.setTramiteId(currentTramite.getId());
            documento.setBloqueado(ESiNo.NO.getCodigo());
            documento.setUsuarioCreador(this.currentUser.getLogin());
            documento.setUsuarioLog(this.currentUser.getLogin());
            documento.setFechaLog(new Date(System.currentTimeMillis()));
            documento.setNumeroDocumento(numeroResolucion);
            documento.setFechaDocumento(fechaResolucion);
            documento.setEstructuraOrganizacionalCod(this.currentUser.
                getCodigoEstructuraOrganizacional());

            // Subo el archivo a Alfresco y actualizo el documento
            documento = this.getTramiteService().guardarDocumento(this.currentUser, documento);

            //Le asocia la resolución al trámite y actualiza el trámite
            if (documento != null) {
                currentTramite.setResultadoDocumento(documento);
                currentTramite = this.getTramiteService().guardarActualizarTramite(currentTramite);
                if (currentTramite.getResultadoDocumento() == null || currentTramite.
                    getResultadoDocumento().getId() == null) {
                    this.addMensajeError("Error al guardar el documento de resolución.");
                    LOGGER.debug("Error al asociar el documento de la resolución en el trámite");
                    throw new Exception("Error al guardar el documento de resolución");
                }
            } else {
                this.addMensajeError("Error al guardar el documento de resolución.");
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                throw new Exception("Error al guardar el documento de resolución.");
            }

        } catch (ExcepcionSNC e) {
            LOGGER.error(e.getMensaje(), e);
            LOGGER.debug("Error al guardar el documento de la resolución en la base de datos");
            this.addMensajeError("Error al guardar la resolución.");
            // Lanzar la excepción al método padre (generarResolucion).
            throw (e);
        }
        return documento;
    }

    private boolean forwardThisProcess() {
        if (this.validateProcess()) {
            this.setupProcessMessage();
            //this.doDatabaseStatesUpdate();
            this.forwardProcess();
            return true;
        } else {
            return false;
        }
    }
// end of class
}
