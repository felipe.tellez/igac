/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.actualizacion;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.generales.Plantilla;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @description Managed bean para gestionar la visualización y el envio de un correo electrónico en
 * el proceso de actualización.
 *
 * @version 1.0
 *
 * @author david.cifuentes
 */
@Component("visualizacionCorreoActualizacion")
@Scope("session")
public class VisualizacionCorreoActualizacionMB extends SNCManagedBean
    implements Serializable {

    /** Serial */
    private static final long serialVersionUID = -5172998811646733085L;

    /** Logger */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(VisualizacionCorreoActualizacionMB.class);

    /** ---------------------------------- */
    /** ----------- SERVICIOS ------------ */
    /** ---------------------------------- */
    /** ---------------------------------- */
    /** ----------- VARIABLES ------------ */
    /** ---------------------------------- */
    /** Asunto del correo electrónico */
    private String asunto;

    /** Array de los correos electrónicos de los destinatarios */
    private String[] destinatarios;

    /** Array de los correos electrónicos de los destinatarios */
    private String remitente;

    /** Array con los documentos adjuntos */
    private String[] adjuntos;

    /** Código de la plantilla definda en la enumeración EPlantilla */
    private String codigoPlantillaContenido;

    /** Array con los parametros que se reemplazaran en la plantilla del asunto */
    private Object[] parametrosPlantillaAsunto;

    /** Array con los parametros que se reemplazaran en la plantilla del contenido */
    private Object[] parametrosPlantillaContenido;

    /** Variable que contiene el texto del contenido del correo */
    private String encabezadoCorreo;

    /** Array con los parametros que se reemplazaran en la plantilla del encabezado */
    private Object[] parametrosPlantillaEncabezado;

    /** Variable que contiene el texto del encabezado del correo */
    private String contenidoCorreo;

    /** Variable que almacena el nombre de los archivos adjuntos */
    private String[] titulosAdjuntos;

    /** Format de los mensajes */
    private MessageFormat messageFormat;

    /** Código de la plantilla del encabezado definida en la enumeración EPlantilla */
    private String codigoPlantillaEncabezado;

    /** ---------------------------------- */
    /** -------------- INIT -------------- */
    /** ---------------------------------- */
    @PostConstruct
    public void init() {
        this.codigoPlantillaEncabezado = EPlantilla.ENCABEZADO_CORREO_ELECTRONICO.getCodigo();
    }

    /** ---------------------------------- */
    /** ------- GETTERS Y SETTERS -------- */
    /** ---------------------------------- */
    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String[] getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(String[] destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public String[] getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(String[] adjuntos) {
        this.adjuntos = adjuntos;
    }

    public String[] getTitulosAdjuntos() {
        return titulosAdjuntos;
    }

    public void setTitulosAdjuntos(String[] titulosAdjuntos) {
        this.titulosAdjuntos = titulosAdjuntos;
    }

    public String getEncabezadoCorreo() {
        return encabezadoCorreo;
    }

    public void setEncabezadoCorreo(String encabezadoCorreo) {
        this.encabezadoCorreo = encabezadoCorreo;
    }

    public Object[] getParametrosPlantillaEncabezado() {
        return parametrosPlantillaEncabezado;
    }

    public void setParametrosPlantillaEncabezado(
        Object[] parametrosPlantillaEncabezado) {
        this.parametrosPlantillaEncabezado = parametrosPlantillaEncabezado;
    }

    public MessageFormat getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(MessageFormat messageFormat) {
        this.messageFormat = messageFormat;
    }

    public String getCodigoPlantillaEncabezado() {
        return codigoPlantillaEncabezado;
    }

    public void setCodigoPlantillaEncabezado(String codigoPlantillaEncabezado) {
        this.codigoPlantillaEncabezado = codigoPlantillaEncabezado;
    }

    public String getContenidoCorreo() {
        return contenidoCorreo;
    }

    public void setContenidoCorreo(String contenidoCorreo) {
        this.contenidoCorreo = contenidoCorreo;
    }

    public String getCodigoPlantillaContenido() {
        return codigoPlantillaContenido;
    }

    public void setCodigoPlantillaContenido(String codigoPlantillaContenido) {
        this.codigoPlantillaContenido = codigoPlantillaContenido;
    }

    public Object[] getParametrosPlantillaContenido() {
        return parametrosPlantillaContenido;
    }

    public void setParametrosPlantillaContenido(
        Object[] parametrosPlantillaContenido) {
        this.parametrosPlantillaContenido = parametrosPlantillaContenido;
    }

    /** -------------------------------- */
    /** ----------- MÉTODOS ------------ */
    /** -------------------------------- */
    /**
     * Método que debe ser llamado desde otros managed beans, con el propósito de inicializar las
     * variables para el envio de correo electrónico desde el presente managed bean.
     *
     * @author david.cifuentes
     *
     * @param asunto Asunto del correo electrónico.
     * @param destinatarios Array de los correos electrónicos de los destinatarios.
     * @param remitente Correo electrónico del usuario que envia el correo.
     * @param adjuntos Array con los documentos adjuntos, estos deben estar definidos de la
     * siguiente manera contextDirectoryPath/nameFile
     * @param nombrePlantillaContenido Código de la plantilla definda en la enumeración EPlantilla.
     * @param parametrosPlantillaContenido Array con los parametros que se reemplazaran en la
     * plantilla, estos deben estar en el orden que se reemplazaran en la plantilla.
     * @param titulos Array con los titulos de los documentos adjuntos, estos deben estar con el
     * formato nameFile.fileExtension
     */
    public void inicializarParametros(String asunto, String[] destinatarios,
        String remitente, String[] adjuntos,
        String codigoPlantillaContenido,
        String[] parametrosPlantillaAsunto,
        String[] parametrosPlantillaContenido,
        String[] titulosAdjuntos) {

        this.asunto = asunto;
        this.destinatarios = destinatarios;
        this.remitente = remitente;
        this.adjuntos = adjuntos;
        this.codigoPlantillaContenido = codigoPlantillaContenido;
        this.parametrosPlantillaAsunto = parametrosPlantillaAsunto;
        this.parametrosPlantillaContenido = parametrosPlantillaContenido;
        this.titulosAdjuntos = titulosAdjuntos;

        if (validarParamentros()) {
            // Inicializar el encabezado del correo.
            this.inicializarEncabezado();
            // Inicializar el contenido del correo.
            this.inicializarContenido();
        } else {
            LOGGER.warn("ERROR EN LA INICIALIZACIÓN DE PARÁMETROS - FALTAN PARÁMETROS OBLIGATORIOS");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.cerrarVisualizacionCorreo();
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que valida los parametros obligatorios en el envio del correo electrónico.
     *
     * @author david.cifuentes
     * @return
     */
    public boolean validarParamentros() {
        boolean valido = true;
        // Validar los parametros obligatorios.
        if (this.asunto == null ||
             this.asunto.trim().isEmpty() ||
             this.destinatarios == null ||
             this.destinatarios.length < 1 ||
             this.remitente == null ||
             this.remitente.trim().isEmpty() ||
             this.codigoPlantillaContenido == null ||
             this.codigoPlantillaContenido.trim().isEmpty()) {
            valido = false;
        }
        return valido;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que inicializa la visualización del enabezado del correo electrónico.
     *
     * @author david.cifuentes
     */
    public void inicializarEncabezado() {
        // Consulta de la plantilla del encabezado
        Plantilla plantillaEncabezado = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(this.codigoPlantillaEncabezado);

        if (plantillaEncabezado != null) {

            // Consulta del encabezado del correo.
            this.encabezadoCorreo = plantillaEncabezado.getHtml();

            if (encabezadoCorreo != null) {

                // Reemplazar los valores en el encabezado.
                reemplazarValoresEncabezado();

                // Se instancia el messageFormat.
                Locale colombia = new Locale("ES", "es_CO");
                this.messageFormat = new MessageFormat(this.encabezadoCorreo, colombia);

                // Se reemplazan los parametros en el contenido de la plantilla.
                this.encabezadoCorreo = this.messageFormat
                    .format(this.parametrosPlantillaEncabezado);

            } else {
                LOGGER.warn("ERROR - No se pudo recuperar la plantilla: " +
                     this.codigoPlantillaContenido);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.cerrarVisualizacionCorreo();

            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que reemplaza los parametros del encabezado.
     *
     * @author david.cifuentes
     */
    public void reemplazarValoresEncabezado() {

        Calendar hoy = Calendar.getInstance();
        Date date = new java.util.Date(hoy.getTimeInMillis());
        SimpleDateFormat formatDate = new SimpleDateFormat("EEEEEEEEE dd 'de' MMMMM 'de' yyyy");
        String fecha = formatDate.format(date);

        this.parametrosPlantillaEncabezado = new Object[4];
        // Cargue visual de los destinatarios
        String destinatariosVar = "";
        for (String d : this.destinatarios) {
            destinatariosVar = destinatariosVar + d + "; ";
        }

        // Remitente
        this.parametrosPlantillaEncabezado[0] = this.remitente;
        // Destinatarios
        this.parametrosPlantillaEncabezado[1] = destinatariosVar;
        // Fecha
        this.parametrosPlantillaEncabezado[2] = fecha;

        // Asunto
        // Se instancia el messageFormat.
        Locale colombia = new Locale("ES", "es_CO");
        this.messageFormat = new MessageFormat(this.asunto, colombia);

        // Se reemplazan los parametros en el contenido del asunto
        this.asunto = this.messageFormat
            .format(this.parametrosPlantillaAsunto);

        this.parametrosPlantillaEncabezado[3] = this.asunto;
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que inicializa la visualización del contenido del correo electrónico.
     *
     * @author david.cifuentes
     */
    public void inicializarContenido() {

        // Consulta de la plantilla del contenido.
        Plantilla plantillaContenido = this.getGeneralesService()
            .recuperarPlantillaPorCodigo(this.codigoPlantillaContenido);

        if (plantillaContenido != null) {

            // Consulta del contenido del correo.
            this.contenidoCorreo = plantillaContenido.getHtml();

            if (contenidoCorreo != null) {

                // Se instancia el messageFormat.
                Locale colombia = new Locale("ES", "es_CO");
                this.messageFormat = new MessageFormat(this.contenidoCorreo, colombia);

                // Se reemplazan los parametros en el contenido de la plantilla.
                this.contenidoCorreo = this.messageFormat
                    .format(this.parametrosPlantillaContenido);

            } else {
                LOGGER.warn("ERROR - No se pudo recuperar la plantilla: " +
                     this.codigoPlantillaContenido);
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.cerrarVisualizacionCorreo();

            }
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método que realiza el envio del correo electrónico, con el contenido y los valores de
     * destinatarios y remitentes instanciados.
     *
     * @author david.cifuentes
     * @modified by javier.aponte se establece error en false si envia satisfactoriamente el correo
     */
    public void enviarCorreo() {

        boolean errorEC = true;
        for (String correoDestinatario : this.destinatarios) {
            if (correoDestinatario != null && !correoDestinatario.trim().isEmpty()) {

                try {
                    this.getGeneralesService().enviarCorreo(
                        correoDestinatario, this.asunto, this.contenidoCorreo, this.adjuntos,
                        this.titulosAdjuntos);
                    errorEC = false;
                } catch (Exception ex) {
                    LOGGER.error("Error enviando correo en método " +
                        "VisualizacionCorreoActualizacionMB#enviarCorreo al destinatario " +
                         correoDestinatario, ex);
                    errorEC = true;
                    continue;
                }
            }
        }

        if (!errorEC) {
            this.addMensajeInfo("Se envió el correo electrónico satisfactoriamente");
            LOGGER.info("Se envió el correo electrónico satisfactoriamente");
        } else {
            this.addMensajeWarn("Ocurrió algún error en el envío de correos");
            LOGGER.warn("Ocurrió algún error en el envío de correos");
        }
        try {
            // Se le envía una copia al remitente para que archive el correo enviado.
            this.getGeneralesService().enviarCorreo(this.remitente,
                this.asunto, this.contenidoCorreo, this.adjuntos,
                this.titulosAdjuntos);

        } catch (Exception e) {
            LOGGER.error("Error al enviar el correo electrónico: " + e);
            this.addMensajeError("Error al enviar el correo electrónico.");
        }
    }

    /** --------------------------------------------------------------------- */
    /**
     * Método ejecutado al dar click sobre el botón cancelar, reinicia las variables del managed
     * bean y lo remueve de la sesión.
     *
     * @author david.cifuentes
     *
     */
    public void cerrarVisualizacionCorreo() {
        this.asunto = "";
        this.codigoPlantillaContenido = "";
        this.remitente = "";
        this.encabezadoCorreo = "";
        this.contenidoCorreo = "";
        this.adjuntos = null;
        this.destinatarios = null;
        this.parametrosPlantillaContenido = null;

        // Remover el managed bean de la sesión.
        UtilidadesWeb.removerManagedBean("visualizacionCorreoActualizacion");
        return;
    }
    // end of class
}
