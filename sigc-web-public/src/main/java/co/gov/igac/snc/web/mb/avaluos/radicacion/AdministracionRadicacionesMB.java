package co.gov.igac.snc.web.mb.avaluos.radicacion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.ETramiteTipoTramite;
import co.gov.igac.snc.util.FiltroDatosConsultaRadicacionesCorrespondencia;
import co.gov.igac.snc.util.RadicacionNuevaAvaluoComercialDTO;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.GeneralMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed Bean para CU-SA-AC-047 (Administrar Radicaciones)
 *
 * @author rodrigo.hernandez
 *
 */
@Component("administracionRadicacionesAvaluo")
@Scope("session")
public class AdministracionRadicacionesMB extends SNCManagedBean {

    /**
     * Serial Id
     */
    private static final long serialVersionUID = -8892740765731101282L;

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionRadicacionesMB.class);

    /**
     * Variable para almacenar datos de los filtros de busqueda
     */
    private FiltroDatosConsultaRadicacionesCorrespondencia filtroDatos;

    /**
     * Lista para selectOneMenu de departamentos
     */
    private List<SelectItem> departamentosCombo;

    private ArrayList<Departamento> departamentos;

    /**
     * Lista para selectOneMenu de municipios
     */
    private List<SelectItem> municipiosCombo;

    /**
     * Lista para selectOneMenu de Territorial
     */
    private List<SelectItem> territorialesCombo;

    /**
     * Lista para selectOneMenu de Tipo de trámite
     */
    private List<SelectItem> tiposTramiteCombo;

    /**
     * Lista para selectOneMenu de Tipo de empresa
     */
    private List<SelectItem> tiposEmpresaCombo;

    /**
     * Lista de radicaciones nuevas traídas desde correspondencia
     */
    private List<RadicacionNuevaAvaluoComercialDTO> listaRadicacionesNuevas;

    /**
     * Radicación seleccionada desde la tabla
     */
    private RadicacionNuevaAvaluoComercialDTO radicacionNuevaSeleccionada;

    /**
     * Variable que indica el tipo de origen de búsqueda </br> <b>True: </b>El origen es <b><i>por
     * correspondencia</i></b> </br> <b>False: </b>El origen es <b><i>por tramite de avaluo</i></b>
     */
    private boolean banderaEsOrigenPorCorrespondencia;

    /**
     * Variable que indica si el usuario en sesion pertenece a Sede Central</br>
     * <b>True: </b> Si pertenece a sede central </br> <b>False: </b>No pertenece a sede central
     */
    private boolean banderaEsUsuarioDeCentral;

    /**
     * Variable que indica si este MB está siendo llamado desde MB de CU-SA-AC-152 Recibir
     * documentación <b>True: </b> Está siendo llamado desde CU-152 </br> <b>False: </b>No Está
     * siendo llamado desde CU-152
     *
     */
    private boolean banderaEsLlamadoDesdeCU152;

    /**
     * Usuario en sesión
     */
    private UsuarioDTO usuario;

    @Autowired
    private GeneralMB generalMB;

    @Autowired
    private ReclasificacionDocumentoTramiteMB reclasificacionDocumentoTramiteMB;

    public List<SelectItem> getDepartamentosCombo() {
        return departamentosCombo;
    }

    public void setDepartamentosCombo(List<SelectItem> departamentosCombo) {
        this.departamentosCombo = departamentosCombo;
    }

    public List<SelectItem> getMunicipiosCombo() {
        return this.municipiosCombo;
    }

    public void setMunicipiosCombo(List<SelectItem> municipiosCombo) {
        this.municipiosCombo = municipiosCombo;
    }

    public boolean isBanderaEsOrigenPorCorrespondencia() {
        return banderaEsOrigenPorCorrespondencia;
    }

    public void setBanderaEsOrigenPorCorrespondencia(
        boolean banderaEsOrigenPorCorrespondencia) {
        this.banderaEsOrigenPorCorrespondencia = banderaEsOrigenPorCorrespondencia;
    }

    public ArrayList<Departamento> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(ArrayList<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public List<SelectItem> getTerritorialesCombo() {
        return territorialesCombo;
    }

    public void setTerritorialesCombo(List<SelectItem> territorialesCombo) {
        this.territorialesCombo = territorialesCombo;
    }

    public List<SelectItem> getTiposTramiteCombo() {
        return tiposTramiteCombo;
    }

    public void setTiposTramiteCombo(List<SelectItem> tiposTramiteCombo) {
        this.tiposTramiteCombo = tiposTramiteCombo;
    }

    public List<SelectItem> getTiposEmpresaCombo() {
        return tiposEmpresaCombo;
    }

    public void setTiposEmpresaCombo(List<SelectItem> tiposEmpresaCombo) {
        this.tiposEmpresaCombo = tiposEmpresaCombo;
    }

    public boolean isBanderaEsUsuarioDeCentral() {
        return banderaEsUsuarioDeCentral;
    }

    public void setBanderaEsUsuarioDeCentral(boolean banderaEsUsuarioDeCentral) {
        this.banderaEsUsuarioDeCentral = banderaEsUsuarioDeCentral;
    }

    public List<RadicacionNuevaAvaluoComercialDTO> getListaRadicacionesNuevas() {
        return listaRadicacionesNuevas;
    }

    public void setListaRadicacionesNuevas(
        List<RadicacionNuevaAvaluoComercialDTO> listaRadicacionesNuevas) {
        this.listaRadicacionesNuevas = listaRadicacionesNuevas;
    }

    public RadicacionNuevaAvaluoComercialDTO getRadicacionNuevaSeleccionada() {
        return radicacionNuevaSeleccionada;
    }

    public void setRadicacionNuevaSeleccionada(
        RadicacionNuevaAvaluoComercialDTO radicacionNuevaSeleccionada) {
        this.radicacionNuevaSeleccionada = radicacionNuevaSeleccionada;
    }

    public FiltroDatosConsultaRadicacionesCorrespondencia getFiltroDatos() {
        return filtroDatos;
    }

    public void setFiltroDatos(
        FiltroDatosConsultaRadicacionesCorrespondencia filtroDatos) {
        this.filtroDatos = filtroDatos;
    }

    public boolean isBanderaEsLlamadoDesdeCU152() {
        return banderaEsLlamadoDesdeCU152;
    }

    public void setBanderaEsLlamadoDesdeCU152(boolean banderaEsLlamadoDesdeCU152) {
        this.banderaEsLlamadoDesdeCU152 = banderaEsLlamadoDesdeCU152;
    }

    // -------------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("Inicio AdministracionRadicacionesMB#init");

        this.iniciarDatos();

        LOGGER.debug("Fin AdministracionRadicacionesMB#init");
    }

    // ---------------------------------------------------------------------------------------------------------
    /**
     * Método para iniciar datos (...)
     *
     * @author rodrigo.hernandez
     */
    private void iniciarDatos() {
        LOGGER.debug("Inicio AdministracionRadicacionesMB#iniciarDatos");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.iniciarValores();
        this.iniciarListas();
        this.iniciarListasCombos();
        this.iniciarBanderas();

        this.validarSiUsuarioPerteneceASedeCentral();

        LOGGER.debug("Fin AdministracionRadicacionesMB#iniciarDatos");
    }

    /**
     * Método para iniciar datos del MB
     *
     * @author rodrigo.hernandez
     */
    private void iniciarValores() {
        this.filtroDatos = new FiltroDatosConsultaRadicacionesCorrespondencia();
        this.filtroDatos.setCodigoTerritorial(this.usuario
            .getCodigoTerritorial());
    }

    /**
     * Método para iniciar variables bandera
     *
     * @author rodrigo.hernandez
     */
    private void iniciarBanderas() {
        this.banderaEsOrigenPorCorrespondencia = true;
        this.banderaEsUsuarioDeCentral = false;
        this.banderaEsLlamadoDesdeCU152 = false;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar los SelectOneMenu
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListasCombos() {
        LOGGER.debug("Inicio AdministracionRadicacionesMB#iniciarListasCombos");

        this.cargarComboDepartamentos();
        this.cargarComboTerritoriales();
        this.cargarComboTiposEmpresa();
        this.cargarComboTiposTramite();

        LOGGER.debug("Fin AdministracionRadicacionesMB#iniciarListasCombos");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para iniciar listas usadas
     *
     * @author rodrigo.hernandez
     */
    private void iniciarListas() {
        this.listaRadicacionesNuevas = new ArrayList<RadicacionNuevaAvaluoComercialDTO>();
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para validar si el usuario en sesion pertenece a sede central
     *
     * @author rodrigo.hernandez
     */
    private void validarSiUsuarioPerteneceASedeCentral() {
        LOGGER.debug("Inicio AdministracionRadicacionesMB#validarSiUsuarioPerteneceASedeCentral");

        // TODO:: rodrigo.hernandez :: pedir usuario funcionario radicador para
        // sede central y probar la validación
        if (this.usuario.getCodigoTerritorial() == null) {
            this.banderaEsUsuarioDeCentral = true;
        } else {
            this.banderaEsUsuarioDeCentral = false;
        }

        LOGGER.debug("Fin AdministracionRadicacionesMB#validarSiUsuarioPerteneceASedeCentral");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de Departamentos
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboDepartamentos() {

        LOGGER.debug("Inicio AdministracionRadicados#cargarComboDepartamentos");

        this.departamentos = (ArrayList<Departamento>) this
            .getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.filtroDatos.getCodigoTerritorial());

        this.departamentosCombo = this.generalMB
            .getListaCodigoYDepartamentoPorTerritorial(this.filtroDatos
                .getCodigoTerritorial());

        LOGGER.debug("Fin AdministracionRadicados#cargarComboDepartamentos");

    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de Municipios
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboMunicipios() {
        LOGGER.debug("Inicio AdministracionRadicados#cargarComboMunicipios");

        if (this.filtroDatos.getCodigoDepartamentoOrigen() != null &&
             !this.filtroDatos.getCodigoDepartamentoOrigen().isEmpty()) {
            this.municipiosCombo = this.generalMB
                .getListaCodigoYMunicipioPorDepartamento(this.filtroDatos
                    .getCodigoDepartamentoOrigen());
        }

        LOGGER.debug("Fin AdministracionRadicados#cargarComboMunicipios");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de territoriales
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboTerritoriales() {
        LOGGER.debug("Inicio AdministracionRadicados#cargarComboTerritoriales");

        this.territorialesCombo = this.generalMB
            .getTerritorialesConDireccionGeneral();

        LOGGER.debug("Fin AdministracionRadicados#cargarComboTerritoriales");
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de tipos de empresa
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboTiposEmpresa() {
        LOGGER.debug("Inicio AdministracionRadicados#cargarComboTiposEmpresa");

        this.tiposEmpresaCombo = this.generalMB.getPersonaTipoPersonaV();

        LOGGER.debug("Fin AdministracionRadicados#cargarComboTiposEmpresa");
    }

    // --------------------------------------------------------------------------------------------
    /**
     * Método para cargar el combo de tipos de tramite
     *
     * @author rodrigo.hernandez
     */
    private void cargarComboTiposTramite() {
        LOGGER.debug("Inicio AdministracionRadicados#cargarComboTiposTramite");

        this.tiposTramiteCombo = new ArrayList<SelectItem>();

        for (SelectItem si : this.generalMB.getAvaluosComercialesTipoTramite()) {
            if (!si.getValue().equals(
                ETramiteTipoTramite.SOLICITUD_DE_CORRECCION
                    .getCodigo())) {
                this.tiposTramiteCombo.add(si);
            }
        }

        LOGGER.debug("Fin AdministracionRadicados#cargarComboTiposTramite");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que actualiza los municipios en cada cambio de departamento.
     *
     * @author rodrigo.hernandez
     */
    public void departamentoCambio() {
        LOGGER.debug("Inicio AdministracionRadicados#departamentoCambio");

        this.cargarComboMunicipios();

        LOGGER.debug("Fin AdministracionRadicados#departamentoCambio");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para reiniciar valores de campos cuando se cambia el valor del origen de busqueda
     *
     * @author rodrigo.hernandez
     */
    public void origenBusquedaCambio() {
        LOGGER.debug("Inicio AdministracionRadicados#origenBusquedaCambio");

        this.iniciarValores();

        LOGGER.debug("Fin AdministracionRadicados#origenBusquedaCambio");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para buscar radicaciones según filtros seleccionados
     *
     * @author rodrigo.hernandez
     */
    public void buscarRadicaciones() {
        LOGGER.debug("Inicio AdministracionRadicados#buscarRadicaciones");

        this.iniciarListas();

        if (this.validarDatosBusqueda()) {

            if (!this.banderaEsOrigenPorCorrespondencia) {
                RadicacionNuevaAvaluoComercialDTO radicacionNueva = this
                    .getAvaluosService().buscarRadicacion(
                        this.filtroDatos.getNumeroRadicacion(), this.usuario);

                if (radicacionNueva != null) {
                    this.listaRadicacionesNuevas.add(radicacionNueva);
                }

            } else {
                this.buscarListaRadicacionesAvaluos();
            }
        }

        LOGGER.debug("Fin AdministracionRadicados#buscarRadicaciones");
    }

    public void buscarListaRadicacionesAvaluos() {
        LOGGER.debug("Inicio AdministracionRadicados#buscarListaRadicacionesAvaluos");

        this.listaRadicacionesNuevas = this.getAvaluosService()
            .buscarRadicacionesNuevasAvaluosComerciales(this.filtroDatos, this.usuario);

        LOGGER.debug("Fin  AdministracionRadicados#buscarListaRadicacionesAvaluos");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para reclasificar la radicacion seleccionada
     *
     * @author rodrigo.hernandez
     */
    public void reclasificarTramite() {
        LOGGER.debug("Inicio AdministracionRadicados#reclasificarTramite");

        this.reclasificacionDocumentoTramiteMB
            .cargarRadicacionNueva(this.radicacionNuevaSeleccionada);

        LOGGER.debug("Fin AdministracionRadicados#reclasificarTramite");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para anexar una solicitud a la radicacion seleccionada
     *
     * @author rodrigo.hernandez
     */
    public void anexarSolicitud() {
        LOGGER.debug("Inicio AdministracionRadicados#anexarSolicitud");

        // TODO :: rodrigo.hernandez :: 14-09-2012 :: Implementar anexo de
        // solicitud al process
        LOGGER.debug("Fin AdministracionRadicados#anexarSolicitud");
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para realizar las validaciones necesarias para ejecutar la busqueda de radicaciones
     *
     * @author rodrigo.hernandez
     *
     * @return
     */
    private boolean validarDatosBusqueda() {
        LOGGER.debug("Inicio AdministracionRadicados#validarDatosBusqueda");

        boolean result = true;
        String mensaje = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String fechaDesde = this.filtroDatos.getFechaDesde() != null ? sdf
            .format(this.filtroDatos.getFechaDesde()) : "";
        String fechaHasta = this.filtroDatos.getFechaHasta() != null ? sdf
            .format(this.filtroDatos.getFechaHasta()) : "";

        LOGGER.debug(fechaDesde);
        LOGGER.debug(fechaHasta);

        if (this.filtroDatos.getFechaDesde() != null &&
             this.filtroDatos.getFechaHasta() != null) {
            // Se valida que la fecha hasta no sea posterior a la fecha desde
            if (this.filtroDatos.getFechaDesde().after(
                this.filtroDatos.getFechaHasta())) {
                result = false;
                mensaje =
                    "La fecha Radicación Desde es mayor que la Fecha Radicación Hasta. Intente nuevamente";
                this.addMensajeError(mensaje);
            }
        }

        // Se valida que existan valores para ambas fechas
        if (this.filtroDatos.getFechaDesde() == null &&
             this.filtroDatos.getFechaHasta() != null) {
            result = false;
            mensaje = "Debe Ingresar la fecha Radicación Desde. Intente nuevamente";
            this.addMensajeError(mensaje);
        }
        // Se valida que existan valores para ambas fechas
        if (this.filtroDatos.getFechaDesde() != null &&
             this.filtroDatos.getFechaHasta() == null) {
            result = false;
            mensaje = "Debe Ingresar la fecha Radicación Hasta. Intente nuevamente";
            this.addMensajeError(mensaje);
        }

        LOGGER.debug("Fin AdministracionRadicados#validarDatosBusqueda");

        return result;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método encargado de terminar la sesión de la oferta inmobiliaria y terminar
     *
     * @author rodrigo.hernandez
     */
    public String cerrar() {

        LOGGER.debug("iniciando AdministracionRadicados#cerrar");

        UtilidadesWeb.removerManagedBean("administracionRadicacionesAvaluo");

        LOGGER.debug("finalizando AdministracionRadicados#cerrar");

        return ConstantesNavegacionWeb.INDEX;
    }

    /**
     * Método que se llama desde CU-SA-AC-152 RecepcionDocumentacionMB
     *
     * @author rodrigo.hernandez
     */
    public void cargarDesdeCU152(Solicitud solicitud) {
        LOGGER.debug("iniciando AdministracionRadicados#cargarDesdeCU152");

        this.banderaEsLlamadoDesdeCU152 = true;

        LOGGER.debug("Fin AdministracionRadicados#cargarDesdeCU152");
    }

}
