/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.validacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.impl.procesador.ProcesadorDocumentosImpl;
import co.gov.igac.sigc.documental.interfaces.IProcesadorDocumentos;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.FirmaUsuario;
import co.gov.igac.snc.persistence.entity.generales.Parametro;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import co.gov.igac.snc.web.util.procesos.ProcesosUtilidadesWeb;

/**
 * Managed bean para el CU Notificar por edicto
 *
 * @author pedro.garcia
 *
 */
@Component("notificacionPorEdicto")
@Scope("session")
public class NotificacionPorEdictoMB extends ProcessFlowManager {

    /**
     *
     */
    private static final long serialVersionUID = 5374779828066661542L;

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificacionPorEdictoMB.class);

    //------------------ services   ----------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * trámite sober el que se está trabajando
     */
    private Tramite currentTramite;

    /**
     * id del trámite seleccionado de la tabla
     */
    private long currentTramiteId;

    /**
     * usuario de la sesión
     */
    private UsuarioDTO currentUser;

    /**
     * lista con los solicitantes del trámite a quienes se notifica por edicto
     */
    private ArrayList<Solicitante> solicitantesNotificacionPorEdicto;

    /**
     * objeto Documento que se guarda en la base de datos
     */
    private Documento documentoEdicto;

    /**
     * Solicitantes seleccionados para notificacion
     */
    private Solicitante[] solicitantesSelecionados;

    /**
     * fechas del edicto
     */
    private Date fechaFijacionEdicto;
    private Date fechaDesfijacionEdicto;
    private Date fechaHoy;

    private int diasFijacionEdicto;

    //--------------Variables para el reporte ----------------------------
    /**
     * Objeto que contiene los datos del reporte de memorando de comisión
     */
    private ReporteDTO reporteNotificacionPorEdicto;

    //--------------     flags  --------
    private boolean edictoGuardado;

    //Manejo de tramites masivos
    /**
     * bandera para la direccion del edicto
     */
    private boolean banderaConDireccion;

    /**
     * Mensaje de confirmacion edicto
     */
    private String mensajeConfirmaEdicto;

    //-----   BPM  --------
    private Actividad currentProcessActivity;

    private Date fechaComunicNotif;

    private String medioComunicNotif;

    private String observacionesComunicNotif;

    //--------------  methods   ---------------
    public boolean isEdictoGuardado() {
        return this.edictoGuardado;
    }

    public void setEdictoGuardado(boolean edictoGuardado) {
        this.edictoGuardado = edictoGuardado;
    }

    public Date getFechaComunicNotif() {
        return fechaComunicNotif;
    }

    public void setFechaComunicNotif(Date fechaComunicNotif) {
        this.fechaComunicNotif = fechaComunicNotif;
    }

    public String getMedioComunicNotif() {
        return medioComunicNotif;
    }

    public void setMedioComunicNotif(String medioComunicNotif) {
        this.medioComunicNotif = medioComunicNotif;
    }

    public String getObservacionesComunicNotif() {
        return observacionesComunicNotif;
    }

    public void setObservacionesComunicNotif(String observacionesComunicNotif) {
        this.observacionesComunicNotif = observacionesComunicNotif;
    }

    public boolean isBanderaConDireccion() {
        return banderaConDireccion;
    }

    public void setBanderaConDireccion(boolean banderaConDireccion) {
        this.banderaConDireccion = banderaConDireccion;
    }

    public String getMensajeConfirmaEdicto() {
        return mensajeConfirmaEdicto;
    }

    public void setMensajeConfirmaEdicto(String mensajeConfirmaEdicto) {
        this.mensajeConfirmaEdicto = mensajeConfirmaEdicto;
    }

    public ReporteDTO getReporteNotificacionPorEdicto() {
        return reporteNotificacionPorEdicto;
    }

    public void setReporteNotificacionPorEdicto(
        ReporteDTO reporteNotificacionPorEdicto) {
        this.reporteNotificacionPorEdicto = reporteNotificacionPorEdicto;
    }

    public ArrayList<Solicitante> getSolicitantesNotificacionPorEdicto() {
        return this.solicitantesNotificacionPorEdicto;
    }

    public void setSolicitantesNotificacionPorEdicto(
        ArrayList<Solicitante> solicitantesNotificacionPorEdicto) {
        this.solicitantesNotificacionPorEdicto = solicitantesNotificacionPorEdicto;
    }

    public Date getFechaHoy() {
        return this.fechaHoy;
    }

    public void setFechaHoy(Date fechaHoy) {
        this.fechaHoy = fechaHoy;
    }

    public Date getFechaFijacionEdicto() {
        return this.fechaFijacionEdicto;
    }

    public void setFechaFijacionEdicto(Date fechaFijacionEdicto) {
        this.fechaFijacionEdicto = fechaFijacionEdicto;
    }

    public Solicitante[] getSolicitantesSelecionados() {
        return solicitantesSelecionados;
    }

    public void setSolicitantesSelecionados(Solicitante[] solicitantesSelecionados) {
        this.solicitantesSelecionados = solicitantesSelecionados;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * esta hay que calcularla
     *
     * @return
     */
    public Date getFechaDesfijacionEdicto() {

        if (this.fechaFijacionEdicto != null) {
//            ArrayList<Object> parametros = new ArrayList<Object>();
//            Object[] respuestaEjecucion = new Object[1];
//            Timestamp ts2;

//  OJO: no borrar lo comentado.
//       Sirve para ver cómo se hacía usando la clase que ejecuta procedimientos almacenados.
//       Ahora se ejecuta el query directamente
//            ts2 = new Timestamp(this.fechaFijacionEdicto.getTime());
//
//            //D: primer parámetro es la fecha desde la cual se calcula
//            parametros.add(0, ts2);
//            //D: segundo parámetro es el número de días hábiles que se le suma a la fecha
//            parametros.add(1, new BigDecimal(this.diasFijacionEdicto));
//            //D: tercer parámetro es el código de oficina (por si se debe calcular para alguna en
//            //  particular). Está definido como '0' por defecto en la función, luego se le pasa ese valor
//            parametros.add(2, "0");
//
//            //D: el parámetro schema (el último) se debe enviar como null porque la función no se
//            //   invoca desde un esquema en particular
//            respuestaEjecucion =
//                this.getGeneralesService().ejecutarSPFuncion(
//                EProcedimientoAlmacenadoFuncion.FNC_SUMAR_DIAS_HABILES, parametros,
//                EProcedimientoAlmacenadoFuncionTipo.FUNCION, null);
//
//            this.fechaDesfijacionEdicto =
//                (respuestaEjecucion[0] != null)? (Date) respuestaEjecucion[0] : this.fechaHoy;
            this.fechaDesfijacionEdicto =
                this.getGeneralesService().obtenerFechaConAdicionDiasHabiles(
                    this.fechaFijacionEdicto, this.diasFijacionEdicto);
        }

        return this.fechaDesfijacionEdicto;
    }

    public void setFechaDesfijacionEdicto(Date fechaDesfijacionEdicto) {
        this.fechaDesfijacionEdicto = fechaDesfijacionEdicto;
    }
//--------------------------------------------------------------------------------------------------

    public Tramite getCurrentTramite() {
        return this.currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("on NotificacionPorEdictoMB#init ");

        this.currentUser = MenuMB.getMenu().getUsuarioDto();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "notificacionPorEdicto");

        this.fechaHoy = new Date(System.currentTimeMillis());

        this.currentTramiteId =
            this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();

        //Obtener la actividad actual:
        this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
            this.currentTramiteId);

        //D: buscar los trámites
        this.currentTramite =
            this.getTramiteService().buscarTramiteSolicitudPorId(this.currentTramiteId);

        //D: para este CU se obtienen todos los solicitantes
        this.solicitantesNotificacionPorEdicto = (ArrayList<Solicitante>) this.getTramiteService().
            obtenerSolicitantesTodosDeTramite(
                this.currentTramiteId, this.currentTramite.getSolicitud().getId(),
                this.currentTramite.getSolicitud().getTipo());

        List<Solicitante> solicitantesPropietariosNotificacionPorEdicto =
            new ArrayList<Solicitante>();
        //felipe.cadena::14-07-2015::tramites masivos::tramites masivos
        if (this.currentTramite.isRectificacionMatriz() || this.currentTramite.isTerceraMasiva()) {
            List<Solicitante> solicitantesPropietarios = this.getTramiteService().
                obtenerSolicitantesParaNotificacion(this.currentTramiteId);
            solicitantesPropietariosNotificacionPorEdicto.addAll(solicitantesPropietarios);
        }

        // #14642 :: Si el solicitante es el mismo propietario, se toma solo el solicitante :: david.cifuentes :: 11/11/2015
        if (!solicitantesPropietariosNotificacionPorEdicto.isEmpty() &&
            this.solicitantesNotificacionPorEdicto != null) {
            List<Solicitante> propietariosANotificar = new ArrayList<Solicitante>();
            for (Solicitante sp : solicitantesPropietariosNotificacionPorEdicto) {
                for (Solicitante s : this.solicitantesNotificacionPorEdicto) {
                    if (!s.getNumeroIdentificacion().equals(sp.getNumeroIdentificacion())) {
                        propietariosANotificar.add(sp);
                    }
                }
            }
            if (!propietariosANotificar.isEmpty()) {
                this.solicitantesNotificacionPorEdicto.addAll(propietariosANotificar);
            }
        }

        //felipe.cadena::14-07-2015::tramites masivos::Se cargan los documentos de edictos previos
        this.determinarDocumentosSolicitantes();

        if (this.solicitantesNotificacionPorEdicto != null) {
            //D: no debería pasar
            if (this.solicitantesNotificacionPorEdicto.isEmpty()) {
                this.addMensajeError("No hay solicitantes para este trámite.");
                return;
            }
        }

        //D: obtener el parámetro del número de días que debe fijarse un edicto
        Parametro paramDB;
        paramDB =
            this.getGeneralesService().getCacheParametroPorNombre(
                EParametro.DIAS_FIJACION_EDICTO.toString());
        this.diasFijacionEdicto = paramDB.getValorNumero().intValue();

    }

    /**
     * Determina cuales solicitantes ya tienen documentos seleccionados
     */
    public void determinarDocumentosSolicitantes() {

        Long tipoDocumentoId =
            ETipoDocumento.EDICTO_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_Y_REPOSICION.
                getId();
        if (this.currentTramite.getSolicitud().isViaGubernativa()) {
            tipoDocumentoId = ETipoDocumento.EDICTO_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId();
        }

        List<TramiteDocumento> docsTramite = (ArrayList<TramiteDocumento>) this.getTramiteService().
            obtenerTramiteDocumentosDeTramitePorTipoDoc(
                this.currentTramiteId, tipoDocumentoId);

        for (TramiteDocumento tramiteDocumento : docsTramite) {
            for (Solicitante sol : this.solicitantesNotificacionPorEdicto) {
                if (sol.getNumeroIdentificacion().
                    equals(tramiteDocumento.getIdPersonaNotificacion())) {
                    sol.setDocumentoNotificacion(tramiteDocumento);
                    break;
                }
            }
        }

        //Para evitar null pointer en la vista
        for (Solicitante sol : this.solicitantesNotificacionPorEdicto) {
            if (sol.getDocumentoNotificacion() == null) {
                sol.setDocumentoNotificacion(new TramiteDocumento());
            }
        }

    }

    /**
     * Determina que mensaje se debe mostrar en el dialogo de confirmacion
     */
    public void determinarMensajeConfirmacion() {
        if (this.banderaConDireccion) {
            this.mensajeConfirmaEdicto = "¿Esta seguro de generar el aviso con dirección? ";
        } else {
            this.mensajeConfirmaEdicto = "¿Esta seguro de generar el aviso sin dirección? ";
        }
    }

    /**
     * Determina si la generacion del edicto se realiza con direccion o sin direccion
     *
     * @author felipe.cadena
     */
    public void generarEdictoOpcional() {

        if (this.banderaConDireccion) {
            this.generarEdictoConDireccion();
        } else {
            this.generarEdicto();
        }

        //D: mover el proceso
        Date fechaNotificacion = this.determinarNotificacionSolicitantes();
        if (fechaNotificacion != null) {
            LOGGER.debug("Todos los solicitantes han sido notificados");
            this.forwardThisProcess();

            // felipe.cadena::21-09-2015::Si todos los solicitantes
            // se notificaron con direccion se cambia la fecha de avance a solo
            // un día
            if (this.determinarNotificacionPorDireccion()) {
                this.asignarFechaAvanceAviso();
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Generar edicto" Genera el reporte con el edicto y lo deja en la variable
     * que se usa como value en el componente p:download de la página donde se pinta
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarEdicto() {

        EReporteServiceSNC enumeracionReporte;
        String fechaAux;
        Map<String, String> parameters;
        UsuarioDTO responsableConservacion = null;
        FirmaUsuario firma = null;
        String documentoFirma = "";
        parameters = new HashMap<String, String>();

        SimpleDateFormat formatoFecha = new SimpleDateFormat(Constantes.FORMATO_FECHA_EDICTO);
        enumeracionReporte = EReporteServiceSNC.EDICTO;

        //felipe.cadena :: 15-07-2015 :: Se genera la fecha de desfijacion
        this.fechaDesfijacionEdicto =
             this.getGeneralesService().obtenerFechaConAdicionDiasHabiles(
                this.fechaFijacionEdicto, this.diasFijacionEdicto);

        String urlSubreporteResoluciones = this.getTramiteService().
            obtenerUrlSubreporteResolucionesResuelve(this.currentTramite.getId());

        if (this.currentTramite.isActualizacion()) {
            enumeracionReporte = EReporteServiceSNC.EDICTO_ACTUALIZACION;
        }

        parameters.put("TRAMITE_ID", String.valueOf(this.currentTramiteId));
        fechaAux = formatoFecha.format(this.fechaFijacionEdicto);
        parameters.put("FECHA_FIJACION", fechaAux);
        fechaAux = formatoFecha.format(this.fechaDesfijacionEdicto);
        parameters.put("FECHA_DESFIJACION", fechaAux);
        parameters.put("NUMERO_DIAS_FIJACION", String.valueOf(this.diasFijacionEdicto));
        parameters.put("NOMBRE_SUBREPORTE_RESUELVE", urlSubreporteResoluciones);

        //obtener responsable conservacion 
        List<UsuarioDTO> responsablesConservacion = new ArrayList<UsuarioDTO>();

        responsablesConservacion = (List<UsuarioDTO>) this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.currentUser
                    .getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION);
        if (responsablesConservacion != null &&
             !responsablesConservacion.isEmpty() &&
             responsablesConservacion.get(0) != null) {
            responsableConservacion = responsablesConservacion.get(0);

            firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                responsableConservacion.getNombreCompleto());

            if (firma != null && responsableConservacion.getNombreCompleto().equals(firma.
                getNombreFuncionarioFirma())) {

                documentoFirma = this.getGeneralesService().descargarArchivoAlfrescoYSubirAPreview(
                    firma.getDocumentoId().getIdRepositorioDocumentos());

                parameters.put("FIRMA_USUARIO", documentoFirma);
            }
        }

        this.reporteNotificacionPorEdicto = this.reportsService.generarReporte(parameters,
            enumeracionReporte, this.currentUser);

        //juanfelipe.garcia :: #4545 cuando se genera el reporte se debe guardar el edicto
        guardarEdicto();
        //felipe.cadena:: 14-07-2015 :: actualiza el estado de los solicitantes
        determinarDocumentosSolicitantes();

        if (this.reporteNotificacionPorEdicto == null) {
            Exception ex = new Exception(Constantes.MENSAJE_ERROR_REPORTE);
            this.addMensajeError("Error al generar el edicto.");
            LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex,
                enumeracionReporte.getNombreReporte());
        }

    }

    /**
     * Genera avisos individuales para cuando se tenga disponible la direccion
     *
     * @author felipe.cadena
     */
    /*
     * @modified by leidy.gonzalez :: #13244:: 10/07/2015:: Se agrega parametro de firma al reporte
     * a generar si existe la firma asociada al funcionario que esta firmando el documento.
     */
    public void generarEdictoConDireccion() {

        EReporteServiceSNC enumeracionReporte;
        String fechaAux;
        Map<String, String> parameters;
        List<String> rutasMemorandos = new ArrayList<String>();
        UsuarioDTO responsableConservacion = null;
        FirmaUsuario firma = null;
        String documentoFirma = "";

        SimpleDateFormat formatoFecha = new SimpleDateFormat(Constantes.FORMATO_FECHA_EDICTO);
        enumeracionReporte = EReporteServiceSNC.AVISO_PROPIETARIO_CON_DIRECCION;

        //obtener responsable conservacion 
        List<UsuarioDTO> responsablesConservacion = new ArrayList<UsuarioDTO>();

        responsablesConservacion = (List<UsuarioDTO>) this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.currentUser
                    .getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION);
        if (responsablesConservacion != null &&
             !responsablesConservacion.isEmpty() &&
             responsablesConservacion.get(0) != null) {
            responsableConservacion = responsablesConservacion.get(0);
        }

        for (Solicitante solicitante : this.solicitantesSelecionados) {

            parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID", String.valueOf(this.currentTramiteId));

            // Parametro del solicitante
            parameters.put("SOLICITANTE_ID", "" +
                 solicitante.getId());

            parameters.put("NUMERO_RADICADO", "");

            fechaAux = formatoFecha.format(this.fechaFijacionEdicto);
            parameters.put("FECHA_FIJACION", fechaAux);

            if (responsableConservacion != null &&
                 responsableConservacion.getNombreCompleto() != null) {
                parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                    responsableConservacion.getNombreCompleto());

                firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                    responsableConservacion.getNombreCompleto());

                if (firma != null && responsableConservacion.getNombreCompleto().equals(firma.
                    getNombreFuncionarioFirma())) {

                    documentoFirma = this.getGeneralesService().
                        descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                            getIdRepositorioDocumentos());

                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }
            }

            //parameters.put("NOMBRE_SUBREPORTE_RESUELVE", urlSubreporteResoluciones);
            ReporteDTO reporte = this.reportsService.generarReporte(parameters, enumeracionReporte,
                this.currentUser);

            if (reporte == null) {
                Exception ex = new Exception(Constantes.MENSAJE_ERROR_REPORTE);
                this.addMensajeError("Error al generar el edicto.");
                LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
                throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex,
                    enumeracionReporte.getNombreReporte());
            } else {
                rutasMemorandos.add(reporte.getRutaCargarReporteAAlfresco());
            }

        }

        //felipe.cadena::15-07-2015::unir documentos resultants
        if (!rutasMemorandos.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
            this.reporteNotificacionPorEdicto =
                 this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

        //determinarDocumentosSolicitantes();
        if (this.reporteNotificacionPorEdicto == null) {
            Exception ex = new Exception(Constantes.MENSAJE_ERROR_REPORTE);
            this.addMensajeError("Error al generar el edicto.");
            LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex,
                enumeracionReporte.getNombreReporte());
        }

    }

    /**
     * Genera avisos individuales para cuando se tenga disponible la direccion
     *
     * @author felipe.cadena
     */
    public void radicarEdictoConDireccion() {

        EReporteServiceSNC enumeracionReporte;
        String fechaAux;
        Map<String, String> parameters;
        List<String> rutasMemorandos = new ArrayList<String>();
        UsuarioDTO responsableConservacion = null;
        FirmaUsuario firma = null;
        String documentoFirma = "";

        SimpleDateFormat formatoFecha = new SimpleDateFormat(Constantes.FORMATO_FECHA_EDICTO);
        enumeracionReporte = EReporteServiceSNC.AVISO_PROPIETARIO_CON_DIRECCION;

        //obtener responsable conservacion 
        List<UsuarioDTO> responsablesConservacion = new ArrayList<UsuarioDTO>();

        responsablesConservacion = (List<UsuarioDTO>) this
            .getTramiteService()
            .buscarFuncionariosPorRolYTerritorial(
                this.currentUser
                    .getDescripcionEstructuraOrganizacional(),
                ERol.RESPONSABLE_CONSERVACION);
        if (responsablesConservacion != null &&
             !responsablesConservacion.isEmpty() &&
             responsablesConservacion.get(0) != null) {
            responsableConservacion = responsablesConservacion.get(0);
        }

        for (Solicitante solicitante : this.solicitantesSelecionados) {

            //TODO:: felipe.cadena:: definir los parametros correspondientes al aviso
            String numeroRadicacion = obtenerNumeroRadicado(solicitante);

            if (numeroRadicacion == null) {
                this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_001.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_001.getMensajeTecnico());
                return;
            }

            parameters = new HashMap<String, String>();
            parameters.put("TRAMITE_ID", String.valueOf(this.currentTramiteId));

            // Parametro del solicitante
            parameters.put("SOLICITANTE_ID", "" +
                 solicitante.getId());

            parameters.put("NUMERO_RADICADO", numeroRadicacion);

            fechaAux = formatoFecha.format(this.fechaFijacionEdicto);
            parameters.put("FECHA_FIJACION", fechaAux);

            if (responsableConservacion != null &&
                 responsableConservacion.getNombreCompleto() != null) {
                parameters.put("NOMBRE_RESPONSABLE_CONSERVACION",
                    responsableConservacion.getNombreCompleto());

                firma = this.getGeneralesService().buscarDocumentoFirmaPorUsuario(
                    responsableConservacion.getNombreCompleto());

                if (firma != null && responsableConservacion.getNombreCompleto().equals(firma.
                    getNombreFuncionarioFirma())) {

                    documentoFirma = this.getGeneralesService().
                        descargarArchivoAlfrescoYSubirAPreview(firma.getDocumentoId().
                            getIdRepositorioDocumentos());

                    parameters.put("FIRMA_USUARIO", documentoFirma);
                }
            }

            ReporteDTO reporte = this.reportsService.generarReporte(parameters, enumeracionReporte,
                this.currentUser);

            if (reporte == null) {
                Exception ex = new Exception(Constantes.MENSAJE_ERROR_REPORTE);
                this.addMensajeError("Error al generar el edicto.");
                LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
                //throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex, enumeracionReporte.getNombreReporte());
            } else {
                rutasMemorandos.add(reporte.getRutaCargarReporteAAlfresco());
            }

            this.guardarDocumentoEdicto(reporte, numeroRadicacion);
            this.guardarTramiteDocumentosEdictoSol(solicitante);

        }

        //felipe.cadena::15-07-2015::unir documentos resultants
        if (!rutasMemorandos.isEmpty()) {
            IProcesadorDocumentos procesadorDocumentos = new ProcesadorDocumentosImpl();
            String documentoSalida = procesadorDocumentos.unirArchivosPDF(rutasMemorandos);
            this.reporteNotificacionPorEdicto =
                 this.reportsService.generarReporteDesdeUnaRutaDeArchivo(documentoSalida);
        }

        this.banderaConDireccion = false;
        this.determinarDocumentosSolicitantes();

        if (this.reporteNotificacionPorEdicto == null) {
            Exception ex = new Exception(Constantes.MENSAJE_ERROR_REPORTE);
            this.addMensajeError("Error al generar el edicto.");
            LOGGER.error("error generando reporte: " + ex.getMessage(), ex);
            throw SNCWebServiceExceptions.EXCEPCION_0002.getExcepcion(LOGGER, ex,
                enumeracionReporte.getNombreReporte());
        }

    }

    /**
     * Método para obtener el número de radicado de correspondencia
     *
     * @author javier.aponte
     *
     * @modified by javier.aponte 21/02/2014
     *
     * @modified by javier.aponte 28/02/2014 incidencia #7028 Se cambia codigo de estructura
     * organizacional por el código de la territorial
     */
    private String obtenerNumeroRadicado(Solicitante solicitante) {

        String numeroRadicado;

        List<Object> parametros = new ArrayList<Object>();

        parametros.add(this.currentUser.getCodigoTerritorial()); // Territorial=compania
        parametros.add(this.currentUser.getLogin()); // Usuario
        parametros.add("EE"); // tipo correspondencia
        parametros.add(String.valueOf(ETipoDocumento.OFICIO_COMUNICACION_DE_NOTIFICACION.getId())); // tipo contenido solicitud.getTipo()
        parametros.add(BigDecimal.valueOf(1)); //Cantidad de folios en documento principal
        parametros.add(BigDecimal.valueOf(0)); //Número de anexos
        parametros.add("Comunicación de notificación"); // Asunto
        parametros.add(""); // Observaciones
        parametros.add(""); // Dependencia origen
        parametros.add(""); // Funcionario origen
        parametros.add(""); // Cargo origen
        parametros.add(""); // Publicacion origen
        parametros.add("01"); // Sistema envio
        parametros.add(""); // Radicacion anterior
        parametros.add(""); // Radicacion responde
        parametros.add("6"); // presentacion, 6 internet
        parametros.add(""); // Destino compania = territorial
        parametros.add("0"); // Destino copia
        parametros.add(""); // Destino lugar
        parametros.add(solicitante.getNombreCompleto()); // Destino persona
        parametros.add(""); // Destino cargo
        parametros.add(solicitante.getDireccion()); // Direccion destino
        parametros.add(solicitante.getTipoIdentificacion()); // Tipo identificacion destino
        parametros.add(solicitante.getNumeroIdentificacion()); // Identificacion destino
        parametros.add(""); // Ciudad destino
        parametros.add(""); // Solicitante tipo documento
        parametros.add(""); // Solicitante  identificacion
        parametros.add(""); // Solicitante nombre
        parametros.add(""); // Solicitante teléfono
        parametros.add(""); // Solicitante email

        numeroRadicado = this.getGeneralesService().obtenerNumeroRadicado(parametros);

        return numeroRadicado;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "aceptar" en la ventana del doc con el edicto
     *
     * La validación de la fecha hay que hacerla aquí (no sirve en el xhtml)
     *
     * 1. guarda el documento en el gestor documental e inserta el respectivo registro en BD 2.
     * crear los registros en la tabla Tramite_Documento 3. mueve el proceso
     *
     */
    public void guardarEdicto() {

        if (fechaFijacionEdicto == null) {
            this.addMensajeError("Por favor ingrese la fecha de fijación del edicto");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return;
        }

        int exceptionIdentifier = 0;
        try {
            //D: 1.
            if (!this.guardarDocumentoEdicto(null, null)) {
                exceptionIdentifier = 3;
                throw new Exception("No se pudo guardar el documento del edicto de notificación");
            }

            //D: 2.
            if (!this.guardarTramiteDocumentosNotifPorEdicto()) {
                exceptionIdentifier = 1;
                throw new Exception(
                    "Ocurrió algún error al guardar registros en la tabla Tramite_Documento");
            }
        } catch (Exception ex) {
            switch (exceptionIdentifier) {
                case (1): {
                    throw SNCWebServiceExceptions.EXCEPCION_0001.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                case (3): {
                    throw SNCWebServiceExceptions.EXCEPCION_0003.getExcepcion(LOGGER, ex, ex.
                        getMessage());
                }
                default: {
                    break;
                }
            }
        }

        this.edictoGuardado = true;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "cerrar" de la ventana principal
     */
    public String closeCUWindow() {

        this.tareasPendientesMB.init();
        UtilidadesWeb.removerManagedBean("notificacionPorEdicto");
        return ConstantesNavegacionWeb.INDEX;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * Guarda registros en la tabla Tramite_Documento por cada uno de los solicitantes a los que se
     * les envía la comunicación.
     *
     * PRE: se ha ejecutado el método guardarDocumentoEdicto
     *
     * Deja en null la fecha de notificación (y el método de notificación) porque ese criterio (que
     * sea null) se utiliza para saber si ya se hizo el registro de la comunicación de notificación
     *
     * @return
     */
    private boolean guardarTramiteDocumentosNotifPorEdicto() {

        boolean answer = true;
        TramiteDocumento tramDoc, tempTramDoc;

        if (this.documentoEdicto == null) {
            return false;
        }

        //felipe.cadena::14-07-2015::Tramites masivos::Solo se genera el edicto para los solicitantes seleccionados
        for (Solicitante solicitante : this.solicitantesSelecionados) {
            tramDoc = new TramiteDocumento();
            tramDoc.setTramite(this.currentTramite);
            tramDoc.setDocumento(this.documentoEdicto);
            tramDoc.setIdRepositorioDocumentos(
                this.documentoEdicto.getIdRepositorioDocumentos());
            tramDoc.setPersonaNotificacion(solicitante.getNombreCompleto());
            tramDoc.setIdPersonaNotificacion(solicitante.getNumeroIdentificacion());
            tramDoc.setfecha(this.fechaHoy);
            tramDoc.setObservacionNotificacion("Notificado por edicto");
            tramDoc.setFechaFijacionEdicto(this.fechaFijacionEdicto);
            tramDoc.setFechaDesfijacionEdicto(this.fechaDesfijacionEdicto);
            tramDoc.setFechaLog(new Date(System.currentTimeMillis()));
            tramDoc.setUsuarioLog(this.currentUser.getLogin());

            try {
                tempTramDoc = this.getTramiteService().actualizarTramiteDocumento(tramDoc);
                solicitante.setDocumentoNotificacion(tempTramDoc);
            } catch (Exception ex) {
                LOGGER.error("Error guardando un TramiteDocumento en " +
                    "NotificacionPorEdictoMB#guardarTramiteDocumentosComunicNotif: " +
                    ex.getMessage(), ex);
                return false;
            }
        }

        return answer;
    }

    /**
     * Guarda registros en la tabla Tramite_Documento para un solicitante determinado
     *
     * @author felipe.cadena
     * @return
     */
    private boolean guardarTramiteDocumentosEdictoSol(Solicitante solicitante) {

        boolean answer = true;
        TramiteDocumento tramDoc, tempTramDoc;

        if (this.documentoEdicto == null) {
            return false;
        }
        tramDoc = new TramiteDocumento();
        tramDoc.setTramite(this.currentTramite);
        tramDoc.setDocumento(this.documentoEdicto);
        tramDoc.setIdRepositorioDocumentos(
            this.documentoEdicto.getIdRepositorioDocumentos());
        tramDoc.setPersonaNotificacion(solicitante.getNombreCompleto());
        tramDoc.setIdPersonaNotificacion(solicitante.getNumeroIdentificacion());
        tramDoc.setfecha(this.fechaHoy);
        tramDoc.setObservacionNotificacion(Constantes.NOTIFICADO_CON_DIRECCION);
        tramDoc.setFechaFijacionEdicto(this.fechaFijacionEdicto);
        //tramDoc.setFechaDesfijacionEdicto(this.fechaDesfijacionEdicto);
        tramDoc.setFechaLog(new Date(System.currentTimeMillis()));
        tramDoc.setUsuarioLog(this.currentUser.getLogin());

        try {
            tempTramDoc = this.getTramiteService().actualizarTramiteDocumento(tramDoc);
            solicitante.setDocumentoNotificacion(tempTramDoc);
        } catch (Exception ex) {
            LOGGER.error("Error guardando un TramiteDocumento en " +
                "NotificacionPorEdictoMB#guardarTramiteDocumentosComunicNotif: " +
                ex.getMessage(), ex);
            return false;
        }
        return answer;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Guarda el documento con la comunicación de la notificación en el servidor documental, e
     * inserta el respectivo registro en la tabla Documento
     *
     * @return
     */
    /*
     * @modified felipe.cadena::15-07-2015::Se agrega funcionalidad para edictos con direccion
     */
    private boolean guardarDocumentoEdicto(ReporteDTO reporte, String radicado) {

        boolean answer = true;
        Documento tempDocumento;
        TipoDocumento tipoDoc;
        Long tipoDocumentoId;
        Date fechaLogYRadicacion;

        tipoDocumentoId =
            ETipoDocumento.EDICTO_TRAMITES_CATASTRALES_REVISION_DE_AVALUO_AUTOAVALUO_Y_REPOSICION.
                getId();
        if (this.currentTramite.getSolicitud().isViaGubernativa()) {
            tipoDocumentoId = ETipoDocumento.EDICTO_QUEJA_APELACION_Y_REVOCATORIA_DIRECTA.getId();
        }
        try {
            tipoDoc =
                this.getGeneralesService().buscarTipoDocumentoPorId(tipoDocumentoId);
        } catch (Exception ex) {
            LOGGER.error("error buscando TipoDocumento con id " + tipoDocumentoId + " : " + ex.
                getMessage(), ex);
            return false;
        }

        tempDocumento = new Documento();
        fechaLogYRadicacion = new Date(System.currentTimeMillis());

        //D: se arma el objeto Documento con todos los atributos posibles...
        //D: el método que guarda el documento en el gestor documental recibe el nombre del archivo
        //   sin la ruta
        if (reporte == null) {
            if (this.reporteNotificacionPorEdicto != null) {
                tempDocumento.setArchivo(this.reporteNotificacionPorEdicto.
                    getRutaCargarReporteAAlfresco());
            }
        } else {
            tempDocumento.setArchivo(reporte.getRutaCargarReporteAAlfresco());
        }
        tempDocumento.setTramiteId(this.currentTramiteId);
        tempDocumento.setTipoDocumento(tipoDoc);
        tempDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        tempDocumento.setBloqueado(ESiNo.NO.getCodigo());
        tempDocumento.setUsuarioCreador(this.currentUser.getLogin());
        tempDocumento.setEstructuraOrganizacionalCod(
            this.currentUser.getCodigoEstructuraOrganizacional());
        tempDocumento.setFechaRadicacion(fechaLogYRadicacion);

        if (radicado != null) {
            tempDocumento.setNumeroRadicacion(radicado);
        }

        tempDocumento.setFechaLog(fechaLogYRadicacion);
        tempDocumento.setUsuarioLog(this.currentUser.getLogin());
        tempDocumento.setDescripcion("Documento de edicto de notificación generado por el sistema");

        try {
            //D: se guarda el documento
            this.documentoEdicto =
                this.getTramiteService().guardarDocumento(this.currentUser, tempDocumento);
        } catch (Exception ex) {
            LOGGER.error("error guardando Documento : " + ex.getMessage(), ex);
            return false;
        }

        return answer;
    }

    /**
     * Determina si todos los solicitantes ya fueron notificados
     *
     * @author felipe.cadena
     *
     * @return -- si todos fueron notificados retorna la fecha del ultimo notificado, null de otra
     * manera
     */
    public Date determinarNotificacionSolicitantes() {

        Date maxFecha = null;

        if (this.solicitantesNotificacionPorEdicto != null &&
             !this.solicitantesNotificacionPorEdicto.isEmpty() &&
             this.solicitantesNotificacionPorEdicto.get(0).getDocumentoNotificacion().getId() !=
            null &&
             this.solicitantesNotificacionPorEdicto.get(0).getDocumentoNotificacion().
                getFechaDesfijacionEdicto() != null) {
            maxFecha = this.solicitantesNotificacionPorEdicto.get(0).getDocumentoNotificacion().
                getFechaDesfijacionEdicto();

        } else {
            return null;
        }

        for (Solicitante solicitante : this.solicitantesNotificacionPorEdicto) {

            if (solicitante.getDocumentoNotificacion().getId() != null &&
                 solicitante.getDocumentoNotificacion().getFechaDesfijacionEdicto() != null) {
                if (maxFecha.before(solicitante.getDocumentoNotificacion().
                    getFechaDesfijacionEdicto())) {
                    maxFecha = solicitante.getDocumentoNotificacion().getFechaDesfijacionEdicto();
                }
            } else {
                return null;
            }
        }
        return maxFecha;
    }

    /**
     * Valida que el usuario que vera el documento tenga un documento asociado
     *
     * @author felipe.cadena
     */
    public void validarVerEdicto() {
        if (this.solicitantesSelecionados == null || this.solicitantesSelecionados.length > 1) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_003.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_003.getMensajeTecnico());
            return;
        }
        if (this.solicitantesSelecionados[0].getDocumentoNotificacion().getId() == null) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_008.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_008.getMensajeTecnico());
            return;
        }

        this.reporteNotificacionPorEdicto = this.reportsService.
            consultarReporteDeGestorDocumental(this.solicitantesSelecionados[0].
                getDocumentoNotificacion().getIdRepositorioDocumentos());

    }

    /**
     * Valida que el usuario que vera el documento tenga un documento asociado
     *
     * @author felipe.cadena
     */
    public void validarRegistroEdicto() {

        if (this.solicitantesSelecionados == null || this.solicitantesSelecionados.length == 0) {
            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_005.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_005.getMensajeTecnico());
            return;
        }
        for (Solicitante solicitante : this.solicitantesSelecionados) {
            if (solicitante.getNotificadoPorDireccion().equals(ESiNo.NO.getCodigo())) {
                this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_006.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_006.getMensajeTecnico());
            }
            if (solicitante.getDocumentoNotificacion().getFechaDesfijacionEdicto() != null) {
                this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_007.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_007.getMensajeTecnico());
            }
        }
    }

    /**
     * Valida que no se generen documentos de solicitantes a los cuales ya se les ha generado uno
     * previamente
     *
     * @author felipe.cadena
     */
    public void validarGeneracionEdicto() {

        for (Solicitante solicitante : this.solicitantesSelecionados) {

            if (solicitante.getDocumentoNotificacion().getFechaFijacionEdicto() != null) {
                this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_004.toString());
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_004.getMensajeTecnico());
                return;
            }

        }

        //felipe.cadena::#15024::03-05-2016::Se valida la fecha de fijacion solo si se va a generar un nuevo aviso
        if (this.fechaFijacionEdicto == null || "".equals(this.fechaFijacionEdicto)) {
            this.addMensajeError(ECodigoErrorVista.NOTIFICACION_AVISO_010.toString());
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

    }

    /**
     * action del botón "Aceptar" de la pantalla de registro de aviso
     *
     * @author felipe.cadena
     */
    public void registrarComunicacionNotificacion() {

        for (Solicitante solicitanteSeleccionado : this.solicitantesSelecionados) {

            TramiteDocumento tramiteDocumentoARegistrar = null;

            if (solicitanteSeleccionado.getDocumentoNotificacion().getFechaDesfijacionEdicto() !=
                null) {

                this.addMensajeInfo(
                    "Ya se había registrado la comunicación de notificación para el solicitante seleccionado");
                return;

            }

            tramiteDocumentoARegistrar = solicitanteSeleccionado.getDocumentoNotificacion();
            //@modified by leidy.gonzalez:: 18401:: 31/07/2016:: Se aumenta un dia a la fecha de fijacion
            // almacenada para los documentos d ecomunicacion con direccion.
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());

            cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 12);

            this.fechaFijacionEdicto = cal.getTime();

            tramiteDocumentoARegistrar
                .setFechaDesfijacionEdicto(this.fechaFijacionEdicto);
            tramiteDocumentoARegistrar
                .setMetodoNotificacion(this.medioComunicNotif);
            tramiteDocumentoARegistrar
                .setObservacionNotificacion(tramiteDocumentoARegistrar
                    .getObservacionNotificacion() + ": " + this.observacionesComunicNotif);

            tramiteDocumentoARegistrar = this.getTramiteService()
                .actualizarTramiteDocumento(tramiteDocumentoARegistrar);

            //D: mover el proceso
            Date fechaNotificacion = this.determinarNotificacionSolicitantes();
            if (fechaNotificacion != null) {
                LOGGER.debug("Todos los solicitantes han sido notificados");
                this.forwardThisProcess();

                // felipe.cadena::21-09-2015::Si todos los solicitantes
                // se notificaron con direccion se cambia la fecha de avance a
                // solo un día
                if (this.determinarNotificacionPorDireccion()) {
                    this.asignarFechaAvanceAviso();
                }
            }
        }

        this.observacionesComunicNotif = "";
    }

    /**
     * Determina si todos los solicitantes fueron notificados con direccion para determinar el
     * tiempo de espera de la actividad
     *
     * @author felipe.cadena
     *
     * @return
     */
    public Boolean determinarNotificacionPorDireccion() {

        Boolean resultado = false;

        if (this.solicitantesNotificacionPorEdicto != null) {
            for (Solicitante solicitante : this.solicitantesNotificacionPorEdicto) {

                //si alguno de los solicitantes no se notifica con direccion se determina que todo el tramite no va por direccion
                if (ESiNo.NO.getCodigo().equals(solicitante.getNotificadoPorDireccion())) {
                    return false;
                }
            }

            //Todos los solicitants fueron notificados con direccion
            return true;
        }

        return resultado;

    }

    /**
     * Metodo para cambiar el tiempo de espera para los tramites que fueron notificados con
     * direccion, en este caso es solo de un dia. Normalmente el tiempo de es de 5 dias
     *
     * @author felipe.cadena
     *
     *
     */
    public void asignarFechaAvanceAviso() {

        try {
            List<Actividad> acts = this.getProcesosService().getActividadesPorIdObjetoNegocio(
                this.currentTramite.getId());
            Actividad act = null;

            if (acts != null && !acts.isEmpty() && acts.get(0) != null) {
                act = acts.get(0);
            }

            if (act != null) {
                //TODO::felipe.cadena::invocar metodo process para cambiar fecha en que caduca la actividad
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, 1);
                this.getProcesosService().replanificarActividad(act.getId(), c,
                    "Notificacion por dirección");
            } else {
                this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_008.toString());
                LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_008.getMensajeTecnico());
            }
        } catch (Exception ex) {

            this.addMensajeWarn(ECodigoErrorVista.NOTIFICACION_AVISO_008.toString());
            LOGGER.error(ECodigoErrorVista.NOTIFICACION_AVISO_008.getMensajeTecnico() + "EX: " + ex.
                getMessage());
        }

    }

    //--------------------------------------------------------------------------------------------------
    //----------------   BPM  ---------------------
    /**
     * solo hay que validar que se haya generado y guardado el doc del edicto
     */
    @Implement
    @Override
    public boolean validateProcess() {
        boolean answer = this.edictoGuardado;
        return answer;
    }
//------------------------------------------------------------------------------

    /**
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO messageDTO;
        String observaciones = "", processTransition = "";

        // IMPORTANTE:
        // Sin importar que transición o usuario se pase, el process en este punto
        // lo moverá automaticamente a la actividad ACT_VALIDACION_REGISTRAR_AVISO del usuario
        // "snc_tiempos_muertos" cuando expire el tiempo definido.
        processTransition = ProcesoDeConservacion.ACT_VALIDACION_REGISTRAR_AVISO;
        observaciones = "";

        messageDTO =
            ProcesosUtilidadesWeb.setupIndividualProcessMessage(
                this.currentProcessActivity.getId(), processTransition, this.currentUser,
                observaciones);

        messageDTO.setUsuarioActual(this.currentUser);

        this.setMessage(messageDTO);

    }
//------------------------------------------------------------------------------

    /**
     * no hay que hacer actualizaciones de BD
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {

    }
//-------------------------------------------------------------------------------------

    private void forwardThisProcess() {
        this.validateProcess();
        this.setupProcessMessage();
        this.doDatabaseStatesUpdate();
        this.forwardProcess();
    }

//end of class
}
