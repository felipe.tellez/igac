package co.gov.igac.snc.web.mb.avaluos.controlCalidad;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EPlantilla;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EDatosBaseCorreoElectronico;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 *
 * Managed bean del CU-SA-AC-039 Generar Oficio de NO aprobación de Avalúo.
 *
 *
 * @author christian.rodriguez
 * @cu CU-SA-AC-039
 */
@Component("generarOficioDeNoAprobacionAvaluo")
@Scope("session")
public class GeneracionOficioDeNoAprobacionAvaluoMB extends SNCManagedBean {

    private static final long serialVersionUID = 604549531386297112L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(GeneracionOficioDeNoAprobacionAvaluoMB.class);

    // --------------------------------Atributos--------------------------------
    /**
     * Avaluo seleccionado del cual se va a generar el oficio
     */
    private Avaluo avaluo;

    /**
     * Fecha en que se generó o modificó el documento de NO aprobación de avalúo
     */
    private Date fechaReporte;

    /**
     * la territorial donde se está realizando el avalúo.
     */
    private EstructuraOrganizacional territorialAvaluo;

    /**
     * nombre de la persona que está ejerciendo como director Territorial, en el momento de generar
     * el documento, de la territorial donde se está realizando el avalúo
     */
    private UsuarioDTO directorTerritorial;

    /**
     * Revisión de control de calidad de la cual se quiere generar el oficio
     */
    private ControlCalidadAvaluoRev revision;

    /**
     * Profesional que está realizando el control de calidad del avalúo
     */
    private ProfesionalAvaluo profesionalCalidad;

    /**
     * Coordinador GIT de avalúos de la territorial en la que se realizó el avaluo
     */
    private UsuarioDTO coordinadorGITAvaluos;

    /**
     * Observaciones adicionales del informe de no aprobación
     */
    private String observacionesAdicionales;

    /**
     * Usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    // --------------------------------Reportes---------------------------------
    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte del oficio de no aprobación
     */
    private ReporteDTO oficioNoAprobacionReporte;

    // --------------------------------Banderas---------------------------------
    // -------------------------- Variables Visor GIS --------------------------
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    @Autowired
    protected IContextListener contextoWeb;

    // ----------------------------Métodos SET y GET----------------------------
    public Avaluo getAvaluo() {
        return this.avaluo;
    }

    public Date getFechaReporte() {
        return this.fechaReporte;
    }

    public EstructuraOrganizacional getTerritorialAvaluo() {
        return this.territorialAvaluo;
    }

    public UsuarioDTO getDirectorTerritorial() {
        return this.directorTerritorial;
    }

    public ControlCalidadAvaluoRev getRevision() {
        return this.revision;
    }

    public ProfesionalAvaluo getProfesionalCalidad() {
        return this.profesionalCalidad;
    }

    public UsuarioDTO getCoordinadorGITAvaluos() {
        return this.coordinadorGITAvaluos;
    }

    public String getObservacionesAdicionales() {
        return this.observacionesAdicionales;
    }

    public void setObservacionesAdicionales(String observacionesAdicionales) {
        this.observacionesAdicionales = observacionesAdicionales;
    }

    public String getApplicationClientName() {
        return this.applicationClientName;
    }

    public String getModuleLayer() {
        return this.moduleLayer;
    }

    public String getModuleTools() {
        return this.moduleTools;
    }

    // -----------------------Métodos SET y GET reportes------------------------
    public ReporteDTO getOficioNoAprobacionReporte() {
        return this.oficioNoAprobacionReporte;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        this.extraerDatosProcess();
    }

    // -------------------------------------------------------------------------
    /**
     * Método que en un futuro deberá leer los datos del arbol del proceso y extrae el la
     * información de la revisión de control calidad
     *
     * @author christian.rodriguez
     */
    private void extraerDatosProcess() {

        this.inicializarDatosQuemados();
    }

    private void inicializarDatosQuemados() {
        this.cargarDatos(1L);
    }

    // -------------------------------------------------------------------------
    /**
     * Carga los datos necesarios por este caso de uso
     *
     * @author christian.rodriguez
     * @param idControlCalidadAvaluoRevision Identificador de la {@link ControlCalidadAvaluoRev}
     */
    private void cargarDatos(Long idControlCalidadAvaluoRevision) {
        if (idControlCalidadAvaluoRevision != null) {

            this.revision = this.getAvaluosService()
                .obtenerControlCalidadAvaluoRevPorId(
                    idControlCalidadAvaluoRevision);

            this.avaluo = this.getAvaluosService()
                .obtenerAvaluoPorIdConAtributos(
                    this.revision.getControlCalidadAvaluo()
                        .getAvaluoId());

            this.territorialAvaluo = this.getGeneralesService()
                .buscarEstructuraOrganizacionalPorCodigo(
                    String.valueOf(this.avaluo
                        .getEstructuraOrganizacionalCod()));

            this.profesionalCalidad = this.getAvaluosService()
                .obtenerProfesionalAvaluoPorId(
                    this.revision.getControlCalidadAvaluo()
                        .getProfesionalAvaluos().getId());

            List<UsuarioDTO> directoresTerritorial = this.getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    this.territorialAvaluo.getNombre(),
                    ERol.DIRECTOR_TERRITORIAL);

            if (!directoresTerritorial.isEmpty()) {
                this.directorTerritorial = directoresTerritorial.get(0);
            }

            List<UsuarioDTO> coordinadoresGITAvaluos = this
                .getTramiteService()
                .buscarFuncionariosPorRolYTerritorial(
                    Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                    ERol.COORDINADOR_GIT_AVALUOS);

            if (!coordinadoresGITAvaluos.isEmpty()) {
                this.coordinadorGITAvaluos = coordinadoresGITAvaluos.get(0);
            }

            this.fechaReporte = new Date();

            this.inicializarVariablesVisorGIS();

            this.usuario = MenuMB.getMenu().getUsuarioDto();

        }
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que genera el oficio o lo consulta si ya existe
     *
     * @author christian.rodriguez
     */
    public void generarOficio() {

        Documento oficioDocumento = this.getAvaluosService()
            .consultarOficioDeNoAprobacionAvaluoPorIdAvaluo(
                this.avaluo.getId());

        if (oficioDocumento == null) {
            if (!this.generarReporte()) {
                this.addMensajeError("No se pudo generar el oficio de no aprobación");
                return;
            }

            oficioDocumento = this.guardarDocumento();
            if (oficioDocumento == null) {
                this.addMensajeError("No se pudo guardar el oficio de no aprobación");
                return;
            }

        }

        this.cargarOficioDeNoAprobacion(oficioDocumento.getId());
    }

    // -------------------------------------------------------------------------
    /**
     * Genera el {@link Documento} y el {@link TramiteDocumento} asociados al oficio y los guarda en
     * al BD
     *
     * @author christian.rodriguez
     */
    private Documento guardarDocumento() {

        TipoDocumento tipoDocumentoOP = new TipoDocumento();
        tipoDocumentoOP.setId(ETipoDocumento.AVALUOS_OFICIO_NO_APROBACION
            .getId());
        tipoDocumentoOP.setNombre(ETipoDocumento.AVALUOS_OFICIO_NO_APROBACION
            .getNombre());

        Documento nuevoDocumento = new Documento();
        nuevoDocumento.setArchivo(this.getOficioNoAprobacionReporte()
            .getNombreReporte());
        nuevoDocumento.setTipoDocumento(tipoDocumentoOP);
        nuevoDocumento.setEstado(EDocumentoEstado.CARGADO.getCodigo());
        nuevoDocumento.setBloqueado(ESiNo.NO.getCodigo());
        nuevoDocumento.setUsuarioCreador(this.usuario.getLogin());
        nuevoDocumento.setEstructuraOrganizacionalCod(this.usuario
            .getCodigoEstructuraOrganizacional());
        nuevoDocumento.setFechaLog(new Date(System.currentTimeMillis()));
        nuevoDocumento.setUsuarioLog(this.usuario.getLogin());
        nuevoDocumento.setDescripcion("Oficio de no aprobación del avalúo " +
             this.avaluo.getSecRadicado());

        nuevoDocumento = this.getGeneralesService().guardarDocumento(
            nuevoDocumento);

        nuevoDocumento = this.guardarDocumentoEnGestorDocumental(
            nuevoDocumento, tipoDocumentoOP);

        if (nuevoDocumento != null) {
            nuevoDocumento = this.getGeneralesService().guardarDocumento(
                nuevoDocumento);
        }

        TramiteDocumento trDocumento = new TramiteDocumento();
        trDocumento.setDocumento(nuevoDocumento);
        trDocumento.setTramite(this.avaluo.getTramite());
        trDocumento.setFechaLog(new Date());
        trDocumento.setfecha(new Date());
        trDocumento.setUsuarioLog(this.usuario.getLogin());

        trDocumento = this.getTramiteService().actualizarTramiteDocumento(
            trDocumento);

        this.revision.setSoporteDocumento(nuevoDocumento);
        this.revision = this.getAvaluosService()
            .guardarActualizarControlCalidadAValuoRev(this.revision,
                this.usuario);

        return nuevoDocumento;

    }

    // -------------------------------------------------------------------------
    /**
     * Método para guardar un documento de avalúos en el gestor de documentos
     *
     * @author christian.rodriguez
     * @param documento {@link Documento} a guardar
     * @param tipoDocumento {@link TipoDocumento} del documento a guardar
     * @return {@link Documento} guardado con el id del gestor de documentos
     */
    private Documento guardarDocumentoEnGestorDocumental(Documento documento,
        TipoDocumento tipoDocumento) {

        Solicitud solTemp = this.avaluo.getTramite().getSolicitud();

        DocumentoSolicitudDTO datosGestorDocumental = DocumentoSolicitudDTO
            .crearArgumentoCargarSolicitudAvaluoComercial(solTemp
                .getNumero(), solTemp.getFecha(), tipoDocumento
                .getNombre(), this.oficioNoAprobacionReporte
                    .getRutaCargarReporteAAlfresco());

        documento = this.getGeneralesService()
            .guardarDocumentosSolicitudAvaluoAlfresco(this.usuario,
                datosGestorDocumental, documento);

        if (documento != null && documento.getIdRepositorioDocumentos() == null) {
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }
        return documento;

    }

    // -------------------------------------------------------------------------
    /**
     * Método que genera el reporte del oficio de no aprobación
     *
     * @author christian.rodriguez
     */
    private boolean generarReporte() {

        boolean reporteGenerado = false;

        // TODO::christian.rodriguez :: Cambiar la url del reporte cuando esté
        // definido
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

        // TODO::christian.rodriguez :: Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_RADICADO", "");

        this.oficioNoAprobacionReporte = this.reportsService.generarReporte(
            parameters, enumeracionReporte, this.usuario);

        reporteGenerado = true;

        return reporteGenerado;

    }

    // ----------------------------------------------------------------------
    /**
     * Método que se encarga de recuperar el oficio de no aprobación del avaluo seleccionado para
     * ser visualizado
     *
     * @author christian.rodriguez
     */
    private void cargarOficioDeNoAprobacion(Long idDocumento) {

        if (this.avaluo != null) {

            Documento ordenPracticaDocumento = this.getGeneralesService()
                .buscarDocumentoPorId(idDocumento);

            this.oficioNoAprobacionReporte = this.reportsService
                .consultarReporteDeGestorDocumental(ordenPracticaDocumento
                    .getIdRepositorioDocumentos());

            if (this.oficioNoAprobacionReporte == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "El oficio de no aprobación no pudo ser cargado.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        }
    }

    // -------------------------------------------------------------------------
    /**
     * Listener que envia el oficio
     *
     * @author christian.rodriguez
     */
    public void enviarOficio() {

        // Generación de destinatarios
        List<String> destinatarios = new ArrayList<String>();
        List<String> destinatariosCC = new ArrayList<String>();

        String correoAvaluador = this.profesionalCalidad.getCorreoElectronico();

        UsuarioDTO usuarioDirectorTerritorial = this
            .getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(
                this.territorialAvaluo.getNombre(),
                ERol.DIRECTOR_TERRITORIAL).get(0);
        UsuarioDTO usuarioCoordinadorGITAvaluos = this
            .getGeneralesService()
            .obtenerFuncionarioTerritorialYRol(
                Constantes.NOMBRE_LDAP_ESTRUCTURA_ORGANIZACIONAL_SEDE_CENTRAL,
                ERol.COORDINADOR_GIT_AVALUOS).get(0);

        destinatarios.add(correoAvaluador);
        destinatariosCC.add(usuarioDirectorTerritorial.getLogin().concat(
            EDatosBaseCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC
                .getDato()));
        destinatariosCC.add(usuarioCoordinadorGITAvaluos.getLogin().concat(
            EDatosBaseCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC
                .getDato()));
        destinatariosCC.add("christian.rodriguez@igac.gov.co");

        // Generación de los datos del cuerpo del correo
        Object[] datosCorreo = new Object[2];
        datosCorreo[0] = this.usuario.getNombreCompleto();
        datosCorreo[1] = this.avaluo.getSecRadicado();

        // Generación de adjuntos
        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};

        this.generarOficio();

        adjuntos[0] = this.oficioNoAprobacionReporte.getArchivoReporte()
            .getAbsolutePath();
        adjuntosNombres[0] = this.oficioNoAprobacionReporte.getArchivoReporte()
            .getName();

        // Envio correo
        boolean notificacionExitosa = this.getGeneralesService()
            .enviarCorreoElectronicoConCuerpo(
                EPlantilla.MENSAJE_COMUNICACION_NO_APROBACION_AVALUO,
                destinatarios, destinatariosCC, null, datosCorreo,
                adjuntos, adjuntosNombres);

        if (notificacionExitosa) {
            this.addMensajeInfo(
                "Se envió exitosamente el oficio de no aprobación por correo electrónico.");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            this.addMensajeError(
                "No se pudo enviar el oficio de no aprobación por correo electrónico.");
            context.addCallbackParam("error", "error");
        }

    }

}
