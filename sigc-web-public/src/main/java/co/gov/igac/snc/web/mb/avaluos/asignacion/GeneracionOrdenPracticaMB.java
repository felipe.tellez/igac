/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.avaluos.asignacion;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.DocumentoSolicitudDTO;
import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.EOrdenPracticaEstado;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.constantes.ConstantesComunicacionesCorreoElectronico;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * MB para el caso de uso Generar orden de práctica de avalúo
 *
 *
 * @cu CU-SA-AC-009
 * @cu CU-SA-AC-167
 * @author pedro.garcia
 * @modified christian.rodriguez
 */
@Component("generacionOrdenPractica")
@Scope("session")
public class GeneracionOrdenPracticaMB extends SNCManagedBean {

    private static final long serialVersionUID = 273439640577161317L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneracionOrdenPracticaMB.class);

    //-----------  MB  ----
    /**
     * Trámites de avalúo seleccionados de la tabla de trámites disponibles para incluir en una
     * orden de práctica. Son los que van a hacer parte de la orden de práctica que se genere, y que
     * se muestran en la ventana de información de orden de practica.
     */
    private Avaluo[] selectedAvaluosForOP;

    /**
     * guarda el avalúo que se 'selecciona' de la tabla, para ver la información predial
     */
    private Avaluo selectedAvaluo;

    /**
     * variable donde se cargan los resultados paginados de la consulta de avalúos a los que ya se
     * les ha asignado ejecutor y están disponibles para incluirse en una orden de práctica
     */
    private LazyDataModel<Avaluo> lazyWorkingAvaluos;

    /**
     * lista de avaluadores de los tramites seleccionados para la orden de práctica
     */
    private ArrayList<ProfesionalAvaluo> avaluadoresTramitesOP;

    /**
     * contrato interadministrativo de los trámite seleccionados, del cual se muestran los datos en
     * el encabezado de la página
     */
    private ContratoInteradministrativo contratoInteradministrativoAvaluos;

    /**
     * lista donde se almacenan los id de los avaluos seleccionados para hacer parte de la orden de
     * práctica. Se usa para actualizar las entradas de la tabla Avaluo_Asignacion_Profesional con
     * la orden de practica que se crea, y que los agrupa.
     */
    private ArrayList<Long> avaluosOrdenPractica;

    /**
     * Solicitud a la que pertenecen los trámites de avaluó de la orden de práctica (una de las
     * condiciones es que todos los avalúos pertenezacan a la misma Solicitud)
     */
    private Solicitud solicitudTramitesAvaluo;

    /**
     * territorial a la que pertenece el usuario que tiene la sesión
     */
    private EstructuraOrganizacional territorialUsuario;

    /**
     * Ordenes de práctica pendientes por enviar (en estado {@link EOrdenPracticaEstado#GENERADA})
     */
    private List<OrdenPractica> ordenesPracticaPorEnviar;

    /**
     * Lista de ordenes de practica selecionadas para envio
     */
    private OrdenPractica[] ordenesPracticaSeleccionadas;

    /**
     * ID del tab de la página de enviar ordenes pendientes. se usa para cuando se ejecuta el
     * listener del cambio de tabs {@link #onTabChange(TabChangeEvent)}
     */
    private final String TAB_ENVIO_ID = "enviarOrdenesPracticaTAB";

    /**
     * Variable que almacenara la orden de práctica seleccionada para visualización o edición
     */
    private OrdenPractica ordenPracticaSeleccionada;

    //--------   flags  ----
    /**
     * indica si se deben mostrar los datos de contrato en el encabezado (si existe contrato)
     */
    private boolean mostrarDatosContrato;

    /**
     * indica si ya fue generada la orden de práctica
     */
    private boolean ordenPracticaGenerada;

    /**
     * indica si la orden de práctica se está creando o modificando para permitir el ingreso de
     * datos
     */
    private boolean creandoOModificandoOP;

    /**
     * indica si la orden de práctica fue enviada por correo a quienes corresponde
     */
    private boolean ordenPracticaEnviada;

    /**
     * indica si ya se generó por primera vez el Documento con la orden de práctica
     */
    private boolean ordenPracticaGeneradaPrimeraVez;

    //----------------------  BPM   -----------------------
    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    //---------
    //----------------    reportes   ------
    private ReportesUtil reportesService = ReportesUtil.getInstance();

    /**
     * DTO con los datos del reporte de la orden de practica
     */
    private ReporteDTO ordenPracticaReporteDTO;

    //-------------
    /**
     * usuario que tiene la sesión
     */
    private UsuarioDTO loggedInUser;

//--------------- getters and setters  ------------------------------
    public boolean isOrdenPracticaGenerada() {
        return this.ordenPracticaGenerada;
    }

    public void setOrdenPracticaGenerada(boolean ordenPracticaGenerada) {
        this.ordenPracticaGenerada = ordenPracticaGenerada;
    }

    public boolean isCreandoOModificandoOP() {
        return this.creandoOModificandoOP;
    }

    public void setCreandoOModificandoOP(boolean creandoOModificandoOP) {
        this.creandoOModificandoOP = creandoOModificandoOP;
    }

    public boolean isOrdenPracticaEnviada() {
        return this.ordenPracticaEnviada;
    }

    public void setOrdenPracticaEnviada(boolean ordenPracticaEnviada) {
        this.ordenPracticaEnviada = ordenPracticaEnviada;
    }

    public ContratoInteradministrativo getContratoInteradministrativoAvaluos() {
        return this.contratoInteradministrativoAvaluos;
    }

    public void setContratoInteradministrativoAvaluos(
        ContratoInteradministrativo contratoInteradminHeader) {
        this.contratoInteradministrativoAvaluos = contratoInteradminHeader;
    }

    public boolean isMostrarDatosContrato() {
        return this.mostrarDatosContrato;
    }

    public void setMostrarDatosContrato(boolean mostrarDatosContrato) {
        this.mostrarDatosContrato = mostrarDatosContrato;
    }

    public ArrayList<ProfesionalAvaluo> getAvaluadoresTramitesOP() {
        return this.avaluadoresTramitesOP;
    }

    public void setAvaluadoresTramitesOP(ArrayList<ProfesionalAvaluo> avaluadoresTramitesOP) {
        this.avaluadoresTramitesOP = avaluadoresTramitesOP;
    }

    public void setLazyWorkingAvaluos(LazyDataModel<Avaluo> lazyWorkingAvaluos) {
        this.lazyWorkingAvaluos = lazyWorkingAvaluos;
    }

    public LazyDataModel<Avaluo> getLazyWorkingAvaluos() {
        return this.lazyWorkingAvaluos;
    }

    public Avaluo getSelectedAvaluo() {
        return this.selectedAvaluo;
    }

    public void setSelectedAvaluo(Avaluo selectedAvaluo) {
        this.selectedAvaluo = selectedAvaluo;
    }

    public Avaluo[] getSelectedAvaluosForOP() {
        return this.selectedAvaluosForOP;
    }

    public void setSelectedAvaluosForOP(Avaluo[] selectedAvaluosForOP) {
        this.selectedAvaluosForOP = selectedAvaluosForOP;
    }

    public List<OrdenPractica> getOrdenesPracticaPorEnviar() {
        return this.ordenesPracticaPorEnviar;
    }

    public OrdenPractica[] getOrdenesPracticaSeleccionadas() {
        return this.ordenesPracticaSeleccionadas;
    }

    public void setOrdenesPracticaSeleccionadas(
        OrdenPractica[] ordenesPracticaSeleccionadas) {
        this.ordenesPracticaSeleccionadas = ordenesPracticaSeleccionadas;
    }

    public ReporteDTO getOrdenPracticaReporteDTO() {
        return this.ordenPracticaReporteDTO;
    }

    public OrdenPractica getOrdenPracticaSeleccionada() {
        return this.ordenPracticaSeleccionada;
    }

    public void setOrdenPracticaSeleccionada(OrdenPractica ordenPracticaSeleccionada) {
        this.ordenPracticaSeleccionada = ordenPracticaSeleccionada;
    }

    public EstructuraOrganizacional getTerritorialUsuario() {
        return this.territorialUsuario;
    }
//--------------   methods   ----------------------------

    //--------------------------------------------------------------------------------------------------
    /**
     * constructor del mb
     *
     * @author pedro.garcia
     * @version 2.0
     */
    public GeneracionOrdenPracticaMB() {

        this.lazyWorkingAvaluos = new LazyDataModel<Avaluo>() {

            @Override
            public List<Avaluo> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<Avaluo> answer = new ArrayList<Avaluo>();

                populateAvaluosLazyly(answer, first, pageSize, sortField, sortField, filters);

                return answer;
            }
        };
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {

        LOGGER.debug("init de GeneracionOrdenPracticaMB");

        this.loggedInUser = MenuMB.getMenu().getUsuarioDto();

        // D: se obtienen los ids de los trámites objetivo a partir del árbol de tareas
//		this.idsTramitesActividades = this.tareasPendientesMB
//				.obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();
//FIXME :: pedro.garcia :: datos quemados para pruebas. Uncomment the line obtaining ids from BPM
        this.idsTramitesActividades = new long[9];
        this.idsTramitesActividades[0] = 43470l;
        this.idsTramitesActividades[1] = 43439l;
        this.idsTramitesActividades[2] = 43327l;
        this.idsTramitesActividades[3] = 43322l;
        this.idsTramitesActividades[4] = 43318l;
        this.idsTramitesActividades[5] = 43286l;
        this.idsTramitesActividades[6] = 43268l;
        this.idsTramitesActividades[7] = 43267l;
        this.idsTramitesActividades[8] = 43086l;

        this.avaluosOrdenPractica = new ArrayList<Long>();
        this.territorialUsuario = this.getGeneralesService().
            buscarEstructuraOrganizacionalPorCodigo(
                this.loggedInUser.getCodigoEstructuraOrganizacional());

//TODO :: snc.avaluos :: ojo
//OJO: posiblemente esto deba cambiar cuando se haga el CU 200:
//     si se debe mantener un estado para las ordenes no enviadas se debe hacer algo como lo que se hace
//     en AsignacionProfesionalAvaluos
        List<Avaluo> rows;
        rows = this.getAvaluosService().obtenerAvaluosPorIdsParaOrdenPractica(
            this.idsTramitesActividades, null, null, null);

        this.lazyWorkingAvaluos.setRowCount(rows.size());

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "Generar orden de práctica" en la pantalla principal del cu
     *
     * 1. Hace las validaciones necesarias sobre los trámites seleccionados. Si todo está bien,
     * genera un número de orden de práctica. Validaciones (de los trámites) 1.1. que correspondan a
     * la misma solicitud 1.2. que correspondan al mismo solicitante 1.3. que tengan a los mismos
     * avaluadores asignados 1.4. que correspondan al mismo contrato interadministrativo, si lo hay
     * 1.5. que todos tengan un valor positivo para el dato 'días de entrega' (en bd es 'plazo de
     * ejecución')
     *
     * 2. arma la lista de avaluadores de los avalúos seleccionados. También va llenando el hashmap
     * de ids de avaluos contra ids de avaluadores 3. obtiene al número de orden de práctica
     */
    public void generarOrdenPracticaInit() {

        String errorMessage;
        Long idSolicitudCurrent;
        String nombreSolicitanteCurrent;
        List<ProfesionalAvaluo> avaluadoresCurrent;
        ContratoInteradministrativo contratoInterCurrent, contratoInterTemp;
        Avaluo firstAvaluo;

        this.ordenPracticaSeleccionada = new OrdenPractica();

        this.ordenPracticaSeleccionada.setFecha(new Date(System.currentTimeMillis()));

        errorMessage = null;
        firstAvaluo = this.selectedAvaluosForOP[0];
        idSolicitudCurrent = firstAvaluo.getTramite().getSolicitud().getId();

        this.creandoOModificandoOP = true;
        this.ordenPracticaEnviada = false;

        //D: los trámites de avalúos tienen un solo solicitante
        nombreSolicitanteCurrent = firstAvaluo.getTramite().getSolicitanteTramites().get(0).
            getNombreCompleto();

        avaluadoresCurrent = firstAvaluo.getAvaluadores();

        contratoInterCurrent = this.getAvaluosService().
            obtenerContratoInteradministrativoPorAvaluo(firstAvaluo.getId());

        if (contratoInterCurrent != null) {
            this.contratoInteradministrativoAvaluos = contratoInterCurrent;
            this.mostrarDatosContrato = true;
        } else {
            this.mostrarDatosContrato = false;
        }

        //D: 1.
        this.solicitudTramitesAvaluo = firstAvaluo.getTramite().getSolicitud();
        for (Avaluo avaluo : this.selectedAvaluosForOP) {

            contratoInterTemp =
                this.getAvaluosService().obtenerContratoInteradministrativoPorAvaluo(avaluo.getId());

            //D: 1.1.
            if (this.solicitudTramitesAvaluo.getId().compareTo(idSolicitudCurrent) != 0) {
                errorMessage = "Los trámites seleccionados no corresponden a la misma solicitud";
                break;
            } //D: 1.2.
            else if (avaluo.getTramite().getSolicitanteTramites().get(0).getNombreCompleto().
                compareToIgnoreCase(nombreSolicitanteCurrent) != 0) {
                errorMessage = "Los trámites seleccionados no tienen el mismo solicitante";
                break;
            } //D: 1.3.
            else if (!CollectionUtils.isEqualCollection(avaluadoresCurrent, avaluo.getAvaluadores())) {
                errorMessage =
                    "Los avaluadores asignados a los contratos seleccionados no son los mismos";
                break;
            } //D: 1.4.
            //TODO :: snc.avaluos :: averiguar qué pasa cuando no hay contrato (ver doc 009:R.E 15)
            else if (contratoInterCurrent != null && contratoInterTemp != null &&
                !contratoInterCurrent.equals(contratoInterTemp)) {
                errorMessage =
                    "Los trámites seleccionados no pertenecen todos al mismo contrato interadministrativo";
                break;
            }

            //D: 1.5.
        }

        if (errorMessage != null && !errorMessage.isEmpty()) {
            this.addMensajeError(errorMessage);
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "pailander");
        } else {
            //D: 2.
            ArrayList<ProfesionalAvaluo> avaluadoresTramite;
            this.avaluadoresTramitesOP = new ArrayList<ProfesionalAvaluo>();
            for (Avaluo avaluo : this.selectedAvaluosForOP) {
                avaluadoresTramite = (ArrayList<ProfesionalAvaluo>) avaluo.getAvaluadores();
                for (ProfesionalAvaluo avaluador : avaluadoresTramite) {
                    if (!this.avaluadoresTramitesOP.contains(avaluador)) {
                        this.avaluadoresTramitesOP.add(avaluador);
                    }
                }
                this.avaluosOrdenPractica.add(avaluo.getId());
            }

            //D: 3.
            this.ordenPracticaSeleccionada.setNumero(this.getAvaluosService().
                obtenerNumeroOrdenPractica());
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del boton "aceptar" en la página principal del CU
     *
     * 1. generar el documento de la orden de práctica
     *
     * 2. armar y guardar el objeto OrdenPractica. Si ya se había generado, solo se actualiza con el
     * nuevo id de documento 3. Si es creación -no modificación- de la orden de práctica, se
     * actualizan las filas de la tabla Avaluo_Asignacion_Profesional para actualizar el id de la
     * orden de práctica
     */
    public void generarOrdenPractica() {

        boolean aapUpdate;
        Long idOrdenPracticaIoU;

        RequestContext context = RequestContext.getCurrentInstance();

        //D: 1.
        if (!generarReporteOrdenPractica()) {
            this.addMensajeError("No se pudo generar el documento con la orden de práctica.");
            return;
        }

        Documento documentoGuardado = this.generarDocumentoOrdenPracticaDB();
        if (documentoGuardado == null) {
            return;
        }

        if (documentoGuardado.getIdRepositorioDocumentos() != null) {
            this.getGeneralesService().borrarDocumentoEnAlfresco(
                documentoGuardado.getIdRepositorioDocumentos());
        }

        documentoGuardado = this.guardarOrdenPracticaEnGestorDocumental(
            documentoGuardado);

        //D. 2.
        if (!this.ordenPracticaGeneradaPrimeraVez) {
            this.ordenPracticaSeleccionada.setUsuarioLog(this.loggedInUser.getLogin());
            this.ordenPracticaSeleccionada.setEstado(EOrdenPracticaEstado.GENERADA.getCodigo());
        }

        this.ordenPracticaSeleccionada.setDocumentoId(documentoGuardado.getId());
        this.ordenPracticaSeleccionada.setFechaLog(new Date(System.currentTimeMillis()));

        idOrdenPracticaIoU = this.getAvaluosService().
            guardarActualizarOrdenPractica(this.ordenPracticaSeleccionada);

        if (idOrdenPracticaIoU == null) {
            context.addCallbackParam("error", "error");
            this.addMensajeError("Ocurrió un error guardando el registro de la orden de práctica");
            return;
        } else {
            this.ordenPracticaSeleccionada.setId(idOrdenPracticaIoU);
        }

        //D: 3.
        if (!this.ordenPracticaGeneradaPrimeraVez) {
            aapUpdate = this.getAvaluosService().
                actualizarOrdenPracticaEnAvaluoAsignacionProfesional(this.ordenPracticaSeleccionada.
                    getNumero(),
                    this.avaluosOrdenPractica);
        } else {
            aapUpdate = true;
        }

        if (!aapUpdate) {
            context.addCallbackParam("error", "error");
            this.addMensajeError("Ocurrió un error actualizando los registros de " +
                "Avaluo_Asignacion_Profesional");
            return;
        }

        this.ordenPracticaGenerada = true;
        //D: este se pone en true la primera vez que se genera la orden de práctica, y nunca se 
        //   cambia de valor
        this.ordenPracticaGeneradaPrimeraVez = true;

        this.addMensajeInfo("Orden de práctica " +
             this.ordenPracticaSeleccionada.getNumero() + " guardada satisfactoriamente.");

        this.creandoOModificandoOP = false;

    }

    // -------------------------------------------------------------------------------------------
    /**
     * Método encargado de generar el objeto documento para la orden de práctica y guardarlo en la
     * db
     *
     * @author christian.rodriguez
     * @return el objeto creado o null si no se pudo completar la operación
     */
    private Documento generarDocumentoOrdenPracticaDB() {
        Documento documentoGuardado;

        if (!this.ordenPracticaGeneradaPrimeraVez) {

            TipoDocumento tipoDocumentoOP = new TipoDocumento();
            tipoDocumentoOP.setId(ETipoDocumento.MEMORANDO_DE_ORDEN_DE_PRACTICA
                .getId());
            tipoDocumentoOP
                .setNombre(ETipoDocumento.MEMORANDO_DE_ORDEN_DE_PRACTICA
                    .getNombre());

            documentoGuardado = new Documento();
            documentoGuardado.setArchivo(this.ordenPracticaReporteDTO
                .getNombreReporte());
            documentoGuardado.setTipoDocumento(tipoDocumentoOP);
            documentoGuardado.setEstado(EDocumentoEstado.CARGADO.getCodigo());
            documentoGuardado.setBloqueado(ESiNo.NO.getCodigo());
            documentoGuardado.setUsuarioCreador(this.loggedInUser.getLogin());
            documentoGuardado.setEstructuraOrganizacionalCod(this.loggedInUser
                .getCodigoEstructuraOrganizacional());
            documentoGuardado.setFechaLog(new Date(System.currentTimeMillis()));
            documentoGuardado.setUsuarioLog(this.loggedInUser.getLogin());
            documentoGuardado
                .setDescripcion("Memorando de orden de práctica generado por el sistema");

            documentoGuardado = this.getGeneralesService().guardarDocumento(
                documentoGuardado);

            documentoGuardado = this.getGeneralesService()
                .buscarDocumentoPorIdConTipoDocumento(
                    documentoGuardado.getId());

            if (documentoGuardado == null || documentoGuardado.getId() == null) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");
                this.addMensajeError("Ocurrió un error al guardar el documento en la base de datos");
                return null;
            }
        } else {
            documentoGuardado = this.getGeneralesService()
                .buscarDocumentoPorIdConTipoDocumento(
                    this.ordenPracticaSeleccionada.getDocumentoId());
        }
        return documentoGuardado;
    }

    //-------------------------------------------------------------------------------------------
    /**
     * Método encargado de guardar la orden de practica en alfresco
     *
     * @author pedro.garcia
     * @modified metodo extraido de {@link #generarOrdenPractica()} ya que se me ahcia complicado
     * entender todo en el mismo :(
     * @param documentoGuardado documento que se quiere guardar en el gestor documental
     * @return documento actualizado con el id del repositorio en alfresco o null si ocurrio algún
     * problema
     */
    private Documento guardarOrdenPracticaEnGestorDocumental(
        Documento documentoGuardado) {

//TODO :: snc.avaluos :: determinar si se debe guardar un TramiteDocumento
        Solicitud solTemp = this.selectedAvaluosForOP[0].getTramite().getSolicitud();

        if (this.selectedAvaluosForOP[0].getTramite().getSolicitud() == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("Error tratando de obtener la solicitud relacionada");
            return null;
        }

        DocumentoSolicitudDTO datosGestorDocumental =
            DocumentoSolicitudDTO.crearArgumentoCargarSolicitudAvaluoComercial(
                solTemp.getNumero(), solTemp.getFecha(), documentoGuardado.getTipoDocumento().
                getNombre(),
                this.ordenPracticaReporteDTO.getRutaCargarReporteAAlfresco());

//TODO :: snc.avaluos :: revisar este método: puede que no aplique para todos los documentos de avalúos
        //N: el método actualiza el Documento cambiando el id del documento en el gestor documental
        documentoGuardado = this.getGeneralesService().guardarDocumentosSolicitudAvaluoAlfresco(
            this.loggedInUser, datosGestorDocumental, documentoGuardado);

        if (documentoGuardado.getIdRepositorioDocumentos() == null) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            this.addMensajeError("No se pudo guardar el documento en el gestor documental");
        }

        return documentoGuardado;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * hace la búsqueda de trámites para ser incluidos en una orden de práctica
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     */
    private void populateAvaluosLazyly(List<Avaluo> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<Avaluo> rows;
        int size;

        rows = this.getAvaluosService().obtenerAvaluosPorIdsParaOrdenPractica(
            this.idsTramitesActividades, sortField, sortOrder, filters, first, pageSize);

        if (rows != null) {
            size = rows.size();

            for (int i = 0; i < size; i++) {
                answer.add(rows.get(i));
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * listener del evento de edición de una fila de la tabla de avalúos que hacen parte de la orden
     * de práctica. Actualiza el Avaluo que -posiblemente- se modificó
     *
     * @param event
     */
    public void editarAvaluoOPListener(RowEditEvent event) {

        Avaluo avaluoToUpdate = (Avaluo) event.getObject();
        this.getAvaluosService().actualizarAvaluo(avaluoToUpdate);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botópn "cancelar" de la pantalla con la info para la orden de práctica
     */
    public void cancelarGeneracionOP() {
        this.selectedAvaluosForOP = null;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Llama al servicio de reportes para generar el documento reporte de la orden de práctica
     *
     * @author pedro.garcia
     */
    private boolean generarReporteOrdenPractica() {

        LOGGER.debug("generarReporteOrdenPractica");

        boolean reporteGenerado = false;

//TODO :: snc.avaluos :: Reemplazar parámetros para el reporte
        EReporteServiceSNC enumeracionReporte = EReporteServiceSNC.REPORTE_PRUEBA;

//TODO :: snc.avaluos :: Reemplazar parametros para el reporte.
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("NUMERO_RADICADO", "");

        this.ordenPracticaReporteDTO = this.reportesService.generarReporte(
            parameters, enumeracionReporte, this.loggedInUser);

        reporteGenerado = true;

        return reporteGenerado;
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se ejecuta para cargar el documento asociado a la orden de práctica y
     * visualizarlo en el visualizador de memorandos
     *
     * @author christian.rodriguez
     */
    public void cargarDocumentoOrdenPractica() {
        if (this.ordenPracticaSeleccionada != null) {

            Documento documentoOrdenPractica = this.getGeneralesService()
                .buscarDocumentoPorId(
                    this.ordenPracticaSeleccionada.getDocumentoId());

            this.ordenPracticaReporteDTO = this.reportesService
                .consultarReporteDeGestorDocumental(
                    documentoOrdenPractica.getIdRepositorioDocumentos());

            if (this.ordenPracticaReporteDTO == null) {

                RequestContext context = RequestContext.getCurrentInstance();
                String mensaje = "La orden prática asociada no pudo ser cargada.";
                this.addMensajeError(mensaje);
                context.addCallbackParam("error", "error");
            }
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "modificar" en la página de información de la orden de práctica
     */
    public void modificarOrdenPracticaInit() {

        this.ordenPracticaSeleccionada.setFecha(new Date(System.currentTimeMillis()));
        this.ordenPracticaGenerada = false;
        this.creandoOModificandoOP = true;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Listener del botón enviar, se encarga de llamar al método de enviar el correo y luego recarga
     * la tabla de ordenes por enviar.
     *
     * @author christian.rodriguez
     */
    public void enviarOrdenPractica() {
        this.enviarCorreosOP();
        this.cargarOrdenesPracticaPorEnviar();
    }

    //---------------------------------------------------------------------------------------------------
    /**
     * Envía correo a los avaluadores y a sus correspondientes interventores con la orden de
     * práctica
     */
    private void enviarCorreosOP() {

        boolean envioCorreoExitoso = false;
        ArrayList<String> destinatariosTO, destinatariosCC;
        String cuerpoMensaje, asuntoMensaje, despedidaMensaje, inteventorEmail,
            directorTerritorialEmail, interventorCIEmail;
        String[] adjuntos = {""};
        String[] adjuntosNombres = {""};
        MessageFormat messageFormat;
        Object[] argumentos;
        ProfesionalAvaluo interventor;

        destinatariosTO = new ArrayList<String>();
        for (ProfesionalAvaluo pa : this.avaluadoresTramitesOP) {
            destinatariosTO.add(pa.getCorreoElectronico());
        }

        destinatariosCC = new ArrayList<String>();
        //D: si hay contrato interadministrativo se envía correo al interventor del contrato
        if (this.contratoInteradministrativoAvaluos != null) {
            interventorCIEmail = this.contratoInteradministrativoAvaluos.getInterventor().
                getCorreoElectronico();
            destinatariosCC.add(interventorCIEmail);
        }

        //D: para cada avaluador, si tiene contrato de prestación de servicios, se envía correo al
        //  interventor. Los avaluadores tienen el contrato (objeto ProfesionalAvaluosContrato)
        //  activo definido (como resultado de la consulta de los avalúos)
        //  También para cada uno, si no está en la sede central, se envía correo al director territorial
        for (ProfesionalAvaluo pa : this.avaluadoresTramitesOP) {
            if (pa.getTipoVinculacion().equals(
                EProfesionalTipoVinculacion.CONTRATO_DE_PRESTACION_DE_SERVICIOS.getCodigo())) {
                if (pa.getContratoActivo().getInterventor() != null) {
                    interventor = pa.getContratoActivo().getInterventor();
                    inteventorEmail = interventor.getCorreoElectronico();
                    if (!destinatariosCC.contains(inteventorEmail)) {
                        destinatariosCC.add(inteventorEmail);
                    }
                }
            }

            if (!pa.getTerritorial().getCodigo().equals(Constantes.DIRECCION_GENERAL_CODIGO)) {

                List<UsuarioDTO> funcionarios =
                    this.getGeneralesService().obtenerFuncionarioTerritorialYRol(
                        pa.getTerritorial().getCodigo(), ERol.DIRECTOR_TERRITORIAL);
                if (funcionarios != null && !funcionarios.isEmpty()) {
                    directorTerritorialEmail = funcionarios.get(0).getLogin() +
                        ConstantesComunicacionesCorreoElectronico.EXTENSION_EMAIL_INSTITUCIONAL_IGAC;
                    if (!destinatariosCC.contains(directorTerritorialEmail)) {
                        destinatariosCC.add(directorTerritorialEmail);
                    }
                }
            }
        }

        adjuntos[0] = this.ordenPracticaReporteDTO.getArchivoReporte()
            .getAbsolutePath();

        messageFormat =
            new MessageFormat(ConstantesComunicacionesCorreoElectronico.ASUNTO_CORREO_ORDEN_PRACTICA);
        argumentos = new Object[1];
        argumentos[0] = this.ordenPracticaSeleccionada.getNumero();
        asuntoMensaje = messageFormat.format(argumentos);

        messageFormat =
            new MessageFormat(ConstantesComunicacionesCorreoElectronico.CUERPO_CORREO_ORDEN_PRACTICA);
        cuerpoMensaje = messageFormat.format(argumentos);

        messageFormat =
            new MessageFormat(
                ConstantesComunicacionesCorreoElectronico.DESPEDIDA_CORREO_ORDEN_PRACTICA);
        argumentos = new Object[2];
        argumentos[0] = this.loggedInUser.getNombreCompleto();
        argumentos[1] = this.loggedInUser.getPrimerRol();
        despedidaMensaje = messageFormat.format(argumentos);

        cuerpoMensaje += despedidaMensaje;

        envioCorreoExitoso = this.getGeneralesService().enviarCorreo(destinatariosTO,
            destinatariosCC,
            null, asuntoMensaje, cuerpoMensaje, adjuntos, adjuntosNombres);
        if (!envioCorreoExitoso) {
            this.addMensajeError("No se pudo enviar el correo con la orden de práctica a los " +
                "avaluadores involucrados");
            return;
        } else {
            this.addMensajeInfo("Orden de práctica " +
                 this.ordenPracticaSeleccionada.getNumero() +
                 " enviada satisfactoriamente");
            this.ordenPracticaSeleccionada.setEstado(EOrdenPracticaEstado.ENVIADA.getCodigo());
            this.getAvaluosService().guardarActualizarOrdenPractica(this.ordenPracticaSeleccionada);
        }

    }

    // ------------------------------------------------------------------
    /**
     * método encargado de cargar las ordenes de practica con estado
     * {@link EOrdenPracticaEstado#GENERADA}
     *
     * @author christian.rodriguez
     */
    private void cargarOrdenesPracticaPorEnviar() {
        this.ordenesPracticaPorEnviar = this.getAvaluosService()
            .consultarOrdenesPracticaPorEnviar();
    }

    // ------------------------------------------------------------------
    /**
     * Listener del cambio de tab. Si se activa el tab de enviar ordenes entonces se cargan las
     * {@link #ordenesPracticaPorEnviar}
     *
     * @author christian.rodriguez
     * @param event
     */
    public void onTabChange(TabChangeEvent event) {

        String tabActivaId = event.getTab().getId();

        if (tabActivaId.equalsIgnoreCase(this.TAB_ENVIO_ID)) {
            this.cargarOrdenesPracticaPorEnviar();
            this.ordenesPracticaSeleccionadas = new OrdenPractica[]{};
        } else {
            this.selectedAvaluosForOP = new Avaluo[]{};
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón 'Enviar ordenes seleccionadas'. Se encarga de enviar el correo a los
     * avaluadores de las {@link #ordenesPracticaSeleccionadas} y cambiar el estado a
     * {@link EOrdenPracticaEstado#ENVIADA}
     *
     * @author christian.rodriguez
     */
    public void enviarOrdenesSeleccionadas() {

        if (this.ordenesPracticaSeleccionadas != null &&
             this.ordenesPracticaSeleccionadas.length != 0) {

            for (OrdenPractica ordenPractica : this.ordenesPracticaSeleccionadas) {
                this.ordenPracticaSeleccionada = ordenPractica;
                this.cargarAtributosOrdenPractica();
                this.enviarCorreosOP();
                this.cargarOrdenesPracticaPorEnviar();
            }
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Método encargado de cargar los valores requeridos por el método {@link #enviarCorreosOP()}
     * para poder enviar la orden correctamente
     *
     * @author christian.rodriguez
     */
    private void cargarAtributosOrdenPractica() {

        this.cargarDocumentoOrdenPractica();

        List<Avaluo> avaluosOrdenPractica = this.getAvaluosService()
            .consultarAvaluosPorOrdenPracticaId(
                this.ordenPracticaSeleccionada.getId());

        this.selectedAvaluosForOP = new Avaluo[avaluosOrdenPractica.size()];
        for (int i = 0; i < avaluosOrdenPractica.size(); i++) {
            this.selectedAvaluosForOP[i] = avaluosOrdenPractica.get(i);
        }

        ArrayList<ProfesionalAvaluo> avaluadoresTramite;
        this.avaluadoresTramitesOP = new ArrayList<ProfesionalAvaluo>();
        for (Avaluo avaluo : avaluosOrdenPractica) {
            avaluadoresTramite = (ArrayList<ProfesionalAvaluo>) avaluo
                .getAvaluadores();
            for (ProfesionalAvaluo avaluador : avaluadoresTramite) {
                if (!this.avaluadoresTramitesOP.contains(avaluador)) {
                    this.avaluadoresTramitesOP.add(avaluador);
                }
            }
            this.avaluosOrdenPractica.add(avaluo.getId());
        }

        Avaluo firstAvaluo = this.selectedAvaluosForOP[0];

        ContratoInteradministrativo contratoInterCurrent = this
            .getAvaluosService()
            .obtenerContratoInteradministrativoPorAvaluo(
                firstAvaluo.getId());

        if (contratoInterCurrent != null) {
            this.contratoInteradministrativoAvaluos = contratoInterCurrent;
            this.mostrarDatosContrato = true;
        } else {
            this.mostrarDatosContrato = false;
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Listener del commandlink de la columna numero de la tabla de ordenes para enviar
     *
     * @author christian.rodriguez
     */
    public void cargarOrdenParaEdicion() {
        this.cargarAtributosOrdenPractica();
        this.ordenPracticaGenerada = true;
        this.creandoOModificandoOP = false;
        this.ordenPracticaGeneradaPrimeraVez = true;
    }

    // ----------------------------------------------------------------------
    /**
     * Listener del botón cerrar
     *
     * @return
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

//end of class
}
