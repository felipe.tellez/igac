/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tests;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.persistence.entity.avaluos.Avaluo;
import co.gov.igac.snc.persistence.entity.avaluos.ContratoInteradministrativo;
import co.gov.igac.snc.persistence.entity.avaluos.ControlCalidadAvaluoRev;
import co.gov.igac.snc.persistence.entity.avaluos.OrdenPractica;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.util.EAvaluoDetalle;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaAvaluo;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.avaluos.asignacion.AdminOrdenesPracticaAvaluadorMB;
import co.gov.igac.snc.web.mb.avaluos.asignacion.AdministracionDetallesAvaluoMB;
import co.gov.igac.snc.web.mb.avaluos.asignacion.ConsultaOrdenPracticaPorRadicacionMB;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.ConsultaInformeCorreccionesAlProtocoloMB;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.GeneracionDocumentoAprobacionMB;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.GeneracionDocumentoLiquidacionCostoAvaluoMB;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.GeneracionOficioInterventorDelAvaluadorMB;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.GeneracionOficioTerritorialCostosHonorariosMB;
import co.gov.igac.snc.web.mb.avaluos.controlCalidad.GeneracionReporteControlCalidadGITMB;
import co.gov.igac.snc.web.mb.avaluos.ejecucion.AdministrarAreasAdoptadasMB;
import co.gov.igac.snc.web.mb.avaluos.ejecucion.CargaDetallesAvaluoDesdeArchivoMB;
import co.gov.igac.snc.web.mb.avaluos.radicacion.SolicitudSuspencionCancelacionAmpliacionDeTiemposMB;
import co.gov.igac.snc.web.mb.conservacion.ConsultaPredioMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * @author pedro.garcia
 */
@Component("testsAvaluos")
@Scope("session")
public class TestsAvaluosMB extends SNCManagedBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestsAvaluosMB.class);

    @Autowired
    private IContextListener contextoWebApp;

    private Avaluo currentAvaluo;

    private SolicitanteSolicitud solicitanteTramite;

    private EstructuraOrganizacional territorialUsuario;

    private UsuarioDTO currentUser;

    private ArrayList<Predio> prediosAvaluos;

    private Predio selectedPredioForDetailsChecking;

    // -------------------- VARIABLES USADAS PARA INTEGRACION CON VISOR
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;

    private ContratoInteradministrativo currentContract;

//------     methods   -----------------
    public ContratoInteradministrativo getCurrentContract() {
        return this.currentContract;
    }

    public void setCurrentContract(ContratoInteradministrativo currentContract) {
        this.currentContract = currentContract;
    }

    public String getApplicationClientName() {
        return this.applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public String getModuleLayer() {
        return this.moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return this.moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public Predio getSelectedPredioForDetailsChecking() {
        return this.selectedPredioForDetailsChecking;
    }

    public void setSelectedPredioForDetailsChecking(Predio predioSeleccionado) {
        this.selectedPredioForDetailsChecking = predioSeleccionado;
    }

    public ArrayList<Predio> getPrediosAvaluos() {
        return this.prediosAvaluos;
    }

    public void setPrediosAvaluos(ArrayList<Predio> prediosAvaluos) {
        this.prediosAvaluos = prediosAvaluos;
    }

    public EstructuraOrganizacional getTerritorialUsuario() {
        return this.territorialUsuario;
    }

    public void setTerritorialUsuario(EstructuraOrganizacional territorialUsuario) {
        this.territorialUsuario = territorialUsuario;
    }

    public Avaluo getCurrentAvaluo() {
        return this.currentAvaluo;
    }

    public void setCurrentAvaluo(Avaluo currentAvaluo) {
        this.currentAvaluo = currentAvaluo;
    }

    public SolicitanteSolicitud getSolicitanteTramite() {
        return this.solicitanteTramite;
    }

    public void setSolicitanteTramite(SolicitanteSolicitud solicitanteTramite) {
        this.solicitanteTramite = solicitanteTramite;
    }

//--------------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        Long avaluoId = 149L;
        this.currentAvaluo = this.getAvaluosService().obtenerInfoEncabezadoAvaluoPorId(avaluoId);
//
//        this.solicitanteTramite = this.currentAvaluo.getTramite().getSolicitud().getSolicitanteSolicituds().get(0);
//        this.currentUser = MenuMB.getMenu().getUsuarioDto();
//        String codigoTerritorial = this.currentUser.getCodigoEstructuraOrganizacional();
//        this.territorialUsuario = this.getGeneralesService().obtenerTerritorialPorCodigo(
//                codigoTerritorial);
//
//
//        this.prediosAvaluos = (ArrayList<Predio>)
//            this.getAvaluosService().obtenerPrediosDeAvaluo(avaluoId);
//
//
//        inicializarVariablesVisorGIS();

        this.currentContract = this.getAvaluosService().
            obtenerContratoInteradministrativoPorIdConEntidad(1l);
    }
//--------------------------------------------------------------------------------------------------

    public void inicializarConsultaPredioMB() {

        //this.predioSeleccionado = this.getConservacionService().buscarPredioPorNumeroPredialParaAvaluoComercial(this.numeroPredio);
        ConsultaPredioMB mb001 = (ConsultaPredioMB) UtilidadesWeb.getManagedBean("consultaPredio");
        mb001.cleanAllMB();

        if (this.selectedPredioForDetailsChecking != null) {
            mb001.setSelectedPredio1(this.selectedPredioForDetailsChecking);
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Inicializa las variables rqueridas para el funcionamiento del visor GIS
     *
     * @author christian.rodriguez
     */
    private void inicializarVariablesVisorGIS() {

        this.applicationClientName = this.contextoWebApp.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.OFERTAS_INMOBILIARIAS.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 112 para cargar las ordenes de practica
     * asociadas a los tramites de una solicitud
     *
     * @author christian.rodriguez
     */
    public void cargarOrdenPracticaPorRadicacioCU112() {
        String radicado = "60402012ER00041219";

        ConsultaOrdenPracticaPorRadicacionMB mb =
            (ConsultaOrdenPracticaPorRadicacionMB) UtilidadesWeb
                .getManagedBean("consultarOrdenesPracticaRad");
        mb.cargarDesdeOtrosMB(radicado);
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 154 para solicitar la suspencioon,
     * cancelacion o ampliamiento de un tramite de avaluos
     *
     * @author christian.rodriguez
     */
    public void cargarSolicitarSuspencionCancelacionAmpliacionTiemposCU154() {
        String radicado = "60402012ER00041219-1";

        SolicitudSuspencionCancelacionAmpliacionDeTiemposMB mb =
            (SolicitudSuspencionCancelacionAmpliacionDeTiemposMB) UtilidadesWeb
                .getManagedBean("solicitudCanSusAmplTiempos");
        mb.cargarDesdeOtrosMB(radicado);
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 021 para cargar la consulta de controles
     * de calidad
     *
     * @author christian.rodriguez
     */
    public void cargarConsultaInformeCorreccionesAlProtocoloCU021() {

        ConsultaInformeCorreccionesAlProtocoloMB mb =
            (ConsultaInformeCorreccionesAlProtocoloMB) UtilidadesWeb
                .getManagedBean("consultaInformeCorrecccionesProtocolo");
        mb.cargarDesdeOtrosMB();
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 111 para administrar las
     * {@link OrdenPractica} de un {@link ProfesionalAvaluo}
     *
     * @author christian.rodriguez
     */
    public void cargarAdminOrdenesPracticaAvaluadorCU111() {
        AdminOrdenesPracticaAvaluadorMB mb = (AdminOrdenesPracticaAvaluadorMB) UtilidadesWeb
            .getManagedBean("adminOrdenesPracticaAvaluador");

        ProfesionalAvaluo profesional = this.getAvaluosService().obtenerProfesionalAvaluoPorId(6L);

        List<ProfesionalAvaluo> profesionales = new ArrayList<ProfesionalAvaluo>();
        profesionales.add(profesional);

        mb.cargarDesdeOtrosCU(profesionales);
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 111 para administrar las
     * {@link OrdenPractica} de un {@link ProfesionalAvaluo}
     *
     * @author christian.rodriguez
     */
    public void cargarAdminOrdenesPracticaAvaluadoresCU111() {
        AdminOrdenesPracticaAvaluadorMB mb = (AdminOrdenesPracticaAvaluadorMB) UtilidadesWeb
            .getManagedBean("adminOrdenesPracticaAvaluador");

        ProfesionalAvaluo profesional = this.getAvaluosService().obtenerProfesionalAvaluoPorId(6L);

        List<ProfesionalAvaluo> profesionales = new ArrayList<ProfesionalAvaluo>();
        profesionales.add(profesional);

        profesional = this.getAvaluosService().obtenerProfesionalAvaluoPorId(7L);
        profesionales.add(profesional);

        mb.cargarDesdeOtrosCU(profesionales);
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 056
     *
     * @author felipe.cadena
     */
    public void cargarGenerarDocumentoAprobacionCU056() {

        GeneracionDocumentoAprobacionMB mb = (GeneracionDocumentoAprobacionMB) UtilidadesWeb
            .getManagedBean("generacionDocumentoAprobacion");

        List<Avaluo> avs = new ArrayList<Avaluo>();
        Avaluo ava = new Avaluo();
        ava.setId(172L);
        avs.add(ava);
        ava = new Avaluo();
        ava.setId(173L);
        avs.add(ava);
        ava = new Avaluo();
        ava.setId(174L);
        avs.add(ava);

        mb.cargarDesdeOtrosMB(avs);
    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 065
     *
     * @author felipe.cadena
     */
    public void cargarGenerarReporteCCGITCU065() {

        GeneracionReporteControlCalidadGITMB mb =
            (GeneracionReporteControlCalidadGITMB) UtilidadesWeb
                .getManagedBean("generacionReporteControlCalidadGIT");

        ControlCalidadAvaluoRev ccr = new ControlCalidadAvaluoRev();
        ccr.setId(122L);

        mb.cargarDesdeOtrosMB(ccr);
    }
    // -------------------------------------------------------------------

    /**
     * Método usado por la página de prueba del caso de uso 058
     *
     * @author felipe.cadena
     */
    public void cargarZonasCU058() {

        CargaDetallesAvaluoDesdeArchivoMB mb = (CargaDetallesAvaluoDesdeArchivoMB) UtilidadesWeb
            .getManagedBean("cargaDetallesAvaluoDesdeArchivo");

        mb.cargarDetalles(172L, "080010109000003910015000000000",
            EAvaluoDetalle.ZONAS_ECONOMICAS);
    }
    // -------------------------------------------------------------------

    /**
     * Método usado por la página de prueba del caso de uso 070
     *
     * @author felipe.cadena
     */
    public void cargarCultivosCU070() {

        CargaDetallesAvaluoDesdeArchivoMB mb = (CargaDetallesAvaluoDesdeArchivoMB) UtilidadesWeb
            .getManagedBean("cargaDetallesAvaluoDesdeArchivo");

        mb.cargarDetalles(172L, "080010109000003910015000000000",
            EAvaluoDetalle.CULTIVOS);

    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 101
     *
     * @author felipe.cadena
     */
    public void cargarMaquinariaCU101() {

        CargaDetallesAvaluoDesdeArchivoMB mb = (CargaDetallesAvaluoDesdeArchivoMB) UtilidadesWeb
            .getManagedBean("cargaDetallesAvaluoDesdeArchivo");

        mb.cargarDetalles(172L, "080010109000003910015000000000",
            EAvaluoDetalle.MAQUINARIA);

    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso 026
     *
     * @author felipe.cadena
     */
    public void cargarPrediosCU026() {

        CargaDetallesAvaluoDesdeArchivoMB mb = (CargaDetallesAvaluoDesdeArchivoMB) UtilidadesWeb
            .getManagedBean("cargaDetallesAvaluoDesdeArchivo");

        mb.cargarDetalles(172L, "080010109000003910015000000000",
            EAvaluoDetalle.PREDIOS);

    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso terreno 160
     *
     * @author felipe.cadena
     */
    public void administrarAreasAdoptadasTerrenoCU160() {

        AdministrarAreasAdoptadasMB mb = (AdministrarAreasAdoptadasMB) UtilidadesWeb
            .getManagedBean("administrarAreasAdoptadas");

        mb.registrarAreasTerreno(172L);

    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso construccion 160
     *
     * @author felipe.cadena
     */
    public void administrarAreasAdoptadasConstruccionCU160() {

        AdministrarAreasAdoptadasMB mb = (AdministrarAreasAdoptadasMB) UtilidadesWeb
            .getManagedBean("administrarAreasAdoptadas");

        mb.registrarAreasConstruccion(172L, 1L);

    }

    // -------------------------------------------------------------------
    /**
     * Método usado por la página de prueba del caso de uso consulta 160
     *
     * @author felipe.cadena
     */
    public void consultaAreasAdoptadasTerrenoCU160() {

        AdministrarAreasAdoptadasMB mb = (AdministrarAreasAdoptadasMB) UtilidadesWeb
            .getManagedBean("administrarAreasAdoptadas");

        mb.consultarAreasAdoptadas(172L);

    }
    // -------------------------------------------------------------------

    /**
     * Método usado por la página de prueba del caso de uso 124
     *
     * @author felipe.cadena
     */
    public void cargarAnexosCU124() {

        CargaDetallesAvaluoDesdeArchivoMB mb = (CargaDetallesAvaluoDesdeArchivoMB) UtilidadesWeb
            .getManagedBean("cargaDetallesAvaluoDesdeArchivo");

        mb.cargarDetalles(172L, "080010109000003910015000000000",
            EAvaluoDetalle.ANEXOS);

    }

    /**
     * Método usado para cargar el CU 029 en el que se genera el documento de liquidacion de costos
     * de los avaluos
     *
     * @author christian.rodriguez
     */
    public void cargarGenerarDocumentoDeLiquidacionCostoAvaluoCU029() {

        GeneracionDocumentoLiquidacionCostoAvaluoMB mb =
            (GeneracionDocumentoLiquidacionCostoAvaluoMB) UtilidadesWeb
                .getManagedBean("generacionDocLiquidacionAvaluo");

        List<Long> tramitesId = new ArrayList<Long>();
        tramitesId.add(43086L);
        tramitesId.add(43267L);
        tramitesId.add(43268L);
        tramitesId.add(43286L);

        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();
        filtro.setTramitesId(tramitesId);

        List<Avaluo> avaluosALiquidar = this.getAvaluosService()
            .buscarAvaluosPorFiltro(filtro);

        mb.cargarDesdeOtrosCU(avaluosALiquidar);

    }

    /**
     * Método usado para cargar el CU 061 Generar oficio para interventor de avaluador. Generar
     * oficio para territorial de costos honorarios. Se llama desde el CU-025
     *
     * @author rodrigo.hernandez
     */
    public void cargarGenerarOficioInterventorDelAvaluadorCU061() {

        GeneracionOficioInterventorDelAvaluadorMB mb =
            (GeneracionOficioInterventorDelAvaluadorMB) UtilidadesWeb
                .getManagedBean("generarOficioInterventorDelAvaluador");

        List<Long> tramitesId = new ArrayList<Long>();
        tramitesId.add(43327L);

        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();
        filtro.setTramitesId(tramitesId);

        List<Avaluo> avaluosALiquidar = this.getAvaluosService()
            .buscarAvaluosPorFiltro(filtro);

        mb.cargarDesdeOtrosMB(avaluosALiquidar);

    }

    /**
     * Método usado para cargar el CU-030. Generar oficio para territorial de costos
     * honorarios.Generar oficio para territorial de costos honorarios. Se llama desde el CU-025
     *
     * @author rodrigo.hernandez
     */
    public void cargarGenerarOficioTerritorialCostosHonorariosCU030() {

        /*
         * Base honorarios. Se envia desde el CU-025
         */
        Double baseHonorarios = 500000D;

        GeneracionOficioTerritorialCostosHonorariosMB mb =
            (GeneracionOficioTerritorialCostosHonorariosMB) UtilidadesWeb
                .getManagedBean("generarOficioTerritorialCostosHonorarios");

        List<Long> tramitesId = new ArrayList<Long>();
        tramitesId.add(43327L);

        FiltroDatosConsultaAvaluo filtro = new FiltroDatosConsultaAvaluo();
        filtro.setTramitesId(tramitesId);

        List<Avaluo> avaluosALiquidar = this.getAvaluosService()
            .buscarAvaluosPorFiltro(filtro);

        mb.cargarDesdeOtrosMB(avaluosALiquidar, baseHonorarios);

    }
//--------------------------------------------------------------------------------------------------

    /**
     * Métodos usados por la página de prueba de los casos de uso de administración de detalles del
     * avalúo
     *
     * @author pedro.garcia
     */
    // para anexos
    public String alistarAdminDetallesCU130() {

        AdministracionDetallesAvaluoMB mb = (AdministracionDetallesAvaluoMB) UtilidadesWeb.
            getManagedBean("administracionDetallesAvaluo");
        mb.setTipoDetalleAvaluo(1);
        mb.init();
        return "/avaluos/ejecucion/administrarDetallesAvaluo.jsf";
    }

    // para cultivos
    public String alistarAdminDetallesCU126() {

        AdministracionDetallesAvaluoMB mb = (AdministracionDetallesAvaluoMB) UtilidadesWeb.
            getManagedBean("administracionDetallesAvaluo");
        mb.setTipoDetalleAvaluo(3);
        mb.init();
        return "/avaluos/ejecucion/administrarDetallesAvaluo.jsf";
    }

    // para maquinarias
    public String alistarAdminDetallesCU127() {

        AdministracionDetallesAvaluoMB mb = (AdministracionDetallesAvaluoMB) UtilidadesWeb.
            getManagedBean("administracionDetallesAvaluo");
        mb.setTipoDetalleAvaluo(4);
        mb.init();
        return "/avaluos/ejecucion/administrarDetallesAvaluo.jsf";
    }

    // para zonas
    public String alistarAdminDetallesCU075() {

        AdministracionDetallesAvaluoMB mb = (AdministracionDetallesAvaluoMB) UtilidadesWeb.
            getManagedBean("administracionDetallesAvaluo");
        mb.setTipoDetalleAvaluo(5);
        mb.init();
        return "/avaluos/ejecucion/administrarDetallesAvaluo.jsf";
    }

    // para predios
    public String alistarAdminDetallesCU080() {

        AdministracionDetallesAvaluoMB mb = (AdministracionDetallesAvaluoMB) UtilidadesWeb.
            getManagedBean("administracionDetallesAvaluo");
        mb.setTipoDetalleAvaluo(6);
        mb.init();
        return "/avaluos/ejecucion/administrarDetallesAvaluo.jsf";
    }

//--------------------------------------------------------------------------------------------------
}
