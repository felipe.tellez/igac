package co.gov.igac.snc.web.mb.conservacion.proyeccion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.conservacion.PFichaMatrizPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PFoto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.conservacion.PPredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.PUnidadConstruccionComp;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.conservacion.PredioZona;
import co.gov.igac.snc.persistence.entity.conservacion.UnidadConstruccion;
import co.gov.igac.snc.persistence.entity.conservacion.UsoHomologado;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.formacion.VUsoConstruccionZona;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.vistas.VPPredioAvaluoDecreto;
import co.gov.igac.snc.persistence.util.EInfoAdicionalCampo;
import co.gov.igac.snc.persistence.util.EParametro;
import co.gov.igac.snc.persistence.util.EPredioCondicionPropiedad;
import co.gov.igac.snc.persistence.util.EProyeccionCancelaInscribe;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.EUnidadTipoConstruccion;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.web.mb.conservacion.ProyectarConservacionMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 * SNCManagedBean encargado del control de la pantalla involucrada con la gestión del detalle del
 * avalúo
 *
 * @author fabio.navarrete
 */
@Component("gestionDetalleAvaluo")
@Scope("session")
public class GestionDetalleAvaluoMB extends SNCManagedBean {

    /**
     *
     */
    private static final long serialVersionUID = -3377897744189610075L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(ProyectarConservacionMB.class);

    // ******* SERVICIOS ********//
    // ******* VARIABLES ********//
    /**
     * Tramite seleccionado.
     */
    private Tramite tramite;
    /**
	 * Avalúos catastrales aplicados al predio.
	 */
	private List<VPPredioAvaluoDecreto> pPrediosAvaluosCatastrales;
    /**
     * Usuario autenticado
     */
    UsuarioDTO usuario = MenuMB.getMenu().getUsuarioDto();

    /**
     * Predio del tramite de desenglobe.
     */
    private PPredio predioSeleccionado;

    /**
     * Usos homlogados para los tramites de cancelacion masiva
     */
    private Map<String, List<String>> usosHomologados;

    /**
     * Variables usadas para manejo de construcciones nuevas y vigentes.
     */
    private List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas;
    private PUnidadConstruccion[] unidadConstruccionConvencionalVigentesSeleccionadas;
    private PUnidadConstruccion[] unidadConstruccionNoConvencionalVigentesSeleccionadas;

    //busqueda de construcciones por predio
    private String numeroPredialConsulta;
    private List<UnidadConstruccion> unidadConstruccionConvencionalTotales;
    private List<UnidadConstruccion> unidadConstruccionNoConvencionalTotales;
    private List<UnidadConstruccion> unidadConstruccionConvencionalTotalesFM;
    private List<UnidadConstruccion> unidadConstruccionNoConvencionalTotalesFM;

    //Listas de unidades construccion del englobe virtual
    private List<PUnidadConstruccion> unidadConstruccionComunesConvencionalEnglobeVirtual;
    private List<PUnidadConstruccion> unidadConstruccionComunesNoConvencionalEnglobeVirtual;
    private List<PUnidadConstruccion> uniFiltroConstruccionComunesConvencionalEnglobeVirtual;
    private List<PUnidadConstruccion> uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual;
    private List<PUnidadConstruccion> unidadConstruccionConvencionalVigentesEV;
    private List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentesEV;
    private List<Predio> prediosOriginales;
    private List<PPredio> pPrediosOriginales;
    private List<PUnidadConstruccion> unidadConstruccionOriginalesEnglobeVirtual;

    private String numeroFichaMatriz;

    /**
     * Variable usada para almacenar informacion del historico de zonas
     */
    private List<PredioZona> historicoPredioZonaSeleccionado;

    /**
     * Variable usada para almacenar informacion de zonas editar del predio seleccionado
     */
    private List<PPredioZona> zonasVigentesPredioSeleccionado;
    /**
     * Variable usada para almacenar informacion de zonas editar del predio seleccionado
     */
    private List<PPredioZona> zonasIntermediasDiferentes;
    
    /**
     * Zonas vigentes seleccionadas para modificar o eliminar
     */
    private PPredioZona zonaVigenteSeleccionada;

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del las vigencias que
     * deben ser registrada manualmente
     */
    private List<SelectItem> listVigencia = new ArrayList<SelectItem>();

    /**
     * Variable que almacena la vigencia que se selecciono a ser registrada manualmente
     */
    private String vigenciaSeleccionada;

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del las zonas fisicas
     * que debe ser registrada manualmente
     */
    private List<SelectItem> listZonaFisica = new ArrayList<SelectItem>();

    /**
     * Variable que almacena la vigencia que se selecciono a ser registrada manualmente
     */
    private String zonaFisicaSeleccionada;

    /**
     * Retorna los valores para un combo box (selectOneMenu) con los registros del las zonas
     * geoeconomicas que debe ser registrada manualmente
     */
    private List<SelectItem> listZonaGeo = new ArrayList<SelectItem>();

    /**
     * Variable que almacena la zona geoeconomica seleccionada
     *
     */
    private String zonaGeoSeleccionada;

    /**
     * Variable que activa o desactiva el Panel de registro de nuevas zonas
     */
    private boolean actNuevaZona;

    /**
     * Variable que almacena el valor de la nueva area ingresada por el usuario
     */
    private Double areaNueva;

    /**
     * Variable que activa o desactiva el registro de nuevas zonas o la edicion de esta
     */
    private boolean editaZona;

    /**
     * Variable que activa o desactiva los botones de historico y registro de nuevas zonas
     */
    private boolean habilitaHistYRegZonas;

    /**
     * Variable que activa o desactiva los botones de historico y registro de nuevas zonas
     */
    private boolean habilitaMsjHistYRegZonas;

    /**
     * Variable que almacena la sumatoria de areas de terreno de las zonas para la vigencia
     * registrada
     */
    private Double sumAreasRegistradas;

    /**
     * Variable que almacena la sumatoria de areas de terreno de las zonas para la vigencia
     * registrada
     */
    private Double sumAreasCalculadas;

    /**
     * Variable que activa o desactiva de acuerso a si se mantiene o no la ficha original
     */
    private boolean mantieneFicha;
    
    private Date fechaActualizacionAvaluo ;

    /**
     * Variable que almacena la unidad de construccion convencional seleccionada
     */
    private PUnidadConstruccion[] uniConstruccionConvencionalSelecEV;

    /**
     * Variable que almacena la unidad de construccion no convencional seleccionada
     */
    private PUnidadConstruccion[] uniConstruccionNoConvencionalSelecEV;

    private PUnidadConstruccion replicaUnidadConstruccionSeleccionada;

    private double diferenciaAreas;

    private String msjZonasFichaMatriz;

    // ******* INIT ********//
    @PostConstruct
    public void init() {
        try {

            // Trámite
            this.tramite = this.getProyectarConservacionMB().getTramite();

            // Predio
            this.predioSeleccionado = this.getProyectarConservacionMB()
                .getPredioSeleccionado();

            if (this.tramite.isCancelacionMasiva()) {
                this.inicializarUsosHomologados();
            }

            if (this.tramite.isEnglobeVirtual()) {

                List<PUnidadConstruccion> consConVigentes =
                    this.getProyectarConservacionMB().
                        getpUnidadConstruccionComunesConvencionalEnglobeVirtual();

                List<PUnidadConstruccion> consNoConVigentes =
                    this.getProyectarConservacionMB().
                        getpUnidadConstruccionComunesNoConvencionalEnglobeVirtual();

                if (consConVigentes != null &&
                     !consConVigentes.isEmpty()) {

                    for (PUnidadConstruccion consConVigente : consConVigentes) {

                        if (consConVigente.getAreaConstruida() == null &&
                             consConVigente.getAreaConstruida().equals(0.0)) {

                            this.getProyectarConservacionMB().
                                getpUnidadConstruccionComunesConvencionalEnglobeVirtual().remove(
                                    consConVigente);
                        }

                    }

                }

                if (consNoConVigentes != null &&
                     !consNoConVigentes.isEmpty()) {

                    for (PUnidadConstruccion consNoConVigente : consNoConVigentes) {

                        if (consNoConVigente.getAreaConstruida() == null &&
                             consNoConVigente.getAreaConstruida().equals(0.0)) {

                            this.getProyectarConservacionMB().
                                getpUnidadConstruccionComunesNoConvencionalEnglobeVirtual().remove(
                                    consNoConVigente);
                        }

                    }

                }
                //variables para inicializar registro e historico de zonas
                this.actNuevaZona = false;
                this.editaZona = false;
                this.zonaFisicaSeleccionada = null;
                this.zonaGeoSeleccionada = null;
                this.vigenciaSeleccionada = null;
                this.habilitaHistYRegZonas = false;

            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            this.addMensajeError("Error en el init de FichaMatrizMB ");
        }
    }

    // ******* GETTERs - SETTERs ********//
    /**
     * @return the tramite
     */
    public Tramite getTramite() {
        return tramite;
    }

    public boolean isHabilitaMsjHistYRegZonas() {
        return habilitaMsjHistYRegZonas;
    }

    public void setHabilitaMsjHistYRegZonas(boolean habilitaMsjHistYRegZonas) {
        this.habilitaMsjHistYRegZonas = habilitaMsjHistYRegZonas;
    }

    /**
     * @param tramite the tramite to set
     */
    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public String getNumeroPredialConsulta() {
        return numeroPredialConsulta;
    }

    public void setNumeroPredialConsulta(String numeroPredialConsulta) {
        this.numeroPredialConsulta = numeroPredialConsulta;
    }

    /**
     * @return the predioSeleccionado
     */
    public PPredio getPredioSeleccionado() {
        return predioSeleccionado;
    }

    /**
     * @param predioSeleccionado the predioSeleccionado to set
     */
    public void setPredioSeleccionado(PPredio predioSeleccionado) {
        this.predioSeleccionado = predioSeleccionado;
    }

    /**
     * @return the unidadConstruccionConvencionalVigentes
     */
    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalVigentes() {
        return unidadConstruccionConvencionalVigentes;
    }

    /**
     * @param unidadConstruccionConvencionalVigentes the unidadConstruccionConvencionalVigentes to
     * set
     */
    public void setUnidadConstruccionConvencionalVigentes(
        List<PUnidadConstruccion> unidadConstruccionConvencionalVigentes) {
        this.unidadConstruccionConvencionalVigentes = unidadConstruccionConvencionalVigentes;
    }

    /**
     * @return the unidadConstruccionNoConvencionalVigentes
     */
    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalVigentes() {
        return unidadConstruccionNoConvencionalVigentes;
    }

    /**
     * @param unidadConstruccionNoConvencionalVigentes the unidadConstruccionNoConvencionalVigentes
     * to set
     */
    public void setUnidadConstruccionNoConvencionalVigentes(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentes) {
        this.unidadConstruccionNoConvencionalVigentes = unidadConstruccionNoConvencionalVigentes;
    }

    /**
     * @return the unidadConstruccionConvencionalNuevas
     */
    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevas() {
        return unidadConstruccionConvencionalNuevas;
    }

    /**
     * @param unidadConstruccionConvencionalNuevas the unidadConstruccionConvencionalNuevas to set
     */
    public void setUnidadConstruccionConvencionalNuevas(
        List<PUnidadConstruccion> unidadConstruccionConvencionalNuevas) {
        this.unidadConstruccionConvencionalNuevas = unidadConstruccionConvencionalNuevas;
    }

    /**
     * @return the unidadConstruccionNoConvencionalNuevas
     */
    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevas() {
        return unidadConstruccionNoConvencionalNuevas;
    }

    /**
     * @param unidadConstruccionNoConvencionalNuevas the unidadConstruccionNoConvencionalNuevas to
     * set
     */
    public void setUnidadConstruccionNoConvencionalNuevas(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevas) {
        this.unidadConstruccionNoConvencionalNuevas = unidadConstruccionNoConvencionalNuevas;
    }

    /**
     * @return the unidadConstruccionConvencionalNuevasNoCanceladas
     */
    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalNuevasNoCanceladas() {
        return unidadConstruccionConvencionalNuevasNoCanceladas;
    }

    /**
     * @param unidadConstruccionConvencionalNuevasNoCanceladas the
     * unidadConstruccionConvencionalNuevasNoCanceladas to set
     */
    public void setUnidadConstruccionConvencionalNuevasNoCanceladas(
        List<PUnidadConstruccion> unidadConstruccionConvencionalNuevasNoCanceladas) {
        this.unidadConstruccionConvencionalNuevasNoCanceladas =
            unidadConstruccionConvencionalNuevasNoCanceladas;
    }

    /**
     * @return the unidadConstruccionNoConvencionalNuevasNoCanceladas
     */
    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalNuevasNoCanceladas() {
        return unidadConstruccionNoConvencionalNuevasNoCanceladas;
    }

    /**
     * @param unidadConstruccionNoConvencionalNuevasNoCanceladas the
     * unidadConstruccionNoConvencionalNuevasNoCanceladas to set
     */
    public void setUnidadConstruccionNoConvencionalNuevasNoCanceladas(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalNuevasNoCanceladas) {
        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            unidadConstruccionNoConvencionalNuevasNoCanceladas;
    }

    /**
     * @return the unidadConstruccionConvencionalVigentesSeleccionadas
     */
    public PUnidadConstruccion[] getUnidadConstruccionConvencionalVigentesSeleccionadas() {
        return unidadConstruccionConvencionalVigentesSeleccionadas;
    }

    /**
     * @param unidadConstruccionConvencionalVigentesSeleccionadas the
     * unidadConstruccionConvencionalVigentesSeleccionadas to set
     */
    public void setUnidadConstruccionConvencionalVigentesSeleccionadas(
        PUnidadConstruccion[] unidadConstruccionConvencionalVigentesSeleccionadas) {
        this.unidadConstruccionConvencionalVigentesSeleccionadas =
            unidadConstruccionConvencionalVigentesSeleccionadas;
    }

    /**
     * @return the unidadConstruccionNoConvencionalVigentesSeleccionadas
     */
    public PUnidadConstruccion[] getUnidadConstruccionNoConvencionalVigentesSeleccionadas() {
        return unidadConstruccionNoConvencionalVigentesSeleccionadas;
    }

    /**
     * @param unidadConstruccionNoConvencionalVigentesSeleccionadas the
     * unidadConstruccionNoConvencionalVigentesSeleccionadas to set
     */
    public void setUnidadConstruccionNoConvencionalVigentesSeleccionadas(
        PUnidadConstruccion[] unidadConstruccionNoConvencionalVigentesSeleccionadas) {
        this.unidadConstruccionNoConvencionalVigentesSeleccionadas =
            unidadConstruccionNoConvencionalVigentesSeleccionadas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionComunesConvencionalEnglobeVirtual() {

        List<PUnidadConstruccion> unidadConstruccionComunesConvencionalEnglobeVirtualTemp =
            new ArrayList<PUnidadConstruccion>();

        if (this.unidadConstruccionComunesConvencionalEnglobeVirtual != null) {
            for (PUnidadConstruccion pUnidadConstruccionTemp
                : unidadConstruccionComunesConvencionalEnglobeVirtual) {
                if (pUnidadConstruccionTemp.getAreaConstruida() != 0.0) {
                    unidadConstruccionComunesConvencionalEnglobeVirtualTemp.add(
                        pUnidadConstruccionTemp);
                }
            }
        }

        return unidadConstruccionComunesConvencionalEnglobeVirtualTemp;
    }

    public void setUnidadConstruccionComunesConvencionalEnglobeVirtual(
        List<PUnidadConstruccion> unidadConstruccionComunesConvencionalEnglobeVirtual) {
        this.unidadConstruccionComunesConvencionalEnglobeVirtual =
            unidadConstruccionComunesConvencionalEnglobeVirtual;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionComunesNoConvencionalEnglobeVirtual() {

        List<PUnidadConstruccion> unidadConstruccionComunesNoConvencionalEnglobeVirtualTemp =
            new ArrayList<PUnidadConstruccion>();

        if (this.unidadConstruccionComunesNoConvencionalEnglobeVirtual != null) {
            for (PUnidadConstruccion pUnidadConstruccionTemp
                : unidadConstruccionComunesNoConvencionalEnglobeVirtual) {
                if (pUnidadConstruccionTemp.getAreaConstruida() != 0.0) {
                    unidadConstruccionComunesNoConvencionalEnglobeVirtualTemp.add(
                        pUnidadConstruccionTemp);
                }
            }
        }

        return unidadConstruccionComunesNoConvencionalEnglobeVirtualTemp;
    }

    public void setUnidadConstruccionComunesNoConvencionalEnglobeVirtual(
        List<PUnidadConstruccion> unidadConstruccionComunesNoConvencionalEnglobeVirtual) {
        this.unidadConstruccionComunesNoConvencionalEnglobeVirtual =
            unidadConstruccionComunesNoConvencionalEnglobeVirtual;
    }

    public List<Predio> getPrediosOriginales() {
        return prediosOriginales;
    }

    public void setPrediosOriginales(List<Predio> prediosOriginales) {
        this.prediosOriginales = prediosOriginales;
    }

    public List<PPredio> getpPrediosOriginales() {
        return pPrediosOriginales;
    }

    public void setpPrediosOriginales(List<PPredio> pPrediosOriginales) {
        this.pPrediosOriginales = pPrediosOriginales;
    }

    public List<PredioZona> getHistoricoPredioZonaSeleccionado() {
        return historicoPredioZonaSeleccionado;
    }

    public void setHistoricoPredioZonaSeleccionado(List<PredioZona> historicoPredioZonaSeleccionado) {
        this.historicoPredioZonaSeleccionado = historicoPredioZonaSeleccionado;
    }

    public List<PPredioZona> getZonasVigentesPredioSeleccionado() {
        return zonasVigentesPredioSeleccionado;
    }

    public void setZonasVigentesPredioSeleccionado(List<PPredioZona> zonasVigentesPredioSeleccionado) {
        this.zonasVigentesPredioSeleccionado = zonasVigentesPredioSeleccionado;
    }

    public PPredioZona getZonaVigenteSeleccionada() {
        return zonaVigenteSeleccionada;
    }

    public void setZonaVigenteSeleccionada(PPredioZona zonaVigenteSeleccionada) {
        this.zonaVigenteSeleccionada = zonaVigenteSeleccionada;
    }

    public List<SelectItem> getListVigencia() {
        return listVigencia;
    }

    public void setListVigencia(List<SelectItem> listVigencia) {
        this.listVigencia = listVigencia;
    }

    public String getVigenciaSeleccionada() {
        return vigenciaSeleccionada;
    }

    public void setVigenciaSeleccionada(String vigenciaSeleccionada) {
        this.vigenciaSeleccionada = vigenciaSeleccionada;
    }

    public List<SelectItem> getListZonaFisica() {
        return listZonaFisica;
    }

    public void setListZonaFisica(List<SelectItem> listZonaFisica) {
        this.listZonaFisica = listZonaFisica;
    }

    public String getZonaFisicaSeleccionada() {
        return zonaFisicaSeleccionada;
    }

    public void setZonaFisicaSeleccionada(String zonaFisicaSeleccionada) {
        this.zonaFisicaSeleccionada = zonaFisicaSeleccionada;
    }

    public List<SelectItem> getListZonaGeo() {
        return listZonaGeo;
    }

    public void setListZonaGeo(List<SelectItem> listZonaGeo) {
        this.listZonaGeo = listZonaGeo;
    }

    public String getZonaGeoSeleccionada() {
        return zonaGeoSeleccionada;
    }

    public void setZonaGeoSeleccionada(String zonaGeoSeleccionada) {
        this.zonaGeoSeleccionada = zonaGeoSeleccionada;
    }

    public boolean isActNuevaZona() {
        return actNuevaZona;
    }

    public void setActNuevaZona(boolean actNuevaZona) {
        this.actNuevaZona = actNuevaZona;
    }

    public Double getAreaNueva() {
        return areaNueva;
    }

    public void setAreaNueva(Double areaNueva) {
        this.areaNueva = areaNueva;
    }

    public boolean isEditaZona() {
        return editaZona;
    }

    public void setEditaZona(boolean editaZona) {
        this.editaZona = editaZona;
    }

    public Double getSumAreasRegistradas() {
        return sumAreasRegistradas;
    }

    public void setSumAreasRegistradas(Double sumAreasRegistradas) {
        this.sumAreasRegistradas = sumAreasRegistradas;
    }

    public Double getSumAreasCalculadas() {
        return sumAreasCalculadas;
    }

    public void setSumAreasCalculadas(Double sumAreasCalculadas) {
        this.sumAreasCalculadas = sumAreasCalculadas;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionConvencionalVigentesEV() {
        return unidadConstruccionConvencionalVigentesEV;
    }

    public void setUnidadConstruccionConvencionalVigentesEV(
        List<PUnidadConstruccion> unidadConstruccionConvencionalVigentesEV) {
        this.unidadConstruccionConvencionalVigentesEV = unidadConstruccionConvencionalVigentesEV;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionNoConvencionalVigentesEV() {
        return unidadConstruccionNoConvencionalVigentesEV;
    }

    public void setUnidadConstruccionNoConvencionalVigentesEV(
        List<PUnidadConstruccion> unidadConstruccionNoConvencionalVigentesEV) {
        this.unidadConstruccionNoConvencionalVigentesEV = unidadConstruccionNoConvencionalVigentesEV;
    }

    public boolean isHabilitaHistYRegZonas() {
        return habilitaHistYRegZonas;
    }

    public void setHabilitaHistYRegZonas(boolean habilitaHistYRegZonas) {
        this.habilitaHistYRegZonas = habilitaHistYRegZonas;
    }

    public boolean isMantieneFicha() {
        return mantieneFicha;
    }

    public void setMantieneFicha(boolean mantieneFicha) {
        this.mantieneFicha = mantieneFicha;
    }

    public List<PUnidadConstruccion> getUniFiltroConstruccionComunesConvencionalEnglobeVirtual() {
        return uniFiltroConstruccionComunesConvencionalEnglobeVirtual;
    }

    public void setUniFiltroConstruccionComunesConvencionalEnglobeVirtual(
        List<PUnidadConstruccion> uniFiltroConstruccionComunesConvencionalEnglobeVirtual) {
        this.uniFiltroConstruccionComunesConvencionalEnglobeVirtual =
            uniFiltroConstruccionComunesConvencionalEnglobeVirtual;
    }

    public List<PUnidadConstruccion> getUniFiltroConstruccionComunesNoConvencionalEnglobeVirtual() {
        return uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual;
    }

    public void setUniFiltroConstruccionComunesNoConvencionalEnglobeVirtual(
        List<PUnidadConstruccion> uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual) {
        this.uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual =
            uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual;
    }

    public PUnidadConstruccion[] getUniConstruccionConvencionalSelecEV() {
        return uniConstruccionConvencionalSelecEV;
    }

    public void setUniConstruccionConvencionalSelecEV(
        PUnidadConstruccion[] uniConstruccionConvencionalSelecEV) {
        this.uniConstruccionConvencionalSelecEV = uniConstruccionConvencionalSelecEV;
    }

    public PUnidadConstruccion[] getUniConstruccionNoConvencionalSelecEV() {
        return uniConstruccionNoConvencionalSelecEV;
    }

    public void setUniConstruccionNoConvencionalSelecEV(
        PUnidadConstruccion[] uniConstruccionNoConvencionalSelecEV) {
        this.uniConstruccionNoConvencionalSelecEV = uniConstruccionNoConvencionalSelecEV;
    }

    public String getNumeroFichaMatriz() {
        return numeroFichaMatriz;
    }

    public void setNumeroFichaMatriz(String numeroFichaMatriz) {
        this.numeroFichaMatriz = numeroFichaMatriz;
    }

    public List<PUnidadConstruccion> getUnidadConstruccionOriginalesEnglobeVirtual() {
        return unidadConstruccionOriginalesEnglobeVirtual;
    }

    public void setUnidadConstruccionOriginalesEnglobeVirtual(
        List<PUnidadConstruccion> unidadConstruccionOriginalesEnglobeVirtual) {
        this.unidadConstruccionOriginalesEnglobeVirtual = unidadConstruccionOriginalesEnglobeVirtual;
    }

    public PUnidadConstruccion getReplicaUnidadConstruccionSeleccionada() {
        return replicaUnidadConstruccionSeleccionada;
    }

    public void setReplicaUnidadConstruccionSeleccionada(
        PUnidadConstruccion replicaUnidadConstruccionSeleccionada) {
        this.replicaUnidadConstruccionSeleccionada = replicaUnidadConstruccionSeleccionada;
    }

    public double getDiferenciaAreas() {
        return diferenciaAreas;
    }

    public void setDiferenciaAreas(double diferenciaAreas) {
        this.diferenciaAreas = diferenciaAreas;
    }

    public String getMsjZonasFichaMatriz() {
        return msjZonasFichaMatriz;
    }

    public void setMsjZonasFichaMatriz(String msjZonasFichaMatriz) {
        this.msjZonasFichaMatriz = msjZonasFichaMatriz;
    }

    // ******* METHODS *******//
    public void asociarConstruccionesNoConvencionalesVigentes() {
        this.asociarConstruccionesVigentes(
            this.unidadConstruccionNoConvencionalVigentesSeleccionadas,
            this.unidadConstruccionNoConvencionalNuevas);
    }

    public void asociarConstruccionesVigentes(
        PUnidadConstruccion[] pUnidadConstruccionSeleccionadas,
        List<PUnidadConstruccion> unidadConstruccionNuevas) {
        if (pUnidadConstruccionSeleccionadas.length <= 0) {
            this.addMensajeWarn("Debe seleccionar al menos una unidad de construccion a asociar");
        } else {
            for (PUnidadConstruccion pVigenteSelect : pUnidadConstruccionSeleccionadas) {
                for (PUnidadConstruccion pNuevo : unidadConstruccionNuevas) {
                    if (pVigenteSelect.getId().equals(pNuevo.getId())) {
                        pNuevo.setCancelaInscribe(EProyeccionCancelaInscribe.MODIFICA
                            .getCodigo());
                        pNuevo.setUsuarioLog(MenuMB.getMenu().getUsuarioDto()
                            .getLogin());
                        pNuevo.setFechaLog(new Date());
                    }
                }
            }
            this.updateConstruccionesNuevasNoCanceladas();
        }
    }

    /**
     * Método que actualiza las listas de unidades de construcción nuevas - no canceladas
     *
     * @author fabio.navarrete
     */
    private void updateConstruccionesNuevasNoCanceladas() {
        this.unidadConstruccionConvencionalNuevasNoCanceladas = new ArrayList<PUnidadConstruccion>();
        for (PUnidadConstruccion puc : this.unidadConstruccionConvencionalNuevas) {
            if (puc.getCancelaInscribe() == null ||
                 !puc.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                this.unidadConstruccionConvencionalNuevasNoCanceladas.add(puc);
            }
        }

        this.unidadConstruccionNoConvencionalNuevasNoCanceladas =
            new ArrayList<PUnidadConstruccion>();
        for (PUnidadConstruccion puc : this.unidadConstruccionNoConvencionalNuevas) {
            if (puc.getCancelaInscribe() == null ||
                 !puc.getCancelaInscribe().equals(
                    EProyeccionCancelaInscribe.CANCELA.getCodigo())) {
                this.unidadConstruccionNoConvencionalNuevasNoCanceladas
                    .add(puc);
            }
        }
    }

    /**
     * Inicializa los usos de construccion homologados usadoa para tramites de cancelacion masiva
     *
     * @author felipe.cadena
     */
    private void inicializarUsosHomologados() {

        List<UsoHomologado> usos = this.getConservacionService().obtenerUsosHomologados();

        //solo para test no integrar
        if (usos == null || usos.isEmpty()) {
            usos = new ArrayList<UsoHomologado>();
            UsoHomologado uh = new UsoHomologado();
            uh.setCodigoOrigen("1");
            uh.setCodigoDestino("53");
            usos.add(uh);
            uh = new UsoHomologado();
            uh.setCodigoOrigen("1");
            uh.setCodigoDestino("70");
            usos.add(uh);
        }
        //----------------------
        this.usosHomologados = new TreeMap<String, List<String>>();

        for (UsoHomologado uso : usos) {
            if (this.usosHomologados.get(uso.getCodigoOrigen()) == null) {
                List<String> usosHom = new ArrayList<String>();
                usosHom.add(uso.getCodigoDestino());
                this.usosHomologados.put(uso.getCodigoOrigen(), usosHom);
            } else {
                this.usosHomologados.get(uso.getCodigoOrigen())
                    .add(uso.getCodigoDestino());
            }
        }

    }

    /**
     * Prepara las construccions disponibles para los tramites de cancelacion masiva.
     *
     * @author felipe.cadena
     */
    public void cargarConstruccionesCancelacionMasiva() {
        EntradaProyeccionMB entradaProyeccionMB = (EntradaProyeccionMB) UtilidadesWeb.
            getManagedBean("entradaProyeccion");

        List<Predio> prediosOriginales = this.getConservacionService().
            buscarPrediosPanelAvaluoPorTramiteId(this.tramite.getId());
        entradaProyeccionMB.inicializarPrediosCancelacionMasiva();
        List<PPredio> prediosResultantes = entradaProyeccionMB.
            getPrediosResultantesCancelacionMasiva();
        List<PUnidadConstruccion> unidadesResultantes = new ArrayList<PUnidadConstruccion>();

        for (PPredio pp : prediosResultantes) {
            unidadesResultantes.addAll(pp.getPUnidadConstruccions());
        }

        this.getProyectarConservacionMB().setUnidadesConstruccionConvencional(
            new ArrayList<UnidadConstruccion>());
        this.getProyectarConservacionMB().setUnidadesConstruccionNoConvencional(
            new ArrayList<UnidadConstruccion>());
        this.getProyectarConservacionMB().setUnidadesConstruccionConvencionalFM(
            new ArrayList<UnidadConstruccion>());
        this.getProyectarConservacionMB().setUnidadesConstruccionNoConvencionalFM(
            new ArrayList<UnidadConstruccion>());

        this.unidadConstruccionConvencionalTotales = new ArrayList<UnidadConstruccion>();
        this.unidadConstruccionNoConvencionalTotales = new ArrayList<UnidadConstruccion>();
        this.unidadConstruccionConvencionalTotalesFM = new ArrayList<UnidadConstruccion>();
        this.unidadConstruccionNoConvencionalTotalesFM = new ArrayList<UnidadConstruccion>();

        for (Predio pp : prediosOriginales) {
            for (UnidadConstruccion uni : pp.getUnidadesConstruccionConvencional()) {
                if (uni.getAnioCancelacion() == null) {
                    boolean unidadExiste = false;
                    for (PUnidadConstruccion puc : unidadesResultantes) {
                        if (puc.getProvienePredio() != null &&
                             uni.getPredio().getId().equals(puc.getProvienePredio().getId()) &&
                             uni.getUnidad().equals(puc.getProvieneUnidad())) {
                            unidadExiste = true;
                            break;
                        }
                    }
                    if (!unidadExiste) {
                        if (pp.getId().equals(this.tramite.getPredio().getId())) {
                            this.getProyectarConservacionMB().
                                getUnidadesConstruccionConvencionalFM().add(uni);
                            this.unidadConstruccionConvencionalTotalesFM.add(uni);
                        } else {
                            this.getProyectarConservacionMB().getUnidadesConstruccionConvencional().
                                add(uni);
                            this.unidadConstruccionConvencionalTotales.add(uni);
                        }
                    }
                }
            }
        }
        for (Predio pp : prediosOriginales) {
            for (UnidadConstruccion uni : pp.getUnidadesConstruccionNoConvencional()) {
                if (uni.getAnioCancelacion() == null) {
                    boolean unidadExiste = false;
                    for (PUnidadConstruccion puc : unidadesResultantes) {
                        if (puc.getProvienePredio() != null &&
                             uni.getPredio().getId().equals(puc.getProvienePredio().getId()) &&
                             uni.getUnidad().equals(puc.getProvieneUnidad())) {
                            unidadExiste = true;
                            break;
                        }
                    }
                    if (!unidadExiste) {
                        if (pp.getId().equals(this.tramite.getPredio().getId())) {
                            this.getProyectarConservacionMB().
                                getUnidadesConstruccionNoConvencionalFM().add(uni);
                            this.unidadConstruccionNoConvencionalTotalesFM.add(uni);
                        } else {
                            this.getProyectarConservacionMB().
                                getUnidadesConstruccionNoConvencional().add(uni);
                            this.unidadConstruccionNoConvencionalTotales.add(uni);
                        }
                    }
                }
            }
        }
    }

    /**
     * Raliza validaciones asociadas a las construcciones para tramites de cancelacion masiva
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validacionesConstruccionesCancelacionMasiva(PUnidadConstruccion cons) {

        List<PUnidadConstruccion> consPredioActual = this.getProyectarConservacionMB().
            getPredioSeleccionado()
            .getPUnidadesConstruccionConvencionalNuevas();
        if (consPredioActual != null) {
            consPredioActual.addAll(this.getProyectarConservacionMB().getPredioSeleccionado()
                .getPUnidadesConstruccionNoConvencionalNuevas());
        } else {
            consPredioActual = this.getProyectarConservacionMB().getPredioSeleccionado()
                .getPUnidadesConstruccionNoConvencionalNuevas();
        }

        if (cons.getUnidad() == null || cons.getUnidad().isEmpty()) {
            this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_010
                .getMensajeUsuario());
            LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_010
                .getMensajeTecnico());
            this.addErrorCallback();
            return false;
        }
        if (cons.getTotalPuntaje() != null) {
            if (cons.getTipoConstruccion().equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo()) &&
                 cons.getTotalPuntaje() != this.getProyectarConservacionMB().
                getTotalPuntosConstruccion()) {
                this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_015
                    .getMensajeUsuario(cons.getTotalPuntaje().toString()));
                LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_015
                    .getMensajeTecnico());
                this.addErrorCallback();
                return false;
            }
        }

        for (PUnidadConstruccion puc : consPredioActual) {

            if (cons.getId() == null || !cons.getId().equals(puc.getId())) {
                if (cons.getUnidad().equals(puc.getUnidad())) {
                    this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_009
                        .getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_009
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return false;
                }
            }

        }

        return true;
    }

    /**
     * Raliza validaciones sobre las construcciones a asociar para tramites de cancelacion masiva
     *
     * @author felipe.cadena
     * @return
     */
    public boolean validacionesConstruccionesAsociacionCM(List<UnidadConstruccion> construcciones) {

        boolean valido = true;
        if (construcciones != null) {
            for (UnidadConstruccion puc : construcciones) {

                Double valorUnidad = this.getConservacionService().
                    obtenerValorUnidadConstruccionPorProcedimientoDB(this.predioSeleccionado.
                        getNumeroPredial().substring(0, 7),
                        this.predioSeleccionado.getSector(),
                        this.predioSeleccionado.getDestino(),
                        puc.getUsoConstruccion().getIdConFormato(),
                        this.obtenerZonaFisicaMayorArea(),
                        puc.getTotalPuntaje().longValue(),
                        this.predioSeleccionado.getId());

                if (valorUnidad.equals(0d)) {
                    this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_011
                        .getMensajeUsuario(puc.getUnidad()));
                    LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_011
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    valido = false;
                }

                if (valorUnidad.equals(0d)) {
                    this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_012
                        .getMensajeUsuario(puc.getUnidad()));
                    LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_012
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    valido = false;
                }

                if (puc.getAreaConstruida() == null || puc.getAreaConstruida().equals(0d)) {
                    this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_013
                        .getMensajeUsuario(puc.getUnidad()));
                    LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_013
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    valido = false;
                }

                if (puc.getUsoConstruccion() == null) {
                    this.addMensajeError(ECodigoErrorVista.CANCELACION_MASIVA_014
                        .getMensajeUsuario(puc.getUnidad()));
                    LOGGER.warn(ECodigoErrorVista.CANCELACION_MASIVA_014
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return false;
                }
            }
        }

        return valido;
    }

    /**
     * Retorna la zona de mayor area relacionada al predio de la ficha matriz
     *
     * @author felipe.cadena
     * @return
     */
    public String obtenerZonaFisicaMayorArea() {
        String zonaMayor = "";
        Double area = 0.0;

        if (this.predioSeleccionado.getPPredioZonas() != null && !this.predioSeleccionado.
            getPPredioZonas().isEmpty()) {

            for (PPredioZona ppz : this.predioSeleccionado.getPPredioZonas()) {

                if (ppz.getCancelaInscribe().equals(EProyeccionCancelaInscribe.INSCRIBE.getCodigo())) {
                    if (ppz.getArea() > area) {
                        zonaMayor = ppz.getZonaFisica();
                    }
                }
            }
        }

        return zonaMayor;
    }

    /**
     * retorna los usos disponibles para las construcciones en los tramites de cancelacion masiva,
     * estos pueden variar si se trata de una construccion nueva o una ya existente, si el uso es
     * null es por que es una construccion nueva
     *
     * @param usoActual
     * @return
     */
    public List<VUsoConstruccionZona> obtenerUsosCancelacionMasiva(UsoConstruccion usoActual) {

        List<VUsoConstruccionZona> usosCancelacion = new ArrayList<VUsoConstruccionZona>();
        List<VUsoConstruccionZona> usosBase = this.getConservacionService()
            .obtenerVUsosConstruccionZona(
                this.predioSeleccionado.getNumeroPredial()
                    .substring(0, 7));
        if (usoActual == null || usoActual.getId().equals(Constantes.USO_CONSTRUCCION_CIMIENTOS)) {
            for (VUsoConstruccionZona uso : usosBase) {
                if (uso.getId().intValue() == Constantes.USO_CONSTRUCCION_CIMIENTOS) {
                    usosCancelacion.add(uso);
                }
            }
        } else {
            List<String> usosHomologadosUnidad = new ArrayList<String>();
            usosHomologadosUnidad.add(usoActual.getId().toString());
            if (this.usosHomologados.get(usoActual.getId().toString()) != null) {
                usosHomologadosUnidad.addAll(this.usosHomologados.get(usoActual.getId().toString()));
            }

            for (VUsoConstruccionZona uso : usosBase) {
                for (String usoHomologado : usosHomologadosUnidad) {
                    if (usoHomologado.equals(uso.getId().toString())) {
                        usosCancelacion.add(uso);
                    }
                }
            }
        }
        return usosCancelacion;
    }

    /**
     * Filtra las construcciones disponibles para asociar
     *
     * @author felipe.cadena
     */
    public void filtrarConstruccionesPorNumeroPredial() {

        this.getProyectarConservacionMB().setUnidadesConstruccionConvencional(
            new ArrayList<UnidadConstruccion>());
        this.getProyectarConservacionMB().setUnidadesConstruccionNoConvencional(
            new ArrayList<UnidadConstruccion>());
        this.getProyectarConservacionMB().setUnidadesConstruccionConvencionalFM(
            new ArrayList<UnidadConstruccion>());
        this.getProyectarConservacionMB().setUnidadesConstruccionNoConvencionalFM(
            new ArrayList<UnidadConstruccion>());

        for (UnidadConstruccion uct : this.unidadConstruccionConvencionalTotales) {
            if (uct.getPredio().getNumeroPredial().startsWith(this.numeroPredialConsulta)) {
                this.getProyectarConservacionMB().getUnidadesConstruccionConvencional().add(uct);
            }
        }
        for (UnidadConstruccion uct : this.unidadConstruccionConvencionalTotalesFM) {
            if (uct.getPredio().getNumeroPredial().startsWith(this.numeroPredialConsulta)) {
                this.getProyectarConservacionMB().getUnidadesConstruccionConvencionalFM().add(uct);
            }
        }
        for (UnidadConstruccion uct : this.unidadConstruccionNoConvencionalTotales) {
            if (uct.getPredio().getNumeroPredial().startsWith(this.numeroPredialConsulta)) {
                this.getProyectarConservacionMB().getUnidadesConstruccionNoConvencional().add(uct);
            }
        }
        for (UnidadConstruccion uct : this.unidadConstruccionNoConvencionalTotalesFM) {
            if (uct.getPredio().getNumeroPredial().startsWith(this.numeroPredialConsulta)) {
                this.getProyectarConservacionMB().getUnidadesConstruccionNoConvencionalFM().add(uct);
            }
        }

    }

    /**
     * Prepara las construccions disponibles para los tramites de englobe virtual masiva.
     *
     * @author leidy.gonzalez
     */
    public void cargarConstruccionesEnglobeVirtual() {

        if (this.predioSeleccionado.isEsPredioFichaMatriz()) {
            this.getFichaMatrizMB().init();
        }

        this.mantieneFicha = false;

        this.unidadConstruccionOriginalesEnglobeVirtual = new ArrayList<PUnidadConstruccion>();

        this.numeroFichaMatriz = this.getTramite().obtenerInfoCampoAdicional(
            EInfoAdicionalCampo.NUMERO_FICHA_MATRIZ_EV);

        if (!this.getEntradaProyeccionMB().getNuevaFichaMatriz().isEmpty() &&
             this.getEntradaProyeccionMB().getNuevaFichaMatriz().equals(ESiNo.NO.getCodigo())) {
            this.mantieneFicha = true;
        } else {
            this.mantieneFicha = false;
        }

        this.unidadConstruccionComunesConvencionalEnglobeVirtual =
            new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionComunesNoConvencionalEnglobeVirtual =
            new ArrayList<PUnidadConstruccion>();

        this.unidadConstruccionConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalNuevas = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionConvencionalVigentesEV = new ArrayList<PUnidadConstruccion>();
        this.unidadConstruccionNoConvencionalVigentesEV = new ArrayList<PUnidadConstruccion>();

        this.getProyectarConservacionMB().setpUnidadConstruccionComunesConvencionalEnglobeVirtual(
            new ArrayList<PUnidadConstruccion>());
        this.getProyectarConservacionMB().setpUnidadConstruccionComunesNoConvencionalEnglobeVirtual(
            new ArrayList<PUnidadConstruccion>());

        this.unidadConstruccionOriginalesEnglobeVirtual = this.getConservacionService()
            .buscarUnidConstOriginalesCanceladasPorTramite(
                this.tramite.getId());

        //Se carga Unidades Convencionales Comunes(FM), condicion 0 y 5
        if (this.unidadConstruccionOriginalesEnglobeVirtual != null &&
             !this.unidadConstruccionOriginalesEnglobeVirtual.isEmpty()) {

            for (PUnidadConstruccion pUnidadConstTemp
                : this.unidadConstruccionOriginalesEnglobeVirtual) {

                //Se carga Unidades Convencionales Comunes
                if (pUnidadConstTemp.getTipoConstruccion().trim().
                    equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {

                    if (pUnidadConstTemp.getAreaConstruida() != 0) {

                        this.unidadConstruccionComunesConvencionalEnglobeVirtual.add(
                            pUnidadConstTemp);

                    }

                }

                //Se carga Unidades No Convencionales Comunes
                if (pUnidadConstTemp.getTipoConstruccion().trim().
                    equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {

                    if (pUnidadConstTemp.getAreaConstruida() != 0) {

                        this.unidadConstruccionComunesNoConvencionalEnglobeVirtual.add(
                            pUnidadConstTemp);

                    }

                }

            }

        }

        //Se cargan las unidades Privadas del predio seleccionado en la listVigencia de construcciones nuevas
        //por defecto
        List<PUnidadConstruccion> pUnidadesConst = new ArrayList<PUnidadConstruccion>();

        if (this.getProyectarConservacionMB().getPredioSeleccionado() != null) {

            pUnidadesConst = this.getConservacionService().
                buscarUnidConstOriginalesPorIdPredio(this.getProyectarConservacionMB()
                    .getPredioSeleccionado().getId());
        }

        if (pUnidadesConst != null && !pUnidadesConst.isEmpty()) {

            //Se carga Unidades Convencionales Privadas
            for (PUnidadConstruccion uni : pUnidadesConst) {

                if (uni.getTipoConstruccion() != null &&
                     uni.getTipoConstruccion().trim()
                        .equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {

                    this.unidadConstruccionConvencionalNuevas.add(uni);

                    if (this.mantieneFicha) {

                        if (EProyeccionCancelaInscribe.MODIFICA
                            .getCodigo().equals(uni.getCancelaInscribe()) ||
                             uni.getCancelaInscribe() == null) {

                            this.unidadConstruccionConvencionalVigentesEV.add(uni);
                        }
                    }

                }

                //Se carga Unidades No Convencionales Privadas
                if (uni.getTipoConstruccion() != null &&
                     uni.getTipoConstruccion().trim()
                        .equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {

                    this.unidadConstruccionNoConvencionalNuevas.add(uni);

                    if (this.mantieneFicha) {

                        if (EProyeccionCancelaInscribe.MODIFICA
                            .getCodigo().equals(uni.getCancelaInscribe()) ||
                             uni.getCancelaInscribe() == null) {

                            this.unidadConstruccionNoConvencionalVigentesEV.add(uni);
                        }
                    }
                }

            }

        }

    }

    /**
     * Filtra las construcciones disponibles para asociar del Englobe Virtual
     *
     * @author leidy.gonzalez
     */
    public void filtrarConstruccionesPorNumeroPredialEV() {

        this.cargarConstruccionesEnglobeVirtual();

        this.uniFiltroConstruccionComunesConvencionalEnglobeVirtual =
            new ArrayList<PUnidadConstruccion>();
        this.uniFiltroConstruccionComunesConvencionalEnglobeVirtual.addAll(
            this.unidadConstruccionComunesConvencionalEnglobeVirtual);

        this.uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual =
            new ArrayList<PUnidadConstruccion>();
        this.uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual.addAll(
            this.unidadConstruccionComunesNoConvencionalEnglobeVirtual);

        for (PUnidadConstruccion uct : this.uniFiltroConstruccionComunesConvencionalEnglobeVirtual) {
            if (uct.getPPredio().getNumeroPredial().startsWith(this.numeroPredialConsulta)) {
                this.unidadConstruccionComunesConvencionalEnglobeVirtual =
                    new ArrayList<PUnidadConstruccion>();
                this.unidadConstruccionComunesConvencionalEnglobeVirtual.add(uct);
                break;
            }
        }
        for (PUnidadConstruccion uct : this.uniFiltroConstruccionComunesNoConvencionalEnglobeVirtual) {
            if (uct.getPPredio().getNumeroPredial().startsWith(this.numeroPredialConsulta)) {
                this.unidadConstruccionComunesNoConvencionalEnglobeVirtual =
                    new ArrayList<PUnidadConstruccion>();
                this.unidadConstruccionComunesNoConvencionalEnglobeVirtual.add(uct);
                break;
            }
        }

    }

    /**
     * Raliza validaciones sobre las construcciones a asociar para tramites de englobe virtual
     *
     * @author leidy.gonzalez
     * @return
     */
    public boolean validacionesConstruccionesAsociacionEV() {

        boolean valido = true;
        Predio predioProv = new Predio();

        if (this.uniConstruccionConvencionalSelecEV.length == 0 &&
             this.uniConstruccionNoConvencionalSelecEV.length == 0) {

            this.addMensajeWarn("Debe seleccionar al menos una construcción para asociarla");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
            return valido = false;
        }

        String unidadConstruccionUnidadSiguiente = new String();
        PUnidadConstruccion pVigenteSelectNueva = new PUnidadConstruccion();

        //CONSTRUCCIONES CONVENCIONALES
        List<PUnidadConstruccion> unidadesExistentes = this.getConservacionService().
            obtenerPUnidadesConstConvPorIdPredio(this.getProyectarConservacionMB().
                getPredioSeleccionado().getId());

        //Consultar Unidades de Construccion que existen en Base de Datos
        if (unidadesExistentes != null && !unidadesExistentes.isEmpty()) {

            for (PUnidadConstruccion pVigenteSelect
                : this.uniConstruccionConvencionalSelecEV) {

                for (PUnidadConstruccion puce : unidadesExistentes) {

                    if (puce.getCancelaInscribe() != null && !puce.getCancelaInscribe().isEmpty() &&
                         EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo().equals(puce.getCancelaInscribe())) {

                        //Valida que solo se asocia una vez la unidad al predio actual
                        if (pVigenteSelect.getUnidad() != null && !pVigenteSelect.getUnidad().
                            isEmpty() &&
                             pVigenteSelect.getPPredio() != null && pVigenteSelect.getPPredio().
                            getId() != null &&
                             puce.getProvieneUnidad() != null && !puce.getProvieneUnidad().isEmpty() &&
                             puce.getProvienePredio() != null && puce.getProvienePredio().getId() !=
                            null &&
                             (pVigenteSelect.getUnidad().equals(puce.getProvieneUnidad())) &&
                             (pVigenteSelect.getPPredio().getId().equals(puce.getProvienePredio().
                                getId()))) {

                            this.uniConstruccionConvencionalSelecEV = new PUnidadConstruccion[0];

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_005
                                .getMensajeUsuario(pVigenteSelect.getUnidad()));
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_005
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;

                        }

                        if (pVigenteSelect.getAreaConstruida() == 0) {

                            this.uniConstruccionConvencionalSelecEV = new PUnidadConstruccion[0];

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_006
                                .getMensajeUsuario(pVigenteSelect.getUnidad()));
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_006
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;
                        }
                    }

                }
                // Cálculo del valor de la unidad para la nueva
                // PUnidadConstruccion
                pVigenteSelectNueva = new PUnidadConstruccion();

                unidadConstruccionUnidadSiguiente = this.getProyectarConservacionMB().
                    calcularUnidadConstruccionUnidad();

                if (unidadConstruccionUnidadSiguiente != null &&
                     !unidadConstruccionUnidadSiguiente.isEmpty()) {

                    pVigenteSelectNueva.setUnidad(unidadConstruccionUnidadSiguiente);
                } else {
                    pVigenteSelectNueva.setUnidad(pVigenteSelect.getUnidad());
                }

                pVigenteSelectNueva
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                pVigenteSelectNueva.setUsuarioLog(this.getProyectarConservacionMB().getUsuario().
                    getLogin());
                pVigenteSelectNueva.setFechaLog(new Date());
                pVigenteSelectNueva.setAreaConstruida(0.0);
                pVigenteSelectNueva.setPPredio(this.getProyectarConservacionMB().
                    getPredioSeleccionado());
                pVigenteSelectNueva.setId(null);
                pVigenteSelectNueva.setTipoDominio(pVigenteSelect.getTipoDominio());
                pVigenteSelectNueva.setTotalPuntaje(pVigenteSelect.getTotalPuntaje());
                pVigenteSelectNueva.setTipoCalificacion(pVigenteSelect.getTipoCalificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setAvaluo(pVigenteSelect.getAvaluo());
                pVigenteSelectNueva.setProvieneUnidad(pVigenteSelect.getUnidad());
                //Obtener Predio
                predioProv = this.getConservacionService().
                    obtenerPredioPorId(pVigenteSelect.getPPredio().getId());
                pVigenteSelectNueva.setProvienePredio(predioProv);
                pVigenteSelectNueva.setTipoConstruccion(pVigenteSelect.getTipoConstruccion());
                pVigenteSelectNueva.setOriginalTramite(ESiNo.SI.getCodigo());
                pVigenteSelectNueva.setUsoConstruccion(pVigenteSelect.getUsoConstruccion());
                pVigenteSelectNueva.setValorM2Construccion(pVigenteSelect.getValorM2Construccion());
                pVigenteSelectNueva.setPisoUbicacion(pVigenteSelect.getPisoUbicacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalLocales(pVigenteSelect.getTotalLocales());
                pVigenteSelectNueva.setTotalHabitaciones(pVigenteSelect.getTotalHabitaciones());
                pVigenteSelectNueva.setTotalBanios(pVigenteSelect.getTotalBanios());
                pVigenteSelectNueva.setTipificacion(pVigenteSelect.getTipificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());

                this.predioSeleccionado.getPUnidadConstruccions().add(
                    pVigenteSelectNueva);

                pVigenteSelectNueva = this.getConservacionService().actualizarPUnidadDeConstruccion(
                    pVigenteSelectNueva);

                this.unidadConstruccionConvencionalNuevas.add(pVigenteSelectNueva);

                //Se inserta a las construcciones asociadas las P Unidades Construccion Componete
                //y la P Fotos de la construccion Comun
                if (pVigenteSelectNueva != null) {

                    this.asociarConstruccionCompYPFotosUnidadesAsocidas(pVigenteSelectNueva,
                        pVigenteSelect);

                }

            }

            this.uniConstruccionConvencionalSelecEV = new PUnidadConstruccion[0];

            //Cuando no existen unidades de connvencionales nuevas    
        } else {

            for (PUnidadConstruccion pVigenteSelect
                : this.uniConstruccionConvencionalSelecEV) {

                // Cálculo del valor de la unidad para la nueva
                // PUnidadConstruccion
                pVigenteSelectNueva = new PUnidadConstruccion();

                unidadConstruccionUnidadSiguiente = this.getProyectarConservacionMB().
                    calcularUnidadConstruccionUnidad();

                if (unidadConstruccionUnidadSiguiente != null &&
                     !unidadConstruccionUnidadSiguiente.isEmpty()) {

                    pVigenteSelectNueva.setUnidad(unidadConstruccionUnidadSiguiente);
                } else {
                    pVigenteSelectNueva.setUnidad(pVigenteSelect.getUnidad());
                }

                pVigenteSelectNueva
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                pVigenteSelectNueva.setUsuarioLog(this.getProyectarConservacionMB().getUsuario().
                    getLogin());
                pVigenteSelectNueva.setFechaLog(new Date());
                pVigenteSelectNueva.setAreaConstruida(0.0);
                pVigenteSelectNueva.setPPredio(this.getProyectarConservacionMB().
                    getPredioSeleccionado());
                pVigenteSelectNueva.setId(null);
                pVigenteSelectNueva.setTipoDominio(pVigenteSelect.getTipoDominio());
                pVigenteSelectNueva.setTotalPuntaje(pVigenteSelect.getTotalPuntaje());
                pVigenteSelectNueva.setTipoCalificacion(pVigenteSelect.getTipoCalificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setAvaluo(pVigenteSelect.getAvaluo());
                pVigenteSelectNueva.setProvieneUnidad(pVigenteSelect.getUnidad());
                //Obtener Predio
                predioProv = this.getConservacionService().
                    obtenerPredioPorId(pVigenteSelect.getPPredio().getId());
                pVigenteSelectNueva.setProvienePredio(predioProv);
                pVigenteSelectNueva.setTipoConstruccion(pVigenteSelect.getTipoConstruccion());
                pVigenteSelectNueva.setOriginalTramite(ESiNo.SI.getCodigo());
                pVigenteSelectNueva.setUsoConstruccion(pVigenteSelect.getUsoConstruccion());
                pVigenteSelectNueva.setValorM2Construccion(pVigenteSelect.getValorM2Construccion());
                pVigenteSelectNueva.setPisoUbicacion(pVigenteSelect.getPisoUbicacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalLocales(pVigenteSelect.getTotalLocales());
                pVigenteSelectNueva.setTotalHabitaciones(pVigenteSelect.getTotalHabitaciones());
                pVigenteSelectNueva.setTotalBanios(pVigenteSelect.getTotalBanios());
                pVigenteSelectNueva.setTipificacion(pVigenteSelect.getTipificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());

                this.predioSeleccionado.getPUnidadConstruccions().add(
                    pVigenteSelectNueva);

                this.getConservacionService().actualizarPUnidadDeConstruccion(
                    pVigenteSelectNueva);

                this.unidadConstruccionConvencionalNuevas.add(pVigenteSelectNueva);

                //Se inserta a las construcciones asociadas las P Unidades Construccion Componete
                //y la P Fotos de la construccion Comun
                if (pVigenteSelectNueva != null) {

                    this.asociarConstruccionCompYPFotosUnidadesAsocidas(pVigenteSelectNueva,
                        pVigenteSelect);

                }

            }

            this.uniConstruccionConvencionalSelecEV = new PUnidadConstruccion[0];

        }

        //CONSTRUCCIONES NO CONVENCIONALES
        List<PUnidadConstruccion> unidadesExistentesNC = this.getConservacionService().
            obtenerPUnidadesConstNoConvPorIdPredio(this.getProyectarConservacionMB().
                getPredioSeleccionado().getId());

        if (unidadesExistentesNC != null && !unidadesExistentesNC.isEmpty()) {

            for (PUnidadConstruccion pVigenteSelect
                : this.uniConstruccionNoConvencionalSelecEV) {

                pVigenteSelectNueva = new PUnidadConstruccion();

                for (PUnidadConstruccion puce : unidadesExistentesNC) {

                    if (puce.getCancelaInscribe() != null && !puce.getCancelaInscribe().isEmpty() &&
                         EProyeccionCancelaInscribe.INSCRIBE
                            .getCodigo().equals(puce.getCancelaInscribe())) {

                        //Valida que solo se asocia una vez la unidad al predio actual
                        if (pVigenteSelect.getUnidad() != null && !pVigenteSelect.getUnidad().
                            isEmpty() &&
                             pVigenteSelect.getPPredio() != null && pVigenteSelect.getPPredio().
                            getId() != null &&
                             puce.getProvieneUnidad() != null && !puce.getProvieneUnidad().isEmpty() &&
                             puce.getProvienePredio() != null && puce.getProvienePredio().getId() !=
                            null &&
                             (pVigenteSelect.getUnidad().equals(puce.getProvieneUnidad())) &&
                             (pVigenteSelect.getPPredio().getId().equals(puce.getProvienePredio().
                                getId()))) {

                            this.uniConstruccionConvencionalSelecEV = new PUnidadConstruccion[0];

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_005
                                .getMensajeUsuario(pVigenteSelect.getUnidad()));
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_005
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;

                        }

                        if (pVigenteSelect.getAreaConstruida() == 0) {

                            this.uniConstruccionConvencionalSelecEV = new PUnidadConstruccion[0];

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_006
                                .getMensajeUsuario(pVigenteSelect.getUnidad()));
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_006
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;
                        }

                    }

                }

                // Cálculo del valor de la unidad para la nueva
                // PUnidadConstruccion
                pVigenteSelectNueva = new PUnidadConstruccion();
                unidadConstruccionUnidadSiguiente = this.getProyectarConservacionMB().
                    calcularUnidadConstruccionUnidad();

                if (unidadConstruccionUnidadSiguiente != null &&
                     !unidadConstruccionUnidadSiguiente.isEmpty()) {

                    pVigenteSelectNueva.setUnidad(unidadConstruccionUnidadSiguiente);
                } else {
                    pVigenteSelectNueva.setUnidad(pVigenteSelect.getUnidad());
                }

                pVigenteSelectNueva
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                pVigenteSelectNueva.setUsuarioLog(this.getProyectarConservacionMB().getUsuario().
                    getLogin());
                pVigenteSelectNueva.setFechaLog(new Date());
                pVigenteSelectNueva.setAreaConstruida(0.0);
                pVigenteSelectNueva.setPPredio(this.getProyectarConservacionMB().
                    getPredioSeleccionado());
                pVigenteSelectNueva.setId(null);
                pVigenteSelectNueva.setTipoDominio(pVigenteSelect.getTipoDominio());
                pVigenteSelectNueva.setTotalPuntaje(pVigenteSelect.getTotalPuntaje());
                pVigenteSelectNueva.setTipoCalificacion(pVigenteSelect.getTipoCalificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setAvaluo(pVigenteSelect.getAvaluo());
                pVigenteSelectNueva.setProvieneUnidad(pVigenteSelect.getUnidad());
                //Obtener Predio
                predioProv = this.getConservacionService().
                    obtenerPredioPorId(pVigenteSelect.getPPredio().getId());
                pVigenteSelectNueva.setProvienePredio(predioProv);
                pVigenteSelectNueva.setTipoConstruccion(pVigenteSelect.getTipoConstruccion());
                pVigenteSelectNueva.setOriginalTramite(ESiNo.SI.getCodigo());
                pVigenteSelectNueva.setUsoConstruccion(pVigenteSelect.getUsoConstruccion());
                pVigenteSelectNueva.setValorM2Construccion(pVigenteSelect.getValorM2Construccion());
                pVigenteSelectNueva.setPisoUbicacion(pVigenteSelect.getPisoUbicacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalLocales(pVigenteSelect.getTotalLocales());
                pVigenteSelectNueva.setTotalHabitaciones(pVigenteSelect.getTotalHabitaciones());
                pVigenteSelectNueva.setTotalBanios(pVigenteSelect.getTotalBanios());
                pVigenteSelectNueva.setTipificacion(pVigenteSelect.getTipificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());

                this.getProyectarConservacionMB().getPredioSeleccionado().getPUnidadConstruccions().
                    add(
                        pVigenteSelectNueva);
                this.getConservacionService().actualizarPUnidadDeConstruccion(
                    pVigenteSelectNueva);
                this.unidadConstruccionNoConvencionalNuevas.add(pVigenteSelectNueva);

                //Se inserta a las construcciones asociadas las P Unidades Construccion Componete
                //y la P Fotos de la construccion Comun
                if (pVigenteSelectNueva != null) {

                    this.asociarConstruccionCompYPFotosUnidadesAsocidas(pVigenteSelectNueva,
                        pVigenteSelect);

                }

            }

            this.uniConstruccionNoConvencionalSelecEV = new PUnidadConstruccion[0];
            //Cundo no existen unidades no convencionales nuevas
        } else {

            for (PUnidadConstruccion pVigenteSelect
                : this.uniConstruccionNoConvencionalSelecEV) {

                pVigenteSelectNueva = new PUnidadConstruccion();

                // Cálculo del valor de la unidad para la nueva
                // PUnidadConstruccion
                unidadConstruccionUnidadSiguiente = this.getProyectarConservacionMB().
                    calcularUnidadConstruccionUnidad();

                if (unidadConstruccionUnidadSiguiente != null &&
                     !unidadConstruccionUnidadSiguiente.isEmpty()) {

                    pVigenteSelectNueva.setUnidad(unidadConstruccionUnidadSiguiente);
                } else {
                    pVigenteSelectNueva.setUnidad(pVigenteSelect.getUnidad());
                }

                pVigenteSelectNueva
                    .setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                        .getCodigo());
                pVigenteSelectNueva.setUsuarioLog(this.getProyectarConservacionMB().getUsuario().
                    getLogin());
                pVigenteSelectNueva.setFechaLog(new Date());
                pVigenteSelectNueva.setAreaConstruida(0.0);
                pVigenteSelectNueva.setPPredio(this.getProyectarConservacionMB().
                    getPredioSeleccionado());
                pVigenteSelectNueva.setId(null);
                pVigenteSelectNueva.setTipoDominio(pVigenteSelect.getTipoDominio());
                pVigenteSelectNueva.setTotalPuntaje(pVigenteSelect.getTotalPuntaje());
                pVigenteSelectNueva.setTipoCalificacion(pVigenteSelect.getTipoCalificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setAvaluo(pVigenteSelect.getAvaluo());
                pVigenteSelectNueva.setProvieneUnidad(pVigenteSelect.getUnidad());
                //Obtener Predio
                predioProv = this.getConservacionService().
                    obtenerPredioPorId(pVigenteSelect.getPPredio().getId());
                pVigenteSelectNueva.setProvienePredio(predioProv);
                pVigenteSelectNueva.setTipoConstruccion(pVigenteSelect.getTipoConstruccion());
                pVigenteSelectNueva.setOriginalTramite(ESiNo.SI.getCodigo());
                pVigenteSelectNueva.setUsoConstruccion(pVigenteSelect.getUsoConstruccion());
                pVigenteSelectNueva.setValorM2Construccion(pVigenteSelect.getValorM2Construccion());
                pVigenteSelectNueva.setPisoUbicacion(pVigenteSelect.getPisoUbicacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());
                pVigenteSelectNueva.setTotalPisosUnidad(pVigenteSelect.getTotalPisosUnidad());
                pVigenteSelectNueva.setTotalLocales(pVigenteSelect.getTotalLocales());
                pVigenteSelectNueva.setTotalHabitaciones(pVigenteSelect.getTotalHabitaciones());
                pVigenteSelectNueva.setTotalBanios(pVigenteSelect.getTotalBanios());
                pVigenteSelectNueva.setTipificacion(pVigenteSelect.getTipificacion());
                pVigenteSelectNueva.setTotalPisosConstruccion(pVigenteSelect.
                    getTotalPisosConstruccion());

                this.getProyectarConservacionMB().getPredioSeleccionado().getPUnidadConstruccions().
                    add(
                        pVigenteSelectNueva);
                pVigenteSelectNueva = this.getConservacionService().actualizarPUnidadDeConstruccion(
                    pVigenteSelectNueva);
                this.unidadConstruccionNoConvencionalNuevas.add(pVigenteSelectNueva);

                //Se inserta a las construcciones asociadas las P Unidades Construccion Componete
                //y la P Fotos de la construccion Comun
                if (pVigenteSelectNueva != null) {

                    this.asociarConstruccionCompYPFotosUnidadesAsocidas(pVigenteSelectNueva,
                        pVigenteSelect);

                }
            }

            this.uniConstruccionNoConvencionalSelecEV = new PUnidadConstruccion[0];

        }

        this.cargarConstruccionesEnglobeVirtual();

        return valido;
    }

    /**
     * Realiza validaciones asociadas a las construcciones para tramites de englobe virtual
     *
     * @author leidy.gonzalez
     * @return
     */
    public boolean validacionesConstruccionesEnglobeVirtual(PUnidadConstruccion conSeleccionada) {

        boolean valido = true;
        Double difConSelyVigente = 0.0;
        Double difConSelyOrig = 0.0;
        boolean validaAreaOrigCero = false;

        List<PUnidadConstruccion> consConVigentes =
            this.unidadConstruccionComunesConvencionalEnglobeVirtual;

        List<PUnidadConstruccion> consPredioActual = this.unidadConstruccionConvencionalNuevas;

        //ORIGINALES DEL TRAMITE
        this.unidadConstruccionOriginalesEnglobeVirtual = this.getConservacionService()
            .buscarUnidConstOriginalesCanceladasPorTramite(
                this.tramite.getId());

        if (this.unidadConstruccionOriginalesEnglobeVirtual != null &&
             !this.unidadConstruccionOriginalesEnglobeVirtual.isEmpty()) {

            for (PUnidadConstruccion pUnidadSel : this.unidadConstruccionOriginalesEnglobeVirtual) {

                if (this.replicaUnidadConstruccionSeleccionada.getProvieneUnidad() != null &&
                     this.replicaUnidadConstruccionSeleccionada.getProvienePredio() != null &&
                     pUnidadSel.getPPredio().getId().equals(
                        this.replicaUnidadConstruccionSeleccionada.getProvienePredio().getId()) &&
                     pUnidadSel.getUnidad().equals(this.replicaUnidadConstruccionSeleccionada.
                        getProvieneUnidad()) &&
                     pUnidadSel.getTipoConstruccion().equals(
                        this.replicaUnidadConstruccionSeleccionada.getTipoConstruccion())) {

                    if (pUnidadSel.getAreaConstruida() == 0) {

                        validaAreaOrigCero = true;

                        if (pUnidadSel.getTipoConstruccion().trim().
                            equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {
                            conSeleccionada.setAreaConstruida(this.getProyectarConservacionMB().
                                getAreaNoConvencional());
                        }

                        difConSelyOrig = ((this.replicaUnidadConstruccionSeleccionada.
                            getAreaConstruida() +
                             pUnidadSel.getAreaConstruida()) -
                             conSeleccionada.getAreaConstruida());

                        if (difConSelyOrig < 0) {

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_007
                                .getMensajeUsuario());
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_007
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;

                        } else {

                            pUnidadSel.setAreaConstruida(difConSelyOrig);
                            this.getProyectarConservacionMB().setAreaNoConvencional(difConSelyOrig);
                            //Almacena area  a conSeleccionada
                            this.getConservacionService().guardarProyeccion(this.
                                getProyectarConservacionMB().getUsuario(),
                                pUnidadSel);

                            pUnidadSel = this.getConservacionService().
                                actualizarPUnidadDeConstruccion(
                                    pUnidadSel);

                        }

                    } else {
                        validaAreaOrigCero = false;
                    }
                }

            }

        }

        //CONVENCIONALES
        //Valida que la cosntruccion ya se haya agregado antes a otro predio
        if (consConVigentes != null && !consConVigentes.isEmpty() &&
             !validaAreaOrigCero) {

            for (PUnidadConstruccion consConVigente : consConVigentes) {

                if (consConVigente.getUnidad() != null &&
                     !consConVigente.getUnidad().isEmpty() &&
                     conSeleccionada.getProvieneUnidad() != null &&
                     !conSeleccionada.getProvieneUnidad().isEmpty() &&
                     consConVigente.getPPredio() != null &&
                     consConVigente.getPPredio().getId() != null &&
                     conSeleccionada.getProvienePredio() != null &&
                     conSeleccionada.getProvienePredio().getId() != null) {

                    if (consConVigente.getUnidad().
                        equals(conSeleccionada.getProvieneUnidad()) &&
                         consConVigente.getPPredio().getId().
                            equals(conSeleccionada.getProvienePredio().getId()) &&
                         consConVigente.getTipoConstruccion().equals(conSeleccionada.
                            getTipoConstruccion())) {

                        //Compara la diferencia del area seleccionada y almacenada
                        //con el area de construccion vigente
                        difConSelyVigente = 0.0;

                        if (this.replicaUnidadConstruccionSeleccionada != null) {

                            difConSelyVigente = ((this.replicaUnidadConstruccionSeleccionada.
                                getAreaConstruida() +
                                 consConVigente.getAreaConstruida()) -
                                 conSeleccionada.getAreaConstruida());
                        }

                        if (difConSelyVigente < 0) {

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_007
                                .getMensajeUsuario());
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_007
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;

                        } else {

                            if (consPredioActual != null && !consPredioActual.isEmpty()) {

                                for (PUnidadConstruccion puc : consPredioActual) {

                                    //solo se valida cuando la construccion seleccionada sea la que se encuentra
                                    // en la listVigencia del predio actual
                                    if (conSeleccionada.getId().equals(puc.getId())) {

                                        //Se verifica si es la 1ra vez que se ingresa el area a la construccion
                                        puc.setAreaConstruida(conSeleccionada.getAreaConstruida());

                                        //Almacena area  a conSeleccionada
                                        this.getConservacionService().guardarProyeccion(this.
                                            getProyectarConservacionMB().getUsuario(),
                                            puc);

                                        puc = this.getConservacionService().
                                            actualizarPUnidadDeConstruccion(
                                                puc);

                                        //Se resta area a la construcccion vigente
                                        consConVigente.setAreaConstruida(difConSelyVigente);

                                        consConVigente = this.getConservacionService().
                                            actualizarPUnidadDeConstruccion(
                                                consConVigente);

                                    } else {
                                        //Verifica que la letra asignar en la unidad ya no haya sido asignada
                                        //en la listVigencia de construcciones asociadas
                                        if (conSeleccionada.getUnidad().equals(puc.getUnidad())) {

                                            this.addMensajeError(
                                                ECodigoErrorVista.MODIFICACION_PH_008
                                                    .getMensajeUsuario());
                                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_008
                                                .getMensajeTecnico());
                                            this.addErrorCallback();
                                            valido = false;
                                            return valido;
                                        }
                                    }

                                }

                                //Almacenamiento
                                this.getConservacionService().guardarProyeccion(this.
                                    getProyectarConservacionMB().getUsuario(),
                                    consConVigente);
                            }

                        }

                    }

                }
            }

        }

        //NO CONVENCIONALES
        List<PUnidadConstruccion> consPredioActualNoConv =
            this.unidadConstruccionNoConvencionalNuevas;

        List<PUnidadConstruccion> consNoConVigentes =
            this.unidadConstruccionComunesNoConvencionalEnglobeVirtual;

        //Valida que la cosntruccion ya se haya agregado antes a otro predio
        if (consNoConVigentes != null && !consNoConVigentes.isEmpty() &&
             !validaAreaOrigCero) {

            for (PUnidadConstruccion consNoConVigente : consNoConVigentes) {

                if (consNoConVigente.getUnidad() != null &&
                     !consNoConVigente.getUnidad().isEmpty() &&
                     conSeleccionada.getProvieneUnidad() != null &&
                     !conSeleccionada.getProvieneUnidad().isEmpty() &&
                     consNoConVigente.getPPredio() != null &&
                     consNoConVigente.getPPredio().getId() != null &&
                     conSeleccionada.getProvienePredio() != null &&
                     conSeleccionada.getProvienePredio().getId() != null) {

                    if (consNoConVigente.getUnidad().
                        equals(conSeleccionada.getProvieneUnidad()) &&
                         consNoConVigente.getPPredio().getId().
                            equals(conSeleccionada.getProvienePredio().getId()) &&
                         consNoConVigente.getTipoConstruccion().equals(conSeleccionada.
                            getTipoConstruccion())) {

                        //Compara la diferencia del area seleccionada y almacenada
                        //con el area de construccion vigente
                        difConSelyVigente = 0.0;
                        conSeleccionada.setAreaConstruida(this.getProyectarConservacionMB().
                            getAreaNoConvencional());

                        if (this.replicaUnidadConstruccionSeleccionada != null) {

                            difConSelyVigente = ((this.replicaUnidadConstruccionSeleccionada.
                                getAreaConstruida() +
                                 consNoConVigente.getAreaConstruida()) -
                                 conSeleccionada.getAreaConstruida());
                        }

                        if (difConSelyVigente < 0) {

                            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_007
                                .getMensajeUsuario());
                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_007
                                .getMensajeTecnico());
                            this.addErrorCallback();
                            valido = false;
                            return valido;

                        } else {

                            if (consPredioActualNoConv != null && !consPredioActualNoConv.isEmpty()) {

                                for (PUnidadConstruccion puc : consPredioActualNoConv) {

                                    //solo se valida cuando la construccion seleccionada sea la que se encuentra
                                    // en la listVigencia del predio actual
                                    if (conSeleccionada.getId().equals(puc.getId())) {

                                        //Se verifica si es la 1ra vez que se ingresa el area a la construccion
                                        puc.setAreaConstruida(conSeleccionada.getAreaConstruida());

                                        //Almacena area  a conSeleccionada
                                        this.getConservacionService().guardarProyeccion(this.
                                            getProyectarConservacionMB().getUsuario(),
                                            puc);

                                        //Se resta area a la construcccion vigente
                                        consNoConVigente.setAreaConstruida(difConSelyVigente);

                                        consNoConVigente = this.getConservacionService().
                                            actualizarPUnidadDeConstruccion(
                                                consNoConVigente);

                                    } else {
                                        //Verifica que la letra asignar en la unidad ya no haya sido asignada
                                        //en la listVigencia de construcciones asociadas
                                        if (conSeleccionada.getUnidad().equals(puc.getUnidad())) {

                                            this.addMensajeError(
                                                ECodigoErrorVista.MODIFICACION_PH_008
                                                    .getMensajeUsuario());
                                            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_008
                                                .getMensajeTecnico());
                                            this.addErrorCallback();
                                            valido = false;
                                            return valido;
                                        }
                                    }

                                }

                                //Almacenamiento
                                this.getConservacionService().guardarProyeccion(this.
                                    getProyectarConservacionMB().getUsuario(),
                                    consNoConVigente);
                            }

                        }

                    }

                }
            }

        }

        this.cargarConstruccionesEnglobeVirtual();

        return valido;
    }

    /**
     * Elimina construcciones nuevas para los tramites de englobe Virtual masiva.
     *
     * @author leidy.gonzalez
     */
    public void eliminarConstruccionesEnglobeVirtual(
        PUnidadConstruccion unidadConstruccionSeleccionada) {

        if (unidadConstruccionSeleccionada != null) {

            //Se verifica si se habia almacenado valor al area y se elimina
            if (unidadConstruccionSeleccionada.getAreaConstruida() != 0) {

                Double sumaAreaConstruccionOriginal = 0.0;

                if (this.unidadConstruccionOriginalesEnglobeVirtual != null &&
                     !this.unidadConstruccionOriginalesEnglobeVirtual.isEmpty()) {

                    for (PUnidadConstruccion pUnidadConstTemp
                        : this.unidadConstruccionOriginalesEnglobeVirtual) {

                        if ((pUnidadConstTemp.getPPredio().getPFichaMatrizs() != null &&
                             !pUnidadConstTemp.getPPredio().getPFichaMatrizs().isEmpty()) ||
                             (EPredioCondicionPropiedad.CP_5.getCodigo().equals(pUnidadConstTemp.
                                getPPredio().getCondicionPropiedad()) ||
                             EPredioCondicionPropiedad.CP_0.getCodigo().equals(pUnidadConstTemp.
                                getPPredio().getCondicionPropiedad())) &&
                             (EProyeccionCancelaInscribe.MODIFICA.getCodigo().equals(
                                pUnidadConstTemp.getCancelaInscribe()) ||
                             pUnidadConstTemp.getCancelaInscribe() == null)) {

                            //Se carga Unidades Convencionales Comunes
                            if (pUnidadConstTemp.getTipoConstruccion().trim().
                                equals(EUnidadTipoConstruccion.CONVENCIONAL.getCodigo())) {

                                if (pUnidadConstTemp.getAnioCancelacion() == null &&
                                     unidadConstruccionSeleccionada.getProvieneUnidad().
                                        equals(pUnidadConstTemp.getUnidad()) &&
                                     unidadConstruccionSeleccionada.getProvienePredio().getId().
                                        equals(pUnidadConstTemp.getPPredio().getId())) {

                                    sumaAreaConstruccionOriginal = 0.0;

                                    sumaAreaConstruccionOriginal = pUnidadConstTemp.
                                        getAreaConstruida() +
                                         unidadConstruccionSeleccionada.getAreaConstruida();

                                    pUnidadConstTemp.setAreaConstruida(sumaAreaConstruccionOriginal);

                                    this.getConservacionService().actualizarPUnidadDeConstruccion(
                                        pUnidadConstTemp);
                                }

                            }

                            //Se carga Unidades No Convencionales Comunes
                            if (pUnidadConstTemp.getTipoConstruccion().trim().
                                equals(EUnidadTipoConstruccion.NO_CONVENCIONAL.getCodigo())) {

                                if (pUnidadConstTemp.getAnioCancelacion() == null &&
                                     unidadConstruccionSeleccionada.getProvieneUnidad().
                                        equals(pUnidadConstTemp.getUnidad()) &&
                                     unidadConstruccionSeleccionada.getProvienePredio().getId().
                                        equals(pUnidadConstTemp.getPPredio().getId())) {

                                    sumaAreaConstruccionOriginal = 0.0;

                                    sumaAreaConstruccionOriginal = pUnidadConstTemp.
                                        getAreaConstruida() +
                                         unidadConstruccionSeleccionada.getAreaConstruida();

                                    pUnidadConstTemp.setAreaConstruida(sumaAreaConstruccionOriginal);

                                    this.getConservacionService().actualizarPUnidadDeConstruccion(
                                        pUnidadConstTemp);

                                }

                            }

                        }
                    }

                }

            }

            List<PUnidadConstruccion> pUnidadesConst = this.getConservacionService().
                obtenerPUnidadsConstruccionPorIdPredio(this.getProyectarConservacionMB().
                    getPredioSeleccionado().getId());

            PUnidadConstruccion conSeleccionadaBD = new PUnidadConstruccion();

            if (unidadConstruccionSeleccionada.getId() != null) {

                conSeleccionadaBD = this.getConservacionService().
                    buscarUnidadDeConstruccionPorSuId(unidadConstruccionSeleccionada.getId());
            }

            if (pUnidadesConst != null && !pUnidadesConst.isEmpty() &&
                 conSeleccionadaBD != null) {

                for (int i = 0; i <= pUnidadesConst
                    .size(); i++) {
                    if (pUnidadesConst.get(i)
                        .getId() != null &&
                         pUnidadesConst.get(i)
                            .getId().longValue() == unidadConstruccionSeleccionada.getId().
                            longValue()) {
                        pUnidadesConst.remove(i);
                        break;

                    }
                }
            }

            this.getProyectarConservacionMB().getPredioSeleccionado().getPUnidadConstruccions().
                remove(unidadConstruccionSeleccionada);

            this.getConservacionService().
                eliminarPunidadConstruccion(unidadConstruccionSeleccionada);

        }

        this.cargarConstruccionesEnglobeVirtual();

    }

    /**
     * Metodo que carga el historico de zonas en el tramite de englobe virtual
     *
     *
     * @author leidy.gonzalez
     */
    public void cargaHistoricoZonasPredio() {

        Calendar cal = Calendar.getInstance();
        int anioActual = cal.get(Calendar.YEAR);
        this.historicoPredioZonaSeleccionado = new ArrayList<PredioZona>();

        if (this.getProyectarConservacionMB().getPredioSeleccionado() != null) {

            this.predioSeleccionado = this.getProyectarConservacionMB().getPredioSeleccionado();

            List<PredioZona> historicoPredioZonaTemp = this.getConservacionService().
                obtenerZonasPorIdPredio(this.predioSeleccionado.getId());

            for (PredioZona predioZona : historicoPredioZonaTemp) {

                if (predioZona.getVigencia() != null) {

                    int anioVigencia = UtilidadesWeb
                        .getAnioDeUnDate(predioZona.getVigencia());

                    if (anioVigencia != anioActual) {
                        this.historicoPredioZonaSeleccionado.add(predioZona);
                    }

                } else {
                    this.historicoPredioZonaSeleccionado.add(predioZona);
                }

            }
        }
    }

    /**
     * Metodo que carga informacion de zonas vigentes en el tramite de englobe virtual
     * o de rectificación de área de terreno
     *
     * @author leidy.gonzalez
     */
    public void cargaZonasVigentesPredio() {
        this.zonasVigentesPredioSeleccionado = new ArrayList<PPredioZona>();
        this.zonasIntermediasDiferentes = new ArrayList<PPredioZona>();
        List<PPredioZona> zonasVigenciaActual =  new ArrayList<PPredioZona>();
        Map<Integer, List<PPredioZona>> zonasPorVigencia = new HashMap<Integer, List<PPredioZona>>();
        List<PPredioZona> zonasVigTemp = new ArrayList<PPredioZona>();
        List<Integer> zonasVigencias = new ArrayList<Integer>();
        this.actNuevaZona = false;
        this.editaZona = false;
        this.vigenciaSeleccionada = null;
        this.zonaFisicaSeleccionada = null;
        this.zonaGeoSeleccionada = null;
        this.habilitaHistYRegZonas = false;
        this.habilitaMsjHistYRegZonas = false;
        this.fechaActualizacionAvaluo =null;
        
 
       if (!this.getGeneralMB().validarExistenciaCopiaEdicion(this.tramite,
            ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId(), null)) {
            return;
        }

        if (!this.getGeneralMB().validarExistenciaCopiaEdicion(this.tramite,
            ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId(), null)) {
            return;
        }
        this.pPrediosAvaluosCatastrales = this.getFormacionService()
				.buscarAvaluosCatastralesPorIdPPredio(this.predioSeleccionado.getId());
        if(pPrediosAvaluosCatastrales!=null) {
        //LOGGER.debug("pPrediosAvaluosCatastrales "+pPrediosAvaluosCatastrales.size());
            for (VPPredioAvaluoDecreto vppad : this.pPrediosAvaluosCatastrales) {
				if(vppad.getActualizacion()!=null && vppad.getActualizacion().equals("SI")) {
				   this.fechaActualizacionAvaluo=vppad.getVigencia();
				   break;
				}
			}
        }
      //  LOGGER.debug("this.predioSeleccionado.getId() "+this.predioSeleccionado.getId());
        if ((this.getProyectarConservacionMB().getPredioSeleccionado() != null &&
             (EPredioCondicionPropiedad.CP_8.getCodigo().
                equals(this.getProyectarConservacionMB().getPredioSeleccionado().
                    getCondicionPropiedadNumeroPredial()) || this.getProyectarConservacionMB().getPredioSeleccionado().isEsPredioFichaMatriz()))
                // #67461 :: CC-NP-CO-129 Ajustar liquidación de rectificación área de terreno :: david.cifuentes
                || this.tramite.isRectificacionArea()){

            this.predioSeleccionado = this.getProyectarConservacionMB().getPredioSeleccionado();

            List<PPredioZona> zonasVigentesTemp = this.getConservacionService().
            		buscarZonasVigentes(this.predioSeleccionado.getId());
           // LOGGER.debug("zonasVigentesTemp "+zonasVigentesTemp.size()+" this.predioSeleccionado.getId() "+this.predioSeleccionado.getId());
            //se calcula la vigencia
            Calendar vigenciaTramite = Calendar.getInstance();
            vigenciaTramite.setTime(this.predioSeleccionado.getFechaInscripcionCatastral());
            
            Calendar vigenciaActual = Calendar.getInstance();
            vigenciaActual.setTime(this.predioSeleccionado.getFechaLog());
            
            Calendar vigenciaLimite = Calendar.getInstance();
            vigenciaLimite.setTime(this.predioSeleccionado.getFechaLog());
            
            if(this.fechaActualizacionAvaluo!=null) {
            	 vigenciaLimite.setTime(this.fechaActualizacionAvaluo);
            }
            if (vigenciaTramite != null &&
                    (vigenciaTramite.get(Calendar.YEAR) != vigenciaActual.get(Calendar.YEAR))) {
                    vigenciaTramite.add(Calendar.YEAR, 1);
            }
            for (PPredioZona pPredioZona : zonasVigentesTemp) {
         	   int vigencia =pPredioZona.getVigencia().getYear() + 1900;
         	 // LOGGER.debug("++ZonaFisica: "+pPredioZona.getZonaFisica()+" ZonaGeoeconomica  "+pPredioZona.getZonaGeoeconomica());
              
         	  if (vigencia > Integer.valueOf(vigenciaTramite.get(Calendar.YEAR)).intValue()  && vigencia < Integer.valueOf(vigenciaLimite.get(Calendar.YEAR)).intValue()) {
                if (!zonasPorVigencia.containsKey(vigencia)) {
                      List<PPredioZona> zonas = new ArrayList<PPredioZona>();
                      zonas.add(pPredioZona);
                      zonasPorVigencia.put(vigencia, zonas);
                      zonasVigencias.add(vigencia);
                  } else {
                      zonasPorVigencia.get(vigencia).add(pPredioZona);
                  }
                     
                 }else {
                	if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pPredioZona.getCancelaInscribe()) 
                			&& vigencia==Integer.valueOf(vigenciaActual.get(Calendar.YEAR)).intValue()) {
                 	 zonasVigenciaActual.add(pPredioZona);
                	 }  
               }
            }
            //LOGGER.debug("zonasPorVigencia "+zonasPorVigencia.keySet());
             
            
            if (zonasPorVigencia != null &&
                 !zonasPorVigencia.isEmpty()) {
	    	  for (Integer vigenciaMap  :zonasVigencias ) {
	    		List<PPredioZona> listaZonasIntermedias=   zonasPorVigencia.get(vigenciaMap);
	    	   if(!validarZonasIgualesSinorden(listaZonasIntermedias,zonasVigenciaActual) || validarExisteZonaTemp(listaZonasIntermedias)) {
	       		 for (PPredioZona pPredioZona : listaZonasIntermedias) {
                	int vigencia =pPredioZona.getVigencia().getYear() + 1900;
                 	// Se habilitan las zonas intermedias en cualquier estado 
                	 if(this.tramite.isRectificacionArea()){
                     //LOGGER.debug("agregando ZonaFisica: "+pPredioZona.getZonaFisica()+" ZonaGeoeconomica  "+pPredioZona.getZonaGeoeconomica());
                            if(!EProyeccionCancelaInscribe.CANCELA.getCodigo().equals(pPredioZona.getCancelaInscribe())) { 
                    		this.zonasVigentesPredioSeleccionado.add(pPredioZona);
	                        }
                            zonasVigTemp.add(pPredioZona);
                            this.zonasIntermediasDiferentes.add(pPredioZona);
                    	// }
                     }else if (pPredioZona.getCancelaInscribe() != null &&
                             EProyeccionCancelaInscribe.TEMP
                             .getCodigo().equals(pPredioZona.getCancelaInscribe())) {
                     	// if (vigencia > Integer.valueOf(vigenciaTramite.get(Calendar.YEAR)).intValue()  && vigencia < Integer.valueOf(vigenciaLog.get(Calendar.YEAR)).intValue()) {
                               if (pPredioZona.getArea() == 0) {
 	                              zonasVigTemp.add(pPredioZona);
 	                           }
                   //               LOGGER.debug("agregando ZonaFisica: "+pPredioZona.getZonaFisica()+" ZonaGeoeconomica  "+pPredioZona.getZonaGeoeconomica());
                               
 	                        this.zonasVigentesPredioSeleccionado.add(pPredioZona);
 	                        this.zonasIntermediasDiferentes.add(pPredioZona);
                      //	 }
                     }
                   }
	       	     }
	       	   }
            }

            int contador = 0;
            String msjZonasFichaMatrizTemp = new String();
            //LOGGER.debug("zonasVigentesPredioSeleccionado "+zonasVigentesPredioSeleccionado.size());
            
            if (this.zonasIntermediasDiferentes != null &&
                 !this.zonasIntermediasDiferentes.isEmpty()) {
                for (PPredioZona zonasVig : this.zonasIntermediasDiferentes) {

                    contador++;

                    int anioVigencia = UtilidadesWeb
                        .getAnioDeUnDate(zonasVig.getVigencia());

                    //Habilita boton consultar y registrar historico
                    this.habilitaHistYRegZonas = true;
                    this.habilitaMsjHistYRegZonas = true;

                    if (contador == 1) {

                        msjZonasFichaMatrizTemp = ECodigoErrorVista.MODIFICACION_PH_056
                            .getMensajeUsuario(zonasVig.getPPredio().getNumeroPredial(), String.
                                valueOf(anioVigencia));

                    } else if (contador > 1) {
                        msjZonasFichaMatrizTemp = msjZonasFichaMatrizTemp + "\n El predio" +
                            zonasVig.getPPredio().getNumeroPredial() +
                             " requiere el registro manual de zonas para la vigencia " + String.
                                valueOf(anioVigencia) +
                            " , por lo cual no puede realizarse el cálculo del avalúo, verifique.";
                    }

                }

                this.msjZonasFichaMatriz = msjZonasFichaMatrizTemp;
                
               

            }
            if(!this.habilitaHistYRegZonas && fechaActualizacionAvaluo!=null) {
            	if(Integer.valueOf(vigenciaLimite.get(Calendar.YEAR)).intValue()-Integer.valueOf(vigenciaTramite.get(Calendar.YEAR)).intValue()>1) {
            	   this.habilitaHistYRegZonas = true;
                   this.habilitaMsjHistYRegZonas = true;
            	}
            }

            if (zonasVigTemp.size() == 0 ||
                 zonasVigTemp.isEmpty()) {
                this.msjZonasFichaMatriz = new String();
                this.habilitaMsjHistYRegZonas = false;
            }

        }
    }
    
    
    /**
     * Valida si dos conjuntos de zonas son iguales en sus duplas sin importar el orden
     *
     *
     * @author vsocarras
     *
     */
    private boolean validarZonasIgualesSinorden(List<PPredioZona> zona1, List<PPredioZona> zona2) {
   	 
        boolean result = true;
        if(zona1.size()!=zona2.size())
        	  return false;
        for (PPredioZona ppz1 : zona1) {
            boolean existe = false;
            for (PPredioZona ppz2 : zona2) {
            	if (ppz1.getZonaFisica().equals(ppz2.getZonaFisica()) &&
                    ppz1.getZonaGeoeconomica().equals(ppz2.getZonaGeoeconomica())) {
                    existe = true;
                    break;
                }
            }
            if (!existe) {
                return false;
            }
        }
        
        

        return result;

    }
    
    private boolean validarExisteZonaTemp(List<PPredioZona> zonas) {
    	boolean validacion =false;
    	
    	for (PPredioZona pPredioZona : zonas) {
			if(pPredioZona.getCancelaInscribe()!=null && EProyeccionCancelaInscribe.TEMP
	                .getCodigo().equals(pPredioZona.getCancelaInscribe())) {
				validacion=true;
				break;
			}
		}
    	
    	return validacion;
    }

    /**
     * Metodo que carga informacion de zonas fisicas y geoeconomicas de acuerdo a la vigencia
     * seleccionada en el tramite de englobe virtual
     *
     * @author leidy.gonzalez
     */
    public void cargaZonasFisicaYGeo() {

        String tipoZona = null;

        if (!this.editaZona) {
            this.zonaFisicaSeleccionada = null;
            this.zonaGeoSeleccionada = null;
        }

        Calendar calendar = Calendar.getInstance();

        //Obtener zona del Predio seleccionado
        if (this.getProyectarConservacionMB().getPredioSeleccionado().getNumeroPredial() != null &&
             !this.getProyectarConservacionMB().getPredioSeleccionado().getNumeroPredial().isEmpty()) {

            tipoZona = this.getProyectarConservacionMB().getPredioSeleccionado().getNumeroPredial().
                substring(0, 7);
        }

        if (this.vigenciaSeleccionada != null &&
             !this.vigenciaSeleccionada.isEmpty() &&
             !tipoZona.isEmpty()) {

            //Llamar procedimiento almacenado que consulte las zonas: fisica y geoeconomicas
            //filtrado por vigencia y zona
            calendar.clear();
            calendar.set(Calendar.DATE, 1);
            calendar.set(Calendar.MONTH, 0);
            calendar.set(Calendar.YEAR, Integer.parseInt(this.vigenciaSeleccionada));

            Date vigenciaDate = calendar.getTime();
            LOGGER.debug("vigenciaDate:"+vigenciaDate+" tipoZona:"+tipoZona);

            //invocar procedimiento BD calculo de zonas 
            Object[] result = this.getConservacionService().calculoZonasFisicasYGeoeconomicas(
                vigenciaDate, tipoZona);

            if (result[0] != null) {
                ArrayList<Object> zonasFisica = (ArrayList) result[0];
                String zonaFisica = new String();

                if (zonasFisica != null && !zonasFisica.isEmpty()) {

                    for (Object zonaFisicaO : zonasFisica) {

                        if (((Object[]) zonaFisicaO).length > 0) {
                            zonaFisica = (((Object[]) zonaFisicaO)[0]).toString();
                            listZonaFisica.add(new SelectItem(zonaFisica, zonaFisica));
                        }

                    }

                }

            }

            if (result[1] != null) {
                String zonaGeo = new String();
                ArrayList<Object> zonasGeo = (ArrayList) result[1];

                if (zonasGeo != null && !zonasGeo.isEmpty()) {

                    for (Object zonasGeoO : zonasGeo) {

                        if (((Object[]) zonasGeoO).length > 0) {
                            zonaGeo = (((Object[]) zonasGeoO)[0]).toString();
                            listZonaGeo.add(new SelectItem(zonaGeo, zonaGeo));
                        }
                    }
                }

            }

            if (result[2] != null) {
                ArrayList<Object> errores = (ArrayList) result[2];
                String error = new String();

                if (errores != null && !errores.isEmpty()) {

                    for (Object errorO : errores) {

                        if (((Object[]) errorO).length > 0) {
                            error = (((Object[]) errorO)[0]).toString();
                            this.addMensajeError(ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(
                                error));
                            LOGGER.error("ErrorCargando Zona:"+ECodigoErrorVista.ERROR_DB_PROC.getMensajeUsuario(
                                    error));
                        }
                    }
                }

            }
        }
    }

    /**
     * Metodo para registrar zonas al predio seleccionado en el tramite de englobe virtual
     * o en tramites de rectificación de área
     *
     * @author leidy.gonzalez
     */
    public void editaZonasPredio() {

        this.actNuevaZona = true;
        this.editaZona = true;
        this.vigenciaSeleccionada = null;
        this.zonaFisicaSeleccionada = null;
        this.zonaGeoSeleccionada = null;
        this.listZonaFisica = new ArrayList<SelectItem>();
        this.listVigencia = new ArrayList<SelectItem>();
        this.listZonaGeo = new ArrayList<SelectItem>();

        if (this.zonaVigenteSeleccionada != null) {

            if (this.zonaVigenteSeleccionada.getVigencia() != null) {

                String anioVigencia = String.valueOf(UtilidadesWeb
                    .getAnioDeUnDate(this.zonaVigenteSeleccionada.getVigencia()));

                //Carga año de vigencia para modificar 
                this.listVigencia.add(new SelectItem(anioVigencia, String.valueOf(anioVigencia)));

                this.vigenciaSeleccionada = anioVigencia;
            }

            if (this.zonaVigenteSeleccionada.getZonaFisica() != null &&
                 !this.zonaVigenteSeleccionada.getZonaFisica().isEmpty()) {
                this.zonaFisicaSeleccionada = this.zonaVigenteSeleccionada.getZonaFisica();

                this.listZonaFisica.add(new SelectItem(this.zonaVigenteSeleccionada.getZonaFisica(),
                    this.zonaVigenteSeleccionada.getZonaFisica()));
            }

            if (this.zonaVigenteSeleccionada.getZonaGeoeconomica() != null &&
                 !this.zonaVigenteSeleccionada.getZonaGeoeconomica().isEmpty()) {

                this.zonaGeoSeleccionada = this.zonaVigenteSeleccionada.getZonaGeoeconomica();

                this.listZonaGeo.add(new SelectItem(this.zonaVigenteSeleccionada.
                    getZonaGeoeconomica(), this.zonaVigenteSeleccionada.getZonaGeoeconomica()));
            }

            if (this.zonaVigenteSeleccionada.getArea() != null) {

                this.areaNueva = this.zonaVigenteSeleccionada.getArea();
            }

            this.cargaZonasFisicaYGeo();
        }
    }

    /**
     * Metodo para registrar zonas al predio seleccionado en el tramite de englobe virtual
     * o trámites de rectificación de área
     *
     * @author leidy.gonzalez
     */
    public void registraZonasPredio() {
    // LOGGER.debug("------------registraZonasPredio--------------"+this.fechaActualizacionAvaluo);
        this.actNuevaZona = true;
        this.editaZona = false;
        this.vigenciaSeleccionada = null;
        this.zonaFisicaSeleccionada = null;
        this.zonaGeoSeleccionada = null;
        this.areaNueva = null;
        this.listVigencia = new ArrayList<SelectItem>();
        int anioVigencia = 0;
        Map<String, SelectItem> mapVigencia = new HashMap<String, SelectItem>(); //solo se usas para validar que no se repita an la listas
        
        if (this.zonasIntermediasDiferentes != null &&
             !this.zonasIntermediasDiferentes.isEmpty()) {
            for (PPredioZona zonasVig : this.zonasIntermediasDiferentes) {
                if(this.tramite.isRectificacionArea()) {

                    anioVigencia = UtilidadesWeb.getAnioDeUnDate(zonasVig.getVigencia());
                    if(!mapVigencia.containsKey(String.valueOf(anioVigencia))) {
                     mapVigencia.put(String.valueOf(anioVigencia), new SelectItem(anioVigencia));
                     this.listVigencia.add(new SelectItem(anioVigencia));
                    }
                } else {
                    if (zonasVig != null && zonasVig.getCancelaInscribe() != null
                            && EProyeccionCancelaInscribe.TEMP.getCodigo().equals(zonasVig.getCancelaInscribe())) {

                        anioVigencia = UtilidadesWeb.getAnioDeUnDate(zonasVig.getVigencia());
                        if(!mapVigencia.containsKey(String.valueOf(anioVigencia))) {
                         mapVigencia.put(String.valueOf(anioVigencia), new SelectItem(anioVigencia));
                         this.listVigencia.add(new SelectItem(anioVigencia));
                        }
                    }
                }
            }

            //Carga año de vigencia para modificar 
            /*for (String key : mapVigencia.keySet()) {
                mapVigencia.get(key);
                this.listVigencia.add(mapVigencia.get(key));
            }*/
           

        }
        if(listVigencia.isEmpty() && this.fechaActualizacionAvaluo!=null) {
        	Calendar fechaInscripcion = Calendar.getInstance();
        	fechaInscripcion.setTime(this.predioSeleccionado.getFechaInscripcionCatastral());
        	Calendar fechaActualizacion = Calendar.getInstance();
        	fechaActualizacion.setTime(this.fechaActualizacionAvaluo);
        	
        	int vigencia=Integer.valueOf(fechaInscripcion.get(Calendar.YEAR)).intValue()+2;
        	int vigenciaActualizacion=Integer.valueOf(fechaActualizacion.get(Calendar.YEAR)).intValue();
        	//LOGGER.debug("vigencia "+vigencia+" vigenciaActualizacion "+vigenciaActualizacion);
        	while(vigencia<vigenciaActualizacion) {
              this.listVigencia.add(new SelectItem(vigencia));
              vigencia++;
        	}
            
        }
    }

    /**
     * Metodo para registrar zonas al predio seleccionado en el tramite de englobe virtual
     *
     *
     * @author leidy.gonzalez
     */
    public void guardarNuevaZona() throws ParseException {

        this.actNuevaZona = true;
        Double valorUnidadTerreno = 0.0;
        Double avaluo = 0.0;
        Double valorM2Terreno = 0.0;
        Double areaTerreno = 0.0;
        Date vigenciaDate = null;
        PPredioZona pz = new PPredioZona();
        Calendar calendar = Calendar.getInstance();

        List<PPredioZona> zonasPredCondNueve = new ArrayList<PPredioZona>();

        if (this.areaNueva == 0.0) {

            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_057
                .getMensajeUsuario());
            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_057
                .getMensajeTecnico());
            this.addErrorCallback();
            return;
        }

        if (this.vigenciaSeleccionada == null ||
             this.vigenciaSeleccionada.isEmpty() ||
             this.zonaFisicaSeleccionada == null ||
             this.zonaFisicaSeleccionada.isEmpty() ||
             this.zonaGeoSeleccionada == null ||
             this.zonaGeoSeleccionada.isEmpty() ||
             this.areaNueva == null) {

            this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_041
                .getMensajeUsuario());
            LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_041
                .getMensajeTecnico());
            this.addErrorCallback();
            return;

        } else {

            if (!this.validarZonasPredio(this.zonaVigenteSeleccionada, this.areaNueva)) {
                return;
            }

            if (this.editaZona) {

                if (this.zonaVigenteSeleccionada != null) {

                    //Redondea a Dos cifras Area de Terreno
                    areaTerreno = Utilidades.redondearNumeroADosCifrasDecimales(Double.valueOf(
                        this.areaNueva));
                    this.areaNueva = areaTerreno;
                    zonaVigenteSeleccionada.setArea(areaTerreno);
                    zonaVigenteSeleccionada.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
                    zonaVigenteSeleccionada.setZonaFisica(this.zonaFisicaSeleccionada);
                    zonaVigenteSeleccionada.setZonaGeoeconomica(this.zonaGeoSeleccionada);
                    zonaVigenteSeleccionada.setPPredio(this.getProyectarConservacionMB().
                        getPredioSeleccionado());
                    //Se obtiene en formato fecha la vigencia

                    calendar.clear();
                    calendar.set(Calendar.DATE, 1);
                    calendar.set(Calendar.MONTH, 0);
                    calendar.set(Calendar.YEAR, Integer.parseInt(this.vigenciaSeleccionada));

                    vigenciaDate = calendar.getTime();
                    zonaVigenteSeleccionada.setVigencia(vigenciaDate);

                    //Calculo del valor del Terreno Procedimiento
                    valorUnidadTerreno = this.getConservacionService().
                        obtenerValorUnidadTerreno(this.getProyectarConservacionMB().
                            getPredioSeleccionado().getNumeroPredial().substring(0, 7),
                            this.getProyectarConservacionMB().getPredioSeleccionado().getDestino(),
                            zonaVigenteSeleccionada.getZonaGeoeconomica(), this.
                            getProyectarConservacionMB().getPredioSeleccionado().getId(),new Timestamp(calendar.getTime().getTime()));

                    zonaVigenteSeleccionada.setValorM2Terreno(valorUnidadTerreno);

                    //Si el predio es rural se convierte  de hectareas a mts2
                    if (this.getProyectarConservacionMB().getPredioSeleccionado().isPredioRural()) {
                        valorM2Terreno = zonaVigenteSeleccionada.getValorM2Terreno() / 10000;

                        zonaVigenteSeleccionada.setValorM2Terreno(valorM2Terreno);
                    }

                    //Calculo del Avaluo: Area * valorM2Terreno
                    avaluo = zonaVigenteSeleccionada.getArea() * zonaVigenteSeleccionada.
                        getValorM2Terreno();

                    zonaVigenteSeleccionada.setAvaluo(avaluo);

                    //Actualiza zona al almacenar
                    zonaVigenteSeleccionada = this.getConservacionService().guardarActualizarZona(
                        zonaVigenteSeleccionada);

                    zonasPredCondNueve.add(this.zonaVigenteSeleccionada);
                    
                    //Se actializa la zona al la proyecccion
                    for(PPredioZona pPredioZona :this.getProyectarConservacionMB().getPredioSeleccionado().getPPredioZonas()) {
                    	if(pPredioZona.getId().equals(zonaVigenteSeleccionada.getId())) {
                    		pPredioZona.setZonaFisica(zonaVigenteSeleccionada.getZonaFisica());
                    		pPredioZona.setZonaGeoeconomica(zonaVigenteSeleccionada.getZonaGeoeconomica());
                    		pPredioZona.setAvaluo(zonaVigenteSeleccionada.getAvaluo());
                    		pPredioZona.setArea(zonaVigenteSeleccionada.getArea());
                    		pPredioZona.setCancelaInscribe(zonaVigenteSeleccionada.getCancelaInscribe());
                    		pPredioZona.setValorM2Terreno(zonaVigenteSeleccionada.getValorM2Terreno());
                    		break;
                    	}
                    }

                    this.addMensajeInfo("La zona registrada ha sido almacenada correctamente");

                } else {

                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_067
                        .getMensajeUsuario());
                    LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_067
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return;
                }

            } else {

                //Redondea a Dos cifras Area de Terreno
                areaTerreno = Utilidades.redondearNumeroADosCifrasDecimales(Double.valueOf(
                    this.areaNueva));
                this.areaNueva = areaTerreno;
                pz.setArea(areaTerreno);
                pz.setZonaFisica(this.zonaFisicaSeleccionada);
                pz.setZonaGeoeconomica(this.zonaGeoSeleccionada);
                pz.setPPredio(this.getProyectarConservacionMB().getPredioSeleccionado());
                pz.setCancelaInscribe(EProyeccionCancelaInscribe.TEMP.getCodigo());
                //Se obtiene en formato fecha la vigencia
                calendar.clear();
                calendar.set(Calendar.DATE, 1);
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.YEAR, Integer.parseInt(this.vigenciaSeleccionada));

                vigenciaDate = calendar.getTime();
                pz.setVigencia(vigenciaDate);

                pz.setFechaInscripcionCatastral(this.getProyectarConservacionMB().
                    getPredioSeleccionado().getFechaInscripcionCatastral());
                pz.setFechaLog(new Date());
                pz.setUsuarioLog(this.getProyectarConservacionMB().getUsuario().getLogin());

                //Calculo del valor del Terreno Procedimiento
                valorUnidadTerreno = this.getConservacionService().
                    obtenerValorUnidadTerreno(this.getProyectarConservacionMB().
                        getPredioSeleccionado().getNumeroPredial().substring(0, 7),
                        this.getProyectarConservacionMB().getPredioSeleccionado().getDestino(),
                        pz.getZonaGeoeconomica(), this.getProyectarConservacionMB().
                        getPredioSeleccionado().getId(),new Timestamp(calendar.getTime().getTime()));

                pz.setValorM2Terreno(valorUnidadTerreno);

                //Si el predio es rural se convierte  de hectareas a mts2
                if (this.getProyectarConservacionMB().getPredioSeleccionado().isPredioRural()) {

                    valorM2Terreno = pz.getValorM2Terreno() / 10000;

                    pz.setValorM2Terreno(valorM2Terreno);
                }

                //Calculo del Avaluo sin Procedimiento: Area*ValorM2Terreno
                avaluo = pz.getArea() * pz.getValorM2Terreno();

                pz.setAvaluo(avaluo);

                pz = this.getConservacionService().guardarActualizarZona(pz);

                //Se almacena la nueva zona ingresada
                //this.zonasVigentesPredioSeleccionado.add(pz);

                zonasPredCondNueve.add(pz);

                this.addMensajeInfo("La zona registrada ha sido almacenada correctamente");
            }

            //Se verifica si el Predio es Ficha Matriz para almacenar las zonas a los predios de Con 9
            if (this.getProyectarConservacionMB().getPredioSeleccionado().isEsPredioFichaMatriz()) {

                //Consulta todos los predios condicion 9 del tramite
                List<PPredio> prediosCond9 = this.getConservacionService().
                    buscarPprediosCond9PorIdTramite(this.tramite.getId());

                if (prediosCond9 != null && !prediosCond9.isEmpty() &&
                     zonasPredCondNueve != null && !zonasPredCondNueve.isEmpty()) {

                    this.asociarZonasPredioFMCond9(zonasPredCondNueve, prediosCond9);
                }

            }

            //Se recalcula el avaluo retroactivo para la ficha matriz
            if (this.predioSeleccionado != null && this.predioSeleccionado.isEsPredioFichaMatriz()) {
                this.calcularAvaluoRetractivoPredio(this.predioSeleccionado, this.tramite.getId(),
                    this.usuario);
            }

        }

        cargaZonasVigentesPredio();
        this.limpiarCamposZonas();
    }

    /**
     * Función encargada de eliminar la PPredioZonas que se encuentre seleccionada
     *
     * @author leidy.gonzalez
     */
    public void eliminarPZona() {
        try {
            LOGGER.info("eliminarPZona");
            //Variable para contar la cantidad registros que existen por vigencia
            int contador = 0;
            //Variable para vigencia seleccionada
            Calendar cSel = Calendar.getInstance();
            cSel.setTime(this.zonaVigenteSeleccionada.getVigencia());
            int vigSel = cSel.get(Calendar.YEAR);

            //Valida si el registro es el ultimo en P_PREDIO_ZONA para la vigencia seleccionada
            for (PPredioZona pPredioZona : this.zonasVigentesPredioSeleccionado) {

                //Se verifica la sumatoria de la vigencia seleccionada
                Calendar c = Calendar.getInstance();
                c.setTime(pPredioZona.getVigencia());
                int vigenciaZona = c.get(Calendar.YEAR);

                if (vigSel == vigenciaZona) {
                    contador++;
                }
            }

            //Si solo queda un registro se actualiza el area seleccionada dejandola en 0
            //De lo contrario se elimina el registro
            if (contador == 1) {

                this.zonaVigenteSeleccionada.setArea(0d);
                this.zonaVigenteSeleccionada.setZonaFisica("00");
                this.zonaVigenteSeleccionada.setZonaGeoeconomica("00");
                this.zonaVigenteSeleccionada.setAvaluo(0d);
                this.zonaVigenteSeleccionada.setValorM2Terreno(0d);

                this.zonaVigenteSeleccionada = this.getConservacionService().guardarActualizarZona(
                    this.zonaVigenteSeleccionada);

            } else if (contador > 1) {
                this.getConservacionService().eliminarZona(this.zonaVigenteSeleccionada);
                this.getProyectarConservacionMB().getPredioSeleccionado().getPPredioZonas().remove(
                    this.zonaVigenteSeleccionada);
                this.zonasVigentesPredioSeleccionado.remove(this.zonaVigenteSeleccionada);

                for (int i = 0; i < this.zonasVigentesPredioSeleccionado
                    .size(); i++) {
                    if (this.zonasVigentesPredioSeleccionado.get(i)
                        .getId().longValue() == this.zonaVigenteSeleccionada.getId().longValue()) {
                        this.zonasVigentesPredioSeleccionado.remove(i);

                    }
                }
            }

            this.addMensajeInfo("La zona ha sido eliminada correctamente");

            //Se recarga la lista    
            this.cargaZonasVigentesPredio();

            // ACTUALIZAR AVALÚO
//			if (this.banderaPredioFiscal) {
//				this.guardarAvaluoPredioFiscal();
//			} else {
//				this.recalcularAvaluo(true);
//			}
//			// CONSULTAR PREDIO ACTUALIZADO
//			if (this.getProyectarConservacionMB().getPredioSeleccionado() != null) {
//				this.getProyectarConservacionMB().getPredioSeleccionado() = this.getConservacionService()
//						.obtenerPPredioCompletoById(
//								this.getProyectarConservacionMB().getPredioSeleccionado().getId());
//			}
//			this.setAvaluo();
//			// Se actualiza el valor de las áreas
//			this.guardarAreas();
        } catch (Exception e) {
            this.addMensajeError(e);
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Metodo para limpiar campos de zonas
     *
     * @author leidy.gonzalez
     */
    public void limpiarCamposZonas() {

        this.actNuevaZona = false;
        this.editaZona = false;
        this.vigenciaSeleccionada = null;
        this.zonaFisicaSeleccionada = null;
        this.zonaGeoSeleccionada = null;
        this.areaNueva = null;
        this.listZonaGeo = new ArrayList<SelectItem>();
        this.listVigencia = new ArrayList<SelectItem>();
        this.listZonaFisica = new ArrayList<SelectItem>();
    }

    public boolean obtenerReplicaConstruccionSeleccionada(
        PUnidadConstruccion unidadConstruccionSeleccionada) {

        this.replicaUnidadConstruccionSeleccionada = new PUnidadConstruccion();

        if (unidadConstruccionSeleccionada != null) {

            this.replicaUnidadConstruccionSeleccionada =
                (PUnidadConstruccion) unidadConstruccionSeleccionada
                    .clone();

            return true;

        } else {
            return false;
        }

    }

    /**
     * Metodo para validar zonas de predio
     *
     * @author leidy.gonzalez
     */
    private boolean validarZonasPredio(PPredioZona zonasVigentesPredioSeleccionado, Double areaNueva) {
        boolean retorno = true;
        Double areaTotal = 0d;
        Double areaVigenciaActual = 0d;
        this.diferenciaAreas = 0d;

        //Variable para vigencia seleccionada
        int vigSel = Integer.parseInt(this.vigenciaSeleccionada);
        //Variable para vigencia actual
        int vigenciaActual = Calendar.getInstance().get(Calendar.YEAR);
        //Variable para diferencia de Area
        this.diferenciaAreas = this.getGeneralesService().getCacheParametroPorNombre(
            EParametro.DIFERENCIA_AREA_TERRENO_PERMITIDA.name()).getValorNumero();

        if (!this.editaZona) {
            areaTotal = areaNueva;

        }
        //Valida Zonas del predio seleccionado
        //CONSULTA ZONAS DEL PREDIO
        List<PPredioZona> zonas = this.getConservacionService().
            buscarZonasNoCanceladaPorPPredioId(this.getProyectarConservacionMB().getPredioSeleccionado().getId());
        if (zonas != null && !zonas.isEmpty()) {

            for (PPredioZona zona : zonas) {
            	 
                //Se verifica la sumatoria de la vigencia seleccionada
                Calendar c = Calendar.getInstance();
                c.setTime(zona.getVigencia());
                int vigenciaZona = c.get(Calendar.YEAR);
                
                //Valida que no exista una zona fisica y geoeconimica ya registrada para la misma vigencia
                if (!this.editaZona &&
                     zona.getZonaFisica().equals(this.zonaFisicaSeleccionada) &&
                     zona.getZonaGeoeconomica().equals(this.zonaGeoSeleccionada) &&
                     vigSel == vigenciaZona) {

                    this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_072.getMensajeUsuario(
                        String.valueOf(vigSel),
                        String.valueOf(vigSel)));
                    LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_072
                        .getMensajeTecnico());
                    this.addErrorCallback();
                    return retorno = false;
                }

                if (vigSel == vigenciaZona) {

                    if (this.editaZona) {
                    	
                        if (zonasVigentesPredioSeleccionado.getId().equals(zona.getId())) {
                            areaTotal = areaTotal + areaNueva;
                        } else {
                            areaTotal += zona.getArea();
                        }
                        LOGGER.debug("zona.getId:"+zona.getId()+" areaTotal: "+areaTotal);
                        
                    } else {
                        areaTotal += zona.getArea();
                    }

                }

                //Se verifica la sumatoria del area de la vigencia actual
                if (!EProyeccionCancelaInscribe.CANCELA.getCodigo().
                    equals(zona.getCancelaInscribe()) &&
                     vigenciaActual == vigenciaZona) {

                    areaVigenciaActual += zona.getArea();
                }
            }

        }

        //Valida diferencia de areas
        if (areaVigenciaActual != null) {
        	areaVigenciaActual = Utilidades.redondearNumeroADosCifrasDecimales(areaVigenciaActual);
            areaTotal = Utilidades.redondearNumeroADosCifrasDecimales(areaTotal);
            int comparacionAreas = Double.compare(areaTotal, areaVigenciaActual);
            if (comparacionAreas > 0) {
                this.addMensajeError(ECodigoErrorVista.MODIFICACION_PH_076.getMensajeUsuario(String.
                    valueOf(vigSel)));
                LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_076
                    .getMensajeTecnico());
                this.addErrorCallback();
                return retorno = false;
            }

            if (comparacionAreas < 0) {

                this.addMensajeWarn(ECodigoErrorVista.MODIFICACION_PH_073.getMensajeUsuario(String.
                    valueOf(vigSel), String.valueOf(vigSel)));
                LOGGER.warn(ECodigoErrorVista.MODIFICACION_PH_073
                    .getMensajeTecnico());
                return retorno = true;

            }

        }
        return retorno = true;
    }

    /**
     * Metodo que permite insertar la Unidades de Construccion Componente y las P Fotos para la
     * asociacion de una nueva construccion
     *
     * @author leidy.gonzalez
     */
    public void asociarConstruccionCompYPFotosUnidadesAsocidas(PUnidadConstruccion pUnidadNueva,
        PUnidadConstruccion pVigenteSelect) {

        //Se consulta unidad nueva en la Base de Datos
        PUnidadConstruccion pUnidadNuevaTemp = this.getConservacionService().
            obtenerPUnidadsConstruccionPorIdPredioYUnidad(pUnidadNueva.getPPredio().getId(),
                pUnidadNueva.getUnidad());

        //Variables para consulta de Construcciones componentes y fotos asociadas a la unidad
        //de construccion de la que proviene la construccion seleccionada
        List<PUnidadConstruccionComp> unidadesConstComp = new ArrayList<PUnidadConstruccionComp>();
        List<PFoto> pFotos = new ArrayList<PFoto>();
        //Variables en las que se almacenara las fotos y componentes a asociar a la Unidad de Construccion
        //Seleccionada
        List<PFoto> pNuevasFotos = new ArrayList<PFoto>();
        List<PUnidadConstruccionComp> unidadesNuevasConstComp =
            new ArrayList<PUnidadConstruccionComp>();

        //Consulta P Unidades Construccion Componente
        unidadesConstComp =
            this.getConservacionService().buscarUnidadDeConstruccionPorUnidadConstruccionId(
                pVigenteSelect.getId());

        pFotos = this.getConservacionService().buscarPFotosUnidadConstruccion(
            pVigenteSelect.getId(), false);

        //Inserta PUnidadConstruccionComp
        if (unidadesConstComp != null && !unidadesConstComp.isEmpty()) {
            for (PUnidadConstruccionComp pUnidadConstruccionCompTemp : unidadesConstComp) {

                PUnidadConstruccionComp pUnidadConstruccionComp = new PUnidadConstruccionComp();
                pUnidadConstruccionComp.setId(null);
                pUnidadConstruccionComp.setComponente(pUnidadConstruccionCompTemp.getComponente());
                pUnidadConstruccionComp.setElementoCalificacion(pUnidadConstruccionCompTemp.
                    getElementoCalificacion());
                pUnidadConstruccionComp.setDetalleCalificacion(pUnidadConstruccionCompTemp.
                    getDetalleCalificacion());
                pUnidadConstruccionComp.setPuntos(pUnidadConstruccionCompTemp.getPuntos());
                pUnidadConstruccionComp.setFechaLog(new Date(System.currentTimeMillis()));
                pUnidadConstruccionComp.setUsuarioLog(pUnidadConstruccionCompTemp.getUsuarioLog());
                pUnidadConstruccionComp.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                    .getCodigo());
                pUnidadConstruccionComp.setPUnidadConstruccion(pUnidadNuevaTemp);

                unidadesNuevasConstComp.add(pUnidadConstruccionComp);
            }

            if (unidadesNuevasConstComp != null && !unidadesNuevasConstComp.isEmpty()) {
                this.getConservacionService()
                    .insertarMultiplePUnidadConstruccionComp(unidadesNuevasConstComp);
            }
        }

        //Inserta  PFoto
        if (pFotos != null && !pFotos.isEmpty()) {

            for (PFoto pFotoTemp : pFotos) {

                PFoto pFoto = new PFoto();

                pFoto.setPPredio(this.predioSeleccionado);
                pFoto.setPUnidadConstruccion(pUnidadNuevaTemp);
                pFoto.setTramiteId(this.tramite.getId());
                pFoto.setTipo(pFotoTemp.getTipo());
                pFoto.setDescripcion(pFotoTemp.getDescripcion());
                pFoto.setFecha(new Date(System.currentTimeMillis()));
                pFoto.setUsuarioLog(pFotoTemp.getUsuarioLog());
                pFoto.setCancelaInscribe(EProyeccionCancelaInscribe.INSCRIBE
                    .getCodigo());
                pFoto.setFechaLog(new Date(System.currentTimeMillis()));
                pFoto.setDocumento(pFotoTemp.getDocumento());

                pNuevasFotos.add(pFoto);

            }
            if (pNuevasFotos != null && !pNuevasFotos.isEmpty()) {
                this.getConservacionService().guardarPFotos(pNuevasFotos);
            }
        }

    }

    /**
     * Metodo que permite insertar las Zonas que se encuentran modificando manualmente a los predios
     * cond 9 de la FM
     *
     * @author leidy.gonzalez
     */
    public void asociarZonasPredioFMCond9(List<PPredioZona> zonasPredCondNueve,
        List<PPredio> prediosCond9) {

        List<PPredioZona> nuevasZonasCond9 = new ArrayList<PPredioZona>();
        List<PFichaMatrizPredio> fichaMatrizPredios = this.getFichaMatrizMB().
            getFichaMatrizPredios();

        for (PPredio pPredio : prediosCond9) {

            for (PPredioZona pPredioZona : zonasPredCondNueve) {

                if (pPredioZona.getPPredio() != null &&
                     pPredioZona.getPPredio().getId() ==
                    pPredio.getId()) {

                    Double areaTerreno = Utilidades.redondearNumeroADosCifrasDecimales(Double.
                        valueOf(this.areaNueva));

                    if (fichaMatrizPredios != null && !fichaMatrizPredios.isEmpty()) {
                        for (PFichaMatrizPredio pFichaMatrizPredio : fichaMatrizPredios) {
                            if (pPredio.getNumeroPredial().equals(pFichaMatrizPredio.
                                getNumeroPredial())) {

                                pPredioZona.setArea(areaTerreno * pFichaMatrizPredio.
                                    getCoeficiente());
                                //Redondear a 1000
                                //Calculo del Avaluo sin Procedimiento: Area*ValorM2Terreno
                                Double avaluo = pPredioZona.getArea() * pPredioZona.
                                    getValorM2Terreno();

                                pPredioZona.setAvaluo(avaluo);

                                pPredioZona = this.getConservacionService().guardarActualizarZona(
                                    pPredioZona);
                            }
                        }
                    }

                } else {

                    PPredioZona pPredZonaNuev = new PPredioZona();
                    pPredZonaNuev.setId(null);

                    pPredZonaNuev.setZonaFisica(pPredioZona.getZonaFisica());
                    pPredZonaNuev.setZonaGeoeconomica(pPredioZona.getZonaGeoeconomica());
                    pPredZonaNuev.setPPredio(pPredio);
                    pPredZonaNuev.setCancelaInscribe(pPredioZona.getCancelaInscribe());

                    pPredZonaNuev.setVigencia(pPredioZona.getVigencia());

                    pPredZonaNuev.setFechaInscripcionCatastral(pPredioZona.
                        getFechaInscripcionCatastral());
                    pPredZonaNuev.setFechaLog(new Date());
                    pPredZonaNuev.setUsuarioLog(pPredioZona.getUsuarioLog());

                    pPredZonaNuev.setValorM2Terreno(pPredioZona.getValorM2Terreno());

                    if (fichaMatrizPredios != null && !fichaMatrizPredios.isEmpty()) {
                        for (PFichaMatrizPredio pFichaMatrizPredio : fichaMatrizPredios) {
                            //verifica el coeficiente de copropiedad del predio
                            if (pPredio.getNumeroPredial().equals(pFichaMatrizPredio.
                                getNumeroPredial())) {

                                pPredZonaNuev.setArea(pPredioZona.getArea() * pFichaMatrizPredio.
                                    getCoeficiente());
                                //Redondear a 1000
                                //Calculo del Avaluo sin Procedimiento: Area*ValorM2Terreno
                                Double avaluo = pPredZonaNuev.getArea() * pPredioZona.
                                    getValorM2Terreno();

                                pPredZonaNuev.setAvaluo(avaluo);

                                nuevasZonasCond9.add(pPredZonaNuev);
                            }
                        }
                    }

                }

            }
        }

        if (nuevasZonasCond9 != null && !nuevasZonasCond9.isEmpty()) {
            this.getConservacionService().guardarListaPPredioZona(nuevasZonasCond9);
        }

    }

    /**
     * Calcula los avaluos retroactivos para el predio dado
     *
     * @author felipe.cadena
     * @param predio
     */
    private void calcularAvaluoRetractivoPredio(PPredio predio, Long tramiteId, UsuarioDTO usuario) {

        Object[] result = this.getFormacionService()
            .liquidarAvaluosParaUnPredio(tramiteId,
                predio.getId(),
                predio.getFechaInscripcionCatastral(), usuario);

        this.procesarProcedimientoDB(result,
            "Se procesó correctamente el cálculo de los avalúos para este predio.");

    }
}
