/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import java.util.Date;

/**
 * DTO de la clase TramitePredioEnglobe
 *
 * @author juanfelipe.garcia
 */
public class TramitePredioEnglobeDTO implements java.io.Serializable {

    private static long serialVersionUID = 3956227729553241947L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    // Fields
    private Long id;
    private Tramite tramite;
    private Predio predio;
    private String englobePrincipal;
    private String usuarioLog;
    private Date fechaLog;

    // Constructors
    /** default constructor */
    public TramitePredioEnglobeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tramite getTramite() {
        return tramite;
    }

    public void setTramite(Tramite tramite) {
        this.tramite = tramite;
    }

    public Predio getPredio() {
        return predio;
    }

    public void setPredio(Predio predio) {
        this.predio = predio;
    }

    public String getEnglobePrincipal() {
        return englobePrincipal;
    }

    public void setEnglobePrincipal(String englobePrincipal) {
        this.englobePrincipal = englobePrincipal;
    }

    public String getUsuarioLog() {
        return usuarioLog;
    }

    public void setUsuarioLog(String usuarioLog) {
        this.usuarioLog = usuarioLog;
    }

    public Date getFechaLog() {
        return fechaLog;
    }

    public void setFechaLog(Date fechaLog) {
        this.fechaLog = fechaLog;
    }
}
