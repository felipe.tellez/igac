package co.gov.igac.snc.web.mb.avaluos.administracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.avaluos.ProfesionalAvaluo;
import co.gov.igac.snc.persistence.util.EProfesionalTipoVinculacion;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.FiltroDatosConsultaProfesionalAvaluos;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * ManagedBean del caso de uso 095 de avalúos
 *
 * @author christian.rodriguez
 */
@Component("adminProfesionalAvaluosNomina")
@Scope("session")
public class AdminProfesionalAvaluosNominaMB extends SNCManagedBean implements
    Serializable {

    private static final long serialVersionUID = 2734396405771696317L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdminProfesionalAvaluosNominaMB.class);

    /**
     * Filtro usado para la consutla de profesionales
     */
    private FiltroDatosConsultaProfesionalAvaluos filtroProfesional;

    /**
     * Lista con los profesionales encontrados que se van a mostrar en la tabla de resultados
     */
    private List<ProfesionalAvaluo> profesionales;

    /**
     * Bandera que determina si se muestran todas las columnas las de la tabla de profesionales
     */
    private boolean banderaTablaExpandida;

    /**
     * Variable que contiene el profesional de avalúos que se está editando o creando
     */
    private ProfesionalAvaluo profesionalSeleccionado;

    /**
     * Variable que almacena el código de la territorial seleccionada
     */
    private String territorialSeleccionadaCodigo;

    /**
     * Variable que almacena el código del departamento seleccionado
     */
    private String departamentoSeleccionadoCodigo;

    /**
     * Variable que almacena el código del municipio seleccionado
     */
    private String municipioSeleccionadoCodigo;

    /**
     * Variable que almacena el código de la profesion seleccionada
     */
    private String profesionSeleccionadaCodigo;

    /**
     * Lista de items para el combo de Departamento
     */
    private ArrayList<SelectItem> departamentosItemList;

    /**
     * Lista de items para el combo de Departamento
     */
    private ArrayList<SelectItem> municipiosItemList;

    /**
     * usuario logeado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Variables temporales usadas al momento de validar si ya existe un profesional de avaluos con
     * un numero y tipo de identificación
     */
    private String numIdentificacionTemp;
    private String tipoIdentificacionTemp;

    // --------------------------------Banderas---------------------------------
    // ----------------------------Métodos SET y GET----------------------------
    public FiltroDatosConsultaProfesionalAvaluos getFiltroProfesional() {
        return this.filtroProfesional;
    }

    public void setFiltroProfesional(
        FiltroDatosConsultaProfesionalAvaluos filtroProfesional) {
        this.filtroProfesional = filtroProfesional;
    }

    public List<ProfesionalAvaluo> getProfesionales() {
        return this.profesionales;
    }

    public boolean isBanderaTablaExpandida() {
        return this.banderaTablaExpandida;
    }

    public void setBanderaTablaExpandida(boolean banderaTablaExpandida) {
        this.banderaTablaExpandida = banderaTablaExpandida;
    }

    public ProfesionalAvaluo getProfesionalSeleccionado() {
        return this.profesionalSeleccionado;
    }

    public void setProfesionalSeleccionado(
        ProfesionalAvaluo profesionalSeleccionado) {
        this.profesionalSeleccionado = profesionalSeleccionado;
    }

    public String getTerritorialSeleccionadaCodigo() {
        return this.territorialSeleccionadaCodigo;
    }

    public void setTerritorialSeleccionadaCodigo(
        String territorialSeleccionadaCodigo) {
        this.territorialSeleccionadaCodigo = territorialSeleccionadaCodigo;
    }

    public String getDepartamentoSeleccionadoCodigo() {
        return this.departamentoSeleccionadoCodigo;
    }

    public void setDepartamentoSeleccionadoCodigo(
        String departamentoSeleccionadoCodigo) {
        this.departamentoSeleccionadoCodigo = departamentoSeleccionadoCodigo;
    }

    public String getMunicipioSeleccionadoCodigo() {
        return this.municipioSeleccionadoCodigo;
    }

    public void setMunicipioSeleccionadoCodigo(
        String municipioSeleccionadoCodigo) {
        this.municipioSeleccionadoCodigo = municipioSeleccionadoCodigo;
    }

    public String getProfesionSeleccionadaCodigo() {
        return this.profesionSeleccionadaCodigo;
    }

    public void setProfesionSeleccionadaCodigo(
        String profesionSeleccionadaCodigo) {
        this.profesionSeleccionadaCodigo = profesionSeleccionadaCodigo;
    }

    public ArrayList<SelectItem> getDepartamentosItemList() {
        return this.departamentosItemList;
    }

    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    // -----------------------Métodos SET y GET banderas------------------------
    // ---------------------------------Métodos---------------------------------
    @PostConstruct
    public void init() {
        LOGGER.debug("on AdministracionProfesionalAvaluosNominaMB init");
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.filtroProfesional = new FiltroDatosConsultaProfesionalAvaluos();
    }

    // ----------------------------------------------------------------------
    /**
     * Listener con el método que realiza la consulta de los profesionales dependiendo de los
     * filtros ingresados por el usuario
     *
     * @author christian.rodriguez
     */
    public void buscar() {

        if (filtroProfesional != null) {

            this.profesionales = this
                .getAvaluosService()
                .buscarProfesionalesAvaluosPorFiltro(this.filtroProfesional);
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se encarga de eliminar un profesional de avalúos siempre y cuando no tenga
     * asignaciones de avalúos o tareas a realizar
     *
     * @author christian.rodriguez
     */
    public void eliminarProfesional() {

        if (this.profesionalSeleccionado != null) {

            boolean profesionalEliminado = this.getAvaluosService()
                .eliminarProfesionalAvaluos(this.profesionalSeleccionado);

            if (profesionalEliminado) {
                this.addMensajeInfo("El profesional " +
                     this.profesionalSeleccionado.getTipoIdentificacion() +
                     " " +
                     this.profesionalSeleccionado
                        .getNumeroIdentificacion() +
                     " fue eliminado satisfactóriamente.");

                this.buscar();
            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError(
                    "Este avaluador tiene a su cargo actividades, por lo tanto no puede eliminarse");
                context.addCallbackParam("error", "error");
            }
        }
    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se encarga de guardar/actualizar el profesional seleccionado
     *
     * @author christian.rodriguez
     */
    public void guardarProfesional() {
        if (this.profesionalSeleccionado != null &&
             this.validarProfesionalExistente()) {

            this.ajustarObjetoProfesional();

            this.profesionalSeleccionado = this.getAvaluosService()
                .actualizarProfesionalAvaluos(this.profesionalSeleccionado,
                    this.profesionSeleccionadaCodigo, this.usuario);

            if (this.profesionalSeleccionado != null &&
                 this.profesionalSeleccionado.getId() != null) {

                this.addMensajeInfo("El profesional " +
                     this.profesionalSeleccionado.getTipoIdentificacion() +
                     " " +
                     this.profesionalSeleccionado
                        .getNumeroIdentificacion() +
                     " fue guardado satisfactóriamente.");

                this.buscar();

            } else {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError("No fue posible guardar el profesional.");
                context.addCallbackParam("error", "error");
            }
        }
    }

    // --------------------------------------------------------------------------
    /**
     * Método que se encarga de rellenar el objeto de profesional con los datos ingresados por el
     * usuario
     *
     * @author christian.rodriguez
     */
    private void ajustarObjetoProfesional() {

        EstructuraOrganizacional territorial = this.getGeneralesService()
            .buscarEstructuraOrganizacionalPorCodigo(
                this.territorialSeleccionadaCodigo);
        Departamento depto = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(
                this.departamentoSeleccionadoCodigo);
        Municipio municipio = this.getGeneralesService()
            .getCacheMunicipioByCodigo(this.municipioSeleccionadoCodigo);

        this.profesionalSeleccionado.setTerritorial(territorial);
        this.profesionalSeleccionado.setDireccionDepartamento(depto);
        this.profesionalSeleccionado.setDireccionMunicipio(municipio);
        this.profesionalSeleccionado.setActivo(ESiNo.SI.getCodigo());
        this.profesionalSeleccionado
            .setTipoVinculacion(EProfesionalTipoVinculacion.CARRERA_ADMINISTRATIVA
                .getCodigo());
        this.profesionalSeleccionado.setCalificacionPromedio(0L);
    }

    // ----------------------------------------------------------------------
    /**
     * listener que inicializar los valores necesarios para agregar un nuevo profesional
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaCrearProfesional() {
        this.profesionalSeleccionado = new ProfesionalAvaluo();

        this.territorialSeleccionadaCodigo = "";

        this.actualizarListaDepartamentos();
        this.actualizarListaMunicipios();

        this.numIdentificacionTemp = "";
        this.tipoIdentificacionTemp = "";

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que carga los valores predeterminados para editar un profesional de avaluos
     *
     * @author christian.rodriguez
     */
    public void cargarVentanaEditarProfesional() {

        this.tipoIdentificacionTemp = this.profesionalSeleccionado
            .getTipoIdentificacion();
        this.numIdentificacionTemp = this.profesionalSeleccionado
            .getNumeroIdentificacion();

        this.territorialSeleccionadaCodigo = this.profesionalSeleccionado
            .getTerritorial().getCodigo();

        this.actualizarListaDepartamentos();
        this.departamentoSeleccionadoCodigo = this.profesionalSeleccionado
            .getDireccionDepartamento().getCodigo();

        this.actualizarListaMunicipios();
        this.municipioSeleccionadoCodigo = this.profesionalSeleccionado
            .getDireccionMunicipio().getCodigo();

        this.profesionSeleccionadaCodigo = String
            .valueOf(this.profesionalSeleccionado.getProfesion().getId());

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que actualiza la lista de departamentos disponibles para seleccionar dependiendo de
     * la territorial seleccionada
     *
     * @author christian.rodriguez
     */
    public void actualizarListaDepartamentos() {

        this.departamentoSeleccionadoCodigo = "";

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.territorialSeleccionadaCodigo != null) {
            List<Departamento> departamentos = this.getGeneralesService()
                .getCacheDepartamentosPorTerritorial(
                    this.territorialSeleccionadaCodigo);

            for (Departamento departamento : departamentos) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }

        this.actualizarListaMunicipios();

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que actualiza la lista de municipio disponibles para seleccionar dependiendo del
     * departamento seleccionado
     *
     * @author christian.rodriguez
     */
    public void actualizarListaMunicipios() {

        this.municipioSeleccionadoCodigo = "";

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.departamentoSeleccionadoCodigo != null) {
            List<Municipio> municipios = this.getGeneralesService()
                .getCacheMunicipiosByDepartamento(
                    this.departamentoSeleccionadoCodigo);

            for (Municipio municipio : municipios) {
                this.municipiosItemList.add(new SelectItem(municipio
                    .getCodigo(), municipio.getNombre()));
            }
        }

    }

    // ----------------------------------------------------------------------
    /**
     * Listener que se ejecuta al ingresar un número de identificación o al seleccionar un tipo de
     * documento. valida que no exista un profesional registrado con estos datos, de lo contrario
     * genera un error. Retorna true si no existe profesional con los datos ingresados, false si ya
     * existe alguno
     *
     * @author christian.rodriguez
     */
    public boolean validarProfesionalExistente() {
        if (this.profesionalSeleccionado != null &&
             this.profesionalSeleccionado.getNumeroIdentificacion() != null &&
             this.profesionalSeleccionado.getTipoIdentificacion() != null &&
             (!this.numIdentificacionTemp
                .equals(this.profesionalSeleccionado
                    .getNumeroIdentificacion()) || !this.tipoIdentificacionTemp
                .equals(this.profesionalSeleccionado
                    .getTipoIdentificacion()))) {

            FiltroDatosConsultaProfesionalAvaluos filtroTemp =
                new FiltroDatosConsultaProfesionalAvaluos();
            filtroTemp.setNumeroIdentificacion(this.profesionalSeleccionado
                .getNumeroIdentificacion());
            filtroTemp.setTipoIdentificacion(this.profesionalSeleccionado
                .getTipoIdentificacion());

            List<ProfesionalAvaluo> profesionalesTemp = this
                .getAvaluosService().buscarProfesionalesAvaluosPorFiltro(
                    filtroTemp);

            if (profesionalesTemp == null || !profesionalesTemp.isEmpty()) {
                RequestContext context = RequestContext.getCurrentInstance();
                this.addMensajeError(
                    "Este número de identificación ya se encuentra asignado a un avaluador.");
                context.addCallbackParam("error", "error");
                return false;
            }
        }
        return true;
    }

    // ----------------------------------------------------------------------
    /**
     * Lsitener del botón cerrar
     *
     * @author christian.rodriguez
     * @return indice de navegación faces
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBeans();
        return ConstantesNavegacionWeb.INDEX;
    }

}
