package co.gov.igac.snc.web.controller.conservacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.documental.DocumentalServiceFactory;
import co.gov.igac.sigc.documental.IDocumentosService;
import co.gov.igac.sigc.documental.vo.DocumentoVO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.persistence.entity.conservacion.PPredio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.generales.LogAcceso;
import co.gov.igac.snc.persistence.entity.generales.LogAccion;
import co.gov.igac.snc.persistence.entity.generales.TipoDocumento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDigitalizacion;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumento;
import co.gov.igac.snc.persistence.entity.tramite.TramiteInconsistencia;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ETipoDocumento;
import co.gov.igac.snc.persistence.util.ETrueFalse;
import co.gov.igac.snc.util.ELogAccionEvento;
import co.gov.igac.snc.util.Utilidades;
import co.gov.igac.snc.vo.ActividadVO;
import co.gov.igac.snc.vo.ErrorVO;
import co.gov.igac.snc.vo.PredioInfoVO;
import co.gov.igac.snc.vo.RespuestaVO;
import co.gov.igac.snc.vo.ResultadoVO;
import co.gov.igac.snc.web.controller.BaseController;
import co.gov.igac.snc.web.util.UtilidadesWeb;

@Controller
@RequestMapping(value = "/tramite/**")
public class TramiteController extends BaseController {

    private static final long serialVersionUID = 3627611204946405416L;
    private static final Logger LOGGER = LoggerFactory
        .getLogger(TramiteController.class);

    public TramiteController() {
    }

    /** **********************************************************************************************
     * OPERACIONES DEL SERVICIO DE TRAMITES *
	 * ********************************************************************************************** */
    /**
     * Obtener información proyectada sobre un tramite
     *
     * @author andres.eslava
     * @para modelMap
     * @return
     * @author andres.eslava
     */
    @RequestMapping(value = "/conservacion/depuracionGeo/consultarInformacionProyectada/{tramiteId}")
    public void consultarInformacionProyectada(ModelMap modelMap,
        @PathVariable Long tramiteId) {
        try {
            ActividadVO actividadVo = this.getTramiteService().
                obtenerInformacionPredialProyectadaTramite(tramiteId);
            List<ActividadVO> listaDeActividades = new LinkedList<ActividadVO>();
            listaDeActividades.add(actividadVo);
            ResultadoVO resultadoVo = new ResultadoVO(listaDeActividades);
            modelMap.addAttribute(resultadoVo);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error obteniendo la informacion proyectada del tramite");
            modelMap.addAttribute(error);
            LOGGER.error(error.getMensajeError(), e);
        }
    }

    /**
     * Obtiene los detalles de los predios involucrados en un tramite
     *
     * @param modelMap
     * @return
     * @author andres.eslava
     */
    @RequestMapping(value = "/conservacion/depuracionGeo/consultarTramite/{tramiteId}")
    public void obtenerTramiteConInsonsitencias(ModelMap modelMap,
        @PathVariable Long tramiteId) {
        try {
            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
            ResultadoVO resultadoVO = this.getTramiteService().obtenerInformacionTramiteDepuracion(
                usuario, tramiteId);
            modelMap.addAttribute("resultado", resultadoVO);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO(
                "Error obteniendo la informacion del tramite con sus inconsistencias");
            modelMap.addAttribute("Error", error);
            LOGGER.error(error.getMensajeError(), e);
        }

        //return null;
    }

    /**
     * Obtiene el detalle de un predio por id el predio
     *
     * @param modelMap
     * @return
     * @author andres.eslava
     */
    @RequestMapping(value = "/conservacion/depuracionGeo/consultarPredio/{numeroPredial}")
    public void obtenerPredioTramiteConInsonsitencias(ModelMap modelMap,
        @PathVariable String numeroPredial) {

        //Se registra el acceso al servicio 
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());

        try {
            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
            ResultadoVO resultadoVO = this.getTramiteService().consultarPredioDepuracion(usuario,
                numeroPredial);
            modelMap.addAttribute("resultado", resultadoVO);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO(
                "Error obteniendo la informacion del tramite con sus inconsistencias");
            modelMap.addAttribute("Error", error);
            LOGGER.error(error.getMensajeError(), e);
        }

        //return null;
    }

    /**
     *
     * @author franz.gamba
     * @param modelMap
     * @param tramiteId
     * @param fileAfter
     */
    @RequestMapping(value = "/imagen/{tramiteId}")
    public void cargarImagenAlTramite(ModelMap modelMap,
        @PathVariable Long tramiteId,
        @RequestParam("imagen") MultipartFile fileAfter,
        @RequestParam("imagen2") MultipartFile fileBefore) {
        if (tramiteId != null) {
            if (this.getTramiteService().existeElTramite(tramiteId)) {
                String mensajeAntes = new String();
                String mensajeDespues = new String();

                RespuestaVO respuesta = null;

                if (!fileAfter.isEmpty()) {
                    if (!fileBefore.isEmpty()) {
                        try {
                            String extAf = fileAfter.getName();
                            String extBf = fileBefore.getName();

                            File parentDir1 = new File(
                                FileUtils.getTempDirectory().getAbsolutePath());
                            File outputFile1 = File.createTempFile("img_", "." +
                                 extAf + ".jpg", parentDir1);
                            File parentDir2 = new File(
                                FileUtils.getTempDirectory().getAbsolutePath());
                            File outputFile2 = File.createTempFile("img_", "." +
                                 extBf + ".jpg", parentDir2);

                            OutputStream out1 = new FileOutputStream(
                                outputFile1);
                            OutputStream out2 = new FileOutputStream(
                                outputFile2);

                            out1.write(fileAfter.getBytes());
                            out2.write(fileBefore.getBytes());
                            /**
                             * Creación del Documento
                             */
                            TipoDocumento tipoDoc = new TipoDocumento();
                            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
                            // TODO franz.gamba :: 28-10-11: si solo se realiza
                            // la
                            // busqueda
                            // para construir el documento, se puede contruir el
                            // trámite
                            // con id y es suficiente :: juan.agudelo
                            Tramite tram = this.getTramiteService()
                                .buscarTramitePorId(tramiteId);

                            // imagen antes
                            Documento document = new Documento(usuario, tram,
                                outputFile1.getName(), true);

                            Documento documentoRetorno = this
                                .getTramiteService().guardarDocumento(
                                    usuario, document);
                            mensajeAntes = documentoRetorno
                                .getIdRepositorioDocumentos();
                            // imagen despues
                            document = new Documento(usuario, tram,
                                outputFile2.getName(), false);

                            documentoRetorno = this.getTramiteService()
                                .guardarDocumento(usuario, document);
                            mensajeDespues = documentoRetorno
                                .getIdRepositorioDocumentos();

                            out1.close();
                            out2.close();

                            respuesta = new RespuestaVO(
                                RespuestaVO.CARGA_ARCHIVO_OK);
                            modelMap.addAttribute("resultado", respuesta);

                        } catch (IOException e) {
                            LOGGER.error(e.getMessage(), e);
                            ErrorVO error = new ErrorVO("Error en el servidor");
                            modelMap.addAttribute("respuesta", error);
                        }

                    } else {
                        LOGGER.warn("imagen vacia" + tramiteId);
                        ErrorVO error = new ErrorVO("Imagen Vacia");
                        modelMap.addAttribute("respuesta", error);
                    }
                } else {
                    LOGGER.warn("imagen vacia" + tramiteId);
                    ErrorVO error = new ErrorVO("Imagen Vacia");
                    modelMap.addAttribute("respuesta", error);
                }
            } else {
                LOGGER.warn("No existe trámite con id " + tramiteId);
                ErrorVO error = new ErrorVO("El trámite no existe");
                modelMap.addAttribute("respuesta", error);
            }
        } else {
            LOGGER.warn("tramite nulo " + tramiteId);
            ErrorVO error = new ErrorVO("El identificador del tramite es nulo");
            modelMap.addAttribute("respuesta", error);
        }

    }

    /**
     *
     * @author franz.gamba
     * @param modelMap
     * @param tramiteId
     * @param fileBefore
     * @modified andres.eslava 25-04-2013 se agregan dos parametros, sobre el extend de la imagen,
     * que son agregados en el objeto documento relacionado al tramite, en el campo observaciones.
     */
    @RequestMapping(value = "/imagen/inicial/{tramiteId}")
    public void cargarImagenInicialAlTramite(ModelMap modelMap,
        @PathVariable Long tramiteId,
        @RequestParam("imagen") MultipartFile fileBefore,
        @RequestParam("extendImagenLowerLeft") String extendImagenLowerLeft,
        @RequestParam("extendImagenUpperRight") String extendImagenUpperRight) {
        if (tramiteId != null) {
            if (this.getTramiteService().existeElTramite(tramiteId)) {
                if (!this.getTramiteService()
                    .existeImagenInicialTramitePorIdTramite(tramiteId)) {
                    String mensajeAntes;
                    RespuestaVO respuesta = null;

                    if (!fileBefore.isEmpty()) {
                        try {
                            String extAf = fileBefore.getName();

                            File parentDir1 = new File(
                                FileUtils.getTempDirectory().getAbsolutePath());
                            File outputFile1 = File.createTempFile("img_ini_",
                                "." + extAf + ".jpg", parentDir1);

                            OutputStream out1 = new FileOutputStream(
                                outputFile1);

                            out1.write(fileBefore.getBytes());

                            /**
                             * Creación del Documento
                             */
                            TipoDocumento tipoDoc = new TipoDocumento();
                            UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();

                            Tramite tram = this.getTramiteService()
                                .buscarTramitePorId(tramiteId);

                            // imagen antes
                            Documento document = new Documento(usuario, tram,
                                outputFile1.getName(), true);

                            document.setObservaciones(extendImagenLowerLeft + ":" +
                                extendImagenUpperRight);

                            Documento documentoRetorno = this
                                .getTramiteService().guardarDocumento(
                                    usuario, document);

                            TramiteDocumento tramiteDoc = new TramiteDocumento();
                            tramiteDoc.setFechaLog(new Date());
                            tramiteDoc.setUsuarioLog(usuario.getLogin());
                            tramiteDoc.setDocumento(documentoRetorno);
                            tramiteDoc.setfecha(new Date());
                            tramiteDoc.setTramite(tram);

                            tramiteDoc = this.getTramiteService().actualizarTramiteDocumento(
                                tramiteDoc);

                            mensajeAntes = documentoRetorno
                                .getIdRepositorioDocumentos();

                            out1.close();

                            respuesta = new RespuestaVO(
                                RespuestaVO.CARGA_ARCHIVO_OK);
                            modelMap.addAttribute("resultado", respuesta);

                        } catch (IOException e) {
                            LOGGER.error(e.getMessage(), e);
                            ErrorVO error = new ErrorVO("Error en el servidor");
                            modelMap.addAttribute("respuesta", error);
                        }

                    } else {
                        LOGGER.warn("imagen vacia" + tramiteId);
                        ErrorVO error = new ErrorVO("Imagen Vacia");
                        modelMap.addAttribute("respuesta", error);
                    }
                } else {
                    LOGGER.warn("Ya existe una imagen inicial para el trámite.");
                    ErrorVO error = new ErrorVO("Ya existe una imagen inicial para el trámite.");
                    modelMap.addAttribute("respuesta", error);
                }
            } else {
                LOGGER.warn("No existe trámite con id " + tramiteId);
                ErrorVO error = new ErrorVO("El trámite no existe");
                modelMap.addAttribute("respuesta", error);
            }
        } else {
            LOGGER.warn("tramite nulo " + tramiteId);
            ErrorVO error = new ErrorVO("El identificador del tramite es nulo");
            modelMap.addAttribute("respuesta", error);
        }

    }
//--------------------------------------------------------------------------------------------------    

    /**
     *
     * @author franz.gamba
     * @param modelMap
     * @param tramiteId
     * @param fileAfter
     */
    /*
     * @modified andres.eslava 25-04-2013 se agregan dos parametros, sobre el extend de la imagen,
     * que son agregados en el objeto documento relacionado al tramite, en el campo observaciones.
     */
    @RequestMapping(value = "/imagen/final/{tramiteId}")
    public void cargarImagenFinalAlTramite(ModelMap modelMap,
        @PathVariable Long tramiteId,
        @RequestParam("imagen") MultipartFile fileAfter,
        @RequestParam("extendImagenLowerLeft") String extendImagenLowerLeft,
        @RequestParam("extendImagenUpperRight") String extendImagenUpperRight) {

        String mensajeAntes;
        if (tramiteId != null) {
            if (this.getTramiteService().existeElTramite(tramiteId)) {
                RespuestaVO respuesta = null;

                if (!fileAfter.isEmpty()) {
                    try {
                        String extAf = fileAfter.getName();

                        File parentDir1 = new File(UtilidadesWeb.obtenerRutaTemporalArchivos());
                        File outputFile1 = File.createTempFile("img_fin_", "." +
                             extAf + ".jpg", parentDir1);

                        OutputStream out1 = new FileOutputStream(outputFile1);

                        out1.write(fileAfter.getBytes());

                        /**
                         * Creación del Documento
                         */
                        UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
                        Documento document = this.getTramiteService()
                            .obtenerImagenDespuesEdicionPorTramiteId(tramiteId);

                        Tramite tram = new Tramite();

                        if (document == null) {
                            tram = this.getTramiteService()
                                .buscarTramitePorId(tramiteId);

                            // imagen despues
                            document = new Documento(usuario, tram,
                                outputFile1.getName(), false);

                        } else {
                            document.setArchivo(outputFile1.getName());
                            document.setUsuarioLog(usuario.getLogin());

                        }
                        document.setObservaciones(extendImagenLowerLeft + ":" +
                            extendImagenUpperRight);
                        Documento documentoRetorno = this
                            .getTramiteService().guardarDocumento(
                                usuario, document);
                        tram.setId(documentoRetorno.getTramiteId());

                        TramiteDocumento tramiteDoc = new TramiteDocumento();
                        tramiteDoc.setFechaLog(new Date());
                        tramiteDoc.setUsuarioLog(usuario.getLogin());
                        tramiteDoc.setDocumento(documentoRetorno);
                        tramiteDoc.setfecha(new Date());
                        tramiteDoc.setTramite(tram);

                        tramiteDoc = this.getTramiteService()
                            .actualizarTramiteDocumento(tramiteDoc);

                        mensajeAntes = documentoRetorno
                            .getIdRepositorioDocumentos();

                        out1.close();

                        respuesta = new RespuestaVO(
                            RespuestaVO.CARGA_ARCHIVO_OK);
                        modelMap.addAttribute("resultado", respuesta);

                    } catch (IOException e) {
                        LOGGER.error(e.getMessage(), e);
                        ErrorVO error = new ErrorVO("Error en el servidor");
                        modelMap.addAttribute("respuesta", error);
                    }

                } else {
                    LOGGER.warn("imagen vacia" + tramiteId);
                    ErrorVO error = new ErrorVO("Imagen Vacia");
                    modelMap.addAttribute("respuesta", error);
                }
            } else {
                LOGGER.warn("No existe trámite con id " + tramiteId);
                ErrorVO error = new ErrorVO("El trámite no existe");
                modelMap.addAttribute("respuesta", error);
            }
        } else {
            LOGGER.warn("tramite nulo " + tramiteId);
            ErrorVO error = new ErrorVO("El identificador del tramite es nulo");
            modelMap.addAttribute("respuesta", error);
        }

    }

    /**
     * Servicio que avanza el proceso del tramite desde la actividad MODIFICAR INFO GEOGRAFICA a
     * MODIFICAR INFO ALFANUMERICA
     *
     * @author franz.gamba
     * @param modelMap
     * @param tramiteId
     *
     * @modified andres.eslava :: 25/07/2013 :: Se agrega parametro actividad asociada para avanzar
     * correctamente el proceso y se corrige el tag para errores entregados al editor en el XML
     */
    @RequestMapping(value = "/proceso/avanzar/{tramiteId}")
    public void avanzarProcesoDesdeModifInfoGeo(ModelMap modelMap,
        @PathVariable Long tramiteId,
        @RequestParam("actividadActual") String actividadActual) {

        RespuestaVO respuesta = null;

        TramiteDigitalizacion unTramiteDigitalizacion;
        UsuarioDTO usuarioEnContexto;
        String[] roles;
        Tramite tramite = this.getTramiteService().buscarTramitePorId(tramiteId);
        if (tramiteId != null && tramiteId > 0) {
            try {
                usuarioEnContexto = this.obtenerDatosUsuarioAutenticado();
                roles = usuarioEnContexto.getRoles();

                boolean processFlag = this.getTramiteService().avanzarProcesoAModifInfoAlfanumerica(
                    tramiteId, actividadActual, usuarioEnContexto);
                if (processFlag) {
                    respuesta = new RespuestaVO("El proceso de la tarea con Id: " + tramiteId +
                        " avanzó exitosamente.");
                    if (!tramite.getFuncionarioEjecutor().equals(usuarioEnContexto.getLogin())) {
                        for (int i = 0; i < roles.length; i++) {
                            if (roles[i].equals(ERol.DIGITALIZADOR_DEPURACION.toString())) {
                                unTramiteDigitalizacion = this.getTramiteService().
                                    buscarTramiteDigitalizacionActivaTramite(usuarioEnContexto.
                                        getLogin(), tramiteId);
                                unTramiteDigitalizacion.setFechaDigitalizacion(new Date());
                                this.getTramiteService().guardarActualizarTramiteDigitalizacion(
                                    unTramiteDigitalizacion);
                            }
                        }
                    }
                    modelMap.addAttribute("respuesta", respuesta);
                } else {
                    respuesta = new RespuestaVO("No fue posible avanzar el proceso para " +
                        "la actividad con Id: " + tramiteId);
                    ErrorVO error = new ErrorVO("Error Avanzando El Proceso");
                    modelMap.addAttribute("Error", error);
                    LOGGER.info(error.getMensajeError() + " " + respuesta.getMensaje());
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                ErrorVO error = new ErrorVO("Error en el servidor");
                modelMap.addAttribute("Error", error);
            }
        } else {
            ErrorVO error = new ErrorVO("Parámetros no válidos.");
            modelMap.addAttribute("Error", error);
        }
    }

//--------------------------------------------------------------------------------------------------    
    /**
     * Método que obtiene una réplica sin versionamiento
     *
     * @param modelMap
     * @param tramiteId
     * @modified
     * @author andres.eslava
     */
    @RequestMapping(value = "/replica/consulta/{tramiteId}")
    public void getNombreReplicaConsultaTramite(ModelMap modelMap, @PathVariable Long tramiteId) {

        String publicUrl;

        try {
            RespuestaVO respuesta = null;
            String idDocumentalReplica = this.getTramiteService().
                getIdRepositorioDocumentosDeReplicaGDBdeTramite(tramiteId,
                    ETipoDocumento.COPIA_BD_GEOGRAFICA_CONSULTA.getId());
            if (idDocumentalReplica != null && !idDocumentalReplica.isEmpty()) {
                IDocumentosService service = DocumentalServiceFactory.getService();
                DocumentoVO documentoVO = service.cargarDocumentoPrivadoEnWorkspacePreview(
                    idDocumentalReplica);
                publicUrl = documentoVO.getUrlPublico();
                respuesta = new RespuestaVO(publicUrl);
                modelMap.addAttribute("resultado", respuesta);
            } else {
                ErrorVO error = new ErrorVO("No se encontro replica de consulta para el tramite " +
                    tramiteId);
                modelMap.addAttribute("respuesta", error);
                LOGGER.info(error.getMensajeError());
            }
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta de la replica de consulta del tramite [TramiteController]: " +
                 ex);
        }
    }

    /**
     * Método que obtiene una réplica a partir del id del trámite asociado.
     *
     * @author lorena.salamanca
     * @param modelMap
     * @param tramiteId
     * @modified lorena.salamanca :: Se deja de usar la carpeta compartida, este metodo ahora
     * descarga a la carpeta de archivos temporales del sistema (del pc local) la replica original y
     * la publica en la web.
     * @modified juanfelipe.garcia :: cambio en la arquitectura de la documental(todo queda en
     * alfresco)
     */
    @RequestMapping(value = "/replica/original/{tramiteId}")
    public void getNombreReplicaGDBdeTramite(ModelMap modelMap, @PathVariable Long tramiteId) {

        //Se registra el acceso al servicio 
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());

        String publicUrl;

        try {
            RespuestaVO respuesta = null;
            String idDocumentalReplica = this.getTramiteService().
                getIdRepositorioDocumentosDeReplicaGDBdeTramite(tramiteId,
                    ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId());
            if (idDocumentalReplica != null && !idDocumentalReplica.isEmpty()) {
                IDocumentosService service = DocumentalServiceFactory.getService();

                DocumentoVO documentoVO = service.cargarDocumentoPrivadoEnWorkspacePreview(
                    idDocumentalReplica);
                publicUrl = documentoVO.getUrlPublico();
                respuesta = new RespuestaVO(publicUrl);
                modelMap.addAttribute("resultado", respuesta);
            } else {
                ErrorVO error = new ErrorVO("No se encontro replica original para el tramite " +
                    tramiteId);
                modelMap.addAttribute("respuesta", error);
                LOGGER.info(error.getMensajeError());
            }
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta de la replica original del tramite [TramiteController]: " +
                 ex);
        }
    }

    /**
     * Método que descarga una réplica (Copia de trabajo) del servidor si tiene un id de trámite
     * asociado.
     *
     * @author lorena.salamanca
     * @param modelMap
     * @param tramiteId
     */
    /*
     * @modified lorena.salamanca :: Se deja de usar la carpeta compartida, este metodo ahora
     * descarga a la carpeta de archivos temporales del sistema (del pc local) la replica final y la
     * publica en la web. @modified juanfelipe.garcia cambio en la arquitectura de la
     * documental(todo queda en alfresco)
     */
    @RequestMapping(value = "/replica/copiaTrabajo/{tramiteId}", method = RequestMethod.GET)
    public void getReplicaGDBfromOutFolderToTempFolderdeTramite(
        ModelMap modelMap, @PathVariable Long tramiteId) {

        //Se registra el acceso al servicio 
        this.registrarAccion(new Object() {
        }.getClass().getEnclosingMethod().getName());

        RespuestaVO respuesta = null;
        String publicUrl;

        try {
            String idDocumentalReplicaFinal = this.getTramiteService()
                .getIdRepositorioDocumentosDeReplicaGDBdeTramite(tramiteId,
                    ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());
            if (idDocumentalReplicaFinal != null && !idDocumentalReplicaFinal.isEmpty()) {
                IDocumentosService service = DocumentalServiceFactory
                    .getService();

                DocumentoVO documentoVO = service.cargarDocumentoPrivadoEnWorkspacePreview(
                    idDocumentalReplicaFinal);
                publicUrl = documentoVO.getUrlPublico();

                respuesta = new RespuestaVO(publicUrl);
                modelMap.addAttribute("resultado", respuesta);
            } else {
                ErrorVO error = new ErrorVO(
                    "No se encontro replica final para el tramite " +
                     tramiteId);
                modelMap.addAttribute("respuesta", error);
                LOGGER.info(error.getMensajeError());
            }
        } catch (Exception ex) {
            LOGGER.error(
                "Error no determinado en la consulta de la replica final del tramite [TramiteController]: " +
                 ex);
        }
    }

    /**
     * Método que sube una réplica (Copia de trabajo) al servidor si tiene un id de trámite
     * asociado. recibe el zip de la replica final, lo sube y crea el Documento en la base de datos.
     *
     * @author lorena.salamanca
     *
     * @param modelMap Map para el manejo de parámetros de respuesta (Spring MVC)
     * @param tramiteId Identificador del trámite
     * @param finalFlag	true:finaliza la edición geográfica, y avanza el trámite false:sube el
     * archivo de trabajo sin finalizar la edición
     * @param prediosAreas	Area final de los predios Editados
     * @param codigoOrganizacional	Código organizacional de la territorial a la que pertenecen los
     * predios
     * @param replicaFile	Archivo zip encriptado con los datos geográficos editados en la aplicación
     * desktop
     *
     * @modified andres.eslava 15/05/2013 fragmentacion del codigo y eliminacion de validaciones
     * repetidas
     *
     */
    @RequestMapping(value = "/replica/copiaTrabajo/{tramiteId}", method = RequestMethod.POST)
    public void cargarReplicaEnElServidor(ModelMap modelMap,
        @PathVariable Long tramiteId,
        @RequestParam("final") String finalFlag,
        @RequestParam("prediosAreas") String prediosAreas,
        @RequestParam("codigoOrganizacional") String codigoOrganizacional,
        @RequestParam("replicaFile") MultipartFile replicaFile,
        @RequestParam("actividadActual") String actividadActual,
        @RequestParam("tramiteMasivo") Boolean isTramiteMasivo) {

        RespuestaVO respuesta = null;
        String[] respuestaCargaFinal = new String[2];

        //Validacion de datos enviados
        Integer opcion = Integer.valueOf(validacionParaCargaCopiaFinal(finalFlag, replicaFile,
            tramiteId)[0]);
        String mensajeValidacion =
            validacionParaCargaCopiaFinal(finalFlag, replicaFile, tramiteId)[1];

        switch (opcion) {
            case (0): {
                LOGGER.warn(
                    "La informacion enviada no cumple los requisitios minimos para la carga de la GDB editada");
                ErrorVO error = new ErrorVO(mensajeValidacion);
                modelMap.addAttribute("respuesta", error);
            }
            //El ejecutor confirma la finalización de los cambios geograficos    
            case (1): {
                if (finalFlag.equals(ETrueFalse.TRUE.getCodigo())) {
                    respuestaCargaFinal = cargaGDBFinalizaTramiteGeografico(modelMap, replicaFile,
                        tramiteId, codigoOrganizacional, prediosAreas, actividadActual,
                        isTramiteMasivo);

                    if (Boolean.valueOf(respuestaCargaFinal[0])) {
                        respuesta = new RespuestaVO(respuestaCargaFinal[1]);
                        modelMap.addAttribute("resultado", respuesta);
                    } else {
                        LOGGER.warn("Error subiendo la información final de la edicion geográfica");
                        ErrorVO error = new ErrorVO(respuestaCargaFinal[1]);
                        modelMap.addAttribute("respuesta", error);
                    }
                } // Opcion si no ha finalizado la edicion geográfica.    
                else {
                    if (subirReplicaFinal(modelMap, replicaFile, tramiteId, codigoOrganizacional)) {
                        LOGGER.info("La version de trabajo ha sido subida exitosamente");
                        respuesta = new RespuestaVO("La replica" + replicaFile.getName() +
                            "ha sido subida exitosamente");
                        modelMap.addAttribute("resultado", respuesta);
                    } else {
                        ErrorVO error = new ErrorVO("Error cargando la copia de trabajo");
                        modelMap.addAttribute("respuesta", error);
                        LOGGER.warn(error.getMensajeError());
                    }
                }
            }
        }
    }

    /**
     * Actualizar las tareas depuracion relacionadas al ambiende desktop sobre los datos geograficos
     *
     * @param modelMap respuesta al editor geográfico.
     * @param tramiteId identificador del tramite asociado.
     * @param tareasDepuracion lista de tareas de depuración.
     *
     * @author andres.eslava
     */
    @RequestMapping(value = "/conservacion/depuracionGeo/cargarTareasDepuracion/{tramiteId}",
        method = RequestMethod.POST)
    public void cargarListaTareasDepuracion(ModelMap modelMap,
        @PathVariable Long tramiteId,
        @RequestParam("tareasDepuracion") String tareasDepuracion,
        @RequestParam("actividadActual") String actividadActual) {

        try {
            List<TramiteInconsistencia> inconsistenciasEnviadasEditor =
                new ArrayList<TramiteInconsistencia>();
            Class clase = ProcesoDeConservacion.class;
            Field unField = clase.getField(actividadActual);
            String cadenaActividadProceso = (String) unField.get(null);
            UsuarioDTO usuario;

            usuario = this.obtenerDatosUsuarioAutenticado();
            String[] tareasDepuracionGeneradaValidadores = tareasDepuracion.split("¥");
            int cantidadTareasDepuracion = tareasDepuracionGeneradaValidadores.length;
            for (int i = 0; i < cantidadTareasDepuracion; i++) {
                String[] infoTareaDepuracion = tareasDepuracionGeneradaValidadores[i].split("::");
                TramiteInconsistencia unTramiteInconsitencia = new TramiteInconsistencia();

                unTramiteInconsitencia.setTipo(infoTareaDepuracion[0]);
                unTramiteInconsitencia.setInconsistencia(infoTareaDepuracion[1] + "::" +
                    infoTareaDepuracion[2] + "::" + infoTareaDepuracion[3] + "::" +
                    infoTareaDepuracion[5]);
                unTramiteInconsitencia.setNumeroPredial(infoTareaDepuracion[4]);
                inconsistenciasEnviadasEditor.add(unTramiteInconsitencia);
            }

            this.getTramiteService().actualizarTramitesDepuracion(inconsistenciasEnviadasEditor,
                cadenaActividadProceso, tramiteId, usuario);

            RespuestaVO respuesta = new RespuestaVO("Se actualizaron las inconsistencias con exito");
            modelMap.addAttribute("Respuesta", respuesta);

        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error en la creacion de las inconsistencias de depuracion");
            modelMap.addAttribute("Respuesta", error);
            LOGGER.error(error.getMensajeError(), e);
        }
    }

    /**
     * Obtiene la informacion del tramite para edicion geografica
     *
     * @param modelmap estructura mapa de respuesta
     * @param tramiteId identificador del tramite del que se quiere la informacion de edicion
     * @author andres.eslava
     */
    @RequestMapping(value = "/conservacion/obtenerInfoEdicion/{tramiteId}")
    public void obtenerInformacionEdicionPorTramiteId(ModelMap modelMap,
        @PathVariable Long tramiteId) {

        //Obtiene usuario en contexto             
        try {
            UsuarioDTO usuarioContexto = obtenerDatosUsuarioAutenticado();
            ResultadoVO resultado = this.getTramiteService().
                obtenerInformacionDetalladaEdicionGeografica(tramiteId, usuarioContexto);
            modelMap.addAttribute("Resultado", resultado);
        } catch (Exception e) {
            ErrorVO error = new ErrorVO("Error en la generacion de la lista de tareas ");
            modelMap.addAttribute("Error", error);
            LOGGER.error(error.getMensajeError(), e);

        }
    }

    /** **********************************************************************************************
     * MIEMBROS PRIVADOS PARA OPERACIONES DEL WS	*
	 * ********************************************************************************************** */
    /**
     * Método para la actualizacion de los posibles predios de un tramite geografico
     *
     * @author franz.gamba
     * @param tramiteId
     * @return
     */
    private boolean actualizacionDeAvaluosCatastrales(Long tramiteId) {

        List<PPredio> predios = this.getConservacionService().obtenerPPrediosPorTramiteIdActAvaluos(
            tramiteId);

        if (predios != null && !predios.isEmpty()) {
            for (PPredio pp : predios) {
                Object[] errores = this.getFormacionService().recalcularAvaluosDecretoParaUnPredio(
                    tramiteId, pp.getId(), new Date(), obtenerDatosUsuarioAutenticado());
                if (Utilidades.hayErrorEnEjecucionSP(errores)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Avanza el proceso dando por terminado el proceso geografico
     *
     * @param modelMap referencia a la respuesta de la operacion del web service
     * @param tramiteId identificador del tramite
     * @return estructura de mapa de respuesta actualziada
     *
     * @author andres.eslava
     */
    private Boolean avanzarProcesoFinalizaEdicionGeografica(Long tramiteId, String actividadActual,
        UsuarioDTO unUsuario) {
        try {
            boolean processFlag = this.getTramiteService().avanzarProcesoAModifInfoAlfanumerica(
                tramiteId, actividadActual, unUsuario);
            return Boolean.valueOf(processFlag ? "True" : "False");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    /**
     * Actualizacion de areas de los predios involucrados en el tramite geografico.
     *
     * @param prediosAreas Informacion de areas actualizadas.
     * @param tramite Tramite realacionado a la gestion geografico, necesario para relacionar la
     * actualizacion de areas.
     *
     * @return 1 si actualizo las areas de los predios y 0 en caso contrario.
     *
     * @author andres.eslava
     */
    private boolean actualizarAreasModificadasGeograficamente(String prediosAreas, Tramite tramite,
        Boolean isTramiteMasivo) {

        boolean formatoFlag = this.validarFormatoPrediosAreas(prediosAreas);
        LOGGER.info("Realiza modificación areas geográficas");
        if (formatoFlag) {
            List<String> predios = new ArrayList<String>();
            List<Double> areas = new ArrayList<Double>();
            String[] parejas = prediosAreas.split(";");

            for (String s : parejas) {
                String[] valores = s.split(":");
                if (Double.parseDouble(valores[0]) >= 0 && Double.parseDouble(valores[1]) >= 0) {
                    predios.add(valores[0]);
                    areas.add(Double.parseDouble(valores[1]));
                }
            }

            boolean updateDatos = this.getTramiteService().updateAreasDePrediosEdicionGeografica(
                predios, areas, tramite, obtenerDatosUsuarioAutenticado(), isTramiteMasivo);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo que carga el zip de la replica en el FTP y crea el Documento asociado en la BD.
     *
     * @param modelMap Respuesta al cliente del WS.
     * @param replicaFile Archivo que contiene la GDB.
     * @param tramiteId Identificador del tramite.
     * @param codigoOrganizacional	Identificador de la territorial.
     *
     * @return 1 upload exitoso, 0 en caso contrario.
     */
    /*
     * @modified andres.eslava 15/05/2013 modificacion para documentacion del caso de uso. @modified
     * juanfelipe.garcia :: cambio en la arquitectura de la documental(todo queda en alfresco)
     * @modified pedro.garcia 09-09-2013 + el id del Documento se guarda en el campo
     * idRepositorioDocumentos (lo estaba guardando en 'archivo'), y en el campo 'archivo' se guarda
     * el tipo de documento. + cambios de nombres de variables para que se entienda el código
     *
     *
     */
    private boolean subirReplicaFinal(ModelMap modelMap, MultipartFile replicaFile,
        Long tramiteId, String codigoOrganizacional) {

        boolean banderaSubioReplica = true;
        File parentDir = FileUtils.getTempDirectory();
        String idDocumentoReplicaCargada;

        try {
            String nombreReplicaEstandar = "tr_" + codigoOrganizacional + "_" + tramiteId +
                ".gdb.zip.enc.zip";
            //Se verifica que el archivo venga con el nombre estandar de la replica
            if (replicaFile.getOriginalFilename().equals(nombreReplicaEstandar)) {
                File outputFile1 = new File(parentDir, replicaFile.getOriginalFilename());
                OutputStream out1 = new FileOutputStream(outputFile1);
                out1.write(replicaFile.getBytes());
                out1.close();
                IDocumentosService service = DocumentalServiceFactory.getService();
                idDocumentoReplicaCargada = service.cargarDocumentoWorkspaceTramite(outputFile1.
                    getPath(), String.valueOf(tramiteId), codigoOrganizacional,
                    ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getNombre());
                LOGGER.debug(idDocumentoReplicaCargada);

                String idDocumentoReplicaFinalExistente = this.getTramiteService().
                    getIdRepositorioDocumentosDeReplicaGDBdeTramite(tramiteId,
                        ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());

                //Se verifica si ya existia una replica final, (CopiaTrabajo) si es asi, 
                //solo se actualiza 
                if (idDocumentoReplicaFinalExistente != null && !idDocumentoReplicaFinalExistente.
                    isEmpty()) {
                    LOGGER.warn("La replica final ya existia, reemplazando...");
                    Documento docReplicaFinal = this.getGeneralesService().
                        buscarDocumentoPorTramiteIdyTipoDocumento(tramiteId,
                            ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());
                    if (docReplicaFinal != null) {
                        docReplicaFinal.setArchivo(ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.
                            getNombre());
                        docReplicaFinal.setIdRepositorioDocumentos(idDocumentoReplicaCargada);
                        docReplicaFinal = this.getGeneralesService().guardarDocumento(
                            docReplicaFinal);
                    }

                } else {
                    LOGGER.info("La replica final no existía, creando...");
                    UsuarioDTO usuario = obtenerDatosUsuarioAutenticado();
                    List<TramiteDocumento> listTramitesDocumentos =
                        new ArrayList<TramiteDocumento>();
                    TramiteDocumento tramiteDocumento = new TramiteDocumento();
                    Tramite t = this.getTramiteService().buscarTramitePorId(tramiteId);
                    Documento doc = new Documento();
                    TipoDocumento tipoD = new TipoDocumento();
                    tipoD.setId(ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getId());

                    doc.setArchivo(ETipoDocumento.COPIA_BD_GEOGRAFICA_FINAL.getNombre());
                    doc.setIdRepositorioDocumentos(idDocumentoReplicaCargada);
                    doc.setTipoDocumento(tipoD);
                    doc.setFechaLog(new Date());
                    doc.setFechaRadicacion(new Date());
                    doc.setUsuarioCreador(usuario.getLogin());
                    doc.setUsuarioLog(usuario.getLogin());
                    doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
                    doc.setBloqueado(ESiNo.NO.getCodigo());
                    doc.setTramiteId(tramiteId);
                    doc = this.getGeneralesService().guardarDocumento(doc);

                    tramiteDocumento.setTramite(t);
                    tramiteDocumento.setUsuarioLog(usuario.getLogin());
                    tramiteDocumento.setFechaLog(new Date());
                    tramiteDocumento.setfecha(new Date());
                    tramiteDocumento.setDocumento(doc);

                    listTramitesDocumentos.add(tramiteDocumento);
                    t.setTramiteDocumentos(listTramitesDocumentos);

                    this.getTramiteService().actualizarTramiteDocumento(tramiteDocumento);
                }
            } else {
                ErrorVO error = new ErrorVO(
                    "El archivo que intenta subir no tiene el nombre estandar de los archivos de replica.");
                modelMap.addAttribute("respuesta", error);
                banderaSubioReplica = false;
                LOGGER.warn(error.getMensajeError());
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            banderaSubioReplica = false;
        }
        return banderaSubioReplica;

    }
//--------------------------------------------------------------------------------------------------	

    /**
     * Valida el formato de la informacion de areas de las modificaciones geograficas
     *
     * @param prediosAreas datos de areas modificadas en la edicion geografica
     * @return 1 si el formato es adecuado 0 en caso contrario
     * @author ...
     */
    /*
     * @modified andres.eslava 10-05-2013 se documenta el metodo, no estada documentado y se ignora
     * el author original. tambien se modifica como metodo privado.
     *
     */
    private boolean validarFormatoPrediosAreas(String prediosAreas) {
        char[] cadena = prediosAreas.toCharArray();
        for (char c : cadena) {
            if (c == ':') {
                return true;
            }
        }
        return false;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Valida los datos para la persistencia de la GDB en el FTP, en los campos de bandera, archivo,
     * tramite.
     *
     * @param banderaFinal representa la finalizacion de la edicion geográfica.
     * @param archivoReplica archivo gdb comprimido y encriptado que representa cambios en la info.
     * geografica.
     * @param tramiteId identificador del tramite asociado.
     * @return 0 si hay errores en bandera, archivoReplica o TramiteId y 1 Si puede continuar en la
     * escritura del archivo.
     *
     * @author andres.eslava
     */
//TODO :: andres.eslava :: usar tabs de 4 espacios :: pedro.garcia
    private String[] validacionParaCargaCopiaFinal(String banderaFinal, MultipartFile archivoReplica,
        Long tramiteId) {

        LOGGER.info("Validando informacion para la carga de la copia");

        String respuesta[] = new String[2];

        if (banderaFinal == null) {
            respuesta[0] = "0";
            respuesta[1] = "No especifico la bandera de finalizacion";
            return respuesta;
        } else if (archivoReplica == null || archivoReplica.isEmpty()) {
            respuesta[0] = "0";
            respuesta[1] = "No adjunto el archivo o esta vacio";
            return respuesta;
        } else if (tramiteId == null) {
            respuesta[0] = "0";
            respuesta[1] = "No especifico el tramite";
            return respuesta;
        } else if (!this.getTramiteService().existeElTramite(tramiteId)) {
            respuesta[0] = "0";
            respuesta[1] = "El tramite enviado no existe";
            return respuesta;
        } else if (this.getTramiteService().getIdRepositorioDocumentosDeReplicaGDBdeTramite(
            tramiteId, ETipoDocumento.COPIA_BD_GEOGRAFICA_ORIGINAL.getId()) == null) {
            respuesta[0] = "0";
            respuesta[1] = "No existe una GBD Original asociada al tramite";
            return respuesta;
        } else {
            respuesta[0] = "1";
            respuesta[1] = "Solicitud correcta";
            return respuesta;
        }
    }

    /**
     * Carga la GDB cuando se ha finalizado de hacer los cambios geográficos y se determina que es
     * la ultima edición requerida en el tramite, replica los cambios en los datos
     * alfanumericos(Actualizacion del tramite, actualizacion Avaluos, avance del proceso);
     *
     * @param modelMap
     * @param replicaFile
     * @param tramiteId
     * @param codigoOrganizacional
     * @param prediosAreas
     * @return Arreglo [0] = 0 Error realizando la operación , 1 Exito en la Operacion [1]	= Mensaje
     * de error o exito respectivamente [0].
     * @author andres.eslava
     *
     */
    /*
     * @modified andres.eslava::13/Jul/2015::se agrega el proceso geografico asincrono para tramites
     * masivos refs #13126
     */
    private String[] cargaGDBFinalizaTramiteGeografico(ModelMap modelMap,
        MultipartFile replicaFile, Long tramiteId,
        String codigoOrganizacional, String prediosAreas, String actividadActual,
        Boolean isTramiteMasivo) {

        //RespuestaVO respuesta = null;
        LOGGER.info("ModificaciónPH:: Entra acá");
        String respuesta[] = new String[2];
        respuesta[0] = "false";
        TramiteDigitalizacion unTramiteDigitalizacion;

        String[] roles;
        Tramite tramite = null;
        UsuarioDTO usuarioEnContexto = null;
        Boolean banderaUploadReplica = null;
        Boolean banderaAvanceProceso = Boolean.FALSE;
        Boolean banderaActualizacionAvaluos = null;
        String controlRegistroDigitalizador = "";

        //Obtiene el tramite
        try {
            tramite = this.getTramiteService().buscarTramitePorId(tramiteId);
            if (tramite == null) {
                throw new Exception("No se encontro el tramite...");
            }
        } catch (Exception e) {
            respuesta[1] = "Error recuperando el trámite de la base de datos";
            LOGGER.error(respuesta[1], e);
            return respuesta;
        }

        // Obtiene el usuario autenticado y sus roles
        try {
            usuarioEnContexto = this.obtenerDatosUsuarioAutenticado();
            roles = usuarioEnContexto.getRoles();

        } catch (Exception e) {
            respuesta[1] = "Error recuperando el usuario autenticado(Contexto)";
            LOGGER.error(respuesta[1], e);
            return respuesta;
        }

        // Carga replica en el servidor documental
        try {
            banderaUploadReplica = subirReplicaFinal(modelMap, replicaFile, tramiteId,
                codigoOrganizacional);
            LOGGER.info("ModificaciónPH:: sale de subir replica final: " + banderaUploadReplica);
        } catch (Exception e) {
            respuesta[1] = "Error cargando el archivo al servidor documental";
            LOGGER.error(respuesta[1], e);
            return respuesta;
        }

        // Actualizar las areas 
        if (banderaUploadReplica) {
            //caso no masivo
            if (!isTramiteMasivo) {
                LOGGER.info("ModificaciónPH:: Entra caso no masivo: " + prediosAreas);
                if (!prediosAreas.isEmpty()) {
                    try {
                        this.actualizarAreasModificadasGeograficamente(prediosAreas, tramite,
                            isTramiteMasivo);
                    } catch (Exception e) {
                        respuesta[1] = "No se actualizaron las areas de los predios";
                        LOGGER.error(respuesta[1], e);
                        return respuesta;
                    }

                    try {
                        banderaActualizacionAvaluos = this.actualizacionDeAvaluosCatastrales(
                            tramiteId);
                        if (!banderaActualizacionAvaluos) {
                            respuesta[1] =
                                "No se pudo realizar la actualización de los avaluos catastrales";
                            return respuesta;
                        }
                    } catch (Exception e) {
                        respuesta[1] =
                            "No se pudo realizar la actualización de los avaluos catastrales";
                        LOGGER.error(respuesta[1], e);
                        return respuesta;
                    }

                    //felipe.cadena :: #9339 :: 04/09/2014 :: Se calculan las zonas para los predios de condicion 8 o 9
                    if (tramite.isTipoInscripcionPH() || tramite.isTipoInscripcionCondominio() ||
                        tramite.isEnglobeVirtual()) {
                        try {
                            this.getTramiteService().calcularAreasUnidadesPrediales(tramite);
                        } catch (Exception e) {
                            respuesta[1] =
                                "No se pudo realizar la actualización de las zonas de los PH y Condominios";
                            LOGGER.error(respuesta[1], e);
                            return respuesta;
                        }
                    }
                }
            } //Caso masivo 
            else {
                LOGGER.info("ModificaciónPH:: Entra caso masivo: " + prediosAreas);
                try {
                    this.actualizarAreasModificadasGeograficamente(prediosAreas, tramite,
                        isTramiteMasivo);
                } catch (Exception e) {
                    respuesta[1] = "No se actualizaron las areas de los predios";
                    LOGGER.error(respuesta[1], e);
                    return respuesta;
                }
            }
        } else {
            respuesta[1] = "Error cargando el archivo al servidor documental";
            return respuesta;
        }
        // Avance de proceso                 
        try {
            if (!isTramiteMasivo) {
                banderaAvanceProceso = this.avanzarProcesoFinalizaEdicionGeografica(tramiteId,
                    actividadActual, usuarioEnContexto);
            }
        } catch (Exception e) {
            respuesta[1] = "No se pudo realizar el avance de proceso";
            LOGGER.error(respuesta[1], e);
            return respuesta;
        }

        if (!isTramiteMasivo) {
            //Cambia el estado GDB para confirmar que paso por el editor geografico
            tramite.setEstadoGdb(null);
            //Se actualiza el estado del tramite
            if (tramite.isCancelacionMasiva()) {
                this.getTramiteService().guardarActualizarTramite2(tramite);
            }
            try {
                // Registra el tiempo y responsable de la digitalizacion
                String ejecutorTramite;
                if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_EJECUTOR)) {
                    TramiteDepuracion td = this.getTramiteService().
                        buscarUltimoTramiteDepuracionPorIdTramite(tramiteId);
                    ejecutorTramite = td.getEjecutor();

                } else if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_DIGITALIZACION)) {
                    TramiteDepuracion td = this.getTramiteService().
                        buscarUltimoTramiteDepuracionPorIdTramite(tramiteId);
                    ejecutorTramite = td.getDigitalizador();

                } else if (actividadActual.equals(
                    ProcesoDeConservacion.ACT_DEPURACION_AJUSTAR_INFO_ESP_TOPOGRAFIA)) {
                    TramiteDepuracion td = this.getTramiteService().
                        buscarUltimoTramiteDepuracionPorIdTramite(tramiteId);
                    ejecutorTramite = td.getTopografo();

                } else {
                    ejecutorTramite = tramite.getFuncionarioEjecutor();
                }

                if (!ejecutorTramite.equals(usuarioEnContexto.getLogin())) {
                    for (int i = 0; i < roles.length; i++) {
                        if (roles[i].equals(ERol.DIGITALIZADOR_DEPURACION.toString())) {
                            unTramiteDigitalizacion = this.getTramiteService().
                                buscarTramiteDigitalizacionActivaTramite(usuarioEnContexto.
                                    getLogin(), tramiteId);
                            unTramiteDigitalizacion.setFechaDigitalizacion(new Date());
                            this.getTramiteService().guardarActualizarTramiteDigitalizacion(
                                unTramiteDigitalizacion);
                        }
                    }
                }

            } catch (Exception e) {
                LOGGER.error("Error registrando el digitalizador tramite id =" + tramite.getId(), e);
                controlRegistroDigitalizador =
                    "Hay un error registrando el digitalizador tramite id =" + tramite.getId();
            }
        } else {
            respuesta[1] = "El avance se realizará de manera asincrónica";
            LOGGER.warn(respuesta[1]);
            return respuesta;
        }
        //registra el cambio de estado en  el tramite 
        try {
            this.getTramiteService().actualizarTramite(tramite);
            respuesta[0] = "true";
            respuesta[1] = " La edición geográfica fue cargada con éxito" +
                 controlRegistroDigitalizador;
            return respuesta;
        } catch (Exception e) {
            respuesta[1] = " El avance fue exitoso pero" +
                 " no se pudo acutalizar el estado GDB del trámite " +
                 controlRegistroDigitalizador +
                 " Comuniquese con el administador del sistema. ";
            LOGGER.error(respuesta[1], e);
            return respuesta;
        } finally {
            respuesta[1] += controlRegistroDigitalizador;
            return respuesta;
        }
    }

    /**
     * Registra el acceso al servicio y la acción dependiendo el metodo invocado
     *
     * @param nombreMetodo Nombre del metodo desde que se invoco.
     */
    public void registrarAccion(String nombreMetodo) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.
            currentRequestAttributes();
        HttpServletRequest hsr = attr.getRequest();
        LogAcceso la = UtilidadesWeb.obtenerInfoSesionDesktop(hsr);
        Long accesoId = this.getGeneralesService().registrarAcceso(la);
        LogAccion accion = new LogAccion(accesoId,
            ELogAccionEvento.ACCION.toString(),
            "Servicio rest",
            nombreMetodo,
            hsr.getRequestURI());
        this.getGeneralesService().registrarlogAccion(accion);

    }

    /**
     * Obtener informacion numeros prediales invariables en la edicion geografica
     *
     * @author andres.eslava
     */
    @RequestMapping(value = "/conservacion/depuracionGeo/prediosInvariables/{codigoManzana}")
    public void obtenerPrediosInvariablesEdicionDepuracion(ModelMap modelMap,
        @PathVariable String codigoManzana) {
        LOGGER.debug("TramiteController#obtenerPrediosInvariablesEdicionDepuracion...INICIA");
        try {

            ActividadVO actividadVO = new ActividadVO();
            List<ActividadVO> actividades = new LinkedList<ActividadVO>();
            UsuarioDTO usuario = super.obtenerDatosUsuarioAutenticado();

            List<PredioInfoVO> predios = this.getConservacionService().
                obtenerPrediosInvariantesDepuracion(codigoManzana);

            actividadVO.setPrediosInvariantes(predios);
            actividades.add(actividadVO);
            ResultadoVO resultado = new ResultadoVO(usuario, actividades);
            modelMap.addAttribute("RESULTADO", resultado);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            e.printStackTrace();
            ErrorVO error = new ErrorVO(e.getMessage());
            modelMap.addAttribute("ERROR", error);
        }
    }

}
