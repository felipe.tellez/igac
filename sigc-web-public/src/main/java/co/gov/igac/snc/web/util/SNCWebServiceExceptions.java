/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.util;

import co.gov.igac.snc.excepciones.ESeveridadExcepcionSNC;
import co.gov.igac.snc.excepciones.ExcepcionSNC;

/**
 * Listado de Excepciones Manejadas en la capa web
 *
 * @author pedro.garcia
 */
public class SNCWebServiceExceptions {

    //D: WFFXYnnn
    //   W = web,
    //   FF =
    //  	IN = Infraestructura,
    //      AD = acceso a datos, 
    //      CE = correo electrónico,
    //      GD = gestor documental,
    //      PR = procesos,
    //      RP = reportes,
    //      SA = servidor de aplicaciones ...
    //
    //   X = proceso
    //      C = conservación, ...
    //   Y = subproceso
    //      * = cualquiera,  1 = radicación, 2 = asignación, 3 = ejecución, 4 = validación
    //   nnn = número excepción
    public static ExcepcionSNC EXCEPCION_INFRAESTRUCTURA_0001 = new ExcepcionSNC("WIN000001",
        ESeveridadExcepcionSNC.ERROR,
        "Error de Configuración de la Aplicación");

    //D: {0} = qué estaba consultando
    public static ExcepcionSNC EXCEPCION_0001 = new ExcepcionSNC("WADC4001",
        ESeveridadExcepcionSNC.ERROR,
        "Error en consulta a la base de datos: {0}");

    //D: {0} = nombre del reporte
    public static ExcepcionSNC EXCEPCION_0002 = new ExcepcionSNC("WRPC4001",
        ESeveridadExcepcionSNC.ERROR,
        "Error en la generación de un reporte: {0}");

    public static ExcepcionSNC EXCEPCION_0003 = new ExcepcionSNC("WGDC4001",
        ESeveridadExcepcionSNC.ERROR,
        "Error con gestor documental: {0}");

    public static ExcepcionSNC EXCEPCION_0004 = new ExcepcionSNC("WCEC4001",
        ESeveridadExcepcionSNC.ERROR,
        "Error con envío de correo electrónico: {0}");

    public static ExcepcionSNC EXCEPCION_0005 = new ExcepcionSNC("WGDC4002",
        ESeveridadExcepcionSNC.ERROR,
        "Error obteniendo el contexto para obtener las rutas temporales de documentos: ");

    public static ExcepcionSNC EXCEPCION_0006 = new ExcepcionSNC("WADC*002",
        ESeveridadExcepcionSNC.ERROR,
        "Error insertando o actualizando datos a la base de datos: {0}");

    public static ExcepcionSNC EXCEPCION_0007 = new ExcepcionSNC("WADC*001",
        ESeveridadExcepcionSNC.ERROR,
        "Error debido a inconsistencia de datos: {0}");

    //D: {0} = id del trámite
    //   {1} = número de radicación
    //   {2} = id de la actividad
    public static ExcepcionSNC EXCEPCION_0008 = new ExcepcionSNC("WPRC4003",
        ESeveridadExcepcionSNC.ERROR,
        "Error moviendo el proceso de un trámite (id trámite, num. radicación, id actividad): [{0},{1},{2}]");

    //ejemplo
    //D: {0} = tipo de objeto (entity)
    //   {1} = id del objeto
//    public static ExcepcionSNC EXCEPCION_0002 = new ExcepcionSNC("CN020001",
//            ESeveridadExcepcionSNC.ERROR, "No se encontró un objeto por su id: [{0},{1}]");
}
