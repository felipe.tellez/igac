package co.gov.igac.snc.web.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.persistence.entity.generales.ProductoCatastralJob;
import co.gov.igac.snc.web.controller.AbstractLocator;

/**
 * Tarea que se encarga de finalizar los jobs pendientes de procesar en SNC (Estas tareas son
 * manejadas por un pool de instancias asociados a una cola administrada por spring en la capa web)
 *
 * @author Juan Carlos
 *
 */
public class VerificarJobTask extends AbstractLocator implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = -7332773578521079455L;
    public static Logger LOGGER = LoggerFactory.getLogger(VerificarJobTask.class);

    private ProductoCatastralJob job;

    /**
     *
     * @param job a ejecutar
     */
    public VerificarJobTask(ProductoCatastralJob job) {
        this.job = job;
    }

    /**
     * Método encargado de enviar la ejecución del job pendiente
     */
    public void run() {
        this.getGeneralesService().verificarEjecucionJobPendienteEnSNC(job.getId());
    }

}
