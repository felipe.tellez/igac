package co.gov.igac.snc.web.mb.actualizacion.reconocimiento;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.ActualizacionReconocimiento;
import co.gov.igac.snc.persistence.entity.actualizacion.ReconocimientoPredio;
import co.gov.igac.snc.persistence.entity.actualizacion.RecursoHumano;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.util.EActualizacionContratoActividad;
import co.gov.igac.snc.web.mb.actualizacion.alistamiento.ClasificacionManzanasYAreasParaLevantamientoTopograficoMB;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.FiltroPredio;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * CU-302 Creado para la asignacion de trabajos de reconocimiento en el proceso de actualizacion
 *
 * @author andres.eslava
 *
 */
@Component("asignaTrabajoReconocimiento")
@Scope("session")
public class AsignaTrabajoReconocimientoMB extends SNCManagedBean {

    private static final long serialVersionUID = 1L;

    /**
     * Log aplicación
     */
    private static final Logger LOGGER = LoggerFactory.
        getLogger(AsignaTrabajoReconocimientoMB.class);

    /**
     * Reconocedor seleccionado para la asignaciond de predios.
     */
    private Long itemReconocedorSeleccionado;

    /**
     * Lista de reconocedores
     */
    private List<SelectItem> listaReconocedores;

    /**
     * Habilita la busqueda por rango.
     */
    private boolean busquedaPorRango;

    /**
     * Lista de manzanas asignadas al cordinador
     */
    private List<ReconocimientoPredio> prediosCordinador;

    /**
     * Reconocedor Seleccionado
     */
    private RecursoHumano reconocedorSeleccionado;

    /**
     * Usuario cordinador autenticado en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Actualizacion asociada
     */
    private Actualizacion actualizacionAsociada;

    /**
     * Datos busqueda de numero predial inicial
     */
    private FiltroPredio filtroInicial;

    /**
     * Datos buscqueda de numero predial final
     */
    private FiltroPredio filtroFinal;

    /**
     * Predios seleccionados para asignacion
     */
    private ReconocimientoPredio[] prediosSeleccionados;

    /**
     * Actualizacion Reconocimiento Asociada
     */
    private ActualizacionReconocimiento reconocimientoAsociado;

    /**
     * bandera Preasignacion sirve para saber si ya se realizo al menos una asignacion
     *
     */
    private Boolean banderaPreasignacion;

    /**
     * Determina si al confirmar(persistir) la asignacion faltan predios asignados al cordinador por
     * asignar.
     */
    private Boolean banderaFaltanPrediosPorAsignar;

    /**
     * Reconocedor por el cual se hace el filtro
     */
    private String reconocedorSeleccionadoParaFiltro;

    /**
     * Lista de reconocedores para filtro
     *
     */
    private List<SelectItem> listaReconocedoresFiltro;

    /**
     * Bandera que habilita el filtro por reconocedor.
     */
    private Boolean banderaHabilitaFiltroReconocedor;

    /**
     * Listado de busqueda por rango manzanas
     */
    private List<ReconocimientoPredio> listaPrediosPorRango;

    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.prediosCordinador = new LinkedList<ReconocimientoPredio>();
        this.listaReconocedores = new LinkedList<SelectItem>();

        actualizacionAsociada = this.getActualizacionService().getActualizacionConResponsables(450L);
        reconocimientoAsociado = this.getActualizacionService().getActualizacionReconocimientoById(
            1L);

        this.filtroInicial = new FiltroPredio();
        this.filtroFinal = new FiltroPredio();

        this.filtroInicial.setNumeroPredialS1(actualizacionAsociada.getDepartamento().getCodigo());
        this.filtroInicial.setNumeroPredialS2(actualizacionAsociada.getMunicipio().
            getCodigo3Digitos());

        this.banderaHabilitaFiltroReconocedor = Boolean.FALSE;

        this.cargarListaReconocedores();

        listaReconocedoresFiltro = new LinkedList<SelectItem>();

        listaReconocedoresFiltro = new LinkedList<SelectItem>();

        this.establecerReconocimientoPredios();

        this.estableceListaReconocedoresParaFiltro();

        this.busquedaPorRango = false;
    }

    /** *************************************************************************************************************
     ** GETTERS AND SETTERS	**	 
	*************************************************************************************************************** */
    public List<SelectItem> getListaReconocedoresFiltro() {
        return listaReconocedoresFiltro;
    }

    public void setListaReconocedoresFiltro(List<SelectItem> listaReconocedoresFiltro) {
        this.listaReconocedoresFiltro = listaReconocedoresFiltro;
    }

    public Boolean getBanderaFaltanPrediosPorAsignar() {
        return banderaFaltanPrediosPorAsignar;
    }

    public void setBanderaFaltanPrediosPorAsignar(
        Boolean banderaFaltanPrediosPorAsignar) {
        this.banderaFaltanPrediosPorAsignar = banderaFaltanPrediosPorAsignar;
    }

    public RecursoHumano getReconocedorSeleccionado() {
        return reconocedorSeleccionado;
    }

    public FiltroPredio getFiltroInicial() {
        return filtroInicial;
    }

    public void setFiltroInicial(FiltroPredio filtroInicial) {
        this.filtroInicial = filtroInicial;
    }

    public FiltroPredio getFiltroFinal() {
        return filtroFinal;
    }

    public void setFiltroFinal(FiltroPredio filtroFinal) {
        this.filtroFinal = filtroFinal;
    }

    public void setReconocedorSeleccionado(RecursoHumano reconocedorSeleccionado) {
        this.reconocedorSeleccionado = reconocedorSeleccionado;
    }

    public Actualizacion getActualizacionAsociada() {
        return actualizacionAsociada;
    }

    public void setActualizacionAsociada(Actualizacion actualizacionAsociada) {
        this.actualizacionAsociada = actualizacionAsociada;
    }

    public List<ReconocimientoPredio> getPrediosCordinador() {
        return prediosCordinador;
    }

    public void setPrediosCordinador(
        List<ReconocimientoPredio> prediosCordinador) {
        this.prediosCordinador = prediosCordinador;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Long getItemReconocedorSeleccionado() {
        return itemReconocedorSeleccionado;
    }

    public void setItemReconocedorSeleccionado(Long itemReconocedorSeleccionado) {
        this.itemReconocedorSeleccionado = itemReconocedorSeleccionado;
    }

    public List<SelectItem> getListaReconocedores() {
        return listaReconocedores;
    }

    public void setListaReconocedores(List<SelectItem> listaReconocedores) {
        this.listaReconocedores = listaReconocedores;
    }

    public boolean isBusquedaPorRango() {
        return busquedaPorRango;
    }

    public void setBusquedaPorRango(boolean busquedaPorRango) {
        this.busquedaPorRango = busquedaPorRango;
    }

    public ReconocimientoPredio[] getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(ReconocimientoPredio[] prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    public ActualizacionReconocimiento getReconocimientoAsociado() {
        return reconocimientoAsociado;
    }

    public void setReconocimientoAsociado(
        ActualizacionReconocimiento reconocimientoAsociado) {
        this.reconocimientoAsociado = reconocimientoAsociado;
    }

    public Boolean getBanderaPreasignacion() {
        return banderaPreasignacion;
    }

    public void setBanderaPreasignacion(Boolean banderaPreasignacion) {
        this.banderaPreasignacion = banderaPreasignacion;
    }

    public Boolean getBanderaHabilitaFiltroReconocedor() {
        return banderaHabilitaFiltroReconocedor;
    }

    public void setBanderaHabilitaFiltroReconocedor(
        Boolean habilitaFiltroReconocimiento) {
        this.banderaHabilitaFiltroReconocedor = habilitaFiltroReconocimiento;
    }

    public String getReconocedorSeleccionadoParaFiltro() {
        return reconocedorSeleccionadoParaFiltro;
    }

    public void setReconocedorSeleccionadoParaFiltro(
        String reconocedorSeleccionadoParaFiltro) {
        this.reconocedorSeleccionadoParaFiltro = reconocedorSeleccionadoParaFiltro;
    }

    public List<ReconocimientoPredio> getListaPrediosPorRango() {
        return listaPrediosPorRango;
    }

    public void setListaPrediosPorRango(
        List<ReconocimientoPredio> listaPrediosPorRango) {
        this.listaPrediosPorRango = listaPrediosPorRango;
    }

    /** *************************************************************************************************************
     ** FUNCIONALIDAD Y VISTA **	 
	*************************************************************************************************************** */
    /**
     * Metodo para cambiar el estado que determina la disposicion de la vista en terminos de la
     * busqueda por rango de numeros prediales.
     *
     * @author andres.eslava
     *
     */
    public void cambioEstadoBusquedaPorRango() {
        if (busquedaPorRango) {
            this.busquedaPorRango = false;
        } else {
            this.busquedaPorRango = true;
        }

    }

    /**
     * Carga la vista con los reconocedores existentes, desde recursos humanos
     *
     * @author andres.eslava
     */
    public void cargarListaReconocedores() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#cargarListaReconocedores...INICIA");
        try {
            List<RecursoHumano> reconocedores = this.getActualizacionService().
                obtenerRecursoHumanoPorActividad(EActualizacionContratoActividad.RECONOCEDORES.
                    toString());
            for (RecursoHumano reconocedor : reconocedores) {
                listaReconocedores.add(new SelectItem(reconocedor.getId(), reconocedor.getNombre()));
            }
        } catch (Exception e) {
            this.addMensajeError("Error al obtener la lista de reconocedores");

        } finally {
            LOGGER.debug("AsignaTrabajoReconocimientoMB#cargarListaReconocedores...FINALIZA");
        }

    }

    /**
     * Obtiene las manzanas asignadas al coordinador de la actualización
     *
     * @author andres.eslava
     */
    public void establecerReconocimientoPredios() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#obtenerManzanas...INICIA");

        try {
            RecursoHumano cordinador = this.getActualizacionService()
                .obtenerRecursoHumanoPorIdentificacionYActualizacionId(
                    usuario.getIdentificacion(),
                    actualizacionAsociada.getId());

            List<ActualizacionReconocimiento> manzanasCordinador =
                this.getActualizacionService().obtenerManzanasAsignadasPorReconocedor(cordinador);

            for (ActualizacionReconocimiento reconocimiento : manzanasCordinador) {
                List<Predio> predios = this.getActualizacionService().
                    getPrediosLibreActualizacionReconocimientoYSaldoConservacion(reconocimiento.
                        getManzanaCodigo());
                for (Predio predio : predios) {
                    ReconocimientoPredio unReconocimiento = new ReconocimientoPredio();
                    unReconocimiento.setActualizacionReconocimiento(this.reconocimientoAsociado);
                    unReconocimiento.setFecha(new Date(System.currentTimeMillis()));
                    unReconocimiento.setUsuarioLog(this.usuario.getNombreCompleto());
                    unReconocimiento.setFecha(new Date(System.currentTimeMillis()));
                    unReconocimiento.setNumeroPredial(predio.getNumeroPredial());
                    prediosCordinador.add(unReconocimiento);
                }

                List<ReconocimientoPredio> reconocimientosExistentes = this.
                    getActualizacionService().obtenerReconocimientosPredioPorManzana(reconocimiento.
                        getManzanaCodigo());
                for (ReconocimientoPredio unaActReconocimiento : reconocimientosExistentes) {
                    prediosCordinador.add(unaActReconocimiento);
                }
            }

            // Busca saldos de conservacion asociados
            if (!prediosCordinador.isEmpty()) {
                for (ReconocimientoPredio reconocimientoPredio : prediosCordinador) {
                    reconocimientoPredio.setTramitesAsociadosSaldosConservacion(this.
                        getActualizacionService().getSaldosConservacionByNumeroPredial(
                            reconocimientoPredio.getNumeroPredial()));
                }
            }
        } catch (Exception e) {
            this.addMensajeError("Error al obtener las manzanas del cordinador");
        } finally {
            LOGGER.debug("AsignaTrabajoReconocimientoMB#obtenerManzanas...FINALIZA");
        }
    }

    /**
     * Establece el reconocedor seleccionado de la lista de reconocedores
     *
     * @author andres.eslava
     */
    public void establecerReconocedorSeleccionado() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#establecerReconocerSeleccionado...INICIA");
        try {
            if (itemReconocedorSeleccionado != null) {
                this.setReconocedorSeleccionado(this.getActualizacionService()
                    .buscarRecursoHumanoporId(
                        this.itemReconocedorSeleccionado));

            } else {
                this.addMensajeError("Error recuperando el reconocedor seleccionado ");

            }
        } catch (Exception e) {
            this.addMensajeError("Error recuperando el reconocedor seleccionado ");
        } finally {
            LOGGER.debug("AsignaTrabajoReconocimientoMB#establecerReconocerSeleccionado...FINALIZA");
        }
    }

    /**
     * Asina un reconocedor a los predios seleccionados
     *
     * @author andres.eslava
     */
    public void asignaReconocedorAPrediosCordinador() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#asignaReconocedorAPrediosCordinador...INICIA");
        if (prediosSeleccionados.length > 0 && reconocedorSeleccionado != null) {
            for (int i = 0; i < prediosSeleccionados.length; i++) {
                prediosSeleccionados[i].setRecursoHumano(this.reconocedorSeleccionado);
                prediosSeleccionados[i].setEstado("Asignado trabajo a reconocedor");
                this.setBanderaPreasignacion(Boolean.TRUE);
            }
        } else {
            this.addMensajeWarn("Debe seleccionar un reconocedor y seleccionar al menos un predio ");
        }
        LOGGER.debug(
            "AsignaTrabajoReconocimientoMB#establecerReconocerAPrediosCordinador...FINALIZA");
    }

    /**
     * Persiste la asignacion de predios al reconocedor
     *
     * @author andres.eslava
     */
    public void confirmaAsignacionReconocedor() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#confirmaAsignacionReconocedor...INICIA");
        // TODO andres.eslava::12-12-12::falta validar si faltan predios por asignar::andres.eslava
        if (prediosSeleccionados.length > 0 && banderaPreasignacion) {
            for (int i = 0; i < prediosSeleccionados.length; i++) {
                this.getActualizacionService().guardarReconocimientoPredio(
                    prediosSeleccionados[i]);
                this.setBanderaPreasignacion(Boolean.FALSE);
            }
        } else {
            this.addMensajeError("Debe realizar almenos una asignacion");
        }
        LOGGER.debug("AsignaTrabajoReconocimientoMB#confirmaAsignacionReconocedor...FINALIZA");
    }

    /**
     * Valida si de los predios seleccionados aun faltan predios por asignar
     *
     * @author andres.eslava
     */
    public void validaPrediosSinAsignar() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#validaPrediosSinAsignar...INICIA");

        if (prediosSeleccionados.length > 0) {
            for (int i = 0; i < prediosSeleccionados.length; i++) {
                if (prediosSeleccionados[i].getRecursoHumano() != null) {
                    this.setBanderaFaltanPrediosPorAsignar(Boolean.TRUE);
                    break;
                }
            }
        }
        LOGGER.debug("AsignaTrabajoReconocimientoMB#validaPrediosSinAsignar...FINALIZA");
    }

    /**
     * Carga la lista de reconocedores para realizar el filtro
     *
     * @author andres.eslava
     */
    public void estableceListaReconocedoresParaFiltro() {
        LOGGER.debug("AsignaTrabajoReconocimientoMB#estableceListaReconocedoresParaFiltro...INICIA");

        if (!prediosCordinador.isEmpty()) {
            for (ReconocimientoPredio predio : prediosCordinador) {
                if ((predio.getRecursoHumano() != null) && !(predio.getRecursoHumano().getNombre().
                    isEmpty())) {
                    listaReconocedoresFiltro.add(new SelectItem(predio.getRecursoHumano().getId(),
                        predio.getRecursoHumano().getNombre()));
                    banderaHabilitaFiltroReconocedor = Boolean.TRUE;
                }
            }
        } else {
            this.addMensajeWarn("No hay reconocedores asignados a los predios.");
        }

        LOGGER.debug(
            "AsignaTrabajoReconocimientoMB#estableceListaReconocedoresParaFiltro...FINALIZA");
    }

}
