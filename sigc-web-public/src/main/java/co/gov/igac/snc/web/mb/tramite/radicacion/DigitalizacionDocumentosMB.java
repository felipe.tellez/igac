/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.tramite.radicacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.generales.Documento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDocumentacion;
import co.gov.igac.snc.persistence.util.EDocumentoEstado;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ECodigoErrorVista;
import co.gov.igac.snc.web.util.IServicioTemporalDocumento;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ServicioTemporalDocumento;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 *
 * Managed bean para la digitalización y revisión de la digitalización de documentos asociados a un
 * trámite
 *
 * @author pedro.garcia
 */
@Component("digitalizacionDocumentos")
@Scope("session")
public class DigitalizacionDocumentosMB extends ProcessFlowManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DigitalizacionDocumentosMB.class);
    /**
     * Tipo MIME del documento temporal para su visualización
     */
    private String tipoMimeDocTemporal;

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    private IServicioTemporalDocumento servicioTemporalDocumento;
    /**
     * Ruta del documento cargado
     */
    private String rutaMostrar;

    /**
     * Archivo a cargar en la carpeta temporal
     */
    private File archivoResultado;

    /**
     * nombre temporal del archivo cargado
     */
    private String nombreTemporalArchivo;

    /**
     * usuario loggeado
     */
    private UsuarioDTO usuario;

    /**
     * id del trámite que se escoge de la lista de trámites asociados a alguna actividad del árbol
     */
    private Long selectedTramiteId;

    /**
     * trámite que se consulta a partir del id selectedTramiteId
     */
    private Tramite currentTramite;

    /**
     * documentación del trámite que se ha seleccionado de la tabla
     */
    private TramiteDocumentacion selectedTramiteDocumentacion;

    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentActivity;

    /**
     * nombre de la actividad actual
     */
    private String currentActivityName;

    /**
     * variable que guarda el valor del id de la territorial del usuario
     */
    private String territorialId;

    /**
     * Bandera para controlar el boton aceptar cuando no se ha cargado completamente el docto
     */
    private boolean successUploadFlag;

    /**
     * Bandera que evita que se guarde el documento a la mitad de la carga de bytes
     */
    private boolean loadingFileFlag = true;

//----------  methods   -----------------
    public TramiteDocumentacion getSelectedTramiteDocumentacion() {
        return this.selectedTramiteDocumentacion;
    }

    public void setSelectedTramiteDocumentacion(
        TramiteDocumentacion selectedTramiteDocumentacion) {
        this.selectedTramiteDocumentacion = selectedTramiteDocumentacion;

        // Hubo la necesidad de setear esto aquí debido a que no se estaba refrescando
        // el valor de la variable adecuadamente.
        if (this.selectedTramiteDocumentacion != null &&
             this.selectedTramiteDocumentacion.getDocumentoSoporte() != null &&
             this.selectedTramiteDocumentacion.getDocumentoSoporte()
                .getArchivo() != null) {
            this.rutaMostrar = this.selectedTramiteDocumentacion
                .getDocumentoSoporte().getArchivo();
        }
    }

    public String getTipoMimeDocTemporal() {
        return tipoMimeDocTemporal;
    }

    public void setTipoMimeDocTemporal(String tipoMimeDocTemporal) {
        this.tipoMimeDocTemporal = tipoMimeDocTemporal;
    }

    //--------------------------------------------------------------------------------------------------
    public Tramite getCurrentTramite() {

        return this.currentTramite;
    }

    public void setCurrentTramite(Tramite currentTramite) {
        this.currentTramite = currentTramite;
    }

    public Long getSelectedTramiteId() {
        return this.selectedTramiteId;
    }

    public void setSelectedTramiteId(Long selectedTramiteId) {
        this.selectedTramiteId = selectedTramiteId;
    }
//--------------------------------------------------------------------------------------------------

    public boolean isSuccessUploadFlag() {
        return this.successUploadFlag;
    }

    public void setSuccessUploadFlag(boolean successUploadFlag) {
        this.successUploadFlag = successUploadFlag;
    }

    public boolean isLoadingFileFlag() {
        return this.loadingFileFlag;
    }

    public void setLoadingFileFlag(boolean loadingFileFlag) {
        this.loadingFileFlag = loadingFileFlag;
    }

    public String getRutaMostrar() {
        return rutaMostrar;
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {

        LOGGER.debug("on DigitalizacionDocumentosMB#init ");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        //D: con este método se obtiene el código de la territorial. Si es uoc da el código de la uoc
        this.territorialId = MenuMB.getMenu().getUsuarioDto().getCodigoEstructuraOrganizacional();

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "digitalizacionDocumentos");

        this.selectedTramiteId =
            this.tareasPendientesMB.getInstanciaSeleccionada().getIdObjetoNegocio();

        if (this.selectedTramiteId != null) {
            this.currentTramite =
                this.getTramiteService().consultarDocumentacionDeTramite(this.selectedTramiteId);
            for (TramiteDocumentacion td : this.currentTramite.getTramiteDocumentacions()) {
                if (td.getDocumentoSoporte() != null) {
                    if (td.getDocumentoSoporte().getArchivo() != null &&
                         !"".equals(td.getDocumentoSoporte().getArchivo()) &&
                         !" ".equals(td.getDocumentoSoporte().getArchivo())) {
                        td.setDigital(ESiNo.SI.getCodigo());

                        this.getTramiteService().guardarTramiteDocumentacion(td);
                    }
                }
            }
        }

        this.loadingFileFlag = true;

        this.currentActivity = this.tareasPendientesMB.getInstanciaSeleccionada();

        try {
            this.getProcesosService().reclamarActividad(this.currentActivity.getId(), this.usuario);
        } catch (ExcepcionSNC e) {
            this.addMensajeInfo(e.getMensaje());
        }

        this.currentActivityName = this.currentActivity.getNombre();

        this.successUploadFlag = false;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * método action del botón "cargar documento"
     */
    public boolean initCargarDocumento() {
        if (this.selectedTramiteDocumentacion == null) {
            this.addMensajeError("Debe seleccionar un documento");
            return false;
        }
        if (this.selectedTramiteDocumentacion == null) {
            this.addMensajeError("Debe seleccionar un documento del trámite!");
            return false;
        }

        return true;

    }
//--------------------------------------------------------------------------------------------------

    /**
     * @see ProcessFlowManager#validateProcess()
     *
     * valida que se hayan cargado todos los documentos que sean 'aportados' y 'digitales' Esta
     * validación me sirve para ambos casos: digitalizacion y controlde calidad de digitalización
     *
     * @author pedro.garcia
     */
    /*
     * @modified by juanfelipe.garcia :: 23-08-2013 :: ajuste en la validación cuando el documento
     * no es digital (esta en null en la DB, ya se solucionó para tramites nuevos, pero se adiciona
     * la validación para que no se reviente la aplicación con los tramites viejos)
     */
    @Implement
    public boolean validateProcess() {

        //D: se revisa que se hayan cargado todos los documentos que sean 'aportados' y 'digitales'
        for (TramiteDocumentacion td : this.currentTramite.getTramiteDocumentacions()) {
            if (td.getAportado().equalsIgnoreCase(ESiNo.SI.getCodigo()) &&
                 td.getDigital() != null && td.getDigital().equalsIgnoreCase(ESiNo.SI.getCodigo())) {
                if (td.getDocumentoSoporte() == null || !td.getDocumentoSoporte().getEstado().
                    equalsIgnoreCase(EDocumentoEstado.CARGADO.getCodigo())) {
                    this.addMensajeError(
                        "No se han cargado todos los documentos que se debieron digitalizar");
                    return false;
                }
            }
        }
        return true;
    }
    //--------------------------------------------------------------------------------------------------

    /**
     * @see ProcessFlowManager#setMessage() ambos avances de proceso manejados por este MB son de
     * tipo INDIVIDUAL. Hay que armar la el ActivityMessageDTO
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void setupProcessMessage() {

        ActivityMessageDTO message = new ActivityMessageDTO();
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosRol = new ArrayList<UsuarioDTO>();
        String transitionName = "paila";

        message.setActivityId(this.currentActivity.getId());

        if (this.currentActivityName.contains(ProcesoDeConservacion.ACT_DIGITALIZACION_ESCANEAR)) {

            //D: cuando la actividad se dirige a un rol y no a un usuario específico, se debe asignar
            //   el atributo SolicitudCatastral.rolTerritorial
            solicitudCatastral.setRol(ERol.CONTROL_DIGITALIZACION.getDistinguishedName());
            solicitudCatastral.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
            //D: en este caso no se define una transición
            transitionName = ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO;
        } else if (this.currentActivityName.contains(
            ProcesoDeConservacion.ACT_DIGITALIZACION_CONTROLAR_CALIDAD_ESCANEO)) {

            //D: cuando la actividad se dirige a un usuario no específico de un rol, se usa el método
            //   para obtener los usuarios por rol de una territorial
            usuariosRol =
                this.getTramiteService().buscarFuncionariosPorRolYTerritorial(
                    this.usuario.getDescripcionEstructuraOrganizacional(),
                    ERol.FUNCIONARIO_RADICADOR);
//TODO :: fredy.wilches :: 15-09-2011 :: qué pasa si no encuentra un usuario con ese rol en esa territorial? ::
            solicitudCatastral.setTerritorial(this.usuario.getDescripcionEstructuraOrganizacional());
            transitionName = ProcesoDeConservacion.ACT_RADICACION_REVISAR_SOLICITUDES;
        }

        solicitudCatastral.setUsuarios(usuariosRol);
        message.setTransition(transitionName);
        solicitudCatastral.setTransicion(transitionName);

        //D: OJO ya no hay que hacer esto porque se cambiarían los valores que trae desde atrás
        //solicitudCatastral = ProcesosUtilidadesWeb.setTramiteInfoEnSolicitudCatastral(
        //        solicitudCatastral, this.currentTramite);
        message.setSolicitudCatastral(solicitudCatastral);
        message.setUsuarioActual(this.usuario);
        this.setMessage(message);

        if (this.getMessage().getComment() != null) {
            message.setComment(this.getMessage().getComment());
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * se usa también para ambos casos: pasar a revisión de digitalización o al siguiente estado
     *
     * OJO: con el cambio de lo de estados en el trámite ya no hay que hacer nada en este método
     *
     * @author pedro.garcia
     */
    @Implement
    @Override
    public void doDatabaseStatesUpdate() {
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo para cargar un documento de forma temporal. Es el método listener del fileupload de la
     * ventana donde se cargan los documentos digitalizables en la actividad Digitalizar
     *
     * @author felipe.cadena
     *
     * @param eventoCarga
     */
    /*
     * @modified leidy.gonzalez #10683 12-12-2014 :: cambio de caracteres especiales por espacion
     */
    public void archivoUploadTemp(FileUploadEvent eventoCarga) {

        this.loadingFileFlag = false;

        //String nombreArchivoSinCaracteresEspeciales = eventoCarga.getFile().getFileName().replaceAll("[^a-zA-Z0-9./]", "_");
        this.rutaMostrar = eventoCarga.getFile().getFileName();

        boolean validacionB = this.initCargarDocumento();

        if (validacionB == true) {

            //OJO: aquí, al no adicionar una parte random al nombre hace que si en la carpeta temporal local
            //  ya existía un archivo con el mismo nombre, se sobreescriba
            Object[] cargaArchivo = UtilidadesWeb.cargarArchivoATemp(eventoCarga, false);

            if (cargaArchivo[1] == null) {

                this.archivoResultado = (File) cargaArchivo[0];

                String mensaje = "Documento cargado satisfactoriamente.";
                this.addMensajeInfo(mensaje);

            } else {
                LOGGER.error("ERROR: " + cargaArchivo[2]);
                String mensaje = (String) cargaArchivo[1];
                this.addMensajeError(mensaje);
            }

            this.nombreTemporalArchivo = new String();

            this.nombreTemporalArchivo = this.rutaMostrar;

        }
        this.loadingFileFlag = true;

    }
//--------------------------------------------------------------------------------------------------        

    /**
     * Método para guardar de manera definitiva un un documento en el gestor documental
     *
     * @author felipe.cadena
     */
//TODO :: felipe.cadena :: 13-08-2013 :: 
    //¿por qué el método no está declarado como private?. 
    //Según estándar los métodos llevan nombres de verbos en infinitivo. 
    //No se está haciendo alguna 'confirmación' de algo. 

    public void confirmaCargaDocumento() {

        this.servicioTemporalDocumento = new ServicioTemporalDocumento();

        // Generación del objeto para alfresco
        if (this.rutaMostrar != null) {
            this.selectedTramiteDocumentacion.getDocumentoSoporte()
                .setArchivo(this.rutaMostrar);
        }

        //Se coloca un predio vacio con el número temporal del tramite. 
        if (this.currentTramite.isQuinta() ||
             (this.currentTramite.getSolicitud() != null && this.currentTramite
            .getSolicitud().isViaGubernativa())) {
            Predio pTemp = new Predio();
            pTemp.
                setNumeroPredial(this.selectedTramiteDocumentacion.getTramite().getNumeroPredial());
            this.selectedTramiteDocumentacion.getTramite().setPredio(pTemp);
        }

        this.selectedTramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());

        //Si ya existe un archivo asociado solo se actualiza la nueva información del archivo.
        if (this.selectedTramiteDocumentacion.getDocumentoSoporte() != null) {
            this.selectedTramiteDocumentacion = this.getTramiteService()
                .actualizarTramiteDocumentacionEnAlfresco(this.selectedTramiteDocumentacion,
                    this.usuario);
            if (this.selectedTramiteDocumentacion == null) {
                LOGGER.error("Ocurrió un error al cargar el documento del trámite.");
                this.addMensajeErrorWithCallback(
                    "Ocurrió un error al cargar el documento del trámite.");
                return;
            } else {
                this.addMensajeInfo("El documento se cargó satisfactoriamente.");
                this.selectedTramiteDocumentacion.getDocumentoSoporte()
                    .setEstado(EDocumentoEstado.CARGADO.getCodigo());
            }
        } else {
            this.selectedTramiteDocumentacion.getDocumentoSoporte()
                .setEstado(EDocumentoEstado.CARGADO.getCodigo());

            try {

                this.selectedTramiteDocumentacion.setDocumentoSoporte(
                    this.getTramiteService().guardarDocumento(usuario,
                        this.selectedTramiteDocumentacion.getDocumentoSoporte()));

                this.selectedTramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
                this.selectedTramiteDocumentacion = this.getTramiteService().
                    guardarTramiteDocumentacion(this.selectedTramiteDocumentacion);
                this.addMensajeInfo("El documento se cargó satisfactoriamente.");
                this.currentTramite = this.getTramiteService().consultarDocumentacionDeTramite(
                    this.selectedTramiteId);
                this.successUploadFlag = true;
            } catch (Exception e) {
                String mensaje = "El documento no pudo ser almacenado, por favor intente nuevamente";
                this.addMensajeErrorWithCallback(mensaje);
            }
        }

    }
//--------------------------------------------------------------------------------------------------

    /**
     * metodo de carga de archivos
     *
     * @author juan.agudelo
     */
    public void archivoUpload(FileUploadEvent eventoCarga) {

        this.loadingFileFlag = false;
        boolean validacionB = initCargarDocumento();

        if (validacionB == true) {
            this.servicioTemporalDocumento = new ServicioTemporalDocumento();
            //String nombreArchivoSinCaracteresEspeciales = eventoCarga.getFile().getFileName().replaceAll("[^a-zA-Z0-9./]", "_");
            this.rutaMostrar = eventoCarga.getFile().getFileName();

            try {
                String directorioTemporal = UtilidadesWeb.obtenerRutaTemporalArchivos();
                this.archivoResultado = new File(directorioTemporal, this.rutaMostrar);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

            FileOutputStream fileOutputStream = null;

            try {
                fileOutputStream = new FileOutputStream(this.archivoResultado);

                byte[] buffer = new byte[Math.round(eventoCarga.getFile()
                    .getSize())];

                int bulk;
                InputStream inputStream = eventoCarga.getFile()
                    .getInputstream();
                while (true) {
                    bulk = inputStream.read(buffer);
                    if (bulk < 0) {
                        break;
                    }
                    fileOutputStream.write(buffer, 0, bulk);
                    fileOutputStream.flush();
                }
                fileOutputStream.close();
                inputStream.close();

                this.nombreTemporalArchivo = new String();
                this.nombreTemporalArchivo = eventoCarga.getFile().getFileName();

                // Generación del objeto para alfresco
                this.selectedTramiteDocumentacion.getDocumentoSoporte().setArchivo(this.rutaMostrar);

                this.selectedTramiteDocumentacion.setDigital(ESiNo.SI.getCodigo());
                if (this.selectedTramiteDocumentacion.getDocumentoSoporte() != null) {
                    boolean deleteFlag = this.getTramiteService().
                        eliminarTramiteDocumentacionConDocumentoAsociado(
                            this.selectedTramiteDocumentacion);

                    if (deleteFlag) {

                        this.selectedTramiteDocumentacion.setId(null);

                        Documento doc = this.generarDocumento(this.selectedTramiteDocumentacion);
                        doc = this.getGeneralesService().actualizarDocumento(doc);
                        this.selectedTramiteDocumentacion.setDocumentoSoporte(doc);
                    } else {
                        LOGGER.error("Ocurrió un error al reemplazar el documento del trámite.");
                    }
                } else {
                    this.selectedTramiteDocumentacion.getDocumentoSoporte()
                        .setEstado(EDocumentoEstado.CARGADO.getCodigo());
                }

                try {
                    Documento docSoporte;
                    docSoporte = this.getTramiteService().guardarDocumento(
                        this.usuario, this.selectedTramiteDocumentacion.getDocumentoSoporte());
                    if (docSoporte != null) {
                        this.selectedTramiteDocumentacion.setDocumentoSoporte(docSoporte);
                    }

                    this.selectedTramiteDocumentacion.setAportado(ESiNo.SI.getCodigo());
                    this.selectedTramiteDocumentacion = this.getTramiteService().
                        guardarTramiteDocumentacion(this.selectedTramiteDocumentacion);
                    this.addMensajeInfo("El documento se cargó satisfactoriamente.");
                    this.currentTramite = this.getTramiteService().consultarDocumentacionDeTramite(
                        this.selectedTramiteId);
                    this.successUploadFlag = true;
                } catch (Exception e) {
                    String mensaje =
                        "El documento no pudo ser almacenado, por favor intente nuevamente";
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");
                    this.addMensajeError(mensaje);
                }

            } catch (IOException e) {
                String mensaje = "Los archivos seleccionados no pudieron ser cargados";
                this.addMensajeError(mensaje);
            }
        }
        this.loadingFileFlag = true;
    }
//--------------------------------------------------------------------------------------------------

    public void seleccionarFila(SelectEvent e) {
    }

    public void desseleccionarFila(UnselectEvent e) {
        this.selectedTramiteDocumentacion = null;
    }

    public String cargarDocumentoTemporalPrev() {

        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        if (this.selectedTramiteDocumentacion
            .getDocumentoSoporte().getIdRepositorioDocumentos() != null &&
            !this.selectedTramiteDocumentacion
                .getDocumentoSoporte().getIdRepositorioDocumentos().isEmpty()) {
            this.selectedTramiteDocumentacion
                .getDocumentoSoporte().setArchivo(this.getGeneralesService().
                    descargarArchivoDeAlfrescoATemp(this.selectedTramiteDocumentacion
                        .getDocumentoSoporte().getIdRepositorioDocumentos()));
        }
        return this.tipoMimeDocTemporal = fileNameMap
            .getContentTypeFor(this.selectedTramiteDocumentacion
                .getDocumentoSoporte().getArchivo());
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Metodo utilizado para guardar documentos adicionales y ejecutado en la ventana modal de
     * nuevoDocumentoTramite como accion sobre el boton Aceptar
     */
    public void guardarDocumentacion() {

        String lastDocSop = this.selectedTramiteDocumentacion.getDocumentoSoporte().getArchivo();

        try {
            if (this.rutaMostrar != null && !this.rutaMostrar.trim().isEmpty()) {
                this.confirmaCargaDocumento();
                this.selectedTramiteDocumentacion = this.getTramiteService()
                    .guardarTramiteDocumentacion(
                        this.selectedTramiteDocumentacion);

                this.currentTramite = this.getTramiteService()
                    .consultarDocumentacionDeTramite(this.selectedTramiteId);
                this.successUploadFlag = true;
            } else {
                this.addMensajeErrorWithCallback("Debe seleccionar un documento");
                return;
            }
        } catch (ExcepcionSNC e) {
            LOGGER.error(ECodigoErrorVista.SUBIR_FOTO_002.getMensajeTecnico());
            this.selectedTramiteDocumentacion.getDocumentoSoporte().setArchivo(lastDocSop);
            this.addMensajeErrorWithCallback(ECodigoErrorVista.SUBIR_FOTO_001.toString());
            return;
        }
    }
//--------------------------------------------------------------------------------------------------

    /**
     * Crea un objeto Documento que se va a insertar en la bd como parte de un TramiteDocumentacion
     *
     * @param tdTemp
     * @return
     */
    private Documento generarDocumento(TramiteDocumentacion tdTemp) {

        Documento doc = new Documento();
        Date currentDate = new Date(System.currentTimeMillis());

        doc.setUsuarioCreador(this.usuario.getLogin());
        doc.setFechaDocumento(currentDate);
        doc.setBloqueado(ESiNo.NO.getCodigo());
        doc.setUsuarioLog(this.usuario.getLogin());
        doc.setFechaLog(currentDate);
        doc.setTipoDocumento(tdTemp.getTipoDocumento());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setNombreDeArchivoCargado(this.nombreTemporalArchivo);
        doc.setTramiteId(tdTemp.getTramite().getId());
        doc.setDescripcion(tdTemp.getDetalle());
        doc.setEstado(EDocumentoEstado.RECIBIDO.getCodigo());
        doc.setCantidadFolios(this.selectedTramiteDocumentacion.getCantidadFolios());

        if (this.rutaMostrar != null && !this.rutaMostrar.isEmpty()) {
            doc.setArchivo(this.rutaMostrar);
        } else {
            doc.setArchivo("");
        }

        return doc;
    }
//end of class
}
