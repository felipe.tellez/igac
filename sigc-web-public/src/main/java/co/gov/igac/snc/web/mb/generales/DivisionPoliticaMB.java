package co.gov.igac.snc.web.mb.generales;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.persistence.entity.generales.Barrio;
import co.gov.igac.persistence.entity.generales.Comuna;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.ManzanaVereda;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.persistence.entity.generales.Sector;
import co.gov.igac.persistence.entity.generales.Zona;

import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.web.mb.actualizacion.planificacion.RegistrarDiagnosticoMunicipioMB;
import co.gov.igac.snc.web.util.EOrden;
import co.gov.igac.snc.web.util.SNCManagedBean;

/**
 * SNCManagedBean para administrar division politica del pais.
 *
 * @author lorena.salamanca
 */
@Component("divisionPolitica")
@Scope("session")
public class DivisionPoliticaMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RegistrarDiagnosticoMunicipioMB.class);

    /**
     * Listas de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
     */
    private ArrayList<Departamento> departamentos;
    private ArrayList<Municipio> municipios;
    private ArrayList<Zona> zonas;
    private ArrayList<Sector> sectores;
    private ArrayList<Comuna> comunas;
    private ArrayList<Barrio> barrios;
    private ArrayList<ManzanaVereda> manzanasveredas;

    /**
     * Listas de items para los combos de Departamento, Municipio, Zona, Sector, Comuna, Barrio y
     * Manzana/Vereda
     */
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;
    private ArrayList<SelectItem> zonasItemList;
    private ArrayList<SelectItem> sectoresItemList;
    private ArrayList<SelectItem> comunasItemList;
    private ArrayList<SelectItem> barriosItemList;
    private ArrayList<SelectItem> manzanasveredasItemList;

    /**
     * Orden para la listas de Departamento, Municipio, Zona, Sector, Comuna, Barrio y
     * Manzana/Vereda
     */
    private EOrden ordenDepartamentos = EOrden.CODIGO;
    private EOrden ordenMunicipios = EOrden.CODIGO;
    private EOrden ordenZonas = EOrden.CODIGO;
    private EOrden ordenSectores = EOrden.CODIGO;
    private EOrden ordenComunas = EOrden.CODIGO;
    private EOrden ordenBarrios = EOrden.CODIGO;
    private EOrden ordenManzanasveredas = EOrden.CODIGO;

    /**
     * Códigos de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
     */
    private String selectedDepartamentoCod;
    private String selectedMunicipioCod;
    private String selectedZonaCod;
    private String selectedSectorCod;
    private String selectedComunaCod;
    private String selectedBarrioCod;
    private String selectedManzanaVeredaCod;

    /**
     * Objetos de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
     */
    private Departamento selectedDepartamento;
    private Municipio selectedMunicipio;
    private Zona selectedZona;
    private Sector selectedSector;
    private Comuna selectedComuna;
    private Barrio selectedBarrio;
    private ManzanaVereda selectedManzanaVereda;

    /**
     * Indican si los combos están ordenados por nombre (si no, lo están por código)
     */
    private boolean departamentosOrderedByNombre;
    private boolean municipiosOrderedByNombre;
    private boolean zonasOrderedByNombre;
    private boolean sectoresOrderedByNombre;
    private boolean comunasOrderedByNombre;
    private boolean barriosOrderedByNombre;
    private boolean manzanasveredasOrderedByNombre;

    /**
     * Cadenas digitadas para crear un Departamento, Municipio, Zona, Sector, Comuna, Barrio o
     * Manzana/Vereda
     */
    private String nombreIngresado;
    private String codigoIngresado;

    /**
     * Nombres de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
     */
    private String municipioNombre;
    private String departamentoNombre;
    private String zonaNombre;
    private String sectorNombre;
    private String comunaNombre;
    private String barrioNombre;
    private String manzanaVeredaNombre;

    /**
     * Booleanos para el render de los grupos de Departamento, Municipio, Zona, Sector, Comuna,
     * Barrio y Manzana/Vereda
     */
    private boolean municipioGrupoFlag;
    private boolean zonaGrupoFlag;
    private boolean sectorGrupoFlag;
    private boolean comunaGrupoFlag;
    private boolean barrioGrupoFlag;
    private boolean manzanaVeredaGrupoFlag;

    @PostConstruct
    public void init() {
        LOGGER.debug("init de DivisionPoliticaMB");

        this.ordenDepartamentos = EOrden.NOMBRE;
        this.departamentosOrderedByNombre = true;
        this.ordenMunicipios = EOrden.NOMBRE;
        this.municipiosOrderedByNombre = true;
        this.ordenZonas = EOrden.NOMBRE;
        this.zonasOrderedByNombre = true;
        this.ordenSectores = EOrden.NOMBRE;
        this.sectoresOrderedByNombre = true;
        this.ordenComunas = EOrden.NOMBRE;
        this.comunasOrderedByNombre = true;
        this.ordenBarrios = EOrden.NOMBRE;
        this.barriosOrderedByNombre = true;
        this.ordenManzanasveredas = EOrden.NOMBRE;
        this.manzanasveredasOrderedByNombre = true;

        this.municipioGrupoFlag = true;
        this.zonaGrupoFlag = true;
        this.sectorGrupoFlag = true;
        this.comunaGrupoFlag = true;
        this.barrioGrupoFlag = true;
        this.manzanaVeredaGrupoFlag = true;

        this.departamentos = (ArrayList<Departamento>) this.getGeneralesService()
            .getCacheDepartamentosPorPais(Constantes.COLOMBIA);
        updateMunicipiosItemList();

    }

    //****************************************************************************************
    //Actions de los commandLink de ordenamiento de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
    //****************************************************************************************
    public void cambiarOrdenDepartamentos() {

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            this.ordenDepartamentos = EOrden.NOMBRE;
            this.departamentosOrderedByNombre = true;
        } else {
            this.ordenDepartamentos = EOrden.CODIGO;
            this.departamentosOrderedByNombre = false;
        }

        this.updateDepartamentosItemList();
    }

    public void cambiarOrdenMunicipios() {

        if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
            this.ordenMunicipios = EOrden.NOMBRE;
            this.municipiosOrderedByNombre = true;
        } else {
            this.ordenMunicipios = EOrden.CODIGO;
            this.municipiosOrderedByNombre = false;
        }

        this.updateMunicipiosItemList();
    }

    public void cambiarOrdenZonas() {

        if (this.ordenZonas.equals(EOrden.CODIGO)) {
            this.ordenZonas = EOrden.NOMBRE;
            this.zonasOrderedByNombre = true;
        } else {
            this.ordenZonas = EOrden.CODIGO;
            this.zonasOrderedByNombre = false;
        }

        this.updateZonasItemList();
    }

    public void cambiarOrdenSectores() {

        if (this.ordenSectores.equals(EOrden.CODIGO)) {
            this.ordenSectores = EOrden.NOMBRE;
            this.sectoresOrderedByNombre = true;
        } else {
            this.ordenSectores = EOrden.CODIGO;
            this.sectoresOrderedByNombre = false;
        }

        this.updateSectoresItemList();
    }

    public void cambiarOrdenComunas() {

        if (this.ordenComunas.equals(EOrden.CODIGO)) {
            this.ordenComunas = EOrden.NOMBRE;
            this.comunasOrderedByNombre = true;
        } else {
            this.ordenComunas = EOrden.CODIGO;
            this.comunasOrderedByNombre = false;
        }

        this.updateComunasItemList();
    }

    public void cambiarOrdenBarrios() {

        if (this.ordenBarrios.equals(EOrden.CODIGO)) {
            this.ordenBarrios = EOrden.NOMBRE;
            this.barriosOrderedByNombre = true;
        } else {
            this.ordenBarrios = EOrden.CODIGO;
            this.barriosOrderedByNombre = false;
        }

        this.updateBarriosItemList();
    }

    public void cambiarOrdenManzanasVeredas() {

        if (this.ordenManzanasveredas.equals(EOrden.CODIGO)) {
            this.ordenManzanasveredas = EOrden.NOMBRE;
            this.manzanasveredasOrderedByNombre = true;
        } else {
            this.ordenManzanasveredas = EOrden.CODIGO;
            this.manzanasveredasOrderedByNombre = false;
        }

        this.updateManzanasVeredasOrderedByNombreItemList();
    }

    //****************************************************************************************
    //Métodos de actualización de los itemList de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
    //****************************************************************************************
    private void updateDepartamentosItemList() {

        this.departamentosItemList = new ArrayList<SelectItem>();
        this.departamentosItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
            Collections.sort(this.departamentos);
        } else {
            Collections.sort(this.departamentos, Departamento.getComparatorNombre());
        }

        for (Departamento departamento : this.departamentos) {
            if (this.ordenDepartamentos.equals(EOrden.CODIGO)) {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getCodigo() + "-" +
                     departamento.getNombre()));
            } else {
                this.departamentosItemList.add(new SelectItem(departamento
                    .getCodigo(), departamento.getNombre()));
            }
        }
    }

    private void updateMunicipiosItemList() {

        this.municipiosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.selectedDepartamentoCod != null) {
            this.municipios = (ArrayList<Municipio>) this.getGeneralesService()
                .getCacheMunicipiosPorDepartamento(this.selectedDepartamentoCod);

            if (this.municipios != null && !this.municipios.isEmpty()) {
                if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                    Collections.sort(this.municipios);
                } else {
                    Collections.sort(this.municipios, Municipio.getComparatorNombre());
                }

                for (Municipio municipio : this.municipios) {
                    if (this.ordenMunicipios.equals(EOrden.CODIGO)) {
                        this.municipiosItemList.add(new SelectItem(municipio
                            .getCodigo(), municipio.getCodigo3Digitos() + "-" + municipio.
                            getNombre()));
                    } else {
                        this.municipiosItemList.add(new SelectItem(municipio.getCodigo(),
                            municipio.getNombre()));
                    }
                }
            }
        }
    }

    private void updateZonasItemList() {

        this.zonasItemList = new ArrayList<SelectItem>();
        this.zonasItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.selectedMunicipioCod != null) {
            this.zonas = (ArrayList<Zona>) this.getGeneralesService()
                .getZonasFromMunicipio(this.selectedMunicipioCod);

            if (this.zonas != null && !this.zonas.isEmpty()) {
                if (this.ordenZonas.equals(EOrden.CODIGO)) {
                    Collections.sort(this.zonas);
                } else {
                    Collections.sort(this.zonas, Zona.getComparatorNombre());
                }

                for (Zona zona : this.zonas) {
                    if (this.ordenZonas.equals(EOrden.CODIGO)) {
                        this.zonasItemList.add(new SelectItem(zona
                            .getCodigo(), zona.getCodigo3Digitos() + "-" + zona.getNombre()));
                    } else {
                        this.zonasItemList.add(new SelectItem(zona.getCodigo(),
                            zona.getNombre()));
                    }
                }
            }
        }
    }

    private void updateSectoresItemList() {

        this.sectoresItemList = new ArrayList<SelectItem>();
        this.sectoresItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.selectedZonaCod != null) {
            this.sectores = (ArrayList<Sector>) this.getGeneralesService()
                .getSectoresFromZona(this.selectedZonaCod);

            if (this.sectores != null && !this.sectores.isEmpty()) {
                if (this.ordenSectores.equals(EOrden.CODIGO)) {
                    Collections.sort(this.sectores);
                } else {
                    Collections.sort(this.sectores, Sector.getComparatorNombre());
                }

                for (Sector sector : this.sectores) {
                    if (this.ordenSectores.equals(EOrden.CODIGO)) {
                        this.sectoresItemList.add(new SelectItem(sector
                            .getCodigo(), sector.getCodigo3Digitos() + "-" + sector.getNombre()));
                    } else {
                        this.sectoresItemList.add(new SelectItem(sector.getCodigo(),
                            sector.getNombre()));
                    }
                }
            }
        }
    }

    private void updateComunasItemList() {

        this.comunasItemList = new ArrayList<SelectItem>();
        this.comunasItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.selectedSectorCod != null) {
            this.comunas = (ArrayList<Comuna>) this.getGeneralesService()
                .getComunasFromSector(this.selectedSectorCod);

            if (this.comunas != null && !this.comunas.isEmpty()) {
                if (this.ordenComunas.equals(EOrden.CODIGO)) {
                    Collections.sort(this.comunas);
                } else {
                    Collections.sort(this.comunas, Comuna.getComparatorNombre());
                }

                for (Comuna comuna : this.comunas) {
                    if (this.ordenComunas.equals(EOrden.CODIGO)) {
                        this.comunasItemList.add(new SelectItem(comuna
                            .getCodigo(), comuna.getCodigo3Digitos() + "-" + comuna.getNombre()));
                    } else {
                        this.comunasItemList.add(new SelectItem(comuna.getCodigo(),
                            comuna.getNombre()));
                    }
                }
            }
        }
    }

    private void updateBarriosItemList() {

        this.barriosItemList = new ArrayList<SelectItem>();
        this.barriosItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.selectedComunaCod != null) {
            this.barrios = (ArrayList<Barrio>) this.getGeneralesService()
                .getBarriosFromComuna(this.selectedComunaCod);

            if (this.barrios != null && !this.barrios.isEmpty()) {
                if (this.ordenBarrios.equals(EOrden.CODIGO)) {
                    Collections.sort(this.barrios);
                } else {
                    Collections.sort(this.barrios, Barrio.getComparatorNombre());
                }

                for (Barrio barrio : this.barrios) {
                    if (this.ordenBarrios.equals(EOrden.CODIGO)) {
                        this.barriosItemList.add(new SelectItem(barrio
                            .getCodigo(), barrio.getCodigo3Digitos() + "-" + barrio.getNombre()));
                    } else {
                        this.barriosItemList.add(new SelectItem(barrio.getCodigo(),
                            barrio.getNombre()));
                    }
                }
            }
        }
    }

    private void updateManzanasVeredasOrderedByNombreItemList() {

        this.manzanasveredasItemList = new ArrayList<SelectItem>();
        this.manzanasveredasItemList.add(new SelectItem("", this.DEFAULT_COMBOS));

        if (this.selectedBarrioCod != null) {
            this.manzanasveredas = (ArrayList<ManzanaVereda>) this.getGeneralesService()
                .getManzanaVeredaFromBarrio(this.selectedBarrioCod);

            if (this.manzanasveredas != null && !this.manzanasveredas.isEmpty()) {
                if (this.ordenManzanasveredas.equals(EOrden.CODIGO)) {
                    Collections.sort(this.manzanasveredas);
                } else {
                    Collections.sort(this.manzanasveredas, ManzanaVereda.getComparatorNombre());
                }

                for (ManzanaVereda manzanaVereda : this.manzanasveredas) {
                    if (this.ordenManzanasveredas.equals(EOrden.CODIGO)) {
                        this.manzanasveredasItemList.add(new SelectItem(manzanaVereda
                            .getCodigo(), manzanaVereda.getCodigo3Digitos() + "-" + manzanaVereda.
                            getNombre()));
                    } else {
                        this.manzanasveredasItemList.add(new SelectItem(manzanaVereda.getCodigo(),
                            manzanaVereda.getNombre()));
                    }
                }
            }
        }
    }

    //****************************************************************************************
    //Listeners de cambio en el comboBox de Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
    //****************************************************************************************
    public void onChangeDepartamentosListener() {
        this.selectedMunicipio = null;
        this.updateMunicipiosItemList();
    }

    public void onChangeMunicipiosListener() {
        this.selectedZonaCod = null;
        this.updateZonasItemList();
        this.municipioGrupoFlag = false;
    }

    public void onChangeZonaListener() {
        this.selectedSectorCod = null;
        updateSectoresItemList();
        this.zonaGrupoFlag = false;
    }

    public void onChangeSectorListener() {
        this.selectedComunaCod = null;
        updateComunasItemList();
        this.sectorGrupoFlag = false;
    }

    public void onChangeComunaListener() {
        this.selectedBarrioCod = null;
        updateBarriosItemList();
        this.comunaGrupoFlag = false;
    }

    public void onChangeBarrioListener() {
        this.selectedManzanaVeredaCod = null;
        updateManzanasVeredasOrderedByNombreItemList();
        this.barrioGrupoFlag = false;
    }

    public void onChangeManzanaListener() {
        this.manzanaVeredaGrupoFlag = false;
    }

    //****************************************************************************************
    //Métodos para agregar Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
    //****************************************************************************************
    public void agregar() {
        if (this.codigoIngresado.length() == 2) {
            this.agregarDepartamento();
        } else if (this.codigoIngresado.length() == 5) {
            this.agregarMunicipio();
        } else if (this.codigoIngresado.length() == 7) {
            this.agregarZona();
        } else if (this.codigoIngresado.length() == 9) {
            this.agregarSector();
        } else if (this.codigoIngresado.length() == 11) {
            this.agregarComuna();
        } else if (this.codigoIngresado.length() == 13) {
            this.agregarBarrio();
        } else if (this.codigoIngresado.length() == 17) {
            this.agregarManzanaVereda();
        }
    }

    public void agregarDepartamento() {
        Departamento deptoUsar = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(this.selectedDepartamentoCod);
        Departamento newDepartamento = new Departamento(this.codigoIngresado,
            deptoUsar.getPais(), this.nombreIngresado, deptoUsar.getUsuarioLog(),
            deptoUsar.getFechaLog());
        this.getGeneralesService().agregarDepartamento(newDepartamento);
        this.updateDepartamentosItemList();
        this.addMensajeInfo("Departamento creado");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    public void agregarMunicipio() {
        Departamento deptoUsar = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(this.selectedDepartamentoCod);
        Municipio newDepartamento = new Municipio(this.selectedDepartamentoCod +
             this.codigoIngresado, deptoUsar, this.nombreIngresado,
            deptoUsar.getUsuarioLog(), deptoUsar.getFechaLog());
        this.getGeneralesService().agregarMunicipio(newDepartamento);
        this.updateMunicipiosItemList();
        this.addMensajeInfo("Municipio creado");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    public void agregarZona() {
        Municipio deptoUsar = this.getGeneralesService().getCacheMunicipioByCodigo(
            this.selectedMunicipioCod);
        Zona newDepartamento = new Zona(this.selectedMunicipioCod + this.codigoIngresado, deptoUsar,
            this.nombreIngresado, deptoUsar.getUsuarioLog(),
            deptoUsar.getFechaLog());
        this.getGeneralesService().agregarZona(newDepartamento);
        this.updateZonasItemList();
        this.addMensajeInfo("Zona creada");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    public void agregarSector() {
        Zona deptoUsar = this.getGeneralesService().getCacheZonaByCodigo(this.selectedZonaCod);
        Sector newDepartamento = new Sector(this.selectedZonaCod + this.codigoIngresado, deptoUsar,
            this.nombreIngresado, deptoUsar.getUsuarioLog(),
            deptoUsar.getFechaLog());
        this.getGeneralesService().agregarSector(newDepartamento);
        this.updateSectoresItemList();
        this.addMensajeInfo("Sector creado");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    public void agregarComuna() {
        Sector deptoUsar = this.getGeneralesService().getCacheSectorByCodigo(this.selectedSectorCod);
        Comuna newDepartamento = new Comuna(this.selectedSectorCod + this.codigoIngresado,
            deptoUsar, this.nombreIngresado, deptoUsar.getUsuarioLog(),
            deptoUsar.getFechaLog());
        this.getGeneralesService().agregarComuna(newDepartamento);
        this.updateComunasItemList();
        this.addMensajeInfo("Comuna creada");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    public void agregarBarrio() {
        Comuna deptoUsar = this.getGeneralesService().getCacheComunaByCodigo(this.selectedComunaCod);
        Barrio newDepartamento = new Barrio(this.selectedComunaCod + this.codigoIngresado,
            deptoUsar, this.nombreIngresado, deptoUsar.getUsuarioLog(),
            deptoUsar.getFechaLog());
        this.getGeneralesService().agregarBarrio(newDepartamento);
        this.updateBarriosItemList();
        this.addMensajeInfo("Barrio creado");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    public void agregarManzanaVereda() {
        Barrio deptoUsar = this.getGeneralesService().getCacheBarrioByCodigo(this.selectedBarrioCod);
        ManzanaVereda newDepartamento = new ManzanaVereda(this.selectedBarrioCod +
             this.codigoIngresado, deptoUsar, this.nombreIngresado,
            deptoUsar.getUsuarioLog(), deptoUsar.getFechaLog());
        this.getGeneralesService().agregarManzanaVereda(newDepartamento);
        this.updateManzanasVeredasOrderedByNombreItemList();
        this.addMensajeInfo("ManzanaVereda creada");
        this.nombreIngresado = "";
        this.codigoIngresado = "";
    }

    //****************************************************************************************
    //Métodos para modificar Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
    //****************************************************************************************
    public void modificar() {
        if (this.codigoIngresado.length() == 2) {
            this.modificarDepartamento();
        } else if (this.codigoIngresado.length() == 5) {
            this.modificarMunicipio();
        } else if (this.codigoIngresado.length() == 7) {
            this.modificarZona();
        } else if (this.codigoIngresado.length() == 9) {
            this.modificarSector();
        } else if (this.codigoIngresado.length() == 11) {
            this.modificarComuna();
        } else if (this.codigoIngresado.length() == 13) {
            this.modificarBarrio();
        } else if (this.codigoIngresado.length() == 17) {
            this.modificarManzanaVereda();
        }
    }

    public void modificarDepartamento() {
        Departamento deptoModificar = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(this.selectedDepartamentoCod);
        deptoModificar = new Departamento(this.selectedDepartamentoCod, deptoModificar.getPais(),
            this.departamentoNombre, deptoModificar.getUsuarioLog(),
            deptoModificar.getFechaLog());
        this.getGeneralesService().modificarDepartamento(deptoModificar);
        this.addMensajeInfo("Departamento modificado");
    }

    public void modificarMunicipio() {
        Departamento deptoModificar = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(this.selectedDepartamentoCod);
        Municipio newDepartamento = new Municipio(this.selectedMunicipioCod, deptoModificar,
            this.municipioNombre, deptoModificar.getUsuarioLog(), deptoModificar.getFechaLog());
        this.getGeneralesService().modificarMunicipio(newDepartamento);
        this.addMensajeInfo("Municipio modificado");
    }

    public void modificarZona() {
        Municipio deptoModificar = this.getGeneralesService().getCacheMunicipioByCodigo(
            this.selectedMunicipioCod);
        Zona newDepartamento = new Zona(this.selectedZonaCod, deptoModificar, this.zonaNombre,
            deptoModificar.getUsuarioLog(), deptoModificar.getFechaLog());
        this.getGeneralesService().modificarZona(newDepartamento);
        this.addMensajeInfo("Zona modificada");
    }

    public void modificarSector() {
        Zona deptoModificar = this.getGeneralesService().getCacheZonaByCodigo(this.selectedZonaCod);
        Sector newDepartamento = new Sector(this.selectedSectorCod, deptoModificar,
            this.sectorNombre,
            deptoModificar.getUsuarioLog(), deptoModificar.getFechaLog());
        this.getGeneralesService().modificarSector(newDepartamento);
        this.addMensajeInfo("Sector modificado");
    }

    public void modificarComuna() {
        Sector deptoModificar = this.getGeneralesService().getCacheSectorByCodigo(
            this.selectedSectorCod);
        Comuna newDepartamento = new Comuna(this.selectedComunaCod, deptoModificar,
            this.comunaNombre,
            deptoModificar.getUsuarioLog(), deptoModificar.getFechaLog());
        this.getGeneralesService().modificarComuna(newDepartamento);
        this.addMensajeInfo("Comuna modificada");
    }

    public void modificarBarrio() {
        Comuna deptoModificar = this.getGeneralesService().getCacheComunaByCodigo(
            this.selectedComunaCod);
        Barrio newDepartamento = new Barrio(this.selectedBarrioCod, deptoModificar,
            this.barrioNombre,
            deptoModificar.getUsuarioLog(), deptoModificar.getFechaLog());
        this.getGeneralesService().modificarBarrio(newDepartamento);
        this.addMensajeInfo("Barrio modificado");
    }

    public void modificarManzanaVereda() {
        Barrio deptoModificar = this.getGeneralesService().getCacheBarrioByCodigo(
            this.selectedBarrioCod);
        ManzanaVereda newDepartamento = new ManzanaVereda(this.selectedManzanaVeredaCod,
            deptoModificar,
            this.manzanaVeredaNombre, deptoModificar.getUsuarioLog(),
            deptoModificar.getFechaLog());
        this.getGeneralesService().modificarManzanaVereda(newDepartamento);
        this.addMensajeInfo("ManzanaVereda modificada");
    }

    //****************************************************************************************
    //Métodos para eliminar Departamento, Municipio, Zona, Sector, Comuna, Barrio y Manzana/Vereda
    //****************************************************************************************
    public void borrar() {
        if (this.codigoIngresado.length() == 2) {
            this.borrarDepartamento();
        } else if (this.codigoIngresado.length() == 5) {
            this.borrarMunicipio();
        } else if (this.codigoIngresado.length() == 7) {
            this.borrarZona();
        } else if (this.codigoIngresado.length() == 9) {
            this.borrarSector();
        } else if (this.codigoIngresado.length() == 11) {
            this.borrarComuna();
        } else if (this.codigoIngresado.length() == 13) {
            this.borrarBarrio();
        } else if (this.codigoIngresado.length() == 17) {
            this.borrarManzanaVereda();
        }
    }

    public void borrarDepartamento() {
        Departamento deptoBorrar = this.getGeneralesService()
            .getCacheDepartamentoByCodigo(this.selectedDepartamentoCod);
        this.getGeneralesService().borrarDepartamento(deptoBorrar);
        this.addMensajeInfo("Departamento eliminado");
    }

    public void borrarMunicipio() {
        Municipio deptoBorrar = this.getGeneralesService().getCacheMunicipioByCodigo(
            this.selectedMunicipioCod);
        this.getGeneralesService().borrarMunicipio(deptoBorrar);
        this.addMensajeInfo("Municipio eliminado");
    }

    public void borrarZona() {
        Zona deptoBorrar = this.getGeneralesService().getCacheZonaByCodigo(this.selectedZonaCod);
        this.getGeneralesService().borrarZona(deptoBorrar);
        this.addMensajeInfo("Zona eliminada");
    }

    public void borrarSector() {
        Sector deptoBorrar = this.getGeneralesService().getCacheSectorByCodigo(
            this.selectedSectorCod);
        this.getGeneralesService().borrarSector(deptoBorrar);
        this.addMensajeInfo("Sector eliminado");
    }

    public void borrarComuna() {
        Comuna deptoBorrar = this.getGeneralesService().getCacheComunaByCodigo(
            this.selectedComunaCod);
        this.getGeneralesService().borrarComuna(deptoBorrar);
        this.addMensajeInfo("Comuna eliminada");
    }

    public void borrarBarrio() {
        Barrio deptoBorrar = this.getGeneralesService().getCacheBarrioByCodigo(
            this.selectedBarrioCod);
        this.getGeneralesService().borrarBarrio(deptoBorrar);
        this.addMensajeInfo("Barrio eliminado");
    }

    public void borrarManzanaVereda() {
        ManzanaVereda deptoBorrar = this.getGeneralesService()
            .getCacheManzanaVeredaByCodigo(this.selectedManzanaVeredaCod);
        this.getGeneralesService().borrarManzanaVereda(deptoBorrar);
        this.addMensajeInfo("ManzanaVereda eliminada");
    }

    //****************************************************************************************
    //Getters y Setters
    //****************************************************************************************
    public ArrayList<SelectItem> getZonasItemList() {
        return this.zonasItemList;
    }

    public void setZonasItemList(ArrayList<SelectItem> zonasItemList) {
        this.zonasItemList = zonasItemList;
    }

    public Departamento getSelectedDepartamento() {
        return this.selectedDepartamento;
    }

    public void setSelectedDepartamento(Departamento selectedDepartamento) {
        this.selectedDepartamento = selectedDepartamento;
    }

    public Municipio getSelectedMunicipio() {
        return this.selectedMunicipio;
    }

    public void setSelectedMunicipio(Municipio selectedMunicipio) {
        this.selectedMunicipio = selectedMunicipio;
    }

    public String getSelectedDepartamentoCod() {
        return this.selectedDepartamentoCod;
    }

    public void setSelectedDepartamentoCod(String codigo) {

        this.selectedDepartamentoCod = codigo;
        Departamento dptoSeleccionado = null;
        if (codigo != null && this.departamentos != null) {
            for (Departamento departamento : this.departamentos) {
                if (codigo.equals(departamento.getCodigo())) {
                    dptoSeleccionado = departamento;
                    break;
                }
            }
        }
        this.selectedDepartamento = dptoSeleccionado;
        this.departamentoNombre = dptoSeleccionado.getNombre();
    }

    public String getSelectedMunicipioCod() {
        return this.selectedMunicipioCod;
    }

    public void setSelectedMunicipioCod(String codigo) {
        this.selectedMunicipioCod = codigo;

        Municipio municipioSelected = null;
        if (codigo != null && this.municipios != null) {
            for (Municipio municipio : this.municipios) {
                if (codigo.equals(municipio.getCodigo())) {
                    municipioSelected = municipio;
                    break;
                }
            }
        }
        this.selectedMunicipio = municipioSelected;
        this.municipioNombre = municipioSelected.getNombre();
    }

    public String getSelectedZonaCod() {
        return this.selectedZonaCod;
    }

    public void setSelectedZonaCod(String codigo) {
        this.selectedZonaCod = codigo;

        Zona zonaSelected = null;
        if (codigo != null && this.zonas != null) {
            for (Zona zona : this.zonas) {
                if (codigo.equals(zona.getCodigo())) {
                    zonaSelected = zona;
                    break;
                }
            }
        }
        this.selectedZona = zonaSelected;
        this.zonaNombre = zonaSelected.getNombre();
    }

    public String getSelectedSectorCod() {
        return this.selectedSectorCod;
    }

    public void setSelectedSectorCod(String codigo) {
        this.selectedSectorCod = codigo;

        Sector sectorSelected = null;
        if (codigo != null && this.sectores != null) {
            for (Sector sector : this.sectores) {
                if (codigo.equals(sector.getCodigo())) {
                    sectorSelected = sector;
                    break;
                }
            }
        }
        this.selectedSector = sectorSelected;
        this.sectorNombre = sectorSelected.getNombre();
    }

    public String getSelectedComunaCod() {
        return this.selectedComunaCod;
    }

    public void setSelectedComunaCod(String codigo) {
        this.selectedComunaCod = codigo;

        Comuna comunaSelected = null;
        if (codigo != null && this.comunas != null) {
            for (Comuna comuna : this.comunas) {
                if (codigo.equals(comuna.getCodigo())) {
                    comunaSelected = comuna;
                    break;
                }
            }
        }
        this.selectedComuna = comunaSelected;
        this.comunaNombre = comunaSelected.getNombre();
    }

    public String getSelectedBarrioCod() {
        return this.selectedBarrioCod;
    }

    public void setSelectedBarrioCod(String codigo) {
        this.selectedBarrioCod = codigo;

        Barrio barrioSelected = null;
        if (codigo != null && this.barrios != null) {
            for (Barrio barrio : this.barrios) {
                if (codigo.equals(barrio.getCodigo())) {
                    barrioSelected = barrio;
                    break;
                }
            }
        }
        this.selectedBarrio = barrioSelected;
        this.barrioNombre = barrioSelected.getNombre();
    }

    public String getSelectedManzanaVeredaCod() {
        return this.selectedManzanaVeredaCod;
    }

    public void setSelectedManzanaVeredaCod(String codigo) {
        this.selectedManzanaVeredaCod = codigo;

        ManzanaVereda manzanaVeredaSelected = null;
        if (codigo != null && this.manzanasveredas != null) {
            for (ManzanaVereda manzanaVereda : this.manzanasveredas) {
                if (codigo.equals(manzanaVereda.getCodigo())) {
                    manzanaVeredaSelected = manzanaVereda;
                    break;
                }
            }
        }
        this.selectedManzanaVereda = manzanaVeredaSelected;
        this.manzanaVeredaNombre = manzanaVeredaSelected.getNombre();
    }

    public ArrayList<Departamento> getDepartamentos() {
        return this.departamentos;
    }

    public void setDepartamentos(ArrayList<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public ArrayList<Municipio> getMunicipios() {
        return this.municipios;
    }

    public void setMunicipios(ArrayList<Municipio> municipios) {
        this.municipios = municipios;
    }

    public ArrayList<SelectItem> getDepartamentosItemList() {
        if (this.departamentosItemList == null) {
            this.updateDepartamentosItemList();
        }

        return this.departamentosItemList;
    }

    public void setDepartamentosItemList(ArrayList<SelectItem> departamentosItemList) {
        this.departamentosItemList = departamentosItemList;
    }

    public ArrayList<SelectItem> getMunicipiosItemList() {
        return this.municipiosItemList;
    }

    public void setMunicipiosItemList(ArrayList<SelectItem> municipiosItemList) {
        this.municipiosItemList = municipiosItemList;
    }

    public EOrden getOrdenDepartamentos() {
        return this.ordenDepartamentos;
    }

    public void setOrdenDepartamentos(EOrden ordenDepartamentos) {
        this.ordenDepartamentos = ordenDepartamentos;
    }

    public EOrden getOrdenMunicipios() {
        return this.ordenMunicipios;
    }

    public void setOrdenMunicipios(EOrden ordenMunicipios) {
        this.ordenMunicipios = ordenMunicipios;
    }

    public boolean isMunicipiosOrderedByNombre() {
        return this.municipiosOrderedByNombre;
    }

    public void setMunicipiosOrderedByNombre(boolean municipiosOrderedByNombre) {
        this.municipiosOrderedByNombre = municipiosOrderedByNombre;
    }

    public boolean isDepartamentosOrderedByNombre() {
        return this.departamentosOrderedByNombre;
    }

    public void setDepartamentosOrderedByNombre(boolean departamentosOrderedByNombre) {
        this.departamentosOrderedByNombre = departamentosOrderedByNombre;
    }

    public boolean isZonasOrderedByNombre() {
        return this.zonasOrderedByNombre;
    }

    public void setZonasOrderedByNombre(boolean zonasOrderedByNombre) {
        this.zonasOrderedByNombre = zonasOrderedByNombre;
    }

    public ArrayList<Zona> getZonas() {
        return zonas;
    }

    public void setZonas(ArrayList<Zona> zonas) {
        this.zonas = zonas;
    }

    public Zona getSelectedZona() {
        return selectedZona;
    }

    public void setSelectedZona(Zona selectedZona) {
        this.selectedZona = selectedZona;
    }

    public Sector getSelectedSector() {
        return selectedSector;
    }

    public void setSelectedSector(Sector selectedSector) {
        this.selectedSector = selectedSector;
    }

    public ArrayList<Sector> getSectores() {
        return sectores;
    }

    public void setSectores(ArrayList<Sector> sectores) {
        this.sectores = sectores;
    }

    public ArrayList<SelectItem> getSectoresItemList() {
        return sectoresItemList;
    }

    public void setSectoresItemList(ArrayList<SelectItem> sectoresItemList) {
        this.sectoresItemList = sectoresItemList;
    }

    public ArrayList<Comuna> getComunas() {
        return comunas;
    }

    public void setComunas(ArrayList<Comuna> comunas) {
        this.comunas = comunas;
    }

    public ArrayList<Barrio> getBarrios() {
        return barrios;
    }

    public void setBarrios(ArrayList<Barrio> barrios) {
        this.barrios = barrios;
    }

    public ArrayList<ManzanaVereda> getManzanasveredas() {
        return manzanasveredas;
    }

    public void setManzanasveredas(ArrayList<ManzanaVereda> manzanasveredas) {
        this.manzanasveredas = manzanasveredas;
    }

    public ArrayList<SelectItem> getComunasItemList() {
        return comunasItemList;
    }

    public void setComunasItemList(ArrayList<SelectItem> comunasItemList) {
        this.comunasItemList = comunasItemList;
    }

    public ArrayList<SelectItem> getBarriosItemList() {
        return barriosItemList;
    }

    public void setBarriosItemList(ArrayList<SelectItem> barriosItemList) {
        this.barriosItemList = barriosItemList;
    }

    public ArrayList<SelectItem> getManzanasveredasItemList() {
        return manzanasveredasItemList;
    }

    public void setManzanasveredasItemList(ArrayList<SelectItem> manzanasveredasItemList) {
        this.manzanasveredasItemList = manzanasveredasItemList;
    }

    public EOrden getOrdenZonas() {
        return ordenZonas;
    }

    public void setOrdenZonas(EOrden ordenZonas) {
        this.ordenZonas = ordenZonas;
    }

    public EOrden getOrdenSectores() {
        return ordenSectores;
    }

    public void setOrdenSectores(EOrden ordenSectores) {
        this.ordenSectores = ordenSectores;
    }

    public EOrden getOrdenComunas() {
        return ordenComunas;
    }

    public void setOrdenComunas(EOrden ordenComunas) {
        this.ordenComunas = ordenComunas;
    }

    public EOrden getOrdenBarrios() {
        return ordenBarrios;
    }

    public void setOrdenBarrios(EOrden ordenBarrios) {
        this.ordenBarrios = ordenBarrios;
    }

    public EOrden getOrdenManzanasveredas() {
        return ordenManzanasveredas;
    }

    public void setOrdenManzanasveredas(EOrden ordenManzanasveredas) {
        this.ordenManzanasveredas = ordenManzanasveredas;
    }

    public Comuna getSelectedComuna() {
        return selectedComuna;
    }

    public void setSelectedComuna(Comuna selectedComuna) {
        this.selectedComuna = selectedComuna;
    }

    public Barrio getSelectedBarrio() {
        return selectedBarrio;
    }

    public void setSelectedBarrio(Barrio selectedBarrio) {
        this.selectedBarrio = selectedBarrio;
    }

    public ManzanaVereda getSelectedManzanaVereda() {
        return selectedManzanaVereda;
    }

    public void setSelectedManzanaVereda(ManzanaVereda selectedManzanaVereda) {
        this.selectedManzanaVereda = selectedManzanaVereda;
    }

    public boolean isSectoresOrderedByNombre() {
        return sectoresOrderedByNombre;
    }

    public void setSectoresOrderedByNombre(boolean sectoresOrderedByNombre) {
        this.sectoresOrderedByNombre = sectoresOrderedByNombre;
    }

    public boolean isComunasOrderedByNombre() {
        return comunasOrderedByNombre;
    }

    public void setComunasOrderedByNombre(boolean comunasOrderedByNombre) {
        this.comunasOrderedByNombre = comunasOrderedByNombre;
    }

    public boolean isBarriosOrderedByNombre() {
        return barriosOrderedByNombre;
    }

    public void setBarriosOrderedByNombre(boolean barriosOrderedByNombre) {
        this.barriosOrderedByNombre = barriosOrderedByNombre;
    }

    public boolean isManzanasveredasOrderedByNombre() {
        return manzanasveredasOrderedByNombre;
    }

    public void setManzanasveredasOrderedByNombre(boolean manzanasveredasOrderedByNombre) {
        this.manzanasveredasOrderedByNombre = manzanasveredasOrderedByNombre;
    }

    public String getNombreIngresado() {
        return nombreIngresado;
    }

    public void setNombreIngresado(String nombreIngresado) {
        this.nombreIngresado = nombreIngresado;
    }

    public String getCodigoIngresado() {
        return codigoIngresado;
    }

    public void setCodigoIngresado(String codigoIngresado) {
        this.codigoIngresado = codigoIngresado;
    }

    public String getMunicipioNombre() {
        return municipioNombre;
    }

    public void setMunicipioNombre(String municipioNombre) {
        this.municipioNombre = municipioNombre;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getZonaNombre() {
        return zonaNombre;
    }

    public void setZonaNombre(String zonaNombre) {
        this.zonaNombre = zonaNombre;
    }

    public String getSectorNombre() {
        return sectorNombre;
    }

    public void setSectorNombre(String sectorNombre) {
        this.sectorNombre = sectorNombre;
    }

    public String getComunaNombre() {
        return comunaNombre;
    }

    public void setComunaNombre(String comunaNombre) {
        this.comunaNombre = comunaNombre;
    }

    public String getBarrioNombre() {
        return barrioNombre;
    }

    public void setBarrioNombre(String barrioNombre) {
        this.barrioNombre = barrioNombre;
    }

    public String getManzanaVeredaNombre() {
        return manzanaVeredaNombre;
    }

    public void setManzanaVeredaNombre(String manzanaVeredaNombre) {
        this.manzanaVeredaNombre = manzanaVeredaNombre;
    }

    public boolean isMunicipioGrupoFlag() {
        return municipioGrupoFlag;
    }

    public void setMunicipioGrupoFlag(boolean municipioGrupoFlag) {
        this.municipioGrupoFlag = municipioGrupoFlag;
    }

    public boolean isZonaGrupoFlag() {
        return zonaGrupoFlag;
    }

    public void setZonaGrupoFlag(boolean zonaGrupoFlag) {
        this.zonaGrupoFlag = zonaGrupoFlag;
    }

    public boolean isSectorGrupoFlag() {
        return sectorGrupoFlag;
    }

    public void setSectorGrupoFlag(boolean sectorGrupoFlag) {
        this.sectorGrupoFlag = sectorGrupoFlag;
    }

    public boolean isComunaGrupoFlag() {
        return comunaGrupoFlag;
    }

    public void setComunaGrupoFlag(boolean comunaGrupoFlag) {
        this.comunaGrupoFlag = comunaGrupoFlag;
    }

    public boolean isBarrioGrupoFlag() {
        return barrioGrupoFlag;
    }

    public void setBarrioGrupoFlag(boolean barrioGrupoFlag) {
        this.barrioGrupoFlag = barrioGrupoFlag;
    }

    public boolean isManzanaVeredaGrupoFlag() {
        return manzanaVeredaGrupoFlag;
    }

    public void setManzanaVeredaGrupoFlag(boolean manzanaVeredaGrupoFlag) {
        this.manzanaVeredaGrupoFlag = manzanaVeredaGrupoFlag;
    }

}
