package co.gov.igac.snc.web.listener;

import org.springframework.context.ApplicationContext;

/**
 * ContextListener donde se cargan e inicializan Parámetros de la aplicación web
 *
 * @author juan.mendez
 *
 */
public interface IContextListener {

    /**
     * métodos getter y setter para la propiedad de nombre de la aplicación como se ve en la url del
     * navegador del cliente
     *
     * @author pedro.garcia
     */
    public String getClientAppNameUrl();

    /**
     *
     * @param appName
     */
    public void setClientAppNameUrl(String appName);

    /**
     * Obtiene Referencia al Contexto de Spring.
     *
     * @author juan.mendez
     * @return
     */
    public ApplicationContext getApplicationContext();

}
