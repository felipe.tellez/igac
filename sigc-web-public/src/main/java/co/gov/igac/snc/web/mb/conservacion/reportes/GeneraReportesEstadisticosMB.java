package co.gov.igac.snc.web.mb.conservacion.reportes;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.sigc.reportes.EReporteServiceSNC;
import co.gov.igac.sigc.reportes.EReportesEstadisticos;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.mb.util.CombosDeptosMunisMB;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.ReporteDTO;
import co.gov.igac.snc.web.util.ReportesUtil;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Managed bean para generar reportes estadísticos
 *
 * @author javier.aponte
 */
@Component("generaReportesEstadisticos")
@Scope("session")
public class GeneraReportesEstadisticosMB extends ProcessFlowManager {

    private static final long serialVersionUID = -8676935321345468431L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneraReportesEstadisticosMB.class);

    /**
     * variable que almacena los datos del usuario en sesión
     */
    private UsuarioDTO usuario;

    /**
     * Código del departamento seleccionado
     */
    private String selectedDepartamentoCod;

    /**
     * Código del municipio seleccionado
     */
    private String selectedMunicipioCod;

    /**
     * Código del reporte seleccionado
     */
    private Long reporteSeleccionado;

    /**
     * Url del reporte dinamico
     */
    private ReporteDTO reporteEstadisticoDTO;

    /**
     * Utilidad para la creación del reporte
     */
    private ReportesUtil reportsService = ReportesUtil.getInstance();

    @Autowired
    private TareasPendientesMB tareasPendientesMB;

    public ReporteDTO getReporteEstadisticoDTO() {
        return reporteEstadisticoDTO;
    }

    public void setReporteEstadisticoDTO(ReporteDTO reporteEstadisticoDTO) {
        this.reporteEstadisticoDTO = reporteEstadisticoDTO;
    }

    public Long getReporteSeleccionado() {
        return reporteSeleccionado;
    }

    public void setReporteSeleccionado(Long reporteSeleccionado) {
        this.reporteSeleccionado = reporteSeleccionado;
    }

    // -----------------------------------MÉTODOS--------------------------------------------------------
    @PostConstruct
    public void init() {

        LOGGER.debug("init GeneraReportesEstadisticosMB");

        this.usuario = MenuMB.getMenu().getUsuarioDto();

    }

    public void realizarConsultaReporte() {

        CombosDeptosMunisMB combosDeptosMunisMB = (CombosDeptosMunisMB) UtilidadesWeb
            .getManagedBean("combosDeptosMunis");

        this.selectedDepartamentoCod = combosDeptosMunisMB.getSelectedDepartamentoCod();

        this.selectedMunicipioCod = combosDeptosMunisMB.getSelectedMunicipioCod();

        Date fechaActual = new Date();

        Map<String, String> parameters = new HashMap<String, String>();

        EReporteServiceSNC enumeracionReporte = null;

        if (EReportesEstadisticos.DISTRIBUCION_POR_RANGOS.getCodigo().equals(
            this.reporteSeleccionado)) {
            parameters.put("MUNICIPIO", selectedMunicipioCod);
            parameters.put("VIGENCIA", String.valueOf(UtilidadesWeb.getAnioDeUnDate(fechaActual)));

            enumeracionReporte = EReporteServiceSNC.ESTADISTICO_DISTRIBUCION_POR_RANGOS;
        }

        if (EReportesEstadisticos.PREDIOS_POR_ZONA.getCodigo().equals(this.reporteSeleccionado)) {
            parameters.put("MUNICIPIO", selectedMunicipioCod);
            parameters.put("VIGENCIA", String.valueOf(UtilidadesWeb.getAnioDeUnDate(fechaActual)));

            enumeracionReporte = EReporteServiceSNC.ESTADISTICO_PREDIOS_POR_ZONA;
        }

        if (EReportesEstadisticos.GLOBALES_POR_DEPARTAMENTO.getCodigo().equals(
            this.reporteSeleccionado)) {
            parameters.put("MUNICIPIO", selectedDepartamentoCod + "%");
            parameters.put("VIGENCIA", String.valueOf(UtilidadesWeb.getAnioDeUnDate(fechaActual)));

            enumeracionReporte = EReporteServiceSNC.ESTADISTICO_GLOBALES_POR_DEPARTAMENTO;
        }

        if (enumeracionReporte != null) {
            this.reporteEstadisticoDTO = this.reportsService.generarReporte(parameters,
                enumeracionReporte, usuario);
        }
    }

    @Override
    public boolean validateProcess() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setupProcessMessage() {
        // TODO Auto-generated method stub

    }

    @Override
    public void doDatabaseStatesUpdate() {
        // TODO Auto-generated method stub

    }
//--------------------------------------------------------------------------------------------------    

    /**
     * Método encargado de terminar la sesión sobre el botón cerrar
     *
     * @author javier.barajas
     */
    public String cerrar() {

        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");

        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;
    }

//end of class
}
