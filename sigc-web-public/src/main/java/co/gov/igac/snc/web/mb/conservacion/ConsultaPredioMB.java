/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.Departamento;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.persistence.entity.generales.Municipio;
import co.gov.igac.snc.persistence.entity.conservacion.PPersonaPredio;
import co.gov.igac.snc.persistence.entity.conservacion.Predio;
import co.gov.igac.snc.persistence.entity.formacion.UsoConstruccion;
import co.gov.igac.snc.persistence.entity.generales.Dominio;
import co.gov.igac.snc.persistence.entity.generales.MunicipioComplemento;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDetallePredio;
import co.gov.igac.snc.persistence.util.EDominio;
import co.gov.igac.snc.persistence.util.EPredioBloqueoTipoBloqueo;
import co.gov.igac.snc.persistence.util.EPredioTipoCatastro;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.util.Constantes;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.FiltroDatosConsultaPredio;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * Clase que hace las veces de managed bean para ConsultaPredio
 *
 * @author pedro.garcia
 *
 * @version 2.0
 * @cu se usa en varios en donde se requiere hacer búsqueda de predios
 */
@Component("consultaPredio")
@Scope("session")
public class ConsultaPredioMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = -5172998811646733085L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaPredioMB.class);

    /** Interfaces de servicio */
    /**
     * contexto usado para las cosas de documentos
     */
    @Autowired
    private IContextListener contextoWeb;

    /**
     * Objeto usado como contenedor de los datos suministrados para la consulta
     */
    private FiltroDatosConsultaPredio datosConsultaPredio;

    /**
     * listas con los valores de los combos de selección para la búsqueda
     */
    private ArrayList<SelectItem> territorialesItemList;
    private ArrayList<SelectItem> UOCItemList;
    private ArrayList<SelectItem> departamentosItemList;
    private ArrayList<SelectItem> municipiosItemList;
    private ArrayList<SelectItem> usosConstruccionItemList;
    private ArrayList<SelectItem> destinosPredioItemList;

    private boolean renderResultPanel;

    public ArrayList<Object[]> prediosSeleccionados = new ArrayList<Object[]>();

    private LazyDataModel<Object[]> lazyModel;

    private Object[] selectedPredioRow;

    //OJO: no borrar
    private String selectedPredioNumeroPredial;

    /**
     * No guarda un número predial sino el arreglo de objetos completo que se muestra en la lista
     */
    private Object[] selectedPredio2;

    /**
     * Primero se usó el Object[], pero luego hubo que usar este mismo mb cuando se tienen objetos
     * Predio
     */
    private Predio selectedPredio1;

    private Long selectedPredioId;

    /**
     * nombre de la aplicación web (esta aplicacíon). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /**
     * se usa para pasar un id que sirva para saber qué método se debe ejecutar cuando en el CC de
     * consulta de predios se selecciona uno de los predios del resultado de la búsqueda
     */
    private int listenerMethodIdentifier;

    /**
     * se usa para que no se consulte lista de departamentos usando la territorial ya que en
     * consulta inmob no se usa
     */
    private boolean seUsaTerritorial;

    /**
     * Se usa para determinar si los departamentos se setean desde otro managedBean (FIJOS) así no
     * se usan las listas estandares (TODOS los DEPTOS)
     *
     * @author christian.rodriguez
     */
    private boolean seUsanDeptosFijos;

    /**
     * Estas variables se usan para que el mapa sepa qué capa y qué herramientas mostrar
     */
    private String moduleLayer;
    private String moduleTools;
    private UsuarioDTO usuario;
    private boolean usuarioDelegado;
    private boolean usuarioHabilitado;

    private List<Municipio> municipiosDelegados;
    private List<Municipio> municipiosHabilitadas;

    /**
     * id del trámite del que se van a consultar los predios asociados, para mostrarlos y ver
     * detalles de cada uno
     */
    private Long idTramiteDePredios;

    /**
     * lista de predios asociados a un determinado trámite
     */
    private ArrayList<TramiteDetallePredio> prediosDeTramiteEnglobe;

    /**
     * lista de personas predios asociados a un determinado trámite
     */
    private PPersonaPredio personaPredios;

    /**
     * lista de personas predios asociados a un determinado trámite
     */
    private ArrayList<Long> idPredios;

    //------------- banderas  ----
    /**
     * se usa para evitar que haga una búsqueda inicial que sucede sin que debiera ser así al hacer
     * load del lazy datamodel cuando se ejecuta el constructor del mb
     */
    private boolean mustSearch;
    private boolean predioFiscal;
    /**
     * id del predio del que se van a consultar los predios asociados, para mostrarlos y ver
     * detalles de cada uno
     */
    private Long idDePersonasPredios;

    /**
     * se usa no renderizar el boton Usar predio seleccionado(s) en el caso de que exista un predio
     * bloqueo en la lista de predios seleccionados.
     */
    private boolean predioBloqueo;

    private String numeroPredial;

    /**
     *
     * Se usa para identificar cuando un predio es migrado desde Cobol, Actualmente se usa para
     * renderizar la pestaña de "Origen del Predio", en el detalle de un predio seleccionado.
     *
     * @author juan.cruz
     * @date 30-06-2017
     */
    private boolean migradoCobol;

    /**
     *
     * Se usa para identificar cuando el predio que se está consultando es un predio Origen.
     *
     * @author juan.cruz
     * @date 21-11-2017
     */
    private boolean predioOrigen;

    /**
     * Se usa para identificar el orden de los predios hijos y padres
     *
     * @author juan.cruz
     * @date 28-12-2017
     */
    private ArrayList<Object[]> arbolFamiliarPredios;

    /**
     * Variable usada para mapear el contenido de un objeto {@link Predio} al set del objeto
     * selectedPredio2 de tipo {@link Object[]} usado para las operaciones en los MB que consumen
     * {@link ConsultaPredioMB}
     */
    private Predio predioUtil;

    //--------------------- Getters and Setters  -------------------------------------------------------------------
    public String getNumeroPredial() {
        return numeroPredial;
    }

    public void setNumeroPredial(String numeroPredial) {
        this.numeroPredial = numeroPredial;
    }

    public ArrayList<TramiteDetallePredio> getPrediosDeTramiteEnglobe() {
        return this.prediosDeTramiteEnglobe;
    }

    public ArrayList<Long> getIdPredios() {
        return idPredios;
    }

    public void setIdPredios(ArrayList<Long> idPredios) {
        this.idPredios = idPredios;
    }

    public PPersonaPredio getPersonaPredios() {
        return personaPredios;
    }

    public void setPersonaPredios(PPersonaPredio personaPredios) {
        this.personaPredios = personaPredios;
    }

    public void setPrediosDeTramiteEnglobe(ArrayList<TramiteDetallePredio> prediosDeTramite) {
        this.prediosDeTramiteEnglobe = prediosDeTramite;
    }

    public ArrayList<SelectItem> getUOCItemList() {
        return UOCItemList;
    }

    public void setUOCItemList(ArrayList<SelectItem> UOCItemList) {
        this.UOCItemList = UOCItemList;
    }

    public boolean isUsuarioDelegado() {
        return usuarioDelegado;
    }

    public void setUsuarioDelegado(boolean usuarioDelegado) {
        this.usuarioDelegado = usuarioDelegado;
    }
    
    public boolean isUsuarioHabilitado(){
        return usuarioHabilitado;
    }
    
    public void setUsuarioHabilitado(boolean usuarioHabilitado){
        this.usuarioHabilitado=usuarioHabilitado;
    }

    public Long getIdTramiteDePredios() {
        return this.idTramiteDePredios;
    }

    public void setIdTramiteDePredios(Long idTramiteDePredios) {
        this.idTramiteDePredios = idTramiteDePredios;
    }

    public int getListenerMethodIdentifier() {
        return this.listenerMethodIdentifier;
    }

    public void setListenerMethodIdentifier(int listenerMethodIdentifier) {
        this.listenerMethodIdentifier = listenerMethodIdentifier;
    }

    public boolean isSeUsaTerritorial() {
        return this.seUsaTerritorial;
    }

    public void setSeUsaTerritorial(boolean seUsaTerritorial) {
        this.seUsaTerritorial = seUsaTerritorial;
    }

    public boolean isSeUsanDeptosFijos() {
        return this.seUsanDeptosFijos;
    }

    public void setSeUsanDeptosFijos(boolean seUsanDeptosFijos) {
        this.seUsanDeptosFijos = seUsanDeptosFijos;
    }

    // --------------------------------------------------------------------------------------------------
    public ArrayList<Object[]> getPrediosSeleccionados() {
        return this.prediosSeleccionados;
    }

//--------------------------------------------------------------------------------------------------
    public ArrayList<SelectItem> getUsosConstruccionItemList() {
        return usosConstruccionItemList;
    }

    public void setUsosConstruccionItemList(List<UsoConstruccion> usosConstruccionList) {

        for (UsoConstruccion uc : usosConstruccionList) {
            this.usosConstruccionItemList.add(new SelectItem(uc.getId(), uc.getNombre()));
        }
    }

//--------------------------------------------------------------------------------------------------
    public ArrayList<SelectItem> getDestinosPredioItemList() {
        return destinosPredioItemList;
    }

    public void setDestinosPredioItemList(
        ArrayList<SelectItem> destinosPredioItemList) {
        this.destinosPredioItemList = destinosPredioItemList;
    }

    public Object[] getSelectedPredioRow() {
        return this.selectedPredioRow;
    }

    public void setSelectedPredioRow(Object[] selectedPredioRow) {
        this.selectedPredioRow = selectedPredioRow;
        //this.selectedPredioNumeroPredial = (String) this.selectedPredioRow[15];
    }

    public String getApplicationClientName() {
        return this.applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public Long getIdDePersonasPredios() {
        return idDePersonasPredios;
    }

    public void setIdDePersonasPredios(Long idDePersonasPredios) {
        this.idDePersonasPredios = idDePersonasPredios;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * getters y setters del predio seleccionado cuando es Object[]
     */
    public Object[] getSelectedPredio2() {
        return this.selectedPredio2;
    }

    public void setSelectedPredio2(Object[] selectedPredio2_p) {
        this.selectedPredio2 = selectedPredio2_p;

        //D: el id del predio viene en la posición 2 del arreglo
        this.selectedPredioId = Long.valueOf(this.selectedPredio2[2].toString());
        this.selectedPredioNumeroPredial = (String) this.selectedPredio2[15];
        this.migradoCobol = this.getConservacionService().isPredioMigradoCobol(
            (String) this.selectedPredio2[14]);
    }

    /**
     * getters y setters del predio seleccionado cuando es Predio. OJO: todo se debe manejar como si
     * el predio fuera de la forma Object[] para no tener que cambiar muchas cosas.
     */
    public Predio getSelectedPredio1() {
        return this.selectedPredio1;
    }

    public void setSelectedPredio1(Predio selectedPredio_p) {
        this.selectedPredio1 = selectedPredio_p;
        this.selectedPredioId = Long.valueOf(selectedPredio_p.getId());
        this.selectedPredioNumeroPredial = (String) selectedPredio_p.getNumeroPredial();
        this.migradoCobol = this.getConservacionService().isPredioMigradoCobol(
            this.selectedPredioNumeroPredial);
    }

    public Predio getPredioUtil() {
        return predioUtil;
    }

    public void setPredioUtil(Predio predioUtil) {
        this.predioUtil = predioUtil;
        if (this.predioUtil != null) {
            FiltroDatosConsultaPredio filtroConsulta = new FiltroDatosConsultaPredio();
            filtroConsulta.setNumeroPredial(this.predioUtil.getNumeroPredial());
            List<Object[]> rows = this.getConservacionService().buscarPredios(filtroConsulta, 1, 1);
            this.selectedPredio2 = (rows != null && rows.size() > 0) ? rows.get(0) :
                this.selectedPredio2;
        }
    }

    public String getSelectedPredioNumeroPredial() {
        //D: el número predial viene en la posición 15 del arreglo
        return (String) this.selectedPredio2[15];
    }

    public void setSelectedPredioNumeroPredial(String selectedPredioNumeroPredial) {
        this.selectedPredioNumeroPredial = selectedPredioNumeroPredial;
    }

    public Long getSelectedPredioId() {
        //return (Long)this.selectedPredioNumeroPredial2[2];
        return this.selectedPredioId;
    }

    public void setPrediosSeleccionados(ArrayList<Object[]> prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    public void setSelectedPredioId(Long selectedPredioId) {
        this.selectedPredioId = selectedPredioId;
    }

    public String getModuleLayer() {
        return this.moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return this.moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public boolean isPredioFiscal() {
        return predioFiscal;
    }

    public void setPredioFiscal(boolean predioFiscal) {
        this.predioFiscal = predioFiscal;
    }

    public boolean isPredioBloqueo() {
        return predioBloqueo;
    }

    public void setPredioBloqueo(boolean predioBloqueo) {
        this.predioBloqueo = predioBloqueo;
    }

    public boolean isMigradoCobol() {
        return migradoCobol;
    }

    public void setMigradoCobol(boolean esMigradoCobol) {
        this.migradoCobol = esMigradoCobol;
    }

    public boolean isPredioOrigen() {
        return predioOrigen;
    }

    public void setPredioOrigen(boolean predioOrigen) {
        this.predioOrigen = predioOrigen;
    }

    public ArrayList<Object[]> getArbolFamiliarPredios() {
        return arbolFamiliarPredios;
    }

    public void setArbolFamiliarPredios(ArrayList<Object[]> arbolFamiliarPredios) {
        this.arbolFamiliarPredios = arbolFamiliarPredios;
    }

//--------------------- Metoths  -------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
    /**
     * @version 2.0
     */
    @SuppressWarnings("serial")
    public ConsultaPredioMB() {

        this.lazyModel = new LazyDataModel<Object[]>() {

            @Override
            public List<Object[]> load(int first, int pageSize, String sortField,
                SortOrder sortOrder,
                Map<String, String> filters) {
                List<Object[]> answer = new ArrayList<Object[]>();

                populatePrediosLazyly(answer, first, pageSize);

                return answer;
            }
        };
    }
//--------------------------------------------------------------------------------------------------

    @PostConstruct
    public void init() {
        LOGGER.debug("entra al init del ConsultaPredioMB");

        this.mustSearch = false;
        this.predioFiscal = false;
        this.usuario = MenuMB.getMenu().getUsuarioDto();
        this.usuarioDelegado = MenuMB.getMenu().isUsuarioDelegado();
        this.usuarioHabilitado = MenuMB.getMenu().isUsuarioHabilitado();
        this.municipiosDelegados = this.getGeneralesService().obtenerMunicipiosDelegados();
        this.municipiosHabilitadas = this.getGeneralesService().obtenerMunicipiosHabilitados();
        this.arbolFamiliarPredios = new ArrayList<Object[]>();

        this.seUsanDeptosFijos = true;
        if (this.usuarioDelegado) {
            this.initDelegados();
        }

        List<UsoConstruccion> ucList;

        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.setRenderResultPanel(false);

        this.territorialesItemList = new ArrayList<SelectItem>();
        this.departamentosItemList = new ArrayList<SelectItem>();
        this.municipiosItemList = new ArrayList<SelectItem>();

        LOGGER.debug("hace el init de la lista de Territoriales");
        if (this.territorialesItemList == null || this.territorialesItemList.isEmpty()) {
            // this.territorialesItemList.add(new SelectItem("", "Seleccione"));
            List<EstructuraOrganizacional> eoList;

            if (this.usuarioDelegado || this.usuarioHabilitado) {
                eoList = new ArrayList<EstructuraOrganizacional>();
                eoList.add(this.getGeneralesService().
                    buscarEstructuraOrganizacionalPorCodigo(this.usuario.getCodigoTerritorial()));
            } else {
                eoList = this.getGeneralesService().getCacheTerritoriales();
            }

            this.setTerritorialesItemList(eoList);
        }

        this.destinosPredioItemList = new ArrayList<SelectItem>();
        List<Dominio> destinosPredioDominio = this.getGeneralesService()
            .getCacheDominioPorNombre(EDominio.PREDIO_DESTINO);
        for (Dominio d : destinosPredioDominio) {
            // D: OJO el valor de un dato que corresponda a un dominio es el
            // código de ese dominio, no el id
            this.destinosPredioItemList.add(new SelectItem(d.getCodigo(), d
                .getCodigo() + "-" + d.getValor()));
        }

        this.usosConstruccionItemList = new ArrayList<SelectItem>();
        ucList = this.getConservacionService().obtenerUsosConstruccion();
        this.setUsosConstruccionItemList(ucList);

        //this.applicationClientName = "sigc-web-public";
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.seUsaTerritorial = true;
        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.CONSERVACION.getCodigo();
        this.datosConsultaPredio.setTerritorialId(this.usuario.getCodigoTerritorial());
        this.onChangeTerritorial();
        this.predioBloqueo = true;
    }

    /**
     * Inicializa la informacion necesaria cuando se trata de un usuario delegado.
     */
    public void initDelegados() {
        this.seUsanDeptosFijos = true;
    }

//-------------------------------------------------------------------------------------------------
    public FiltroDatosConsultaPredio getDatosConsultaPredio() {
        return this.datosConsultaPredio;
    }

    public void setDatosConsultaPredio(FiltroDatosConsultaPredio datosC) {
        this.datosConsultaPredio = datosC;
    }

    public void setTerritorialesItemList(List<EstructuraOrganizacional> items) {
        LOGGER.debug("entra al que retorna la lista de T");

        for (EstructuraOrganizacional territorial : items) {
            this.territorialesItemList.add(new SelectItem(territorial
                .getCodigo(), territorial.getNombre()));
        }
    }

    // ------------------
    public ArrayList<SelectItem> getTerritorialesItemList() {
        return this.territorialesItemList;
    }

//-------------------------------------------------------------------------------------------------
    /**
     * @return the departamentosItemList
     */
    public ArrayList<SelectItem> getDepartamentosItemList() {

        List<Departamento> deptosList;

        // TODO se podría llevar un flag que indique si la territorial cambió
        // para evitar hacer esta consulta
        // siempre. Lo mismo aplica para municipios
        if (this.datosConsultaPredio.getTerritorialId() != null &&
            this.datosConsultaPredio.getTerritorialId().compareTo("") != 0) {
            deptosList = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.datosConsultaPredio.getTerritorialId());
        } else {
            if (!this.seUsaTerritorial) {
                deptosList = this.getGeneralesService().getCacheDepartamentosPorPais(
                    Constantes.COLOMBIA);
            } else {
                deptosList = new ArrayList<Departamento>();
                this.datosConsultaPredio.setDepartamentoId("");
                this.setMunicipiosItemList(new ArrayList<Municipio>());
            }
        }
        if (!this.seUsanDeptosFijos) {
            this.setDepartamentosItemList(deptosList);
        }

        return this.departamentosItemList;
    }

    // ----------------
    /**
     * @param departamentosItemList the departamentosItemList to set
     */
    public void setDepartamentosItemList(List<Departamento> departamentosList) {
        LOGGER.debug("entra al que retorna la lista de D");

        // D: para evitar que queden los elementos de búsquedas anteriores
        if (!this.departamentosItemList.isEmpty()) {
            this.departamentosItemList.clear();
        }
        
        datosConsultaPredio.setDepartamentoId(null);
        
        for (Departamento departamento : departamentosList) {
            this.departamentosItemList.add(new SelectItem(departamento.getCodigo(),
                departamento.getNombre()));
        }

        //D: para hacer que no quede seleccionado un municipio cuando se cambia de departamento y
        //   no se selecciona un nuevo municipio
        //this.datosConsultaPredio.setMunicipioId(null);
    }

    // --------------------------------------------------------------------------------------------------
    /**
     * @return the municipiosItemList
     */
    public ArrayList<SelectItem> getMunicipiosItemList() {

        List<Municipio> municipiosList = new LinkedList<Municipio>();
        List<Municipio> municipiosTerritorial;

        if (this.datosConsultaPredio.getDepartamentoId() != null &&
             this.datosConsultaPredio.getDepartamentoId().compareTo("") != 0) {

            municipiosList = new ArrayList<Municipio>();
            String codigoEstructura;
            if (this.datosConsultaPredio.getUoc() == null || this.datosConsultaPredio.getUoc().
                isEmpty()) {
                codigoEstructura = this.datosConsultaPredio.getTerritorialId();
            } else {
                codigoEstructura = this.datosConsultaPredio.getUoc();
            }
            
             municipiosTerritorial = this.getGeneralesService().
                    getCacheMunicipiosPorCodigoEstructuraOrganizacional(codigoEstructura);
             
            /*if (!this.UOCItemList.isEmpty() && (this.datosConsultaPredio.getUoc() != null
                    && !this.datosConsultaPredio.getUoc().isEmpty())) {
                municipiosTerritorial = this.getGeneralesService().
                    getCacheMunicipiosPorDepartamento(this.datosConsultaPredio.getDepartamentoId());
            } else {
                municipiosTerritorial = this.getGeneralesService().
                    getCacheMunicipiosPorCodigoEstructuraOrganizacional(codigoEstructura);
            }*/
             
            for (Municipio municipio : municipiosTerritorial) {
                if (municipio.getDepartamento().getCodigo().equals(this.datosConsultaPredio.
                    getDepartamentoId())) {
                    municipiosList.add(municipio);
                }
            }

        } else {
            municipiosList = new ArrayList<Municipio>();
        }
        this.setMunicipiosItemList(municipiosList);

        return this.municipiosItemList;

    }

    public void limpiarDptosMunicipios() {
        this.datosConsultaPredio.setDepartamentoId("");
        this.datosConsultaPredio.setMunicipioId("");
    }

    // -----------------
    /**
     * @param municipiosItemList the municipiosItemList to set
     */
    public void setMunicipiosItemList(List<Municipio> municipiosList) {

        // D: para evitar que queden los elementos de búsquedas anteriores
        if (!this.municipiosItemList.isEmpty()) {
            this.municipiosItemList.clear();
        }

        for (Municipio municipio : municipiosList) {

            MunicipioComplemento mc = this.getGeneralesService().
                obtenerMunicipioComplementoPorCodigo(municipio.getCodigo());
            if (mc == null) {
                continue;
            }
            SelectItem si = new SelectItem(municipio.getCodigo(),
                municipio.getNombre());
            if (mc.getConInformacionGdb().equals(ESiNo.NO.getCodigo())) {
                si.setDisabled(true);
            }

            this.municipiosItemList.add(si);
        }

    }

//--------------------------------------------------------------------------------------------------
    /**
     * @return the renderResultPanel
     */
    public boolean isRenderResultPanel() {
        return renderResultPanel;
    }

    /**
     * @param renderResultPanel the renderResultPanel to set
     */
    public void setRenderResultPanel(boolean renderResultPanel) {
        this.renderResultPanel = renderResultPanel;
    }

// --------------------------------------------------------------------------------------------------
//TODO OJO en este momento el número predial ya debe venir con el formato que debe tener
//--------------------------------------------------------------------------------------------------
    /**
     * Método que es el action que se ejecuta al dar click en buscar en consultaPredio.xhtml
     */
    public void buscarPredios() {
        LOGGER.info("ConsultaPredioMB#buscarPredios() action called");

        this.mustSearch = true;
        String direccionNormalizada;
        int numeroPrediosResultado;

        //D: si el primer segmento del número predial tiene algún valor, significa que se debe tener 
        //   en cuenta el número predial como criterio de búsqueda
        if (this.datosConsultaPredio.getNumeroPredialS1() != null &&
            this.datosConsultaPredio.getNumeroPredialS1().compareTo("") != 0) {
            this.datosConsultaPredio.assembleNumeroPredialFromSegments();
        }

        //D: se normaliza la dirección dada por el usuario porque se compara con el campo que la
        //   guarda así en la tabla
        if (this.datosConsultaPredio.getDireccionPredio() != null &&
            this.datosConsultaPredio.getDireccionPredio().compareTo("") != 0) {
            direccionNormalizada = this.getGeneralesService().obtenerDireccionNormalizada(
                this.datosConsultaPredio.getDireccionPredio());
            this.datosConsultaPredio.setDireccionPredio(direccionNormalizada);
        }

        //D: hay que hacer esto para que funcione bien la paginación
        //N: NO BORRAR HASTA QUE QUEDE DEFINITIVO EL USO DEL SP
        //D: este usa el método normal.
        // OJO: si queda este, arreglar lo del nombre del propietario porque está como un solo campo en el query nativo
        //this.lazyModel.setRowCount(
        //        this.getConservacionService().contarResultadosBusquedaPredios(this.datosConsultaPredio));
        //felipe.cadena::09-12-2016::Las consultas de delegadas siempre deben tener especificado este dato
        if (this.usuarioDelegado) {
            this.datosConsultaPredio.setTerritorialId(this.usuario.getCodigoTerritorial());
        }

        //D: este usa el procedimiento almacenado
        numeroPrediosResultado =
            this.getConservacionService().contarPredios(this.datosConsultaPredio);
//TODO :: sacar esto de un properties ??? :: pedro.garcia :: 08-05-2012 ::
        if (numeroPrediosResultado > 1000) {
            this.addMensajeError("La búsqueda retorna un número muy grande de resultados. " +
                "Por favor afine los criterios de búsqueda");

            //D: si no se ponía esto aquí, de todas formas hacía la búsqueda
            this.mustSearch = false;
            return;
        }

        //D: parece que no se necesita hacer esto aquí porque la tabla, al tener un lazydatamodel,
        //   se refresca sola, y por lo tanto llama al método populatePrediosLazyly
        //this.prediosSearchResultList = (ArrayList<Object[]>)this.remoteConservacion.searchPredios(this.datosConsultaPredio);
        //this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.renderResultPanel = true;

    }
//--------------------------------------------------------------------------------------------------

    private void populatePrediosLazyly(List<Object[]> answer, int first, int pageSize) {

        List<Object[]> rows;
        int size, count;

        if (this.mustSearch) {
            //N: NO BORRAR HASTA QUE QUEDE DEFINITIVO EL USO DEL SP
            //D: este usa el método normal
            //rows = this.getConservacionService().searchPredios(this.datosConsultaPredio, first, pageSize);

            //D: este usa el procedimiento
            rows = this.getConservacionService().buscarPredios(this.datosConsultaPredio,
                first, pageSize);

            if (rows != null) {
                size = rows.size();

                count = this.getConservacionService().contarPredios(this.datosConsultaPredio);
                this.lazyModel.setRowCount(count);

                for (int i = 0; i < size; i++) {
                    //se descartan los predios delegados para no delegados
                    if (!this.usuarioDelegado) {
                        boolean delegado = false;
                        boolean habilitado = false;
                        for (Municipio mun : this.municipiosDelegados) {
                            if (mun.getCodigo().equals(rows.get(i)[4])) {
                                delegado = true;
                            }
                        }
                        for (Municipio mun : this.municipiosHabilitadas) {
                            if (mun.getCodigo().equals(rows.get(i)[4])) {
                                habilitado = true;
                            }
                        }
                        if (!delegado && !habilitado) {
                            answer.add(rows.get(i));
                        }
                    } else {
                        answer.add(rows.get(i));
                    }

                }

            }
        } else {
            this.lazyModel.setRowCount(0);
        }

    }
//--------------------------------------------------------------------------------------------------

    public void cleanAllMB() {
        this.cleanRemoveSessionMB(null);

        /*if (this.predioBloqueo) {
            this.ConsultarBloqueoPredioPreliminar();
        }*/

        //this.dialog.setVisible(false);
        /* javier.aponte comentado para que funcione los detalles del predio, por favor si lo
         * descomentan verificar en radicacion-gestionar solicitudes y en asignación en
         * detalleTramiteEnAsignacion
         * UtilidadesWeb.removerManagedBean("consultaDatosUbicacionPredio");
         * ConsultaDatosUbicacionPredioMB mbDatosUbicacion = (ConsultaDatosUbicacionPredioMB)
         * UtilidadesWeb.getManagedBean("consultaDatosUbicacionPredio"); mbDatosUbicacion.init();
         *
         * UtilidadesWeb.removerManagedBean("consultaPropietarios"); ConsultaPropietariosMB
         * mbPropietarios = (ConsultaPropietariosMB)
         * UtilidadesWeb.getManagedBean("consultaPropietarios"); mbPropietarios.init();
         *
         * UtilidadesWeb.removerManagedBean("consultaJustDerProp");
         * ConsultaJustificacionDerechoPropiedadMB mbDerechoPropiedad =
         * (ConsultaJustificacionDerechoPropiedadMB)
         * UtilidadesWeb.getManagedBean("consultaJustDerProp"); mbDerechoPropiedad.init();
         *
         * UtilidadesWeb.removerManagedBean("consultaDetalleAvaluo"); ConsultaDetalleAvaluoMB
         * mbDetalleAvaluo = (ConsultaDetalleAvaluoMB)
         * UtilidadesWeb.getManagedBean("consultaDetalleAvaluo"); mbDetalleAvaluo.init();
         *
         * UtilidadesWeb.removerManagedBean("consultaTramitesPredio"); ConsultaTramitesPredioMB
         * mbTramitesPredio = (ConsultaTramitesPredioMB)
         * UtilidadesWeb.getManagedBean("consultaTramitesPredio"); mbTramitesPredio.init();
         *
         * UtilidadesWeb.removerManagedBean("consultaFichaMatrizPredio");
         * ConsultaFichaMatrizPredioMB mbFichaMatrizPredio = (ConsultaFichaMatrizPredioMB)
         * UtilidadesWeb.getManagedBean("consultaFichaMatrizPredio"); mbFichaMatrizPredio.init();
         *
         * UtilidadesWeb.removerManagedBean("consultaRegistroFotograficoPredio");
         * ConsultaRegistroFotograficoPredioMB mbRegistroFotograficoPredio =
         * (ConsultaRegistroFotograficoPredioMB)
         * UtilidadesWeb.getManagedBean("consultaRegistroFotograficoPredio");
    	mbRegistroFotograficoPredio.init(); */
        //this.dialog.setVisible(true);

    }

    /**
     * Método para cerrar los managed bean de sesión de las consultas de detalles los predios.
     * También se remueven las referencias que de este mb tienen otros mb que son sus usuarios
     */
    /* public void cerrarSesionesMB() {
     *
     * //D: se remueven primero las referencias que de este mb tienen otros mb que son sus usuarios
     * for (IConsultaPredioWeb mb : this.listenersMB){ mb.removerReferencia(); } this.listenersMB =
     * new ArrayList<IConsultaPredioWeb>();
     *
     * UtilidadesWeb.removerManagedBean("consultaPredio");
     * UtilidadesWeb.removerManagedBean("consultaDatosUbicacionPredio");
     * UtilidadesWeb.removerManagedBean("consultaPropietarios");
     * UtilidadesWeb.removerManagedBean("consultaJustDerProp");
     * UtilidadesWeb.removerManagedBean("consultaDetalleAvaluo");
     * UtilidadesWeb.removerManagedBean("consultaTramitesPredio");
     * UtilidadesWeb.removerManagedBean("consultaFichaMatrizPredio");
     * UtilidadesWeb.removerManagedBean("consultaRegistroFotograficoPredio");
	} */
//--------------------------------------------------------------------------------------------------
    /**
     * Remueve de la sesión los mb asociados con cada uno de los tabs que se muestran en los
     * detalles de un predio. Se debe hacer para forzar el init de aquellos cada vez que se
     * seleccione un predio diferente
     *
     * @param event
     */
    public void cleanRemoveSessionMB(CloseEvent event) {
        UtilidadesWeb.removerManagedBean("consultaDatosUbicacionPredio");

        UtilidadesWeb.removerManagedBean("consultaPropietarios");

        UtilidadesWeb.removerManagedBean("consultaJustDerProp");

        UtilidadesWeb.removerManagedBean("consultaDetalleAvaluo");

        UtilidadesWeb.removerManagedBean("consultaTramitesPredio");

        UtilidadesWeb.removerManagedBean("consultaFichaMatrizPredio");

        UtilidadesWeb.removerManagedBean("consultaFichasEscaneadasPredio");

        UtilidadesWeb.removerManagedBean("consultaRegistroFotograficoPredio");

        UtilidadesWeb.removerManagedBean("consultaHistoricoAvaluoPredio");

        UtilidadesWeb.removerManagedBean("consultaOrigenPredio");
        
        UtilidadesWeb.removerManagedBean("consultaImagenPredio");
    }

    public LazyDataModel<Object[]> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Object[]> lazyModel) {
        this.lazyModel = lazyModel;
    }

//--------------------------------------------------------------------------------------------------
    /**
     * retorna la lista de predios seleccionados como una lista de objetos Predio (no como una lista
     * de arreglos de objetos, que es como lo devuelve la consulta)
     *
     * @documented pedro.garcia
     * @return
     */
    public List<Predio> getPrediosSeleccionados2() {
        List<Predio> predios = new ArrayList<Predio>();
        for (Object[] p : this.prediosSeleccionados) {
            Predio predio = new Predio();

            Departamento d = new Departamento();
            d.setNombre((String) p[0]);
            d.setCodigo((String) p[3]);
            predio.setDepartamento(d);

            Municipio m = new Municipio();
            m.setNombre((String) p[1]);
            m.setCodigo((String) p[4]);
            predio.setMunicipio(m);
            //pq big decimal???
            predio.setId(((BigDecimal) p[2]).longValue());
            //predio.setId((Long) (p[2]));
            predio.setNumeroPredial((String) p[14]);
            predio.setChip(p[35] != null ? (String) p[34] : "");
            predio.setNip(p[16] != null ? (String) p[16] : "");

            predio.setZonaUnidadOrganica(p[19] != null ? (String) p[19] : "");
            predio.setEstado((String) p[32]);
            predio.setZonaUnidadOrganica(p[19] != null ? (String) p[19] : "");

            predio.setConsecutivoCatastral(p[17] != null ? (BigDecimal) p[17] : new BigDecimal(0));
            predio.setTipoAvaluo((String) p[5]);
            predio.setTipoCatastro((String) p[29]);

            //condicion propiedad
            predio.setCondicionPropiedad(p[10] != null ? (String) p[10] : "");

            predios.add(predio);
        }

        for (Predio predio : predios) {

            if (predio.getTipoCatastro().equals(EPredioTipoCatastro.VIGENCIAS_VIEJAS.getCodigo())) {
                this.predioFiscal = true;
                return predios;
            }
        }
        this.predioFiscal = false;

        return predios;
    }
//--------------------------------------------------------------------------------------------------

    /**
     * @modified pedro.garcia cambio de firma del método: para admitir varargs. Se usa para saber si
     * el predio se adiciona o reemplaza al que esté en el arreglo (para los casos donde solo se
     * quiere un predio seleccionado)
     * @param replacePredio optional argument false es el valor que se considera por defecto; true
     * si se quiere que en lugar de adicionarlo a la lista remplace al que esté allí
     */
//TODO::pedro.garcia::cambiar a private cuando se haya puesto la búsqueda con el componente en todos los sitios::pedro.garcia
    public void adicionarPredio(boolean... replacePredio) {

        try {
            String numeroPredial = this.datosConsultaPredio.getNumeroPredial();
            FiltroDatosConsultaPredio filtro = new FiltroDatosConsultaPredio();
            filtro.setNumeroPredial(numeroPredial);
            List<Object[]> predio = this.getConservacionService().searchPredios(filtro, 0, 1);
            if (predio.size() > 0) {
                if (!estaSeleccionado(numeroPredial)) {
                    //D: si el predio debe reemplazar al que estaba (o, a los que estaban)
                    if (replacePredio != null && replacePredio.length > 0 &&
                        replacePredio[0] == true) {
                        this.prediosSeleccionados.clear();
                    }
                    this.prediosSeleccionados.add(predio.get(0));
                } else {
                    this.addMensajeError("El predio " + numeroPredial +
                        " ya se encuentra seleccionado");
                }
            } else {
                this.addMensajeError("No se encontró el predio " + numeroPredial);
            }

            this.datosConsultaPredio.setNumeroPredial(null);

        } catch (Exception e) {
            LOGGER.error("error adicionando/reemplazando predio: " + e.getMessage());
        }
    }
//-------------------------------------------------

    /**
     * estos dos métodos se crearon para solucionar el problema de no poder pasar parámetros
     * directamente desde el jsf de consulta de predios
     *
     * @author pedro.garcia
     */
    public void adicionarPredio1() {
        this.adicionarPredio();
    }

    public void adicionarPredio2(SelectEvent event) {
        this.adicionarPredioListener(event, false);
    }

    public void adicionarPredio3() {
        this.adicionarPredio(true);
    }

    public void adicionarPredio4(SelectEvent event) {
        this.adicionarPredioListener(event, true);
    }

    public void adicionarPredioListener(SelectEvent event) {
        this.adicionarPredioListener(event, true);
    }

    //
    public void seleccionPredioDeResultadosBusquedaListener(SelectEvent event) {
        switch (this.listenerMethodIdentifier) {

            //D: el predio seleccionado debe reemplazar a lo que ya están en la lista para que solo
            //   haya uno
            case (1): {
                this.adicionarPredioListener(event, true);
                break;
            }

            //D: se pueden seleccionar varios predios que se van sumando a la lista
            case (2): {
                this.adicionarPredioListener(event, false);
                break;
            }
            default: {
                break;
            }
        }
    }

    //-------------------------------------------------
    /**
     * @modified pedro.garcia cambio de firma del método: para admitir varargs. Se usa para saber si
     * el predio se adiciona o reemplaza al que esté en el arreglo (para los casos donde solo se
     * quiere un predio seleccionado)
     * @param replacePredio optional argument false es el valor que se considera por defecto; true
     * si se quiere que en lugar de adicionarlo a la lista remplace al que esté allí
     */
    //TODO::pedro.garcia::cambiar a private cuando se haya puesto la búsqueda con el componente en todos los sitios::pedro.garcia
    //TODO::pedro.garcia::revisar, porque fue necesario adicionar el metodo previo, ya que este no es valido para JSF como un listener, y ya se usaba::fredy.wilches
    private void adicionarPredioListener(SelectEvent event, boolean... replacePredio) {
        this.predioBloqueo = true;
        try {
            this.numeroPredial = ((Object[]) event.getObject())[14].toString();
            FiltroDatosConsultaPredio filtro = new FiltroDatosConsultaPredio();
            filtro.setNumeroPredial(numeroPredial);
            List<Object[]> predio = this.getConservacionService().searchPredios(filtro, 0, 1);
            if (predio.size() > 0) {
                if (!estaSeleccionado(numeroPredial)) {
                    //D: si el predio debe reemplazar al que estaba (o, a los que estaban)
                    if (replacePredio != null && replacePredio.length > 0 &&
                        replacePredio[0] == true) {
                        this.prediosSeleccionados.clear();
                    }
                    this.prediosSeleccionados.add(predio.get(0));
                    BigDecimal id = (BigDecimal) ((Object[]) predio.get(0))[2];

                    List<Tramite> tramites = (ArrayList<Tramite>) this.getTramiteService().
                        buscarTramitesPendientesDePredio(id.longValue());
                    if (tramites != null && tramites.size() > 0) {
                        this.addMensajeWarn("El predio " + numeroPredial +
                            " tiene trámites en ejecución");
                    }
                } else {
                    this.addMensajeError("El predio " + numeroPredial +
                        " ya se encuentra seleccionado");
                }
            } else {
                this.addMensajeError("No se encontró el predio " + numeroPredial);
            }

            if (this.getConservacionService().busquedaPredioBloqueoByTipoBloqueo(numeroPredial,
                EPredioBloqueoTipoBloqueo.SUSPENDER.getCodigo()) != null) {

                this.predioBloqueo = false;
            } else {
                this.predioBloqueo = true;
            }

        } catch (Exception e) {
            LOGGER.error("error adicionando/reemplazando predio: " + e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------

    public boolean estaSeleccionado(String numeroPredial) {
        for (Object[] predio : this.prediosSeleccionados) {
            if (numeroPredial.equals((String) predio[14])) {
                return true;
            }
        }
        return false;
    }

    public void eliminarPredio() {
        BigDecimal predioId = (BigDecimal) this.selectedPredio2[2];
        for (Object[] o : this.prediosSeleccionados) {
            if (predioId.equals(o[2])) {
                this.prediosSeleccionados.remove(o);
                break;
            }
        }

        validarPrediosSeleccionadosEstadoBloqueo();

    }

    public void limpiarPredios() {
        prediosSeleccionados = new ArrayList<Object[]>();
    }

//--------------------------------------------------------------------------------------------------
    public void doNothing() {
        LOGGER.debug("doing nothing");
    }
//--------------------------------------------------------------------------------------------------

    /**
     * action del botón "limpiar" pone los combos de departamentos y municipios como al comienzo
     */
    public void resetForm() {

        //D: esto hace que, cuando se refresque la página y se vuelvan a armar los combos, queden
        //  como al inicio 
        this.datosConsultaPredio.setTerritorialId(null);

        //D: definir a mano valores null o vacíos para los otros campos
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
    }

    public void onChangeTerritorial() {
        FiltroDatosConsultaPredio consulta = this.getDatosConsultaPredio();

        this.datosConsultaPredio.setUoc(null);
        List<EstructuraOrganizacional> uocs = this.getGeneralesService().
            buscarUOCporCodigoTerritorial(this.datosConsultaPredio.getTerritorialId());
        this.UOCItemList = new ArrayList<SelectItem>();

        if (uocs != null && !uocs.isEmpty()) {

            for (EstructuraOrganizacional eo : uocs) {
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        }

        List<Departamento> d = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
            this.datosConsultaPredio.getTerritorialId());

        if (d != null && !d.isEmpty()) {
            this.setDepartamentosItemList(d);
            //consulta.setDepartamentoId(d.get(0).getCodigo());
            this.setDatosConsultaPredio(consulta);
            this.onChangeDepartamento();

        } else {
            for (EstructuraOrganizacional eo : uocs) {
                this.UOCItemList.add(new SelectItem(eo.getCodigo(), eo.getNombre()));
            }
        }
    }

    /**
     * Listener para el evento de cambio de UOC
     *
     * @author felipe.cadena
     */
    public void onChangeUOC() {
        FiltroDatosConsultaPredio consulta = this.getDatosConsultaPredio();
        List<Departamento> d;
        if (this.datosConsultaPredio.getUoc() != null && !this.datosConsultaPredio.getUoc().
            isEmpty()) {
            d = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.datosConsultaPredio.getUoc());
        } else {
            d = this.getGeneralesService().getCacheDepartamentosPorTerritorial(
                this.datosConsultaPredio.getTerritorialId());

        }

        if (d != null && !d.isEmpty()) {
            this.setDepartamentosItemList(d);
            //consulta.setDepartamentoId(d.get(0).getCodigo());
            this.setDatosConsultaPredio(consulta);
            this.onChangeDepartamento();
        }

    }

    public void onChangeDepartamento() {
        FiltroDatosConsultaPredio consultaD = this.getDatosConsultaPredio();
        if (consultaD.getDepartamentoId() != null) {
            //List<Municipio> municipiosTerritorial = this.getGeneralesService().getCacheMunicipiosPorDepartamento(consultaD.getDepartamentoId());
            List<Municipio> listM = new ArrayList<Municipio>();
            List<Municipio> municipiosTerritorial = this.getGeneralesService().
                getCacheMunicipiosPorCodigoEstructuraOrganizacional(consultaD.getTerritorialId());

            for (Municipio municipio : municipiosTerritorial) {
                if (municipio.getDepartamento().getCodigo().equals(consultaD.getDepartamentoId())) {
                    listM.add(municipio);
                }
            }

            this.setMunicipiosItemList(listM);

            consultaD.setNumeroPredialS1(consultaD.getDepartamentoId());

            this.setDatosConsultaPredio(consultaD);
        } else {
            consultaD.setMunicipioId(null);
            consultaD.setNumeroPredialS1(null);
            consultaD.setNumeroPredialS2(null);
        }
    }

    /**
     * Listener para el evento de cambio de municipio
     *
     * @author felipe.cadena
     */
    public void onChangeMunicipio() {
        FiltroDatosConsultaPredio consultaD = this.getDatosConsultaPredio();
        consultaD.setNumeroPredialS2(consultaD.getMunicipioId().substring(2));
    }

    /**
     * Método para limpiar los campos de busqueda en la selección de predios, además reinicia las
     * listas de selecciones anteriores. Se utiliza en el boton 'Limpiar' del panel de busqueda.
     *
     * @author felipe.cadena
     */
    public void limpiarBusquedasPrevias() {
        this.datosConsultaPredio = new FiltroDatosConsultaPredio();
        this.datosConsultaPredio.setTerritorialId(this.usuario.getCodigoTerritorial());
        this.onChangeTerritorial();

        this.prediosSeleccionados = new ArrayList<Object[]>();
        this.lazyModel.setRowCount(0);

        this.mustSearch = false;
    }

    /**
     * Método para limpiar los campos de la busqueda avanzada en la selección de predios, además
     * reinicia las listas de selecciones anteriores. Se utiliza en el boton 'Limpiar' del panel de
     * busqueda avanzada.
     *
     * @author felipe.cadena
     */
    public void limpiarBusquedasAvanzadasPrevias() {
        this.datosConsultaPredio.setUsoConstruccionId(null);
        this.datosConsultaPredio.setDestinoPredioId(null);
        this.datosConsultaPredio.setAreaConstruidaMax(0);
        this.datosConsultaPredio.setAreaConstruidaMin(0);
        this.datosConsultaPredio.setAreaTerrenoMax(0);
        this.datosConsultaPredio.setAreaTerrenoMin(0);
        this.datosConsultaPredio.setNip(null);
        this.datosConsultaPredio.setNumeroRegistro(null);

        this.prediosSeleccionados = new ArrayList<Object[]>();
        this.lazyModel.setRowCount(0);

        this.mustSearch = false;
    }

    //--------------------------------------------------------------------------------------------------
    /**
     * Método que busca los predios asociados a un trámite para ver sus detalles.
     *
     * OJO: Se asume que el trámite es de segunda englobe ya que este método se usa cuando se tiene
     * un trámite de este tipo y se quiere listar los trámites asociados para hacer links para ver
     * sus detalles
     *
     * @author pedro.garcia
     */
    public void consultarPrediosDelTramiteParaDetalles() {

        if (this.idTramiteDePredios != null) {
            this.prediosDeTramiteEnglobe = (ArrayList<TramiteDetallePredio>) this.
                getTramiteService().
                buscarPredioEnglobesPorTramite(this.idTramiteDePredios);
            if (this.prediosDeTramiteEnglobe == null) {
                LOGGER.error("La consulta de predios asociados al trámite " +
                    this.idTramiteDePredios.toString() + " no dió resultados, lo cual es un error");
            }
        } else {
            LOGGER.error(
                "No se tiene un valor para el id del trámite cuyos predios se quiere consultar");
        }
    }

    /**
     * Método que valida si en la lista de predios seleccionados se encuentra alguno en estado
     * bloqueado.
     *
     * @author dumar.penuela
     */
    public void validarPrediosSeleccionadosEstadoBloqueo() {
        predioBloqueo = false;
    }

    /**
     * Método que asegura la presentación de los datos del predio bloqueado al hacer clic sobre
     * el tipo de bloqueo.
     *
     * @author hector.arias
     */
    public void ConsultarBloqueoPredioPreliminar() {
        ConsultaDatosUbicacionPredioMB mbDatosUbicacion = (ConsultaDatosUbicacionPredioMB) UtilidadesWeb.getManagedBean("consultaDatosUbicacionPredio");
        mbDatosUbicacion.init();

        ConsultaBloqueoPredioMB consultaBloqueoPredioMB = (ConsultaBloqueoPredioMB) UtilidadesWeb.getManagedBean("consultaBloqueoPredio");
        consultaBloqueoPredioMB.init();
    }
    
    //end of class    
}
