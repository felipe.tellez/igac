package co.gov.igac.snc.web.mb.avaluos.ingresoSolicitud;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.persistence.entity.avaluos.AvaluoPredio;
import co.gov.igac.snc.persistence.entity.tramite.Solicitante;
import co.gov.igac.snc.persistence.entity.tramite.SolicitanteSolicitud;
import co.gov.igac.snc.persistence.entity.tramite.Solicitud;
import co.gov.igac.snc.persistence.entity.tramite.SolicitudPredio;
import co.gov.igac.snc.persistence.util.EPersonaTipoPersona;
import co.gov.igac.snc.persistence.util.ESiNo;
import co.gov.igac.snc.persistence.util.ESolicitanteSolicitudRelac;
import co.gov.igac.snc.web.util.SNCManagedBean;

@Component("adminInfoContactoPredios")
@Scope("session")
public class AdministracionInfoContactoDePrediosMB extends SNCManagedBean implements Serializable {

    private static final long serialVersionUID = 3192560069126536848L;

    private static final Logger LOGGER = LoggerFactory
        .getLogger(AdministracionInfoContactoDePrediosMB.class);

    /**
     * Usuario activo en el sistema
     */
    private UsuarioDTO usuario;

    /**
     * Contiene la información del contacto que se está creando
     */
    private SolicitanteSolicitud contactoSeleccionado;

    /**
     * Contactos asociados a la solicitud y al solicitante
     */
    private List<SolicitanteSolicitud> contactosSeleccionados;

    /**
     * Lsita de predios seleccionados a los cuales se les quiere adminsitrar los contactos
     */
    private List<SolicitudPredio> prediosSeleccionados;

    /**
     * Lista de predios recibidos en AdministracionPrediosAvaluosMB
     */
    private SolicitudPredio[] arrayPrediosSeleccionados;

    /**
     * Guarda la solicitud seleccionada que está asociada a los predios y a los solicitantes y
     * contactos
     */
    private Solicitud solicitudSeleccionada;

    /**
     * Guarda un solicitante de manera temporal para usar al momento de edicion y guardado del
     * contacto
     */
    private Solicitante solicitanteTemporal;

    /**
     * Solicitante de la solicitud
     */
    private SolicitanteSolicitud solicitante;

    /**
     * Se activa para decir que se está editando un contacto, si está en false es porque se está
     * creando uno nuevo
     */
    private boolean banderaEsEdicion;

    // ---------------------Metodos GET y SET----------------------- //
    public List<SolicitanteSolicitud> getContactosSeleccionados() {
        return contactosSeleccionados;
    }

    public void setContactosSeleccionados(List<SolicitanteSolicitud> contactosSeleccionados) {
        this.contactosSeleccionados = contactosSeleccionados;
    }

    public List<SolicitudPredio> getPrediosSeleccionados() {
        return prediosSeleccionados;
    }

    public void setPrediosSeleccionados(List<SolicitudPredio> prediosSeleccionados) {
        this.prediosSeleccionados = prediosSeleccionados;
    }

    public SolicitanteSolicitud getContactoSeleccionado() {
        return contactoSeleccionado;
    }

    public void setContactoSeleccionado(SolicitanteSolicitud contactoSeleccionado) {
        this.contactoSeleccionado = contactoSeleccionado;
    }

    public SolicitanteSolicitud getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(SolicitanteSolicitud solicitante) {
        this.solicitante = solicitante;
    }

    public SolicitudPredio[] getArrayPrediosSeleccionados() {
        return arrayPrediosSeleccionados;
    }

    public void setArrayPrediosSeleccionados(SolicitudPredio[] arrayPrediosSeleccionados) {
        this.arrayPrediosSeleccionados = arrayPrediosSeleccionados;
    }

    // ----------------------------Metodos-------------------------- //
    @PostConstruct
    public void init() {

        LOGGER.debug("AdministracionInfoContactoDePrediosMB#init");
        this.contactoSeleccionado = new SolicitanteSolicitud();

    }

    // ------------------------------------------------------------------------------------
    /**
     * Inicializa las variables asociadas para el manejo de los contactos de una solicitud
     *
     * @author christian.rodriguez
     */
    /*
     * @modified rodrigo.hernandez - Eliminacion de codigo debido a integracion con
     * DiligenciamientoSolicitudMB
     */
    public Solicitud getSolicitudSeleccionada() {
        return this.solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(Solicitud solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public Solicitante getSolicitanteTemporal() {
        return this.solicitanteTemporal;
    }

    public void setSolicitanteTemporal(Solicitante solicitanteTemporal) {
        this.solicitanteTemporal = solicitanteTemporal;
    }

    public boolean isBanderaEsEdicion() {
        return this.banderaEsEdicion;
    }

    public void setBanderaEsEdicion(boolean banderaEsEdicion) {
        this.banderaEsEdicion = banderaEsEdicion;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Elimina el contacto de la tabla de solicitante solicitud en la bd, si el contacto esta
     * asociad a algun predio no puede ser eliminado
     *
     * @author christian.rodriguez
     */
    public void eliminarContacto() {

        if (this.contactoSeleccionado != null) {

            boolean contactoTienePrediosAsociados = false;

            for (SolicitudPredio predio : this.prediosSeleccionados) {

                SolicitanteSolicitud contacto = predio.getContacto();

                if (contacto != null &&
                     contacto.getId().compareTo(this.contactoSeleccionado.getId()) == 0) {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.addCallbackParam("error", "error");

                    this.addMensajeError(
                        "No se puede Eliminar el contacto seleccionado porque tiene " +
                         "información relacionada de predios, intente nuevamente");

                    contactoTienePrediosAsociados = true;
                    break;
                }
            }

            if (!contactoTienePrediosAsociados) {
                this.getTramiteService().eliminarSolicitanteSolicitud(this.contactoSeleccionado);
            }

        }

        this.cargarContactosAsociadosASolicitante();
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que ejecuta la consulta para traer los contactos asociados a una solicitud y a un
     * solicitante
     *
     * @author christian.rodriguez
     * @modified rodrigo.hernandez - se envia el id de la solicitudSeleccionada para buscar
     * contactos
     */
    public void cargarContactosAsociadosASolicitante() {

        this.contactosSeleccionados = this.getTramiteService()
            .buscarSolicitantesSolicitudBySolicitudIdRelacion(
                this.solicitudSeleccionada.getId(),
                ESolicitanteSolicitudRelac.CONTACTO.toString());

        this.contactoSeleccionado = new SolicitanteSolicitud();
    }

    // ------------------------------------------------------------------------------------
    /**
     * Crea un solicitante de tipo CONTACTO, si el solicitante ya existe en al tabla de solicitantes
     * no lo modifica, si no, lo crea. Tambien se crea un registro en la tabla de solicitante
     * solicitud por cada contacto creado
     *
     * @author christian.rodriguez
     *
     */
    public void guardarContacto() {

        if (this.contactoSeleccionado != null) {

            if (this.contactoSeleccionado.getSolicitante() == null) {
                this.solicitanteTemporal = this.crearSolicitante(this.contactoSeleccionado);
            } else {
                this.solicitanteTemporal = this.modificarSolicitante(
                    this.contactoSeleccionado.getSolicitante(), contactoSeleccionado);
            }

            this.solicitanteTemporal = this.getTramiteService().guardarActualizarSolicitante(
                this.solicitanteTemporal);

            this.contactoSeleccionado.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
            this.contactoSeleccionado.setUsuarioLog(this.usuario.getLogin());
            this.contactoSeleccionado.setFechaLog(new Date());

            this.contactoSeleccionado.setNotificacionEmail(ESiNo.NO.getCodigo());
            this.contactoSeleccionado.setRelacion(ESolicitanteSolicitudRelac.CONTACTO.toString());
            this.contactoSeleccionado.setSolicitud(this.solicitudSeleccionada);
            this.contactoSeleccionado.setSolicitante(this.solicitanteTemporal);
            this.contactoSeleccionado.setUsuarioLog(this.usuario.getLogin());
            this.contactoSeleccionado.setFechaLog(new Date());

            if (this.banderaEsEdicion ||
                 !this.contactoExisteEnLista(this.contactosSeleccionados,
                    this.contactoSeleccionado)) {

                this.contactoSeleccionado = this.getTramiteService()
                    .guardarActualizarSolicitanteSolicitud(this.contactoSeleccionado);

                this.cargarContactosAsociadosASolicitante();

            } else {

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("error", "error");

                this.addMensajeError(
                    "Ya existe el contacto. Si desea editarlo use el botón de editar");
            }

        }

    }

    // ------------------------------------------------------------------------------------
    /**
     * Determina si un contacto existe en una lsita de contactos
     *
     * @author christian.rodriguez
     * @param contactos lista de contactos en la que se quiere verificar
     * @param contacto contacto que se desea bucar
     * @return verdadero si existe, falso si no
     */
    private boolean contactoExisteEnLista(List<SolicitanteSolicitud> contactos,
        SolicitanteSolicitud contacto) {
        if (contactos != null) {
            for (SolicitanteSolicitud contactoTemp : contactos) {

                if (contactoTemp.getTipoIdentificacion().equals(contacto.getTipoIdentificacion())) {
                    if (contactoTemp.getNumeroIdentificacion().equals(
                        contacto.getNumeroIdentificacion())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Crea un objeto de tipo solicitante con los datos de un objeto de tipo solicitanteSolicitud
     *
     * @author christian.rodriguez
     * @param contactoSeleccionado solicitantesolicitud con los datos
     * @return solicitante con los datos del solicitantesolicitud
     */
    private Solicitante crearSolicitante(SolicitanteSolicitud contactoSeleccionado) {

        Solicitante solicitante = new Solicitante();

        solicitante.setTipoIdentificacion(contactoSeleccionado.getTipoIdentificacion());
        solicitante.setNumeroIdentificacion(contactoSeleccionado.getNumeroIdentificacion());
        solicitante.setPrimerApellido(contactoSeleccionado.getPrimerApellido());
        solicitante.setSegundoApellido(contactoSeleccionado.getSegundoApellido());
        solicitante.setPrimerNombre(contactoSeleccionado.getPrimerNombre());
        solicitante.setSegundoApellido(contactoSeleccionado.getSegundoApellido());
        solicitante.setCorreoElectronico(contactoSeleccionado.getCorreoElectronico());
        solicitante.setTelefonoPrincipal(contactoSeleccionado.getTelefonoPrincipal());
        solicitante.setTelefonoPrincipalExt(contactoSeleccionado.getTelefonoPrincipalExt());
        solicitante.setTelefonoCelular(contactoSeleccionado.getTelefonoCelular());

        solicitante.setTipoPersona(EPersonaTipoPersona.NATURAL.getCodigo());
        solicitante.setUsuarioLog(this.usuario.getLogin());
        solicitante.setFechaLog(new Date());

        return solicitante;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Copia los datos de un contacto a un solicitante
     *
     * @author christian.rodriguez
     * @param solicitante {@link Solicitante} a modificar
     * @param contactoSeleccionado {@link SolicitanteSolicitud} contacto del cual copiar los datos
     * @return solicitante con los datos del solicitantesolicitud
     */
    private Solicitante modificarSolicitante(Solicitante solicitante,
        SolicitanteSolicitud contactoSeleccionado) {

        solicitante.setTipoIdentificacion(contactoSeleccionado.getTipoIdentificacion());
        solicitante.setNumeroIdentificacion(contactoSeleccionado.getNumeroIdentificacion());
        solicitante.setPrimerApellido(contactoSeleccionado.getPrimerApellido());
        solicitante.setSegundoApellido(contactoSeleccionado.getSegundoApellido());
        solicitante.setPrimerNombre(contactoSeleccionado.getPrimerNombre());
        solicitante.setSegundoApellido(contactoSeleccionado.getSegundoApellido());
        solicitante.setCorreoElectronico(contactoSeleccionado.getCorreoElectronico());
        solicitante.setTelefonoPrincipal(contactoSeleccionado.getTelefonoPrincipal());
        solicitante.setTelefonoPrincipalExt(contactoSeleccionado.getTelefonoPrincipalExt());
        solicitante.setTelefonoCelular(contactoSeleccionado.getTelefonoCelular());

        return solicitante;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Buscar un solicitante usando el tipo de documento y el número de documento seleccionado
     *
     * @author christian.rodriguez
     */
    public void buscarYCargarContacto() {

        if (this.contactoSeleccionado != null) {

            if (this.contactoSeleccionadoExiste()) {
                this.cargarDatosSolicitanteSolicitudSeleccionado(this.solicitanteTemporal);
            }
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método que revisa si el contacto seleccionado existe como solicitante en la base de datos
     *
     * @author christian.rodriguez
     * @return true si el contacto existe, false si no
     */
    private boolean contactoSeleccionadoExiste() {
        String numeroIdenfiticacion = this.contactoSeleccionado.getNumeroIdentificacion();
        String tipoIdenfiticacion = this.contactoSeleccionado.getTipoIdentificacion();

        this.solicitanteTemporal = this.getTramiteService()
            .buscarSolicitantePorTipoYNumeroDeDocumento(numeroIdenfiticacion,
                tipoIdenfiticacion);

        if (this.solicitanteTemporal != null && this.solicitanteTemporal.getId() != null) {
            return true;
        } else {
            return false;
        }
    }

    // ------------------------------------------------------------------------------------
    /**
     * Carga el solicitante solicitud con lso datos de un solicitante
     *
     * @author christian.rodriguez
     * @param contactoTemporal solicitante con los datos del contacto
     */
    private void cargarDatosSolicitanteSolicitudSeleccionado(Solicitante contactoTemporal) {

        this.contactoSeleccionado.setTipoIdentificacion(contactoTemporal.getTipoIdentificacion());
        this.contactoSeleccionado.setNumeroIdentificacion(contactoTemporal
            .getNumeroIdentificacion());
        this.contactoSeleccionado.setPrimerApellido(contactoTemporal.getPrimerApellido());
        this.contactoSeleccionado.setSegundoApellido(contactoTemporal.getSegundoApellido());
        this.contactoSeleccionado.setPrimerNombre(contactoTemporal.getPrimerNombre());
        this.contactoSeleccionado.setSegundoApellido(contactoTemporal.getSegundoApellido());
        this.contactoSeleccionado.setCorreoElectronico(contactoTemporal.getCorreoElectronico());
        this.contactoSeleccionado.setTelefonoPrincipal(contactoTemporal.getTelefonoPrincipal());
        this.contactoSeleccionado.setTelefonoPrincipalExt(contactoTemporal
            .getTelefonoPrincipalExt());
        this.contactoSeleccionado.setTelefonoCelular(contactoTemporal.getTelefonoCelular());
        this.contactoSeleccionado.setSolicitante(contactoTemporal);

    }

    // ------------------------------------------------------------------------------------
    /**
     * Reinicializa la variable de contacto seleccionado
     *
     * @author christian.rodriguez
     */
    public void nuevoContacto() {
        this.banderaEsEdicion = false;
        this.contactoSeleccionado = new SolicitanteSolicitud();

    }

    // ------------------------------------------------------------------------------------
    /**
     * Carga la variable de contacto con un nuevo contacto
     *
     * @author christian.rodriguez
     */
    public void editarContacto() {
        this.banderaEsEdicion = true;
    }

    // ------------------------------------------------------------------------------------
    /**
     * encargado de asociar un contacto a los predios seleccionados
     *
     * @author christian.rodriguez
     */
    public void asociarContacto() {

        if (this.prediosSeleccionados != null && this.contactoSeleccionado != null) {

            for (SolicitudPredio predio : this.prediosSeleccionados) {
                predio.setContacto(this.contactoSeleccionado);

                if (predio.getAvaluoPredioId() != null) {
                    AvaluoPredio avaluoPredioAsociado = this.getAvaluosService()
                        .obtenerAvaluoPredioPorId(predio.getAvaluoPredioId());

                    avaluoPredioAsociado.setContacto(this.contactoSeleccionado);
                    this.getAvaluosService().guardarActualizarAvaluoPredio(avaluoPredioAsociado);
                }
            }

            this.getTramiteService().guardarActualizarSolicitudesPredios(this.prediosSeleccionados);

            this.cargarContactosAsociadosASolicitante();

            this.addMensajeInfo("Contacto asociado a los predios seleccionados satisfactoriamente.");

        } else {

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");

            this.addMensajeError("Debe seleccionar un contacto a asociar al o a los predios.");

        }

    }

    /**
     * Método que sirve para para cargar los datos requeridos por este caso de uso, se usa para que
     * este managedBean pueda ser reusado desde diferentes casos de uso (otros managedBeans)
     *
     * @author rodrigo.hernandez
     *
     * @param banderaModoLecturaHabilitado
     * @param solicitante
     * @param solicitud
     * @param listaContactos
     * @param usuario
     */
    public void cargarVariablesExternas(boolean banderaModoLecturaHabilitado,
        SolicitanteSolicitud solicitante, Solicitud solicitud,
        List<SolicitanteSolicitud> listaContactos, UsuarioDTO usuario) {

        this.solicitudSeleccionada = solicitud;
        this.solicitante = solicitante;

        this.usuario = usuario;

        // Se cargan los contactos asociados a la solicitud
        this.contactosSeleccionados = listaContactos;

        this.banderaEsEdicion = !banderaModoLecturaHabilitado;
    }

    // ------------------------------------------------------------------------------------
    /**
     * Método para convertir el array creado a partir de la seleccion de la tabla de predios en
     * administracion de predios
     *
     * @author rodrigo.hernandez
     */
    public void iniciarListaPrediosSeleccionados() {
        this.prediosSeleccionados = Arrays.asList(this.arrayPrediosSeleccionados);
        this.contactoSeleccionado = new SolicitanteSolicitud();
    }

}
