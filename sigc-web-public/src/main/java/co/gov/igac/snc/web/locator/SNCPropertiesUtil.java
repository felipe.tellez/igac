package co.gov.igac.snc.web.locator;

import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.gov.igac.snc.excepciones.ExcepcionSNC;
import co.gov.igac.snc.web.util.SNCWebServiceExceptions;

/**
 *
 * Clase utilitaria para la lectura del archivo .properties con la configuración para SNC
 *
 * @author juan.mendez
 * @version 2.0
 */
public class SNCPropertiesUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SNCPropertiesUtil.class);

    private static SNCPropertiesUtil instance;

    private ResourceBundle rs;

    /**
     *
     */
    private SNCPropertiesUtil() {
        try {
            rs = ResourceBundle.getBundle("application");

        } catch (Exception e) {
            throw SNCWebServiceExceptions.EXCEPCION_INFRAESTRUCTURA_0001.getExcepcion(LOGGER, e, e.
                getMessage());
        }

    }

    public static synchronized SNCPropertiesUtil getInstance() throws ExcepcionSNC {
        if (instance == null) {
            instance = new SNCPropertiesUtil();
        }
        return instance;
    }

    /**
     *
     * @param propertyName
     * @return
     */
    public String getProperty(String propertyName) {
        String propertyValue = null;
        try {
            propertyValue = rs.getString(propertyName);
        } catch (Exception e) {
            throw SNCWebServiceExceptions.EXCEPCION_INFRAESTRUCTURA_0001.getExcepcion(LOGGER, e, e.
                getMessage(),
                propertyName);
        }
        return propertyValue;
    }

}
