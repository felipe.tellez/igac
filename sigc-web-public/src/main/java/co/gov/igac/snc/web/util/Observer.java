package co.gov.igac.snc.web.util;

import java.util.List;

import co.gov.igac.snc.persistence.entity.conservacion.Predio;

//import co.gov.igac.snc.persistence.entity.conservacion.Predio;
public interface Observer {

    public void actualizar(List<Predio> predios);
}
