/**
 * IGAC proyecto SNC
 */
package co.gov.igac.snc.web.mb.conservacion.depuracion;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.persistence.entity.generales.EstructuraOrganizacional;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.objetosNegocio.SolicitudCatastral;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.comun.anotaciones.Implement;
import co.gov.igac.snc.ldap.ELDAPEstadoUsuario;
import co.gov.igac.snc.persistence.entity.tramite.TramiteDepuracion;
import co.gov.igac.snc.persistence.util.EEstructuraOrganizacional;
import co.gov.igac.snc.persistence.util.ERol;
import co.gov.igac.snc.util.EVisorGISLayer;
import co.gov.igac.snc.util.EVisorGISTools;
import co.gov.igac.snc.util.constantes.ConstantesNavegacionWeb;
import co.gov.igac.snc.web.listener.IContextListener;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.ActivityMessageDTO;
import co.gov.igac.snc.web.util.ProcessFlowManager;
import co.gov.igac.snc.web.util.UtilidadesWeb;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * MB para el caso de uso Asignar digitalizador o topografo
 *
 * @author felipe.cadena
 * @CU CU-SA-AC-202
 */
@Component("asignarDigitalizadorTopografo")
@Scope("session")
public class AsignacionDigitalizadorTopografoMB extends ProcessFlowManager {

    private static final long serialVersionUID = 5608198030936651458L;
    private static final Logger LOGGER =
        LoggerFactory.getLogger(AsignacionDigitalizadorTopografoMB.class);
    //------------------ services   ------
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    @Autowired
    private IContextListener contextService;
    /**
     * Se usa aquí solo para invocar su init al cerrar la ventana del cu
     */
    @Autowired
    private TareasPendientesMB tareasPendientes;
    //----- bpm
    /**
     * actividad actual
     */
    private Actividad currentProcessActivity;
    /**
     * usuario de la sesión
     */
    private UsuarioDTO usuario;
    /**
     * funcionario seleccionado
     */
    private UsuarioDTO funcionarioSelecionado;
    /**
     * Trámites seleccionados de la tabla de trámites sin ejecutor asignado
     */
    private TramiteDepuracion[] TramitesSeleccionados;
    /**
     * Trámites seleccionados de la tabla de trámites con ejecutor asignado
     */
    private TramiteDepuracion[] TramitesSeleccionadosAsignados;
    /**
     * Rol de usuario en sesión
     */
    private String rolUsuario;
    private List<TramiteDepuracion> tramites;
    /**
     * Listado de tramites para asignar.
     */
    private LazyDataModel<TramiteDepuracion> lazyTramites;
    /**
     * lista de los id de trámites que son el objeto de negocio de las instancias de actividades del
     * árbol
     */
    private long[] idsTramitesActividades;
    /**
     * Listado de tramites ya asignados.
     */
    private List<TramiteDepuracion> tramitesAsignados;
    /**
     * Listado de tramites para asignar.
     */
    private List<UsuarioDTO> funcionarios;
    /**
     * Banderas para el tipo de funcionario que se va a asignar.
     */
    private boolean banderaTopografo;
    private boolean banderaDigitalizador;
    private boolean banderaEjecutor;
    private String rol;
    /**
     * Nombre de las variables necesarias para llamar al mapa (Estas variables parametrizan las
     * capas y las herramientas del mapa)
     */
    private String moduleLayer;
    private String moduleTools;
    private String prediosParaZoom;
    /**
     * Nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;
    @Autowired
    private IContextListener contextoWeb;

    //---------------------Getters and Setters---------------------//
    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public LazyDataModel<TramiteDepuracion> getLazyTramites() {
        return lazyTramites;
    }

    public void setLazyTramites(LazyDataModel<TramiteDepuracion> lazyTramites) {
        this.lazyTramites = lazyTramites;
    }

    public List<TramiteDepuracion> getTramitesAsignados() {
        return tramitesAsignados;
    }

    public void setTramitesAsignados(List<TramiteDepuracion> tramitesAsignados) {
        this.tramitesAsignados = tramitesAsignados;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public UsuarioDTO getFuncionarioSelecionado() {
        return funcionarioSelecionado;
    }

    public void setFuncionarioSelecionado(UsuarioDTO funcionarioSelecionado) {
        this.funcionarioSelecionado = funcionarioSelecionado;
    }

    public TramiteDepuracion[] getTramitesSeleccionados() {
        return TramitesSeleccionados;
    }

    public void setTramitesSeleccionados(TramiteDepuracion[] TramitesSeleccionados) {
        this.TramitesSeleccionados = TramitesSeleccionados;
    }

    public TramiteDepuracion[] getTramitesSeleccionadosAsignados() {
        return TramitesSeleccionadosAsignados;
    }

    public void setTramitesSeleccionadosAsignados(TramiteDepuracion[] TramitesSeleccionadosAsignados) {
        this.TramitesSeleccionadosAsignados = TramitesSeleccionadosAsignados;
    }

    public String getModuleLayer() {
        return moduleLayer;
    }

    public void setModuleLayer(String moduleLayer) {
        this.moduleLayer = moduleLayer;
    }

    public String getModuleTools() {
        return moduleTools;
    }

    public void setModuleTools(String moduleTools) {
        this.moduleTools = moduleTools;
    }

    public String getPrediosParaZoom() {
        return prediosParaZoom;
    }

    public void setPrediosParaZoom(String prediosParaZoom) {
        this.prediosParaZoom = prediosParaZoom;
    }

    public List<TramiteDepuracion> getTramites() {
        return tramites;
    }

    public void setTramites(List<TramiteDepuracion> tramites) {
        this.tramites = tramites;
    }

    public List<UsuarioDTO> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<UsuarioDTO> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public boolean isBanderaTopografo() {
        return banderaTopografo;
    }

    public void setBanderaTopografo(boolean banderaTopografo) {
        this.banderaTopografo = banderaTopografo;
    }

    public boolean isBanderaDigitalizador() {
        return banderaDigitalizador;
    }

    public void setBanderaDigitalizador(boolean banderaDigitalizador) {
        this.banderaDigitalizador = banderaDigitalizador;
    }

    public boolean isBanderaEjecutor() {
        return banderaEjecutor;
    }

    public void setBanderaEjecutor(boolean banderaEjecutor) {
        this.banderaEjecutor = banderaEjecutor;
    }

    public String getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(String rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    @PostConstruct
    public void init() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#init ");

        //D: se setea este bean como el actual para lo de procesos
        this.registerCurrentManagedBean(this, "asignarDigitalizadorTopografo");

        if (this.tareasPendientesMB.getActividadesSeleccionadas() == null) {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
            return;
        }

        // D: se obtienen los ids de los trámites objetivo a partir del árbol de
        // tareas
        this.idsTramitesActividades = this.tareasPendientesMB
            .obtenerIdsTramitesDeInstanciasActividadesSeleccionadas();

        if (this.idsTramitesActividades != null) {
            this.currentProcessActivity = this.tareasPendientesMB.buscarActividadPorIdTramite(
                this.idsTramitesActividades[0]);
        } else {
            this.addMensajeError(
                "Error con respecto al árbol de tareas. Puede ser que no haya una actividad seleccionada");
        }

        this.lazyTramites = new LazyDataModel<TramiteDepuracion>() {
            @Override
            public List<TramiteDepuracion> load(int first, int pageSize,
                String sortField, SortOrder sortOrder, Map<String, String> filters) {
                List<TramiteDepuracion> answer = new ArrayList<TramiteDepuracion>();

                populateTramitesLazyly(answer, first, pageSize, sortField, sortOrder.toString(),
                    filters);

                tramitesAsignados = new ArrayList<TramiteDepuracion>();
                for (TramiteDepuracion tda : answer) {
                    if (tda.getDigitalizador() != null) {
                        tramitesAsignados.add(tda);
                    }
                    if (tda.getEjecutor() != null) {
                        tramitesAsignados.add(tda);
                    }
                    if (tda.getTopografo() != null) {
                        tramitesAsignados.add(tda);
                    }
                }

                for (int i = 0; i < tramitesAsignados.size(); i++) {
                    TramiteDepuracion td = tramitesAsignados.get(i);
                    Integer idx = null;
                    for (TramiteDepuracion tda : answer) {
                        if (tda.getId() == td.getId()) {
                            idx = answer.indexOf(tda);
                        }
                    }
                    if (idx != null) {
                        answer.remove(idx.intValue());
                    }
                }

                for (TramiteDepuracion td : answer) {
                    EstructuraOrganizacional eu = getGeneralesService().
                        obtenerEstructuraOrganizacionalPorMunicipioCodigo(td.getTramite().
                            getMunicipio().getCodigo());
                    td.setEstructuraOrganizacional(eu);
                }

                return answer;
            }
        };

        this.lazyTramites.setRowCount(this.idsTramitesActividades.length);

        this.tramitesAsignados = new ArrayList<TramiteDepuracion>();

        this.usuario = MenuMB.getMenu().getUsuarioDto();

        this.determinarRol();
        this.inicializarVariablesVisorGIS();

    }

    /**
     * hace la búsqueda de trámites para asignar ejecutor
     *
     * @param answer
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     *
     * @modified leidy.gonzalez
     */
    private void populateTramitesLazyly(List<TramiteDepuracion> answer, int first,
        int pageSize, String sortField, String sortOrder, Map<String, String> filters) {

        List<TramiteDepuracion> rows;
        int size;

        rows = this.getTramiteService().buscarTramitesDepuracionParaAsignacionDeFuncionario(
            this.idsTramitesActividades, sortField, sortOrder, filters, first, pageSize);

        if (rows == null) {
            this.addMensajeError("No existen trámites para asignar a depuración");
            return;
        }
        size = rows.size();

        //this.showTableWorkingTramites = (size > 0) ? true : false;
        for (int i = 0; i < size; i++) {
            answer.add(rows.get(i));
        }

    }

    /**
     * Método para inicializar las variables del visor GIS
     *
     * @author felipe.cadena
     */
    private void inicializarVariablesVisorGIS() {
        this.applicationClientName = this.contextoWeb.getClientAppNameUrl();

        this.moduleLayer = EVisorGISLayer.CONSERVACION.getCodigo();
        this.moduleTools = EVisorGISTools.NO_HERRAMIENTAS.getCodigo();
        this.prediosParaZoom = "";
    }

    /**
     * Método para determinar el tipo de rol de la actividad y en base a eso determinar el tipo de
     * funcionario que se va a asignar.
     *
     * @author felipe.cadena
     */
    public void determinarRol() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#determinarRol ");

        String actividad = this.currentProcessActivity.getNombre();

        if (actividad.equals(ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_DIGITALIZADOR.toString())) {

            this.rol = ERol.RESPONSABLE_SIG.toString();
            this.banderaDigitalizador = true;
            this.banderaEjecutor = false;
            this.banderaTopografo = false;
        }
        if (actividad.equals(ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_EJECUTOR.toString())) {

            this.rol = ERol.RESPONSABLE_CONSERVACION.toString();
            this.banderaDigitalizador = false;
            this.banderaEjecutor = true;
            this.banderaTopografo = false;
        }
        if (actividad.equals(ProcesoDeConservacion.ACT_DEPURACION_ASIGNAR_TOPOGRAFO.toString())) {

            this.rol = ERol.DIRECTOR_TERRITORIAL.toString();
            this.banderaDigitalizador = false;
            this.banderaEjecutor = false;
            this.banderaTopografo = true;
        }

        List<UsuarioDTO> funcionariosDisponibles = new ArrayList<UsuarioDTO>();
        if (this.banderaDigitalizador) {
            funcionariosDisponibles = this
                .getTramiteService().buscarFuncionariosDigitalizadoresPorTerritorialDTO(
                    this.usuario.getDescripcionEstructuraOrganizacional());
        }
        if (this.banderaEjecutor) {
            funcionariosDisponibles = this
                .getTramiteService().buscarFuncionariosEjecutoresPorTerritorialDTO(this.usuario
                    .getDescripcionEstructuraOrganizacional());
        }
        if (this.banderaTopografo) {
            funcionariosDisponibles = this
                .getTramiteService().buscarFuncionariosTopografosPorTerritorialDTO(this.usuario
                    .getDescripcionEstructuraOrganizacional());
        }

        //felipe.cadena::#15864::01-03-2016::Se valida que no esten disponibles
        //para asignacion los usuarios expirados o inactivos
        this.funcionarios = new ArrayList<UsuarioDTO>();
        for (UsuarioDTO usuarioDTO : funcionariosDisponibles) {
            if (!usuarioDTO.getFechaExpiracion().before(new Date()) &&
                 ELDAPEstadoUsuario.ACTIVO.codeExist(usuarioDTO.getEstado())) {
                this.funcionarios.add(usuarioDTO);
            }
        }

    }

    /**
     * Método para reiniciar las selecciones hechas previamente
     *
     * @param event
     */
    public void handleDialogClose(CloseEvent event) {
        LOGGER.debug("Cierra modal de asignación de funcionario");

        this.TramitesSeleccionados = null;
        this.funcionarioSelecionado = null;

    }

    /**
     * Método para preparar los números prediales de los predios asociados a los trámites
     * seleccionados
     *
     * @author felipe.cadena
     */
    public void prepararPrediosVisor() {
        LOGGER.debug("on AsignarDigitalizadorTopografoMB#prepararPrediosVisor");
        this.prediosParaZoom = "";

        for (TramiteDepuracion td : this.TramitesSeleccionados) {
            this.prediosParaZoom += td.getTramite().getPredio().getNumeroPredial() + ",";
        }

        this.prediosParaZoom = this.prediosParaZoom.substring(0, this.prediosParaZoom.length() - 1);
        LOGGER.debug("P zoom " + this.prediosParaZoom);
    }

    /**
     * Método para asignar tramites a un funcionario.
     *
     * @author felipe.cadena
     */
    public void asignarFuncionario() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#asignarFuncionario ");

        for (TramiteDepuracion td : this.TramitesSeleccionados) {
            if (this.banderaEjecutor) {
                td.setNombreEjecutor(this.funcionarioSelecionado.getNombreCompleto());
                td.setEjecutor(this.funcionarioSelecionado.getLogin());
            }
            if (this.banderaDigitalizador) {
                td.setNombreDigitalizador(this.funcionarioSelecionado.getNombreCompleto());
                td.setDigitalizador(this.funcionarioSelecionado.getLogin());
            }
            if (this.banderaTopografo) {
                td.setNombreTopografo(this.funcionarioSelecionado.getNombreCompleto());
                td.setTopografo(this.funcionarioSelecionado.getLogin());
            }
            this.getTramiteService().actualizarTramiteDepuracion(td);
        }
    }

    /**
     * Metodo que se llama desde el boton de asignar para determinar si los tramites son validos
     * para la asignacion
     *
     * @author felipe.cadena
     */
    public void validarPrediosSeleccionados() {

        if (!this.validarSeleccion()) {
            this.addMensajeError(
                "Solo se pueden asignar tramites que pertenezcan a la misma estructura organizacional");
            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("error", "error");
        }

        this.inicializarDigitalizadores(this.TramitesSeleccionados[0].getEstructuraOrganizacional());

    }

    /** --------------------------------------------------------------------- */
    /**
     * Método para cargar los digitalizadores disponibles en la territorial
     *
     * @author felipe.cadena
     */
    public void inicializarDigitalizadores(EstructuraOrganizacional eo) {

        if (this.banderaDigitalizador) {
            List<UsuarioDTO> dgs = new ArrayList<UsuarioDTO>();
            if (eo.getTipo().equals(EEstructuraOrganizacional.UNIDAD_OPERATIVA_CATASTRAL.getTipo())) {
                if (this.usuario.getDescripcionUOC() != null) {
                    dgs = this
                        .getTramiteService().buscarFuncionariosDigitalizadoresPorTerritorialDTO(
                            this.usuario.getDescripcionUOC());
                }
            }

            //si no existe digitalizador en la UOC se buscan en la territorial.
            if (dgs.isEmpty()) {
                dgs = this
                    .getTramiteService().buscarFuncionariosDigitalizadoresPorTerritorialDTO(
                        this.usuario.getDescripcionTerritorial());
            }

            //felipe.cadena::#15864::01-03-2016::Se valida que no esten disponibles
            //para asignacion los usuarios expirados o inactivos
            this.funcionarios = new ArrayList<UsuarioDTO>();

            for (UsuarioDTO usuarioDTO : dgs) {
                if (!usuarioDTO.getFechaExpiracion().before(new Date()) &&
                     ELDAPEstadoUsuario.ACTIVO.codeExist(usuarioDTO.getEstado())) {
                    this.funcionarios.add(usuarioDTO);
                }
            }

        }
    }

    /**
     * Metodo para validar la seleccion de los predios a asignar
     *
     * @author felipe.cadena
     * @return
     */
    private boolean validarSeleccion() {

        String estructuraInicial = "";

        if (this.TramitesSeleccionados.length > 1) {
            estructuraInicial = this.TramitesSeleccionados[0].getEstructuraOrganizacional().
                getCodigo();

            for (TramiteDepuracion td : this.TramitesSeleccionados) {

                String eo = td.getEstructuraOrganizacional().getCodigo();

                if (!eo.equals(estructuraInicial)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Método para desasignar tramites a un funcionario.
     *
     * @author felipe.cadena
     */
    public void desasignarFuncionario() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#desasignarFuncionario ");

        for (TramiteDepuracion td : this.TramitesSeleccionadosAsignados) {
            if (this.banderaEjecutor) {
                td.setNombreEjecutor(null);
                td.setEjecutor(null);
            }
            if (this.banderaDigitalizador) {
                td.setNombreDigitalizador(null);
                td.setDigitalizador(null);
            }
            if (this.banderaTopografo) {
                td.setNombreTopografo(null);
                td.setTopografo(null);
            }
            this.getTramiteService().actualizarTramiteDepuracion(td);
        }
    }

    /**
     * Método para avanzar el proceso despues de la asignación.
     *
     * @author felipe.cadena
     */
    public String avanzarProceso() {

        LOGGER.debug("on AsignarDigitalizadorTopografoMB#avanzarProceso ");
        this.setupProcessMessage();
        this.forwardProcess();

        //D: hay que hacer esto aquí porque no se puede factorizar. (me da pereza explicar por qué)
        //D: forzar el init cuando vuelva a seleccionar la actividad en el árbol
        UtilidadesWeb.removerManagedBean("estableceProcedenciaSolicitud");
        //D: forzar el refrezco del árbol de tareas
        this.tareasPendientesMB.init();

        return ConstantesNavegacionWeb.INDEX;

    }

    /**
     * Método encargado de terminar la sesión y redirigir a la pantalla principal
     *
     * @author felipe.cadena
     */
    public String cerrarVentanaCU() {

        this.tareasPendientes.init();

        return ConstantesNavegacionWeb.INDEX;
    }

    //--------------------------------------------------------------------------------------------------
    //----------- bpm
    /**
     *
     */
    @Implement
    public boolean validateProcess() {
        return true;
    }
//-------------------------------------------------------------------------------

    @Implement
    public void setupProcessMessage() {
//TODO :: felipe.cadena::definir detalles del proceso para el subproceso de depuración y la actividad en particular

        Actividad activity;
        ActivityMessageDTO message = new ActivityMessageDTO();
        ActivityMessageDTO messageDTO, messageDTODefault;
        SolicitudCatastral solicitudCatastral = new SolicitudCatastral();
        List<UsuarioDTO> usuariosActividad = new ArrayList<UsuarioDTO>();
        List<ActivityMessageDTO> processMessages = new ArrayList<ActivityMessageDTO>();
        String transitionName = "";
        String observaciones = "";

        UsuarioDTO usuarioObjetivoActividad;

        if (this.getMessage() != null) {
            messageDTODefault = this.getMessage();
            observaciones = messageDTODefault.getComment();
        }

        for (TramiteDepuracion currentTD : this.tramitesAsignados) {
            messageDTO = new ActivityMessageDTO();
            usuarioObjetivoActividad = new UsuarioDTO();
            solicitudCatastral = new SolicitudCatastral();
            usuariosActividad = new ArrayList<UsuarioDTO>();

            activity = this.tareasPendientesMB.buscarActividadPorIdTramite(currentTD.getTramite().
                getId());
            messageDTO.setComment(observaciones);
            messageDTO.setActivityId(activity.getId());

            if (this.rol.equals(ERol.RESPONSABLE_SIG.toString())) {

                usuarioObjetivoActividad = this.getGeneralesService().getCacheUsuario(currentTD.
                    getDigitalizador());
                usuariosActividad.add(usuarioObjetivoActividad);
                transitionName = ProcesoDeConservacion.ACT_DEPURACION_REVISAR_TAREAS_POR_REALIZAR;
            } else if (this.rol.equals(ERol.RESPONSABLE_CONSERVACION.toString())) {

                usuarioObjetivoActividad = this.usuario;
                usuariosActividad.add(usuarioObjetivoActividad);

                transitionName = ProcesoDeConservacion.ACT_DEPURACION_SOLICITAR_COMISION_EJECUTOR;
            } else {

                usuarioObjetivoActividad = this.getGeneralesService().getCacheUsuario(currentTD.
                    getTopografo());
                usuariosActividad.add(usuarioObjetivoActividad);

                transitionName = ProcesoDeConservacion.ACT_DEPURACION_EVALUAR_INFO_ASOCIADA;
            }

            solicitudCatastral.setUsuarios(usuariosActividad);
            solicitudCatastral.setTransicion(transitionName);
            messageDTO.setUsuarioActual(this.usuario);
            //solicitudCatastral.setNumeroRadicacion(currentTramite.getNumeroRadicacion());
            messageDTO.setSolicitudCatastral(solicitudCatastral);

            processMessages.add(messageDTO);

        }

        //message.setActivityId(this.currentProcessActivity.getId());
        // solicitudCatastral.setUsuarios(usuariosActividad);
        // message.setTransition(transitionName);
        // solicitudCatastral.setTransicion(transitionName);
        //message.setSolicitudCatastral(solicitudCatastral);
        if (!processMessages.isEmpty()) {
            this.setMasiveMessages(processMessages);
        }
    }
//--------------------------------------------------

    /**
     *
     */
    @Implement
    public void doDatabaseStatesUpdate() {
        // do nothing
    }
}
