package co.gov.igac.snc.web.mb.actualizacion.alistamiento;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.gov.igac.generales.dto.UsuarioDTO;
import co.gov.igac.snc.apiprocesos.nucleoPredial.conservacion.ProcesoDeConservacion;
import co.gov.igac.snc.apiprocesos.tareas.comun.Actividad;
import co.gov.igac.snc.persistence.entity.actualizacion.Actualizacion;
import co.gov.igac.snc.persistence.entity.actualizacion.SaldoConservacion;
import co.gov.igac.snc.persistence.entity.tramite.Tramite;
import co.gov.igac.snc.web.mb.generales.MenuMB;
import co.gov.igac.snc.web.mb.tareas.TareasPendientesMB;
import co.gov.igac.snc.web.util.SNCManagedBean;
import co.gov.igac.snc.web.util.UtilidadesWeb;

/**
 * @author lorena.salamanca
 * @modified javier.barajas modificacion y creacion de funciones de interaccion con la capa de
 * negocios
 * @cu 315 Gestion de Tramites a Conservación
 */
@Component("gestionarEntregaTramitesConservacion")
@Scope("session")
public class GestionEntregaTramitesConservacionMB extends SNCManagedBean {

    private static final long serialVersionUID = -446391752734179640L;

    private static final Logger LOGGER = LoggerFactory.getLogger(
        GestionEntregaTramitesConservacionMB.class);

    /**
     * managed bean que maneja la interacción con el árbol de tareas
     */
    @Autowired
    private TareasPendientesMB tareasPendientesMB;
    /**
     * actividad actual del árbol de actividades
     */
    private Actividad currentActivity;

    private UsuarioDTO usuario;

    private Actualizacion actualizacion;

    /**
     * nombre de la aplicación web (esta aplicacion). Se toma de un archivo de configuración, y se
     * usa para pasársela como parámetro a la función js del mapa
     */
    private String applicationClientName;

    /** Listas para mostrar los saldos de tramites de conservacion */
    private List<Actividad> saldosActividades;
    private List<Tramite> saldosTramites;
    private List<Tramite> selectedTramites;

    //---------------------------------------------------------------------------------------------
    //Métodos
    //---------------------------------------------------------------------------------------------
    public Actividad getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Actividad currentActivity) {
        this.currentActivity = currentActivity;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Actualizacion getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(Actualizacion actualizacion) {
        this.actualizacion = actualizacion;
    }

    public List<Actividad> getSaldosActividades() {
        return saldosActividades;
    }

    public void setSaldosActividades(List<Actividad> saldosActividades) {
        this.saldosActividades = saldosActividades;
    }

    public List<Tramite> getSaldosTramites() {
        return saldosTramites;
    }

    public void setSaldosTramites(List<Tramite> saldosTramites) {
        this.saldosTramites = saldosTramites;
    }

    public String getApplicationClientName() {
        return applicationClientName;
    }

    public void setApplicationClientName(String applicationClientName) {
        this.applicationClientName = applicationClientName;
    }

    public List<Tramite> getSelectedTramites() {
        return selectedTramites;
    }

    public void setSelectedTramites(List<Tramite> selectedTramites) {
        this.selectedTramites = selectedTramites;
    }

    //---------------------------------------------------------------------------------------------
    @PostConstruct
    public void init() {

        this.usuario = MenuMB.getMenu().getUsuarioDto();
        Long idActualizacion = 250L;
        this.actualizacion = this.getActualizacionService().
            recuperarActualizacionPorId(idActualizacion);

        this.saldosActividades = this.getProcesosService().consultarListaActividades(
            this.usuario.getDescripcionTerritorial(),
            ProcesoDeConservacion.ACT_ASIGNACION_ASIGNAR_TRAMITES);
        LOGGER.debug("------------tAMAÑO DEL SALDO ACTIVIDADES:" + this.saldosActividades.size());
        List<Long> idObjetosNegocio = new LinkedList<Long>();
        /* for(Actividad a : this.saldosActividades){ idObjetosNegocio.add(a.getIdObjetoNegocio());			
		} */
        idObjetosNegocio.add(43389L);
        idObjetosNegocio.add(43390L);
        idObjetosNegocio.add(43388L);
        idObjetosNegocio.add(43389L);
        idObjetosNegocio.add(43382L);
        idObjetosNegocio.add(43368L);
        idObjetosNegocio.add(43324L);

        this.saldosTramites = this.getTramiteService().obtenerSaldosDeConservacion(idObjetosNegocio);

        this.applicationClientName = this.getContexto().getClientAppNameUrl();
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Metodo qeu persiste los saldos de Conservacion y los asigna a una actualizacion
     */
    public String entregarTramitesAConservacion() {

        List<SaldoConservacion> saldosConservacion = new LinkedList<SaldoConservacion>();

        for (Tramite tram : this.selectedTramites) {
            SaldoConservacion saldo = new SaldoConservacion();
            saldo.setActualizacion(this.actualizacion);
            saldo.setFechaLog(new Date());
            saldo.setTramite(tram);

            saldosConservacion.add(saldo);

            tram.setFuncionarioEjecutor(
                this.actualizacion.getActualizacionResponsables().get(0).getUsuarioLog());
            tram.setNombreFuncionarioEjecutor(
                this.actualizacion.getActualizacionResponsables().get(0).getNombre());
            this.getTramiteService().actualizarTramite(tram);
        }

        saldosConservacion = this.getActualizacionService().
            guardarYActualizarSaldosDeConservacion(saldosConservacion);

        UtilidadesWeb.removerManagedBean("gestionarTramitesConservacion");
        this.tareasPendientesMB.init();
        return "index";
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Método que cierra la pantalla
     */
    public String cerrar() {
        UtilidadesWeb.removerManagedBean("gestionarTramitesConservacion");
        this.tareasPendientesMB.init();
        return "index";
    }
}
